package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.camelgames.framework.network.AsyncHttpRequest;
import com.mobclix.android.sdk.Mobclix;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class MobclixCreative extends ViewFlipper {
    private static final String TAG = "MobclixCreative";
    /* access modifiers changed from: private */
    public static boolean isPlaying = false;
    Action action;
    /* access modifiers changed from: private */
    public Stack<Thread> asyncRequestThreads = new Stack<>();
    private String creativeId = "";
    /* access modifiers changed from: private */
    public Thread customAdThread;
    final ResourceResponseHandler handler = new ResourceResponseHandler();
    /* access modifiers changed from: private */
    public boolean hasAutoplayed = false;
    private boolean initialized = false;
    private MobclixInstrumentation instrumentation = MobclixInstrumentation.getInstance();
    /* access modifiers changed from: private */
    public boolean loop = true;
    /* access modifiers changed from: private */
    public int numPages = 1;
    private ArrayList<String> onLoadUrls = new ArrayList<>();
    private ArrayList<String> onTouchUrls = new ArrayList<>();
    final PageCycleHandler pageCycleHandler = new PageCycleHandler();
    /* access modifiers changed from: private */
    public Timer pageCycleTimer = null;
    MobclixAdView parentAdView;
    boolean trackingPixelsFired = false;
    private int transitionTime = 3000;
    private String transitionType = "none";
    private String type = "";
    /* access modifiers changed from: private */
    public int visiblePage = 0;

    MobclixCreative(MobclixAdView a, JSONObject responseObject, boolean ap) {
        super(a.getContext());
        this.parentAdView = a;
        String instrPath = this.instrumentation.benchmarkStart(this.instrumentation.startGroup(this.parentAdView.instrumentationGroup, MobclixInstrumentation.ADVIEW), "handle_response");
        requestDisallowInterceptTouchEvent(true);
        if (responseObject == null) {
            addView(new CustomAdPage(this));
            this.numPages = 1;
            this.type = "customAd";
            this.initialized = true;
            return;
        }
        String instrPath2 = this.instrumentation.benchmarkStart(instrPath, "b_build_models");
        JSONObject creative = responseObject;
        try {
            JSONObject eventUrls = creative.getJSONObject("eventUrls");
            try {
                JSONArray t = eventUrls.getJSONArray("onShow");
                for (int i = 0; i < t.length(); i++) {
                    this.onLoadUrls.add(t.getString(i));
                }
            } catch (Exception e) {
            }
            JSONArray t2 = eventUrls.getJSONArray("onTouch");
            for (int i2 = 0; i2 < t2.length(); i2++) {
                this.onTouchUrls.add(t2.getString(i2));
            }
        } catch (Exception e2) {
        }
        try {
            instrPath = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkStart(this.instrumentation.benchmarkFinishPath(instrPath2), "c_build_creative"), "a_determine_type");
            JSONObject properties = creative.getJSONObject("props");
            try {
                this.creativeId = creative.getString("id");
            } catch (JSONException e3) {
            }
            this.type = creative.getString("type");
            String instrPath3 = this.instrumentation.benchmarkFinishPath(instrPath);
            try {
                this.hasAutoplayed = ap;
                this.action = new Action(creative.getJSONObject("action"), this);
            } catch (Exception e4) {
                this.action = new Action(this);
            }
            instrPath = this.instrumentation.benchmarkStart(instrPath3, "b_get_view");
            if (this.type.equals("html")) {
                HTMLPage p = this.parentAdView.mHTMLPagePool.getHTMLPage(this);
                p.loadAd(properties.getString("html"));
                addView(p);
                this.numPages = 1;
                this.initialized = true;
            } else if (this.type.equals("openallocation")) {
                addView(new OpenAllocationPage(properties, this));
                this.numPages = 1;
                this.initialized = true;
            } else {
                try {
                    this.transitionType = properties.getString("transitionType");
                } catch (JSONException e5) {
                }
                setAnimationType(this, this.transitionType);
                try {
                    this.transitionTime = (int) (properties.getDouble("transitionTime") * 1000.0d);
                } catch (JSONException e6) {
                }
                if (this.transitionTime == 0) {
                    this.transitionTime = 3000;
                }
                try {
                    this.loop = properties.getBoolean("loop");
                } catch (JSONException e7) {
                }
                if (this.type.equals("image")) {
                    JSONArray t3 = properties.getJSONArray("images");
                    this.numPages = t3.length();
                    for (int i3 = 0; i3 < t3.length(); i3++) {
                        addView(new ImagePage(t3.getString(i3), this));
                    }
                } else if (this.type.equals("text")) {
                    JSONArray t4 = properties.getJSONArray("texts");
                    this.numPages = t4.length();
                    for (int i4 = 0; i4 < t4.length(); i4++) {
                        addView(new TextPage(t4.getJSONObject(i4), this));
                    }
                }
                String instrPath4 = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkFinishPath(instrPath), "f_load_ad_creative");
                runNextAsyncRequest();
                String instrPath5 = this.instrumentation.benchmarkFinishPath(instrPath4);
                this.initialized = true;
            }
        } catch (JSONException e8) {
            String instrPath6 = this.instrumentation.benchmarkFinishPath(this.instrumentation.benchmarkFinishPath(this.instrumentation.benchmarkFinishPath(instrPath)));
            this.instrumentation.finishGroup(this.parentAdView.instrumentationGroup);
        }
    }

    public String getType() {
        return this.type;
    }

    public String getCreativeId() {
        return this.creativeId;
    }

    public boolean getHasAutoplayed() {
        return this.hasAutoplayed;
    }

    public boolean isInitialized() {
        return this.initialized;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 7) {
                try {
                    super.onDetachedFromWindow();
                    super.stopFlipping();
                } catch (IllegalArgumentException e) {
                    Log.w(TAG, "Android project  issue 6191  workaround.");
                    super.stopFlipping();
                } catch (Throwable th) {
                    super.stopFlipping();
                    throw th;
                }
            } else {
                super.onDetachedFromWindow();
            }
        } catch (Exception e2) {
            super.onDetachedFromWindow();
        }
    }

    private int dp(int p) {
        return (int) (this.parentAdView.scale * ((float) p));
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public void onPause() {
        synchronized (this) {
            if (this.pageCycleTimer != null) {
                this.pageCycleTimer.cancel();
                this.pageCycleTimer.purge();
            }
        }
        try {
            if (getCurrentView().getClass() == HTMLPage.class) {
                MobclixJavascriptInterface i = ((HTMLPage) getCurrentView()).webview.getJavascriptInterface();
                i.pauseListeners();
                if (!i.expanded) {
                    i.adWillBecomeHidden();
                }
            }
        } catch (Exception e) {
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public void onResume() {
        synchronized (this) {
            if (this.pageCycleTimer != null) {
                this.pageCycleTimer.cancel();
                this.pageCycleTimer.purge();
                this.pageCycleTimer = new Timer();
                this.pageCycleTimer.scheduleAtFixedRate(new PageCycleThread(), (long) this.transitionTime, (long) this.transitionTime);
            }
        }
        try {
            if (getCurrentView().getClass() == HTMLPage.class) {
                MobclixJavascriptInterface i = ((HTMLPage) getCurrentView()).webview.getJavascriptInterface();
                i.resumeListeners();
                if (!i.expanded && i.expanderActivity == null) {
                    i.adDidReturnFromHidden();
                }
                i.expanded = false;
            }
        } catch (Exception e) {
        }
    }

    public void onStop() {
        onPause();
        try {
            if (getCurrentView().getClass() == HTMLPage.class) {
                ((HTMLPage) getCurrentView()).webview.getJavascriptInterface().adWillTerminate();
            }
        } catch (Exception e) {
        }
    }

    public boolean onTouchEvent(MotionEvent e) {
        try {
            if (this.type.equals("html")) {
                return false;
            }
            if (e.getAction() == 0) {
                fireOnTouchTrackingPixels();
                return this.action.act();
            }
            return false;
        } catch (Exception e2) {
        }
    }

    public void runNextAsyncRequest() {
        if (!this.asyncRequestThreads.isEmpty()) {
            this.asyncRequestThreads.pop().start();
            return;
        }
        this.parentAdView.getNextAdAttempts = 0;
        this.parentAdView.utilityView.loaded = true;
        this.parentAdView.utilityView.bringToFront();
        String instrPath = this.instrumentation.benchmarkStart(this.instrumentation.startGroup(this.parentAdView.instrumentationGroup, MobclixInstrumentation.ADVIEW), "handle_response");
        this.visiblePage = 0;
        String instrPath2 = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkStart(this.instrumentation.benchmarkStart(instrPath, "c_build_creative"), "b_get_view"), "c_deque_view");
        while (this.parentAdView.getChildCount() > 1) {
            if (this.parentAdView.getChildAt(0) == this.parentAdView.prevAd) {
                try {
                    MobclixCreative c = (MobclixCreative) this.parentAdView.getChildAt(1);
                    for (int i = 0; i < c.getChildCount(); i++) {
                        try {
                            ((Page) c.getChildAt(i)).dealloc();
                        } catch (Exception e) {
                        }
                    }
                } catch (Exception e2) {
                }
                this.parentAdView.removeViewAt(1);
            } else {
                try {
                    MobclixCreative c2 = (MobclixCreative) this.parentAdView.getChildAt(0);
                    for (int i2 = 0; i2 < c2.getChildCount(); i2++) {
                        try {
                            ((Page) c2.getChildAt(i2)).dealloc();
                        } catch (Exception e3) {
                        }
                    }
                } catch (Exception e4) {
                }
                this.parentAdView.removeViewAt(0);
            }
        }
        String instrPath3 = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkFinishPath(this.instrumentation.benchmarkFinishPath(instrPath2)), "e_add_view");
        this.parentAdView.addView(this);
        String instrPath4 = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkFinishPath(this.instrumentation.benchmarkFinishPath(instrPath3)), "d_bring_onscreen");
        if (this.parentAdView.prevAd != null) {
            this.parentAdView.prevAd.onStop();
            if (this.parentAdView.rotate) {
                setAnimationType(this.parentAdView, "flipRight");
            }
        }
        this.parentAdView.showNext();
        String instrPath5 = this.instrumentation.benchmarkFinishPath(instrPath4);
        if (this.numPages > 1) {
            onPause();
            this.pageCycleTimer = new Timer();
            this.pageCycleTimer.scheduleAtFixedRate(new PageCycleThread(), (long) this.transitionTime, (long) this.transitionTime);
        }
        String instrPath6 = this.instrumentation.benchmarkStart(instrPath5, "e_trigger_events");
        if (this.parentAdView.getVisibility() == 0 && !this.type.equals("html")) {
            fireOnShowTrackingPixels();
        }
        String instrPath7 = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkFinishPath(instrPath6), "f_notify_delegates");
        Iterator<MobclixAdViewListener> it = this.parentAdView.listeners.iterator();
        while (it.hasNext()) {
            MobclixAdViewListener listener = it.next();
            if (listener != null) {
                listener.onSuccessfulLoad(this.parentAdView);
            }
        }
        this.parentAdView.lastAdLoad = 0;
        String instrPath8 = this.instrumentation.benchmarkStart(this.instrumentation.benchmarkFinishPath(instrPath7), "h_handle_autoplay");
        if (this.action != null && this.action.getAutoplay() && this.parentAdView.allowAutoplay() && !this.hasAutoplayed && !isPlaying && Mobclix.getInstance().hasBeenIntervalSinceLastAutoplay(this.parentAdView.size)) {
            this.hasAutoplayed = true;
            this.action.act();
            MobclixAdView.lastAutoplayTime.put(this.parentAdView.size, Long.valueOf(System.currentTimeMillis()));
        }
        String instrPath9 = this.instrumentation.benchmarkFinishPath(this.instrumentation.benchmarkFinishPath(instrPath8));
        this.instrumentation.finishGroup(this.parentAdView.instrumentationGroup);
    }

    /* access modifiers changed from: package-private */
    public void fireOnShowTrackingPixels() {
        Iterator<String> it = this.onLoadUrls.iterator();
        while (it.hasNext()) {
            new Thread(new Mobclix.FetchImageThread(it.next(), new Mobclix.BitmapHandler())).start();
        }
        this.trackingPixelsFired = true;
    }

    /* access modifiers changed from: package-private */
    public void fireOnTouchTrackingPixels() {
        Iterator<String> it = this.onTouchUrls.iterator();
        while (it.hasNext()) {
            new Thread(new Mobclix.FetchImageThread(it.next(), new Mobclix.BitmapHandler())).start();
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v6, types: [android.view.animation.Animation] */
    /* JADX WARN: Type inference failed for: r2v49, types: [com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation] */
    /* JADX WARN: Type inference failed for: r2v50, types: [com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation] */
    /* JADX WARN: Type inference failed for: r2v51, types: [android.view.animation.TranslateAnimation] */
    /* JADX WARN: Type inference failed for: r2v52, types: [android.view.animation.TranslateAnimation] */
    /* JADX WARN: Type inference failed for: r2v53, types: [android.view.animation.TranslateAnimation] */
    /* JADX WARN: Type inference failed for: r0v8 */
    /* JADX WARN: Type inference failed for: r0v9 */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    public void setAnimationType(android.widget.ViewFlipper r12, java.lang.String r13) {
        /*
            r11 = this;
            if (r13 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.lang.String r2 = "fade"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x002c
            android.view.animation.AlphaAnimation r0 = new android.view.animation.AlphaAnimation
            r2 = 1065353216(0x3f800000, float:1.0)
            r3 = 0
            r0.<init>(r2, r3)
            android.view.animation.AlphaAnimation r1 = new android.view.animation.AlphaAnimation
            r2 = 0
            r3 = 1065353216(0x3f800000, float:1.0)
            r1.<init>(r2, r3)
        L_0x001b:
            r2 = 300(0x12c, double:1.48E-321)
            r0.setDuration(r2)
            r2 = 300(0x12c, double:1.48E-321)
            r1.setDuration(r2)
            r12.setOutAnimation(r0)
            r12.setInAnimation(r1)
            goto L_0x0002
        L_0x002c:
            java.lang.String r2 = "slideRight"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x0051
            android.view.animation.TranslateAnimation r0 = new android.view.animation.TranslateAnimation
            r1 = 2
            r2 = 0
            r3 = 2
            r4 = 1065353216(0x3f800000, float:1.0)
            r5 = 2
            r6 = 0
            r7 = 2
            r8 = 0
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            android.view.animation.TranslateAnimation r1 = new android.view.animation.TranslateAnimation
            r2 = 2
            r3 = -1082130432(0xffffffffbf800000, float:-1.0)
            r4 = 2
            r5 = 0
            r6 = 2
            r7 = 0
            r8 = 2
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x001b
        L_0x0051:
            java.lang.String r2 = "slideLeft"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x0077
            android.view.animation.TranslateAnimation r0 = new android.view.animation.TranslateAnimation
            r3 = 2
            r4 = 0
            r5 = 2
            r6 = -1082130432(0xffffffffbf800000, float:-1.0)
            r7 = 2
            r8 = 0
            r9 = 2
            r10 = 0
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
            android.view.animation.TranslateAnimation r1 = new android.view.animation.TranslateAnimation
            r2 = 2
            r3 = 1065353216(0x3f800000, float:1.0)
            r4 = 2
            r5 = 0
            r6 = 2
            r7 = 0
            r8 = 2
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x001b
        L_0x0077:
            java.lang.String r2 = "slideUp"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x009e
            android.view.animation.TranslateAnimation r0 = new android.view.animation.TranslateAnimation
            r3 = 2
            r4 = 0
            r5 = 2
            r6 = 0
            r7 = 2
            r8 = 0
            r9 = 2
            r10 = -1082130432(0xffffffffbf800000, float:-1.0)
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
            android.view.animation.TranslateAnimation r1 = new android.view.animation.TranslateAnimation
            r2 = 2
            r3 = 0
            r4 = 2
            r5 = 0
            r6 = 2
            r7 = 1065353216(0x3f800000, float:1.0)
            r8 = 2
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x001b
        L_0x009e:
            java.lang.String r2 = "slideDown"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x00c5
            android.view.animation.TranslateAnimation r0 = new android.view.animation.TranslateAnimation
            r3 = 2
            r4 = 0
            r5 = 2
            r6 = 0
            r7 = 2
            r8 = 0
            r9 = 2
            r10 = 1065353216(0x3f800000, float:1.0)
            r2 = r0
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
            android.view.animation.TranslateAnimation r1 = new android.view.animation.TranslateAnimation
            r2 = 2
            r3 = 0
            r4 = 2
            r5 = 0
            r6 = 2
            r7 = -1082130432(0xffffffffbf800000, float:-1.0)
            r8 = 2
            r9 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x001b
        L_0x00c5:
            java.lang.String r2 = "flipRight"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x0117
            com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation r0 = new com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation
            r4 = 0
            r5 = 1119092736(0x42b40000, float:90.0)
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getWidth()
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r6 = r2 / r3
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getHeight()
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r7 = r2 / r3
            r8 = 0
            r9 = 1
            r2 = r0
            r3 = r11
            r2.<init>(r4, r5, r6, r7, r8, r9)
            com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation r1 = new com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation
            r3 = -1028390912(0xffffffffc2b40000, float:-90.0)
            r4 = 0
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getWidth()
            float r2 = (float) r2
            r5 = 1073741824(0x40000000, float:2.0)
            float r5 = r2 / r5
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getHeight()
            float r2 = (float) r2
            r6 = 1073741824(0x40000000, float:2.0)
            float r6 = r2 / r6
            r7 = 0
            r8 = 0
            r2 = r11
            r1.<init>(r3, r4, r5, r6, r7, r8)
            r2 = 300(0x12c, double:1.48E-321)
            r1.setStartOffset(r2)
            goto L_0x001b
        L_0x0117:
            java.lang.String r2 = "flipLeft"
            boolean r2 = r13.equals(r2)
            if (r2 == 0) goto L_0x0002
            com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation r0 = new com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation
            r4 = 0
            r5 = -1028390912(0xffffffffc2b40000, float:-90.0)
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getWidth()
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r6 = r2 / r3
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getHeight()
            float r2 = (float) r2
            r3 = 1073741824(0x40000000, float:2.0)
            float r7 = r2 / r3
            r8 = 0
            r9 = 1
            r2 = r0
            r3 = r11
            r2.<init>(r4, r5, r6, r7, r8, r9)
            com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation r1 = new com.mobclix.android.sdk.MobclixCreative$Rotate3dAnimation
            r3 = 1119092736(0x42b40000, float:90.0)
            r4 = 0
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getWidth()
            float r2 = (float) r2
            r5 = 1073741824(0x40000000, float:2.0)
            float r5 = r2 / r5
            com.mobclix.android.sdk.MobclixAdView r2 = r11.parentAdView
            int r2 = r2.getHeight()
            float r2 = (float) r2
            r6 = 1073741824(0x40000000, float:2.0)
            float r6 = r2 / r6
            r7 = 0
            r8 = 0
            r2 = r11
            r1.<init>(r3, r4, r5, r6, r7, r8)
            r2 = 300(0x12c, double:1.48E-321)
            r1.setStartOffset(r2)
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixCreative.setAnimationType(android.widget.ViewFlipper, java.lang.String):void");
    }

    class ResourceResponseHandler extends Handler {
        ResourceResponseHandler() {
        }

        public void handleMessage(Message msg) {
            try {
                MobclixCreative.this.runNextAsyncRequest();
            } catch (Exception e) {
            }
        }
    }

    class PageCycleHandler extends Handler {
        PageCycleHandler() {
        }

        public void handleMessage(Message msg) {
            int nextPage = MobclixCreative.this.visiblePage + 1;
            if (nextPage >= MobclixCreative.this.numPages) {
                if (!MobclixCreative.this.loop) {
                    MobclixCreative.this.pageCycleTimer.cancel();
                    return;
                }
                nextPage = 0;
            }
            MobclixCreative.this.visiblePage = nextPage;
            MobclixCreative.this.showNext();
        }
    }

    class PageCycleThread extends TimerTask {
        PageCycleThread() {
        }

        public void run() {
            MobclixCreative.this.pageCycleHandler.sendEmptyMessage(0);
        }
    }

    private class CustomAdThread implements Runnable {
        private String url;

        CustomAdThread(String fetchUrl) {
            this.url = fetchUrl;
        }

        /* JADX WARN: Type inference failed for: r2v3, types: [java.net.URLConnection] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r4 = this;
                r1 = 0
                java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x0029, all -> 0x002e }
                java.lang.String r3 = r4.url     // Catch:{ Exception -> 0x0029, all -> 0x002e }
                r2.<init>(r3)     // Catch:{ Exception -> 0x0029, all -> 0x002e }
                java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Exception -> 0x0029, all -> 0x002e }
                r0 = r2
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0029, all -> 0x002e }
                r1 = r0
                java.lang.String r2 = "GET"
                r1.setRequestMethod(r2)     // Catch:{ Exception -> 0x0029, all -> 0x002e }
                java.lang.String r2 = "User-Agent"
                com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.Mobclix.getInstance()     // Catch:{ Exception -> 0x0029, all -> 0x002e }
                java.lang.String r3 = r3.getUserAgent()     // Catch:{ Exception -> 0x0029, all -> 0x002e }
                r1.setRequestProperty(r2, r3)     // Catch:{ Exception -> 0x0029, all -> 0x002e }
                r1.connect()     // Catch:{ Exception -> 0x0029, all -> 0x002e }
                r1.disconnect()
            L_0x0028:
                return
            L_0x0029:
                r2 = move-exception
                r1.disconnect()
                goto L_0x0028
            L_0x002e:
                r2 = move-exception
                r1.disconnect()
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixCreative.CustomAdThread.run():void");
        }
    }

    static class Page extends RelativeLayout {
        protected HashMap<String, Integer> alignmentMap = new HashMap<>();
        protected int layer;
        protected MobclixCreative parentCreative = null;
        protected int resourceId;
        protected String type;

        Page(Context c) {
            super(c);
            this.alignmentMap.put("center", 17);
            this.alignmentMap.put("left", 19);
            this.alignmentMap.put("right", 21);
        }

        Page(MobclixCreative c) {
            super(c.getContext());
            this.alignmentMap.put("center", 17);
            this.alignmentMap.put("left", 19);
            this.alignmentMap.put("right", 21);
            this.parentCreative = c;
            try {
                ViewParent v = this.parentCreative.parentAdView.utilityView.getParent();
                if (v != null) {
                    ((ViewGroup) v).removeView(this.parentCreative.parentAdView.utilityView);
                }
                addView(this.parentCreative.parentAdView.utilityView);
            } catch (Exception e) {
            }
            this.parentCreative.parentAdView.utilityView.loaded = false;
            this.parentCreative.parentAdView.utilityView.undisplayed = true;
        }

        public MobclixCreative getParentCreative() {
            return this.parentCreative;
        }

        public void setResourceId(int r) {
            this.resourceId = r;
        }

        public void setLayer(int l) {
            this.layer = l;
        }

        public void setType(String t) {
            this.type = t;
        }

        public int getResourceId() {
            return this.resourceId;
        }

        public int getLayer() {
            return this.layer;
        }

        public String getType() {
            return this.type;
        }

        public int getColorFromJSON(JSONObject c) {
            try {
                return Color.argb(c.getInt("a"), c.getInt("r"), c.getInt("g"), c.getInt("b"));
            } catch (JSONException e) {
                return 0;
            }
        }

        /* access modifiers changed from: package-private */
        public int dp(int p) {
            return (int) (this.parentCreative.parentAdView.scale * ((float) p));
        }

        /* access modifiers changed from: package-private */
        public void dealloc() {
        }
    }

    private static class TextPage extends Page {
        private String bAlign = "center";
        private int bColor = -16776961;
        private String bText = "";
        private TextView bTextView;
        /* access modifiers changed from: private */
        public BitmapDrawable backgroundBitmapDrawable = null;
        private int bgColor = -1;
        private String bgImgUrl = "null";
        private String hAlign = "center";
        private int hColor = -16776961;
        private String hText = "";
        private TextView hTextView;
        private String leftIconUrl = "null";
        /* access modifiers changed from: private */
        public ImageView leftIconView;
        private String rightIconUrl = "null";
        /* access modifiers changed from: private */
        public ImageView rightIconView;

        TextPage(JSONObject resource, MobclixCreative p) {
            super(p);
            try {
                this.bgColor = getColorFromJSON(resource.getJSONObject("bgColor"));
            } catch (JSONException e) {
            }
            try {
                this.bgImgUrl = resource.getString("bgImg");
            } catch (JSONException e2) {
            }
            try {
                this.leftIconUrl = resource.getString("leftIcon");
            } catch (JSONException e3) {
            }
            if (this.leftIconUrl.equals("")) {
                this.leftIconUrl = "null";
            }
            try {
                this.rightIconUrl = resource.getString("rightIcon");
            } catch (JSONException e4) {
            }
            if (this.rightIconUrl.equals("")) {
                this.rightIconUrl = "null";
            }
            try {
                JSONObject text = resource.getJSONObject("headerText");
                try {
                    this.hAlign = text.getString("alignment");
                } catch (JSONException e5) {
                }
                try {
                    this.hText = text.getString("text");
                } catch (JSONException e6) {
                }
                if (this.hText.equals("null")) {
                    this.hText = "";
                }
                this.hColor = getColorFromJSON(text.getJSONObject("color"));
            } catch (JSONException e7) {
            }
            try {
                JSONObject text2 = resource.getJSONObject("bodyText");
                try {
                    this.bAlign = text2.getString("alignment");
                } catch (JSONException e8) {
                }
                try {
                    this.bText = text2.getString("text");
                } catch (JSONException e9) {
                }
                if (this.bText.equals("null")) {
                    this.bText = "";
                }
                this.bColor = getColorFromJSON(text2.getJSONObject("color"));
            } catch (JSONException e10) {
            }
            createLayout();
            loadIcons();
            if (!this.bgImgUrl.equals("null")) {
                loadBackgroundImage();
            }
        }

        public void createLayout() {
            RelativeLayout.LayoutParams textLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams leftIconLayoutParams = new RelativeLayout.LayoutParams(dp(48), dp(48));
            leftIconLayoutParams.addRule(15);
            RelativeLayout.LayoutParams rightIconLayoutParams = new RelativeLayout.LayoutParams(dp(48), dp(48));
            rightIconLayoutParams.addRule(15);
            if (this.leftIconUrl.equals("null") && this.rightIconUrl.equals("null")) {
                textLayoutParams.setMargins(dp(5), 0, dp(5), 0);
            } else if (!this.leftIconUrl.equals("null") && this.rightIconUrl.equals("null")) {
                textLayoutParams.setMargins(dp(60), 0, dp(5), 0);
                leftIconLayoutParams.addRule(9);
                leftIconLayoutParams.setMargins(dp(5), 0, 0, 0);
            } else if (!this.leftIconUrl.equals("null") || this.rightIconUrl.equals("null")) {
                textLayoutParams.setMargins(dp(60), 0, dp(60), 0);
                leftIconLayoutParams.addRule(9);
                leftIconLayoutParams.setMargins(dp(5), 0, 0, 0);
                rightIconLayoutParams.addRule(11);
                rightIconLayoutParams.setMargins(0, 0, dp(5), 0);
            } else {
                textLayoutParams.setMargins(dp(5), 0, dp(60), 0);
                rightIconLayoutParams.addRule(11);
                rightIconLayoutParams.setMargins(0, 0, dp(5), 0);
            }
            LinearLayout textLayout = new LinearLayout(this.parentCreative.parentAdView.getContext());
            textLayout.setOrientation(1);
            textLayout.setLayoutParams(textLayoutParams);
            textLayout.setGravity(16);
            if (!this.hText.equals("")) {
                this.hTextView = new TextView(this.parentCreative.parentAdView.getContext());
                this.hTextView.setGravity(((Integer) this.alignmentMap.get(this.hAlign)).intValue());
                this.hTextView.setText(Html.fromHtml("<b>" + this.hText + "</b>"));
                this.hTextView.setTextColor(this.hColor);
                textLayout.addView(this.hTextView);
            }
            if (!this.bText.equals("")) {
                this.bTextView = new TextView(this.parentCreative.parentAdView.getContext());
                this.bTextView.setGravity(((Integer) this.alignmentMap.get(this.bAlign)).intValue());
                this.bTextView.setText(this.bText);
                this.bTextView.setTextColor(this.bColor);
                textLayout.addView(this.bTextView);
            }
            addView(textLayout);
            if (!this.leftIconUrl.equals("null")) {
                this.leftIconView = new ImageView(this.parentCreative.parentAdView.getContext());
                this.leftIconView.setLayoutParams(leftIconLayoutParams);
                addView(this.leftIconView);
            }
            if (!this.rightIconUrl.equals("null")) {
                this.rightIconView = new ImageView(this.parentCreative.parentAdView.getContext());
                this.rightIconView.setLayoutParams(rightIconLayoutParams);
                addView(this.rightIconView);
            }
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            setBackgroundColor(this.bgColor);
        }

        public void loadIcons() {
            if (!this.leftIconUrl.equals("null")) {
                this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.leftIconUrl, new Mobclix.BitmapHandler() {
                    public void handleMessage(Message m) {
                        if (this.bmImg != null) {
                            TextPage.this.leftIconView.setImageBitmap(this.bmImg);
                        }
                        TextPage.this.getParentCreative().handler.sendEmptyMessage(0);
                    }
                })));
            }
            if (!this.rightIconUrl.equals("null")) {
                this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.rightIconUrl, new Mobclix.BitmapHandler() {
                    public void handleMessage(Message m) {
                        if (this.bmImg != null) {
                            TextPage.this.rightIconView.setImageBitmap(this.bmImg);
                        }
                        TextPage.this.getParentCreative().handler.sendEmptyMessage(0);
                    }
                })));
            }
        }

        public void loadBackgroundImage() {
            this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.bgImgUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        TextPage.this.backgroundBitmapDrawable = new BitmapDrawable(this.bmImg);
                        TextPage.this.setBackgroundDrawable(TextPage.this.backgroundBitmapDrawable);
                    }
                    TextPage.this.getParentCreative().handler.sendEmptyMessage(0);
                }
            })));
        }

        /* access modifiers changed from: package-private */
        public void dealloc() {
            try {
                ((BitmapDrawable) this.leftIconView.getDrawable()).getBitmap().recycle();
            } catch (Exception e) {
            }
            try {
                ((BitmapDrawable) this.rightIconView.getDrawable()).getBitmap().recycle();
            } catch (Exception e2) {
            }
            try {
                this.backgroundBitmapDrawable.getBitmap().recycle();
            } catch (Exception e3) {
            }
        }
    }

    private static class ImagePage extends Page {
        private String imgUrl;
        /* access modifiers changed from: private */
        public ImageView imgView;

        ImagePage(String url, MobclixCreative c) {
            super(c);
            this.imgUrl = url;
            createLayout();
            loadImage();
        }

        public void createLayout() {
            RelativeLayout.LayoutParams imgLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
            imgLayoutParams.addRule(15);
            this.imgView = new ImageView(this.parentCreative.parentAdView.getContext());
            this.imgView.setLayoutParams(imgLayoutParams);
            addView(this.imgView);
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        }

        public void loadImage() {
            this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchImageThread(this.imgUrl, new Mobclix.BitmapHandler() {
                public void handleMessage(Message m) {
                    if (this.bmImg != null) {
                        ImagePage.this.imgView.setImageBitmap(this.bmImg);
                    }
                    ImagePage.this.getParentCreative().handler.sendEmptyMessage(0);
                }
            })));
        }

        /* access modifiers changed from: package-private */
        public void dealloc() {
            try {
                ((BitmapDrawable) this.imgView.getDrawable()).getBitmap().recycle();
            } catch (Exception e) {
            }
        }
    }

    static class HTMLPagePool {
        private ArrayList<HTMLPage> availablePages = new ArrayList<>();

        /* access modifiers changed from: package-private */
        public synchronized HTMLPage getHTMLPage(MobclixFullScreenAdView fsAdView) {
            HTMLPage hTMLPage;
            if (this.availablePages.size() > 0) {
                HTMLPage p = this.availablePages.get(0);
                this.availablePages.remove(0);
                hTMLPage = p;
            } else {
                hTMLPage = new HTMLPage(fsAdView);
            }
            return hTMLPage;
        }

        /* access modifiers changed from: package-private */
        public synchronized HTMLPage getHTMLPage(MobclixCreative c) {
            HTMLPage hTMLPage;
            if (this.availablePages.size() > 0) {
                HTMLPage p = this.availablePages.get(0);
                p.parentCreative = c;
                this.availablePages.remove(0);
                try {
                    ViewParent v = c.parentAdView.utilityView.getParent();
                    if (v != null) {
                        ((ViewGroup) v).removeView(c.parentAdView.utilityView);
                    }
                    p.addView(c.parentAdView.utilityView);
                } catch (Exception e) {
                }
                c.parentAdView.utilityView.loaded = false;
                c.parentAdView.utilityView.undisplayed = true;
                hTMLPage = p;
            } else {
                hTMLPage = new HTMLPage(c);
            }
            return hTMLPage;
        }

        /* access modifiers changed from: package-private */
        public void addHTMLPage(HTMLPage page) {
            try {
                if (page.getParent() != null) {
                    ((ViewGroup) page.getParent()).removeView(page);
                }
            } catch (Exception e) {
            }
            if (this.availablePages.size() > 2) {
                try {
                    page.webview.destroy();
                    page.webview = null;
                } catch (Exception e2) {
                }
            } else {
                this.availablePages.add(page);
            }
        }
    }

    static class HTMLPage extends Page {
        /* access modifiers changed from: private */
        public MobclixFullScreenAdView fullScreenAdView = null;
        private String html;
        private MobclixJavascriptInterface jsInterface;
        /* access modifiers changed from: private */
        public MobclixWebView webview;

        HTMLPage(MobclixCreative c) {
            super(c);
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            requestDisallowInterceptTouchEvent(true);
            try {
                createLayout();
            } catch (Exception e) {
            }
        }

        HTMLPage(MobclixFullScreenAdView fsAdView) {
            super(fsAdView.getActivity());
            this.fullScreenAdView = fsAdView;
            requestDisallowInterceptTouchEvent(true);
            try {
                createLayout();
            } catch (Exception e) {
            }
        }

        public void createLayout() {
            RelativeLayout.LayoutParams webviewLayoutParams;
            if (this.fullScreenAdView == null) {
                this.webview = new MobclixWebView(this.parentCreative);
                this.jsInterface = new MobclixJavascriptInterface(this.webview, false);
            } else {
                this.webview = new MobclixWebView(this.fullScreenAdView);
                this.jsInterface = new MobclixJavascriptInterface(this.webview, true);
            }
            this.webview.requestDisallowInterceptTouchEvent(true);
            if (this.fullScreenAdView == null) {
                webviewLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
            } else {
                webviewLayoutParams = new RelativeLayout.LayoutParams(this.fullScreenAdView.screenWidth, this.fullScreenAdView.screenHeight);
            }
            this.webview.setLayoutParams(webviewLayoutParams);
            this.webview.setScrollBarStyle(33554432);
            this.webview.addJavascriptInterface(this.jsInterface, "MOBCLIX");
            this.webview.setJavascriptInterface(this.jsInterface);
            WebSettings settings = this.webview.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setAllowFileAccess(true);
            this.webview.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    try {
                        Uri uri = Uri.parse(url);
                        if (HTMLPage.this.webview.loaded) {
                            if (!HTMLPage.this.webview.displayed) {
                                return true;
                            }
                            if (!HTMLPage.this.webview.touched && HTMLPage.this.parentCreative != null) {
                                if (!HTMLPage.this.parentCreative.parentAdView.allowAutoplay() || !Mobclix.getInstance().hasBeenIntervalSinceLastAutoplay(HTMLPage.this.parentCreative.parentAdView.size)) {
                                    return true;
                                }
                                HTMLPage.this.parentCreative.hasAutoplayed = true;
                                MobclixAdView.lastAutoplayTime.put(HTMLPage.this.parentCreative.parentAdView.size, Long.valueOf(System.currentTimeMillis()));
                            }
                        }
                        if (uri.getScheme().equals("tel") || uri.getScheme().equals("mailto")) {
                            if (HTMLPage.this.parentCreative != null) {
                                HTMLPage.this.parentCreative.action.type = "null";
                                HTMLPage.this.parentCreative.action.act();
                            }
                            if (HTMLPage.this.webview.getJavascriptInterface().expanderActivity != null) {
                                HTMLPage.this.webview.getJavascriptInterface().expanderActivity.wasAdActivity = true;
                            }
                            HTMLPage.this.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
                            return true;
                        } else if (uri.getScheme().equals("sms")) {
                            if (HTMLPage.this.parentCreative != null) {
                                HTMLPage.this.parentCreative.action.type = "null";
                                HTMLPage.this.parentCreative.action.act();
                            }
                            if (HTMLPage.this.webview.getJavascriptInterface().expanderActivity != null) {
                                HTMLPage.this.webview.getJavascriptInterface().expanderActivity.wasAdActivity = true;
                            }
                            String[] smsUrl = url.split(":");
                            String tmp = String.valueOf(smsUrl[0]) + "://";
                            for (int i = 1; i < smsUrl.length; i++) {
                                tmp = String.valueOf(tmp) + smsUrl[i];
                            }
                            String body = Uri.parse(tmp).getQueryParameter("body");
                            Intent mIntent = new Intent("android.intent.action.VIEW", Uri.parse(url.split("\\?")[0]));
                            mIntent.putExtra("sms_body", body);
                            HTMLPage.this.getContext().startActivity(mIntent);
                            return true;
                        } else {
                            try {
                                if (uri.getHost().equals("market.android.com") || uri.getScheme().equals("market")) {
                                    if (HTMLPage.this.webview.getJavascriptInterface().expanderActivity != null) {
                                        HTMLPage.this.webview.getJavascriptInterface().expanderActivity.wasAdActivity = true;
                                    }
                                    HTMLPage.this.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
                                    if (HTMLPage.this.parentCreative != null) {
                                        HTMLPage.this.parentCreative.action.type = "null";
                                        HTMLPage.this.parentCreative.action.act();
                                    }
                                    return true;
                                }
                            } catch (Exception e) {
                            }
                            String shouldOpenInNewWindow = uri.getQueryParameter("shouldOpenInNewWindow");
                            if (shouldOpenInNewWindow == null) {
                                shouldOpenInNewWindow = "";
                            } else {
                                shouldOpenInNewWindow.toLowerCase();
                            }
                            if ((HTMLPage.this.webview.getJavascriptInterface().expanderActivity != null || shouldOpenInNewWindow.equals("no")) && !shouldOpenInNewWindow.equals("yes")) {
                                HTMLPage.this.webview.loadUrl(url);
                            } else {
                                if (HTMLPage.this.webview.getJavascriptInterface().expanderActivity != null) {
                                    HTMLPage.this.webview.getJavascriptInterface().expanderActivity.wasAdActivity = true;
                                }
                                HTMLPage.this.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
                                if (HTMLPage.this.parentCreative != null) {
                                    HTMLPage.this.parentCreative.action.type = "null";
                                    HTMLPage.this.parentCreative.action.act();
                                }
                            }
                            return true;
                        }
                    } catch (Exception e2) {
                        Exception exc = e2;
                        return false;
                    }
                }

                public void onPageFinished(WebView view, String url) {
                    CookieSyncManager.getInstance().sync();
                    if (!HTMLPage.this.webview.loaded && HTMLPage.this.parentCreative != null) {
                        HTMLPage.this.getParentCreative().handler.sendEmptyMessage(0);
                    }
                    if (!HTMLPage.this.webview.loaded && HTMLPage.this.fullScreenAdView != null) {
                        HTMLPage.this.fullScreenAdView.onPageFinished(HTMLPage.this.webview);
                    }
                    HTMLPage.this.webview.loaded = true;
                }

                public void onLoadResource(WebView view, String url) {
                }
            });
            this.webview.setWebChromeClient(new WebChromeClient() {
                public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                    if (!HTMLPage.this.webview.displayed) {
                        return true;
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(HTMLPage.this.webview.getTopContext());
                    builder.setMessage(message).setCancelable(false);
                    builder.setPositiveButton(17039370, new Mobclix.ObjectOnClickListener(result) {
                        public void onClick(DialogInterface dialog, int id) {
                            ((JsResult) this.obj1).confirm();
                        }
                    });
                    builder.create().show();
                    return true;
                }

                public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                    if (!HTMLPage.this.webview.displayed) {
                        return true;
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(HTMLPage.this.webview.getTopContext());
                    builder.setMessage(message).setCancelable(false);
                    builder.setPositiveButton(17039370, new Mobclix.ObjectOnClickListener(result) {
                        public void onClick(DialogInterface dialog, int id) {
                            ((JsResult) this.obj1).confirm();
                        }
                    });
                    builder.setNegativeButton(17039360, new Mobclix.ObjectOnClickListener(result) {
                        public void onClick(DialogInterface dialog, int id) {
                            ((JsResult) this.obj1).cancel();
                        }
                    });
                    builder.create().show();
                    return true;
                }

                public boolean onJsBeforeUnload(WebView view, String url, String message, JsResult result) {
                    if (!HTMLPage.this.webview.displayed) {
                        return true;
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(HTMLPage.this.webview.getTopContext());
                    builder.setMessage(message).setTitle("Confirm Navigation").setCancelable(false);
                    builder.setPositiveButton("Leave this Page", new Mobclix.ObjectOnClickListener(result) {
                        public void onClick(DialogInterface dialog, int id) {
                            ((JsResult) this.obj1).confirm();
                        }
                    });
                    builder.setNegativeButton("Stay on this Page", new Mobclix.ObjectOnClickListener(result) {
                        public void onClick(DialogInterface dialog, int id) {
                            ((JsResult) this.obj1).cancel();
                        }
                    });
                    builder.create().show();
                    return true;
                }

                public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
                    if (!HTMLPage.this.webview.displayed) {
                        return true;
                    }
                    Context mContext = HTMLPage.this.webview.getTopContext();
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    EditText editInput = new EditText(mContext);
                    if (defaultValue != null) {
                        editInput.setText(defaultValue);
                    }
                    builder.setMessage(message).setView(editInput).setCancelable(false).setPositiveButton(17039370, new Mobclix.ObjectOnClickListener(result, editInput) {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            ((JsPromptResult) this.obj1).confirm(((EditText) this.obj2).getText().toString());
                        }
                    }).setNegativeButton(17039360, new Mobclix.ObjectOnClickListener(result) {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            ((JsPromptResult) this.obj1).cancel();
                        }
                    });
                    builder.create().show();
                    return true;
                }

                public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
                    try {
                        HTMLPage.this.webview.mCustomViewCallback = callback;
                        if (HTMLPage.this.webview.displayed) {
                            HTMLPage.this.webview.jsInterface.checkPermissionsForUserInteraction("null", "error");
                            if (HTMLPage.this.parentCreative != null) {
                                if (!HTMLPage.this.webview.touched && HTMLPage.this.parentCreative != null) {
                                    if (HTMLPage.this.parentCreative.parentAdView.allowAutoplay() && Mobclix.getInstance().hasBeenIntervalSinceLastAutoplay(HTMLPage.this.parentCreative.parentAdView.size)) {
                                        HTMLPage.this.parentCreative.hasAutoplayed = true;
                                        MobclixAdView.lastAutoplayTime.put(HTMLPage.this.parentCreative.parentAdView.size, Long.valueOf(System.currentTimeMillis()));
                                    } else if (view instanceof FrameLayout) {
                                        HTMLPage.this.webview.killCustomViewVideo(view, callback);
                                        return;
                                    } else {
                                        return;
                                    }
                                }
                                if (view instanceof FrameLayout) {
                                    HTMLPage.this.webview.showCustomViewVideo(view, callback);
                                }
                            } else if (view instanceof FrameLayout) {
                                HTMLPage.this.webview.showCustomViewVideo(view, callback);
                            }
                        } else if (view instanceof FrameLayout) {
                            HTMLPage.this.webview.killCustomViewVideo(view, callback);
                        }
                    } catch (Exception e) {
                    }
                }
            });
            this.webview.setDownloadListener(new DownloadListener() {
                public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                    try {
                        if (HTMLPage.this.webview.displayed) {
                            if (!HTMLPage.this.webview.touched && HTMLPage.this.parentCreative != null) {
                                if (HTMLPage.this.parentCreative.parentAdView.allowAutoplay() && Mobclix.getInstance().hasBeenIntervalSinceLastAutoplay(HTMLPage.this.parentCreative.parentAdView.size)) {
                                    HTMLPage.this.parentCreative.hasAutoplayed = true;
                                    MobclixAdView.lastAutoplayTime.put(HTMLPage.this.parentCreative.parentAdView.size, Long.valueOf(System.currentTimeMillis()));
                                } else {
                                    return;
                                }
                            }
                            if (mimetype.equals("video/mp4") || mimetype.equals("video/3gp") || mimetype.equals("video/m4v") || mimetype.equals("video/quicktime")) {
                                Intent intent = new Intent("android.intent.action.VIEW");
                                intent.setDataAndType(Uri.parse(url), "video/*");
                                HTMLPage.this.parentCreative.getContext().startActivity(intent);
                            }
                        }
                    } catch (Exception e) {
                    }
                }
            });
            this.webview.setFocusable(true);
            addView(this.webview);
        }

        /* access modifiers changed from: package-private */
        public void loadAd(String h) {
            this.html = h;
            this.webview.setAdHtml(this.html);
            this.webview.loadAd();
        }

        public void onDetachedFromWindow() {
            try {
                this.jsInterface.pauseListeners();
            } catch (Exception e) {
            }
        }

        /* access modifiers changed from: package-private */
        public void dealloc() {
            try {
                this.jsInterface.pauseListeners();
            } catch (Exception e) {
            }
            try {
                this.webview.reset();
                this.parentCreative.parentAdView.mHTMLPagePool.addHTMLPage(this);
            } catch (Exception e2) {
            }
        }
    }

    private static class CustomAdPage extends Page {
        private ImageView imgView;

        CustomAdPage(MobclixCreative c) {
            super(c);
            createLayout();
        }

        public void createLayout() {
            try {
                Bitmap myBitmap = BitmapFactory.decodeStream(this.parentCreative.parentAdView.getContext().openFileInput(String.valueOf(this.parentCreative.parentAdView.size) + "_mc_cached_custom_ad.png"));
                RelativeLayout.LayoutParams imgLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
                imgLayoutParams.addRule(15);
                this.imgView = new ImageView(this.parentCreative.parentAdView.getContext());
                this.imgView.setLayoutParams(imgLayoutParams);
                this.imgView.setImageBitmap(myBitmap);
                addView(this.imgView);
                setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            } catch (Exception e) {
            }
            getParentCreative().handler.sendEmptyMessage(0);
        }

        /* access modifiers changed from: package-private */
        public void dealloc() {
            try {
                ((BitmapDrawable) this.imgView.getDrawable()).getBitmap().recycle();
            } catch (Exception e) {
            }
        }
    }

    static class OpenAllocationPage extends Page {
        String network = "openadmob";
        ViewGroup oaAdView;
        int oaPageDisplayed = 0;
        String params = null;

        OpenAllocationPage(JSONObject p, MobclixCreative c) {
            super(c);
            int openAllocationCode;
            try {
                this.network = p.getString("network");
            } catch (Exception e) {
            }
            try {
                StringBuffer paramsBuffer = new StringBuffer();
                JSONObject pp = p.getJSONObject("params");
                Iterator<?> i = pp.keys();
                while (i.hasNext()) {
                    String k = i.next().toString();
                    String v = pp.get(k).toString();
                    paramsBuffer.append("&").append(k);
                    paramsBuffer.append("=").append(v);
                }
                this.params = paramsBuffer.toString();
            } catch (Exception e2) {
            }
            boolean eventConsumed = false;
            if (this.network.equals("openadmob")) {
                openAllocationCode = MobclixAdViewListener.SUBALLOCATION_ADMOB;
            } else if (this.network.equals("opengoogle")) {
                openAllocationCode = MobclixAdViewListener.SUBALLOCATION_GOOGLE;
            } else if (this.network.equals("openmillennial")) {
                openAllocationCode = MobclixAdViewListener.SUBALLOCATION_MILLENNIAL;
            } else {
                openAllocationCode = MobclixAdViewListener.SUBALLOCATION_OTHER;
            }
            Iterator<MobclixAdViewListener> it = this.parentCreative.parentAdView.listeners.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener listener = it.next();
                if (listener != null) {
                    eventConsumed = eventConsumed || listener.onOpenAllocationLoad(this.parentCreative.parentAdView, openAllocationCode);
                }
            }
            if (eventConsumed) {
                this.parentCreative.parentAdView.lastAdLoad = 0;
            } else if (openAllocationCode == -111111) {
                try {
                    millennialAllocation();
                } catch (Throwable th) {
                    this.parentCreative.parentAdView.getNextAd(this.params);
                }
            } else if (openAllocationCode == -750 || openAllocationCode == -10100) {
                try {
                    googleAllocation();
                } catch (Throwable th2) {
                    this.parentCreative.parentAdView.getNextAd(this.params);
                }
            } else {
                this.parentCreative.parentAdView.lastAdLoad = 0;
            }
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* access modifiers changed from: package-private */
        public void millennialAllocation() throws Exception {
            HashMap<String, String> mSettings = Mobclix.adUnitSettings.get(this.parentCreative.parentAdView.size).openAllocationSettings.get("openmillennial");
            Hashtable<String, String> map = new Hashtable<>();
            for (String k : mSettings.keySet()) {
                if (!k.equals("key1")) {
                    try {
                        String key = ((String) mSettings.get(k)).split("=")[0];
                        if (key.length() != ((String) mSettings.get(k)).length()) {
                            map.put(key, ((String) mSettings.get(k)).substring(key.length() + 1, ((String) mSettings.get(k)).length()));
                        }
                    } catch (Exception e) {
                    }
                }
            }
            map.put("height", Integer.toString((int) this.parentCreative.parentAdView.height));
            map.put("width", Integer.toString((int) this.parentCreative.parentAdView.width));
            try {
                if (MobclixDemographics.demo != null) {
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.Birthdate)) {
                        map.put("age", Integer.toString((int) (((double) (System.currentTimeMillis() - new SimpleDateFormat("yyyymmdd").parse(MobclixDemographics.demo.get(MobclixDemographics.Birthdate)).getTime())) / 3.15576E10d)));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.Education)) {
                        switch (Integer.parseInt(MobclixDemographics.demo.get(MobclixDemographics.Education))) {
                            case 1:
                                map.put("education", "highschool");
                                break;
                            case 2:
                                map.put("education", "somecollege");
                                break;
                            case 4:
                                map.put("education", "bachelor");
                                break;
                            case 5:
                                map.put("education", "masters");
                                break;
                            case 6:
                                map.put("education", "phd");
                                break;
                        }
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.Ethnicity)) {
                        switch (Integer.parseInt(MobclixDemographics.demo.get(MobclixDemographics.Ethnicity))) {
                            case 0:
                                map.put("ethnicity", "other");
                                break;
                            case 2:
                                map.put("ethnicity", "asian");
                                break;
                            case 3:
                                map.put("ethnicity", "africanamerican");
                                break;
                            case 4:
                                map.put("ethnicity", "hispanic");
                                break;
                            case 5:
                                map.put("ethnicity", "nativeamerican");
                                break;
                            case 6:
                                map.put("ethnicity", "white");
                                break;
                        }
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.Gender)) {
                        switch (Integer.parseInt(MobclixDemographics.demo.get(MobclixDemographics.Gender))) {
                            case 0:
                                map.put("gender", "unknown");
                                break;
                            case 1:
                                map.put("gender", "male");
                                break;
                            case 2:
                                map.put("gender", "female");
                                break;
                        }
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.Income)) {
                        map.put("income", MobclixDemographics.demo.get(MobclixDemographics.Income));
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.MaritalStatus)) {
                        switch (Integer.parseInt(MobclixDemographics.demo.get(MobclixDemographics.MaritalStatus))) {
                            case 1:
                                map.put("marital", "single");
                                break;
                            case 3:
                                map.put("marital", "relationship");
                                break;
                        }
                    }
                    if (MobclixDemographics.demo.containsKey(MobclixDemographics.PostalCode)) {
                        map.put("zip", MobclixDemographics.demo.get(MobclixDemographics.PostalCode));
                    }
                }
            } catch (Exception e2) {
            }
            StringBuilder keywordsBuffer = new StringBuilder();
            Iterator<MobclixAdViewListener> it = this.parentCreative.parentAdView.listeners.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener listener = it.next();
                if (listener != null) {
                    String keywords = listener.keywords();
                    if (keywords == null) {
                        keywords = "";
                    }
                    if (!keywords.equals("")) {
                        keywordsBuffer.append(keywords).append(" ");
                    }
                }
            }
            if (keywordsBuffer.length() > 0) {
                map.put("keywords", keywordsBuffer.toString());
            }
            Class<?> MMAdViewClass = Class.forName("com.millennialmedia.android.MMAdView");
            Constructor<?> con = MMAdViewClass.getConstructor(Activity.class, String.class, String.class, Integer.TYPE, Hashtable.class);
            String type = "MMBannerAdTop";
            if (this.parentCreative.parentAdView.size == "300x250") {
                type = "MMBannerAdRectangle";
            }
            this.oaAdView = (FrameLayout) con.newInstance((Activity) this.parentCreative.getContext(), mSettings.get("key1"), type, -1, map);
            Class<?> MMAdListenerClass = Class.forName("com.millennialmedia.android.MMAdView$MMAdListener");
            MMInvocationHandler mMInvocationHandler = new MMInvocationHandler();
            Object mMAdListener = MMAdListenerClass.cast(Proxy.newProxyInstance(MMAdListenerClass.getClassLoader(), new Class[]{MMAdListenerClass}, mMInvocationHandler));
            MMAdViewClass.getMethod("setListener", MMAdListenerClass).invoke(this.oaAdView, mMAdListener);
            Method m = MMAdViewClass.getMethod("setId", Integer.TYPE);
            int viewid = (int) (System.currentTimeMillis() % 1000000000);
            m.invoke(this.oaAdView, Integer.valueOf(viewid));
            this.oaAdView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Iterator<MobclixAdViewListener> it = OpenAllocationPage.this.parentCreative.parentAdView.listeners.iterator();
                    while (it.hasNext()) {
                        MobclixAdViewListener listener = it.next();
                        if (listener != null) {
                            listener.onAdClick(OpenAllocationPage.this.parentCreative.parentAdView);
                        }
                    }
                }
            });
            this.oaAdView.setVisibility(4);
            this.parentCreative.parentAdView.addView(this.oaAdView);
            MMAdViewClass.getMethod("callForAd", new Class[0]).invoke(this.oaAdView, new Object[0]);
        }

        public class MMInvocationHandler implements InvocationHandler {
            public MMInvocationHandler() {
            }

            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (method.getName().equals("MMAdReturned")) {
                    ((Activity) OpenAllocationPage.this.parentCreative.parentAdView.getContext()).runOnUiThread(new Runnable() {
                        public void run() {
                            if (OpenAllocationPage.this.oaPageDisplayed == 1) {
                                try {
                                    ViewParent v = OpenAllocationPage.this.oaAdView.getParent();
                                    if (v != null) {
                                        ((ViewGroup) v).removeView(OpenAllocationPage.this.oaAdView);
                                    }
                                } catch (Exception e) {
                                }
                                OpenAllocationPage.this.addView(OpenAllocationPage.this.oaAdView);
                                OpenAllocationPage.this.oaAdView.setVisibility(0);
                                OpenAllocationPage.this.getParentCreative().handler.sendEmptyMessage(0);
                            }
                            OpenAllocationPage.this.oaPageDisplayed++;
                        }
                    });
                    return null;
                } else if (!method.getName().equals("MMAdFailed")) {
                    return null;
                } else {
                    ((Activity) OpenAllocationPage.this.parentCreative.parentAdView.getContext()).runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                ViewParent v = OpenAllocationPage.this.oaAdView.getParent();
                                if (v != null) {
                                    ((ViewGroup) v).removeView(OpenAllocationPage.this.oaAdView);
                                }
                            } catch (Exception e) {
                            }
                            if (OpenAllocationPage.this.params == null) {
                                OpenAllocationPage.this.params = "";
                            }
                            OpenAllocationPage.this.parentCreative.parentAdView.getNextAd(OpenAllocationPage.this.params);
                        }
                    });
                    return null;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void adMobAllocation() throws Exception {
            Class<?> AdMobAdViewClass = Class.forName("com.admob.android.ads.AdView");
            this.oaAdView = (RelativeLayout) AdMobAdViewClass.getConstructor(Activity.class).newInstance((Activity) this.parentCreative.getContext());
            Class<?> AdMobAdListenerClass = Class.forName("com.admob.android.ads.AdListener");
            AdMobInvocationHandler adMobInvocationHandler = new AdMobInvocationHandler();
            Object adMobAdListener = AdMobAdListenerClass.cast(Proxy.newProxyInstance(AdMobAdListenerClass.getClassLoader(), new Class[]{AdMobAdListenerClass}, adMobInvocationHandler));
            AdMobAdViewClass.getMethod("setAdListener", AdMobAdListenerClass).invoke(this.oaAdView, adMobAdListener);
            StringBuilder keywordsBuffer = new StringBuilder();
            StringBuilder queryBuffer = new StringBuilder();
            Iterator<MobclixAdViewListener> it = this.parentCreative.parentAdView.listeners.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener listener = it.next();
                if (listener != null) {
                    String keywords = listener.keywords();
                    if (keywords == null) {
                        keywords = "";
                    }
                    if (!keywords.equals("")) {
                        keywordsBuffer.append(keywords).append(" ");
                    }
                    String query = listener.query();
                    if (query == null) {
                        query = "";
                    }
                    if (!query.equals("")) {
                        queryBuffer.append(query).append(" ");
                    }
                }
            }
            if (keywordsBuffer.length() > 0) {
                AdMobAdViewClass.getMethod("setKeywords", String.class).invoke(this.oaAdView, keywordsBuffer.toString());
            }
            if (queryBuffer.length() > 0) {
                AdMobAdViewClass.getMethod("setSearchQuery", String.class).invoke(this.oaAdView, queryBuffer.toString());
            }
            this.oaAdView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Iterator<MobclixAdViewListener> it = OpenAllocationPage.this.parentCreative.parentAdView.listeners.iterator();
                    while (it.hasNext()) {
                        MobclixAdViewListener listener = it.next();
                        if (listener != null) {
                            listener.onAdClick(OpenAllocationPage.this.parentCreative.parentAdView);
                        }
                    }
                }
            });
            this.oaAdView.setVisibility(4);
            this.parentCreative.parentAdView.addView(this.oaAdView);
        }

        public class AdMobInvocationHandler implements InvocationHandler {
            public AdMobInvocationHandler() {
            }

            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                try {
                    ViewParent v = OpenAllocationPage.this.oaAdView.getParent();
                    if (v != null) {
                        ((ViewGroup) v).removeView(OpenAllocationPage.this.oaAdView);
                    }
                } catch (Exception e) {
                }
                if (!method.getName().equals("onReceiveAd")) {
                    if (OpenAllocationPage.this.params == null) {
                        OpenAllocationPage.this.params = "";
                    }
                    OpenAllocationPage.this.parentCreative.parentAdView.getNextAd(OpenAllocationPage.this.params);
                    return null;
                } else if (OpenAllocationPage.this.oaPageDisplayed != 0) {
                    return null;
                } else {
                    OpenAllocationPage.this.oaPageDisplayed++;
                    OpenAllocationPage.this.addView(OpenAllocationPage.this.oaAdView);
                    OpenAllocationPage.this.oaAdView.setVisibility(0);
                    OpenAllocationPage.this.getParentCreative().handler.sendEmptyMessage(0);
                    return null;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void googleAllocation() throws Exception {
            Object obj;
            Object obj2;
            Class<?> googleAdSizeClass = Class.forName("com.google.ads.AdSize");
            Field[] consts = googleAdSizeClass.getFields();
            int i = 0;
            Object adsize = null;
            Object adsize2 = null;
            while (i < consts.length) {
                if (consts[i].getName().equals("BANNER")) {
                    obj = consts[i].get(null);
                } else {
                    obj = adsize2;
                }
                if (consts[i].getName().equals("IAB_MRECT")) {
                    obj2 = consts[i].get(null);
                } else {
                    obj2 = adsize;
                }
                i++;
                adsize = obj2;
                adsize2 = obj;
            }
            Class<?> googleAdViewClass = Class.forName("com.google.ads.AdView");
            Constructor<?> con = googleAdViewClass.getConstructor(Activity.class, googleAdSizeClass, String.class);
            if (this.parentCreative.parentAdView.size.equals("300x250")) {
                adsize2 = adsize;
            }
            this.oaAdView = (RelativeLayout) con.newInstance((Activity) this.parentCreative.getContext(), adsize2, Mobclix.getInstance().getAdMobApplicationId());
            Class<?> AdMobAdListenerClass = Class.forName("com.google.ads.AdListener");
            googleAdViewClass.getMethod("setAdListener", AdMobAdListenerClass).invoke(this.oaAdView, AdMobAdListenerClass.cast(Proxy.newProxyInstance(AdMobAdListenerClass.getClassLoader(), new Class[]{AdMobAdListenerClass}, new AdMobInvocationHandler())));
            Class<?> googleAdRequestClass = Class.forName("com.google.ads.AdRequest");
            Object newInstance = googleAdRequestClass.getConstructor(new Class[0]).newInstance(new Object[0]);
            HashSet hashSet = new HashSet();
            Iterator<MobclixAdViewListener> it = this.parentCreative.parentAdView.listeners.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener listener = it.next();
                if (listener != null) {
                    String k = listener.keywords();
                    if (k == null) {
                        k = "";
                    }
                    if (!k.equals("")) {
                        String[] ks = k.split(",\\s*");
                        for (int i2 = 0; i2 < ks.length; i2++) {
                            hashSet.add(ks[i2]);
                        }
                    }
                }
            }
            if (hashSet.size() > 0) {
                googleAdRequestClass.getMethod("setKeywords", Set.class).invoke(newInstance, hashSet);
            }
            googleAdRequestClass.getMethod("setTesting", Boolean.TYPE).invoke(newInstance, true);
            googleAdViewClass.getMethod("loadAd", googleAdRequestClass).invoke(this.oaAdView, newInstance);
            this.oaAdView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Iterator<MobclixAdViewListener> it = OpenAllocationPage.this.parentCreative.parentAdView.listeners.iterator();
                    while (it.hasNext()) {
                        MobclixAdViewListener listener = it.next();
                        if (listener != null) {
                            listener.onAdClick(OpenAllocationPage.this.parentCreative.parentAdView);
                        }
                    }
                }
            });
            this.oaAdView.setVisibility(4);
            this.parentCreative.parentAdView.addView(this.oaAdView);
        }

        /* access modifiers changed from: package-private */
        public void dealloc() {
            try {
                if (this.oaAdView.getClass().getName().equals("com.google.ads.AdView")) {
                    this.oaAdView.setOnClickListener(null);
                    Class.forName("com.google.ads.AdView").getMethod("destroy", new Class[0]).invoke(this.oaAdView, new Object[0]);
                    ((ViewGroup) this.oaAdView.getParent()).removeView(this.oaAdView);
                }
                if (this.oaAdView.getClass().getName().equals("com.millennialmedia.android.MMAdView")) {
                    this.oaAdView.setOnClickListener(null);
                    Class.forName("com.millennialmedia.android.MMAdView").getMethod("halt", new Class[0]).invoke(this.oaAdView, new Object[0]);
                    ((ViewGroup) this.oaAdView.getParent()).removeView(this.oaAdView);
                }
            } catch (Throwable th) {
            }
        }
    }

    class Action {
        boolean autoplay = false;
        String browserType = "standard";
        String cachedHTML = "";
        Bitmap cachedImageBitmap = null;
        int cachedImageHeight;
        int cachedImageWidth;
        private ArrayList<String> onShowUrls = new ArrayList<>();
        private ArrayList<String> onTouchUrls = new ArrayList<>();
        /* access modifiers changed from: private */
        public MobclixCreative parentCreative;
        private JSONObject rawJSON;
        String type = "null";
        String url = "";

        Action(MobclixCreative creative) {
            this.parentCreative = creative;
        }

        Action(JSONObject action, MobclixCreative creative) {
            try {
                this.rawJSON = action;
                this.parentCreative = creative;
                this.type = action.getString("type");
                if (action.has("autoplay")) {
                    this.autoplay = action.getBoolean("autoplay");
                }
                try {
                    JSONObject eventUrls = action.getJSONObject("eventUrls");
                    JSONArray t = eventUrls.getJSONArray("onShow");
                    for (int i = 0; i < t.length(); i++) {
                        this.onShowUrls.add(t.getString(i));
                    }
                    JSONArray t2 = eventUrls.getJSONArray("onTouch");
                    for (int i2 = 0; i2 < t2.length(); i2++) {
                        this.onTouchUrls.add(t2.getString(i2));
                    }
                } catch (Exception e) {
                }
                if (this.type.equals("url")) {
                    try {
                        this.url = action.getString("url");
                    } catch (JSONException e2) {
                    }
                    try {
                        this.browserType = action.getString("browserType");
                    } catch (JSONException e3) {
                    }
                    try {
                        if (action.getBoolean("preload")) {
                            loadPreload();
                        }
                    } catch (JSONException e4) {
                    }
                } else if (this.type.equals("html")) {
                    try {
                        this.url = action.getString("baseUrl");
                    } catch (JSONException e5) {
                    }
                    try {
                        this.cachedHTML = action.getString("html");
                    } catch (JSONException e6) {
                    }
                    try {
                        this.browserType = action.getString("browserType");
                    } catch (JSONException e7) {
                    }
                }
            } catch (JSONException e8) {
            }
        }

        public boolean getAutoplay() {
            return this.autoplay;
        }

        /* access modifiers changed from: package-private */
        public void loadPreload() {
            this.parentCreative.asyncRequestThreads.push(new Thread(new Mobclix.FetchResponseThread(this.url, new Handler() {
                public void handleMessage(Message m) {
                    String type = m.getData().getString("type");
                    if (type.equals("success")) {
                        Action.this.cachedHTML = m.getData().getString(AsyncHttpRequest.KEY_RESPONSE);
                    } else if (type.equals("failure")) {
                        Action.this.cachedHTML = "";
                    }
                    Action.this.parentCreative.handler.sendEmptyMessage(0);
                }
            })));
        }

        public boolean act() {
            try {
                Iterator<String> it = this.onShowUrls.iterator();
                while (it.hasNext()) {
                    new Thread(new Mobclix.FetchImageThread(it.next(), new Mobclix.BitmapHandler())).start();
                }
            } catch (Exception e) {
            }
            Iterator<MobclixAdViewListener> it2 = MobclixCreative.this.parentAdView.listeners.iterator();
            while (it2.hasNext()) {
                MobclixAdViewListener listener = it2.next();
                if (listener != null) {
                    listener.onAdClick(MobclixCreative.this.parentAdView);
                }
            }
            MobclixCreative.isPlaying = true;
            if (this.type.equals("url") || this.type.equals("html")) {
                loadUrl();
            } else if (this.type.equals("video")) {
                Intent mIntent = new Intent();
                String packageName = MobclixCreative.this.parentAdView.getContext().getPackageName();
                mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "video").putExtra(String.valueOf(packageName) + ".data", this.rawJSON.toString());
                MobclixCreative.this.getContext().startActivity(mIntent);
            }
            MobclixCreative.isPlaying = false;
            return true;
        }

        /* access modifiers changed from: package-private */
        public void loadUrl() {
            try {
                if (!this.url.equals("")) {
                    Uri uri = Uri.parse(this.url);
                    String[] tmp = this.url.split("mobclix://");
                    if (tmp.length <= 1) {
                        tmp = this.url.split("mobclix%3A%2F%2F");
                        if (tmp.length <= 1) {
                            for (int i = 0; i < Mobclix.getInstance().getNativeUrls().size(); i++) {
                                if (uri.getHost().equals(Mobclix.getInstance().getNativeUrls().get(i))) {
                                    MobclixCreative.this.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
                                    return;
                                }
                            }
                            if (!this.cachedHTML.equals("")) {
                                Intent mIntent = new Intent();
                                String packageName = MobclixCreative.this.parentAdView.getContext().getPackageName();
                                try {
                                    this.rawJSON.put("cachedHTML", this.cachedHTML);
                                } catch (JSONException e) {
                                }
                                mIntent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "browser").putExtra(String.valueOf(packageName) + ".data", this.rawJSON.toString());
                                MobclixCreative.this.getContext().startActivity(mIntent);
                            } else if (this.browserType.equals("minimal")) {
                                Intent mIntent2 = new Intent();
                                String packageName2 = MobclixCreative.this.parentAdView.getContext().getPackageName();
                                mIntent2.setClassName(packageName2, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName2) + ".type", "browser").putExtra(String.valueOf(packageName2) + ".data", this.rawJSON.toString());
                                MobclixCreative.this.getContext().startActivity(mIntent2);
                                return;
                            } else {
                                MobclixCreative.this.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
                            }
                            CookieSyncManager.getInstance().sync();
                            return;
                        }
                    }
                    String customAdString = tmp[1];
                    MobclixCreative.this.customAdThread = new Thread(new CustomAdThread(this.url));
                    MobclixCreative.this.customAdThread.start();
                    Iterator<MobclixAdViewListener> it = MobclixCreative.this.parentAdView.listeners.iterator();
                    while (it.hasNext()) {
                        MobclixAdViewListener listener = it.next();
                        if (listener != null) {
                            listener.onCustomAdTouchThrough(MobclixCreative.this.parentAdView, customAdString);
                        }
                    }
                }
            } catch (Exception e2) {
                Exception exc = e2;
            }
        }
    }

    private class Rotate3dAnimation extends Animation {
        private Camera mCamera;
        private final float mCenterX;
        private final float mCenterY;
        private final float mDepthZ;
        private final float mFromDegrees;
        private final boolean mReverse;
        private final float mToDegrees;

        public Rotate3dAnimation(float fromDegrees, float toDegrees, float centerX, float centerY, float depthZ, boolean reverse) {
            this.mFromDegrees = fromDegrees;
            this.mToDegrees = toDegrees;
            this.mCenterX = centerX;
            this.mCenterY = centerY;
            this.mDepthZ = depthZ;
            this.mReverse = reverse;
        }

        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
            this.mCamera = new Camera();
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float interpolatedTime, Transformation t) {
            float fromDegrees = this.mFromDegrees;
            float degrees = fromDegrees + ((this.mToDegrees - fromDegrees) * interpolatedTime);
            float centerX = this.mCenterX;
            float centerY = this.mCenterY;
            Camera camera = this.mCamera;
            Matrix matrix = t.getMatrix();
            camera.save();
            if (this.mReverse) {
                camera.translate(0.0f, 0.0f, this.mDepthZ * interpolatedTime);
            } else {
                camera.translate(0.0f, 0.0f, this.mDepthZ * (1.0f - interpolatedTime));
            }
            camera.rotateY(degrees);
            camera.getMatrix(matrix);
            camera.restore();
            matrix.preTranslate(-centerX, -centerY);
            matrix.postTranslate(centerX, centerY);
        }
    }
}
