package com.pontiflex.mobile.webview.utilities;

import android.content.Context;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class PackageHelper {
    private static final int BUFFERED_READER_SIZE = 256;
    private static final String OLD_RES_PATH = "pflx_res_old";
    private static final String PontiflexSdkVersionBasename = "pflx_version";
    private static final String RES_JS_CLIENT = "js-client";
    private static final String RES_PATH = "pflx_res";
    private static PackageHelper instance;
    private String baseHtmlResourcesPath = null;

    public static PackageHelper getInstance(Context context) {
        if (instance == null) {
            instance = createInstance(context);
        }
        if (instance != null) {
            return instance;
        }
        throw new IllegalStateException();
    }

    public static PackageHelper createInstance(Context context) {
        if (instance == null) {
            instance = new PackageHelper(context);
        }
        return instance;
    }

    private PackageHelper(Context context) {
    }

    public int getLayoutId(String name, Context context) {
        return getIdentifier(name, "layout", context);
    }

    public int getDrawableId(String name, Context context) {
        return getIdentifier(name, "drawable", context);
    }

    public int getItemId(String id, Context context) {
        return getIdentifier(id, "id", context);
    }

    public int getIdentifier(String id, String resource, Context context) {
        if (context != null) {
            return context.getResources().getIdentifier(id, resource, context.getPackageName());
        }
        throw new IllegalArgumentException();
    }

    public JarInputStream getJarInputStream(Context context) {
        try {
            return new JarInputStream(context.getAssets().open("pontiflex_sdk.jar"));
        } catch (IOException e) {
            return null;
        }
    }

    public String getBaseHtmlResourcesPath() {
        return this.baseHtmlResourcesPath;
    }

    public List<String> getHtmlPaths(Context context) {
        List<String> result = new ArrayList<>();
        File resDir = new File(context.getFilesDir(), "pflx_res/js-client");
        if (!new File(resDir, "html").exists()) {
            resDir = new File(context.getFilesDir(), "pflx_res_old/js-client");
            if (!new File(resDir, "html").exists()) {
                Log.e("Pontiflex SDK", "Error while scanning for html files");
                return null;
            }
        }
        File htmlDir = new File(resDir, "html");
        String[] values = null;
        try {
            values = context.getAssets().list("");
        } catch (IOException e) {
        }
        if (values == null || values.length <= 0 || !Arrays.asList(values).contains("html")) {
            this.baseHtmlResourcesPath = "file://" + resDir.getPath();
        } else {
            this.baseHtmlResourcesPath = "file:///android_asset";
        }
        fillPaths(htmlDir, "html", result);
        return result;
    }

    private void fillPaths(File htmlDir, String basePath, List<String> pathsList) {
        String[] paths;
        try {
            if (htmlDir.exists() && htmlDir.isDirectory() && (paths = htmlDir.list()) != null) {
                for (int ii = 0; ii < paths.length; ii++) {
                    if (paths[ii] == null || !paths[ii].endsWith(".html")) {
                        fillPaths(new File(htmlDir, paths[ii]), basePath + File.separator + paths[ii], pathsList);
                    } else {
                        pathsList.add(basePath + File.separator + paths[ii]);
                    }
                }
            }
        } catch (Exception e) {
            Log.e("Pontiflex SDK", "Not able to find html paths", e);
        }
    }

    public InputStream getHtmlResource(String name, Context context) {
        JarInputStream jis = getJarInputStream(context);
        if (jis != null) {
            return getJarResource(jis, name);
        }
        int pflxStringId = getIdentifier(name, "raw", context);
        if (pflxStringId > 0) {
            return context.getResources().openRawResource(pflxStringId);
        }
        return null;
    }

    private InputStream getJarResource(JarInputStream jis, String name) {
        JarEntry entry;
        do {
            try {
                entry = jis.getNextJarEntry();
                if (entry == null) {
                    return null;
                }
            } catch (IOException e) {
                Log.e("Pontiflex SDK", "Unknown jar resource", e);
            }
        } while (!entry.getName().contains(name));
        return jis;
    }

    /* JADX INFO: finally extract failed */
    public static String convertStreamToString(InputStream is) throws IOException {
        if (is == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), BUFFERED_READER_SIZE);
            while (true) {
                String line = reader.readLine();
                if (line != null) {
                    sb.append(line);
                } else {
                    is.close();
                    return sb.toString();
                }
            }
        } catch (Throwable th) {
            is.close();
            throw th;
        }
    }

    public Properties loadPontiflexSdkVersion(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        }
        InputStream is = null;
        try {
            is = new FileInputStream(new File(new File(context.getFilesDir(), "pflx_res/js-client"), "pflx_version.xml"));
        } catch (FileNotFoundException e) {
            Log.e("Pontiflex SDK", "Not able to open pflx version file");
        }
        return parsePontiflexStrings(is, new Properties());
    }

    /* access modifiers changed from: protected */
    public Properties parsePontiflexStrings(InputStream is, Properties props) {
        try {
            XMLReader reader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            PontiflexStringsHandler psh = new PontiflexStringsHandler(props);
            reader.setContentHandler(psh);
            try {
                reader.parse(new InputSource(is));
                return psh.getProperties();
            } catch (IOException e) {
            }
        } catch (ParserConfigurationException e2) {
            Log.e("Pontiflex SDK", "Error parsing string properties", e2);
            return props;
        } catch (SAXException e3) {
            Log.e("Pontiflex SDK", "Error on SAX parsing string properties", e3);
            return props;
        }
    }
}
