package com.pontiflex.mobile.webview.sdk.activities;

import android.os.Build;
import com.pontiflex.mobile.webview.sdk.IPflexJSInterface;

/* compiled from: BaseActivity */
class PflexVersionJSInterface implements IPflexJSInterface {
    private BaseActivity baseActivity;

    protected PflexVersionJSInterface(BaseActivity activity) {
        this.baseActivity = activity;
    }

    public String getAppName() {
        return this.baseActivity.getAppName(this.baseActivity.getApplicationContext());
    }

    public String getAppVersionCode() {
        return this.baseActivity.getAppVersionCode(this.baseActivity.getApplicationContext());
    }

    public String getAppVersionName() {
        return this.baseActivity.getAppVersionName(this.baseActivity.getApplicationContext());
    }

    public String getOsVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getUserAgent() {
        return this.baseActivity.getUserAgent(this.baseActivity.getApplicationContext());
    }

    public String getLocale() {
        return this.baseActivity.getLocale(this.baseActivity.getApplicationContext());
    }

    public String getPostalCode() {
        return this.baseActivity.getPostalCode();
    }
}
