package com.pontiflex.mobile.webview.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import com.pontiflex.mobile.webview.sdk.IAdManager;
import com.pontiflex.mobile.webview.sdk.activities.MultiOfferActivity;
import com.pontiflex.mobile.webview.sdk.activities.RegistrationActivity;
import com.pontiflex.mobile.webview.utilities.AppInfo;
import com.pontiflex.mobile.webview.utilities.FieldValidator;
import com.pontiflex.mobile.webview.utilities.HtmlPageRegistry;
import com.pontiflex.mobile.webview.utilities.PackageHelper;
import com.pontiflex.mobile.webview.utilities.RegistrationStorage;
import com.pontiflex.mobile.webview.utilities.UpdateResourceUtil;
import com.pontiflex.mobile.webview.utilities.VersionHelper;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;

public class AdManager implements IAdManager {
    private static final String AdPath = "ads";
    private static final String AppInfoPflxAdUrl = "pflexadsUrl";
    private static final String AppInfoPid = "pid";
    private static final String AppInfoSubSourceId = "subSourceId";
    public static final int DefaultAdRefreshIntervalMs = 60000;
    public static final boolean DefaultAllowReadDeviceData = true;
    private static final String DefaultAppInfoName = "AppInfo.json";
    public static final int DefaultRegistrationInterval = 0;
    public static IAdManager.RegistrationMode DefaultRegistrationMode = IAdManager.RegistrationMode.RegistrationAdHoc;
    public static final boolean DefaultRegistrationRequired = false;
    public static final boolean DefaultShowingMultiAdView = true;
    private static final String JsonPath = "json";
    private static final String KeystoreFilename = "PontiflexRegistrationData";
    public static final String LeadPostUrlStringKey = "LeadUrlString";
    protected static AdManager instance = null;
    private boolean allowReadDeviceData = true;
    private AppInfo appInfo;
    private Application application = null;
    private PflxEnvironment environment = null;
    private boolean isRegistrationRequired = false;
    private boolean isShowingMultiAdView = true;
    private PackageHelper pkgHelper;
    private int refreshIntervalMs = DefaultAdRefreshIntervalMs;
    private int registrationInterval = 0;
    private IAdManager.RegistrationMode registrationMode = DefaultRegistrationMode;
    private RegistrationStorage registrationStorage;
    private boolean resourceInitializationInProgress = false;
    private VersionHelper versionHelper;

    public enum PflxEnvironment {
        DevelopmentEnvironment,
        StagingEnvironment,
        PreproductionEnvironment,
        ProductionEnvironment
    }

    public static IAdManager getInstance(Application application2) {
        return getAdManagerInstance(application2);
    }

    protected static void initializeInstance(Application application2) {
        SharedPreferences prefs = null;
        if (application2 != null) {
            prefs = application2.getSharedPreferences("Pontiflex", 0);
        }
        if (instance != null && prefs != null) {
            instance.setAllowReadDeviceData(prefs.getBoolean("AllowReadDeviceData", true));
            instance.setRegistrationRequired(prefs.getBoolean("RegistrationRequired", false));
            instance.setShowingMultiAdView(prefs.getBoolean("ShowingMultiAdView", true));
            String registrationMode2 = prefs.getString("RegistrationMode", DefaultRegistrationMode.toString());
            SharedPreferences.Editor editor = instance.getSharedPreferencesEditor();
            if (editor != null) {
                editor.putString("RegistrationMode", String.valueOf(registrationMode2));
                editor.commit();
            }
        }
    }

    public static IAdManager createInstance(Application application2) {
        if (instance == null) {
            instance = new AdManager(application2, DefaultAppInfoName);
        }
        if (instance != null) {
            instance.incrementLaunchCount();
        }
        return instance;
    }

    public static IAdManager createInstance(Application application2, String appInfoName) {
        if (instance == null) {
            instance = new AdManager(application2, appInfoName);
        }
        if (instance != null) {
            instance.incrementLaunchCount();
        }
        return instance;
    }

    protected static IAdManager createInstance(Application application2, PflxEnvironment env, boolean force) {
        if (force) {
            instance = null;
        }
        if (instance == null) {
            instance = new AdManager(application2, DefaultAppInfoName);
            instance.setEnvironment(env);
        }
        if (instance != null) {
            instance.incrementLaunchCount();
        }
        return instance;
    }

    public static AdManager getAdManagerInstance(Application application2) {
        if (instance == null) {
            createInstance(application2);
            initializeInstance(application2);
        }
        if (instance != null) {
            return instance;
        }
        throw new IllegalStateException();
    }

    protected AdManager(Application application2, String appInfoName) {
        this.application = application2;
        Context context = getContext();
        if (context != null) {
            this.pkgHelper = PackageHelper.createInstance(context);
            this.appInfo = AppInfo.createAppInfo(context, appInfoName);
            this.versionHelper = VersionHelper.createInstance(context);
            if (!isResourceInitializationInProgress()) {
                new Handler().post(new Runnable() {
                    public void run() {
                        try {
                            AdManager.this.setResourceInitializationInProgress(true);
                            UpdateResourceUtil.initializeResources(AdManager.this.getContext());
                            HtmlPageRegistry.createInstance(AdManager.this.getContext());
                        } finally {
                            AdManager.this.setResourceInitializationInProgress(false);
                        }
                    }
                });
            }
        }
    }

    public boolean isAllowReadDeviceData() {
        return this.allowReadDeviceData;
    }

    public void setAllowReadDeviceData(boolean allowReadDeviceData2) {
        this.allowReadDeviceData = allowReadDeviceData2;
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        if (editor != null) {
            editor.putBoolean("AllowReadDeviceData", allowReadDeviceData2);
            editor.commit();
        }
    }

    public void setRegistrationMode(IAdManager.RegistrationMode registrationMode2) {
        setRegistrationMode(registrationMode2, null);
    }

    public void setRegistrationMode(IAdManager.RegistrationMode registrationMode2, Activity context) {
        this.registrationMode = registrationMode2;
        if (registrationMode2 == IAdManager.RegistrationMode.RegistrationAtLaunch) {
            launchActivityAfterMillis(2000, context, true);
        } else if (registrationMode2 == IAdManager.RegistrationMode.RegistrationAfterIntervalInLaunches && shouldLaunchOnInterval(getLaunchCount(), false)) {
            launchActivityAfterMillis(2000, context, true);
        }
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        if (editor != null) {
            editor.putString("RegistrationMode", String.valueOf(registrationMode2));
            editor.commit();
        }
    }

    public IAdManager.RegistrationMode getRegistrationMode() {
        return this.registrationMode;
    }

    public void setRegistrationRequired(boolean isRegistrationRequired2) {
        this.isRegistrationRequired = isRegistrationRequired2;
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        if (editor != null) {
            editor.putBoolean("RegistrationRequired", isRegistrationRequired2);
            editor.commit();
        }
    }

    public boolean isRegistrationRequired() {
        return this.isRegistrationRequired;
    }

    public void setRegistrationInterval(int registrationInterval2) {
        if (registrationInterval2 < 0) {
            registrationInterval2 = 0;
        }
        this.registrationInterval = registrationInterval2;
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        if (editor != null) {
            editor.putInt("registrationInterval", registrationInterval2);
            editor.commit();
        }
    }

    public int getRegistrationInterval() {
        SharedPreferences prefs = getSharedPreferences();
        if (prefs != null) {
            this.registrationInterval = prefs.getInt("registrationInterval", 0);
        }
        return this.registrationInterval;
    }

    public void setShowingMultiAdView(boolean isShowingMultiAdView2) {
        this.isShowingMultiAdView = isShowingMultiAdView2;
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        if (editor != null) {
            editor.putBoolean("ShowingMultiAdView", isShowingMultiAdView2);
            editor.commit();
        }
    }

    public boolean isShowingMultiAdView() {
        return this.isShowingMultiAdView;
    }

    public boolean setRegistrationData(Map<String, String> registrationData) {
        if (registrationData == null || registrationData.isEmpty()) {
            Log.i("Pontiflex SDK", "Empty registration data.");
            return false;
        }
        Collection<String> fieldNames = this.appInfo.getOrderedFieldNames();
        if (fieldNames == null || fieldNames.size() == 0) {
            Log.e("Pontiflex SDK", "No registration fields are present.");
            return false;
        }
        RegistrationStorage storage = getRegistrationStorage();
        boolean validRegistationData = true;
        for (String key : registrationData.keySet()) {
            if (!fieldNames.contains(key)) {
                Log.e("Pontiflex SDK", key + " is not a valid registration field");
                validRegistationData = false;
            } else {
                String value = registrationData.get(key);
                if (FieldValidator.validate(key, value, this.appInfo.getValidationsForFieldNamed(key), registrationData, getContext())) {
                    storage.put(key, value);
                    commitStorageChanges(storage);
                } else {
                    validRegistationData = false;
                    Log.e("Pontiflex SDK", "Validation failed for " + key + ": " + value);
                }
            }
        }
        return validRegistationData;
    }

    public boolean setRegistrationData(String key, String value) {
        for (String fieldKey : this.appInfo.getOrderedFieldNames()) {
            if (fieldKey.equals(key)) {
                if (key.equals(this.appInfo.getPostalCodeFieldName())) {
                    String countryFieldName = this.appInfo.getCountryFieldName();
                    if (getRegistrationStorage().get(countryFieldName) == null) {
                        getRegistrationStorage().put(countryFieldName, "US");
                    }
                }
                Map<String, String> fields = new HashMap<>();
                RegistrationStorage storage = getRegistrationStorage();
                for (String field : storage.keys()) {
                    fields.put(field, storage.get(field));
                }
                if (FieldValidator.validate(key, value, this.appInfo.getValidationsForFieldNamed(key), fields, getContext())) {
                    storage.put(key, value);
                    commitStorageChanges(storage);
                    return true;
                }
                Log.e("Pontiflex SDK", "Validation failed for " + key + ": " + value);
            }
        }
        Log.e("Pontiflex SDK", key + " is not a valid registration field");
        return false;
    }

    public void commitStorageChanges(RegistrationStorage storage) {
        storage.commit(getContext());
    }

    public String getRegistrationData(String key) {
        return getRegistrationStorage().get(key);
    }

    public void clearRegistrationStorage() {
        getRegistrationStorage().clear(getContext());
    }

    public boolean hasValidRegistrationData() {
        for (String required : this.appInfo.getOrderedFieldNames()) {
            if (getRegistrationStorage().get(required) == null) {
                return false;
            }
        }
        return true;
    }

    public void startRegistrationActivity() {
        startRegistrationActivity(null);
    }

    public void startMultiOfferActivity() {
        startMultiOfferActivity(null);
    }

    public void startRegistrationActivity(Activity activityContext) {
        boolean isActivityContext;
        if (!isResourceInitializationInProgress()) {
            if (activityContext != null) {
                isActivityContext = true;
            } else {
                isActivityContext = false;
            }
            Context context = activityContext;
            if (context == null && this.application != null) {
                context = this.application.getApplicationContext();
            }
            if (context != null && !hasValidRegistrationData()) {
                Intent intent = new Intent(context, RegistrationActivity.class);
                if (isActivityContext) {
                    activityContext.startActivityForResult(intent, 1);
                    return;
                }
                intent.addFlags(268435456);
                context.startActivity(intent);
            }
        }
    }

    public void startMultiOfferActivity(Activity activityContext) {
        boolean isActivityContext;
        if (!isResourceInitializationInProgress() && isOnline()) {
            if (activityContext != null) {
                isActivityContext = true;
            } else {
                isActivityContext = false;
            }
            Context context = activityContext;
            if (context == null && this.application != null) {
                context = this.application.getApplicationContext();
            }
            if (context != null) {
                Intent intent = new Intent(context, MultiOfferActivity.class);
                intent.putExtra("offerCount", 3);
                if (isActivityContext) {
                    activityContext.startActivityForResult(intent, 1);
                    return;
                }
                intent.addFlags(268435456);
                context.startActivity(intent);
            }
        }
    }

    public PackageHelper getPkgHelper() {
        return this.pkgHelper;
    }

    /* access modifiers changed from: protected */
    public Application getApplication() {
        return this.application;
    }

    /* access modifiers changed from: protected */
    public Context getContext() {
        return getApplication().getApplicationContext();
    }

    private void launchActivityAfterMillis(int ms, final Activity context, final boolean isRegistration) {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (isRegistration) {
                    AdManager.this.startRegistrationActivity(context);
                } else {
                    AdManager.this.startMultiOfferActivity(context);
                }
            }
        }, (long) ms);
    }

    public boolean isOnline() {
        NetworkInfo info = ((ConnectivityManager) getContext().getSystemService("connectivity")).getActiveNetworkInfo();
        if (info != null) {
            return info.isConnected();
        }
        return false;
    }

    public boolean isOffline() {
        return !isOnline();
    }

    public int getRefreshIntervalMillis() {
        return this.refreshIntervalMs;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0014, code lost:
        if (((r5 - (r6 ? 1 : 0)) % r4.registrationInterval) == 0) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean shouldLaunchOnInterval(int r5, boolean r6) {
        /*
            r4 = this;
            r3 = 0
            r2 = 1
            if (r5 != r2) goto L_0x0006
            if (r6 != 0) goto L_0x0016
        L_0x0006:
            if (r5 <= r2) goto L_0x001a
            int r0 = r4.registrationInterval
            if (r0 == 0) goto L_0x0016
            if (r6 == 0) goto L_0x0018
            r0 = r2
        L_0x000f:
            int r0 = r5 - r0
            int r1 = r4.registrationInterval
            int r0 = r0 % r1
            if (r0 != 0) goto L_0x001a
        L_0x0016:
            r0 = r2
        L_0x0017:
            return r0
        L_0x0018:
            r0 = r3
            goto L_0x000f
        L_0x001a:
            r0 = r3
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.pontiflex.mobile.webview.sdk.AdManager.shouldLaunchOnInterval(int, boolean):boolean");
    }

    private void incrementLaunchCount() {
        setLaunchCount(getLaunchCount() + 1);
    }

    private void setLaunchCount(int count) {
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        if (editor != null) {
            editor.putInt("launchCount", count);
            editor.commit();
        }
    }

    /* access modifiers changed from: protected */
    public int getLaunchCount() {
        return getSharedPreferences().getInt("launchCount", 0);
    }

    public String getAdHostString() {
        try {
            return this.appInfo.getString(AppInfoPflxAdUrl);
        } catch (JSONException e) {
            return null;
        }
    }

    public String getAdUrl() {
        return getAdUrl(1);
    }

    public String getAdUrl(int offerCount) {
        Uri uri = null;
        try {
            uri = Uri.parse(getAdHostString()).buildUpon().appendPath(AdPath).appendPath(JsonPath).appendPath(Integer.toString(this.appInfo.getInt(AppInfoPid))).appendQueryParameter(AppInfoSubSourceId, this.appInfo.getString(AppInfoSubSourceId)).appendQueryParameter("numOffers", Integer.toString(offerCount)).appendQueryParameter("version", "2").build();
        } catch (JSONException e) {
            Log.e("Pontiflex SDK", "Error parsing json", e);
        }
        if (uri != null) {
            return uri.toString();
        }
        return null;
    }

    public RegistrationStorage getRegistrationStorage() {
        if (this.registrationStorage == null) {
            this.registrationStorage = new RegistrationStorage(this.application.getApplicationContext(), KeystoreFilename);
        }
        return this.registrationStorage;
    }

    public Iterator<String> getRegistrationKeysIterator() {
        return getRegistrationStorage().keys().iterator();
    }

    /* access modifiers changed from: protected */
    public SharedPreferences getSharedPreferences() {
        if (this.application != null) {
            return this.application.getSharedPreferences("Pontiflex", 0);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public SharedPreferences.Editor getSharedPreferencesEditor() {
        SharedPreferences prefs = getSharedPreferences();
        if (prefs != null) {
            return prefs.edit();
        }
        return null;
    }

    protected AdManager() {
    }

    public AppInfo getAppInfo() {
        return this.appInfo;
    }

    /* access modifiers changed from: protected */
    public void setAppInfo(AppInfo appInfo2) {
        this.appInfo = appInfo2;
    }

    public void setEnvironment(PflxEnvironment environment2) {
        this.environment = environment2;
    }

    public PflxEnvironment getEnvironment() {
        return this.environment;
    }

    public static String getKeystoreFilename() {
        return KeystoreFilename;
    }

    public static String getDefaultAppInfoName() {
        return DefaultAppInfoName;
    }

    public VersionHelper getVersionHelper() {
        return this.versionHelper;
    }

    public boolean isResourceInitializationInProgress() {
        return this.resourceInitializationInProgress;
    }

    public void setResourceInitializationInProgress(boolean resourceInitializationInProgress2) {
        this.resourceInitializationInProgress = resourceInitializationInProgress2;
    }

    public String getSdkVersionCode() {
        return VersionHelper.getInstance(getContext()).getPflxSdkVersionCode(getContext());
    }

    public String getSdkVersionName() {
        return VersionHelper.getInstance(getContext()).getPflxSdkVersionName(getContext());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pontiflex.mobile.webview.sdk.AdManager.showAd(boolean, android.app.Activity):boolean
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.pontiflex.mobile.webview.sdk.AdManager.showAd(int, android.app.Activity):boolean
      com.pontiflex.mobile.webview.sdk.IAdManager.showAd(int, android.app.Activity):boolean
      com.pontiflex.mobile.webview.sdk.AdManager.showAd(boolean, android.app.Activity):boolean */
    public boolean showAd() {
        return showAd(true, (Activity) null);
    }

    public boolean showAd(int launchInterval) {
        return showAd(launchInterval, true, true, null);
    }

    public boolean showAd(boolean withRegistration) {
        return showAd(withRegistration, (Activity) null);
    }

    public boolean showAd(int launchInterval, boolean showOnFirstLaunch, boolean withRegistration) {
        return showAd(launchInterval, showOnFirstLaunch, withRegistration, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.pontiflex.mobile.webview.sdk.AdManager.showAd(boolean, android.app.Activity):boolean
     arg types: [int, android.app.Activity]
     candidates:
      com.pontiflex.mobile.webview.sdk.AdManager.showAd(int, android.app.Activity):boolean
      com.pontiflex.mobile.webview.sdk.IAdManager.showAd(int, android.app.Activity):boolean
      com.pontiflex.mobile.webview.sdk.AdManager.showAd(boolean, android.app.Activity):boolean */
    public boolean showAd(Activity context) {
        return showAd(true, context);
    }

    public boolean showAd(int launchInterval, Activity context) {
        return showAd(launchInterval, true, true, context);
    }

    public boolean showAd(boolean withRegistration, Activity context) {
        return showAd(0, true, withRegistration, context);
    }

    public boolean showAd(int launchInterval, boolean showOnFirstLaunch, boolean withRegistration, Activity context) {
        boolean z;
        if (launchInterval < 0) {
            launchInterval = 0;
        }
        setRegistrationInterval(launchInterval);
        this.registrationMode = launchInterval == 0 ? IAdManager.RegistrationMode.RegistrationAtLaunch : IAdManager.RegistrationMode.RegistrationAfterIntervalInLaunches;
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        if (editor != null) {
            editor.putString("RegistrationMode", String.valueOf(this.registrationMode));
            editor.commit();
        }
        if (!shouldLaunchOnInterval(getLaunchCount(), showOnFirstLaunch)) {
            return false;
        }
        if (!withRegistration || this.appInfo.getRegistrationOffer() == null || hasValidRegistrationData()) {
            z = false;
        } else {
            z = true;
        }
        launchActivityAfterMillis(2000, context, z);
        return true;
    }
}
