package com.pontiflex.mobile.webview.utilities;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import com.pontiflex.mobile.webview.sdk.AdManager;
import java.lang.reflect.InvocationTargetException;

public class DeviceHelper {
    public static String getDeviceData(Context context, String field) {
        if (context == null) {
            throw new IllegalArgumentException("context is null");
        } else if (field == null) {
            throw new IllegalArgumentException("field is null");
        } else if ("email".equalsIgnoreCase(field)) {
            return getDeviceSetupEmail(context);
        } else {
            if ("phone".equalsIgnoreCase(field)) {
                return getDeviceSetupPhone(context);
            }
            String countryFieldName = AdManager.getAdManagerInstance((Application) context.getApplicationContext()).getAppInfo().getCountryFieldName();
            if (countryFieldName == null || !countryFieldName.equalsIgnoreCase(field)) {
                return "";
            }
            return getDeviceCountry(context);
        }
    }

    private static String getDeviceSetupPhone(Context context) {
        if (context == null) {
            throw new IllegalArgumentException();
        } else if (!AdManager.getAdManagerInstance((Application) context.getApplicationContext()).isAllowReadDeviceData()) {
            return "";
        } else {
            StringBuilder builder = new StringBuilder();
            if (context.getPackageManager().checkPermission("android.permission.READ_PHONE_STATE", context.getPackageName()) == 0) {
                builder.append(((TelephonyManager) context.getSystemService("phone")).getLine1Number());
            }
            return builder.toString();
        }
    }

    /* JADX INFO: Multiple debug info for r0v20 java.lang.reflect.Method: [D('getMethod' java.lang.reflect.Method), D('parameterTypes' java.lang.Class[])] */
    /* JADX INFO: Multiple debug info for r0v23 java.lang.Object: [D('accountsArray' java.lang.Object), D('getAccountsMethod' java.lang.reflect.Method)] */
    /* JADX INFO: Multiple debug info for r0v25 java.lang.Object[]: [D('accountsArray' java.lang.Object), D('objArray' java.lang.Object[])] */
    /* JADX INFO: Multiple debug info for r0v31 int: [D('i$' int), D('account' java.lang.Object)] */
    public static String getDeviceSetupEmail(Context context) {
        IllegalAccessException e;
        InvocationTargetException e2;
        NoSuchFieldException e3;
        NoSuchMethodException e4;
        LinkageError e5;
        ClassNotFoundException e6;
        if (context == null) {
            throw new IllegalArgumentException();
        }
        AdManager adManager = AdManager.getAdManagerInstance((Application) context.getApplicationContext());
        if (!adManager.isAllowReadDeviceData()) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        if (context.getPackageManager().checkPermission("android.permission.GET_ACCOUNTS", context.getPackageName()) == 0) {
            try {
                Class<?> cls = Class.forName("android.accounts.AccountManager");
                if (cls == null) {
                    try {
                        return builder.toString();
                    } catch (ClassNotFoundException e7) {
                        e6 = e7;
                        Log.i("Pontiflex SDK", "Current version of android doesn't support Accounts apis ", e6);
                        return builder.toString();
                    } catch (LinkageError e8) {
                        e5 = e8;
                        Log.e("Pontiflex SDK", "Current version of android doesn't support Accounts apis ", e5);
                        return builder.toString();
                    } catch (NoSuchMethodException e9) {
                        e4 = e9;
                        Log.i("Pontiflex SDK", "Current version of android doesn't support Accounts apis ", e4);
                        return builder.toString();
                    } catch (NoSuchFieldException e10) {
                        e3 = e10;
                        Log.i("Pontiflex SDK", "Current version of android doesn't support Accounts apis ", e3);
                        return builder.toString();
                    } catch (InvocationTargetException e11) {
                        e2 = e11;
                        Log.e("Pontiflex SDK", "Current version of android doesn't support Accounts apis ", e2);
                        return builder.toString();
                    } catch (IllegalAccessException e12) {
                        e = e12;
                        Log.e("Pontiflex SDK", "Current version of android doesn't support Accounts apis ", e);
                        return builder.toString();
                    }
                } else {
                    Object objAccountManager = cls.getMethod("get", Context.class).invoke(cls, context);
                    if (objAccountManager == null) {
                        return builder.toString();
                    }
                    Object accountsArray = objAccountManager.getClass().getMethod("getAccounts", new Class[0]).invoke(objAccountManager, new Object[0]);
                    if (accountsArray == null) {
                        return builder.toString();
                    }
                    if (accountsArray.getClass().isArray()) {
                        Object[] arr$ = (Object[]) accountsArray;
                        int len$ = arr$.length;
                        int i$ = 0;
                        while (true) {
                            if (i$ >= len$) {
                                break;
                            }
                            Object account = arr$[i$];
                            if (!("" + account.getClass().getField("type").get(account)).contains("MOTHER_USER_CREDS_TYPE")) {
                                String emailAccName = "" + account.getClass().getField("name").get(account);
                                if (FieldValidator.validate("email", emailAccName, adManager.getAppInfo().getValidationsForFieldNamed("email"), null, context)) {
                                    builder.append(emailAccName);
                                    break;
                                }
                            }
                            i$++;
                        }
                    }
                }
            } catch (ClassNotFoundException e13) {
                e6 = e13;
            } catch (LinkageError e14) {
                e5 = e14;
                Log.e("Pontiflex SDK", "Current version of android doesn't support Accounts apis ", e5);
                return builder.toString();
            } catch (NoSuchMethodException e15) {
                e4 = e15;
                Log.i("Pontiflex SDK", "Current version of android doesn't support Accounts apis ", e4);
                return builder.toString();
            } catch (NoSuchFieldException e16) {
                e3 = e16;
                Log.i("Pontiflex SDK", "Current version of android doesn't support Accounts apis ", e3);
                return builder.toString();
            } catch (InvocationTargetException e17) {
                e2 = e17;
                Log.e("Pontiflex SDK", "Current version of android doesn't support Accounts apis ", e2);
                return builder.toString();
            } catch (IllegalAccessException e18) {
                e = e18;
                Log.e("Pontiflex SDK", "Current version of android doesn't support Accounts apis ", e);
                return builder.toString();
            }
        }
        return builder.toString();
    }

    public static String getDeviceCountry(Context context) {
        return context.getResources().getConfiguration().locale.getCountry();
    }

    public static int getScreenOrientation(Activity activity) {
        try {
            Display getOrient = activity.getWindowManager().getDefaultDisplay();
            if (0 != 0) {
                return 0;
            }
            int orientation = activity.getApplicationContext().getResources().getConfiguration().orientation;
            if (orientation != 0) {
                return orientation;
            }
            if (getOrient.getWidth() == getOrient.getHeight()) {
                return 3;
            }
            if (getOrient.getWidth() < getOrient.getHeight()) {
                return 1;
            }
            return 2;
        } catch (Exception e) {
            Log.e("Pontiflex SDK", "Error in detrmining screen orientation", e);
            return 0;
        }
    }

    public static int getScreenWidth(Activity activity) {
        try {
            return activity.getWindowManager().getDefaultDisplay().getWidth();
        } catch (Exception e) {
            Log.e("Pontiflex SDK", "Error in detrmining screen width", e);
            return -1;
        }
    }

    public static int getScreenHeight(Activity activity) {
        try {
            return activity.getWindowManager().getDefaultDisplay().getHeight();
        } catch (Exception e) {
            Log.e("Pontiflex SDK", "Error in detrmining screen height", e);
            return -1;
        }
    }

    public static int getScreenDensity(Activity activity) {
        try {
            DisplayMetrics dm = activity.getApplicationContext().getResources().getDisplayMetrics();
            return dm.getClass().getField("densityDpi").getInt(dm);
        } catch (NoSuchFieldException e) {
            Log.i("Pontiflex SDK", "Screen density not available.");
            return 0;
        } catch (IllegalAccessException e2) {
            Log.e("Pontiflex SDK", "Not able to to get Screen density", e2);
            return 0;
        } catch (Exception e3) {
            Log.e("Pontiflex SDK", "Not able to to get Screen density", e3);
            return 0;
        }
    }

    public static int getScreenSize(Activity activity) {
        try {
            Configuration config = activity.getApplicationContext().getResources().getConfiguration();
            return config.getClass().getField("screenLayout").getInt(config) & 15;
        } catch (NoSuchFieldException e) {
            Log.i("Pontiflex SDK", "Screen size not available.");
            return -1;
        } catch (IllegalAccessException e2) {
            Log.e("Pontiflex SDK", "Not able to to get Screen size", e2);
            return -1;
        } catch (Exception e3) {
            Log.e("Pontiflex SDK", "Not able to to get Screen size", e3);
            return -1;
        }
    }
}
