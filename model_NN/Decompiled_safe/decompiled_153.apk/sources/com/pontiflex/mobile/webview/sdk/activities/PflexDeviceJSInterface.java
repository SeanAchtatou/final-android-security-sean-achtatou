package com.pontiflex.mobile.webview.sdk.activities;

import com.pontiflex.mobile.webview.sdk.IPflexJSInterface;
import com.pontiflex.mobile.webview.utilities.DeviceHelper;

/* compiled from: BaseActivity */
class PflexDeviceJSInterface implements IPflexJSInterface {
    private BaseActivity baseActivity;

    protected PflexDeviceJSInterface(BaseActivity activity) {
        this.baseActivity = activity;
    }

    public String getDeviceData(String field) {
        return this.baseActivity.getDeviceData(field);
    }

    public int getScreenOrientation() {
        return DeviceHelper.getScreenOrientation(this.baseActivity);
    }

    public int getScreenWidth() {
        return DeviceHelper.getScreenWidth(this.baseActivity);
    }

    public int getScreenHeight() {
        return DeviceHelper.getScreenHeight(this.baseActivity);
    }

    public int getScreenDensity() {
        return DeviceHelper.getScreenDensity(this.baseActivity);
    }

    public int getScreenSize() {
        return DeviceHelper.getScreenSize(this.baseActivity);
    }
}
