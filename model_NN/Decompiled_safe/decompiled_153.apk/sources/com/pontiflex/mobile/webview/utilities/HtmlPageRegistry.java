package com.pontiflex.mobile.webview.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class HtmlPageRegistry {
    private static HtmlPageRegistry _instance = null;
    private String baseHtmlResourcesPath = null;
    protected List<String> registry = null;

    public enum Page {
        registration,
        multioffer,
        privacy,
        privacylist
    }

    public enum ScreenDensity {
        ldpi,
        mdpi,
        hdpi,
        xhdpi,
        DEFAULT
    }

    public enum ScreenOrientation {
        portrait,
        landscape,
        DEFAULT
    }

    public enum ScreenSize {
        small,
        normal,
        large,
        xlarge,
        DEFAULT
    }

    public static HtmlPageRegistry getInstance(Context context) {
        if (_instance == null) {
            createInstance(context);
        }
        if (_instance != null) {
            return _instance;
        }
        throw new IllegalStateException();
    }

    public static HtmlPageRegistry createInstance(Context context) {
        if (_instance == null) {
            _instance = new HtmlPageRegistry(context);
        }
        return _instance;
    }

    private HtmlPageRegistry(Context context) {
        initializeRegistry(context);
    }

    private void initializeRegistry(Context context) {
        this.registry = new ArrayList();
        PackageHelper pkgHelper = PackageHelper.getInstance(context);
        List<String> htmlPaths = pkgHelper.getHtmlPaths(context);
        Log.d("Pontiflex SDK", "htmlPaths: " + htmlPaths.toString());
        this.baseHtmlResourcesPath = pkgHelper.getBaseHtmlResourcesPath();
        for (String path : htmlPaths) {
            registerPath(path);
        }
    }

    /* access modifiers changed from: protected */
    public void registerPath(String path) {
        this.registry.add(path);
    }

    private String getHtml(InputStream stream) {
        if (stream == null) {
            return null;
        }
        int size = 0;
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buffer = new byte[8192];
            while (true) {
                int read = stream.read(buffer);
                if (read < 0) {
                    return new String(out.toByteArray());
                }
                out.write(buffer, 0, read);
                size += read;
            }
        } catch (IOException e) {
            IOException e2 = e;
            Log.e("Pontiflex SDK", "Couldn't load html file: " + e2.getMessage(), e2);
            return null;
        }
    }

    public String getRegistrationHtml(Activity activity, boolean useScreenSize) {
        return getHtml(activity, useScreenSize, Page.registration);
    }

    public String getMultiOfferHtml(Activity activity, boolean useScreenSize) {
        return getHtml(activity, useScreenSize, Page.multioffer);
    }

    public String getPrivacyHtml(Activity activity, boolean useScreenSize) {
        return getHtml(activity, useScreenSize, Page.privacy);
    }

    public String getPrivacyListHtml(Activity activity, boolean useScreenSize) {
        return getHtml(activity, useScreenSize, Page.privacylist);
    }

    public String getRegistrationHtmlPath(Activity activity, boolean useScreenSize) {
        return getHtmlPath(activity, useScreenSize, Page.registration);
    }

    public String getMultiOfferHtmlPath(Activity activity, boolean useScreenSize) {
        return getHtmlPath(activity, useScreenSize, Page.multioffer);
    }

    public String getPrivacyHtmlPath(Activity activity, boolean useScreenSize) {
        return getHtmlPath(activity, useScreenSize, Page.privacy);
    }

    public String getPrivacyListHtmlPath(Activity activity, boolean useScreenSize) {
        return getHtmlPath(activity, useScreenSize, Page.privacylist);
    }

    /* access modifiers changed from: protected */
    public String getHtml(Activity activity, boolean useScreenSize, Page p) {
        String path = getHtmlPath(activity, useScreenSize, p);
        if (path != null) {
            return getHtml(PackageHelper.getInstance(activity.getApplicationContext()).getHtmlResource(path, activity.getApplicationContext()));
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public String getHtmlPath(Activity activity, boolean useScreenSize, Page p) {
        ScreenOrientation so = getScreenOrientation(activity);
        ScreenDensity sd = getScreenDensity(activity);
        ScreenSize ss = getScreenSize(activity);
        if (useScreenSize) {
            String path = getHtmlPath(p, so, ss);
        }
        String path2 = getHtmlPath(p, so, sd);
        if (path2 != null) {
            return this.baseHtmlResourcesPath + "/" + path2;
        }
        return path2;
    }

    /* access modifiers changed from: protected */
    public String getHtmlPath(Page p, ScreenOrientation so, ScreenSize ss) {
        String path = getKeyPath(p, so, ss);
        if (this.registry.contains(path)) {
            return path;
        }
        String path2 = getKeyPath(p, so, ScreenSize.DEFAULT);
        if (this.registry.contains(path2)) {
            return path2;
        }
        String path3 = getKeyPath(p, ScreenOrientation.DEFAULT, ss);
        if (this.registry.contains(path3)) {
            return path3;
        }
        String path4 = getKeyPath(p, ScreenOrientation.DEFAULT, ScreenSize.DEFAULT);
        if (this.registry.contains(path4)) {
            return path4;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String getHtmlPath(Page p, ScreenOrientation so, ScreenDensity sd) {
        String path = getKeyPath(p, so, sd);
        if (this.registry.contains(path)) {
            return path;
        }
        String path2 = getKeyPath(p, so, ScreenDensity.DEFAULT);
        if (this.registry.contains(path2)) {
            return path2;
        }
        String path3 = getKeyPath(p, ScreenOrientation.DEFAULT, sd);
        if (this.registry.contains(path3)) {
            return path3;
        }
        String path4 = getKeyPath(p, ScreenOrientation.DEFAULT, ScreenDensity.DEFAULT);
        if (this.registry.contains(path4)) {
            return path4;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String getKeyPath(Page p, ScreenOrientation so, ScreenSize ss) {
        StringBuilder sb = new StringBuilder();
        sb.append("html").append(File.separator);
        if (!ScreenOrientation.DEFAULT.equals(so)) {
            sb.append(so.toString()).append(File.separator);
        }
        if (!ScreenSize.DEFAULT.equals(ss)) {
            sb.append(ss.toString()).append(File.separator);
        }
        sb.append(p.toString()).append(".html");
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public String getKeyPath(Page p, ScreenOrientation so, ScreenDensity sd) {
        StringBuilder sb = new StringBuilder();
        sb.append("html").append(File.separator);
        if (!ScreenOrientation.DEFAULT.equals(so)) {
            sb.append(so.toString()).append(File.separator);
        }
        if (!ScreenDensity.DEFAULT.equals(sd)) {
            sb.append(sd.toString()).append(File.separator);
        }
        sb.append(p.toString()).append(".html");
        return sb.toString();
    }

    private ScreenOrientation getScreenOrientation(Activity activity) {
        Display getOrient = activity.getWindowManager().getDefaultDisplay();
        int orientation = 0;
        if (0 == 0 && (orientation = activity.getApplicationContext().getResources().getConfiguration().orientation) == 0) {
            if (getOrient.getWidth() == getOrient.getHeight()) {
                orientation = 3;
            } else if (getOrient.getWidth() < getOrient.getHeight()) {
                orientation = 1;
            } else {
                orientation = 2;
            }
        }
        if (orientation == 1) {
            return ScreenOrientation.portrait;
        }
        if (orientation == 2) {
            return ScreenOrientation.landscape;
        }
        return ScreenOrientation.DEFAULT;
    }

    private ScreenSize getScreenSize(Activity activity) {
        Configuration config = activity.getApplicationContext().getResources().getConfiguration();
        int screensize = 0;
        try {
            screensize = config.getClass().getField("screenLayout").getInt(config) & 15;
        } catch (NoSuchFieldException e) {
            Log.i("Pontiflex SDK", "Screen size not available.");
        } catch (IllegalAccessException e2) {
            Log.e("Pontiflex SDK", "Not able to to get Screen size", e2);
        }
        if (screensize == 1) {
            return ScreenSize.small;
        }
        if (screensize == 2) {
            return ScreenSize.normal;
        }
        if (screensize == 3) {
            return ScreenSize.large;
        }
        if (screensize == 4) {
            return ScreenSize.xlarge;
        }
        if (screensize == 0) {
            return ScreenSize.normal;
        }
        return ScreenSize.DEFAULT;
    }

    private ScreenDensity getScreenDensity(Activity activity) {
        DisplayMetrics dm = activity.getApplicationContext().getResources().getDisplayMetrics();
        int density = 0;
        try {
            density = dm.getClass().getField("densityDpi").getInt(dm);
        } catch (NoSuchFieldException e) {
            Log.i("Pontiflex SDK", "Screen density not available.");
        } catch (IllegalAccessException e2) {
            Log.e("Pontiflex SDK", "Not able to to get Screen density", e2);
        }
        if (density == 120) {
            return ScreenDensity.ldpi;
        }
        if (density == 160) {
            return ScreenDensity.mdpi;
        }
        if (density == 240) {
            return ScreenDensity.hdpi;
        }
        if (density == 320) {
            return ScreenDensity.xhdpi;
        }
        return ScreenDensity.DEFAULT;
    }

    /* access modifiers changed from: protected */
    public boolean isRegistryEmpty() {
        return this.registry == null || this.registry.isEmpty();
    }

    /* access modifiers changed from: protected */
    public void clearRegistry() {
        if (this.registry != null) {
            this.registry.clear();
        }
    }

    public String getBaseHtmlResourcesPath() {
        return this.baseHtmlResourcesPath;
    }
}
