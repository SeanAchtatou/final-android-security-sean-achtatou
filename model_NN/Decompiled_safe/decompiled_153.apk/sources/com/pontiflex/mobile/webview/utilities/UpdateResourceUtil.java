package com.pontiflex.mobile.webview.utilities;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.pontiflex.mobile.webview.models.ResourceData;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

public class UpdateResourceUtil {
    static final String PontiflexSdkVersionBasename = "pflx_version";
    static final List<String> RESOURCES_INSIDE_JAR = Arrays.asList("jsclient");
    static final HashMap<String, String> RESOURCES_MAP = new HashMap<String, String>() {
        {
            put("jsclient", "js-client.zip");
            put("appinfojson", "AppInfo.json");
        }
    };
    private static final String RES_JS_CLIENT = "js-client";
    static final String RES_PATH = "pflx_res";
    static final String TEMP_RES_PATH = "pflx_res_temp";
    static final List<String> VERSIONED_RESOURCES = Arrays.asList("jsclient");
    static final String lastUpdatedDateSuffix = "_lastUpdatedDate";
    static final String versionCodeSuffix = "_versionCode";

    /* JADX INFO: Multiple debug info for r0v5 int: [D('httpClient' org.apache.http.impl.client.DefaultHttpClient), D('statusCode' int)] */
    /* JADX INFO: Multiple debug info for r0v6 java.io.InputStream: [D('inputStream' java.io.InputStream), D('statusCode' int)] */
    /* JADX INFO: Multiple debug info for r5v18 java.io.File: [D('dirPath' java.io.File), D('resourceFile' java.io.File)] */
    /* JADX INFO: Multiple debug info for r5v24 byte[]: [D('buffer' byte[]), D('resourceFile' java.io.File)] */
    /* JADX INFO: Multiple debug info for r1v6 java.util.Iterator<java.lang.String>: [D('i$' java.util.Iterator), D('params' org.apache.http.params.HttpParams)] */
    public static boolean downloadResource(String url, Map<String, String> headers, String resourcePath) {
        Throwable th;
        FileOutputStream fOut;
        if (url == null) {
            throw new IllegalArgumentException("Invalid Download Resource url");
        } else if (resourcePath == null) {
            throw new IllegalArgumentException("Invalid store path for downloaded resource.");
        } else {
            DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpGet request = new HttpGet(url);
            if (headers != null) {
                for (String headerKey : headers.keySet()) {
                    request.addHeader(headerKey, headers.get(headerKey));
                }
            }
            FileOutputStream fOut2 = null;
            try {
                HttpResponse response = httpClient.execute(request);
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    InputStream inputStream = response.getEntity().getContent();
                    new File(resourcePath.substring(0, resourcePath.lastIndexOf((String) File.separator))).mkdirs();
                    File resourceFile = new File(resourcePath);
                    if (resourceFile.exists()) {
                        resourceFile.delete();
                    }
                    FileOutputStream fOut3 = new FileOutputStream(resourceFile);
                    try {
                        byte[] buffer = new byte[256];
                        while (true) {
                            int len = inputStream.read(buffer);
                            if (len > 0) {
                                fOut3.write(buffer, 0, len);
                            } else {
                                closeStreams(null, fOut3, null);
                                return true;
                            }
                        }
                    } catch (ClientProtocolException e) {
                        e = e;
                        fOut2 = fOut3;
                        try {
                            Log.d("Pontiflex SDK", "Error in the HTTP protocol", e);
                            closeStreams(null, fOut2, null);
                            return false;
                        } catch (Throwable th2) {
                            Throwable th3 = th2;
                            fOut = fOut2;
                            th = th3;
                            closeStreams(null, fOut, null);
                            throw th;
                        }
                    } catch (IOException e2) {
                        e = e2;
                        fOut2 = fOut3;
                        Log.d("Pontiflex SDK", "Error in the Read or write from/to download stream", e);
                        closeStreams(null, fOut2, null);
                        return false;
                    } catch (Throwable th4) {
                        th = th4;
                        fOut = fOut3;
                        closeStreams(null, fOut, null);
                        throw th;
                    }
                } else {
                    Log.d("Pontiflex SDK", "Invalid HTTP response statuscode: " + statusCode);
                    closeStreams(null, null, null);
                    return false;
                }
            } catch (ClientProtocolException e3) {
                e = e3;
                Log.d("Pontiflex SDK", "Error in the HTTP protocol", e);
                closeStreams(null, fOut2, null);
                return false;
            } catch (IOException e4) {
                e = e4;
                Log.d("Pontiflex SDK", "Error in the Read or write from/to download stream", e);
                closeStreams(null, fOut2, null);
                return false;
            }
        }
    }

    public static boolean copyResource(String fromPath, String toPath) {
        if (fromPath == null || toPath == null) {
            throw new IllegalArgumentException("Invalid from or to path for copy operation");
        }
        File srcFile = new File(fromPath);
        if (!srcFile.exists()) {
            throw new IllegalArgumentException("Source file doesn't exist.");
        }
        new File(toPath.substring(0, toPath.lastIndexOf(File.separator))).mkdirs();
        File destFile = new File(toPath);
        if (destFile.exists()) {
            destFile.delete();
        }
        FileChannel source = null;
        FileChannel destination = null;
        try {
            source = new FileInputStream(srcFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
            if (source != null) {
                try {
                    source.close();
                } catch (IOException e) {
                }
            }
            if (destination == null) {
                return true;
            }
            try {
                destination.close();
                return true;
            } catch (IOException e2) {
                return true;
            }
        } catch (FileNotFoundException e3) {
            Log.d("Pontiflex SDK", "Unable to create destination file for copy", e3);
            if (source != null) {
                try {
                    source.close();
                } catch (IOException e4) {
                }
            }
            if (destination != null) {
                try {
                    destination.close();
                } catch (IOException e5) {
                }
            }
        } catch (IOException e6) {
            Log.d("Pontiflex SDK", "Unable to copy from source file to destination file", e6);
            if (source != null) {
                try {
                    source.close();
                } catch (IOException e7) {
                }
            }
            if (destination != null) {
                try {
                    destination.close();
                } catch (IOException e8) {
                }
            }
        } catch (Throwable th) {
            if (source != null) {
                try {
                    source.close();
                } catch (IOException e9) {
                }
            }
            if (destination != null) {
                try {
                    destination.close();
                } catch (IOException e10) {
                }
            }
            throw th;
        }
        return false;
    }

    public static boolean moveResource(String fromPath, String toPath) {
        if (fromPath == null || toPath == null) {
            throw new IllegalArgumentException("Invalid from or to path for move operation");
        }
        File srcFile = new File(fromPath);
        if (!srcFile.exists()) {
            throw new IllegalArgumentException("Source file doesn't exist.");
        }
        new File(toPath.substring(0, toPath.lastIndexOf(File.separator))).mkdirs();
        File destFile = new File(toPath);
        if (destFile.exists()) {
            destFile.delete();
        }
        return srcFile.renameTo(destFile);
    }

    public static String calculateChecksum(String resourcePath) {
        int read;
        if (resourcePath == null) {
            throw new IllegalArgumentException("Invalid path of resource for checksum calculation");
        }
        try {
            InputStream fin = new FileInputStream(new File(resourcePath));
            MessageDigest md5er = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[1024];
            do {
                read = fin.read(buffer);
                if (read > 0) {
                    md5er.update(buffer, 0, read);
                }
            } while (read != -1);
            fin.close();
            byte[] digest = md5er.digest();
            if (digest == null) {
                return null;
            }
            String strDigest = "";
            for (int i = 0; i < digest.length; i++) {
                strDigest = strDigest + Integer.toString((digest[i] & 255) + 256, 16).substring(1);
            }
            return strDigest;
        } catch (Exception e) {
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r13v5 byte[]: [D('buf' byte[]), D('dir' java.io.File)] */
    /* JADX INFO: Multiple debug info for r1v11 java.util.Enumeration<? extends java.util.zip.ZipEntry>: [D('entries' java.util.Enumeration<? extends java.util.zip.ZipEntry>), D('zipFile' java.io.File)] */
    /* JADX INFO: Multiple debug info for r4v8 int: [D('len' int), D('fos' java.io.FileOutputStream)] */
    public static boolean extractResource(String sourcePath) {
        FileWriter fw;
        FileOutputStream fos;
        InputStream is;
        InputStream is2;
        FileWriter fw2;
        FileOutputStream fos2;
        FileOutputStream fos3;
        FileWriter fw3;
        InputStream is3;
        Throwable th;
        FileWriter fw4;
        if (sourcePath == null) {
            throw new IllegalArgumentException("Invalid path for resource to be extracted.");
        }
        File zipFile = new File(sourcePath);
        if (!zipFile.exists()) {
            return false;
        }
        String dirPath = sourcePath.substring(0, sourcePath.lastIndexOf(File.separator)) + File.separator + sourcePath.substring(sourcePath.lastIndexOf(File.separator) + 1, sourcePath.lastIndexOf("."));
        File dir = new File(dirPath);
        if (dir.exists()) {
            deleteDir(dir);
        }
        dir.mkdirs();
        byte[] buf = new byte[1024];
        HashMap<String, Boolean> zipElements = new HashMap<>();
        try {
            ZipFile zipFileToExtract = new ZipFile(zipFile);
            Enumeration<? extends ZipEntry> entries = zipFileToExtract.entries();
            String replacementPath = "file://" + dirPath;
            FileOutputStream fos4 = null;
            InputStream is4 = null;
            FileWriter fw5 = null;
            while (entries.hasMoreElements()) {
                try {
                    ZipEntry entry = (ZipEntry) entries.nextElement();
                    zipElements.put(entry.getName(), Boolean.valueOf(entry.isDirectory()));
                    File outputFile = new File(dirPath, entry.getName());
                    if (entry.isDirectory()) {
                        outputFile.mkdir();
                    } else {
                        if (outputFile.exists()) {
                            outputFile.delete();
                        }
                        InputStream is5 = zipFileToExtract.getInputStream(entry);
                        try {
                            FileOutputStream fos5 = new FileOutputStream(outputFile);
                            try {
                                if (entry.getName().endsWith(".html")) {
                                    String htmlString = PackageHelper.convertStreamToString(is5).replaceAll("file:///android_asset", replacementPath);
                                    fw4 = new FileWriter(outputFile);
                                    try {
                                        fw4.write(htmlString);
                                        fw4.flush();
                                        fw4.close();
                                    } catch (ZipException e) {
                                        e = e;
                                        fw = fw4;
                                        fos = fos5;
                                        is = is5;
                                        try {
                                            Log.e("Pontiflex SDK", "Not able to extract html package", e);
                                            closeStreams(is2, fos2, fw2);
                                            return false;
                                        } catch (Throwable th2) {
                                            Throwable th3 = th2;
                                            fos3 = fos2;
                                            fw3 = fw2;
                                            is3 = is2;
                                            th = th3;
                                            closeStreams(is3, fos3, fw3);
                                            throw th;
                                        }
                                    } catch (IOException e2) {
                                        e = e2;
                                        fw2 = fw4;
                                        fos2 = fos5;
                                        is2 = is5;
                                        Log.e("Pontiflex SDK", "Not able to extract html package", e);
                                        closeStreams(is2, fos2, fw2);
                                        return false;
                                    } catch (Throwable th4) {
                                        th = th4;
                                        fw3 = fw4;
                                        is3 = is5;
                                        fos3 = fos5;
                                        closeStreams(is3, fos3, fw3);
                                        throw th;
                                    }
                                } else {
                                    while (true) {
                                        int len = is5.read(buf);
                                        if (len <= 0) {
                                            break;
                                        }
                                        fos5.write(buf, 0, len);
                                    }
                                    fw4 = fw5;
                                }
                                fw5 = fw4;
                                fos4 = fos5;
                                is4 = is5;
                            } catch (ZipException e3) {
                                e = e3;
                                fw = fw5;
                                fos = fos5;
                                is = is5;
                                Log.e("Pontiflex SDK", "Not able to extract html package", e);
                                closeStreams(is2, fos2, fw2);
                                return false;
                            } catch (IOException e4) {
                                e = e4;
                                fw2 = fw5;
                                fos2 = fos5;
                                is2 = is5;
                                Log.e("Pontiflex SDK", "Not able to extract html package", e);
                                closeStreams(is2, fos2, fw2);
                                return false;
                            } catch (Throwable th5) {
                                th = th5;
                                fw3 = fw5;
                                is3 = is5;
                                fos3 = fos5;
                                closeStreams(is3, fos3, fw3);
                                throw th;
                            }
                        } catch (ZipException e5) {
                            e = e5;
                            fw = fw5;
                            fos = fos4;
                            is = is5;
                            Log.e("Pontiflex SDK", "Not able to extract html package", e);
                            closeStreams(is2, fos2, fw2);
                            return false;
                        } catch (IOException e6) {
                            e = e6;
                            fw2 = fw5;
                            fos2 = fos4;
                            is2 = is5;
                            Log.e("Pontiflex SDK", "Not able to extract html package", e);
                            closeStreams(is2, fos2, fw2);
                            return false;
                        } catch (Throwable th6) {
                            th = th6;
                            fw3 = fw5;
                            is3 = is5;
                            fos3 = fos4;
                            closeStreams(is3, fos3, fw3);
                            throw th;
                        }
                    }
                } catch (ZipException e7) {
                    e = e7;
                    fw = fw5;
                    fos = fos4;
                    is = is4;
                    Log.e("Pontiflex SDK", "Not able to extract html package", e);
                    closeStreams(is2, fos2, fw2);
                    return false;
                } catch (IOException e8) {
                    e = e8;
                    fw2 = fw5;
                    fos2 = fos4;
                    is2 = is4;
                    Log.e("Pontiflex SDK", "Not able to extract html package", e);
                    closeStreams(is2, fos2, fw2);
                    return false;
                } catch (Throwable th7) {
                    th = th7;
                    fw3 = fw5;
                    is3 = is4;
                    fos3 = fos4;
                    closeStreams(is3, fos3, fw3);
                    throw th;
                }
            }
            Log.d("Pontiflex SDK", "Extracting pontiflex package is successful");
            boolean validateExtractedResources = validateExtractedResources(zipElements, dirPath);
            closeStreams(is4, fos4, fw5);
            return validateExtractedResources;
        } catch (ZipException e9) {
            e = e9;
            fw = null;
            fos = null;
            is = null;
            Log.e("Pontiflex SDK", "Not able to extract html package", e);
            closeStreams(is2, fos2, fw2);
            return false;
        } catch (IOException e10) {
            e = e10;
            fw2 = null;
            fos2 = null;
            is2 = null;
            Log.e("Pontiflex SDK", "Not able to extract html package", e);
            closeStreams(is2, fos2, fw2);
            return false;
        } catch (Throwable th8) {
            th = th8;
            fw3 = null;
            is3 = null;
            fos3 = null;
            closeStreams(is3, fos3, fw3);
            throw th;
        }
    }

    private static boolean deleteDir(File dir) {
        if (dir == null) {
            throw new IllegalArgumentException("invalid dir to delete");
        }
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String file : children) {
                if (!deleteDir(new File(dir, file))) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean validateExtractedResources(java.util.Map<java.lang.String, java.lang.Boolean> r7, java.lang.String r8) {
        /*
            r6 = 0
            if (r7 == 0) goto L_0x000b
            int r3 = r7.size()
            if (r3 == 0) goto L_0x000b
            if (r8 != 0) goto L_0x000d
        L_0x000b:
            r3 = r6
        L_0x000c:
            return r3
        L_0x000d:
            java.util.Set r3 = r7.keySet()
            java.util.Iterator r2 = r3.iterator()
        L_0x0015:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x005c
            java.lang.Object r0 = r2.next()
            java.lang.String r0 = (java.lang.String) r0
            java.io.File r1 = new java.io.File
            r1.<init>(r8, r0)
            boolean r3 = r1.exists()
            if (r3 == 0) goto L_0x003c
            boolean r4 = r1.isDirectory()
            java.lang.Object r3 = r7.get(r0)
            java.lang.Boolean r3 = (java.lang.Boolean) r3
            boolean r3 = r3.booleanValue()
            if (r4 == r3) goto L_0x0015
        L_0x003c:
            java.lang.String r3 = "Pontiflex SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Resource: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r5 = " was not extracted correctly."
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            android.util.Log.e(r3, r4)
            r3 = r6
            goto L_0x000c
        L_0x005c:
            r3 = 1
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.pontiflex.mobile.webview.utilities.UpdateResourceUtil.validateExtractedResources(java.util.Map, java.lang.String):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    public static void initializeResources(Context context) {
        Exception e;
        boolean status;
        FileWriter fw = null;
        for (String resourceKey : RESOURCES_MAP.keySet()) {
            String resource = RESOURCES_MAP.get(resourceKey);
            String dirPath = context.getFilesDir().getPath() + "/" + RES_PATH;
            String path = dirPath + "/" + resource;
            new File(dirPath).mkdirs();
            if (new File(path).exists()) {
                if (!checkVersion(context, resourceKey)) {
                    Log.d("Pontiflex SDK", "Version check failed for resource: " + resource);
                } else if (!checkNonVersionResource(context, resourceKey)) {
                    Log.d("Pontiflex SDK", "non versioned resource is not updated from server resource: " + resource);
                } else {
                    closeStreams(null, null, fw);
                }
            }
            Log.d("Pontiflex SDK", "Initialization started for resource: " + resource);
            if (RESOURCES_INSIDE_JAR.contains(resourceKey)) {
                status = extractResourceFromJarAndCopy(path, context, resource);
                if (!status) {
                    status = copyResourceFromAsset(resource, path, context);
                }
            } else {
                status = copyResourceFromAsset(resource, path, context);
            }
            if (!status) {
                Log.e("Pontiflex SDK", "Initialization failed for resource: " + resource);
                closeStreams(null, null, fw);
            } else {
                if (resourceKey.equals("appinfojson")) {
                    AppInfo appInfo = AppInfo.createAppInfo(context);
                    FileWriter fw2 = new FileWriter(path, false);
                    try {
                        fw2.append((CharSequence) appInfo.toString());
                        fw2.flush();
                        fw = fw2;
                    } catch (Exception e2) {
                        e = e2;
                        fw = fw2;
                        try {
                            Log.e("Pontiflex SDK", "Resource initilization failed: ", e);
                            closeStreams(null, null, fw);
                        } catch (Throwable th) {
                            th = th;
                            closeStreams(null, null, fw);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        fw = fw2;
                        closeStreams(null, null, fw);
                        throw th;
                    }
                }
                try {
                    if (!resource.toLowerCase().endsWith("zip") || extractResource(path)) {
                        VersionHelper.getInstance(context).resetCache();
                        closeStreams(null, null, fw);
                    } else {
                        Log.e("Pontiflex SDK", "Initialization failed for resource: " + resource + " during extraction");
                        closeStreams(null, null, fw);
                    }
                } catch (Exception e3) {
                    e = e3;
                    Log.e("Pontiflex SDK", "Resource initilization failed: ", e);
                    closeStreams(null, null, fw);
                }
            }
        }
    }

    /* JADX INFO: Multiple debug info for r1v7 int: [D('i$' int), D('fileName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v16 java.io.InputStream: [D('arr$' java.lang.String[]), D('is' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r12v19 byte[]: [D('path' java.lang.String), D('buf' byte[])] */
    /* JADX INFO: Multiple debug info for r1v10 int: [D('len' int), D('fileName' java.lang.String)] */
    private static boolean copyResourceFromAsset(String resourceName, String path, Context context) {
        InputStream is;
        String[] fileNames;
        FileOutputStream fos;
        InputStream is2;
        FileOutputStream fos2;
        Throwable th;
        FileOutputStream fos3;
        try {
            String[] fileNames2 = context.getAssets().list("");
            if (fileNames2 != null) {
                try {
                    if (fileNames2.length != 0) {
                        int i = resourceName.lastIndexOf(".");
                        String prefix = resourceName.substring(0, i);
                        String suffix = resourceName.substring(i + 1);
                        String[] arr$ = fileNames2;
                        int len$ = arr$.length;
                        int i$ = 0;
                        while (i$ < len$) {
                            String fileName = arr$[i$];
                            if (!fileName.startsWith(prefix) || !fileName.endsWith(suffix)) {
                                i$++;
                            } else {
                                InputStream is3 = context.getAssets().open(fileName);
                                try {
                                    fos3 = new FileOutputStream(path);
                                } catch (IOException e) {
                                    e = e;
                                    is = is3;
                                    fileNames = fileNames2;
                                    fos = null;
                                    try {
                                        Log.e("Pontiflex SDK", "Copying resource: " + resourceName + " failed from assets for dev", e);
                                        closeStreams(is, fos, null);
                                        return false;
                                    } catch (Throwable th2) {
                                        fos2 = fos;
                                        th = th2;
                                        is2 = is;
                                        closeStreams(is2, fos2, null);
                                        throw th;
                                    }
                                } catch (Throwable th3) {
                                    fos2 = null;
                                    is2 = is3;
                                    th = th3;
                                    closeStreams(is2, fos2, null);
                                    throw th;
                                }
                                try {
                                    byte[] buf = new byte[256];
                                    while (true) {
                                        int len = is3.read(buf);
                                        if (len > 0) {
                                            fos3.write(buf, 0, len);
                                        } else {
                                            fos3.flush();
                                            closeStreams(is3, fos3, null);
                                            return true;
                                        }
                                    }
                                } catch (IOException e2) {
                                    e = e2;
                                    is = is3;
                                    fos = fos3;
                                    fileNames = fileNames2;
                                    Log.e("Pontiflex SDK", "Copying resource: " + resourceName + " failed from assets for dev", e);
                                    closeStreams(is, fos, null);
                                    return false;
                                } catch (Throwable th4) {
                                    fos2 = fos3;
                                    is2 = is3;
                                    th = th4;
                                    closeStreams(is2, fos2, null);
                                    throw th;
                                }
                            }
                        }
                        closeStreams(null, null, null);
                        return false;
                    }
                } catch (IOException e3) {
                    e = e3;
                    fos = null;
                    is = null;
                    fileNames = fileNames2;
                    Log.e("Pontiflex SDK", "Copying resource: " + resourceName + " failed from assets for dev", e);
                    closeStreams(is, fos, null);
                    return false;
                } catch (Throwable th5) {
                    th = th5;
                    fos2 = null;
                    is2 = null;
                    closeStreams(is2, fos2, null);
                    throw th;
                }
            }
            closeStreams(null, null, null);
            return false;
        } catch (IOException e4) {
            e = e4;
            is = null;
            fileNames = null;
            fos = null;
        } catch (Throwable th6) {
            fos2 = null;
            is2 = null;
            th = th6;
            closeStreams(is2, fos2, null);
            throw th;
        }
    }

    private static boolean extractResourceFromJarAndCopy(String path, Context context, String resource) {
        if (path == null || context == null || resource == null) {
            throw new IllegalArgumentException("Invalid arguments to extract resource and copy");
        }
        try {
            String[] values = context.getAssets().list("");
            if (values == null || values.length <= 0) {
                return false;
            }
            if (!Arrays.asList(values).contains("pontiflex_sdk.jar")) {
                return copyResourceFromZipFile(context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).sourceDir, resource, path);
            }
            String packagePath = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).sourceDir;
            new File(context.getFilesDir() + "/" + TEMP_RES_PATH).mkdirs();
            String tempPath = context.getFilesDir() + "/" + TEMP_RES_PATH + "/pontiflex_sdk.jar";
            boolean status = copyResourceFromZipFile(packagePath, "assets/pontiflex_sdk.jar", tempPath);
            if (status) {
                status = copyResourceFromZipFile(tempPath, resource, path);
            }
            File f = new File(tempPath);
            if (f.exists()) {
                f.delete();
            }
            return status;
        } catch (IOException e) {
            Log.e("Pontiflex SDK", "Error while getting asset files", e);
            return false;
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("Pontiflex SDK", "Error while getting package srouce file path", e2);
            return false;
        }
    }

    /* JADX INFO: Multiple debug info for r0v9 java.io.InputStream: [D('is' java.io.InputStream), D('resourceEntry' java.util.zip.ZipEntry)] */
    /* JADX INFO: Multiple debug info for r11v13 'fos'  java.io.FileOutputStream: [D('resourceName' java.lang.String), D('fos' java.io.FileOutputStream)] */
    /* JADX INFO: Multiple debug info for r10v21 byte[]: [D('buf' byte[]), D('zipPath' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r12v13 int: [D('len' int), D('destPath' java.lang.String)] */
    private static boolean copyResourceFromZipFile(String zipPath, String resourceName, String destPath) {
        FileOutputStream fos;
        InputStream is;
        IOException iOException;
        InputStream is2;
        FileOutputStream fos2;
        ZipEntry resourceEntry;
        try {
            ZipFile zipFile = new ZipFile(zipPath);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            String prefix = resourceName.substring(0, resourceName.lastIndexOf("."));
            String suffix = resourceName.substring(resourceName.lastIndexOf(".") + 1);
            while (true) {
                if (!entries.hasMoreElements()) {
                    resourceEntry = null;
                    break;
                }
                ZipEntry entry = (ZipEntry) entries.nextElement();
                if (!entry.isDirectory() && entry.getName().startsWith(prefix) && entry.getName().endsWith(suffix)) {
                    resourceEntry = entry;
                    break;
                }
            }
            if (resourceEntry != null) {
                InputStream is3 = zipFile.getInputStream(resourceEntry);
                try {
                    fos = new FileOutputStream(destPath);
                    try {
                        byte[] buf = new byte[256];
                        while (true) {
                            int len = is3.read(buf);
                            if (len > 0) {
                                fos.write(buf, 0, len);
                            } else {
                                fos.flush();
                                closeStreams(is3, fos, null);
                                return true;
                            }
                        }
                    } catch (IOException e) {
                        e = e;
                        is = is3;
                        try {
                            Log.e("Pontiflex SDK", "Error while copying resource from zip file", e);
                            closeStreams(is, fos, null);
                            return false;
                        } catch (Throwable e2) {
                            IOException iOException2 = e2;
                            fos2 = fos;
                            is2 = is;
                            iOException = iOException2;
                            closeStreams(is2, fos2, null);
                            throw iOException;
                        }
                    } catch (Throwable th) {
                        iOException = th;
                        fos2 = fos;
                        is2 = is3;
                        closeStreams(is2, fos2, null);
                        throw iOException;
                    }
                } catch (IOException e3) {
                    e = e3;
                    fos = null;
                    is = is3;
                    Log.e("Pontiflex SDK", "Error while copying resource from zip file", e);
                    closeStreams(is, fos, null);
                    return false;
                } catch (Throwable th2) {
                    iOException = th2;
                    is2 = is3;
                    fos2 = null;
                    closeStreams(is2, fos2, null);
                    throw iOException;
                }
            } else {
                Log.e("Pontiflex SDK", "Resource: " + resourceName + " was not found inside zip file: " + zipPath);
                closeStreams(null, null, null);
                return false;
            }
        } catch (IOException e4) {
            e = e4;
            fos = null;
            is = null;
            Log.e("Pontiflex SDK", "Error while copying resource from zip file", e);
            closeStreams(is, fos, null);
            return false;
        } catch (Throwable th3) {
            iOException = th3;
            is2 = null;
            fos2 = null;
            closeStreams(is2, fos2, null);
            throw iOException;
        }
    }

    private static boolean checkNonVersionResource(Context context, String resourceKey) {
        try {
            if (new ResourceData(context).getResourceData(resourceKey + lastUpdatedDateSuffix) == null) {
                return false;
            }
            return true;
        } catch (Exception e) {
            Log.e("Pontiflex SDK", "Not able to resource data file not found", e);
            return false;
        }
    }

    /* JADX INFO: Multiple debug info for r0v13 boolean: [D('path' java.lang.String), D('status' boolean)] */
    /* JADX INFO: Multiple debug info for r4v11 'currentVersion'  java.lang.String: [D('currentVersion' java.lang.String), D('currentProp' java.util.Properties)] */
    private static boolean checkVersion(Context context, String resourceKey) {
        if (!VERSIONED_RESOURCES.contains(resourceKey)) {
            return true;
        }
        try {
            String currentVersion = new ResourceData(context).getResourceData(resourceKey + versionCodeSuffix);
            if (currentVersion == null) {
                File currentVersionFile = new File(new File(new File(context.getFilesDir(), RES_PATH), RES_JS_CLIENT), "pflx_version.xml");
                if (!currentVersionFile.exists()) {
                    return false;
                }
                Properties currentProp = new Properties();
                PackageHelper.getInstance(context).parsePontiflexStrings(new FileInputStream(currentVersionFile), currentProp);
                currentVersion = currentProp.getProperty("PFLEX_SDK_VERSION_CODE");
                if (currentVersion == null) {
                    return false;
                }
            }
            File tmpDir = new File(context.getFilesDir(), TEMP_RES_PATH);
            tmpDir.mkdirs();
            if (!extractResourceFromJarAndCopy(context.getFilesDir().getPath() + "/" + TEMP_RES_PATH + "/" + PontiflexSdkVersionBasename + ".xml", context, "pflx_version.xml")) {
                return false;
            }
            File newVersionFile = new File(tmpDir, "pflx_version.xml");
            Properties newProp = new Properties();
            PackageHelper.getInstance(context).parsePontiflexStrings(new FileInputStream(newVersionFile), newProp);
            return !compareVersion(newProp.getProperty("PFLEX_SDK_VERSION_CODE"), currentVersion);
        } catch (FileNotFoundException e) {
            Log.e("Pontiflex SDK", "version file not found", e);
            return false;
        }
    }

    public static boolean compareVersion(String newVersion, String currentVersion) {
        boolean result = false;
        if (newVersion == null || "".equals(newVersion)) {
            return false;
        }
        if (currentVersion == null || "".equals(currentVersion)) {
            return false;
        }
        String[] currentVersionIdentifiers = currentVersion.split("\\.");
        String[] newVersionIdentifiers = newVersion.split("\\.");
        int ii = 0;
        while (true) {
            if (ii >= newVersionIdentifiers.length) {
                break;
            }
            int newInt = Integer.parseInt(newVersionIdentifiers[ii]);
            int currentInt = -1;
            if (currentVersionIdentifiers.length > ii) {
                currentInt = Integer.parseInt(currentVersionIdentifiers[ii]);
            }
            if (newInt > currentInt) {
                result = true;
                break;
            }
            ii++;
        }
        return result;
    }

    private static void closeStreams(InputStream is, FileOutputStream fos, FileWriter fw) {
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
                Log.e("Pontiflex SDK", "Not able to close streams", e);
                return;
            }
        }
        if (fos != null) {
            fos.close();
        }
        if (fw != null) {
            fw.close();
        }
    }
}
