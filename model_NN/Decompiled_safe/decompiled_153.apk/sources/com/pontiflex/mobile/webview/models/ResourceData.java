package com.pontiflex.mobile.webview.models;

import android.content.Context;
import com.pontiflex.mobile.webview.utilities.RegistrationStorage;

public class ResourceData extends RegistrationStorage {
    private static final String defaultFileName = "PflxResourceData";

    public ResourceData(Context context) {
        super(context, defaultFileName);
    }

    public ResourceData(Context context, String resourceDataFileName) {
        super(context, resourceDataFileName);
    }

    public String getResourceData(String key) {
        return get(key);
    }

    public void setResourceData(String key, String value) {
        put(key, value);
    }

    public void removeResourceData(String key) {
        remove(key);
    }
}
