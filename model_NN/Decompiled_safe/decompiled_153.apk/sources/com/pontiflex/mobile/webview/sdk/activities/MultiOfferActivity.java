package com.pontiflex.mobile.webview.sdk.activities;

import android.os.Bundle;
import com.pontiflex.mobile.webview.utilities.HtmlPageRegistry;

public class MultiOfferActivity extends BaseActivity {
    /* access modifiers changed from: protected */
    public void initializeData() {
    }

    /* access modifiers changed from: protected */
    public void setUpViews() {
        setUpWebview();
        this.webView.loadUrl(HtmlPageRegistry.getInstance(getApplicationContext()).getMultiOfferHtmlPath(this, false));
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }
}
