package com.pontiflex.mobile.webview.sdk;

import android.app.Activity;
import java.util.Map;

public interface IAdManager {
    public static final int SHOW_AD_REQUEST = 1;

    public enum RegistrationMode {
        RegistrationAtLaunch,
        RegistrationAfterIntervalInLaunches,
        RegistrationAdHoc
    }

    void clearRegistrationStorage();

    String getRegistrationData(String str);

    int getRegistrationInterval();

    RegistrationMode getRegistrationMode();

    String getSdkVersionCode();

    String getSdkVersionName();

    boolean hasValidRegistrationData();

    boolean isRegistrationRequired();

    boolean isShowingMultiAdView();

    boolean setRegistrationData(String str, String str2);

    boolean setRegistrationData(Map<String, String> map);

    void setRegistrationInterval(int i);

    void setRegistrationMode(RegistrationMode registrationMode);

    void setRegistrationMode(RegistrationMode registrationMode, Activity activity);

    void setRegistrationRequired(boolean z);

    void setShowingMultiAdView(boolean z);

    boolean showAd();

    boolean showAd(int i);

    boolean showAd(int i, Activity activity);

    boolean showAd(int i, boolean z, boolean z2);

    boolean showAd(int i, boolean z, boolean z2, Activity activity);

    boolean showAd(Activity activity);

    boolean showAd(boolean z);

    boolean showAd(boolean z, Activity activity);

    void startMultiOfferActivity();

    void startMultiOfferActivity(Activity activity);

    void startRegistrationActivity();

    void startRegistrationActivity(Activity activity);
}
