package com.pontiflex.mobile.webview.sdk.activities;

import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import com.pontiflex.mobile.webview.sdk.AdManager;
import com.pontiflex.mobile.webview.sdk.IPflexJSInterface;
import com.pontiflex.mobile.webview.utilities.VersionHelper;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: BaseActivity */
class PflexJSInterface implements IPflexJSInterface {
    private BaseActivity baseActivity;
    private WebView innerWebView;

    protected PflexJSInterface(BaseActivity activity, WebView webview) {
        this.baseActivity = activity;
        this.innerWebView = webview;
    }

    public void showKeyboard() {
        if (Integer.parseInt(VersionHelper.getInstance(this.baseActivity.getApplicationContext()).getAndroidOsSdkVersionCode()) >= 8) {
            ((InputMethodManager) this.baseActivity.getSystemService("input_method")).toggleSoftInput(0, 0);
        }
    }

    public void finishActivity() {
        this.baseActivity.setResult(-1);
        this.baseActivity.finish();
    }

    public void logMessage(String message) {
        Log.d("Pontiflex SDK", this.baseActivity.getClass().getName() + " - " + message);
    }

    public boolean isOnline() {
        return this.baseActivity.isOnline();
    }

    public void initializeData() {
        this.baseActivity.initializeData();
        boolean signupRequired = AdManager.getAdManagerInstance(this.baseActivity.getApplication()).isRegistrationRequired();
        this.innerWebView.loadUrl("javascript:PFLEX.AppInfo.eventHandler.fire('onSetConfig',{signUpRequired:" + signupRequired + ", showMultiAd:" + AdManager.getAdManagerInstance(this.baseActivity.getApplication()).isShowingMultiAdView() + "});");
    }

    public String getAppInfoJsonContent() {
        return this.baseActivity.getAppInfoJsonContent();
    }

    public String getLocale() {
        return Locale.getDefault().getLanguage();
    }

    public void cancelProgressDialog() {
        this.baseActivity.cancelProgressDialog();
    }

    public String getBasePath() {
        return this.baseActivity.getBasePath();
    }

    public void asyncHttpRequest(String url, boolean isGet, String headers) {
        Map<String, String> headerMap = new HashMap<>();
        try {
            JSONObject headerJson = new JSONObject(headers);
            Iterator it = headerJson.keys();
            while (it.hasNext()) {
                String key = it.next();
                headerMap.put(key, headerJson.getString(key));
            }
        } catch (JSONException e) {
            Log.e("Pontiflex SDK", "Error reading json object for headers", e);
        }
        this.baseActivity.asyncHttpRequest(url, isGet, headerMap);
    }
}
