package com.pontiflex.mobile.webview.sdk.activities;

import android.webkit.WebView;
import com.pontiflex.mobile.webview.sdk.IPflexJSInterface;

/* compiled from: BaseActivity */
class PflexStorageJSInterface implements IPflexJSInterface {
    private BaseActivity baseActivity;
    private WebView innerWebView;

    protected PflexStorageJSInterface(BaseActivity activity, WebView webview) {
        this.baseActivity = activity;
        this.innerWebView = webview;
    }

    public boolean hasValidRegistrationData() {
        return this.baseActivity.hasValidRegistrationData();
    }

    public void addRegistrationData(String key, String value) {
        this.baseActivity.addRegistrationData(key, value);
    }

    public String getRegistrationData(String key) {
        return this.baseActivity.getRegistrationData(key);
    }

    public void clearRegistrationData() {
        this.baseActivity.clearRegistrationData();
    }

    public void saveState() {
        this.innerWebView.loadUrl("javascript:PFLEX.StorageUtil.saveState()");
    }

    public void clearState() {
        this.innerWebView.loadUrl("javascript:PFLEX.NativeStorage.clearState()");
    }

    public String getItem(String key) {
        return this.baseActivity.getStateData(key);
    }

    public void removeItem(String key) {
        this.baseActivity.clearStateData(key);
    }

    public void setItem(String key, String value) {
        this.baseActivity.saveStateData(key, value);
    }

    public String getAdditionalData(String key) {
        return this.baseActivity.getAdditionalData(key);
    }

    public void setAdditionalData(String key, String value) {
        this.baseActivity.saveAdditionalData(key, value);
    }
}
