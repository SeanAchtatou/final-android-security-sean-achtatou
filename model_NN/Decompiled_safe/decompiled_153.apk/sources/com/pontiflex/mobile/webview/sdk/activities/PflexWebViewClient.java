package com.pontiflex.mobile.webview.sdk.activities;

import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/* compiled from: BaseActivity */
class PflexWebViewClient extends WebViewClient {
    BaseActivity baseActivity = null;

    public PflexWebViewClient(BaseActivity baseActivity2) {
        this.baseActivity = baseActivity2;
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }

    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        Log.d("Pontiflex SDK", "Webview error is " + description);
    }
}
