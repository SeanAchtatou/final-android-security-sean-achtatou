package com.pontiflex.mobile.webview.sdk.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

/* compiled from: BaseActivity */
final class PflexWebChromeClient extends WebChromeClient {
    BaseActivity baseActivity = null;

    public PflexWebChromeClient(BaseActivity baseActivity2) {
        this.baseActivity = baseActivity2;
    }

    public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
        AlertDialog dialog = new AlertDialog.Builder(view.getContext()).setMessage(message).setPositiveButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                result.confirm();
            }
        }).create();
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode != 4) {
                    return false;
                }
                PflexWebChromeClient.this.baseActivity.cancelProgressDialog();
                PflexWebChromeClient.this.baseActivity.cancelAlertDialog();
                return false;
            }
        });
        this.baseActivity.jsResult = result;
        this.baseActivity.jsConfirmAlertDialog = dialog;
        if (this.baseActivity.isFinishing()) {
            return true;
        }
        dialog.show();
        return true;
    }

    public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
        AlertDialog dialog = new AlertDialog.Builder(view.getContext()).setMessage(message).setPositiveButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                result.confirm();
            }
        }).create();
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode != 4) {
                    return false;
                }
                PflexWebChromeClient.this.baseActivity.cancelProgressDialog();
                PflexWebChromeClient.this.baseActivity.cancelAlertDialog();
                return false;
            }
        });
        this.baseActivity.jsResult = result;
        this.baseActivity.jsConfirmAlertDialog = dialog;
        if (this.baseActivity.isFinishing()) {
            return true;
        }
        dialog.show();
        return true;
    }

    public void onProgressChanged(WebView view, int progress) {
        this.baseActivity.setProgress(progress * 100);
    }

    public void onReceivedTitle(WebView view, String title) {
        this.baseActivity.setTitle(title);
        super.onReceivedTitle(view, title);
    }

    public void onConsoleMessage(String message, int lineNumber, String sourceID) {
        Log.d("WebView", message + " -- From line " + lineNumber + " of " + sourceID);
    }
}
