package com.pontiflex.mobile.webview.utilities;

import android.os.AsyncTask;
import android.util.Log;
import com.pontiflex.mobile.webview.sdk.activities.BaseActivity;
import java.util.Map;

public class UpdateResourceTask extends AsyncTask<Object, Integer, Boolean> {
    private BaseActivity baseActivity;

    public UpdateResourceTask(BaseActivity baseActivity2) {
        this.baseActivity = baseActivity2;
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(Object... params) {
        if (params == null || params.length < 8) {
            throw new IllegalArgumentException("Invalid parameters for UpdateResourceTask");
        }
        String url = (String) params[0];
        String expectedCheckSum = (String) params[1];
        String resourcePath = (String) params[2];
        String resourceOldPath = (String) params[3];
        String resourceTempPath = (String) params[4];
        Map<String, String> headers = (Map) params[5];
        boolean extract = ((Boolean) params[6]).booleanValue();
        String resourceKey = (String) params[7];
        boolean success = false;
        try {
            if (!UpdateResourceUtil.copyResource(resourcePath, resourceOldPath)) {
                Log.d(getClass().getName(), "Copying Resource from current to old location failed.");
                return false;
            } else if (!UpdateResourceUtil.downloadResource(url, headers, resourceTempPath)) {
                Log.e(getClass().getName(), "Downloading Resource from server and storing in temp path failed.");
                this.baseActivity.doUpdateResourcesCallBack(resourceKey, success);
                return false;
            } else if (!expectedCheckSum.equals(UpdateResourceUtil.calculateChecksum(resourceTempPath))) {
                Log.e(getClass().getName(), "Downloading Resource checksum doesn't match with expected checksum.");
                this.baseActivity.doUpdateResourcesCallBack(resourceKey, success);
                return false;
            } else if (!UpdateResourceUtil.moveResource(resourceTempPath, resourcePath)) {
                Log.e(getClass().getName(), "Moving Resource temp path to current path failed.");
                this.baseActivity.doUpdateResourcesCallBack(resourceKey, success);
                return false;
            } else {
                if (extract) {
                    if (!UpdateResourceUtil.extractResource(resourcePath)) {
                        Log.e(getClass().getName(), "Extracting Resource to current path failed.");
                        this.baseActivity.doUpdateResourcesCallBack(resourceKey, success);
                        return false;
                    } else if (!UpdateResourceUtil.extractResource(resourceOldPath)) {
                        Log.e(getClass().getName(), "Extracting Resource to old path failed.");
                        this.baseActivity.doUpdateResourcesCallBack(resourceKey, success);
                        return false;
                    }
                }
                success = true;
                this.baseActivity.doUpdateResourcesCallBack(resourceKey, success);
                return Boolean.valueOf(success);
            }
        } catch (Exception e) {
            Log.e("Pontiflex SDK", "ResourceUpdateTask failed: ", e);
        } finally {
            this.baseActivity.doUpdateResourcesCallBack(resourceKey, success);
        }
    }
}
