package com.pontiflex.mobile.webview.sdk.activities;

import android.util.Log;
import com.pontiflex.mobile.webview.sdk.IPflexJSInterface;
import com.pontiflex.mobile.webview.utilities.UpdateResourceTask;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: BaseActivity */
class PflexResourceJSInterface implements IPflexJSInterface {
    private BaseActivity baseActivity;

    protected PflexResourceJSInterface(BaseActivity baseActivity2) {
        this.baseActivity = baseActivity2;
    }

    public String getResourceData(String resourceFileName, String key) {
        return this.baseActivity.getResourceData(resourceFileName, key);
    }

    public void setResourceData(String resourceFileName, String key, String value) {
        this.baseActivity.setResourceData(resourceFileName, key, value);
    }

    public String getResourceContent(String resourcePath, String fromAssets, String resourceKey) {
        return this.baseActivity.getResourceContent(resourcePath, Boolean.valueOf(fromAssets).booleanValue(), resourceKey);
    }

    public void updateResource(String url, String expectedHash, String resourcePath, String resourceOldPath, String resourceTempPath, String headers, String extract, String resourceKey) {
        Map<String, String> headerMap = new HashMap<>();
        try {
            JSONObject jSONObject = new JSONObject(headers);
            Iterator it = jSONObject.keys();
            while (it.hasNext()) {
                String key = it.next();
                headerMap.put(key, jSONObject.getString(key));
            }
        } catch (JSONException e) {
            Log.e("Pontiflex SDK", "Error reading json object for headers", e);
        }
        String basePath = this.baseActivity.getApplicationContext().getFilesDir().getAbsolutePath();
        new UpdateResourceTask(this.baseActivity).execute(url, expectedHash, basePath + "/" + resourcePath, basePath + "/" + resourceOldPath, basePath + "/" + resourceTempPath, headerMap, Boolean.valueOf(extract), resourceKey);
    }

    public void updateResourceCallback(String resourceKey, boolean updateSuccessful) {
        this.baseActivity.webView.loadUrl("javascript:PFLEX.UpdateResource.eventHandler.fire('onUpdateOfResource',{resourceKey:'" + resourceKey + "', updateSuccessful:" + updateSuccessful + "});");
    }
}
