package com.pontiflex.mobile.webview.sdk;

import android.app.Application;
import com.pontiflex.mobile.webview.sdk.AdManager;

public class AdManagerFactory {
    public static IAdManager createInstance(Application application) {
        return AdManager.createInstance(application);
    }

    public static IAdManager createInstance(Application application, AdManager.PflxEnvironment env) {
        return AdManager.createInstance(application, env, false);
    }
}
