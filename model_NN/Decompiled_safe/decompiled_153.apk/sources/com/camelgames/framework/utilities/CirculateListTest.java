package com.camelgames.framework.utilities;

import junit.framework.Assert;
import org.junit.Test;

public class CirculateListTest {
    /* Debug info: failed to restart local var, previous not found, register: 5 */
    @Test
    public void testRemoveAt() {
        CirculateList<Integer> list = new CirculateList<>(10);
        for (int i = 0; i < 8; i++) {
            list.addLast(Integer.valueOf(i));
        }
        list.removeAt(0);
        int count = 8 - 1;
        Assert.assertEquals(list.getCount(), count);
        for (int i2 = 0; i2 < count; i2++) {
            Assert.assertEquals(((Integer) list.indexOf(i2)).intValue(), i2 + 1);
        }
        list.clear();
        for (int i3 = 0; i3 < 8; i3++) {
            list.addLast(Integer.valueOf(i3));
        }
        list.removeAt(8 - 1);
        int count2 = 8 - 1;
        Assert.assertEquals(list.getCount(), count2);
        for (int i4 = 0; i4 < count2; i4++) {
            Assert.assertEquals(((Integer) list.indexOf(i4)).intValue(), i4);
        }
    }
}
