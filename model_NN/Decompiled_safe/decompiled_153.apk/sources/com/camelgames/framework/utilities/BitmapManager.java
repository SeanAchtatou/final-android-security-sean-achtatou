package com.camelgames.framework.utilities;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.WeakHashMap;

public class BitmapManager {
    static final int ImageViewSize = 512;
    static final int MaxPixelCount = 2097152;
    private static final String TAG = "BitmapManager";
    private static BitmapManager sManager = null;
    private final WeakHashMap<Thread, ThreadStatus> mThreadStatus = new WeakHashMap<>();

    private enum State {
        CANCEL,
        ALLOW
    }

    private static class ThreadStatus {
        public BitmapFactory.Options mOptions;
        public State mState;

        private ThreadStatus() {
            this.mState = State.ALLOW;
        }

        /* synthetic */ ThreadStatus(ThreadStatus threadStatus) {
            this();
        }

        public String toString() {
            String s;
            if (this.mState == State.CANCEL) {
                s = "Cancel";
            } else if (this.mState == State.ALLOW) {
                s = "Allow";
            } else {
                s = "?";
            }
            return "thread state = " + s + ", options = " + this.mOptions;
        }
    }

    private BitmapManager() {
    }

    private synchronized ThreadStatus getOrCreateThreadStatus(Thread t) {
        ThreadStatus status;
        status = this.mThreadStatus.get(t);
        if (status == null) {
            status = new ThreadStatus(null);
            this.mThreadStatus.put(t, status);
        }
        return status;
    }

    /* access modifiers changed from: package-private */
    public synchronized void removeDecodingOptions(Thread t) {
        this.mThreadStatus.get(t).mOptions = null;
    }

    public synchronized boolean canThreadDecoding(Thread t) {
        boolean result;
        boolean z;
        ThreadStatus status = this.mThreadStatus.get(t);
        if (status == null) {
            z = true;
        } else {
            if (status.mState != State.CANCEL) {
                result = true;
            } else {
                result = false;
            }
            z = result;
        }
        return z;
    }

    public synchronized void allowThreadDecoding(Thread t) {
        getOrCreateThreadStatus(t).mState = State.ALLOW;
    }

    public synchronized void cancelThreadDecoding(Thread t) {
        ThreadStatus status = getOrCreateThreadStatus(t);
        status.mState = State.CANCEL;
        if (status.mOptions != null) {
            status.mOptions.requestCancelDecode();
        }
        notifyAll();
    }

    public static synchronized BitmapManager instance() {
        BitmapManager bitmapManager;
        synchronized (BitmapManager.class) {
            if (sManager == null) {
                sManager = new BitmapManager();
            }
            bitmapManager = sManager;
        }
        return bitmapManager;
    }

    public static Bitmap transparent(InputStream is, int transparentColor, int size) {
        Bitmap bitmap = BitmapFactory.decodeStream(is);
        try {
            is.close();
            int[] pixels = new int[(bitmap.getWidth() * bitmap.getHeight())];
            bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
            for (int i = 0; i < pixels.length; i++) {
                if (isSameColor(pixels[i], transparentColor)) {
                    pixels[i] = 0;
                }
            }
            return Bitmap.createScaledBitmap(Bitmap.createBitmap(pixels, bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888), size, size, false);
        } catch (IOException e) {
            return null;
        }
    }

    public static boolean isSameColor(int color1, int color2) {
        return (color1 << 24) == (color2 << 24);
    }

    public static File saveBmpAsPng(Bitmap bitmap, String filename) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 75, bytes);
        bitmap.recycle();
        byte[] b = bytes.toByteArray();
        try {
            File myFile = new File(String.valueOf(filename) + ".png");
            if (myFile.exists()) {
                myFile.delete();
                myFile.createNewFile();
            }
            OutputStream filoutputStream = new FileOutputStream(myFile);
            filoutputStream.write(b);
            filoutputStream.flush();
            filoutputStream.close();
            return myFile;
        } catch (IOException e) {
            return null;
        }
    }

    /* JADX INFO: finally extract failed */
    public static Bitmap makeBitmap(Uri uri, ContentResolver resolver) {
        if (resolver == null) {
            return null;
        }
        try {
            ParcelFileDescriptor pfd = resolver.openFileDescriptor(uri, "r");
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                FileDescriptor fd = pfd.getFileDescriptor();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFileDescriptor(fd, null, options);
                if (options.mCancel || options.outWidth == -1 || options.outHeight == -1) {
                    closeSilently(pfd);
                    return null;
                }
                options.inSampleSize = computeSampleSize(options, ImageViewSize, MaxPixelCount);
                options.inJustDecodeBounds = false;
                options.inDither = false;
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Log.v("size", String.valueOf(options.inSampleSize));
                Bitmap decodeFileDescriptor = BitmapFactory.decodeFileDescriptor(fd, null, options);
                closeSilently(pfd);
                return decodeFileDescriptor;
            } catch (OutOfMemoryError e) {
                Log.e(TAG, "Got out of mem exception ", e);
                closeSilently(pfd);
                return null;
            } catch (Throwable th) {
                closeSilently(pfd);
                throw th;
            }
        } catch (FileNotFoundException e2) {
            return null;
        }
    }

    public static int computeSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
        int initialSize = computeInitialSampleSize(options, minSideLength, maxNumOfPixels);
        if (initialSize > 8) {
            return ((initialSize + 7) / 8) * 8;
        }
        int roundedSize = 1;
        while (roundedSize < initialSize) {
            roundedSize <<= 1;
        }
        return roundedSize;
    }

    private static int computeInitialSampleSize(BitmapFactory.Options options, int minSideLength, int maxNumOfPixels) {
        double w = (double) options.outWidth;
        double h = (double) options.outHeight;
        int lowerBound = (int) Math.ceil(Math.sqrt((w * h) / ((double) maxNumOfPixels)));
        int upperBound = (int) Math.min(Math.floor(w / ((double) minSideLength)), Math.floor(h / ((double) minSideLength)));
        if (upperBound < lowerBound) {
            return lowerBound;
        }
        return upperBound;
    }

    public static void closeSilently(ParcelFileDescriptor c) {
        if (c != null) {
            try {
                c.close();
            } catch (Throwable th) {
            }
        }
    }

    public static boolean CheckBmpSize(int PicWidth, int PicHeight) {
        if (PicWidth < 130 || PicHeight < 130) {
            return false;
        }
        return true;
    }
}
