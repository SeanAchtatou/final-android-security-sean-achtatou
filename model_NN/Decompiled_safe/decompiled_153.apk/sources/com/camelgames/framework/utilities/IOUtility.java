package com.camelgames.framework.utilities;

import android.content.res.AssetFileDescriptor;
import com.camelgames.framework.ui.UIUtility;
import com.camelgames.ndk.JNILibrary;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class IOUtility {
    private static AssetFileDescriptor[] afds;

    public static IntBuffer createIntBuffer(int size) {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(size * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        return byteBuffer.asIntBuffer();
    }

    public static IntBuffer createIntBuffer(int[] intData) {
        IntBuffer intBuffer = createIntBuffer(intData.length);
        intBuffer.put(intData);
        intBuffer.position(0);
        return intBuffer;
    }

    public static FloatBuffer createFloatBuffer(int size) {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(size * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        return byteBuffer.asFloatBuffer();
    }

    public static FloatBuffer createFloatBuffer(float[] floatData) {
        FloatBuffer floatBuffer = createFloatBuffer(floatData.length);
        floatBuffer.put(floatData);
        floatBuffer.position(0);
        return floatBuffer;
    }

    public static ShortBuffer createShortBuffer(int size) {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(size * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        return byteBuffer.asShortBuffer();
    }

    public static ShortBuffer createShortBuffer(short[] shortData) {
        ShortBuffer shortBuffer = createShortBuffer(shortData.length);
        shortBuffer.put(shortData);
        shortBuffer.position(0);
        return shortBuffer;
    }

    public static ByteBuffer createByteBuffer(int size) {
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(size);
        byteBuffer.order(ByteOrder.nativeOrder());
        return byteBuffer;
    }

    public static ByteBuffer createByteBuffer(byte[] byteData) {
        ByteBuffer byteBuffer = createByteBuffer(byteData.length);
        byteBuffer.put(byteData);
        byteBuffer.position(0);
        return byteBuffer;
    }

    public static Integer getResourceIdByName(Class resourceClass, String resourceName) {
        Field[] fields = resourceClass.getFields();
        int length = fields.length;
        int i = 0;
        while (i < length) {
            Field field = fields[i];
            try {
                if (field.getName().equals(resourceName)) {
                    return Integer.valueOf(field.getInt(resourceClass));
                }
                i++;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (SecurityException e2) {
                e2.printStackTrace();
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
            }
        }
        return -1;
    }

    public static String getResourceNameById(Class resourceClass, int resourceId) {
        Field[] fields = resourceClass.getFields();
        int length = fields.length;
        int i = 0;
        while (i < length) {
            Field field = fields[i];
            try {
                if (field.getInt(resourceClass) == resourceId) {
                    return field.getName();
                }
                i++;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (SecurityException e2) {
                e2.printStackTrace();
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
            }
        }
        return null;
    }

    public static int[] getResourceIdsByPrefix(Class resourceClass, String prefix) {
        return getResourceIdsByPrefix(resourceClass, prefix, null);
    }

    public static int[] getResourceIdsByPrefix(Class resourceClass, String prefix, Comparator<String> comparator) {
        Field[] fields = resourceClass.getFields();
        ArrayList<String> names = new ArrayList<>();
        for (int i = 0; i < fields.length; i++) {
            if (fields[i].getName().indexOf(prefix) == 0) {
                names.add(fields[i].getName());
            }
        }
        String[] resourceNames = new String[names.size()];
        names.toArray(resourceNames);
        if (comparator != null) {
            Arrays.sort(resourceNames, comparator);
        }
        int[] ids = new int[resourceNames.length];
        for (int i2 = 0; i2 < resourceNames.length; i2++) {
            try {
                ids[i2] = resourceClass.getDeclaredField(resourceNames[i2]).getInt(resourceClass);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (SecurityException e2) {
                e2.printStackTrace();
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
            } catch (NoSuchFieldException e4) {
                e4.printStackTrace();
            }
        }
        return ids;
    }

    public static void passNativeFileInfo(int[] resIds) {
        int i;
        afds = new AssetFileDescriptor[resIds.length];
        int length = resIds.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            int resId = resIds[i2];
            AssetFileDescriptor afd = UIUtility.getMainAcitvity().getApplicationContext().getResources().openRawResourceFd(resId);
            if (afd != null) {
                i = i3 + 1;
                afds[i3] = afd;
                JNILibrary.passNativeFileInfo(resId, afd.getFileDescriptor(), (int) afd.getStartOffset(), (int) afd.getLength());
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
        }
    }
}
