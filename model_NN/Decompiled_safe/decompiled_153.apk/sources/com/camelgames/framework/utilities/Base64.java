package com.camelgames.framework.utilities;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

public class Base64 {
    private static char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".toCharArray();
    private static byte[] codes = new byte[256];

    public static char[] encode(byte[] data) {
        int i;
        char[] out = new char[(((data.length + 2) / 3) * 4)];
        int i2 = 0;
        int index = 0;
        while (i2 < data.length) {
            boolean quad = false;
            boolean trip = false;
            int val = (data[i2] & 255) << 8;
            if (i2 + 1 < data.length) {
                val |= data[i2 + 1] & 255;
                trip = true;
            }
            int val2 = val << 8;
            if (i2 + 2 < data.length) {
                val2 |= data[i2 + 2] & 255;
                quad = true;
            }
            out[index + 3] = alphabet[quad ? val2 & 63 : 64];
            int val3 = val2 >> 6;
            int i3 = index + 2;
            char[] cArr = alphabet;
            if (trip) {
                i = val3 & 63;
            } else {
                i = 64;
            }
            out[i3] = cArr[i];
            int val4 = val3 >> 6;
            out[index + 1] = alphabet[val4 & 63];
            out[index] = alphabet[(val4 >> 6) & 63];
            i2 += 3;
            index += 4;
        }
        return out;
    }

    public static byte[] decode(char[] data) {
        int value;
        int tempLen = data.length;
        for (int ix = 0; ix < data.length; ix++) {
            if (data[ix] > 255 || codes[data[ix]] < 0) {
                tempLen--;
            }
        }
        int len = (tempLen / 4) * 3;
        if (tempLen % 4 == 3) {
            len += 2;
        }
        if (tempLen % 4 == 2) {
            len++;
        }
        byte[] out = new byte[len];
        int shift = 0;
        int accum = 0;
        int index = 0;
        for (int ix2 = 0; ix2 < data.length; ix2++) {
            if (data[ix2] > 255) {
                value = -1;
            } else {
                value = codes[data[ix2]];
            }
            if (value >= 0) {
                shift += 6;
                accum = (accum << 6) | value;
                if (shift >= 8) {
                    shift -= 8;
                    out[index] = (byte) ((accum >> shift) & 255);
                    index++;
                }
            }
        }
        if (index == out.length) {
            return out;
        }
        throw new Error("Miscalculated data length (wrote " + index + " instead of " + out.length + ")");
    }

    static {
        for (int i = 0; i < 256; i++) {
            codes[i] = -1;
        }
        for (int i2 = 65; i2 <= 90; i2++) {
            codes[i2] = (byte) (i2 - 65);
        }
        for (int i3 = 97; i3 <= 122; i3++) {
            codes[i3] = (byte) ((i3 + 26) - 97);
        }
        for (int i4 = 48; i4 <= 57; i4++) {
            codes[i4] = (byte) ((i4 + 52) - 48);
        }
        codes[43] = 62;
        codes[47] = 63;
    }

    public static void main(String[] args) {
        boolean decode = false;
        if (args.length == 0) {
            System.out.println("usage:  java Base64 [-d[ecode]] filename");
            System.exit(0);
        }
        for (int i = 0; i < args.length; i++) {
            if ("-decode".equalsIgnoreCase(args[i])) {
                decode = true;
            } else if ("-d".equalsIgnoreCase(args[i])) {
                decode = true;
            }
        }
        String filename = args[args.length - 1];
        File file = new File(filename);
        if (!file.exists()) {
            System.out.println("Error:  file '" + filename + "' doesn't exist!");
            System.exit(0);
        }
        if (decode) {
            writeBytes(file, decode(readChars(file)));
        } else {
            writeChars(file, encode(readBytes(file)));
        }
    }

    private static byte[] readBytes(File file) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            InputStream is = new BufferedInputStream(new FileInputStream(file));
            byte[] buf = new byte[16384];
            while (true) {
                int count = is.read(buf);
                if (count == -1) {
                    break;
                } else if (count > 0) {
                    baos.write(buf, 0, count);
                }
            }
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return baos.toByteArray();
    }

    private static char[] readChars(File file) {
        CharArrayWriter caw = new CharArrayWriter();
        try {
            Reader in = new BufferedReader(new FileReader(file));
            char[] buf = new char[16384];
            while (true) {
                int count = in.read(buf);
                if (count == -1) {
                    break;
                } else if (count > 0) {
                    caw.write(buf, 0, count);
                }
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return caw.toCharArray();
    }

    private static void writeBytes(File file, byte[] data) {
        try {
            OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
            os.write(data);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void writeChars(File file, char[] data) {
        try {
            Writer os = new BufferedWriter(new FileWriter(file));
            os.write(data);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
