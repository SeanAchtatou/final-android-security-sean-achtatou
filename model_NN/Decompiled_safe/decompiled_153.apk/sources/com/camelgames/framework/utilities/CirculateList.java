package com.camelgames.framework.utilities;

public class CirculateList<T> {
    private int capacity = 1024;
    private int first = (this.capacity / 2);
    private Object[] items;
    private int iterator = -1;
    private int last = this.first;

    public CirculateList(int capacity2) {
        this.capacity = capacity2;
        int i = capacity2 / 2;
        this.first = i;
        this.last = i;
        this.items = new Object[capacity2];
    }

    public boolean isEmpty() {
        return this.first == this.last;
    }

    public int getCount() {
        int count = this.last - this.first;
        if (count < 0) {
            return count + this.capacity;
        }
        return count;
    }

    public T indexOf(int index) {
        int actualIndex = this.first + index;
        return this.items[actualIndex >= this.capacity ? actualIndex - this.capacity : actualIndex];
    }

    public T getFirst() {
        return this.items[this.first];
    }

    public T getLast() {
        return this.items[this.last == 0 ? this.capacity - 1 : this.last - 1];
    }

    public void addFirst(T item) {
        this.first--;
        if (this.first < 0) {
            this.first = this.capacity - 1;
        }
        if (this.first == this.last) {
            throw new IllegalStateException();
        }
        this.items[this.first] = item;
    }

    public void addLast(T item) {
        this.items[this.last] = item;
        this.last++;
        if (this.last >= this.capacity) {
            this.last = 0;
        }
        if (this.first == this.last) {
            throw new IllegalStateException();
        }
    }

    public void removeFirst() {
        if (this.first != this.last) {
            this.items[this.first] = null;
            this.first++;
            if (this.first >= this.capacity) {
                this.first = 0;
            }
        }
    }

    public void removeLast() {
        if (this.first != this.last) {
            this.last--;
            if (this.last < 0) {
                this.last = this.capacity - 1;
            }
            this.items[this.last] = null;
        }
    }

    public void removeAt(int index) {
        int actualIndex = this.first + index;
        if (this.first < this.last && this.first <= actualIndex && actualIndex < this.last) {
            for (int i = actualIndex; i < this.last; i++) {
                this.items[i] = this.items[i + 1];
            }
            removeLast();
        } else if (this.first <= this.last) {
        } else {
            if (this.last > actualIndex || actualIndex >= this.first) {
                for (int i2 = actualIndex; i2 < this.capacity + this.last; i2++) {
                    this.items[i2 % this.capacity] = this.items[(i2 + 1) % this.capacity];
                }
                removeLast();
            }
        }
    }

    public void clear() {
        if (this.first < this.last) {
            for (int i = this.first; i < this.last; i++) {
                this.items[i] = null;
            }
        } else if (this.first > this.last) {
            for (int i2 = this.first; i2 < this.capacity; i2++) {
                this.items[i2] = null;
            }
            for (int i3 = 0; i3 < this.last; i3++) {
                this.items[i3] = null;
            }
        }
        int i4 = this.capacity / 2;
        this.last = i4;
        this.first = i4;
        this.iterator = -1;
    }

    public boolean pepareIterate() {
        this.iterator = this.first;
        return this.first != this.last;
    }

    public T next() {
        T[] tArr = this.items;
        int i = this.iterator;
        this.iterator = i + 1;
        T item = tArr[i];
        if (this.iterator == this.capacity) {
            this.iterator = 0;
        }
        return item;
    }
}
