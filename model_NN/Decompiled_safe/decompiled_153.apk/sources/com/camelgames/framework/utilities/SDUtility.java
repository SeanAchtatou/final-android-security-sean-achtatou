package com.camelgames.framework.utilities;

import android.os.Environment;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class SDUtility {
    private static String Game_Folder_Name = null;
    private static String Game_Image_Folder_Name = null;
    public static final String Root_Folder_Name = "/sdcard/.camelgames/";
    public static final byte code = 123;

    public static boolean getFiles(String folderName, ArrayList<String> names, ArrayList<File> files) {
        boolean getFile = false;
        File folder = new File(folderName);
        if (folder.exists()) {
            for (File file : folder.listFiles()) {
                files.add(file);
                names.add(cutShortName(file.getName()));
                getFile = true;
            }
        }
        return getFile;
    }

    public static ArrayList<File> getFilesInsideFolder(String folderName) {
        ArrayList<File> files = new ArrayList<>();
        File folder = new File(folderName);
        if (folder.exists()) {
            for (File file : folder.listFiles()) {
                if (file.isFile()) {
                    files.add(file);
                }
            }
        }
        return files;
    }

    public static void removeFile(String folderName, int index) {
        File[] files;
        File folder = new File(folderName);
        if (folder.exists() && (files = folder.listFiles()) != null && index < files.length) {
            files[index].delete();
        }
    }

    public static void removeFiles(String folderName, String name) {
        File folder = new File(folderName);
        if (folder.exists()) {
            for (File file : folder.listFiles()) {
                if (cutShortName(file.getName()).equals(name)) {
                    file.delete();
                }
            }
        }
    }

    public static boolean isSDPresent() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static String cutShortName(String fileName) {
        int start = fileName.lastIndexOf(47) + 1;
        int end = fileName.lastIndexOf(46);
        if (end >= 0) {
            return fileName.substring(start, end);
        }
        return fileName.substring(start);
    }

    public static <T> T loadObject(String fileName, boolean needEncrypt) {
        File file = openFile(fileName);
        if (file != null) {
            return loadObject(file, needEncrypt);
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0029 A[SYNTHETIC, Splitter:B:18:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x002e A[Catch:{ IOException -> 0x005d }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0036 A[SYNTHETIC, Splitter:B:25:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x003b A[Catch:{ IOException -> 0x003f }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0044 A[SYNTHETIC, Splitter:B:32:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0049 A[Catch:{ IOException -> 0x004d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T> T loadObject(java.io.File r7, boolean r8) {
        /*
            r6 = 0
            byte[] r0 = load(r7, r8)
            if (r0 != 0) goto L_0x0009
            r5 = r6
        L_0x0008:
            return r5
        L_0x0009:
            r1 = 0
            r3 = 0
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream     // Catch:{ ClassNotFoundException -> 0x0026, IOException -> 0x0033, all -> 0x0041 }
            r2.<init>(r0)     // Catch:{ ClassNotFoundException -> 0x0026, IOException -> 0x0033, all -> 0x0041 }
            java.io.ObjectInputStream r4 = new java.io.ObjectInputStream     // Catch:{ ClassNotFoundException -> 0x005f, IOException -> 0x0056, all -> 0x004f }
            r4.<init>(r2)     // Catch:{ ClassNotFoundException -> 0x005f, IOException -> 0x0056, all -> 0x004f }
            java.lang.Object r5 = r4.readObject()     // Catch:{ ClassNotFoundException -> 0x0062, IOException -> 0x0059, all -> 0x0052 }
            if (r2 == 0) goto L_0x001e
            r2.close()     // Catch:{ IOException -> 0x0024 }
        L_0x001e:
            if (r4 == 0) goto L_0x0008
            r4.close()     // Catch:{ IOException -> 0x0024 }
            goto L_0x0008
        L_0x0024:
            r6 = move-exception
            goto L_0x0008
        L_0x0026:
            r5 = move-exception
        L_0x0027:
            if (r1 == 0) goto L_0x002c
            r1.close()     // Catch:{ IOException -> 0x005d }
        L_0x002c:
            if (r3 == 0) goto L_0x0031
            r3.close()     // Catch:{ IOException -> 0x005d }
        L_0x0031:
            r5 = r6
            goto L_0x0008
        L_0x0033:
            r5 = move-exception
        L_0x0034:
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ IOException -> 0x003f }
        L_0x0039:
            if (r3 == 0) goto L_0x0031
            r3.close()     // Catch:{ IOException -> 0x003f }
            goto L_0x0031
        L_0x003f:
            r5 = move-exception
            goto L_0x0031
        L_0x0041:
            r5 = move-exception
        L_0x0042:
            if (r1 == 0) goto L_0x0047
            r1.close()     // Catch:{ IOException -> 0x004d }
        L_0x0047:
            if (r3 == 0) goto L_0x004c
            r3.close()     // Catch:{ IOException -> 0x004d }
        L_0x004c:
            throw r5
        L_0x004d:
            r6 = move-exception
            goto L_0x004c
        L_0x004f:
            r5 = move-exception
            r1 = r2
            goto L_0x0042
        L_0x0052:
            r5 = move-exception
            r3 = r4
            r1 = r2
            goto L_0x0042
        L_0x0056:
            r5 = move-exception
            r1 = r2
            goto L_0x0034
        L_0x0059:
            r5 = move-exception
            r3 = r4
            r1 = r2
            goto L_0x0034
        L_0x005d:
            r5 = move-exception
            goto L_0x0031
        L_0x005f:
            r5 = move-exception
            r1 = r2
            goto L_0x0027
        L_0x0062:
            r5 = move-exception
            r3 = r4
            r1 = r2
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.camelgames.framework.utilities.SDUtility.loadObject(java.io.File, boolean):java.lang.Object");
    }

    public static byte[] load(String fileName, boolean needEncrypt) {
        File file = openFile(fileName);
        if (file != null) {
            return load(file, needEncrypt);
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x002a A[SYNTHETIC, Splitter:B:18:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0032 A[SYNTHETIC, Splitter:B:23:0x0032] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] load(java.io.File r7, boolean r8) {
        /*
            r1 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0027, all -> 0x002f }
            r2.<init>(r7)     // Catch:{ IOException -> 0x0027, all -> 0x002f }
            int r4 = r2.available()     // Catch:{ IOException -> 0x003f, all -> 0x003c }
            byte[] r0 = new byte[r4]     // Catch:{ IOException -> 0x003f, all -> 0x003c }
            r2.read(r0)     // Catch:{ IOException -> 0x003f, all -> 0x003c }
            if (r8 == 0) goto L_0x0015
            r3 = 0
        L_0x0012:
            int r5 = r0.length     // Catch:{ IOException -> 0x003f, all -> 0x003c }
            if (r3 < r5) goto L_0x001d
        L_0x0015:
            if (r2 == 0) goto L_0x001a
            r2.close()     // Catch:{ IOException -> 0x0036 }
        L_0x001a:
            r1 = r2
            r5 = r0
        L_0x001c:
            return r5
        L_0x001d:
            byte r5 = r0[r3]     // Catch:{ IOException -> 0x003f, all -> 0x003c }
            r5 = r5 ^ 123(0x7b, float:1.72E-43)
            byte r5 = (byte) r5     // Catch:{ IOException -> 0x003f, all -> 0x003c }
            r0[r3] = r5     // Catch:{ IOException -> 0x003f, all -> 0x003c }
            int r3 = r3 + 1
            goto L_0x0012
        L_0x0027:
            r5 = move-exception
        L_0x0028:
            if (r1 == 0) goto L_0x002d
            r1.close()     // Catch:{ IOException -> 0x0038 }
        L_0x002d:
            r5 = 0
            goto L_0x001c
        L_0x002f:
            r5 = move-exception
        L_0x0030:
            if (r1 == 0) goto L_0x0035
            r1.close()     // Catch:{ IOException -> 0x003a }
        L_0x0035:
            throw r5
        L_0x0036:
            r5 = move-exception
            goto L_0x001a
        L_0x0038:
            r5 = move-exception
            goto L_0x002d
        L_0x003a:
            r6 = move-exception
            goto L_0x0035
        L_0x003c:
            r5 = move-exception
            r1 = r2
            goto L_0x0030
        L_0x003f:
            r5 = move-exception
            r1 = r2
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.camelgames.framework.utilities.SDUtility.load(java.io.File, boolean):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0026 A[SYNTHETIC, Splitter:B:15:0x0026] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002b A[Catch:{ IOException -> 0x002f }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0034 A[SYNTHETIC, Splitter:B:22:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0039 A[Catch:{ IOException -> 0x0041 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void saveObject(java.lang.String r7, java.io.Serializable r8, boolean r9) {
        /*
            r3 = 0
            r0 = 0
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0023, all -> 0x0031 }
            r1.<init>()     // Catch:{ IOException -> 0x0023, all -> 0x0031 }
            java.io.ObjectOutputStream r4 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x004a, all -> 0x0043 }
            r4.<init>(r1)     // Catch:{ IOException -> 0x004a, all -> 0x0043 }
            r4.writeObject(r8)     // Catch:{ IOException -> 0x004d, all -> 0x0046 }
            byte[] r2 = r1.toByteArray()     // Catch:{ IOException -> 0x004d, all -> 0x0046 }
            save(r7, r2, r9)     // Catch:{ IOException -> 0x004d, all -> 0x0046 }
            if (r1 == 0) goto L_0x001b
            r1.close()     // Catch:{ IOException -> 0x003d }
        L_0x001b:
            if (r4 == 0) goto L_0x0051
            r4.close()     // Catch:{ IOException -> 0x003d }
            r0 = r1
            r3 = r4
        L_0x0022:
            return
        L_0x0023:
            r5 = move-exception
        L_0x0024:
            if (r0 == 0) goto L_0x0029
            r0.close()     // Catch:{ IOException -> 0x002f }
        L_0x0029:
            if (r3 == 0) goto L_0x0022
            r3.close()     // Catch:{ IOException -> 0x002f }
            goto L_0x0022
        L_0x002f:
            r5 = move-exception
            goto L_0x0022
        L_0x0031:
            r5 = move-exception
        L_0x0032:
            if (r0 == 0) goto L_0x0037
            r0.close()     // Catch:{ IOException -> 0x0041 }
        L_0x0037:
            if (r3 == 0) goto L_0x003c
            r3.close()     // Catch:{ IOException -> 0x0041 }
        L_0x003c:
            throw r5
        L_0x003d:
            r5 = move-exception
            r0 = r1
            r3 = r4
            goto L_0x0022
        L_0x0041:
            r6 = move-exception
            goto L_0x003c
        L_0x0043:
            r5 = move-exception
            r0 = r1
            goto L_0x0032
        L_0x0046:
            r5 = move-exception
            r0 = r1
            r3 = r4
            goto L_0x0032
        L_0x004a:
            r5 = move-exception
            r0 = r1
            goto L_0x0024
        L_0x004d:
            r5 = move-exception
            r0 = r1
            r3 = r4
            goto L_0x0024
        L_0x0051:
            r0 = r1
            r3 = r4
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.camelgames.framework.utilities.SDUtility.saveObject(java.lang.String, java.io.Serializable, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0028 A[SYNTHETIC, Splitter:B:19:0x0028] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0031 A[SYNTHETIC, Splitter:B:24:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void save(java.lang.String r6, byte[] r7, boolean r8) {
        /*
            java.io.File r3 = createFile(r6)
            if (r3 == 0) goto L_0x001a
            r0 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0025, all -> 0x002e }
            r1.<init>(r3)     // Catch:{ IOException -> 0x0025, all -> 0x002e }
            if (r8 == 0) goto L_0x0012
            r2 = 0
        L_0x000f:
            int r4 = r7.length     // Catch:{ IOException -> 0x003c, all -> 0x0039 }
            if (r2 < r4) goto L_0x001b
        L_0x0012:
            r1.write(r7)     // Catch:{ IOException -> 0x003c, all -> 0x0039 }
            if (r1 == 0) goto L_0x001a
            r1.close()     // Catch:{ IOException -> 0x0037 }
        L_0x001a:
            return
        L_0x001b:
            byte r4 = r7[r2]     // Catch:{ IOException -> 0x003c, all -> 0x0039 }
            r4 = r4 ^ 123(0x7b, float:1.72E-43)
            byte r4 = (byte) r4     // Catch:{ IOException -> 0x003c, all -> 0x0039 }
            r7[r2] = r4     // Catch:{ IOException -> 0x003c, all -> 0x0039 }
            int r2 = r2 + 1
            goto L_0x000f
        L_0x0025:
            r4 = move-exception
        L_0x0026:
            if (r0 == 0) goto L_0x001a
            r0.close()     // Catch:{ IOException -> 0x002c }
            goto L_0x001a
        L_0x002c:
            r4 = move-exception
            goto L_0x001a
        L_0x002e:
            r4 = move-exception
        L_0x002f:
            if (r0 == 0) goto L_0x0034
            r0.close()     // Catch:{ IOException -> 0x0035 }
        L_0x0034:
            throw r4
        L_0x0035:
            r5 = move-exception
            goto L_0x0034
        L_0x0037:
            r4 = move-exception
            goto L_0x001a
        L_0x0039:
            r4 = move-exception
            r0 = r1
            goto L_0x002f
        L_0x003c:
            r4 = move-exception
            r0 = r1
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.camelgames.framework.utilities.SDUtility.save(java.lang.String, byte[], boolean):void");
    }

    public static File openFile(String fileStr) {
        if (!isSDPresent()) {
            return null;
        }
        File file = new File(fileStr);
        if (file.exists()) {
            return file;
        }
        return null;
    }

    public static File createFile(String fileStr) {
        if (!isSDPresent()) {
            return null;
        }
        File folderFile = new File(fileStr.substring(0, fileStr.lastIndexOf(47) + 1));
        if (!folderFile.exists()) {
            folderFile.mkdirs();
        }
        File file = new File(fileStr);
        try {
            file.createNewFile();
            return file;
        } catch (IOException e) {
            return null;
        }
    }

    public static boolean hasFile(String fileName) {
        if (!isSDPresent()) {
            return false;
        }
        return new File(fileName).exists();
    }

    public static void setGameName(String gameName) {
        Game_Folder_Name = "/sdcard/.camelgames/." + gameName + '/';
        Game_Image_Folder_Name = String.valueOf(Game_Folder_Name) + "images" + '/';
    }

    public static String getGameFolder() {
        return Game_Folder_Name;
    }

    public static String getImageFolder() {
        return Game_Image_Folder_Name;
    }
}
