package com.camelgames.framework.scenes;

public interface Scene {
    void finish();

    void start();
}
