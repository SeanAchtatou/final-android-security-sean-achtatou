package com.camelgames.framework.scenes;

import com.camelgames.framework.entities.EntityManager;
import com.camelgames.framework.events.EventManager;

public class SceneManager {
    public static final SceneManager instance = new SceneManager();
    private Scene currentScene;

    private SceneManager() {
    }

    public void changeScene(Scene scene) {
        if (this.currentScene != null) {
            this.currentScene.finish();
        }
        clean();
        this.currentScene = scene;
        if (this.currentScene != null) {
            this.currentScene.start();
        }
    }

    public void clean() {
        EntityManager.getInstance().clearTemporary();
        EventManager.getInstance().cancelAllEvents();
    }

    public Scene getCurrentScene() {
        return this.currentScene;
    }
}
