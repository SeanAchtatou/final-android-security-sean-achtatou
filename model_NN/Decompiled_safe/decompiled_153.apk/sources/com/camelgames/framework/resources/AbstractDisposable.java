package com.camelgames.framework.resources;

public abstract class AbstractDisposable implements Disposable {
    protected boolean isDisposed;

    /* access modifiers changed from: protected */
    public abstract void disposeInternal();

    /* access modifiers changed from: protected */
    public void finalize() {
        dispose();
    }

    public void dispose() {
        if (!this.isDisposed) {
            this.isDisposed = true;
            disposeInternal();
        }
    }

    public boolean isDisposed() {
        return this.isDisposed;
    }
}
