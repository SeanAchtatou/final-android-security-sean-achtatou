package com.camelgames.framework.resources;

public interface Disposable {
    void dispose();
}
