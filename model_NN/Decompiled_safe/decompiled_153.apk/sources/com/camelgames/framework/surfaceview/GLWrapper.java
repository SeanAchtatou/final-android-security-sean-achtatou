package com.camelgames.framework.surfaceview;

import javax.microedition.khronos.opengles.GL;

public interface GLWrapper {
    GL wrap(GL gl);
}
