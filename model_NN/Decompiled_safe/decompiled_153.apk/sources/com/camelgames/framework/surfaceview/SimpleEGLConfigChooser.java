package com.camelgames.framework.surfaceview;

class SimpleEGLConfigChooser extends ComponentSizeChooser {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SimpleEGLConfigChooser(boolean pWithDepthBuffer) {
        super(4, 4, 4, 0, pWithDepthBuffer ? 16 : 0, 0);
        this.mRedSize = 5;
        this.mGreenSize = 6;
        this.mBlueSize = 5;
    }
}
