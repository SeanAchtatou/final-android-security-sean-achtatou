package com.camelgames.framework.events;

public enum EventType {
    GraphicsCreated,
    GraphicsDestroyed,
    GraphicsResized,
    StaticEdgesCreated,
    StaticEdgesDestroyed,
    Collide,
    SensorContact,
    BallCollide,
    SingleTap,
    DoubleTap,
    Catched,
    LevelUp,
    LevelFinished,
    LevelFailed,
    LevelDown,
    LoadLevel,
    ReadyToLoadLevel,
    GotScore,
    MainMenu,
    Restart,
    Replay,
    NewGame,
    ContinueGame,
    SelectLevel,
    Customise,
    GameBegin,
    Tutorial,
    HighScore,
    UICommand,
    Button,
    SoundOn,
    SoundOff,
    Tick,
    Captured,
    Triggered,
    Fall,
    LineCreated,
    LineDestroyed,
    CarCreated,
    AttachedToJoint,
    EraseAll,
    RagdollDied,
    PapaStackLoadLevel,
    StartToRecord,
    BrickChanged,
    Favourite,
    LevelEditorCopyBrick,
    LevelEditorDeleteBrick,
    LevelEditorNewBrick,
    LevelEditorEditQueue,
    LevelEditorEditGround,
    LevelEditorStart,
    LevelEditorExit,
    LevelEditorSave,
    LevelEditorTest,
    UploadLevel,
    ReadyToLand,
    Landed,
    Crash,
    Warning,
    Arrive,
    JointCreated,
    Shoot,
    StartExplode,
    AttachRagdoll,
    AttachStick,
    Bomb,
    BombLarge,
    PressSwitch,
    Purchased,
    Refunded
}
