package com.camelgames.framework.events;

public class EntityEvent extends AbstractEvent {
    private final int entityId;

    public EntityEvent(EventType type, int id) {
        super(type);
        this.entityId = id;
    }

    public int getEntityId() {
        return this.entityId;
    }
}
