package com.camelgames.framework.events;

public class TapEvent extends AbstractEvent {
    private float x;
    private float y;

    public TapEvent(EventType type, float x2, float y2) {
        super(type);
        this.x = x2;
        this.y = y2;
    }

    public void set(EventType type, float x2, float y2) {
        setType(type);
        this.x = x2;
        this.y = y2;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }
}
