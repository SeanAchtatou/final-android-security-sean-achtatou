package com.camelgames.framework.events;

import com.box2d.b2SensorContactWrapper;

public class SensorContactEvent extends AbstractEvent {
    public static final int NON_EXIST = -100;
    private b2SensorContactWrapper contact;

    public SensorContactEvent(b2SensorContactWrapper contact2) {
        super(EventType.SensorContact);
        this.contact = contact2;
    }

    public void setContact(b2SensorContactWrapper contact2) {
        this.contact = contact2;
    }

    public b2SensorContactWrapper getContact() {
        return this.contact;
    }

    public int tryGetOtherId(int bodyId) {
        int bodyId1 = this.contact.getBodyId1();
        int bodyId2 = this.contact.getBodyId2();
        if (bodyId1 == bodyId) {
            return bodyId2;
        }
        if (bodyId2 == bodyId) {
            return bodyId1;
        }
        return -100;
    }
}
