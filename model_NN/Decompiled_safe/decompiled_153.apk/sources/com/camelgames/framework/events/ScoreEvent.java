package com.camelgames.framework.events;

public class ScoreEvent extends EntityEvent {
    private final int score;

    public ScoreEvent(int score2, int id) {
        super(EventType.GotScore, id);
        this.score = score2;
    }

    public int getScore() {
        return this.score;
    }
}
