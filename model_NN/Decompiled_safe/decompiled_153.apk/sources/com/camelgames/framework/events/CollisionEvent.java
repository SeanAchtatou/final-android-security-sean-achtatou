package com.camelgames.framework.events;

import com.box2d.b2ContactWrapper;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.physics.PhysicsManager;

public class CollisionEvent extends AbstractEvent {
    public static final int NON_EXIST = -100;
    private b2ContactWrapper contact;

    public CollisionEvent(b2ContactWrapper contact2) {
        super(EventType.Collide);
        this.contact = contact2;
    }

    public void setContact(b2ContactWrapper contact2) {
        this.contact = contact2;
    }

    public b2ContactWrapper getContact() {
        return this.contact;
    }

    public int tryGetOtherId(int bodyId) {
        int bodyId1 = this.contact.getBodyId1();
        int bodyId2 = this.contact.getBodyId2();
        if (bodyId1 == bodyId) {
            return bodyId2;
        }
        if (bodyId2 == bodyId) {
            return bodyId1;
        }
        return -100;
    }

    public void getCollisionPos(Vector2 pos) {
        pos.X = PhysicsManager.physicsToScreen(this.contact.getTouchX());
        pos.Y = PhysicsManager.physicsToScreen(this.contact.getTouchY());
    }
}
