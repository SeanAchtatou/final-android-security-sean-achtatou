package com.camelgames.framework.events;

public class PhysicsBodyEvent extends AbstractEvent {
    private final int entityId;

    public PhysicsBodyEvent(EventType type, int id) {
        super(type);
        this.entityId = id;
    }

    public int getEntityId() {
        return this.entityId;
    }
}
