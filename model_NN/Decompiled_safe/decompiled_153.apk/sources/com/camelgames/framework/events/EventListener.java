package com.camelgames.framework.events;

public interface EventListener {
    void HandleEvent(AbstractEvent abstractEvent);
}
