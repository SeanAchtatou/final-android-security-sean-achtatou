package com.camelgames.framework.events;

public class LoadLevelEvent extends AbstractEvent {
    private final int levelIndex;

    public LoadLevelEvent(int levelIndex2) {
        super(EventType.LoadLevel);
        this.levelIndex = levelIndex2;
    }

    public int getLevelIndex() {
        return this.levelIndex;
    }
}
