package com.camelgames.framework.events;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public final class EventManager {
    private static EventManager _instance = new EventManager();
    private final int _NumQueuesMax = 2;
    private HashMap<EventType, ArrayList<EventListener>> _dicListeners = new HashMap<>();
    private LinkedList<AbstractEvent>[] _eventQueues = ((LinkedList[]) Array.newInstance(LinkedList.class, 2));
    private int _iActiveQueue;
    private ListenerBuffer waitToAdd = new ListenerBuffer(null);
    private ListenerBuffer waitToDelete = new ListenerBuffer(null);

    public static EventManager getInstance() {
        return _instance;
    }

    private EventManager() {
        for (int i = 0; i < 2; i++) {
            this._eventQueues[i] = new LinkedList<>();
        }
    }

    public void addListener(EventType type, EventListener listener) {
        this.waitToAdd.add(type, listener);
        this.waitToDelete.remove(type, listener);
    }

    public void removeListener(EventType type, EventListener listener) {
        this.waitToDelete.add(type, listener);
        this.waitToAdd.remove(type, listener);
    }

    private void sync() {
        if (!this.waitToAdd.types.isEmpty()) {
            for (int i = 0; i < this.waitToAdd.types.size(); i++) {
                addListenerInternal(this.waitToAdd.types.get(i), this.waitToAdd.listeners.get(i));
            }
            this.waitToAdd.clear();
        }
        if (!this.waitToDelete.types.isEmpty()) {
            for (int i2 = 0; i2 < this.waitToDelete.types.size(); i2++) {
                removeListenerInternal(this.waitToDelete.types.get(i2), this.waitToDelete.listeners.get(i2));
            }
            this.waitToDelete.clear();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    private void addListenerInternal(EventType type, EventListener listener) {
        if (!this._dicListeners.containsKey(type)) {
            this._dicListeners.put(type, new ArrayList());
            this._dicListeners.get(type).add(listener);
        } else if (!this._dicListeners.get(type).contains(listener)) {
            this._dicListeners.get(type).add(listener);
        }
    }

    private void removeListenerInternal(EventType type, EventListener listener) {
        if (this._dicListeners.containsKey(type)) {
            this._dicListeners.get(type).remove(listener);
        }
    }

    public void sendAll() {
        sync();
        int iQueueToProcess = this._iActiveQueue;
        this._iActiveQueue = (this._iActiveQueue + 1) % 2;
        LinkedList<AbstractEvent> currentEvents = this._eventQueues[iQueueToProcess];
        for (AbstractEvent e = currentEvents.poll(); e != null; e = currentEvents.poll()) {
            sendEvent(e);
        }
    }

    public void postEvent(AbstractEvent e) {
        this._eventQueues[this._iActiveQueue].add(e);
    }

    public void postEvent(EventType type) {
        postEvent(new AbstractEvent(type));
    }

    public void cancelAllEvents() {
        for (List<AbstractEvent> events : this._eventQueues) {
            for (AbstractEvent event : events) {
                event.setCancel(true);
            }
        }
    }

    private void sendEvent(AbstractEvent e) {
        ArrayList<EventListener> items = this._dicListeners.get(e.getType());
        if (items != null) {
            for (int i = 0; i < items.size() && !e.getCancel(); i++) {
                ((EventListener) items.get(i)).HandleEvent(e);
            }
        }
    }

    public void reset() {
        this.waitToAdd.clear();
        this.waitToDelete.clear();
        this._dicListeners.clear();
        for (int i = 0; i < 2; i++) {
            this._eventQueues[i].clear();
        }
        this._iActiveQueue = 0;
    }

    private static class ListenerBuffer {
        public LinkedList<EventListener> listeners;
        public LinkedList<EventType> types;

        private ListenerBuffer() {
            this.types = new LinkedList<>();
            this.listeners = new LinkedList<>();
        }

        /* synthetic */ ListenerBuffer(ListenerBuffer listenerBuffer) {
            this();
        }

        public void add(EventType type, EventListener listener) {
            int i = 0;
            while (i < this.types.size()) {
                if (!this.types.get(i).equals(type) || !this.listeners.get(i).equals(listener)) {
                    i++;
                } else {
                    return;
                }
            }
            this.types.add(type);
            this.listeners.add(listener);
        }

        public void remove(EventType type, EventListener listener) {
            int i = 0;
            while (i < this.types.size()) {
                if (!this.types.get(i).equals(type) || !this.listeners.get(i).equals(listener)) {
                    i++;
                } else {
                    this.types.remove(i);
                    this.listeners.remove(i);
                    return;
                }
            }
        }

        public void clear() {
            this.types.clear();
            this.listeners.clear();
        }
    }
}
