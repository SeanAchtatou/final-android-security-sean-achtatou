package com.camelgames.framework.events;

public class FromUIEvent extends AbstractEvent {
    private Run run;

    public interface Run {
        void run();
    }

    public FromUIEvent(Run run2) {
        super(EventType.UICommand);
        this.run = run2;
    }

    public Run getRun() {
        return this.run;
    }
}
