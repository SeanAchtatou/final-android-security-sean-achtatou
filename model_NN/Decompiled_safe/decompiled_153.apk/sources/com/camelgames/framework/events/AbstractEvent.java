package com.camelgames.framework.events;

public class AbstractEvent {
    private boolean cancel;
    private EventType type;

    public AbstractEvent(EventType type2) {
        this.type = type2;
    }

    public EventType getType() {
        return this.type;
    }

    public void setType(EventType type2) {
        this.type = type2;
    }

    public boolean getCancel() {
        return this.cancel;
    }

    public void setCancel(boolean cancel2) {
        this.cancel = cancel2;
    }
}
