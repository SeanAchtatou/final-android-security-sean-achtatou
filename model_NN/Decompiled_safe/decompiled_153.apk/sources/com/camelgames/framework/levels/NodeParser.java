package com.camelgames.framework.levels;

import android.content.res.XmlResourceParser;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public interface NodeParser {
    void clear();

    String getNodeName();

    LevelScriptItem parse(XmlResourceParser xmlResourceParser) throws XmlPullParserException, IOException;
}
