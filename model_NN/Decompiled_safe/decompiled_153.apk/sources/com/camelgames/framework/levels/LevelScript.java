package com.camelgames.framework.levels;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class LevelScript implements LevelScriptItem {
    private ArrayList<LevelScriptItem> items = new ArrayList<>();

    public void load() {
        Iterator<LevelScriptItem> it = this.items.iterator();
        while (it.hasNext()) {
            it.next().load();
        }
    }

    public void add(LevelScriptItem item) {
        if (item != null && !this.items.contains(item)) {
            this.items.add(item);
        }
    }

    public Collection<LevelScriptItem> getItems() {
        return this.items;
    }
}
