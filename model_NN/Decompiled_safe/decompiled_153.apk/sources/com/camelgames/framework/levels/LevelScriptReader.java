package com.camelgames.framework.levels;

import android.content.res.XmlResourceParser;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.tiles.TilesetManager;
import java.io.IOException;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParserException;

public class LevelScriptReader {
    public static final float scale = 0.9375f;
    public static final float scaleX = (GraphicsManager.getInstance().getXScale() * 0.9375f);
    public static final float scaleY = (GraphicsManager.getInstance().getYScale() * 0.9375f);
    private HashMap<String, NodeParser> parsers = new HashMap<>();

    public LevelScript read(XmlResourceParser xmlParser) {
        LevelScriptItem item;
        TilesetManager.getInstance().clear();
        clear();
        LevelScript levelScript = new LevelScript();
        while (moveToStartTag(xmlParser)) {
            try {
                NodeParser parser = getNodeParser(xmlParser.getName());
                if (!(parser == null || (item = parser.parse(xmlParser)) == null)) {
                    levelScript.add(item);
                }
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        xmlParser.close();
        return levelScript;
    }

    public void addParser(NodeParser parser) {
        this.parsers.put(parser.getNodeName(), parser);
    }

    public static boolean moveToStartTag(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        int eventType = xmlParser.getEventType();
        while (eventType != 1) {
            eventType = xmlParser.next();
            if (eventType == 2) {
                return true;
            }
        }
        return false;
    }

    public static void xmlRequireStartTag(XmlResourceParser xmlParser, String tagName) throws XmlPullParserException, IOException {
        xmlParser.require(2, null, tagName);
    }

    public static void xmlRequireEndTag(XmlResourceParser xmlParser, String tagName) throws XmlPullParserException, IOException {
        xmlParser.require(3, null, tagName);
    }

    public static float getScreenX(XmlResourceParser xmlParser, String name) {
        return ((float) getInt(xmlParser, name, 0)) * scaleX;
    }

    public static float getScreenY(XmlResourceParser xmlParser, String name) {
        return ((float) getInt(xmlParser, name, 0)) * scaleY;
    }

    public static float getScreenXFloat(XmlResourceParser xmlParser, String name) {
        return getFloat(xmlParser, name, 0.0f) * scaleX;
    }

    public static float getScreenYFloat(XmlResourceParser xmlParser, String name) {
        return getFloat(xmlParser, name, 0.0f) * scaleY;
    }

    public static float getFloat(XmlResourceParser xmlParser, String name, float defaultValue) {
        return xmlParser.getAttributeFloatValue(null, name, defaultValue);
    }

    public static int getInt(XmlResourceParser xmlParser, String name, int defaultValue) {
        return xmlParser.getAttributeIntValue(null, name, defaultValue);
    }

    public static boolean getBoolean(XmlResourceParser xmlParser, String name) {
        return xmlParser.getAttributeBooleanValue(null, name, false);
    }

    public static String getString(XmlResourceParser xmlParser, String name) {
        return xmlParser.getAttributeValue(null, name);
    }

    public static float getXScaleByVFloat(XmlResourceParser xmlParser, String name) {
        return getXScaleByVFloat(getFloat(xmlParser, name, 0.0f) * scaleY);
    }

    public static float getXScaleByVFloat(float x) {
        return ((((float) GraphicsManager.screenWidth()) - (512.0f * scaleY)) * 0.5f) + x;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    /* access modifiers changed from: protected */
    public NodeParser getNodeParser(String nodeName) {
        if (this.parsers.containsKey(nodeName)) {
            return this.parsers.get(nodeName);
        }
        return null;
    }

    private void clear() {
        for (NodeParser parser : this.parsers.values()) {
            parser.clear();
        }
    }
}
