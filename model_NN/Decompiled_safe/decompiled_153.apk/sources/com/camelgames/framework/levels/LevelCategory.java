package com.camelgames.framework.levels;

import com.camelgames.framework.utilities.IOUtility;
import java.util.Comparator;

public class LevelCategory {
    private final Class RXmlClass;
    private final Comparator<String> comparator;
    private int[] levelIds;
    private final String prefix;

    public LevelCategory(Class RXmlClass2, final String prefix2) {
        this.RXmlClass = RXmlClass2;
        this.prefix = prefix2;
        this.comparator = new Comparator<String>() {
            public int compare(String str1, String str2) {
                String str12 = str1.toLowerCase();
                String str22 = str2.toLowerCase();
                int start1 = str12.indexOf(prefix2);
                int start2 = str22.indexOf(prefix2);
                if (start1 < 0 || start2 < 0) {
                    return 0;
                }
                String level1Str = str12.substring(start1 + prefix2.length());
                String level2Str = str22.substring(start2 + prefix2.length());
                try {
                    return Integer.parseInt(level1Str) - Integer.parseInt(level2Str);
                } catch (NumberFormatException e) {
                    return level1Str.compareTo(level2Str);
                }
            }
        };
    }

    public int[] getLevelIds() {
        if (this.levelIds == null) {
            this.levelIds = IOUtility.getResourceIdsByPrefix(this.RXmlClass, this.prefix, this.comparator);
        }
        return this.levelIds;
    }

    public int getLevelsCount() {
        if (this.levelIds == null) {
            this.levelIds = IOUtility.getResourceIdsByPrefix(this.RXmlClass, this.prefix, this.comparator);
        }
        return this.levelIds.length;
    }
}
