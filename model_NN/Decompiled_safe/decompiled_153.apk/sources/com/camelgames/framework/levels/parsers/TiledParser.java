package com.camelgames.framework.levels.parsers;

import android.content.res.XmlResourceParser;
import com.camelgames.framework.graphics.tiles.TileLayerData;
import com.camelgames.framework.graphics.tiles.TileSet;
import com.camelgames.framework.levels.LevelScriptReader;
import com.camelgames.framework.utilities.Base64;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;
import org.xmlpull.v1.XmlPullParserException;

public class TiledParser {
    public static TileSet parseTileSet(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        TileSet set = new TileSet();
        set.firstGid = LevelScriptReader.getInt(xmlParser, "firstgid", 0);
        set.tileWidth = LevelScriptReader.getInt(xmlParser, "tilewidth", 0);
        set.tileHeight = LevelScriptReader.getInt(xmlParser, "tileheight", 0);
        set.tileSpacing = LevelScriptReader.getInt(xmlParser, "spacing", 0);
        set.tileMargin = LevelScriptReader.getInt(xmlParser, "margin", 0);
        LevelScriptReader.moveToStartTag(xmlParser);
        LevelScriptReader.xmlRequireStartTag(xmlParser, "image");
        set.imageStr = LevelScriptReader.getString(xmlParser, "source");
        set.load();
        return set;
    }

    public static TileLayerData parseTiledLayer(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        TileLayerData tileLayerData = new TileLayerData();
        int layerWidth = LevelScriptReader.getInt(xmlParser, "width", 0);
        int layerHeight = LevelScriptReader.getInt(xmlParser, "height", 0);
        tileLayerData.name = LevelScriptReader.getString(xmlParser, "name");
        LevelScriptReader.moveToStartTag(xmlParser);
        tileLayerData.properties = readProperties(xmlParser);
        tileLayerData.data = readData(xmlParser, layerWidth, layerHeight);
        return tileLayerData;
    }

    private static HashMap<String, String> readProperties(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        if (!xmlParser.getName().equalsIgnoreCase("properties")) {
            return null;
        }
        HashMap<String, String> properties = new HashMap<>();
        while (LevelScriptReader.moveToStartTag(xmlParser) && xmlParser.getName().equalsIgnoreCase("property")) {
            properties.put(LevelScriptReader.getString(xmlParser, "name"), LevelScriptReader.getString(xmlParser, "value"));
        }
        return properties;
    }

    private static int[][] readData(XmlResourceParser xmlParser, int layerWidth, int layerHeight) throws XmlPullParserException, IOException {
        String data;
        InputStream is;
        int[][] tiles = (int[][]) Array.newInstance(Integer.TYPE, layerHeight, layerWidth);
        LevelScriptReader.xmlRequireStartTag(xmlParser, "data");
        String encoding = LevelScriptReader.getString(xmlParser, "encoding");
        String compression = LevelScriptReader.getString(xmlParser, "compression");
        if ("base64".equalsIgnoreCase(encoding) && (data = xmlParser.nextText()) != null) {
            ByteArrayInputStream bais = new ByteArrayInputStream(Base64.decode(data.trim().toCharArray()));
            if ("gzip".equalsIgnoreCase(compression)) {
                is = new GZIPInputStream(bais);
            } else {
                is = bais;
            }
            for (int y = 0; y < tiles.length; y++) {
                for (int x = 0; x < tiles[0].length; x++) {
                    tiles[y][x] = 0 | is.read() | (is.read() << 8) | (is.read() << 16) | (is.read() << 24);
                }
            }
        }
        return tiles;
    }
}
