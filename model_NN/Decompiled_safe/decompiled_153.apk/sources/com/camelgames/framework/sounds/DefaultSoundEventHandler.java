package com.camelgames.framework.sounds;

import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.sounds.SoundManagerBase;

public class DefaultSoundEventHandler implements SoundEventHandler {
    private final EventType eventType;
    private final SoundManagerBase.Sound sound;

    public DefaultSoundEventHandler(EventType eventType2, SoundManagerBase.Sound sound2) {
        this.eventType = eventType2;
        this.sound = sound2;
    }

    public void HandleEvent(AbstractEvent e) {
        if (this.sound != null) {
            this.sound.play();
        }
    }

    public EventType getEventType() {
        return this.eventType;
    }

    public void stopSounds() {
        if (this.sound != null) {
            this.sound.stop();
        }
    }
}
