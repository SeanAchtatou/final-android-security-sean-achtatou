package com.camelgames.framework.sounds;

import com.camelgames.framework.events.EventListener;
import com.camelgames.framework.events.EventType;

public interface SoundEventHandler extends EventListener {
    EventType getEventType();

    void stopSounds();
}
