package com.camelgames.framework.sounds;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.SoundPool;
import com.camelgames.framework.ConfigurationManager;
import com.camelgames.framework.Skeleton.EventListenerUtil;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.events.EventListener;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.math.MathUtils;
import com.camelgames.framework.ui.UIUtility;
import com.mobclix.android.sdk.Base64;
import java.io.IOException;
import java.util.ArrayList;

public abstract class SoundManagerBase implements EventListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$events$EventType = null;
    public static final String SOUND_CONFIG = "SoundConfig";
    protected int MAX_STREAMS = 8;
    private ArrayList<BackgroundSound> backgroundSounds = new ArrayList<>();
    protected EventListenerUtil eventListenerUtil = new EventListenerUtil();
    /* access modifiers changed from: private */
    public boolean isMute;
    private ArrayList<SoundEventHandler> soundEventHandlers = new ArrayList<>();
    protected SoundPool soundPool;

    /* access modifiers changed from: protected */
    public abstract void initiateInternal(Context context);

    /* access modifiers changed from: protected */
    public abstract void internalHandleEvent(AbstractEvent abstractEvent);

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$events$EventType() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$framework$events$EventType;
        if (iArr == null) {
            iArr = new int[EventType.values().length];
            try {
                iArr[EventType.Arrive.ordinal()] = 61;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[EventType.AttachRagdoll.ordinal()] = 65;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[EventType.AttachStick.ordinal()] = 66;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[EventType.AttachedToJoint.ordinal()] = 40;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[EventType.BallCollide.ordinal()] = 8;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[EventType.Bomb.ordinal()] = 67;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[EventType.BombLarge.ordinal()] = 68;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[EventType.BrickChanged.ordinal()] = 45;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[EventType.Button.ordinal()] = 30;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[EventType.Captured.ordinal()] = 34;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[EventType.CarCreated.ordinal()] = 39;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[EventType.Catched.ordinal()] = 11;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[EventType.Collide.ordinal()] = 6;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[EventType.ContinueGame.ordinal()] = 23;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[EventType.Crash.ordinal()] = 59;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[EventType.Customise.ordinal()] = 25;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[EventType.DoubleTap.ordinal()] = 10;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[EventType.EraseAll.ordinal()] = 41;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[EventType.Fall.ordinal()] = 36;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[EventType.Favourite.ordinal()] = 46;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[EventType.GameBegin.ordinal()] = 26;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[EventType.GotScore.ordinal()] = 18;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[EventType.GraphicsCreated.ordinal()] = 1;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[EventType.GraphicsDestroyed.ordinal()] = 2;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[EventType.GraphicsResized.ordinal()] = 3;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[EventType.HighScore.ordinal()] = 28;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[EventType.JointCreated.ordinal()] = 62;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[EventType.Landed.ordinal()] = 58;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[EventType.LevelDown.ordinal()] = 15;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[EventType.LevelEditorCopyBrick.ordinal()] = 47;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[EventType.LevelEditorDeleteBrick.ordinal()] = 48;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[EventType.LevelEditorEditGround.ordinal()] = 51;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[EventType.LevelEditorEditQueue.ordinal()] = 50;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[EventType.LevelEditorExit.ordinal()] = 53;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[EventType.LevelEditorNewBrick.ordinal()] = 49;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[EventType.LevelEditorSave.ordinal()] = 54;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[EventType.LevelEditorStart.ordinal()] = 52;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[EventType.LevelEditorTest.ordinal()] = 55;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[EventType.LevelFailed.ordinal()] = 14;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[EventType.LevelFinished.ordinal()] = 13;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[EventType.LevelUp.ordinal()] = 12;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[EventType.LineCreated.ordinal()] = 37;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[EventType.LineDestroyed.ordinal()] = 38;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[EventType.LoadLevel.ordinal()] = 16;
            } catch (NoSuchFieldError e44) {
            }
            try {
                iArr[EventType.MainMenu.ordinal()] = 19;
            } catch (NoSuchFieldError e45) {
            }
            try {
                iArr[EventType.NewGame.ordinal()] = 22;
            } catch (NoSuchFieldError e46) {
            }
            try {
                iArr[EventType.PapaStackLoadLevel.ordinal()] = 43;
            } catch (NoSuchFieldError e47) {
            }
            try {
                iArr[EventType.PressSwitch.ordinal()] = 69;
            } catch (NoSuchFieldError e48) {
            }
            try {
                iArr[EventType.Purchased.ordinal()] = 70;
            } catch (NoSuchFieldError e49) {
            }
            try {
                iArr[EventType.RagdollDied.ordinal()] = 42;
            } catch (NoSuchFieldError e50) {
            }
            try {
                iArr[EventType.ReadyToLand.ordinal()] = 57;
            } catch (NoSuchFieldError e51) {
            }
            try {
                iArr[EventType.ReadyToLoadLevel.ordinal()] = 17;
            } catch (NoSuchFieldError e52) {
            }
            try {
                iArr[EventType.Refunded.ordinal()] = 71;
            } catch (NoSuchFieldError e53) {
            }
            try {
                iArr[EventType.Replay.ordinal()] = 21;
            } catch (NoSuchFieldError e54) {
            }
            try {
                iArr[EventType.Restart.ordinal()] = 20;
            } catch (NoSuchFieldError e55) {
            }
            try {
                iArr[EventType.SelectLevel.ordinal()] = 24;
            } catch (NoSuchFieldError e56) {
            }
            try {
                iArr[EventType.SensorContact.ordinal()] = 7;
            } catch (NoSuchFieldError e57) {
            }
            try {
                iArr[EventType.Shoot.ordinal()] = 63;
            } catch (NoSuchFieldError e58) {
            }
            try {
                iArr[EventType.SingleTap.ordinal()] = 9;
            } catch (NoSuchFieldError e59) {
            }
            try {
                iArr[EventType.SoundOff.ordinal()] = 32;
            } catch (NoSuchFieldError e60) {
            }
            try {
                iArr[EventType.SoundOn.ordinal()] = 31;
            } catch (NoSuchFieldError e61) {
            }
            try {
                iArr[EventType.StartExplode.ordinal()] = 64;
            } catch (NoSuchFieldError e62) {
            }
            try {
                iArr[EventType.StartToRecord.ordinal()] = 44;
            } catch (NoSuchFieldError e63) {
            }
            try {
                iArr[EventType.StaticEdgesCreated.ordinal()] = 4;
            } catch (NoSuchFieldError e64) {
            }
            try {
                iArr[EventType.StaticEdgesDestroyed.ordinal()] = 5;
            } catch (NoSuchFieldError e65) {
            }
            try {
                iArr[EventType.Tick.ordinal()] = 33;
            } catch (NoSuchFieldError e66) {
            }
            try {
                iArr[EventType.Triggered.ordinal()] = 35;
            } catch (NoSuchFieldError e67) {
            }
            try {
                iArr[EventType.Tutorial.ordinal()] = 27;
            } catch (NoSuchFieldError e68) {
            }
            try {
                iArr[EventType.UICommand.ordinal()] = 29;
            } catch (NoSuchFieldError e69) {
            }
            try {
                iArr[EventType.UploadLevel.ordinal()] = 56;
            } catch (NoSuchFieldError e70) {
            }
            try {
                iArr[EventType.Warning.ordinal()] = 60;
            } catch (NoSuchFieldError e71) {
            }
            $SWITCH_TABLE$com$camelgames$framework$events$EventType = iArr;
        }
        return iArr;
    }

    public class BackgroundSound {
        private boolean isPlaying;
        private MediaPlayer mediaPlayer;

        public BackgroundSound(int resId) {
            this.mediaPlayer = MediaPlayer.create(UIUtility.getMainAcitvity(), resId);
            this.mediaPlayer.setLooping(true);
            SoundManagerBase.this.addBackgroundSound(this);
        }

        public void dispose() {
            SoundManagerBase.this.removeBackgroundSound(this);
            if (this.mediaPlayer != null) {
                this.mediaPlayer.release();
                this.mediaPlayer = null;
            }
        }

        public void play() {
            if (!SoundManagerBase.this.isMute) {
                prepareAndPlay();
            }
            this.isPlaying = true;
        }

        public void stop() {
            this.isPlaying = false;
            if (this.mediaPlayer != null) {
                this.mediaPlayer.stop();
            }
        }

        public void setMute(boolean isMute) {
            if (isMute) {
                if (this.mediaPlayer.isPlaying()) {
                    this.mediaPlayer.stop();
                }
            } else if (this.isPlaying && !this.mediaPlayer.isPlaying()) {
                prepareAndPlay();
            }
        }

        private void prepareAndPlay() {
            this.mediaPlayer.stop();
            try {
                this.mediaPlayer.prepare();
            } catch (IOException | IllegalStateException e) {
            }
            this.mediaPlayer.start();
        }
    }

    public class Sound {
        public long lastPlayedTime;
        public int priority;
        public int soundId;
        public long stopDurartion;
        public int streamId;
        public float volume = 0.5f;

        public Sound() {
        }

        public Sound(int resId, long stopDuration, int priority2) {
            initiate(SoundManagerBase.this.soundPool.load(UIUtility.getMainAcitvity(), resId, 1), stopDuration, priority2);
        }

        public void initiate(int soundId2, long stopDuration) {
            initiate(soundId2, stopDuration, 0);
        }

        public void initiate(int soundId2, long stopDuration, int priority2) {
            this.soundId = soundId2;
            this.stopDurartion = stopDuration;
            this.priority = priority2;
        }

        public void play() {
            if (!SoundManagerBase.this.isMute) {
                long curTime = System.currentTimeMillis();
                if (curTime > this.lastPlayedTime + this.stopDurartion) {
                    this.streamId = SoundManagerBase.this.soundPool.play(this.soundId, this.volume, this.volume, this.priority, 0, 1.0f);
                    this.lastPlayedTime = curTime;
                }
            }
        }

        public void stop() {
            if (this.streamId > 0) {
                SoundManagerBase.this.soundPool.stop(this.streamId);
            }
        }
    }

    public class RandomSounds extends Sound {
        private int previousRandom = -1;
        private int[] soundIds;

        public RandomSounds(int[] resIds, long stopDuration, int priority) {
            super();
            this.stopDurartion = stopDuration;
            this.priority = priority;
            this.soundIds = new int[resIds.length];
            for (int i = 0; i < resIds.length; i++) {
                this.soundIds[i] = SoundManagerBase.this.soundPool.load(UIUtility.getMainAcitvity(), resIds[i], 1);
            }
        }

        public void play() {
            if (!SoundManagerBase.this.isMute) {
                long curTime = System.currentTimeMillis();
                if (curTime > this.lastPlayedTime + this.stopDurartion) {
                    int random = MathUtils.randomInt(this.soundIds.length);
                    while (random == this.previousRandom) {
                        random = MathUtils.randomInt(this.soundIds.length);
                    }
                    this.soundId = this.soundIds[random];
                    this.streamId = SoundManagerBase.this.soundPool.play(this.soundId, this.volume, this.volume, this.priority, 0, 1.0f);
                    this.lastPlayedTime = curTime;
                    this.previousRandom = random;
                }
            }
        }
    }

    public void load(Context context) {
        if (this.soundPool == null) {
            this.eventListenerUtil.addEventType(EventType.SoundOn);
            this.eventListenerUtil.addEventType(EventType.SoundOff);
            this.soundPool = new SoundPool(this.MAX_STREAMS, 3, 0);
            initiateInternal(context);
            setMute(ConfigurationManager.getInstance().getBoolean(SOUND_CONFIG, false));
        }
    }

    public void HandleEvent(AbstractEvent e) {
        if (this.soundPool != null) {
            switch ($SWITCH_TABLE$com$camelgames$framework$events$EventType()[e.getType().ordinal()]) {
                case 31:
                    setMute(false);
                    return;
                case Base64.ORDERED /*32*/:
                    setMute(true);
                    return;
                default:
                    dispatchSoundEffect(e);
                    internalHandleEvent(e);
                    return;
            }
        }
    }

    public void filpMute() {
        setMute(!this.isMute);
    }

    public void setMute(boolean isMute2) {
        if (isMute2) {
            this.eventListenerUtil.removeListener(this);
            EventManager.getInstance().addListener(EventType.SoundOn, this);
        } else {
            this.eventListenerUtil.addListener(this);
        }
        this.isMute = isMute2;
        ConfigurationManager.getInstance().pubBoolean(SOUND_CONFIG, isMute2);
        setMuteForBackgroundSounds(isMute2);
    }

    public void setActivityState(boolean isRunning) {
        if (isRunning) {
            if (!this.isMute) {
                setMuteForBackgroundSounds(false);
            }
        } else if (!this.isMute) {
            setMuteForBackgroundSounds(true);
        }
    }

    private void setMuteForBackgroundSounds(boolean isMute2) {
        for (int i = 0; i < this.backgroundSounds.size(); i++) {
            this.backgroundSounds.get(i).setMute(isMute2);
        }
    }

    public boolean isMute() {
        return this.isMute;
    }

    public void stopSound() {
        for (int i = 0; i < this.soundEventHandlers.size(); i++) {
            this.soundEventHandlers.get(i).stopSounds();
        }
    }

    public void addSoundEventHandler(EventType eventType, Sound sound) {
        addSoundEventHandler(new DefaultSoundEventHandler(eventType, sound));
    }

    public void addSoundEventHandler(SoundEventHandler soundEventHandler) {
        this.soundEventHandlers.add(soundEventHandler);
        this.eventListenerUtil.addEventType(soundEventHandler.getEventType());
    }

    public void addBackgroundSound(BackgroundSound backgroundSound) {
        if (!this.backgroundSounds.contains(backgroundSound)) {
            this.backgroundSounds.add(backgroundSound);
        }
    }

    public void removeBackgroundSound(BackgroundSound backgroundSound) {
        this.backgroundSounds.remove(backgroundSound);
    }

    private void dispatchSoundEffect(AbstractEvent e) {
        for (int i = 0; i < this.soundEventHandlers.size(); i++) {
            SoundEventHandler handler = this.soundEventHandlers.get(i);
            if (handler.getEventType().equals(e.getType())) {
                handler.HandleEvent(e);
            }
        }
    }
}
