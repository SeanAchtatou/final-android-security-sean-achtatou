package com.camelgames.framework;

import android.app.Activity;
import android.content.Context;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.camelgames.framework.entities.EntityManager;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.altas.AltasTextureManager;
import com.camelgames.framework.graphics.font.TextBuilder;
import com.camelgames.framework.graphics.textures.TextureUtility;
import com.camelgames.framework.surfaceview.EGLConfigChooser;
import com.camelgames.framework.surfaceview.GLSurfaceView;
import com.camelgames.framework.touch.TouchManager;
import com.camelgames.framework.ui.UIUtility;
import com.camelgames.framework.ui.actions.ActionManager;
import com.camelgames.framework.utilities.SDUtility;
import com.camelgames.ndk.JNILibrary;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

public abstract class GLScreenViewBase extends GLSurfaceView implements GLSurfaceView.Renderer {
    protected static boolean isInitialized;
    protected int clearBits = 16640;
    private boolean drawFPS;
    private float fixedTimeFrame = 0.05f;
    private int fps;
    private float frameAverage = ((float) this.target);
    protected boolean isStoped = true;
    private long lastFrame = System.currentTimeMillis();
    private long previousTime = 0;
    private int target = 16;
    private TextBuilder textBuilder;
    private int touchSleepTime;

    /* access modifiers changed from: protected */
    public abstract Class<?> getArrayClass();

    /* access modifiers changed from: protected */
    public abstract Class<?> getDrawableClass();

    /* access modifiers changed from: protected */
    public abstract String getGameFolderName();

    public GLScreenViewBase(Context context) {
        super(context);
        construct();
    }

    public GLScreenViewBase(Context context, AttributeSet attrs) {
        super(context, attrs);
        construct();
    }

    public GLScreenViewBase(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        construct();
    }

    private void construct() {
        initiateEGLConfig();
        setRenderer(this);
        requestFocus();
        setFocusableInTouchMode(true);
        UIUtility.setMainActivity((Activity) getContext());
        SDUtility.setGameName(getGameFolderName());
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GraphicsManager.getInstance().initiate(gl);
        TextureUtility.getInstance().initiated(getDrawableClass(), 520093696);
        AltasTextureManager.getInstance().initiate(getArrayClass());
        createdInternal(gl);
    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
        this.isStoped = true;
        GraphicsManager.getInstance().clearColor(gl);
        GraphicsManager.getInstance().changeSize(width, height);
        EntityManager.getInstance().initiate();
        JNILibrary.gl = GraphicsManager.getInstance().getGL();
        JNILibrary.screenWidth = width;
        JNILibrary.screenHeight = height;
        JNILibrary.deviceReset(width, height);
        if (!isInitialized) {
            AltasTextureManager.getInstance().pushAltasInfos();
            pushAnimationInfos();
            isInitialized = true;
        }
        pushFiles();
        initiateInternal(GraphicsManager.screenWidth(), GraphicsManager.screenHeight());
        this.previousTime = SystemClock.elapsedRealtime();
        this.isStoped = false;
    }

    public void onDrawFrame(GL10 gl) {
        long currentTime = SystemClock.elapsedRealtime();
        if (this.previousTime == 0) {
            this.previousTime = currentTime;
            return;
        }
        float elapsedTime = 0.001f * ((float) (currentTime - this.previousTime));
        if (elapsedTime > this.fixedTimeFrame) {
            elapsedTime = this.fixedTimeFrame;
        }
        this.previousTime = currentTime;
        preUpdate(elapsedTime);
        if (!isPaused()) {
            onUpdateInternal(elapsedTime);
            EntityManager.getInstance().update(elapsedTime);
        } else {
            elapsedTime = 0.0f;
        }
        preRender(gl);
        GraphicsManager.getInstance().render(gl, elapsedTime);
        JNILibrary.step(elapsedTime);
        if (this.drawFPS) {
            drawFPS(gl);
        }
    }

    /* access modifiers changed from: protected */
    public void preUpdate(float elapsedTime) {
        TouchManager.getInstace().flushInputs();
        EventManager.getInstance().sendAll();
        ActionManager.instance.update(elapsedTime);
    }

    /* access modifiers changed from: protected */
    public void preRender(GL10 gl) {
        gl.glClear(this.clearBits);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (this.isStoped) {
            return false;
        }
        TouchManager.getInstace().addKey(keyCode);
        return true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.isStoped) {
            return false;
        }
        TouchManager.getInstace().addTouch((int) event.getX(), (int) event.getY(), event.getAction());
        if (this.touchSleepTime > 0) {
            try {
                Thread.sleep((long) this.touchSleepTime);
            } catch (InterruptedException e) {
            }
        }
        return true;
    }

    public void drawFPS(GL10 gl) {
        long current = System.currentTimeMillis();
        this.frameAverage = ((this.frameAverage * 10.0f) + ((float) (current - this.lastFrame))) / 11.0f;
        this.lastFrame = current;
        this.fps = (int) (1000.0f / this.frameAverage);
        this.textBuilder.drawFpsString(gl, this.fps, 16, 40, 16, 10);
    }

    public void setDrawFPS(TextBuilder textBuilder2, boolean drawFPS2) {
        this.textBuilder = textBuilder2;
        this.drawFPS = drawFPS2;
    }

    /* access modifiers changed from: protected */
    public void initiateEGLConfig() {
        setEGLConfigChooser(new EGLConfigChooser() {
            public EGLConfig chooseConfig(EGL10 egl, EGLDisplay display) {
                EGLConfig[] configs = new EGLConfig[1];
                egl.eglChooseConfig(display, new int[]{12325, 16, 12344}, configs, 1, new int[1]);
                return configs[0];
            }
        });
    }

    /* access modifiers changed from: protected */
    public void createdInternal(GL10 gl) {
    }

    /* access modifiers changed from: protected */
    public void initiateInternal(int width, int height) {
    }

    /* access modifiers changed from: protected */
    public void onUpdateInternal(float elapsedTime) {
    }

    /* access modifiers changed from: protected */
    public boolean isPaused() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void pushAnimationInfos() {
    }

    /* access modifiers changed from: protected */
    public void pushFiles() {
    }

    /* access modifiers changed from: protected */
    public void setFixedTimeFrame(float timeFrame) {
        this.fixedTimeFrame = timeFrame;
    }

    /* access modifiers changed from: protected */
    public void setTouchSleepTime(int touchSleepTime2) {
        this.touchSleepTime = touchSleepTime2;
    }
}
