package com.camelgames.framework.network;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.Serializable;

public class AsyncHttpRequest extends Thread {
    private static final String ENCODING = "UTF-8";
    public static final String KEY_RESPONSE = "response";
    public static final String NO_CONNECTION = "NoConnection";
    public static final String OUT_OF_SERVICE = "OutOfService";
    private static final float TIME_OUT = 10.0f;
    public static ToStringProcessor toStringProcessor = new ToStringProcessor();
    private boolean isCanceled;
    private final Handler mHandler;
    private RunInternal runInternal;

    public interface InputStreamProcessor {
        boolean process(InputStream inputStream, Bundle bundle);
    }

    private interface RunInternal {
        void run();
    }

    public AsyncHttpRequest(Handler handler) {
        this.mHandler = handler;
    }

    public void run() {
        if (this.runInternal != null) {
            this.runInternal.run();
        }
    }

    public void cancel() {
        this.isCanceled = true;
        interrupt();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.framework.network.AsyncHttpRequest.start(java.lang.String, com.camelgames.framework.network.AsyncHttpRequest$InputStreamProcessor):void
     arg types: [java.lang.String, com.camelgames.framework.network.AsyncHttpRequest$ToStringProcessor]
     candidates:
      com.camelgames.framework.network.AsyncHttpRequest.start(java.lang.String, java.lang.Object):void
      com.camelgames.framework.network.AsyncHttpRequest.start(java.lang.String, com.camelgames.framework.network.AsyncHttpRequest$InputStreamProcessor):void */
    public void start(String url) {
        start(url, (InputStreamProcessor) toStringProcessor);
    }

    public void start(final String url, final InputStreamProcessor inputStreamProcessor) {
        this.runInternal = new RunInternal() {
            public void run() {
                AsyncHttpRequest.this.request(url, inputStreamProcessor);
            }
        };
        start();
    }

    public void start(String url, Object data) {
        start(url, data, toStringProcessor);
    }

    public void start(final String url, final Object data, final InputStreamProcessor inputStreamProcessor) {
        this.runInternal = new RunInternal() {
            public void run() {
                AsyncHttpRequest.this.sendObject(url, data, inputStreamProcessor);
            }
        };
        start();
    }

    /* JADX WARN: Type inference failed for: r4v4, types: [java.net.URLConnection] */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void request(java.lang.String r7, com.camelgames.framework.network.AsyncHttpRequest.InputStreamProcessor r8) {
        /*
            r6 = this;
            android.os.Bundle r1 = new android.os.Bundle
            r1.<init>()
            r2 = 0
            java.net.URL r4 = new java.net.URL     // Catch:{ IOException -> 0x003f }
            r4.<init>(r7)     // Catch:{ IOException -> 0x003f }
            java.net.URLConnection r4 = r4.openConnection()     // Catch:{ IOException -> 0x003f }
            r0 = r4
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x003f }
            r2 = r0
            r4 = 10000(0x2710, float:1.4013E-41)
            r2.setConnectTimeout(r4)     // Catch:{ IOException -> 0x003f }
            r4 = 1
            r2.setDoInput(r4)     // Catch:{ IOException -> 0x003f }
            r2.connect()     // Catch:{ IOException -> 0x003f }
            int r4 = r2.getResponseCode()     // Catch:{ IOException -> 0x003f }
            r5 = 200(0xc8, float:2.8E-43)
            if (r4 != r5) goto L_0x0037
            java.io.InputStream r4 = r2.getInputStream()     // Catch:{ IOException -> 0x003f }
            r8.process(r4, r1)     // Catch:{ IOException -> 0x003f }
        L_0x002e:
            if (r2 == 0) goto L_0x0033
            r2.disconnect()
        L_0x0033:
            r6.invokeHandler(r1)
            return
        L_0x0037:
            java.lang.String r4 = "response"
            java.lang.String r5 = "NoConnection"
            r1.putString(r4, r5)     // Catch:{ IOException -> 0x003f }
            goto L_0x002e
        L_0x003f:
            r4 = move-exception
            r3 = r4
            java.lang.String r4 = "response"
            java.lang.String r5 = "OutOfService"
            r1.putString(r4, r5)     // Catch:{ all -> 0x004e }
            if (r2 == 0) goto L_0x0033
            r2.disconnect()
            goto L_0x0033
        L_0x004e:
            r4 = move-exception
            if (r2 == 0) goto L_0x0054
            r2.disconnect()
        L_0x0054:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.camelgames.framework.network.AsyncHttpRequest.request(java.lang.String, com.camelgames.framework.network.AsyncHttpRequest$InputStreamProcessor):void");
    }

    /* JADX WARN: Type inference failed for: r6v4, types: [java.net.URLConnection] */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void sendObject(java.lang.String r9, java.lang.Object r10, com.camelgames.framework.network.AsyncHttpRequest.InputStreamProcessor r11) {
        /*
            r8 = this;
            android.os.Bundle r1 = new android.os.Bundle
            r1.<init>()
            r2 = 0
            java.net.URL r6 = new java.net.URL     // Catch:{ IOException -> 0x0061 }
            r6.<init>(r9)     // Catch:{ IOException -> 0x0061 }
            java.net.URLConnection r6 = r6.openConnection()     // Catch:{ IOException -> 0x0061 }
            r0 = r6
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0061 }
            r2 = r0
            r6 = 10000(0x2710, float:1.4013E-41)
            r2.setConnectTimeout(r6)     // Catch:{ IOException -> 0x0061 }
            r6 = 1
            r2.setDoInput(r6)     // Catch:{ IOException -> 0x0061 }
            r6 = 1
            r2.setDoOutput(r6)     // Catch:{ IOException -> 0x0061 }
            java.lang.String r6 = "POST"
            r2.setRequestMethod(r6)     // Catch:{ IOException -> 0x0061 }
            java.lang.String r6 = "Content-type"
            java.lang.String r7 = "application/x-java-serialized-object"
            r2.setRequestProperty(r6, r7)     // Catch:{ IOException -> 0x0061 }
            r2.connect()     // Catch:{ IOException -> 0x0061 }
            java.io.OutputStream r5 = r2.getOutputStream()     // Catch:{ IOException -> 0x0061 }
            java.io.ObjectOutputStream r4 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0061 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x0061 }
            r4.writeObject(r10)     // Catch:{ IOException -> 0x0061 }
            r4.flush()     // Catch:{ IOException -> 0x0061 }
            r4.close()     // Catch:{ IOException -> 0x0061 }
            int r6 = r2.getResponseCode()     // Catch:{ IOException -> 0x0061 }
            r7 = 200(0xc8, float:2.8E-43)
            if (r6 != r7) goto L_0x0059
            java.io.InputStream r6 = r2.getInputStream()     // Catch:{ IOException -> 0x0061 }
            r11.process(r6, r1)     // Catch:{ IOException -> 0x0061 }
        L_0x0050:
            if (r2 == 0) goto L_0x0055
            r2.disconnect()
        L_0x0055:
            r8.invokeHandler(r1)
            return
        L_0x0059:
            java.lang.String r6 = "response"
            java.lang.String r7 = "NoConnection"
            r1.putString(r6, r7)     // Catch:{ IOException -> 0x0061 }
            goto L_0x0050
        L_0x0061:
            r6 = move-exception
            r3 = r6
            java.lang.String r6 = "response"
            java.lang.String r7 = "OutOfService"
            r1.putString(r6, r7)     // Catch:{ all -> 0x0070 }
            if (r2 == 0) goto L_0x0055
            r2.disconnect()
            goto L_0x0055
        L_0x0070:
            r6 = move-exception
            if (r2 == 0) goto L_0x0076
            r2.disconnect()
        L_0x0076:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.camelgames.framework.network.AsyncHttpRequest.sendObject(java.lang.String, java.lang.Object, com.camelgames.framework.network.AsyncHttpRequest$InputStreamProcessor):void");
    }

    private void invokeHandler(Bundle b) {
        try {
            if (this.mHandler != null && !this.isCanceled) {
                Message msg = this.mHandler.obtainMessage();
                msg.setData(b);
                this.mHandler.sendMessage(msg);
            }
        } catch (Exception e) {
        }
    }

    public static class ToStringProcessor implements InputStreamProcessor {
        public boolean process(InputStream inputStream, Bundle b) {
            try {
                StringBuilder outputBuilder = new StringBuilder();
                if (inputStream != null) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, AsyncHttpRequest.ENCODING));
                    while (true) {
                        String string = reader.readLine();
                        if (string == null) {
                            break;
                        }
                        outputBuilder.append(string).append(10);
                    }
                }
                b.putString(AsyncHttpRequest.KEY_RESPONSE, outputBuilder.toString());
                return true;
            } catch (IOException e) {
                return false;
            }
        }
    }

    public static class ToObjectProcessor<T extends Serializable> implements InputStreamProcessor {
        public boolean process(InputStream inputStream, Bundle b) {
            try {
                b.putSerializable(AsyncHttpRequest.KEY_RESPONSE, (Serializable) new ObjectInputStream(inputStream).readObject());
                return true;
            } catch (ClassNotFoundException e) {
                return false;
            } catch (IOException e2) {
                return false;
            }
        }
    }
}
