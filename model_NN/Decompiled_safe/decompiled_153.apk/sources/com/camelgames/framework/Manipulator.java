package com.camelgames.framework;

import com.camelgames.framework.Skeleton.RenderableListener;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.graphics.Renderable;
import com.camelgames.framework.touch.TouchManager;
import javax.microedition.khronos.opengles.GL10;

public class Manipulator extends RenderableListener implements TouchManager.TouchProcessor {
    private boolean isStoped;

    public Manipulator() {
        setPriority(Renderable.PRIORITY.HIGH);
    }

    public void setStoped(boolean isStoped2) {
        this.isStoped = isStoped2;
    }

    public boolean isStoped() {
        return this.isStoped;
    }

    public void HandleEvent(AbstractEvent e) {
    }

    public void render(GL10 gl, float elapsedTime) {
    }

    public void touchAction(int x, int y, int action) {
        if (!this.isStoped) {
            switch (action) {
                case 0:
                    touchDown(x, y);
                    return;
                case 1:
                    touchUp(x, y);
                    return;
                case 2:
                    touchMove(x, y);
                    return;
                default:
                    return;
            }
        }
    }

    public void touchAction(int x, int y, int action, long eventTime) {
        if (!this.isStoped) {
            switch (action) {
                case 0:
                    touchDown(x, y, eventTime);
                    return;
                case 1:
                    touchUp(x, y, eventTime);
                    return;
                case 2:
                    touchMove(x, y, eventTime);
                    return;
                default:
                    return;
            }
        }
    }

    public void update(float elapsedTime) {
        if (!this.isStoped) {
            updateInternal(elapsedTime);
        }
    }

    public void finish() {
    }

    /* access modifiers changed from: protected */
    public void updateInternal(float elapsedTime) {
    }

    /* access modifiers changed from: protected */
    public void touchDown(int x, int y) {
    }

    /* access modifiers changed from: protected */
    public void touchMove(int x, int y) {
    }

    /* access modifiers changed from: protected */
    public void touchUp(int x, int y) {
    }

    /* access modifiers changed from: protected */
    public void touchDown(int x, int y, long eventTime) {
    }

    /* access modifiers changed from: protected */
    public void touchMove(int x, int y, long eventTime) {
    }

    /* access modifiers changed from: protected */
    public void touchUp(int x, int y, long eventTime) {
    }
}
