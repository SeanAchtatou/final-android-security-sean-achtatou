package com.camelgames.framework.math;

public class Vector3 {
    public float X;
    public float Y;
    public float Z;

    public Vector3() {
    }

    public Vector3(float x, float y, float z) {
        this.X = x;
        this.Y = y;
        this.Z = z;
    }

    public Vector3(float value) {
        this.Z = value;
        this.Y = value;
        this.X = value;
    }

    public Vector3(Vector2 value, float z) {
        this.X = value.X;
        this.Y = value.Y;
        this.Z = z;
    }

    public Vector3(Vector3 value) {
        this.X = value.X;
        this.Y = value.Y;
        this.Z = value.Z;
    }

    public boolean Equals(Vector3 other) {
        return this.X == other.X && this.Y == other.Y && this.Z == other.Z;
    }

    public float Length() {
        return (float) Math.sqrt((double) ((this.X * this.X) + (this.Y * this.Y) + (this.Z * this.Z)));
    }

    public float LengthSquared() {
        return (this.X * this.X) + (this.Y * this.Y) + (this.Z * this.Z);
    }

    public void Set(float v) {
        this.X = v;
        this.Y = v;
        this.Z = v;
    }

    public void Set(float x, float y, float z) {
        this.X = x;
        this.Y = y;
        this.Z = z;
    }

    public void Set(Vector3 v) {
        this.X = v.X;
        this.Y = v.Y;
        this.Z = v.Z;
    }

    public void Set(Vector2 v, float z) {
        this.X = v.X;
        this.Y = v.Y;
        this.Z = z;
    }

    public void Add(Vector3 v) {
        this.X += v.X;
        this.Y += v.Y;
        this.Z += v.Z;
    }

    public void Subtract(Vector3 v) {
        this.X -= v.X;
        this.Y -= v.Y;
        this.Z -= v.Z;
    }

    public void Multiply(Vector3 v) {
        this.X *= v.X;
        this.Y *= v.Y;
        this.Z *= v.Z;
    }

    public void Multiply(float scale) {
        this.X *= scale;
        this.Y *= scale;
        this.Z *= scale;
    }

    public static float Distance(Vector3 value1, Vector3 value2) {
        float num3 = value1.X - value2.X;
        float num2 = value1.Y - value2.Y;
        float num = value1.Z - value2.Z;
        return (float) Math.sqrt((double) ((num3 * num3) + (num2 * num2) + (num * num)));
    }

    public static float Distance(float x, float y, float z) {
        return (float) Math.sqrt((double) ((x * x) + (y * y) + (z * z)));
    }

    public static float DistanceSquared(Vector3 value1, Vector3 value2) {
        float num3 = value1.X - value2.X;
        float num2 = value1.Y - value2.Y;
        float num = value1.Z - value2.Z;
        return (num3 * num3) + (num2 * num2) + (num * num);
    }

    public static float DistanceSquared(float x, float y, float z) {
        return (x * x) + (y * y) + (z * z);
    }

    public static float Dot(Vector3 vector1, Vector3 vector2) {
        return (vector1.X * vector2.X) + (vector1.Y * vector2.Y) + (vector1.Z * vector2.Z);
    }

    public static void Normalize(Vector3 value, Vector3 result) {
        float num = 1.0f / ((float) Math.sqrt((double) (((value.X * value.X) + (value.Y * value.Y)) + (value.Z * value.Z))));
        result.X = value.X * num;
        result.Y = value.Y * num;
        result.Z = value.Z * num;
    }

    public static void Cross(Vector3 vector1, Vector3 vector2, Vector3 result) {
        float num3 = (vector1.Y * vector2.Z) - (vector1.Z * vector2.Y);
        float num2 = (vector1.Z * vector2.X) - (vector1.X * vector2.Z);
        result.X = num3;
        result.Y = num2;
        result.Z = (vector1.X * vector2.Y) - (vector1.Y * vector2.X);
    }

    public static void Reflect(Vector3 vector, Vector3 normal, Vector3 result) {
        float num = (vector.X * normal.X) + (vector.Y * normal.Y) + (vector.Z * normal.Z);
        result.X = vector.X - ((2.0f * num) * normal.X);
        result.Y = vector.Y - ((2.0f * num) * normal.Y);
        result.Z = vector.Z - ((2.0f * num) * normal.Z);
    }

    public static void Clamp(Vector3 value1, Vector3 min, Vector3 max, Vector3 result) {
        float x = value1.X;
        if (x > max.X) {
            x = max.X;
        }
        if (x < min.X) {
            x = min.X;
        }
        float y = value1.Y;
        if (y > max.Y) {
            y = max.Y;
        }
        if (y < min.Y) {
            y = min.Y;
        }
        float z = value1.Z;
        if (z > max.Z) {
            z = max.Z;
        }
        if (z < min.Z) {
            z = min.Z;
        }
        result.X = x;
        result.Y = y;
        result.Z = z;
    }

    public static void Lerp(Vector3 value1, Vector3 value2, float amount, Vector3 result) {
        result.X = value1.X + ((value2.X - value1.X) * amount);
        result.Y = value1.Y + ((value2.Y - value1.Y) * amount);
        result.Z = value1.Z + ((value2.Z - value1.Z) * amount);
    }

    public static void Barycentric(Vector3 value1, Vector3 value2, Vector3 value3, float amount1, float amount2, Vector3 result) {
        result.X = value1.X + ((value2.X - value1.X) * amount1) + ((value3.X - value1.X) * amount2);
        result.Y = value1.Y + ((value2.Y - value1.Y) * amount1) + ((value3.Y - value1.Y) * amount2);
        result.Z = value1.Z + ((value2.Z - value1.Z) * amount1) + ((value3.Z - value1.Z) * amount2);
    }

    public static void SmoothStep(Vector3 value1, Vector3 value2, float amount, Vector3 result) {
        if (amount > 1.0f) {
            amount = 1.0f;
        } else if (amount < 0.0f) {
            amount = 0.0f;
        }
        float amount2 = amount * amount * (3.0f - (2.0f * amount));
        result.X = value1.X + ((value2.X - value1.X) * amount2);
        result.Y = value1.Y + ((value2.Y - value1.Y) * amount2);
        result.Z = value1.Z + ((value2.Z - value1.Z) * amount2);
    }

    public static void CatmullRom(Vector3 value1, Vector3 value2, Vector3 value3, Vector3 value4, float amount, Vector3 result) {
        float num = amount * amount;
        float num2 = amount * num;
        result.X = ((value2.X * 2.0f) + (((-value1.X) + value3.X) * amount) + (((((value1.X * 2.0f) - (value2.X * 5.0f)) + (value3.X * 4.0f)) - value4.X) * num) + (((((-value1.X) + (value2.X * 3.0f)) - (value3.X * 3.0f)) + value4.X) * num2)) * 0.5f;
        result.Y = ((value2.Y * 2.0f) + (((-value1.Y) + value3.Y) * amount) + (((((value1.Y * 2.0f) - (value2.Y * 5.0f)) + (value3.Y * 4.0f)) - value4.Y) * num) + (((((-value1.Y) + (value2.Y * 3.0f)) - (value3.Y * 3.0f)) + value4.Y) * num2)) * 0.5f;
        result.Z = ((value2.Z * 2.0f) + (((-value1.Z) + value3.Z) * amount) + (((((value1.Z * 2.0f) - (value2.Z * 5.0f)) + (value3.Z * 4.0f)) - value4.Z) * num) + (((((-value1.Z) + (value2.Z * 3.0f)) - (value3.Z * 3.0f)) + value4.Z) * num2)) * 0.5f;
    }

    public static void Hermite(Vector3 value1, Vector3 tangent1, Vector3 value2, Vector3 tangent2, float amount, Vector3 result) {
        float num = amount * amount;
        float num2 = amount * num;
        float num6 = ((2.0f * num2) - (3.0f * num)) + 1.0f;
        float num5 = (-2.0f * num2) + (3.0f * num);
        float num4 = (num2 - (2.0f * num)) + amount;
        float num3 = num2 - num;
        result.X = (value1.X * num6) + (value2.X * num5) + (tangent1.X * num4) + (tangent2.X * num3);
        result.Y = (value1.Y * num6) + (value2.Y * num5) + (tangent1.Y * num4) + (tangent2.Y * num3);
        result.Z = (value1.Z * num6) + (value2.Z * num5) + (tangent1.Z * num4) + (tangent2.Z * num3);
    }

    public static void Transform(Vector3 position, Matrix4 matrix, Vector3 result) {
        float num3 = (position.X * matrix.M00) + (position.Y * matrix.M10) + (position.Z * matrix.M20) + matrix.M30;
        float num2 = (position.X * matrix.M01) + (position.Y * matrix.M11) + (position.Z * matrix.M21) + matrix.M31;
        result.X = num3;
        result.Y = num2;
        result.Z = (position.X * matrix.M02) + (position.Y * matrix.M12) + (position.Z * matrix.M22) + matrix.M32;
    }

    public static void InverseTransformShortcut(Vector3 position, Matrix4 matrix, Vector3 result) {
        float x = position.X - matrix.M30;
        float y = position.Y - matrix.M31;
        float z = position.Z - matrix.M32;
        result.X = (matrix.M00 * x) + (matrix.M01 * y) + (matrix.M02 * z);
        result.Y = (matrix.M10 * x) + (matrix.M11 * y) + (matrix.M12 * z);
        result.Z = (matrix.M20 * x) + (matrix.M21 * y) + (matrix.M22 * z);
    }

    /* JADX INFO: Multiple debug info for r4v1 float: [D('num5' float), D('num2' float)] */
    /* JADX INFO: Multiple debug info for r12v2 float: [D('num3' float), D('rotation' com.camelgames.framework.math.Quaternion)] */
    public static void Transform(Vector3 value, Quaternion rotation, Vector3 result) {
        float num12 = rotation.X + rotation.X;
        float num2 = rotation.Y + rotation.Y;
        float num = rotation.Z + rotation.Z;
        float num11 = rotation.W * num12;
        float num10 = rotation.W * num2;
        float num9 = rotation.W * num;
        float num8 = rotation.X * num12;
        float num7 = rotation.X * num2;
        float num6 = rotation.X * num;
        float num22 = num2 * rotation.Y;
        float num4 = rotation.Y * num;
        float num3 = rotation.Z * num;
        float num15 = (value.X * ((1.0f - num22) - num3)) + (value.Y * (num7 - num9)) + (value.Z * (num6 + num10));
        float num14 = (((1.0f - num8) - num3) * value.Y) + ((num7 + num9) * value.X) + (value.Z * (num4 - num11));
        float f = ((num6 - num10) * value.X) + ((num11 + num4) * value.Y);
        result.X = num15;
        result.Y = num14;
        result.Z = (value.Z * ((1.0f - num8) - num22)) + f;
    }

    public static void Negate(Vector3 value, Vector3 result) {
        result.X = -value.X;
        result.Y = -value.Y;
        result.Z = -value.Z;
    }

    public static void Add(Vector3 value1, Vector3 value2, Vector3 result) {
        result.X = value1.X + value2.X;
        result.Y = value1.Y + value2.Y;
        result.Z = value1.Z + value2.Z;
    }

    public static void Subtract(Vector3 value1, Vector3 value2, Vector3 result) {
        result.X = value1.X - value2.X;
        result.Y = value1.Y - value2.Y;
        result.Z = value1.Z - value2.Z;
    }

    public static void Multiply(Vector3 value1, Vector3 value2, Vector3 result) {
        result.X = value1.X * value2.X;
        result.Y = value1.Y * value2.Y;
        result.Z = value1.Z * value2.Z;
    }

    public static void Multiply(Vector3 value1, float value2, Vector3 result) {
        result.X = value1.X * value2;
        result.Y = value1.Y * value2;
        result.Z = value1.Z * value2;
    }

    public static void Divide(Vector3 value1, Vector3 value2, Vector3 result) {
        result.X = value1.X / value2.X;
        result.Y = value1.Y / value2.Y;
        result.Z = value1.Z / value2.Z;
    }

    public static void Divide(Vector3 value1, float value2, Vector3 result) {
        float num = 1.0f / value2;
        result.X = value1.X * num;
        result.Y = value1.Y * num;
        result.Z = value1.Z * num;
    }
}
