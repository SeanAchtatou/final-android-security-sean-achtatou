package com.camelgames.framework.math;

public class WeightRandom {
    private int[] minWeightIndexs;
    private int[] weights;

    public WeightRandom(int count) {
        this.weights = new int[count];
        this.minWeightIndexs = new int[count];
    }

    public void changeWeight(int columnIndex, int delta) {
        int[] iArr = this.weights;
        iArr[columnIndex] = iArr[columnIndex] + delta;
    }

    public int getRandom() {
        this.minWeightIndexs[0] = 0;
        int min = this.weights[0];
        int minCount = 1;
        for (int i = 1; i < this.weights.length; i++) {
            if (this.weights[i] < min) {
                min = this.weights[i];
                this.minWeightIndexs[0] = i;
                minCount = 1;
            } else if (this.weights[i] == min) {
                this.minWeightIndexs[minCount] = i;
                minCount++;
            }
        }
        return this.minWeightIndexs[MathUtils.randomInt(minCount)];
    }
}
