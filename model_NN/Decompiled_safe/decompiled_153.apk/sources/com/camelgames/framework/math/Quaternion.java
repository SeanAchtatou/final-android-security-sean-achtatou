package com.camelgames.framework.math;

public class Quaternion {
    public float W;
    public float X;
    public float Y;
    public float Z;

    public Quaternion() {
    }

    public Quaternion(float x, float y, float z, float w) {
        this.X = x;
        this.Y = y;
        this.Z = z;
        this.W = w;
    }

    public Quaternion(Vector3 vectorPart, float scalarPart) {
        this.X = vectorPart.X;
        this.Y = vectorPart.Y;
        this.Z = vectorPart.Z;
        this.W = scalarPart;
    }

    public void set(Quaternion other) {
        this.X = other.X;
        this.Y = other.Y;
        this.Z = other.Z;
        this.W = other.W;
    }

    public boolean Equals(Quaternion other) {
        return this.X == other.X && this.Y == other.Y && this.Z == other.Z && this.W == other.W;
    }

    public float LengthSquared() {
        return (this.X * this.X) + (this.Y * this.Y) + (this.Z * this.Z) + (this.W * this.W);
    }

    public float Length() {
        return (float) Math.sqrt((double) ((this.X * this.X) + (this.Y * this.Y) + (this.Z * this.Z) + (this.W * this.W)));
    }

    public static void Normalize(Quaternion quaternion, Quaternion result) {
        float num = 1.0f / ((float) Math.sqrt((double) ((((quaternion.X * quaternion.X) + (quaternion.Y * quaternion.Y)) + (quaternion.Z * quaternion.Z)) + (quaternion.W * quaternion.W))));
        result.X = quaternion.X * num;
        result.Y = quaternion.Y * num;
        result.Z = quaternion.Z * num;
        result.W = quaternion.W * num;
    }

    public static void Conjugate(Quaternion value, Quaternion result) {
        result.X = -value.X;
        result.Y = -value.Y;
        result.Z = -value.Z;
        result.W = value.W;
    }

    public static void Inverse(Quaternion quaternion, Quaternion result) {
        float num = 1.0f / ((((quaternion.X * quaternion.X) + (quaternion.Y * quaternion.Y)) + (quaternion.Z * quaternion.Z)) + (quaternion.W * quaternion.W));
        result.X = (-quaternion.X) * num;
        result.Y = (-quaternion.Y) * num;
        result.Z = (-quaternion.Z) * num;
        result.W = quaternion.W * num;
    }

    public static void CreateFromAxisAngle(Vector3 axis, float angle, Quaternion result) {
        float num2 = angle * 0.5f;
        float num = (float) Math.sin((double) num2);
        float num3 = (float) Math.cos((double) num2);
        result.X = axis.X * num;
        result.Y = axis.Y * num;
        result.Z = axis.Z * num;
        result.W = num3;
    }

    public static void CreateFromYawPitchRoll(Vector3 euler, Quaternion result) {
        CreateFromYawPitchRoll(euler.Z, euler.Y, euler.X, result);
    }

    public static void CreateFromYawPitchRoll(float yaw, float pitch, float roll, Quaternion result) {
        float r = roll * 0.5f;
        float sr = (float) Math.sin((double) r);
        float cr = (float) Math.cos((double) r);
        float p = pitch * 0.5f;
        float sp = (float) Math.sin((double) p);
        float cp = (float) Math.cos((double) p);
        float y = yaw * 0.5f;
        float sy = (float) Math.sin((double) y);
        float cy = (float) Math.cos((double) y);
        result.X = ((cy * cp) * sr) - ((sy * sp) * cr);
        result.Y = (cy * sp * cr) + (sy * cp * sr);
        result.Z = ((sy * cp) * cr) - ((cy * sp) * sr);
        result.W = (cy * cp * cr) + (sy * sp * sr);
        Normalize(result, result);
    }

    public static void CreateFromRotationMatrix(Matrix4 matrix, Quaternion result) {
        float num8 = matrix.M00 + matrix.M11 + matrix.M22;
        if (num8 > 0.0f) {
            float num = (float) Math.sqrt((double) (num8 + 1.0f));
            result.W = num * 0.5f;
            float num2 = 0.5f / num;
            result.X = (matrix.M12 - matrix.M21) * num2;
            result.Y = (matrix.M20 - matrix.M02) * num2;
            result.Z = (matrix.M01 - matrix.M10) * num2;
        } else if (matrix.M00 >= matrix.M11 && matrix.M00 >= matrix.M22) {
            float num7 = (float) Math.sqrt((double) (((matrix.M00 + 1.0f) - matrix.M11) - matrix.M22));
            float num4 = 0.5f / num7;
            result.X = 0.5f * num7;
            result.Y = (matrix.M01 + matrix.M10) * num4;
            result.Z = (matrix.M02 + matrix.M20) * num4;
            result.W = (matrix.M12 - matrix.M21) * num4;
        } else if (matrix.M11 > matrix.M22) {
            float num6 = (float) Math.sqrt((double) (((matrix.M11 + 1.0f) - matrix.M00) - matrix.M22));
            float num3 = 0.5f / num6;
            result.X = (matrix.M10 + matrix.M01) * num3;
            result.Y = 0.5f * num6;
            result.Z = (matrix.M21 + matrix.M12) * num3;
            result.W = (matrix.M20 - matrix.M02) * num3;
        } else {
            float num5 = (float) Math.sqrt((double) (((matrix.M22 + 1.0f) - matrix.M00) - matrix.M11));
            float num22 = 0.5f / num5;
            result.X = (matrix.M20 + matrix.M02) * num22;
            result.Y = (matrix.M21 + matrix.M12) * num22;
            result.Z = 0.5f * num5;
            result.W = (matrix.M01 - matrix.M10) * num22;
        }
    }

    public static float Dot(Quaternion quaternion1, Quaternion quaternion2) {
        return (quaternion1.X * quaternion2.X) + (quaternion1.Y * quaternion2.Y) + (quaternion1.Z * quaternion2.Z) + (quaternion1.W * quaternion2.W);
    }

    public static void Slerp(Quaternion quaternion1, Quaternion quaternion2, float amount, Quaternion result) {
        float num3;
        float num2;
        float num = amount;
        float num4 = (quaternion1.X * quaternion2.X) + (quaternion1.Y * quaternion2.Y) + (quaternion1.Z * quaternion2.Z) + (quaternion1.W * quaternion2.W);
        boolean flag = false;
        if (num4 < 0.0f) {
            flag = true;
            num4 = -num4;
        }
        if (num4 > 0.999999f) {
            num3 = 1.0f - num;
            num2 = flag ? -num : num;
        } else {
            float num5 = (float) Math.acos((double) num4);
            float num6 = (float) (1.0d / Math.sin((double) num5));
            num3 = ((float) Math.sin((double) ((1.0f - num) * num5))) * num6;
            num2 = flag ? ((float) (-Math.sin((double) (num * num5)))) * num6 : ((float) Math.sin((double) (num * num5))) * num6;
        }
        result.X = (quaternion1.X * num3) + (quaternion2.X * num2);
        result.Y = (quaternion1.Y * num3) + (quaternion2.Y * num2);
        result.Z = (quaternion1.Z * num3) + (quaternion2.Z * num2);
        result.W = (quaternion1.W * num3) + (quaternion2.W * num2);
    }

    public static void Lerp(Quaternion quaternion1, Quaternion quaternion2, float amount, Quaternion result) {
        float num = amount;
        float num2 = 1.0f - num;
        if ((quaternion1.X * quaternion2.X) + (quaternion1.Y * quaternion2.Y) + (quaternion1.Z * quaternion2.Z) + (quaternion1.W * quaternion2.W) >= 0.0f) {
            result.X = (quaternion1.X * num2) + (quaternion2.X * num);
            result.Y = (quaternion1.Y * num2) + (quaternion2.Y * num);
            result.Z = (quaternion1.Z * num2) + (quaternion2.Z * num);
            result.W = (quaternion1.W * num2) + (quaternion2.W * num);
        } else {
            result.X = (quaternion1.X * num2) - (quaternion2.X * num);
            result.Y = (quaternion1.Y * num2) - (quaternion2.Y * num);
            result.Z = (quaternion1.Z * num2) - (quaternion2.Z * num);
            result.W = (quaternion1.W * num2) - (quaternion2.W * num);
        }
        float num3 = 1.0f / ((float) Math.sqrt((double) ((((result.X * result.X) + (result.Y * result.Y)) + (result.Z * result.Z)) + (result.W * result.W))));
        result.X *= num3;
        result.Y *= num3;
        result.Z *= num3;
        result.W *= num3;
    }

    public static void Negate(Quaternion quaternion, Quaternion result) {
        result.X = -quaternion.X;
        result.Y = -quaternion.Y;
        result.Z = -quaternion.Z;
        result.W = -quaternion.W;
    }

    public static void Add(Quaternion quaternion1, Quaternion quaternion2, Quaternion result) {
        result.X = quaternion1.X + quaternion2.X;
        result.Y = quaternion1.Y + quaternion2.Y;
        result.Z = quaternion1.Z + quaternion2.Z;
        result.W = quaternion1.W + quaternion2.W;
    }

    public static void Subtract(Quaternion quaternion1, Quaternion quaternion2, Quaternion result) {
        result.X = quaternion1.X - quaternion2.X;
        result.Y = quaternion1.Y - quaternion2.Y;
        result.Z = quaternion1.Z - quaternion2.Z;
        result.W = quaternion1.W - quaternion2.W;
    }

    /* JADX INFO: Multiple debug info for r11v1 float: [D('num' float), D('quaternion1' com.camelgames.framework.math.Quaternion)] */
    /* JADX INFO: Multiple debug info for r12v4 float: [D('quaternion2' com.camelgames.framework.math.Quaternion), D('num10' float)] */
    public static void Multiply(Quaternion quaternion1, Quaternion quaternion2, Quaternion result) {
        float x = quaternion1.X;
        float y = quaternion1.Y;
        float z = quaternion1.Z;
        float w = quaternion1.W;
        float num4 = quaternion2.X;
        float num3 = quaternion2.Y;
        float num2 = quaternion2.Z;
        float num = quaternion2.W;
        float num11 = (z * num4) - (x * num2);
        float num10 = (x * num3) - (y * num4);
        result.X = ((y * num2) - (z * num3)) + (num4 * w) + (x * num);
        result.Y = num11 + (y * num) + (num3 * w);
        result.Z = num10 + (z * num) + (num2 * w);
        result.W = (num * w) - (((x * num4) + (y * num3)) + (z * num2));
    }

    public static void Multiply(Quaternion quaternion1, float scaleFactor, Quaternion result) {
        result.X = quaternion1.X * scaleFactor;
        result.Y = quaternion1.Y * scaleFactor;
        result.Z = quaternion1.Z * scaleFactor;
        result.W = quaternion1.W * scaleFactor;
    }

    /* JADX INFO: Multiple debug info for r11v6 float: [D('num5' float), D('num14' float)] */
    /* JADX INFO: Multiple debug info for r11v7 float: [D('num5' float), D('num' float)] */
    public static void Divide(Quaternion quaternion1, Quaternion quaternion2, Quaternion result) {
        float x = quaternion1.X;
        float y = quaternion1.Y;
        float z = quaternion1.Z;
        float w = quaternion1.W;
        float num14 = 1.0f / ((((quaternion2.X * quaternion2.X) + (quaternion2.Y * quaternion2.Y)) + (quaternion2.Z * quaternion2.Z)) + (quaternion2.W * quaternion2.W));
        float num4 = (-quaternion2.X) * num14;
        float num3 = (-quaternion2.Y) * num14;
        float num2 = (-quaternion2.Z) * num14;
        float num = num14 * quaternion2.W;
        float num12 = (z * num4) - (x * num2);
        float num11 = (x * num3) - (y * num4);
        result.X = ((y * num2) - (z * num3)) + (num4 * w) + (x * num);
        result.Y = num12 + (y * num) + (num3 * w);
        result.Z = num11 + (z * num) + (num2 * w);
        result.W = (num * w) - (((x * num4) + (y * num3)) + (z * num2));
    }

    public static Quaternion createIndentity() {
        return new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
    }
}
