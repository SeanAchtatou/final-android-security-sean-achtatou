package com.camelgames.framework.math;

public class Point2 {
    public int X;
    public int Y;

    public Point2() {
    }

    public Point2(Point2 p2) {
        this.X = p2.X;
        this.Y = p2.Y;
    }

    public static Point2 create(String str) {
        Point2 v = new Point2();
        v.set(str);
        return v;
    }

    public String toString() {
        return String.valueOf(this.X) + "," + this.Y;
    }

    public void set(String str) {
        if (str != null) {
            int splitIndex = str.indexOf(44);
            this.X = Integer.parseInt(str.substring(0, splitIndex));
            this.Y = Integer.parseInt(str.substring(splitIndex + 1, str.length()));
        }
    }
}
