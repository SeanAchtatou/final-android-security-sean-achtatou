package com.camelgames.framework.math;

public class Rect {
    public float height;
    public float left;
    public float top;
    public float width;

    public Rect() {
    }

    public Rect(float left2, float top2, float width2, float height2) {
        this.left = left2;
        this.top = top2;
        this.width = width2;
        this.height = height2;
    }

    public boolean isInside(float x, float y) {
        return this.left <= x && x <= this.left + this.width && this.top <= y && y <= this.top + this.height;
    }

    public boolean isInside(float x, float y, float radius) {
        return MathUtils.length(x - this.left, y - this.top) <= radius || MathUtils.length(x - this.left, (y - this.top) - this.height) <= radius || MathUtils.length((x - this.left) - this.width, y - this.top) <= radius || MathUtils.length((x - this.left) - this.width, (y - this.top) - this.height) <= radius;
    }

    public float getX() {
        return this.left + (0.5f * this.width);
    }

    public float getY() {
        return this.top + (0.5f * this.height);
    }
}
