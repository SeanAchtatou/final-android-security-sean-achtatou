package com.camelgames.framework.math;

public class Matrix4 {
    public float M00;
    public float M01;
    public float M02;
    public float M03;
    public float M10;
    public float M11;
    public float M12;
    public float M13;
    public float M20;
    public float M21;
    public float M22;
    public float M23;
    public float M30;
    public float M31;
    public float M32;
    public float M33;

    public void getTranslation(Vector3 vector) {
        vector.X = this.M30;
        vector.Y = this.M31;
        vector.Z = this.M32;
    }

    public void setTransltation(Vector3 value) {
        this.M30 = value.X;
        this.M31 = value.Y;
        this.M32 = value.Z;
    }

    public Matrix4() {
    }

    public Matrix4(float m11, float m12, float m13, float m14, float m21, float m22, float m23, float m24, float m31, float m32, float m33, float m34, float m41, float m42, float m43, float m44) {
        this.M00 = m11;
        this.M01 = m12;
        this.M02 = m13;
        this.M03 = m14;
        this.M10 = m21;
        this.M11 = m22;
        this.M12 = m23;
        this.M13 = m24;
        this.M20 = m31;
        this.M21 = m32;
        this.M22 = m33;
        this.M23 = m34;
        this.M30 = m41;
        this.M31 = m42;
        this.M32 = m43;
        this.M33 = m44;
    }

    public static Matrix4 CreateTranslation(Vector3 position, Matrix4 matrix) {
        matrix.M00 = 1.0f;
        matrix.M01 = 0.0f;
        matrix.M02 = 0.0f;
        matrix.M03 = 0.0f;
        matrix.M10 = 0.0f;
        matrix.M11 = 1.0f;
        matrix.M12 = 0.0f;
        matrix.M13 = 0.0f;
        matrix.M20 = 0.0f;
        matrix.M21 = 0.0f;
        matrix.M22 = 1.0f;
        matrix.M23 = 0.0f;
        matrix.M30 = position.X;
        matrix.M31 = position.Y;
        matrix.M32 = position.Z;
        matrix.M33 = 1.0f;
        return matrix;
    }

    public static Matrix4 CreateTranslation(float xPosition, float yPosition, float zPosition, Matrix4 matrix) {
        matrix.M00 = 1.0f;
        matrix.M01 = 0.0f;
        matrix.M02 = 0.0f;
        matrix.M03 = 0.0f;
        matrix.M10 = 0.0f;
        matrix.M11 = 1.0f;
        matrix.M12 = 0.0f;
        matrix.M13 = 0.0f;
        matrix.M20 = 0.0f;
        matrix.M21 = 0.0f;
        matrix.M22 = 1.0f;
        matrix.M23 = 0.0f;
        matrix.M30 = xPosition;
        matrix.M31 = yPosition;
        matrix.M32 = zPosition;
        matrix.M33 = 1.0f;
        return matrix;
    }

    public static Matrix4 CreateScale(float xScale, float yScale, float zScale, Matrix4 matrix) {
        matrix.M00 = xScale;
        matrix.M01 = 0.0f;
        matrix.M02 = 0.0f;
        matrix.M03 = 0.0f;
        matrix.M10 = 0.0f;
        matrix.M11 = yScale;
        matrix.M12 = 0.0f;
        matrix.M13 = 0.0f;
        matrix.M20 = 0.0f;
        matrix.M21 = 0.0f;
        matrix.M22 = zScale;
        matrix.M23 = 0.0f;
        matrix.M30 = 0.0f;
        matrix.M31 = 0.0f;
        matrix.M32 = 0.0f;
        matrix.M33 = 1.0f;
        return matrix;
    }

    public static Matrix4 CreateScale(Vector3 scales, Matrix4 matrix) {
        float x = scales.X;
        float y = scales.Y;
        float z = scales.Z;
        matrix.M00 = x;
        matrix.M01 = 0.0f;
        matrix.M02 = 0.0f;
        matrix.M03 = 0.0f;
        matrix.M10 = 0.0f;
        matrix.M11 = y;
        matrix.M12 = 0.0f;
        matrix.M13 = 0.0f;
        matrix.M20 = 0.0f;
        matrix.M21 = 0.0f;
        matrix.M22 = z;
        matrix.M23 = 0.0f;
        matrix.M30 = 0.0f;
        matrix.M31 = 0.0f;
        matrix.M32 = 0.0f;
        matrix.M33 = 1.0f;
        return matrix;
    }

    public static Matrix4 CreateRotationX(float radians, Matrix4 matrix) {
        float num2 = (float) Math.cos((double) radians);
        float num = (float) Math.sin((double) radians);
        matrix.M00 = 1.0f;
        matrix.M01 = 0.0f;
        matrix.M02 = 0.0f;
        matrix.M03 = 0.0f;
        matrix.M10 = 0.0f;
        matrix.M11 = num2;
        matrix.M12 = num;
        matrix.M13 = 0.0f;
        matrix.M20 = 0.0f;
        matrix.M21 = -num;
        matrix.M22 = num2;
        matrix.M23 = 0.0f;
        matrix.M30 = 0.0f;
        matrix.M31 = 0.0f;
        matrix.M32 = 0.0f;
        matrix.M33 = 1.0f;
        return matrix;
    }

    public static Matrix4 CreateRotationY(float radians, Matrix4 result) {
        float num2 = (float) Math.cos((double) radians);
        float num = (float) Math.sin((double) radians);
        result.M00 = num2;
        result.M01 = 0.0f;
        result.M02 = -num;
        result.M03 = 0.0f;
        result.M10 = 0.0f;
        result.M11 = 1.0f;
        result.M12 = 0.0f;
        result.M13 = 0.0f;
        result.M20 = num;
        result.M21 = 0.0f;
        result.M22 = num2;
        result.M23 = 0.0f;
        result.M30 = 0.0f;
        result.M31 = 0.0f;
        result.M32 = 0.0f;
        result.M33 = 1.0f;
        return result;
    }

    public static Matrix4 CreateRotationZ(float radians, Matrix4 result) {
        float num2 = (float) Math.cos((double) radians);
        float num = (float) Math.sin((double) radians);
        result.M00 = num2;
        result.M01 = num;
        result.M02 = 0.0f;
        result.M03 = 0.0f;
        result.M10 = -num;
        result.M11 = num2;
        result.M12 = 0.0f;
        result.M13 = 0.0f;
        result.M20 = 0.0f;
        result.M21 = 0.0f;
        result.M22 = 1.0f;
        result.M23 = 0.0f;
        result.M30 = 0.0f;
        result.M31 = 0.0f;
        result.M32 = 0.0f;
        result.M33 = 1.0f;
        return result;
    }

    /* JADX INFO: Multiple debug info for r11v1 float: [D('angle' float), D('num10' float)] */
    public static Matrix4 CreateFromAxisAngle(Vector3 axis, float angle, Matrix4 result) {
        float x = axis.X;
        float y = axis.Y;
        float z = axis.Z;
        float num2 = (float) Math.sin((double) angle);
        float num = (float) Math.cos((double) angle);
        float num11 = x * x;
        float num10 = y * y;
        float num9 = z * z;
        float num8 = x * y;
        float num7 = x * z;
        float num6 = y * z;
        result.M00 = num11 + ((1.0f - num11) * num);
        result.M01 = (num8 - (num * num8)) + (num2 * z);
        result.M02 = (num7 - (num * num7)) - (num2 * y);
        result.M03 = 0.0f;
        result.M10 = (num8 - (num * num8)) - (num2 * z);
        result.M11 = num10 + ((1.0f - num10) * num);
        result.M12 = (num6 - (num * num6)) + (num2 * x);
        result.M13 = 0.0f;
        result.M20 = (num7 - (num * num7)) + (num2 * y);
        result.M21 = (num6 - (num * num6)) - (num2 * x);
        result.M22 = (num * (1.0f - num9)) + num9;
        result.M23 = 0.0f;
        result.M30 = 0.0f;
        result.M31 = 0.0f;
        result.M32 = 0.0f;
        result.M33 = 1.0f;
        return result;
    }

    public static Matrix4 CreateFromQuaternion(Quaternion quaternion, Matrix4 result) {
        float xx = quaternion.X * quaternion.X;
        float yy = quaternion.Y * quaternion.Y;
        float zz = quaternion.Z * quaternion.Z;
        float xy = quaternion.X * quaternion.Y;
        float zw = quaternion.Z * quaternion.W;
        float xz = quaternion.Z * quaternion.X;
        float yw = quaternion.Y * quaternion.W;
        float yz = quaternion.Y * quaternion.Z;
        float xw = quaternion.X * quaternion.W;
        result.M00 = 1.0f - ((yy + zz) * 2.0f);
        result.M01 = (xy + zw) * 2.0f;
        result.M02 = (xz - yw) * 2.0f;
        result.M03 = 0.0f;
        result.M10 = (xy - zw) * 2.0f;
        result.M11 = 1.0f - ((zz + xx) * 2.0f);
        result.M12 = (yz + xw) * 2.0f;
        result.M13 = 0.0f;
        result.M20 = (xz + yw) * 2.0f;
        result.M21 = (yz - xw) * 2.0f;
        result.M22 = 1.0f - ((yy + xx) * 2.0f);
        result.M23 = 0.0f;
        result.M30 = 0.0f;
        result.M31 = 0.0f;
        result.M32 = 0.0f;
        result.M33 = 1.0f;
        return result;
    }

    public static Matrix4 CreateFromYawPitchRoll(Vector3 euler, Matrix4 result) {
        Quaternion quaternion = new Quaternion();
        Quaternion.CreateFromYawPitchRoll(euler, quaternion);
        return CreateFromQuaternion(quaternion, result);
    }

    /* JADX INFO: Multiple debug info for r3v4 float: [D('num13' float), D('num11' float)] */
    /* JADX INFO: Multiple debug info for r25v2 float: [D('num12' float), D('rotation' com.camelgames.framework.math.Quaternion)] */
    /* JADX INFO: Multiple debug info for r9v1 float: [D('num19' float), D('num3' float)] */
    /* JADX INFO: Multiple debug info for r2v8 float: [D('num2' float), D('num10' float)] */
    /* JADX INFO: Multiple debug info for r25v5 float: [D('num12' float), D('num' float)] */
    /* JADX INFO: Multiple debug info for r10v1 float: [D('num20' float), D('num30' float)] */
    /* JADX INFO: Multiple debug info for r8v1 float: [D('num18' float), D('num29' float)] */
    /* JADX INFO: Multiple debug info for r7v1 float: [D('num17' float), D('num28' float)] */
    /* JADX INFO: Multiple debug info for r6v1 float: [D('num16' float), D('num27' float)] */
    /* JADX INFO: Multiple debug info for r5v6 float: [D('num15' float), D('num26' float)] */
    /* JADX INFO: Multiple debug info for r25v7 float: [D('num' float), D('num23' float)] */
    /* JADX INFO: Multiple debug info for r24v1 float: [D('value' com.camelgames.framework.math.Matrix4), D('num22' float)] */
    public static Matrix4 Transform(Matrix4 value, Quaternion rotation, Matrix4 result) {
        float num21 = rotation.X + rotation.X;
        float num11 = rotation.Y + rotation.Y;
        float num10 = rotation.Z + rotation.Z;
        float num20 = rotation.W * num21;
        float num19 = rotation.W * num11;
        float num18 = rotation.W * num10;
        float num17 = rotation.X * num21;
        float num16 = rotation.X * num11;
        float num15 = rotation.X * num10;
        float num14 = rotation.Y * num11;
        float num112 = rotation.Y * num10;
        float num12 = rotation.Z * num10;
        float num9 = (1.0f - num14) - num12;
        float num8 = num16 - num18;
        float num7 = num15 + num19;
        float num6 = num16 + num18;
        float num5 = (1.0f - num17) - num12;
        float num4 = num112 - num20;
        float num3 = num15 - num19;
        float num102 = num112 + num20;
        float num = (1.0f - num17) - num14;
        float num37 = (value.M00 * num9) + (value.M01 * num8) + (value.M02 * num7);
        float num36 = (value.M00 * num6) + (value.M01 * num5) + (value.M02 * num4);
        float num35 = (value.M00 * num3) + (value.M01 * num102) + (value.M02 * num);
        float num34 = value.M03;
        float num33 = (value.M10 * num9) + (value.M11 * num8) + (value.M12 * num7);
        float num32 = (value.M10 * num6) + (value.M11 * num5) + (value.M12 * num4);
        float num31 = (value.M10 * num3) + (value.M11 * num102) + (value.M12 * num);
        float num30 = value.M13;
        float num29 = (value.M20 * num9) + (value.M21 * num8) + (value.M22 * num7);
        float num28 = (value.M20 * num6) + (value.M21 * num5) + (value.M22 * num4);
        float num27 = (value.M20 * num3) + (value.M21 * num102) + (value.M22 * num);
        float num26 = value.M23;
        float num25 = (value.M32 * num7) + (value.M30 * num9) + (value.M31 * num8);
        float num24 = (value.M30 * num6) + (num5 * value.M31) + (num4 * value.M32);
        float num2 = (num102 * value.M31) + (num3 * value.M30);
        float num22 = value.M33;
        result.M00 = num37;
        result.M01 = num36;
        result.M02 = num35;
        result.M03 = num34;
        result.M10 = num33;
        result.M11 = num32;
        result.M12 = num31;
        result.M13 = num30;
        result.M20 = num29;
        result.M21 = num28;
        result.M22 = num27;
        result.M23 = num26;
        result.M30 = num25;
        result.M31 = num24;
        result.M32 = (num * value.M32) + num2;
        result.M33 = num22;
        return result;
    }

    public boolean Equals(Matrix4 other) {
        return this.M00 == other.M00 && this.M11 == other.M11 && this.M22 == other.M22 && this.M33 == other.M33 && this.M01 == other.M01 && this.M02 == other.M02 && this.M03 == other.M03 && this.M10 == other.M10 && this.M12 == other.M12 && this.M13 == other.M13 && this.M20 == other.M20 && this.M21 == other.M21 && this.M23 == other.M23 && this.M30 == other.M30 && this.M31 == other.M31 && this.M32 == other.M32;
    }

    public void Set(Matrix4 matrix) {
        this.M00 = matrix.M00;
        this.M01 = matrix.M01;
        this.M02 = matrix.M02;
        this.M03 = matrix.M03;
        this.M10 = matrix.M10;
        this.M11 = matrix.M11;
        this.M12 = matrix.M12;
        this.M13 = matrix.M13;
        this.M20 = matrix.M20;
        this.M21 = matrix.M21;
        this.M22 = matrix.M22;
        this.M23 = matrix.M23;
        this.M30 = matrix.M30;
        this.M31 = matrix.M31;
        this.M32 = matrix.M32;
        this.M33 = matrix.M33;
    }

    public void Set(Quaternion quaternion) {
        CreateFromQuaternion(quaternion, this);
    }

    public float Determinant() {
        float num22 = this.M00;
        float num21 = this.M01;
        float num20 = this.M02;
        float num19 = this.M03;
        float num12 = this.M10;
        float num11 = this.M11;
        float num10 = this.M12;
        float num9 = this.M13;
        float num8 = this.M20;
        float num7 = this.M21;
        float num6 = this.M22;
        float num5 = this.M23;
        float num4 = this.M30;
        float num3 = this.M31;
        float num2 = this.M32;
        float num = this.M33;
        float num18 = (num6 * num) - (num5 * num2);
        float num17 = (num7 * num) - (num5 * num3);
        float num16 = (num7 * num2) - (num6 * num3);
        float num15 = (num8 * num) - (num5 * num4);
        float num14 = (num8 * num2) - (num6 * num4);
        float num13 = (num8 * num3) - (num7 * num4);
        return ((((((num11 * num18) - (num10 * num17)) + (num9 * num16)) * num22) - ((((num12 * num18) - (num10 * num15)) + (num9 * num14)) * num21)) + ((((num12 * num17) - (num11 * num15)) + (num9 * num13)) * num20)) - ((((num12 * num16) - (num11 * num14)) + (num10 * num13)) * num19);
    }

    /* JADX INFO: Multiple debug info for r30v18 float: [D('num' float), D('matrix' com.camelgames.framework.math.Matrix4)] */
    /* JADX INFO: Multiple debug info for r14v1 float: [D('num35' float), D('num21' float)] */
    /* JADX INFO: Multiple debug info for r3v2 float: [D('num31' float), D('num11' float)] */
    /* JADX INFO: Multiple debug info for r2v4 float: [D('num30' float), D('num10' float)] */
    /* JADX INFO: Multiple debug info for r11v7 float: [D('num33' float), D('num29' float)] */
    /* JADX INFO: Multiple debug info for r10v15 float: [D('num28' float), D('num32' float)] */
    public static Matrix4 Invert(Matrix4 matrix, Matrix4 result) {
        float num5 = matrix.M00;
        float num4 = matrix.M01;
        float num3 = matrix.M02;
        float num2 = matrix.M03;
        float num9 = matrix.M10;
        float num8 = matrix.M11;
        float num7 = matrix.M12;
        float num6 = matrix.M13;
        float num17 = matrix.M20;
        float num16 = matrix.M21;
        float num15 = matrix.M22;
        float num14 = matrix.M23;
        float num13 = matrix.M30;
        float num12 = matrix.M31;
        float num11 = matrix.M32;
        float num10 = matrix.M33;
        float num23 = (num15 * num10) - (num14 * num11);
        float num22 = (num16 * num10) - (num14 * num12);
        float num21 = (num16 * num11) - (num15 * num12);
        float num20 = (num17 * num10) - (num14 * num13);
        float num19 = (num17 * num11) - (num15 * num13);
        float num18 = (num17 * num12) - (num16 * num13);
        float num39 = ((num8 * num23) - (num7 * num22)) + (num6 * num21);
        float num38 = -(((num9 * num23) - (num7 * num20)) + (num6 * num19));
        float num37 = ((num9 * num22) - (num8 * num20)) + (num6 * num18);
        float num36 = -(((num9 * num21) - (num8 * num19)) + (num7 * num18));
        float num = 1.0f / ((((num5 * num39) + (num4 * num38)) + (num3 * num37)) + (num2 * num36));
        result.M00 = num39 * num;
        result.M10 = num38 * num;
        result.M20 = num37 * num;
        result.M30 = num36 * num;
        result.M01 = (-(((num4 * num23) - (num3 * num22)) + (num2 * num21))) * num;
        result.M11 = (((num23 * num5) - (num3 * num20)) + (num2 * num19)) * num;
        result.M21 = (-(((num22 * num5) - (num20 * num4)) + (num2 * num18))) * num;
        result.M31 = ((num18 * num3) + ((num5 * num21) - (num19 * num4))) * num;
        float num212 = (num7 * num10) - (num6 * num11);
        float num34 = (num8 * num10) - (num6 * num12);
        float num33 = (num8 * num11) - (num7 * num12);
        float num32 = (num10 * num9) - (num6 * num13);
        float num112 = (num9 * num11) - (num7 * num13);
        float num102 = (num9 * num12) - (num8 * num13);
        result.M02 = (((num4 * num212) - (num3 * num34)) + (num2 * num33)) * num;
        result.M12 = (-(((num5 * num212) - (num3 * num32)) + (num2 * num112))) * num;
        result.M22 = (((num5 * num34) - (num4 * num32)) + (num2 * num102)) * num;
        result.M32 = (-((num102 * num3) + ((num5 * num33) - (num112 * num4)))) * num;
        float num29 = (num7 * num14) - (num6 * num15);
        float num322 = (num8 * num14) - (num6 * num16);
        float num27 = (num8 * num15) - (num7 * num16);
        float num26 = (num9 * num14) - (num6 * num17);
        float num25 = (num9 * num15) - (num7 * num17);
        float num24 = (num9 * num16) - (num8 * num17);
        result.M03 = (-(((num4 * num29) - (num3 * num322)) + (num2 * num27))) * num;
        result.M13 = (((num5 * num29) - (num3 * num26)) + (num2 * num25)) * num;
        result.M23 = (-(((num5 * num322) - (num26 * num4)) + (num2 * num24))) * num;
        result.M33 = num * ((num24 * num3) + ((num5 * num27) - (num25 * num4)));
        return result;
    }

    public static Matrix4 Lerp(Matrix4 matrix1, Matrix4 matrix2, float amount, Matrix4 result) {
        result.M00 = matrix1.M00 + ((matrix2.M00 - matrix1.M00) * amount);
        result.M01 = matrix1.M01 + ((matrix2.M01 - matrix1.M01) * amount);
        result.M02 = matrix1.M02 + ((matrix2.M02 - matrix1.M02) * amount);
        result.M03 = matrix1.M03 + ((matrix2.M03 - matrix1.M03) * amount);
        result.M10 = matrix1.M10 + ((matrix2.M10 - matrix1.M10) * amount);
        result.M11 = matrix1.M11 + ((matrix2.M11 - matrix1.M11) * amount);
        result.M12 = matrix1.M12 + ((matrix2.M12 - matrix1.M12) * amount);
        result.M13 = matrix1.M13 + ((matrix2.M13 - matrix1.M13) * amount);
        result.M20 = matrix1.M20 + ((matrix2.M20 - matrix1.M20) * amount);
        result.M21 = matrix1.M21 + ((matrix2.M21 - matrix1.M21) * amount);
        result.M22 = matrix1.M22 + ((matrix2.M22 - matrix1.M22) * amount);
        result.M23 = matrix1.M23 + ((matrix2.M23 - matrix1.M23) * amount);
        result.M30 = matrix1.M30 + ((matrix2.M30 - matrix1.M30) * amount);
        result.M31 = matrix1.M31 + ((matrix2.M31 - matrix1.M31) * amount);
        result.M32 = matrix1.M32 + ((matrix2.M32 - matrix1.M32) * amount);
        result.M33 = matrix1.M33 + ((matrix2.M33 - matrix1.M33) * amount);
        return result;
    }

    public static Matrix4 Negate(Matrix4 matrix, Matrix4 result) {
        result.M00 = -matrix.M00;
        result.M01 = -matrix.M01;
        result.M02 = -matrix.M02;
        result.M03 = -matrix.M03;
        result.M10 = -matrix.M10;
        result.M11 = -matrix.M11;
        result.M12 = -matrix.M12;
        result.M13 = -matrix.M13;
        result.M20 = -matrix.M20;
        result.M21 = -matrix.M21;
        result.M22 = -matrix.M22;
        result.M23 = -matrix.M23;
        result.M30 = -matrix.M30;
        result.M31 = -matrix.M31;
        result.M32 = -matrix.M32;
        result.M33 = -matrix.M33;
        return result;
    }

    public static Matrix4 Add(Matrix4 matrix1, Matrix4 matrix2, Matrix4 result) {
        result.M00 = matrix1.M00 + matrix2.M00;
        result.M01 = matrix1.M01 + matrix2.M01;
        result.M02 = matrix1.M02 + matrix2.M02;
        result.M03 = matrix1.M03 + matrix2.M03;
        result.M10 = matrix1.M10 + matrix2.M10;
        result.M11 = matrix1.M11 + matrix2.M11;
        result.M12 = matrix1.M12 + matrix2.M12;
        result.M13 = matrix1.M13 + matrix2.M13;
        result.M20 = matrix1.M20 + matrix2.M20;
        result.M21 = matrix1.M21 + matrix2.M21;
        result.M22 = matrix1.M22 + matrix2.M22;
        result.M23 = matrix1.M23 + matrix2.M23;
        result.M30 = matrix1.M30 + matrix2.M30;
        result.M31 = matrix1.M31 + matrix2.M31;
        result.M32 = matrix1.M32 + matrix2.M32;
        result.M33 = matrix1.M33 + matrix2.M33;
        return result;
    }

    public static Matrix4 Subtract(Matrix4 matrix1, Matrix4 matrix2, Matrix4 result) {
        result.M00 = matrix1.M00 - matrix2.M00;
        result.M01 = matrix1.M01 - matrix2.M01;
        result.M02 = matrix1.M02 - matrix2.M02;
        result.M03 = matrix1.M03 - matrix2.M03;
        result.M10 = matrix1.M10 - matrix2.M10;
        result.M11 = matrix1.M11 - matrix2.M11;
        result.M12 = matrix1.M12 - matrix2.M12;
        result.M13 = matrix1.M13 - matrix2.M13;
        result.M20 = matrix1.M20 - matrix2.M20;
        result.M21 = matrix1.M21 - matrix2.M21;
        result.M22 = matrix1.M22 - matrix2.M22;
        result.M23 = matrix1.M23 - matrix2.M23;
        result.M30 = matrix1.M30 - matrix2.M30;
        result.M31 = matrix1.M31 - matrix2.M31;
        result.M32 = matrix1.M32 - matrix2.M32;
        result.M33 = matrix1.M33 - matrix2.M33;
        return result;
    }

    public static Matrix4 Multiply(Matrix4 matrix1, Matrix4 matrix2, Matrix4 result) {
        float num16 = (matrix1.M00 * matrix2.M00) + (matrix1.M01 * matrix2.M10) + (matrix1.M02 * matrix2.M20) + (matrix1.M03 * matrix2.M30);
        float num15 = (matrix1.M00 * matrix2.M01) + (matrix1.M01 * matrix2.M11) + (matrix1.M02 * matrix2.M21) + (matrix1.M03 * matrix2.M31);
        float num14 = (matrix1.M00 * matrix2.M02) + (matrix1.M01 * matrix2.M12) + (matrix1.M02 * matrix2.M22) + (matrix1.M03 * matrix2.M32);
        float num13 = (matrix1.M00 * matrix2.M03) + (matrix1.M01 * matrix2.M13) + (matrix1.M02 * matrix2.M23) + (matrix1.M03 * matrix2.M33);
        float num12 = (matrix1.M10 * matrix2.M00) + (matrix1.M11 * matrix2.M10) + (matrix1.M12 * matrix2.M20) + (matrix1.M13 * matrix2.M30);
        float num11 = (matrix1.M13 * matrix2.M31) + (matrix1.M10 * matrix2.M01) + (matrix1.M11 * matrix2.M11) + (matrix1.M12 * matrix2.M21);
        float num10 = (matrix1.M10 * matrix2.M02) + (matrix1.M11 * matrix2.M12) + (matrix1.M12 * matrix2.M22) + (matrix1.M13 * matrix2.M32);
        float num9 = (matrix1.M10 * matrix2.M03) + (matrix1.M11 * matrix2.M13) + (matrix1.M12 * matrix2.M23) + (matrix1.M13 * matrix2.M33);
        float num8 = (matrix1.M20 * matrix2.M00) + (matrix1.M21 * matrix2.M10) + (matrix1.M22 * matrix2.M20) + (matrix1.M23 * matrix2.M30);
        float num7 = (matrix1.M20 * matrix2.M01) + (matrix1.M21 * matrix2.M11) + (matrix1.M22 * matrix2.M21) + (matrix1.M23 * matrix2.M31);
        float num6 = (matrix1.M20 * matrix2.M02) + (matrix1.M21 * matrix2.M12) + (matrix1.M22 * matrix2.M22) + (matrix1.M23 * matrix2.M32);
        float num5 = (matrix1.M20 * matrix2.M03) + (matrix1.M21 * matrix2.M13) + (matrix1.M22 * matrix2.M23) + (matrix1.M23 * matrix2.M33);
        float num4 = (matrix1.M30 * matrix2.M00) + (matrix1.M31 * matrix2.M10) + (matrix1.M32 * matrix2.M20) + (matrix1.M33 * matrix2.M30);
        float num3 = (matrix1.M33 * matrix2.M31) + (matrix1.M30 * matrix2.M01) + (matrix1.M31 * matrix2.M11) + (matrix1.M32 * matrix2.M21);
        float num2 = (matrix1.M30 * matrix2.M02) + (matrix1.M31 * matrix2.M12) + (matrix1.M32 * matrix2.M22) + (matrix1.M33 * matrix2.M32);
        float f = matrix1.M33 * matrix2.M33;
        result.M00 = num16;
        result.M01 = num15;
        result.M02 = num14;
        result.M03 = num13;
        result.M10 = num12;
        result.M11 = num11;
        result.M12 = num10;
        result.M13 = num9;
        result.M20 = num8;
        result.M21 = num7;
        result.M22 = num6;
        result.M23 = num5;
        result.M30 = num4;
        result.M31 = num3;
        result.M32 = num2;
        result.M33 = f + (matrix1.M30 * matrix2.M03) + (matrix1.M31 * matrix2.M13) + (matrix1.M32 * matrix2.M23);
        return result;
    }

    public static Matrix4 Multiply(Matrix4 matrix1, float scaleFactor, Matrix4 result) {
        float num = scaleFactor;
        result.M00 = matrix1.M00 * num;
        result.M01 = matrix1.M01 * num;
        result.M02 = matrix1.M02 * num;
        result.M03 = matrix1.M03 * num;
        result.M10 = matrix1.M10 * num;
        result.M11 = matrix1.M11 * num;
        result.M12 = matrix1.M12 * num;
        result.M13 = matrix1.M13 * num;
        result.M20 = matrix1.M20 * num;
        result.M21 = matrix1.M21 * num;
        result.M22 = matrix1.M22 * num;
        result.M23 = matrix1.M23 * num;
        result.M30 = matrix1.M30 * num;
        result.M31 = matrix1.M31 * num;
        result.M32 = matrix1.M32 * num;
        result.M33 = matrix1.M33 * num;
        return result;
    }

    public static Matrix4 Divide(Matrix4 matrix1, Matrix4 matrix2, Matrix4 result) {
        result.M00 = matrix1.M00 / matrix2.M00;
        result.M01 = matrix1.M01 / matrix2.M01;
        result.M02 = matrix1.M02 / matrix2.M02;
        result.M03 = matrix1.M03 / matrix2.M03;
        result.M10 = matrix1.M10 / matrix2.M10;
        result.M11 = matrix1.M11 / matrix2.M11;
        result.M12 = matrix1.M12 / matrix2.M12;
        result.M13 = matrix1.M13 / matrix2.M13;
        result.M20 = matrix1.M20 / matrix2.M20;
        result.M21 = matrix1.M21 / matrix2.M21;
        result.M22 = matrix1.M22 / matrix2.M22;
        result.M23 = matrix1.M23 / matrix2.M23;
        result.M30 = matrix1.M30 / matrix2.M30;
        result.M31 = matrix1.M31 / matrix2.M31;
        result.M32 = matrix1.M32 / matrix2.M32;
        result.M33 = matrix1.M33 / matrix2.M33;
        return result;
    }

    public static Matrix4 Divide(Matrix4 matrix1, float divider, Matrix4 result) {
        float num = 1.0f / divider;
        result.M00 = matrix1.M00 * num;
        result.M01 = matrix1.M01 * num;
        result.M02 = matrix1.M02 * num;
        result.M03 = matrix1.M03 * num;
        result.M10 = matrix1.M10 * num;
        result.M11 = matrix1.M11 * num;
        result.M12 = matrix1.M12 * num;
        result.M13 = matrix1.M13 * num;
        result.M20 = matrix1.M20 * num;
        result.M21 = matrix1.M21 * num;
        result.M22 = matrix1.M22 * num;
        result.M23 = matrix1.M23 * num;
        result.M30 = matrix1.M30 * num;
        result.M31 = matrix1.M31 * num;
        result.M32 = matrix1.M32 * num;
        result.M33 = matrix1.M33 * num;
        return result;
    }

    public static Matrix4 createIndentity() {
        return new Matrix4(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
    }
}
