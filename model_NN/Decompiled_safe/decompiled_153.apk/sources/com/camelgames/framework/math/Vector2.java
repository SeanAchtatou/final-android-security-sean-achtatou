package com.camelgames.framework.math;

public final class Vector2 {
    public float X;
    public float Y;

    public Vector2() {
    }

    public Vector2(float x, float y) {
        this.X = x;
        this.Y = y;
    }

    public Vector2(Vector2 v) {
        this.X = v.X;
        this.Y = v.Y;
    }

    public void set(float x, float y) {
        this.X = x;
        this.Y = y;
    }

    public void set(Vector2 v) {
        this.X = v.X;
        this.Y = v.Y;
    }

    public boolean isZero() {
        return this.X == 0.0f && this.Y == 0.0f;
    }

    public void setZero() {
        this.Y = 0.0f;
        this.X = 0.0f;
    }

    public void add(Vector2 v) {
        this.X += v.X;
        this.Y += v.Y;
    }

    public void add(float x, float y) {
        this.X += x;
        this.Y += y;
    }

    public void substract(Vector2 v) {
        this.X -= v.X;
        this.Y -= v.Y;
    }

    public void substract(float x, float y) {
        this.X -= x;
        this.Y -= y;
    }

    public Vector2 multiply(float scale) {
        this.X *= scale;
        this.Y *= scale;
        return this;
    }

    public float getLength() {
        return (float) Math.sqrt((double) ((this.X * this.X) + (this.Y * this.Y)));
    }

    public float getAngle() {
        return (float) Math.atan2((double) this.Y, (double) this.X);
    }

    public float getAngleInDegree() {
        return MathUtils.radToDeg(getAngle());
    }

    public float dot(Vector2 otherVector) {
        return (this.X * otherVector.X) + (this.Y * otherVector.Y);
    }

    public float normalize() {
        float length = getLength();
        if (length > 1.0E-4f) {
            float num = 1.0f / length;
            this.X *= num;
            this.Y *= num;
        }
        return length;
    }

    public void revert() {
        this.X = -this.X;
        this.Y = -this.Y;
    }

    public void RotateVector(float rotation) {
        RotateVector((float) Math.cos((double) rotation), (float) Math.sin((double) rotation));
    }

    public static void RotateVector(Vector2 in, Vector2 out, float rotation) {
        out.set(in);
        out.RotateVector(rotation);
    }

    public void RotateVector(float cos, float sin) {
        float x2 = (this.X * cos) + (this.Y * (-sin));
        float f = this.X;
        float f2 = this.Y;
        this.X = x2;
        this.Y = (f * sin) + (f2 * cos);
    }

    public static void RotateVector(Vector2 in, Vector2 out, float cos, float sin) {
        out.set(in);
        float x2 = (out.X * cos) + (out.Y * (-sin));
        float f = out.X;
        float f2 = out.Y;
        out.X = x2;
        out.Y = (f * sin) + (f2 * cos);
    }

    public String toString() {
        return String.valueOf(this.X) + "," + this.Y;
    }

    public void set(String str) {
        int splitIndex = str.indexOf(44);
        this.X = Float.parseFloat(str.substring(0, splitIndex));
        this.Y = Float.parseFloat(str.substring(splitIndex + 1, str.length()));
    }

    public static Vector2 create(String str) {
        Vector2 v = new Vector2();
        v.set(str);
        return v;
    }

    public static Vector2 getCenter(Vector2 v1, Vector2 v2) {
        return new Vector2((v1.X + v2.X) * 0.5f, (v1.Y + v2.Y) * 0.5f);
    }

    public static void getCenter(Vector2 v1, Vector2 v2, Vector2 out) {
        out.set((v1.X + v2.X) * 0.5f, (v1.Y + v2.Y) * 0.5f);
    }

    public static Vector2 add(Vector2 v1, Vector2 v2) {
        return new Vector2(v1.X + v2.X, v1.Y + v2.Y);
    }

    public static Vector2 substract(Vector2 v1, Vector2 v2) {
        return new Vector2(v1.X - v2.X, v1.Y - v2.Y);
    }

    public static Vector2 multiply(Vector2 v, float scale) {
        Vector2 v2 = new Vector2();
        v2.set(v);
        v2.multiply(scale);
        return v2;
    }

    public static float[] Transform(float[] vertices, Matrix4 matrix, int startIndex, int count) {
        for (int i = 0; i < count; i++) {
            float x = vertices[(i * 2) + startIndex];
            float y = vertices[(i * 2) + startIndex + 1];
            vertices[(i * 2) + startIndex] = (matrix.M00 * x) + (matrix.M10 * y) + matrix.M30;
            vertices[(i * 2) + startIndex + 1] = (matrix.M01 * x) + (matrix.M11 * y) + matrix.M31;
        }
        return vertices;
    }
}
