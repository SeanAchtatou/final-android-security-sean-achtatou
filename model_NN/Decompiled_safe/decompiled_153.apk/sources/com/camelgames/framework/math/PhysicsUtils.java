package com.camelgames.framework.math;

public class PhysicsUtils {
    public static float[] getTopHorizonalBox(float width, float height, float thick) {
        return ArrayVectorUtils.transformTo(getCenterHorizonalBox(width, thick), 4, 0.0f, -0.5f * (height - (0.5f * thick)));
    }

    public static float[] getBottomHorizonalBox(float width, float height, float thick) {
        return ArrayVectorUtils.transformTo(getCenterHorizonalBox(width, thick), 4, 0.0f, (height - (0.5f * thick)) * 0.5f);
    }

    public static float[] getLeftVerticalBox(float width, float height, float thick) {
        float[] vertices = getCenterHorizonalBox(width, thick);
        ArrayVectorUtils.rotate(vertices, 4, -1.5707964f);
        return ArrayVectorUtils.transformTo(vertices, 4, -0.5f * (width - (0.5f * thick)), 0.0f);
    }

    public static float[] getRightVerticalBox(float width, float height, float thick) {
        float[] vertices = getCenterHorizonalBox(width, thick);
        ArrayVectorUtils.rotate(vertices, 4, 1.5707964f);
        return ArrayVectorUtils.transformTo(vertices, 4, (width - (0.5f * thick)) * 0.5f, 0.0f);
    }

    public static float[] getInclineBox(float width, float height, float thick, float rotation) {
        return ArrayVectorUtils.rotate(getCenterHorizonalBox(width, thick), 4, rotation);
    }

    public static float[] getCenterVerticalBox(float height, float thick) {
        float[] vertices = getCenterHorizonalBox(height, thick);
        ArrayVectorUtils.rotate(vertices, 4, -1.5707964f);
        return vertices;
    }

    public static float[] getCenterHorizonalBox(float width, float height) {
        float halfWidth = 0.5f * width;
        float halfHeight = 0.5f * height;
        return new float[]{-halfWidth, -halfHeight, halfWidth, -halfHeight, halfWidth, halfHeight, -halfWidth, halfHeight};
    }
}
