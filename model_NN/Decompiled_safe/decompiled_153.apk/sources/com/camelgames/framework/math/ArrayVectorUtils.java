package com.camelgames.framework.math;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public final class ArrayVectorUtils {
    private static float[] currentLine = new float[4];
    private static Vector2 currentVector = new Vector2();
    public static final int dimention = 2;
    private static float[] intersect = new float[2];
    private static float[] intersect1 = new float[2];
    private static float[] intersect2 = new float[2];
    private static float[] negativeLine1 = new float[4];
    private static float[] negativeLine2 = new float[4];
    private static float[] positiveLine1 = new float[4];
    private static float[] positiveLine2 = new float[4];

    public static void scale(float[] vertices, int count, float xScale, float yScale) {
        for (int i = 0; i < count; i++) {
            int i2 = i * 2;
            vertices[i2] = vertices[i2] * xScale;
            int i3 = (i * 2) + 1;
            vertices[i3] = vertices[i3] * yScale;
        }
    }

    public static float[] rotate(float[] vertices, int count, float rotation) {
        float[] vector = new float[2];
        for (int i = 0; i < count; i++) {
            vector[0] = vertices[i * 2];
            vector[1] = vertices[(i * 2) + 1];
            MathUtils.RotateVector(vector, rotation);
            vertices[i * 2] = vector[0];
            vertices[(i * 2) + 1] = vector[1];
        }
        return vertices;
    }

    public static float[] transformTo(float[] vertices, int count, float centerX, float centerY) {
        for (int i = 0; i < count; i++) {
            int i2 = i * 2;
            vertices[i2] = vertices[i2] - centerX;
            int i3 = (i * 2) + 1;
            vertices[i3] = vertices[i3] - centerY;
        }
        return vertices;
    }

    public static Vector2 getCenter(float[] vertices, int count) {
        float xSum = 0.0f;
        float ySum = 0.0f;
        for (int i = 0; i < count; i++) {
            xSum += vertices[i * 2];
            ySum += vertices[(i * 2) + 1];
        }
        return new Vector2(xSum / ((float) count), ySum / ((float) count));
    }

    public static boolean cut(float[] vertices, int count, float[] cutLine, ArrayList<Float> outVertices1, ArrayList<Float> outVertices2) {
        int intersectCount = 0;
        int cutIndex1 = 0;
        int cutIndex2 = 0;
        int i = 0;
        while (true) {
            if (i >= count) {
                break;
            }
            if (i < count - 1) {
                System.arraycopy(vertices, i * 2, currentLine, 0, 4);
            } else {
                System.arraycopy(vertices, i * 2, currentLine, 0, 2);
                System.arraycopy(vertices, 0, currentLine, 2, 2);
            }
            if (MathUtils.intersectOfLineSegment(currentLine, cutLine, intersect)) {
                intersectCount++;
                if (intersectCount == 1) {
                    System.arraycopy(intersect, 0, intersect1, 0, 2);
                    cutIndex1 = i;
                } else if (intersectCount == 2) {
                    System.arraycopy(intersect, 0, intersect2, 0, 2);
                    cutIndex2 = i;
                    break;
                }
            }
            i++;
        }
        if (intersectCount != 2) {
            return false;
        }
        outVertices1.add(Float.valueOf(intersect1[0]));
        outVertices1.add(Float.valueOf(intersect1[1]));
        int i2 = (cutIndex1 + 1) % count;
        while (i2 != cutIndex2) {
            outVertices1.add(Float.valueOf(vertices[i2 * 2]));
            outVertices1.add(Float.valueOf(vertices[(i2 * 2) + 1]));
            i2 = (i2 + 1) % count;
        }
        outVertices1.add(Float.valueOf(vertices[cutIndex2 * 2]));
        outVertices1.add(Float.valueOf(vertices[(cutIndex2 * 2) + 1]));
        outVertices1.add(Float.valueOf(intersect2[0]));
        outVertices1.add(Float.valueOf(intersect2[1]));
        outVertices2.add(Float.valueOf(intersect2[0]));
        outVertices2.add(Float.valueOf(intersect2[1]));
        int i3 = (cutIndex2 + 1) % count;
        while (i3 != cutIndex1) {
            outVertices2.add(Float.valueOf(vertices[i3 * 2]));
            outVertices2.add(Float.valueOf(vertices[(i3 * 2) + 1]));
            i3 = (i3 + 1) % count;
        }
        outVertices2.add(Float.valueOf(vertices[cutIndex1 * 2]));
        outVertices2.add(Float.valueOf(vertices[(cutIndex1 * 2) + 1]));
        outVertices2.add(Float.valueOf(intersect1[0]));
        outVertices2.add(Float.valueOf(intersect1[1]));
        return true;
    }

    public static float getTransformedRadius(float[] vertices, int count) {
        float sum = 0.0f;
        for (int i = 0; i < count; i++) {
            sum += MathUtils.length(vertices[i * 2], vertices[(i * 2) + 1]);
        }
        return sum / ((float) count);
    }

    public static float getMaxVectorLength(float[] vertices, int count) {
        float max = 0.0f;
        for (int i = 0; i < count; i++) {
            max = Math.max(max, MathUtils.length(vertices[i * 2], vertices[(i * 2) + 1]));
        }
        return max;
    }

    public static float getMinVectorLength(float[] vertices, int count) {
        float min = 0.0f;
        for (int i = 0; i < count; i++) {
            min = Math.min(min, MathUtils.length(vertices[i * 2], vertices[(i * 2) + 1]));
        }
        return min;
    }

    public static void changeToCirlce(float[] vertices, int count, float radius) {
        float gap = 6.2831855f / ((float) (count - 1));
        for (int i = 0; i < count; i++) {
            vertices[i * 2] = (float) (((double) radius) * Math.cos((double) (((float) i) * gap)));
            vertices[(i * 2) + 1] = (float) (((double) radius) * Math.sin((double) (((float) i) * gap)));
        }
    }

    public static boolean couldJoint(float[] vertices, int count, float verticesCenterX, float verticesCenterY, float centerX, float centerY, float radius) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= count - 1) {
                return false;
            }
            int from = i2 * 2;
            int to = (i2 + 1) * 2;
            if (MathUtils.DistanceBetweenPointSegment(vertices[from], vertices[from + 1], vertices[to], vertices[to + 1], centerX - verticesCenterX, centerY - verticesCenterY, null) < radius) {
                return true;
            }
            i = i2 + 1;
        }
    }

    public static boolean hitTest(float[] vertices, int count, float x, float y, float error) {
        for (int i = 0; i < count - 1; i++) {
            int from = i * 2;
            int to = (i + 1) * 2;
            if (MathUtils.DistanceBetweenPointSegment(vertices[from], vertices[from + 1], vertices[to], vertices[to + 1], x, y, null) < error) {
                return true;
            }
        }
        return false;
    }

    public static float distanceBetweenpointSegments(float[] vertices, float x, float y, Vector2 intersect3) {
        int count = vertices.length / 2;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= count - 1) {
                return Float.MAX_VALUE;
            }
            int from = i2 * 2;
            int to = (i2 + 1) * 2;
            if (MathUtils.DistanceBetweenPointSegment(vertices[from], vertices[from + 1], vertices[to], vertices[to + 1], x, y, currentVector) < Float.MAX_VALUE && intersect3 != null) {
                intersect3.set(currentVector);
            }
            i = i2 + 1;
        }
    }

    public static int smoothLine(float[] startPoint, float[] endPoint, float[] lineSmoothed, float threshold, float[] controlPoint) {
        currentVector.set(endPoint[0] - startPoint[0], endPoint[1] - startPoint[1]);
        return smoothLine(startPoint, endPoint, lineSmoothed, controlPoint, Math.max(2, (int) (currentVector.getLength() / threshold)) + 1);
    }

    public static int smoothLine(float[] startPoint, float[] endPoint, float[] lineSmoothed, float[] controlPoint, int splitCount) {
        float deltaT = 1.0f / ((float) splitCount);
        for (int index = 0; index < splitCount + 1; index++) {
            float t = deltaT * ((float) index);
            lineSmoothed[index * 2] = bezierCurve2(startPoint[0], controlPoint[0], endPoint[0], t);
            lineSmoothed[(index * 2) + 1] = bezierCurve2(startPoint[1], controlPoint[1], endPoint[1], t);
        }
        return splitCount + 1;
    }

    public static int smoothLine(float[] startPoint, float[] endPoint, float[] lineSmoothed, float threshold, float[] controlPoint, float[] controlPoint2) {
        currentVector.set(endPoint[0] - startPoint[0], endPoint[1] - startPoint[1]);
        return smoothLine(startPoint, endPoint, lineSmoothed, controlPoint, controlPoint2, Math.max(2, (int) (currentVector.getLength() / threshold)) + 1);
    }

    public static int smoothLine(float[] startPoint, float[] endPoint, float[] lineSmoothed, float[] controlPoint, float[] controlPoint2, int splitCount) {
        float deltaT = 1.0f / ((float) splitCount);
        for (int index = 0; index < splitCount + 1; index++) {
            float t = deltaT * ((float) index);
            lineSmoothed[index * 2] = bezierCurve3(startPoint[0], controlPoint[0], controlPoint2[0], endPoint[0], t);
            lineSmoothed[(index * 2) + 1] = bezierCurve3(startPoint[1], controlPoint[1], controlPoint2[1], endPoint[1], t);
        }
        return splitCount + 1;
    }

    /* JADX INFO: Multiple debug info for r10v3 float: [D('endAngle' float), D('wholeAngle' float)] */
    /* JADX INFO: Multiple debug info for r12v5 float: [D('wholeAngle' float), D('subAngle' float)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public static void circleCurve(Vector2 start, Vector2 end, Vector2 control1, Vector2 control2, float segmentAngle, List<Vector2> curve) {
        float wholeAngle;
        float wholeAngle2;
        float referenceAngle;
        float wholeAngle3;
        float centerX = 0.5f * (end.X + start.X);
        float centerY = 0.5f * (end.Y + start.Y);
        float angle = (float) Math.atan2((double) (end.Y - start.Y), (double) (end.X - start.X));
        float[] centerLine = {centerX, centerY, ((float) Math.cos((double) (1.5707964f + angle))) + centerX, ((float) Math.sin((double) (1.5707964f + angle))) + centerY};
        float[] line2 = {control1.X, control1.Y, control1.X + ((float) Math.cos((double) angle)), ((float) Math.sin((double) angle)) + control1.Y};
        float[] intersect3 = new float[2];
        MathUtils.intersectOfLines(centerLine, line2, intersect3);
        control1.set(intersect3[0], intersect3[1]);
        Vector2 center = new Vector2(control1);
        float radius = MathUtils.length(center.X - start.X, center.Y - start.Y);
        Vector2 v = Vector2.substract(control2, center);
        if (v.getLength() < 0.1f) {
            v.set(centerX - center.X, centerY - center.Y);
        }
        v.normalize();
        control2.set(center.X + (v.X * radius), (v.Y * radius) + center.Y);
        float startAngle = (float) Math.atan2((double) (start.Y - center.Y), (double) (start.X - center.X));
        float endAngle = (float) Math.atan2((double) (end.Y - center.Y), (double) (end.X - center.X));
        float referenceAngle2 = MathUtils.constrainPi(((float) Math.atan2((double) (control2.Y - center.Y), (double) (control2.X - center.X))) - startAngle);
        float wholeAngle4 = MathUtils.constrainPi(endAngle - startAngle);
        boolean find = false;
        if (referenceAngle2 >= 0.0f) {
            if (wholeAngle4 < 0.0f) {
                wholeAngle4 += 6.2831855f;
            }
            if (wholeAngle4 >= referenceAngle2) {
                find = true;
                wholeAngle = wholeAngle4;
            }
            wholeAngle = wholeAngle4;
        } else {
            if (wholeAngle4 >= 0.0f) {
                wholeAngle4 -= 6.2831855f;
            }
            if (wholeAngle4 <= referenceAngle2) {
                find = true;
                wholeAngle = wholeAngle4;
            }
            wholeAngle = wholeAngle4;
        }
        if (!find) {
            if (referenceAngle2 >= 0.0f) {
                referenceAngle = referenceAngle2 - 6.2831855f;
            } else {
                referenceAngle = 6.2831855f + referenceAngle2;
            }
            if (referenceAngle >= 0.0f) {
                if (wholeAngle < 0.0f) {
                    wholeAngle2 = 6.2831855f + wholeAngle;
                } else {
                    wholeAngle2 = wholeAngle;
                }
                if (wholeAngle2 >= referenceAngle) {
                }
            } else {
                if (wholeAngle >= 0.0f) {
                    wholeAngle3 = wholeAngle - 6.2831855f;
                } else {
                    wholeAngle3 = wholeAngle;
                }
                if (wholeAngle2 <= referenceAngle) {
                }
            }
        } else {
            wholeAngle2 = wholeAngle;
        }
        int count = (int) Math.max(1.0f, Math.abs(wholeAngle2) / segmentAngle);
        float subAngle = wholeAngle2 / ((float) count);
        for (int i = 1; i < count; i++) {
            curve.add(new Vector2(center.X + (((float) Math.cos((double) ((((float) i) * subAngle) + startAngle))) * radius), center.Y + (((float) Math.sin((double) ((((float) i) * subAngle) + startAngle))) * radius)));
        }
    }

    public static void bezierCurve3(Vector2 p00, Vector2 p10, Vector2 p20, Vector2 p30, float error, List<Vector2> curve) {
        curve.clear();
        curve.add(new Vector2(p00));
        curve.add(new Vector2(p30));
        bezierCurve3(p00, p10, p20, p30, error, curve, 1);
    }

    /* JADX INFO: Multiple debug info for r8v15 int: [D('p00' com.camelgames.framework.math.Vector2), D('count' int)] */
    public static int bezierCurve3(Vector2 p00, Vector2 p10, Vector2 p20, Vector2 p30, float error, List<Vector2> curve, int startIndex) {
        float dis1 = MathUtils.DistanceBetweenPointLine(p00.X, p00.Y, p30.X, p30.Y, p10.X, p10.Y);
        float dis2 = MathUtils.DistanceBetweenPointLine(p00.X, p00.Y, p30.X, p30.Y, p20.X, p20.Y);
        if (dis1 < error && dis2 < error) {
            return 0;
        }
        Vector2[][] p = (Vector2[][]) Array.newInstance(Vector2.class, 4, 4);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                p[i][j] = new Vector2();
            }
        }
        p[0][0].set(p00);
        p[1][0].set(p10);
        p[2][0].set(p20);
        p[3][0].set(p30);
        bezierCurve3(p, 0.5f);
        curve.add(startIndex, p[3][3]);
        int count = 1 + bezierCurve3(p[0][0], p[1][1], p[2][2], p[3][3], error, curve, startIndex);
        return count + bezierCurve3(p[3][3], p[3][2], p[3][1], p[3][0], error, curve, startIndex + count);
    }

    public static Vector2 bezierCurve3(Vector2 p00, Vector2 p10, Vector2 p20, Vector2 p30, float t) {
        return new Vector2(bezierCurve3(p00.X, p10.X, p20.X, p30.X, t), bezierCurve3(p00.Y, p10.Y, p20.Y, p30.Y, t));
    }

    public static float bezierCurve3(float p00, float p10, float p20, float p30, float t) {
        float p21 = ((1.0f - t) * p10) + (p20 * t);
        return ((1.0f - t) * (((1.0f - t) * (((1.0f - t) * p00) + (p10 * t))) + (p21 * t))) + ((((1.0f - t) * p21) + ((((1.0f - t) * p20) + (p30 * t)) * t)) * t);
    }

    public static void bezierCurve3(Vector2[][] p, float t) {
        p[1][1].set((p[0][0].X * (1.0f - t)) + (p[1][0].X * t), (p[0][0].Y * (1.0f - t)) + (p[1][0].Y * t));
        p[2][1].set((p[1][0].X * (1.0f - t)) + (p[2][0].X * t), (p[1][0].Y * (1.0f - t)) + (p[2][0].Y * t));
        p[3][1].set((p[2][0].X * (1.0f - t)) + (p[3][0].X * t), (p[2][0].Y * (1.0f - t)) + (p[3][0].Y * t));
        p[2][2].set((p[1][1].X * (1.0f - t)) + (p[2][1].X * t), (p[1][1].Y * (1.0f - t)) + (p[2][1].Y * t));
        p[3][2].set((p[2][1].X * (1.0f - t)) + (p[3][1].X * t), (p[2][1].Y * (1.0f - t)) + (p[3][1].Y * t));
        p[3][3].set((p[2][2].X * (1.0f - t)) + (p[3][2].X * t), (p[2][2].Y * (1.0f - t)) + (p[3][2].Y * t));
    }

    public static float bezierCurve2(float p00, float p10, float p20, float t) {
        return ((1.0f - t) * (((1.0f - t) * p00) + (p10 * t))) + ((((1.0f - t) * p10) + (p20 * t)) * t);
    }

    /* JADX INFO: Multiple debug info for r10v1 int: [D('rectOffset' int), D('index' int)] */
    /* JADX INFO: Multiple debug info for r9v1 int: [D('lines' float[]), D('index' int)] */
    /* JADX INFO: Multiple debug info for r14v27 int: [D('index' int), D('needSetFirst2' boolean)] */
    public static void lineToRectStrip(float[] rects, float[] lines, int rectOffset, int linesOffset, int count, float halfThick, boolean needSetFirst2) {
        int index;
        if (linesOffset < count) {
            float[] prepareLine1 = positiveLine2;
            float[] prepareLine2 = negativeLine2;
            int index2 = rectOffset * 2;
            prepareLines(lines, linesOffset, count, prepareLine1, prepareLine2, currentLine, linesOffset, halfThick);
            if (needSetFirst2) {
                int index3 = index2 + 1;
                rects[index2] = positiveLine2[0];
                int index4 = index3 + 1;
                rects[index3] = positiveLine2[1];
                int index5 = index4 + 1;
                rects[index4] = negativeLine2[0];
                index = index5 + 1;
                rects[index5] = negativeLine2[1];
            } else {
                index = index2 + 4;
            }
            System.arraycopy(positiveLine2, 0, positiveLine1, 0, 4);
            System.arraycopy(negativeLine2, 0, negativeLine1, 0, 4);
            int linesOffset2 = index;
            for (int i = linesOffset + 1; i < count - 1; i++) {
                prepareLines(lines, i, count, prepareLine1, prepareLine2, currentLine, i, halfThick);
                float angleChanged = MathUtils.anglesBetween(positiveLine1[2] - positiveLine1[0], positiveLine1[3] - positiveLine1[1], currentLine[2] - currentLine[0], currentLine[3] - currentLine[1]);
                intersect1[0] = positiveLine2[0];
                intersect1[1] = positiveLine2[1];
                intersect2[0] = negativeLine2[0];
                intersect2[1] = negativeLine2[1];
                if (Math.abs(angleChanged) > 2.0943952f) {
                    float[] temp = prepareLine1;
                    prepareLine1 = prepareLine2;
                    prepareLine2 = temp;
                    swap(positiveLine2, negativeLine2);
                }
                MathUtils.intersectOfLines(positiveLine1, positiveLine2, intersect1);
                MathUtils.intersectOfLines(negativeLine1, negativeLine2, intersect2);
                int index6 = linesOffset2 + 1;
                rects[linesOffset2] = intersect1[0];
                int index7 = index6 + 1;
                rects[index6] = intersect1[1];
                int index8 = index7 + 1;
                rects[index7] = intersect2[0];
                linesOffset2 = index8 + 1;
                rects[index8] = intersect2[1];
                System.arraycopy(positiveLine2, 0, positiveLine1, 0, 4);
                System.arraycopy(negativeLine2, 0, negativeLine1, 0, 4);
            }
            int index9 = linesOffset2 + 1;
            rects[linesOffset2] = positiveLine2[2];
            int index10 = index9 + 1;
            rects[index9] = positiveLine2[3];
            int index11 = index10 + 1;
            rects[index10] = negativeLine2[2];
            int i2 = index11 + 1;
            rects[index11] = negativeLine2[3];
        }
    }

    private static void swap(float[] array1, float[] array2) {
        for (int i = 0; i < array1.length; i++) {
            float temp = array1[i];
            array1[i] = array2[i];
            array2[i] = temp;
        }
    }

    private static void prepareLines(float[] vertices, int startIndex, int count, float[] positiveLine22, float[] negativeLine22, float[] currentLine2, int index, float halfThick) {
        currentLine2[0] = vertices[index * 2];
        currentLine2[1] = vertices[(index * 2) + 1];
        if (index == count - 1) {
            currentLine2[2] = (vertices[index * 2] * 2.0f) - vertices[(index - 1) * 2];
            currentLine2[3] = (vertices[(index * 2) + 1] * 2.0f) - vertices[((index - 1) * 2) + 1];
        } else {
            currentLine2[2] = vertices[(index + 1) * 2];
            currentLine2[3] = vertices[((index + 1) * 2) + 1];
        }
        getPNLines(currentLine2, positiveLine22, negativeLine22, halfThick);
    }

    /* JADX INFO: Multiple debug info for r9v4 float: [D('angle' float), D('yOffset' float)] */
    private static void getPNLines(float[] line, float[] positiveLine, float[] negativeLine, float halfThick) {
        float x1 = line[0];
        float y1 = line[1];
        float x2 = line[2];
        float y2 = line[3];
        float angle = 1.5707964f + ((float) Math.atan2((double) (y2 - y1), (double) (x2 - x1)));
        float xOffset = ((float) Math.cos((double) angle)) * halfThick;
        float yOffset = ((float) Math.sin((double) angle)) * halfThick;
        positiveLine[0] = x1 + xOffset;
        positiveLine[1] = y1 + yOffset;
        positiveLine[2] = x2 + xOffset;
        positiveLine[3] = y2 + yOffset;
        negativeLine[0] = x1 - xOffset;
        negativeLine[1] = y1 - yOffset;
        negativeLine[2] = x2 - xOffset;
        negativeLine[3] = y2 - yOffset;
    }
}
