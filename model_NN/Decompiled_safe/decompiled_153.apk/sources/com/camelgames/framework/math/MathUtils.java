package com.camelgames.framework.math;

import java.math.BigDecimal;

public final class MathUtils {
    static final /* synthetic */ boolean $assertionsDisabled;
    public static final float DEG_TO_RAD = 0.017453292f;
    public static final float PI = 3.1415927f;
    public static final float PIOver1d25 = 2.5132742f;
    public static final float PIOver1d5 = 2.0943952f;
    public static final float PIOver2 = 1.5707964f;
    public static final float PIOver2d5 = 1.2566371f;
    public static final float PIOver3 = 1.0471975f;
    public static final float PIOver4 = 0.7853982f;
    public static final float PIOver5 = 0.62831855f;
    public static final float PIOver6 = 0.5235989f;
    public static final float PIOver8 = 0.3926991f;
    public static final float RAD_TO_DEG = 57.295776f;
    public static final float ThreePI = 9.424778f;
    public static final float TwoPI = 6.2831855f;
    private static float[] intersect = new float[2];
    private static float[] line = new float[4];
    private static float[] segment = new float[4];

    static {
        boolean z;
        if (!MathUtils.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        $assertionsDisabled = z;
    }

    public static float radToDeg(float rad) {
        return 57.295776f * rad;
    }

    public static float degToRad(float deg) {
        return 0.017453292f * deg;
    }

    public static float constranDegree360(float deg) {
        while (deg >= 360.0f) {
            deg -= 360.0f;
        }
        while (deg < 0.0f) {
            deg += 360.0f;
        }
        return deg;
    }

    public static float constrainDegree180(float angle) {
        while (angle < -180.0f) {
            angle += 360.0f;
        }
        while (angle >= 180.0f) {
            angle -= 360.0f;
        }
        return angle;
    }

    public static float constrainPi(float angle) {
        while (angle < -3.1415927f) {
            angle += 6.2831855f;
        }
        while (angle >= 3.1415927f) {
            angle -= 6.2831855f;
        }
        return angle;
    }

    public static float constrain2Pi(float angle) {
        while (angle < 0.0f) {
            angle += 6.2831855f;
        }
        while (angle >= 6.2831855f) {
            angle -= 6.2831855f;
        }
        return angle;
    }

    public static float constrainAbs(float num, float absConstrain) {
        if (Math.abs(num) <= absConstrain) {
            return num;
        }
        if (num > 0.0f) {
            return absConstrain;
        }
        return -absConstrain;
    }

    public static float angleConvert(float angle, float fromScreenRate, float toScreenRate) {
        Vector2 v = new Vector2(1.0f, 0.0f);
        v.RotateVector(angle);
        v.X = (v.X * toScreenRate) / fromScreenRate;
        return angleFromTo(1.0f, 0.0f, v.X, v.Y);
    }

    public static float random(float min, float max) {
        return (((float) Math.random()) * (max - min)) + min;
    }

    public static float random(float max) {
        return ((float) Math.random()) * max;
    }

    public static int randomInt(int min, int max) {
        return ((int) (Math.random() * ((double) (max - min)))) + min;
    }

    public static int randomInt(int max) {
        return randomInt(0, max);
    }

    public static boolean randomBoolean() {
        return Math.random() >= 0.5d;
    }

    public static boolean randomBoolean(int divider) {
        return ((float) Math.random()) * ((float) divider) < 1.0f;
    }

    public static int randomAbs(int max, int min) {
        int scope = max - min;
        int rad = randomInt(scope * 2) - scope;
        return rad >= 0 ? rad + min : rad - min;
    }

    public static float toRoundHalfUp(float value, int index) {
        try {
            return new BigDecimal((double) value).setScale(index, 4).floatValue();
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0f;
        }
    }

    public static float anglesBetween(float x1, float y1, float x2, float y2) {
        return Math.abs(constrainPi(angleFromTo(x1, y1, x2, y2)));
    }

    public static float angleFromTo(float x1, float y1, float x2, float y2) {
        return constrain2Pi((float) (Math.atan2((double) y2, (double) x2) - Math.atan2((double) y1, (double) x1)));
    }

    public static float length(float x, float y) {
        return (float) Math.sqrt((double) ((x * x) + (y * y)));
    }

    public static float length(Vector2 v1, Vector2 v2) {
        float x = v1.X - v2.X;
        float y = v1.Y - v2.Y;
        return (float) Math.sqrt((double) ((x * x) + (y * y)));
    }

    public static float lengthSquare(float x, float y) {
        return (x * x) + (y * y);
    }

    public static float lengthSquare(Vector2 v1, Vector2 v2) {
        float x = v1.X - v2.X;
        float y = v1.Y - v2.Y;
        return (x * x) + (y * y);
    }

    public static void swap(double[] array, int a, int b) {
        double t = array[a];
        array[a] = array[b];
        array[b] = t;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 155 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static double nth_element(double[] r12, int r13, int r14) {
        /*
            r11 = 1
            r4 = 0
            int r5 = r13 - r11
            boolean r7 = com.camelgames.framework.math.MathUtils.$assertionsDisabled
            if (r7 != 0) goto L_0x0010
            if (r12 != 0) goto L_0x0010
            java.lang.AssertionError r7 = new java.lang.AssertionError
            r7.<init>()
            throw r7
        L_0x0010:
            boolean r7 = com.camelgames.framework.math.MathUtils.$assertionsDisabled
            if (r7 != 0) goto L_0x001c
            if (r5 >= 0) goto L_0x001c
            java.lang.AssertionError r7 = new java.lang.AssertionError
            r7.<init>()
            throw r7
        L_0x001c:
            boolean r7 = com.camelgames.framework.math.MathUtils.$assertionsDisabled
            if (r7 != 0) goto L_0x0028
            if (r14 >= 0) goto L_0x0028
            java.lang.AssertionError r7 = new java.lang.AssertionError
            r7.<init>()
            throw r7
        L_0x0028:
            boolean r7 = com.camelgames.framework.math.MathUtils.$assertionsDisabled
            if (r7 != 0) goto L_0x008e
            if (r14 <= r5) goto L_0x008e
            java.lang.AssertionError r7 = new java.lang.AssertionError
            r7.<init>()
            throw r7
        L_0x0034:
            int r7 = r4 + r5
            int r6 = r7 >> 1
            int r7 = r4 + 1
            swap(r12, r6, r7)
            r7 = r12[r4]
            r9 = r12[r5]
            int r7 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r7 <= 0) goto L_0x0048
            swap(r12, r4, r5)
        L_0x0048:
            int r7 = r4 + 1
            r7 = r12[r7]
            r9 = r12[r5]
            int r7 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r7 <= 0) goto L_0x0057
            int r7 = r4 + 1
            swap(r12, r7, r5)
        L_0x0057:
            r7 = r12[r4]
            int r9 = r4 + 1
            r9 = r12[r9]
            int r7 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r7 <= 0) goto L_0x0066
            int r7 = r4 + 1
            swap(r12, r4, r7)
        L_0x0066:
            int r2 = r4 + 1
            r3 = r5
            int r7 = r4 + 1
            r0 = r12[r7]
        L_0x006d:
            int r2 = r2 + 1
            r7 = r12[r2]
            int r7 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
            if (r7 < 0) goto L_0x006d
        L_0x0075:
            int r3 = r3 + -1
            r7 = r12[r3]
            int r7 = (r7 > r0 ? 1 : (r7 == r0 ? 0 : -1))
            if (r7 > 0) goto L_0x0075
            if (r3 >= r2) goto L_0x00a4
            int r7 = r4 + 1
            r8 = r12[r3]
            r12[r7] = r8
            r12[r3] = r0
            if (r3 < r14) goto L_0x008b
            int r5 = r3 - r11
        L_0x008b:
            if (r3 > r14) goto L_0x008e
            r4 = r2
        L_0x008e:
            int r7 = r4 + 1
            if (r5 > r7) goto L_0x0034
            int r7 = r4 + 1
            if (r5 != r7) goto L_0x00a1
            r7 = r12[r5]
            r9 = r12[r4]
            int r7 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r7 >= 0) goto L_0x00a1
            swap(r12, r4, r5)
        L_0x00a1:
            r7 = r12[r14]
            return r7
        L_0x00a4:
            swap(r12, r2, r3)
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.camelgames.framework.math.MathUtils.nth_element(double[], int, int):double");
    }

    public static float dot(float x1, float y1, float x2, float y2) {
        return (x1 * x2) + (y1 * y2);
    }

    public static void normalize(float[] vector2) {
        float x = vector2[0];
        float y = vector2[1];
        float num = (float) (1.0d / Math.sqrt((double) ((x * x) + (y * y))));
        vector2[0] = vector2[0] * num;
        vector2[1] = vector2[1] * num;
    }

    public static void RotateVector(float[] vector, float rotation) {
        float M11 = (float) Math.cos((double) rotation);
        float M12 = (float) Math.sin((double) rotation);
        float M21 = (float) (-Math.sin((double) rotation));
        float M22 = (float) Math.cos((double) rotation);
        vector[0] = (vector[0] * M11) + (vector[1] * M21);
        vector[1] = (vector[0] * M12) + (vector[1] * M22);
    }

    /* JADX INFO: Multiple debug info for r1v1 float: [D('sideY' float), D('f' float)] */
    /* JADX INFO: Multiple debug info for r7v1 float: [D('endX' float), D('f' float)] */
    /* JADX INFO: Multiple debug info for r5v1 float: [D('startX' float), D('projectX' float)] */
    /* JADX INFO: Multiple debug info for r6v1 float: [D('projectY' float), D('startY' float)] */
    public static float DistanceBetweenPointSegment(float startX, float startY, float endX, float endY, float pointX, float pointY, Vector2 intersect2) {
        float segmentX = endX - startX;
        float segmentY = endY - startY;
        float f = dot(segmentX, segmentY, pointX - startX, pointY - startY);
        if (f < 0.0f) {
            if (intersect2 != null) {
                intersect2.set(startX, startY);
            }
            return length(pointX - startX, pointY - startY);
        }
        float d = lengthSquare(segmentX, segmentY);
        if (f > d) {
            if (intersect2 != null) {
                intersect2.set(endX, endY);
            }
            return length(pointX - endX, pointY - endY);
        }
        float f2 = f / d;
        float projectX = startX + (f2 * segmentX);
        float startY2 = startY + (f2 * segmentY);
        if (intersect2 != null) {
            intersect2.set(projectX, startY2);
        }
        return length(pointX - projectX, pointY - startY2);
    }

    /* JADX INFO: Multiple debug info for r7v1 float: [D('pointX' float), D('sideX' float)] */
    /* JADX INFO: Multiple debug info for r8v1 float: [D('sideY' float), D('pointY' float)] */
    /* JADX INFO: Multiple debug info for r8v2 float: [D('sideY' float), D('f' float)] */
    /* JADX INFO: Multiple debug info for r5v1 float: [D('endX' float), D('f' float)] */
    /* JADX INFO: Multiple debug info for r3v1 float: [D('startX' float), D('projectX' float)] */
    /* JADX INFO: Multiple debug info for r4v1 float: [D('projectY' float), D('startY' float)] */
    public static boolean PointSegmentProjection(float startX, float startY, float endX, float endY, float pointX, float pointY, Vector2 intersect2) {
        float segmentX = endX - startX;
        float segmentY = endY - startY;
        float f = dot(segmentX, segmentY, pointX - startX, pointY - startY);
        if (f < 0.0f) {
            if (intersect2 != null) {
                intersect2.set(startX, startY);
            }
            return false;
        }
        float d = lengthSquare(segmentX, segmentY);
        if (f > d) {
            if (intersect2 != null) {
                intersect2.set(endX, endY);
            }
            return false;
        }
        float f2 = f / d;
        float projectX = startX + (f2 * segmentX);
        float startY2 = startY + (f2 * segmentY);
        if (intersect2 != null) {
            intersect2.set(projectX, startY2);
        }
        return true;
    }

    public static float DistanceBetweenPointLine(float startX, float startY, float endX, float endY, float pointX, float pointY) {
        float segmentX = endX - startX;
        float segmentY = endY - startY;
        float f = dot(segmentX, segmentY, pointX - startX, pointY - startY) / lengthSquare(segmentX, segmentY);
        return length(pointX - (startX + (f * segmentX)), pointY - (startY + (f * segmentY)));
    }

    public static void PointLineProjection(float startX, float startY, float endX, float endY, float pointX, float pointY, Vector2 intersect2) {
        float segmentX = endX - startX;
        float segmentY = endY - startY;
        float f = dot(segmentX, segmentY, pointX - startX, pointY - startY) / lengthSquare(segmentX, segmentY);
        intersect2.X = (f * segmentX) + startX;
        intersect2.Y = (f * segmentY) + startY;
    }

    public static boolean testRange(float guard1, float guard2, float testValue) {
        return (testValue - guard1) * (guard2 - testValue) >= -0.1f;
    }

    public static boolean testRange(Vector2 guard1, Vector2 guard2, Vector2 testPoint) {
        return testRange(guard1.X, guard2.X, testPoint.X) && testRange(guard1.Y, guard2.Y, testPoint.Y);
    }

    public static boolean intersectOfLineSegment(float[] segment2, float[] line2, float[] intersect2) {
        if (!intersectOfLines(segment2, line2, intersect2)) {
            return false;
        }
        if (!testRange(segment2[0], segment2[2], intersect2[0]) || !testRange(segment2[1], segment2[3], intersect2[1])) {
            return false;
        }
        return true;
    }

    public static boolean intersectOfSegments(float[] segment1, float[] segment2, float[] intersect2) {
        if (!intersectOfLines(segment1, segment2, intersect2)) {
            return false;
        }
        if (!testRange(segment1[0], segment1[2], intersect2[0]) || !testRange(segment1[1], segment1[3], intersect2[1]) || !testRange(segment2[0], segment2[2], intersect2[0]) || !testRange(segment2[1], segment2[3], intersect2[1])) {
            return false;
        }
        return true;
    }

    public static boolean insideConvex(float[] convex, float x, float y) {
        int intersectCount = 0;
        line[0] = x;
        line[1] = y;
        line[2] = 10000.0f + x;
        line[3] = y;
        for (int i = 0; i < (convex.length / 2) - 1; i++) {
            segment[0] = convex[i * 2];
            segment[1] = convex[(i * 2) + 1];
            segment[2] = convex[(i * 2) + 2];
            segment[3] = convex[(i * 2) + 3];
            if (intersectOfSegments(segment, line, intersect)) {
                intersectCount++;
            }
        }
        if (intersectCount % 2 == 1) {
            return true;
        }
        return false;
    }

    /* JADX INFO: Multiple debug info for r0v6 double: [D('d' double), D('ua' double)] */
    public static boolean intersectOfLines(float[] line1, float[] line2, float[] intersect2) {
        double d = (double) (((line2[3] - line2[1]) * (line1[2] - line1[0])) - ((line2[2] - line2[0]) * (line1[3] - line1[1])));
        double n_a = (double) (((line2[2] - line2[0]) * (line1[1] - line2[1])) - ((line1[0] - line2[0]) * (line2[3] - line2[1])));
        if (Math.abs(d) < 9.999999747378752E-5d) {
            return false;
        }
        double ua = n_a / d;
        intersect2[0] = (float) (((double) line1[0]) + (((double) (line1[2] - line1[0])) * ua));
        intersect2[1] = (float) ((ua * ((double) (line1[3] - line1[1]))) + ((double) line1[1]));
        return true;
    }

    public static float reflectAngle(float angle, float normal) {
        return constrain2Pi(((2.0f * normal) - angle) + 3.1415927f);
    }

    public static int largestCommonDivisor(int num1, int num2) {
        int n;
        int m;
        if (num1 > num2) {
            m = num1;
            n = num2;
        } else {
            m = num2;
            n = num1;
        }
        while (m % n != 0) {
            int t = n;
            n = m % n;
            m = t;
        }
        return n;
    }

    public static int getDigitCount(int num) {
        int count = 1;
        int num2 = num / 10;
        while (num2 != 0) {
            num2 /= 10;
            count++;
        }
        return count;
    }
}
