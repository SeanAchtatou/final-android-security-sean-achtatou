package com.camelgames.framework.touch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public final class TouchManager {
    private static TouchManager instance = new TouchManager();
    private TouchBuffer[] buffers = new TouchBuffer[2];
    private int inputBuffer = 0;
    private boolean isStoped;
    private ArrayList<KeyProcessor> keyProcessors = new ArrayList<>();
    private LinkedList<KeyProcessor> keyWaitToAdd = new LinkedList<>();
    private LinkedList<KeyProcessor> keyWaitToDelete = new LinkedList<>();
    private int outputBuffer = 1;
    private TapGesture tapGesture = new TapGesture();
    private ArrayList<TouchProcessor> touchProcessors = new ArrayList<>();
    private LinkedList<TouchProcessor> touchWaitToAdd = new LinkedList<>();
    private LinkedList<TouchProcessor> touchWaitToDelete = new LinkedList<>();

    public interface KeyProcessor {
        void flushKeys(int[] iArr, int i);
    }

    public interface TouchProcessor {
        void touchAction(int i, int i2, int i3);
    }

    public static TouchManager getInstace() {
        return instance;
    }

    private TouchManager() {
        this.buffers[0] = new TouchBuffer();
        this.buffers[1] = new TouchBuffer();
    }

    public void addKey(int keyCode) {
        if (!this.isStoped) {
            this.buffers[this.inputBuffer].addKey(keyCode);
        }
    }

    public void addTouch(int x, int y, int action) {
        if (!this.isStoped) {
            this.buffers[this.inputBuffer].addTouch(x, y, action);
        }
    }

    public void switchBuffer() {
        this.buffers[this.outputBuffer].clear();
        int temp = this.inputBuffer;
        this.inputBuffer = this.outputBuffer;
        this.outputBuffer = temp;
    }

    public void add(TouchProcessor item) {
        if (item != null) {
            if (!this.touchWaitToAdd.contains(item)) {
                this.touchWaitToAdd.add(item);
            }
            if (this.touchWaitToDelete.contains(item)) {
                this.touchWaitToDelete.remove(item);
            }
        }
    }

    public void remove(TouchProcessor item) {
        if (item != null) {
            if (!this.touchWaitToDelete.contains(item)) {
                this.touchWaitToDelete.add(item);
            }
            if (this.touchWaitToAdd.contains(item)) {
                this.touchWaitToAdd.remove(item);
            }
        }
    }

    public void add(KeyProcessor item) {
        if (item != null) {
            if (!this.keyWaitToAdd.contains(item)) {
                this.keyWaitToAdd.add(item);
            }
            if (this.keyWaitToDelete.contains(item)) {
                this.keyWaitToDelete.remove(item);
            }
        }
    }

    public void remove(KeyProcessor item) {
        if (item != null) {
            if (!this.keyWaitToDelete.contains(item)) {
                this.keyWaitToDelete.add(item);
            }
            if (this.keyWaitToAdd.contains(item)) {
                this.keyWaitToAdd.remove(item);
            }
        }
    }

    public void flushInputs() {
        sync();
        if (getKeyCount() > 0) {
            for (int processorIndex = 0; processorIndex < this.keyProcessors.size(); processorIndex++) {
                this.keyProcessors.get(processorIndex).flushKeys(getKeys(), getKeyCount());
            }
        }
        int[] touches = getTouches();
        int count = getTouchCount();
        for (int i = 0; i < count; i++) {
            for (int processorIndex2 = 0; processorIndex2 < this.touchProcessors.size(); processorIndex2++) {
                this.touchProcessors.get(processorIndex2).touchAction(touches[i * 3], touches[(i * 3) + 1], touches[(i * 3) + 2]);
            }
            this.tapGesture.touchAction(touches[i * 3], touches[(i * 3) + 1], touches[(i * 3) + 2]);
        }
        switchBuffer();
    }

    public void cancelGestures() {
        this.tapGesture.cancel();
    }

    public int getKeyCount() {
        return this.buffers[this.outputBuffer].getKeyCount();
    }

    public int[] getKeys() {
        return this.buffers[this.outputBuffer].getKeys();
    }

    public int getTouchCount() {
        return this.buffers[this.outputBuffer].getTouchCount();
    }

    public int[] getTouches() {
        return this.buffers[this.outputBuffer].getTouches();
    }

    public void setStoped(boolean isStoped2) {
        this.isStoped = isStoped2;
    }

    public boolean isStoped() {
        return this.isStoped;
    }

    private void sync() {
        if (!this.touchWaitToAdd.isEmpty()) {
            Iterator<TouchProcessor> it = this.touchWaitToAdd.iterator();
            while (it.hasNext()) {
                TouchProcessor item = it.next();
                if (!this.touchProcessors.contains(item)) {
                    this.touchProcessors.add(item);
                }
            }
            this.touchWaitToAdd.clear();
        }
        if (!this.touchWaitToDelete.isEmpty()) {
            Iterator<TouchProcessor> it2 = this.touchWaitToDelete.iterator();
            while (it2.hasNext()) {
                this.touchProcessors.remove(it2.next());
            }
            this.touchWaitToDelete.clear();
        }
        if (!this.keyWaitToAdd.isEmpty()) {
            Iterator<KeyProcessor> it3 = this.keyWaitToAdd.iterator();
            while (it3.hasNext()) {
                KeyProcessor item2 = it3.next();
                if (!this.keyProcessors.contains(item2)) {
                    this.keyProcessors.add(item2);
                }
            }
            this.keyWaitToAdd.clear();
        }
        if (!this.keyWaitToDelete.isEmpty()) {
            Iterator<KeyProcessor> it4 = this.keyWaitToDelete.iterator();
            while (it4.hasNext()) {
                this.keyProcessors.remove(it4.next());
            }
            this.keyWaitToDelete.clear();
        }
    }

    public static class TouchBuffer {
        int MAX_COUNT = 128;
        final int dimention = 3;
        int keyCount;
        int[] keys = new int[32];
        int touchCount;
        int[] touches = new int[(this.MAX_COUNT * 3)];

        public void addKey(int keyCode) {
            if (this.keyCount < 32) {
                this.keys[this.keyCount] = keyCode;
                this.keyCount++;
            }
        }

        public void addTouch(int x, int y, int action) {
            int index = this.touchCount * 3;
            this.touches[index] = x;
            this.touches[index + 1] = y;
            this.touches[index + 2] = action;
            this.touchCount++;
            if (this.touchCount >= this.MAX_COUNT) {
                int newMaxCount = this.MAX_COUNT * 2;
                int[] newTouches = new int[(newMaxCount * 3)];
                System.arraycopy(this.touches, 0, newTouches, 0, this.MAX_COUNT * 3);
                this.touches = newTouches;
                this.MAX_COUNT = newMaxCount;
            }
        }

        public int getTouchCount() {
            return this.touchCount;
        }

        public int[] getTouches() {
            return this.touches;
        }

        public int getKeyCount() {
            return this.keyCount;
        }

        public int[] getKeys() {
            return this.keys;
        }

        public void clear() {
            this.touchCount = 0;
            this.keyCount = 0;
        }
    }
}
