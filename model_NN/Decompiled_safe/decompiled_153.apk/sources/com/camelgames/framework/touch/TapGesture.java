package com.camelgames.framework.touch;

import android.os.SystemClock;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.events.TapEvent;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.math.MathUtils;
import java.util.LinkedList;

public class TapGesture {
    public static final int radius = ((int) (0.03f * ((float) GraphicsManager.maxScreenSize())));
    public static final float tapLifeTime = 300.0f;
    private float[] downTouch = new float[3];
    private float[] previousTap = new float[3];
    private LinkedList<TapEvent> tapEventsPool = new LinkedList<>();

    public TapGesture() {
        setTouchEventPoolSize(64);
    }

    public void cancel() {
        float[] fArr = this.downTouch;
        this.previousTap[2] = 0.0f;
        fArr[2] = 0.0f;
    }

    public void setTouchEventPoolSize(int size) {
        this.tapEventsPool.clear();
        for (int i = 0; i < size; i++) {
            this.tapEventsPool.add(new TapEvent(EventType.SingleTap, 0.0f, 0.0f));
        }
    }

    public void touchAction(int x, int y, int action) {
        long currentTime = SystemClock.elapsedRealtime();
        if (action == 1) {
            if (MathUtils.length(((float) x) - this.downTouch[0], ((float) y) - this.downTouch[1]) < ((float) radius) && ((float) currentTime) - this.downTouch[2] < 300.0f) {
                postEvent(EventType.SingleTap, (float) x, (float) y);
                if (MathUtils.length(((float) x) - this.previousTap[0], ((float) y) - this.previousTap[1]) < ((float) radius) * 2.0f && ((float) currentTime) - this.previousTap[2] < 300.0f) {
                    postEvent(EventType.DoubleTap, (float) x, (float) y);
                }
                this.previousTap[0] = (float) x;
                this.previousTap[1] = (float) y;
                this.previousTap[2] = (float) currentTime;
            }
        } else if (action == 0) {
            this.downTouch[0] = (float) x;
            this.downTouch[1] = (float) y;
            this.downTouch[2] = (float) currentTime;
        }
    }

    private void postEvent(EventType type, float x, float y) {
        TapEvent e = pollTapEvent();
        e.set(type, x, y);
        EventManager.getInstance().postEvent(e);
    }

    private TapEvent pollTapEvent() {
        TapEvent e = this.tapEventsPool.poll();
        e.setCancel(false);
        this.tapEventsPool.add(e);
        return e;
    }
}
