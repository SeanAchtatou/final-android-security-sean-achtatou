package com.camelgames.framework.physics;

import com.box2d.b2Body;
import com.box2d.b2Joint;
import com.box2d.b2MouseJoint;
import com.box2d.b2MouseJointDef;
import com.box2d.b2Vec2;

public class MouseJoint {
    protected b2Body body;
    protected b2MouseJoint joint;
    private b2MouseJointDef jointDef = new b2MouseJointDef();

    public void removeJoint() {
        if (this.joint != null) {
            PhysicsBodyUtil.destroyJoint(this.joint);
            this.joint = null;
        }
    }

    public void createJoint(b2Body body2, float anchorX, float anchorY, float maxForce) {
        removeJoint();
        this.body = body2;
        this.jointDef.setBodyA(PhysicsManager.instance.getGroundBody());
        this.jointDef.setBodyB(body2);
        this.jointDef.setMaxForce(maxForce);
        this.jointDef.setTarget(new b2Vec2(PhysicsManager.screenToPhysics(anchorX), PhysicsManager.screenToPhysics(anchorY)));
        this.joint = new b2MouseJoint(b2Joint.getCPtr(PhysicsManager.instance.getWorld().CreateJoint(this.jointDef)), false);
    }

    public void createJoint(b2Body body2, float anchorX, float anchorY, float maxForce, float frequencyHz, float dampingRadio) {
        removeJoint();
        this.body = body2;
        this.jointDef.setBodyA(PhysicsManager.instance.getGroundBody());
        this.jointDef.setBodyB(body2);
        this.jointDef.setMaxForce(maxForce);
        this.jointDef.setFrequencyHz(frequencyHz);
        this.jointDef.setDampingRatio(dampingRadio);
        this.jointDef.setTarget(new b2Vec2(PhysicsManager.screenToPhysics(anchorX), PhysicsManager.screenToPhysics(anchorY)));
        this.joint = new b2MouseJoint(b2Joint.getCPtr(PhysicsManager.instance.getWorld().CreateJoint(this.jointDef)), false);
    }

    public b2Body getBody() {
        return this.body;
    }

    public float getMaxForce() {
        return this.joint.GetMaxForce();
    }

    public b2MouseJoint getJoint() {
        return this.joint;
    }

    public void setTarget(float targetX, float targetY) {
        if (this.joint != null) {
            this.joint.SetTarget(PhysicsManager.screenToPhysics(targetX), PhysicsManager.screenToPhysics(targetY));
        }
    }
}
