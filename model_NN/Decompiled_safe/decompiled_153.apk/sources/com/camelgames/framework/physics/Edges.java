package com.camelgames.framework.physics;

public class Edges {
    public float[] vertices;

    public int getCount() {
        return this.vertices.length / 2;
    }
}
