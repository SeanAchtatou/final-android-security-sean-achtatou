package com.camelgames.framework.physics;

import com.box2d.b2Body;
import com.box2d.b2DistanceJoint;
import com.box2d.b2DistanceJointDef;
import com.box2d.b2Joint;
import com.box2d.b2Vec2;
import com.camelgames.framework.math.Vector2;

public class DistanceJoint {
    protected Vector2 anchor1;
    protected Vector2 anchor2;
    protected b2Body body1;
    protected b2Body body2;
    protected b2DistanceJoint joint;

    public b2Body getBody1() {
        return this.body1;
    }

    public b2Body getBody2() {
        return this.body2;
    }

    public Vector2 getAnchor1() {
        return this.anchor1;
    }

    public Vector2 getAnchor2() {
        return this.anchor2;
    }

    public boolean isJointed() {
        return this.joint != null;
    }

    public void removeJoint() {
        if (this.joint != null) {
            PhysicsBodyUtil.destroyJoint(this.joint);
            this.joint = null;
        }
    }

    public void jointBodies(b2Body body12, b2Body body22, Vector2 anchor12, Vector2 anchor22, float length) {
        this.body1 = body12;
        this.body2 = body22;
        this.anchor1 = anchor12;
        this.anchor2 = anchor22;
        removeJoint();
        b2DistanceJointDef jointDef = new b2DistanceJointDef();
        jointDef.setLength(PhysicsManager.screenToPhysics(length));
        b2Vec2 b2Anchor1 = new b2Vec2();
        b2Vec2 b2Anchor2 = new b2Vec2();
        PhysicsBodyUtil.screenToPhysics(anchor12, b2Anchor1);
        PhysicsBodyUtil.screenToPhysics(anchor22, b2Anchor2);
        jointDef.Initialize(body12, body22, b2Anchor1, b2Anchor2);
        jointDef.setCollideConnected(false);
        this.joint = new b2DistanceJoint(b2Joint.getCPtr(PhysicsManager.instance.getWorld().CreateJoint(jointDef)), false);
        jointDef.delete();
    }

    public void setHZDamp(float frequencyHz, float dampingRatio) {
        this.joint.SetFrequency(frequencyHz);
        this.joint.SetDampingRatio(dampingRatio);
    }

    public b2DistanceJoint getJoint() {
        return this.joint;
    }
}
