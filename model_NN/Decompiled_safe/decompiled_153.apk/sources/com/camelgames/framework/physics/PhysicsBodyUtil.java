package com.camelgames.framework.physics;

import com.box2d.JNILibrary;
import com.box2d.b2Body;
import com.box2d.b2BodyDef;
import com.box2d.b2BodyType;
import com.box2d.b2Fixture;
import com.box2d.b2Joint;
import com.box2d.b2Vec2;
import com.camelgames.framework.math.Vector2;

public class PhysicsBodyUtil {
    private static final b2Vec2 b2Vec2Copy = new b2Vec2();
    private static final b2Vec2 b2Vec2Copy2 = new b2Vec2();
    private static float[] floatData = new float[4];
    private static final Vector2 vector2Copy = new Vector2();
    private b2Body body;
    private int bodyCPtr;
    private int categoryBits = 1;
    private int groupIndex = 0;
    private boolean isCollision = true;
    private int maskBits = 65535;

    public b2Body getBody() {
        return this.body;
    }

    public void setPosition(float screenX, float screenY) {
        setPosition(this.body, screenX, screenY);
    }

    public void getPosition(Vector2 position) {
        getPosition(this.body, position);
    }

    public void setLinearVelocity(float screenX, float screenY) {
        setLinearVelocity(this.body, screenX, screenY);
    }

    public void getLinearVelocity(Vector2 velocity) {
        getLinearVelocity(this.body, velocity);
    }

    public void setAngularVelocity(float angularVelocity) {
        this.body.SetAngularVelocity(angularVelocity);
    }

    public float getAngularVelocity() {
        return this.body.GetAngularVelocity();
    }

    public void zeroVelocity() {
        zeroVelocity(this.body);
    }

    public void scaleVelocity(float scale) {
        scaleVelocity(scale);
    }

    public void applyForce(float forceX, float forceY, float x, float y) {
        applyForce(this.body, forceX, forceY, x, y);
    }

    public void applyImpulse(float impulseX, float impulseY, float x, float y) {
        applyImpulse(this.body, impulseX, impulseY, x, y);
    }

    public void applyTorque(float torque) {
        applyTorque(this.body, torque);
    }

    public void setAngle(float angle) {
        setAngle(this.body, angle);
    }

    public float getAngle() {
        return getAngle(this.body);
    }

    public void setAwake(boolean isAwake) {
        if (this.body != null) {
            this.body.SetAwake(isAwake);
        }
    }

    public void setActive(boolean isActive) {
        if (this.body != null) {
            this.body.SetActive(isActive);
        }
    }

    public float GetMass() {
        return this.body.GetMass();
    }

    public void setMassCenter(float offsetX, float offsetY) {
        if (this.body != null) {
            this.body.SetMassCenter(PhysicsManager.screenToPhysics(offsetX), PhysicsManager.screenToPhysics(offsetY));
        }
    }

    public void setBody(b2Body body2) {
        this.body = body2;
    }

    public void setBody(int id, float screenX, float screenY, float angle) {
        destroyBody();
        this.body = createBody(screenX, screenY, angle);
        this.body.SetId(id);
        this.bodyCPtr = b2Body.getCPtr(this.body);
    }

    public float[] syncPhysics() {
        JNILibrary.syncData(this.bodyCPtr, floatData);
        return floatData;
    }

    public void destroyBody() {
        if (this.body != null) {
            destroyBody(this.body);
            this.body = null;
            this.isCollision = true;
        }
    }

    public void setFilterData(int categoryBits2, int maskBits2, int groupIndex2) {
        this.categoryBits = categoryBits2;
        this.maskBits = maskBits2;
        this.groupIndex = groupIndex2;
        setCollision(true);
    }

    public boolean isCollison() {
        return this.isCollision;
    }

    public void setCollision(boolean isCollision2) {
        if (this.body != null) {
            this.body.SetAwake(true);
            this.isCollision = isCollision2;
            if (isCollision2) {
                this.body.setFilter(this.categoryBits, this.maskBits, this.groupIndex);
            } else {
                this.body.setFilter(0, 0, 0);
            }
        }
    }

    public void setBodyType(b2BodyType type) {
        if (this.body != null) {
            this.body.SetType(type);
        }
    }

    public void setStatic() {
        if (this.body != null) {
            this.body.SetType(b2BodyType.b2_staticBody);
        }
    }

    public void setDynamic() {
        if (this.body != null) {
            this.body.SetType(b2BodyType.b2_dynamicBody);
        }
    }

    public static b2Body createBody(Vector2 screenPos, float angle) {
        return createBody(screenPos.X, screenPos.Y, angle);
    }

    public static b2Body createBody(float screenX, float screenY, float angle) {
        b2BodyDef bodyDef = new b2BodyDef();
        bodyDef.setType(b2BodyType.b2_dynamicBody);
        bodyDef.setPosition(PhysicsManager.screenToPhysics(screenX), PhysicsManager.screenToPhysics(screenY));
        bodyDef.setAngle(angle);
        b2Body body2 = PhysicsManager.instance.getWorld().CreateBody(bodyDef);
        bodyDef.delete();
        return body2;
    }

    public static void destroyBody(b2Body body2) {
        if (body2 != null) {
            PhysicsManager.instance.getWorld().DestroyBody(body2);
        }
    }

    public static void destroyJoint(b2Joint joint) {
        if (joint != null) {
            PhysicsManager.instance.getWorld().DestroyJoint(joint);
        }
    }

    public static boolean isInsideFixture(b2Fixture fixture, float x, float y) {
        if (fixture == null) {
            return false;
        }
        b2Vec2Copy.Set(PhysicsManager.screenToPhysics(x), PhysicsManager.screenToPhysics(y));
        return fixture.TestPoint(b2Vec2Copy);
    }

    public static boolean isActive(b2Body body2) {
        if (body2 != null) {
            return body2.IsActive();
        }
        return false;
    }

    public static void setPosition(b2Body body2, float screenX, float screenY) {
        if (body2 != null) {
            body2.SetPosition(PhysicsManager.screenToPhysics(screenX), PhysicsManager.screenToPhysics(screenY));
        }
    }

    public static void getPosition(b2Body body2, Vector2 position) {
        if (body2 != null) {
            position.set(PhysicsManager.physicsToScreen(body2.getPositionX()), PhysicsManager.physicsToScreen(body2.getPositionY()));
        }
    }

    public static float getAngle(b2Body body2) {
        if (body2 != null) {
            return body2.GetAngle();
        }
        return 0.0f;
    }

    public static void setAngle(b2Body body2, float angle) {
        if (body2 != null) {
            body2.SetAngle(angle);
        }
    }

    public static void setLinearVelocity(b2Body body2, float screenX, float screenY) {
        if (body2 != null) {
            body2.SetLinearVelocity(PhysicsManager.screenToPhysics(screenX), PhysicsManager.screenToPhysics(screenY));
        }
    }

    public static void getLinearVelocity(b2Body body2, Vector2 velocity) {
        if (body2 != null) {
            velocity.X = PhysicsManager.physicsToScreen(body2.GetLinearVelocityX());
            velocity.Y = PhysicsManager.physicsToScreen(body2.GetLinearVelocityY());
        }
    }

    public static void zeroVelocity(b2Body body2) {
        if (body2 != null) {
            body2.SetLinearVelocity(0.0f, 0.0f);
            body2.SetAngularVelocity(0.0f);
        }
    }

    public static void applyForce(b2Body body2, float forceX, float forceY, float x, float y) {
        if (body2 != null) {
            body2.ApplyForce(forceX, forceY, PhysicsManager.screenToPhysics(x), PhysicsManager.screenToPhysics(y));
        }
    }

    public static void applyImpulse(b2Body body2, float impulseX, float impulseY, float x, float y) {
        if (body2 != null) {
            body2.ApplyLinearImpulse(impulseX, impulseY, PhysicsManager.screenToPhysics(x), PhysicsManager.screenToPhysics(y));
        }
    }

    public static void applyTorque(b2Body body2, float torque) {
        if (body2 != null) {
            body2.ApplyTorque(torque);
        }
    }

    public static void getWorldPoint(b2Body body2, Vector2 localPoint, Vector2 worldPoint) {
        if (body2 != null) {
            screenToPhysics(localPoint, b2Vec2Copy);
            body2.GetWorldPoint(b2Vec2Copy, b2Vec2Copy2);
            physicsToScreen(b2Vec2Copy2, worldPoint);
        }
    }

    public static void getLocalPoint(b2Body body2, Vector2 worldPoint, Vector2 localPoint) {
        if (body2 != null) {
            screenToPhysics(worldPoint, b2Vec2Copy);
            body2.GetLocalPoint(b2Vec2Copy, b2Vec2Copy2);
            physicsToScreen(b2Vec2Copy2, localPoint);
        }
    }

    public static void screenToPhysics(Vector2 v, b2Vec2 b2V) {
        vector2Copy.set(v);
        PhysicsManager.screenToPhysics(vector2Copy);
        b2V.Set(vector2Copy.X, vector2Copy.Y);
    }

    private static void physicsToScreen(b2Vec2 b2V, Vector2 v) {
        v.set(b2V.getX(), b2V.getY());
        PhysicsManager.physicsToScreen(v);
    }
}
