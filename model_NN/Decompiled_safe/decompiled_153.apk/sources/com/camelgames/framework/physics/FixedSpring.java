package com.camelgames.framework.physics;

import com.box2d.b2Body;
import com.camelgames.framework.math.Vector2;

public class FixedSpring extends Spring {
    private PrismaticJoint prismaticJoint;

    public FixedSpring(b2Body body1, b2Body body2, float k, float damping, Vector2 anchorWorld1, Vector2 anchorWorld2) {
        this(body1, body2, k, damping, anchorWorld1, anchorWorld2, -0.8f, 1.0f);
    }

    public FixedSpring(b2Body body1, b2Body body2, float k, float damping, Vector2 anchorWorld1, Vector2 anchorWorld2, float lowerTranslationScale, float upperTranslationScale) {
        super(body1, body2, k, damping, anchorWorld1, anchorWorld2);
        Vector2 axis = Vector2.substract(anchorWorld2, anchorWorld1);
        float length = axis.normalize();
        this.prismaticJoint = new PrismaticJoint(body1, body2, anchorWorld2, axis);
        this.prismaticJoint.jointBodies(lowerTranslationScale * length, upperTranslationScale * length);
    }
}
