package com.camelgames.framework.physics;

import com.box2d.b2Body;
import com.box2d.b2BodyDef;
import com.box2d.b2BodyType;
import com.box2d.b2ContactWrapper;
import com.box2d.b2Fixture;
import com.box2d.b2MyContactListener;
import com.box2d.b2MyHelper;
import com.box2d.b2MyVertices;
import com.box2d.b2SensorContactWrapper;
import com.box2d.b2Vec2;
import com.box2d.b2World;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.events.CollisionEvent;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.events.SensorContactEvent;
import com.camelgames.framework.math.Vector2;
import java.util.LinkedList;

public final class PhysicsManager {
    public static final PhysicsManager instance = new PhysicsManager();
    private static float scaleIn = 0.05f;
    private static float scaleOut = (1.0f / scaleIn);
    private ContactsProcessor contactProcessor;
    private float elapsedTime;
    private float gravityX;
    private float gravityY;
    private b2Body groundBody;
    private boolean isInitialized = false;
    private boolean isStoped;
    private float lowerX;
    private float lowerY;
    private b2MyHelper myHelper;
    private float upperX;
    private float upperY;
    private b2World world;

    private PhysicsManager() {
    }

    public boolean isInitialized() {
        return this.isInitialized;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.box2d.b2World.<init>(com.box2d.b2Vec2, boolean):void
     arg types: [com.box2d.b2Vec2, int]
     candidates:
      com.box2d.b2World.<init>(int, boolean):void
      com.box2d.b2World.<init>(com.box2d.b2Vec2, boolean):void */
    public void initiate(float lowerX2, float lowerY2, float upperX2, float upperY2, float gravity) {
        if (!this.isInitialized) {
            this.lowerX = lowerX2;
            this.lowerY = lowerY2;
            this.upperX = upperX2;
            this.upperY = upperY2;
            this.gravityY = gravity;
            this.world = new b2World(new b2Vec2(0.0f, gravity), true);
            this.myHelper = new b2MyHelper(this.world);
            this.contactProcessor = new ContactsProcessor(this.myHelper.getContactListener());
            this.isInitialized = true;
        }
    }

    public boolean isOutOfBoundary(float x, float y) {
        return x < this.lowerX || x > this.upperX || y < this.lowerY || y > this.upperY;
    }

    public float getLowerX() {
        return this.lowerX;
    }

    public float getUpperX() {
        return this.upperX;
    }

    public float getLowerY() {
        return this.lowerY;
    }

    public float getUpperY() {
        return this.upperY;
    }

    public void addEdgesToGround(Edges edge, float friction, float restitution) {
        if (this.groundBody == null) {
            createGroundBody();
        }
        createEdges(this.groundBody, edge, friction, restitution);
        this.groundBody.setFilter(1, 65535, -1);
    }

    public void addLoopsToGround(Edges edge, float friction, float restitution) {
        if (this.groundBody == null) {
            createGroundBody();
        }
        createLoops(this.groundBody, edge, friction, restitution);
        this.groundBody.setFilter(1, 65535, -1);
    }

    public void addRectToGround(float x, float y, float width, float height, float friction, float restitution) {
        if (this.groundBody == null) {
            createGroundBody();
        }
        createRect(this.groundBody, width, height, x, y, 0.0f, 1.0f, friction, restitution);
        this.groundBody.setFilter(1, 65535, -1);
    }

    public void destoryGround() {
        if (this.groundBody != null) {
            EventManager.getInstance().postEvent(new AbstractEvent(EventType.StaticEdgesDestroyed));
            this.world.DestroyBody(this.groundBody);
            this.groundBody.delete();
            this.groundBody = null;
        }
    }

    public b2Body getGroundBody() {
        if (this.groundBody == null) {
            createGroundBody();
        }
        return this.groundBody;
    }

    public b2World getWorld() {
        return this.world;
    }

    public float getGravityX() {
        return this.gravityX;
    }

    public float getGravityY() {
        return this.gravityY;
    }

    public float getVelocityYByGravity(float distance) {
        return (float) Math.sqrt((double) Math.abs(2.0f * this.gravityY * distance * scaleOut));
    }

    public void setGravity(float gravityX2, float gravityY2) {
        this.gravityX = gravityX2;
        this.gravityY = gravityY2;
        this.world.SetGravity(new b2Vec2(gravityX2, gravityY2));
    }

    public void setStoped(boolean isStoped2) {
        clearContacts();
        this.isStoped = isStoped2;
    }

    public void clearContacts() {
        this.contactProcessor.clear();
    }

    public boolean isStoped() {
        return this.isStoped;
    }

    public void update(float elapsedTime2, int velocityIterations, int positionIterations) {
        if (!this.isStoped) {
            this.elapsedTime = elapsedTime2;
            this.world.Step(this.elapsedTime, velocityIterations, positionIterations);
            this.contactProcessor.handCollision();
        }
    }

    public float getElapsedTime() {
        return this.elapsedTime;
    }

    public static float screenToPhysics(float screenNum) {
        return scaleIn * screenNum;
    }

    public static float physicsToScreen(float physicsNum) {
        return scaleOut * physicsNum;
    }

    public static void screenToPhysics(Vector2 vec2) {
        vec2.set(screenToPhysics(vec2.X), screenToPhysics(vec2.Y));
    }

    public static void physicsToScreen(Vector2 vec2) {
        vec2.set(physicsToScreen(vec2.X), physicsToScreen(vec2.Y));
    }

    public void createEdges(b2Body body, Edges edge, float friction, float restitution) {
        int count = edge.getCount();
        b2MyVertices myVertices = new b2MyVertices(count);
        for (int i = 0; i < count; i++) {
            myVertices.Set(screenToPhysics(edge.vertices[i * 2]), screenToPhysics(edge.vertices[(i * 2) + 1]), i);
        }
        this.myHelper.CreateEdges(body, myVertices, 1.0f, friction, restitution);
    }

    public void createLoops(b2Body body, Edges edge, float friction, float restitution) {
        int count = edge.getCount();
        b2MyVertices myVertices = new b2MyVertices(count);
        for (int i = 0; i < count; i++) {
            myVertices.Set(screenToPhysics(edge.vertices[i * 2]), screenToPhysics(edge.vertices[(i * 2) + 1]), i);
        }
        this.myHelper.CreateLoop(body, myVertices, 1.0f, friction, restitution);
    }

    public b2Fixture createCircle(b2Body body, float radius, float density, float friction, float restitution) {
        return this.myHelper.CreateCircle(body, screenToPhysics(radius), density, friction, restitution);
    }

    public b2Fixture createCircle(b2Body body, float radius, float centerX, float centerY, float density, float friction, float restitution) {
        return this.myHelper.CreateCircle(body, screenToPhysics(radius), screenToPhysics(centerX), screenToPhysics(centerY), density, friction, restitution);
    }

    public b2Fixture createRect(b2Body body, float width, float height, float density, float friction, float restitution) {
        return this.myHelper.CreateRect(body, screenToPhysics(width), screenToPhysics(height), density, friction, restitution);
    }

    public b2Fixture createRect(b2Body body, float width, float height, float centerX, float centerY, float angle, float density, float friction, float restitution) {
        return this.myHelper.CreateRect(body, screenToPhysics(width), screenToPhysics(height), screenToPhysics(centerX), screenToPhysics(centerY), angle, density, friction, restitution);
    }

    public b2Fixture createPolygon(b2Body body, float[] vertices, int count, float density, float friction, float restitution) {
        b2MyVertices myVertices = new b2MyVertices(count);
        for (int i = 0; i < count; i++) {
            myVertices.Set(screenToPhysics(vertices[i * 2]), screenToPhysics(vertices[(i * 2) + 1]), i);
        }
        return this.myHelper.CreatePolygon(body, myVertices, density, friction, restitution);
    }

    public void createProfileBoxes(b2Body body, float[] vertices, int count, float thick, float density, float friction, float restitution) {
        b2MyVertices myVertices = new b2MyVertices(count);
        for (int i = 0; i < count; i++) {
            myVertices.Set(screenToPhysics(vertices[i * 2]), screenToPhysics(vertices[(i * 2) + 1]), i);
        }
        this.myHelper.CreateProfileBoxes(body, myVertices, screenToPhysics(thick), density, friction, restitution);
    }

    public void setCollisionImpulseThreshold(float impluseThreshold) {
        this.contactProcessor.setCollisionImpulseThreshold(impluseThreshold);
    }

    public void setCollisionPool(int contactCount, int sensorCount) {
        this.contactProcessor.setCollisionPool(contactCount, sensorCount);
    }

    public b2MyHelper getHelper() {
        return this.myHelper;
    }

    private void createGroundBody() {
        b2BodyDef groundBodyDef = new b2BodyDef();
        groundBodyDef.setPosition(0.0f, 0.0f);
        groundBodyDef.setType(b2BodyType.b2_staticBody);
        this.groundBody = this.world.CreateBody(groundBodyDef);
        this.groundBody.SetId(-1);
        groundBodyDef.delete();
    }

    public static void setScaleIn(float scaleIn2) {
        scaleIn = scaleIn2;
        scaleOut = 1.0f / scaleIn2;
    }

    public static float getScaleIn() {
        return scaleIn;
    }

    private static class ContactsProcessor {
        private LinkedList<CollisionEvent> collisionEventsPool = new LinkedList<>();
        private b2MyContactListener contactListener;
        private b2ContactWrapper[] contacts;
        private LinkedList<SensorContactEvent> sensorContactEventsPool = new LinkedList<>();
        private b2SensorContactWrapper[] sensorContacts;

        public ContactsProcessor(b2MyContactListener contactListener2) {
            this.contactListener = contactListener2;
            setCollisionPool(32, 32);
        }

        public void setCollisionPool(int contactCount, int sensorCount) {
            this.contactListener.clear();
            this.contactListener.initiate(contactCount, sensorCount);
            this.contacts = new b2ContactWrapper[contactCount];
            for (int i = 0; i < contactCount; i++) {
                this.contacts[i] = new b2ContactWrapper();
                this.contactListener.put(i, this.contacts[i]);
            }
            this.sensorContacts = new b2SensorContactWrapper[sensorCount];
            for (int i2 = 0; i2 < sensorCount; i2++) {
                this.sensorContacts[i2] = new b2SensorContactWrapper();
                this.contactListener.put(i2, this.sensorContacts[i2]);
            }
            this.collisionEventsPool.clear();
            for (int i3 = 0; i3 < contactCount; i3++) {
                this.collisionEventsPool.add(new CollisionEvent(null));
            }
            this.sensorContactEventsPool.clear();
            for (int i4 = 0; i4 < sensorCount; i4++) {
                this.sensorContactEventsPool.add(new SensorContactEvent(null));
            }
        }

        public void handCollision() {
            int contactCount = Math.min(this.collisionEventsPool.size(), this.contactListener.getContactCount());
            for (int i = 0; i < contactCount; i++) {
                CollisionEvent ce = pollCollisionEvent();
                ce.setContact(this.contacts[i]);
                EventManager.getInstance().postEvent(ce);
            }
            int sensorCount = Math.min(this.sensorContactEventsPool.size(), this.contactListener.getSensorContactCount());
            for (int i2 = 0; i2 < sensorCount; i2++) {
                SensorContactEvent se = pollSensorEvent();
                se.setContact(this.sensorContacts[i2]);
                EventManager.getInstance().postEvent(se);
            }
            this.contactListener.clear();
        }

        public void setCollisionImpulseThreshold(float impluseThreshold) {
            this.contactListener.setImpulseThreshold(impluseThreshold);
        }

        public void clear() {
            this.contactListener.clear();
        }

        private CollisionEvent pollCollisionEvent() {
            CollisionEvent e = this.collisionEventsPool.poll();
            e.setCancel(false);
            this.collisionEventsPool.add(e);
            return e;
        }

        private SensorContactEvent pollSensorEvent() {
            SensorContactEvent e = this.sensorContactEventsPool.poll();
            e.setCancel(false);
            this.sensorContactEventsPool.add(e);
            return e;
        }
    }
}
