package com.camelgames.framework.physics;

import com.box2d.b2Body;
import com.box2d.b2Joint;
import com.box2d.b2RevoluteJoint;
import com.box2d.b2RevoluteJointDef;
import com.box2d.b2Vec2;
import com.camelgames.framework.math.Vector2;

public class Bridge {
    private b2Body[] bodies;
    private int[] bodyCPtrs;
    private int brickCount;
    private float brickHeight;
    private float brickWidth;
    private boolean isActive;
    private b2RevoluteJointDef jointDef = new b2RevoluteJointDef();
    private b2RevoluteJoint[] joints;

    public static Bridge createFixedBridge(float brickWidth2, float brickHeight2, b2Body body1, Vector2 anchorWorld1, b2Body body2, Vector2 anchorWorld2, int brickCount2, float density, float friction, float restitution) {
        Bridge bridge = new Bridge(anchorWorld1, anchorWorld2, brickWidth2, brickHeight2, brickCount2, density, friction, restitution);
        bridge.attachAnchorHead(body1, anchorWorld1);
        bridge.attachAnchorTail(body2, anchorWorld2);
        return bridge;
    }

    public Bridge(Vector2 anchorWorld1, Vector2 anchorWorld2, float brickWidth2, float brickHeight2, int brickCount2, float density, float friction, float restitution) {
        this.brickWidth = brickWidth2;
        this.brickHeight = brickHeight2;
        this.brickCount = brickCount2;
        Vector2 v = Vector2.substract(anchorWorld2, anchorWorld1);
        float length = v.normalize();
        float angle = v.getAngle();
        float gap = ((((float) brickCount2) * brickWidth2) - length) / ((float) (brickCount2 - 1));
        this.joints = new b2RevoluteJoint[(brickCount2 + 1)];
        this.bodies = new b2Body[brickCount2];
        b2Body previousBody = null;
        for (int i = 0; i < brickCount2; i++) {
            float slot = (0.5f * brickWidth2) + ((brickWidth2 - gap) * ((float) i));
            float x = anchorWorld1.X + (v.X * slot);
            float y = anchorWorld1.Y + (v.Y * slot);
            b2Body body = PhysicsBodyUtil.createBody(x, y, angle);
            PhysicsManager.instance.createRect(body, brickWidth2, brickHeight2, 0.0f, 0.0f, angle, density, friction, restitution);
            if (previousBody != null) {
                float jointOffset = 0.5f * (brickWidth2 - gap);
                this.joints[i] = jointBodies(previousBody, body, new Vector2(x - (v.X * jointOffset), y - (v.Y * jointOffset)));
            }
            this.bodies[i] = body;
            previousBody = body;
        }
        this.bodyCPtrs = new int[this.bodies.length];
        for (int i2 = 0; i2 < this.bodyCPtrs.length; i2++) {
            this.bodyCPtrs[i2] = b2Body.getCPtr(this.bodies[i2]);
        }
        this.isActive = true;
    }

    private b2RevoluteJoint jointBodies(b2Body body1, b2Body body2, Vector2 anchor) {
        this.jointDef.setCollideConnected(false);
        b2Vec2 b2Vec2Anchor = new b2Vec2();
        PhysicsBodyUtil.screenToPhysics(anchor, b2Vec2Anchor);
        this.jointDef.Initialize(body1, body2, b2Vec2Anchor);
        return new b2RevoluteJoint(b2Joint.getCPtr(PhysicsManager.instance.getWorld().CreateJoint(this.jointDef)), false);
    }

    public void dispose() {
        for (b2Body body : this.bodies) {
            PhysicsBodyUtil.destroyBody(body);
        }
        this.bodies = null;
        this.joints = null;
    }

    public void setHeadPosition(float x, float y, float angle) {
        b2Body headBody = this.bodies[0];
        PhysicsBodyUtil.setPosition(headBody, x, y);
        PhysicsBodyUtil.setAngle(headBody, angle);
    }

    public void setTailPosition(float x, float y, float angle) {
        b2Body tailBody = this.bodies[this.bodies.length - 1];
        PhysicsBodyUtil.setPosition(tailBody, x, y);
        PhysicsBodyUtil.setAngle(tailBody, angle);
    }

    public void setPosition(Vector2 anchorWorld1, Vector2 anchorWorld2) {
        Vector2 v = Vector2.substract(anchorWorld2, anchorWorld1);
        float length = v.normalize();
        float angle = v.getAngle();
        float gap = ((((float) this.brickCount) * this.brickWidth) - length) / ((float) (this.brickCount - 1));
        for (int i = 0; i < this.brickCount; i++) {
            float slot = (0.5f * this.brickWidth) + ((this.brickWidth - gap) * ((float) i));
            PhysicsBodyUtil.setPosition(this.bodies[i], anchorWorld1.X + (v.X * slot), anchorWorld1.Y + (v.Y * slot));
            PhysicsBodyUtil.setAngle(this.bodies[i], angle);
        }
    }

    public void attachAnchorHead(b2Body body, Vector2 anchorWorld) {
        b2RevoluteJoint joint = jointBodies(body, this.bodies[0], anchorWorld);
        if (this.joints[0] != null) {
            PhysicsBodyUtil.destroyJoint(this.joints[0]);
        }
        this.joints[0] = joint;
    }

    public void attachAnchorTail(b2Body body, Vector2 anchorWorld) {
        b2RevoluteJoint joint = jointBodies(this.bodies[this.bodies.length - 1], body, anchorWorld);
        if (this.joints[this.joints.length - 1] != null) {
            PhysicsBodyUtil.destroyJoint(this.joints[this.joints.length - 1]);
        }
        this.joints[this.joints.length - 1] = joint;
    }

    public void setNoCollision() {
        for (b2Body body : this.bodies) {
            body.setFilter(0, 0, 0);
        }
    }

    public float getBrickWidth() {
        return this.brickWidth;
    }

    public float getBrickHeight() {
        return this.brickHeight;
    }

    public int getBrickCount() {
        return this.brickCount;
    }

    public int[] getBodyCPtrs() {
        return this.bodyCPtrs;
    }

    public b2Body[] getBodies() {
        return this.bodies;
    }

    public b2Joint[] getJoints() {
        return this.joints;
    }

    public void setJointMotoTorque(float motorSpeed, float torque) {
        for (b2RevoluteJoint joint : this.joints) {
            if (joint != null) {
                joint.SetMotorSpeed(motorSpeed);
                joint.SetMaxMotorTorque(torque);
            }
        }
    }

    public float getBrickMass() {
        return this.bodies[0].GetMass();
    }

    public float getTotalMass() {
        return getBrickMass() * ((float) this.bodies.length);
    }

    public boolean isActive() {
        return this.isActive;
    }

    public void setActive(boolean isActive2) {
        if (this.isActive != isActive2) {
            for (b2Body body : this.bodies) {
                body.SetActive(isActive2);
            }
            this.isActive = isActive2;
        }
    }
}
