package com.camelgames.framework.physics;

import com.box2d.b2Body;
import com.box2d.b2Joint;
import com.box2d.b2RevoluteJoint;
import com.box2d.b2RevoluteJointDef;
import com.box2d.b2Vec2;
import com.camelgames.framework.math.Vector2;

public class RevoluteJoint {
    protected Vector2 anchorLocal1 = new Vector2();
    protected Vector2 anchorWorld = new Vector2();
    protected b2Body body1;
    protected b2Body body2;
    protected b2RevoluteJoint joint;
    protected Vector2 position = new Vector2();

    public void setBody1(b2Body body) {
        this.body1 = body;
        this.joint = null;
    }

    public b2Body getBody1() {
        return this.body1;
    }

    public void setBody2(b2Body body) {
        this.body2 = body;
        this.joint = null;
    }

    public b2Body getBody2() {
        return this.body2;
    }

    public boolean isJointed() {
        return this.joint != null;
    }

    public void setAnchor(float anchorX, float anchorY) {
        this.anchorWorld.set(anchorX, anchorY);
    }

    public void jointBodies(b2Body body12, b2Body body22, float anchorX, float anchorY, float lowerAngle, float upperAngle) {
        setBody1(body12);
        setBody2(body22);
        setAnchor(anchorX, anchorY);
        jointBodies(0.0f, 0.0f, lowerAngle, upperAngle, false);
    }

    public void jointBodies() {
        jointBodies(0.0f, 0.0f, 0.0f, 0.0f, false);
    }

    public void jointBodies(float motorSpeed, float maxMotorTorque, float lowerAngle, float upperAngle, boolean isCollideConnected) {
        if (this.body1 != null && this.body2 != null) {
            removeJoint();
            b2RevoluteJointDef jointDef = new b2RevoluteJointDef();
            b2Vec2 anchor = new b2Vec2();
            PhysicsBodyUtil.screenToPhysics(this.anchorWorld, anchor);
            jointDef.Initialize(this.body1, this.body2, anchor);
            jointDef.setCollideConnected(isCollideConnected);
            if (Math.abs(motorSpeed) > 0.001f) {
                jointDef.setMaxMotorTorque(maxMotorTorque);
                jointDef.setMotorSpeed(motorSpeed);
                jointDef.setEnableMotor(true);
            }
            if (!(lowerAngle == 0.0f && upperAngle == 0.0f)) {
                jointDef.setEnableLimit(true);
                jointDef.setLowerAngle(lowerAngle);
                jointDef.setUpperAngle(upperAngle);
            }
            this.joint = new b2RevoluteJoint(b2Joint.getCPtr(PhysicsManager.instance.getWorld().CreateJoint(jointDef)), false);
            PhysicsBodyUtil.getLocalPoint(this.body1, this.anchorWorld, this.anchorLocal1);
            jointDef.delete();
        }
    }

    public Vector2 getPosition() {
        PhysicsBodyUtil.getWorldPoint(this.body1, this.anchorLocal1, this.position);
        return this.position;
    }

    public float getAngle() {
        return this.body2.GetAngle();
    }

    public void removeJoint() {
        if (this.joint != null) {
            PhysicsBodyUtil.destroyJoint(this.joint);
            this.joint = null;
        }
    }

    public void EnableMotor(boolean flag) {
        this.joint.EnableMotor(flag);
    }

    public void SetMotorSpeed(float speed) {
        this.joint.SetMotorSpeed(speed);
    }

    public void SetMaxMotorTorque(float torque) {
        this.joint.SetMaxMotorTorque(torque);
    }

    public float getJointSpeed() {
        return this.joint.GetJointSpeed();
    }

    public float getJointAngle() {
        return this.joint.GetJointAngle();
    }

    public float getMotorTorque() {
        return this.joint.GetMotorTorque(1.0f);
    }

    public b2RevoluteJoint getJoint() {
        return this.joint;
    }
}
