package com.camelgames.framework.physics;

import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.ndk.graphics.Sprite2D;
import javax.microedition.khronos.opengles.GL10;

public class TextureEntity extends PhysicsalRenderableListenerEntity {
    protected Sprite2D texture;

    public TextureEntity() {
        this.texture = new Sprite2D();
    }

    public TextureEntity(int resId) {
        this.texture = new Sprite2D();
        this.texture.setTexId(resId);
    }

    public TextureEntity(float width, float height) {
        this.texture = new Sprite2D(width, height);
        super.setSize(width, height);
    }

    public void render(GL10 gl, float elapsedTime) {
        this.texture.render(elapsedTime);
    }

    public void setTexId(int resourceId) {
        this.texture.setTexId(resourceId);
    }

    public Sprite2D getSprite() {
        return this.texture;
    }

    public void setSizeByPixelScale(float scale) {
        this.texture.setSizeByPixelScale(scale);
        super.setSize(this.texture.getWidth(), this.texture.getHeight());
    }

    public void setWidthConstrainProportion(float width) {
        this.texture.setWidthConstrainProportion(width);
        super.setSize(this.texture.getWidth(), this.texture.getHeight());
    }

    public void setHeightConstrainProportion(float height) {
        this.texture.setHeightConstrainProportion(height);
        super.setSize(this.texture.getWidth(), this.texture.getHeight());
    }

    public void setSize(float width, float height) {
        this.texture.setSize(width, height);
        super.setSize(width, height);
    }

    public void setCenterTop(float centerX, float top) {
        setPosition(centerX, (0.5f * getHeight()) + top);
    }

    public void setCenterBottom(float centerX, float bottom) {
        setPosition(centerX, bottom - (0.5f * getHeight()));
    }

    public void SyncPhysicsPosition() {
        super.SyncPhysicsPosition();
        this.texture.setPosition(this.position.X, this.position.Y, this.angle);
    }

    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        this.texture.setPosition(x, y);
    }

    public void setAngle(float angle) {
        super.setAngle(angle);
        this.texture.setAngle(angle);
    }

    public void setColor(float r, float g, float b) {
        this.texture.setColor(r, g, b);
    }

    public void setOpacity(float alpha) {
        this.texture.setAlpha(alpha);
    }

    public void HandleEvent(AbstractEvent e) {
    }

    public void update(float elapsedTime) {
        SyncPhysicsPosition();
    }
}
