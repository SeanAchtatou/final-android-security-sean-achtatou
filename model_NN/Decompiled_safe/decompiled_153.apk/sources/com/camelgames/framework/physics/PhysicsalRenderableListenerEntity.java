package com.camelgames.framework.physics;

import com.box2d.b2Body;
import com.box2d.b2BodyType;
import com.camelgames.framework.Skeleton.RenderableListenerEntity;
import com.camelgames.framework.math.MathUtils;
import com.camelgames.framework.math.Vector2;

public abstract class PhysicsalRenderableListenerEntity extends RenderableListenerEntity {
    private static final boolean hasPhysicsBoundary;
    private static final float lowerX = PhysicsManager.instance.getLowerX();
    private static final float lowerY = PhysicsManager.instance.getLowerY();
    private static final float upperX = PhysicsManager.instance.getUpperX();
    private static final float upperY = PhysicsManager.instance.getUpperY();
    private float fixedAngle;
    private Vector2 fixedPosition;
    private boolean isActive = false;
    private boolean isAwake = true;
    private boolean isFixedPosition;
    private PhysicsBodyUtil physicsBodyUtil = new PhysicsBodyUtil();

    static {
        boolean z;
        if (lowerX == upperX || lowerY == upperY) {
            z = false;
        } else {
            z = true;
        }
        hasPhysicsBoundary = z;
    }

    /* access modifiers changed from: protected */
    public void disposeInternal() {
        destroyBody();
        super.disposeInternal();
    }

    public void setAwake(boolean isAwake2) {
        this.physicsBodyUtil.setAwake(isAwake2);
        this.isAwake = isAwake2;
    }

    public boolean isAwake() {
        return this.isAwake;
    }

    public void setActive(boolean isActive2) {
        this.physicsBodyUtil.setActive(isActive2);
        this.isActive = isActive2;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public void getLinearVelocity(Vector2 velocity) {
        this.physicsBodyUtil.getLinearVelocity(velocity);
    }

    public void setLinearVelocity(float screenX, float screenY) {
        this.physicsBodyUtil.setLinearVelocity(screenX, screenY);
    }

    public void scaleVelocity(float scale) {
        this.physicsBodyUtil.scaleVelocity(scale);
    }

    public float getAngularVelocity() {
        return this.physicsBodyUtil.getAngularVelocity();
    }

    public void setAngularVelocity(float angularVelocity) {
        this.physicsBodyUtil.setAngularVelocity(angularVelocity);
    }

    public void applyForce(float forceX, float forceY) {
        this.physicsBodyUtil.applyForce(forceX, forceY, this.position.X, this.position.Y);
    }

    public void applyForce(float forceX, float forceY, float x, float y) {
        this.physicsBodyUtil.applyForce(forceX, forceY, x, y);
    }

    public void applyImpulse(float impulseX, float impulseY) {
        this.physicsBodyUtil.applyImpulse(impulseX, impulseY, this.position.X, this.position.Y);
    }

    public void applyTorque(float torque) {
        this.physicsBodyUtil.applyTorque(torque);
    }

    public boolean isOutOfBoundary() {
        return this.position.X < lowerX || this.position.X > upperX || this.position.Y < lowerY || this.position.Y > upperY;
    }

    public void setAngle(float angle) {
        super.setAngle(angle);
        this.fixedAngle = this.angle;
        this.physicsBodyUtil.setAngle(this.angle);
    }

    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        setOnlyPhysicsPosition(x, y);
    }

    public void setOnlyPhysicsPosition(float x, float y) {
        this.physicsBodyUtil.setPosition(x, y);
    }

    public b2Body getBody() {
        return this.physicsBodyUtil.getBody();
    }

    public void SyncPhysicsPosition() {
        boolean z;
        if (this.isActive) {
            float[] floatData = this.physicsBodyUtil.syncPhysics();
            if (floatData[0] != 0.0f) {
                z = true;
            } else {
                z = false;
            }
            this.isAwake = z;
            if (this.isAwake) {
                if (this.isFixedPosition) {
                    setPosition(PhysicsManager.screenToPhysics(this.fixedPosition.X), PhysicsManager.screenToPhysics(this.fixedPosition.Y));
                    setAngle(this.fixedAngle);
                    this.physicsBodyUtil.zeroVelocity();
                } else {
                    this.position.X = PhysicsManager.physicsToScreen(floatData[1]);
                    this.position.Y = PhysicsManager.physicsToScreen(floatData[2]);
                    this.angle = MathUtils.constrainPi(floatData[3]);
                }
            }
            if (hasPhysicsBoundary && isOutOfBoundary()) {
                setActive(false);
            }
        }
    }

    public void zeroVelocity() {
        this.physicsBodyUtil.zeroVelocity();
    }

    public void setFilterData(int categoryBits, int maskBits, int groupIndex) {
        this.physicsBodyUtil.setFilterData(categoryBits, maskBits, groupIndex);
    }

    public void setCollision(boolean isCollision) {
        this.physicsBodyUtil.setCollision(isCollision);
    }

    public void setStatic() {
        this.physicsBodyUtil.setBodyType(b2BodyType.b2_staticBody);
    }

    public void setDynamic() {
        this.physicsBodyUtil.setBodyType(b2BodyType.b2_dynamicBody);
    }

    public b2Body createBody() {
        this.physicsBodyUtil.setBody(getId(), getX(), getY(), getAngle());
        this.isActive = true;
        return this.physicsBodyUtil.getBody();
    }

    public void destroyBody() {
        this.physicsBodyUtil.destroyBody();
        this.isActive = false;
    }

    public float GetMass() {
        return this.physicsBodyUtil.GetMass();
    }

    public void setMassCenter(float offsetX, float offsetY) {
        this.physicsBodyUtil.setMassCenter(offsetX, offsetY);
    }

    public void setFixedPosition(boolean isFixedPosition2) {
        this.isFixedPosition = isFixedPosition2;
        if (isFixedPosition2) {
            if (this.fixedPosition == null) {
                this.fixedPosition = new Vector2();
            }
            this.fixedPosition.set(this.position);
            this.fixedAngle = this.angle;
        }
    }
}
