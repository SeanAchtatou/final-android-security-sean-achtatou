package com.camelgames.framework.physics;

import com.box2d.b2Body;
import com.box2d.b2Joint;
import com.box2d.b2LineJoint;
import com.box2d.b2LineJointDef;
import com.box2d.b2Vec2;
import com.camelgames.framework.math.Vector2;

public class LineJoint {
    protected Vector2 anchor;
    protected Vector2 axis;
    protected b2Body body1;
    protected b2Body body2;
    protected b2LineJoint joint;

    public LineJoint(b2Body body12, b2Body body22, Vector2 anchor2, Vector2 axis2) {
        this.body1 = body12;
        this.body2 = body22;
        this.anchor = anchor2;
        this.axis = axis2;
        this.axis.normalize();
    }

    public b2Body getBody1() {
        return this.body1;
    }

    public b2Body getBody2() {
        return this.body2;
    }

    public Vector2 getAnchor() {
        return this.anchor;
    }

    public Vector2 getAxis() {
        return this.axis;
    }

    public boolean isJointed() {
        return this.joint != null;
    }

    public void removeJoint() {
        if (this.joint != null) {
            PhysicsBodyUtil.destroyJoint(this.joint);
            this.joint = null;
        }
    }

    public void jointBodies(float lowerTranslation, float upperTranslation) {
        jointBodies(lowerTranslation, upperTranslation, 0.0f, 0.0f);
    }

    public void jointBodies(float lowerTranslation, float upperTranslation, float motorForce, float motorSpeed) {
        removeJoint();
        b2LineJointDef jointDef = new b2LineJointDef();
        b2Vec2 b2VecAnchor = new b2Vec2();
        b2Vec2 b2VecAxis = new b2Vec2();
        PhysicsBodyUtil.screenToPhysics(this.anchor, b2VecAnchor);
        b2VecAxis.Set(this.axis.X, this.axis.Y);
        jointDef.Initialize(this.body1, this.body2, b2VecAnchor, b2VecAxis);
        jointDef.setLowerTranslation(PhysicsManager.screenToPhysics(lowerTranslation));
        jointDef.setUpperTranslation(PhysicsManager.screenToPhysics(upperTranslation));
        jointDef.setEnableLimit(true);
        jointDef.setCollideConnected(false);
        if (motorSpeed != 0.0f) {
            jointDef.setMaxMotorForce(motorForce);
            jointDef.setMotorSpeed(PhysicsManager.screenToPhysics(motorSpeed));
            jointDef.setEnableMotor(true);
        }
        this.joint = new b2LineJoint(b2Joint.getCPtr(PhysicsManager.instance.getWorld().CreateJoint(jointDef)), false);
        jointDef.delete();
    }

    public void EnableMotor(boolean flag) {
        this.joint.EnableMotor(flag);
    }

    public void SetMotorSpeed(float speed) {
        this.joint.SetMotorSpeed(PhysicsManager.screenToPhysics(speed));
    }

    public float GetMotorSpeed() {
        return PhysicsManager.physicsToScreen(this.joint.GetMotorSpeed());
    }

    public void SetMaxMotorForce(float force) {
        this.joint.SetMaxMotorForce(force);
    }

    public float getJointTranslation() {
        return PhysicsManager.physicsToScreen(this.joint.GetJointTranslation());
    }

    public b2LineJoint getJoint() {
        return this.joint;
    }
}
