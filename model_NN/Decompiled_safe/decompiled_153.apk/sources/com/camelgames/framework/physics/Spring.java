package com.camelgames.framework.physics;

import com.box2d.b2Body;
import com.camelgames.framework.math.Vector2;

public class Spring {
    private final Vector2 anchorLocal1 = new Vector2();
    private final Vector2 anchorLocal2 = new Vector2();
    private final Vector2 anchorWorld1 = new Vector2();
    private final Vector2 anchorWorld2 = new Vector2();
    private b2Body body1;
    private b2Body body2;
    private float damping = 0.0f;
    private float k = 1.0f;
    private float length = 0.0f;
    private Vector2 positionV = new Vector2();
    private Vector2 velocity1 = new Vector2();
    private Vector2 velocity2 = new Vector2();
    private Vector2 velocityV = new Vector2();

    public Spring(b2Body body12, b2Body body22, float k2, float damping2, Vector2 anchorWorld12, Vector2 anchorWorld22) {
        Vector2 vector = Vector2.substract(anchorWorld12, anchorWorld22);
        this.body1 = body12;
        this.body2 = body22;
        this.k = k2;
        this.damping = damping2;
        this.length = vector.getLength();
        PhysicsBodyUtil.getLocalPoint(body12, anchorWorld12, this.anchorLocal1);
        PhysicsBodyUtil.getLocalPoint(body22, anchorWorld22, this.anchorLocal2);
    }

    public void update(float elapsedTime) {
        PhysicsBodyUtil.getWorldPoint(this.body1, this.anchorLocal1, this.anchorWorld1);
        PhysicsBodyUtil.getWorldPoint(this.body2, this.anchorLocal2, this.anchorWorld2);
        this.positionV.set(this.anchorWorld2.X - this.anchorWorld1.X, this.anchorWorld2.Y - this.anchorWorld1.Y);
        float force = (this.positionV.normalize() - this.length) * this.k;
        float forceX = force * this.positionV.X;
        float forceY = force * this.positionV.Y;
        PhysicsBodyUtil.applyForce(this.body1, forceX, forceY, this.anchorWorld1.X, this.anchorWorld1.Y);
        PhysicsBodyUtil.applyForce(this.body2, -forceX, -forceY, this.anchorWorld2.X, this.anchorWorld2.Y);
        if (this.damping != 0.0f) {
            PhysicsBodyUtil.getLinearVelocity(this.body1, this.velocity1);
            PhysicsBodyUtil.getLinearVelocity(this.body2, this.velocity2);
            this.velocityV.set(this.velocity2.X - this.velocity1.X, this.velocity2.Y - this.velocity1.Y);
            float dampingForce = this.velocityV.dot(this.positionV) * this.damping;
            float dampingForceX = this.positionV.X * dampingForce;
            float dampingForceY = this.positionV.Y * dampingForce;
            PhysicsBodyUtil.applyForce(this.body1, dampingForceX, dampingForceY, this.anchorWorld1.X, this.anchorWorld1.Y);
            PhysicsBodyUtil.applyForce(this.body2, -dampingForceX, -dampingForceY, this.anchorWorld2.X, this.anchorWorld2.Y);
        }
    }

    public void setBody1(b2Body body) {
        this.body1 = body;
    }

    public b2Body getBody1() {
        return this.body1;
    }

    public void setBody2(b2Body body) {
        this.body2 = body;
    }

    public b2Body getBody2() {
        return this.body2;
    }

    public void setK(float k2) {
        this.k = k2;
    }

    public float getK() {
        return this.k;
    }

    public void setDamping(float damping2) {
        this.damping = damping2;
    }

    public float getDamping() {
        return this.damping;
    }

    public void setLength(float length2) {
        this.length = length2;
    }

    public float getLength() {
        return this.length;
    }

    public Vector2 getAnchor1() {
        return this.anchorWorld1;
    }

    public Vector2 getAnchor2() {
        return this.anchorWorld2;
    }
}
