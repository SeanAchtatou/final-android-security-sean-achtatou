package com.camelgames.framework;

import android.content.SharedPreferences;
import com.camelgames.framework.ui.UIUtility;

public class ConfigurationManager {
    public static final String DefaultPrefsName = "DefaultPrefs";
    private static ConfigurationManager instance = new ConfigurationManager();

    public static ConfigurationManager getInstance() {
        return instance;
    }

    private ConfigurationManager() {
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return getDefaultPreferences().getBoolean(key, defaultValue);
    }

    public void pubBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = getDefaultPreferences().edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public int getInt(String key, int defaultValue) {
        return getDefaultPreferences().getInt(key, defaultValue);
    }

    public void putInt(String key, int value) {
        SharedPreferences.Editor editor = getDefaultPreferences().edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public String getString(String key, String defaultValue) {
        return getDefaultPreferences().getString(key, defaultValue);
    }

    public void putString(String key, String value) {
        SharedPreferences.Editor editor = getDefaultPreferences().edit();
        editor.putString(key, value);
        editor.commit();
    }

    public SharedPreferences getDefaultPreferences() {
        return UIUtility.getMainAcitvity().getSharedPreferences(DefaultPrefsName, 0);
    }
}
