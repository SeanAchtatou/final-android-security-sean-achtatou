package com.camelgames.framework.entities.transitions;

import com.camelgames.framework.Skeleton.RenderableListenerEntity;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.utilities.Excute;
import com.camelgames.ndk.JNILibrary;
import javax.microedition.khronos.opengles.GL10;

public class FlatTransition extends RenderableListenerEntity {
    private float delayTime;
    private Excute finishedCallback;
    private boolean isBackward;
    protected float process;
    private final Integer resourceId;
    private int screenTexWidth;
    private float speed;
    private int[] texCoordsEOS = new int[4];

    public FlatTransition(Integer resourceId2, float speed2, boolean isPortrait) {
        this.resourceId = resourceId2;
        this.speed = speed2;
        System.arraycopy(GraphicsManager.getInstance().getScreenTexCoordsOES(isPortrait), 0, this.texCoordsEOS, 0, 4);
        this.screenTexWidth = this.texCoordsEOS[2];
    }

    public void setCallback(Excute finishedCallback2) {
        this.finishedCallback = finishedCallback2;
    }

    public void setBackward() {
        this.isBackward = true;
        this.process = 1.0f;
        this.speed = -this.speed;
    }

    public void render(GL10 gl, float elapsedTime) {
        if (this.process != 0.0f) {
            gl.glBlendFunc(1, 771);
            gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            this.texCoordsEOS[2] = (int) (((float) this.screenTexWidth) * this.process);
            JNILibrary.bindTexture(this.resourceId.intValue());
            GraphicsManager.getInstance().drawTexiOES(this.texCoordsEOS, 0, 0, (int) (((float) GraphicsManager.screenWidth()) * this.process), GraphicsManager.screenHeight());
        }
    }

    public void HandleEvent(AbstractEvent e) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void update(float elapsedTime) {
        if (isFinished()) {
            return;
        }
        if (this.delayTime > 0.0f) {
            this.delayTime -= elapsedTime;
            return;
        }
        this.process += this.speed * elapsedTime;
        this.process = Math.max(0.0f, Math.min(1.0f, this.process));
        if (this.finishedCallback != null && isFinished()) {
            this.finishedCallback.excute();
        }
    }

    public boolean isFinished() {
        if (!this.isBackward) {
            return this.process == 1.0f;
        }
        if (this.process == 0.0f) {
            return true;
        }
        return false;
    }

    public void setDelayTime(float delayTime2) {
        this.delayTime = delayTime2;
    }

    public float getDelayTime() {
        return this.delayTime;
    }
}
