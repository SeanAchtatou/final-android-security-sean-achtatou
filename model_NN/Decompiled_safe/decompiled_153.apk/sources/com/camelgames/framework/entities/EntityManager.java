package com.camelgames.framework.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeSet;

public final class EntityManager {
    public static final int UNKNOWN_ID = -1;
    private static EntityManager instance = new EntityManager();
    private ArrayList<Entity> entities = new ArrayList<>();
    private HashMap<Integer, Entity> entitiesMap = new HashMap<>();
    private boolean isInitialized;
    private int nextID = 1;
    private TreeSet<Integer> unusedIDs = new TreeSet<>();
    private LinkedList<Entity> waitToDeleteEntities = new LinkedList<>();
    private LinkedList<Entity> waitToRegisterEntities = new LinkedList<>();
    private LinkedList<Entity> waitToUnregisterEntities = new LinkedList<>();

    public static EntityManager getInstance() {
        return instance;
    }

    private EntityManager() {
    }

    public void initiate() {
        if (!this.isInitialized) {
            this.isInitialized = true;
        }
    }

    public void update(float elapsedTime) {
        sync();
        for (int i = 0; i < this.entities.size(); i++) {
            this.entities.get(i).update(elapsedTime);
        }
    }

    public void register(Entity item) {
        if (item == null) {
            return;
        }
        if (this.entitiesMap.containsKey(Integer.valueOf(item.getId()))) {
            this.waitToUnregisterEntities.remove(item);
        } else if (!this.waitToRegisterEntities.contains(item)) {
            item.setId(allocateId());
            this.waitToRegisterEntities.add(item);
        }
    }

    public void unregister(Entity item) {
        if (item == null) {
            return;
        }
        if (this.waitToRegisterEntities.contains(item)) {
            recallId(item.getId());
            item.setId(-1);
            this.waitToRegisterEntities.remove(item);
        } else if (this.entitiesMap.containsKey(Integer.valueOf(item.getId()))) {
            this.waitToUnregisterEntities.add(item);
        }
    }

    public void delete(Entity item) {
        if (item == null) {
            return;
        }
        if (!this.entitiesMap.containsKey(Integer.valueOf(item.getId()))) {
            recallId(item.getId());
            item.dispose();
            item.setId(-1);
            this.waitToRegisterEntities.remove(item);
            this.waitToUnregisterEntities.remove(item);
        } else if (!this.waitToDeleteEntities.contains(item)) {
            this.waitToDeleteEntities.add(item);
        }
    }

    public void clearAll() {
        sync();
        for (int i = 0; i < this.entities.size(); i++) {
            this.entities.get(i).dispose();
        }
        this.entities.clear();
        this.entitiesMap.clear();
        this.waitToRegisterEntities.clear();
        this.waitToUnregisterEntities.clear();
        this.waitToDeleteEntities.clear();
    }

    public void clearTemporary() {
        sync();
        for (int i = 0; i < this.entities.size(); i++) {
            if (!this.entities.get(i).isPermanent()) {
                delete(this.entities.get(i));
            }
        }
        sync();
        this.waitToRegisterEntities.clear();
        this.waitToUnregisterEntities.clear();
        this.waitToDeleteEntities.clear();
    }

    public Entity getEntityFromID(int id) {
        return this.entitiesMap.get(Integer.valueOf(id));
    }

    public int allocateId() {
        if (this.unusedIDs.size() > 0) {
            int id = this.unusedIDs.first().intValue();
            this.unusedIDs.remove(Integer.valueOf(id));
            return id;
        }
        int i = this.nextID;
        this.nextID = i + 1;
        return i;
    }

    private void recallId(int id) {
        if (id != -1) {
            this.unusedIDs.add(Integer.valueOf(id));
        }
    }

    private void sync() {
        if (!this.waitToRegisterEntities.isEmpty()) {
            Iterator<Entity> it = this.waitToRegisterEntities.iterator();
            while (it.hasNext()) {
                add(it.next());
            }
            this.waitToRegisterEntities.clear();
        }
        if (!this.waitToUnregisterEntities.isEmpty()) {
            Iterator<Entity> it2 = this.waitToUnregisterEntities.iterator();
            while (it2.hasNext()) {
                Entity entity = it2.next();
                if (entity != null) {
                    int id = entity.getId();
                    remove(id);
                    entity.setId(-1);
                    recallId(id);
                }
            }
            this.waitToUnregisterEntities.clear();
        }
        while (this.waitToDeleteEntities.size() > 0) {
            Entity endity = this.waitToDeleteEntities.poll();
            if (endity != null) {
                int id2 = endity.getId();
                remove(id2);
                endity.dispose();
                endity.setId(-1);
                recallId(id2);
            }
        }
    }

    private void add(Entity entity) {
        if (entity != null && !this.entitiesMap.containsKey(Integer.valueOf(entity.getId()))) {
            this.entities.add(entity);
            this.entitiesMap.put(Integer.valueOf(entity.getId()), entity);
        }
    }

    private Entity remove(int id) {
        Entity entity = this.entitiesMap.remove(Integer.valueOf(id));
        if (entity != null) {
            this.entities.remove(entity);
        }
        return entity;
    }
}
