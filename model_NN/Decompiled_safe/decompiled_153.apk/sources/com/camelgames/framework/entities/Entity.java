package com.camelgames.framework.entities;

import com.camelgames.framework.resources.Disposable;

public interface Entity extends Disposable {
    int getId();

    boolean isPermanent();

    void setId(int i);

    void update(float f);
}
