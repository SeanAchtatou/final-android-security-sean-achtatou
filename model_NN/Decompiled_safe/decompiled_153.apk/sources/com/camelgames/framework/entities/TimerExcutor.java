package com.camelgames.framework.entities;

import com.camelgames.framework.Skeleton.AbstractEntity;
import com.camelgames.framework.utilities.Excute;

public class TimerExcutor extends AbstractEntity {
    private Excute excute;
    private float time;

    public TimerExcutor(float time2, Excute excute2) {
        this.excute = excute2;
        this.time = time2;
    }

    public void update(float elapsedTime) {
        this.time -= elapsedTime;
        if (this.time <= 0.0f) {
            this.excute.excute();
            delete();
        }
    }
}
