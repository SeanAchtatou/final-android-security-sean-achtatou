package com.camelgames.framework.graphics.skinned2d;

import com.camelgames.framework.math.MathUtils;
import com.camelgames.framework.math.Vector2;

public class Joint {
    float absoluteCos;
    float absoluteSin;
    float angle;
    public float angleAbsolute;
    public Joint[] children;
    public Vector2 endAbsolute = new Vector2();
    public boolean fixedInAnimation;
    public boolean flagUpdatedByNode;
    public int id;
    public boolean isActive = true;
    float length;
    public String name;
    public Joint parent;
    float rotateCos;
    float rotateSin;
    public Vector2 startAbsolute = new Vector2();
    float startAngle;
    float startCos;
    float startSin;

    public void setAngle(float angle2) {
        if (this.isActive) {
            this.angle = angle2;
            this.rotateCos = (float) Math.cos((double) angle2);
            this.rotateSin = (float) Math.sin((double) angle2);
        }
    }

    public void setStartAngle(float startAngle2) {
        if (this.isActive) {
            this.startAngle = startAngle2;
            this.startCos = (float) Math.cos((double) startAngle2);
            this.startSin = (float) Math.sin((double) startAngle2);
        }
    }

    public void update() {
        if (this.isActive) {
            if (this.parent != null) {
                this.angleAbsolute = this.angle + this.parent.angleAbsolute;
                this.startAbsolute.set(this.parent.endAbsolute);
                this.absoluteCos = (this.parent.absoluteCos * this.rotateCos) + (this.parent.absoluteSin * (-this.rotateSin));
                this.absoluteSin = (this.parent.absoluteCos * this.rotateSin) + (this.parent.absoluteSin * this.rotateCos);
            } else {
                this.angleAbsolute = this.angle + this.startAngle;
                this.absoluteCos = (this.startCos * this.rotateCos) + (this.startSin * (-this.rotateSin));
                this.absoluteSin = (this.startCos * this.rotateSin) + (this.startSin * this.rotateCos);
            }
            this.endAbsolute.X = this.startAbsolute.X + (this.length * this.absoluteCos);
            this.endAbsolute.Y = this.startAbsolute.Y + (this.length * this.absoluteSin);
        }
    }

    public void revertSetEndAbsolute(float endAbsoluteX, float endAbsoluteY, float angleAbsolute2) {
        if (this.isActive && !this.flagUpdatedByNode) {
            this.flagUpdatedByNode = true;
            this.endAbsolute.X = endAbsoluteX;
            this.endAbsolute.Y = endAbsoluteY;
            this.angleAbsolute = angleAbsolute2;
            if (this.children != null) {
                for (Joint child : this.children) {
                    child.startAbsolute.X = endAbsoluteX;
                    child.startAbsolute.Y = endAbsoluteY;
                    if (child.fixedInAnimation) {
                        float childAbsoluteAngle = angleAbsolute2 + child.angle;
                        child.revertSetEndAbsolute(child.startAbsolute.X + (child.length * ((float) Math.cos((double) childAbsoluteAngle))), child.startAbsolute.Y + (child.length * ((float) Math.sin((double) childAbsoluteAngle))), childAbsoluteAngle);
                    }
                }
            }
        }
    }

    public void revertLengthAngle() {
        if (!this.isActive) {
            return;
        }
        if (!this.flagUpdatedByNode) {
            if (this.children != null) {
                Joint child = this.children[0];
                this.angleAbsolute = child.angleAbsolute - child.angle;
                revertSetEndAbsolute(child.endAbsolute.X - (child.length * ((float) Math.cos((double) child.angleAbsolute))), child.endAbsolute.Y - (child.length * ((float) Math.sin((double) child.angleAbsolute))), this.angleAbsolute);
                this.startAbsolute.X = this.endAbsolute.X - (this.length * ((float) Math.cos((double) this.angleAbsolute)));
                this.startAbsolute.Y = this.endAbsolute.Y - (this.length * ((float) Math.sin((double) this.angleAbsolute)));
                if (this.parent == null) {
                    this.startAngle = this.angleAbsolute - this.angle;
                }
            }
        } else if (!this.fixedInAnimation) {
            if (this.parent != null) {
                this.angle = MathUtils.constrainPi(this.angleAbsolute - this.parent.angleAbsolute);
            } else {
                this.angle = MathUtils.constrainPi(this.angleAbsolute - this.startAngle);
            }
            float x = this.endAbsolute.X - this.startAbsolute.X;
            float y = this.endAbsolute.Y - this.startAbsolute.Y;
            this.length = (float) Math.sqrt((double) ((x * x) + (y * y)));
        }
    }

    public Joint getRoot() {
        return this.parent == null ? this : this.parent.getRoot();
    }
}
