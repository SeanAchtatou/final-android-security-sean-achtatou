package com.camelgames.framework.graphics.skinned2d;

import com.camelgames.framework.math.MathUtils;

public class JointFrame {
    public float[] jointPositions;
    public float time;

    public void scale(float scale) {
        int count = this.jointPositions.length / 2;
        for (int i = 0; i < count; i++) {
            float[] fArr = this.jointPositions;
            int i2 = i * 2;
            fArr[i2] = fArr[i2] * scale;
        }
    }

    public void play(Joint[] joints) {
        for (int i = 0; i < joints.length; i++) {
            Joint joint = joints[i];
            joint.length = this.jointPositions[i * 2];
            joint.setAngle(this.jointPositions[(i * 2) + 1]);
            joint.update();
        }
    }

    public static void playFrame(JointFrame frame1, JointFrame frame2, float weight, Joint[] joints) {
        for (int i = 0; i < joints.length; i++) {
            Joint joint = joints[i];
            float length1 = frame1.jointPositions[i * 2];
            float length2 = frame2.jointPositions[i * 2];
            float angle1 = frame1.jointPositions[(i * 2) + 1];
            float angle2 = frame2.jointPositions[(i * 2) + 1];
            joint.length = ((length2 - length1) * weight) + length1;
            joint.setAngle((MathUtils.constrainPi(angle2 - angle1) * weight) + angle1);
            joint.update();
        }
    }
}
