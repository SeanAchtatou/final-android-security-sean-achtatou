package com.camelgames.framework.graphics.transforms;

import javax.microedition.khronos.opengles.GL10;

public interface Transform {
    void transform(GL10 gl10);

    void transform(float[] fArr);
}
