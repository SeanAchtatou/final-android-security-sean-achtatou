package com.camelgames.framework.graphics.tiles;

import java.util.ArrayList;
import java.util.Iterator;

public class TilesetManager {
    private static TilesetManager instance = new TilesetManager();
    private ArrayList<TileSet> tileSets = new ArrayList<>();

    public static TilesetManager getInstance() {
        return instance;
    }

    private TilesetManager() {
    }

    public void add(TileSet tileSet) {
        if (tileSet != null && !this.tileSets.contains(tileSet)) {
            int insertIndex = 0;
            while (insertIndex < this.tileSets.size() && this.tileSets.get(insertIndex).firstGid >= tileSet.firstGid) {
                insertIndex++;
            }
            this.tileSets.add(insertIndex, tileSet);
        }
    }

    public TileSet getTileSet(int gid) {
        Iterator<TileSet> it = this.tileSets.iterator();
        while (it.hasNext()) {
            TileSet tileSet = it.next();
            if (tileSet.firstGid <= gid) {
                return tileSet;
            }
        }
        return null;
    }

    public void remove(TileSet tileSet) {
        this.tileSets.remove(tileSet);
    }

    public void clear() {
        this.tileSets.clear();
    }
}
