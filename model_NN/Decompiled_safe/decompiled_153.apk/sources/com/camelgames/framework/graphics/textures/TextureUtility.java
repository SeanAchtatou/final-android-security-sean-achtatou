package com.camelgames.framework.graphics.textures;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import com.camelgames.framework.graphics.altas.AltasResource;
import com.camelgames.framework.graphics.altas.AltasTextureManager;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.ui.UIUtility;
import com.camelgames.framework.utilities.IOUtility;
import com.camelgames.framework.utilities.SDUtility;
import com.camelgames.ndk.graphics.TextureManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class TextureUtility {
    private static TextureUtility instance = new TextureUtility();
    private int currentFileResId;
    private Class drawableClass;
    private int fileResIdStart;
    private HashMap<Integer, TextureFile> imageFiles = new HashMap<>();
    private boolean isInitiated;
    private HashMap<Integer, Vector2> resourceSizeMapping = new HashMap<>();

    public static TextureUtility getInstance() {
        return instance;
    }

    private TextureUtility() {
    }

    public void initiated(Class drawableClass2, int fileResIdStart2) {
        if (!this.isInitiated) {
            this.drawableClass = drawableClass2;
            this.fileResIdStart = fileResIdStart2;
            this.currentFileResId = this.fileResIdStart;
            this.isInitiated = true;
        }
        syncImagesFromSD();
    }

    public void syncImagesFromSD() {
        loadImagesFromSDFolder(SDUtility.getImageFolder());
    }

    public void clearImageFiles() {
        if (!this.imageFiles.isEmpty()) {
            Integer[] resIds = new Integer[this.imageFiles.size()];
            this.imageFiles.keySet().toArray(resIds);
            for (Integer id : resIds) {
                removeResId(id);
            }
        }
        this.currentFileResId = this.fileResIdStart;
    }

    public void loadImagesFromSDFolder(String folder) {
        ArrayList<File> files = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();
        SDUtility.getFiles(folder, names, files);
        for (int i = 0; i < names.size(); i++) {
            if (tryGetResIdFromImageFiles((String) names.get(i)) == null) {
                addImageFile((File) files.get(i), (String) names.get(i));
            }
        }
    }

    public int addImageFile(File file, String name) {
        this.imageFiles.put(Integer.valueOf(this.currentFileResId), new TextureFile(file, name));
        int i = this.currentFileResId;
        this.currentFileResId = i + 1;
        return i;
    }

    public void removeImageFile(String name) {
        Integer resId = tryGetResIdFromImageFiles(name);
        if (resId != null) {
            removeResId(resId);
        }
    }

    public void removeResId(Integer resId) {
        this.resourceSizeMapping.remove(resId);
        this.imageFiles.remove(resId);
        TextureManager.getInstance().removeTexture(resId.intValue());
    }

    public Integer getDrawableResourceId(String name) {
        Integer resId = tryGetResIdFromImageFiles(name);
        if (resId == null) {
            return IOUtility.getResourceIdByName(this.drawableClass, name);
        }
        return resId;
    }

    private Integer tryGetResIdFromImageFiles(String name) {
        for (Map.Entry<Integer, TextureFile> entry : this.imageFiles.entrySet()) {
            if (((TextureFile) entry.getValue()).name.equals(name)) {
                return (Integer) entry.getKey();
            }
        }
        return null;
    }

    public int getResIdFromName(String name) {
        if (name.indexOf("altas") == 0) {
            return AltasTextureManager.getInstance().getAltasResource(name).getSubResourceId().intValue();
        }
        return getDrawableResourceId(name).intValue();
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public Vector2 getTextureSize(Integer resourceId) {
        if (!this.resourceSizeMapping.containsKey(resourceId)) {
            Vector2 size = getBitmapSize(resourceId);
            if (size != null) {
                this.resourceSizeMapping.put(resourceId, size);
            } else {
                this.resourceSizeMapping.put(resourceId, new Vector2(1.0f, 1.0f));
            }
        }
        return this.resourceSizeMapping.get(resourceId);
    }

    public static Bitmap getCropedScaledBitmap(int resourceId, int x, int y, int chopWidth, int chopHeight, int scaleWidth, int scaleHeight) {
        Bitmap originalBitmap = getChopedBitmap(resourceId, x, y, chopWidth, chopHeight);
        if (originalBitmap != null) {
            return Bitmap.createScaledBitmap(originalBitmap, scaleWidth, scaleHeight, false);
        }
        return null;
    }

    public static Bitmap getScaledCropedBitmap(int resourceId, int scaleWidth, int scaleHeight, int x, int y, int chopWidth, int chopHeight) {
        Bitmap originalBitmap = getBitmap(Integer.valueOf(resourceId));
        if (originalBitmap == null) {
            return null;
        }
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(originalBitmap, scaleWidth, scaleHeight, false);
        originalBitmap.recycle();
        Bitmap chopedBitmap = Bitmap.createBitmap(scaledBitmap, x, y, chopWidth, chopHeight);
        scaledBitmap.recycle();
        return chopedBitmap;
    }

    public static Bitmap getChopedBitmap(BitmapDrawable bitmapDrawable, int x, int y, int width, int height) {
        Bitmap originalBitmap;
        if (bitmapDrawable == null || (originalBitmap = bitmapDrawable.getBitmap()) == null) {
            return null;
        }
        return Bitmap.createBitmap(originalBitmap, x, y, width, height);
    }

    public static Bitmap getChopedBitmap(int resourceId, int x, int y, int width, int height) {
        Bitmap originalBitmap = getBitmap(Integer.valueOf(resourceId));
        if (originalBitmap == null) {
            return null;
        }
        Bitmap choped = Bitmap.createBitmap(originalBitmap, x, y, width, height);
        originalBitmap.recycle();
        return choped;
    }

    public static Bitmap getScaledBitmap(BitmapDrawable bd, int width, int height) {
        Bitmap originalBitmap;
        if (bd == null || (originalBitmap = bd.getBitmap()) == null) {
            return null;
        }
        return Bitmap.createScaledBitmap(originalBitmap, width, height, false);
    }

    public static Bitmap getScaledBitmap(int resourceId, int width, int height) {
        Bitmap originalBitmap = getBitmap(Integer.valueOf(resourceId));
        if (originalBitmap == null) {
            return null;
        }
        Bitmap scaled = Bitmap.createScaledBitmap(originalBitmap, width, height, false);
        originalBitmap.recycle();
        return scaled;
    }

    public static Bitmap getBitmap(Integer resourceId) {
        return getBitmap(UIUtility.getMainAcitvity(), resourceId);
    }

    public static Bitmap getBitmap(Context context, Integer resourceId) {
        if (resourceId == null || resourceId.intValue() <= 0) {
            return null;
        }
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inScaled = false;
        if (isResIdInsideFiles(resourceId)) {
            try {
                return BitmapFactory.decodeStream(new FileInputStream(getInstance().imageFiles.get(resourceId).file), null, opts);
            } catch (FileNotFoundException e) {
                return null;
            }
        } else if (AltasTextureManager.getInstance().isIdInside(resourceId)) {
            return getAltasBitmap(resourceId.intValue());
        } else {
            return BitmapFactory.decodeResource(context.getResources(), resourceId.intValue(), opts);
        }
    }

    private static Bitmap getAltasBitmap(int resourceId) {
        AltasResource altasResource = AltasTextureManager.getInstance().getAltasResource(Integer.valueOf(resourceId));
        Bitmap originalBitmap = getBitmap(altasResource.getAltasResourceId());
        if (originalBitmap == null) {
            return null;
        }
        Bitmap choped = Bitmap.createBitmap(originalBitmap, altasResource.getLeft(), altasResource.getTop(), altasResource.getWidth(), altasResource.getHeight());
        originalBitmap.recycle();
        return choped;
    }

    public static Vector2 getBitmapSize(Integer resourceId) {
        if (resourceId == null || resourceId.intValue() <= 0) {
            return null;
        }
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inScaled = false;
        opts.inJustDecodeBounds = true;
        if (isResIdInsideFiles(resourceId)) {
            try {
                BitmapFactory.decodeStream(new FileInputStream(getInstance().imageFiles.get(resourceId).file), null, opts);
                return new Vector2((float) opts.outWidth, (float) opts.outHeight);
            } catch (FileNotFoundException e) {
                return null;
            }
        } else {
            BitmapFactory.decodeResource(UIUtility.getMainAcitvity().getResources(), resourceId.intValue(), opts);
            return new Vector2((float) opts.outWidth, (float) opts.outHeight);
        }
    }

    private static boolean isResIdInsideFiles(Integer resourceId) {
        int idGap = resourceId.intValue() - getInstance().fileResIdStart;
        return idGap >= 0 && idGap < 1000;
    }

    private static class TextureFile {
        public File file;
        public String name;

        public TextureFile(File file2, String name2) {
            this.file = file2;
            this.name = name2;
        }
    }
}
