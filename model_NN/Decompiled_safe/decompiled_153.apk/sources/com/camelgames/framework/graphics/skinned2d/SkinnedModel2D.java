package com.camelgames.framework.graphics.skinned2d;

import android.content.res.XmlResourceParser;
import com.camelgames.framework.graphics.textures.TextureUtility;
import com.camelgames.framework.levels.LevelScriptReader;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.math.Vector3;
import com.camelgames.framework.ui.UIUtility;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParserException;

public class SkinnedModel2D {
    private static Vector2 v = new Vector2();
    private Animation[] animations;
    ImageNode[] imageNodes;
    Joint[] joints;
    private Vector3[] nodesPositions;
    private PhysicsJointInfo[] physicsJointInfos;

    public static SkinnedModel2D loadFromXml(int resId, float scale) {
        SkinnedModel2D model = new SkinnedModel2D();
        XmlResourceParser xmlParser = UIUtility.getMainAcitvity().getResources().getXml(resId);
        while (LevelScriptReader.moveToStartTag(xmlParser)) {
            try {
                String nodeName = xmlParser.getName();
                if (nodeName.equals("Joints")) {
                    model.loadJoints(xmlParser);
                } else if (nodeName.equals("Images")) {
                    model.loadImageNodes(xmlParser);
                } else if (nodeName.equals("Animations")) {
                    model.loadAnimations(xmlParser);
                } else if (nodeName.equals("Physics")) {
                    model.loadPhysicsInfo(xmlParser);
                }
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        xmlParser.close();
        model.scale(scale);
        return model;
    }

    private void loadJoints(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        int jointsCount = LevelScriptReader.getInt(xmlParser, "Count", 0);
        this.joints = new Joint[jointsCount];
        while (xmlParser.nextTag() == 2) {
            Joint joint = new Joint();
            joint.id = LevelScriptReader.getInt(xmlParser, "Id", 0);
            joint.name = LevelScriptReader.getString(xmlParser, "Name");
            joint.fixedInAnimation = LevelScriptReader.getBoolean(xmlParser, "FixedInAni");
            int parentId = LevelScriptReader.getInt(xmlParser, "ParentId", 0);
            if (parentId >= 0) {
                joint.parent = this.joints[parentId];
            }
            this.joints[joint.id] = joint;
            xmlParser.nextTag();
        }
        ArrayList<Joint> children = new ArrayList<>();
        for (int i = 0; i < jointsCount; i++) {
            Joint parent = this.joints[i];
            children.clear();
            for (int j = i + 1; j < jointsCount; j++) {
                Joint joint2 = this.joints[j];
                if (joint2.parent == parent) {
                    children.add(joint2);
                }
            }
            if (children.size() > 0) {
                parent.children = new Joint[children.size()];
                children.toArray(parent.children);
            }
        }
    }

    private void loadImageNodes(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        this.imageNodes = new ImageNode[LevelScriptReader.getInt(xmlParser, "Count", 0)];
        int index = 0;
        while (xmlParser.nextTag() == 2) {
            ImageNode imageNode = new ImageNode();
            imageNode.name = LevelScriptReader.getString(xmlParser, "Name");
            imageNode.setLengthAngle(LevelScriptReader.getFloat(xmlParser, "Length", 0.0f), LevelScriptReader.getFloat(xmlParser, "Angle", 0.0f), LevelScriptReader.getFloat(xmlParser, "SAngle", 0.0f));
            imageNode.bindingJoint = this.joints[LevelScriptReader.getInt(xmlParser, "Joint", 0)];
            imageNode.initiateSprite(TextureUtility.getInstance().getResIdFromName(LevelScriptReader.getString(xmlParser, "Pic")));
            this.imageNodes[index] = imageNode;
            xmlParser.nextTag();
            index++;
        }
    }

    private void loadAnimations(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        this.animations = new Animation[LevelScriptReader.getInt(xmlParser, "Count", 0)];
        int index = 0;
        while (xmlParser.nextTag() == 2) {
            Animation animation = new Animation(this);
            animation.loadFrames(xmlParser);
            this.animations[index] = animation;
            index++;
        }
        this.animations[0].getFrames()[0].play(this.joints);
    }

    private void loadPhysicsInfo(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        this.physicsJointInfos = new PhysicsJointInfo[LevelScriptReader.getInt(xmlParser, "Count", 0)];
        int index = 0;
        while (xmlParser.nextTag() == 2) {
            PhysicsJointInfo pj = new PhysicsJointInfo();
            String node0Name = LevelScriptReader.getString(xmlParser, "Body0");
            String node1Name = LevelScriptReader.getString(xmlParser, "Body1");
            pj.node0 = getImageByName(node0Name);
            pj.node1 = getImageByName(node1Name);
            pj.lowerAngle = LevelScriptReader.getFloat(xmlParser, "Lower", 0.0f);
            pj.upperAngle = LevelScriptReader.getFloat(xmlParser, "Upper", 0.0f);
            pj.offsetX0 = LevelScriptReader.getFloat(xmlParser, "X0", 0.0f);
            pj.offsetY0 = LevelScriptReader.getFloat(xmlParser, "Y0", 0.0f);
            pj.offsetX1 = LevelScriptReader.getFloat(xmlParser, "X1", 0.0f);
            pj.offsetY1 = LevelScriptReader.getFloat(xmlParser, "Y1", 0.0f);
            this.physicsJointInfos[index] = pj;
            xmlParser.nextTag();
            index++;
        }
    }

    private void scale(float scale) {
        for (Animation scale2 : this.animations) {
            scale2.scale(scale);
        }
        for (ImageNode scale3 : this.imageNodes) {
            scale3.scale(scale);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateJoints() {
        for (Joint joint : this.joints) {
            joint.update();
        }
    }

    /* access modifiers changed from: package-private */
    public void updateImageNodes() {
        for (ImageNode imageNode : this.imageNodes) {
            imageNode.update();
        }
    }

    public void setPosition(ImageNode image, float x, float y, float angle, boolean update) {
        Joint root = this.joints[0];
        v.X = root.startAbsolute.X - image.positionAbsolute.X;
        v.Y = root.startAbsolute.Y - image.positionAbsolute.Y;
        v.RotateVector(angle - image.angleAbsolute);
        root.startAbsolute.X = v.X + x;
        root.startAbsolute.Y = v.Y + y;
        root.setStartAngle((root.startAngle + angle) - image.angleAbsolute);
        if (update) {
            updateJoints();
            updateImageNodes();
        }
    }

    public void setOri(float oriX, float oriY, float oriAngle, boolean update) {
        this.joints[0].startAbsolute.set(oriX, oriY);
        this.joints[0].setStartAngle(oriAngle);
        if (update) {
            updateJoints();
            updateImageNodes();
        }
    }

    public Vector2 getOri() {
        return this.joints[0].startAbsolute;
    }

    public float getOriAngle() {
        return this.joints[0].startAngle;
    }

    public Joint getRoot() {
        return this.joints[0];
    }

    public Animation getAnimationByName(String name) {
        for (Animation animation : this.animations) {
            if (animation.getName().equals(name)) {
                return animation;
            }
        }
        return null;
    }

    public Animation getAnimationByIndex(int index) {
        return this.animations[index];
    }

    public ImageNode getImageByName(String name) {
        for (ImageNode image : this.imageNodes) {
            if (image.name.equals(name)) {
                return image;
            }
        }
        return null;
    }

    public Joint getJointByName(String name) {
        for (Joint joint : this.joints) {
            if (joint.name.equals(name)) {
                return joint;
            }
        }
        return null;
    }

    public AnimationPlayer createAnimationPlayer(String animationName) {
        return new AnimationPlayer(getAnimationByName(animationName));
    }

    public PhysicsJointInfo[] getPhysicsJointInfos() {
        return this.physicsJointInfos;
    }

    public void render(float elapsedTime) {
        for (ImageNode image : this.imageNodes) {
            image.render(elapsedTime);
        }
    }

    public void setColor(float r, float g, float b) {
        for (ImageNode image : this.imageNodes) {
            image.getSprite().setColor(r, g, b);
        }
    }

    public Vector3[] getNodesPositions() {
        if (this.nodesPositions == null) {
            this.nodesPositions = new Vector3[this.imageNodes.length];
            for (int i = 0; i < this.nodesPositions.length; i++) {
                this.nodesPositions[i] = new Vector3();
            }
        }
        return this.nodesPositions;
    }

    public Vector3[] recordNodesPosition() {
        getNodesPositions();
        for (int i = 0; i < this.imageNodes.length; i++) {
            this.imageNodes[i].getSpritePosition(this.nodesPositions[i]);
        }
        return this.nodesPositions;
    }

    public void castFadeFrame(JointFrame fadeFrame, Vector3[] nodesPositions2) {
        if (nodesPositions2 != null) {
            for (Joint joint : this.joints) {
                joint.flagUpdatedByNode = false;
            }
            for (int i = 0; i < this.imageNodes.length; i++) {
                Vector3 nodePosition = nodesPositions2[i];
                this.imageNodes[i].updateJoint(nodePosition.X, nodePosition.Y, nodePosition.Z);
            }
            for (int i2 = 0; i2 < this.joints.length; i2++) {
                Joint joint2 = this.joints[i2];
                joint2.revertLengthAngle();
                fadeFrame.jointPositions[i2 * 2] = joint2.length;
                fadeFrame.jointPositions[(i2 * 2) + 1] = joint2.angle;
            }
            return;
        }
        for (int i3 = 0; i3 < this.joints.length; i3++) {
            Joint joint3 = this.joints[i3];
            fadeFrame.jointPositions[i3 * 2] = joint3.length;
            fadeFrame.jointPositions[(i3 * 2) + 1] = joint3.angle;
        }
    }

    public ImageNode[] getImages() {
        return this.imageNodes;
    }

    public JointFrame createJointFrame() {
        JointFrame fadeFrame = new JointFrame();
        fadeFrame.jointPositions = new float[(this.joints.length * 2)];
        return fadeFrame;
    }
}
