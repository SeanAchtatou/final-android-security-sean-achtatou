package com.camelgames.framework.graphics.hardware;

import com.camelgames.framework.graphics.GraphicsManager;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL11;

public class HardwareVertexBuffer extends HardwareBuffer {
    private int VBOId;
    private int usage = 35044;
    private FloatBuffer verticesBuffer;

    public void fillBufferData(GL11 gl11, FloatBuffer verticesBuffer2, int usage2) {
        if (verticesBuffer2 != null) {
            if (this.VBOId <= 0) {
                this.VBOId = createVBO(gl11);
            }
            this.verticesBuffer = verticesBuffer2;
            this.usage = usage2;
            fillBufferData(gl11, this.VBOId, verticesBuffer2, verticesBuffer2.capacity() * 4, usage2);
        }
    }

    public void updateData(GL11 gl11, int offset, int size) {
        gl11.glBindBuffer(34962, this.VBOId);
        gl11.glBufferSubData(34962, offset * 4, size * 4, this.verticesBuffer);
        gl11.glBindBuffer(34962, 0);
    }

    public void prepareTexCoordPointer(GL11 gl11) {
        gl11.glBindBuffer(34962, this.VBOId);
        gl11.glTexCoordPointer(2, 5126, 0, 0);
        gl11.glBindBuffer(34962, 0);
    }

    public void prepareVertexPointer(GL11 gl11) {
        gl11.glBindBuffer(34962, this.VBOId);
        gl11.glVertexPointer(2, 5126, 0, 0);
        gl11.glBindBuffer(34962, 0);
    }

    public FloatBuffer getBuffer() {
        return this.verticesBuffer;
    }

    public void deviceLost(GL11 gl11) {
        this.VBOId = 0;
    }

    public void deviceReset(GL11 gl11) {
        fillBufferData(gl11, this.verticesBuffer, this.usage);
    }

    /* access modifiers changed from: protected */
    public void disposeInternal() {
        releaseVBO(GraphicsManager.getInstance().getGL11(), this.VBOId);
        this.VBOId = 0;
        this.verticesBuffer = null;
        super.disposeInternal();
    }
}
