package com.camelgames.framework.graphics.skinned2d;

import com.camelgames.framework.math.MathUtils;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.math.Vector3;

public class AnimationPlayer {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$graphics$skinned2d$AnimationPlayer$Status;
    private float accumulatedTime;
    private final float duration;
    private Vector2 fadeEndOri = new Vector2();
    private float fadeEndOriAngle;
    private final JointFrame fadeFrame;
    private Vector2 fadeStartOri = new Vector2();
    private float fadeStartOriAngle;
    private float fadeTime;
    private final JointFrame[] frames;
    private boolean isLoop;
    private final SkinnedModel2D model;
    private int nextIndex;
    private boolean oriChanging;
    private int previousIndex;
    private Status status = Status.Stopped;

    private enum Status {
        Fading,
        Playing,
        Stopped
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$graphics$skinned2d$AnimationPlayer$Status() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$framework$graphics$skinned2d$AnimationPlayer$Status;
        if (iArr == null) {
            iArr = new int[Status.values().length];
            try {
                iArr[Status.Fading.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Status.Playing.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Status.Stopped.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$camelgames$framework$graphics$skinned2d$AnimationPlayer$Status = iArr;
        }
        return iArr;
    }

    public AnimationPlayer(Animation animation) {
        this.model = animation.getModel();
        this.duration = animation.getDuration();
        this.frames = animation.getFrames();
        this.fadeFrame = this.model.createJointFrame();
    }

    public void setLoop(boolean isLoop2) {
        this.isLoop = isLoop2;
    }

    public Boolean isLoop() {
        return Boolean.valueOf(this.isLoop);
    }

    public JointFrame[] getFrames() {
        return this.frames;
    }

    public void play() {
        setPlaying(0.0f, Status.Playing);
    }

    public void stop() {
        setPlaying(0.0f, Status.Stopped);
    }

    private void setPlaying(float fadeTime2, Status status2) {
        this.fadeTime = fadeTime2;
        this.accumulatedTime = 0.0f;
        this.status = status2;
        if (!status2.equals(Status.Stopped)) {
            update(0.0f);
        }
    }

    public void recordFadePlay(ImageNode tipNode, float tipX, float tipY, float tipAngle, float fadeTime2) {
        Vector3[] nodesPositions = this.model.recordNodesPosition();
        this.frames[0].play(this.model.joints);
        if (tipNode == null) {
            this.model.setOri(tipX, tipY, tipAngle, false);
        } else {
            tipNode.update();
            this.model.setPosition(tipNode, tipX, tipY, tipAngle, false);
        }
        this.fadeEndOri.set(this.model.getOri());
        this.fadeEndOriAngle = this.model.getOriAngle();
        this.model.castFadeFrame(this.fadeFrame, nodesPositions);
        this.fadeStartOri.set(this.model.getOri());
        this.fadeStartOriAngle = this.model.getOriAngle();
        this.oriChanging = true;
        setPlaying(fadeTime2, Status.Fading);
    }

    public void fadePlay(float fadeEndOriX, float fadeEndOriY, float fadeEndOriAngle2, float fadeTime2) {
        this.fadeStartOri.set(this.model.getOri());
        this.fadeStartOriAngle = this.model.getOriAngle();
        this.fadeEndOri.set(fadeEndOriX, fadeEndOriY);
        this.fadeEndOriAngle = fadeEndOriAngle2;
        this.oriChanging = true;
        this.model.castFadeFrame(this.fadeFrame, null);
        setPlaying(fadeTime2, Status.Fading);
    }

    public boolean isFadingFinished() {
        return !this.status.equals(Status.Fading);
    }

    public boolean update(float elapsedTime) {
        switch ($SWITCH_TABLE$com$camelgames$framework$graphics$skinned2d$AnimationPlayer$Status()[this.status.ordinal()]) {
            case 1:
                playCurrent();
                this.accumulatedTime += elapsedTime;
                if (this.accumulatedTime >= this.fadeTime) {
                    this.status = Status.Playing;
                    this.accumulatedTime -= this.fadeTime;
                }
                return true;
            case 2:
                this.nextIndex = 0;
                while (this.nextIndex < this.frames.length && this.frames[this.nextIndex].time < this.accumulatedTime) {
                    this.nextIndex++;
                }
                if (this.nextIndex >= this.frames.length) {
                    int length = this.frames.length - 1;
                    this.nextIndex = length;
                    this.previousIndex = length;
                } else if (this.nextIndex == 0) {
                    this.previousIndex = this.nextIndex;
                } else {
                    this.previousIndex = this.nextIndex - 1;
                }
                playCurrent();
                this.accumulatedTime += elapsedTime;
                if (this.accumulatedTime >= this.duration) {
                    if (this.isLoop) {
                        this.accumulatedTime -= this.duration;
                    } else {
                        this.accumulatedTime = this.duration;
                        this.status = Status.Stopped;
                    }
                }
                return true;
            default:
                return false;
        }
    }

    public void playCurrent() {
        switch ($SWITCH_TABLE$com$camelgames$framework$graphics$skinned2d$AnimationPlayer$Status()[this.status.ordinal()]) {
            case 1:
                float weight = this.accumulatedTime / this.fadeTime;
                if (this.oriChanging) {
                    float oriAngle = this.fadeStartOriAngle + (MathUtils.constrainPi(this.fadeEndOriAngle - this.fadeStartOriAngle) * weight);
                    this.model.setOri(this.fadeStartOri.X + ((this.fadeEndOri.X - this.fadeStartOri.X) * weight), this.fadeStartOri.Y + ((this.fadeEndOri.Y - this.fadeStartOri.Y) * weight), oriAngle, false);
                }
                JointFrame.playFrame(this.fadeFrame, this.frames[0], weight, this.model.joints);
                break;
            case 2:
                if (this.previousIndex != this.nextIndex) {
                    JointFrame firstFrame = this.frames[this.previousIndex];
                    JointFrame secondFrame = this.frames[this.nextIndex];
                    float firstPart = this.accumulatedTime - firstFrame.time;
                    JointFrame.playFrame(firstFrame, secondFrame, firstPart / (firstPart + (secondFrame.time - this.accumulatedTime)), this.model.joints);
                    break;
                } else {
                    this.frames[this.previousIndex].play(this.model.joints);
                    break;
                }
        }
        this.model.updateImageNodes();
    }
}
