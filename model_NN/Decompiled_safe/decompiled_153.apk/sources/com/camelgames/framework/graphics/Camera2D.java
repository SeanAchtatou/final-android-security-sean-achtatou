package com.camelgames.framework.graphics;

import android.opengl.GLU;
import com.camelgames.framework.math.Vector2;

public class Camera2D extends Camera {
    private float bottomBound;
    private float cameraX;
    private float cameraY;
    private boolean isRestored;
    private float leftBound;
    private float rightBound;
    private float topBound;
    private boolean updateBounds;
    private float zoom = 1.0f;

    public void restoreCamera() {
        setGluOrtho2D();
        if (!this.isRestored) {
            setCamera((float) (this.screenWidth / 2), (float) (this.screenHeight / 2));
            this.isRestored = true;
            return;
        }
        translateCamera();
    }

    public void setCamera(float x, float y) {
        this.cameraX = x;
        this.cameraY = y;
        translateCamera();
        if (this.updateBounds) {
            this.leftBound = screenToWorldX(0.0f);
            this.rightBound = screenToWorldX((float) this.screenWidth);
            this.topBound = screenToWorldY(0.0f);
            this.bottomBound = screenToWorldY((float) this.screenHeight);
        }
    }

    public float getCameraTop() {
        return this.topBound;
    }

    public void setCameraTop(float top) {
        setCamera(this.cameraX, (0.5f * getScreenViewHeight()) + top);
    }

    public float getCameraBottom() {
        return this.bottomBound;
    }

    public void setCameraBottom(float bottom) {
        setCamera(this.cameraX, bottom - (0.5f * getScreenViewHeight()));
    }

    public void setCameraX(float x) {
        setCamera(x, this.cameraY);
    }

    public float getCameraX() {
        return this.cameraX;
    }

    public void setCameraY(float y) {
        setCamera(this.cameraX, y);
    }

    public float getCameraY() {
        return this.cameraY;
    }

    public void moveCamera(float offsetX, float offsetY) {
        setCamera(this.cameraX + offsetX, this.cameraY + offsetY);
    }

    public void getMappedCamera(Vector2 anchor) {
        anchor.set(anchor.X - ((anchor.X - (((float) this.screenWidth) * 0.5f)) * this.zoom), anchor.Y - ((anchor.Y - (((float) this.screenHeight) * 0.5f)) * this.zoom));
    }

    public void getZoomAnchor(Vector2 anchor) {
        anchor.X = (this.cameraX - ((((float) this.screenWidth) * 0.5f) * this.zoom)) / (1.0f - this.zoom);
        anchor.Y = (this.cameraY - ((((float) this.screenHeight) * 0.5f) * this.zoom)) / (1.0f - this.zoom);
    }

    public boolean zoom(float zoomOffset, float guard) {
        float zoom2 = this.zoom + zoomOffset;
        boolean isGuarded = false;
        if (zoomOffset < 0.0f) {
            if (zoom2 <= guard) {
                zoom2 = guard;
                isGuarded = true;
            }
        } else if (zoom2 >= guard) {
            zoom2 = guard;
            isGuarded = true;
        }
        setZoom(zoom2);
        return isGuarded;
    }

    public void setZoom(float zoom2) {
        this.zoom = zoom2;
        setGluOrtho2D();
    }

    public float getZoom() {
        return this.zoom;
    }

    public void screenToWorld(Vector2 screenV) {
        screenV.set(screenToWorldX(screenV.X), screenToWorldY(screenV.Y));
    }

    public float screenToWorldX(float screenX) {
        return ((screenX - (0.5f * ((float) this.screenWidth))) * this.zoom) + this.cameraX;
    }

    public float screenToWorldY(float screenY) {
        return ((screenY - (0.5f * ((float) this.screenHeight))) * this.zoom) + this.cameraY;
    }

    public float worldToScreenX(float viewX) {
        return ((viewX - this.cameraX) / this.zoom) + (0.5f * ((float) this.screenWidth));
    }

    public float worldToScreenY(float viewY) {
        return ((viewY - this.cameraY) / this.zoom) + (0.5f * ((float) this.screenHeight));
    }

    public float getScreenViewWidth() {
        return this.zoom * ((float) this.screenWidth);
    }

    public float getScreenViewHeight() {
        return this.zoom * ((float) this.screenHeight);
    }

    public void setUpdateBounds(boolean updateBounds2) {
        this.updateBounds = updateBounds2;
        if (updateBounds2) {
            this.leftBound = screenToWorldX(0.0f);
            this.rightBound = screenToWorldX((float) this.screenWidth);
            this.topBound = screenToWorldY(0.0f);
            this.bottomBound = screenToWorldY((float) this.screenHeight);
        }
    }

    public boolean isUpdateBounds() {
        return this.updateBounds;
    }

    public boolean isXInScreen(float x, float radius) {
        return this.leftBound - radius < x && x < this.rightBound + radius;
    }

    public boolean isYInScreen(float y, float radius) {
        return this.topBound - radius < y && y < this.bottomBound + radius;
    }

    public boolean isYAboveBottom(float y) {
        return y < this.bottomBound;
    }

    public boolean isYBelowTop(float y) {
        return y > this.topBound;
    }

    public boolean isInScreen(float x, float y, float radius) {
        return this.leftBound - radius < x && x < this.rightBound + radius && this.topBound - radius < y && y < this.bottomBound + radius;
    }

    public boolean isInScreen(float left, float top, float right, float bottom) {
        return left < this.rightBound && this.leftBound < right && top < this.bottomBound && this.topBound < bottom;
    }

    public void follow(float targetX, float targetY, float followSpeed) {
        this.cameraX += (targetX - this.cameraX) * followSpeed;
        this.cameraY += (targetY - this.cameraY) * followSpeed;
        setCamera(this.cameraX, this.cameraY);
    }

    public void followX(float targetX, float followSpeed) {
        this.cameraX += (targetX - this.cameraX) * followSpeed;
        setCamera(this.cameraX, this.cameraY);
    }

    public void followY(float targetY, float followSpeed) {
        this.cameraY += (targetY - this.cameraY) * followSpeed;
        setCamera(this.cameraX, this.cameraY);
    }

    private void setGluOrtho2D() {
        this.gl.glMatrixMode(5889);
        this.gl.glLoadIdentity();
        float halfWidth = 0.5f * getScreenViewWidth();
        float halfHeight = 0.5f * getScreenViewHeight();
        GLU.gluOrtho2D(this.gl, -halfWidth, halfWidth, halfHeight, -halfHeight);
        this.gl.glMatrixMode(5888);
    }

    private void translateCamera() {
        this.gl.glLoadIdentity();
        this.gl.glTranslatef(-this.cameraX, -this.cameraY, 0.0f);
    }
}
