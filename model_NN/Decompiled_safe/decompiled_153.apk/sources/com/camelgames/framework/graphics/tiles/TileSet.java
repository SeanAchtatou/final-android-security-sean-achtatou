package com.camelgames.framework.graphics.tiles;

import com.camelgames.framework.graphics.altas.AltasResource;
import com.camelgames.framework.graphics.altas.AltasTextureManager;
import com.camelgames.framework.graphics.textures.TextureUtility;
import com.camelgames.framework.math.Vector2;
import com.camelgames.ndk.JNILibrary;

public class TileSet {
    public int firstGid;
    public String imageStr;
    public Integer resourceId;
    public int textureHeight;
    public int textureHeightWhole;
    public int textureLeft;
    public int textureTop;
    public int textureWidth;
    public int textureWidthWhole;
    public int tileColumns;
    public int tileHeight;
    public int tileMargin;
    public int tileRows;
    public int tileSpacing;
    public int tileWidth;

    public void load() {
        int dotIndex = this.imageStr.indexOf(46);
        if (dotIndex > 0) {
            setResourceId(TextureUtility.getInstance().getDrawableResourceId(this.imageStr.substring(0, dotIndex)));
        } else {
            setAltasResource(AltasTextureManager.getInstance().getAltasResource(this.imageStr));
        }
        TilesetManager.getInstance().add(this);
    }

    public void bindTexture() {
        JNILibrary.bindTexture(this.resourceId.intValue());
    }

    private void setResourceId(Integer resourceId2) {
        this.resourceId = resourceId2;
        Vector2 size = TextureUtility.getInstance().getTextureSize(resourceId2);
        this.textureLeft = 0;
        this.textureTop = 0;
        int i = (int) size.X;
        this.textureWidth = i;
        this.textureWidthWhole = i;
        int i2 = (int) size.Y;
        this.textureHeight = i2;
        this.textureHeightWhole = i2;
        this.tileColumns = (this.textureWidth - this.tileSpacing) / this.tileWidth;
        this.tileRows = (this.textureHeight - this.tileSpacing) / this.tileHeight;
    }

    private void setAltasResource(AltasResource altasResource) {
        setResourceId(altasResource.getAltasResourceId());
        Vector2 size = TextureUtility.getInstance().getTextureSize(this.resourceId);
        this.textureWidthWhole = (int) size.X;
        this.textureHeightWhole = (int) size.Y;
        this.textureLeft = altasResource.getLeft();
        this.textureTop = altasResource.getTop();
        this.textureWidth = altasResource.getWidth();
        this.textureHeight = altasResource.getHeight();
        this.tileColumns = (this.textureWidth - this.tileSpacing) / this.tileWidth;
        this.tileRows = (this.textureHeight - this.tileSpacing) / this.tileHeight;
    }
}
