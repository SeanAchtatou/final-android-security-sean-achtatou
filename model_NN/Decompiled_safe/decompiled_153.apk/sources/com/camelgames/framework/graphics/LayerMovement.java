package com.camelgames.framework.graphics;

public interface LayerMovement {
    float getScale();

    float getXDelta();

    float getYDelta();

    void update();
}
