package com.camelgames.framework.graphics.hardware;

import com.camelgames.framework.graphics.GraphicsManager;
import java.nio.ShortBuffer;
import javax.microedition.khronos.opengles.GL11;

public class HardwareIndexBuffer extends HardwareBuffer {
    private int VBOId;
    private ShortBuffer indexBuffer;
    private int usage = 35044;

    public void fillBufferData(GL11 gl11, ShortBuffer indexBuffer2, int usage2) {
        if (indexBuffer2 != null) {
            if (this.VBOId <= 0) {
                this.VBOId = createVBO(gl11);
            }
            this.indexBuffer = indexBuffer2;
            this.usage = usage2;
            fillBufferData(gl11, this.VBOId, indexBuffer2, indexBuffer2.capacity() * 2, usage2);
        }
    }

    public ShortBuffer getBuffer() {
        return this.indexBuffer;
    }

    public void bindBuffer(GL11 gl11) {
        gl11.glBindBuffer(34963, this.VBOId);
    }

    public void drawElements(GL11 gl11, int count, int offset) {
        gl11.glBindBuffer(34963, this.VBOId);
        gl11.glDrawElements(4, count, 5123, offset * 2);
        gl11.glBindBuffer(34963, 0);
    }

    public void deviceLost(GL11 gl11) {
        this.VBOId = 0;
    }

    public void deviceReset(GL11 gl11) {
        fillBufferData(gl11, this.indexBuffer, this.usage);
    }

    /* access modifiers changed from: protected */
    public void disposeInternal() {
        releaseVBO(GraphicsManager.getInstance().getGL11(), this.VBOId);
        this.VBOId = 0;
        this.indexBuffer = null;
        super.disposeInternal();
    }
}
