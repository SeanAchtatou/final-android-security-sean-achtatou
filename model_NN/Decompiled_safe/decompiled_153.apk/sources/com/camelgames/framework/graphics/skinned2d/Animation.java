package com.camelgames.framework.graphics.skinned2d;

import android.content.res.XmlResourceParser;
import com.camelgames.framework.levels.LevelScriptReader;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class Animation {
    private float duration;
    private int fps;
    private JointFrame[] frames;
    private final SkinnedModel2D model;
    private String name;
    private float singleFrameTime;

    public Animation(SkinnedModel2D model2) {
        this.model = model2;
    }

    public void loadFrames(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        LevelScriptReader.xmlRequireStartTag(xmlParser, "Frames");
        setName(LevelScriptReader.getString(xmlParser, "Name"));
        setFPS(LevelScriptReader.getInt(xmlParser, "FPS", 0));
        this.duration = ((float) LevelScriptReader.getInt(xmlParser, "Count", 0)) * this.singleFrameTime;
        this.frames = new JointFrame[LevelScriptReader.getInt(xmlParser, "KeyCount", 0)];
        int jointCount = this.model.joints.length;
        int index = 0;
        while (xmlParser.nextTag() == 2) {
            JointFrame frame = new JointFrame();
            frame.time = LevelScriptReader.getFloat(xmlParser, "Time", 0.0f);
            frame.jointPositions = new float[(jointCount * 2)];
            String[] posStrings = LevelScriptReader.getString(xmlParser, "Pos").split(",");
            for (int i = 0; i < posStrings.length; i++) {
                String posString = posStrings[i];
                int splitIndex = posString.indexOf(32);
                float length = Float.parseFloat(posString.substring(0, splitIndex));
                float angle = Float.parseFloat(posString.substring(splitIndex + 1, posString.length()));
                frame.jointPositions[i * 2] = length;
                frame.jointPositions[(i * 2) + 1] = angle;
            }
            this.frames[index] = frame;
            xmlParser.nextTag();
            index++;
        }
    }

    public void scale(float scale) {
        for (JointFrame scale2 : this.frames) {
            scale2.scale(scale);
        }
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setFPS(int fps2) {
        this.fps = fps2;
        this.singleFrameTime = 1.0f / ((float) fps2);
    }

    public int getFPS() {
        return this.fps;
    }

    public float getDuration() {
        return this.duration;
    }

    public JointFrame[] getFrames() {
        return this.frames;
    }

    public SkinnedModel2D getModel() {
        return this.model;
    }
}
