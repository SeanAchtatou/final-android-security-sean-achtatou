package com.camelgames.framework.graphics.altas;

import com.camelgames.framework.graphics.textures.TextureUtility;
import com.camelgames.framework.utilities.IOUtility;
import com.camelgames.ndk.JNILibrary;
import java.util.HashMap;

public class AltasTextureManager {
    private static final AltasTextureManager instance = new AltasTextureManager();
    private Class arrayClass;
    private boolean isInitiated;
    private HashMap<Integer, AltasResource> subTextures = new HashMap<>();

    public static AltasTextureManager getInstance() {
        return instance;
    }

    private AltasTextureManager() {
    }

    public void initiate(Class arrayClass2) {
        if (!this.isInitiated) {
            this.arrayClass = arrayClass2;
            if (arrayClass2 != null) {
                int[] altasIds = IOUtility.getResourceIdsByPrefix(arrayClass2, "altas", null);
                for (int altasId : altasIds) {
                    getAltasResource(Integer.valueOf(altasId));
                }
            }
            this.isInitiated = true;
            return;
        }
        for (AltasResource resource : this.subTextures.values()) {
            resource.reloadAltasResourceId();
        }
    }

    public AltasResource getAltasResource(Integer resourceId) {
        if (!this.subTextures.containsKey(resourceId)) {
            this.subTextures.put(resourceId, new AltasResource(resourceId.intValue()));
        }
        return this.subTextures.get(resourceId);
    }

    public void pushAltasInfos() {
        int[] altasInfo = new int[((this.subTextures.size() * 6) + 1)];
        int index = 0;
        for (AltasResource resource : this.subTextures.values()) {
            int index2 = index + 1;
            altasInfo[index] = resource.getSubResourceId().intValue();
            Integer altasResourceId = resource.getAltasResourceId();
            if (altasResourceId == null) {
                altasResourceId = -1;
            }
            int index3 = index2 + 1;
            altasInfo[index2] = altasResourceId.intValue();
            int index4 = index3 + 1;
            altasInfo[index3] = resource.getLeft();
            int index5 = index4 + 1;
            altasInfo[index4] = resource.getTop();
            int index6 = index5 + 1;
            altasInfo[index5] = resource.getWidth();
            index = index6 + 1;
            altasInfo[index6] = resource.getHeight();
        }
        int i = index + 1;
        altasInfo[index] = 0;
        JNILibrary.setTextureData(altasInfo);
    }

    public AltasResource getAltasResource(String name) {
        return getAltasResource(IOUtility.getResourceIdByName(this.arrayClass, name));
    }

    public Integer getAltasIdFromSubId(Integer subResourceId) {
        String resourceName = IOUtility.getResourceNameById(this.arrayClass, subResourceId.intValue());
        return TextureUtility.getInstance().getDrawableResourceId(resourceName.substring(0, resourceName.indexOf(95)));
    }

    public boolean isIdInside(Integer subResourceId) {
        return this.subTextures.containsKey(subResourceId);
    }
}
