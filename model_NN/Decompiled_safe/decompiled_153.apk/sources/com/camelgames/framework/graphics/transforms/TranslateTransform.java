package com.camelgames.framework.graphics.transforms;

import android.opengl.Matrix;
import javax.microedition.khronos.opengles.GL10;

public class TranslateTransform implements Transform {
    private float x;
    private float y;

    public TranslateTransform() {
    }

    public TranslateTransform(float x2, float y2) {
        setPosition(x2, y2);
    }

    public void setPosition(float x2, float y2) {
        this.x = x2;
        this.y = y2;
    }

    public void transform(GL10 gl) {
        gl.glTranslatef(this.x, this.y, 0.0f);
    }

    public void transform(float[] matrix) {
        Matrix.translateM(matrix, 0, this.x, this.y, 0.0f);
    }
}
