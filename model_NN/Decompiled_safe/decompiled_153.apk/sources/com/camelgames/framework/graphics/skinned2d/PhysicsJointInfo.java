package com.camelgames.framework.graphics.skinned2d;

public class PhysicsJointInfo {
    public float lowerAngle;
    public ImageNode node0;
    public ImageNode node1;
    public float offsetX0;
    public float offsetX1;
    public float offsetY0;
    public float offsetY1;
    public float upperAngle;
}
