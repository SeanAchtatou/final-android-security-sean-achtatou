package com.camelgames.framework.graphics.font;

import com.camelgames.framework.ui.UIUtility;
import javax.microedition.khronos.opengles.GL10;

public class OESText {
    private float alpha = 1.0f;
    private float b = 1.0f;
    private int fontMargin;
    private int fontSize;
    private float g = 1.0f;
    private boolean isVisible = true;
    private int left;
    private int offsetX;
    private float r = 1.0f;
    private String text;
    private int top;

    public OESText() {
        setFontSize(UIUtility.getDisplayWidthPlusHeight(0.03f));
    }

    public void setLeftTop(int left2, int top2) {
        this.left = left2;
        this.top = top2;
    }

    public void setRightTop(int right, int top2) {
        setLeftTop(right - getTextWidth(), top2);
    }

    public void setCenterTop(int centerX, int top2) {
        setLeftTop(centerX - (getTextWidth() / 2), top2);
    }

    public void setCenter(int centerX, int centerY) {
        setLeftTop(centerX - (getTextWidth() / 2), centerY - (this.fontSize / 2));
    }

    public void setFontSize(int fontSize2) {
        this.fontSize = fontSize2;
        setFontMargin((int) (0.5f * ((float) fontSize2)));
    }

    public void setFontMargin(int fontMargin2) {
        this.fontMargin = fontMargin2;
        this.offsetX = (-(this.fontSize - fontMargin2)) / 2;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public int getLeft() {
        return this.left;
    }

    public int getTop() {
        return this.top;
    }

    public int getFontSize() {
        return this.fontSize;
    }

    public float getFontMargin() {
        return (float) this.fontMargin;
    }

    public int getTextWidth() {
        if (this.text != null) {
            return (this.text.length() + 1) * this.fontMargin;
        }
        return 0;
    }

    public String getText() {
        return this.text;
    }

    public void draw(GL10 gl, TextBuilder textBuilder) {
        if (this.isVisible && this.text != null) {
            prepareColor(gl);
            TextBuilder textBuilder2 = textBuilder;
            textBuilder2.drawString(this.text, this.offsetX + this.left, this.top, this.fontSize, this.fontMargin);
        }
    }

    public void prepareColor(GL10 gl) {
        gl.glColor4f(this.r, this.g, this.b, this.alpha);
    }

    public boolean isVisible() {
        return this.isVisible;
    }

    public void setVisible(boolean isVisible2) {
        this.isVisible = isVisible2;
    }

    public void setColor(float r2, float g2, float b2) {
        this.r = r2;
        this.g = g2;
        this.b = b2;
    }

    public void setAlpha(float alpha2) {
        this.alpha = alpha2;
    }
}
