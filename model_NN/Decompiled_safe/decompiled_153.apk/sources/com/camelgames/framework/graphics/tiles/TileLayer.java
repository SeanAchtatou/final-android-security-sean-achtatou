package com.camelgames.framework.graphics.tiles;

import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.LayerMovement;
import com.camelgames.framework.graphics.hardware.HardwareVertexBuffer;
import com.camelgames.framework.resources.AbstractDisposable;
import com.camelgames.framework.utilities.IOUtility;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

public class TileLayer extends AbstractDisposable {
    private static final int[] texCoordsEOS = new int[4];
    private static final ArrayList<Tile> tileArray = new ArrayList<>();
    private final int columnCount;
    private LayerMovement layerMovement;
    private float left;
    public String name;
    private final int rowCount;
    private HardwareVertexBuffer textureRectThinBuffer;
    private int tileCount;
    private TileSet tileSet;
    private float tileSize;
    private float top;
    private HardwareVertexBuffer verticeRectsBuffer;

    public TileLayer(int columnCount2, int rowCount2) {
        this.columnCount = columnCount2;
        this.rowCount = rowCount2;
    }

    /* access modifiers changed from: protected */
    public void disposeInternal() {
        this.verticeRectsBuffer.dispose();
        this.verticeRectsBuffer = null;
        this.textureRectThinBuffer.dispose();
        this.textureRectThinBuffer = null;
    }

    public void setTile(int column, int row, int gid) {
        int index;
        if (this.tileSet == null) {
            this.tileSet = TilesetManager.getInstance().getTileSet(gid);
            if (this.tileSet != null) {
                texCoordsEOS[2] = this.tileSet.tileWidth;
                texCoordsEOS[3] = -this.tileSet.tileHeight;
            }
        }
        if (this.tileSet != null && (index = gid - this.tileSet.firstGid) >= 0) {
            tileArray.add(new Tile(index % this.tileSet.tileColumns, index / this.tileSet.tileColumns, column, row));
        }
    }

    public void initiate() {
        this.tileCount = tileArray.size();
        float[] textureRectThin = new float[((tileArray.size() * 14) - 6)];
        for (int i = 0; i < this.tileCount; i++) {
            Tile tile = tileArray.get(i);
            float left2 = (((float) ((this.tileSet.textureLeft + ((this.tileSet.tileWidth + this.tileSet.tileSpacing) * tile.TileSetColumn)) + this.tileSet.tileSpacing)) + 0.5f) / ((float) this.tileSet.textureWidthWhole);
            float top2 = (((float) ((this.tileSet.textureTop + ((this.tileSet.tileHeight + this.tileSet.tileSpacing) * tile.TileSetRow)) + this.tileSet.tileSpacing)) + 0.5f) / ((float) this.tileSet.textureHeightWhole);
            float right = (((float) (((this.tileSet.textureLeft + ((this.tileSet.tileWidth + this.tileSet.tileSpacing) * tile.TileSetColumn)) + this.tileSet.tileSpacing) + this.tileSet.tileWidth)) - 0.5f) / ((float) this.tileSet.textureWidthWhole);
            float bottom = (((float) (((this.tileSet.textureTop + ((this.tileSet.tileHeight + this.tileSet.tileSpacing) * tile.TileSetRow)) + this.tileSet.tileSpacing) + this.tileSet.tileHeight)) - 0.5f) / ((float) this.tileSet.textureHeightWhole);
            textureRectThin[(i * 14) + 0] = left2;
            textureRectThin[(i * 14) + 1] = bottom;
            textureRectThin[(i * 14) + 2] = left2;
            textureRectThin[(i * 14) + 3] = top2;
            textureRectThin[(i * 14) + 4] = right;
            textureRectThin[(i * 14) + 5] = bottom;
            textureRectThin[(i * 14) + 6] = right;
            textureRectThin[(i * 14) + 7] = top2;
        }
        this.textureRectThinBuffer = new HardwareVertexBuffer();
        this.textureRectThinBuffer.fillBufferData(GraphicsManager.getInstance().getGL11(), IOUtility.createFloatBuffer(textureRectThin), 35044);
        float[] verticeRects = new float[((tileArray.size() * 14) - 6)];
        for (int i2 = 0; i2 < this.tileCount; i2++) {
            Tile tile2 = tileArray.get(i2);
            int left3 = tile2.Column;
            int top3 = tile2.Row;
            int right2 = left3 + 1;
            int bottom2 = top3 + 1;
            verticeRects[(i2 * 14) + 0] = (float) left3;
            verticeRects[(i2 * 14) + 1] = (float) bottom2;
            verticeRects[(i2 * 14) + 2] = (float) left3;
            verticeRects[(i2 * 14) + 3] = (float) top3;
            verticeRects[(i2 * 14) + 4] = (float) right2;
            verticeRects[(i2 * 14) + 5] = (float) bottom2;
            verticeRects[(i2 * 14) + 6] = (float) right2;
            verticeRects[(i2 * 14) + 7] = (float) top3;
            if (i2 < this.tileCount - 1) {
                verticeRects[(i2 * 14) + 8] = (float) right2;
                verticeRects[(i2 * 14) + 9] = (float) top3;
            }
            if (i2 > 0) {
                verticeRects[((i2 * 14) + 10) - 14] = (float) left3;
                verticeRects[((i2 * 14) + 11) - 14] = (float) bottom2;
                verticeRects[((i2 * 14) + 12) - 14] = (float) left3;
                verticeRects[((i2 * 14) + 13) - 14] = (float) bottom2;
            }
        }
        this.verticeRectsBuffer = new HardwareVertexBuffer();
        this.verticeRectsBuffer.fillBufferData(GraphicsManager.getInstance().getGL11(), FloatBuffer.wrap(verticeRects), 35044);
        tileArray.clear();
    }

    public int getColumns() {
        return this.columnCount;
    }

    public int getRows() {
        return this.rowCount;
    }

    public void setLayerMovement(LayerMovement layerMovement2) {
        this.layerMovement = layerMovement2;
    }

    public void render(GL10 gl) {
        gl.glPushMatrix();
        if (this.layerMovement != null) {
            this.layerMovement.update();
            gl.glTranslatef(this.left + this.layerMovement.getXDelta(), this.top + this.layerMovement.getYDelta(), 0.0f);
            gl.glScalef(this.tileSize * this.layerMovement.getScale(), this.tileSize * this.layerMovement.getScale(), 1.0f);
        } else {
            gl.glTranslatef(this.left, this.top, 0.0f);
            gl.glScalef(this.tileSize, this.tileSize, 1.0f);
        }
        this.tileSet.bindTexture();
        GL11 gl11 = (GL11) gl;
        this.textureRectThinBuffer.prepareTexCoordPointer(gl11);
        this.verticeRectsBuffer.prepareVertexPointer(gl11);
        gl.glDrawArrays(5, 0, (this.tileCount * 7) - 3);
        gl.glPopMatrix();
    }

    public void setLeftTop(float left2, float top2) {
        this.left = left2;
        this.top = top2;
    }

    public void setTileSize(float tileSize2) {
        this.tileSize = tileSize2;
    }

    private static class Tile {
        public int Column;
        public int Row;
        public int TileSetColumn;
        public int TileSetRow;

        public Tile(int tileSetColumn, int tileSetRow, int column, int row) {
            this.TileSetColumn = tileSetColumn;
            this.TileSetRow = tileSetRow;
            this.Column = column;
            this.Row = row;
        }
    }
}
