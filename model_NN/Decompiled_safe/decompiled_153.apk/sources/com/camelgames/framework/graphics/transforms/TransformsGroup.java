package com.camelgames.framework.graphics.transforms;

import java.util.Iterator;
import java.util.LinkedList;
import javax.microedition.khronos.opengles.GL10;

public class TransformsGroup implements Transform {
    private float[] matrix;
    private LinkedList<Transform> transforms = new LinkedList<>();

    public void add(Transform transform) {
        this.transforms.add(transform);
    }

    public void clear() {
        this.transforms.clear();
    }

    public void transform(GL10 gl) {
        if (this.matrix != null) {
            gl.glMultMatrixf(this.matrix, 0);
            return;
        }
        Iterator<Transform> it = this.transforms.iterator();
        while (it.hasNext()) {
            it.next().transform(gl);
        }
    }

    public void transform(float[] matrix2) {
        Iterator<Transform> it = this.transforms.iterator();
        while (it.hasNext()) {
            it.next().transform(matrix2);
        }
    }

    public void initiateMatrix(GL10 gl) {
        this.matrix = new float[]{1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f};
        transform(this.matrix);
    }
}
