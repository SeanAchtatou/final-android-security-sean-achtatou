package com.camelgames.framework.graphics;

import com.camelgames.framework.math.Vector2;
import javax.microedition.khronos.opengles.GL10;

public abstract class Camera {
    protected GL10 gl;
    protected int screenHeight;
    protected int screenWidth;

    public abstract void restoreCamera();

    public abstract void screenToWorld(Vector2 vector2);

    public abstract float screenToWorldX(float f);

    public abstract float screenToWorldY(float f);

    public abstract float worldToScreenX(float f);

    public abstract float worldToScreenY(float f);

    public void initiate(int screenWidth2, int screenHeight2, GL10 gl2) {
        this.screenWidth = screenWidth2;
        this.screenHeight = screenHeight2;
        this.gl = gl2;
        restoreCamera();
    }
}
