package com.camelgames.framework.graphics;

import android.opengl.GLU;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.math.Vector3;

public class Camera3D extends Camera {
    protected Vector3 eye = new Vector3();
    protected float fovy;
    protected Vector3 lookAt = new Vector3();
    protected Vector3 up = new Vector3();
    protected float zFar;
    protected float zNear;

    public Camera3D(float fovy2, float zNear2, float zFar2) {
        this.fovy = fovy2;
        this.zNear = zNear2;
        this.zFar = zFar2;
        this.eye.Set(0.0f, 0.0f, 1.0f);
        this.lookAt.Set(0.0f, 0.0f, 0.0f);
        this.up.Set(0.0f, 1.0f, 0.0f);
    }

    public void restoreCamera() {
        updateProjectionMatrix();
        updateModelViewMatrix();
    }

    public void screenToWorld(Vector2 screenV) {
    }

    public float screenToWorldX(float screenX) {
        return 0.0f;
    }

    public float screenToWorldY(float screenY) {
        return 0.0f;
    }

    public float worldToScreenX(float viewX) {
        return 0.0f;
    }

    public float worldToScreenY(float viewY) {
        return 0.0f;
    }

    public void setupProjection(float fovy2, float aspect, float zNear2, float zFar2) {
        this.fovy = fovy2;
        this.zNear = zNear2;
        this.zFar = zFar2;
        this.gl.glMatrixMode(5889);
        this.gl.glLoadIdentity();
        GLU.gluPerspective(this.gl, fovy2, aspect, zNear2, zFar2);
        this.gl.glMatrixMode(5888);
    }

    public void setCamera(float eyeX, float eyeY, float eyeZ) {
        setCamera(eyeX, eyeY, eyeZ, this.lookAt.X, this.lookAt.Y, this.lookAt.Z, this.up.X, this.up.Y, this.up.Z);
    }

    public void setCamera(float eyeX, float eyeY, float eyeZ, float lookAtX, float lookAtY, float lookAtZ) {
        setCamera(eyeX, eyeY, eyeZ, lookAtX, lookAtY, lookAtZ, this.up.X, this.up.Y, this.up.Z);
    }

    public void setCamera(float eyeX, float eyeY, float eyeZ, float lookAtX, float lookAtY, float lookAtZ, float upX, float upY, float upZ) {
        this.eye.Set(eyeX, eyeY, eyeZ);
        this.lookAt.Set(lookAtX, lookAtY, lookAtZ);
        this.up.Set(upX, upY, upZ);
        updateModelViewMatrix();
    }

    public Vector3 getEye() {
        return this.eye;
    }

    public Vector3 getLookAt() {
        return this.lookAt;
    }

    public Vector3 getUp() {
        return this.up;
    }

    public void updateProjectionMatrix() {
        float aspect = ((float) this.screenWidth) / ((float) this.screenHeight);
        this.gl.glMatrixMode(5889);
        this.gl.glLoadIdentity();
        GLU.gluPerspective(this.gl, this.fovy, aspect, this.zNear, this.zFar);
    }

    public void updateModelViewMatrix() {
        this.gl.glMatrixMode(5888);
        this.gl.glLoadIdentity();
        GLU.gluLookAt(this.gl, this.eye.X, this.eye.Y, this.eye.Z, this.lookAt.X, this.lookAt.Y, this.lookAt.Z, this.up.X, this.up.Y, this.up.Z);
    }
}
