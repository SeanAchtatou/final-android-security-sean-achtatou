package com.camelgames.framework.graphics;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.microedition.khronos.opengles.GL10;

final class RenderQueue {
    private ArrayList<Renderable>[] renderables = ((ArrayList[]) Array.newInstance(ArrayList.class, 5));
    private LinkedList<Renderable> waitToAdd = new LinkedList<>();
    private LinkedList<Renderable> waitToDelete = new LinkedList<>();

    public RenderQueue() {
        for (int i = 0; i < this.renderables.length; i++) {
            this.renderables[i] = new ArrayList<>();
        }
    }

    public void render(GL10 gl, float elapsedTime) {
        sync();
        for (ArrayList<Renderable> items : this.renderables) {
            for (int j = 0; j < items.size(); j++) {
                items.get(j).render(gl, elapsedTime);
            }
        }
    }

    public void Add(Renderable item) {
        if (item != null) {
            if (!this.waitToAdd.contains(item)) {
                this.waitToAdd.add(item);
            }
            if (this.waitToDelete.contains(item)) {
                this.waitToDelete.remove(item);
            }
        }
    }

    public void Remove(Renderable item) {
        if (item != null) {
            if (!this.waitToDelete.contains(item)) {
                this.waitToDelete.add(item);
            }
            if (this.waitToAdd.contains(item)) {
                this.waitToAdd.remove(item);
            }
        }
    }

    public void clear() {
        for (ArrayList<Renderable> queue : this.renderables) {
            queue.clear();
        }
        this.waitToAdd.clear();
        this.waitToDelete.clear();
    }

    public boolean contains(Renderable item) {
        return this.renderables[item.getPriority().ordinal()].contains(item);
    }

    private void sync() {
        if (!this.waitToAdd.isEmpty()) {
            Renderable item = this.waitToAdd.poll();
            while (item != null) {
                if (!contains(item)) {
                    this.renderables[item.getPriority().ordinal()].add(item);
                }
                item = this.waitToAdd.poll();
            }
        }
        if (!this.waitToDelete.isEmpty()) {
            Renderable item2 = this.waitToDelete.poll();
            while (item2 != null) {
                this.renderables[item2.getPriority().ordinal()].remove(item2);
                item2 = this.waitToDelete.poll();
            }
        }
    }
}
