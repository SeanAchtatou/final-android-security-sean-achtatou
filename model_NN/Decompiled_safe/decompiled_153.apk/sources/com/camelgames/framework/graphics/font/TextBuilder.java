package com.camelgames.framework.graphics.font;

import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.ndk.graphics.Texture;
import com.camelgames.ndk.graphics.TextureManager;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;

public class TextBuilder {
    private static final char[] numbers = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    private float a;
    private float b;
    private int baseSize;
    private float g;
    private int[] getImageFontRec_return;
    private int offset;
    private float r;
    private ArrayList<OESText> texts;
    private Texture texture;

    public TextBuilder(Integer resourceId, int baseSize2) {
        this(resourceId, baseSize2, 0);
    }

    public TextBuilder(Integer resourceId, int baseSize2, int offset2) {
        this.r = 1.0f;
        this.g = 1.0f;
        this.b = 1.0f;
        this.a = 1.0f;
        this.texts = new ArrayList<>();
        setResourceId(resourceId.intValue());
        this.baseSize = baseSize2;
        this.offset = offset2;
        int[] iArr = new int[4];
        iArr[2] = baseSize2 - 1;
        iArr[3] = (-baseSize2) + 1;
        this.getImageFontRec_return = iArr;
    }

    public void render(GL10 gl, float elapsedTime) {
        gl.glBlendFunc(1, 771);
        for (int i = 0; i < this.texts.size(); i++) {
            this.texts.get(i).draw(gl, this);
        }
    }

    public void setResourceId(int resourceId) {
        this.texture = TextureManager.getInstance().getTexture(resourceId);
    }

    public void add(OESText text) {
        if (!this.texts.contains(text)) {
            this.texts.add(text);
        }
    }

    public void remove(OESText text) {
        this.texts.remove(text);
    }

    public void clear() {
        this.texts.clear();
    }

    public int getStringWidth(StringBuffer text, int fontSize, float marginPersent) {
        return ((int) (((float) (text.length() * fontSize)) * marginPersent)) + (fontSize / 2);
    }

    public void drawFpsString(GL10 gl, int fps, int startX, int startY, int fontSize, int fontMargin) {
        gl.glColor4f(this.r, this.g, this.b, this.a);
        this.texture.bindTexture();
        int fps2 = Math.max(0, fps) % 100;
        GraphicsManager.getInstance().drawTexiOES(getImageFontRec(numbers[fps2 / 100]), startX, startY, fontSize, fontSize);
        int startX2 = startX + fontMargin;
        GraphicsManager.getInstance().drawTexiOES(getImageFontRec(numbers[(fps2 / 10) % 10]), startX2, startY, fontSize, fontSize);
        int startX3 = startX2 + fontMargin;
        GraphicsManager.getInstance().drawTexiOES(getImageFontRec(numbers[fps2 % 10]), startX3, startY, fontSize, fontSize);
        int startX4 = startX3 + fontMargin;
    }

    public void drawString(String text, int left, int top, int fontSize) {
        drawString(text, left, top, fontSize, (int) (0.6f * ((float) fontSize)));
    }

    public void drawString(String text, int left, int top, int fontSize, int fontMargin) {
        this.texture.bindTexture();
        if (text != null) {
            int row = 0;
            int column = 0;
            for (int i = 0; i < text.length(); i++) {
                if (text.charAt(i) == 10) {
                    column = 0;
                    row++;
                } else {
                    GraphicsManager.getInstance().drawTexiOES(getImageFontRec(text.charAt(i)), (fontMargin * column) + left, (row * fontSize) + top, fontSize, fontSize);
                    column++;
                }
            }
        }
    }

    public void setFPSColor(float r2, float g2, float b2, float a2) {
        this.r = r2;
        this.g = g2;
        this.b = b2;
        this.a = a2;
    }

    private int[] getImageFontRec(char ch) {
        int left = (this.offset + ch) % 16;
        int top = ((this.offset + ch) / 16) + 1;
        this.getImageFontRec_return[0] = (this.baseSize * left) + 1;
        this.getImageFontRec_return[1] = (this.baseSize * top) - 1;
        return this.getImageFontRec_return;
    }

    public int getBaseSize() {
        return this.baseSize;
    }

    public void setOffset(int offset2) {
        this.offset = offset2;
    }

    public int getOffset() {
        return this.offset;
    }
}
