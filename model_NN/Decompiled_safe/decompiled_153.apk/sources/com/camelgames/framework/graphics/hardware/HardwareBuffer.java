package com.camelgames.framework.graphics.hardware;

import com.camelgames.framework.resources.AbstractDisposable;
import java.nio.Buffer;
import javax.microedition.khronos.opengles.GL11;

public abstract class HardwareBuffer extends AbstractDisposable {
    private static int[] buffer = new int[1];

    public abstract void deviceLost(GL11 gl11);

    public abstract void deviceReset(GL11 gl11);

    public HardwareBuffer() {
        HardwareBufferManager.getInstance().add(this);
    }

    public static int createVBO(GL11 gl11) {
        gl11.glGenBuffers(1, buffer, 0);
        return buffer[0];
    }

    public static void fillBufferData(GL11 gl11, int id, Buffer data, int size, int usage) {
        gl11.glBindBuffer(34962, id);
        gl11.glBufferData(34962, size, data, usage);
        gl11.glBindBuffer(34962, 0);
    }

    public static void releaseVBO(GL11 gl11, int id) {
        if (id > 0) {
            buffer[0] = id;
            gl11.glDeleteBuffers(1, buffer, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void disposeInternal() {
        HardwareBufferManager.getInstance().remove(this);
    }
}
