package com.camelgames.framework.graphics.transforms;

import android.opengl.Matrix;
import javax.microedition.khronos.opengles.GL10;

public class RotateTransform implements Transform {
    private float rotation;

    public RotateTransform() {
    }

    public RotateTransform(float rotation2) {
        setRotation(rotation2);
    }

    public void setRotation(float rotation2) {
        this.rotation = rotation2;
    }

    public void transform(GL10 gl) {
        gl.glRotatef(this.rotation, 0.0f, 0.0f, 1.0f);
    }

    public void transform(float[] matrix) {
        Matrix.rotateM(matrix, 0, this.rotation, 0.0f, 0.0f, 1.0f);
    }
}
