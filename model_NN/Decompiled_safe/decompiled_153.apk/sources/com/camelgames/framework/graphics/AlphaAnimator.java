package com.camelgames.framework.graphics;

import javax.microedition.khronos.opengles.GL10;

public class AlphaAnimator {
    private Animator animator = new Animator();
    private float scaleStep;

    public AlphaAnimator() {
        setScaleInfo(1.5f, 0.5f, 0.02f);
    }

    public void update(float elapsedTime) {
        if (!isStoped()) {
            this.animator.update(elapsedTime);
        }
    }

    public boolean isStoped() {
        return this.animator.isStoped();
    }

    public void prepareRender(GL10 gl, float r, float g, float b) {
        gl.glBlendFunc(770, 1);
        gl.glColor4f(r, g, b, getCurrentScale());
    }

    public void setScaleInfo(float maxScale, float minScale, float scaleStep2) {
        this.scaleStep = scaleStep2;
        this.animator.setMaxFrame((int) (maxScale / scaleStep2));
        this.animator.setMinFrame((int) (minScale / scaleStep2));
    }

    public void setCurrentScale(float scale) {
        this.animator.setCurrentFrame((int) (scale / this.scaleStep));
    }

    public float getCurrentScale() {
        return this.scaleStep * ((float) this.animator.getCurrentFrame());
    }

    public void setLoop(boolean isLoop) {
        this.animator.setLoop(isLoop);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void setPosition(float position) {
        float position2 = Math.max(0.0f, Math.min(1.0f, position));
        this.animator.setCurrentFrame((int) (((1.0f - position2) * ((float) this.animator.getMinFrame())) + (((float) this.animator.getMaxFrame()) * position2)));
    }

    public void setStop(boolean isStoped) {
        this.animator.setStoped(isStoped);
    }

    public void setChangeTime(float changeTime) {
        this.animator.setChangeTime(changeTime);
    }

    public void setBackward(boolean isBackward) {
        this.animator.setBackward(isBackward);
    }

    public void setFold(boolean isFold) {
        this.animator.setFold(isFold);
    }
}
