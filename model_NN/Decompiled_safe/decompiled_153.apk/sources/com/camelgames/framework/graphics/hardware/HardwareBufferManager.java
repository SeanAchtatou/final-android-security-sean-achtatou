package com.camelgames.framework.graphics.hardware;

import java.util.Iterator;
import java.util.LinkedList;
import javax.microedition.khronos.opengles.GL11;

public class HardwareBufferManager {
    private static HardwareBufferManager instance = new HardwareBufferManager();
    private LinkedList<HardwareBuffer> buffers = new LinkedList<>();

    public static HardwareBufferManager getInstance() {
        return instance;
    }

    private HardwareBufferManager() {
    }

    public void add(HardwareBuffer buffer) {
        if (buffer != null && !this.buffers.contains(buffer)) {
            this.buffers.add(buffer);
        }
    }

    public void remove(HardwareBuffer buffer) {
        this.buffers.remove(buffer);
    }

    public void deviceLost(GL11 gl11) {
        Iterator<HardwareBuffer> it = this.buffers.iterator();
        while (it.hasNext()) {
            it.next().deviceLost(gl11);
        }
    }

    public void deviceReset(GL11 gl11) {
        Iterator<HardwareBuffer> it = this.buffers.iterator();
        while (it.hasNext()) {
            it.next().deviceReset(gl11);
        }
    }
}
