package com.camelgames.framework.graphics;

import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.graphics.hardware.HardwareBufferManager;
import com.camelgames.framework.graphics.hardware.HardwareIndexBuffer;
import com.camelgames.framework.graphics.hardware.HardwareVertexBuffer;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.utilities.IOUtility;
import com.camelgames.ndk.JNILibrary;
import com.camelgames.ndk.graphics.TextureManager;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

public final class GraphicsManager {
    public static final float[] defaultTexture = {0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f};
    public static final float[] defaultVertices = {-0.5f, -0.5f, -0.5f, 0.5f, 0.5f, -0.5f, 0.5f, 0.5f};
    private static GraphicsManager instance = new GraphicsManager();
    private float[] backgroundColor = {0.0f, 0.0f, 0.0f, 1.0f};
    private Camera camera;
    private HardwareIndexBuffer defaultHardwareIndexRectBuffer;
    private HardwareVertexBuffer defaultHardwareTextureBuffer;
    private HardwareVertexBuffer defaultHardwareVertexBuffer;
    private final FloatBuffer defaultTextureBuffer = IOUtility.createFloatBuffer(defaultTexture);
    private final FloatBuffer defaultVertexBuffer = IOUtility.createFloatBuffer(defaultVertices);
    private GL10 gl;
    private GL11 gl11;
    private GL11Ext gl11Ext;
    private int indexBufferRectCount = 512;
    private int[] landscapeScreenCoordsOES;
    private float[] landscapeScreenTexture = null;
    private FloatBuffer landscapeTextureBuffer;
    private int[] portraitScreenCoordsOES;
    private float[] portraitScreenTexture = null;
    private FloatBuffer portraitTextureBuffer;
    private RenderQueue renderQueue = new RenderQueue();
    private int screenHeight;
    private float screenHeightReciprocal;
    private int screenWidth;
    private float screenWidthReciprocal;
    private float xScale;
    private float yScale;

    public static GraphicsManager getInstance() {
        return instance;
    }

    private GraphicsManager() {
        setBkInWVGA(false);
    }

    public static float[] getCopyOfDefaultTexture() {
        float[] copy = new float[defaultTexture.length];
        System.arraycopy(defaultTexture, 0, copy, 0, defaultTexture.length);
        return copy;
    }

    public int getIndexBufferRectCount() {
        return this.indexBufferRectCount;
    }

    public void initiateIndexBuffer(int indexBufferRectCount2) {
        this.indexBufferRectCount = indexBufferRectCount2;
        short[] indexes = new short[(indexBufferRectCount2 * 6)];
        for (int i = 0; i < indexBufferRectCount2; i++) {
            indexes[(i * 6) + 0] = (short) ((i * 4) + 0);
            indexes[(i * 6) + 1] = (short) ((i * 4) + 1);
            indexes[(i * 6) + 2] = (short) ((i * 4) + 2);
            indexes[(i * 6) + 3] = (short) ((i * 4) + 1);
            indexes[(i * 6) + 4] = (short) ((i * 4) + 3);
            indexes[(i * 6) + 5] = (short) ((i * 4) + 2);
        }
        if (this.defaultHardwareIndexRectBuffer == null) {
            this.defaultHardwareIndexRectBuffer = new HardwareIndexBuffer();
        }
        this.defaultHardwareIndexRectBuffer.fillBufferData(this.gl11, ShortBuffer.wrap(indexes), 35044);
    }

    public static float aspect() {
        return ((float) screenWidth()) / ((float) screenHeight());
    }

    public static int screenWidthPlusHeight() {
        return screenWidth() + screenHeight();
    }

    public static int screenWidth() {
        return instance.getScreenWidth();
    }

    public static int screenHeight() {
        return instance.getScreenHeight();
    }

    public static int maxScreenSize() {
        return Math.max(screenWidth(), screenHeight());
    }

    public static int screenWidth(float rate) {
        return (int) (((float) instance.screenWidth) * rate);
    }

    public static float screenWidthSqt(float rate) {
        return (float) Math.sqrt((double) (((float) instance.screenWidth) * rate));
    }

    public static int screenHeight(float rate) {
        return (int) (((float) instance.screenHeight) * rate);
    }

    public static float screenHeightSqt(float rate) {
        return (float) Math.sqrt((double) (((float) instance.screenHeight) * rate));
    }

    public static void screenToWorld(Vector2 screenV) {
        getInstance().camera.screenToWorld(screenV);
    }

    public static float screenToWorldX(float screenX) {
        return getInstance().camera.screenToWorldX(screenX);
    }

    public static float screenToWorldY(float screenY) {
        return getInstance().camera.screenToWorldY(screenY);
    }

    public static float worldToScreenX(float worldX) {
        return getInstance().camera.worldToScreenX(worldX);
    }

    public static float worldToScreenY(float worldY) {
        return getInstance().camera.worldToScreenY(worldY);
    }

    public void clear() {
        this.renderQueue.clear();
    }

    public void render(GL10 gl2, float elapsedTime) {
        this.renderQueue.render(gl2, elapsedTime);
    }

    public void changeSize(int width, int height) {
        setScreenWidth(width);
        setScreenHeight(height);
        getCamera().initiate(width, height, this.gl);
        this.gl.glViewport(0, 0, this.screenWidth, this.screenHeight);
        EventManager.getInstance().postEvent(EventType.GraphicsResized);
    }

    public float[] getScreenTextureCoords(boolean isPortrait) {
        if (isPortrait) {
            return this.portraitScreenTexture;
        }
        return this.landscapeScreenTexture;
    }

    public int[] getScreenTexCoordsOES(boolean isPortrait) {
        if (isPortrait) {
            return this.portraitScreenCoordsOES;
        }
        return this.landscapeScreenCoordsOES;
    }

    public void setBkInWVGA(boolean isWVGA) {
        if (isWVGA) {
            this.landscapeScreenTexture = new float[]{0.0f, 0.0f, 0.0f, 0.9375f, 0.78125f, 0.0f, 0.78125f, 0.9375f};
            this.portraitScreenTexture = new float[]{0.0f, 0.0f, 0.0f, 0.78125f, 0.9375f, 0.0f, 0.9375f, 0.78125f};
            int[] iArr = new int[4];
            iArr[1] = 480;
            iArr[2] = 800;
            iArr[3] = -480;
            this.landscapeScreenCoordsOES = iArr;
            int[] iArr2 = new int[4];
            iArr2[1] = 800;
            iArr2[2] = 480;
            iArr2[3] = -800;
            this.portraitScreenCoordsOES = iArr2;
        } else {
            this.landscapeScreenTexture = new float[]{0.0f, 0.0f, 0.0f, 0.666f, 1.0f, 0.0f, 1.0f, 0.666f};
            this.portraitScreenTexture = new float[]{0.0f, 0.0f, 0.0f, 1.0f, 0.666f, 0.0f, 0.666f, 1.0f};
            int[] iArr3 = new int[4];
            iArr3[1] = 341;
            iArr3[2] = 512;
            iArr3[3] = -341;
            this.landscapeScreenCoordsOES = iArr3;
            int[] iArr4 = new int[4];
            iArr4[1] = 512;
            iArr4[2] = 341;
            iArr4[3] = -512;
            this.portraitScreenCoordsOES = iArr4;
        }
        this.landscapeTextureBuffer = IOUtility.createFloatBuffer(this.landscapeScreenTexture);
        this.portraitTextureBuffer = IOUtility.createFloatBuffer(this.portraitScreenTexture);
    }

    public void initiate(GL10 gl2) {
        this.gl = gl2;
        this.gl11 = (GL11) gl2;
        this.gl11Ext = (GL11Ext) gl2;
        gl2.glShadeModel(7425);
        gl2.glDisable(3008);
        gl2.glDisable(2896);
        gl2.glDisable(2929);
        gl2.glDisable(3024);
        gl2.glEnable(3553);
        gl2.glEnable(3042);
        gl2.glBlendFunc(1, 771);
        gl2.glEnableClientState(32884);
        gl2.glEnableClientState(32888);
        clearColor(gl2);
        HardwareBufferManager.getInstance().deviceLost(this.gl11);
        HardwareBufferManager.getInstance().deviceReset(this.gl11);
        if (this.defaultHardwareIndexRectBuffer == null) {
            initiateIndexBuffer(this.indexBufferRectCount);
        }
        if (this.defaultHardwareVertexBuffer == null) {
            this.defaultHardwareVertexBuffer = new HardwareVertexBuffer();
            this.defaultHardwareVertexBuffer.fillBufferData(this.gl11, getDefaultVertexBuffer(), 35044);
        }
        if (this.defaultHardwareTextureBuffer == null) {
            this.defaultHardwareTextureBuffer = new HardwareVertexBuffer();
            this.defaultHardwareTextureBuffer.fillBufferData(this.gl11, getDefaultTextureBuffer(), 35044);
        }
        EventManager.getInstance().postEvent(EventType.GraphicsCreated);
    }

    public void AddToRenderQueue(Renderable item) {
        this.renderQueue.Add(item);
    }

    public void RemoveFromQueue(Renderable item) {
        this.renderQueue.Remove(item);
    }

    public boolean contains(Renderable item) {
        return this.renderQueue.contains(item);
    }

    public void setScreenWidth(int width) {
        if (width < 1) {
            width = 1;
        }
        this.screenWidth = width;
        this.screenWidthReciprocal = 1.0f / ((float) width);
        this.xScale = ((float) this.screenWidth) / 480.0f;
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public float getScreenWidthReciprocal() {
        return this.screenWidthReciprocal;
    }

    public void setScreenHeight(int height) {
        if (height < 1) {
            height = 1;
        }
        this.screenHeight = height;
        this.screenHeightReciprocal = 1.0f / ((float) height);
        this.yScale = ((float) this.screenHeight) / 320.0f;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public float getScreenHeightReciprocal() {
        return this.screenHeightReciprocal;
    }

    public GL10 getGL() {
        return this.gl;
    }

    public GL11 getGL11() {
        return this.gl11;
    }

    public GL11Ext getGL11Ext() {
        return this.gl11Ext;
    }

    public void setupWorldMatrix(float x, float y, float width, float height, float angle) {
        this.gl.glTranslatef(x, y, 0.0f);
        this.gl.glRotatef(angle, 0.0f, 0.0f, 1.0f);
        this.gl.glScalef(width, height, 1.0f);
    }

    public void drawScreenTexiOES(Integer resourceId, boolean isPortrait) {
        JNILibrary.bindTexture(resourceId.intValue());
        if (isPortrait) {
            drawTexiOES(this.portraitScreenCoordsOES, 0, 0, this.screenWidth, this.screenHeight);
        } else {
            drawTexiOES(this.landscapeScreenCoordsOES, 0, 0, this.screenWidth, this.screenHeight);
        }
    }

    public void drawScreenTexiOES(boolean isPortrait) {
        if (isPortrait) {
            drawTexiOES(this.portraitScreenCoordsOES, 0, 0, this.screenWidth, this.screenHeight);
        } else {
            drawTexiOES(this.landscapeScreenCoordsOES, 0, 0, this.screenWidth, this.screenHeight);
        }
    }

    public void drawScreenTexiOES(Integer resourceId, int[] texCoordsOES) {
        JNILibrary.bindTexture(resourceId.intValue());
        drawTexiOES(texCoordsOES, 0, 0, this.screenWidth, this.screenHeight);
    }

    public void drawTexiOES(int[] texCoords, int left, int top, int width, int height) {
        this.gl11.glTexParameteriv(3553, 35741, texCoords, 0);
        this.gl11Ext.glDrawTexiOES(left, this.screenHeight - (top + height), 0, width, height);
    }

    public void drawScreenTexture(Integer resourceId, boolean isPortrait) {
        this.gl.glPushMatrix();
        setupWorldMatrix((float) (this.screenWidth / 2), (float) (this.screenHeight / 2), (float) this.screenWidth, (float) this.screenHeight, 0.0f);
        JNILibrary.bindTexture(resourceId.intValue());
        if (isPortrait) {
            drawTexture(getDefaultVertexBuffer(), getPortraitTextureBuffer());
        } else {
            drawTexture(getDefaultVertexBuffer(), getLandscapeTextureBuffer());
        }
        this.gl.glPopMatrix();
    }

    public void drawTexture(FloatBuffer vertexBuffer, FloatBuffer textureBuffer) {
        this.gl.glTexCoordPointer(2, 5126, 0, textureBuffer);
        this.gl.glVertexPointer(2, 5126, 0, vertexBuffer);
        this.gl.glDrawArrays(5, 0, 4);
    }

    public void drawTextureStraight() {
        this.gl.glDrawArrays(5, 0, 4);
    }

    public void prepareDefaultRender() {
        this.gl.glBlendFunc(1, 771);
        this.gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        this.gl.glTexCoordPointer(2, 5126, 0, getDefaultTextureBuffer());
        this.gl.glVertexPointer(2, 5126, 0, getDefaultVertexBuffer());
    }

    public void clearColor(GL10 gl2) {
        gl2.glClearColor(this.backgroundColor[0], this.backgroundColor[1], this.backgroundColor[2], this.backgroundColor[3]);
    }

    public FloatBuffer getDefaultVertexBuffer() {
        return this.defaultVertexBuffer;
    }

    public FloatBuffer getDefaultTextureBuffer() {
        return this.defaultTextureBuffer;
    }

    public FloatBuffer getPortraitTextureBuffer() {
        return this.portraitTextureBuffer;
    }

    public FloatBuffer getLandscapeTextureBuffer() {
        return this.landscapeTextureBuffer;
    }

    public HardwareVertexBuffer getDefaultHardwareVertexBuffer() {
        return this.defaultHardwareVertexBuffer;
    }

    public HardwareVertexBuffer getDefaultHardwareTextureBuffer() {
        return this.defaultHardwareTextureBuffer;
    }

    public HardwareIndexBuffer getDefaultHardwareIndexRectBuffer() {
        return this.defaultHardwareIndexRectBuffer;
    }

    public void defualtHardwareIndexRectDraw(int rectCount, int firstRectIndex) {
        this.defaultHardwareIndexRectBuffer.drawElements(this.gl11, rectCount * 6, firstRectIndex * 6);
    }

    public float getXScale() {
        return this.xScale;
    }

    public float getYScale() {
        return this.yScale;
    }

    public void setBackgroundColor(float[] backgroundColor2) {
        this.backgroundColor = backgroundColor2;
    }

    public float[] getBackgroundColor() {
        return this.backgroundColor;
    }

    public void blureScreen(float opacity, boolean isPortrait) {
        this.gl.glDisable(3553);
        this.gl.glColor4f(0.0f, 0.0f, 0.0f, opacity);
        TextureManager.getInstance().bindTexture(0);
        if (isPortrait) {
            drawTexiOES(this.portraitScreenCoordsOES, 0, 0, this.screenWidth, this.screenHeight);
        } else {
            drawTexiOES(this.landscapeScreenCoordsOES, 0, 0, this.screenWidth, this.screenHeight);
        }
        this.gl.glEnable(3553);
    }

    public void setCamera(Camera camera2) {
        this.camera = camera2;
        if (this.camera != null && this.gl != null) {
            this.camera.initiate(this.screenWidth, this.screenHeight, this.gl);
        }
    }

    public Camera getCamera() {
        if (this.camera == null) {
            this.camera = new Camera2D();
        }
        return this.camera;
    }
}
