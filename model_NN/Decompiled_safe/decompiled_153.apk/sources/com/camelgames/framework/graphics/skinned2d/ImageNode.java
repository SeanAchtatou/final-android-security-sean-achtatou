package com.camelgames.framework.graphics.skinned2d;

import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.math.Vector3;
import com.camelgames.ndk.graphics.Sprite2D;

public class ImageNode {
    public float angleAbsolute;
    private float bindingAngle;
    public Joint bindingJoint;
    private float bindingLength;
    public boolean isActive = true;
    public String name;
    public Vector2 positionAbsolute = new Vector2();
    private int resId;
    private float rotateCos;
    private float rotateSin;
    private float selfAngle;
    private Sprite2D sprite;

    public void setLengthAngle(float bindingLength2, float bindingAngle2, float selfAngle2) {
        this.bindingLength = bindingLength2;
        this.bindingAngle = bindingAngle2;
        this.selfAngle = selfAngle2;
        this.rotateCos = (float) Math.cos((double) bindingAngle2);
        this.rotateSin = (float) Math.sin((double) bindingAngle2);
    }

    public void initiateSprite(int resId2) {
        this.resId = resId2;
        this.sprite = new Sprite2D();
        this.sprite.setTexId(resId2);
    }

    public int getResId() {
        return this.resId;
    }

    public Sprite2D getSprite() {
        return this.sprite;
    }

    public void getSpritePosition(Vector3 position) {
        if (this.isActive) {
            position.X = this.sprite.getX();
            position.Y = this.sprite.getY();
            position.Z = this.sprite.getAngle();
        }
    }

    public void setPosition(ImageNode node) {
        this.positionAbsolute.X = node.positionAbsolute.X;
        this.positionAbsolute.Y = node.positionAbsolute.Y;
        this.angleAbsolute = node.angleAbsolute;
    }

    public void scale(float scale) {
        this.bindingLength *= scale;
        this.sprite.setSizeByPixelScale(scale);
    }

    public void render(float elapsedTime) {
        if (this.isActive) {
            this.sprite.render(elapsedTime);
        }
    }

    public void update() {
        if (this.isActive) {
            float absoluteCos = (this.bindingJoint.absoluteCos * this.rotateCos) + (this.bindingJoint.absoluteSin * (-this.rotateSin));
            float absoluteSin = (this.bindingJoint.absoluteCos * this.rotateSin) + (this.bindingJoint.absoluteSin * this.rotateCos);
            this.positionAbsolute.X = this.bindingJoint.endAbsolute.X + (this.bindingLength * absoluteCos);
            this.positionAbsolute.Y = this.bindingJoint.endAbsolute.Y + (this.bindingLength * absoluteSin);
            this.angleAbsolute = this.bindingJoint.angleAbsolute + this.bindingAngle + this.selfAngle;
            this.sprite.setPosition(this.positionAbsolute.X, this.positionAbsolute.Y, this.angleAbsolute);
        }
    }

    public void setPosition(float x, float y, float angle) {
        if (this.isActive) {
            this.positionAbsolute.X = x;
            this.positionAbsolute.Y = y;
            this.angleAbsolute = angle;
            this.sprite.setPosition(x, y, angle);
        }
    }

    public void updateJoint(float x, float y, float angle) {
        if (this.isActive) {
            this.bindingJoint.revertSetEndAbsolute(x - (this.bindingLength * ((float) Math.cos((double) (angle - this.selfAngle)))), y - (this.bindingLength * ((float) Math.sin((double) (angle - this.selfAngle)))), (angle - this.selfAngle) - this.bindingAngle);
        }
    }
}
