package com.camelgames.framework.graphics.transforms;

import android.opengl.Matrix;
import javax.microedition.khronos.opengles.GL10;

public class ScaleTransform implements Transform {
    private float scaleX;
    private float scaleY;

    public ScaleTransform() {
    }

    public ScaleTransform(float scaleX2, float scaleY2) {
        setScale(scaleX2, scaleY2);
    }

    public void setScale(float scaleX2, float scaleY2) {
        this.scaleX = scaleX2;
        this.scaleY = scaleY2;
    }

    public void transform(GL10 gl) {
        gl.glScalef(this.scaleX, this.scaleY, 1.0f);
    }

    public void transform(float[] matrix) {
        Matrix.scaleM(matrix, 0, this.scaleX, this.scaleY, 1.0f);
    }
}
