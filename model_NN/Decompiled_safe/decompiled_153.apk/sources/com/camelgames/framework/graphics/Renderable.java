package com.camelgames.framework.graphics;

import javax.microedition.khronos.opengles.GL10;

public interface Renderable {

    public enum PRIORITY {
        LOWEST,
        LOW,
        MIDDLE,
        HIGH,
        HIGHEST
    }

    PRIORITY getPriority();

    void render(GL10 gl10, float f);
}
