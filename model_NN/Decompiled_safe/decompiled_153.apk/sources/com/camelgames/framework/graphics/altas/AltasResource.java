package com.camelgames.framework.graphics.altas;

import com.camelgames.framework.graphics.textures.TextureUtility;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.ui.UIUtility;

public class AltasResource {
    private Integer altasResourceId;
    private final int[] rectangle;
    private Integer subResourceId;
    private float[] texCoords;

    public AltasResource(int subResourceId2) {
        this.subResourceId = Integer.valueOf(subResourceId2);
        this.rectangle = UIUtility.getResources().getIntArray(subResourceId2);
        reloadAltasResourceId();
    }

    public void reloadAltasResourceId() {
        if (this.altasResourceId == null || this.altasResourceId.intValue() <= 0) {
            this.altasResourceId = AltasTextureManager.getInstance().getAltasIdFromSubId(this.subResourceId);
            if (this.altasResourceId != null && this.altasResourceId.intValue() > 0) {
                Vector2 size = getAltasSize();
                float left = (((float) this.rectangle[0]) + 0.5f) / size.X;
                float top = (((float) this.rectangle[1]) + 0.5f) / size.Y;
                float right = (((float) (this.rectangle[0] + this.rectangle[2])) - 0.5f) / size.X;
                float bottom = (((float) (this.rectangle[1] + this.rectangle[3])) - 0.5f) / size.Y;
                this.texCoords = new float[]{left, top, left, bottom, right, top, right, bottom};
            }
        }
    }

    public Vector2 getAltasSize() {
        return TextureUtility.getInstance().getTextureSize(this.altasResourceId);
    }

    public Integer getSubResourceId() {
        return this.subResourceId;
    }

    public Integer getAltasResourceId() {
        return this.altasResourceId;
    }

    public float[] getTexCoords() {
        return this.texCoords;
    }

    public float getTexCoordsLeft() {
        return this.texCoords[0];
    }

    public float getTexCoordsRight() {
        return this.texCoords[4];
    }

    public float getTexCoordsTop() {
        return this.texCoords[1];
    }

    public float getTexCoordsBottom() {
        return this.texCoords[3];
    }

    public int getLeft() {
        return this.rectangle[0];
    }

    public int getTop() {
        return this.rectangle[1];
    }

    public int getWidth() {
        return this.rectangle[2];
    }

    public int getHeight() {
        return this.rectangle[3];
    }

    public float getHeightWidthRate() {
        return ((float) getHeight()) / ((float) getWidth());
    }
}
