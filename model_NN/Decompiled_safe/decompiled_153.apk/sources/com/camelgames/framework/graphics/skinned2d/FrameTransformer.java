package com.camelgames.framework.graphics.skinned2d;

public class FrameTransformer {
    private float accumulatedTime;
    public JointFrame end;
    private float fadeTime;
    private boolean isPlaying;
    private SkinnedModel2D model;
    public JointFrame start;

    public FrameTransformer(SkinnedModel2D model2) {
        this.model = model2;
    }

    public void reset() {
        this.end = null;
        this.start = null;
        this.isPlaying = false;
    }

    public void play(JointFrame frame) {
        this.end = frame;
        this.start = frame;
        frame.play(this.model.joints);
        this.isPlaying = false;
    }

    public void play(JointFrame start2, JointFrame end2, float fadeTime2) {
        this.start = start2;
        this.end = end2;
        this.accumulatedTime = 0.0f;
        this.fadeTime = fadeTime2;
        this.isPlaying = true;
    }

    public void update(float elapsedTime) {
        if (this.isPlaying) {
            this.accumulatedTime += elapsedTime;
            if (this.accumulatedTime >= this.fadeTime) {
                this.accumulatedTime = this.fadeTime;
                this.isPlaying = false;
            }
            JointFrame.playFrame(this.start, this.end, this.accumulatedTime / this.fadeTime, this.model.joints);
        } else {
            this.model.updateJoints();
        }
        this.model.updateImageNodes();
    }
}
