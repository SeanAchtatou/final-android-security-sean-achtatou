package com.camelgames.framework.graphics.tiles;

import java.util.HashMap;

public class TileLayerData {
    public int[][] data;
    public String name;
    public HashMap<String, String> properties;
}
