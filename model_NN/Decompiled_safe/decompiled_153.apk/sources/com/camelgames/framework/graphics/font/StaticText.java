package com.camelgames.framework.graphics.font;

import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.math.ArrayVectorUtils;
import com.camelgames.ndk.graphics.FloatArray;
import com.camelgames.ndk.graphics.SpriteRects;

public class StaticText {
    public static SpriteRects create(int resId, int offset, String text, float fontSize, float fontMargin) {
        return create(resId, offset, text, fontSize, fontMargin, 0.0f, 0.0f);
    }

    public static SpriteRects create(int resId, int offset, String text, float fontSize, float fontMargin, float xDelta, float yDelta) {
        float[] vertexes = new float[(text.length() * 8)];
        float[] texCoords = new float[(text.length() * 8)];
        float[] vertex = new float[8];
        float width = ((float) (text.length() - 1)) * fontSize * fontMargin;
        for (int i = 0; i < text.length(); i++) {
            setTexCoords(texCoords, text.charAt((text.length() - i) - 1), i, offset);
            System.arraycopy(GraphicsManager.defaultVertices, 0, vertex, 0, 8);
            ArrayVectorUtils.scale(vertex, 4, fontSize, fontSize);
            ArrayVectorUtils.transformTo(vertex, 4, ((((float) i) * fontSize) * fontMargin) - (0.5f * width), 0.0f);
            System.arraycopy(vertex, 0, vertexes, i * 8, 8);
        }
        if (!(xDelta == 0.0f && yDelta == 0.0f)) {
            ArrayVectorUtils.transformTo(vertexes, vertexes.length / 2, xDelta, yDelta);
        }
        FloatArray verticesArray = new FloatArray(vertexes.length);
        for (int i2 = 0; i2 < vertexes.length; i2++) {
            verticesArray.set(vertexes[i2], i2);
        }
        FloatArray texCoordsArray = new FloatArray(texCoords.length);
        for (int i3 = 0; i3 < texCoords.length; i3++) {
            texCoordsArray.set(texCoords[i3], i3);
        }
        SpriteRects spriteRects = new SpriteRects(verticesArray, texCoordsArray, text.length());
        spriteRects.setTexId(resId);
        return spriteRects;
    }

    private static void setTexCoords(float[] texCoords, char c, int i, int offset) {
        int fontPosition = c + offset;
        float left = ((float) (fontPosition % 16)) / 16.0f;
        float top = ((float) (fontPosition / 16)) / 16.0f;
        float right = left + 0.0625f;
        float bottom = top + 0.0625f;
        texCoords[(i * 8) + 0] = left;
        texCoords[(i * 8) + 1] = top;
        texCoords[(i * 8) + 2] = left;
        texCoords[(i * 8) + 3] = bottom;
        texCoords[(i * 8) + 4] = right;
        texCoords[(i * 8) + 5] = top;
        texCoords[(i * 8) + 6] = right;
        texCoords[(i * 8) + 7] = bottom;
    }
}
