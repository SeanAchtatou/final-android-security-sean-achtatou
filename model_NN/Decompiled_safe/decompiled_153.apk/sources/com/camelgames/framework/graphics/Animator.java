package com.camelgames.framework.graphics;

public class Animator {
    private float accumulatedTime;
    private float changeTime = 0.1f;
    private int currentFrame;
    private boolean isBackward;
    private boolean isFold;
    private boolean isLoop;
    private boolean isStoped;
    private int maxFrame;
    private int minFrame;

    public boolean update(float elapsedTime) {
        if (this.isStoped) {
            return false;
        }
        this.accumulatedTime += elapsedTime;
        if (this.accumulatedTime <= this.changeTime) {
            return false;
        }
        while (this.accumulatedTime > this.changeTime) {
            this.accumulatedTime -= this.changeTime;
        }
        nextFrame();
        return true;
    }

    public void nextFrame() {
        this.accumulatedTime = 0.0f;
        if (!this.isBackward) {
            this.currentFrame++;
            if (this.currentFrame < this.maxFrame) {
                return;
            }
            if (!this.isLoop) {
                this.currentFrame = this.maxFrame - 1;
                this.isStoped = true;
            } else if (this.isFold) {
                this.currentFrame = this.maxFrame - 1;
                this.isBackward = true;
            } else {
                this.currentFrame = this.minFrame;
            }
        } else {
            this.currentFrame--;
            if (this.currentFrame >= this.minFrame) {
                return;
            }
            if (!this.isLoop) {
                this.currentFrame = this.minFrame;
                this.isStoped = true;
            } else if (this.isFold) {
                this.currentFrame = this.minFrame;
                this.isBackward = false;
            } else {
                this.currentFrame = this.maxFrame - 1;
            }
        }
    }

    public void setLoop(boolean isLoop2) {
        this.isLoop = isLoop2;
    }

    public boolean isLoop() {
        return this.isLoop;
    }

    public void setStoped(boolean isStoped2) {
        this.accumulatedTime = 0.0f;
        this.isStoped = isStoped2;
    }

    public boolean isStoped() {
        return this.isStoped;
    }

    public void setMaxFrame(int maxFrame2) {
        this.maxFrame = maxFrame2;
    }

    public int getMaxFrame() {
        return this.maxFrame;
    }

    public void setMinFrame(int minFrame2) {
        this.minFrame = minFrame2;
    }

    public int getMinFrame() {
        return this.minFrame;
    }

    public void setChangeTime(float changeTime2) {
        this.changeTime = changeTime2;
    }

    public float getChangeTime() {
        return this.changeTime;
    }

    public void setCurrentFrame(int currentFrame2) {
        this.currentFrame = currentFrame2;
    }

    public int getCurrentFrame() {
        return this.currentFrame;
    }

    public void setFold(boolean isFold2) {
        this.isFold = isFold2;
    }

    public boolean isFold() {
        return this.isFold;
    }

    public void setBackward(boolean isBackward2) {
        this.isBackward = isBackward2;
    }

    public boolean isBackward() {
        return this.isBackward;
    }
}
