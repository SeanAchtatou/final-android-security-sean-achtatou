package com.camelgames.framework.Skeleton;

import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.Renderable;

public class RenderableUtil {
    protected Renderable.PRIORITY _priority = Renderable.PRIORITY.MIDDLE;

    public Renderable.PRIORITY getPriority() {
        return this._priority;
    }

    public void setPriority(Renderable.PRIORITY priority) {
        this._priority = priority;
    }

    public boolean isVisible(Renderable renderable) {
        return GraphicsManager.getInstance().contains(renderable);
    }

    public void setVisible(Renderable renderable, boolean bVisible) {
        if (bVisible) {
            GraphicsManager.getInstance().AddToRenderQueue(renderable);
        } else {
            GraphicsManager.getInstance().RemoveFromQueue(renderable);
        }
    }
}
