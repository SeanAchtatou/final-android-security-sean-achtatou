package com.camelgames.framework.Skeleton;

import com.camelgames.framework.graphics.Renderable;

public abstract class AbstractRenderable implements Renderable {
    private RenderableUtil rendrableUtil = new RenderableUtil();

    public Renderable.PRIORITY getPriority() {
        return this.rendrableUtil.getPriority();
    }

    public void setPriority(Renderable.PRIORITY priority) {
        this.rendrableUtil.setPriority(priority);
    }

    public void setVisible(boolean isVisible) {
        this.rendrableUtil.setVisible(this, isVisible);
    }

    public boolean isVisible() {
        return this.rendrableUtil.isVisible(this);
    }
}
