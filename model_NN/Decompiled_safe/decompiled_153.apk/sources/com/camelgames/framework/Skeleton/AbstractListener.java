package com.camelgames.framework.Skeleton;

import com.camelgames.framework.events.EventListener;
import com.camelgames.framework.events.EventType;

public abstract class AbstractListener implements EventListener {
    private EventListenerUtil eventListenerUtil = new EventListenerUtil();

    public void addListener() {
        this.eventListenerUtil.addListener(this);
    }

    public void removeListener() {
        this.eventListenerUtil.removeListener(this);
    }

    public void addEventType(EventType type) {
        this.eventListenerUtil.addEventType(type);
    }
}
