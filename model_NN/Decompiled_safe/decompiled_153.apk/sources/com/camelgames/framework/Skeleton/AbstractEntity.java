package com.camelgames.framework.Skeleton;

import com.camelgames.framework.entities.Entity;
import com.camelgames.framework.entities.EntityManager;
import com.camelgames.framework.resources.AbstractDisposable;

public abstract class AbstractEntity extends AbstractDisposable implements Entity {
    private int id = -1;
    private boolean isPermanent;

    public AbstractEntity() {
        register();
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public boolean isPermanent() {
        return this.isPermanent;
    }

    public void setPermanent(boolean isPermanent2) {
        this.isPermanent = isPermanent2;
    }

    /* access modifiers changed from: protected */
    public void disposeInternal() {
        delete();
    }

    public void delete() {
        EntityManager.getInstance().delete(this);
    }

    public void unregister() {
        EntityManager.getInstance().unregister(this);
    }

    public void register() {
        EntityManager.getInstance().register(this);
    }
}
