package com.camelgames.framework.Skeleton;

import com.camelgames.framework.events.EventListener;
import com.camelgames.framework.events.EventType;

public abstract class EntityListener extends AbstractEntity implements EventListener {
    private EventListenerUtil eventListenerUtil = new EventListenerUtil();

    /* access modifiers changed from: protected */
    public void disposeInternal() {
        removeListener();
        super.disposeInternal();
    }

    /* access modifiers changed from: protected */
    public void addListener() {
        this.eventListenerUtil.addListener(this);
    }

    /* access modifiers changed from: protected */
    public void removeListener() {
        this.eventListenerUtil.removeListener(this);
    }

    /* access modifiers changed from: protected */
    public void addEventType(EventType type) {
        this.eventListenerUtil.addEventType(type);
    }
}
