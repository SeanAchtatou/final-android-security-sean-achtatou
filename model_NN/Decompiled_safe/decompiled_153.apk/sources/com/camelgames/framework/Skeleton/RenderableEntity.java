package com.camelgames.framework.Skeleton;

import com.camelgames.framework.graphics.Renderable;
import com.camelgames.framework.math.MathUtils;
import com.camelgames.framework.math.Vector2;

public abstract class RenderableEntity extends AbstractEntity implements Renderable {
    protected float angle;
    protected float height;
    protected Vector2 position = new Vector2();
    private RenderableUtil rendrableUtil = new RenderableUtil();
    protected float width;

    /* access modifiers changed from: protected */
    public void disposeInternal() {
        setVisible(false);
        super.disposeInternal();
    }

    public Renderable.PRIORITY getPriority() {
        return this.rendrableUtil.getPriority();
    }

    public void setPriority(Renderable.PRIORITY priority) {
        this.rendrableUtil.setPriority(priority);
    }

    public float getX() {
        return this.position.X;
    }

    public void setX(float x) {
        setPosition(x, this.position.Y);
    }

    public float getY() {
        return this.position.Y;
    }

    public void setY(float y) {
        setPosition(this.position.X, y);
    }

    public float getAngle() {
        return this.angle;
    }

    public float getLeft() {
        return this.position.X - (0.5f * this.width);
    }

    public float getRight() {
        return this.position.X + (0.5f * this.width);
    }

    public float getTop() {
        return this.position.Y - (0.5f * this.height);
    }

    public float getBottom() {
        return this.position.Y + (0.5f * this.height);
    }

    public void setAngle(float angle2) {
        this.angle = MathUtils.constrainPi(angle2);
    }

    public void rotate(float angleDelta) {
        setAngle(this.angle + angleDelta);
    }

    public void move(float xDelta, float yDelta) {
        setPosition(getX() + xDelta, getY() + yDelta);
    }

    public void setPosition(float x, float y) {
        this.position.X = x;
        this.position.Y = y;
    }

    public void setPosition(Vector2 position2) {
        setPosition(position2.X, position2.Y);
    }

    public void setPosition(float x, float y, float angle2) {
        setPosition(x, y);
        setAngle(angle2);
    }

    public void setSize(float width2, float height2) {
        this.width = width2;
        this.height = height2;
    }

    public float getWidth() {
        return this.width;
    }

    public float getHeight() {
        return this.height;
    }

    public void setVisible(boolean bVisible) {
        this.rendrableUtil.setVisible(this, bVisible);
    }

    public boolean hitTest(float x, float y) {
        return Math.abs(x - this.position.X) < this.width * 0.5f && Math.abs(y - this.position.Y) < this.height * 0.5f;
    }
}
