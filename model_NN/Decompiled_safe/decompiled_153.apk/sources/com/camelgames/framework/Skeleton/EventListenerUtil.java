package com.camelgames.framework.Skeleton;

import com.camelgames.framework.events.EventListener;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import java.util.LinkedList;
import java.util.List;

public class EventListenerUtil {
    private List<EventType> listEventTypes = new LinkedList();

    public void addListener(EventListener listener) {
        for (EventType type : this.listEventTypes) {
            EventManager.getInstance().addListener(type, listener);
        }
    }

    public void removeListener(EventListener listener) {
        for (EventType type : this.listEventTypes) {
            EventManager.getInstance().removeListener(type, listener);
        }
    }

    public void addEventType(EventType type) {
        this.listEventTypes.add(type);
    }
}
