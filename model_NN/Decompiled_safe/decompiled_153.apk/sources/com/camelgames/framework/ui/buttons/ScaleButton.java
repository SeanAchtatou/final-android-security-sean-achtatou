package com.camelgames.framework.ui.buttons;

import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;
import com.camelgames.framework.ui.actions.ActionManager;
import com.camelgames.framework.ui.actions.ScaleAction;
import com.camelgames.framework.ui.buttons.TriggerableUI;

public class ScaleButton extends Button {
    public static final Callback onScaleFinished = new Callback() {
        public void excute(UI ui) {
            ((ScaleButton) ui).onScaleFinished();
        }
    };
    private ScaleAction scaleAction = new ScaleAction();

    public ScaleButton() {
        this.scaleAction.setScale(1.0f, 1.2f);
        this.scaleAction.setWholeActionTime(0.12f);
        this.scaleAction.bind(this, onScaleFinished);
        this.status = TriggerableUI.Status.Ready;
    }

    public void fadeIn() {
        setReady();
    }

    public void fadeOut() {
        if (this.isTriggered) {
            this.status = TriggerableUI.Status.FadeOut;
            ActionManager.instance.add(this.scaleAction);
            return;
        }
        this.status = TriggerableUI.Status.Unknown;
    }

    public void setTriggerScale(float scale) {
        this.scaleAction.setScale(1.0f, scale);
    }

    public void setTriggerTime(float time) {
        this.scaleAction.setWholeActionTime(time);
    }

    /* access modifiers changed from: protected */
    public void onScaleFinished() {
        if (this.isTriggered) {
            excuteFunctionCallback();
        }
        setReady();
    }
}
