package com.camelgames.framework.ui.actions;

import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;
import com.camelgames.framework.ui.actions.AbstractAction;

public class RepeatAction extends AbstractAction {
    private Action action;
    private int repeatedCount;
    private int totalRepeatCount;

    public void initiate(Action action2, int repeatCount) {
        setAction(action2);
        setRepeatCount(repeatCount);
    }

    public void setAction(Action action2) {
        this.action = action2;
        action2.bind(this.bindingUI, null);
    }

    public void setRepeatCount(int repeatCount) {
        this.totalRepeatCount = repeatCount;
    }

    public void bindUI(UI bindingUI) {
        this.bindingUI = bindingUI;
        this.action.bindUI(bindingUI);
    }

    public void bindCallback(Callback onFinished) {
        this.onFinished = onFinished;
        this.action.bindCallback(null);
    }

    public void start() {
        this.repeatedCount = 0;
        this.action.start();
        super.start();
    }

    public void stop() {
        this.repeatedCount = 0;
        this.action.stop();
        super.stop();
    }

    public void update(float elapsedTime) {
        if (this.status.equals(AbstractAction.Status.Running)) {
            this.usedTime += elapsedTime;
            this.action.update(elapsedTime);
            if (this.action.isStopped()) {
                this.repeatedCount++;
                if (this.totalRepeatCount == -1) {
                    this.action.start();
                } else if (this.repeatedCount >= this.totalRepeatCount) {
                    if (this.onFinished != null) {
                        this.onFinished.excute(this.bindingUI);
                    }
                    stop();
                } else {
                    this.action.start();
                }
            }
        }
    }
}
