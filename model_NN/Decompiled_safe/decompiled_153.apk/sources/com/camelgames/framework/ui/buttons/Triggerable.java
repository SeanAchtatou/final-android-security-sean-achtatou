package com.camelgames.framework.ui.buttons;

import com.camelgames.framework.ui.Callback;

public interface Triggerable {
    void excute();

    void fadeIn();

    void fadeOut();

    boolean hitTest(float f, float f2);

    boolean isActive();

    void setActive(boolean z);

    void setFunctionCallback(Callback callback);

    void setTriggeredCallback(Callback callback);
}
