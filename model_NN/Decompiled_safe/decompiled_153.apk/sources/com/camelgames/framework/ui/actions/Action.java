package com.camelgames.framework.ui.actions;

import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;

public interface Action {
    void bind(UI ui, Callback callback);

    void bindCallback(Callback callback);

    void bindUI(UI ui);

    UI getBinding();

    boolean isPaused();

    boolean isRunning();

    boolean isStopped();

    void pause();

    void resume();

    void setUsedTime(float f);

    void start();

    void stop();

    void update(float f);
}
