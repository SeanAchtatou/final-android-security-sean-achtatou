package com.camelgames.framework.ui.buttons;

import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;
import com.camelgames.framework.ui.actions.ActionManager;
import com.camelgames.framework.ui.actions.MoveAction;
import com.camelgames.framework.ui.actions.ScaleAction;
import com.camelgames.framework.ui.buttons.TriggerableUI;

public class MovingButton extends Button {
    public static final Callback defaultOnScaleFinished = new Callback() {
        public void excute(UI ui) {
            ((MovingButton) ui).onScaleFinished();
        }
    };
    public static final Callback defaultOnSlipInFinished = new Callback() {
        public void excute(UI ui) {
            ((MovingButton) ui).onSlipInFinished();
        }
    };
    public static final Callback defualtOnSlipOutFinished = new Callback() {
        public void excute(UI ui) {
            ((MovingButton) ui).onSlipOutFinished();
        }
    };
    private Callback onScaleFinished = defaultOnScaleFinished;
    private Callback onSlipInFinished = defaultOnSlipInFinished;
    private Callback onSlipOutFinished = defualtOnSlipOutFinished;
    private ScaleAction scaleAction = new ScaleAction();
    private MoveAction slipInAction = new MoveAction();
    private MoveAction slipOutAction = new MoveAction();

    public MovingButton() {
        this.scaleAction.setScale(1.0f, 1.1f);
        this.scaleAction.setWholeActionTime(0.12f);
        this.scaleAction.bind(this, this.onScaleFinished);
    }

    public void setMovingTime(float time) {
        this.slipInAction.setWholeActionTime(time);
        this.slipOutAction.setWholeActionTime(time);
    }

    public void setMovingInfo(float startX, float startY, float stayX, float stayY) {
        setMovingInfo(startX, startY, stayX, stayY, startX, startY);
    }

    public void setMovingInfo(float startX, float startY, float stayX, float stayY, float endX, float endY) {
        this.slipInAction.setStart(startX, startY);
        this.slipInAction.setEnd(stayX, stayY);
        this.slipInAction.bind(this, this.onSlipInFinished);
        this.slipOutAction.setStart(stayX, stayY);
        this.slipOutAction.setEnd(endX, endY);
        this.slipOutAction.bind(this, this.onSlipOutFinished);
    }

    public void setTriggerScale(float scale) {
        this.scaleAction.setScale(1.0f, scale);
    }

    public void setTriggerTime(float time) {
        this.scaleAction.setWholeActionTime(time);
    }

    public void fadeIn() {
        setReady();
        this.status = TriggerableUI.Status.FadeIn;
        ActionManager.instance.add(this.slipInAction);
    }

    public void fadeOut() {
        if (this.isTriggered) {
            ActionManager.instance.add(this.scaleAction);
        } else {
            ActionManager.instance.add(this.slipOutAction);
        }
        this.status = TriggerableUI.Status.FadeOut;
    }

    public void setUnknown() {
        super.setUnknown();
        this.slipInAction.moveToStart();
    }

    public void setOnScaleFinished(Callback onScaleFinished2) {
        this.onScaleFinished = onScaleFinished2;
        this.scaleAction.bindCallback(onScaleFinished2);
    }

    public void setOnSlipInFinished(Callback onSlipInFinished2) {
        this.onSlipInFinished = onSlipInFinished2;
        this.slipInAction.bindCallback(onSlipInFinished2);
    }

    public void setOnSlipOutFinished(Callback onSlipOutFinished2) {
        this.onSlipOutFinished = onSlipOutFinished2;
        this.slipOutAction.bindCallback(onSlipOutFinished2);
    }

    /* access modifiers changed from: protected */
    public void onSlipInFinished() {
        this.status = TriggerableUI.Status.Ready;
    }

    /* access modifiers changed from: protected */
    public void onSlipOutFinished() {
        this.status = TriggerableUI.Status.Unknown;
        if (this.isTriggered) {
            excuteFunctionCallback();
        }
    }

    /* access modifiers changed from: protected */
    public void onScaleFinished() {
        ActionManager.instance.add(this.slipOutAction);
    }
}
