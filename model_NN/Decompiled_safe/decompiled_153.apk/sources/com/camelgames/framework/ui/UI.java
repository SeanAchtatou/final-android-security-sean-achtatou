package com.camelgames.framework.ui;

public interface UI extends RenderCompositable {
    float getActualLeft();

    float getActualTop();

    float getActualX();

    float getActualY();

    float getHeight();

    float getScale();

    float getWidth();

    void move(float f, float f2);

    void moveX(float f);

    void moveY(float f);

    void setLeft(float f);

    void setScale(float f);

    void setSize(float f, float f2);

    void setTop(float f);

    void setX(float f);

    void setY(float f);
}
