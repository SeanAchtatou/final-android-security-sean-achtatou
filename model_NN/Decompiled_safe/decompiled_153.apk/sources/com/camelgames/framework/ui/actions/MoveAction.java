package com.camelgames.framework.ui.actions;

public class MoveAction extends AtomAction {
    private float endX;
    private float endY;
    private float startX;
    private float startY;
    private float velocityX;
    private float velocityY;

    public void setStart(float startX2, float startY2) {
        this.startX = startX2;
        this.startY = startY2;
    }

    public void setEnd(float endX2, float endY2) {
        this.endX = endX2;
        this.endY = endY2;
    }

    public void start() {
        this.velocityX = (this.endX - this.startX) / this.wholeActionTime;
        this.velocityY = (this.endY - this.startY) / this.wholeActionTime;
        super.start();
    }

    public void moveToStart() {
        this.bindingUI.setPosition(this.startX, this.startY);
    }

    public void moveToEnd() {
        this.bindingUI.setPosition(this.endX, this.endY);
    }

    /* access modifiers changed from: protected */
    public void action() {
        this.bindingUI.setPosition(this.startX + (this.velocityX * this.usedTime), this.startY + (this.velocityY * this.usedTime));
    }
}
