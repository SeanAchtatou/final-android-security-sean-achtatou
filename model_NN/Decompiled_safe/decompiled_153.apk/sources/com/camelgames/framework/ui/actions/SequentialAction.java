package com.camelgames.framework.ui.actions;

import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;
import com.camelgames.framework.ui.actions.AbstractAction;
import java.util.ArrayList;

public class SequentialAction extends AbstractAction {
    private ArrayList<Action> actions = new ArrayList<>();
    private int currentGeneratorIndex;

    public void bindUI(UI bindingUI) {
        this.bindingUI = bindingUI;
        for (int i = 0; i < this.actions.size(); i++) {
            this.actions.get(i).bindUI(bindingUI);
        }
    }

    public void bindCallback(Callback onFinished) {
        this.onFinished = onFinished;
        for (int i = 0; i < this.actions.size(); i++) {
            this.actions.get(i).bindCallback(null);
        }
    }

    public void start() {
        this.currentGeneratorIndex = 0;
        for (int i = 0; i < this.actions.size(); i++) {
            this.actions.get(i).start();
        }
        super.start();
    }

    public void stop() {
        this.currentGeneratorIndex = 0;
        for (int i = 0; i < this.actions.size(); i++) {
            this.actions.get(i).stop();
        }
        super.stop();
    }

    public void update(float elapsedTime) {
        if (this.status.equals(AbstractAction.Status.Running)) {
            this.usedTime += elapsedTime;
            Action currentAction = this.actions.get(this.currentGeneratorIndex);
            currentAction.update(elapsedTime);
            if (currentAction.isStopped()) {
                this.currentGeneratorIndex++;
                if (this.currentGeneratorIndex >= this.actions.size()) {
                    if (this.onFinished != null) {
                        this.onFinished.excute(this.bindingUI);
                    }
                    stop();
                }
            }
        }
    }

    public void add(Action action) {
        if (!this.actions.contains(action)) {
            action.bind(this.bindingUI, null);
            this.actions.add(action);
        }
    }

    public void addFirst(Action action) {
        if (!this.actions.contains(action)) {
            action.bind(this.bindingUI, null);
            this.actions.add(0, action);
        }
    }

    public void remove(Action action) {
        this.actions.remove(action);
    }
}
