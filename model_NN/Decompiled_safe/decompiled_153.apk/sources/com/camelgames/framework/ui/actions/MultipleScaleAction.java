package com.camelgames.framework.ui.actions;

import com.camelgames.framework.ui.actions.MutipleAction;

public class MultipleScaleAction extends MutipleAction {
    private float scaleVelocity;
    private float[] scales;
    private float startScale;

    public void initiate(float[] scales2, float[] times) {
        initiate(scales2, times, null);
    }

    public void initiate(float[] scales2, float[] times, MutipleAction.StepFinishedCallback onMoveStepFinished) {
        this.scales = scales2;
        super.initiate(times, onMoveStepFinished);
    }

    public void prepareStep(int step) {
        super.prepareStep(step);
        this.startScale = this.scales[step];
        this.scaleVelocity = (this.scales[step + 1] - this.startScale) / this.wholeActionTime;
    }

    /* access modifiers changed from: protected */
    public void action() {
        this.bindingUI.setScale(this.startScale + (this.scaleVelocity * this.usedTime));
        super.action();
    }
}
