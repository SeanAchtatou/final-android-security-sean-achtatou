package com.camelgames.framework.ui;

import com.camelgames.framework.graphics.Renderable;

public interface RenderCompositable extends Renderable {
    void addChild(RenderCompositable renderCompositable);

    void clearChildren();

    void detachFromParent();

    RenderCompositable getParent();

    void removeChild(RenderCompositable renderCompositable);

    void setParent(RenderCompositable renderCompositable);

    void setPosition(float f, float f2);

    void updateActualPosition(float f, float f2);
}
