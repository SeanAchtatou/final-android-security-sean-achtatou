package com.camelgames.framework.ui.actions;

import java.util.ArrayList;

public class ActionManager {
    public static final ActionManager instance = new ActionManager();
    private ArrayList<Action> actions = new ArrayList<>();

    private ActionManager() {
    }

    public void update(float elapsedTime) {
        int i = 0;
        while (i < this.actions.size()) {
            Action action = this.actions.get(i);
            action.update(elapsedTime);
            if (action.isStopped()) {
                this.actions.remove(i);
            } else {
                i++;
            }
        }
    }

    public void add(Action action) {
        if (!this.actions.contains(action)) {
            action.start();
            this.actions.add(action);
        }
    }

    public void remove(Action action) {
        this.actions.remove(action);
    }

    public void clear() {
        this.actions.clear();
    }
}
