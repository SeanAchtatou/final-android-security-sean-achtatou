package com.camelgames.framework.ui.actions;

import com.camelgames.framework.ui.actions.AbstractAction;

public abstract class AtomAction extends AbstractAction {
    protected float wholeActionTime = 1.0f;

    /* access modifiers changed from: protected */
    public abstract void action();

    public void start() {
        super.start();
        action();
    }

    public void update(float elapsedTime) {
        if (this.status.equals(AbstractAction.Status.Running)) {
            this.usedTime += elapsedTime;
            if (this.usedTime >= this.wholeActionTime) {
                this.usedTime = this.wholeActionTime;
            }
            action();
            if (this.usedTime >= this.wholeActionTime) {
                if (this.onFinished != null) {
                    this.onFinished.excute(this.bindingUI);
                }
                stop();
            }
        }
    }

    public void setWholeActionTime(float time) {
        this.wholeActionTime = time;
    }

    public float getWholeActionTime() {
        return this.wholeActionTime;
    }
}
