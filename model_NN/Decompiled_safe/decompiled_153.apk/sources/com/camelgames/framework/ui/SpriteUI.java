package com.camelgames.framework.ui;

import com.camelgames.ndk.graphics.OESSprite;
import javax.microedition.khronos.opengles.GL10;

public class SpriteUI extends AbstractUI {
    protected OESSprite texture;

    public void initiateByWidth(int resourceId, float width) {
        this.texture = new OESSprite();
        this.texture.setTexId(resourceId);
        this.texture.setWidthConstrainProportion(width);
        this.texture.setPosition((int) getActualX(), (int) getActualY());
        setSize(width, (float) this.texture.getHeight());
    }

    public void initiateByHeight(int resourceId, float height) {
        this.texture = new OESSprite();
        this.texture.setTexId(resourceId);
        this.texture.setHeightConstrainProportion(height);
        this.texture.setPosition((int) getActualX(), (int) getActualY());
        setSize((float) this.texture.getWidth(), height);
    }

    public void initiateByPixelRate(int resourceId, float pixelRate) {
        this.texture = new OESSprite();
        this.texture.setTexId(resourceId);
        this.texture.setSizeByPixelScale(pixelRate);
        this.texture.setPosition((int) getActualX(), (int) getActualY());
        setSize((float) this.texture.getWidth(), (float) this.texture.getHeight());
    }

    public void initiate(int resourceId, float width, float height) {
        this.texture = new OESSprite();
        this.texture.setTexId(resourceId);
        this.texture.setSize((int) width, (int) height);
        this.texture.setPosition((int) getActualX(), (int) getActualY());
        setSize(width, height);
    }

    public void render(GL10 gl, float elapsedTime) {
        if (this.texture != null) {
            this.texture.drawOES();
        }
        renderChildren(gl, elapsedTime);
    }

    public void updateActualPosition(float parentActualX, float parentActualY) {
        super.updateActualPosition(parentActualX, parentActualY);
        if (this.texture != null) {
            this.texture.setPosition((int) this.actualX, (int) this.actualY);
        }
    }

    public void setSize(float width, float height) {
        super.setSize(width, height);
        this.texture.setSize((int) width, (int) height);
    }

    public void setScale(float scale) {
        super.setScale(scale);
        this.texture.setScale(scale);
    }

    public void setTexId(int resourceId) {
        this.texture.setTexId(resourceId);
    }

    public void setTexture(OESSprite texture2) {
        this.texture = texture2;
    }

    public OESSprite getTexture() {
        return this.texture;
    }
}
