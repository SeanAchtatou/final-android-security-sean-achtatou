package com.camelgames.framework.ui;

import com.camelgames.ndk.graphics.NumberText;
import javax.microedition.khronos.opengles.GL10;

public class NumberUI extends AbstractUI {
    protected NumberText texture;

    public void updateActualPosition(float parentActualX, float parentActualY) {
        super.updateActualPosition(parentActualX, parentActualY);
        if (this.texture != null) {
            this.texture.setPosition(this.actualX, this.actualY);
        }
    }

    public void render(GL10 gl, float elapsedTime) {
        if (this.texture != null) {
            this.texture.render();
        }
        renderChildren(gl, elapsedTime);
    }

    public void setNumber(float number) {
        this.texture.setNumber(number);
    }

    public void setScale(float scale) {
        super.setScale(scale);
        this.texture.setScale(scale);
    }

    public void setTexture(NumberText texture2) {
        this.texture = texture2;
    }

    public NumberText getTexture() {
        return this.texture;
    }
}
