package com.camelgames.framework.ui.buttons;

import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.RenderCompositable;
import com.camelgames.framework.ui.RenderCompositableNode;
import com.camelgames.framework.ui.UI;

public class ButtonGroup extends RenderCompositableNode {
    private Callback triggeredCallback = new Callback() {
        public void excute(UI area) {
            ButtonGroup.this.fadeOut();
        }
    };

    public void addChild(RenderCompositable child) {
        if (child instanceof Button) {
            super.addChild(child);
            ((Button) child).setTriggeredCallback(this.triggeredCallback);
        }
    }

    public void removeChild(RenderCompositable child) {
        if (child instanceof Button) {
            super.removeChild(child);
            ((Button) child).setTriggeredCallback(null);
        }
    }

    public void clearChildren() {
        if (this.children != null) {
            for (int i = 0; i < this.children.size(); i++) {
                Button button = (Button) this.children.get(i);
                button.setParent(null);
                button.setTriggeredCallback(null);
            }
            this.children.clear();
        }
    }

    public void fadeIn() {
        if (this.children != null) {
            for (int i = 0; i < this.children.size(); i++) {
                ((Button) this.children.get(i)).fadeIn();
            }
        }
    }

    public void fadeOut() {
        if (this.children != null) {
            for (int i = 0; i < this.children.size(); i++) {
                ((Button) this.children.get(i)).fadeOut();
            }
        }
    }

    public void setUnknown() {
        if (this.children != null) {
            for (int i = 0; i < this.children.size(); i++) {
                ((Button) this.children.get(i)).setUnknown();
            }
        }
    }

    public boolean hitTest(float x, float y) {
        if (this.children != null) {
            for (int i = 0; i < this.children.size(); i++) {
                Button button = (Button) this.children.get(i);
                if (button.hitTest(x, y)) {
                    button.excute();
                    return true;
                }
            }
        }
        return false;
    }

    public void setTriggeredCallback(Callback callback) {
        this.triggeredCallback = callback;
    }
}
