package com.camelgames.framework.ui.buttons;

import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.ui.AbstractUI;
import com.camelgames.framework.ui.Callback;

public abstract class TriggerableUI extends AbstractUI implements Triggerable {
    protected Callback functionCallback;
    protected float hitTestScale = 1.0f;
    protected boolean isActive = true;
    protected boolean isEnabled = true;
    protected boolean isTriggered;
    protected boolean needTriggerSound = true;
    protected Status status = Status.Unknown;
    protected Callback triggeredCallback;

    protected enum Status {
        FadeIn,
        Ready,
        FadeOut,
        Unknown
    }

    public void setTriggeredCallback(Callback callback) {
        this.triggeredCallback = callback;
    }

    public void setFunctionCallback(Callback callback) {
        this.functionCallback = callback;
    }

    public void excuteFunctionCallback() {
        if (this.functionCallback != null) {
            this.functionCallback.excute(this);
        }
    }

    public void excuteTriggerCallback() {
        if (this.triggeredCallback != null) {
            this.triggeredCallback.excute(this);
        }
    }

    public boolean hitTest(float x, float y) {
        if (!this.isActive || !this.isEnabled) {
            return false;
        }
        float offsetX = ((this.actualX - x) * 2.0f) / this.hitTestScale;
        float offsetY = ((this.actualY - y) * 2.0f) / this.hitTestScale;
        if ((-this.width) >= offsetX || offsetX >= this.width || (-this.height) >= offsetY || offsetY >= this.height) {
            return false;
        }
        return true;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public void setActive(boolean isActive2) {
        this.isActive = isActive2;
    }

    public boolean isEnabled() {
        return this.isEnabled;
    }

    public void setEnabled(boolean isEnabled2) {
        this.isEnabled = isEnabled2;
    }

    public void excute() {
        if (this.isActive && !this.isTriggered && isReadyToTrigger()) {
            this.isTriggered = true;
            fadeOut();
            excuteTriggerCallback();
            if (this.needTriggerSound) {
                EventManager.getInstance().postEvent(EventType.Button);
            }
        }
    }

    public void enableSound(boolean enable) {
        this.needTriggerSound = enable;
    }

    public void setHitTestScale(float scale) {
        this.hitTestScale = scale;
    }

    public boolean isReadyToTrigger() {
        return this.functionCallback != null && this.status.equals(Status.Ready);
    }

    public void setReady() {
        this.status = Status.Ready;
        this.isTriggered = false;
    }

    public void setUnknown() {
        this.status = Status.Unknown;
        this.isTriggered = false;
    }
}
