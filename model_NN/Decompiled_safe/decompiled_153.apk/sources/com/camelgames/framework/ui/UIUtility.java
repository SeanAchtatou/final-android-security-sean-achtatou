package com.camelgames.framework.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.view.Display;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;

public class UIUtility {
    public static final int CANNOT_ACCESS_INTERNET_ID = 1;
    public static final int INPUT_NAME_ID = 3;
    public static final int NOT_IN_LITE = 5;
    public static final int NO_SD = 4;
    public static final int OUT_OF_SERVICE_ID = 2;
    private static Display display;
    private static Activity mainActivity;

    public static class NetworkRunningViews {
        public TextView info;
        public View progress;
    }

    public static void setMainActivity(Activity activity) {
        mainActivity = activity;
        display = mainActivity.getWindowManager().getDefaultDisplay();
    }

    public static Activity getMainAcitvity() {
        return mainActivity;
    }

    public static Resources getResources() {
        return mainActivity.getResources();
    }

    public static int getDisplayWidth() {
        return getDisplayWidth(1.0f);
    }

    public static int getDisplayWidth(float rate) {
        return (int) (((float) display.getWidth()) * rate);
    }

    public static int getDisplayHeight() {
        return getDisplayHeight(1.0f);
    }

    public static int getDisplayHeight(float rate) {
        return (int) (((float) display.getHeight()) * rate);
    }

    public static int getDisplayWidthPlusHeight() {
        return getDisplayWidthPlusHeight(1.0f);
    }

    public static int getDisplayWidthPlusHeight(float rate) {
        return (int) (((float) (display.getWidth() + display.getHeight())) * rate);
    }

    public static float getWidthHeightRate() {
        return ((float) display.getWidth()) / ((float) display.getHeight());
    }

    public static void setProgressInfo(int infoStrId, NetworkRunningViews views) {
        if (views.progress != null && views.info != null) {
            views.progress.setVisibility(8);
            views.info.setText(infoStrId);
        }
    }

    public static Dialog showOkDialog(Context context, int title, DialogInterface.OnClickListener clickListener) {
        return new AlertDialog.Builder(context).setIcon(17301507).setTitle(title).setPositiveButton(17039370, clickListener).create();
    }

    public static void setButtonSize(View button, int buttonHeight) {
        button.measure(getDisplayWidth(), getDisplayHeight());
        setSize(button, (int) (((float) button.getMeasuredWidth()) * (((float) buttonHeight) / ((float) button.getMeasuredHeight()))), buttonHeight);
    }

    public static void setButtonSize(View button, int buttonHeight, int measureWidth, int measureHeight) {
        button.measure(measureWidth, measureHeight);
        setSize(button, (int) (((float) button.getMeasuredWidth()) * (((float) buttonHeight) / ((float) button.getMeasuredHeight()))), buttonHeight);
    }

    public static void setLeftTopForRL(View view, float xRate, float yRate) {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
        lp.leftMargin = (int) (((float) getDisplayWidth()) * xRate);
        lp.topMargin = (int) (((float) getDisplayHeight()) * yRate);
    }

    public static void setLeftTopForRLAbsolute(View view, int left, int top) {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
        lp.leftMargin = left;
        lp.topMargin = top;
    }

    public static void setCenterForRL(View view, float xRate, float yRate) {
        view.measure(getDisplayWidth(), getDisplayHeight());
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
        int width = getWidth(view, lp);
        int height = getHeight(view, lp);
        lp.leftMargin = (int) ((((float) getDisplayWidth()) * xRate) - (((float) width) * 0.5f));
        lp.topMargin = (int) ((((float) getDisplayHeight()) * yRate) - (((float) height) * 0.5f));
        view.requestLayout();
    }

    public static void setSize(View view, int width, int height) {
        view.getLayoutParams().width = width;
        view.getLayoutParams().height = height;
    }

    public static int getWidth(View view) {
        view.measure(getDisplayWidth(), getDisplayHeight());
        return getWidth(view, (RelativeLayout.LayoutParams) view.getLayoutParams());
    }

    public static int getWidth(View view, RelativeLayout.LayoutParams lp) {
        int width = lp.width;
        if (lp.width == -1) {
            return getDisplayWidth();
        }
        if (lp.width == -2) {
            return view.getMeasuredWidth();
        }
        return width;
    }

    public static int getHeight(View view, RelativeLayout.LayoutParams lp) {
        int height = lp.height;
        if (lp.height == -1) {
            return getDisplayHeight();
        }
        if (lp.height == -2) {
            return view.getMeasuredHeight();
        }
        return height;
    }

    public static void postButtonEvent(EventType type) {
        EventManager.getInstance().postEvent(EventType.Button);
        EventManager.getInstance().postEvent(type);
    }

    public static void postButtonEvent(AbstractEvent event) {
        EventManager.getInstance().postEvent(EventType.Button);
        EventManager.getInstance().postEvent(event);
    }
}
