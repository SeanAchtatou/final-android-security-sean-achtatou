package com.camelgames.framework.ui.actions;

import com.camelgames.framework.ui.UI;

public abstract class MutipleAction extends AtomAction {
    protected StepFinishedCallback onStepFinished;
    protected int step;
    protected float[] times;

    public interface StepFinishedCallback {
        void onStepFinished(UI ui, int i);
    }

    public void initiate(float[] times2, StepFinishedCallback onMoveStepFinished) {
        this.times = times2;
        this.onStepFinished = onMoveStepFinished;
    }

    public void start() {
        prepareStep(0);
        super.start();
    }

    public int getStep() {
        return this.step;
    }

    public void prepareStep(int step2) {
        this.step = step2;
        this.usedTime = 0.0f;
        this.wholeActionTime = this.times[step2];
    }

    /* access modifiers changed from: protected */
    public void action() {
        if (this.usedTime >= this.wholeActionTime) {
            if (this.onStepFinished != null) {
                this.onStepFinished.onStepFinished(this.bindingUI, this.step);
            }
            this.step++;
            if (this.step < this.times.length) {
                prepareStep(this.step);
            }
        }
    }
}
