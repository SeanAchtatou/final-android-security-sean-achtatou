package com.camelgames.framework.ui.actions;

import com.camelgames.framework.ui.actions.MutipleAction;

public class MultipleMoveAction extends MutipleAction {
    private float startX;
    private float startY;
    private float[] targets;
    private float velocityX;
    private float velocityY;

    public void initiate(float[] targets2, float[] times) {
        initiate(targets2, times, null);
    }

    public void initiate(float[] targets2, float[] times, MutipleAction.StepFinishedCallback onMoveStepFinished) {
        this.targets = targets2;
        super.initiate(times, onMoveStepFinished);
    }

    public void prepareStep(int step) {
        super.prepareStep(step);
        this.startX = this.targets[step * 2];
        this.startY = this.targets[(step * 2) + 1];
        this.velocityX = (this.targets[(step * 2) + 2] - this.startX) / this.wholeActionTime;
        this.velocityY = (this.targets[(step * 2) + 3] - this.startY) / this.wholeActionTime;
    }

    /* access modifiers changed from: protected */
    public void action() {
        this.bindingUI.setPosition(this.startX + (this.velocityX * this.usedTime), this.startY + (this.velocityY * this.usedTime));
        super.action();
    }
}
