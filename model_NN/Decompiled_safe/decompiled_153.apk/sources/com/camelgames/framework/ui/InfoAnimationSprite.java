package com.camelgames.framework.ui;

import com.camelgames.ndk.graphics.Sprite2D;

public class InfoAnimationSprite {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$ui$InfoAnimationSprite$Phase;
    private float accumulatedTime;
    private float fadeIn = 1.0f;
    private float fadeOut = 1.0f;
    private float maxScale = 1.2f;
    private Phase phase = Phase.Finished;
    private Sprite2D sprite;
    private float stay = 2.0f;

    private enum Phase {
        FadeIn,
        Stay,
        FadeOut,
        Finished
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$ui$InfoAnimationSprite$Phase() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$framework$ui$InfoAnimationSprite$Phase;
        if (iArr == null) {
            iArr = new int[Phase.values().length];
            try {
                iArr[Phase.FadeIn.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Phase.FadeOut.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Phase.Finished.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Phase.Stay.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$camelgames$framework$ui$InfoAnimationSprite$Phase = iArr;
        }
        return iArr;
    }

    public void setPhaseTime(float fadeIn2, float stay2, float fadeOut2) {
        this.fadeIn = fadeIn2;
        this.stay = stay2;
        this.fadeOut = fadeOut2;
    }

    public void setSprite(Sprite2D sprite2) {
        this.sprite = sprite2;
    }

    public void setMaxScale(float maxScale2) {
        this.maxScale = maxScale2;
    }

    public void start() {
        this.accumulatedTime = 0.0f;
        this.phase = Phase.FadeIn;
    }

    public void render(float elapsedTime) {
        switch ($SWITCH_TABLE$com$camelgames$framework$ui$InfoAnimationSprite$Phase()[this.phase.ordinal()]) {
            case 1:
                this.accumulatedTime += elapsedTime * 1.6f;
                if (this.accumulatedTime < this.fadeIn) {
                    float alpha = this.accumulatedTime / this.fadeIn;
                    this.sprite.setScale(this.maxScale - ((this.maxScale - 1.0f) * alpha));
                    this.sprite.setColor(alpha);
                    break;
                } else {
                    this.phase = Phase.Stay;
                    this.accumulatedTime -= this.fadeIn;
                    this.sprite.setScale(1.0f);
                    break;
                }
            case 2:
                this.accumulatedTime += elapsedTime;
                if (this.accumulatedTime >= this.stay) {
                    this.phase = Phase.FadeOut;
                    this.accumulatedTime -= this.stay;
                    break;
                }
                break;
            case 3:
                this.accumulatedTime += elapsedTime * 1.6f;
                if (this.accumulatedTime < this.fadeOut) {
                    float alpha2 = 1.0f - (this.accumulatedTime / this.fadeOut);
                    this.sprite.setScale(((this.maxScale - 1.0f) * (1.0f - alpha2)) + 1.0f);
                    this.sprite.setColor(alpha2);
                    break;
                } else {
                    this.phase = Phase.Finished;
                    this.accumulatedTime = 0.0f;
                    break;
                }
        }
        if (!isFinished()) {
            this.sprite.render(elapsedTime);
        }
    }

    public boolean isFinished() {
        return this.phase.equals(Phase.Finished);
    }
}
