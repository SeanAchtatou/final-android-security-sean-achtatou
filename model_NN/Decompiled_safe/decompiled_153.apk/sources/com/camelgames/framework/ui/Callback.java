package com.camelgames.framework.ui;

public interface Callback {
    void excute(UI ui);
}
