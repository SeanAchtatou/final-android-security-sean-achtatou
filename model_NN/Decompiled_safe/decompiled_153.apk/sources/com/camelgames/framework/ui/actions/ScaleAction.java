package com.camelgames.framework.ui.actions;

public class ScaleAction extends AtomAction {
    private float endScale = 2.0f;
    private float scaleVelocity;
    private float startScale = 1.0f;

    public void setScale(float startScale2, float endScale2) {
        this.startScale = startScale2;
        this.endScale = endScale2;
    }

    public void start() {
        this.scaleVelocity = (this.endScale - this.startScale) / this.wholeActionTime;
        super.start();
    }

    /* access modifiers changed from: protected */
    public void action() {
        this.bindingUI.setScale(this.startScale + (this.scaleVelocity * this.usedTime));
    }
}
