package com.camelgames.framework.ui;

import com.camelgames.framework.Skeleton.AbstractRenderable;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;

public class RenderCompositableNode extends AbstractRenderable implements RenderCompositable {
    protected float actualX;
    protected float actualY;
    protected ArrayList<RenderCompositable> children;
    protected RenderCompositable parent;
    protected float parentActualX;
    protected float parentActualY;
    protected float relativeX;
    protected float relativeY;

    public void addChild(RenderCompositable child) {
        if (child != null) {
            if (this.children == null) {
                this.children = new ArrayList<>();
            }
            if (!this.children.contains(child)) {
                this.children.add(child);
                child.setParent(this);
                child.updateActualPosition(this.actualX, this.actualY);
            }
        }
    }

    public void removeChild(RenderCompositable child) {
        if (this.children != null && child != null) {
            child.setParent(null);
            this.children.remove(child);
        }
    }

    public void clearChildren() {
        if (this.children != null) {
            for (int i = 0; i < this.children.size(); i++) {
                this.children.get(i).setParent(null);
            }
            this.children.clear();
        }
    }

    public RenderCompositable getParent() {
        return this.parent;
    }

    public void setParent(RenderCompositable parent2) {
        this.parent = parent2;
    }

    public void detachFromParent() {
        if (this.parent != null) {
            this.parent.removeChild(this);
        }
    }

    public void setPosition(float relativeX2, float relativeY2) {
        this.relativeX = relativeX2;
        this.relativeY = relativeY2;
        updateActualPosition(this.parentActualX, this.parentActualY);
    }

    public void updateActualPosition(float parentActualX2, float parentActualY2) {
        this.parentActualX = parentActualX2;
        this.parentActualY = parentActualY2;
        this.actualX = this.relativeX + parentActualX2;
        this.actualY = this.relativeY + parentActualY2;
        if (this.children != null) {
            for (int i = 0; i < this.children.size(); i++) {
                this.children.get(i).updateActualPosition(this.actualX, this.actualY);
            }
        }
    }

    public void render(GL10 gl, float elapsedTime) {
        renderChildren(gl, elapsedTime);
    }

    public void renderChildren(GL10 gl, float elapsedTime) {
        if (this.children != null) {
            for (int i = 0; i < this.children.size(); i++) {
                this.children.get(i).render(gl, elapsedTime);
            }
        }
    }

    public float getRelativeX() {
        return this.relativeX;
    }

    public float getRelativeY() {
        return this.relativeY;
    }

    public float getActualX() {
        return this.actualX;
    }

    public float getActualY() {
        return this.actualY;
    }
}
