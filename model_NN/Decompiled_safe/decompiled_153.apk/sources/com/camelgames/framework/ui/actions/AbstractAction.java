package com.camelgames.framework.ui.actions;

import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;

public abstract class AbstractAction implements Action {
    protected UI bindingUI;
    protected Callback onFinished;
    protected Status status = Status.Stopped;
    protected float usedTime;

    protected enum Status {
        Stopped,
        Running,
        Paused
    }

    public void bind(UI bindingUI2, Callback onFinished2) {
        bindUI(bindingUI2);
        bindCallback(onFinished2);
    }

    public void bindUI(UI bindingUI2) {
        this.bindingUI = bindingUI2;
    }

    public void bindCallback(Callback onFinished2) {
        this.onFinished = onFinished2;
    }

    public UI getBinding() {
        return this.bindingUI;
    }

    public void pause() {
        this.status = Status.Paused;
    }

    public void resume() {
        this.status = Status.Running;
    }

    public void start() {
        this.usedTime = 0.0f;
        this.status = Status.Running;
    }

    public void stop() {
        this.usedTime = 0.0f;
        this.status = Status.Stopped;
    }

    public void setUsedTime(float usedTime2) {
        this.usedTime = usedTime2;
    }

    public float getTimeUsed() {
        return this.usedTime;
    }

    public boolean isPaused() {
        return this.status.equals(Status.Paused);
    }

    public boolean isRunning() {
        return this.status.equals(Status.Running);
    }

    public boolean isStopped() {
        return this.status.equals(Status.Stopped);
    }
}
