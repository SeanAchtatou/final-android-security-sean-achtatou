package com.camelgames.framework.ui;

import com.camelgames.framework.ui.actions.Action;
import com.camelgames.framework.ui.actions.ActionManager;

public abstract class AbstractUI extends RenderCompositableNode implements UI {
    protected Action action;
    protected float height;
    protected float scale = 1.0f;
    protected float width;

    public float getWidth() {
        return this.width;
    }

    public float getHeight() {
        return this.height;
    }

    public void setSize(float width2, float height2) {
        this.width = width2;
        this.height = height2;
    }

    public float getScale() {
        return this.scale;
    }

    public void setScale(float scale2) {
        this.scale = scale2;
    }

    public float getActualLeft() {
        return this.actualX - (0.5f * this.width);
    }

    public float getActualRight() {
        return this.actualX + (0.5f * this.width);
    }

    public float getActualTop() {
        return this.actualY - (0.5f * this.height);
    }

    public float getActualBottom() {
        return this.actualY + (0.5f * this.height);
    }

    public void setLeft(float relativeLeft) {
        setX((0.5f * this.width) + relativeLeft);
    }

    public void setTop(float relativeTop) {
        setY((0.5f * this.height) + relativeTop);
    }

    public void setX(float relativeX) {
        setPosition(relativeX, this.relativeY);
    }

    public void setY(float relativeY) {
        setPosition(this.relativeX, relativeY);
    }

    public void moveX(float deltaX) {
        setX(this.relativeX + deltaX);
    }

    public void moveY(float deltaY) {
        setY(this.relativeY + deltaY);
    }

    public void move(float deltaX, float deltaY) {
        setPosition(this.relativeX + deltaX, this.relativeY + deltaY);
    }

    public void setLeftTop(float relativeLeft, float relativeTop) {
        setPosition((this.width * 0.5f) + relativeLeft, (this.height * 0.5f) + relativeTop);
    }

    public void setAction(Action action2) {
        this.action = action2;
        this.action.bindUI(this);
    }

    public Action getAction() {
        return this.action;
    }

    public void startAction() {
        if (this.action != null) {
            this.action.bindUI(this);
            ActionManager.instance.add(this.action);
        }
    }

    public void stopAction() {
        ActionManager.instance.remove(this.action);
    }
}
