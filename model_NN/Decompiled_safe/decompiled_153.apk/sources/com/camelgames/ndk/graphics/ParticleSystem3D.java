package com.camelgames.ndk.graphics;

public class ParticleSystem3D {
    public static final int DurationInfinity = NDK_GraphicsJNI.ParticleSystem3D_DurationInfinity_get();
    public static final int StartSizeEqualToEndSize = NDK_GraphicsJNI.ParticleSystem3D_StartSizeEqualToEndSize_get();
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected ParticleSystem3D(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(ParticleSystem3D obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_ParticleSystem3D(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public ParticleSystem3D(int maxParticleCount) {
        this(NDK_GraphicsJNI.new_ParticleSystem3D(maxParticleCount), true);
    }

    public void addTexture(int texId) {
        NDK_GraphicsJNI.ParticleSystem3D_addTexture(this.swigCPtr, texId);
    }

    public void setTexture(Texture pTexture) {
        NDK_GraphicsJNI.ParticleSystem3D_setTexture(this.swigCPtr, Texture.getCPtr(pTexture));
    }

    public void clearTexture() {
        NDK_GraphicsJNI.ParticleSystem3D_clearTexture(this.swigCPtr);
    }

    public boolean update(float elpasedTime, float eyeX, float eyeY, float eyeZ) {
        return NDK_GraphicsJNI.ParticleSystem3D_update(this.swigCPtr, elpasedTime, eyeX, eyeY, eyeZ);
    }

    public void reset() {
        NDK_GraphicsJNI.ParticleSystem3D_reset(this.swigCPtr);
    }

    public boolean isActive() {
        return NDK_GraphicsJNI.ParticleSystem3D_isActive(this.swigCPtr);
    }

    public void setActive(boolean active) {
        NDK_GraphicsJNI.ParticleSystem3D_setActive(this.swigCPtr, active);
    }

    public void stop() {
        NDK_GraphicsJNI.ParticleSystem3D_stop(this.swigCPtr);
    }

    public boolean isFull() {
        return NDK_GraphicsJNI.ParticleSystem3D_isFull(this.swigCPtr);
    }

    public void setEmissionRate(float emissionRate) {
        NDK_GraphicsJNI.ParticleSystem3D_setEmissionRate(this.swigCPtr, emissionRate);
    }

    public void setDuration(float duration) {
        NDK_GraphicsJNI.ParticleSystem3D_setDuration(this.swigCPtr, duration);
    }

    public void setPosition(float centerX, float centerY, float centerZ, float posVarX, float posVarY, float posVarZ) {
        NDK_GraphicsJNI.ParticleSystem3D_setPosition(this.swigCPtr, centerX, centerY, centerZ, posVarX, posVarY, posVarZ);
    }

    public void setRotation(float rotation, float rotationVar) {
        NDK_GraphicsJNI.ParticleSystem3D_setRotation(this.swigCPtr, rotation, rotationVar);
    }

    public void setLinarSpeed(float speed, float speedVar) {
        NDK_GraphicsJNI.ParticleSystem3D_setLinarSpeed(this.swigCPtr, speed, speedVar);
    }

    public void setGravity(float gravityX, float gravityY, float gravityZ) {
        NDK_GraphicsJNI.ParticleSystem3D_setGravity(this.swigCPtr, gravityX, gravityY, gravityZ);
    }

    public void setSize(float startSize, float startSizeVar, float endSize, float endSizeVar) {
        NDK_GraphicsJNI.ParticleSystem3D_setSize(this.swigCPtr, startSize, startSizeVar, endSize, endSizeVar);
    }

    public void setSpin(float startSpin, float startSpinVar, float endSpin, float endSpinVar) {
        NDK_GraphicsJNI.ParticleSystem3D_setSpin(this.swigCPtr, startSpin, startSpinVar, endSpin, endSpinVar);
    }

    public void setLife(float life, float lifeVar) {
        NDK_GraphicsJNI.ParticleSystem3D_setLife(this.swigCPtr, life, lifeVar);
    }

    public void setColor(FloatArray pData) {
        NDK_GraphicsJNI.ParticleSystem3D_setColor__SWIG_0(this.swigCPtr, FloatArray.getCPtr(pData));
    }

    public void setColor(float r, float g, float b, float a) {
        NDK_GraphicsJNI.ParticleSystem3D_setColor__SWIG_1(this.swigCPtr, r, g, b, a);
    }

    public void setStartColor(float r, float g, float b, float a) {
        NDK_GraphicsJNI.ParticleSystem3D_setStartColor(this.swigCPtr, r, g, b, a);
    }

    public void setEndColor(float r, float g, float b, float a) {
        NDK_GraphicsJNI.ParticleSystem3D_setEndColor(this.swigCPtr, r, g, b, a);
    }

    public void setStartEndColorEqual(boolean equal) {
        NDK_GraphicsJNI.ParticleSystem3D_setStartEndColorEqual(this.swigCPtr, equal);
    }

    public boolean addParticle() {
        return NDK_GraphicsJNI.ParticleSystem3D_addParticle(this.swigCPtr);
    }
}
