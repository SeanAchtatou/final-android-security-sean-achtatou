package com.camelgames.ndk.graphics;

public class Texture extends ITexture {
    private int swigCPtr;

    public Texture(int cPtr, boolean cMemoryOwn) {
        super(NDK_GraphicsJNI.SWIGTextureUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    public static int getCPtr(Texture obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_Texture(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.Texture.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.Texture.<init>(int, int):void
      com.camelgames.ndk.graphics.Texture.<init>(int, boolean):void */
    public Texture(int id, int resourceId) {
        this(NDK_GraphicsJNI.new_Texture(id, resourceId), true);
    }

    public void initiate(int resourceId) {
        NDK_GraphicsJNI.Texture_initiate(this.swigCPtr, resourceId);
    }

    public int getId() {
        return NDK_GraphicsJNI.Texture_getId(this.swigCPtr);
    }

    public int getResourceId() {
        return NDK_GraphicsJNI.Texture_getResourceId(this.swigCPtr);
    }

    public int getTextureId() {
        return NDK_GraphicsJNI.Texture_getTextureId(this.swigCPtr);
    }

    public void bindTexture() {
        NDK_GraphicsJNI.Texture_bindTexture(this.swigCPtr);
    }

    public int getWidth() {
        return NDK_GraphicsJNI.Texture_getWidth(this.swigCPtr);
    }

    public int getHeight() {
        return NDK_GraphicsJNI.Texture_getHeight(this.swigCPtr);
    }

    public void load() {
        NDK_GraphicsJNI.Texture_load(this.swigCPtr);
    }

    public void unload() {
        NDK_GraphicsJNI.Texture_unload(this.swigCPtr);
    }

    public void textureLost() {
        NDK_GraphicsJNI.Texture_textureLost(this.swigCPtr);
    }

    public void textureRest() {
        NDK_GraphicsJNI.Texture_textureRest(this.swigCPtr);
    }

    public float getLeftCoord() {
        return NDK_GraphicsJNI.Texture_getLeftCoord(this.swigCPtr);
    }

    public float getTopCoord() {
        return NDK_GraphicsJNI.Texture_getTopCoord(this.swigCPtr);
    }

    public float getRightCoord() {
        return NDK_GraphicsJNI.Texture_getRightCoord(this.swigCPtr);
    }

    public float getBottomCoord() {
        return NDK_GraphicsJNI.Texture_getBottomCoord(this.swigCPtr);
    }

    public int getAltasLeft() {
        return NDK_GraphicsJNI.Texture_getAltasLeft(this.swigCPtr);
    }

    public int getAltasTop() {
        return NDK_GraphicsJNI.Texture_getAltasTop(this.swigCPtr);
    }

    public int getAltasWidth() {
        return NDK_GraphicsJNI.Texture_getAltasWidth(this.swigCPtr);
    }

    public int getAltasHeight() {
        return NDK_GraphicsJNI.Texture_getAltasHeight(this.swigCPtr);
    }

    public void setAltasTexCoords(int altasLeft, int altasTop, int altasWidth, int altasHeight) {
        NDK_GraphicsJNI.Texture_setAltasTexCoords(this.swigCPtr, altasLeft, altasTop, altasWidth, altasHeight);
    }

    public void setTexCoords(float leftCoord, float topCoord, float rightCoord, float bottomCoord) {
        NDK_GraphicsJNI.Texture_setTexCoords(this.swigCPtr, leftCoord, topCoord, rightCoord, bottomCoord);
    }

    public void setGLPara(float MAG_FILTER, float MIN_FILTER, float WRAP_S, float WRAP_T) {
        NDK_GraphicsJNI.Texture_setGLPara(this.swigCPtr, MAG_FILTER, MIN_FILTER, WRAP_S, WRAP_T);
    }

    public boolean isAltas() {
        return NDK_GraphicsJNI.Texture_isAltas(this.swigCPtr);
    }
}
