package com.camelgames.ndk.graphics;

public class Sprite2D extends Sprite {
    private int swigCPtr;

    protected Sprite2D(int cPtr, boolean cMemoryOwn) {
        super(NDK_GraphicsJNI.SWIGSprite2DUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(Sprite2D obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_Sprite2D(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.Sprite2D.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.Sprite2D.<init>(float, float):void
      com.camelgames.ndk.graphics.Sprite2D.<init>(int, boolean):void */
    public Sprite2D(float widthPara, float heightPara) {
        this(NDK_GraphicsJNI.new_Sprite2D__SWIG_0(widthPara, heightPara), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.Sprite2D.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.Sprite2D.<init>(float, float):void
      com.camelgames.ndk.graphics.Sprite2D.<init>(int, boolean):void */
    public Sprite2D(float widthPara) {
        this(NDK_GraphicsJNI.new_Sprite2D__SWIG_1(widthPara), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.Sprite2D.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.Sprite2D.<init>(float, float):void
      com.camelgames.ndk.graphics.Sprite2D.<init>(int, boolean):void */
    public Sprite2D() {
        this(NDK_GraphicsJNI.new_Sprite2D__SWIG_2(), true);
    }

    public void setTexId(int resourceId) {
        NDK_GraphicsJNI.Sprite2D_setTexId(this.swigCPtr, resourceId);
    }

    public int getTextureId() {
        return NDK_GraphicsJNI.Sprite2D_getTextureId(this.swigCPtr);
    }

    public void setTexture(ITexture pTexture) {
        NDK_GraphicsJNI.Sprite2D_setTexture(this.swigCPtr, ITexture.getCPtr(pTexture));
    }

    public ITexture getTexutre() {
        int cPtr = NDK_GraphicsJNI.Sprite2D_getTexutre(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new ITexture(cPtr, false);
    }

    public void setSizeByPixelScale(float scale) {
        NDK_GraphicsJNI.Sprite2D_setSizeByPixelScale(this.swigCPtr, scale);
    }

    public void setWidthConstrainProportion(float width) {
        NDK_GraphicsJNI.Sprite2D_setWidthConstrainProportion(this.swigCPtr, width);
    }

    public void setHeightConstrainProportion(float height) {
        NDK_GraphicsJNI.Sprite2D_setHeightConstrainProportion(this.swigCPtr, height);
    }

    public void setSize(float width, float height) {
        NDK_GraphicsJNI.Sprite2D_setSize(this.swigCPtr, width, height);
    }

    public float getWidth() {
        return NDK_GraphicsJNI.Sprite2D_getWidth(this.swigCPtr);
    }

    public float getHeight() {
        return NDK_GraphicsJNI.Sprite2D_getHeight(this.swigCPtr);
    }

    public void setLeftTop(float left, float top) {
        NDK_GraphicsJNI.Sprite2D_setLeftTop(this.swigCPtr, left, top);
    }

    public void setRightBottom(float right, float bottom) {
        NDK_GraphicsJNI.Sprite2D_setRightBottom(this.swigCPtr, right, bottom);
    }

    public float getLeft() {
        return NDK_GraphicsJNI.Sprite2D_getLeft(this.swigCPtr);
    }

    public void setLeft(float left) {
        NDK_GraphicsJNI.Sprite2D_setLeft(this.swigCPtr, left);
    }

    public float getRight() {
        return NDK_GraphicsJNI.Sprite2D_getRight(this.swigCPtr);
    }

    public void setRight(float right) {
        NDK_GraphicsJNI.Sprite2D_setRight(this.swigCPtr, right);
    }

    public float getTop() {
        return NDK_GraphicsJNI.Sprite2D_getTop(this.swigCPtr);
    }

    public void setTop(float top) {
        NDK_GraphicsJNI.Sprite2D_setTop(this.swigCPtr, top);
    }

    public float getBottom() {
        return NDK_GraphicsJNI.Sprite2D_getBottom(this.swigCPtr);
    }

    public void setBottom(float bottom) {
        NDK_GraphicsJNI.Sprite2D_setBottom(this.swigCPtr, bottom);
    }

    public void bindVertices() {
        NDK_GraphicsJNI.Sprite2D_bindVertices(this.swigCPtr);
    }

    public void bindTexCoords() {
        NDK_GraphicsJNI.Sprite2D_bindTexCoords(this.swigCPtr);
    }

    public void setTexCoords(float leftCoord, float topCoord, float rightCoord, float bottomCoord) {
        NDK_GraphicsJNI.Sprite2D_setTexCoords(this.swigCPtr, leftCoord, topCoord, rightCoord, bottomCoord);
    }

    public void updateVertices() {
        NDK_GraphicsJNI.Sprite2D_updateVertices(this.swigCPtr);
    }

    public void drawOES() {
        NDK_GraphicsJNI.Sprite2D_drawOES(this.swigCPtr);
    }
}
