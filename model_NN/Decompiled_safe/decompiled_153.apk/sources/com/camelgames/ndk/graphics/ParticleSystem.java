package com.camelgames.ndk.graphics;

public class ParticleSystem {
    public static final int DurationInfinity = NDK_GraphicsJNI.ParticleSystem_DurationInfinity_get();
    public static final int StartSizeEqualToEndSize = NDK_GraphicsJNI.ParticleSystem_StartSizeEqualToEndSize_get();
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected ParticleSystem(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(ParticleSystem obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_ParticleSystem(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public ParticleSystem(int maxParticleCount) {
        this(NDK_GraphicsJNI.new_ParticleSystem(maxParticleCount), true);
    }

    public void addTexture(int texId) {
        NDK_GraphicsJNI.ParticleSystem_addTexture(this.swigCPtr, texId);
    }

    public void setTexture(Texture pTexture) {
        NDK_GraphicsJNI.ParticleSystem_setTexture(this.swigCPtr, Texture.getCPtr(pTexture));
    }

    public void clearTexture() {
        NDK_GraphicsJNI.ParticleSystem_clearTexture(this.swigCPtr);
    }

    public boolean update(float elpasedTime) {
        return NDK_GraphicsJNI.ParticleSystem_update(this.swigCPtr, elpasedTime);
    }

    public void reset() {
        NDK_GraphicsJNI.ParticleSystem_reset(this.swigCPtr);
    }

    public boolean isActive() {
        return NDK_GraphicsJNI.ParticleSystem_isActive(this.swigCPtr);
    }

    public void setActive(boolean active) {
        NDK_GraphicsJNI.ParticleSystem_setActive(this.swigCPtr, active);
    }

    public void stop() {
        NDK_GraphicsJNI.ParticleSystem_stop(this.swigCPtr);
    }

    public boolean isFull() {
        return NDK_GraphicsJNI.ParticleSystem_isFull(this.swigCPtr);
    }

    public void setEmissionRate(float emissionRate) {
        NDK_GraphicsJNI.ParticleSystem_setEmissionRate(this.swigCPtr, emissionRate);
    }

    public void setDuration(float duration) {
        NDK_GraphicsJNI.ParticleSystem_setDuration(this.swigCPtr, duration);
    }

    public void setPosition(float centerX, float centerY, float posVarX, float posVarY) {
        NDK_GraphicsJNI.ParticleSystem_setPosition(this.swigCPtr, centerX, centerY, posVarX, posVarY);
    }

    public void setRotation(float rotation, float rotationVar) {
        NDK_GraphicsJNI.ParticleSystem_setRotation(this.swigCPtr, rotation, rotationVar);
    }

    public void setLinarSpeed(float speed, float speedVar) {
        NDK_GraphicsJNI.ParticleSystem_setLinarSpeed(this.swigCPtr, speed, speedVar);
    }

    public void setGravity(float gravityX, float gravityY) {
        NDK_GraphicsJNI.ParticleSystem_setGravity(this.swigCPtr, gravityX, gravityY);
    }

    public void setSize(float startSize, float startSizeVar, float endSize, float endSizeVar) {
        NDK_GraphicsJNI.ParticleSystem_setSize(this.swigCPtr, startSize, startSizeVar, endSize, endSizeVar);
    }

    public void setSpin(float startSpin, float startSpinVar, float endSpin, float endSpinVar) {
        NDK_GraphicsJNI.ParticleSystem_setSpin(this.swigCPtr, startSpin, startSpinVar, endSpin, endSpinVar);
    }

    public void setLife(float life, float lifeVar) {
        NDK_GraphicsJNI.ParticleSystem_setLife(this.swigCPtr, life, lifeVar);
    }

    public void setColor(FloatArray pData) {
        NDK_GraphicsJNI.ParticleSystem_setColor__SWIG_0(this.swigCPtr, FloatArray.getCPtr(pData));
    }

    public void setColor(float r, float g, float b, float a) {
        NDK_GraphicsJNI.ParticleSystem_setColor__SWIG_1(this.swigCPtr, r, g, b, a);
    }

    public void setStartColor(float r, float g, float b, float a) {
        NDK_GraphicsJNI.ParticleSystem_setStartColor(this.swigCPtr, r, g, b, a);
    }

    public void setEndColor(float r, float g, float b, float a) {
        NDK_GraphicsJNI.ParticleSystem_setEndColor(this.swigCPtr, r, g, b, a);
    }

    public boolean addParticle() {
        return NDK_GraphicsJNI.ParticleSystem_addParticle(this.swigCPtr);
    }
}
