package com.camelgames.ndk.graphics;

public class LeftAlignedNumberText extends NumberText {
    private int swigCPtr;

    protected LeftAlignedNumberText(int cPtr, boolean cMemoryOwn) {
        super(NDK_GraphicsJNI.SWIGLeftAlignedNumberTextUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(LeftAlignedNumberText obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_LeftAlignedNumberText(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    public LeftAlignedNumberText(int capacityPara) {
        this(NDK_GraphicsJNI.new_LeftAlignedNumberText(capacityPara), true);
    }

    public void setPosition(float left, float top) {
        NDK_GraphicsJNI.LeftAlignedNumberText_setPosition(this.swigCPtr, left, top);
    }
}
