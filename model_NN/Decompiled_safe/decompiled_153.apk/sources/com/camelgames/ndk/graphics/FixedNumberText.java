package com.camelgames.ndk.graphics;

public class FixedNumberText extends LeftAlignedNumberText {
    private int swigCPtr;

    protected FixedNumberText(int cPtr, boolean cMemoryOwn) {
        super(NDK_GraphicsJNI.SWIGFixedNumberTextUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(FixedNumberText obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_FixedNumberText(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.FixedNumberText.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.FixedNumberText.<init>(int, int):void
      com.camelgames.ndk.graphics.FixedNumberText.<init>(int, boolean):void */
    public FixedNumberText(int capacityPara, int fixedCountPara) {
        this(NDK_GraphicsJNI.new_FixedNumberText(capacityPara, fixedCountPara), true);
    }

    public void fixDigitsCount(int count) {
        NDK_GraphicsJNI.FixedNumberText_fixDigitsCount(this.swigCPtr, count);
    }
}
