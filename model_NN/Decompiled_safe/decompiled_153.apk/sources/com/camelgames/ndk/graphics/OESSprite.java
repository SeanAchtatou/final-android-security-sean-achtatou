package com.camelgames.ndk.graphics;

public class OESSprite {
    public static final int InverseX = NDK_GraphicsJNI.OESSprite_InverseX_get();
    public static final int InverseY = NDK_GraphicsJNI.OESSprite_InverseY_get();
    public static final int NoInverse = NDK_GraphicsJNI.OESSprite_NoInverse_get();
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected OESSprite(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(OESSprite obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_OESSprite(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public OESSprite() {
        this(NDK_GraphicsJNI.new_OESSprite(), true);
    }

    public void setRect(int left, int top, int width, int height) {
        NDK_GraphicsJNI.OESSprite_setRect(this.swigCPtr, left, top, width, height);
    }

    public void setLeftTop(int left, int top) {
        NDK_GraphicsJNI.OESSprite_setLeftTop(this.swigCPtr, left, top);
    }

    public void setPosition(int x, int y) {
        NDK_GraphicsJNI.OESSprite_setPosition(this.swigCPtr, x, y);
    }

    public void move(int deltaX, int deltaY) {
        NDK_GraphicsJNI.OESSprite_move(this.swigCPtr, deltaX, deltaY);
    }

    public void setSizeByPixelScale(float scale) {
        NDK_GraphicsJNI.OESSprite_setSizeByPixelScale(this.swigCPtr, scale);
    }

    public void setSize(int width, int height) {
        NDK_GraphicsJNI.OESSprite_setSize(this.swigCPtr, width, height);
    }

    public void setScale(float scale) {
        NDK_GraphicsJNI.OESSprite_setScale(this.swigCPtr, scale);
    }

    public float getScale() {
        return NDK_GraphicsJNI.OESSprite_getScale(this.swigCPtr);
    }

    public int getLeft() {
        return NDK_GraphicsJNI.OESSprite_getLeft(this.swigCPtr);
    }

    public int getTop() {
        return NDK_GraphicsJNI.OESSprite_getTop(this.swigCPtr);
    }

    public int getWidth() {
        return NDK_GraphicsJNI.OESSprite_getWidth(this.swigCPtr);
    }

    public int getHeight() {
        return NDK_GraphicsJNI.OESSprite_getHeight(this.swigCPtr);
    }

    public int getRight() {
        return NDK_GraphicsJNI.OESSprite_getRight(this.swigCPtr);
    }

    public int getBottom() {
        return NDK_GraphicsJNI.OESSprite_getBottom(this.swigCPtr);
    }

    public int getCenterX() {
        return NDK_GraphicsJNI.OESSprite_getCenterX(this.swigCPtr);
    }

    public int getCenterY() {
        return NDK_GraphicsJNI.OESSprite_getCenterY(this.swigCPtr);
    }

    public void setWidthConstrainProportion(float width) {
        NDK_GraphicsJNI.OESSprite_setWidthConstrainProportion(this.swigCPtr, width);
    }

    public void setHeightConstrainProportion(float height) {
        NDK_GraphicsJNI.OESSprite_setHeightConstrainProportion(this.swigCPtr, height);
    }

    public void drawOES() {
        NDK_GraphicsJNI.OESSprite_drawOES(this.swigCPtr);
    }

    public void setTexId(int id) {
        NDK_GraphicsJNI.OESSprite_setTexId(this.swigCPtr, id);
    }

    public int getTextureId() {
        return NDK_GraphicsJNI.OESSprite_getTextureId(this.swigCPtr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.Texture.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.Texture.<init>(int, int):void
      com.camelgames.ndk.graphics.Texture.<init>(int, boolean):void */
    public Texture getTexture() {
        int cPtr = NDK_GraphicsJNI.OESSprite_getTexture(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new Texture(cPtr, false);
    }

    public void setMappingType(int mappingType) {
        NDK_GraphicsJNI.OESSprite_setMappingType(this.swigCPtr, mappingType);
    }

    public void setAltasTexCoords(int altasLeft, int altasTop, int altasWidth, int altasHeight) {
        NDK_GraphicsJNI.OESSprite_setAltasTexCoords(this.swigCPtr, altasLeft, altasTop, altasWidth, altasHeight);
    }

    public boolean hitTest(float x, float y) {
        return NDK_GraphicsJNI.OESSprite_hitTest(this.swigCPtr, x, y);
    }

    public void setColor(float a) {
        NDK_GraphicsJNI.OESSprite_setColor__SWIG_0(this.swigCPtr, a);
    }

    public void setColor(float r, float g, float b) {
        NDK_GraphicsJNI.OESSprite_setColor__SWIG_1(this.swigCPtr, r, g, b);
    }

    public void setColor(float r, float g, float b, float a) {
        NDK_GraphicsJNI.OESSprite_setColor__SWIG_2(this.swigCPtr, r, g, b, a);
    }

    public void setAlpha(float a) {
        NDK_GraphicsJNI.OESSprite_setAlpha(this.swigCPtr, a);
    }
}
