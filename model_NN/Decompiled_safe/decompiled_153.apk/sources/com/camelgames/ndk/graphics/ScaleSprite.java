package com.camelgames.ndk.graphics;

public class ScaleSprite {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected ScaleSprite(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(ScaleSprite obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_ScaleSprite(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public ScaleSprite(Sprite pSpritePara) {
        this(NDK_GraphicsJNI.new_ScaleSprite__SWIG_0(Sprite.getCPtr(pSpritePara)), true);
    }

    public ScaleSprite() {
        this(NDK_GraphicsJNI.new_ScaleSprite__SWIG_1(), true);
    }

    public void setSprite(Sprite pSprite) {
        NDK_GraphicsJNI.ScaleSprite_setSprite(this.swigCPtr, Sprite.getCPtr(pSprite));
    }

    public Sprite getSprite() {
        int cPtr = NDK_GraphicsJNI.ScaleSprite_getSprite(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new Sprite(cPtr, false);
    }

    public void render(float elapsedTime) {
        NDK_GraphicsJNI.ScaleSprite_render(this.swigCPtr, elapsedTime);
    }

    public void setLoop(boolean loop) {
        NDK_GraphicsJNI.ScaleSprite_setLoop(this.swigCPtr, loop);
    }

    public void setFold(boolean fold) {
        NDK_GraphicsJNI.ScaleSprite_setFold(this.swigCPtr, fold);
    }

    public void setBackward(boolean backward) {
        NDK_GraphicsJNI.ScaleSprite_setBackward(this.swigCPtr, backward);
    }

    public void setChangeTime(float changeTime) {
        NDK_GraphicsJNI.ScaleSprite_setChangeTime(this.swigCPtr, changeTime);
    }

    public void setStop(boolean stop) {
        NDK_GraphicsJNI.ScaleSprite_setStop(this.swigCPtr, stop);
    }

    public boolean isStopped() {
        return NDK_GraphicsJNI.ScaleSprite_isStopped(this.swigCPtr);
    }

    public void setFrozen(boolean frozen) {
        NDK_GraphicsJNI.ScaleSprite_setFrozen(this.swigCPtr, frozen);
    }

    public boolean isFrozen() {
        return NDK_GraphicsJNI.ScaleSprite_isFrozen(this.swigCPtr);
    }

    public float getScale() {
        return NDK_GraphicsJNI.ScaleSprite_getScale(this.swigCPtr);
    }

    public void setScaleInfo(float maxScale, float minScale, float scaleStep) {
        NDK_GraphicsJNI.ScaleSprite_setScaleInfo(this.swigCPtr, maxScale, minScale, scaleStep);
    }

    public void setCurrentScale(float scale) {
        NDK_GraphicsJNI.ScaleSprite_setCurrentScale(this.swigCPtr, scale);
    }
}
