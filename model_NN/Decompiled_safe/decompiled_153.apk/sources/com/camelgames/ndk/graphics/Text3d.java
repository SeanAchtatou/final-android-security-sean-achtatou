package com.camelgames.ndk.graphics;

public class Text3d {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected Text3d(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(Text3d obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_Text3d(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.Text3d.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.Text3d.<init>(int, char):void
      com.camelgames.ndk.graphics.Text3d.<init>(int, boolean):void */
    public Text3d(int capacityPara, char emptyCharPara) {
        this(NDK_GraphicsJNI.new_Text3d(capacityPara, emptyCharPara), true);
    }

    public void initiate(TextBuilder pTextBuilder, float fontWidth, float fontHeight) {
        NDK_GraphicsJNI.Text3d_initiate(this.swigCPtr, TextBuilder.getCPtr(pTextBuilder), fontWidth, fontHeight);
    }

    public void setChar(char c, int i) {
        NDK_GraphicsJNI.Text3d_setChar(this.swigCPtr, c, i);
    }

    public void setNumber(float number) {
        NDK_GraphicsJNI.Text3d_setNumber(this.swigCPtr, number);
    }

    public void render(float elapsedTime) {
        NDK_GraphicsJNI.Text3d_render(this.swigCPtr, elapsedTime);
    }

    public int getTextureId() {
        return NDK_GraphicsJNI.Text3d_getTextureId(this.swigCPtr);
    }

    public void move(float deltaX, float deltaY, float deltaZ) {
        NDK_GraphicsJNI.Text3d_move(this.swigCPtr, deltaX, deltaY, deltaZ);
    }

    public void setColor(float a) {
        NDK_GraphicsJNI.Text3d_setColor__SWIG_0(this.swigCPtr, a);
    }

    public void setColor(float r, float g, float b, float a) {
        NDK_GraphicsJNI.Text3d_setColor__SWIG_1(this.swigCPtr, r, g, b, a);
    }
}
