package com.camelgames.ndk.graphics;

public class SpriteRects {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected SpriteRects(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(SpriteRects obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_SpriteRects(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public SpriteRects(FloatArray verticesArray, FloatArray texCoordsArray, int rectCountsPara) {
        this(NDK_GraphicsJNI.new_SpriteRects(FloatArray.getCPtr(verticesArray), FloatArray.getCPtr(texCoordsArray), rectCountsPara), true);
    }

    public void render(float elapsedTime) {
        NDK_GraphicsJNI.SpriteRects_render(this.swigCPtr, elapsedTime);
    }

    public void setTexId(int resourceId) {
        NDK_GraphicsJNI.SpriteRects_setTexId(this.swigCPtr, resourceId);
    }

    public int getTextureId() {
        return NDK_GraphicsJNI.SpriteRects_getTextureId(this.swigCPtr);
    }

    public void setPosition(float x, float y, float radianAngle) {
        NDK_GraphicsJNI.SpriteRects_setPosition(this.swigCPtr, x, y, radianAngle);
    }

    public void setX(float x) {
        NDK_GraphicsJNI.SpriteRects_setX(this.swigCPtr, x);
    }

    public float getX() {
        return NDK_GraphicsJNI.SpriteRects_getX(this.swigCPtr);
    }

    public void setY(float y) {
        NDK_GraphicsJNI.SpriteRects_setY(this.swigCPtr, y);
    }

    public float getY() {
        return NDK_GraphicsJNI.SpriteRects_getY(this.swigCPtr);
    }

    public void setAngle(float radianAngle) {
        NDK_GraphicsJNI.SpriteRects_setAngle(this.swigCPtr, radianAngle);
    }

    public float getAngle() {
        return NDK_GraphicsJNI.SpriteRects_getAngle(this.swigCPtr);
    }

    public void move(float deltaX, float deltaY) {
        NDK_GraphicsJNI.SpriteRects_move(this.swigCPtr, deltaX, deltaY);
    }

    public void rotate(float deltaAngle) {
        NDK_GraphicsJNI.SpriteRects_rotate(this.swigCPtr, deltaAngle);
    }

    public void setColor(float a) {
        NDK_GraphicsJNI.SpriteRects_setColor__SWIG_0(this.swigCPtr, a);
    }

    public void setColor(float r, float g, float b, float a) {
        NDK_GraphicsJNI.SpriteRects_setColor__SWIG_1(this.swigCPtr, r, g, b, a);
    }

    public void setScale(float scale) {
        NDK_GraphicsJNI.SpriteRects_setScale(this.swigCPtr, scale);
    }

    public float getScale() {
        return NDK_GraphicsJNI.SpriteRects_getScale(this.swigCPtr);
    }
}
