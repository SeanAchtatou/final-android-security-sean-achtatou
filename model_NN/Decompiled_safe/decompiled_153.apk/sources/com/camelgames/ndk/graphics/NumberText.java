package com.camelgames.ndk.graphics;

public class NumberText {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected NumberText(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(NumberText obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_NumberText(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public void setFloatParser(char dotCharPara, int smallDigitsCountPara) {
        NDK_GraphicsJNI.NumberText_setFloatParser(this.swigCPtr, dotCharPara, smallDigitsCountPara);
    }

    public void setColor(float r, float g, float b, float a) {
        NDK_GraphicsJNI.NumberText_setColor(this.swigCPtr, r, g, b, a);
    }

    public void render() {
        NDK_GraphicsJNI.NumberText_render(this.swigCPtr);
    }

    public void renderStaight() {
        NDK_GraphicsJNI.NumberText_renderStaight(this.swigCPtr);
    }

    public void initiate(TextBuilder pTextBuilder, int fontWidth, int fontHeight, int fontMargin) {
        NDK_GraphicsJNI.NumberText_initiate(this.swigCPtr, TextBuilder.getCPtr(pTextBuilder), fontWidth, fontHeight, fontMargin);
    }

    public void setNumber(float number) {
        NDK_GraphicsJNI.NumberText_setNumber(this.swigCPtr, number);
    }

    public void reSize(int capacity) {
        NDK_GraphicsJNI.NumberText_reSize(this.swigCPtr, capacity);
    }

    public void setScale(float scale) {
        NDK_GraphicsJNI.NumberText_setScale(this.swigCPtr, scale);
    }

    public float getScale() {
        return NDK_GraphicsJNI.NumberText_getScale(this.swigCPtr);
    }

    public void setPosition(float x, float y) {
        NDK_GraphicsJNI.NumberText_setPosition(this.swigCPtr, x, y);
    }
}
