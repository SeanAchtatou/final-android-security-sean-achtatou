package com.camelgames.ndk.graphics;

public class FloatArray {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected FloatArray(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    public static int getCPtr(FloatArray obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_FloatArray(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public FloatArray(int count) {
        this(NDK_GraphicsJNI.new_FloatArray(count), true);
    }

    public float get(int index) {
        return NDK_GraphicsJNI.FloatArray_get(this.swigCPtr, index);
    }

    public void set(float value, int index) {
        NDK_GraphicsJNI.FloatArray_set(this.swigCPtr, value, index);
    }

    public int getCount() {
        return NDK_GraphicsJNI.FloatArray_getCount(this.swigCPtr);
    }
}
