package com.camelgames.ndk.graphics;

public class Sprite3D {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected Sprite3D(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(Sprite3D obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_Sprite3D(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public Sprite3D() {
        this(NDK_GraphicsJNI.new_Sprite3D(), true);
    }

    public void setSizeByPixelScale(float scale) {
        NDK_GraphicsJNI.Sprite3D_setSizeByPixelScale(this.swigCPtr, scale);
    }

    public void setWidthConstrainProportion(float width) {
        NDK_GraphicsJNI.Sprite3D_setWidthConstrainProportion(this.swigCPtr, width);
    }

    public void setHeightConstrainProportion(float height) {
        NDK_GraphicsJNI.Sprite3D_setHeightConstrainProportion(this.swigCPtr, height);
    }

    public void setSize(float width, float height) {
        NDK_GraphicsJNI.Sprite3D_setSize(this.swigCPtr, width, height);
    }

    public void setPosition(float x, float y, float z) {
        NDK_GraphicsJNI.Sprite3D_setPosition(this.swigCPtr, x, y, z);
    }

    public static void alignBillboard(float eyeX, float eyeY, float eyeZ, float lookAtX, float lookAtY, float lookAtZ) {
        NDK_GraphicsJNI.Sprite3D_alignBillboard(eyeX, eyeY, eyeZ, lookAtX, lookAtY, lookAtZ);
    }

    public void updateVertices() {
        NDK_GraphicsJNI.Sprite3D_updateVertices(this.swigCPtr);
    }

    public void render(float elapsedTime) {
        NDK_GraphicsJNI.Sprite3D_render(this.swigCPtr, elapsedTime);
    }

    public void setTexId(int resourceId) {
        NDK_GraphicsJNI.Sprite3D_setTexId(this.swigCPtr, resourceId);
    }

    public int getTextureId() {
        return NDK_GraphicsJNI.Sprite3D_getTextureId(this.swigCPtr);
    }

    public void setColor(float a) {
        NDK_GraphicsJNI.Sprite3D_setColor__SWIG_0(this.swigCPtr, a);
    }

    public void setColor(float r, float g, float b, float a) {
        NDK_GraphicsJNI.Sprite3D_setColor__SWIG_1(this.swigCPtr, r, g, b, a);
    }
}
