package com.camelgames.ndk.graphics;

public class ITexture {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected ITexture(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(ITexture obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_ITexture(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public int getTextureId() {
        return NDK_GraphicsJNI.ITexture_getTextureId(this.swigCPtr);
    }

    public void bindTexture() {
        NDK_GraphicsJNI.ITexture_bindTexture(this.swigCPtr);
    }

    public int getAltasLeft() {
        return NDK_GraphicsJNI.ITexture_getAltasLeft(this.swigCPtr);
    }

    public int getAltasTop() {
        return NDK_GraphicsJNI.ITexture_getAltasTop(this.swigCPtr);
    }

    public int getAltasWidth() {
        return NDK_GraphicsJNI.ITexture_getAltasWidth(this.swigCPtr);
    }

    public int getAltasHeight() {
        return NDK_GraphicsJNI.ITexture_getAltasHeight(this.swigCPtr);
    }
}
