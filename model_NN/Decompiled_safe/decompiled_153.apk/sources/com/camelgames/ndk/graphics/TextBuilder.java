package com.camelgames.ndk.graphics;

public class TextBuilder {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected TextBuilder(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(TextBuilder obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_TextBuilder(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public TextBuilder(int resourceId, int unitWidthPixelsPara, int unitHeightPixelsPara, int xOffsetPixelsPara, int yOffsetPixelsPara) {
        this(NDK_GraphicsJNI.new_TextBuilder(resourceId, unitWidthPixelsPara, unitHeightPixelsPara, xOffsetPixelsPara, yOffsetPixelsPara), true);
    }

    public void setColumns(int columns) {
        NDK_GraphicsJNI.TextBuilder_setColumns(this.swigCPtr, columns);
    }

    public void draw(char ch, int left, int top, int fontSize) {
        NDK_GraphicsJNI.TextBuilder_draw(this.swigCPtr, ch, left, top, fontSize);
    }

    public void setTextureResId(int resId) {
        NDK_GraphicsJNI.TextBuilder_setTextureResId(this.swigCPtr, resId);
    }

    public void bindTexture() {
        NDK_GraphicsJNI.TextBuilder_bindTexture(this.swigCPtr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.Texture.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.Texture.<init>(int, int):void
      com.camelgames.ndk.graphics.Texture.<init>(int, boolean):void */
    public Texture getTexture() {
        int cPtr = NDK_GraphicsJNI.TextBuilder_getTexture(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new Texture(cPtr, false);
    }
}
