package com.camelgames.ndk.graphics;

public class Sprite {
    public static final int InverseX = NDK_GraphicsJNI.Sprite_InverseX_get();
    public static final int InverseY = NDK_GraphicsJNI.Sprite_InverseY_get();
    public static final int NoInverse = NDK_GraphicsJNI.Sprite_NoInverse_get();
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected Sprite(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(Sprite obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_Sprite(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public void render(float elapsedTime) {
        NDK_GraphicsJNI.Sprite_render(this.swigCPtr, elapsedTime);
    }

    public static void flush(boolean setBlendFunc) {
        NDK_GraphicsJNI.Sprite_flush(setBlendFunc);
    }

    public static void changeCapacity(int maxRectsCountPara) {
        NDK_GraphicsJNI.Sprite_changeCapacity(maxRectsCountPara);
    }

    public static void set3D() {
        NDK_GraphicsJNI.Sprite_set3D();
    }

    public void setPosition(float x, float y) {
        NDK_GraphicsJNI.Sprite_setPosition__SWIG_0(this.swigCPtr, x, y);
    }

    public void setPosition(float x, float y, float radianAngle) {
        NDK_GraphicsJNI.Sprite_setPosition__SWIG_1(this.swigCPtr, x, y, radianAngle);
    }

    public void setX(float x) {
        NDK_GraphicsJNI.Sprite_setX(this.swigCPtr, x);
    }

    public float getX() {
        return NDK_GraphicsJNI.Sprite_getX(this.swigCPtr);
    }

    public void setY(float y) {
        NDK_GraphicsJNI.Sprite_setY(this.swigCPtr, y);
    }

    public float getY() {
        return NDK_GraphicsJNI.Sprite_getY(this.swigCPtr);
    }

    public void setAngle(float radianAngle) {
        NDK_GraphicsJNI.Sprite_setAngle(this.swigCPtr, radianAngle);
    }

    public float getAngle() {
        return NDK_GraphicsJNI.Sprite_getAngle(this.swigCPtr);
    }

    public void move(float deltaX, float deltaY) {
        NDK_GraphicsJNI.Sprite_move(this.swigCPtr, deltaX, deltaY);
    }

    public void rotate(float deltaAngle) {
        NDK_GraphicsJNI.Sprite_rotate(this.swigCPtr, deltaAngle);
    }

    public void setColor(float a) {
        NDK_GraphicsJNI.Sprite_setColor__SWIG_0(this.swigCPtr, a);
    }

    public void setColor(float r, float g, float b) {
        NDK_GraphicsJNI.Sprite_setColor__SWIG_1(this.swigCPtr, r, g, b);
    }

    public void setColor(float r, float g, float b, float a) {
        NDK_GraphicsJNI.Sprite_setColor__SWIG_2(this.swigCPtr, r, g, b, a);
    }

    public void setAlpha(float a) {
        NDK_GraphicsJNI.Sprite_setAlpha(this.swigCPtr, a);
    }

    public void setScale(float scale) {
        NDK_GraphicsJNI.Sprite_setScale(this.swigCPtr, scale);
    }

    public float getScale() {
        return NDK_GraphicsJNI.Sprite_getScale(this.swigCPtr);
    }

    public void setOffset(float offsetX, float offsetY) {
        NDK_GraphicsJNI.Sprite_setOffset__SWIG_0(this.swigCPtr, offsetX, offsetY);
    }

    public void setOffset(float offsetX, float offsetY, float offsetAngle) {
        NDK_GraphicsJNI.Sprite_setOffset__SWIG_1(this.swigCPtr, offsetX, offsetY, offsetAngle);
    }

    public void bindTexture() {
        NDK_GraphicsJNI.Sprite_bindTexture(this.swigCPtr);
    }

    public void setTexId(int resourceId) {
        NDK_GraphicsJNI.Sprite_setTexId(this.swigCPtr, resourceId);
    }

    public void setMappingType(int value) {
        NDK_GraphicsJNI.Sprite_mappingType_set(this.swigCPtr, value);
    }

    public int getMappingType() {
        return NDK_GraphicsJNI.Sprite_mappingType_get(this.swigCPtr);
    }
}
