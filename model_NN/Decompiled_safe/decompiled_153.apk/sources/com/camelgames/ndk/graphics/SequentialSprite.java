package com.camelgames.ndk.graphics;

public class SequentialSprite extends Sprite2D {
    private int swigCPtr;

    protected SequentialSprite(int cPtr, boolean cMemoryOwn) {
        super(NDK_GraphicsJNI.SWIGSequentialSpriteUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(SequentialSprite obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_SequentialSprite(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.SequentialSprite.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.SequentialSprite.<init>(float, float):void
      com.camelgames.ndk.graphics.SequentialSprite.<init>(int, boolean):void */
    public SequentialSprite(float width, float height) {
        this(NDK_GraphicsJNI.new_SequentialSprite__SWIG_0(width, height), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.SequentialSprite.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.SequentialSprite.<init>(float, float):void
      com.camelgames.ndk.graphics.SequentialSprite.<init>(int, boolean):void */
    public SequentialSprite(float width) {
        this(NDK_GraphicsJNI.new_SequentialSprite__SWIG_1(width), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.SequentialSprite.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.SequentialSprite.<init>(float, float):void
      com.camelgames.ndk.graphics.SequentialSprite.<init>(int, boolean):void */
    public SequentialSprite() {
        this(NDK_GraphicsJNI.new_SequentialSprite__SWIG_2(), true);
    }

    public void setSizeByPixelScale(float scale) {
        NDK_GraphicsJNI.SequentialSprite_setSizeByPixelScale(this.swigCPtr, scale);
    }

    public void setWidthConstrainProportion(float width) {
        NDK_GraphicsJNI.SequentialSprite_setWidthConstrainProportion(this.swigCPtr, width);
    }

    public void setHeightConstrainProportion(float height) {
        NDK_GraphicsJNI.SequentialSprite_setHeightConstrainProportion(this.swigCPtr, height);
    }

    public void setCurrentFrame(int frame) {
        NDK_GraphicsJNI.SequentialSprite_setCurrentFrame(this.swigCPtr, frame);
    }

    public int getCurrentFrame() {
        return NDK_GraphicsJNI.SequentialSprite_getCurrentFrame(this.swigCPtr);
    }

    public void setMinFrame(int minFrame) {
        NDK_GraphicsJNI.SequentialSprite_setMinFrame(this.swigCPtr, minFrame);
    }

    public void setMaxFrame(int maxFrame) {
        NDK_GraphicsJNI.SequentialSprite_setMaxFrame(this.swigCPtr, maxFrame);
    }

    public void setLoop(boolean loop) {
        NDK_GraphicsJNI.SequentialSprite_setLoop(this.swigCPtr, loop);
    }

    public void setFold(boolean fold) {
        NDK_GraphicsJNI.SequentialSprite_setFold(this.swigCPtr, fold);
    }

    public void setBackward(boolean backward) {
        NDK_GraphicsJNI.SequentialSprite_setBackward(this.swigCPtr, backward);
    }

    public void setChangeTime(float changeTime) {
        NDK_GraphicsJNI.SequentialSprite_setChangeTime(this.swigCPtr, changeTime);
    }

    public void setStop(boolean stop) {
        NDK_GraphicsJNI.SequentialSprite_setStop(this.swigCPtr, stop);
    }

    public boolean isStopped() {
        return NDK_GraphicsJNI.SequentialSprite_isStopped(this.swigCPtr);
    }

    public void setFrozen(boolean frozen) {
        NDK_GraphicsJNI.SequentialSprite_setFrozen(this.swigCPtr, frozen);
    }

    public boolean isFrozen() {
        return NDK_GraphicsJNI.SequentialSprite_isFrozen(this.swigCPtr);
    }

    public void setTexId(int resourceId) {
        NDK_GraphicsJNI.SequentialSprite_setTexId(this.swigCPtr, resourceId);
    }

    public int getTextureId() {
        return NDK_GraphicsJNI.SequentialSprite_getTextureId(this.swigCPtr);
    }

    public void render(float elapsedTime) {
        NDK_GraphicsJNI.SequentialSprite_render(this.swigCPtr, elapsedTime);
    }
}
