package com.camelgames.ndk.graphics;

public class TextureManager {
    public static final TextureManager instance;
    protected boolean swigCMemOwn;
    private int swigCPtr;

    static {
        TextureManager textureManager;
        int cPtr = NDK_GraphicsJNI.TextureManager_getInstance();
        if (cPtr == 0) {
            textureManager = null;
        } else {
            textureManager = new TextureManager(cPtr, false);
        }
        instance = textureManager;
    }

    public TextureManager(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(TextureManager obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_TextureManager(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public void bindTexture(int textureId) {
        NDK_GraphicsJNI.TextureManager_bindTexture(this.swigCPtr, textureId);
    }

    public void addTexture(Texture pTexture) {
        NDK_GraphicsJNI.TextureManager_addTexture(this.swigCPtr, Texture.getCPtr(pTexture));
    }

    public void removeTexture(int id) {
        NDK_GraphicsJNI.TextureManager_removeTexture(this.swigCPtr, id);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.Texture.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.Texture.<init>(int, int):void
      com.camelgames.ndk.graphics.Texture.<init>(int, boolean):void */
    public Texture getTexture(int id, boolean createIfNotLoaded) {
        int cPtr = NDK_GraphicsJNI.TextureManager_getTexture__SWIG_0(this.swigCPtr, id, createIfNotLoaded);
        if (cPtr == 0) {
            return null;
        }
        return new Texture(cPtr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.Texture.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.Texture.<init>(int, int):void
      com.camelgames.ndk.graphics.Texture.<init>(int, boolean):void */
    public Texture getTexture(int id) {
        int cPtr = NDK_GraphicsJNI.TextureManager_getTexture__SWIG_1(this.swigCPtr, id);
        if (cPtr == 0) {
            return null;
        }
        return new Texture(cPtr, false);
    }

    public void initiate(int width, int height) {
        NDK_GraphicsJNI.TextureManager_initiate(this.swigCPtr, width, height);
    }

    public void destory() {
        NDK_GraphicsJNI.TextureManager_destory(this.swigCPtr);
    }

    public static TextureManager getInstance() {
        return instance;
    }
}
