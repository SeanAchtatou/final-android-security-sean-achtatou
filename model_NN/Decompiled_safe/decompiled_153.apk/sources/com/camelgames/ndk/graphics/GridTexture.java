package com.camelgames.ndk.graphics;

public class GridTexture extends Texture {
    private int swigCPtr;

    protected GridTexture(int cPtr, boolean cMemoryOwn) {
        super(NDK_GraphicsJNI.SWIGGridTextureUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(GridTexture obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_GridTexture(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    public GridTexture(int id, int resourceId, int gridCount) {
        this(NDK_GraphicsJNI.new_GridTexture(id, resourceId, gridCount), true);
    }

    public int getGridWidth() {
        return NDK_GraphicsJNI.GridTexture_getGridWidth(this.swigCPtr);
    }

    public int getGridHeight() {
        return NDK_GraphicsJNI.GridTexture_getGridHeight(this.swigCPtr);
    }

    public int getGridCount() {
        return NDK_GraphicsJNI.GridTexture_getGridCount(this.swigCPtr);
    }

    public void setAltasTexCoords(int altasLeft, int altasTop, int altasWidth, int altasHeight) {
        NDK_GraphicsJNI.GridTexture_setAltasTexCoords(this.swigCPtr, altasLeft, altasTop, altasWidth, altasHeight);
    }
}
