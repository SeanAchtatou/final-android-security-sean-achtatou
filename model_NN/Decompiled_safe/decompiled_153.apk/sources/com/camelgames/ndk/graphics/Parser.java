package com.camelgames.ndk.graphics;

public class Parser {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected Parser(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(Parser obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_Parser(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public int parseDigits(float number, String digitChars, int capacity) {
        return NDK_GraphicsJNI.Parser_parseDigits(this.swigCPtr, number, digitChars, capacity);
    }
}
