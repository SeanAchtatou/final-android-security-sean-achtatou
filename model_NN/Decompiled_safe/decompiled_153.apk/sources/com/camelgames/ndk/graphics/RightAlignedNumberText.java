package com.camelgames.ndk.graphics;

public class RightAlignedNumberText extends NumberText {
    private int swigCPtr;

    protected RightAlignedNumberText(int cPtr, boolean cMemoryOwn) {
        super(NDK_GraphicsJNI.SWIGRightAlignedNumberTextUpcast(cPtr), cMemoryOwn);
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(RightAlignedNumberText obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_GraphicsJNI.delete_RightAlignedNumberText(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
        super.delete();
    }

    public RightAlignedNumberText(int capacityPara) {
        this(NDK_GraphicsJNI.new_RightAlignedNumberText(capacityPara), true);
    }

    public void setPosition(float right, float top) {
        NDK_GraphicsJNI.RightAlignedNumberText_setPosition(this.swigCPtr, right, top);
    }
}
