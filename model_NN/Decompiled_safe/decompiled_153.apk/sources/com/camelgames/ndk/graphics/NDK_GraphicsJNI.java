package com.camelgames.ndk.graphics;

class NDK_GraphicsJNI {
    public static final native void FixedNumberText_fixDigitsCount(int i, int i2);

    public static final native float FloatArray_get(int i, int i2);

    public static final native int FloatArray_getCount(int i);

    public static final native void FloatArray_set(int i, float f, int i2);

    public static final native int GridTexture_getGridCount(int i);

    public static final native int GridTexture_getGridHeight(int i);

    public static final native int GridTexture_getGridWidth(int i);

    public static final native void GridTexture_setAltasTexCoords(int i, int i2, int i3, int i4, int i5);

    public static final native void ITexture_bindTexture(int i);

    public static final native int ITexture_getAltasHeight(int i);

    public static final native int ITexture_getAltasLeft(int i);

    public static final native int ITexture_getAltasTop(int i);

    public static final native int ITexture_getAltasWidth(int i);

    public static final native int ITexture_getTextureId(int i);

    public static final native void LeftAlignedNumberText_setPosition(int i, float f, float f2);

    public static final native float NumberText_getScale(int i);

    public static final native void NumberText_initiate(int i, int i2, int i3, int i4, int i5);

    public static final native void NumberText_reSize(int i, int i2);

    public static final native void NumberText_render(int i);

    public static final native void NumberText_renderStaight(int i);

    public static final native void NumberText_setColor(int i, float f, float f2, float f3, float f4);

    public static final native void NumberText_setFloatParser(int i, char c, int i2);

    public static final native void NumberText_setNumber(int i, float f);

    public static final native void NumberText_setPosition(int i, float f, float f2);

    public static final native void NumberText_setScale(int i, float f);

    public static final native int OESSprite_InverseX_get();

    public static final native int OESSprite_InverseY_get();

    public static final native int OESSprite_NoInverse_get();

    public static final native void OESSprite_drawOES(int i);

    public static final native int OESSprite_getBottom(int i);

    public static final native int OESSprite_getCenterX(int i);

    public static final native int OESSprite_getCenterY(int i);

    public static final native int OESSprite_getHeight(int i);

    public static final native int OESSprite_getLeft(int i);

    public static final native int OESSprite_getRight(int i);

    public static final native float OESSprite_getScale(int i);

    public static final native int OESSprite_getTexture(int i);

    public static final native int OESSprite_getTextureId(int i);

    public static final native int OESSprite_getTop(int i);

    public static final native int OESSprite_getWidth(int i);

    public static final native boolean OESSprite_hitTest(int i, float f, float f2);

    public static final native void OESSprite_move(int i, int i2, int i3);

    public static final native void OESSprite_setAlpha(int i, float f);

    public static final native void OESSprite_setAltasTexCoords(int i, int i2, int i3, int i4, int i5);

    public static final native void OESSprite_setColor__SWIG_0(int i, float f);

    public static final native void OESSprite_setColor__SWIG_1(int i, float f, float f2, float f3);

    public static final native void OESSprite_setColor__SWIG_2(int i, float f, float f2, float f3, float f4);

    public static final native void OESSprite_setHeightConstrainProportion(int i, float f);

    public static final native void OESSprite_setLeftTop(int i, int i2, int i3);

    public static final native void OESSprite_setMappingType(int i, int i2);

    public static final native void OESSprite_setPosition(int i, int i2, int i3);

    public static final native void OESSprite_setRect(int i, int i2, int i3, int i4, int i5);

    public static final native void OESSprite_setScale(int i, float f);

    public static final native void OESSprite_setSize(int i, int i2, int i3);

    public static final native void OESSprite_setSizeByPixelScale(int i, float f);

    public static final native void OESSprite_setTexId(int i, int i2);

    public static final native void OESSprite_setWidthConstrainProportion(int i, float f);

    public static final native int Parser_parseDigits(int i, float f, String str, int i2);

    public static final native int ParticleSystem3D_DurationInfinity_get();

    public static final native int ParticleSystem3D_StartSizeEqualToEndSize_get();

    public static final native boolean ParticleSystem3D_addParticle(int i);

    public static final native void ParticleSystem3D_addTexture(int i, int i2);

    public static final native void ParticleSystem3D_clearTexture(int i);

    public static final native boolean ParticleSystem3D_isActive(int i);

    public static final native boolean ParticleSystem3D_isFull(int i);

    public static final native void ParticleSystem3D_reset(int i);

    public static final native void ParticleSystem3D_setActive(int i, boolean z);

    public static final native void ParticleSystem3D_setColor__SWIG_0(int i, int i2);

    public static final native void ParticleSystem3D_setColor__SWIG_1(int i, float f, float f2, float f3, float f4);

    public static final native void ParticleSystem3D_setDuration(int i, float f);

    public static final native void ParticleSystem3D_setEmissionRate(int i, float f);

    public static final native void ParticleSystem3D_setEndColor(int i, float f, float f2, float f3, float f4);

    public static final native void ParticleSystem3D_setGravity(int i, float f, float f2, float f3);

    public static final native void ParticleSystem3D_setLife(int i, float f, float f2);

    public static final native void ParticleSystem3D_setLinarSpeed(int i, float f, float f2);

    public static final native void ParticleSystem3D_setPosition(int i, float f, float f2, float f3, float f4, float f5, float f6);

    public static final native void ParticleSystem3D_setRotation(int i, float f, float f2);

    public static final native void ParticleSystem3D_setSize(int i, float f, float f2, float f3, float f4);

    public static final native void ParticleSystem3D_setSpin(int i, float f, float f2, float f3, float f4);

    public static final native void ParticleSystem3D_setStartColor(int i, float f, float f2, float f3, float f4);

    public static final native void ParticleSystem3D_setStartEndColorEqual(int i, boolean z);

    public static final native void ParticleSystem3D_setTexture(int i, int i2);

    public static final native void ParticleSystem3D_stop(int i);

    public static final native boolean ParticleSystem3D_update(int i, float f, float f2, float f3, float f4);

    public static final native int ParticleSystem_DurationInfinity_get();

    public static final native int ParticleSystem_StartSizeEqualToEndSize_get();

    public static final native boolean ParticleSystem_addParticle(int i);

    public static final native void ParticleSystem_addTexture(int i, int i2);

    public static final native void ParticleSystem_clearTexture(int i);

    public static final native boolean ParticleSystem_isActive(int i);

    public static final native boolean ParticleSystem_isFull(int i);

    public static final native void ParticleSystem_reset(int i);

    public static final native void ParticleSystem_setActive(int i, boolean z);

    public static final native void ParticleSystem_setColor__SWIG_0(int i, int i2);

    public static final native void ParticleSystem_setColor__SWIG_1(int i, float f, float f2, float f3, float f4);

    public static final native void ParticleSystem_setDuration(int i, float f);

    public static final native void ParticleSystem_setEmissionRate(int i, float f);

    public static final native void ParticleSystem_setEndColor(int i, float f, float f2, float f3, float f4);

    public static final native void ParticleSystem_setGravity(int i, float f, float f2);

    public static final native void ParticleSystem_setLife(int i, float f, float f2);

    public static final native void ParticleSystem_setLinarSpeed(int i, float f, float f2);

    public static final native void ParticleSystem_setPosition(int i, float f, float f2, float f3, float f4);

    public static final native void ParticleSystem_setRotation(int i, float f, float f2);

    public static final native void ParticleSystem_setSize(int i, float f, float f2, float f3, float f4);

    public static final native void ParticleSystem_setSpin(int i, float f, float f2, float f3, float f4);

    public static final native void ParticleSystem_setStartColor(int i, float f, float f2, float f3, float f4);

    public static final native void ParticleSystem_setTexture(int i, int i2);

    public static final native void ParticleSystem_stop(int i);

    public static final native boolean ParticleSystem_update(int i, float f);

    public static final native void RightAlignedNumberText_setPosition(int i, float f, float f2);

    public static final native int SWIGFixedNumberTextUpcast(int i);

    public static final native int SWIGGridTextureUpcast(int i);

    public static final native int SWIGLeftAlignedNumberTextUpcast(int i);

    public static final native int SWIGRightAlignedNumberTextUpcast(int i);

    public static final native int SWIGSequentialSpriteUpcast(int i);

    public static final native int SWIGSprite2DUpcast(int i);

    public static final native int SWIGTextureUpcast(int i);

    public static final native float ScaleSprite_getScale(int i);

    public static final native int ScaleSprite_getSprite(int i);

    public static final native boolean ScaleSprite_isFrozen(int i);

    public static final native boolean ScaleSprite_isStopped(int i);

    public static final native void ScaleSprite_render(int i, float f);

    public static final native void ScaleSprite_setBackward(int i, boolean z);

    public static final native void ScaleSprite_setChangeTime(int i, float f);

    public static final native void ScaleSprite_setCurrentScale(int i, float f);

    public static final native void ScaleSprite_setFold(int i, boolean z);

    public static final native void ScaleSprite_setFrozen(int i, boolean z);

    public static final native void ScaleSprite_setLoop(int i, boolean z);

    public static final native void ScaleSprite_setScaleInfo(int i, float f, float f2, float f3);

    public static final native void ScaleSprite_setSprite(int i, int i2);

    public static final native void ScaleSprite_setStop(int i, boolean z);

    public static final native int SequentialSprite_getCurrentFrame(int i);

    public static final native int SequentialSprite_getTextureId(int i);

    public static final native boolean SequentialSprite_isFrozen(int i);

    public static final native boolean SequentialSprite_isStopped(int i);

    public static final native void SequentialSprite_render(int i, float f);

    public static final native void SequentialSprite_setBackward(int i, boolean z);

    public static final native void SequentialSprite_setChangeTime(int i, float f);

    public static final native void SequentialSprite_setCurrentFrame(int i, int i2);

    public static final native void SequentialSprite_setFold(int i, boolean z);

    public static final native void SequentialSprite_setFrozen(int i, boolean z);

    public static final native void SequentialSprite_setHeightConstrainProportion(int i, float f);

    public static final native void SequentialSprite_setLoop(int i, boolean z);

    public static final native void SequentialSprite_setMaxFrame(int i, int i2);

    public static final native void SequentialSprite_setMinFrame(int i, int i2);

    public static final native void SequentialSprite_setSizeByPixelScale(int i, float f);

    public static final native void SequentialSprite_setStop(int i, boolean z);

    public static final native void SequentialSprite_setTexId(int i, int i2);

    public static final native void SequentialSprite_setWidthConstrainProportion(int i, float f);

    public static final native void Sprite2D_bindTexCoords(int i);

    public static final native void Sprite2D_bindVertices(int i);

    public static final native void Sprite2D_drawOES(int i);

    public static final native float Sprite2D_getBottom(int i);

    public static final native float Sprite2D_getHeight(int i);

    public static final native float Sprite2D_getLeft(int i);

    public static final native float Sprite2D_getRight(int i);

    public static final native int Sprite2D_getTextureId(int i);

    public static final native int Sprite2D_getTexutre(int i);

    public static final native float Sprite2D_getTop(int i);

    public static final native float Sprite2D_getWidth(int i);

    public static final native void Sprite2D_setBottom(int i, float f);

    public static final native void Sprite2D_setHeightConstrainProportion(int i, float f);

    public static final native void Sprite2D_setLeft(int i, float f);

    public static final native void Sprite2D_setLeftTop(int i, float f, float f2);

    public static final native void Sprite2D_setRight(int i, float f);

    public static final native void Sprite2D_setRightBottom(int i, float f, float f2);

    public static final native void Sprite2D_setSize(int i, float f, float f2);

    public static final native void Sprite2D_setSizeByPixelScale(int i, float f);

    public static final native void Sprite2D_setTexCoords(int i, float f, float f2, float f3, float f4);

    public static final native void Sprite2D_setTexId(int i, int i2);

    public static final native void Sprite2D_setTexture(int i, int i2);

    public static final native void Sprite2D_setTop(int i, float f);

    public static final native void Sprite2D_setWidthConstrainProportion(int i, float f);

    public static final native void Sprite2D_updateVertices(int i);

    public static final native void Sprite3D_alignBillboard(float f, float f2, float f3, float f4, float f5, float f6);

    public static final native int Sprite3D_getTextureId(int i);

    public static final native void Sprite3D_render(int i, float f);

    public static final native void Sprite3D_setColor__SWIG_0(int i, float f);

    public static final native void Sprite3D_setColor__SWIG_1(int i, float f, float f2, float f3, float f4);

    public static final native void Sprite3D_setHeightConstrainProportion(int i, float f);

    public static final native void Sprite3D_setPosition(int i, float f, float f2, float f3);

    public static final native void Sprite3D_setSize(int i, float f, float f2);

    public static final native void Sprite3D_setSizeByPixelScale(int i, float f);

    public static final native void Sprite3D_setTexId(int i, int i2);

    public static final native void Sprite3D_setWidthConstrainProportion(int i, float f);

    public static final native void Sprite3D_updateVertices(int i);

    public static final native float SpriteRects_getAngle(int i);

    public static final native float SpriteRects_getScale(int i);

    public static final native int SpriteRects_getTextureId(int i);

    public static final native float SpriteRects_getX(int i);

    public static final native float SpriteRects_getY(int i);

    public static final native void SpriteRects_move(int i, float f, float f2);

    public static final native void SpriteRects_render(int i, float f);

    public static final native void SpriteRects_rotate(int i, float f);

    public static final native void SpriteRects_setAngle(int i, float f);

    public static final native void SpriteRects_setColor__SWIG_0(int i, float f);

    public static final native void SpriteRects_setColor__SWIG_1(int i, float f, float f2, float f3, float f4);

    public static final native void SpriteRects_setPosition(int i, float f, float f2, float f3);

    public static final native void SpriteRects_setScale(int i, float f);

    public static final native void SpriteRects_setTexId(int i, int i2);

    public static final native void SpriteRects_setX(int i, float f);

    public static final native void SpriteRects_setY(int i, float f);

    public static final native int Sprite_InverseX_get();

    public static final native int Sprite_InverseY_get();

    public static final native int Sprite_NoInverse_get();

    public static final native void Sprite_bindTexture(int i);

    public static final native void Sprite_changeCapacity(int i);

    public static final native void Sprite_flush(boolean z);

    public static final native float Sprite_getAngle(int i);

    public static final native float Sprite_getScale(int i);

    public static final native float Sprite_getX(int i);

    public static final native float Sprite_getY(int i);

    public static final native int Sprite_mappingType_get(int i);

    public static final native void Sprite_mappingType_set(int i, int i2);

    public static final native void Sprite_move(int i, float f, float f2);

    public static final native void Sprite_render(int i, float f);

    public static final native void Sprite_rotate(int i, float f);

    public static final native void Sprite_set3D();

    public static final native void Sprite_setAlpha(int i, float f);

    public static final native void Sprite_setAngle(int i, float f);

    public static final native void Sprite_setColor__SWIG_0(int i, float f);

    public static final native void Sprite_setColor__SWIG_1(int i, float f, float f2, float f3);

    public static final native void Sprite_setColor__SWIG_2(int i, float f, float f2, float f3, float f4);

    public static final native void Sprite_setOffset__SWIG_0(int i, float f, float f2);

    public static final native void Sprite_setOffset__SWIG_1(int i, float f, float f2, float f3);

    public static final native void Sprite_setPosition__SWIG_0(int i, float f, float f2);

    public static final native void Sprite_setPosition__SWIG_1(int i, float f, float f2, float f3);

    public static final native void Sprite_setScale(int i, float f);

    public static final native void Sprite_setTexId(int i, int i2);

    public static final native void Sprite_setX(int i, float f);

    public static final native void Sprite_setY(int i, float f);

    public static final native int Text3d_getTextureId(int i);

    public static final native void Text3d_initiate(int i, int i2, float f, float f2);

    public static final native void Text3d_move(int i, float f, float f2, float f3);

    public static final native void Text3d_render(int i, float f);

    public static final native void Text3d_setChar(int i, char c, int i2);

    public static final native void Text3d_setColor__SWIG_0(int i, float f);

    public static final native void Text3d_setColor__SWIG_1(int i, float f, float f2, float f3, float f4);

    public static final native void Text3d_setNumber(int i, float f);

    public static final native void TextBuilder_bindTexture(int i);

    public static final native void TextBuilder_draw(int i, char c, int i2, int i3, int i4);

    public static final native int TextBuilder_getTexture(int i);

    public static final native void TextBuilder_setColumns(int i, int i2);

    public static final native void TextBuilder_setTextureResId(int i, int i2);

    public static final native void TextureManager_addTexture(int i, int i2);

    public static final native void TextureManager_bindTexture(int i, int i2);

    public static final native void TextureManager_destory(int i);

    public static final native int TextureManager_getInstance();

    public static final native int TextureManager_getTexture__SWIG_0(int i, int i2, boolean z);

    public static final native int TextureManager_getTexture__SWIG_1(int i, int i2);

    public static final native void TextureManager_initiate(int i, int i2, int i3);

    public static final native void TextureManager_removeTexture(int i, int i2);

    public static final native void Texture_bindTexture(int i);

    public static final native int Texture_getAltasHeight(int i);

    public static final native int Texture_getAltasLeft(int i);

    public static final native int Texture_getAltasTop(int i);

    public static final native int Texture_getAltasWidth(int i);

    public static final native float Texture_getBottomCoord(int i);

    public static final native int Texture_getHeight(int i);

    public static final native int Texture_getId(int i);

    public static final native float Texture_getLeftCoord(int i);

    public static final native int Texture_getResourceId(int i);

    public static final native float Texture_getRightCoord(int i);

    public static final native int Texture_getTextureId(int i);

    public static final native float Texture_getTopCoord(int i);

    public static final native int Texture_getWidth(int i);

    public static final native void Texture_initiate(int i, int i2);

    public static final native boolean Texture_isAltas(int i);

    public static final native void Texture_load(int i);

    public static final native void Texture_setAltasTexCoords(int i, int i2, int i3, int i4, int i5);

    public static final native void Texture_setGLPara(int i, float f, float f2, float f3, float f4);

    public static final native void Texture_setTexCoords(int i, float f, float f2, float f3, float f4);

    public static final native void Texture_textureLost(int i);

    public static final native void Texture_textureRest(int i);

    public static final native void Texture_unload(int i);

    public static final native void delete_FixedNumberText(int i);

    public static final native void delete_FloatArray(int i);

    public static final native void delete_GridTexture(int i);

    public static final native void delete_ITexture(int i);

    public static final native void delete_LeftAlignedNumberText(int i);

    public static final native void delete_NumberText(int i);

    public static final native void delete_OESSprite(int i);

    public static final native void delete_Parser(int i);

    public static final native void delete_ParticleSystem(int i);

    public static final native void delete_ParticleSystem3D(int i);

    public static final native void delete_RightAlignedNumberText(int i);

    public static final native void delete_ScaleSprite(int i);

    public static final native void delete_SequentialSprite(int i);

    public static final native void delete_Sprite(int i);

    public static final native void delete_Sprite2D(int i);

    public static final native void delete_Sprite3D(int i);

    public static final native void delete_SpriteRects(int i);

    public static final native void delete_Text3d(int i);

    public static final native void delete_TextBuilder(int i);

    public static final native void delete_Texture(int i);

    public static final native void delete_TextureManager(int i);

    public static final native int new_FixedNumberText(int i, int i2);

    public static final native int new_FloatArray(int i);

    public static final native int new_GridTexture(int i, int i2, int i3);

    public static final native int new_LeftAlignedNumberText(int i);

    public static final native int new_OESSprite();

    public static final native int new_ParticleSystem(int i);

    public static final native int new_ParticleSystem3D(int i);

    public static final native int new_RightAlignedNumberText(int i);

    public static final native int new_ScaleSprite__SWIG_0(int i);

    public static final native int new_ScaleSprite__SWIG_1();

    public static final native int new_SequentialSprite__SWIG_0(float f, float f2);

    public static final native int new_SequentialSprite__SWIG_1(float f);

    public static final native int new_SequentialSprite__SWIG_2();

    public static final native int new_Sprite2D__SWIG_0(float f, float f2);

    public static final native int new_Sprite2D__SWIG_1(float f);

    public static final native int new_Sprite2D__SWIG_2();

    public static final native int new_Sprite3D();

    public static final native int new_SpriteRects(int i, int i2, int i3);

    public static final native int new_Text3d(int i, char c);

    public static final native int new_TextBuilder(int i, int i2, int i3, int i4, int i5);

    public static final native int new_Texture(int i, int i2);

    NDK_GraphicsJNI() {
    }
}
