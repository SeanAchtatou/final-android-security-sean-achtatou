package com.camelgames.ndk.mesh;

public class SubMesh {
    public static final int componentsPerVertice = NDK_MeshJNI.SubMesh_componentsPerVertice_get();
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected SubMesh(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(SubMesh obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_MeshJNI.delete_SubMesh(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public SubMesh() {
        this(NDK_MeshJNI.new_SubMesh(), true);
    }

    public void render() {
        NDK_MeshJNI.SubMesh_render(this.swigCPtr);
    }

    public float getCenterX() {
        return NDK_MeshJNI.SubMesh_getCenterX(this.swigCPtr);
    }

    public float getCenterY() {
        return NDK_MeshJNI.SubMesh_getCenterY(this.swigCPtr);
    }

    public float getCenterZ() {
        return NDK_MeshJNI.SubMesh_getCenterZ(this.swigCPtr);
    }

    public int getSize() {
        return NDK_MeshJNI.SubMesh_getSize(this.swigCPtr);
    }

    public int getVerticesCount() {
        return NDK_MeshJNI.SubMesh_getVerticesCount(this.swigCPtr);
    }
}
