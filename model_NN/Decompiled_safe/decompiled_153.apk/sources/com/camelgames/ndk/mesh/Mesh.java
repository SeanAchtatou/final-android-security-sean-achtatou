package com.camelgames.ndk.mesh;

import com.camelgames.ndk.graphics.Texture;

public class Mesh {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected Mesh(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    public static int getCPtr(Mesh obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_MeshJNI.delete_Mesh(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public Mesh() {
        this(NDK_MeshJNI.new_Mesh(), true);
    }

    public void initiate(int modleResId, int textureResId, float scale) {
        NDK_MeshJNI.Mesh_initiate__SWIG_0(this.swigCPtr, modleResId, textureResId, scale);
    }

    public void initiate(int modleResId, int textureResId) {
        NDK_MeshJNI.Mesh_initiate__SWIG_1(this.swigCPtr, modleResId, textureResId);
    }

    public void render() {
        NDK_MeshJNI.Mesh_render(this.swigCPtr);
    }

    public void bindTexture() {
        NDK_MeshJNI.Mesh_bindTexture(this.swigCPtr);
    }

    public void setTextureResId(int resId) {
        NDK_MeshJNI.Mesh_setTextureResId(this.swigCPtr, resId);
    }

    public void setTexture(Texture pTexture) {
        NDK_MeshJNI.Mesh_setTexture(this.swigCPtr, Texture.getCPtr(pTexture));
    }

    public float getCenterX() {
        return NDK_MeshJNI.Mesh_getCenterX(this.swigCPtr);
    }

    public float getCenterY() {
        return NDK_MeshJNI.Mesh_getCenterY(this.swigCPtr);
    }

    public float getCenterZ() {
        return NDK_MeshJNI.Mesh_getCenterZ(this.swigCPtr);
    }

    public int getSubMeshCount() {
        return NDK_MeshJNI.Mesh_getSubMeshCount(this.swigCPtr);
    }

    public SubMesh getSubMesh(int index) {
        int cPtr = NDK_MeshJNI.Mesh_getSubMesh(this.swigCPtr, index);
        if (cPtr == 0) {
            return null;
        }
        return new SubMesh(cPtr, false);
    }

    public void combineSubMeshes(SubMesh pCombinedSubMesh, int start, int count) {
        NDK_MeshJNI.Mesh_combineSubMeshes(this.swigCPtr, SubMesh.getCPtr(pCombinedSubMesh), start, count);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.ndk.graphics.Texture.<init>(int, boolean):void
     arg types: [int, int]
     candidates:
      com.camelgames.ndk.graphics.Texture.<init>(int, int):void
      com.camelgames.ndk.graphics.Texture.<init>(int, boolean):void */
    public Texture getTexture() {
        int cPtr = NDK_MeshJNI.Mesh_getTexture(this.swigCPtr);
        if (cPtr == 0) {
            return null;
        }
        return new Texture(cPtr, false);
    }
}
