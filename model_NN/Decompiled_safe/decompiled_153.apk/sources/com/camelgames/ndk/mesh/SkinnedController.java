package com.camelgames.ndk.mesh;

public class SkinnedController {
    protected boolean swigCMemOwn;
    private int swigCPtr;

    protected SkinnedController(int cPtr, boolean cMemoryOwn) {
        this.swigCMemOwn = cMemoryOwn;
        this.swigCPtr = cPtr;
    }

    protected static int getCPtr(SkinnedController obj) {
        if (obj == null) {
            return 0;
        }
        return obj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public synchronized void delete() {
        if (this.swigCPtr != 0) {
            if (this.swigCMemOwn) {
                this.swigCMemOwn = false;
                NDK_MeshJNI.delete_SkinnedController(this.swigCPtr);
            }
            this.swigCPtr = 0;
        }
    }

    public SkinnedController() {
        this(NDK_MeshJNI.new_SkinnedController(), true);
    }

    public void initiate(int resId) {
        NDK_MeshJNI.SkinnedController_initiate(this.swigCPtr, resId);
    }

    public void animator(float elapsedTime, int location) {
        NDK_MeshJNI.SkinnedController_animator(this.swigCPtr, elapsedTime, location);
    }
}
