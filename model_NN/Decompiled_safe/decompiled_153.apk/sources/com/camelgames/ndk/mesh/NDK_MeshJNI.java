package com.camelgames.ndk.mesh;

class NDK_MeshJNI {
    public static final native void Mesh_bindTexture(int i);

    public static final native void Mesh_combineSubMeshes(int i, int i2, int i3, int i4);

    public static final native float Mesh_getCenterX(int i);

    public static final native float Mesh_getCenterY(int i);

    public static final native float Mesh_getCenterZ(int i);

    public static final native int Mesh_getSubMesh(int i, int i2);

    public static final native int Mesh_getSubMeshCount(int i);

    public static final native int Mesh_getTexture(int i);

    public static final native void Mesh_initiate__SWIG_0(int i, int i2, int i3, float f);

    public static final native void Mesh_initiate__SWIG_1(int i, int i2, int i3);

    public static final native void Mesh_render(int i);

    public static final native void Mesh_setTexture(int i, int i2);

    public static final native void Mesh_setTextureResId(int i, int i2);

    public static final native void SkinnedController_animator(int i, float f, int i2);

    public static final native void SkinnedController_initiate(int i, int i2);

    public static final native int SubMesh_componentsPerVertice_get();

    public static final native float SubMesh_getCenterX(int i);

    public static final native float SubMesh_getCenterY(int i);

    public static final native float SubMesh_getCenterZ(int i);

    public static final native int SubMesh_getSize(int i);

    public static final native int SubMesh_getVerticesCount(int i);

    public static final native void SubMesh_render(int i);

    public static final native void delete_Mesh(int i);

    public static final native void delete_SkinnedController(int i);

    public static final native void delete_SubMesh(int i);

    public static final native int new_Mesh();

    public static final native int new_SkinnedController();

    public static final native int new_SubMesh();

    NDK_MeshJNI() {
    }
}
