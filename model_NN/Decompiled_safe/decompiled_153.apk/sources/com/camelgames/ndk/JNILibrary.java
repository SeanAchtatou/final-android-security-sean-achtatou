package com.camelgames.ndk;

import android.graphics.Bitmap;
import android.opengl.GLUtils;
import com.camelgames.framework.graphics.textures.TextureUtility;
import com.camelgames.framework.math.Vector2;
import java.io.FileDescriptor;
import javax.microedition.khronos.opengles.GL10;

public class JNILibrary {
    public static GL10 gl;
    public static int screenHeight;
    public static int screenWidth;

    public static final native void appEnd();

    public static final native void appStart();

    public static final native void bindTexture(int i);

    public static final native void deviceReset(int i, int i2);

    public static final native void passNativeFileInfo(int i, FileDescriptor fileDescriptor, int i2, int i3);

    public static final native void setAnimationData(int[] iArr);

    public static final native void setTextureData(int[] iArr);

    public static final native void step(float f);

    public static int texImage2D(int resourceId) {
        Bitmap bitmap = TextureUtility.getBitmap(Integer.valueOf(resourceId));
        int size = (bitmap.getWidth() << 16) | bitmap.getHeight();
        GLUtils.texImage2D(3553, 0, bitmap, 0);
        bitmap.recycle();
        return size;
    }

    public static int getTexSize(int resourceId) {
        Vector2 sizeV = TextureUtility.getInstance().getTextureSize(Integer.valueOf(resourceId));
        return (((int) sizeV.X) << 16) | ((int) sizeV.Y);
    }
}
