package com.camelgames.blowuplite;

public final class R {

    public static final class anim {
        public static final int linear_interpolator = 2130968576;
        public static final int myanim = 2130968577;
    }

    public static final class array {
        public static final int altas1_arm1 = 2131165189;
        public static final int altas1_arm2 = 2131165188;
        public static final int altas1_body = 2131165200;
        public static final int altas1_bomb = 2131165197;
        public static final int altas1_bomb_big = 2131165198;
        public static final int altas1_bomb_config_bk = 2131165196;
        public static final int altas1_bomb_config_down = 2131165203;
        public static final int altas1_bomb_config_up = 2131165204;
        public static final int altas1_bomb_guard = 2131165201;
        public static final int altas1_bomb_magnet = 2131165202;
        public static final int altas1_digits = 2131165186;
        public static final int altas1_explode = 2131165191;
        public static final int altas1_font = 2131165184;
        public static final int altas1_frame = 2131165195;
        public static final int altas1_head = 2131165192;
        public static final int altas1_leg1 = 2131165206;
        public static final int altas1_leg2 = 2131165205;
        public static final int altas1_mainmenu_bottom = 2131165185;
        public static final int altas1_particle = 2131165190;
        public static final int altas1_pause = 2131165187;
        public static final int altas1_record = 2131165194;
        public static final int altas1_switch = 2131165199;
        public static final int altas1_target = 2131165193;
        public static final int altas4_board = 2131165208;
        public static final int altas4_nextlevel = 2131165211;
        public static final int altas4_playagain = 2131165212;
        public static final int altas4_quit = 2131165210;
        public static final int altas4_scoreboard = 2131165207;
        public static final int altas4_soundoff = 2131165214;
        public static final int altas4_soundon = 2131165213;
        public static final int altas4_star = 2131165209;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int altas1 = 2130837504;
        public static final int altas4 = 2130837505;
        public static final int arrow = 2130837506;
        public static final int background = 2130837507;
        public static final int board = 2130837508;
        public static final int bricks = 2130837509;
        public static final int button_back = 2130837510;
        public static final int button_back0 = 2130837511;
        public static final int button_back1 = 2130837512;
        public static final int button_easy = 2130837513;
        public static final int button_hard = 2130837514;
        public static final int button_left = 2130837515;
        public static final int button_left0 = 2130837516;
        public static final int button_left1 = 2130837517;
        public static final int button_levelselect = 2130837518;
        public static final int button_levelselect0 = 2130837519;
        public static final int button_levelselect1 = 2130837520;
        public static final int button_medium = 2130837521;
        public static final int button_moregames = 2130837522;
        public static final int button_moregames0 = 2130837523;
        public static final int button_moregames1 = 2130837524;
        public static final int button_nextlevel0 = 2130837525;
        public static final int button_play = 2130837526;
        public static final int button_play0 = 2130837527;
        public static final int button_play1 = 2130837528;
        public static final int button_playagain0 = 2130837529;
        public static final int button_quit0 = 2130837530;
        public static final int button_ranking = 2130837531;
        public static final int button_ranking0 = 2130837532;
        public static final int button_ranking1 = 2130837533;
        public static final int button_right = 2130837534;
        public static final int button_right0 = 2130837535;
        public static final int button_right1 = 2130837536;
        public static final int button_soundoff = 2130837537;
        public static final int button_soundon = 2130837538;
        public static final int divider = 2130837539;
        public static final int icon = 2130837540;
        public static final int line_knife_dash = 2130837541;
        public static final int line_starwar = 2130837542;
        public static final int line_starwar_dash = 2130837543;
        public static final int logo = 2130837544;
        public static final int magnent_explode = 2130837545;
        public static final int mainmenu_top = 2130837546;
        public static final int pflx_sdk_adbar_arrow = 2130837547;
        public static final int pflx_sdk_adbar_background = 2130837548;
        public static final int pflx_sdk_divider = 2130837549;
        public static final int questionmark = 2130837550;
        public static final int selector = 2130837551;
        public static final int star = 2130837552;
    }

    public static final class id {
        public static final int adview = 2131361793;
        public static final int body_text = 2131361829;
        public static final int button_back = 2131361803;
        public static final int button_easy = 2131361797;
        public static final int button_hard = 2131361799;
        public static final int button_layout = 2131361840;
        public static final int button_left = 2131361801;
        public static final int button_medium = 2131361798;
        public static final int button_right = 2131361802;
        public static final int difficulties = 2131361847;
        public static final int easy = 2131361848;
        public static final int footer = 2131361825;
        public static final int gl_screen_view = 2131361792;
        public static final int grid = 2131361796;
        public static final int hard = 2131361850;
        public static final int header = 2131361826;
        public static final int header_image = 2131361827;
        public static final int info_text = 2131361822;
        public static final int levelselect_button = 2131361807;
        public static final int levelselector_view = 2131361810;
        public static final int list_body_text = 2131361837;
        public static final int list_header_text = 2131361835;
        public static final int list_offer_checkbox = 2131361836;
        public static final int list_offer_image = 2131361834;
        public static final int listview_layout = 2131361839;
        public static final int mainmenu_bottom = 2131361812;
        public static final int mainmenu_buttons = 2131361805;
        public static final int mainmenu_logo = 2131361813;
        public static final int mainmenu_star = 2131361814;
        public static final int mainmenu_top = 2131361811;
        public static final int mainmenu_view = 2131361804;
        public static final int medium = 2131361849;
        public static final int moregames_button = 2131361809;
        public static final int offer_header_text = 2131361828;
        public static final int pflx_close_policies_button = 2131361843;
        public static final int pflx_confirm_button = 2131361821;
        public static final int pflx_decline_text = 2131361820;
        public static final int pflx_logo = 2131361817;
        public static final int pflx_multi_offers_layout = 2131361823;
        public static final int pflx_multi_privacy_policy = 2131361824;
        public static final int pflx_offer_info_text = 2131361831;
        public static final int pflx_offer_privacy_policy = 2131361832;
        public static final int pflx_offer_text1 = 2131361818;
        public static final int pflx_offer_text2 = 2131361819;
        public static final int pflx_offer_top = 2131361833;
        public static final int pflx_other_policies_button = 2131361844;
        public static final int pflx_policy_button_layout = 2131361842;
        public static final int pflx_policy_list_select_button = 2131361838;
        public static final int pflx_privacy_policy_webview = 2131361841;
        public static final int play_button = 2131361806;
        public static final int privacy_text = 2131361845;
        public static final int ranking_button = 2131361808;
        public static final int registration_fields_layout = 2131361830;
        public static final int scoreContainer = 2131361851;
        public static final int scoreTable = 2131361852;
        public static final int selector = 2131361800;
        public static final int soundoff = 2131361816;
        public static final int soundon = 2131361815;
        public static final int subwindow_container = 2131361846;
        public static final int username_edit = 2131361795;
        public static final int username_view = 2131361794;
    }

    public static final class layout {
        public static final int glview = 2130903040;
        public static final int inputusername = 2130903041;
        public static final int levelselector_view = 2130903042;
        public static final int mainmenu_view = 2130903043;
        public static final int pflx_sdk_adview = 2130903044;
        public static final int pflx_sdk_dialog_footer = 2130903045;
        public static final int pflx_sdk_info_text = 2130903046;
        public static final int pflx_sdk_multi_offer = 2130903047;
        public static final int pflx_sdk_offer_confirmation = 2130903048;
        public static final int pflx_sdk_offer_list_item = 2130903049;
        public static final int pflx_sdk_privacy_list = 2130903050;
        public static final int pflx_sdk_privacy_policy = 2130903051;
        public static final int pflx_sdk_privacy_text = 2130903052;
        public static final int scorelist = 2130903053;
    }

    public static final class raw {
        public static final int attach_ragdoll = 2131099648;
        public static final int attach_stick = 2131099649;
        public static final int bomb = 2131099650;
        public static final int button = 2131099651;
        public static final int capture_bomb = 2131099652;
        public static final int large_bomb = 2131099653;
        public static final int lost = 2131099654;
        public static final int message = 2131099655;
        public static final int panda_die = 2131099656;
        public static final int press_switch = 2131099657;
        public static final int tick = 2131099658;
        public static final int touchstar = 2131099659;
        public static final int win = 2131099660;
    }

    public static final class string {
        public static final int app_name = 2131296258;
        public static final int con = 2131296265;
        public static final int inputchar = 2131296257;
        public static final int more_games_link = 2131296256;
        public static final int name = 2131296259;
        public static final int no_network = 2131296260;
        public static final int out_of_service = 2131296261;
        public static final int ranking_name = 2131296264;
        public static final int ranking_submit = 2131296263;
        public static final int ranking_title = 2131296262;
    }

    public static final class style {
        public static final int PontiflexLandscapeMargin = 2131230720;
        public static final int PontiflexLandscapePadding = 2131230721;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }

    public static final class xml {
        public static final int level1 = 2131034112;
        public static final int level10 = 2131034113;
        public static final int level11 = 2131034114;
        public static final int level12 = 2131034115;
        public static final int level13 = 2131034116;
        public static final int level14 = 2131034117;
        public static final int level15 = 2131034118;
        public static final int level16 = 2131034119;
        public static final int level17 = 2131034120;
        public static final int level2 = 2131034121;
        public static final int level3 = 2131034122;
        public static final int level4 = 2131034123;
        public static final int level5 = 2131034124;
        public static final int level6 = 2131034125;
        public static final int level7 = 2131034126;
        public static final int level8 = 2131034127;
        public static final int level81 = 2131034128;
        public static final int level9 = 2131034129;
    }
}
