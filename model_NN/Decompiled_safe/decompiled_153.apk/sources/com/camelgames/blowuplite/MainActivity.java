package com.camelgames.blowuplite;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import com.camelgames.explode.GLScreenView;

public class MainActivity extends Activity {
    private Handler handler = new Handler();
    private GLScreenView mainScreen;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(128, 128);
        setContentView((int) R.layout.glview);
        this.mainScreen = (GLScreenView) findViewById(R.id.gl_screen_view);
        setVolumeControlStream(3);
    }

    public Handler getHandler() {
        return this.handler;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mainScreen.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mainScreen.onPause();
    }
}
