package com.camelgames.explode.server.serializable;

import java.io.Serializable;
import java.util.Date;

public class ScoreBoardInfo implements Serializable {
    public Item currentUserItem;
    public int currentUserSequence;
    public Item[] items;
    public int totalSequence;

    public static class Item implements Serializable {
        public String comments;
        public int levelFinished;
        public int score;
        public Date updatedTime;
        public String userName;
    }
}
