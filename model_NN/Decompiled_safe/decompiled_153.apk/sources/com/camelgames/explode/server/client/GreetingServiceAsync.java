package com.camelgames.explode.server.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface GreetingServiceAsync {
    void greetServer(String str, AsyncCallback<String> asyncCallback);
}
