package com.camelgames.explode.server.serializable;

import java.io.Serializable;

public class Scores implements Serializable {
    public int[] codes;
    public String comments;
    public String userId;
    public String userName;
}
