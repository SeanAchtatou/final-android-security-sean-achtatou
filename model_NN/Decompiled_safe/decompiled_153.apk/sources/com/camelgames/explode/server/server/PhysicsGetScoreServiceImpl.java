package com.camelgames.explode.server.server;

import com.camelgames.explode.server.serializable.ScoreBoardInfo;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PhysicsGetScoreServiceImpl extends RemoteServiceServlet {
    private final int topCount = 50;

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String userId = req.getParameter("userid");
        int difficulty = 0;
        try {
            difficulty = Integer.parseInt(req.getParameter("difficulty"));
        } catch (Exception e) {
        }
        PersistenceManager pm = PMF.get().getPersistenceManager();
        try {
            resp.setContentType("text/plain");
            Query query = pm.newQuery(ScoreRecord.class);
            query.setFilter("difficulty == " + difficulty);
            query.setOrdering("levelFinished desc, score desc, updatedTime asc");
            query.setRange(0, 50);
            List<ScoreRecord> queryRecords = (List) query.execute();
            ArrayList<ScoreBoardInfo.Item> items = new ArrayList<>();
            ScoreBoardInfo.Item currentUserItem = null;
            int sequence = -1;
            for (int i = 0; i < queryRecords.size(); i++) {
                ScoreRecord scoreRecord = (ScoreRecord) queryRecords.get(i);
                if (scoreRecord.getScore() > 0) {
                    UserRecord userRecord = getUserRecord(pm, scoreRecord.getUserId());
                    if (userRecord != null) {
                        items.add(composeItem(scoreRecord, userRecord));
                    }
                    if (scoreRecord.getUserId().equals(userId)) {
                        currentUserItem = composeItem(scoreRecord, getUserRecord(pm, scoreRecord.getUserId()));
                        sequence = i;
                        if (i >= 50) {
                            break;
                        }
                    } else {
                        continue;
                    }
                }
            }
            ScoreBoardInfo scoreBoardInfo = new ScoreBoardInfo();
            if (items.size() > 0) {
                scoreBoardInfo.items = new ScoreBoardInfo.Item[items.size()];
                items.toArray(scoreBoardInfo.items);
            }
            scoreBoardInfo.currentUserItem = currentUserItem;
            scoreBoardInfo.currentUserSequence = sequence;
            scoreBoardInfo.totalSequence = 13657;
            new ObjectOutputStream(resp.getOutputStream()).writeObject(scoreBoardInfo);
        } catch (Exception e2) {
        } finally {
            pm.close();
        }
    }

    private UserRecord getUserRecord(PersistenceManager pm, String userId) {
        return (UserRecord) pm.getObjectById(UserRecord.class, userId);
    }

    private ScoreBoardInfo.Item composeItem(ScoreRecord scoreRecord, UserRecord userRecord) {
        ScoreBoardInfo.Item item = new ScoreBoardInfo.Item();
        item.userName = userRecord.getUserName();
        item.comments = userRecord.getComments();
        item.score = scoreRecord.getScore();
        item.levelFinished = scoreRecord.getLevelFinished();
        item.updatedTime = scoreRecord.getUpdatedTime();
        return item;
    }
}
