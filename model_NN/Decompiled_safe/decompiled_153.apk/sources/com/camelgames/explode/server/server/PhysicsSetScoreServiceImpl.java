package com.camelgames.explode.server.server;

import com.camelgames.explode.server.serializable.ScoreStorage;
import com.camelgames.explode.server.serializable.Scores;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Date;
import java.util.List;
import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

public class PhysicsSetScoreServiceImpl extends RemoteServiceServlet {
    public static Long FLIGHTDIRECTOR = 3L;

    /* access modifiers changed from: protected */
    public String readContent(HttpServletRequest request) throws ServletException, IOException {
        try {
            Scores scores = (Scores) new ObjectInputStream(request.getInputStream()).readObject();
            save(scores.userId, scores.userName, scores.comments, scores.codes);
            return "OK";
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return "OK";
        }
    }

    public String processCall(String payload) {
        return payload;
    }

    private void save(String userId, String userName, String comments, int[] codes) {
        PersistenceManager pm = PMF.get().getPersistenceManager();
        try {
            UserRecord userRecord = getUserRecord(pm, userId);
            if (userRecord == null) {
                userRecord = new UserRecord(userId, userName, FLIGHTDIRECTOR, new Date());
            }
            userRecord.setUserName(userName);
            userRecord.setComments(comments);
            pm.makePersistent(userRecord);
            updateScoreByDifficulty(pm, userId, codes, 0);
            updateScoreByDifficulty(pm, userId, codes, 1);
            updateScoreByDifficulty(pm, userId, codes, 2);
        } catch (Exception e) {
        } finally {
            pm.close();
        }
    }

    private void updateScoreByDifficulty(PersistenceManager pm, String userId, int[] codes, int difficulty) {
        setScore(pm, userId, difficulty, ScoreStorage.getFinishedCount(codes, difficulty), ScoreStorage.getTotalScores(codes, difficulty));
    }

    private void setScore(PersistenceManager pm, String userId, int difficulty, int levelFinished, int score) {
        ScoreRecord scoreRecord = getScoreRecord(pm, userId, difficulty);
        if (scoreRecord == null) {
            pm.makePersistent(new ScoreRecord(levelFinished, difficulty, userId, score, new Date()));
            return;
        }
        boolean needSave = false;
        if (scoreRecord.getLevelFinished() < levelFinished) {
            scoreRecord.setLevelFinished(levelFinished);
            needSave = true;
        }
        if (scoreRecord.getScore() < score) {
            scoreRecord.setScore(score);
            needSave = true;
        }
        if (needSave) {
            scoreRecord.setUpdatedTime(new Date());
            pm.makePersistent(scoreRecord);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    private UserRecord getUserRecord(PersistenceManager pm, String userId) {
        List<UserRecord> queryRecords = (List) pm.newQuery("select from  " + UserRecord.class.getName() + " where userId == \"" + userId + "\"").execute();
        if (queryRecords.size() > 0) {
            return (UserRecord) queryRecords.get(0);
        }
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    private ScoreRecord getScoreRecord(PersistenceManager pm, String userId, int difficulty) {
        List<ScoreRecord> queryRecords = (List) pm.newQuery("select from  " + ScoreRecord.class.getName() + " where userId == \"" + userId + "\"" + " &&  difficulty == " + difficulty).execute();
        if (queryRecords.size() > 0) {
            return (ScoreRecord) queryRecords.get(0);
        }
        return null;
    }
}
