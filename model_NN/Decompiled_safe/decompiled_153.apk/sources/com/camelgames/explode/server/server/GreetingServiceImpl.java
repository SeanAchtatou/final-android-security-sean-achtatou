package com.camelgames.explode.server.server;

import com.camelgames.explode.server.client.GreetingService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {
    public String greetServer(String input) {
        String serverInfo = getServletContext().getServerInfo();
        return "Hello, " + input + "!<br><br>I am running " + serverInfo + ".<br><br>It looks like you are using:<br>" + getThreadLocalRequest().getHeader("User-Agent");
    }
}
