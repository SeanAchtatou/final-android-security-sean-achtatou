package com.camelgames.explode.server.serializable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class ScoreStorage implements Serializable {
    private ArrayList<Integer> codes = new ArrayList<>();
    private String comments = "";
    public int dummy;
    private boolean isModified;
    private String userId;
    private String userName;

    public static int composeCode(int levelIndex, int difficulty, int score) {
        return (difficulty << 28) | (levelIndex << 16) | score;
    }

    public static int getDifficulty(int code) {
        return (code >>> 28) & 15;
    }

    public static int getLevelId(int code) {
        return (code << 4) >>> 20;
    }

    public static int getScore(int code) {
        return (code << 16) >>> 16;
    }

    public void setUserId(String userId2) {
        this.userId = userId2;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserName(String userName2) {
        this.userName = userName2;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setComments(String comments2) {
        this.comments = comments2;
        this.isModified = true;
    }

    public String getComments() {
        return this.comments;
    }

    public void setModified(boolean isModified2) {
        this.isModified = isModified2;
    }

    public boolean isModified() {
        return this.isModified;
    }

    public boolean updateScore(int levelIndex, int difficulty, int score) {
        if (score <= 0) {
            return false;
        }
        int recoredIndex = getRecoredIndex(levelIndex, difficulty);
        if (recoredIndex < 0) {
            this.codes.add(Integer.valueOf(composeCode(levelIndex, difficulty, score)));
            this.isModified = true;
            return true;
        } else if (getScore(this.codes.get(recoredIndex).intValue()) >= score) {
            return false;
        } else {
            this.codes.set(recoredIndex, Integer.valueOf(composeCode(levelIndex, difficulty, score)));
            this.isModified = true;
            return true;
        }
    }

    private int getRecoredIndex(int levelIndex, int difficulty) {
        for (int i = 0; i < this.codes.size(); i++) {
            int code = this.codes.get(i).intValue();
            int thisLevelId = getLevelId(code);
            int thisDifficulty = getDifficulty(code);
            if (thisLevelId == levelIndex && thisDifficulty == difficulty) {
                return i;
            }
        }
        return -1;
    }

    public int getFinishedCount(int difficulty) {
        int count = 0;
        Iterator<Integer> it = this.codes.iterator();
        while (it.hasNext()) {
            int code = it.next().intValue();
            if (isThisDifficulty(difficulty, code) && getScore(code) > 0) {
                count++;
            }
        }
        return count;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public int getScore(int levelIndex, int difficulty) {
        int index = getRecoredIndex(levelIndex, difficulty);
        if (index < 0) {
            return 0;
        }
        return getScore(this.codes.get(index).intValue());
    }

    public int getTotalScore(int difficulty) {
        int totalScore = 0;
        Iterator<Integer> it = this.codes.iterator();
        while (it.hasNext()) {
            int code = it.next().intValue();
            if (isThisDifficulty(difficulty, code)) {
                totalScore += getScore(code);
            }
        }
        return totalScore;
    }

    public int[] getCodes() {
        int[] codesArray = new int[this.codes.size()];
        for (int i = 0; i < this.codes.size(); i++) {
            codesArray[i] = this.codes.get(i).intValue();
        }
        return codesArray;
    }

    public static int getFinishedCount(int[] codes2, int difficulty) {
        int count = 0;
        for (int code : codes2) {
            if (isThisDifficulty(difficulty, code) && getScore(code) > 0) {
                count++;
            }
        }
        return count;
    }

    public static int getTotalScores(int[] codes2, int difficulty) {
        int totalScore = 0;
        for (int code : codes2) {
            if (isThisDifficulty(difficulty, code)) {
                totalScore += getScore(code);
            }
        }
        return totalScore;
    }

    private static boolean isThisDifficulty(int difficulty, int code) {
        return getDifficulty(code) == difficulty;
    }
}
