package com.camelgames.explode.server.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import java.io.IOException;
import java.util.Collection;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PhysicsResetScoreServiceImpl extends RemoteServiceServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PersistenceManager pm = PMF.get().getPersistenceManager();
        String result = "reseted";
        try {
            pm.deletePersistentAll((Collection) pm.newQuery("select from  " + UserRecord.class.getName()).execute());
            pm.deletePersistentAll((Collection) pm.newQuery("select from  " + ScoreRecord.class.getName()).execute());
        } catch (Exception e) {
            result = e.toString();
        } finally {
            pm.close();
        }
        resp.setContentType("text/plain");
        resp.getWriter().println(result);
    }
}
