package com.camelgames.explode.server.server;

import com.google.appengine.api.datastore.Key;
import java.util.Date;
import javax.jdo.JDOFatalInternalException;
import javax.jdo.PersistenceManager;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.identity.ObjectIdentity;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.StateManager;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class ScoreRecord implements javax.jdo.spi.PersistenceCapable {
    private static final byte[] jdoFieldFlags = __jdoFieldFlagsInit();
    private static final String[] jdoFieldNames = __jdoFieldNamesInit();
    private static final Class[] jdoFieldTypes = __jdoFieldTypesInit();
    private static final int jdoInheritedFieldCount = __jdoGetInheritedFieldCount();
    private static final Class jdoPersistenceCapableSuperclass = __jdoPersistenceCapableSuperclassInit();
    @Persistent
    private int difficulty;
    protected transient byte jdoFlags;
    protected transient StateManager jdoStateManager;
    @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
    @PrimaryKey
    private Key key;
    @Persistent
    private int levelFinished;
    @Persistent
    private int score;
    @Persistent
    private Date updatedTime;
    @Persistent
    private String userId;

    static {
        JDOImplHelper.registerClass(___jdo$loadClass("com.camelgames.explode.server.server.ScoreRecord"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new ScoreRecord());
    }

    protected ScoreRecord() {
    }

    private static final byte[] __jdoFieldFlagsInit() {
        return new byte[]{21, 24, 21, 21, 21, 21};
    }

    private static final String[] __jdoFieldNamesInit() {
        return new String[]{"difficulty", "key", "levelFinished", "score", "updatedTime", "userId"};
    }

    private static final Class[] __jdoFieldTypesInit() {
        return new Class[]{Integer.TYPE, ___jdo$loadClass("com.google.appengine.api.datastore.Key"), Integer.TYPE, Integer.TYPE, ___jdo$loadClass("java.util.Date"), ___jdo$loadClass("java.lang.String")};
    }

    protected static int __jdoGetInheritedFieldCount() {
        return 0;
    }

    private static Class __jdoPersistenceCapableSuperclassInit() {
        return null;
    }

    protected static int jdoGetManagedFieldCount() {
        return 6;
    }

    private static int jdoGetdifficulty(ScoreRecord objPC) {
        return (objPC.jdoFlags <= 0 || objPC.jdoStateManager == null || objPC.jdoStateManager.isLoaded(objPC, 0)) ? objPC.difficulty : objPC.jdoStateManager.getIntField(objPC, 0, objPC.difficulty);
    }

    private static Key jdoGetkey(ScoreRecord objPC) {
        return objPC.key;
    }

    private static int jdoGetlevelFinished(ScoreRecord objPC) {
        return (objPC.jdoFlags <= 0 || objPC.jdoStateManager == null || objPC.jdoStateManager.isLoaded(objPC, 2)) ? objPC.levelFinished : objPC.jdoStateManager.getIntField(objPC, 2, objPC.levelFinished);
    }

    private static int jdoGetscore(ScoreRecord objPC) {
        return (objPC.jdoFlags <= 0 || objPC.jdoStateManager == null || objPC.jdoStateManager.isLoaded(objPC, 3)) ? objPC.score : objPC.jdoStateManager.getIntField(objPC, 3, objPC.score);
    }

    private static Date jdoGetupdatedTime(ScoreRecord objPC) {
        return (objPC.jdoFlags <= 0 || objPC.jdoStateManager == null || objPC.jdoStateManager.isLoaded(objPC, 4)) ? objPC.updatedTime : (Date) objPC.jdoStateManager.getObjectField(objPC, 4, objPC.updatedTime);
    }

    private static String jdoGetuserId(ScoreRecord objPC) {
        return (objPC.jdoFlags <= 0 || objPC.jdoStateManager == null || objPC.jdoStateManager.isLoaded(objPC, 5)) ? objPC.userId : objPC.jdoStateManager.getStringField(objPC, 5, objPC.userId);
    }

    private static void jdoSetdifficulty(ScoreRecord objPC, int val) {
        if (objPC.jdoFlags == 0 || objPC.jdoStateManager == null) {
            objPC.difficulty = val;
        } else {
            objPC.jdoStateManager.setIntField(objPC, 0, objPC.difficulty, val);
        }
    }

    private static void jdoSetkey(ScoreRecord objPC, Key val) {
        if (objPC.jdoStateManager == null) {
            objPC.key = val;
        } else {
            objPC.jdoStateManager.setObjectField(objPC, 1, objPC.key, val);
        }
    }

    private static void jdoSetlevelFinished(ScoreRecord objPC, int val) {
        if (objPC.jdoFlags == 0 || objPC.jdoStateManager == null) {
            objPC.levelFinished = val;
        } else {
            objPC.jdoStateManager.setIntField(objPC, 2, objPC.levelFinished, val);
        }
    }

    private static void jdoSetscore(ScoreRecord objPC, int val) {
        if (objPC.jdoFlags == 0 || objPC.jdoStateManager == null) {
            objPC.score = val;
        } else {
            objPC.jdoStateManager.setIntField(objPC, 3, objPC.score, val);
        }
    }

    private static void jdoSetupdatedTime(ScoreRecord objPC, Date val) {
        if (objPC.jdoFlags == 0 || objPC.jdoStateManager == null) {
            objPC.updatedTime = val;
        } else {
            objPC.jdoStateManager.setObjectField(objPC, 4, objPC.updatedTime, val);
        }
    }

    private static void jdoSetuserId(ScoreRecord objPC, String val) {
        if (objPC.jdoFlags == 0 || objPC.jdoStateManager == null) {
            objPC.userId = val;
        } else {
            objPC.jdoStateManager.setStringField(objPC, 5, objPC.userId, val);
        }
    }

    private Object jdoSuperClone() throws CloneNotSupportedException {
        ScoreRecord o = (ScoreRecord) super.clone();
        o.jdoFlags = 0;
        o.jdoStateManager = null;
        return o;
    }

    /* access modifiers changed from: protected */
    public final void jdoCopyField(ScoreRecord obj, int index) {
        switch (index) {
            case 0:
                this.difficulty = obj.difficulty;
                return;
            case 1:
                this.key = obj.key;
                return;
            case 2:
                this.levelFinished = obj.levelFinished;
                return;
            case 3:
                this.score = obj.score;
                return;
            case 4:
                this.updatedTime = obj.updatedTime;
                return;
            case 5:
                this.userId = obj.userId;
                return;
            default:
                throw new IllegalArgumentException(new StringBuffer("out of field index :").append(index).toString());
        }
    }

    public void jdoCopyFields(Object obj, int[] indices) {
        if (this.jdoStateManager == null) {
            throw new IllegalStateException("state manager is null");
        } else if (indices == null) {
            throw new IllegalStateException("fieldNumbers is null");
        } else if (!(obj instanceof ScoreRecord)) {
            throw new IllegalArgumentException("object is not an object of type com.camelgames.explode.server.server.ScoreRecord");
        } else {
            ScoreRecord other = (ScoreRecord) obj;
            if (this.jdoStateManager != other.jdoStateManager) {
                throw new IllegalArgumentException("state managers do not match");
            }
            int i = indices.length - 1;
            if (i >= 0) {
                do {
                    jdoCopyField(other, indices[i]);
                    i--;
                } while (i >= 0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void jdoCopyKeyFieldsFromObjectId(Object oid) {
        if (!(oid instanceof ObjectIdentity)) {
            throw new ClassCastException("key class is not javax.jdo.identity.ObjectIdentity or null");
        }
        this.key = (Key) ((ObjectIdentity) oid).getKey();
    }

    public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer fc, Object oid) {
        if (fc == null) {
            throw new IllegalArgumentException("ObjectIdFieldConsumer is null");
        } else if (!(oid instanceof ObjectIdentity)) {
            throw new ClassCastException("oid is not instanceof javax.jdo.identity.ObjectIdentity");
        } else {
            fc.storeObjectField(1, ((ObjectIdentity) oid).getKey());
        }
    }

    public final void jdoCopyKeyFieldsToObjectId(Object oid) {
        throw new JDOFatalInternalException("It's illegal to call jdoCopyKeyFieldsToObjectId for a class with SingleFieldIdentity.");
    }

    public final void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier fs, Object obj) {
        throw new JDOFatalInternalException("It's illegal to call jdoCopyKeyFieldsToObjectId for a class with SingleFieldIdentity.");
    }

    public final Object jdoGetObjectId() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.getObjectId(this);
        }
        return null;
    }

    public final PersistenceManager jdoGetPersistenceManager() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.getPersistenceManager(this);
        }
        return null;
    }

    public final Object jdoGetTransactionalObjectId() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.getTransactionalObjectId(this);
        }
        return null;
    }

    public final Object jdoGetVersion() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.getVersion(this);
        }
        return null;
    }

    public final boolean jdoIsDeleted() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.isDeleted(this);
        }
        return false;
    }

    public boolean jdoIsDetached() {
        return false;
    }

    public final boolean jdoIsDirty() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.isDirty(this);
        }
        return false;
    }

    public final boolean jdoIsNew() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.isNew(this);
        }
        return false;
    }

    public final boolean jdoIsPersistent() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.isPersistent(this);
        }
        return false;
    }

    public final boolean jdoIsTransactional() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.isTransactional(this);
        }
        return false;
    }

    public void jdoMakeDirty(String fieldName) {
        if (this.jdoStateManager != null) {
            this.jdoStateManager.makeDirty(this, fieldName);
        }
    }

    public javax.jdo.spi.PersistenceCapable jdoNewInstance(StateManager sm) {
        ScoreRecord result = new ScoreRecord();
        result.jdoFlags = 1;
        result.jdoStateManager = sm;
        return result;
    }

    public javax.jdo.spi.PersistenceCapable jdoNewInstance(StateManager sm, Object obj) {
        ScoreRecord result = new ScoreRecord();
        result.jdoFlags = 1;
        result.jdoStateManager = sm;
        result.jdoCopyKeyFieldsFromObjectId(obj);
        return result;
    }

    public final Object jdoNewObjectIdInstance() {
        return new ObjectIdentity(getClass(), this.key);
    }

    public final Object jdoNewObjectIdInstance(Object key2) {
        if (key2 != null) {
            return !(key2 instanceof String) ? new ObjectIdentity(getClass(), key2) : new ObjectIdentity(getClass(), (String) key2);
        }
        throw new IllegalArgumentException("key is null");
    }

    /* access modifiers changed from: protected */
    public final void jdoPreSerialize() {
        if (this.jdoStateManager != null) {
            this.jdoStateManager.preSerialize(this);
        }
    }

    public void jdoProvideField(int index) {
        if (this.jdoStateManager == null) {
            throw new IllegalStateException("state manager is null");
        }
        switch (index) {
            case 0:
                this.jdoStateManager.providedIntField(this, index, this.difficulty);
                return;
            case 1:
                this.jdoStateManager.providedObjectField(this, index, this.key);
                return;
            case 2:
                this.jdoStateManager.providedIntField(this, index, this.levelFinished);
                return;
            case 3:
                this.jdoStateManager.providedIntField(this, index, this.score);
                return;
            case 4:
                this.jdoStateManager.providedObjectField(this, index, this.updatedTime);
                return;
            case 5:
                this.jdoStateManager.providedStringField(this, index, this.userId);
                return;
            default:
                throw new IllegalArgumentException(new StringBuffer("out of field index :").append(index).toString());
        }
    }

    public final void jdoProvideFields(int[] indices) {
        if (indices == null) {
            throw new IllegalArgumentException("argment is null");
        }
        int i = indices.length - 1;
        if (i >= 0) {
            do {
                jdoProvideField(indices[i]);
                i--;
            } while (i >= 0);
        }
    }

    public void jdoReplaceField(int index) {
        if (this.jdoStateManager == null) {
            throw new IllegalStateException("state manager is null");
        }
        switch (index) {
            case 0:
                this.difficulty = this.jdoStateManager.replacingIntField(this, index);
                return;
            case 1:
                this.key = (Key) this.jdoStateManager.replacingObjectField(this, index);
                return;
            case 2:
                this.levelFinished = this.jdoStateManager.replacingIntField(this, index);
                return;
            case 3:
                this.score = this.jdoStateManager.replacingIntField(this, index);
                return;
            case 4:
                this.updatedTime = (Date) this.jdoStateManager.replacingObjectField(this, index);
                return;
            case 5:
                this.userId = this.jdoStateManager.replacingStringField(this, index);
                return;
            default:
                throw new IllegalArgumentException(new StringBuffer("out of field index :").append(index).toString());
        }
    }

    public final void jdoReplaceFields(int[] indices) {
        if (indices == null) {
            throw new IllegalArgumentException("argument is null");
        }
        int i = indices.length;
        if (i > 0) {
            int j = 0;
            do {
                jdoReplaceField(indices[j]);
                j++;
            } while (j < i);
        }
    }

    public final void jdoReplaceFlags() {
        if (this.jdoStateManager != null) {
            this.jdoFlags = this.jdoStateManager.replacingFlags(this);
        }
    }

    public final synchronized void jdoReplaceStateManager(StateManager sm) {
        if (this.jdoStateManager != null) {
            this.jdoStateManager = this.jdoStateManager.replacingStateManager(this, sm);
        } else {
            JDOImplHelper.checkAuthorizedStateManager(sm);
            this.jdoStateManager = sm;
            this.jdoFlags = 1;
        }
    }

    public static Class ___jdo$loadClass(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public ScoreRecord(int levelFinished2, int difficulty2, String userId2, int score2, Date date) {
        this.levelFinished = levelFinished2;
        this.difficulty = difficulty2;
        this.score = score2;
        this.userId = userId2;
        this.updatedTime = date;
    }

    public String getUserId() {
        return jdoGetuserId(this);
    }

    public int getLevelFinished() {
        return jdoGetlevelFinished(this);
    }

    public void setLevelFinished(int levelFinished2) {
        jdoSetlevelFinished(this, levelFinished2);
    }

    public int getDifficulty() {
        return jdoGetdifficulty(this);
    }

    public int getScore() {
        return jdoGetscore(this);
    }

    public void setScore(int score2) {
        jdoSetscore(this, score2);
    }

    public Date getUpdatedTime() {
        return jdoGetupdatedTime(this);
    }

    public void setUpdatedTime(Date updatedTime2) {
        jdoSetupdatedTime(this, updatedTime2);
    }
}
