package com.camelgames.explode.server.server;

import java.util.Date;
import javax.jdo.JDOFatalInternalException;
import javax.jdo.PersistenceManager;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.identity.StringIdentity;
import javax.jdo.spi.JDOImplHelper;
import javax.jdo.spi.PersistenceCapable;
import javax.jdo.spi.StateManager;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class UserRecord implements javax.jdo.spi.PersistenceCapable {
    private static final byte[] jdoFieldFlags = __jdoFieldFlagsInit();
    private static final String[] jdoFieldNames = __jdoFieldNamesInit();
    private static final Class[] jdoFieldTypes = __jdoFieldTypesInit();
    private static final int jdoInheritedFieldCount = __jdoGetInheritedFieldCount();
    private static final Class jdoPersistenceCapableSuperclass = __jdoPersistenceCapableSuperclassInit();
    @Persistent
    private Long appId;
    @Persistent
    private String comments;
    protected transient byte jdoFlags;
    protected transient StateManager jdoStateManager;
    @Persistent
    private Date registeredTime;
    @Persistent
    @PrimaryKey
    private String userId;
    @Persistent
    private String userName;

    static {
        JDOImplHelper.registerClass(___jdo$loadClass("com.camelgames.explode.server.server.UserRecord"), jdoFieldNames, jdoFieldTypes, jdoFieldFlags, jdoPersistenceCapableSuperclass, new UserRecord());
    }

    protected UserRecord() {
    }

    private static final byte[] __jdoFieldFlagsInit() {
        return new byte[]{21, 21, 21, 24, 21};
    }

    private static final String[] __jdoFieldNamesInit() {
        return new String[]{"appId", "comments", "registeredTime", "userId", "userName"};
    }

    private static final Class[] __jdoFieldTypesInit() {
        return new Class[]{___jdo$loadClass("java.lang.Long"), ___jdo$loadClass("java.lang.String"), ___jdo$loadClass("java.util.Date"), ___jdo$loadClass("java.lang.String"), ___jdo$loadClass("java.lang.String")};
    }

    protected static int __jdoGetInheritedFieldCount() {
        return 0;
    }

    private static Class __jdoPersistenceCapableSuperclassInit() {
        return null;
    }

    protected static int jdoGetManagedFieldCount() {
        return 5;
    }

    private static Long jdoGetappId(UserRecord objPC) {
        return (objPC.jdoFlags <= 0 || objPC.jdoStateManager == null || objPC.jdoStateManager.isLoaded(objPC, 0)) ? objPC.appId : (Long) objPC.jdoStateManager.getObjectField(objPC, 0, objPC.appId);
    }

    private static String jdoGetcomments(UserRecord objPC) {
        return (objPC.jdoFlags <= 0 || objPC.jdoStateManager == null || objPC.jdoStateManager.isLoaded(objPC, 1)) ? objPC.comments : objPC.jdoStateManager.getStringField(objPC, 1, objPC.comments);
    }

    private static Date jdoGetregisteredTime(UserRecord objPC) {
        return (objPC.jdoFlags <= 0 || objPC.jdoStateManager == null || objPC.jdoStateManager.isLoaded(objPC, 2)) ? objPC.registeredTime : (Date) objPC.jdoStateManager.getObjectField(objPC, 2, objPC.registeredTime);
    }

    private static String jdoGetuserId(UserRecord objPC) {
        return objPC.userId;
    }

    private static String jdoGetuserName(UserRecord objPC) {
        return (objPC.jdoFlags <= 0 || objPC.jdoStateManager == null || objPC.jdoStateManager.isLoaded(objPC, 4)) ? objPC.userName : objPC.jdoStateManager.getStringField(objPC, 4, objPC.userName);
    }

    private static void jdoSetappId(UserRecord objPC, Long val) {
        if (objPC.jdoFlags == 0 || objPC.jdoStateManager == null) {
            objPC.appId = val;
        } else {
            objPC.jdoStateManager.setObjectField(objPC, 0, objPC.appId, val);
        }
    }

    private static void jdoSetcomments(UserRecord objPC, String val) {
        if (objPC.jdoFlags == 0 || objPC.jdoStateManager == null) {
            objPC.comments = val;
        } else {
            objPC.jdoStateManager.setStringField(objPC, 1, objPC.comments, val);
        }
    }

    private static void jdoSetregisteredTime(UserRecord objPC, Date val) {
        if (objPC.jdoFlags == 0 || objPC.jdoStateManager == null) {
            objPC.registeredTime = val;
        } else {
            objPC.jdoStateManager.setObjectField(objPC, 2, objPC.registeredTime, val);
        }
    }

    private static void jdoSetuserId(UserRecord objPC, String val) {
        if (objPC.jdoStateManager == null) {
            objPC.userId = val;
        } else {
            objPC.jdoStateManager.setStringField(objPC, 3, objPC.userId, val);
        }
    }

    private static void jdoSetuserName(UserRecord objPC, String val) {
        if (objPC.jdoFlags == 0 || objPC.jdoStateManager == null) {
            objPC.userName = val;
        } else {
            objPC.jdoStateManager.setStringField(objPC, 4, objPC.userName, val);
        }
    }

    private Object jdoSuperClone() throws CloneNotSupportedException {
        UserRecord o = (UserRecord) super.clone();
        o.jdoFlags = 0;
        o.jdoStateManager = null;
        return o;
    }

    /* access modifiers changed from: protected */
    public final void jdoCopyField(UserRecord obj, int index) {
        switch (index) {
            case 0:
                this.appId = obj.appId;
                return;
            case 1:
                this.comments = obj.comments;
                return;
            case 2:
                this.registeredTime = obj.registeredTime;
                return;
            case 3:
                this.userId = obj.userId;
                return;
            case 4:
                this.userName = obj.userName;
                return;
            default:
                throw new IllegalArgumentException(new StringBuffer("out of field index :").append(index).toString());
        }
    }

    public void jdoCopyFields(Object obj, int[] indices) {
        if (this.jdoStateManager == null) {
            throw new IllegalStateException("state manager is null");
        } else if (indices == null) {
            throw new IllegalStateException("fieldNumbers is null");
        } else if (!(obj instanceof UserRecord)) {
            throw new IllegalArgumentException("object is not an object of type com.camelgames.explode.server.server.UserRecord");
        } else {
            UserRecord other = (UserRecord) obj;
            if (this.jdoStateManager != other.jdoStateManager) {
                throw new IllegalArgumentException("state managers do not match");
            }
            int i = indices.length - 1;
            if (i >= 0) {
                do {
                    jdoCopyField(other, indices[i]);
                    i--;
                } while (i >= 0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void jdoCopyKeyFieldsFromObjectId(Object oid) {
        if (!(oid instanceof StringIdentity)) {
            throw new ClassCastException("key class is not javax.jdo.identity.StringIdentity or null");
        }
        this.userId = ((StringIdentity) oid).getKey();
    }

    public void jdoCopyKeyFieldsFromObjectId(PersistenceCapable.ObjectIdFieldConsumer fc, Object oid) {
        if (fc == null) {
            throw new IllegalArgumentException("ObjectIdFieldConsumer is null");
        } else if (!(oid instanceof StringIdentity)) {
            throw new ClassCastException("oid is not instanceof javax.jdo.identity.StringIdentity");
        } else {
            fc.storeStringField(3, ((StringIdentity) oid).getKey());
        }
    }

    public final void jdoCopyKeyFieldsToObjectId(Object oid) {
        throw new JDOFatalInternalException("It's illegal to call jdoCopyKeyFieldsToObjectId for a class with SingleFieldIdentity.");
    }

    public final void jdoCopyKeyFieldsToObjectId(PersistenceCapable.ObjectIdFieldSupplier fs, Object obj) {
        throw new JDOFatalInternalException("It's illegal to call jdoCopyKeyFieldsToObjectId for a class with SingleFieldIdentity.");
    }

    public final Object jdoGetObjectId() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.getObjectId(this);
        }
        return null;
    }

    public final PersistenceManager jdoGetPersistenceManager() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.getPersistenceManager(this);
        }
        return null;
    }

    public final Object jdoGetTransactionalObjectId() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.getTransactionalObjectId(this);
        }
        return null;
    }

    public final Object jdoGetVersion() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.getVersion(this);
        }
        return null;
    }

    public final boolean jdoIsDeleted() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.isDeleted(this);
        }
        return false;
    }

    public boolean jdoIsDetached() {
        return false;
    }

    public final boolean jdoIsDirty() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.isDirty(this);
        }
        return false;
    }

    public final boolean jdoIsNew() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.isNew(this);
        }
        return false;
    }

    public final boolean jdoIsPersistent() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.isPersistent(this);
        }
        return false;
    }

    public final boolean jdoIsTransactional() {
        if (this.jdoStateManager != null) {
            return this.jdoStateManager.isTransactional(this);
        }
        return false;
    }

    public void jdoMakeDirty(String fieldName) {
        if (this.jdoStateManager != null) {
            this.jdoStateManager.makeDirty(this, fieldName);
        }
    }

    public javax.jdo.spi.PersistenceCapable jdoNewInstance(StateManager sm) {
        UserRecord result = new UserRecord();
        result.jdoFlags = 1;
        result.jdoStateManager = sm;
        return result;
    }

    public javax.jdo.spi.PersistenceCapable jdoNewInstance(StateManager sm, Object obj) {
        UserRecord result = new UserRecord();
        result.jdoFlags = 1;
        result.jdoStateManager = sm;
        result.jdoCopyKeyFieldsFromObjectId(obj);
        return result;
    }

    public final Object jdoNewObjectIdInstance() {
        return new StringIdentity(getClass(), this.userId);
    }

    public final Object jdoNewObjectIdInstance(Object key) {
        if (key != null) {
            return !(key instanceof String) ? new StringIdentity(getClass(), (String) key) : new StringIdentity(getClass(), (String) key);
        }
        throw new IllegalArgumentException("key is null");
    }

    /* access modifiers changed from: protected */
    public final void jdoPreSerialize() {
        if (this.jdoStateManager != null) {
            this.jdoStateManager.preSerialize(this);
        }
    }

    public void jdoProvideField(int index) {
        if (this.jdoStateManager == null) {
            throw new IllegalStateException("state manager is null");
        }
        switch (index) {
            case 0:
                this.jdoStateManager.providedObjectField(this, index, this.appId);
                return;
            case 1:
                this.jdoStateManager.providedStringField(this, index, this.comments);
                return;
            case 2:
                this.jdoStateManager.providedObjectField(this, index, this.registeredTime);
                return;
            case 3:
                this.jdoStateManager.providedStringField(this, index, this.userId);
                return;
            case 4:
                this.jdoStateManager.providedStringField(this, index, this.userName);
                return;
            default:
                throw new IllegalArgumentException(new StringBuffer("out of field index :").append(index).toString());
        }
    }

    public final void jdoProvideFields(int[] indices) {
        if (indices == null) {
            throw new IllegalArgumentException("argment is null");
        }
        int i = indices.length - 1;
        if (i >= 0) {
            do {
                jdoProvideField(indices[i]);
                i--;
            } while (i >= 0);
        }
    }

    public void jdoReplaceField(int index) {
        if (this.jdoStateManager == null) {
            throw new IllegalStateException("state manager is null");
        }
        switch (index) {
            case 0:
                this.appId = (Long) this.jdoStateManager.replacingObjectField(this, index);
                return;
            case 1:
                this.comments = this.jdoStateManager.replacingStringField(this, index);
                return;
            case 2:
                this.registeredTime = (Date) this.jdoStateManager.replacingObjectField(this, index);
                return;
            case 3:
                this.userId = this.jdoStateManager.replacingStringField(this, index);
                return;
            case 4:
                this.userName = this.jdoStateManager.replacingStringField(this, index);
                return;
            default:
                throw new IllegalArgumentException(new StringBuffer("out of field index :").append(index).toString());
        }
    }

    public final void jdoReplaceFields(int[] indices) {
        if (indices == null) {
            throw new IllegalArgumentException("argument is null");
        }
        int i = indices.length;
        if (i > 0) {
            int j = 0;
            do {
                jdoReplaceField(indices[j]);
                j++;
            } while (j < i);
        }
    }

    public final void jdoReplaceFlags() {
        if (this.jdoStateManager != null) {
            this.jdoFlags = this.jdoStateManager.replacingFlags(this);
        }
    }

    public final synchronized void jdoReplaceStateManager(StateManager sm) {
        if (this.jdoStateManager != null) {
            this.jdoStateManager = this.jdoStateManager.replacingStateManager(this, sm);
        } else {
            JDOImplHelper.checkAuthorizedStateManager(sm);
            this.jdoStateManager = sm;
            this.jdoFlags = 1;
        }
    }

    public static Class ___jdo$loadClass(String className) {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    public UserRecord(String userId2, String userName2, Long appId2, Date registeredTime2) {
        this.userId = userId2;
        this.userName = userName2;
        this.appId = appId2;
        this.registeredTime = registeredTime2;
    }

    public String getUserId() {
        return jdoGetuserId(this);
    }

    public Long getAppId() {
        return jdoGetappId(this);
    }

    public String getUserName() {
        return jdoGetuserName(this);
    }

    public void setUserName(String userName2) {
        jdoSetuserName(this, userName2);
    }

    public Date getRegisteredTime() {
        return jdoGetregisteredTime(this);
    }

    public void setComments(String comments2) {
        jdoSetcomments(this, comments2);
    }

    public String getComments() {
        return jdoGetcomments(this);
    }
}
