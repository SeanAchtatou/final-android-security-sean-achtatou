package com.camelgames.explode.tutorial;

import com.camelgames.blowuplite.R;
import com.camelgames.explode.GLScreenView;
import com.camelgames.framework.graphics.font.OESText;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.buttons.ScaleButton;
import com.camelgames.ndk.graphics.ScaleSprite;
import com.camelgames.ndk.graphics.Sprite2D;
import com.mobclix.android.sdk.MobclixDemographics;
import javax.microedition.khronos.opengles.GL10;

public class TextBlock extends ScaleButton {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$tutorial$TextBlock$Type = null;
    public static final float changeTime = 0.02f;
    public static final float maxScale = 1.5f;
    public static final float minScale = 0.8f;
    public static final float scaleStep = 0.05f;
    private Sprite2D arrawSprite;
    private ScaleSprite arrowTexture;
    private OESText[] drawingTexts;
    private Vector2 target = new Vector2();
    private Type type = Type.NoArrow;

    public enum Type {
        NoArrow,
        Left,
        Right,
        Top,
        Bottom,
        LeftTop,
        LeftBottom,
        RightTop,
        RightBottom
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$tutorial$TextBlock$Type() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$explode$tutorial$TextBlock$Type;
        if (iArr == null) {
            iArr = new int[Type.values().length];
            try {
                iArr[Type.Bottom.ordinal()] = 5;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Type.Left.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Type.LeftBottom.ordinal()] = 7;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Type.LeftTop.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[Type.NoArrow.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[Type.Right.ordinal()] = 3;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[Type.RightBottom.ordinal()] = 9;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[Type.RightTop.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[Type.Top.ordinal()] = 4;
            } catch (NoSuchFieldError e9) {
            }
            $SWITCH_TABLE$com$camelgames$explode$tutorial$TextBlock$Type = iArr;
        }
        return iArr;
    }

    /* JADX INFO: Multiple debug info for r3v1 int: [D('width' int), D('offset' int)] */
    public static TextBlock create(String[] texts, int fontSize, Callback callback) {
        OESText[] drawingTexts2 = new OESText[texts.length];
        int top = fontSize;
        int fontMargin = (int) (0.43f * ((float) fontSize));
        int offset = fontSize - fontMargin;
        int maxLengthIndex = 0;
        for (int i = 0; i < texts.length; i++) {
            drawingTexts2[i] = new OESText();
            drawingTexts2[i].setFontSize(fontSize);
            drawingTexts2[i].setFontMargin(fontMargin);
            drawingTexts2[i].setText(texts[i]);
            drawingTexts2[i].setLeftTop(offset, top);
            top += fontSize;
            if (texts[i].length() > texts[maxLengthIndex].length()) {
                maxLengthIndex = i;
            }
        }
        int offset2 = offset + drawingTexts2[maxLengthIndex].getTextWidth();
        int height = (drawingTexts2.length + 2) * fontSize;
        TextBlock textBlock = new TextBlock((float) (offset2 / 2), (float) (height / 2), (float) offset2, (float) height, callback);
        textBlock.setDrawingTexts(drawingTexts2);
        return textBlock;
    }

    private TextBlock(float x, float y, float width, float height, Callback callback) {
        setTexId(R.drawable.board);
        setPosition(x, y);
        setSize(width, height);
        setFunctionCallback(callback);
    }

    public void setType(Type type2) {
        this.type = type2;
    }

    public void setTarget(Vector2 target2) {
        this.target.set(target2);
    }

    public void updateLayout() {
        float angle = 0.0f;
        float xRate = 0.0f;
        float yRate = 0.0f;
        switch ($SWITCH_TABLE$com$camelgames$explode$tutorial$TextBlock$Type()[this.type.ordinal()]) {
            case 1:
                return;
            case 2:
                angle = 3.1415927f;
                xRate = 1.0f;
                yRate = 0.0f;
                break;
            case 3:
                angle = 0.0f;
                xRate = -1.0f;
                yRate = 0.0f;
                break;
            case 4:
                angle = -1.5707964f;
                xRate = 0.0f;
                yRate = 1.0f;
                break;
            case 5:
                angle = 1.5707964f;
                xRate = 0.0f;
                yRate = -1.0f;
                break;
            case 6:
                angle = -2.3561945f;
                xRate = 1.0f;
                yRate = 1.0f;
                break;
            case MobclixDemographics.ReligionOther /*7*/:
                angle = 2.3561945f;
                xRate = 1.0f;
                yRate = -1.0f;
                break;
            case 8:
                angle = -0.7853982f;
                xRate = -1.0f;
                yRate = 1.0f;
                break;
            case 9:
                angle = 0.7853982f;
                xRate = -1.0f;
                yRate = -1.0f;
                break;
        }
        float arrowSize = ((float) this.drawingTexts[0].getFontSize()) * 1.5f;
        float arrowX = this.target.X - ((((float) Math.cos((double) angle)) * 0.5f) * arrowSize);
        float arrowY = this.target.Y - ((((float) Math.sin((double) angle)) * 0.5f) * arrowSize);
        initiateArrow(arrowSize, arrowX, arrowY, angle);
        setPosition(arrowX + ((getWidth() + arrowSize) * 0.5f * xRate), arrowY + ((getHeight() + arrowSize) * 0.5f * yRate));
    }

    public void render(GL10 gl, float elapsedTime) {
        super.render(gl, elapsedTime);
        if (isActive()) {
            if (this.arrowTexture != null) {
                this.arrowTexture.render(elapsedTime);
            }
            for (OESText text : this.drawingTexts) {
                text.draw(gl, GLScreenView.textBuilder);
            }
        }
    }

    private void initiateArrow(float arrowSize, float x, float y, float angle) {
        this.arrawSprite = new Sprite2D();
        this.arrawSprite.setTexId(R.drawable.arrow);
        this.arrawSprite.setSize(arrowSize, arrowSize);
        this.arrawSprite.setPosition(x, y, 0.0f);
        this.arrawSprite.setAngle(angle);
        this.arrowTexture = new ScaleSprite();
        this.arrowTexture.setSprite(this.arrawSprite);
        this.arrowTexture.setScaleInfo(1.5f, 0.8f, 0.05f);
        this.arrowTexture.setChangeTime(0.02f);
        this.arrowTexture.setLoop(true);
        this.arrowTexture.setFold(true);
    }

    public void setPosition(float x, float y) {
        float xOffset = x - getActualX();
        float yOffset = y - getActualY();
        super.setPosition(x, y);
        if (this.drawingTexts != null) {
            for (OESText text : this.drawingTexts) {
                text.setLeftTop((int) (((float) text.getLeft()) + xOffset), (int) (((float) text.getTop()) + yOffset));
            }
        }
    }

    public void setDrawingTexts(OESText[] drawingTexts2) {
        this.drawingTexts = drawingTexts2;
    }
}
