package com.camelgames.explode.tutorial;

import com.camelgames.explode.game.GameManager;
import com.camelgames.explode.manipulation.BombManipulator;
import com.camelgames.explode.tutorial.TextBlock;
import com.camelgames.explode.ui.PlayingUI;
import com.camelgames.framework.Skeleton.RenderableListenerEntity;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.Renderable;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;
import com.camelgames.framework.ui.UIUtility;
import java.util.LinkedList;
import javax.microedition.khronos.opengles.GL10;

public class Tutorial extends RenderableListenerEntity {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$events$EventType;
    private LinkedList<TextBlock> blocks = new LinkedList<>();
    private Callback callback = new Callback() {
        public void excute(UI ui) {
            Tutorial.this.nextBlock();
        }
    };
    private final int fontSize = UIUtility.getDisplayWidthPlusHeight(0.03f);

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$events$EventType() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$framework$events$EventType;
        if (iArr == null) {
            iArr = new int[EventType.values().length];
            try {
                iArr[EventType.Arrive.ordinal()] = 61;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[EventType.AttachRagdoll.ordinal()] = 65;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[EventType.AttachStick.ordinal()] = 66;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[EventType.AttachedToJoint.ordinal()] = 40;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[EventType.BallCollide.ordinal()] = 8;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[EventType.Bomb.ordinal()] = 67;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[EventType.BombLarge.ordinal()] = 68;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[EventType.BrickChanged.ordinal()] = 45;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[EventType.Button.ordinal()] = 30;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[EventType.Captured.ordinal()] = 34;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[EventType.CarCreated.ordinal()] = 39;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[EventType.Catched.ordinal()] = 11;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[EventType.Collide.ordinal()] = 6;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[EventType.ContinueGame.ordinal()] = 23;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[EventType.Crash.ordinal()] = 59;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[EventType.Customise.ordinal()] = 25;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[EventType.DoubleTap.ordinal()] = 10;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[EventType.EraseAll.ordinal()] = 41;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[EventType.Fall.ordinal()] = 36;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[EventType.Favourite.ordinal()] = 46;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[EventType.GameBegin.ordinal()] = 26;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[EventType.GotScore.ordinal()] = 18;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[EventType.GraphicsCreated.ordinal()] = 1;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[EventType.GraphicsDestroyed.ordinal()] = 2;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[EventType.GraphicsResized.ordinal()] = 3;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[EventType.HighScore.ordinal()] = 28;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[EventType.JointCreated.ordinal()] = 62;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[EventType.Landed.ordinal()] = 58;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[EventType.LevelDown.ordinal()] = 15;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[EventType.LevelEditorCopyBrick.ordinal()] = 47;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[EventType.LevelEditorDeleteBrick.ordinal()] = 48;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[EventType.LevelEditorEditGround.ordinal()] = 51;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[EventType.LevelEditorEditQueue.ordinal()] = 50;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[EventType.LevelEditorExit.ordinal()] = 53;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[EventType.LevelEditorNewBrick.ordinal()] = 49;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[EventType.LevelEditorSave.ordinal()] = 54;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[EventType.LevelEditorStart.ordinal()] = 52;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[EventType.LevelEditorTest.ordinal()] = 55;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[EventType.LevelFailed.ordinal()] = 14;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[EventType.LevelFinished.ordinal()] = 13;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[EventType.LevelUp.ordinal()] = 12;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[EventType.LineCreated.ordinal()] = 37;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[EventType.LineDestroyed.ordinal()] = 38;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[EventType.LoadLevel.ordinal()] = 16;
            } catch (NoSuchFieldError e44) {
            }
            try {
                iArr[EventType.MainMenu.ordinal()] = 19;
            } catch (NoSuchFieldError e45) {
            }
            try {
                iArr[EventType.NewGame.ordinal()] = 22;
            } catch (NoSuchFieldError e46) {
            }
            try {
                iArr[EventType.PapaStackLoadLevel.ordinal()] = 43;
            } catch (NoSuchFieldError e47) {
            }
            try {
                iArr[EventType.PressSwitch.ordinal()] = 69;
            } catch (NoSuchFieldError e48) {
            }
            try {
                iArr[EventType.Purchased.ordinal()] = 70;
            } catch (NoSuchFieldError e49) {
            }
            try {
                iArr[EventType.RagdollDied.ordinal()] = 42;
            } catch (NoSuchFieldError e50) {
            }
            try {
                iArr[EventType.ReadyToLand.ordinal()] = 57;
            } catch (NoSuchFieldError e51) {
            }
            try {
                iArr[EventType.ReadyToLoadLevel.ordinal()] = 17;
            } catch (NoSuchFieldError e52) {
            }
            try {
                iArr[EventType.Refunded.ordinal()] = 71;
            } catch (NoSuchFieldError e53) {
            }
            try {
                iArr[EventType.Replay.ordinal()] = 21;
            } catch (NoSuchFieldError e54) {
            }
            try {
                iArr[EventType.Restart.ordinal()] = 20;
            } catch (NoSuchFieldError e55) {
            }
            try {
                iArr[EventType.SelectLevel.ordinal()] = 24;
            } catch (NoSuchFieldError e56) {
            }
            try {
                iArr[EventType.SensorContact.ordinal()] = 7;
            } catch (NoSuchFieldError e57) {
            }
            try {
                iArr[EventType.Shoot.ordinal()] = 63;
            } catch (NoSuchFieldError e58) {
            }
            try {
                iArr[EventType.SingleTap.ordinal()] = 9;
            } catch (NoSuchFieldError e59) {
            }
            try {
                iArr[EventType.SoundOff.ordinal()] = 32;
            } catch (NoSuchFieldError e60) {
            }
            try {
                iArr[EventType.SoundOn.ordinal()] = 31;
            } catch (NoSuchFieldError e61) {
            }
            try {
                iArr[EventType.StartExplode.ordinal()] = 64;
            } catch (NoSuchFieldError e62) {
            }
            try {
                iArr[EventType.StartToRecord.ordinal()] = 44;
            } catch (NoSuchFieldError e63) {
            }
            try {
                iArr[EventType.StaticEdgesCreated.ordinal()] = 4;
            } catch (NoSuchFieldError e64) {
            }
            try {
                iArr[EventType.StaticEdgesDestroyed.ordinal()] = 5;
            } catch (NoSuchFieldError e65) {
            }
            try {
                iArr[EventType.Tick.ordinal()] = 33;
            } catch (NoSuchFieldError e66) {
            }
            try {
                iArr[EventType.Triggered.ordinal()] = 35;
            } catch (NoSuchFieldError e67) {
            }
            try {
                iArr[EventType.Tutorial.ordinal()] = 27;
            } catch (NoSuchFieldError e68) {
            }
            try {
                iArr[EventType.UICommand.ordinal()] = 29;
            } catch (NoSuchFieldError e69) {
            }
            try {
                iArr[EventType.UploadLevel.ordinal()] = 56;
            } catch (NoSuchFieldError e70) {
            }
            try {
                iArr[EventType.Warning.ordinal()] = 60;
            } catch (NoSuchFieldError e71) {
            }
            $SWITCH_TABLE$com$camelgames$framework$events$EventType = iArr;
        }
        return iArr;
    }

    public Tutorial() {
        createBlock0();
        createBlock1();
        createBlock2();
        createBlock3();
        createBlock4();
        createBlock5();
        addEventType(EventType.SingleTap);
        addListener();
        setPriority(Renderable.PRIORITY.HIGHEST);
        setVisible(true);
        openGameResponse(false);
    }

    /* access modifiers changed from: protected */
    public void disposeInternal() {
        super.disposeInternal();
        openGameResponse(true);
    }

    private void openGameResponse(boolean open) {
        boolean z;
        boolean z2;
        BombManipulator instance = BombManipulator.getInstance();
        if (open) {
            z = false;
        } else {
            z = true;
        }
        instance.setStoped(z);
        PlayingUI instance2 = PlayingUI.getInstance();
        if (open) {
            z2 = false;
        } else {
            z2 = true;
        }
        instance2.setStoped(z2);
    }

    private void createBlock0() {
        TextBlock block = TextBlock.create(new String[]{"The rules are simple. Your objective", "is to blow up the building and make", "all pieces land below the dotted line"}, this.fontSize, this.callback);
        block.setType(TextBlock.Type.Bottom);
        block.setTarget(new Vector2(new Vector2(0.5f * ((float) GraphicsManager.screenWidth()), GameManager.getInstance().getTargetLine() * ((float) GraphicsManager.screenHeight()))));
        block.updateLayout();
        this.blocks.add(block);
    }

    private void createBlock1() {
        TextBlock block = TextBlock.create(new String[]{"Drag bombs from here and release", "to attach them to the building"}, this.fontSize, this.callback);
        block.setType(TextBlock.Type.Bottom);
        block.setTarget(new Vector2(PlayingUI.smallBombDefaultPos));
        block.updateLayout();
        this.blocks.add(block);
    }

    private void createBlock2() {
        TextBlock block = TextBlock.create(new String[]{"Click on a bomb to set", "an explosion delay", "(Measured in milliseconds)"}, this.fontSize, this.callback);
        block.setType(TextBlock.Type.NoArrow);
        block.setPosition(((float) GraphicsManager.screenWidth()) * 0.5f, ((float) GraphicsManager.screenHeight()) * 0.5f);
        this.blocks.add(block);
    }

    private void createBlock3() {
        TextBlock block = TextBlock.create(new String[]{"The camera will zoom in/out automatically", "You can also control zooming by double", "tapping the screen"}, this.fontSize, this.callback);
        block.setType(TextBlock.Type.NoArrow);
        block.setPosition(((float) GraphicsManager.screenWidth()) * 0.5f, ((float) GraphicsManager.screenHeight()) * 0.5f);
        this.blocks.add(block);
    }

    private void createBlock4() {
        TextBlock block = TextBlock.create(new String[]{"If the panda hits the star, you'll", "get a bonus. But if you blow up", "the panda, you'll be punished"}, this.fontSize, this.callback);
        block.setType(TextBlock.Type.NoArrow);
        block.setPosition(((float) GraphicsManager.screenWidth()) * 0.5f, ((float) GraphicsManager.screenHeight()) * 0.5f);
        this.blocks.add(block);
    }

    private void createBlock5() {
        TextBlock block = TextBlock.create(new String[]{"Finally, press the button", "to trigger big blast!"}, this.fontSize, this.callback);
        block.setType(TextBlock.Type.RightBottom);
        block.setTarget(new Vector2(PlayingUI.switchDefaultPos));
        block.updateLayout();
        this.blocks.add(block);
    }

    /* access modifiers changed from: private */
    public void nextBlock() {
        this.blocks.poll();
        if (this.blocks.isEmpty()) {
            delete();
        }
    }

    public void HandleEvent(AbstractEvent e) {
        switch ($SWITCH_TABLE$com$camelgames$framework$events$EventType()[e.getType().ordinal()]) {
            case 9:
                TextBlock block = this.blocks.peek();
                if (block != null) {
                    block.excute();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void update(float elapsedTime) {
    }

    public void render(GL10 gl, float elapsedTime) {
        TextBlock block = this.blocks.peek();
        if (block != null) {
            block.render(gl, elapsedTime);
        }
    }
}
