package com.camelgames.explode.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.camelgames.blowuplite.R;
import com.camelgames.explode.game.GameManager;
import com.camelgames.explode.sound.SoundManager;
import com.camelgames.explode.ui.ExplodeUIUtility;
import com.camelgames.explode.ui.LevelSelectorView;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.graphics.textures.TextureUtility;
import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;
import com.camelgames.framework.ui.UIUtility;

public class MainMenuActivity extends Activity {
    private int buttonHeight = ((int) (((float) UIUtility.getDisplayHeight()) * 0.14f));
    private LevelSelectorView levelSelectorView;
    private View mainMenuButtonsView;
    /* access modifiers changed from: private */
    public View soundOffButton;
    /* access modifiers changed from: private */
    public View soundOnButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().setFlags(128, 128);
        setContentView((int) R.layout.mainmenu_view);
        ExplodeUIUtility.setupBackground(findViewById(R.id.mainmenu_view), (ImageView) findViewById(R.id.mainmenu_top), (ImageView) findViewById(R.id.mainmenu_bottom), (ImageView) findViewById(R.id.mainmenu_logo), (ImageView) findViewById(R.id.mainmenu_star));
        setButtonsListener();
        getViews();
        setVolumeControlStream(3);
        GameManager.getInstance().setQuit();
        mainMenu();
    }

    private void setButtonsListener() {
        View buttonPlay = findViewById(R.id.play_button);
        UIUtility.setButtonSize(buttonPlay, this.buttonHeight);
        UIUtility.setCenterForRL(buttonPlay, 0.5f, 0.25f);
        buttonPlay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EventManager.getInstance().postEvent(EventType.Restart);
                GameManager.getInstance().setPlaying();
                MainMenuActivity.this.finish();
            }
        });
        View buttonLevelSelect = findViewById(R.id.levelselect_button);
        UIUtility.setButtonSize(buttonLevelSelect, this.buttonHeight);
        UIUtility.setCenterForRL(buttonLevelSelect, 0.5f, 0.42f);
        buttonLevelSelect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainMenuActivity.this.selectLevel();
            }
        });
        View buttonRanking = findViewById(R.id.ranking_button);
        UIUtility.setButtonSize(buttonRanking, this.buttonHeight);
        UIUtility.setCenterForRL(buttonRanking, 0.5f, 0.59f);
        buttonRanking.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainMenuActivity.this.startActivity(new Intent(MainMenuActivity.this, ScoreActivity.class));
            }
        });
        View buttonMoregames = findViewById(R.id.moregames_button);
        UIUtility.setButtonSize(buttonMoregames, this.buttonHeight);
        UIUtility.setCenterForRL(buttonMoregames, 0.5f, 0.76f);
        buttonMoregames.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainMenuActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(MainMenuActivity.this.getResources().getString(R.string.more_games_link))));
            }
        });
        int barThick = (int) (0.12f * ((float) UIUtility.getDisplayHeight()));
        int soundButtonHeight = (int) (0.8f * ((float) barThick));
        this.soundOnButton = findViewById(R.id.soundon);
        this.soundOnButton.setBackgroundDrawable(new BitmapDrawable(TextureUtility.getChopedBitmap((int) R.drawable.altas4, 367, 209, 63, 46)));
        UIUtility.setButtonSize(this.soundOnButton, soundButtonHeight);
        UIUtility.setLeftTopForRLAbsolute(this.soundOnButton, soundButtonHeight / 4, (soundButtonHeight / 4) + barThick);
        this.soundOnButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundManager.getInstance().setMute(true);
                MainMenuActivity.this.soundOnButton.setVisibility(8);
                MainMenuActivity.this.soundOffButton.setVisibility(0);
            }
        });
        this.soundOffButton = findViewById(R.id.soundoff);
        this.soundOffButton.setBackgroundDrawable(new BitmapDrawable(TextureUtility.getChopedBitmap((int) R.drawable.altas4, 430, 209, 63, 46)));
        UIUtility.setButtonSize(this.soundOffButton, soundButtonHeight);
        UIUtility.setLeftTopForRLAbsolute(this.soundOffButton, soundButtonHeight / 4, (soundButtonHeight / 4) + barThick);
        this.soundOffButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundManager.getInstance().setMute(false);
                MainMenuActivity.this.soundOffButton.setVisibility(8);
                MainMenuActivity.this.soundOnButton.setVisibility(0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void mainMenu() {
        this.mainMenuButtonsView.setVisibility(0);
        this.levelSelectorView.setVisibility(8);
        if (SoundManager.getInstance().isMute()) {
            this.soundOnButton.setVisibility(8);
            this.soundOffButton.setVisibility(0);
            return;
        }
        this.soundOnButton.setVisibility(0);
        this.soundOffButton.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void selectLevel() {
        this.mainMenuButtonsView.setVisibility(8);
        this.levelSelectorView.setVisibility(0);
        this.soundOnButton.setVisibility(8);
        this.soundOffButton.setVisibility(8);
    }

    private void getViews() {
        this.mainMenuButtonsView = findViewById(R.id.mainmenu_buttons);
        this.levelSelectorView = (LevelSelectorView) findViewById(R.id.levelselector_view);
        this.levelSelectorView.setBackCallback(new Callback() {
            public void excute(UI ui) {
                MainMenuActivity.this.mainMenu();
            }
        });
        findViewById(R.id.adview).setVisibility(0);
    }
}
