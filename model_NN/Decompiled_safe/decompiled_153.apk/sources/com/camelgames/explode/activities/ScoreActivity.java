package com.camelgames.explode.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.camelgames.blowuplite.R;
import com.camelgames.explode.game.GameManager;
import com.camelgames.explode.levels.LevelManager;
import com.camelgames.explode.score.FlightAsyncHttpRequest;
import com.camelgames.explode.score.ScoreManager;
import com.camelgames.explode.server.serializable.ScoreBoardInfo;
import com.camelgames.framework.network.AsyncHttpRequest;
import com.camelgames.framework.ui.UIUtility;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.LinkedList;

public class ScoreActivity extends Activity {
    private FlightAsyncHttpRequest asyncHttpRequest;
    private int buttonHeight = ((int) (((float) UIUtility.getDisplayHeight()) * 0.15f));
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
    /* access modifiers changed from: private */
    public int difficulty;
    private Resources resources;
    private LinkedList<View> viewAdded = new LinkedList<>();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.scorelist);
        final RadioButton easyButton = (RadioButton) findViewById(R.id.easy);
        final RadioButton mediumButton = (RadioButton) findViewById(R.id.medium);
        final RadioButton hardButton = (RadioButton) findViewById(R.id.hard);
        ((RadioGroup) findViewById(R.id.difficulties)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == easyButton.getId()) {
                    ScoreActivity.this.difficulty = LevelManager.DifficultyType.Easy.ordinal();
                } else if (checkedId == mediumButton.getId()) {
                    ScoreActivity.this.difficulty = LevelManager.DifficultyType.Medium.ordinal();
                } else if (checkedId == hardButton.getId()) {
                    ScoreActivity.this.difficulty = LevelManager.DifficultyType.Hard.ordinal();
                }
                ScoreActivity.this.showScoreBoard();
            }
        });
        View moregamesButton = findViewById(R.id.moregames_button);
        UIUtility.setButtonSize(moregamesButton, this.buttonHeight);
        moregamesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ScoreActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(ScoreActivity.this.getResources().getString(R.string.more_games_link))));
            }
        });
        this.resources = getResources();
        if (ScoreManager.getInstance().getUserName() == null) {
            showDialog(3);
        } else {
            sendScoreAndShowScoreBoard();
        }
        GameManager.getInstance().showPontiflexAds();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        cancelRequest();
    }

    private void cancelRequest() {
        if (this.asyncHttpRequest != null) {
            this.asyncHttpRequest.cancel();
            this.asyncHttpRequest = null;
        }
    }

    /* access modifiers changed from: private */
    public void sendScoreAndShowScoreBoard() {
        this.asyncHttpRequest = ScoreManager.getInstance().sendScore(new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what != 0) {
                    String response = msg.getData().getString(AsyncHttpRequest.KEY_RESPONSE);
                    if (AsyncHttpRequest.NO_CONNECTION.equals(response)) {
                        ScoreActivity.this.showDialog(1);
                    } else if (AsyncHttpRequest.OUT_OF_SERVICE.equals(response)) {
                        ScoreActivity.this.showDialog(2);
                    }
                } else {
                    ScoreActivity.this.showScoreBoard();
                }
            }
        });
    }

    public void showScoreBoard() {
        findViewById(R.id.scoreTable).setVisibility(8);
        removeViews((TableLayout) findViewById(R.id.scoreContainer));
        findViewById(16908301).setVisibility(0);
        this.asyncHttpRequest = new FlightAsyncHttpRequest(new Handler() {
            public void handleMessage(Message msg) {
                String response = msg.getData().getString(AsyncHttpRequest.KEY_RESPONSE);
                if (AsyncHttpRequest.NO_CONNECTION.equals(response)) {
                    ScoreActivity.this.showDialog(1);
                } else if (AsyncHttpRequest.OUT_OF_SERVICE.equals(response)) {
                    ScoreActivity.this.showDialog(2);
                } else {
                    try {
                        ScoreBoardInfo scoreBoardInfo = (ScoreBoardInfo) msg.getData().getSerializable(AsyncHttpRequest.KEY_RESPONSE);
                        if (scoreBoardInfo != null) {
                            ScoreActivity.this.lauchScoreBoard(scoreBoardInfo);
                        }
                    } catch (Exception e) {
                        ScoreActivity.this.showDialog(2);
                    }
                }
            }
        });
        this.asyncHttpRequest.showScoreBoard(ScoreManager.getInstance().getUserId(), this.difficulty);
    }

    private void removeViews(TableLayout scoreContainer) {
        Iterator<View> it = this.viewAdded.iterator();
        while (it.hasNext()) {
            scoreContainer.removeView(it.next());
        }
        this.viewAdded.clear();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        if (id == 3) {
            return showInputNameDialog();
        }
        if (id == 1) {
            return UIUtility.showOkDialog(this, R.string.no_network, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    ScoreActivity.this.finish();
                }
            });
        }
        if (id == 2) {
            return UIUtility.showOkDialog(this, R.string.out_of_service, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    ScoreActivity.this.finish();
                }
            });
        }
        return null;
    }

    private Dialog showInputNameDialog() {
        final View textEntryView = LayoutInflater.from(this).inflate((int) R.layout.inputusername, (ViewGroup) null);
        return new AlertDialog.Builder(this).setIcon(17301515).setView(textEntryView).setPositiveButton(this.resources.getString(R.string.ranking_submit), new DialogInterface.OnClickListener() {
            /* Debug info: failed to restart local var, previous not found, register: 3 */
            public void onClick(DialogInterface dialog, int whichButton) {
                String userName = ((EditText) textEntryView.findViewById(R.id.username_edit)).getText().toString();
                userName.trim();
                if (!"".equals(userName)) {
                    ScoreManager.getInstance().setUserName(userName);
                    ScoreActivity.this.sendScoreAndShowScoreBoard();
                } else {
                    ScoreActivity.this.showScoreBoard();
                }
                ((EditText) textEntryView.findViewById(R.id.username_edit)).setText("");
            }
        }).setNegativeButton(this.resources.getString(17039360), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ScoreActivity.this.showScoreBoard();
                ((EditText) textEntryView.findViewById(R.id.username_edit)).setText("");
            }
        }).create();
    }

    private void setRowCell(TableRow row, String text, float rate) {
        TextView cell = new TextView(this);
        cell.setTextAppearance(this, 16973892);
        cell.setTextSize(cell.getTextSize() * rate);
        cell.setText(text);
        cell.setPadding(0, 0, 10, 0);
        row.addView(cell);
    }

    /* access modifiers changed from: private */
    public void lauchScoreBoard(ScoreBoardInfo scoreBoardInfo) {
        if (scoreBoardInfo.items == null || scoreBoardInfo.items.length == 0) {
            throw new RuntimeException();
        }
        TableLayout scoreContainer = (TableLayout) findViewById(R.id.scoreContainer);
        for (int i = 0; i < scoreBoardInfo.items.length; i++) {
            ScoreBoardInfo.Item item = scoreBoardInfo.items[i];
            TableRow row = new TableRow(this);
            if (i == scoreBoardInfo.currentUserSequence) {
                row.setBackgroundColor(Color.argb(51, 255, 0, 0));
            } else if (i % 2 == 0) {
                row.setBackgroundColor(Color.argb(51, 153, 153, 153));
            }
            setRowCell(row, (i + 1) + ".", 1.2f);
            fillRowInfo(item, row);
            addView(scoreContainer, row);
        }
        TableRow row2 = new TableRow(this);
        row2.setBackgroundColor(Color.argb(51, 255, 0, 0));
        setRowCell(row2, "", 1.0f);
        ScoreBoardInfo.Item item2 = new ScoreBoardInfo.Item();
        item2.userName = "YOU!";
        item2.levelFinished = ScoreManager.getInstance().getFinishedCount(this.difficulty);
        item2.score = ScoreManager.getInstance().getTotalScore(this.difficulty);
        fillRowInfo(item2, row2);
        addView(scoreContainer, row2);
        findViewById(16908301).setVisibility(8);
        findViewById(R.id.scoreTable).setVisibility(0);
    }

    private void addView(TableLayout scoreContainer, View row) {
        scoreContainer.addView(row);
        this.viewAdded.add(row);
    }

    private void fillRowInfo(ScoreBoardInfo.Item item, TableRow row) {
        setRowCell(row, item.userName, 1.0f);
        setRowCell(row, new StringBuilder().append(item.levelFinished).toString(), 1.0f);
        setRowCell(row, new StringBuilder().append(item.score).toString(), 1.0f);
        if (item.updatedTime != null) {
            setRowCell(row, this.dateFormat.format(item.updatedTime), 1.0f);
        }
    }
}
