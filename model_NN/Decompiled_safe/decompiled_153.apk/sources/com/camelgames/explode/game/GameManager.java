package com.camelgames.explode.game;

import android.content.Intent;
import android.view.View;
import com.camelgames.blowuplite.MainActivity;
import com.camelgames.blowuplite.R;
import com.camelgames.explode.GLScreenView;
import com.camelgames.explode.activities.MainMenuActivity;
import com.camelgames.explode.entities.Background;
import com.camelgames.explode.levels.LevelManager;
import com.camelgames.explode.manipulation.BombManipulator;
import com.camelgames.explode.sound.SoundManager;
import com.camelgames.explode.tutorial.Tutorial;
import com.camelgames.explode.ui.PlayingUI;
import com.camelgames.framework.Skeleton.EventListenerUtil;
import com.camelgames.framework.entities.EntityManager;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.events.EventListener;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.levels.LevelScript;
import com.camelgames.framework.physics.Edges;
import com.camelgames.framework.physics.PhysicsManager;
import com.camelgames.framework.touch.TouchManager;
import com.camelgames.framework.ui.UIUtility;
import com.pontiflex.mobile.webview.sdk.AdManagerFactory;
import com.pontiflex.mobile.webview.sdk.IAdManager;

public class GameManager implements EventListener, TouchManager.KeyProcessor {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$levels$LevelManager$DifficultyType;
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$events$EventType;
    private static GameManager instance = new GameManager();
    public static int launchCount;
    /* access modifiers changed from: private */
    public IAdManager adManager;
    /* access modifiers changed from: private */
    public View adsView;
    private Background background;
    private float bombPower;
    private EventListenerUtil eventListenerUtil = new EventListenerUtil();
    private boolean isInitiated;
    private int levelGroundTopUnit;
    /* access modifiers changed from: private */
    public MainActivity mainActivity;
    private Mode mode;
    private int standardThickUnit;
    private float targetLine;
    private float unitLength;

    public enum Mode {
        MainMenu,
        Playing,
        Paused,
        GamePass,
        GameFail,
        Quit
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$levels$LevelManager$DifficultyType() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$explode$levels$LevelManager$DifficultyType;
        if (iArr == null) {
            iArr = new int[LevelManager.DifficultyType.values().length];
            try {
                iArr[LevelManager.DifficultyType.Easy.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[LevelManager.DifficultyType.Hard.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[LevelManager.DifficultyType.Medium.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$camelgames$explode$levels$LevelManager$DifficultyType = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$events$EventType() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$framework$events$EventType;
        if (iArr == null) {
            iArr = new int[EventType.values().length];
            try {
                iArr[EventType.Arrive.ordinal()] = 61;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[EventType.AttachRagdoll.ordinal()] = 65;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[EventType.AttachStick.ordinal()] = 66;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[EventType.AttachedToJoint.ordinal()] = 40;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[EventType.BallCollide.ordinal()] = 8;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[EventType.Bomb.ordinal()] = 67;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[EventType.BombLarge.ordinal()] = 68;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[EventType.BrickChanged.ordinal()] = 45;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[EventType.Button.ordinal()] = 30;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[EventType.Captured.ordinal()] = 34;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[EventType.CarCreated.ordinal()] = 39;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[EventType.Catched.ordinal()] = 11;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[EventType.Collide.ordinal()] = 6;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[EventType.ContinueGame.ordinal()] = 23;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[EventType.Crash.ordinal()] = 59;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[EventType.Customise.ordinal()] = 25;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[EventType.DoubleTap.ordinal()] = 10;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[EventType.EraseAll.ordinal()] = 41;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[EventType.Fall.ordinal()] = 36;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[EventType.Favourite.ordinal()] = 46;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[EventType.GameBegin.ordinal()] = 26;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[EventType.GotScore.ordinal()] = 18;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[EventType.GraphicsCreated.ordinal()] = 1;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[EventType.GraphicsDestroyed.ordinal()] = 2;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[EventType.GraphicsResized.ordinal()] = 3;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[EventType.HighScore.ordinal()] = 28;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[EventType.JointCreated.ordinal()] = 62;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[EventType.Landed.ordinal()] = 58;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[EventType.LevelDown.ordinal()] = 15;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[EventType.LevelEditorCopyBrick.ordinal()] = 47;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[EventType.LevelEditorDeleteBrick.ordinal()] = 48;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[EventType.LevelEditorEditGround.ordinal()] = 51;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[EventType.LevelEditorEditQueue.ordinal()] = 50;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[EventType.LevelEditorExit.ordinal()] = 53;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[EventType.LevelEditorNewBrick.ordinal()] = 49;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[EventType.LevelEditorSave.ordinal()] = 54;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[EventType.LevelEditorStart.ordinal()] = 52;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[EventType.LevelEditorTest.ordinal()] = 55;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[EventType.LevelFailed.ordinal()] = 14;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[EventType.LevelFinished.ordinal()] = 13;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[EventType.LevelUp.ordinal()] = 12;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[EventType.LineCreated.ordinal()] = 37;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[EventType.LineDestroyed.ordinal()] = 38;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[EventType.LoadLevel.ordinal()] = 16;
            } catch (NoSuchFieldError e44) {
            }
            try {
                iArr[EventType.MainMenu.ordinal()] = 19;
            } catch (NoSuchFieldError e45) {
            }
            try {
                iArr[EventType.NewGame.ordinal()] = 22;
            } catch (NoSuchFieldError e46) {
            }
            try {
                iArr[EventType.PapaStackLoadLevel.ordinal()] = 43;
            } catch (NoSuchFieldError e47) {
            }
            try {
                iArr[EventType.PressSwitch.ordinal()] = 69;
            } catch (NoSuchFieldError e48) {
            }
            try {
                iArr[EventType.Purchased.ordinal()] = 70;
            } catch (NoSuchFieldError e49) {
            }
            try {
                iArr[EventType.RagdollDied.ordinal()] = 42;
            } catch (NoSuchFieldError e50) {
            }
            try {
                iArr[EventType.ReadyToLand.ordinal()] = 57;
            } catch (NoSuchFieldError e51) {
            }
            try {
                iArr[EventType.ReadyToLoadLevel.ordinal()] = 17;
            } catch (NoSuchFieldError e52) {
            }
            try {
                iArr[EventType.Refunded.ordinal()] = 71;
            } catch (NoSuchFieldError e53) {
            }
            try {
                iArr[EventType.Replay.ordinal()] = 21;
            } catch (NoSuchFieldError e54) {
            }
            try {
                iArr[EventType.Restart.ordinal()] = 20;
            } catch (NoSuchFieldError e55) {
            }
            try {
                iArr[EventType.SelectLevel.ordinal()] = 24;
            } catch (NoSuchFieldError e56) {
            }
            try {
                iArr[EventType.SensorContact.ordinal()] = 7;
            } catch (NoSuchFieldError e57) {
            }
            try {
                iArr[EventType.Shoot.ordinal()] = 63;
            } catch (NoSuchFieldError e58) {
            }
            try {
                iArr[EventType.SingleTap.ordinal()] = 9;
            } catch (NoSuchFieldError e59) {
            }
            try {
                iArr[EventType.SoundOff.ordinal()] = 32;
            } catch (NoSuchFieldError e60) {
            }
            try {
                iArr[EventType.SoundOn.ordinal()] = 31;
            } catch (NoSuchFieldError e61) {
            }
            try {
                iArr[EventType.StartExplode.ordinal()] = 64;
            } catch (NoSuchFieldError e62) {
            }
            try {
                iArr[EventType.StartToRecord.ordinal()] = 44;
            } catch (NoSuchFieldError e63) {
            }
            try {
                iArr[EventType.StaticEdgesCreated.ordinal()] = 4;
            } catch (NoSuchFieldError e64) {
            }
            try {
                iArr[EventType.StaticEdgesDestroyed.ordinal()] = 5;
            } catch (NoSuchFieldError e65) {
            }
            try {
                iArr[EventType.Tick.ordinal()] = 33;
            } catch (NoSuchFieldError e66) {
            }
            try {
                iArr[EventType.Triggered.ordinal()] = 35;
            } catch (NoSuchFieldError e67) {
            }
            try {
                iArr[EventType.Tutorial.ordinal()] = 27;
            } catch (NoSuchFieldError e68) {
            }
            try {
                iArr[EventType.UICommand.ordinal()] = 29;
            } catch (NoSuchFieldError e69) {
            }
            try {
                iArr[EventType.UploadLevel.ordinal()] = 56;
            } catch (NoSuchFieldError e70) {
            }
            try {
                iArr[EventType.Warning.ordinal()] = 60;
            } catch (NoSuchFieldError e71) {
            }
            $SWITCH_TABLE$com$camelgames$framework$events$EventType = iArr;
        }
        return iArr;
    }

    public static GameManager getInstance() {
        return instance;
    }

    private GameManager() {
    }

    public void initiate(MainActivity mainActivity2) {
        this.mainActivity = mainActivity2;
        getViews();
        if (!this.isInitiated) {
            addListeners();
            TouchManager.getInstace().add(this);
            createGround();
            setupPontiflex();
            this.isInitiated = true;
        }
        if (this.mode == null || this.mode.equals(Mode.MainMenu)) {
            mainMenu();
            tryLanchPontiflexRegister();
        } else if (isPaused()) {
        } else {
            if (isQuit()) {
                quit();
            } else if (!this.mode.equals(Mode.Playing)) {
                showAds(true);
            }
        }
    }

    public void HandleEvent(AbstractEvent e) {
        switch ($SWITCH_TABLE$com$camelgames$framework$events$EventType()[e.getType().ordinal()]) {
            case 13:
                LevelManager.getInstance().levelFinished();
                showGameOver(true);
                return;
            case 14:
                showGameOver(false);
                return;
            case 19:
                mainMenu();
                return;
            default:
                return;
        }
    }

    public void setPlaying() {
        this.mode = Mode.Playing;
    }

    public void setQuit() {
        this.mode = Mode.Quit;
    }

    public boolean isQuit() {
        return this.mode.equals(Mode.Quit);
    }

    public boolean isPlaying() {
        return this.mode.equals(Mode.Playing);
    }

    public boolean isPaused() {
        return this.mode.equals(Mode.Paused);
    }

    public boolean isGameFail() {
        return this.mode.equals(Mode.GameFail);
    }

    public boolean isGamePass() {
        return this.mode.equals(Mode.GamePass);
    }

    public void bringMenu() {
        if (isPlaying()) {
            this.mode = Mode.Paused;
            PlayingUI.getInstance().setPausing(true);
            showAds(true);
        } else if (isPaused()) {
            this.mode = Mode.Playing;
            PlayingUI.getInstance().setPausing(false);
            showAds(false);
        }
    }

    public void flushKeys(int[] keys, int keyCount) {
        for (int i = 0; i < keyCount; i++) {
            switch (keys[i]) {
                case 82:
                    bringMenu();
                    break;
            }
        }
    }

    public void startPlay(LevelScript levelScript) {
        if (levelScript != null) {
            setPlaying();
            clearScean();
            levelScript.load();
            updateBombPower();
            this.background = new Background(Integer.valueOf((int) R.drawable.background));
            PlayingUI.getInstance().startNewGame(this.targetLine * ((float) GraphicsManager.screenHeight()));
            PhysicsManager.instance.setStoped(false);
            GLScreenView.changeManipulator(BombManipulator.getInstance());
            PlayingUI.getInstance().setActive(true);
            showAds(false);
            if (LevelManager.getInstance().needShowTutorial().booleanValue()) {
                new Tutorial();
            }
        }
    }

    public Background getBackground() {
        return this.background;
    }

    public float getBombPower() {
        return this.bombPower;
    }

    public void showAds(final boolean show) {
        this.mainActivity.getHandler().post(new Runnable() {
            public void run() {
                if (show) {
                    GameManager.this.adsView.setVisibility(0);
                } else {
                    GameManager.this.adsView.setVisibility(8);
                }
            }
        });
    }

    private void updateBombPower() {
        this.bombPower = 1.0f - (0.3f * ((float) LevelManager.getInstance().getDifficulty()));
        float phyUnit = PhysicsManager.screenToPhysics(this.unitLength);
        this.bombPower *= 80.0f * phyUnit * phyUnit;
    }

    private void mainMenu() {
        this.mode = Mode.MainMenu;
        SoundManager.getInstance().stopSound();
        clearScean();
        this.mainActivity.getHandler().post(new Runnable() {
            public void run() {
                UIUtility.getMainAcitvity().startActivity(new Intent(UIUtility.getMainAcitvity(), MainMenuActivity.class));
            }
        });
    }

    private void showGameOver(boolean success) {
        if (success) {
            this.mode = Mode.GamePass;
        } else {
            this.mode = Mode.GameFail;
        }
        PlayingUI.getInstance().showGameOver(success);
        PhysicsManager.instance.setStoped(true);
    }

    private void quit() {
        this.mode = Mode.MainMenu;
        this.mainActivity.getHandler().post(new Runnable() {
            public void run() {
                GameManager.this.mainActivity.finish();
            }
        });
    }

    private void clearScean() {
        GLScreenView.changeManipulator(null);
        BombManipulator.getInstance().finish();
        PlayingUI.getInstance().setActive(false);
        EntityManager.getInstance().clearTemporary();
        PhysicsManager.instance.setStoped(true);
        EventManager.getInstance().cancelAllEvents();
        TouchManager.getInstace().cancelGestures();
        this.background = null;
    }

    public void setTargetLine(float targetLine2) {
        this.targetLine = targetLine2;
        switch ($SWITCH_TABLE$com$camelgames$explode$levels$LevelManager$DifficultyType()[LevelManager.getInstance().getDifficultyType().ordinal()]) {
            case 2:
                this.targetLine += 0.1f * ((((float) PlayingUI.groundY) / ((float) GraphicsManager.screenHeight())) - targetLine2);
                return;
            case 3:
                this.targetLine += 0.2f * ((((float) PlayingUI.groundY) / ((float) GraphicsManager.screenHeight())) - targetLine2);
                return;
            default:
                return;
        }
    }

    public float getTargetLine() {
        return this.targetLine;
    }

    public void setUnitLength(float unitLength2) {
        this.unitLength = unitLength2;
    }

    public float getUnitLength() {
        return this.unitLength;
    }

    public void setStandardThickUnit(int standardThickUnit2) {
        this.standardThickUnit = standardThickUnit2;
    }

    public int getStandardThickUnit() {
        return this.standardThickUnit;
    }

    public float getStandardThick() {
        return ((float) this.standardThickUnit) * this.unitLength;
    }

    public void setLevelGroundTopUnit(int levelGroundTopUnit2) {
        this.levelGroundTopUnit = levelGroundTopUnit2;
    }

    public float getGroundOfffset() {
        return ((float) PlayingUI.groundY) - (((float) this.levelGroundTopUnit) * this.unitLength);
    }

    private void createGround() {
        Edges edge = new Edges();
        edge.vertices = new float[]{0.0f, (float) PlayingUI.groundY, (float) GraphicsManager.screenWidth(), (float) PlayingUI.groundY};
        PhysicsManager.instance.addEdgesToGround(edge, 0.4f, 0.0f);
    }

    private void getViews() {
        this.adsView = this.mainActivity.findViewById(R.id.adview);
    }

    private void addListeners() {
        this.eventListenerUtil.addEventType(EventType.MainMenu);
        this.eventListenerUtil.addEventType(EventType.Restart);
        this.eventListenerUtil.addEventType(EventType.LevelFinished);
        this.eventListenerUtil.addEventType(EventType.LevelFailed);
        this.eventListenerUtil.addEventType(EventType.LoadLevel);
        this.eventListenerUtil.addEventType(EventType.SelectLevel);
        this.eventListenerUtil.addListener(this);
    }

    private void setupPontiflex() {
        this.mainActivity.getHandler().post(new Runnable() {
            public void run() {
                GameManager.this.adManager = AdManagerFactory.createInstance(GameManager.this.mainActivity.getApplication());
                if (!GameManager.this.mainActivity.isFinishing()) {
                    if (GameManager.this.adManager.hasValidRegistrationData()) {
                        GameManager.this.adManager.startMultiOfferActivity();
                    } else {
                        GameManager.this.adManager.startRegistrationActivity();
                    }
                    GameManager.launchCount = 0;
                }
            }
        });
    }

    public void tryLanchPontiflexRegister() {
        int i = launchCount + 1;
        launchCount = i;
        if (i == 2) {
            this.mainActivity.getHandler().post(new Runnable() {
                public void run() {
                    if (!GameManager.this.mainActivity.isFinishing()) {
                        if (GameManager.this.adManager.hasValidRegistrationData()) {
                            GameManager.this.adManager.startMultiOfferActivity();
                        }
                        GameManager.launchCount = 0;
                    }
                }
            });
        }
    }

    public void showPontiflexAds() {
        this.mainActivity.getHandler().post(new Runnable() {
            public void run() {
                if (!GameManager.this.mainActivity.isFinishing() && GameManager.this.adManager.hasValidRegistrationData()) {
                    GameManager.this.adManager.startMultiOfferActivity();
                    GameManager.launchCount = 0;
                }
            }
        });
    }
}
