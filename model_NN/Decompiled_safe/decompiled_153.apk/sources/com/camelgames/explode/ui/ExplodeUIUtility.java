package com.camelgames.explode.ui;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.camelgames.blowuplite.R;
import com.camelgames.explode.levels.LevelManager;
import com.camelgames.explode.score.ScoreManager;
import com.camelgames.framework.graphics.textures.TextureUtility;
import com.camelgames.framework.levels.LevelScript;
import com.camelgames.framework.levels.LevelScriptItem;
import com.camelgames.framework.ui.UIUtility;

public class ExplodeUIUtility extends UIUtility {
    public static void setupBackground(View rootView, ImageView topView, ImageView downView, ImageView logoView, ImageView starView) {
        rootView.setBackgroundDrawable(new BitmapDrawable(TextureUtility.getChopedBitmap((int) R.drawable.background, 0, 0, 800, 480)));
        int bottomBarThick = (int) (0.12f * ((float) UIUtility.getDisplayHeight()));
        int topBarThick = (int) ((((float) bottomBarThick) * 45.0f) / 36.0f);
        topView.setBackgroundResource(R.drawable.mainmenu_top);
        UIUtility.setSize(topView, UIUtility.getDisplayWidth(), topBarThick);
        UIUtility.setLeftTopForRLAbsolute(topView, 0, -1);
        int starSize = (int) (((float) topBarThick) * 0.9f);
        starView.setBackgroundDrawable(new BitmapDrawable(TextureUtility.getBitmap(Integer.valueOf((int) R.array.altas1_target))));
        UIUtility.setButtonSize(starView, starSize);
        UIUtility.setLeftTopForRLAbsolute(starView, UIUtility.getDisplayWidth(0.451f) - (starSize / 2), 0);
        starView.startAnimation(AnimationUtils.loadAnimation(UIUtility.getMainAcitvity(), R.anim.myanim));
        downView.setBackgroundDrawable(new BitmapDrawable(TextureUtility.getBitmap(Integer.valueOf((int) R.array.altas1_mainmenu_bottom))));
        UIUtility.setSize(downView, UIUtility.getDisplayWidth(), bottomBarThick);
        UIUtility.setLeftTopForRLAbsolute(downView, 0, (UIUtility.getDisplayHeight() - bottomBarThick) + 1);
        int logoHeight = (int) (0.6f * ((float) bottomBarThick));
        int logoWidth = logoHeight * 8;
        logoView.setBackgroundResource(R.drawable.logo);
        UIUtility.setSize(logoView, logoWidth, logoHeight);
        UIUtility.setLeftTopForRLAbsolute(logoView, UIUtility.getDisplayWidth() - logoWidth, UIUtility.getDisplayHeight() - ((bottomBarThick + logoHeight) / 2));
    }

    /* JADX INFO: Multiple debug info for r13v15 com.camelgames.explode.ui.LevelScreenshoter: [D('item' com.camelgames.framework.levels.LevelScriptItem), D('shoter' com.camelgames.explode.ui.LevelScreenshoter)] */
    public static Bitmap getScreenShoot(int levelIndex, int width, int height, Bitmap star, Bitmap questionMark) {
        boolean i;
        LevelScript levelScript = LevelManager.getInstance().getLevelScript(levelIndex);
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444);
        bitmap.setDensity(star.getDensity());
        Canvas canvas = new Canvas(bitmap);
        boolean isFullStar = false;
        int score = ScoreManager.getInstance().getScore(levelIndex, LevelManager.getInstance().getDifficulty());
        if (score > 0) {
            float rate = ((float) height) / ((float) UIUtility.getDisplayHeight());
            for (LevelScriptItem item : levelScript.getItems()) {
                if (item instanceof LevelScreenshoter) {
                    ((LevelScreenshoter) item).draw(canvas, rate);
                }
            }
            int startCount = LevelManager.scoreToStar(score);
            if (startCount == 5) {
                isFullStar = true;
            }
            for (int i2 = 0; i2 < startCount; i2++) {
                canvas.drawBitmap(star, ((float) width) - (((float) star.getWidth()) * (((float) i2) + 1.2f)), ((float) height) - (((float) star.getHeight()) * 1.1f), (Paint) null);
            }
            i = isFullStar;
        } else {
            canvas.drawBitmap(questionMark, 0.5f * ((float) (width - questionMark.getWidth())), 0.5f * ((float) (height - questionMark.getHeight())), (Paint) null);
            i = false;
        }
        String text = new StringBuilder(String.valueOf(levelIndex + 1)).toString();
        Paint paint = new Paint();
        paint.setTextSize(16.0f);
        paint.setAntiAlias(true);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        if (i) {
            paint.setColor(-256);
        } else if (score == 0) {
            paint.setColor(-1);
        } else {
            paint.setColor(-2511486);
        }
        float margin = 0.09f * ((float) height);
        canvas.drawText(text, (((float) width) - (((float) (text.length() * 16)) * 0.5f)) - margin, margin + 16.0f, paint);
        return bitmap;
    }
}
