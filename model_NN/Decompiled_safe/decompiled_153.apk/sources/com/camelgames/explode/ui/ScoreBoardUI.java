package com.camelgames.explode.ui;

import com.camelgames.blowuplite.R;
import com.camelgames.explode.GLScreenView;
import com.camelgames.explode.entities.FireworksManager;
import com.camelgames.explode.levels.LevelManager;
import com.camelgames.explode.score.ScoreManager;
import com.camelgames.framework.Manipulator;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.font.OESText;
import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;
import com.camelgames.framework.ui.buttons.ScaleButton;
import com.camelgames.ndk.graphics.OESSprite;
import java.util.ArrayList;
import java.util.Iterator;
import javax.microedition.khronos.opengles.GL10;

public class ScoreBoardUI extends Manipulator {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$ui$ScoreBoardUI$Status;
    private OESSprite backgroundTexture = new OESSprite();
    private final float buttonHeight;
    private final float buttonWidth;
    private ArrayList<ScaleButton> buttons = new ArrayList<>();
    private FireworksManager fireworksManager = new FireworksManager();
    private FormularBoard formularBoard;
    private OESSprite newRecordTexture;
    private ScaleButton nextButton;
    private ScaleButton playAgainButton;
    private ScaleButton quitButton;
    private int starCount;
    private OESSprite starTexture;
    private Status status = Status.MovingScoreBoard;
    private final int targetCenterX;
    private final int targetTop;

    private enum Status {
        MovingScoreBoard,
        MovingFormualrBoard,
        Finished
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$ui$ScoreBoardUI$Status() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$explode$ui$ScoreBoardUI$Status;
        if (iArr == null) {
            iArr = new int[Status.values().length];
            try {
                iArr[Status.Finished.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Status.MovingFormualrBoard.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Status.MovingScoreBoard.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$camelgames$explode$ui$ScoreBoardUI$Status = iArr;
        }
        return iArr;
    }

    public ScoreBoardUI(int centerX, int top, int width, int height) {
        this.backgroundTexture.setSize(width, height);
        this.backgroundTexture.setTexId(R.array.altas4_scoreboard);
        this.newRecordTexture = new OESSprite();
        this.newRecordTexture.setSize((int) (((float) width) * 0.3f), (int) (((float) height) * 0.3f));
        this.newRecordTexture.setTexId(R.array.altas1_record);
        this.targetCenterX = centerX;
        this.targetTop = top;
        this.buttonWidth = 0.35f * ((float) width);
        this.buttonHeight = this.buttonWidth * 0.45f;
        this.newRecordTexture.setPosition((int) (((float) this.targetCenterX) + (((float) width) * 0.45f)), (int) (((float) top) + (0.7f * ((float) this.newRecordTexture.getHeight()))));
        int starSize = (int) (this.buttonHeight * 0.8f);
        this.starTexture = new OESSprite();
        this.starTexture.setSize(starSize, starSize);
        this.starTexture.setTexId(R.array.altas4_star);
        createButtons(this.buttonWidth, this.buttonHeight);
        setCenterTop(centerX, top);
        int formularWidth = (int) (0.6f * ((float) width));
        this.formularBoard = new FormularBoard(this.targetCenterX - ((int) (((float) (formularWidth + width)) * 0.48f)), (int) ((0.175f * ((float) height)) + ((float) this.targetTop)), formularWidth, (int) (((float) formularWidth) * 0.8f));
    }

    private void createButtons(float buttonWidth2, float buttonHeight2) {
        this.nextButton = new ScaleButton();
        this.nextButton.setTexId(R.array.altas4_nextlevel);
        this.nextButton.setPosition(0.0f, 0.0f);
        this.nextButton.setSize(buttonWidth2, buttonHeight2);
        this.nextButton.setFunctionCallback(new Callback() {
            public void excute(UI ui) {
                EventManager.getInstance().postEvent(EventType.LevelUp);
            }
        });
        this.buttons.add(this.nextButton);
        this.playAgainButton = new ScaleButton();
        this.playAgainButton.setTexId(R.array.altas4_playagain);
        this.playAgainButton.setPosition(0.0f, 0.0f);
        this.playAgainButton.setSize(buttonWidth2, buttonHeight2);
        this.playAgainButton.setFunctionCallback(new Callback() {
            public void excute(UI ui) {
                EventManager.getInstance().postEvent(EventType.Restart);
            }
        });
        this.buttons.add(this.playAgainButton);
        this.quitButton = new ScaleButton();
        this.quitButton.setTexId(R.array.altas4_quit);
        this.quitButton.setPosition(0.0f, 0.0f);
        this.quitButton.setSize(buttonWidth2, buttonHeight2);
        this.quitButton.setFunctionCallback(new Callback() {
            public void excute(UI ui) {
                EventManager.getInstance().postEvent(EventType.MainMenu);
            }
        });
        this.buttons.add(this.quitButton);
    }

    public void setCenterTop(int centerX, int top) {
        int width = this.backgroundTexture.getWidth();
        int height = this.backgroundTexture.getHeight();
        this.backgroundTexture.setLeftTop(centerX - (width / 2), top);
        int top2 = (int) (((float) top) + (0.55f * ((float) height)));
        this.nextButton.setPosition(((float) centerX) - (this.buttonWidth * 0.6f), ((float) top2) + (this.buttonHeight / 2.0f));
        this.playAgainButton.setPosition(((float) centerX) + (this.buttonWidth * 0.6f), ((float) top2) + (this.buttonHeight / 2.0f));
        this.quitButton.setPosition((float) centerX, ((float) top2) + (this.buttonHeight * 1.6f));
    }

    public void setStoped(boolean isStoped) {
        super.setStoped(isStoped);
        if (!isStoped) {
            setCenterTop(this.targetCenterX, this.targetTop - this.backgroundTexture.getHeight());
            this.formularBoard.setInfos(LevelManager.getInstance().getHeightScore(), LevelManager.getInstance().getBonusScore(), LevelManager.getInstance().getDamageScore(), LevelManager.getInstance().getScore());
            this.fireworksManager.setActive(false);
            this.status = Status.MovingScoreBoard;
            this.starCount = 0;
            return;
        }
        this.formularBoard.setTextVisible(false);
        this.fireworksManager.setActive(false);
        this.status = Status.MovingScoreBoard;
        this.starCount = 0;
    }

    /* access modifiers changed from: protected */
    public void updateInternal(float elapsedTime) {
        switch ($SWITCH_TABLE$com$camelgames$explode$ui$ScoreBoardUI$Status()[this.status.ordinal()]) {
            case 1:
                int currentTop = this.backgroundTexture.getTop();
                if (currentTop == this.targetTop) {
                    this.formularBoard.setCenterTop(this.targetCenterX, this.formularBoard.getTargetTop());
                    this.status = Status.MovingFormualrBoard;
                    break;
                } else {
                    setCenterTop(this.targetCenterX, Math.min(this.targetTop, (int) (((float) currentTop) + (1.2f * elapsedTime * ((float) this.backgroundTexture.getHeight())))));
                    return;
                }
            case 2:
                if (this.formularBoard.isInTargetPosition()) {
                    this.formularBoard.setTextVisible(true);
                    this.starCount = LevelManager.getInstance().getStars();
                    if (ScoreManager.getInstance().isMadeNewRecord()) {
                        this.fireworksManager.fire();
                    }
                    this.status = Status.Finished;
                    break;
                }
                break;
        }
        this.formularBoard.update(elapsedTime);
        this.fireworksManager.update(elapsedTime);
    }

    public void render(GL10 gl, float elapsedTime) {
        if (!isStoped()) {
            if (!this.status.equals(Status.MovingScoreBoard)) {
                this.formularBoard.render(gl, elapsedTime);
            }
            this.backgroundTexture.drawOES();
            Iterator<ScaleButton> it = this.buttons.iterator();
            while (it.hasNext()) {
                it.next().render(gl, elapsedTime);
            }
            if (this.starCount > 0) {
                float starY = ((float) this.targetTop) + (((float) this.backgroundTexture.getHeight()) * 0.45f);
                float starX = ((float) this.targetCenterX) - ((0.5f * ((float) (this.starCount - 1))) * ((float) this.starTexture.getWidth()));
                for (int i = 0; i < this.starCount; i++) {
                    this.starTexture.setPosition((int) starX, (int) starY);
                    this.starTexture.drawOES();
                    starX += (float) this.starTexture.getWidth();
                }
                if (this.fireworksManager.isActive()) {
                    this.newRecordTexture.drawOES();
                    this.fireworksManager.render(gl, elapsedTime);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void touchDown(int x, int y) {
        Iterator<ScaleButton> it = this.buttons.iterator();
        while (it.hasNext()) {
            ScaleButton button = it.next();
            if (button.hitTest((float) x, (float) y)) {
                button.excute();
                return;
            }
        }
    }

    private static class FormularBoard {
        private OESSprite backgroundTexture = new OESSprite();
        private OESText bonusText = new OESText();
        private OESText damageText = new OESText();
        private final int fontSize = ((int) (20.0f * GraphicsManager.getInstance().getYScale()));
        private final float heightGap;
        private OESText heightText = new OESText();
        private final float rightMargin = ((float) (this.fontSize / 2));
        private OESText scoreText = new OESText();
        private final int targetCenterX;
        private final int targetTop;
        private ArrayList<OESText> texts = new ArrayList<>();
        private final float topMargin;

        public FormularBoard(int targetCenterX2, int targetTop2, int width, int height) {
            this.backgroundTexture.setSize(width, height);
            this.backgroundTexture.setTexId(R.array.altas4_board);
            this.targetCenterX = targetCenterX2;
            this.targetTop = targetTop2;
            this.topMargin = 0.14f * ((float) height);
            this.heightGap = (((float) height) - this.topMargin) / 4.5f;
            initiateText(this.heightText);
            initiateText(this.bonusText);
            initiateText(this.damageText);
            initiateText(this.scoreText);
            setTextVisible(false);
        }

        public void setTextVisible(boolean isVisible) {
            Iterator<OESText> it = this.texts.iterator();
            while (it.hasNext()) {
                it.next().setVisible(isVisible);
            }
        }

        public void setInfos(int heightScore, int bonusScore, int damageScore, int score) {
            this.heightText.setText(new StringBuilder().append(heightScore).toString());
            this.bonusText.setText(new StringBuilder().append(bonusScore).toString());
            if (damageScore < 0) {
                this.damageText.setText(String.valueOf(-damageScore) + "x0");
            } else {
                this.damageText.setText(new StringBuilder().append(damageScore).toString());
            }
            this.scoreText.setText(new StringBuilder().append(score).toString());
        }

        public void setCenterTop(int centerX, int top) {
            int width = this.backgroundTexture.getWidth();
            int right = (int) (((float) ((width / 2) + centerX)) - this.rightMargin);
            this.backgroundTexture.setLeftTop(centerX - (width / 2), top);
            int top2 = (int) (((float) top) + this.topMargin);
            this.heightText.setRightTop(right, top2);
            int top3 = (int) (((float) top2) + this.heightGap);
            this.bonusText.setRightTop(right, top3);
            int top4 = (int) (((float) top3) + this.heightGap);
            this.damageText.setRightTop(right, top4);
            this.scoreText.setRightTop(right, (int) (((float) top4) + this.heightGap));
        }

        public void update(float elapsedTime) {
            int currentX = this.backgroundTexture.getCenterX();
            if (currentX != this.targetCenterX) {
                setCenterTop(Math.max(this.targetCenterX, (int) (((float) currentX) - ((1.2f * elapsedTime) * ((float) this.backgroundTexture.getWidth())))), this.targetTop);
            }
        }

        public boolean isInTargetPosition() {
            return this.backgroundTexture.getCenterX() == this.targetCenterX;
        }

        public void render(GL10 gl, float elapsedTime) {
            this.backgroundTexture.drawOES();
        }

        public int getTargetTop() {
            return this.targetTop;
        }

        private void initiateText(OESText text) {
            text.setFontSize(this.fontSize);
            text.setColor(0.8509804f, 0.6784314f, 0.50980395f);
            GLScreenView.textBuilder.add(text);
            this.texts.add(text);
        }
    }
}
