package com.camelgames.explode.ui;

import com.camelgames.blowuplite.R;
import com.camelgames.explode.GLScreenView;
import com.camelgames.explode.entities.Bomb;
import com.camelgames.explode.entities.Line;
import com.camelgames.explode.game.GameManager;
import com.camelgames.explode.levels.LevelManager;
import com.camelgames.explode.manipulation.BombManipulator;
import com.camelgames.framework.Manipulator;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.Renderable;
import com.camelgames.framework.graphics.altas.AltasTextureManager;
import com.camelgames.framework.graphics.font.OESText;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.touch.TouchManager;
import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;
import com.camelgames.framework.ui.UIUtility;
import com.camelgames.framework.ui.buttons.ScaleButton;
import com.camelgames.ndk.graphics.OESSprite;
import com.camelgames.ndk.graphics.Sprite2D;
import javax.microedition.khronos.opengles.GL10;

public class PlayingUI extends Manipulator {
    public static final Vector2 bigBombDefaultPos = new Vector2(0.6f * ((float) GraphicsManager.screenWidth()), ((float) GraphicsManager.screenHeight()) * 0.95f);
    public static final float explodeMenuHeight = (explodeMenuWidth * AltasTextureManager.getInstance().getAltasResource(Integer.valueOf((int) R.array.altas1_switch)).getHeightWidthRate());
    public static final float explodeMenuWidth = (0.25f * ((float) GraphicsManager.screenHeight()));
    public static final int groundY = ((int) (0.88f * ((float) GraphicsManager.screenHeight())));
    private static PlayingUI instance = new PlayingUI();
    public static final Vector2 smallBombDefaultPos = new Vector2(0.4f * ((float) GraphicsManager.screenWidth()), ((float) GraphicsManager.screenHeight()) * 0.95f);
    public static final Vector2 switchDefaultPos = new Vector2(((float) GraphicsManager.screenWidth()) - (explodeMenuWidth * 0.5f), (((float) GraphicsManager.screenHeight()) - (explodeMenuHeight * 0.5f)) + 1.0f);
    private BombButton bigBombButton;
    private final int bigFontSize = UIUtility.getDisplayWidthPlusHeight(0.04f);
    /* access modifiers changed from: private */
    public BombConfigUI bombConfigUI;
    private final int fontSize = UIUtility.getDisplayWidthPlusHeight(0.03f);
    private GameFailedUI gameFailUI;
    private GamePausedUI gamePausedUI;
    private OESSprite ground;
    private OESText levelInfoDrawingText = new OESText();
    private BombButton magnentBombButton;
    private Manipulator[] manipulators = new Manipulator[4];
    private ScaleButton[] menuItems = new ScaleButton[5];
    private ScaleButton pauseButton;
    private ScoreBoardUI scoreBoardUI;
    private BombButton smallBombButton;
    private Line targetLine;
    private Line topLine;
    private OESText topLineHeightInfo = new OESText();

    public static PlayingUI getInstance() {
        return instance;
    }

    private PlayingUI() {
        setPriority(Renderable.PRIORITY.HIGHEST);
        initiateManipulators();
        initiateMenu();
        initiateTexts();
        initiateLines();
        int grounWidth = GraphicsManager.screenWidth();
        int groundHeight = (GraphicsManager.screenHeight() - groundY) + 2;
        this.ground = new OESSprite();
        this.ground.setSize(grounWidth, groundHeight);
        this.ground.setTexId(R.array.altas1_mainmenu_bottom);
        this.ground.setPosition(grounWidth / 2, (GraphicsManager.screenHeight() - (groundHeight / 2)) + 1);
    }

    private void initiateMenu() {
        ScaleButton explodeUI = new ScaleButton();
        explodeUI.setTexId(R.array.altas1_switch);
        explodeUI.setPosition(switchDefaultPos.X, switchDefaultPos.Y);
        explodeUI.setSize(explodeMenuWidth, explodeMenuHeight);
        explodeUI.setFunctionCallback(new Callback() {
            public void excute(UI ui) {
                EventManager.getInstance().postEvent(EventType.PressSwitch);
                if (BombManipulator.getInstance().isOperating()) {
                    BombManipulator.getInstance().explode();
                    PlayingUI.this.bombConfigUI.setStoped(true);
                }
            }
        });
        explodeUI.enableSound(false);
        this.menuItems[0] = explodeUI;
        this.smallBombButton = new BombButton(Bomb.Type.Small, R.array.altas1_bomb);
        this.bigBombButton = new BombButton(Bomb.Type.Big, R.array.altas1_bomb_big);
        this.magnentBombButton = new BombButton(Bomb.Type.Magnent, R.array.altas1_bomb_magnet);
        this.menuItems[1] = this.smallBombButton.getUIArea();
        this.menuItems[2] = this.bigBombButton.getUIArea();
        this.menuItems[3] = this.magnentBombButton.getUIArea();
        float pauseButtonWidth = Bomb.basicWidth * 1.5f;
        float pauseButtonHeight = 0.5f * pauseButtonWidth;
        this.pauseButton = new ScaleButton();
        this.pauseButton.setTexId(R.array.altas1_pause);
        this.pauseButton.setPosition(((float) GraphicsManager.screenWidth()) - pauseButtonHeight, 0.7f * pauseButtonHeight);
        this.pauseButton.setSize(pauseButtonWidth, pauseButtonHeight);
        this.pauseButton.setFunctionCallback(new Callback() {
            public void excute(UI ui) {
                GameManager.getInstance().bringMenu();
            }
        });
        this.pauseButton.setHitTestScale(2.0f);
        this.menuItems[4] = this.pauseButton;
    }

    private void initiateTexts() {
        this.levelInfoDrawingText.setLeftTop(this.bigFontSize / 4, ((int) smallBombDefaultPos.Y) - (this.bigFontSize / 2));
        this.levelInfoDrawingText.setFontSize(this.bigFontSize);
        this.levelInfoDrawingText.setColor(0.8509804f, 0.6784314f, 0.50980395f);
        GLScreenView.textBuilder.add(this.levelInfoDrawingText);
        this.topLineHeightInfo.setFontSize((int) (((float) this.fontSize) * 0.8f));
        GLScreenView.textBuilder.add(this.topLineHeightInfo);
    }

    public void startNewGame(float targetHeight) {
        this.targetLine.setStartEnd(new Vector2(0.0f, targetHeight), new Vector2((float) GraphicsManager.screenWidth(), targetHeight));
    }

    public void setPausing(boolean pausing) {
        this.gamePausedUI.setStoped(!pausing);
    }

    public float getHeightCredit() {
        return 1.0f - ((((float) groundY) - this.topLine.getY()) / (((float) groundY) - this.targetLine.getY()));
    }

    public boolean meetTargetLine() {
        return this.topLine.getY() >= this.targetLine.getY();
    }

    /* access modifiers changed from: protected */
    public void updateInternal(float elapsedTime) {
        for (Manipulator manipulator : this.manipulators) {
            manipulator.update(elapsedTime);
        }
        this.targetLine.update(elapsedTime);
        if (!BombManipulator.getInstance().isOperating()) {
            float top = BombManipulator.getInstance().getBricksTop();
            this.topLine.setY(top);
            this.topLine.update(elapsedTime);
            this.topLineHeightInfo.setText(HeightFT.getGameHeight(top));
            this.topLineHeightInfo.setLeftTop(0, ((int) GraphicsManager.worldToScreenY(top)) - this.topLineHeightInfo.getFontSize());
            if (meetTargetLine()) {
                this.topLine.setColor(0.0f, 1.0f, 0.0f);
                this.topLineHeightInfo.setColor(0.0f, 1.0f, 0.0f);
                return;
            }
            this.topLine.setColor(1.0f, 0.0f, 0.0f);
            this.topLineHeightInfo.setColor(1.0f, 0.0f, 0.0f);
        }
    }

    public void render(GL10 gl, float elapsedTime) {
        Sprite2D.flush(true);
        this.ground.drawOES();
        for (ScaleButton menuItem : this.menuItems) {
            menuItem.render(gl, 0.02f);
        }
        for (Manipulator manipulator : this.manipulators) {
            manipulator.render(gl, 0.02f);
        }
        GLScreenView.textBuilder.render(gl, elapsedTime);
        BombManipulator.getInstance().drawMagnifier(gl, elapsedTime);
    }

    public void renderLines(GL10 gl, float elapsedTime) {
        this.targetLine.render(gl, elapsedTime);
        if (!BombManipulator.getInstance().isOperatingStatus()) {
            this.topLine.render(gl, elapsedTime);
        }
    }

    public void setSmallBombCount(int count) {
        this.smallBombButton.setBombCount(count);
    }

    public void setBigBombCount(int count) {
        this.bigBombButton.setBombCount(count);
    }

    public void setMagnentBombCount(int count) {
        this.magnentBombButton.setBombCount(count);
    }

    public void setActive(boolean isActive) {
        if (isActive) {
            int position = setBombButtonPosition(this.magnentBombButton, setBombButtonPosition(this.bigBombButton, setBombButtonPosition(this.smallBombButton, 0)));
            this.topLineHeightInfo.setText(null);
            this.levelInfoDrawingText.setText(LevelManager.getInstance().getLevelInfo());
            for (Manipulator manipulator : this.manipulators) {
                manipulator.setStoped(true);
            }
            TouchManager.getInstace().add(this);
            setVisible(true);
            return;
        }
        TouchManager.getInstace().remove(this);
        setVisible(false);
    }

    private static int setBombButtonPosition(BombButton bombButton, int position) {
        if (!bombButton.isActive()) {
            return position;
        }
        switch (position) {
            case 0:
                bombButton.setPosition(smallBombDefaultPos);
                return position + 1;
            case 1:
                bombButton.setPosition(bigBombDefaultPos);
                return position + 1;
            default:
                return position;
        }
    }

    public boolean hitTest(float x, float y) {
        if (!this.bombConfigUI.isStoped()) {
            if (this.bombConfigUI.hitTest(x, y)) {
                return true;
            }
            this.bombConfigUI.setStoped(true);
        }
        for (ScaleButton item : this.menuItems) {
            if (item.isActive() && item.hitTest(x, y)) {
                return true;
            }
        }
        return false;
    }

    public void showBombConfig(float tapX, float tapY, Bomb bomb) {
        if (bomb == null) {
            return;
        }
        if (this.bombConfigUI.isStoped() || !this.bombConfigUI.hitTest(tapX, tapY)) {
            this.bombConfigUI.setStoped(false);
            this.bombConfigUI.bindBomb(bomb);
        }
    }

    public boolean isShowingBombConfigUI() {
        return !this.bombConfigUI.isStoped();
    }

    public void showGameOver(boolean success) {
        if (success) {
            this.scoreBoardUI.setStoped(false);
        } else {
            this.gameFailUI.setStoped(false);
        }
    }

    private static class HeightFT {
        private static final char[] numbers = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        private static char[] str = {' ', ' ', '.', ' ', 'F', 'T'};

        private HeightFT() {
        }

        public static String getGameHeight(float y) {
            int prefixInt10 = Math.min(99, Math.max(1, ((int) ((500.0f * (((float) PlayingUI.groundY) - y)) / ((float) GraphicsManager.screenHeight()))) / 10));
            int prefixInt1 = prefixInt10 % 10;
            int prefixInt102 = prefixInt10 / 10;
            int postfixInt = prefixInt102 % 10;
            if (prefixInt102 == 0) {
                str[0] = ' ';
            } else {
                str[0] = numbers[prefixInt102];
            }
            str[1] = numbers[prefixInt1];
            str[3] = numbers[postfixInt];
            return new String(str);
        }
    }

    /* access modifiers changed from: protected */
    public void touchDown(int x, int y) {
        ScaleButton[] scaleButtonArr = this.menuItems;
        int length = scaleButtonArr.length;
        int i = 0;
        while (true) {
            if (i < length) {
                ScaleButton item = scaleButtonArr[i];
                if (item.isActive() && item.hitTest((float) x, (float) y)) {
                    item.excute();
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        for (Manipulator manipulator : this.manipulators) {
            if (!manipulator.isStoped()) {
                manipulator.touchAction(x, y, 0);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void touchMove(int x, int y) {
        for (Manipulator manipulator : this.manipulators) {
            if (!manipulator.isStoped()) {
                manipulator.touchAction(x, y, 2);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void touchUp(int x, int y) {
        for (Manipulator manipulator : this.manipulators) {
            if (!manipulator.isStoped()) {
                manipulator.touchAction(x, y, 1);
                return;
            }
        }
    }

    private void initiateLines() {
        float unitLength = 0.06f * ((float) GraphicsManager.screenHeight());
        this.targetLine = new Line(0.3f * unitLength, unitLength, R.drawable.line_knife_dash);
        this.targetLine.setSpeed(0.5f);
        this.topLine = new Line(0.3f * unitLength, 0.7f * unitLength, R.drawable.line_starwar);
        this.topLine.setColor(1.0f, 0.0f, 0.0f);
        this.topLine.setAlpha(0.5f);
        this.topLine.setStartEnd(new Vector2(0.0f, -2.0f), new Vector2((float) GraphicsManager.screenWidth(), -2.0f));
    }

    private void initiateManipulators() {
        this.bombConfigUI = new BombConfigUI((int) (((float) GraphicsManager.screenHeight()) * 0.4f));
        this.manipulators[0] = this.bombConfigUI;
        this.scoreBoardUI = new ScoreBoardUI((int) (((float) GraphicsManager.screenWidth()) * 0.65f), 0, (int) (((float) GraphicsManager.screenHeight()) * 0.83f), (int) (((float) GraphicsManager.screenHeight()) * 0.83f));
        this.manipulators[1] = this.scoreBoardUI;
        this.gameFailUI = new GameFailedUI((int) (((float) GraphicsManager.screenWidth()) * 0.5f), (int) (((float) GraphicsManager.screenHeight()) * 0.3f), (int) (((float) GraphicsManager.screenWidth()) * 0.5f), (int) (((float) GraphicsManager.screenWidth()) * 0.3f), "Failed!");
        this.manipulators[2] = this.gameFailUI;
        this.gamePausedUI = new GamePausedUI((int) (((float) GraphicsManager.screenWidth()) * 0.5f), (int) (((float) GraphicsManager.screenHeight()) * 0.3f), (int) (((float) GraphicsManager.screenWidth()) * 0.5f), (int) (((float) GraphicsManager.screenWidth()) * 0.3f), "Paused");
        this.manipulators[3] = this.gamePausedUI;
    }
}
