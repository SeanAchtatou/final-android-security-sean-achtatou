package com.camelgames.explode.ui;

public interface OnSelectedListener {
    void onSelected(int i);
}
