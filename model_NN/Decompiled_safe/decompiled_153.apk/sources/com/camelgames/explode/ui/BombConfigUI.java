package com.camelgames.explode.ui;

import com.camelgames.blowuplite.R;
import com.camelgames.explode.GLScreenView;
import com.camelgames.explode.entities.Bomb;
import com.camelgames.framework.Manipulator;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.font.OESText;
import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;
import com.camelgames.framework.ui.buttons.ScaleButton;
import com.camelgames.ndk.graphics.OESSprite;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import javax.microedition.khronos.opengles.GL10;

public class BombConfigUI extends Manipulator {
    private static final float delayUnit = 0.25f;
    private static final DecimalFormat formator = new DecimalFormat("0.00");
    private static final float maxDelay = 5.0f;
    private static final float minDelay = 0.0f;
    private OESSprite backgroundTexture = new OESSprite();
    private Bomb bomb;
    private final float buttonOffsetX;
    private final float buttonOffsetY;
    private final float buttonWidth;
    private ArrayList<ScaleButton> buttons = new ArrayList<>();
    private ScaleButton capturedButton;
    private float capturedTime;
    private ScaleButton decreaseButton;
    private OESText delayText = new OESText();
    private ScaleButton increaseButton;
    private float x;
    private float y;

    public BombConfigUI(int width) {
        this.backgroundTexture.setTexId(R.array.altas1_bomb_config_bk);
        this.backgroundTexture.setWidthConstrainProportion((float) width);
        int height = this.backgroundTexture.getHeight();
        this.buttonWidth = 0.36f * ((float) width);
        float buttonHeight = this.buttonWidth;
        this.buttonOffsetX = (((float) width) - (this.buttonWidth * 1.6f)) * 0.5f;
        this.buttonOffsetY = (((float) height) - (buttonHeight * 1.6f)) * 0.5f;
        this.decreaseButton = new ScaleButton();
        this.decreaseButton.setTexId(R.array.altas1_bomb_config_down);
        this.decreaseButton.setPosition(this.x - (this.buttonOffsetX * 1.1f), this.y + this.buttonOffsetY);
        this.decreaseButton.setSize(this.buttonWidth, buttonHeight);
        this.decreaseButton.setFunctionCallback(new Callback() {
            public void excute(UI ui) {
                BombConfigUI.this.setDelay(BombConfigUI.this.getDelay() - BombConfigUI.delayUnit);
            }
        });
        this.decreaseButton.setTriggerTime(0.01f);
        this.decreaseButton.enableSound(false);
        this.buttons.add(this.decreaseButton);
        this.increaseButton = new ScaleButton();
        this.increaseButton.setTexId(R.array.altas1_bomb_config_up);
        this.increaseButton.setPosition(this.x + (this.buttonOffsetX * 0.9f), this.y + this.buttonOffsetY);
        this.increaseButton.setSize(this.buttonWidth, buttonHeight);
        this.increaseButton.setFunctionCallback(new Callback() {
            public void excute(UI area) {
                BombConfigUI.this.setDelay(BombConfigUI.this.getDelay() + BombConfigUI.delayUnit);
            }
        });
        this.increaseButton.setTriggerTime(0.01f);
        this.increaseButton.enableSound(false);
        this.buttons.add(this.increaseButton);
        this.delayText.setFontSize((int) (0.2f * ((float) width)));
        GLScreenView.textBuilder.add(this.delayText);
    }

    public void setPosition(float x2, float y2) {
        this.x = x2;
        this.y = y2;
        this.backgroundTexture.setPosition((int) x2, (int) y2);
        this.decreaseButton.setPosition(x2 - (this.buttonOffsetX * 1.1f), this.buttonOffsetY + y2);
        this.increaseButton.setPosition((this.buttonOffsetX * 0.9f) + x2, this.buttonOffsetY + y2);
        this.delayText.setLeftTop((int) (x2 - (((float) this.delayText.getTextWidth()) * 0.5f)), (int) (y2 - (((float) this.delayText.getFontSize()) * 1.3f)));
    }

    public boolean hitTest(float x2, float y2) {
        return this.backgroundTexture.hitTest(x2, y2);
    }

    public void setStoped(boolean isStoped) {
        super.setStoped(isStoped);
        this.delayText.setVisible(!isStoped);
        this.capturedButton = null;
        this.capturedTime = 0.0f;
        this.bomb = null;
    }

    public void render(GL10 gl, float elapsedTime) {
        if (!isStoped()) {
            this.backgroundTexture.drawOES();
            Iterator<ScaleButton> it = this.buttons.iterator();
            while (it.hasNext()) {
                it.next().render(gl, elapsedTime);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void updateInternal(float elapsedTime) {
        if (this.capturedButton != null) {
            this.capturedTime += elapsedTime;
            if (this.capturedTime > 0.02f) {
                this.capturedButton.excute();
                this.capturedTime = 0.0f;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void touchDown(int x2, int y2) {
        Iterator<ScaleButton> it = this.buttons.iterator();
        while (it.hasNext()) {
            ScaleButton button = it.next();
            if (button.hitTest((float) x2, (float) y2)) {
                captureButton(button);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void touchMove(int x2, int y2) {
    }

    /* access modifiers changed from: protected */
    public void touchUp(int x2, int y2) {
        captureButton(null);
    }

    private void captureButton(ScaleButton capturedButton2) {
        if (this.capturedButton != null && capturedButton2 == null) {
            this.capturedButton.excute();
        }
        this.capturedButton = capturedButton2;
        this.capturedTime = -0.5f;
    }

    public void bindBomb(Bomb bomb2) {
        this.bomb = bomb2;
        if (bomb2 != null) {
            updateDelayText();
            setPosition(GraphicsManager.worldToScreenX(bomb2.getX()), GraphicsManager.worldToScreenY(bomb2.getY()));
        }
    }

    private void updateDelayText() {
        String text = formator.format((double) getDelay());
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            stringBuilder.append((char) (text.charAt(i) + '`'));
        }
        stringBuilder.setCharAt(1, 154);
        this.delayText.setText(stringBuilder.toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* access modifiers changed from: private */
    public void setDelay(float delay) {
        if (this.bomb != null) {
            float delay2 = Math.max(0.0f, Math.min((float) maxDelay, delay));
            if (delay2 != getDelay()) {
                this.bomb.setDelayConfig(delay2);
                updateDelayText();
                EventManager.getInstance().postEvent(EventType.Tick);
            }
        }
    }

    /* access modifiers changed from: private */
    public float getDelay() {
        if (this.bomb != null) {
            return this.bomb.getDelayConfig();
        }
        return 0.0f;
    }
}
