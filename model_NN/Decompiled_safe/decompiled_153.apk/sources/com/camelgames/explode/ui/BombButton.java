package com.camelgames.explode.ui;

import com.camelgames.explode.GLScreenView;
import com.camelgames.explode.entities.Bomb;
import com.camelgames.explode.manipulation.BombManipulator;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.font.OESText;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;
import com.camelgames.framework.ui.UIUtility;
import com.camelgames.framework.ui.buttons.ScaleButton;

public class BombButton {
    /* access modifiers changed from: private */
    public final Bomb.Type bombType;
    private final int fontSize = UIUtility.getDisplayWidthPlusHeight(0.03f);
    private ScaleButton smallBomb;
    /* access modifiers changed from: private */
    public Vector2 smallBombDefaultPos;
    private OESText smallBombInfo = new OESText();

    public BombButton(Bomb.Type type, int resId) {
        this.bombType = type;
        this.smallBomb = new ScaleButton();
        this.smallBomb.setTexId(resId);
        this.smallBomb.setPosition(0.0f, 0.0f);
        this.smallBomb.setSize(Bomb.basicWidth, Bomb.basicWidth);
        this.smallBomb.setFunctionCallback(new Callback() {
            public void excute(UI ui) {
                Bomb bomb;
                if (BombManipulator.getInstance().isOperating() && (bomb = BombManipulator.getInstance().pollBomb(BombButton.this.bombType)) != null) {
                    bomb.setVisible(true);
                    bomb.setPosition(GraphicsManager.screenToWorldX((float) ((int) BombButton.this.smallBombDefaultPos.X)), GraphicsManager.screenToWorldY((float) ((int) BombButton.this.smallBombDefaultPos.Y)));
                    BombManipulator.getInstance().captureBomb(bomb);
                }
            }
        });
        this.smallBomb.setHitTestScale(1.4f);
        this.smallBomb.setTriggerTime(0.0f);
        this.smallBomb.enableSound(false);
        this.smallBombInfo.setFontSize(this.fontSize);
        this.smallBombInfo.setColor(0.8509804f, 0.6784314f, 0.50980395f);
        GLScreenView.textBuilder.add(this.smallBombInfo);
    }

    public void setPosition(Vector2 smallBombDefaultPos2) {
        this.smallBombDefaultPos = smallBombDefaultPos2;
        this.smallBomb.setPosition(smallBombDefaultPos2.X, smallBombDefaultPos2.Y);
        this.smallBombInfo.setLeftTop((int) (smallBombDefaultPos2.X + (Bomb.basicWidth * 0.6f)), ((int) smallBombDefaultPos2.Y) - (this.fontSize / 2));
    }

    public void setBombCount(int count) {
        if (count > 0) {
            this.smallBombInfo.setText("x " + count);
            this.smallBomb.setActive(true);
            return;
        }
        this.smallBombInfo.setText("");
        this.smallBomb.setActive(false);
    }

    public ScaleButton getUIArea() {
        return this.smallBomb;
    }

    public boolean isActive() {
        return this.smallBomb.isActive();
    }
}
