package com.camelgames.explode.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import com.camelgames.blowuplite.R;
import com.camelgames.explode.game.GameManager;
import com.camelgames.explode.levels.LevelManager;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.LoadLevelEvent;
import com.camelgames.framework.graphics.textures.TextureUtility;
import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UIUtility;

public class LevelSelectorView extends RelativeLayout {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$levels$LevelManager$DifficultyType;
    private static final int density;
    /* access modifiers changed from: private */
    public BitmapDrawable board;
    private int buttonHeight;
    private View buttonLeft;
    private View buttonRight;
    private int buttonWidth;
    /* access modifiers changed from: private */
    public Callback callback;
    /* access modifiers changed from: private */
    public final int cellHeight = UIUtility.getDisplayHeight(0.22f);
    /* access modifiers changed from: private */
    public final int cellWidth = ((int) (((float) this.cellHeight) * UIUtility.getWidthHeightRate()));
    /* access modifiers changed from: private */
    public GridView gridView;
    private final int levelPerPage = 9;
    /* access modifiers changed from: private */
    public int pageIndex = -1;
    /* access modifiers changed from: private */
    public Bitmap questionMark;
    private ImageView selector;
    /* access modifiers changed from: private */
    public Bitmap star;

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$levels$LevelManager$DifficultyType() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$explode$levels$LevelManager$DifficultyType;
        if (iArr == null) {
            iArr = new int[LevelManager.DifficultyType.values().length];
            try {
                iArr[LevelManager.DifficultyType.Easy.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[LevelManager.DifficultyType.Hard.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[LevelManager.DifficultyType.Medium.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$camelgames$explode$levels$LevelManager$DifficultyType = iArr;
        }
        return iArr;
    }

    static {
        DisplayMetrics dm = new DisplayMetrics();
        UIUtility.getMainAcitvity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        density = (int) dm.xdpi;
    }

    public LevelSelectorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initiate();
    }

    public LevelSelectorView(Context context) {
        super(context);
        initiate();
    }

    private void initiate() {
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.levelselector_view, this);
        this.gridView = (GridView) findViewById(R.id.grid);
        setButtonsListener();
        initiateBitmaps();
        putSelector(LevelManager.getInstance().getDifficultyType());
        refresh(LevelManager.getInstance().getCurrentLevelIndx() / 9);
    }

    public void setBackCallback(Callback callback2) {
        this.callback = callback2;
    }

    public void refresh(int pageIndex2) {
        int maxLevelCount = LevelManager.getInstance().getLevelCount();
        int pageIndex3 = Math.max(0, Math.min((maxLevelCount - 1) / 9, pageIndex2));
        this.pageIndex = pageIndex3;
        setInfo(-1, pageIndex3 * 9, Math.min(9, maxLevelCount - (pageIndex3 * 9)), new OnSelectedListener() {
            public void onSelected(int position) {
                EventManager.getInstance().postEvent(new LoadLevelEvent(position));
                GameManager.getInstance().setPlaying();
                ((Activity) LevelSelectorView.this.getContext()).finish();
            }
        });
        this.buttonLeft.setVisibility(0);
        this.buttonRight.setVisibility(0);
        if (pageIndex3 == 0) {
            this.buttonLeft.setVisibility(8);
        } else if (pageIndex3 == maxLevelCount / 9) {
            this.buttonRight.setVisibility(8);
        }
    }

    private void setButtonsListener() {
        this.selector = (ImageView) findViewById(R.id.selector);
        int sideButtonHeight = (int) (((float) UIUtility.getDisplayHeight()) * 0.35f);
        this.buttonLeft = findViewById(R.id.button_left);
        UIUtility.setButtonSize(this.buttonLeft, sideButtonHeight);
        UIUtility.setLeftTopForRLAbsolute(this.buttonLeft, 0, UIUtility.getDisplayHeight(0.53f));
        this.buttonLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LevelSelectorView.this.refresh(LevelSelectorView.this.pageIndex - 1);
            }
        });
        this.buttonRight = findViewById(R.id.button_right);
        UIUtility.setButtonSize(this.buttonRight, sideButtonHeight);
        UIUtility.setLeftTopForRLAbsolute(this.buttonRight, UIUtility.getDisplayWidth() - UIUtility.getWidth(this.buttonRight), UIUtility.getDisplayHeight(0.53f));
        this.buttonRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LevelSelectorView.this.refresh(LevelSelectorView.this.pageIndex + 1);
            }
        });
        View buttonEasy = findViewById(R.id.button_easy);
        this.buttonHeight = (int) (((float) UIUtility.getDisplayHeight()) * 0.13f);
        UIUtility.setButtonSize(buttonEasy, this.buttonHeight);
        this.buttonWidth = UIUtility.getWidth(buttonEasy);
        int buttonTop = (int) (((float) this.buttonHeight) * 1.0f);
        UIUtility.setLeftTopForRLAbsolute(buttonEasy, UIUtility.getDisplayWidth() - this.buttonWidth, buttonTop);
        buttonEasy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LevelSelectorView.this.putSelector(LevelManager.DifficultyType.Easy);
            }
        });
        int buttonTop2 = buttonTop + this.buttonHeight;
        View buttonMedium = findViewById(R.id.button_medium);
        UIUtility.setButtonSize(buttonMedium, this.buttonHeight);
        UIUtility.setLeftTopForRLAbsolute(buttonMedium, UIUtility.getDisplayWidth() - this.buttonWidth, buttonTop2);
        buttonMedium.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LevelSelectorView.this.putSelector(LevelManager.DifficultyType.Medium);
            }
        });
        int buttonTop3 = buttonTop2 + this.buttonHeight;
        View buttonHard = findViewById(R.id.button_hard);
        UIUtility.setButtonSize(buttonHard, this.buttonHeight);
        UIUtility.setLeftTopForRLAbsolute(buttonHard, UIUtility.getDisplayWidth() - this.buttonWidth, buttonTop3);
        buttonHard.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LevelSelectorView.this.putSelector(LevelManager.DifficultyType.Hard);
            }
        });
        View buttonBack = findViewById(R.id.button_back);
        UIUtility.setButtonSize(buttonBack, this.buttonHeight);
        UIUtility.setLeftTopForRLAbsolute(buttonBack, 0, (int) (((float) this.buttonHeight) * 1.0f));
        buttonBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LevelSelectorView.this.callback.excute(null);
            }
        });
    }

    private void initiateBitmaps() {
        this.star = TextureUtility.getScaledBitmap((int) R.drawable.star, (int) (((float) this.cellWidth) * 0.15f), (int) (((float) this.cellWidth) * 0.15f));
        this.star.setDensity(density);
        this.questionMark = TextureUtility.getScaledBitmap((int) R.drawable.questionmark, (int) (0.4f * ((float) this.cellHeight)), (int) (0.8f * ((float) this.cellHeight)));
        this.questionMark.setDensity(density);
        Bitmap boardBp = TextureUtility.getScaledBitmap((int) R.drawable.board, this.cellWidth, this.cellHeight);
        boardBp.setDensity(density);
        this.board = new BitmapDrawable(boardBp);
    }

    private void setInfo(int backgroundId, final int startIndex, int cellCount, final OnSelectedListener listener) {
        if (backgroundId > 0) {
            setBackgroundResource(backgroundId);
        } else {
            setBackgroundColor(0);
        }
        this.gridView.setNumColumns(3);
        UIUtility.setSize(this.gridView, (int) (((float) this.cellWidth) * 3.2f), (int) (((float) this.cellHeight) * 3.2f));
        this.gridView.setAdapter((ListAdapter) new ImageAdapter(getContext(), startIndex, cellCount, new View.OnClickListener() {
            public void onClick(View v) {
                int position = LevelSelectorView.this.gridView.getPositionForView(v);
                if (listener != null) {
                    listener.onSelected(startIndex + position);
                }
            }
        }));
    }

    /* access modifiers changed from: private */
    public void putSelector(LevelManager.DifficultyType type) {
        int left = UIUtility.getDisplayWidth() - this.buttonWidth;
        int top = 0;
        switch ($SWITCH_TABLE$com$camelgames$explode$levels$LevelManager$DifficultyType()[type.ordinal()]) {
            case 1:
                top = (int) (1.0f * ((float) this.buttonHeight));
                break;
            case 2:
                top = (int) (2.0f * ((float) this.buttonHeight));
                break;
            case 3:
                top = (int) (3.0f * ((float) this.buttonHeight));
                break;
        }
        UIUtility.setLeftTopForRLAbsolute(this.selector, left, top);
        this.selector.requestLayout();
        if (!LevelManager.getInstance().getDifficultyType().equals(type)) {
            LevelManager.getInstance().setDifficultyType(type);
            refresh(this.pageIndex);
        }
    }

    private class ImageAdapter extends BaseAdapter {
        private Context context;
        private int count;
        private View.OnClickListener listener;
        private int startIndex;

        public ImageAdapter(Context context2, int startIndex2, int count2, View.OnClickListener listener2) {
            this.context = context2;
            this.startIndex = startIndex2;
            this.count = count2;
            this.listener = listener2;
        }

        public int getCount() {
            return this.count;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageButton item = (ImageButton) convertView;
            if (convertView != null) {
                return item;
            }
            ImageButton item2 = new ImageButton(this.context);
            item2.setBackgroundDrawable(LevelSelectorView.this.board);
            item2.setImageBitmap(ExplodeUIUtility.getScreenShoot(this.startIndex + position, LevelSelectorView.this.cellWidth, LevelSelectorView.this.cellHeight, LevelSelectorView.this.star, LevelSelectorView.this.questionMark));
            item2.setLayoutParams(new AbsListView.LayoutParams(LevelSelectorView.this.cellWidth, LevelSelectorView.this.cellHeight));
            item2.setOnClickListener(this.listener);
            return item2;
        }
    }
}
