package com.camelgames.explode.ui;

import android.graphics.Canvas;

public interface LevelScreenshoter {
    void draw(Canvas canvas, float f);
}
