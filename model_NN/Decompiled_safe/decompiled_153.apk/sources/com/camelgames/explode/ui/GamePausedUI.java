package com.camelgames.explode.ui;

import com.camelgames.blowuplite.R;
import com.camelgames.explode.GLScreenView;
import com.camelgames.explode.sound.SoundManager;
import com.camelgames.framework.Manipulator;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.graphics.font.OESText;
import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;
import com.camelgames.framework.ui.buttons.ScaleButton;
import java.util.ArrayList;
import java.util.Iterator;
import javax.microedition.khronos.opengles.GL10;

public class GamePausedUI extends Manipulator {
    private ArrayList<ScaleButton> buttons = new ArrayList<>();
    private ScaleButton soundOffButton;
    private ScaleButton soundOnButton;
    private OESText text = new OESText();

    public GamePausedUI(int centerX, int top, int width, int height, String str) {
        float buttonWidth = 0.4f * ((float) width);
        float buttonHeight = buttonWidth * 0.42f;
        float buttonY = ((float) top) + (0.7f * ((float) height));
        float buttonGap = buttonWidth;
        ScaleButton playAgainButton = new ScaleButton();
        playAgainButton.setTexId(R.array.altas4_playagain);
        playAgainButton.setPosition(((float) centerX) - buttonGap, buttonY);
        playAgainButton.setSize(buttonWidth, buttonHeight);
        playAgainButton.setFunctionCallback(new Callback() {
            public void excute(UI ui) {
                EventManager.getInstance().postEvent(EventType.Restart);
            }
        });
        this.buttons.add(playAgainButton);
        ScaleButton quitButton = new ScaleButton();
        quitButton.setTexId(R.array.altas4_quit);
        quitButton.setPosition(((float) centerX) + buttonGap, buttonY);
        quitButton.setSize(buttonWidth, buttonHeight);
        quitButton.setFunctionCallback(new Callback() {
            public void excute(UI ui) {
                EventManager.getInstance().postEvent(EventType.MainMenu);
            }
        });
        this.buttons.add(quitButton);
        this.soundOnButton = new ScaleButton();
        this.soundOnButton.setTexId(R.array.altas4_soundon);
        this.soundOnButton.setPosition((float) centerX, buttonY);
        this.soundOnButton.setSize(1.39f * buttonHeight, buttonHeight);
        this.soundOnButton.getTexture().setHeightConstrainProportion(buttonHeight);
        this.soundOnButton.setFunctionCallback(new Callback() {
            public void excute(UI ui) {
                SoundManager.getInstance().setMute(true);
                GamePausedUI.this.updateSoundButton();
            }
        });
        this.buttons.add(this.soundOnButton);
        this.soundOffButton = new ScaleButton();
        this.soundOffButton.setTexId(R.array.altas4_soundoff);
        this.soundOffButton.setPosition((float) centerX, buttonY);
        this.soundOffButton.setSize(1.39f * buttonHeight, buttonHeight);
        this.soundOffButton.getTexture().setHeightConstrainProportion(buttonHeight);
        this.soundOffButton.setFunctionCallback(new Callback() {
            public void excute(UI ui) {
                SoundManager.getInstance().setMute(false);
                GamePausedUI.this.updateSoundButton();
            }
        });
        this.buttons.add(this.soundOffButton);
        this.text.setText(str);
        this.text.setFontSize((int) (1.2f * buttonHeight));
        this.text.setCenterTop(centerX, ((int) (0.15f * ((float) height))) + top);
        this.text.setColor(0.8509804f, 0.6784314f, 0.50980395f);
        this.text.setVisible(false);
        GLScreenView.textBuilder.add(this.text);
    }

    public void setText(String text2) {
    }

    public void setStoped(boolean isStoped) {
        super.setStoped(isStoped);
        this.text.setVisible(!isStoped);
        updateSoundButton();
    }

    public void render(GL10 gl, float elapsedTime) {
        if (!isStoped()) {
            Iterator<ScaleButton> it = this.buttons.iterator();
            while (it.hasNext()) {
                it.next().render(gl, elapsedTime);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void touchDown(int x, int y) {
        Iterator<ScaleButton> it = this.buttons.iterator();
        while (it.hasNext()) {
            ScaleButton button = it.next();
            if (button.isActive() && button.hitTest((float) x, (float) y)) {
                button.excute();
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateSoundButton() {
        if (SoundManager.getInstance().isMute()) {
            this.soundOnButton.setActive(false);
            this.soundOffButton.setActive(true);
            return;
        }
        this.soundOnButton.setActive(true);
        this.soundOffButton.setActive(false);
    }
}
