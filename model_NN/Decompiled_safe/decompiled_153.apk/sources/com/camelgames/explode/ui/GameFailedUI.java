package com.camelgames.explode.ui;

import com.camelgames.blowuplite.R;
import com.camelgames.explode.GLScreenView;
import com.camelgames.framework.Manipulator;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.graphics.font.OESText;
import com.camelgames.framework.ui.Callback;
import com.camelgames.framework.ui.UI;
import com.camelgames.framework.ui.buttons.ScaleButton;
import com.camelgames.ndk.graphics.OESSprite;
import java.util.ArrayList;
import java.util.Iterator;
import javax.microedition.khronos.opengles.GL10;

public class GameFailedUI extends Manipulator {
    private OESSprite backgroundTexture = new OESSprite();
    private ArrayList<ScaleButton> buttons = new ArrayList<>();
    private OESText text = new OESText();

    public GameFailedUI(int centerX, int top, int width, int height, String str) {
        this.backgroundTexture.setSize(width, height);
        this.backgroundTexture.setTexId(R.drawable.board);
        this.backgroundTexture.setLeftTop(centerX - (width / 2), top);
        float buttonWidth = 0.4f * ((float) width);
        float buttonHeight = buttonWidth * 0.42f;
        float buttonY = ((float) top) + (0.7f * ((float) height));
        float buttonGap = buttonWidth * 0.6f;
        ScaleButton playAgainButton = new ScaleButton();
        playAgainButton.setTexId(R.array.altas4_playagain);
        playAgainButton.setPosition(0.0f, 0.0f);
        playAgainButton.setSize(buttonWidth, buttonHeight);
        playAgainButton.setFunctionCallback(new Callback() {
            public void excute(UI ui) {
                EventManager.getInstance().postEvent(EventType.Restart);
            }
        });
        playAgainButton.setPosition(((float) centerX) - buttonGap, buttonY);
        this.buttons.add(playAgainButton);
        ScaleButton quitButton = new ScaleButton();
        quitButton.setTexId(R.array.altas4_quit);
        quitButton.setPosition(0.0f, 0.0f);
        quitButton.setSize(buttonWidth, buttonHeight);
        quitButton.setFunctionCallback(new Callback() {
            public void excute(UI ui) {
                EventManager.getInstance().postEvent(EventType.MainMenu);
            }
        });
        quitButton.setPosition(((float) centerX) + buttonGap, buttonY);
        this.buttons.add(quitButton);
        this.text.setText(str);
        this.text.setFontSize((int) buttonHeight);
        this.text.setCenterTop(centerX, ((int) (0.15f * ((float) height))) + top);
        this.text.setColor(0.8509804f, 0.6784314f, 0.50980395f);
        this.text.setVisible(false);
        GLScreenView.textBuilder.add(this.text);
    }

    public void setText(String text2) {
    }

    public void setStoped(boolean isStoped) {
        super.setStoped(isStoped);
        this.text.setVisible(!isStoped);
    }

    public void render(GL10 gl, float elapsedTime) {
        if (!isStoped()) {
            this.backgroundTexture.drawOES();
            Iterator<ScaleButton> it = this.buttons.iterator();
            while (it.hasNext()) {
                it.next().render(gl, elapsedTime);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void touchDown(int x, int y) {
        Iterator<ScaleButton> it = this.buttons.iterator();
        while (it.hasNext()) {
            ScaleButton button = it.next();
            if (button.hitTest((float) x, (float) y)) {
                button.excute();
                return;
            }
        }
    }
}
