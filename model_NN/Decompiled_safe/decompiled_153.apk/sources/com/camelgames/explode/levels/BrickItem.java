package com.camelgames.explode.levels;

import android.content.res.XmlResourceParser;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.camelgames.explode.entities.Brick;
import com.camelgames.explode.game.GameManager;
import com.camelgames.explode.ui.LevelScreenshoter;
import com.camelgames.framework.levels.LevelScriptItem;
import com.camelgames.framework.levels.LevelScriptReader;
import com.camelgames.framework.levels.NodeParser;
import com.camelgames.framework.math.ArrayVectorUtils;
import com.camelgames.framework.math.Vector2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.xmlpull.v1.XmlPullParserException;

public class BrickItem implements NodeParser, LevelScriptItem, LevelScreenshoter {
    private static final String nodeName = "BrickMItem";
    private ArrayList<ItemNode> nodes = new ArrayList<>();

    public void load() {
        Iterator<ItemNode> it = this.nodes.iterator();
        while (it.hasNext()) {
            createBrick(it.next());
        }
    }

    public String getNodeName() {
        return nodeName;
    }

    public LevelScriptItem parse(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        this.nodes.clear();
        while (xmlParser.nextTag() == 2) {
            ItemNode itemNode = new ItemNode();
            itemNode.parse(xmlParser);
            this.nodes.add(itemNode);
        }
        return this;
    }

    public void draw(Canvas canvas, float rate) {
        Paint paint = new Paint();
        paint.setStrokeWidth(2.0f);
        Iterator<ItemNode> it = this.nodes.iterator();
        while (it.hasNext()) {
            draw(canvas, rate, it.next(), paint);
        }
    }

    private void draw(Canvas canvas, float rate, ItemNode itemNode, Paint paint) {
        if (itemNode.IsSolid) {
            paint.setColor(-8487298);
        } else {
            paint.setColor(-2511486);
        }
        float length = getBrickLength(itemNode) * rate;
        Vector2 referencePoint = getReferencePoint(itemNode);
        float startX = referencePoint.X * rate;
        float startY = referencePoint.Y * rate;
        if (itemNode.AngleUnit == 3) {
            startX = (float) (((double) startX) + (((double) length) / Math.sqrt(2.0d)));
        }
        Vector2 v = new Vector2(length, 0.0f);
        v.RotateVector(0.7853982f * ((float) itemNode.AngleUnit));
        canvas.drawLine(startX, startY, startX + v.X, startY + v.Y, paint);
    }

    private void createBrick(ItemNode node) {
        float length = getBrickLength(node);
        Vector2 referencePoint = getReferencePoint(node);
        float groundOffset = GameManager.getInstance().getGroundOfffset();
        referencePoint.add(0.0f, groundOffset);
        new Brick(ArrayVectorUtils.transformTo(node.points, node.points.length / 2, 0.0f, -groundOffset), length, ((float) node.ThickUnit) * GameManager.getInstance().getUnitLength(), referencePoint, node.AngleUnit * 45, node.HeadType1, node.HeadType2, node.IsSolid).setStatic();
    }

    private Vector2 getReferencePoint(ItemNode node) {
        Vector2 referencePoint = new Vector2(LevelScriptReader.getXScaleByVFloat(((float) node.LeftUnit) * GameManager.getInstance().getUnitLength()), ((float) node.TopUnit) * GameManager.getInstance().getUnitLength());
        if (node.AngleUnit == 0) {
            referencePoint.add(0.0f, GameManager.getInstance().getStandardThick() * 0.5f);
        } else if (node.AngleUnit == 2) {
            referencePoint.add(GameManager.getInstance().getStandardThick() * 0.5f, 0.0f);
        }
        return referencePoint;
    }

    private float getBrickLength(ItemNode node) {
        float length = ((float) node.LengthUnit) * GameManager.getInstance().getUnitLength();
        if (node.AngleUnit == 1 || node.AngleUnit == 3) {
            return (float) (((double) length) * Math.sqrt(2.0d));
        }
        return length;
    }

    public void clear() {
    }
}
