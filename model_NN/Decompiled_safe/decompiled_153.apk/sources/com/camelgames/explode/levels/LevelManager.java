package com.camelgames.explode.levels;

import com.camelgames.blowuplite.R;
import com.camelgames.explode.game.GameManager;
import com.camelgames.explode.manipulation.BombManipulator;
import com.camelgames.explode.score.ScoreManager;
import com.camelgames.explode.ui.PlayingUI;
import com.camelgames.framework.ConfigurationManager;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.events.EventListener;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.events.LoadLevelEvent;
import com.camelgames.framework.levels.LevelCategory;
import com.camelgames.framework.levels.LevelScript;
import com.camelgames.framework.levels.LevelScriptReader;
import com.camelgames.framework.ui.UIUtility;

public class LevelManager implements EventListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$events$EventType = null;
    public static final String DifficultyConfig = "Difficulty";
    public static final String LevelIndexConfig = "CurrentLevelIndex";
    private static LevelManager instance = new LevelManager();
    private int currentLevelIndex = ConfigurationManager.getInstance().getInt(LevelIndexConfig, 0);
    private boolean isRagdollDied;
    private boolean isTargetCatched;
    private LevelCategory levelCategory;
    private int[] levelIds;
    private LevelScriptReader levelScriptReader = new LevelScriptReader();
    private DifficultyType type = DifficultyType.values()[ConfigurationManager.getInstance().getInt(DifficultyConfig, 0)];

    public enum DifficultyType {
        Easy,
        Medium,
        Hard
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$events$EventType() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$framework$events$EventType;
        if (iArr == null) {
            iArr = new int[EventType.values().length];
            try {
                iArr[EventType.Arrive.ordinal()] = 61;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[EventType.AttachRagdoll.ordinal()] = 65;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[EventType.AttachStick.ordinal()] = 66;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[EventType.AttachedToJoint.ordinal()] = 40;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[EventType.BallCollide.ordinal()] = 8;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[EventType.Bomb.ordinal()] = 67;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[EventType.BombLarge.ordinal()] = 68;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[EventType.BrickChanged.ordinal()] = 45;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[EventType.Button.ordinal()] = 30;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[EventType.Captured.ordinal()] = 34;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[EventType.CarCreated.ordinal()] = 39;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[EventType.Catched.ordinal()] = 11;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[EventType.Collide.ordinal()] = 6;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[EventType.ContinueGame.ordinal()] = 23;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[EventType.Crash.ordinal()] = 59;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[EventType.Customise.ordinal()] = 25;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[EventType.DoubleTap.ordinal()] = 10;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[EventType.EraseAll.ordinal()] = 41;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[EventType.Fall.ordinal()] = 36;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[EventType.Favourite.ordinal()] = 46;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[EventType.GameBegin.ordinal()] = 26;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[EventType.GotScore.ordinal()] = 18;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[EventType.GraphicsCreated.ordinal()] = 1;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[EventType.GraphicsDestroyed.ordinal()] = 2;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[EventType.GraphicsResized.ordinal()] = 3;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[EventType.HighScore.ordinal()] = 28;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[EventType.JointCreated.ordinal()] = 62;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[EventType.Landed.ordinal()] = 58;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[EventType.LevelDown.ordinal()] = 15;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[EventType.LevelEditorCopyBrick.ordinal()] = 47;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[EventType.LevelEditorDeleteBrick.ordinal()] = 48;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[EventType.LevelEditorEditGround.ordinal()] = 51;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[EventType.LevelEditorEditQueue.ordinal()] = 50;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[EventType.LevelEditorExit.ordinal()] = 53;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[EventType.LevelEditorNewBrick.ordinal()] = 49;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[EventType.LevelEditorSave.ordinal()] = 54;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[EventType.LevelEditorStart.ordinal()] = 52;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[EventType.LevelEditorTest.ordinal()] = 55;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[EventType.LevelFailed.ordinal()] = 14;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[EventType.LevelFinished.ordinal()] = 13;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[EventType.LevelUp.ordinal()] = 12;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[EventType.LineCreated.ordinal()] = 37;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[EventType.LineDestroyed.ordinal()] = 38;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[EventType.LoadLevel.ordinal()] = 16;
            } catch (NoSuchFieldError e44) {
            }
            try {
                iArr[EventType.MainMenu.ordinal()] = 19;
            } catch (NoSuchFieldError e45) {
            }
            try {
                iArr[EventType.NewGame.ordinal()] = 22;
            } catch (NoSuchFieldError e46) {
            }
            try {
                iArr[EventType.PapaStackLoadLevel.ordinal()] = 43;
            } catch (NoSuchFieldError e47) {
            }
            try {
                iArr[EventType.PressSwitch.ordinal()] = 69;
            } catch (NoSuchFieldError e48) {
            }
            try {
                iArr[EventType.Purchased.ordinal()] = 70;
            } catch (NoSuchFieldError e49) {
            }
            try {
                iArr[EventType.RagdollDied.ordinal()] = 42;
            } catch (NoSuchFieldError e50) {
            }
            try {
                iArr[EventType.ReadyToLand.ordinal()] = 57;
            } catch (NoSuchFieldError e51) {
            }
            try {
                iArr[EventType.ReadyToLoadLevel.ordinal()] = 17;
            } catch (NoSuchFieldError e52) {
            }
            try {
                iArr[EventType.Refunded.ordinal()] = 71;
            } catch (NoSuchFieldError e53) {
            }
            try {
                iArr[EventType.Replay.ordinal()] = 21;
            } catch (NoSuchFieldError e54) {
            }
            try {
                iArr[EventType.Restart.ordinal()] = 20;
            } catch (NoSuchFieldError e55) {
            }
            try {
                iArr[EventType.SelectLevel.ordinal()] = 24;
            } catch (NoSuchFieldError e56) {
            }
            try {
                iArr[EventType.SensorContact.ordinal()] = 7;
            } catch (NoSuchFieldError e57) {
            }
            try {
                iArr[EventType.Shoot.ordinal()] = 63;
            } catch (NoSuchFieldError e58) {
            }
            try {
                iArr[EventType.SingleTap.ordinal()] = 9;
            } catch (NoSuchFieldError e59) {
            }
            try {
                iArr[EventType.SoundOff.ordinal()] = 32;
            } catch (NoSuchFieldError e60) {
            }
            try {
                iArr[EventType.SoundOn.ordinal()] = 31;
            } catch (NoSuchFieldError e61) {
            }
            try {
                iArr[EventType.StartExplode.ordinal()] = 64;
            } catch (NoSuchFieldError e62) {
            }
            try {
                iArr[EventType.StartToRecord.ordinal()] = 44;
            } catch (NoSuchFieldError e63) {
            }
            try {
                iArr[EventType.StaticEdgesCreated.ordinal()] = 4;
            } catch (NoSuchFieldError e64) {
            }
            try {
                iArr[EventType.StaticEdgesDestroyed.ordinal()] = 5;
            } catch (NoSuchFieldError e65) {
            }
            try {
                iArr[EventType.Tick.ordinal()] = 33;
            } catch (NoSuchFieldError e66) {
            }
            try {
                iArr[EventType.Triggered.ordinal()] = 35;
            } catch (NoSuchFieldError e67) {
            }
            try {
                iArr[EventType.Tutorial.ordinal()] = 27;
            } catch (NoSuchFieldError e68) {
            }
            try {
                iArr[EventType.UICommand.ordinal()] = 29;
            } catch (NoSuchFieldError e69) {
            }
            try {
                iArr[EventType.UploadLevel.ordinal()] = 56;
            } catch (NoSuchFieldError e70) {
            }
            try {
                iArr[EventType.Warning.ordinal()] = 60;
            } catch (NoSuchFieldError e71) {
            }
            $SWITCH_TABLE$com$camelgames$framework$events$EventType = iArr;
        }
        return iArr;
    }

    public static LevelManager getInstance() {
        return instance;
    }

    private LevelManager() {
        EventManager.getInstance().addListener(EventType.LevelUp, this);
        EventManager.getInstance().addListener(EventType.LevelDown, this);
        EventManager.getInstance().addListener(EventType.LoadLevel, this);
        EventManager.getInstance().addListener(EventType.Restart, this);
        EventManager.getInstance().addListener(EventType.Catched, this);
        EventManager.getInstance().addListener(EventType.RagdollDied, this);
    }

    public void initiate() {
        this.levelScriptReader.addParser(new RootItem());
        this.levelScriptReader.addParser(new GroundItem());
        this.levelScriptReader.addParser(new BrickItem());
        this.levelScriptReader.addParser(new RagdollItem());
        this.levelScriptReader.addParser(new TargetItem());
        searchLevelsFromContext();
    }

    public void HandleEvent(AbstractEvent e) {
        switch ($SWITCH_TABLE$com$camelgames$framework$events$EventType()[e.getType().ordinal()]) {
            case 11:
                this.isTargetCatched = true;
                return;
            case 12:
                levelUp();
                return;
            case 15:
                if (this.currentLevelIndex > 0) {
                    this.currentLevelIndex--;
                } else {
                    this.currentLevelIndex = this.levelIds.length - 1;
                }
                loadLevel(this.currentLevelIndex);
                return;
            case 16:
                loadLevel(((LoadLevelEvent) e).getLevelIndex());
                BombManipulator.getInstance().clearPreviousBombLayout();
                return;
            case 20:
                reloadLevel();
                return;
            case 42:
                this.isRagdollDied = true;
                return;
            default:
                return;
        }
    }

    public String getLevelInfo() {
        int levelCount = getLevelCount();
        if (levelCount > 0) {
            return new StringBuilder().append(this.currentLevelIndex + 1).append('/').append(levelCount).toString();
        }
        return "";
    }

    public int getLevelCount() {
        if (this.levelIds != null) {
            return this.levelIds.length;
        }
        return 0;
    }

    public int getCurrentLevelIndx() {
        return this.currentLevelIndex;
    }

    public int getCurrentLevelScore() {
        return ScoreManager.getInstance().getScore(this.currentLevelIndex, getDifficulty());
    }

    public int getDifficulty() {
        return this.type.ordinal();
    }

    public void reloadLevel() {
        loadLevel(this.currentLevelIndex);
        BombManipulator.getInstance().restoreToPreviousBombLayout();
    }

    public void loadLevel(int levelIndex) {
        ConfigurationManager.getInstance().putInt(LevelIndexConfig, levelIndex);
        if (levelIndex >= this.levelIds.length) {
            levelIndex = 0;
        }
        this.currentLevelIndex = levelIndex;
        this.isTargetCatched = false;
        this.isRagdollDied = false;
        GameManager.getInstance().startPlay(loadFromResourceId(this.levelIds[this.currentLevelIndex]));
    }

    public void levelFinished() {
        ScoreManager.getInstance().updateScore(this.currentLevelIndex, getScore(), getDifficulty());
    }

    public int getHeightScore() {
        return (int) (PlayingUI.getInstance().getHeightCredit() * 1000.0f);
    }

    public int getBonusScore() {
        return this.isTargetCatched ? 500 : 0;
    }

    public int getDamageScore() {
        int damageScore = this.isRagdollDied ? 250 : 0;
        if (!this.isTargetCatched) {
            return damageScore * -1;
        }
        return damageScore;
    }

    public int getScore() {
        return (getHeightScore() + getBonusScore()) - Math.max(0, getDamageScore());
    }

    public int getStars() {
        return scoreToStar(getScore());
    }

    public static int scoreToStar(int score) {
        if (score > 0) {
            return Math.min(5, (score / 250) + 1);
        }
        return 0;
    }

    private void levelUp() {
        if (this.currentLevelIndex < this.levelIds.length - 1) {
            loadLevel(this.currentLevelIndex + 1);
        } else {
            loadLevel(0);
        }
        BombManipulator.getInstance().clearPreviousBombLayout();
    }

    public LevelScript getLevelScript(int index) {
        if (index >= this.levelIds.length || index < 0) {
            return null;
        }
        return loadFromResourceId(this.levelIds[index]);
    }

    private LevelScript loadFromResourceId(int id) {
        return this.levelScriptReader.read(UIUtility.getMainAcitvity().getResources().getXml(id));
    }

    private void searchLevelsFromContext() {
        this.levelCategory = new LevelCategory(R.xml.class, "level");
        this.levelIds = new int[this.levelCategory.getLevelsCount()];
        System.arraycopy(this.levelCategory.getLevelIds(), 0, this.levelIds, 0, this.levelCategory.getLevelsCount());
    }

    public Boolean needShowTutorial() {
        return this.type.equals(DifficultyType.Easy) && this.currentLevelIndex == 0;
    }

    public void setDifficultyType(DifficultyType type2) {
        ConfigurationManager.getInstance().putInt(DifficultyConfig, type2.ordinal());
        this.type = type2;
    }

    public DifficultyType getDifficultyType() {
        return this.type;
    }

    public boolean isTargetCatched() {
        return this.isTargetCatched;
    }

    public boolean isRagdollDied() {
        return this.isRagdollDied;
    }
}
