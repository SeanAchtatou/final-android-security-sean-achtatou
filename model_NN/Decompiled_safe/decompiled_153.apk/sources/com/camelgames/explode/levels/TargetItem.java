package com.camelgames.explode.levels;

import android.content.res.XmlResourceParser;
import com.camelgames.explode.entities.Target;
import com.camelgames.explode.game.GameManager;
import com.camelgames.framework.levels.LevelScriptItem;
import com.camelgames.framework.levels.LevelScriptReader;
import com.camelgames.framework.levels.NodeParser;
import com.camelgames.framework.math.Vector2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.xmlpull.v1.XmlPullParserException;

public class TargetItem implements NodeParser, LevelScriptItem {
    private static final String nodeName = "TargetMItem";
    private ArrayList<ItemNode> nodes = new ArrayList<>();

    public void load() {
        Iterator<ItemNode> it = this.nodes.iterator();
        while (it.hasNext()) {
            createTarget(it.next());
        }
    }

    public String getNodeName() {
        return nodeName;
    }

    public LevelScriptItem parse(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        this.nodes.clear();
        while (xmlParser.nextTag() == 2) {
            ItemNode itemNode = new ItemNode();
            itemNode.parse(xmlParser);
            this.nodes.add(itemNode);
        }
        return this;
    }

    private void createTarget(ItemNode node) {
        Vector2 referencePoint = new Vector2(LevelScriptReader.getXScaleByVFloat(((float) node.LeftUnit) * GameManager.getInstance().getUnitLength()), (((float) node.TopUnit) * GameManager.getInstance().getUnitLength()) + GameManager.getInstance().getGroundOfffset());
        new Target(referencePoint.X, referencePoint.Y);
    }

    public void clear() {
    }
}
