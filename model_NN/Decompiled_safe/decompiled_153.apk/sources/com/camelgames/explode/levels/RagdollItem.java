package com.camelgames.explode.levels;

import android.content.res.XmlResourceParser;
import com.camelgames.blowuplite.R;
import com.camelgames.explode.entities.ragdoll.Ragdoll;
import com.camelgames.explode.game.GameManager;
import com.camelgames.framework.graphics.altas.AltasTextureManager;
import com.camelgames.framework.levels.LevelScriptItem;
import com.camelgames.framework.levels.LevelScriptReader;
import com.camelgames.framework.levels.NodeParser;
import com.camelgames.framework.math.MathUtils;
import com.camelgames.framework.math.Vector2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.xmlpull.v1.XmlPullParserException;

public class RagdollItem implements NodeParser, LevelScriptItem {
    private static final String nodeName = "RagdollMItem";
    private ArrayList<ItemNode> nodes = new ArrayList<>();

    public void load() {
        Iterator<ItemNode> it = this.nodes.iterator();
        while (it.hasNext()) {
            createRagdoll(it.next());
        }
    }

    public String getNodeName() {
        return nodeName;
    }

    public LevelScriptItem parse(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        this.nodes.clear();
        while (xmlParser.nextTag() == 2) {
            ItemNode itemNode = new ItemNode();
            itemNode.parse(xmlParser);
            this.nodes.add(itemNode);
        }
        return this;
    }

    private void createRagdoll(ItemNode node) {
        Vector2 referencePoint = new Vector2(LevelScriptReader.getXScaleByVFloat(((float) node.LeftUnit) * GameManager.getInstance().getUnitLength()), (((float) node.TopUnit) * GameManager.getInstance().getUnitLength()) + GameManager.getInstance().getGroundOfffset());
        Ragdoll ragdoll = new Ragdoll(getRagdollConfig2());
        ragdoll.setPosition(referencePoint.X, referencePoint.Y);
        ragdoll.setAngle(MathUtils.random(0.0f, 6.2831855f));
    }

    private static Ragdoll.Config getRagdollConfig2() {
        return new Ragdoll.Config(AltasTextureManager.getInstance().getAltasIdFromSubId(Integer.valueOf((int) R.array.altas1_arm1)), new Ragdoll.Config.PieceInfo(Integer.valueOf((int) R.array.altas1_head), 20, 20, 16.0f, 16.0f), new Ragdoll.Config.PieceInfo(Integer.valueOf((int) R.array.altas1_body), 20, 20, 10.0f, 20.0f), new Ragdoll.Config.PieceInfo(Integer.valueOf((int) R.array.altas1_arm1), 10, 10, 2.0f, 10.0f), new Ragdoll.Config.PieceInfo(Integer.valueOf((int) R.array.altas1_arm2), 20, 20, 2.0f, 10.0f), new Ragdoll.Config.PieceInfo(Integer.valueOf((int) R.array.altas1_leg1), 10, 10, 2.0f, 10.0f), new Ragdoll.Config.PieceInfo(Integer.valueOf((int) R.array.altas1_leg2), 10, 10, 2.0f, 10.0f));
    }

    public void clear() {
    }
}
