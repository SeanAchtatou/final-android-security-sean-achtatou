package com.camelgames.explode.levels;

import android.content.res.XmlResourceParser;
import com.camelgames.framework.levels.LevelScriptReader;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParserException;

public class ItemNode {
    public int AngleUnit;
    public int HeadType1;
    public int HeadType2;
    public boolean IsSolid;
    public int LeftUnit;
    public int LengthUnit;
    public int ThickUnit;
    public int TopUnit;
    public float[] points;

    public void parse(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        LevelScriptReader.xmlRequireStartTag(xmlParser, "Item");
        this.LeftUnit = LevelScriptReader.getInt(xmlParser, "LeftUnit", 0);
        this.TopUnit = LevelScriptReader.getInt(xmlParser, "TopUnit", 0);
        this.LengthUnit = LevelScriptReader.getInt(xmlParser, "WidthUnit", 0);
        this.ThickUnit = LevelScriptReader.getInt(xmlParser, "HeightUnit", 0);
        this.AngleUnit = LevelScriptReader.getInt(xmlParser, "AngleUnit", 0);
        this.HeadType1 = LevelScriptReader.getInt(xmlParser, "HeadType1", 0);
        this.HeadType2 = LevelScriptReader.getInt(xmlParser, "HeadType2", 0);
        this.IsSolid = LevelScriptReader.getBoolean(xmlParser, "IsSolid");
        ArrayList<Float> pointArray = new ArrayList<>();
        while (xmlParser.nextTag() == 2) {
            LevelScriptReader.xmlRequireStartTag(xmlParser, "Point");
            float x = LevelScriptReader.getXScaleByVFloat(xmlParser, "X");
            float y = LevelScriptReader.getScreenYFloat(xmlParser, "Y");
            pointArray.add(Float.valueOf(x));
            pointArray.add(Float.valueOf(y));
            xmlParser.nextTag();
        }
        this.points = new float[pointArray.size()];
        for (int i = 0; i < this.points.length; i++) {
            this.points[i] = ((Float) pointArray.get(i)).floatValue();
        }
    }
}
