package com.camelgames.explode.levels;

import android.content.res.XmlResourceParser;
import com.camelgames.explode.game.GameManager;
import com.camelgames.framework.levels.LevelScriptItem;
import com.camelgames.framework.levels.NodeParser;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class GroundItem implements NodeParser, LevelScriptItem {
    private static final String nodeName = "GroundMItem";

    public void load() {
    }

    public String getNodeName() {
        return nodeName;
    }

    public LevelScriptItem parse(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        if (xmlParser.nextTag() == 2) {
            ItemNode itemNode = new ItemNode();
            itemNode.parse(xmlParser);
            GameManager.getInstance().setLevelGroundTopUnit(itemNode.TopUnit);
        }
        return this;
    }

    public void clear() {
    }
}
