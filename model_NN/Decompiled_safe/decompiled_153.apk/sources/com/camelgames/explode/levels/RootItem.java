package com.camelgames.explode.levels;

import android.content.res.XmlResourceParser;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.camelgames.explode.entities.BigBomb;
import com.camelgames.explode.entities.BombGuard;
import com.camelgames.explode.entities.MagnetBomb;
import com.camelgames.explode.entities.SmallBomb;
import com.camelgames.explode.game.GameManager;
import com.camelgames.explode.ui.LevelScreenshoter;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.levels.LevelScriptItem;
import com.camelgames.framework.levels.LevelScriptReader;
import com.camelgames.framework.levels.NodeParser;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class RootItem implements NodeParser, LevelScriptItem, LevelScreenshoter {
    private static final String nodeName = "ROOT";
    private int StandardThickUnit;
    private float UnitLength;
    private int bigBomb;
    private float leftGuard;
    private int magnetBomb;
    private float rightGuard;
    private int smallBomb;
    private float targetLine;

    public void load() {
        if (this.UnitLength > 0.0f) {
            GameManager.getInstance().setUnitLength(this.UnitLength);
            GameManager.getInstance().setStandardThickUnit(this.StandardThickUnit);
            GameManager.getInstance().setTargetLine(this.targetLine);
        }
        for (int i = 0; i < this.smallBomb; i++) {
            new SmallBomb();
        }
        for (int i2 = 0; i2 < this.bigBomb; i2++) {
            new BigBomb();
        }
        for (int i3 = 0; i3 < this.magnetBomb; i3++) {
            new MagnetBomb();
        }
        if (this.leftGuard != 0.0f) {
            new BombGuard(LevelScriptReader.getXScaleByVFloat(this.leftGuard * 512.0f * LevelScriptReader.scaleY), true);
        }
        if (this.rightGuard != 0.0f) {
            new BombGuard(LevelScriptReader.getXScaleByVFloat((1.0f - this.rightGuard) * 512.0f * LevelScriptReader.scaleY), false);
        }
    }

    public void draw(Canvas canvas, float rate) {
        if (this.UnitLength > 0.0f) {
            GameManager.getInstance().setUnitLength(this.UnitLength);
            GameManager.getInstance().setStandardThickUnit(this.StandardThickUnit);
            GameManager.getInstance().setTargetLine(this.targetLine);
        }
        Paint paint = new Paint();
        paint.setStrokeWidth(2.0f);
        paint.setColor(-65536);
        if (this.leftGuard != 0.0f) {
            float x = this.leftGuard * ((float) GraphicsManager.screenWidth()) * rate;
            canvas.drawLine(x, ((float) canvas.getHeight()) * 0.15f, x, ((float) canvas.getHeight()) * 0.85f, paint);
        }
        if (this.rightGuard != 0.0f) {
            float x2 = (1.0f - this.rightGuard) * ((float) GraphicsManager.screenWidth()) * rate;
            canvas.drawLine(x2, ((float) canvas.getHeight()) * 0.15f, x2, ((float) canvas.getHeight()) * 0.85f, paint);
        }
    }

    public String getNodeName() {
        return nodeName;
    }

    public LevelScriptItem parse(XmlResourceParser xmlParser) throws XmlPullParserException, IOException {
        this.UnitLength = LevelScriptReader.getScreenYFloat(xmlParser, "UnitLength");
        this.StandardThickUnit = LevelScriptReader.getInt(xmlParser, "StandardThick", 2);
        this.smallBomb = LevelScriptReader.getInt(xmlParser, "SmallBomb", 0);
        this.bigBomb = LevelScriptReader.getInt(xmlParser, "BigBomb", 0);
        this.magnetBomb = LevelScriptReader.getInt(xmlParser, "MagnetBomb", 0);
        this.leftGuard = LevelScriptReader.getFloat(xmlParser, "MarginLeft", 0.0f);
        this.rightGuard = LevelScriptReader.getFloat(xmlParser, "MarginRight", 0.0f);
        this.targetLine = LevelScriptReader.getFloat(xmlParser, "TargetLine", 0.0f);
        return this;
    }

    public void clear() {
    }
}
