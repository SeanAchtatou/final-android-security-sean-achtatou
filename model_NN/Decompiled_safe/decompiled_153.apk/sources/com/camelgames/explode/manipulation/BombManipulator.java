package com.camelgames.explode.manipulation;

import com.camelgames.blowuplite.R;
import com.camelgames.explode.GLScreenView;
import com.camelgames.explode.entities.Bomb;
import com.camelgames.explode.entities.BombAttachable;
import com.camelgames.explode.entities.Brick;
import com.camelgames.explode.entities.ragdoll.Ragdoll;
import com.camelgames.explode.game.GameManager;
import com.camelgames.explode.ui.PlayingUI;
import com.camelgames.framework.Manipulator;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.events.TapEvent;
import com.camelgames.framework.graphics.Camera2D;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.Renderable;
import com.camelgames.framework.math.MathUtils;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.physics.PhysicsManager;
import com.camelgames.ndk.graphics.OESSprite;
import com.camelgames.ndk.graphics.Sprite2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import javax.microedition.khronos.opengles.GL10;

public class BombManipulator extends Manipulator {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$entities$Bomb$Type;
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$manipulation$BombManipulator$CameraStatus;
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$manipulation$BombManipulator$GameStatus;
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$events$EventType;
    private static BombManipulator instance = new BombManipulator();
    private final float attachThreshold = (0.03f * ((float) GraphicsManager.screenHeight()));
    private ArrayList<BombAttachable> attachers = new ArrayList<>();
    private LinkedList<Bomb> bigBombsQueue = new LinkedList<>();
    private Vector2 bombAttachedPosition = new Vector2();
    private LinkedList<Bomb> bombsUsed = new LinkedList<>();
    private ArrayList<Brick> bricks = new ArrayList<>();
    private Camera2D camera = ((Camera2D) GraphicsManager.getInstance().getCamera());
    private Vector2 cameraMapped = new Vector2();
    private CameraStatus cameraStatus = CameraStatus.Normal;
    private float cameraXMovingSpeed;
    private float cameraYMovingSpeed;
    private Bomb currentBomb;
    private OESSprite frameTexture = new OESSprite();
    private GameStatus gameStatus = GameStatus.Operating;
    private boolean isTouchCaptured;
    private LinkedList<Bomb> magnentBombsQueue = new LinkedList<>();
    private final float maxTimeAfterExploded = 8.0f;
    private final float maxTimeAfterLineMeet = 4.0f;
    private final float maxZoomIn = 0.6f;
    private Vector2 minBombAttachedPosition = new Vector2();
    private final float movingSpeed = (((float) GraphicsManager.screenHeight()) * 0.8f);
    private Vector2 originalAttacherWorld = new Vector2();
    private Vector2 originalTouchWorld = new Vector2();
    private ArrayList<Bomb.Info> previousBombLayout = new ArrayList<>();
    private Vector2 previousTouch = new Vector2();
    private final float senseMarginY = (((float) GraphicsManager.screenHeight()) * 0.25f);
    private final float senseMartinX = (((float) GraphicsManager.screenWidth()) * 0.25f);
    private LinkedList<Bomb> smallBombsQueue = new LinkedList<>();
    private float timeAfterExploded;
    private float timeAfterLineMeet;
    private final float triggerZoomOutTime = 2.0f;
    private float waitedTime;
    private final float waitingThreshod = (0.01f * ((float) GraphicsManager.screenHeight()));
    private Vector2 zoomAnchor = new Vector2();
    private final float zoomSpeed = 1.0f;

    private enum CameraStatus {
        Normal,
        ZoomIn,
        ZoomOut,
        Zoomed
    }

    private enum GameStatus {
        Operating,
        Exploded,
        PendingJuge,
        Finished
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$entities$Bomb$Type() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$explode$entities$Bomb$Type;
        if (iArr == null) {
            iArr = new int[Bomb.Type.values().length];
            try {
                iArr[Bomb.Type.Big.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Bomb.Type.Magnent.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Bomb.Type.Small.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$camelgames$explode$entities$Bomb$Type = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$manipulation$BombManipulator$CameraStatus() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$explode$manipulation$BombManipulator$CameraStatus;
        if (iArr == null) {
            iArr = new int[CameraStatus.values().length];
            try {
                iArr[CameraStatus.Normal.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[CameraStatus.ZoomIn.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[CameraStatus.ZoomOut.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[CameraStatus.Zoomed.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$camelgames$explode$manipulation$BombManipulator$CameraStatus = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$manipulation$BombManipulator$GameStatus() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$explode$manipulation$BombManipulator$GameStatus;
        if (iArr == null) {
            iArr = new int[GameStatus.values().length];
            try {
                iArr[GameStatus.Exploded.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[GameStatus.Finished.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[GameStatus.Operating.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[GameStatus.PendingJuge.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$camelgames$explode$manipulation$BombManipulator$GameStatus = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$framework$events$EventType() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$framework$events$EventType;
        if (iArr == null) {
            iArr = new int[EventType.values().length];
            try {
                iArr[EventType.Arrive.ordinal()] = 61;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[EventType.AttachRagdoll.ordinal()] = 65;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[EventType.AttachStick.ordinal()] = 66;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[EventType.AttachedToJoint.ordinal()] = 40;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[EventType.BallCollide.ordinal()] = 8;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[EventType.Bomb.ordinal()] = 67;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[EventType.BombLarge.ordinal()] = 68;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[EventType.BrickChanged.ordinal()] = 45;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[EventType.Button.ordinal()] = 30;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[EventType.Captured.ordinal()] = 34;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[EventType.CarCreated.ordinal()] = 39;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[EventType.Catched.ordinal()] = 11;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[EventType.Collide.ordinal()] = 6;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[EventType.ContinueGame.ordinal()] = 23;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[EventType.Crash.ordinal()] = 59;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[EventType.Customise.ordinal()] = 25;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[EventType.DoubleTap.ordinal()] = 10;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[EventType.EraseAll.ordinal()] = 41;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[EventType.Fall.ordinal()] = 36;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[EventType.Favourite.ordinal()] = 46;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[EventType.GameBegin.ordinal()] = 26;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[EventType.GotScore.ordinal()] = 18;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[EventType.GraphicsCreated.ordinal()] = 1;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[EventType.GraphicsDestroyed.ordinal()] = 2;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[EventType.GraphicsResized.ordinal()] = 3;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[EventType.HighScore.ordinal()] = 28;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[EventType.JointCreated.ordinal()] = 62;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[EventType.Landed.ordinal()] = 58;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[EventType.LevelDown.ordinal()] = 15;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[EventType.LevelEditorCopyBrick.ordinal()] = 47;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[EventType.LevelEditorDeleteBrick.ordinal()] = 48;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[EventType.LevelEditorEditGround.ordinal()] = 51;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[EventType.LevelEditorEditQueue.ordinal()] = 50;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[EventType.LevelEditorExit.ordinal()] = 53;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[EventType.LevelEditorNewBrick.ordinal()] = 49;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[EventType.LevelEditorSave.ordinal()] = 54;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[EventType.LevelEditorStart.ordinal()] = 52;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[EventType.LevelEditorTest.ordinal()] = 55;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[EventType.LevelFailed.ordinal()] = 14;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[EventType.LevelFinished.ordinal()] = 13;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[EventType.LevelUp.ordinal()] = 12;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[EventType.LineCreated.ordinal()] = 37;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[EventType.LineDestroyed.ordinal()] = 38;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[EventType.LoadLevel.ordinal()] = 16;
            } catch (NoSuchFieldError e44) {
            }
            try {
                iArr[EventType.MainMenu.ordinal()] = 19;
            } catch (NoSuchFieldError e45) {
            }
            try {
                iArr[EventType.NewGame.ordinal()] = 22;
            } catch (NoSuchFieldError e46) {
            }
            try {
                iArr[EventType.PapaStackLoadLevel.ordinal()] = 43;
            } catch (NoSuchFieldError e47) {
            }
            try {
                iArr[EventType.PressSwitch.ordinal()] = 69;
            } catch (NoSuchFieldError e48) {
            }
            try {
                iArr[EventType.Purchased.ordinal()] = 70;
            } catch (NoSuchFieldError e49) {
            }
            try {
                iArr[EventType.RagdollDied.ordinal()] = 42;
            } catch (NoSuchFieldError e50) {
            }
            try {
                iArr[EventType.ReadyToLand.ordinal()] = 57;
            } catch (NoSuchFieldError e51) {
            }
            try {
                iArr[EventType.ReadyToLoadLevel.ordinal()] = 17;
            } catch (NoSuchFieldError e52) {
            }
            try {
                iArr[EventType.Refunded.ordinal()] = 71;
            } catch (NoSuchFieldError e53) {
            }
            try {
                iArr[EventType.Replay.ordinal()] = 21;
            } catch (NoSuchFieldError e54) {
            }
            try {
                iArr[EventType.Restart.ordinal()] = 20;
            } catch (NoSuchFieldError e55) {
            }
            try {
                iArr[EventType.SelectLevel.ordinal()] = 24;
            } catch (NoSuchFieldError e56) {
            }
            try {
                iArr[EventType.SensorContact.ordinal()] = 7;
            } catch (NoSuchFieldError e57) {
            }
            try {
                iArr[EventType.Shoot.ordinal()] = 63;
            } catch (NoSuchFieldError e58) {
            }
            try {
                iArr[EventType.SingleTap.ordinal()] = 9;
            } catch (NoSuchFieldError e59) {
            }
            try {
                iArr[EventType.SoundOff.ordinal()] = 32;
            } catch (NoSuchFieldError e60) {
            }
            try {
                iArr[EventType.SoundOn.ordinal()] = 31;
            } catch (NoSuchFieldError e61) {
            }
            try {
                iArr[EventType.StartExplode.ordinal()] = 64;
            } catch (NoSuchFieldError e62) {
            }
            try {
                iArr[EventType.StartToRecord.ordinal()] = 44;
            } catch (NoSuchFieldError e63) {
            }
            try {
                iArr[EventType.StaticEdgesCreated.ordinal()] = 4;
            } catch (NoSuchFieldError e64) {
            }
            try {
                iArr[EventType.StaticEdgesDestroyed.ordinal()] = 5;
            } catch (NoSuchFieldError e65) {
            }
            try {
                iArr[EventType.Tick.ordinal()] = 33;
            } catch (NoSuchFieldError e66) {
            }
            try {
                iArr[EventType.Triggered.ordinal()] = 35;
            } catch (NoSuchFieldError e67) {
            }
            try {
                iArr[EventType.Tutorial.ordinal()] = 27;
            } catch (NoSuchFieldError e68) {
            }
            try {
                iArr[EventType.UICommand.ordinal()] = 29;
            } catch (NoSuchFieldError e69) {
            }
            try {
                iArr[EventType.UploadLevel.ordinal()] = 56;
            } catch (NoSuchFieldError e70) {
            }
            try {
                iArr[EventType.Warning.ordinal()] = 60;
            } catch (NoSuchFieldError e71) {
            }
            $SWITCH_TABLE$com$camelgames$framework$events$EventType = iArr;
        }
        return iArr;
    }

    public static BombManipulator getInstance() {
        return instance;
    }

    private BombManipulator() {
        this.frameTexture.setSize(GLScreenView.scissorWidth, GLScreenView.scissorHeight);
        this.frameTexture.setTexId(R.array.altas1_frame);
        this.frameTexture.setLeftTop(GraphicsManager.screenWidth() - this.frameTexture.getWidth(), 0);
        setPriority(Renderable.PRIORITY.MIDDLE);
        addEventType(EventType.SingleTap);
        addEventType(EventType.DoubleTap);
        addListener();
    }

    public void addBrick(Brick brick) {
        if (brick != null && !this.bricks.contains(brick)) {
            this.bricks.add(brick);
        }
        addAttacher(brick);
    }

    public void removeBrick(Brick brick) {
        this.bricks.remove(brick);
        removeAttacher(brick);
    }

    public void addAttacher(BombAttachable attacher) {
        if (attacher != null && !this.attachers.contains(attacher)) {
            this.attachers.add(attacher);
        }
    }

    public void removeAttacher(BombAttachable attacher) {
        this.attachers.remove(attacher);
    }

    public void addBomb(Bomb bomb) {
        if (bomb != null && !this.bombsUsed.contains(bomb)) {
            switch ($SWITCH_TABLE$com$camelgames$explode$entities$Bomb$Type()[bomb.getType().ordinal()]) {
                case 1:
                    this.smallBombsQueue.add(bomb);
                    break;
                case 2:
                    this.bigBombsQueue.add(bomb);
                    break;
                case 3:
                    this.magnentBombsQueue.add(bomb);
                    break;
            }
            updateBombCount();
        }
    }

    public void removeBomb(Bomb bomb) {
        this.bombsUsed.remove(bomb);
        this.smallBombsQueue.remove(bomb);
        this.bigBombsQueue.remove(bomb);
        this.magnentBombsQueue.remove(bomb);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Bomb pollBomb(Bomb.Type type) {
        switch ($SWITCH_TABLE$com$camelgames$explode$entities$Bomb$Type()[type.ordinal()]) {
            case 1:
                return this.smallBombsQueue.poll();
            case 2:
                return this.bigBombsQueue.poll();
            case 3:
                return this.magnentBombsQueue.poll();
            default:
                return null;
        }
    }

    public boolean isOperating() {
        return this.gameStatus.equals(GameStatus.Operating) && !GameManager.getInstance().isPaused();
    }

    public boolean isOperatingStatus() {
        return this.gameStatus.equals(GameStatus.Operating);
    }

    public void clearPreviousBombLayout() {
        this.previousBombLayout.clear();
    }

    public void restoreToPreviousBombLayout() {
        BombAttachable attacher;
        Iterator<Bomb.Info> it = this.previousBombLayout.iterator();
        while (it.hasNext()) {
            Bomb.Info info = it.next();
            Bomb bomb = fetchBomb(info);
            if (bomb != null) {
                useBomb(bomb);
                if (info.bindToRagdoll) {
                    attacher = tryAttachToRagdoll(bomb);
                } else {
                    attacher = tryAttach(bomb);
                }
                if (attacher == null) {
                    recycleBomb(bomb);
                }
            }
        }
    }

    private Ragdoll tryAttachToRagdoll(Bomb bomb) {
        for (int i = 0; i < this.attachers.size(); i++) {
            BombAttachable attacher = this.attachers.get(i);
            if (attacher instanceof Ragdoll) {
                attacher.getAttachPoint(bomb, this.bombAttachedPosition);
                bomb.attachTo(attacher, this.bombAttachedPosition);
                return (Ragdoll) attacher;
            }
        }
        return null;
    }

    private BombAttachable tryAttach(Bomb bomb) {
        BombAttachable nearestAttacher = null;
        float minDistance = this.attachThreshold;
        for (int i = 0; i < this.attachers.size(); i++) {
            BombAttachable attacher = this.attachers.get(i);
            float distance = attacher.getAttachPoint(bomb, this.bombAttachedPosition);
            if (distance < minDistance) {
                minDistance = distance;
                this.minBombAttachedPosition.set(this.bombAttachedPosition);
                nearestAttacher = attacher;
                if (distance <= 0.0f) {
                    break;
                }
            }
        }
        if (minDistance >= this.attachThreshold) {
            return null;
        }
        bomb.attachTo(nearestAttacher, this.minBombAttachedPosition);
        return nearestAttacher;
    }

    private Bomb fetchBomb(Bomb.Info info) {
        Bomb bomb = pollBomb(info.bombType);
        if (bomb != null) {
            bomb.setPosition(info.position);
            bomb.syncDrapPosition();
            bomb.setDelayConfig(info.delayConfig);
            bomb.setVisible(true);
        }
        return bomb;
    }

    public void explode() {
        if (couldExplode()) {
            Iterator<Brick> it = this.bricks.iterator();
            while (it.hasNext()) {
                it.next().setDynamic();
            }
            clearPreviousBombLayout();
            for (int i = 0; i < this.attachers.size(); i++) {
                BombAttachable attacher = this.attachers.get(i);
                attacher.explode();
                attacher.getBombInfo(this.previousBombLayout);
            }
            EventManager.getInstance().postEvent(EventType.StartExplode);
            this.timeAfterExploded = 0.0f;
            this.timeAfterLineMeet = 0.0f;
            this.gameStatus = GameStatus.Exploded;
            GameManager.getInstance().showAds(true);
        }
    }

    public boolean couldExplode() {
        if (isOperating()) {
            for (int i = 0; i < this.attachers.size(); i++) {
                if (this.attachers.get(i).isAttached()) {
                    return true;
                }
            }
        }
        return false;
    }

    public float getBricksTop() {
        float top = Float.MAX_VALUE;
        for (int i = 0; i < this.attachers.size(); i++) {
            BombAttachable attacher = this.attachers.get(i);
            if (attacher.isActive()) {
                float y = attacher.getTop();
                if (y < top) {
                    top = y;
                }
            }
        }
        return top;
    }

    public void finish() {
        this.attachers.clear();
        this.bricks.clear();
        this.bombsUsed.clear();
        this.smallBombsQueue.clear();
        this.bigBombsQueue.clear();
        this.magnentBombsQueue.clear();
        this.currentBomb = null;
        this.isTouchCaptured = false;
        this.gameStatus = GameStatus.Operating;
        this.cameraXMovingSpeed = 0.0f;
        this.cameraYMovingSpeed = 0.0f;
    }

    private Bomb testHitBomb(float x, float y) {
        float viewX = this.camera.screenToWorldX(x);
        float viewY = this.camera.screenToWorldY(y);
        Bomb nearestBomb = null;
        float minDistance = Float.MAX_VALUE;
        Iterator<Bomb> it = this.bombsUsed.iterator();
        while (it.hasNext()) {
            Bomb bomb = it.next();
            if (bomb.hitTest(viewX, viewY)) {
                float distance = bomb.getHitDistance(viewX, viewY);
                if (distance < minDistance) {
                    nearestBomb = bomb;
                    minDistance = distance;
                }
            }
        }
        return nearestBomb;
    }

    public boolean isGameFinished() {
        return this.gameStatus.equals(GameStatus.Finished);
    }

    /* access modifiers changed from: protected */
    public void touchDown(int x, int y) {
        if (isOperating()) {
            this.cameraXMovingSpeed = 0.0f;
            this.cameraYMovingSpeed = 0.0f;
            this.previousTouch.set((float) x, (float) y);
            this.originalTouchWorld.set((float) x, (float) y);
            this.camera.screenToWorld(this.originalTouchWorld);
            if (PlayingUI.getInstance().hitTest((float) x, (float) y)) {
                this.isTouchCaptured = false;
                return;
            }
            Bomb nearestBomb = testHitBomb((float) x, (float) y);
            if (nearestBomb != null) {
                captureBomb(nearestBomb);
            }
            this.isTouchCaptured = true;
        }
    }

    /* access modifiers changed from: protected */
    public void touchMove(int x, int y) {
        if (isOperating() && this.isTouchCaptured) {
            if (isCaptured()) {
                this.currentBomb.setDragPosition((this.originalAttacherWorld.X + this.camera.screenToWorldX((float) x)) - this.originalTouchWorld.X, (this.originalAttacherWorld.Y + this.camera.screenToWorldY((float) y)) - this.originalTouchWorld.Y);
                if (tryAttach(this.currentBomb) == null) {
                    this.currentBomb.setPosition(this.currentBomb.getDragPosition());
                    this.currentBomb.detachFrom();
                }
                if (!isWaiting(x, y)) {
                    this.waitedTime = 0.0f;
                }
                if (this.cameraStatus.equals(CameraStatus.Zoomed)) {
                    this.cameraXMovingSpeed = getCameraXMovingSpeed(x);
                    this.cameraYMovingSpeed = getCameraYMovingSpeed(y);
                }
            } else if (this.cameraStatus.equals(CameraStatus.Zoomed)) {
                float zoom = this.camera.getZoom();
                setCamera(this.camera.getCameraX() - ((((float) x) - this.previousTouch.X) * zoom), this.camera.getCameraY() - ((((float) y) - this.previousTouch.Y) * zoom));
                this.waitedTime = 0.0f;
            }
            this.previousTouch.set((float) x, (float) y);
        }
    }

    /* access modifiers changed from: protected */
    public void touchUp(int x, int y) {
        if (isOperating()) {
            this.cameraXMovingSpeed = 0.0f;
            this.cameraYMovingSpeed = 0.0f;
            releaseBomb();
        }
    }

    private void setCamera(float cameraX, float cameraY) {
        float zoom = this.camera.getZoom();
        float minX = ((float) GraphicsManager.screenWidth()) * zoom * 0.5f;
        float maxX = ((float) GraphicsManager.screenWidth()) - minX;
        float minY = ((float) GraphicsManager.screenHeight()) * zoom * 0.5f;
        float maxY = minY + ((1.0f - zoom) * ((float) PlayingUI.groundY));
        this.camera.setCamera(Math.max(minX, Math.min(maxX, cameraX)), Math.max(minY, Math.min(maxY, cameraY)));
    }

    /* access modifiers changed from: protected */
    public void updateInternal(float elapsedTime) {
        switch ($SWITCH_TABLE$com$camelgames$explode$manipulation$BombManipulator$CameraStatus()[this.cameraStatus.ordinal()]) {
            case 1:
                if (isCaptured()) {
                    this.cameraStatus = CameraStatus.ZoomIn;
                    this.zoomAnchor.set(this.currentBomb.getX(), this.currentBomb.getY());
                    break;
                }
                break;
            case 2:
                if (this.camera.zoom(-1.0f * elapsedTime, 0.6f)) {
                    this.cameraStatus = CameraStatus.Zoomed;
                    this.waitedTime = 0.0f;
                }
                mapCamera(this.zoomAnchor);
                break;
            case 3:
                if (this.camera.zoom(1.0f * elapsedTime, 1.0f)) {
                    this.cameraStatus = CameraStatus.Normal;
                }
                mapCamera(this.zoomAnchor);
                break;
            case 4:
                if (isCaptured() && !(this.cameraXMovingSpeed == 0.0f && this.cameraYMovingSpeed == 0.0f)) {
                    setCamera(this.camera.getCameraX() - (this.cameraXMovingSpeed * elapsedTime), this.camera.getCameraY() - (this.cameraYMovingSpeed * elapsedTime));
                }
                if (!isCaptured() && !PlayingUI.getInstance().isShowingBombConfigUI()) {
                    this.waitedTime += elapsedTime;
                    if (this.waitedTime >= 2.0f) {
                        this.cameraStatus = CameraStatus.ZoomOut;
                        this.camera.getZoomAnchor(this.zoomAnchor);
                        break;
                    }
                } else {
                    this.waitedTime = 0.0f;
                    break;
                }
                break;
        }
        switch ($SWITCH_TABLE$com$camelgames$explode$manipulation$BombManipulator$GameStatus()[this.gameStatus.ordinal()]) {
            case 2:
                if (this.bombsUsed.isEmpty()) {
                    this.gameStatus = GameStatus.PendingJuge;
                    break;
                }
                break;
            case 3:
                this.timeAfterExploded += PhysicsManager.instance.getElapsedTime();
                if (!PlayingUI.getInstance().meetTargetLine()) {
                    this.timeAfterLineMeet = 0.0f;
                    if (this.timeAfterExploded <= 16.0f) {
                        if (this.timeAfterExploded > 8.0f && !isMovingDramatically()) {
                            loseLevel();
                            break;
                        }
                    } else {
                        loseLevel();
                        break;
                    }
                } else {
                    this.timeAfterLineMeet += PhysicsManager.instance.getElapsedTime();
                    if (this.timeAfterLineMeet <= 8.0f) {
                        if (this.timeAfterLineMeet >= 4.0f && !isMovingDramatically()) {
                            passLevel();
                            break;
                        }
                    } else {
                        passLevel();
                        break;
                    }
                }
        }
        PlayingUI.getInstance().update(elapsedTime);
    }

    public void loseLevel() {
        if (!this.gameStatus.equals(GameStatus.Finished)) {
            EventManager.getInstance().postEvent(EventType.LevelFailed);
            this.gameStatus = GameStatus.Finished;
        }
    }

    private void passLevel() {
        if (!this.gameStatus.equals(GameStatus.Finished)) {
            EventManager.getInstance().postEvent(EventType.LevelFinished);
            this.gameStatus = GameStatus.Finished;
        }
    }

    public void HandleEvent(AbstractEvent e) {
        if (!isStoped()) {
            switch ($SWITCH_TABLE$com$camelgames$framework$events$EventType()[e.getType().ordinal()]) {
                case 9:
                    if (isOperating()) {
                        TapEvent tap1 = (TapEvent) e;
                        Bomb nearestBomb = testHitBomb(tap1.getX(), tap1.getY());
                        if (nearestBomb != null && nearestBomb.isAttached()) {
                            PlayingUI.getInstance().showBombConfig(tap1.getX(), tap1.getY(), nearestBomb);
                            releaseBomb();
                            return;
                        }
                        return;
                    }
                    return;
                case 10:
                    if (!isGameFinished()) {
                        TapEvent tap = (TapEvent) e;
                        if (PlayingUI.getInstance().hitTest(tap.getX(), tap.getY())) {
                            return;
                        }
                        if (this.cameraStatus.equals(CameraStatus.Normal)) {
                            this.cameraStatus = CameraStatus.ZoomIn;
                            this.zoomAnchor.set(tap.getX(), tap.getY());
                            return;
                        } else if (this.cameraStatus.equals(CameraStatus.Zoomed)) {
                            this.cameraStatus = CameraStatus.ZoomOut;
                            this.camera.getZoomAnchor(this.zoomAnchor);
                            return;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public void render(GL10 gl, float elapsedTime) {
        for (int i = 0; i < this.bricks.size(); i++) {
            this.bricks.get(i).render(gl, elapsedTime);
        }
    }

    public void drawMagnifier(GL10 gl, float elapsedTime) {
        if (isCaptured()) {
            gl.glPushMatrix();
            gl.glEnable(3089);
            float marginX = 0.38f * ((float) GLScreenView.scissorWidth);
            float marginY = 0.38f * ((float) GLScreenView.scissorHeight);
            float bombCenterX = Math.max(marginX, Math.min(((float) GraphicsManager.screenWidth()) - marginX, this.currentBomb.getX()));
            float bombCenterY = Math.max(marginY, Math.min(((float) GraphicsManager.screenHeight()) - marginY, this.currentBomb.getY()));
            float magnifierX = this.camera.screenToWorldX((float) GLScreenView.scissorCenterX);
            GameManager.getInstance().getBackground().renderRelative(gl, GLScreenView.scissorCenterX - ((int) this.camera.worldToScreenX(bombCenterX)), GLScreenView.scissorCenterY - ((int) this.camera.worldToScreenY(bombCenterY)));
            gl.glTranslatef(magnifierX - bombCenterX, this.camera.screenToWorldY((float) GLScreenView.scissorCenterY) - bombCenterY, 0.0f);
            for (int i = 0; i < this.attachers.size(); i++) {
                BombAttachable attacher = this.attachers.get(i);
                if (attacher.getMinDistance(this.currentBomb.getX(), this.currentBomb.getY(), null) < ((float) GLScreenView.scissorWidth) * 0.5f) {
                    attacher.render(gl, elapsedTime);
                }
            }
            Iterator<Bomb> it = this.bombsUsed.iterator();
            while (it.hasNext()) {
                it.next().render(gl, elapsedTime);
            }
            this.frameTexture.drawOES();
            Sprite2D.flush(true);
            gl.glDisable(3089);
            gl.glPopMatrix();
        }
    }

    public void captureBomb(Bomb bomb) {
        releaseBomb();
        if (bomb != null) {
            this.currentBomb = bomb;
            this.currentBomb.syncDrapPosition();
            this.originalAttacherWorld.set(this.currentBomb.getX(), this.currentBomb.getY());
            if (!this.currentBomb.isAttached()) {
                useBomb(this.currentBomb);
            }
            EventManager.getInstance().postEvent(EventType.Captured);
            this.isTouchCaptured = true;
        }
    }

    public ArrayList<BombAttachable> getAttachers() {
        return this.attachers;
    }

    private void useBomb(Bomb bomb) {
        removeBomb(bomb);
        this.bombsUsed.add(bomb);
        updateBombCount();
    }

    private void releaseBomb() {
        if (isCaptured()) {
            recycleBomb(this.currentBomb);
            this.currentBomb = null;
        }
        this.isTouchCaptured = false;
    }

    private void recycleBomb(Bomb bomb) {
        if (!bomb.isAttached()) {
            bomb.setDelayConfig(0.0f);
            bomb.setVisible(false);
            this.bombsUsed.remove(bomb);
            addBomb(bomb);
            updateBombCount();
        }
    }

    private void updateBombCount() {
        PlayingUI.getInstance().setSmallBombCount(this.smallBombsQueue.size());
        PlayingUI.getInstance().setBigBombCount(this.bigBombsQueue.size());
        PlayingUI.getInstance().setMagnentBombCount(this.magnentBombsQueue.size());
    }

    private boolean isCaptured() {
        return this.currentBomb != null;
    }

    private boolean isWaiting(int x, int y) {
        return MathUtils.length(((float) x) - this.previousTouch.X, ((float) y) - this.previousTouch.Y) < this.waitingThreshod;
    }

    private void mapCamera(Vector2 anchor) {
        this.cameraMapped.set(anchor);
        this.camera.getMappedCamera(this.cameraMapped);
        setCamera(this.cameraMapped.X, this.cameraMapped.Y);
    }

    private boolean isMovingDramatically() {
        for (int i = 0; i < this.bricks.size(); i++) {
            if (this.bricks.get(i).isMovingDramatically()) {
                return true;
            }
        }
        return false;
    }

    private float getCameraXMovingSpeed(int x) {
        if (((float) x) < this.senseMartinX) {
            return ((this.senseMartinX - ((float) x)) / this.senseMartinX) * this.movingSpeed;
        }
        if (((float) x) > ((float) GraphicsManager.screenWidth()) - this.senseMartinX) {
            return (((((float) GraphicsManager.screenWidth()) - this.senseMartinX) - ((float) x)) / this.senseMartinX) * this.movingSpeed;
        }
        return 0.0f;
    }

    private float getCameraYMovingSpeed(int y) {
        if (((float) y) < this.senseMarginY) {
            return ((this.senseMarginY - ((float) y)) / this.senseMarginY) * this.movingSpeed;
        }
        if (((float) y) > ((float) GraphicsManager.screenHeight()) - this.senseMarginY) {
            return (((((float) GraphicsManager.screenHeight()) - this.senseMarginY) - ((float) y)) / this.senseMarginY) * this.movingSpeed;
        }
        return 0.0f;
    }
}
