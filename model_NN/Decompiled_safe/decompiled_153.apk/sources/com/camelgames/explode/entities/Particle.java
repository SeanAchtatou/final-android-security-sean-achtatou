package com.camelgames.explode.entities;

import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.ndk.graphics.OESSprite;
import javax.microedition.khronos.opengles.GL10;

public class Particle {
    public boolean active;
    public float b;
    public float fade;
    public float g;
    public float life;
    public float r;
    public float size;
    public float x;
    public float xGravity;
    public float xSpeed;
    public float y;
    public float yGravity;
    public float ySpeed;

    public void update(float elapsedTime) {
        if (this.active) {
            this.x += this.xSpeed * elapsedTime;
            this.y += this.ySpeed * elapsedTime;
            this.xSpeed += this.xGravity;
            this.ySpeed += this.yGravity;
            this.life -= this.fade * elapsedTime;
            if (this.life <= 0.0f) {
                this.active = false;
            }
        }
    }

    public void render(GL10 gl, OESSprite texture) {
        if (this.active) {
            texture.setColor(this.r * this.life, this.g * this.life, this.b * this.life, this.life);
            texture.setPosition((int) GraphicsManager.worldToScreenX(this.x), (int) GraphicsManager.worldToScreenY(this.y));
            texture.setSize((int) this.size, (int) this.size);
            texture.drawOES();
        }
    }
}
