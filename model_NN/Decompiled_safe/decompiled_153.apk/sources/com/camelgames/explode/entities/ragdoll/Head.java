package com.camelgames.explode.entities.ragdoll;

import com.camelgames.explode.entities.ragdoll.Ragdoll;
import com.camelgames.framework.physics.PhysicsManager;

public class Head extends Piece {
    public Head(Ragdoll.Config.PieceInfo info) {
        super(info.getResourceId(), info.getWidth(), info.getHeight());
        createBody();
        PhysicsManager.instance.createCircle(getBody(), 0.5f * info.getPhysicsHeight(), density * 0.1f, friction, restitution);
        setFilterData(1, 3, 1);
    }
}
