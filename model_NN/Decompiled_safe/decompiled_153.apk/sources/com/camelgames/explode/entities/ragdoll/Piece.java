package com.camelgames.explode.entities.ragdoll;

import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.physics.PhysicsManager;
import com.camelgames.framework.physics.PhysicsalRenderableListenerEntity;
import com.camelgames.ndk.graphics.Sprite2D;
import javax.microedition.khronos.opengles.GL10;

public abstract class Piece extends PhysicsalRenderableListenerEntity {
    public static final int categoryBits = 1;
    public static float density = 0.25f;
    public static float friction = 0.5f;
    public static final int groupIndex = 1;
    public static final int maskBits = 3;
    public static float restitution = 0.3f;
    private float height;
    private Sprite2D texture = new Sprite2D();
    private float width;

    public Piece(Integer resourceId, float width2, float height2) {
        this.texture.setTexId(resourceId.intValue());
        this.texture.setSize(width2, height2);
        this.width = width2;
        this.height = height2;
    }

    public void render(GL10 gl, float elapsedTime) {
        this.texture.render(elapsedTime);
    }

    public void SyncPhysicsPosition() {
        super.SyncPhysicsPosition();
        this.texture.setPosition(this.position.X, this.position.Y, this.angle);
    }

    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        this.texture.setPosition(this.position.X, this.position.Y);
    }

    public void setAngle(float angle) {
        super.setAngle(angle);
        this.texture.setAngle(angle);
    }

    public float getWidth() {
        return this.width;
    }

    public float getHeight() {
        return this.height;
    }

    public float getLeft() {
        return this.position.X - (0.5f * this.width);
    }

    public float getRight() {
        return this.position.X + (0.5f * this.width);
    }

    public float getTop() {
        return this.position.Y - (0.5f * this.height);
    }

    public float getBottom() {
        return this.position.Y + (0.5f * this.height);
    }

    public void setCenterTop(float centerX, float top) {
        setPosition(centerX, (0.5f * this.height) + top);
    }

    public void setCenterBottom(float centerX, float bottom) {
        setPosition(centerX, bottom - (0.5f * this.height));
    }

    public void HandleEvent(AbstractEvent e) {
    }

    public void update(float elapsedTime) {
        SyncPhysicsPosition();
    }

    /* access modifiers changed from: protected */
    public void initiateRectPhysics(float width2, float height2) {
        createBody();
        PhysicsManager.instance.createRect(getBody(), width2, height2, density, friction, restitution);
    }

    /* access modifiers changed from: protected */
    public void initiateCirclePhysics(float radius) {
        createBody();
        PhysicsManager.instance.createCircle(getBody(), radius, density, friction, restitution);
    }
}
