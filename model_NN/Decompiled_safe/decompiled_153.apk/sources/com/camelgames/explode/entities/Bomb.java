package com.camelgames.explode.entities;

import com.camelgames.blowuplite.R;
import com.camelgames.explode.entities.ragdoll.Ragdoll;
import com.camelgames.explode.game.GameManager;
import com.camelgames.explode.manipulation.BombManipulator;
import com.camelgames.framework.Skeleton.RenderableListenerEntity;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.graphics.Camera2D;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.Renderable;
import com.camelgames.framework.math.MathUtils;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.physics.PhysicsManager;
import com.camelgames.framework.utilities.Excute;
import com.camelgames.ndk.graphics.SequentialSprite;
import com.camelgames.ndk.graphics.Sprite2D;
import javax.microedition.khronos.opengles.GL10;

public class Bomb extends RenderableListenerEntity {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$entities$Bomb$Status;
    public static final float basicWidth = (0.1f * ((float) GraphicsManager.screenHeight()));
    private static final float hitTestRadius = (0.05f * ((float) GraphicsManager.screenWidthPlusHeight()));
    private static final Vector2 impulse = new Vector2(-1.0f, 0.0f);
    private float attachedAngleOffset;
    protected Vector2 attachedOffset = new Vector2();
    /* access modifiers changed from: private */
    public BombAttachable attacher;
    private Sprite2D bombTexture;
    private Camera2D camera = ((Camera2D) GraphicsManager.getInstance().getCamera());
    private SequentialSprite crashTexture;
    private float delayConfig;
    private float delayTime;
    private Vector2 dragPosition = new Vector2();
    private Excute impulseExcutor;
    private Status status = Status.Waiting;

    public static class Info {
        public boolean bindToRagdoll;
        public Type bombType;
        public float delayConfig;
        public Vector2 position;
    }

    private enum Status {
        Waiting,
        Delay,
        Exploding,
        Exploded
    }

    public enum Type {
        Small,
        Big,
        Magnent
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$entities$Bomb$Status() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$explode$entities$Bomb$Status;
        if (iArr == null) {
            iArr = new int[Status.values().length];
            try {
                iArr[Status.Delay.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Status.Exploded.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Status.Exploding.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Status.Waiting.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$camelgames$explode$entities$Bomb$Status = iArr;
        }
        return iArr;
    }

    public Type getType() {
        return Type.Small;
    }

    public Bomb() {
        BombManipulator.getInstance().addBomb(this);
        this.bombTexture = createBombTexture();
        this.bombTexture.setColor(0.3f);
        this.crashTexture = createExplodeTexture();
        setPriority(Renderable.PRIORITY.HIGH);
    }

    public Info getInfo() {
        Info info = new Info();
        info.position = new Vector2(this.position);
        info.delayConfig = this.delayConfig;
        info.bombType = getType();
        return info;
    }

    /* access modifiers changed from: protected */
    public Sprite2D createBombTexture() {
        Sprite2D texture = new Sprite2D(basicWidth, basicWidth);
        texture.setTexId(R.array.altas1_bomb);
        return texture;
    }

    /* access modifiers changed from: protected */
    public SequentialSprite createExplodeTexture() {
        SequentialSprite texture = new SequentialSprite(basicWidth * 2.5f, basicWidth * 2.5f);
        texture.setTexId(R.array.altas1_explode);
        texture.setChangeTime(0.1f);
        texture.setLoop(false);
        texture.setStop(true);
        return texture;
    }

    /* access modifiers changed from: protected */
    public void disposeInternal() {
        BombManipulator.getInstance().removeBomb(this);
        detachFrom();
        super.disposeInternal();
    }

    public boolean hitTest(float x, float y) {
        return getHitDistance(x, y) < hitTestRadius * this.camera.getZoom();
    }

    public float getHitDistance(float x, float y) {
        return MathUtils.length(x - getX(), y - getY());
    }

    public void render(GL10 gl, float elapsedTime) {
        switch ($SWITCH_TABLE$com$camelgames$explode$entities$Bomb$Status()[this.status.ordinal()]) {
            case 4:
                this.crashTexture.render(elapsedTime);
                return;
            default:
                this.bombTexture.render(elapsedTime);
                return;
        }
    }

    public void HandleEvent(AbstractEvent e) {
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void update(float elapsedTime) {
        switch ($SWITCH_TABLE$com$camelgames$explode$entities$Bomb$Status()[this.status.ordinal()]) {
            case 1:
                break;
            case 2:
                this.delayTime += PhysicsManager.instance.getElapsedTime();
                if (this.delayTime >= this.delayConfig) {
                    startExplode();
                    break;
                }
                break;
            case 3:
            default:
                return;
            case 4:
                if (this.crashTexture.isStopped()) {
                    delete();
                    return;
                }
                return;
        }
        if (isAttached()) {
            syncPosAngle();
        }
        this.bombTexture.setPosition(getX(), getY(), getAngle());
    }

    public Vector2 getDragPosition() {
        return this.dragPosition;
    }

    public void syncDrapPosition() {
        this.dragPosition.set(this.position);
    }

    public void setDragPosition(float x, float y) {
        this.dragPosition.set(x, y);
    }

    public void explode() {
        this.impulseExcutor = new Excute() {
            /* Debug info: failed to restart local var, previous not found, register: 2 */
            public void excute() {
                if (Bomb.this.attacher instanceof Brick) {
                    Bomb.this.applyImpulseToBrick((Brick) Bomb.this.attacher);
                } else if (Bomb.this.attacher instanceof Ragdoll) {
                    Bomb.this.applyImpulseToRagdoll((Ragdoll) Bomb.this.attacher);
                }
            }
        };
        this.status = Status.Delay;
    }

    /* access modifiers changed from: protected */
    public void applyImpulseToRagdoll(Ragdoll ragdoll) {
        ragdoll.breakOut(impulse.getLength() * 1.0f);
    }

    /* access modifiers changed from: protected */
    public void applyImpulseToBrick(Brick attachedBrick) {
        Vector2 mainImpulse = Vector2.multiply(impulse, GameManager.getInstance().getBombPower());
        attachedBrick.breakOut(Vector2.substract(this.attachedOffset, getBreakOffset(attachedBrick, 0.5f)), mainImpulse);
        splitBrick(attachedBrick, 1.0f, new Vector2(impulse)).delete();
        attachedBrick.applyImpulse(-mainImpulse.X, -mainImpulse.Y);
    }

    /* access modifiers changed from: protected */
    public Brick splitBrick(Brick originalBrick, float thickRate, Vector2 impulse2) {
        Vector2 breakPoint = getBreakOffset(originalBrick, thickRate);
        if (originalBrick.getDesignAngle() == 135.0f) {
            breakPoint.substract(originalBrick.getLocalCenter().X, -originalBrick.getLocalCenter().Y);
        } else {
            breakPoint.substract(originalBrick.getLocalCenter());
        }
        return originalBrick.breakOut(breakPoint, new Vector2(impulse2));
    }

    public boolean isAttached() {
        return this.attacher != null;
    }

    public BombAttachable getAttacher() {
        return this.attacher;
    }

    public void moveAttachedOffset(float deltaX, float deltaY) {
        this.attachedOffset.add(deltaX, deltaY);
    }

    public void setAttacher(BombAttachable attacher2) {
        if (this.attacher != null) {
            this.attacher.detach(this);
        }
        this.attacher = attacher2;
        attacher2.attach(this);
    }

    public void attachTo(BombAttachable attacher2, Vector2 attachedPos) {
        if (attacher2 != this.attacher) {
            detachFrom();
            this.attacher = attacher2;
            attacher2.attach(this);
            this.bombTexture.setColor(1.0f);
            if (attacher2 instanceof Ragdoll) {
                EventManager.getInstance().postEvent(EventType.AttachRagdoll);
            } else {
                EventManager.getInstance().postEvent(EventType.AttachStick);
            }
        }
        this.attachedOffset.set(attachedPos.X - attacher2.getX(), attachedPos.Y - attacher2.getY());
        this.attachedOffset.RotateVector(-attacher2.getAngle());
        this.attachedAngleOffset = attacher2.getBombAngle() - attacher2.getAngle();
        syncPosAngle();
    }

    public void detachFrom() {
        if (this.attacher != null) {
            this.attacher.detach(this);
            this.attacher = null;
            setAngle(0.0f);
            this.attachedOffset.setZero();
            this.attachedAngleOffset = 0.0f;
            this.bombTexture.setColor(0.3f);
        }
    }

    public void setDelayConfig(float delay) {
        this.delayConfig = delay;
    }

    public float getDelayConfig() {
        return this.delayConfig;
    }

    public boolean isExploding() {
        return this.status.equals(Status.Exploding);
    }

    public boolean isExploded() {
        return this.status.equals(Status.Exploded);
    }

    /* access modifiers changed from: protected */
    public Vector2 getBreakOffset(Brick attachedBrick, float thickRate) {
        Vector2 offset = new Vector2(attachedBrick.getThick() * thickRate, 0.0f);
        if (attachedBrick.getDesignAngle() == 45.0f) {
            offset.RotateVector(0.7853982f);
        } else if (attachedBrick.getDesignAngle() == 135.0f) {
            offset.RotateVector(-0.7853982f);
        } else if (attachedBrick.getDesignAngle() == 90.0f) {
            offset.RotateVector(1.5707964f);
        }
        return offset;
    }

    private void startExplode() {
        this.status = Status.Exploding;
        if (this.impulseExcutor != null) {
            this.impulseExcutor.excute();
        }
        this.status = Status.Exploded;
        this.crashTexture.setPosition(this.position.X, this.position.Y, this.angle);
        this.crashTexture.setStop(false);
        EventManager.getInstance().postEvent(EventType.Bomb);
    }

    private void syncPosAngle() {
        if (isAttached()) {
            this.position.set(this.attachedOffset);
            this.position.RotateVector(this.attacher.getAngle());
            this.position.add(this.attacher.getX(), this.attacher.getY());
            this.angle = this.attachedAngleOffset + this.attacher.getAngle();
        }
    }
}
