package com.camelgames.explode.entities;

import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.utilities.IOUtility;
import com.camelgames.ndk.graphics.Texture;
import com.camelgames.ndk.graphics.TextureManager;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

public class Line {
    private static final float[] textureCoords = {0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 1.0f};
    private float alpha = 1.0f;
    private float blue = 1.0f;
    private float green = 1.0f;
    private float length;
    private Texture lineTexture;
    private final FloatBuffer lineTextureCoordsBuffer = IOUtility.createFloatBuffer(textureCoords);
    private float offset;
    private Vector2 position = new Vector2();
    private float red = 1.0f;
    private float speed;
    private float textureUnit;
    private float thick;
    private float unitLength;
    private final FloatBuffer vertexBuffer = GraphicsManager.getInstance().getDefaultVertexBuffer();

    public Line(float thick2, float unitLength2, int resourceId) {
        this.thick = thick2;
        this.unitLength = unitLength2;
        this.lineTexture = TextureManager.getInstance().getTexture(resourceId);
        this.lineTexture.setGLPara(9729.0f, 9729.0f, 10497.0f, 33071.0f);
    }

    public void setStartEnd(Vector2 startPos, Vector2 endPos) {
        Vector2.getCenter(startPos, endPos, this.position);
        this.length = Vector2.substract(endPos, startPos).normalize();
        this.textureUnit = this.length / this.unitLength;
        this.lineTextureCoordsBuffer.put(0, 0.0f);
        this.lineTextureCoordsBuffer.put(2, 0.0f);
        this.lineTextureCoordsBuffer.put(4, this.textureUnit);
        this.lineTextureCoordsBuffer.put(6, this.textureUnit);
    }

    public void setSpeed(float speed2) {
        this.speed = speed2;
    }

    public void update(float elapsedTime) {
        if (this.speed != 0.0f) {
            this.offset += this.speed * elapsedTime;
            while (this.offset >= 1.0f) {
                this.offset -= 1.0f;
            }
            while (this.offset <= -1.0f) {
                this.offset += 1.0f;
            }
            this.lineTextureCoordsBuffer.put(0, this.offset);
            this.lineTextureCoordsBuffer.put(2, this.offset);
            this.lineTextureCoordsBuffer.put(4, this.offset + this.textureUnit);
            this.lineTextureCoordsBuffer.put(6, this.offset + this.textureUnit);
        }
    }

    public void render(GL10 gl, float elapsedTime) {
        this.lineTexture.bindTexture();
        gl.glBlendFunc(1, 771);
        gl.glColor4f(this.alpha * this.red, this.alpha * this.green, this.alpha * this.blue, this.alpha);
        gl.glPushMatrix();
        gl.glTranslatef(this.position.X, this.position.Y, 0.0f);
        gl.glScalef(this.length, this.thick, 1.0f);
        gl.glTexCoordPointer(2, 5126, 0, this.lineTextureCoordsBuffer);
        gl.glVertexPointer(2, 5126, 0, this.vertexBuffer);
        gl.glDrawArrays(5, 0, 4);
        gl.glPopMatrix();
    }

    public float getX() {
        return this.position.X;
    }

    public void setX(float x) {
        this.position.X = x;
    }

    public float getY() {
        return this.position.Y;
    }

    public void setY(float y) {
        this.position.Y = y;
    }

    public void setColor(float r, float g, float b) {
        this.red = r;
        this.green = g;
        this.blue = b;
    }

    public void setAlpha(float alpha2) {
        this.alpha = alpha2;
    }
}
