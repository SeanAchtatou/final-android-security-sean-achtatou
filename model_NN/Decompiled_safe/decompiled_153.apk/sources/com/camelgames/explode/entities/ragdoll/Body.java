package com.camelgames.explode.entities.ragdoll;

import com.camelgames.explode.entities.ragdoll.Ragdoll;
import com.camelgames.framework.graphics.GraphicsManager;

public class Body extends Piece {
    public static final float radius = (0.03f * ((float) GraphicsManager.screenHeight()));

    public Body(Ragdoll.Config.PieceInfo info) {
        super(info.getResourceId(), info.getWidth(), info.getHeight());
        initiateBoundaryCirclePhysics(radius);
    }

    public float getRadius() {
        return radius;
    }

    private void initiateBoundaryCirclePhysics(float radius2) {
        initiateCirclePhysics(radius2);
        setFilterData(1, 3, 1);
    }
}
