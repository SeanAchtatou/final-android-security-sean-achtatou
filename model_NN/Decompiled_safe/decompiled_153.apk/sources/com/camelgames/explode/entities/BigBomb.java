package com.camelgames.explode.entities;

import com.camelgames.blowuplite.R;
import com.camelgames.explode.entities.Bomb;
import com.camelgames.explode.entities.ragdoll.Ragdoll;
import com.camelgames.explode.game.GameManager;
import com.camelgames.framework.math.Vector2;
import com.camelgames.ndk.graphics.Sprite2D;

public class BigBomb extends Bomb {
    private static final Vector2 impulse = new Vector2(-2.0f, 0.0f);
    private static final Vector2 impulseSmall = new Vector2(0.0f, -0.8f);
    private static final Vector2 impulseSmall2 = new Vector2(0.0f, 0.8f);

    public Bomb.Type getType() {
        return Bomb.Type.Big;
    }

    /* access modifiers changed from: protected */
    public Sprite2D createBombTexture() {
        Sprite2D texture = new Sprite2D(basicWidth, basicWidth);
        texture.setTexId(R.array.altas1_bomb_big);
        return texture;
    }

    /* access modifiers changed from: protected */
    public void applyImpulseToRagdoll(Ragdoll ragdoll) {
        ragdoll.breakOut(impulse.getLength() * 1.0f);
    }

    /* access modifiers changed from: protected */
    public void applyImpulseToBrick(Brick attachedBrick) {
        Vector2 mainImpulse = Vector2.multiply(impulse, GameManager.getInstance().getBombPower());
        attachedBrick.breakOut(Vector2.substract(this.attachedOffset, getBreakOffset(attachedBrick, 0.5f)), mainImpulse);
        splitBrick(attachedBrick, 0.5f, Vector2.multiply(impulseSmall, GameManager.getInstance().getBombPower()));
        splitBrick(attachedBrick, 0.5f, Vector2.multiply(impulseSmall2, GameManager.getInstance().getBombPower()));
        attachedBrick.applyImpulse(-mainImpulse.X, -mainImpulse.Y);
    }
}
