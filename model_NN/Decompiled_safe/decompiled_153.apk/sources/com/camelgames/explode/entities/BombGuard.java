package com.camelgames.explode.entities;

import com.camelgames.blowuplite.R;
import com.camelgames.explode.manipulation.BombManipulator;
import com.camelgames.explode.ui.PlayingUI;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.events.CollisionEvent;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.physics.PhysicsManager;
import com.camelgames.framework.physics.PhysicsalRenderableListenerEntity;
import com.camelgames.ndk.graphics.SequentialSprite;
import com.camelgames.ndk.graphics.Sprite2D;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;

public class BombGuard extends PhysicsalRenderableListenerEntity {
    private static final int bombCount = Math.round(((float) PlayingUI.groundY) / bombGap);
    /* access modifiers changed from: private */
    public static final float bombGap = (0.73f * bombWidth);
    /* access modifiers changed from: private */
    public static final Sprite2D bombTexture = new Sprite2D(bombWidth, bombWidth);
    /* access modifiers changed from: private */
    public static final float bombWidth = (0.2f * ((float) GraphicsManager.screenHeight()));
    private ArrayList<BombItem> bombs = new ArrayList<>();
    private boolean isExploded;

    static {
        bombTexture.setTexId(R.array.altas1_bomb_guard);
    }

    public BombGuard(float guardX, boolean isGuardingLeft) {
        float bombX;
        if (isGuardingLeft) {
            bombX = guardX - (bombGap * 0.5f);
        } else {
            bombX = guardX + (bombGap * 0.5f);
        }
        float bombY = ((float) PlayingUI.groundY) - (bombGap * 0.5f);
        for (int i = 0; i < bombCount; i++) {
            this.bombs.add(new BombItem(bombX, bombY));
            bombY -= bombGap;
        }
        initiatePhysics(bombX);
        addEventType(EventType.Collide);
        addListener();
        setVisible(true);
    }

    public void render(GL10 gl, float elapsedTime) {
        for (int i = 0; i < this.bombs.size(); i++) {
            this.bombs.get(i).render(gl, elapsedTime);
        }
    }

    public void HandleEvent(AbstractEvent e) {
        if (!this.isExploded && e.getType().equals(EventType.Collide)) {
            CollisionEvent collisionEvent = (CollisionEvent) e;
            if (collisionEvent.tryGetOtherId(getId()) != -100) {
                float collisionY = PhysicsManager.physicsToScreen(collisionEvent.getContact().getTouchY());
                int firstExplodeIndex = -1;
                int i = 0;
                while (true) {
                    if (i >= this.bombs.size()) {
                        break;
                    } else if (this.bombs.get(i).hitTest(collisionY)) {
                        firstExplodeIndex = i;
                        break;
                    } else {
                        i++;
                    }
                }
                if (firstExplodeIndex >= 0) {
                    for (int i2 = 0; i2 < this.bombs.size(); i2++) {
                        this.bombs.get(i2).explode(0.2f * ((float) Math.abs(i2 - firstExplodeIndex)));
                    }
                }
                destroyBody();
                BombManipulator.getInstance().loseLevel();
                this.isExploded = true;
            }
        }
    }

    public void update(float elapsedTime) {
        boolean isFinished = true;
        for (int i = 0; i < this.bombs.size(); i++) {
            BombItem bomb = this.bombs.get(i);
            bomb.update(elapsedTime);
            if (isFinished && !bomb.isFinished().booleanValue()) {
                isFinished = false;
            }
        }
        if (isFinished) {
            delete();
        }
    }

    private void initiatePhysics(float bombX) {
        createBody();
        PhysicsManager.instance.createRect(getBody(), bombGap, (float) GraphicsManager.screenHeight(), 1.0f, 1.0f, 0.0f);
        getBody().SetCollisionCallback(true);
        setStatic();
        setPosition(bombX, 0.5f * ((float) GraphicsManager.screenHeight()));
    }

    private static class BombItem {
        private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$entities$BombGuard$BombItem$Status;
        private float delay;
        private SequentialSprite explodeTexture;
        private Status status = Status.Normal;
        private float x;
        private float y;

        private enum Status {
            Normal,
            WaitToExplode,
            Exploding,
            Finished
        }

        static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$entities$BombGuard$BombItem$Status() {
            int[] iArr = $SWITCH_TABLE$com$camelgames$explode$entities$BombGuard$BombItem$Status;
            if (iArr == null) {
                iArr = new int[Status.values().length];
                try {
                    iArr[Status.Exploding.ordinal()] = 3;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[Status.Finished.ordinal()] = 4;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[Status.Normal.ordinal()] = 1;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[Status.WaitToExplode.ordinal()] = 2;
                } catch (NoSuchFieldError e4) {
                }
                $SWITCH_TABLE$com$camelgames$explode$entities$BombGuard$BombItem$Status = iArr;
            }
            return iArr;
        }

        public BombItem(float x2, float y2) {
            this.x = x2;
            this.y = y2;
        }

        public void explode(float delay2) {
            this.delay = delay2;
            this.status = Status.WaitToExplode;
            this.explodeTexture = new SequentialSprite(BombGuard.bombWidth * 2.0f, BombGuard.bombWidth * 2.0f);
            this.explodeTexture.setTexId(R.array.altas1_explode);
            this.explodeTexture.setChangeTime(0.08f);
            this.explodeTexture.setLoop(false);
            this.explodeTexture.setStop(true);
            this.explodeTexture.setPosition(this.x, this.y, 0.0f);
        }

        public void update(float elapsedTime) {
            switch ($SWITCH_TABLE$com$camelgames$explode$entities$BombGuard$BombItem$Status()[this.status.ordinal()]) {
                case 2:
                    this.delay -= elapsedTime;
                    if (this.delay <= 0.0f) {
                        this.status = Status.Exploding;
                        this.explodeTexture.setStop(false);
                        EventManager.getInstance().postEvent(EventType.BombLarge);
                        return;
                    }
                    return;
                case 3:
                    if (this.explodeTexture.isStopped()) {
                        this.status = Status.Finished;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }

        public void render(GL10 gl, float elapsedTime) {
            switch ($SWITCH_TABLE$com$camelgames$explode$entities$BombGuard$BombItem$Status()[this.status.ordinal()]) {
                case 1:
                case 2:
                    BombGuard.bombTexture.setPosition(this.x, this.y, 0.0f);
                    BombGuard.bombTexture.render(elapsedTime);
                    return;
                case 3:
                    this.explodeTexture.render(elapsedTime);
                    return;
                default:
                    return;
            }
        }

        public boolean hitTest(float testY) {
            return Math.abs(testY - this.y) <= 0.5f * BombGuard.bombGap;
        }

        public Boolean isFinished() {
            return Boolean.valueOf(this.status.equals(Status.Finished));
        }
    }
}
