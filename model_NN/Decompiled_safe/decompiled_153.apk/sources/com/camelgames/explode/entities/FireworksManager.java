package com.camelgames.explode.entities;

import com.camelgames.blowuplite.R;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.math.MathUtils;
import com.camelgames.ndk.graphics.OESSprite;
import javax.microedition.khronos.opengles.GL10;

public class FireworksManager {
    private FireworksEffect[] fireworks = new FireworksEffect[5];
    private boolean isActive;

    public FireworksManager() {
        OESSprite fireworksTexture = new OESSprite();
        fireworksTexture.setSize((int) (((float) GraphicsManager.screenHeight()) * 0.1f), (int) (((float) GraphicsManager.screenHeight()) * 0.1f));
        fireworksTexture.setTexId(R.array.altas1_particle);
        for (int i = 0; i < this.fireworks.length; i++) {
            this.fireworks[i] = new FireworksEffect(fireworksTexture, 40);
        }
    }

    public void fire() {
        for (FireworksEffect item : this.fireworks) {
            float screenWidth = (float) GraphicsManager.screenWidth();
            float screenHeight = (float) GraphicsManager.screenHeight();
            item.startRising(MathUtils.random(0.4f, 0.6f) * screenWidth, screenHeight, MathUtils.random(-0.2f * screenWidth, 0.2f * screenWidth), MathUtils.random(-0.25f * screenHeight, -0.35f * screenHeight), MathUtils.random(0.05f * screenHeight, 0.1f * screenHeight));
        }
        this.isActive = true;
    }

    public boolean isFinished() {
        for (FireworksEffect item : this.fireworks) {
            if (!item.isFinished()) {
                return false;
            }
        }
        return true;
    }

    public void update(float elapsedTime) {
        if (this.isActive) {
            for (FireworksEffect item : this.fireworks) {
                item.update(elapsedTime);
            }
            if (isFinished()) {
                fire();
            }
        }
    }

    public void render(GL10 gl, float elapsedTime) {
        if (this.isActive) {
            for (FireworksEffect item : this.fireworks) {
                item.render(gl, elapsedTime);
            }
        }
    }

    public void setActive(boolean isActive2) {
        this.isActive = isActive2;
    }

    public boolean isActive() {
        return this.isActive;
    }
}
