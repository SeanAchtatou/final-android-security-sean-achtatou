package com.camelgames.explode.entities;

import com.camelgames.blowuplite.R;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.events.CollisionEvent;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.Renderable;
import com.camelgames.framework.physics.PhysicsManager;
import com.camelgames.framework.physics.PhysicsalRenderableListenerEntity;
import com.camelgames.ndk.graphics.OESSprite;
import com.camelgames.ndk.graphics.Sprite2D;
import javax.microedition.khronos.opengles.GL10;

public class Target extends PhysicsalRenderableListenerEntity {
    private FireworksEffect fireworksEffect;
    private boolean isCatched;
    private float radius = (15.0f * GraphicsManager.getInstance().getYScale());
    private Sprite2D star = new Sprite2D(this.radius * 2.0f, this.radius * 2.0f);

    public Target(float originalX, float originalY) {
        addEventType(EventType.Collide);
        addListener();
        this.star.setTexId(R.array.altas1_target);
        OESSprite fireworksTexture = new OESSprite();
        fireworksTexture.setSize((int) (((float) GraphicsManager.screenHeight()) * 0.1f), (int) (((float) GraphicsManager.screenHeight()) * 0.1f));
        fireworksTexture.setTexId(R.array.altas1_particle);
        this.fireworksEffect = new FireworksEffect(fireworksTexture, 40);
        this.fireworksEffect.prepareExplode(originalX, originalY, 0.2f * ((float) GraphicsManager.screenWidth()), ((float) GraphicsManager.screenHeight()) * 0.1f);
        setPriority(Renderable.PRIORITY.HIGH);
        setVisible(true);
        initiatePhysics();
        setPosition(originalX, originalY);
    }

    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        this.star.setPosition(this.position.X, this.position.Y);
    }

    public void HandleEvent(AbstractEvent e) {
        if (e.getType() == EventType.Collide && !this.isCatched && ((CollisionEvent) e).tryGetOtherId(getId()) != -100) {
            this.isCatched = true;
            destroyBody();
            EventManager.getInstance().postEvent(new AbstractEvent(EventType.Catched));
        }
    }

    public void update(float elapsedTime) {
        this.star.setAngle(this.star.getAngle() + (1.0471975f * elapsedTime));
        if (this.isCatched) {
            this.fireworksEffect.update(elapsedTime);
            if (this.fireworksEffect.isFinished()) {
                delete();
            }
        }
    }

    public void render(GL10 gl, float elapsedTime) {
        if (this.isCatched) {
            this.fireworksEffect.render(gl, elapsedTime);
        } else {
            this.star.render(elapsedTime);
        }
    }

    private void initiatePhysics() {
        createBody();
        getBody().SetCollisionCallback(true);
        PhysicsManager.instance.createCircle(getBody(), this.radius, 2.0f, 0.3f, 0.0f);
        setFilterData(2, 2, 1);
        setStatic();
    }
}
