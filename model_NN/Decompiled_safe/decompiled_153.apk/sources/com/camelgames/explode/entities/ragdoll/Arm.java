package com.camelgames.explode.entities.ragdoll;

import com.camelgames.explode.entities.ragdoll.Ragdoll;

public class Arm extends Piece {
    public Arm(Ragdoll.Config.PieceInfo info) {
        super(info.getResourceId(), info.getWidth(), info.getHeight());
        initiateCirclePhysics(0.5f * info.getPhysicsHeight());
        setFilterData(2, 2, -1);
    }
}
