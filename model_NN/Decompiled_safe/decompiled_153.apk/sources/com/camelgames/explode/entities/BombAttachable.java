package com.camelgames.explode.entities;

import com.camelgames.explode.entities.Bomb;
import com.camelgames.framework.math.Vector2;
import java.util.Collection;
import javax.microedition.khronos.opengles.GL10;

public interface BombAttachable {
    void applyMagnet(float f, float f2, float f3, float f4);

    void attach(Bomb bomb);

    void delete();

    void detach(Bomb bomb);

    void explode();

    float getAngle();

    float getAttachPoint(Bomb bomb, Vector2 vector2);

    float getBombAngle();

    void getBombInfo(Collection<Bomb.Info> collection);

    float getMinDistance(float f, float f2, Vector2 vector2);

    float getTop();

    float getX();

    float getY();

    boolean isActive();

    boolean isAttached();

    void render(GL10 gl10, float f);
}
