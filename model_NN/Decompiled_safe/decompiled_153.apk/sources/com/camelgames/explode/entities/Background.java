package com.camelgames.explode.entities;

import com.camelgames.explode.ui.PlayingUI;
import com.camelgames.framework.Skeleton.RenderableListenerEntity;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.graphics.Camera2D;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.Renderable;
import com.camelgames.ndk.graphics.Texture;
import com.camelgames.ndk.graphics.TextureManager;
import javax.microedition.khronos.opengles.GL10;

public class Background extends RenderableListenerEntity {
    private static final float screenHeight = ((float) GraphicsManager.screenHeight());
    private static final float screenWidth = ((float) GraphicsManager.screenWidth());
    private static final int[] texCoords = GraphicsManager.getInstance().getScreenTexCoordsOES(false);
    private Camera2D camera = ((Camera2D) GraphicsManager.getInstance().getCamera());
    private int height;
    private int left;
    private Texture texture;
    private int top;
    private int width;

    public Background(Integer resourceId) {
        this.texture = TextureManager.getInstance().getTexture(resourceId.intValue());
        setPosition(((float) GraphicsManager.getInstance().getScreenWidth()) * 0.5f, ((float) GraphicsManager.getInstance().getScreenHeight()) * 0.5f);
        setPriority(Renderable.PRIORITY.LOWEST);
        setVisible(true);
    }

    public void render(GL10 gl, float elapsedTime) {
        float cameraZoom = this.camera.getZoom();
        float zoom = ((1.0f - cameraZoom) * 0.5f) + cameraZoom;
        float cameraX = this.camera.getCameraX();
        float cameraY = this.camera.getCameraY();
        this.width = (int) (screenWidth / zoom);
        this.height = (int) (screenHeight / zoom);
        this.left = (int) (((screenWidth - ((float) this.width)) - ((cameraX - (screenWidth * 0.5f)) / zoom)) * 0.5f);
        this.top = (int) (((screenHeight - ((float) this.height)) - ((cameraY - (screenHeight * 0.5f)) / zoom)) * 0.5f);
        gl.glBlendFunc(1, 771);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        this.texture.bindTexture();
        GraphicsManager.getInstance().drawTexiOES(texCoords, this.left, this.top, this.width, this.height);
        PlayingUI.getInstance().renderLines(gl, elapsedTime);
    }

    public void renderRelative(GL10 gl, int offsetX, int offsetY) {
        gl.glBlendFunc(1, 771);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        this.texture.bindTexture();
        GraphicsManager.getInstance().drawTexiOES(texCoords, this.left + offsetX, this.top + offsetY, this.width, this.height);
    }

    public void update(float elapsedTime) {
    }

    public void HandleEvent(AbstractEvent e) {
    }
}
