package com.camelgames.explode.entities.ragdoll;

import com.camelgames.explode.entities.Bomb;
import com.camelgames.explode.entities.BombAttachable;
import com.camelgames.explode.manipulation.BombManipulator;
import com.camelgames.framework.Skeleton.RenderableListenerEntity;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.events.EventManager;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.Renderable;
import com.camelgames.framework.math.MathUtils;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.physics.RevoluteJoint;
import java.util.Collection;
import javax.microedition.khronos.opengles.GL10;

public class Ragdoll extends RenderableListenerEntity implements BombAttachable {
    private static Vector2 direction = new Vector2();
    /* access modifiers changed from: private */
    public static final float scaleRate = (((float) GraphicsManager.screenHeight()) / 320.0f);
    private Assemblies assemblies;
    private Bomb bomb;
    private float boundingRadius;
    private Status status = Status.StandTo;

    public enum Status {
        Flying,
        StandTo
    }

    public Ragdoll(Config config) {
        this.assemblies = new Assemblies(config);
        this.assemblies.assemblyBody();
        this.boundingRadius = this.assemblies.getBodyPiece().getWidth() * 2.0f;
        setPriority(Renderable.PRIORITY.HIGH);
        setVisible(true);
        BombManipulator.getInstance().addAttacher(this);
    }

    /* access modifiers changed from: protected */
    public void disposeInternal() {
        this.assemblies.dispose();
        this.assemblies = null;
        BombManipulator.getInstance().removeAttacher(this);
        super.disposeInternal();
    }

    public void render(GL10 gl, float elapsedTime) {
        this.assemblies.render(gl, elapsedTime);
    }

    public void update(float elapsedTime) {
        if (this.status.equals(Status.Flying)) {
            float bodyX = this.assemblies.getBodyPiece().getX();
            float bodyY = this.assemblies.getBodyPiece().getY();
            if (bodyX < (-this.boundingRadius) || bodyX > ((float) GraphicsManager.getInstance().getScreenWidth()) + this.boundingRadius || bodyY > ((float) GraphicsManager.getInstance().getScreenHeight()) + this.boundingRadius) {
                standTo();
            }
        }
    }

    public void HandleEvent(AbstractEvent e) {
    }

    public void setPosition(float bodyPositionX, float bodyPositionY) {
        if (!this.assemblies.isActive()) {
            this.assemblies.setActive(true);
        }
        this.assemblies.arrangePieces(bodyPositionX, bodyPositionY);
    }

    public void setAngle(float angle) {
        this.assemblies.setRotation(angle);
    }

    public void applyMagnet(float forceStength, float magnetRadius, float x, float y) {
        if (this.assemblies.isBroken()) {
            for (Piece piece : this.assemblies.pieces) {
                direction.set(x - piece.getX(), y - piece.getY());
                if (direction.normalize() < magnetRadius) {
                    float impulseStrenth = forceStength * 0.1f;
                    piece.applyForce(direction.X * impulseStrenth, direction.Y * impulseStrenth, piece.getX(), piece.getY());
                }
            }
            return;
        }
        direction.set(x - this.assemblies.body.getX(), y - this.assemblies.body.getY());
        if (direction.normalize() < magnetRadius) {
            float impulseStrenth2 = forceStength;
            this.assemblies.body.applyForce(direction.X * impulseStrenth2, direction.Y * impulseStrenth2);
        }
    }

    public void fire(float impulseX, float impulseY) {
        this.assemblies.setFixedPosition(false);
        this.assemblies.setCollision(true);
        this.assemblies.zeroVelocity();
        this.assemblies.setImpulse(impulseX, impulseY);
        this.status = Status.Flying;
        setVisible(true);
    }

    public float getMass() {
        return this.assemblies.getMass();
    }

    public Status getStatus() {
        return this.status;
    }

    public void standTo() {
        this.assemblies.setPositon((-this.boundingRadius) * 2.0f, 0.0f);
        this.assemblies.setFixedPosition(true);
        this.assemblies.setCollision(false);
        setVisible(false);
        this.status = Status.StandTo;
    }

    public boolean isIdInside(int id) {
        return this.assemblies.isIdInside(id);
    }

    public boolean isActive() {
        return this.assemblies.getBodyPiece().isActive();
    }

    public void breakOut(float impulse) {
        this.assemblies.breakOut(impulse);
        EventManager.getInstance().postEvent(EventType.RagdollDied);
    }

    public void attach(Bomb bomb2) {
        this.bomb = bomb2;
    }

    public void detach(Bomb bomb2) {
        if (this.bomb == bomb2) {
            this.bomb = null;
        }
    }

    public boolean isAttached() {
        return this.bomb != null;
    }

    public void explode() {
        if (this.bomb != null) {
            this.bomb.explode();
        }
    }

    public void getBombInfo(Collection<Bomb.Info> infos) {
        if (this.bomb != null) {
            Bomb.Info info = this.bomb.getInfo();
            info.bindToRagdoll = true;
            infos.add(info);
        }
    }

    public boolean couldAttach(Bomb bomb2) {
        return !isAttached() || bomb2.getAttacher() == this;
    }

    public float getAttachPoint(Bomb bomb2, Vector2 attachPoint) {
        if (!couldAttach(bomb2)) {
            return Float.MAX_VALUE;
        }
        Vector2 bombPos = bomb2.getDragPosition();
        attachPoint.set(getX(), getY());
        float distance = MathUtils.length(attachPoint, bombPos);
        if (bomb2.getAttacher() == this) {
            if (distance < this.assemblies.getBodyRadius() * 2.0f) {
                distance = -1.0f;
            } else {
                bomb2.setPosition(bomb2.getDragPosition());
                bomb2.detachFrom();
                distance = Float.MAX_VALUE;
            }
        } else if (distance < this.assemblies.getBodyRadius() * 1.0f) {
            distance = -1.0f;
        }
        return distance;
    }

    public float getMinDistance(float x, float y, Vector2 intersect) {
        if (intersect != null) {
            intersect.set(this.assemblies.getBodyX(), this.assemblies.getBodyY());
        }
        return MathUtils.length(getX() - x, getY() - y);
    }

    public float getBombAngle() {
        return getAngle();
    }

    public float getX() {
        return this.assemblies.getBodyX();
    }

    public float getY() {
        return this.assemblies.getBodyY();
    }

    public float getAngle() {
        return this.assemblies.getBodyAngle();
    }

    public float getTop() {
        return this.assemblies.getBodyPiece().getTop();
    }

    private static class Assemblies {
        private static Vector2 direction = new Vector2();
        /* access modifiers changed from: private */
        public Body body;
        private Config config;
        private Head head;
        private boolean isBroken;
        private RevoluteJoint[] joints;
        private float mass;
        public Piece[] pieces;

        public Assemblies(Config config2) {
            this.config = config2;
        }

        public void dispose() {
            for (Piece piece : this.pieces) {
                piece.delete();
            }
            this.pieces = null;
            this.joints = null;
        }

        public Body getBodyPiece() {
            return this.body;
        }

        public void setActive(boolean isActive) {
            for (Piece piece : this.pieces) {
                piece.setActive(isActive);
            }
        }

        public void setCollision(boolean isCollision) {
            for (Piece piece : this.pieces) {
                piece.setCollision(isCollision);
            }
        }

        public void setRotation(float angle) {
            float oriX = this.body.getX();
            float oriY = this.body.getY();
            for (Piece piece : this.pieces) {
                rotatePiece(piece, oriX, oriY, angle);
            }
        }

        public void render(GL10 gl, float elapsedTime) {
            for (int i = 1; i < this.pieces.length; i++) {
                this.pieces[i].render(gl, elapsedTime);
            }
            this.head.render(gl, elapsedTime);
        }

        public void zeroVelocity() {
            for (Piece piece : this.pieces) {
                piece.zeroVelocity();
            }
        }

        public boolean isActive() {
            for (Piece piece : this.pieces) {
                if (!piece.getBody().IsActive()) {
                    return false;
                }
            }
            return true;
        }

        /* access modifiers changed from: private */
        public void arrangePieces(float bodyPositionX, float bodyPositionY) {
            this.body.setPosition(bodyPositionX, bodyPositionY);
            this.head.setCenterBottom(bodyPositionX, this.body.getTop() + (0.2f * this.body.getHeight()));
            this.pieces[2].setCenterTop(this.body.getLeft() + (this.body.getWidth() * 0.25f), this.head.getBottom() + (this.body.getHeight() * 0.1f));
            this.pieces[3].setCenterTop(this.pieces[2].getX(), this.pieces[2].getBottom() - (this.pieces[3].getHeight() * 0.1f));
            this.pieces[4].setCenterTop(this.body.getRight() - (this.body.getWidth() * 0.25f), this.head.getBottom() + (this.body.getHeight() * 0.1f));
            this.pieces[5].setCenterTop(this.pieces[4].getX(), this.pieces[4].getBottom() - (this.pieces[4].getHeight() * 0.1f));
            this.pieces[6].setCenterTop(this.body.getLeft() + (this.body.getWidth() * 0.25f), this.body.getBottom() - (this.body.getHeight() * 0.1f));
            this.pieces[7].setCenterTop(this.pieces[6].getX(), this.pieces[6].getBottom() - (this.pieces[6].getHeight() * 0.1f));
            this.pieces[8].setCenterTop(this.body.getRight() - (this.body.getWidth() * 0.25f), this.body.getBottom() - (this.body.getHeight() * 0.1f));
            this.pieces[9].setCenterTop(this.pieces[8].getX(), this.pieces[8].getBottom() - (this.pieces[9].getHeight() * 0.1f));
        }

        public void setImpulse(float impulseX, float impulseY) {
            for (Piece piece : this.pieces) {
                piece.applyImpulse(impulseX, impulseY);
            }
        }

        public boolean isBroken() {
            return this.isBroken;
        }

        public void breakOut(float impulse) {
            this.isBroken = true;
            for (RevoluteJoint joint : this.joints) {
                joint.removeJoint();
            }
            for (Piece piece : this.pieces) {
                if (piece != this.body) {
                    direction.set(piece.getX() - this.body.getX(), piece.getY() - this.body.getY());
                    direction.normalize();
                    direction.multiply(impulse);
                    piece.applyImpulse(direction.X, direction.Y);
                    piece.setFilterData(1, 3, 1);
                }
            }
        }

        public float getMass() {
            return this.mass;
        }

        private void rotatePiece(Piece piece, float oriX, float oriY, float rotation) {
            float[] vector = {piece.getX() - oriX, piece.getY() - oriY};
            MathUtils.RotateVector(vector, rotation);
            piece.setPosition(vector[0] + oriX, vector[1] + oriY);
            piece.setAngle(rotation);
        }

        public void movePieces(float xDelta, float yDelta) {
            for (Piece piece : this.pieces) {
                piece.move(xDelta, yDelta);
            }
        }

        public void setPositon(float x, float y) {
            movePieces(x - this.body.getX(), y - this.body.getY());
        }

        public float getBodyX() {
            return this.body.getX();
        }

        public float getBodyY() {
            return this.body.getY();
        }

        public float getBodyAngle() {
            return this.body.getAngle();
        }

        public float getBodyRadius() {
            return this.body.getRadius();
        }

        public void setFixedPosition(boolean isFixed) {
            for (Piece piece : this.pieces) {
                piece.setFixedPosition(isFixed);
            }
        }

        public void assemblyBody() {
            if (!(this.pieces == null && this.joints == null)) {
                dispose();
            }
            this.pieces = new Piece[10];
            this.joints = new RevoluteJoint[9];
            Piece[] pieceArr = this.pieces;
            Head head2 = new Head(this.config.head);
            this.head = head2;
            pieceArr[0] = head2;
            Piece[] pieceArr2 = this.pieces;
            Body body2 = new Body(this.config.body);
            this.body = body2;
            pieceArr2[1] = body2;
            for (int i = 2; i < this.pieces.length; i++) {
                this.pieces[i] = new Arm(this.config.arms[i - 2]);
            }
            arrangePieces(0.0f, 0.0f);
            for (int i2 = 0; i2 < this.joints.length; i2++) {
                this.joints[i2] = new RevoluteJoint();
            }
            setJoint(this.joints[0], this.head, this.body, -1.0471975f, 1.0471975f);
            setJoint(this.joints[1], this.body, this.pieces[2], -3.1415927f, 0.0f);
            setJoint(this.joints[2], this.pieces[2], this.pieces[3], -2.5132742f, 2.5132742f);
            setJoint(this.joints[3], this.body, this.pieces[4], 0.0f, 3.1415927f);
            setJoint(this.joints[4], this.pieces[4], this.pieces[5], -2.5132742f, 2.5132742f);
            setJoint(this.joints[5], this.body, this.pieces[6], -1.5707964f, 1.5707964f);
            setJoint(this.joints[6], this.pieces[6], this.pieces[7], -2.5132742f, 2.5132742f);
            setJoint(this.joints[7], this.body, this.pieces[8], -1.5707964f, 1.5707964f);
            setJoint(this.joints[8], this.pieces[8], this.pieces[9], -2.5132742f, 2.5132742f);
            this.mass = 0.0f;
            Piece[] pieceArr3 = this.pieces;
            int length = pieceArr3.length;
            for (int i3 = 0; i3 < length; i3++) {
                this.mass += pieceArr3[i3].getBody().GetMass();
            }
        }

        public boolean isIdInside(int id) {
            for (Piece piece : this.pieces) {
                if (piece.getId() == id) {
                    return true;
                }
            }
            return false;
        }

        private void setJoint(RevoluteJoint joint, Piece pieces1, Piece pieces2, float lowerAngle, float upperAngle) {
            joint.jointBodies(pieces2.getBody(), pieces1.getBody(), pieces2.getX(), pieces2.getTop(), lowerAngle, upperAngle);
        }
    }

    public static class Config {
        public PieceInfo[] arms = new PieceInfo[8];
        public PieceInfo body;
        public PieceInfo head;
        public Integer resourceId;

        public Config(Integer resourceId2, PieceInfo head2, PieceInfo body2, PieceInfo arm1, PieceInfo arm2, PieceInfo leg1, PieceInfo leg2) {
            this.resourceId = resourceId2;
            this.head = head2;
            this.body = body2;
            this.arms[0] = arm1;
            this.arms[1] = arm2;
            this.arms[2] = arm1;
            this.arms[3] = arm2;
            this.arms[4] = leg1;
            this.arms[5] = leg2;
            this.arms[6] = leg1;
            this.arms[7] = leg2;
        }

        public static class PieceInfo {
            private float height;
            private float physicsHeight;
            private float physicsWidth;
            private Integer resourceId;
            private float width;

            public PieceInfo(Integer resourceId2, int width2, int height2, float physicsWidth2, float physicsHeight2) {
                this.resourceId = resourceId2;
                this.width = ((float) width2) * Ragdoll.scaleRate;
                this.height = ((float) height2) * Ragdoll.scaleRate;
                this.physicsWidth = Ragdoll.scaleRate * physicsWidth2;
                this.physicsHeight = Ragdoll.scaleRate * physicsHeight2;
            }

            public Integer getResourceId() {
                return this.resourceId;
            }

            public float getWidth() {
                return this.width;
            }

            public float getHeight() {
                return this.height;
            }

            public float getPhysicsWidth() {
                return this.physicsWidth;
            }

            public float getPhysicsHeight() {
                return this.physicsHeight;
            }
        }
    }
}
