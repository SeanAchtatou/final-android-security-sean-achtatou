package com.camelgames.explode.entities;

import com.camelgames.blowuplite.R;
import com.camelgames.explode.entities.Bomb;
import com.camelgames.explode.game.GameManager;
import com.camelgames.explode.manipulation.BombManipulator;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.math.ArrayVectorUtils;
import com.camelgames.framework.math.MathUtils;
import com.camelgames.framework.math.Vector2;
import com.camelgames.framework.physics.PhysicsManager;
import com.camelgames.framework.physics.PhysicsalRenderableListenerEntity;
import com.camelgames.ndk.graphics.SpriteRects;
import com.camelgames.ndk.graphics.Texture;
import com.camelgames.ndk.graphics.TextureManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import javax.microedition.khronos.opengles.GL10;

public class Brick extends PhysicsalRenderableListenerEntity implements BombAttachable {
    private static final Texture brickTexture = TextureManager.getInstance().getTexture(R.drawable.bricks);
    private static final float[] cutLine = new float[4];
    private static final float density = 1.0f;
    private static final Vector2 direction = new Vector2();
    private static final float friction = 0.5f;
    private static final ArrayList<Float> group1 = new ArrayList<>();
    private static final ArrayList<Float> group2 = new ArrayList<>();
    private static final Vector2 intersect = new Vector2();
    private static final Comparator<Bomb> leftRightComparator = new Comparator<Bomb>() {
        public int compare(Bomb bomb1, Bomb bomb2) {
            float gap = bomb1.getX() - bomb2.getX();
            if (gap < 0.0f) {
                return -1;
            }
            if (gap == 0.0f) {
                return 0;
            }
            return 1;
        }
    };
    private static final Vector2 linearVelocity = new Vector2();
    private static final float restitution = 0.0f;
    private static final Comparator<Bomb> topBottomComparator = new Comparator<Bomb>() {
        public int compare(Bomb bomb1, Bomb bomb2) {
            float gap = bomb1.getY() - bomb2.getY();
            if (gap < 0.0f) {
                return -1;
            }
            if (gap == 0.0f) {
                return 0;
            }
            return 1;
        }
    };
    private ArrayList<Bomb> attachedBombs = new ArrayList<>();
    private int designAngle;
    private float designAngleInRadians;
    private final Vector2 endPoint = new Vector2();
    private int head1;
    private int head2;
    private boolean isSolid;
    private float length;
    private Vector2 localCenter;
    private float minGapSquare;
    private int pointCount;
    private float[] points;
    private float radiusSquare;
    private SpriteRects spriteRects;
    private final Vector2 startPoint = new Vector2();
    private float thick;

    static {
        brickTexture.setGLPara(9729.0f, 9729.0f, 33071.0f, 10497.0f);
    }

    public Brick(float[] points2, float length2, float thick2, Vector2 referencePoint, int designAngle2, int head12, int head22, boolean isSolid2) {
        initiate(points2, length2, thick2, referencePoint, designAngle2, head12, head22, isSolid2);
        BombManipulator.getInstance().addBrick(this);
    }

    /* access modifiers changed from: protected */
    public void disposeInternal() {
        BombManipulator.getInstance().removeBrick(this);
        super.disposeInternal();
    }

    public void render(GL10 gl, float elapsedTime) {
        this.spriteRects.setPosition(this.position.X, this.position.Y, this.angle);
        this.spriteRects.render(elapsedTime);
    }

    public void HandleEvent(AbstractEvent e) {
    }

    public void update(float elapsedTime) {
        SyncPhysicsPosition();
    }

    public void applyMagnet(float forceStength, float magnetRadius, float x, float y) {
        if (getMinDistance(x, y, intersect) < magnetRadius) {
            direction.set(x - intersect.X, y - intersect.Y);
            direction.normalize();
            applyForce(direction.X * forceStength, direction.Y * forceStength, intersect.X, intersect.Y);
        }
    }

    public float getDesignAngle() {
        return (float) this.designAngle;
    }

    public float getThick() {
        return this.thick;
    }

    public boolean isSolid() {
        return this.isSolid;
    }

    public Brick breakOut(Vector2 breakPoint, Vector2 impulseStrength) {
        float lengthSplit;
        group1.clear();
        group2.clear();
        cutLine[0] = breakPoint.X;
        cutLine[1] = breakPoint.Y;
        cutLine[2] = cutLine[0] + ((float) Math.cos((double) (this.designAngleInRadians + 1.5707964f)));
        cutLine[3] = cutLine[1] + ((float) Math.sin((double) (this.designAngleInRadians + 1.5707964f)));
        if (!ArrayVectorUtils.cut(this.points, this.pointCount - 1, cutLine, group1, group2)) {
            return null;
        }
        float[] pointsLeft = buildPoints(group1);
        float[] pointsSplit = buildPoints(group2);
        Vector2 centerLeft = ArrayVectorUtils.getCenter(pointsLeft, pointsLeft.length / 2);
        Vector2 centerSplit = ArrayVectorUtils.getCenter(pointsSplit, pointsSplit.length / 2);
        Vector2 vector2 = new Vector2(breakPoint);
        float impulseRotation = 0.0f;
        Vector2 vector22 = new Vector2(getLocalCenter());
        Vector2 vector23 = new Vector2(breakPoint);
        Vector2 vector24 = new Vector2(vector22);
        vector24.multiply(-1.0f);
        int splitBrickHead1 = this.head1;
        int splitBrickHead2 = -1;
        int leftBrickHead1 = -1;
        int leftBrickHead2 = this.head2;
        if (this.designAngle == 90) {
            if (centerLeft.Y < centerSplit.Y) {
                float[] temp = pointsSplit;
                pointsSplit = pointsLeft;
                pointsLeft = temp;
            }
            impulseRotation = 1.5707964f;
            lengthSplit = (friction * this.length) + vector2.Y;
        } else {
            if (centerLeft.X < centerSplit.X) {
                float[] temp2 = pointsSplit;
                pointsSplit = pointsLeft;
                pointsLeft = temp2;
            }
            if (vector2.X >= 0.0f) {
                lengthSplit = (friction * this.length) + vector2.getLength();
            } else {
                lengthSplit = (friction * this.length) - vector2.getLength();
            }
            if (this.designAngle == 45) {
                impulseRotation = 0.7853982f;
            } else if (this.designAngle == 135) {
                impulseRotation = -0.7853982f;
                vector23.set(breakPoint.X, vector24.Y);
                vector24.set(vector24.X, breakPoint.Y);
                splitBrickHead1 = -1;
                splitBrickHead2 = this.head2;
                leftBrickHead1 = this.head1;
                leftBrickHead2 = -1;
            }
        }
        float worldAngle = this.angle;
        Vector2 vector25 = new Vector2(this.position);
        Brick brickSplit = new Brick(pointsSplit, lengthSplit, this.thick, vector24, this.designAngle, splitBrickHead1, splitBrickHead2, this.isSolid);
        initiate(pointsLeft, this.length - lengthSplit, this.thick, vector23, this.designAngle, leftBrickHead1, leftBrickHead2, this.isSolid);
        boolean needReAttach = true;
        int i = 0;
        while (i < this.attachedBombs.size()) {
            Bomb bomb = this.attachedBombs.get(i);
            if (bomb.isExploding()) {
                needReAttach = false;
            } else if (!bomb.isExploded()) {
                if (needReAttach) {
                    bomb.setAttacher(brickSplit);
                    bomb.moveAttachedOffset(-brickSplit.getX(), -brickSplit.getY());
                } else {
                    bomb.moveAttachedOffset(-this.position.X, -this.position.Y);
                }
            }
            i++;
        }
        toWorld(brickSplit, vector25, worldAngle);
        toWorld(this, vector25, worldAngle);
        impulseStrength.RotateVector(impulseRotation + worldAngle);
        brickSplit.applyImpulse(impulseStrength.X, impulseStrength.Y);
        return brickSplit;
    }

    private void toWorld(Brick brick, Vector2 worldPosition, float worldAngle) {
        Vector2 brickCenter = new Vector2(brick.position);
        brickCenter.RotateVector(worldAngle);
        brickCenter.add(worldPosition);
        brick.setPosition(brickCenter);
        brick.setAngle(worldAngle);
    }

    public Vector2 getLocalCenter() {
        return this.localCenter;
    }

    public boolean isMovingDramatically() {
        if (!isActive()) {
            return false;
        }
        getLinearVelocity(linearVelocity);
        return linearVelocity.getLength() > density || Math.abs(getAngularVelocity()) > density;
    }

    public void attach(Bomb bomb) {
        if (bomb != null && !this.attachedBombs.contains(bomb)) {
            this.attachedBombs.add(bomb);
        }
    }

    public void detach(Bomb bomb) {
        this.attachedBombs.remove(bomb);
    }

    public boolean isAttached() {
        return this.attachedBombs.size() > 0;
    }

    public boolean couldAttach(Bomb bomb) {
        if (this.isSolid) {
            return false;
        }
        if (MathUtils.lengthSquare(bomb.getDragPosition(), this.position) > this.radiusSquare - this.minGapSquare) {
            return false;
        }
        for (int i = 0; i < this.attachedBombs.size(); i++) {
            Bomb attachedBomb = this.attachedBombs.get(i);
            if (attachedBomb == bomb) {
                return true;
            }
            if (MathUtils.length(attachedBomb.getX() - bomb.getX(), attachedBomb.getY() - bomb.getY()) < this.thick * 2.0f) {
                return false;
            }
        }
        return true;
    }

    public void explode() {
        if (this.attachedBombs.size() > 0) {
            if (this.designAngle == 90) {
                Collections.sort(this.attachedBombs, topBottomComparator);
            } else {
                Collections.sort(this.attachedBombs, leftRightComparator);
            }
            Iterator<Bomb> it = this.attachedBombs.iterator();
            while (it.hasNext()) {
                it.next().explode();
            }
        }
    }

    public float getTop() {
        float top = Float.MAX_VALUE;
        float sin = (float) Math.sin((double) this.angle);
        float cos = (float) Math.cos((double) this.angle);
        for (int i = 0; i < this.pointCount - 1; i++) {
            float y = (this.points[i * 2] * sin) + (this.points[(i * 2) + 1] * cos);
            if (top > y) {
                top = y;
            }
        }
        return getY() + top;
    }

    public float getBombAngle() {
        return this.designAngleInRadians - 1.5707964f;
    }

    public float getAttachPoint(Bomb bomb, Vector2 attachPoint) {
        if (!couldAttach(bomb)) {
            return Float.MAX_VALUE;
        }
        Vector2 bombPos = bomb.getDragPosition();
        float distance = MathUtils.DistanceBetweenPointSegment(this.startPoint.X, this.startPoint.Y, this.endPoint.X, this.endPoint.Y, bombPos.X, bombPos.Y, attachPoint);
        if (MathUtils.testRange(this.startPoint, this.endPoint, attachPoint)) {
            return distance;
        }
        return Float.MAX_VALUE;
    }

    public float getMinDistance(float x, float y, Vector2 intersect2) {
        return MathUtils.DistanceBetweenPointSegment(this.startPoint.X, this.startPoint.Y, this.endPoint.X, this.endPoint.Y, x, y, intersect2);
    }

    public void getBombInfo(Collection<Bomb.Info> infos) {
        for (int i = 0; i < this.attachedBombs.size(); i++) {
            Bomb.Info info = this.attachedBombs.get(i).getInfo();
            info.bindToRagdoll = false;
            infos.add(info);
        }
    }

    private static float[] buildPoints(ArrayList<Float> group) {
        float[] points2 = new float[(group.size() + 2)];
        for (int i = 0; i < group.size(); i++) {
            points2[i] = group.get(i).floatValue();
        }
        points2[points2.length - 2] = points2[0];
        points2[points2.length - 1] = points2[1];
        return points2;
    }

    private void initiate(float[] points2, float length2, float thick2, Vector2 referencePoint, int designAngle2, int head12, int head22, boolean isSolid2) {
        this.length = length2;
        this.thick = thick2;
        this.minGapSquare = thick2 * thick2 * 1.2f;
        this.designAngle = designAngle2;
        this.designAngleInRadians = ((float) (designAngle2 / 45)) * 0.7853982f;
        this.points = points2;
        this.pointCount = points2.length / 2;
        this.head1 = head12;
        this.head2 = head22;
        this.isSolid = isSolid2;
        this.spriteRects = TripleRender.getSpriteRects(length2, thick2 * 1.25f, (float) designAngle2, head12, head22, isSolid2);
        this.spriteRects.setTexId(R.drawable.bricks);
        this.localCenter = TripleRender.getLocalCenter(length2, (float) designAngle2);
        initiatePoints(referencePoint);
        initiatePhysics();
    }

    private void initiatePhysics() {
        createBody();
        PhysicsManager.instance.createPolygon(getBody(), this.points, this.pointCount - 1, density, friction, 0.0f);
    }

    private void initiatePoints(Vector2 referencePoint) {
        Vector2 center = getLocalCenter();
        setPosition(center.X + referencePoint.X, center.Y + referencePoint.Y);
        ArrayVectorUtils.transformTo(this.points, this.points.length / 2, this.position.X, this.position.Y);
        Vector2 leftPoint = new Vector2((this.length * friction) - (GameManager.getInstance().getStandardThick() * 1.3f), 0.0f);
        leftPoint.RotateVector(this.designAngleInRadians);
        this.startPoint.set(this.position);
        this.startPoint.add(leftPoint);
        this.endPoint.set(this.position);
        this.endPoint.substract(leftPoint);
        this.radiusSquare = this.length * this.length * 0.25f;
    }
}
