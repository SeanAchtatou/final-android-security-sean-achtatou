package com.camelgames.explode.entities;

import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.math.MathUtils;
import com.camelgames.framework.math.Matrix4;
import com.camelgames.framework.math.Vector2;
import com.camelgames.ndk.graphics.FloatArray;
import com.camelgames.ndk.graphics.SpriteRects;

public class TripleRender {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$entities$TripleRender$HeadType = null;
    private static final float texOffset = 0.00390625f;

    public enum HeadType {
        Normal,
        Right,
        Left,
        Sharp,
        None
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$entities$TripleRender$HeadType() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$explode$entities$TripleRender$HeadType;
        if (iArr == null) {
            iArr = new int[HeadType.values().length];
            try {
                iArr[HeadType.Left.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[HeadType.None.ordinal()] = 5;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[HeadType.Normal.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[HeadType.Right.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[HeadType.Sharp.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            $SWITCH_TABLE$com$camelgames$explode$entities$TripleRender$HeadType = iArr;
        }
        return iArr;
    }

    public static Vector2 getLocalCenter(float length, float designAngle) {
        Vector2 localCenter = new Vector2();
        if (designAngle == 0.0f) {
            localCenter.set(0.5f * length, 0.0f);
        } else if (designAngle == 90.0f) {
            localCenter.set(0.0f, 0.5f * length);
        } else {
            float half = (0.5f * length) / ((float) Math.sqrt(2.0d));
            localCenter.set(half, half);
        }
        return localCenter;
    }

    public static SpriteRects getSpriteRects(float length, float thick, float designAngle, int head1, int head2, boolean isSolid) {
        HeadType headType1 = convert(head1, designAngle);
        HeadType headType2 = getOpposite(convert(head2, designAngle));
        if (designAngle == 135.0f) {
            headType1 = getOpposite(headType1);
            headType2 = getOpposite(headType2);
        }
        return initiateBuffers(length, thick, headType1, headType2, MathUtils.degToRad(designAngle - 90.0f), isSolid);
    }

    public static HeadType convert(int head, float designAngle) {
        switch (head) {
            case 0:
                return HeadType.Normal;
            case 1:
                return HeadType.Right;
            case 2:
                return HeadType.Left;
            case 3:
                return HeadType.Sharp;
            default:
                return HeadType.None;
        }
    }

    public static HeadType getOpposite(HeadType currentType) {
        switch ($SWITCH_TABLE$com$camelgames$explode$entities$TripleRender$HeadType()[currentType.ordinal()]) {
            case 2:
                return HeadType.Left;
            case 3:
                return HeadType.Right;
            default:
                return currentType;
        }
    }

    /* JADX INFO: Multiple debug info for r23v1 float: [D('length' float), D('lengthLeft' float)] */
    /* JADX INFO: Multiple debug info for r23v2 float: [D('length' float), D('lengthLeft' float)] */
    /* JADX INFO: Multiple debug info for r24v3 float[]: [D('texCoords' float[]), D('thick' float)] */
    /* JADX INFO: Multiple debug info for r25v9 'texCoords'  float[]: [D('texCoords' float[]), D('head1' com.camelgames.explode.entities.TripleRender$HeadType)] */
    /* JADX INFO: Multiple debug info for r23v25 float: [D('length' float), D('lengthLeft' float)] */
    private static SpriteRects initiateBuffers(float length, float thick, HeadType head1, HeadType head2, float rotateAngleRadians, boolean isSolid) {
        float lengthLeft;
        float[] texCoords;
        int itemCount;
        float offsetY = 0.0f;
        float lengthLeft2 = length;
        boolean isHead1None = head1.equals(HeadType.None);
        boolean isHead2None = head2.equals(HeadType.None);
        if (isHead1None && !isHead2None) {
            offsetY = (-thick) * 0.25f;
            lengthLeft = length - (0.5f * thick);
        } else if (!isHead1None && isHead2None) {
            offsetY = thick * 0.25f;
            lengthLeft = length - (0.5f * thick);
        } else if (isHead1None || isHead2None) {
            lengthLeft = lengthLeft2;
        } else {
            lengthLeft = length - thick;
        }
        int itemCount2 = 3;
        if (head1.equals(HeadType.None)) {
            itemCount2 = 3 - 1;
        }
        if (head2.equals(HeadType.None)) {
            itemCount2--;
        }
        float[] texCoordsWhole = new float[(itemCount2 * 8)];
        float[] verticesWhole = new float[(itemCount2 * 8)];
        getVertices(thick, lengthLeft + 1.0f, 0.0f, offsetY, 0.0f, rotateAngleRadians, verticesWhole, 0 * 8);
        float[] texCoords2 = getTexCoords(HeadType.None, isSolid);
        int round = Math.round((lengthLeft / thick) / 4.0f);
        if (round > 1) {
            texCoords2[3] = (float) round;
            texCoords2[7] = (float) round;
        }
        System.arraycopy(texCoords2, 0, texCoordsWhole, 0, 8);
        int itemCount3 = 0 + 1;
        if (!head1.equals(HeadType.None)) {
            getVertices(thick, thick, 0.0f, ((-(lengthLeft + thick)) * 0.5f) + offsetY, 0.0f, rotateAngleRadians, verticesWhole, itemCount3 * 8);
            texCoords = getTexCoords(head1, isSolid);
            System.arraycopy(texCoords, 0, texCoordsWhole, itemCount3 * 8, 8);
            itemCount3++;
        } else {
            texCoords = texCoords2;
        }
        if (!head2.equals(HeadType.None)) {
            getVertices(-thick, thick, 0.0f, ((lengthLeft + thick) * 0.5f) + offsetY, 3.1415927f, rotateAngleRadians, verticesWhole, itemCount3 * 8);
            System.arraycopy(getTexCoords(head2, isSolid), 0, texCoordsWhole, itemCount3 * 8, 8);
            itemCount = itemCount3 + 1;
        } else {
            itemCount = itemCount3;
        }
        FloatArray floatArray = new FloatArray(verticesWhole.length);
        for (int i = 0; i < verticesWhole.length; i++) {
            floatArray.set(verticesWhole[i], i);
        }
        FloatArray floatArray2 = new FloatArray(texCoordsWhole.length);
        for (int i2 = 0; i2 < texCoordsWhole.length; i2++) {
            floatArray2.set(texCoordsWhole[i2], i2);
        }
        return new SpriteRects(floatArray, floatArray2, itemCount);
    }

    private static void getVertices(float width, float height, float offsetX, float offsetY, float offsetAngleRadians, float rotateAngleRadians, float[] vertices, int startIndex) {
        System.arraycopy(GraphicsManager.defaultVertices, 0, vertices, startIndex, 8);
        Matrix4 finalMat = Matrix4.createIndentity();
        Matrix4 temp = new Matrix4();
        Matrix4.CreateScale(width, height, 1.0f, finalMat);
        Matrix4.Multiply(finalMat, Matrix4.CreateRotationZ(offsetAngleRadians, temp), finalMat);
        Matrix4.Multiply(finalMat, Matrix4.CreateTranslation(offsetX, offsetY, 0.0f, temp), finalMat);
        Matrix4.Multiply(finalMat, Matrix4.CreateRotationZ(rotateAngleRadians, temp), finalMat);
        Vector2.Transform(vertices, finalMat, startIndex, 4);
    }

    private static float[] getTexCoords(HeadType headType, boolean isSolid) {
        switch ($SWITCH_TABLE$com$camelgames$explode$entities$TripleRender$HeadType()[headType.ordinal()]) {
            case 1:
                if (isSolid) {
                    return new float[]{0.75390625f, 0.50390625f, 0.75390625f, 0.75390625f, 1.0039062f, 0.50390625f, 1.0039062f, 0.75390625f};
                }
                return new float[]{0.50390625f, 0.50390625f, 0.50390625f, 0.75390625f, 0.75390625f, 0.50390625f, 0.75390625f, 0.75390625f};
            case 2:
                if (isSolid) {
                    return new float[]{0.75390625f, 0.25390625f, 0.75390625f, 0.50390625f, 1.0039062f, 0.25390625f, 1.0039062f, 0.50390625f};
                }
                return new float[]{0.50390625f, 0.25390625f, 0.50390625f, 0.50390625f, 0.75390625f, 0.25390625f, 0.75390625f, 0.50390625f};
            case 3:
                if (isSolid) {
                    return new float[]{0.75390625f, texOffset, 0.75390625f, 0.25390625f, 1.0039062f, texOffset, 1.0039062f, 0.25390625f};
                }
                return new float[]{0.50390625f, texOffset, 0.50390625f, 0.25390625f, 0.75390625f, texOffset, 0.75390625f, 0.25390625f};
            case 4:
                if (isSolid) {
                    return new float[]{0.75390625f, 0.75390625f, 0.75390625f, 1.0039062f, 1.0039062f, 0.75390625f, 1.0039062f, 1.0039062f};
                }
                return new float[]{0.50390625f, 0.75390625f, 0.50390625f, 1.0039062f, 0.75390625f, 0.75390625f, 0.75390625f, 1.0039062f};
            default:
                if (isSolid) {
                    return new float[]{0.25390625f, texOffset, 0.25390625f, 1.0039062f, 0.50390625f, texOffset, 0.50390625f, 1.0039062f};
                }
                return new float[]{texOffset, texOffset, texOffset, 1.0039062f, 0.25390625f, texOffset, 0.25390625f, 1.0039062f};
        }
    }
}
