package com.camelgames.explode.entities;

import com.camelgames.blowuplite.R;
import com.camelgames.explode.entities.Bomb;
import com.camelgames.explode.entities.ragdoll.Ragdoll;
import com.camelgames.explode.game.GameManager;
import com.camelgames.explode.manipulation.BombManipulator;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.ndk.graphics.SequentialSprite;
import com.camelgames.ndk.graphics.Sprite2D;
import java.util.ArrayList;

public class MagnetBomb extends Bomb {
    private static final float magnetRadius = ((float) GraphicsManager.screenHeight(0.2f));

    public Bomb.Type getType() {
        return Bomb.Type.Magnent;
    }

    public void update(float elapsedTime) {
        if (isExploded()) {
            applyForce();
        }
        super.update(elapsedTime);
    }

    /* access modifiers changed from: protected */
    public Sprite2D createBombTexture() {
        Sprite2D texture = new Sprite2D(basicWidth, basicWidth);
        texture.setTexId(R.array.altas1_bomb_magnet);
        return texture;
    }

    /* access modifiers changed from: protected */
    public SequentialSprite createExplodeTexture() {
        SequentialSprite texture = new SequentialSprite(basicWidth * 2.5f, basicWidth * 2.5f);
        texture.setTexId(R.drawable.magnent_explode);
        texture.setChangeTime(0.02f);
        texture.setLoop(false);
        texture.setStop(true);
        return texture;
    }

    /* access modifiers changed from: protected */
    public void applyImpulseToRagdoll(Ragdoll ragdoll) {
        ragdoll.breakOut(0.1f);
    }

    /* access modifiers changed from: protected */
    public void applyImpulseToBrick(Brick attachedBrick) {
        attachedBrick.delete();
    }

    private void applyForce() {
        float forceStength = GameManager.getInstance().getBombPower() * 10.0f;
        ArrayList<BombAttachable> attachers = BombManipulator.getInstance().getAttachers();
        for (int i = 0; i < attachers.size(); i++) {
            attachers.get(i).applyMagnet(forceStength, magnetRadius, this.position.X, this.position.Y);
        }
    }
}
