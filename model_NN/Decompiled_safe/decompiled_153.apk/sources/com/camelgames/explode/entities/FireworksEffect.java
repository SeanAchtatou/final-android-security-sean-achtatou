package com.camelgames.explode.entities;

import com.camelgames.framework.math.MathUtils;
import com.camelgames.ndk.graphics.OESSprite;
import javax.microedition.khronos.opengles.GL10;

public class FireworksEffect {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$entities$FireworksEffect$Status;
    private static float[][] colors = {new float[]{0.53333336f, 0.90588236f, 0.9843137f}, new float[]{0.9843137f, 0.9411765f, 0.7294118f}, new float[]{1.0f, 0.9764706f, 0.7372549f}, new float[]{0.5921569f, 1.0f, 0.34509805f}, new float[]{1.0f, 0.22745098f, 0.21176471f}, new float[]{1.0f, 0.70980394f, 0.89411765f}, new float[]{0.6313726f, 1.0f, 0.26666668f}, new float[]{0.972549f, 0.5019608f, 1.0f}, new float[]{0.6784314f, 0.99215686f, 0.99607843f}, new float[]{1.0f, 0.8745098f, 0.7372549f}, new float[]{0.7372549f, 1.0f, 0.8039216f}, new float[]{0.9647059f, 1.0f, 0.7372549f}};
    private final OESSprite particleTexture;
    private final Particle[] particles;
    private final Particle shell;
    private float speed;
    private Status status = Status.Rising;

    private enum Status {
        Rising,
        Explode,
        Finished
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$camelgames$explode$entities$FireworksEffect$Status() {
        int[] iArr = $SWITCH_TABLE$com$camelgames$explode$entities$FireworksEffect$Status;
        if (iArr == null) {
            iArr = new int[Status.values().length];
            try {
                iArr[Status.Explode.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Status.Finished.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Status.Rising.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$camelgames$explode$entities$FireworksEffect$Status = iArr;
        }
        return iArr;
    }

    public FireworksEffect(OESSprite particleTexture2, int particleCount) {
        this.particleTexture = particleTexture2;
        this.shell = new Particle();
        this.particles = new Particle[particleCount];
        for (int loop = 0; loop < particleCount; loop++) {
            this.particles[loop] = new Particle();
        }
    }

    public void startRising(float x, float y, float speedX, float speedY, float size) {
        this.status = Status.Rising;
        this.speed = speedY;
        int colorIndex = MathUtils.randomInt(colors.length);
        initiateParticle(this.shell, x, y, speedX, speedY, MathUtils.random(0.5f, 0.6f), colorIndex, size);
        for (Particle particle : this.particles) {
            particle.r = colors[colorIndex][0];
            particle.g = colors[colorIndex][1];
            particle.b = colors[colorIndex][2];
            particle.size = size;
            initiateForRising(x, y, speedX, speedY, particle);
        }
    }

    public void prepareExplode(float x, float y, float speed2, float size) {
        this.status = Status.Explode;
        int colorIndex = MathUtils.randomInt(colors.length);
        float angleDelta = 6.2831855f / ((float) this.particles.length);
        float angle = 0.0f;
        for (Particle particle : this.particles) {
            initiateParticle(particle, x, y, speed2 * ((float) Math.cos((double) angle)), speed2 * ((float) Math.sin((double) angle)), angle, colorIndex, size);
            angle += angleDelta;
        }
    }

    private void initiateParticle(Particle particle, float x, float y, float speedX, float speedY, float angle, int colorIndex, float size) {
        initiateParticle(particle, x, y, speedX, speedY, MathUtils.random(0.3f, 0.7f));
        particle.r = colors[colorIndex][0];
        particle.g = colors[colorIndex][1];
        particle.b = colors[colorIndex][2];
        particle.size = size;
    }

    private void initiateParticle(Particle particle, float originX, float originY, float speedX, float speedY, float fade) {
        particle.life = 1.0f;
        particle.active = true;
        particle.fade = fade;
        particle.x = originX;
        particle.y = originY;
        particle.xSpeed = speedX;
        particle.ySpeed = speedY;
        particle.xGravity = 0.0f;
        particle.yGravity = 0.0f;
    }

    private void initiateForRising(float x, float y, float speedX, float speedY, Particle particle) {
        float fade = MathUtils.random(0.1f, 0.9f);
        initiateParticle(particle, x, y, speedX * MathUtils.random(1.0f - (fade / 2.0f), 1.0f), speedY * MathUtils.random(1.0f - (fade / 2.0f), 1.0f), fade);
        particle.life = MathUtils.random(0.5f, 1.0f);
    }

    private void explode(float speed2, float size) {
        this.status = Status.Explode;
        float angleDelta = 6.2831855f / ((float) this.particles.length);
        float angle = 0.0f;
        for (Particle particle : this.particles) {
            initiateParticle(particle, particle.x, particle.y, speed2 * ((float) Math.cos((double) angle)), speed2 * ((float) Math.sin((double) angle)), MathUtils.random(0.2f, 0.8f));
            angle += angleDelta;
        }
    }

    public boolean isFinished() {
        for (Particle partical : this.particles) {
            if (partical.active) {
                return false;
            }
        }
        return true;
    }

    public void update(float elapsedTime) {
        switch ($SWITCH_TABLE$com$camelgames$explode$entities$FireworksEffect$Status()[this.status.ordinal()]) {
            case 1:
                this.shell.update(elapsedTime);
                for (Particle partical : this.particles) {
                    partical.update(elapsedTime);
                    if (!partical.active) {
                        initiateForRising(this.shell.x, this.shell.y, this.shell.xSpeed, this.shell.ySpeed, partical);
                    }
                }
                if (!this.shell.active) {
                    explode(this.speed * 0.5f, this.shell.size);
                    return;
                }
                return;
            case 2:
                for (Particle partical2 : this.particles) {
                    partical2.update(elapsedTime);
                }
                return;
            default:
                return;
        }
    }

    public void render(GL10 gl, float elapsedTime) {
        gl.glBlendFunc(770, 1);
        for (Particle partical : this.particles) {
            partical.render(gl, this.particleTexture);
        }
    }
}
