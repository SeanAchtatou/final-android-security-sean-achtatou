package com.camelgames.explode.sound;

import android.content.Context;
import com.camelgames.blowuplite.R;
import com.camelgames.framework.events.AbstractEvent;
import com.camelgames.framework.events.EventType;
import com.camelgames.framework.sounds.SoundManagerBase;

public class SoundManager extends SoundManagerBase {
    private static SoundManager instance = new SoundManager();
    private SoundManagerBase.Sound attachRagdoll = new SoundManagerBase.Sound();
    private SoundManagerBase.Sound attachStick = new SoundManagerBase.Sound();
    private SoundManagerBase.Sound bomb = new SoundManagerBase.Sound();
    private SoundManagerBase.Sound bombLarge = new SoundManagerBase.Sound();
    private SoundManagerBase.Sound button = new SoundManagerBase.Sound();
    private SoundManagerBase.Sound captureBomb = new SoundManagerBase.Sound();
    private SoundManagerBase.Sound lost = new SoundManagerBase.Sound();
    private SoundManagerBase.Sound pressSwitch = new SoundManagerBase.Sound();
    private SoundManagerBase.Sound ragdollDie = new SoundManagerBase.Sound();
    private SoundManagerBase.Sound tick = new SoundManagerBase.Sound();
    private SoundManagerBase.Sound touchStar = new SoundManagerBase.Sound();
    private SoundManagerBase.Sound win = new SoundManagerBase.Sound();

    public static SoundManager getInstance() {
        return instance;
    }

    private SoundManager() {
    }

    /* access modifiers changed from: protected */
    public void initiateInternal(Context context) {
        loadSoundFromContext(context);
        registSounds();
    }

    private void registSounds() {
        addSoundEventHandler(EventType.Button, this.button);
        addSoundEventHandler(EventType.Bomb, this.bomb);
        addSoundEventHandler(EventType.BombLarge, this.bombLarge);
        addSoundEventHandler(EventType.AttachRagdoll, this.attachRagdoll);
        addSoundEventHandler(EventType.RagdollDied, this.ragdollDie);
        addSoundEventHandler(EventType.AttachStick, this.attachStick);
        addSoundEventHandler(EventType.Captured, this.captureBomb);
        addSoundEventHandler(EventType.PressSwitch, this.pressSwitch);
        addSoundEventHandler(EventType.Tick, this.tick);
        addSoundEventHandler(EventType.LevelFinished, this.win);
        addSoundEventHandler(EventType.LevelFailed, this.lost);
        addSoundEventHandler(EventType.Catched, this.touchStar);
    }

    /* access modifiers changed from: protected */
    public void internalHandleEvent(AbstractEvent e) {
    }

    private void loadSoundFromContext(Context context) {
        this.button.initiate(this.soundPool.load(context, R.raw.button, 1), 0);
        this.bomb.initiate(this.soundPool.load(context, R.raw.bomb, 1), 0);
        this.attachRagdoll.initiate(this.soundPool.load(context, R.raw.attach_ragdoll, 1), 0);
        this.attachStick.initiate(this.soundPool.load(context, R.raw.attach_stick, 1), 500);
        this.captureBomb.initiate(this.soundPool.load(context, R.raw.capture_bomb, 1), 0);
        this.bombLarge.initiate(this.soundPool.load(context, R.raw.large_bomb, 1), 0);
        this.pressSwitch.initiate(this.soundPool.load(context, R.raw.press_switch, 1), 0);
        this.tick.initiate(this.soundPool.load(context, R.raw.tick, 1), 200);
        this.win.initiate(this.soundPool.load(context, R.raw.win, 1), 1000);
        this.lost.initiate(this.soundPool.load(context, R.raw.lost, 1), 1000);
        this.touchStar.initiate(this.soundPool.load(context, R.raw.touchstar, 1), 1000);
        this.ragdollDie.initiate(this.soundPool.load(context, R.raw.panda_die, 1), 1000);
    }
}
