package com.camelgames.explode.score;

import com.camelgames.explode.server.serializable.ScoreStorage;
import com.camelgames.framework.utilities.SDUtility;

public class FlightControlSDUtility {
    public static String Game_Folder_Name = "/sdcard/.camelgames/.explode/";
    public static final String Score_Name = (String.valueOf(Game_Folder_Name) + "score.dat");

    public static void storeScores(ScoreStorage scoreStorage) {
        SDUtility.saveObject(Score_Name, scoreStorage, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.framework.utilities.SDUtility.loadObject(java.lang.String, boolean):T
     arg types: [java.lang.String, int]
     candidates:
      com.camelgames.framework.utilities.SDUtility.loadObject(java.io.File, boolean):T
      com.camelgames.framework.utilities.SDUtility.loadObject(java.lang.String, boolean):T */
    public static ScoreStorage loadScore() {
        return (ScoreStorage) SDUtility.loadObject(Score_Name, true);
    }
}
