package com.camelgames.explode.score;

import android.os.Bundle;
import android.os.Handler;
import com.camelgames.explode.server.serializable.ScoreBoardInfo;
import com.camelgames.explode.server.serializable.Scores;
import com.camelgames.framework.network.AsyncHttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

public final class FlightAsyncHttpRequest extends AsyncHttpRequest {
    public static final String SERVICE_URL = "http://blowup-camelgames.appspot.com/camelscore/";
    public static final String SHOW_BOARD_URL = "http://blowup-camelgames.appspot.com/camelscore/getscore?";
    public static final String SUBMIT_SCORE_URL = "http://blowup-camelgames.appspot.com/camelscore/setscore?";
    private static ToScoreBoardInfoProcessor toScoreBoardInfoProcessor = new ToScoreBoardInfoProcessor(null);

    public FlightAsyncHttpRequest(Handler handler) {
        super(handler);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.camelgames.framework.network.AsyncHttpRequest.start(java.lang.String, com.camelgames.framework.network.AsyncHttpRequest$InputStreamProcessor):void
     arg types: [java.lang.String, com.camelgames.explode.score.FlightAsyncHttpRequest$ToScoreBoardInfoProcessor]
     candidates:
      com.camelgames.framework.network.AsyncHttpRequest.start(java.lang.String, java.lang.Object):void
      com.camelgames.framework.network.AsyncHttpRequest.start(java.lang.String, com.camelgames.framework.network.AsyncHttpRequest$InputStreamProcessor):void */
    public void showScoreBoard(String userId, int difficulty) {
        start("http://blowup-camelgames.appspot.com/camelscore/getscore?userid=" + userId + "&difficulty=" + difficulty, (AsyncHttpRequest.InputStreamProcessor) toScoreBoardInfoProcessor);
    }

    public void sendScore(Scores data) {
        start(SUBMIT_SCORE_URL, data);
    }

    private static class ToScoreBoardInfoProcessor implements AsyncHttpRequest.InputStreamProcessor {
        private ToScoreBoardInfoProcessor() {
        }

        /* synthetic */ ToScoreBoardInfoProcessor(ToScoreBoardInfoProcessor toScoreBoardInfoProcessor) {
            this();
        }

        public boolean process(InputStream inputStream, Bundle b) {
            try {
                b.putSerializable(AsyncHttpRequest.KEY_RESPONSE, (ScoreBoardInfo) new ObjectInputStream(inputStream).readObject());
                return true;
            } catch (ClassNotFoundException e) {
                return false;
            } catch (IOException e2) {
                return false;
            }
        }
    }
}
