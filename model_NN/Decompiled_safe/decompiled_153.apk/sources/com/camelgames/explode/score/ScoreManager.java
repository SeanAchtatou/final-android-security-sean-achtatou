package com.camelgames.explode.score;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.camelgames.explode.server.serializable.ScoreStorage;
import com.camelgames.explode.server.serializable.Scores;
import com.camelgames.framework.network.AsyncHttpRequest;
import com.camelgames.framework.utilities.SDUtility;
import java.util.UUID;

public class ScoreManager {
    private static final String SCORE_FILE = "ScoreFile";
    private static ScoreManager instance = new ScoreManager();
    private Handler handler;
    private boolean isLoaded;
    private boolean isMadeNewRecord;
    private int previousRecored;
    /* access modifiers changed from: private */
    public ScoreStorage scoreStorage;

    public static ScoreManager getInstance() {
        return instance;
    }

    private ScoreManager() {
    }

    public void load(Context context) {
        if (!this.isLoaded) {
            this.scoreStorage = FlightControlSDUtility.loadScore();
            if (this.scoreStorage == null) {
                this.scoreStorage = loadScoreFromContext(context);
            }
            if (this.scoreStorage == null) {
                this.scoreStorage = new ScoreStorage();
            }
            this.isLoaded = true;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v13, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.camelgames.explode.server.serializable.ScoreStorage} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0025 A[SYNTHETIC, Splitter:B:14:0x0025] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002a A[Catch:{ IOException -> 0x002e }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0033 A[SYNTHETIC, Splitter:B:21:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0038 A[Catch:{ IOException -> 0x003c }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0041 A[SYNTHETIC, Splitter:B:28:0x0041] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0046 A[Catch:{ IOException -> 0x004a }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x004f A[SYNTHETIC, Splitter:B:35:0x004f] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0054 A[Catch:{ IOException -> 0x005b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.camelgames.explode.server.serializable.ScoreStorage loadScoreFromContext(android.content.Context r7) {
        /*
            r4 = 0
            r1 = 0
            r2 = 0
            java.lang.String r5 = "ScoreFile"
            java.io.FileInputStream r1 = r7.openFileInput(r5)     // Catch:{ ClassCastException -> 0x0022, ClassNotFoundException -> 0x0030, IOException -> 0x003e, all -> 0x004c }
            java.io.ObjectInputStream r3 = new java.io.ObjectInputStream     // Catch:{ ClassCastException -> 0x0022, ClassNotFoundException -> 0x0030, IOException -> 0x003e, all -> 0x004c }
            r3.<init>(r1)     // Catch:{ ClassCastException -> 0x0022, ClassNotFoundException -> 0x0030, IOException -> 0x003e, all -> 0x004c }
            java.lang.Object r5 = r3.readObject()     // Catch:{ ClassCastException -> 0x0066, ClassNotFoundException -> 0x0063, IOException -> 0x0060, all -> 0x005d }
            r0 = r5
            com.camelgames.explode.server.serializable.ScoreStorage r0 = (com.camelgames.explode.server.serializable.ScoreStorage) r0     // Catch:{ ClassCastException -> 0x0066, ClassNotFoundException -> 0x0063, IOException -> 0x0060, all -> 0x005d }
            r4 = r0
            if (r3 == 0) goto L_0x001b
            r3.close()     // Catch:{ IOException -> 0x0058 }
        L_0x001b:
            if (r1 == 0) goto L_0x0069
            r1.close()     // Catch:{ IOException -> 0x0058 }
            r2 = r3
        L_0x0021:
            return r4
        L_0x0022:
            r5 = move-exception
        L_0x0023:
            if (r2 == 0) goto L_0x0028
            r2.close()     // Catch:{ IOException -> 0x002e }
        L_0x0028:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x002e }
            goto L_0x0021
        L_0x002e:
            r5 = move-exception
            goto L_0x0021
        L_0x0030:
            r5 = move-exception
        L_0x0031:
            if (r2 == 0) goto L_0x0036
            r2.close()     // Catch:{ IOException -> 0x003c }
        L_0x0036:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x003c }
            goto L_0x0021
        L_0x003c:
            r5 = move-exception
            goto L_0x0021
        L_0x003e:
            r5 = move-exception
        L_0x003f:
            if (r2 == 0) goto L_0x0044
            r2.close()     // Catch:{ IOException -> 0x004a }
        L_0x0044:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x004a }
            goto L_0x0021
        L_0x004a:
            r5 = move-exception
            goto L_0x0021
        L_0x004c:
            r5 = move-exception
        L_0x004d:
            if (r2 == 0) goto L_0x0052
            r2.close()     // Catch:{ IOException -> 0x005b }
        L_0x0052:
            if (r1 == 0) goto L_0x0057
            r1.close()     // Catch:{ IOException -> 0x005b }
        L_0x0057:
            throw r5
        L_0x0058:
            r5 = move-exception
            r2 = r3
            goto L_0x0021
        L_0x005b:
            r6 = move-exception
            goto L_0x0057
        L_0x005d:
            r5 = move-exception
            r2 = r3
            goto L_0x004d
        L_0x0060:
            r5 = move-exception
            r2 = r3
            goto L_0x003f
        L_0x0063:
            r5 = move-exception
            r2 = r3
            goto L_0x0031
        L_0x0066:
            r5 = move-exception
            r2 = r3
            goto L_0x0023
        L_0x0069:
            r2 = r3
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.camelgames.explode.score.ScoreManager.loadScoreFromContext(android.content.Context):com.camelgames.explode.server.serializable.ScoreStorage");
    }

    public void updateScore(int levelIndex, int record, int difficulty) {
        this.previousRecored = record;
        if (this.scoreStorage.updateScore(levelIndex, difficulty, record)) {
            this.isMadeNewRecord = true;
            storeScore();
            return;
        }
        this.isMadeNewRecord = false;
    }

    public boolean isMadeNewRecord() {
        return this.isMadeNewRecord;
    }

    public int getPreviousRecord() {
        return this.previousRecored;
    }

    public void setUserName(String userName) {
        if (!"".equals(userName)) {
            this.scoreStorage.setUserName(userName);
            storeScore();
        }
    }

    public String getUserName() {
        return this.scoreStorage.getUserName();
    }

    public String getUserId() {
        if (this.scoreStorage.getUserId() == null) {
            this.scoreStorage.setUserId(UUID.randomUUID().toString());
            storeScore();
        }
        return this.scoreStorage.getUserId();
    }

    public int getScore(int levelIndex, int difficulty) {
        return this.scoreStorage.getScore(levelIndex, difficulty);
    }

    public int getTotalScore(int difficulty) {
        return this.scoreStorage.getTotalScore(difficulty);
    }

    public int getFinishedCount(int difficulty) {
        return this.scoreStorage.getFinishedCount(difficulty);
    }

    public FlightAsyncHttpRequest sendScore(final Handler handler2) {
        this.handler = handler2;
        getUserId();
        if (this.scoreStorage.getUserName() == null) {
            invokeHandler();
            return null;
        } else if (this.scoreStorage.isModified()) {
            Scores scores = new Scores();
            scores.userId = this.scoreStorage.getUserId();
            scores.userName = this.scoreStorage.getUserName();
            scores.comments = this.scoreStorage.getComments();
            scores.codes = this.scoreStorage.getCodes();
            FlightAsyncHttpRequest sendScore = new FlightAsyncHttpRequest(new Handler() {
                public void handleMessage(Message msg) {
                    String response = msg.getData().getString(AsyncHttpRequest.KEY_RESPONSE);
                    if (!AsyncHttpRequest.NO_CONNECTION.equals(response) && !AsyncHttpRequest.OUT_OF_SERVICE.equals(response)) {
                        ScoreManager.this.scoreStorage.setModified(false);
                        ScoreManager.this.storeScore();
                    } else if (handler2 != null) {
                        Message msg1 = handler2.obtainMessage();
                        msg1.setData(msg1.getData());
                        handler2.sendMessage(msg1);
                        return;
                    }
                    ScoreManager.this.invokeHandler();
                }
            });
            sendScore.sendScore(scores);
            return sendScore;
        } else {
            invokeHandler();
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void invokeHandler() {
        if (this.handler != null) {
            this.handler.sendEmptyMessage(0);
        }
    }

    /* access modifiers changed from: private */
    public void storeScore() {
        if (SDUtility.isSDPresent()) {
            FlightControlSDUtility.storeScores(this.scoreStorage);
        } else {
            storeScoreInContext();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0026 A[SYNTHETIC, Splitter:B:13:0x0026] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x002b A[Catch:{ IOException -> 0x002f }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0034 A[SYNTHETIC, Splitter:B:20:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0039 A[Catch:{ IOException -> 0x0040 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void storeScoreInContext() {
        /*
            r6 = this;
            r0 = 0
            r1 = 0
            android.app.Activity r3 = com.camelgames.framework.ui.UIUtility.getMainAcitvity()     // Catch:{ IOException -> 0x0023, all -> 0x0031 }
            java.lang.String r4 = "ScoreFile"
            r5 = 0
            java.io.FileOutputStream r0 = r3.openFileOutput(r4, r5)     // Catch:{ IOException -> 0x0023, all -> 0x0031 }
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0023, all -> 0x0031 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0023, all -> 0x0031 }
            com.camelgames.explode.server.serializable.ScoreStorage r3 = r6.scoreStorage     // Catch:{ IOException -> 0x0045, all -> 0x0042 }
            r2.writeObject(r3)     // Catch:{ IOException -> 0x0045, all -> 0x0042 }
            if (r2 == 0) goto L_0x001c
            r2.close()     // Catch:{ IOException -> 0x003d }
        L_0x001c:
            if (r0 == 0) goto L_0x0048
            r0.close()     // Catch:{ IOException -> 0x003d }
            r1 = r2
        L_0x0022:
            return
        L_0x0023:
            r3 = move-exception
        L_0x0024:
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ IOException -> 0x002f }
        L_0x0029:
            if (r0 == 0) goto L_0x0022
            r0.close()     // Catch:{ IOException -> 0x002f }
            goto L_0x0022
        L_0x002f:
            r3 = move-exception
            goto L_0x0022
        L_0x0031:
            r3 = move-exception
        L_0x0032:
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ IOException -> 0x0040 }
        L_0x0037:
            if (r0 == 0) goto L_0x003c
            r0.close()     // Catch:{ IOException -> 0x0040 }
        L_0x003c:
            throw r3
        L_0x003d:
            r3 = move-exception
            r1 = r2
            goto L_0x0022
        L_0x0040:
            r4 = move-exception
            goto L_0x003c
        L_0x0042:
            r3 = move-exception
            r1 = r2
            goto L_0x0032
        L_0x0045:
            r3 = move-exception
            r1 = r2
            goto L_0x0024
        L_0x0048:
            r1 = r2
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.camelgames.explode.score.ScoreManager.storeScoreInContext():void");
    }
}
