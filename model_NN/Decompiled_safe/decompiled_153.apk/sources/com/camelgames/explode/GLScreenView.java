package com.camelgames.explode;

import android.content.Context;
import android.util.AttributeSet;
import com.camelgames.blowuplite.MainActivity;
import com.camelgames.blowuplite.R;
import com.camelgames.explode.game.GameManager;
import com.camelgames.explode.levels.LevelManager;
import com.camelgames.explode.score.ScoreManager;
import com.camelgames.explode.sound.SoundManager;
import com.camelgames.framework.GLScreenViewBase;
import com.camelgames.framework.Manipulator;
import com.camelgames.framework.graphics.GraphicsManager;
import com.camelgames.framework.graphics.font.TextBuilder;
import com.camelgames.framework.physics.PhysicsManager;
import com.camelgames.framework.touch.TouchManager;
import com.camelgames.ndk.JNILibrary;

public class GLScreenView extends GLScreenViewBase {
    public static Manipulator currentManipulator;
    public static int scissorCenterX;
    public static int scissorCenterY;
    public static int scissorHeight;
    public static int scissorWidth;
    public static TextBuilder textBuilder;

    static {
        System.loadLibrary("physics");
        JNILibrary.appStart();
    }

    public GLScreenView(Context context) {
        super(context);
        configBase();
    }

    public GLScreenView(Context context, AttributeSet attrs) {
        super(context, attrs);
        configBase();
    }

    public GLScreenView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        configBase();
    }

    private void configBase() {
        setFixedTimeFrame(0.033333335f);
    }

    public static void changeManipulator(Manipulator manipulator) {
        if (currentManipulator != null) {
            currentManipulator.setVisible(false);
            currentManipulator.setStoped(true);
            currentManipulator.finish();
            TouchManager.getInstace().remove(currentManipulator);
        }
        currentManipulator = manipulator;
        if (currentManipulator != null) {
            currentManipulator.setVisible(true);
            currentManipulator.setStoped(false);
            TouchManager.getInstace().add(currentManipulator);
        }
    }

    /* access modifiers changed from: protected */
    public void initiateInternal(int width, int height) {
        PhysicsManager.instance.initiate(((float) width) * -1.0f, ((float) height) * -1.0f, ((float) width) * 2.0f, ((float) height) * 2.0f, 10.0f);
        PhysicsManager.instance.setCollisionImpulseThreshold(0.0f);
        PhysicsManager.setScaleIn(0.08f / GraphicsManager.getInstance().getYScale());
        LevelManager.getInstance().initiate();
        SoundManager.getInstance().load(getContext());
        ScoreManager.getInstance().load(getContext());
        GraphicsManager.getInstance().setBkInWVGA(true);
        int scissorX = (int) (((float) GraphicsManager.screenWidth()) * 0.7f);
        scissorWidth = GraphicsManager.screenWidth() - scissorX;
        scissorHeight = (int) (((float) GraphicsManager.screenHeight()) * 0.3f);
        scissorCenterX = (scissorWidth / 2) + scissorX;
        scissorCenterY = (scissorHeight / 2) + 0;
        GraphicsManager.getInstance().getGL().glScissor(scissorX, (GraphicsManager.screenHeight() - 0) - scissorHeight, scissorWidth, scissorHeight);
        if (textBuilder == null) {
            textBuilder = new TextBuilder(Integer.valueOf((int) R.drawable.altas1), 32, -32);
        }
        GameManager.getInstance().initiate((MainActivity) getContext());
    }

    /* access modifiers changed from: protected */
    public boolean isPaused() {
        return GameManager.getInstance().isPaused();
    }

    /* access modifiers changed from: protected */
    public void onUpdateInternal(float elapsedTime) {
        PhysicsManager.instance.update(elapsedTime, 8, 10);
        if (currentManipulator != null) {
            currentManipulator.update(elapsedTime);
        }
    }

    /* access modifiers changed from: protected */
    public Class<R.drawable> getDrawableClass() {
        return R.drawable.class;
    }

    /* access modifiers changed from: protected */
    public Class<R.array> getArrayClass() {
        return R.array.class;
    }

    /* access modifiers changed from: protected */
    public String getGameFolderName() {
        return "explodelite";
    }

    /* access modifiers changed from: protected */
    public void pushAnimationInfos() {
        int[] iArr = new int[5];
        iArr[0] = R.array.altas1_explode;
        iArr[1] = 9;
        iArr[2] = R.drawable.magnent_explode;
        iArr[3] = 16;
        JNILibrary.setAnimationData(iArr);
    }
}
