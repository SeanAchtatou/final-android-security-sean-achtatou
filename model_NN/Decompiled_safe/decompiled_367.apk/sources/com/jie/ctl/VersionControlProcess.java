package com.jie.ctl;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Handler;
import com.live.wallpaper.fruit.R;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.StringTokenizer;

public class VersionControlProcess implements Runnable {
    public static final String SHOW_AD = "showad";
    protected static Context a;

    /* renamed from: a  reason: collision with other field name */
    private static VersionControlProcess f2a = null;

    /* renamed from: a  reason: collision with other field name */
    private static final String[] f3a = {"http://market.android.com", "http://market.android.com"};

    /* renamed from: a  reason: collision with other field name */
    private int f4a;

    /* renamed from: a  reason: collision with other field name */
    private NotificationManager f5a;

    /* renamed from: a  reason: collision with other field name */
    private final Handler f6a = new Handler();

    /* renamed from: a  reason: collision with other field name */
    private boolean f7a = false;
    private boolean b = false;

    private PendingIntent a(String str) {
        return PendingIntent.getActivity(a, 0, new Intent("android.intent.action.VIEW", Uri.parse(str)).setFlags(268435456), 268435456);
    }

    private void a() {
        HttpURLConnection httpURLConnection = null;
        try {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(f3a[0]);
            stringBuffer.append("?bn=");
            stringBuffer.append(193);
            stringBuffer.append("&v=");
            stringBuffer.append("1.2");
            HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(stringBuffer.toString()).openConnection();
            try {
                stringBuffer.delete(0, stringBuffer.length());
                httpURLConnection2.setDoOutput(true);
                httpURLConnection2.setDoInput(true);
                httpURLConnection2.setUseCaches(false);
                httpURLConnection2.setRequestMethod("POST");
                if (200 == httpURLConnection2.getResponseCode()) {
                    DataInputStream dataInputStream = new DataInputStream(httpURLConnection2.getInputStream());
                    InputStreamReader inputStreamReader = new InputStreamReader(dataInputStream);
                    byte[] a2 = a(inputStreamReader);
                    inputStreamReader.close();
                    dataInputStream.close();
                    String str = new String(a2, "UTF-8");
                    char charAt = str.charAt(0);
                    if (charAt < '1' || charAt > '6') {
                        this.b = false;
                        requestUpdate(this.f7a);
                        httpURLConnection2.disconnect();
                        return;
                    }
                    StringTokenizer stringTokenizer = new StringTokenizer(str, "|||");
                    int parseInt = Integer.parseInt(stringTokenizer.nextToken());
                    String nextToken = stringTokenizer.nextToken();
                    String nextToken2 = stringTokenizer.nextToken();
                    String nextToken3 = stringTokenizer.nextToken();
                    String nextToken4 = stringTokenizer.nextToken();
                    String nextToken5 = stringTokenizer.nextToken();
                    stringBuffer.append(nextToken);
                    stringBuffer.append(10);
                    stringBuffer.append("下载地址:");
                    stringBuffer.append(nextToken2);
                    stringBuffer.append("\n");
                    stringBuffer.append("发布时间：");
                    stringBuffer.append(nextToken3);
                    stringBuffer.append(10);
                    stringBuffer.append("最新发布应用：");
                    stringBuffer.append(nextToken4);
                    stringBuffer.append(10);
                    stringBuffer.append(nextToken5);
                    SharedPreferences sharedPreferences = a.getSharedPreferences(a.getPackageName() + "_preferences", 0);
                    if (!a.getPackageName().equals(nextToken4)) {
                        if (!comparePackageName(a, nextToken4)) {
                            String string = sharedPreferences.getString("updatePackage", "");
                            if (string.equals("") || !string.equals(nextToken4)) {
                                a(a, parseInt, stringBuffer.toString(), nextToken2);
                            } else if (this.f4a < 10) {
                                this.f4a++;
                            } else {
                                this.f4a = 0;
                                a(a, parseInt, stringBuffer.toString(), nextToken2);
                            }
                        }
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putString("updatePackage", nextToken4);
                        edit.commit();
                    }
                }
                httpURLConnection2.disconnect();
            } catch (Exception e) {
                Exception exc = e;
                httpURLConnection = httpURLConnection2;
                e = exc;
            } catch (Throwable th) {
                Throwable th2 = th;
                httpURLConnection = httpURLConnection2;
                th = th2;
                httpURLConnection.disconnect();
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            try {
                e.printStackTrace();
                httpURLConnection.disconnect();
            } catch (Throwable th3) {
                th = th3;
                httpURLConnection.disconnect();
                throw th;
            }
        }
    }

    private void a(Context context, int i, String str, String str2) {
        switch (i) {
            case 1:
                Intent intent = new Intent(context, AlertDialogActivity.class);
                intent.putExtra("msg", str);
                intent.putExtra("url", str2);
                intent.setFlags(268435456);
                context.startActivity(intent);
                return;
            case 2:
                m0a(str2);
                return;
            case 3:
            default:
                return;
            case 4:
            case 5:
                a(i == 4);
                return;
        }
    }

    private void a(boolean z) {
        SharedPreferences.Editor edit = a.getSharedPreferences(a.getPackageName() + "_preferences", 0).edit();
        edit.putBoolean(SHOW_AD, z);
        edit.commit();
    }

    private byte[] a(InputStreamReader inputStreamReader) {
        byte[] bArr = null;
        StringBuffer stringBuffer = new StringBuffer();
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                stringBuffer.append(readLine).append("\n");
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                bufferedReader.close();
            }
        }
        bArr = stringBuffer.toString().getBytes();
        return bArr;
    }

    public static boolean comparePackageName(Context context, String str) {
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < installedPackages.size(); i++) {
            PackageInfo packageInfo = installedPackages.get(i);
            if ((packageInfo.applicationInfo.flags & 1) <= 0 && str.equals(packageInfo.packageName)) {
                return true;
            }
        }
        return false;
    }

    public static VersionControlProcess getInstance(Context context) {
        if (context != null) {
            a = context;
        }
        if (f2a == null) {
            f2a = new VersionControlProcess();
        }
        return f2a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a  reason: collision with other method in class */
    public void m0a(String str) {
        String string = a.getResources().getString(R.string.update);
        String string2 = a.getResources().getString(R.string.tips);
        String string3 = a.getResources().getString(R.string.word);
        this.f5a = (NotificationManager) a.getSystemService("notification");
        Notification notification = new Notification(R.drawable.stat_notify_marketplace_update, string, System.currentTimeMillis());
        notification.setLatestEventInfo(a, string2, string3, a(str));
        notification.defaults = 1;
        notification.flags = 16;
        this.f5a.notify(1, notification);
    }

    public void requestUpdate(boolean z) {
        SharedPreferences.Editor edit = a.getSharedPreferences(a.getPackageName() + "_preferences", 0).edit();
        edit.putString("packageName", a.getPackageName());
        edit.commit();
        this.f4a = 0;
        if (!this.b) {
            this.f7a = z;
            if (this.f7a) {
                this.f6a.removeCallbacks(this);
                this.f6a.postDelayed(this, 120000);
                return;
            }
            this.f6a.removeCallbacks(this);
        }
    }

    public void run() {
        a();
        this.f6a.removeCallbacks(this);
        this.f6a.postDelayed(this, 43200000);
    }
}
