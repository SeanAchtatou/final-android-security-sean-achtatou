package com.jie.ctl;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class AlertDialogActivity extends Activity implements View.OnClickListener {
    private Button a;

    /* renamed from: a  reason: collision with other field name */
    private String f0a;
    private Button b;

    /* renamed from: b  reason: collision with other field name */
    private String f1b;

    private LinearLayout a() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, 200);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-2, -2);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(layoutParams);
        LinearLayout linearLayout2 = new LinearLayout(this);
        linearLayout2.setOrientation(0);
        linearLayout2.setLayoutParams(layoutParams2);
        linearLayout2.setGravity(16);
        linearLayout2.setPadding(5, 8, 5, 8);
        ImageView imageView = new ImageView(this);
        imageView.setImageDrawable(getApplicationContext().getResources().getDrawable(17301543));
        linearLayout2.addView(imageView);
        TextView textView = new TextView(this);
        textView.setLayoutParams(layoutParams4);
        textView.setGravity(16);
        textView.setPadding(10, 0, 0, 0);
        textView.setText("Update available");
        textView.setTextSize(22.0f);
        textView.setTextColor(-1);
        linearLayout2.addView(textView);
        linearLayout.addView(linearLayout2);
        ImageView imageView2 = new ImageView(this);
        imageView2.setLayoutParams(layoutParams2);
        imageView2.setImageResource(17301525);
        linearLayout.addView(imageView2);
        LinearLayout linearLayout3 = new LinearLayout(this);
        linearLayout3.setLayoutParams(layoutParams2);
        linearLayout3.setPadding(5, 5, 5, 35);
        ScrollView scrollView = new ScrollView(this);
        scrollView.setLayoutParams(layoutParams3);
        scrollView.setVerticalScrollBarEnabled(true);
        TextView textView2 = new TextView(this);
        textView2.setLayoutParams(layoutParams4);
        textView2.setPadding(8, 0, 8, 5);
        textView2.setText(this.f0a);
        textView2.setTextSize(18.0f);
        textView2.setTextColor(-1);
        scrollView.addView(textView2);
        linearLayout3.addView(scrollView);
        linearLayout.addView(linearLayout3);
        LinearLayout linearLayout4 = new LinearLayout(this);
        linearLayout4.setLayoutParams(layoutParams2);
        linearLayout4.setPadding(0, 5, 0, 0);
        linearLayout4.setBackgroundColor(-2236963);
        linearLayout4.setOrientation(0);
        layoutParams4.weight = 1.0f;
        Button button = new Button(this);
        button.setLayoutParams(layoutParams4);
        button.setId(16908313);
        button.setText("Confirm");
        linearLayout4.addView(button);
        Button button2 = new Button(this);
        button2.setLayoutParams(layoutParams4);
        button2.setId(16908314);
        button2.setText("Cancel");
        linearLayout4.addView(button2);
        linearLayout.addView(linearLayout4);
        return linearLayout;
    }

    public void onClick(View view) {
        if (view == this.a) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.f1b)));
        } else if (view == this.b) {
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        Intent intent = getIntent();
        this.f0a = intent.getStringExtra("msg");
        this.f1b = intent.getStringExtra("url");
        setContentView(a());
        this.a = (Button) findViewById(16908313);
        this.a.setOnClickListener(this);
        this.b = (Button) findViewById(16908314);
        this.b.setOnClickListener(this);
    }
}
