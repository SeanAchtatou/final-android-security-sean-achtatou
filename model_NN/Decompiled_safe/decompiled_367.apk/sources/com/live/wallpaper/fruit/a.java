package com.live.wallpaper.fruit;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;
import java.util.ArrayList;

class a {
    float a;

    /* renamed from: a  reason: collision with other field name */
    int f11a = 0;

    /* renamed from: a  reason: collision with other field name */
    Matrix f12a;

    /* renamed from: a  reason: collision with other field name */
    private RectF f13a;

    /* renamed from: a  reason: collision with other field name */
    ArrayList f14a;

    /* renamed from: a  reason: collision with other field name */
    float[] f15a;

    /* renamed from: a  reason: collision with other field name */
    Bitmap[] f16a;
    float b = 250.0f;

    /* renamed from: b  reason: collision with other field name */
    int f17b;

    /* renamed from: b  reason: collision with other field name */
    ArrayList f18b;
    float c;
    float d;
    float e = 0.5f;
    float f = 2.0f;
    float g = -3.0f;
    float h = 6.0f;
    float i;
    float j;
    float k;
    float l;
    float m = 0.0f;

    a(Bitmap[] bitmapArr, float f2, float f3, float f4, float f5) {
        this.f16a = bitmapArr;
        this.a = 255.0f;
        this.c = f2;
        this.d = f3;
        this.i = f4;
        this.j = f5;
        this.f14a = new ArrayList();
        this.f12a = new Matrix();
        this.f15a = new float[9];
        this.f15a[6] = 0.0f;
        this.f15a[7] = 0.0f;
        this.f15a[8] = 1.0f;
        this.f17b = 0;
        this.f18b = new ArrayList();
        this.f13a = new RectF();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        float random = (float) (((double) this.c) + (Math.random() * ((double) (this.d - this.c))));
        for (int i2 = 0; i2 < this.f18b.size(); i2++) {
            d dVar = (d) this.f18b.get(i2);
            dVar.f = (float) ((((double) this.g) + (Math.random() * ((double) this.h))) * ((double) random));
            if (dVar.f36a) {
                dVar.h = (float) (((double) (this.e + (this.f * random))) + Math.random());
            } else {
                dVar.h = ((float) (((double) (this.e + (this.f * random))) + Math.random())) * 0.78f;
            }
        }
        for (int i3 = 0; i3 < this.f14a.size(); i3++) {
            d dVar2 = (d) this.f14a.get(i3);
            dVar2.f = (float) ((((double) this.g) + (Math.random() * ((double) this.h))) * ((double) random));
            if (dVar2.f36a) {
                dVar2.h = (float) (((double) (this.e + (this.f * random))) + Math.random());
            } else {
                dVar2.h = ((float) (((double) (this.e + (this.f * random))) + Math.random())) * 0.78f;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (i2 == 3) {
            this.f17b = 200;
        } else if (i2 == 2) {
            this.f17b = 140;
        } else if (i2 == 1) {
            this.f17b = 80;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Canvas canvas, Paint paint) {
        this.f14a.clear();
        float f2 = this.b;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.f18b.size()) {
                break;
            }
            d dVar = (d) this.f18b.get(i3);
            dVar.g = dVar.h * (1.0f + ((dVar.b / this.j) * 0.5f));
            dVar.a += dVar.f;
            dVar.b += dVar.g;
            if (dVar.b > (-dVar.d) && dVar.b < this.j && dVar.a > (-this.m) - dVar.c && dVar.a < (this.i - this.m) + dVar.c) {
                float f3 = dVar.a + dVar.f34a.x + this.m;
                float f4 = dVar.b + dVar.f34a.y;
                this.f15a[0] = (float) Math.cos((double) dVar.i);
                this.f15a[1] = -((float) Math.sin((double) dVar.i));
                this.f15a[3] = -this.f15a[1];
                this.f15a[4] = this.f15a[0];
                this.f15a[2] = (float) ((((double) (-f3)) * Math.cos((double) dVar.i)) + (((double) f4) * Math.sin((double) dVar.i)) + ((double) f3));
                this.f15a[5] = (float) (((double) f4) + ((((double) (-f3)) * Math.sin((double) dVar.i)) - (((double) f4) * Math.cos((double) dVar.i))));
                this.f12a.setValues(this.f15a);
                canvas.save();
                canvas.setMatrix(this.f12a);
                paint.setAlpha((int) (dVar.e * this.a));
                this.f13a.left = dVar.a + this.m;
                this.f13a.top = dVar.b;
                this.f13a.right = dVar.a + this.m + dVar.c;
                this.f13a.bottom = dVar.b + dVar.d;
                canvas.drawBitmap(dVar.f33a, (Rect) null, this.f13a, paint);
                canvas.restore();
            }
            dVar.i = (float) (((double) dVar.i) + 0.017453292519943295d);
            if (((double) dVar.i) > 6.283185307179586d) {
                dVar.i = 0.0f;
            }
            if (dVar.b > this.j) {
                this.f14a.add(dVar);
                this.f18b.remove(i3);
                i3--;
                b(dVar);
            } else {
                float f5 = dVar.a + dVar.f34a.x;
                float f6 = dVar.b + dVar.f34a.y;
                float f7 = this.k - f5;
                float f8 = this.l - f6;
                float sqrt = (float) ((((double) f2) - Math.sqrt((double) ((f7 * f7) + (f8 * f8)))) / ((double) f2));
                if (sqrt > 0.0f) {
                    float atan2 = (float) Math.atan2((double) f8, (double) f7);
                    dVar.a = (float) (((double) dVar.a) + (Math.cos((double) atan2) * ((double) sqrt) * -12.0d));
                    dVar.b = (float) (Math.min(0.0d, Math.sin((double) atan2) * ((double) sqrt) * -8.0d) + ((double) dVar.b));
                }
            }
            i2 = i3 + 1;
        }
        this.b = (float) (250.0d * Math.pow(0.9900000095367432d, (double) this.f11a));
        this.f11a = this.f11a > 1000 ? this.f11a : this.f11a + 1;
        for (int i4 = 0; i4 < this.f14a.size(); i4++) {
            a((d) this.f14a.get(i4));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(MotionEvent motionEvent) {
        this.f11a = 0;
        this.k = motionEvent.getX() - this.m;
        this.l = motionEvent.getY();
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        int i2;
        int i3;
        if (this.f18b.size() == 0) {
            this.f18b.add(dVar);
            return;
        }
        int size = this.f18b.size() - 1;
        int i4 = (0 + size) / 2;
        int i5 = 0;
        int i6 = size;
        int i7 = 0;
        int i8 = i6;
        while (i5 < i8) {
            int i9 = i7 + 1;
            d dVar2 = (d) this.f18b.get(i4);
            if (dVar2.e <= dVar.e) {
                if (dVar2.e >= dVar.e) {
                    break;
                }
                i2 = i4 + 1;
                i3 = i8;
            } else {
                i3 = i4 - 1;
                i2 = i5;
            }
            i5 = i2;
            i4 = (i2 + i3) / 2;
            i8 = i3;
            i7 = i9;
        }
        d dVar3 = (d) this.f18b.get(i4);
        if (dVar.e > dVar3.e) {
            this.f18b.add(i4 + 1, dVar);
        } else if (dVar.e <= dVar3.e) {
            this.f18b.add(i4, dVar);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.f18b.clear();
        int i2 = (int) (((float) this.f17b) * 0.21f);
        for (int i3 = 0; i3 < i2; i3++) {
            Bitmap bitmap = this.f16a[(int) (Math.random() * 2.0d)];
            d dVar = new d(this);
            dVar.f33a = bitmap;
            dVar.f36a = false;
            b(dVar);
            dVar.b = (float) ((Math.random() * ((double) this.j)) - 100.0d);
            a(dVar);
        }
        int i4 = this.f17b - i2;
        for (int i5 = 0; i5 < i4; i5++) {
            Bitmap bitmap2 = this.f16a[(int) (((Math.random() * 2.0d) + ((double) this.f16a.length)) - 3.0d)];
            d dVar2 = new d(this);
            dVar2.f33a = bitmap2;
            dVar2.f36a = true;
            b(dVar2);
            dVar2.b = (float) ((Math.random() * ((double) this.j)) - 100.0d);
            a(dVar2);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(int i2) {
        switch (i2) {
            case 1:
                this.g = -0.1f;
                this.h = 0.21f;
                this.e = 0.08f;
                this.f = 0.13f;
                return;
            case 2:
                this.g = -1.3f;
                this.h = 2.6f;
                this.e = 0.12f;
                this.f = 1.34f;
                return;
            case 3:
                this.g = -1.6f;
                this.h = 3.1f;
                this.e = 1.2f;
                this.f = 1.9f;
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(d dVar) {
        dVar.i = (float) (Math.random() * 3.141592653589793d * 2.0d);
        dVar.a = (float) (Math.random() * ((double) this.i));
        dVar.b = (float) ((-50.0d * Math.random()) - 20.0d);
        float random = (float) (((double) this.c) + (Math.random() * ((double) (this.d - this.c))));
        dVar.c = ((float) dVar.f33a.getWidth()) * random;
        dVar.d = ((float) dVar.f33a.getHeight()) * random;
        dVar.f34a = new PointF(dVar.c / 2.0f, dVar.d / 2.0f);
        dVar.e = random == 1.0f ? random : 0.85f * random;
        dVar.f = (float) ((((double) this.g) + (Math.random() * ((double) this.h))) * ((double) random));
        dVar.g = (float) (((double) (this.e + (this.f * random))) + Math.random());
        if (dVar.f36a) {
            dVar.h = (float) (((double) ((random * this.f) + this.e)) + Math.random());
        } else {
            dVar.h = ((float) (((double) ((random * this.f) + this.e)) + Math.random())) * 0.78f;
        }
    }
}
