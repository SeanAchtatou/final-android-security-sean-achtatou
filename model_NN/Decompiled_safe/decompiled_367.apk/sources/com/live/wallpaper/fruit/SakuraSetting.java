package com.live.wallpaper.fruit;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.jie.ctl.VersionControlProcess;
import java.util.HashMap;

public class SakuraSetting extends PreferenceActivity {
    private LinearLayout a = null;

    /* renamed from: a  reason: collision with other field name */
    private AdView f8a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getPreferences(0).getBoolean(VersionControlProcess.SHOW_AD, true)) {
            this.a = new LinearLayout(this);
            this.a.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            this.a.setOrientation(1);
            this.f8a = new AdView(this, AdSize.BANNER, "a14d038e9104fbb");
            AdRequest adRequest = new AdRequest();
            HashMap hashMap = new HashMap();
            hashMap.put("color_bg", "000000");
            hashMap.put("color_bg_top", "000000");
            hashMap.put("color_border", "000000");
            hashMap.put("color_link", "FFFFFF");
            hashMap.put("color_text", "FFFFFF");
            hashMap.put("color_url", "FFFFFF");
            adRequest.setExtras(hashMap);
            this.f8a.loadAd(adRequest);
            this.a.addView(this.f8a);
            ListView listView = new ListView(this);
            listView.setId(16908298);
            listView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            listView.setDrawSelectorOnTop(false);
            this.a.addView(listView);
            setContentView(this.a);
        }
        addPreferencesFromResource(R.xml.setting);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.f8a.stopLoading();
        this.f8a.destroy();
        super.onDestroy();
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        String key = preference.getKey();
        if ("more".equals(key)) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:\"LW Livewallpaper\"")));
        } else if ("rec2".equals(key)) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.livewallpaper.livewallpaper.garfruit")));
        } else if ("rec3".equals(key)) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.live.wallpaper.mapleleaf")));
        } else if ("rec1".equals(key)) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.live.wallpaper.fruit")));
        } else if ("pro".equals(key)) {
            new AlertDialog.Builder(this).setTitle((int) R.string.alerttitle).setMessage((int) R.string.altersumary).setPositiveButton((int) R.string.alertyes, new f(this)).setNegativeButton((int) R.string.alertno, new e(this)).create().show();
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }
}
