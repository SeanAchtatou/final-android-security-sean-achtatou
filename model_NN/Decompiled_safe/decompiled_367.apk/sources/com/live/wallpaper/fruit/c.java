package com.live.wallpaper.fruit;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.service.wallpaper.WallpaperService;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.admob.android.ads.AdListener;
import com.admob.android.ads.AdView;
import com.jie.ctl.VersionControlProcess;

class c extends WallpaperService.Engine implements GestureDetector.OnGestureListener, AdListener {
    private float a = 0.0f;

    /* renamed from: a  reason: collision with other field name */
    private int f19a;

    /* renamed from: a  reason: collision with other field name */
    private Bitmap f20a;

    /* renamed from: a  reason: collision with other field name */
    private Paint f21a = new Paint();

    /* renamed from: a  reason: collision with other field name */
    private GestureDetector f22a;

    /* renamed from: a  reason: collision with other field name */
    private WindowManager.LayoutParams f23a = null;

    /* renamed from: a  reason: collision with other field name */
    private WindowManager f24a = null;

    /* renamed from: a  reason: collision with other field name */
    private AdView f25a = null;

    /* renamed from: a  reason: collision with other field name */
    private VersionControlProcess f26a = null;

    /* renamed from: a  reason: collision with other field name */
    final /* synthetic */ SakuraWallpaperService f27a;

    /* renamed from: a  reason: collision with other field name */
    private a f28a;

    /* renamed from: a  reason: collision with other field name */
    private final Runnable f29a = new b(this);

    /* renamed from: a  reason: collision with other field name */
    private boolean f30a = false;

    /* renamed from: a  reason: collision with other field name */
    private Bitmap[] f31a = null;
    private int b;

    /* renamed from: b  reason: collision with other field name */
    private boolean f32b;
    private int c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    c(SakuraWallpaperService sakuraWallpaperService) {
        super(sakuraWallpaperService);
        this.f27a = sakuraWallpaperService;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: package-private */
    public void a() {
        float height;
        float f;
        this.f20a = BitmapFactory.decodeResource(this.f27a.getResources(), R.drawable.background);
        if (this.f32b) {
            height = ((float) this.f27a.f9a) / ((float) this.f20a.getWidth());
            f = (float) this.f27a.f9a;
        } else {
            height = ((float) this.f27a.b) / ((float) this.f20a.getHeight());
            f = 1.125f * ((float) this.f27a.b);
        }
        Matrix matrix = new Matrix();
        matrix.postScale(height, height);
        this.f20a = Bitmap.createBitmap(this.f20a, 0, 0, this.f20a.getWidth(), this.f20a.getHeight(), matrix, true);
        if (this.f32b) {
            this.a = 0.0f;
        } else {
            this.a = ((float) (this.f27a.f9a - this.f20a.getWidth())) * 0.5f;
        }
        this.f28a = new a(this.f31a, 0.4f, 1.0f, f, (float) this.f27a.b);
        this.f28a.m = this.a;
        this.f28a.a(this.b);
        this.f28a.b(this.c);
        this.f28a.b();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        SurfaceHolder surfaceHolder = getSurfaceHolder();
        Canvas lockCanvas = surfaceHolder.lockCanvas();
        long currentTimeMillis = System.currentTimeMillis();
        if (lockCanvas != null) {
            this.f21a.setAntiAlias(true);
            this.f21a.setAlpha(255);
            lockCanvas.save();
            lockCanvas.translate(this.a, 0.0f);
            lockCanvas.drawBitmap(this.f20a, 0.0f, 0.0f, this.f21a);
            this.f28a.a(lockCanvas, this.f21a);
            lockCanvas.restore();
        }
        if (lockCanvas != null) {
            surfaceHolder.unlockCanvasAndPost(lockCanvas);
        }
        this.f27a.f10a.removeCallbacks(this.f29a);
        long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
        if (!this.f30a) {
            return;
        }
        if (currentTimeMillis2 >= ((long) this.f19a)) {
            this.f27a.f10a.post(this.f29a);
        } else {
            this.f27a.f10a.postDelayed(this.f29a, ((long) this.f19a) - currentTimeMillis2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void onCreate(SurfaceHolder surfaceHolder) {
        super.onCreate(surfaceHolder);
        setTouchEventsEnabled(true);
        SharedPreferences sharedPreferences = this.f27a.getSharedPreferences("com.live.wallpaper.fruit_preferences", 0);
        this.b = Integer.parseInt(sharedPreferences.getString(SakuraWallpaperService.QUANTITY, "2"));
        if (this.b <= 0) {
            this.b = 2;
        }
        this.c = Integer.parseInt(sharedPreferences.getString(SakuraWallpaperService.SPEED, "2"));
        if (this.c <= 0) {
            this.c = 2;
        }
        this.f22a = new GestureDetector(this);
        Matrix matrix = new Matrix();
        this.f31a = new Bitmap[6];
        int[] iArr = {R.drawable.flower1, R.drawable.flower2, R.drawable.flower3, R.drawable.petal1, R.drawable.petal2, R.drawable.petal3};
        matrix.reset();
        matrix.postScale(0.5f, 0.5f);
        for (int i = 0; i < 6; i++) {
            this.f31a[i] = BitmapFactory.decodeResource(this.f27a.getResources(), iArr[i]);
            this.f31a[i] = Bitmap.createBitmap(this.f31a[i], 0, 0, this.f31a[i].getWidth(), this.f31a[i].getHeight(), matrix, true);
        }
        this.f19a = 40;
        if (!isPreview()) {
            this.f26a = VersionControlProcess.getInstance(this.f27a);
            this.f26a.requestUpdate(true);
            return;
        }
        this.f24a = (WindowManager) this.f27a.getSystemService("window");
        this.f23a = new WindowManager.LayoutParams(-1, -2);
        this.f23a.alpha = 0.0f;
        this.f23a.flags = 8;
        this.f23a.flags |= 262144;
        this.f23a.flags |= 512;
        this.f23a.type = 2003;
        this.f23a.height = 60;
        this.f23a.gravity = 48;
        this.f23a.format = -1;
        this.f23a.token = null;
        this.f23a.x = 0;
        this.f23a.y = 0;
        this.f25a = new AdView(this.f27a, null);
        this.f25a.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        this.f25a.setBackgroundColor(-16777216);
        this.f25a.setPrimaryTextColor(-1);
        this.f25a.setSecondaryTextColor(-3355444);
        this.f25a.setKeywords("Android app game wallpaper");
        this.f25a.setRequestInterval(20);
    }

    public void onDestroy() {
        super.onDestroy();
        this.f27a.f10a.removeCallbacks(this.f29a);
        if (!isPreview() && this.f26a != null) {
            this.f26a.requestUpdate(false);
            this.f26a = null;
        }
        if (this.f25a != null) {
            this.f25a.setRequestInterval(0);
            this.f25a.setAdListener(null);
            this.f25a.cleanup();
            this.f25a = null;
            if (this.f24a != null) {
                try {
                    this.f24a.removeView(this.f25a);
                    this.f24a = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean onDown(MotionEvent motionEvent) {
        this.f28a.a(motionEvent);
        return false;
    }

    public void onFailedToReceiveAd(AdView adView) {
    }

    public void onFailedToReceiveRefreshedAd(AdView adView) {
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public void onOffsetsChanged(float f, float f2, float f3, float f4, int i, int i2) {
        if (!isPreview() && !this.f32b) {
            this.a = ((float) (this.f27a.f9a - this.f20a.getWidth())) * f;
            this.f28a.m = this.a;
        }
    }

    public void onReceiveAd(AdView adView) {
        if (isPreview()) {
            if (this.f23a != null) {
                this.f23a.alpha = 0.5f;
            }
            if (this.f24a != null && this.f25a != null) {
                try {
                    this.f24a.removeView(this.f25a);
                } catch (Exception e) {
                } finally {
                    this.f24a.addView(this.f25a, this.f23a);
                }
            }
        }
    }

    public void onReceiveRefreshedAd(AdView adView) {
        if (isPreview()) {
            if (this.f23a != null) {
                this.f23a.alpha = 0.5f;
            }
            if (this.f24a != null && this.f25a != null) {
                try {
                    this.f24a.removeView(this.f25a);
                } catch (Exception e) {
                } finally {
                    this.f24a.addView(this.f25a, this.f23a);
                }
            }
        }
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    public void onSurfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        super.onSurfaceChanged(surfaceHolder, i, i2, i3);
        if ((i2 > 0 && i3 > 0 && (this.f27a.f9a != i2 || this.f27a.b != i3)) || this.f32b != SakuraWallpaperService.a) {
            this.f32b = SakuraWallpaperService.a;
            this.f27a.f9a = i2;
            this.f27a.b = i3;
            a();
        }
    }

    public void onSurfaceCreated(SurfaceHolder surfaceHolder) {
        super.onSurfaceCreated(surfaceHolder);
        a();
        b();
    }

    public void onSurfaceDestroyed(SurfaceHolder surfaceHolder) {
        super.onSurfaceDestroyed(surfaceHolder);
        this.f30a = false;
        this.f27a.f10a.removeCallbacks(this.f29a);
    }

    public void onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        this.f22a.onTouchEvent(motionEvent);
    }

    public void onVisibilityChanged(boolean z) {
        super.onVisibilityChanged(z);
        this.f30a = z;
        if (z) {
            SharedPreferences sharedPreferences = this.f27a.getSharedPreferences("com.live.wallpaper.fruit_preferences", 0);
            int parseInt = Integer.parseInt(sharedPreferences.getString(SakuraWallpaperService.QUANTITY, "2"));
            if (parseInt != this.b) {
                this.f28a.a(parseInt);
                this.f28a.b();
                this.b = parseInt;
            }
            int parseInt2 = Integer.parseInt(sharedPreferences.getString(SakuraWallpaperService.SPEED, "2"));
            if (parseInt2 != this.c) {
                this.c = parseInt2;
                this.f28a.b(parseInt2);
                this.f28a.a();
                this.c = parseInt2;
            }
            b();
            if (isPreview() && this.f24a != null && this.f25a != null) {
                this.f25a.setAdListener(this);
                this.f25a.requestFreshAd();
                try {
                    this.f24a.removeView(this.f25a);
                } catch (Exception e) {
                } finally {
                    this.f24a.addView(this.f25a, this.f23a);
                }
            }
        } else {
            this.f27a.f10a.removeCallbacks(this.f29a);
            if (isPreview() && this.f24a != null && this.f25a != null) {
                try {
                    this.f25a.setAdListener(null);
                    this.f24a.removeView(this.f25a);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }
}
