package com.live.wallpaper.fruit;

import android.content.res.Configuration;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.util.DisplayMetrics;

public class SakuraWallpaperService extends WallpaperService {
    public static final String QUANTITY = "quantity";
    public static final String SPEED = "speed";
    /* access modifiers changed from: private */
    public static boolean a = false;
    public static c instance;

    /* renamed from: a  reason: collision with other field name */
    int f9a = 480;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with other field name */
    public final Handler f10a = new Handler();
    int b = 800;

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (configuration.orientation == 1) {
            a = false;
        }
        if (configuration.orientation == 2) {
            a = true;
        }
    }

    public WallpaperService.Engine onCreateEngine() {
        new DisplayMetrics();
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        this.f9a = displayMetrics.widthPixels;
        this.b = displayMetrics.heightPixels;
        if (this.f9a < this.b) {
            a = false;
        } else {
            a = true;
        }
        return new c(this);
    }
}
