package com.live.wallpaper.fruit;

import android.content.Context;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageOptionPreference extends Preference {
    private int a = R.drawable.icon;
    private int b = 0;

    public ImageOptionPreference(Context context) {
        super(context);
    }

    public ImageOptionPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = attributeSet.getAttributeResourceValue(null, "img", 0);
        if (this.a == 0) {
            this.a = R.drawable.icon;
        }
        this.b = attributeSet.getAttributeResourceValue(null, "txt", 0);
    }

    public ImageOptionPreference(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = attributeSet.getAttributeResourceValue(null, "img", 0);
        if (this.a == 0) {
            this.a = R.drawable.icon;
        }
        this.b = attributeSet.getAttributeResourceValue(null, "txt", 0);
    }

    public boolean isPersistent() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onBindView(View view) {
        super.onBindView(view);
        ((ImageView) view.findViewById(R.id.pref_img)).setImageResource(this.a);
        if (this.b != 0) {
            ((TextView) view.findViewById(R.id.pref_title)).setText(this.b);
        }
    }
}
