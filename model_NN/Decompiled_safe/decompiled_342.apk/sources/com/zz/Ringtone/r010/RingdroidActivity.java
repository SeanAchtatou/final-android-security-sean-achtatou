package com.zz.Ringtone.r010;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.zzbook.util.GenUtil;
import com.zzbook.util.OtherUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Semaphore;

public class RingdroidActivity extends ListActivity {
    private static final int CMD_ASSIGN = 3;
    private static final int CMD_DELETE = 2;
    private static final int CMD_EDIT = 1;
    private static final int CMD_PREVIEW = 4;
    private static final int CMD_RESRESH = 6;
    public static final int CMD_SEARCH = 5;
    public static final int NumPerPage = 6;
    protected static final int REQUEST_CODE_EDIT = 1;
    private static final int REQUEST_SEARCH_CODE = 100;
    protected static String strEdit;
    private boolean bNewIntent = false;
    private boolean bSearchState = false;
    private ListView lvRings;
    protected SimpleAdapter mAdapter;
    private ImageButton mBtnNext;
    private ImageButton mBtnPrev;
    protected int mCurItemIndex = 0;
    private String mFilterTxt = "";
    private LocalRunnable mLocalRunnable;
    private TextView mPageInfo;
    /* access modifiers changed from: private */
    public String mStrFile;
    private ProgressDialog mStreamingProgressDlg;
    protected boolean mbGetContentIntent = true;
    private boolean mbResume = false;
    /* access modifiers changed from: private */
    public int miPageIndex = 0;
    private int miPageTotal = 0;
    /* access modifiers changed from: private */
    public int miSetType;
    protected ArrayList<HashMap<String, Object>> mlist;
    protected ArrayList<HashMap<String, Object>> mlist1;
    /* access modifiers changed from: private */
    public ProgressDialog mloadDialog;
    private MediaPlayer previewPlayer;
    private Semaphore semaphore = new Semaphore(1);

    static /* synthetic */ int access$008(RingdroidActivity x0) {
        int i = x0.miPageIndex;
        x0.miPageIndex = i + 1;
        return i;
    }

    static /* synthetic */ int access$010(RingdroidActivity x0) {
        int i = x0.miPageIndex;
        x0.miPageIndex = i - 1;
        return i;
    }

    private void initView() {
        this.mBtnPrev = (ImageButton) findViewById(R.id.prev);
        this.mBtnNext = (ImageButton) findViewById(R.id.next);
        this.mPageInfo = (TextView) findViewById(R.id.page_info);
        this.mBtnPrev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                RingdroidActivity.access$010(RingdroidActivity.this);
                RingdroidActivity.this.getLocalRings();
                RingdroidActivity.this.updateView();
                RingdroidActivity.this.mAdapter.notifyDataSetChanged();
            }
        });
        this.mBtnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                RingdroidActivity.access$008(RingdroidActivity.this);
                RingdroidActivity.this.getLocalRings();
                RingdroidActivity.this.updateView();
                RingdroidActivity.this.mAdapter.notifyDataSetChanged();
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateView() {
        if (this.miPageIndex <= 0) {
            this.mBtnPrev.setVisibility(8);
            this.miPageIndex = 0;
        } else {
            this.mBtnPrev.setVisibility(0);
        }
        if (this.miPageIndex < this.miPageTotal - 1) {
            this.mBtnNext.setVisibility(0);
        } else {
            this.mBtnNext.setVisibility(8);
            this.miPageIndex = this.miPageTotal - 1;
        }
        if (this.miPageTotal <= 0) {
            this.mPageInfo.setText(getResources().getString(R.string.page_info_head) + "0/0");
            return;
        }
        StringBuilder sb = new StringBuilder(getResources().getString(R.string.page_info_head));
        sb.append(this.miPageIndex + 1).append("/");
        sb.append(this.miPageTotal);
        this.mPageInfo.setText(sb.toString());
    }

    private int caclTotalPages(int paramInt) {
        int iPages = paramInt / 6;
        if (paramInt % 6 != 0) {
            return iPages + 1;
        }
        return iPages;
    }

    private void showFinalAlert(CharSequence paramCharSequence) {
        new AlertDialog.Builder(this).setTitle(getResources().getText(R.string.alert_title_failure)).setMessage(paramCharSequence).setPositiveButton((int) R.string.alert_ok_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramInterface, int paramInt) {
                RingdroidActivity.this.finish();
            }
        }).setCancelable(false).show();
    }

    public void onCreate(Bundle paramBundle) {
        this.mloadDialog = new ProgressDialog(this);
        this.mloadDialog.setIndeterminate(true);
        this.mloadDialog.setMessage("Loading...");
        this.previewPlayer = new MediaPlayer();
        super.onCreate(paramBundle);
        setContentView((int) R.layout.ring_select);
        ((AdView) findViewById(R.id.ad)).loadAd(new AdRequest());
        initView();
        String strStatus = Environment.getExternalStorageState();
        if (strStatus.equals("mounted_to")) {
            showFinalAlert(getResources().getText(R.string.sdcard_readonly));
        } else if (strStatus.equals("shared")) {
            showFinalAlert(getResources().getText(R.string.sdcard_shared));
        } else {
            if (!strStatus.equals("mounted")) {
                showFinalAlert(getResources().getText(R.string.no_sdcard));
            }
            this.mlist = new ArrayList<>();
            this.mlist1 = new ArrayList<>();
            try {
                getLocalRings();
                GenUtil.systemPrintln(this.mlist.size() + " ringtones found");
                String[] arrayOfField = new String[CMD_PREVIEW];
                arrayOfField[0] = "_artist";
                arrayOfField[1] = "_album";
                arrayOfField[2] = "_title";
                arrayOfField[CMD_ASSIGN] = "_id";
                int[] arrayOfID = new int[CMD_PREVIEW];
                // fill-array-data instruction
                arrayOfID[0] = 2131099683;
                arrayOfID[1] = 2131099684;
                arrayOfID[2] = 2131099682;
                arrayOfID[3] = 2131099681;
                this.mAdapter = new SimpleAdapter(this, this.mlist1, R.layout.ring_select_row, arrayOfField, arrayOfID);
                this.lvRings = getListView();
                this.lvRings.setAdapter((ListAdapter) this.mAdapter);
                this.lvRings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView adapterView, View view, int paramInt, long paramLong) {
                        RingdroidActivity.this.mCurItemIndex = (RingdroidActivity.this.miPageIndex * 6) + paramInt;
                        Intent localintent = new Intent(RingdroidActivity.this, RingPreviewerActivity.class);
                        localintent.putExtra("cur_index", RingdroidActivity.this.mCurItemIndex);
                        localintent.putExtra("cur_list", RingdroidActivity.this.mlist);
                        RingdroidActivity.this.startActivity(localintent);
                    }
                });
                this.lvRings.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    public boolean onItemLongClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                        RingdroidActivity.this.mCurItemIndex = (RingdroidActivity.this.miPageIndex * 6) + arg2;
                        RingdroidActivity.this.confirmDelete();
                        return true;
                    }
                });
                this.mLocalRunnable = new LocalRunnable();
            } catch (Exception e) {
            }
        }
    }

    private void FilterList(List list, String strFilter) {
        if (strFilter != null && strFilter.length() >= 0) {
            int iIndex = 0;
            while (iIndex < list.size()) {
                if (!((String) ((HashMap) list.get(iIndex)).get("_dir")).toLowerCase().contains(strFilter.toLowerCase())) {
                    list.remove(iIndex);
                    iIndex--;
                }
                iIndex++;
            }
            this.miPageTotal = caclTotalPages(list.size());
            this.mlist1.clear();
            this.miPageIndex = 0;
            int iMax = (this.miPageIndex + 1) * 6 > this.mlist.size() ? this.mlist.size() : (this.miPageIndex + 1) * 6;
            for (int iIndex2 = this.miPageIndex * 6; iIndex2 < iMax; iIndex2++) {
                this.mlist1.add((HashMap) list.get(iIndex2));
            }
            updateView();
        }
    }

    class LocalRunnable implements Runnable {
        LocalRunnable() {
        }

        public void run() {
            RingdroidActivity.this.mloadDialog.show();
            RingdroidActivity.this.refreshListView();
            RingdroidActivity.this.mloadDialog.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public void refreshListView() {
        Semaphore semaphore2;
        try {
            this.semaphore.acquire();
            getLocalRings();
            Log.v("zxl", "refreshListView called������");
            this.mAdapter.notifyDataSetChanged();
            semaphore2 = this.semaphore;
        } catch (Exception e) {
            e.printStackTrace();
            semaphore2 = this.semaphore;
        } catch (Throwable th) {
            this.semaphore.release();
            throw th;
        }
        semaphore2.release();
    }

    public void onResume() {
        if (this.mbResume) {
            this.mLocalRunnable.run();
            Log.v("zxl", "onResume called������");
            updateView();
            this.mAdapter.notifyDataSetChanged();
        } else {
            this.mbResume = true;
        }
        super.onResume();
    }

    public void afterTextChanged(Editable paramEditable) {
        refreshListView();
    }

    public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    }

    public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
    }

    /* access modifiers changed from: protected */
    public AlertDialog onCreateDialog(int iParam) {
        if (iParam != 1) {
            return null;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.ring_picker_title);
        builder.setSingleChoiceItems((int) R.array.ring_types, 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                int unused = RingdroidActivity.this.miSetType = paramInt;
            }
        }).setPositiveButton((int) R.string.alertdialog_ok, new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                switch (RingdroidActivity.this.miSetType) {
                    case 0:
                        File file = new File(RingdroidActivity.this.mStrFile);
                        if (!file.isFile() || !file.exists()) {
                            Toast.makeText(RingdroidActivity.this, (int) R.string.tip_set_ring_failed, 1000).show();
                            return;
                        }
                        ContentValues values = new ContentValues();
                        values.put("_data", file.getAbsolutePath());
                        values.put("title", file.getName());
                        values.put("_size", Long.valueOf(file.length()));
                        values.put("mime_type", "audio/mp3");
                        values.put("is_ringtone", (Boolean) true);
                        values.put("is_notification", (Boolean) false);
                        values.put("is_alarm", (Boolean) false);
                        values.put("is_music", (Boolean) false);
                        RingtoneManager.setActualDefaultRingtoneUri(RingdroidActivity.this, 1, RingdroidActivity.this.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath()), values));
                        Toast.makeText(RingdroidActivity.this, (int) R.string.tip_set_ring_success, 1000).show();
                        return;
                    case 1:
                        File file2 = new File(RingdroidActivity.this.mStrFile);
                        if (!file2.isFile() || !file2.exists()) {
                            Toast.makeText(RingdroidActivity.this, (int) R.string.tip_set_notification_failed, 1000).show();
                            return;
                        }
                        ContentValues values2 = new ContentValues();
                        values2.put("_data", file2.getAbsolutePath());
                        values2.put("title", file2.getName());
                        values2.put("_size", Long.valueOf(file2.length()));
                        values2.put("mime_type", "audio/mp3");
                        values2.put("is_ringtone", (Boolean) false);
                        values2.put("is_notification", (Boolean) true);
                        values2.put("is_alarm", (Boolean) false);
                        values2.put("is_music", (Boolean) false);
                        RingtoneManager.setActualDefaultRingtoneUri(RingdroidActivity.this, 2, RingdroidActivity.this.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(file2.getAbsolutePath()), values2));
                        Toast.makeText(RingdroidActivity.this, (int) R.string.tip_set_notification_success, 1000).show();
                        return;
                    case 2:
                        File file3 = new File(RingdroidActivity.this.mStrFile);
                        if (!file3.isFile() || !file3.exists()) {
                            Toast.makeText(RingdroidActivity.this, (int) R.string.tip_set_alarm_failed, 1000).show();
                            return;
                        }
                        ContentValues values3 = new ContentValues();
                        values3.put("_data", file3.getAbsolutePath());
                        values3.put("title", file3.getName());
                        values3.put("_size", Long.valueOf(file3.length()));
                        values3.put("mime_type", "audio/mp3");
                        values3.put("is_ringtone", (Boolean) false);
                        values3.put("is_notification", (Boolean) false);
                        values3.put("is_alarm", (Boolean) true);
                        values3.put("is_music", (Boolean) false);
                        RingtoneManager.setActualDefaultRingtoneUri(RingdroidActivity.this, RingdroidActivity.CMD_PREVIEW, RingdroidActivity.this.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(file3.getAbsolutePath()), values3));
                        Toast.makeText(RingdroidActivity.this, (int) R.string.tip_set_alarm_success, 1000).show();
                        return;
                    default:
                        return;
                }
            }
        }).setNegativeButton((int) R.string.alertdialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
            }
        });
        return builder.create();
    }

    /* access modifiers changed from: private */
    public void confirmDelete() {
        if (((String) this.mlist.get(this.mCurItemIndex).get("_dir")).length() <= 0) {
            Toast.makeText(this, (int) R.string.delete_failed, 0).show();
        } else {
            new AlertDialog.Builder(this).setTitle((int) R.string.delete_ringtone).setMessage((int) R.string.confirm_delete_ringdroid).setPositiveButton((int) R.string.delete_ok_button, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    try {
                        String strDir = (String) RingdroidActivity.this.mlist.get(RingdroidActivity.this.mCurItemIndex).get("_dir");
                        GenUtil.systemPrintln("confirmDelete: " + strDir);
                        if (strDir.length() > 0) {
                            File file = new File(strDir);
                            try {
                                if (!file.isFile() || !file.exists()) {
                                    Toast.makeText(RingdroidActivity.this, (int) R.string.tip_delete_txt, 1000).show();
                                    RingdroidActivity.this.mlist.remove(RingdroidActivity.this.mCurItemIndex);
                                    RingdroidActivity.this.mAdapter.notifyDataSetChanged();
                                    RingdroidActivity.this.getLocalRings();
                                    RingdroidActivity.this.updateView();
                                    RingdroidActivity.this.mAdapter.notifyDataSetChanged();
                                }
                                file.delete();
                                Toast.makeText(RingdroidActivity.this, (int) R.string.tip_delete_success, 1000).show();
                                RingdroidActivity.this.mlist.remove(RingdroidActivity.this.mCurItemIndex);
                                RingdroidActivity.this.mAdapter.notifyDataSetChanged();
                                RingdroidActivity.this.getLocalRings();
                                RingdroidActivity.this.updateView();
                                RingdroidActivity.this.mAdapter.notifyDataSetChanged();
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(RingdroidActivity.this, (int) R.string.tip_delete_txt, 1000).show();
                            }
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }).setNegativeButton((int) R.string.delete_cancel_button, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                }
            }).setCancelable(true).show();
        }
    }

    private void onRecord() {
        try {
            Intent intent = new Intent("android.intent.action.EDIT", Uri.parse("record"));
            intent.putExtra("was_get_content_intent", this.mbGetContentIntent);
            intent.setClassName(this, strEdit);
            startActivityForResult(intent, 1);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void getLocalRings() {
        int iMin;
        synchronized (this.mlist) {
            if (this.mlist.size() > 0) {
                this.mlist.clear();
            }
            this.mlist.addAll(POPOnlineUtil.getRings(POPOnlineUtil.getRootPath()));
            this.miPageTotal = caclTotalPages(this.mlist.size());
            this.mlist1.clear();
            int iMax = (this.miPageIndex + 1) * 6 > this.mlist.size() ? this.mlist.size() : (this.miPageIndex + 1) * 6;
            if (this.miPageIndex * 6 >= this.mlist.size()) {
                iMin = (this.miPageIndex - 1) * 6;
            } else {
                iMin = this.miPageIndex * 6;
            }
            if (iMin < 0) {
                iMin = 0;
            }
            for (int iIndex = iMin; iIndex < iMax; iIndex++) {
                try {
                    this.previewPlayer.setDataSource((String) this.mlist.get(iIndex).get("_dir"));
                    this.previewPlayer.prepare();
                    this.mlist.get(iIndex).put("_artist", "Length: " + OtherUtil.FormatDuration(this.previewPlayer.getDuration() / 1000));
                    this.previewPlayer.reset();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.mlist1.add(this.mlist.get(iIndex));
            }
            updateView();
        }
    }

    /* access modifiers changed from: protected */
    public void startRingdroidEditor(int iIndexParam) {
        try {
            if (this.mlist.size() > iIndexParam) {
                Intent intent = new Intent("android.intent.action.EDIT", Uri.parse((String) this.mlist.get(iIndexParam).get("_dir")));
                intent.putExtra("was_get_content_intent", this.mbGetContentIntent);
                intent.setClassName(this, strEdit);
                startActivityForResult(intent, 1);
                return;
            }
            Toast toast = Toast.makeText(this, (int) R.string.no_record_tip, 2000);
            toast.setGravity(80, 0, 0);
            toast.show();
        } catch (Exception e) {
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != CMD_PREVIEW) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.bSearchState) {
            this.bNewIntent = true;
            this.bSearchState = false;
            this.mFilterTxt = "";
            refreshListView();
            return true;
        } else if (this.bNewIntent) {
            this.bNewIntent = false;
            return super.onKeyDown(keyCode, event);
        } else {
            quitSystem();
            return true;
        }
    }

    public void quitSystem() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.txt_quit_title)).setMessage(getResources().getString(R.string.txt_quit_body)).setIcon((int) R.drawable.icon).setPositiveButton(getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                RingdroidActivity.this.finish();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        super.onCreateOptionsMenu(paramMenu);
        paramMenu.add(0, 5, 0, (int) R.string.menu__search).setIcon((int) R.drawable.search);
        paramMenu.add(0, 6, 0, (int) R.string.txt_refresh).setIcon((int) R.drawable.ic_menu_refresh);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        super.onOptionsItemSelected(paramMenuItem);
        switch (paramMenuItem.getItemId()) {
            case CMD_SEARCH /*5*/:
                onSearchRequested();
                return true;
            case 6:
                this.mFilterTxt = "";
                refreshListView();
                return true;
            default:
                return true;
        }
    }

    private void handleIntent(Intent intent) {
        if ("android.intent.action.SEARCH".equals(intent.getAction())) {
            this.mFilterTxt = intent.getStringExtra("query");
        }
    }

    public boolean onSearchRequested() {
        startSearch("Search", true, null, false);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent paramIntent) {
        super.onNewIntent(paramIntent);
        handleIntent(paramIntent);
    }
}
