package com.google.gson;

import java.io.IOException;

interface u {
    void a() throws IOException;

    void a(JsonPrimitive jsonPrimitive) throws IOException;

    void a(JsonPrimitive jsonPrimitive, boolean z) throws IOException;

    void a(String str, JsonPrimitive jsonPrimitive, boolean z) throws IOException;

    void a(String str, boolean z) throws IOException;

    void a(boolean z) throws IOException;

    void b() throws IOException;

    void b(String str, boolean z) throws IOException;

    void b(boolean z) throws IOException;

    void c() throws IOException;

    void c(String str, boolean z) throws IOException;

    void c(boolean z) throws IOException;

    void d() throws IOException;

    void e() throws IOException;
}
