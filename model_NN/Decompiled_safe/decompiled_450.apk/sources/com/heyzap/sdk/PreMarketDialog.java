package com.heyzap.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

class PreMarketDialog extends HeyzapDialog {
    private static final String LOG_TAG = "HeyzapSDK";
    protected Drawable gameIcon;
    protected String gameName;
    protected String packageName;

    public PreMarketDialog(Context context, String str) {
        super(context);
        this.packageName = str;
        try {
            this.gameIcon = context.getPackageManager().getApplicationIcon(str);
            this.gameName = Utils.getAppLabel(context);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        setView(buildDialogContentView());
        addPrimaryButton("Install Heyzap", new View.OnClickListener() {
            public void onClick(View view) {
                String str = "market://details?id=com.heyzap.android&referrer=" + HeyzapAnalytics.getAnalyticsReferrer(PreMarketDialog.this.getContext());
                HeyzapAnalytics.trackEvent(PreMarketDialog.this.getContext(), "install-button-clicked");
                Log.d("HeyzapSDK", "Sending player to market, uri: " + str);
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent.addFlags(402653184);
                PreMarketDialog.this.getContext().startActivity(intent);
                PreMarketDialog.this.hide();
            }
        });
        addSecondaryButton("Skip", new View.OnClickListener() {
            public void onClick(View view) {
                HeyzapAnalytics.trackEvent(PreMarketDialog.this.getContext(), "skip-button-clicked");
                PreMarketDialog.this.hide();
            }
        });
    }

    private View buildDialogContentView() {
        float f = getContext().getResources().getDisplayMetrics().density;
        int i = (int) (10.0f * f);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(1);
        LinearLayout linearLayout2 = new LinearLayout(getContext());
        linearLayout2.setPadding(i, i, i, i);
        linearLayout2.setGravity(16);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams((int) (f * 50.0f), (int) (50.0f * f));
        layoutParams2.setMargins(0, 0, i, 0);
        ImageView imageView = new ImageView(getContext());
        imageView.setImageDrawable(this.gameIcon);
        linearLayout2.addView(imageView, layoutParams2);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -1);
        TextView textView = new TextView(getContext());
        textView.setTextSize(2, 17.0f);
        textView.setText("Check in to " + this.gameName);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setGravity(16);
        textView.setTextColor(-1);
        linearLayout2.addView(textView, layoutParams3);
        linearLayout.addView(linearLayout2, layoutParams);
        ImageView imageView2 = new ImageView(getContext());
        Drawables.setImageDrawable(getContext(), imageView2, "dialog_separator.png");
        linearLayout.addView(imageView2, layoutParams);
        LinearLayout linearLayout3 = new LinearLayout(getContext());
        linearLayout3.setPadding(i, i, i, i * 2);
        linearLayout3.setGravity(16);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams((int) (f * 70.0f), (int) (f * 70.0f));
        layoutParams4.setMargins(0, 0, i, 0);
        ImageView imageView3 = new ImageView(getContext());
        Drawables.setImageDrawable(getContext(), imageView3, "heyzap_circle.png");
        linearLayout3.addView(imageView3, layoutParams4);
        TextView textView2 = new TextView(getContext());
        textView2.setLineSpacing(0.0f, 1.2f);
        textView2.setTextColor(-1);
        textView2.setText("Install Heyzap to unlock cool features in " + this.gameName + ". Share games with friends, collect badges, and more! It's free and easy!");
        linearLayout3.addView(textView2, layoutParams);
        linearLayout.addView(linearLayout3, layoutParams);
        return linearLayout;
    }
}
