package com.heyzap.sdk;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

public class HeyzapLib {
    private static final String HEYZAP_INTENT_CLASS = ".CheckinForm";
    public static final String HEYZAP_PACKAGE = "com.heyzap.android";
    private static String packageName;

    static void broadcastEnableSDK(Context context) {
        Intent intent = new Intent("com.heyzap.android.enableSDK");
        intent.putExtra("packageName", context.getPackageName());
        context.sendBroadcast(intent);
    }

    public static void checkin(Context context) {
        checkin(context, null);
    }

    public static void checkin(Context context, String str) {
        sendNotification(context);
        rawCheckin(context, str);
    }

    public static boolean isSupported() {
        return Utils.isSupported();
    }

    static void launchCheckinForm(Context context, String str) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.putExtra("message", str);
        intent.setAction(HEYZAP_PACKAGE);
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.putExtra("packageName", context.getPackageName());
        intent.addFlags(402653184);
        intent.setComponent(new ComponentName(HEYZAP_PACKAGE, "com.heyzap.android.CheckinForm"));
        context.startActivity(intent);
    }

    public static void load(Context context) {
        load(context, true);
    }

    public static void load(Context context, boolean z) {
        sendNotification(context);
        String appLabel = Utils.getAppLabel(context);
        if (appLabel != null && !packageIsInstalled(HEYZAP_PACKAGE, context) && Utils.isSupported()) {
            new PopupToast(context.getApplicationContext(), appLabel).show(8000);
        }
    }

    static boolean packageIsInstalled(String str, Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(str);
            return launchIntentForPackage != null && packageManager.queryIntentActivities(launchIntentForPackage, 65536).size() > 0;
        } catch (Exception e) {
            return false;
        }
    }

    public static void promptCheckin(Context context) {
        load(context, true);
    }

    static void rawCheckin(Context context, String str) {
        packageName = context.getPackageName();
        Log.v(HeyzapAnalytics.LOG_TAG, "checkin-called");
        if (!Utils.isSupported()) {
            Toast.makeText(context, "Sorry, Heyzap needs a newer version of Android to run.", 1).show();
        } else if (packageIsInstalled(HEYZAP_PACKAGE, context)) {
            launchCheckinForm(context, str);
        } else {
            try {
                HeyzapAnalytics.trackEvent(context, "checkin-button-clicked");
                new PreMarketDialog(context.getApplicationContext(), packageName).show();
            } catch (ActivityNotFoundException e) {
            }
        }
    }

    static void sendNotification(final Context context) {
        final String appLabel = Utils.getAppLabel(context);
        if (appLabel != null && !packageIsInstalled(HEYZAP_PACKAGE, context) && Utils.isSupported()) {
            Utils.runOnlyOnce(context, "hz-notificationnnn", new Runnable() {
                public void run() {
                    HeyzapNotification.send(context, appLabel);
                }
            });
        }
    }
}
