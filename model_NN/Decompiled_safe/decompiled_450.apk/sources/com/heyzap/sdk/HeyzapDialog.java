package com.heyzap.sdk;

import android.content.Context;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

class HeyzapDialog extends ClickableToast {
    private static final int BUTTON_HEIGHT = 40;
    private static final int BUTTON_TEXT_SIZE = 13;
    private static final int BUTTON_WIDTH = 120;
    private RelativeLayout buttonRow;
    private View contentView;
    private LinearLayout dialogView = new LinearLayout(getContext());

    public HeyzapDialog(Context context) {
        super(context);
        this.dialogView.setOrientation(1);
        this.dialogView.setBackgroundDrawable(Drawables.getDrawable(context, 0));
        int dpToPx = Utils.dpToPx(context, 2);
        this.dialogView.setPadding(dpToPx, dpToPx, dpToPx, dpToPx);
        int dpToPx2 = Utils.dpToPx(context, 10);
        setPadding(dpToPx2, dpToPx2, dpToPx2, dpToPx2);
        setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                return false;
            }
        });
        addView(this.dialogView, new ViewGroup.LayoutParams(-1, -2));
    }

    private void addButton(String str, int i, int i2, int i3, View.OnClickListener onClickListener) {
        float f = getContext().getResources().getDisplayMetrics().density;
        if (this.buttonRow == null) {
            this.buttonRow = new RelativeLayout(getContext());
            Drawables.setBackgroundDrawable(getContext(), this.buttonRow, 1);
            this.buttonRow.setPadding((int) (f * 2.0f), (int) (5.0f * f), (int) (2.0f * f), (int) (4.0f * f));
            this.dialogView.addView(this.buttonRow, this.dialogView.getChildCount());
        }
        Button button = new Button(getContext());
        Drawables.setBackgroundDrawable(getContext(), button, i2);
        button.setTextColor(i);
        button.setTextSize(13.0f);
        button.setText(str);
        button.setOnClickListener(onClickListener);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (120.0f * f), (int) (f * 40.0f));
        layoutParams.addRule(i3);
        this.buttonRow.addView(button, layoutParams);
    }

    public void addPrimaryButton(String str, View.OnClickListener onClickListener) {
        addButton(str, -1, 2, 11, onClickListener);
    }

    public void addSecondaryButton(String str, View.OnClickListener onClickListener) {
        addButton(str, -16777216, 3, 9, onClickListener);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() != 4) {
            return super.dispatchKeyEvent(keyEvent);
        }
        hide();
        return true;
    }

    public WindowManager.LayoutParams getWmParams() {
        WindowManager.LayoutParams wmParams = super.getWmParams();
        wmParams.flags = 262178;
        wmParams.windowAnimations = 16973826;
        wmParams.dimAmount = 0.5f;
        return wmParams;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 4) {
            hide();
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setView(View view) {
        if (this.contentView != null) {
            this.dialogView.removeView(view);
        }
        this.dialogView.addView(view, 0);
        this.contentView = view;
    }
}
