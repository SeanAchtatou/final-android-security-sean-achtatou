package com.heyzap.sdk;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

class PopupToast extends ClickableToast {
    private static PopupToast previousToastRef;
    private float density = -1.0f;
    private String gameName;
    private RelativeLayout popupView;
    private TextView promptText;
    private Integer windowAnimation;

    public PopupToast(Context context, String str) {
        super(context);
        hideOldPopup();
        previousToastRef = this;
        HeyzapAnalytics.trackEvent(context, "popup-shown");
        this.gameName = str;
        float f = context.getResources().getDisplayMetrics().density;
        this.popupView = new RelativeLayout(context);
        this.popupView.setLayoutParams(new ViewGroup.LayoutParams(-1, Utils.dpToPx(context, 70)));
        ImageView imageView = new ImageView(context);
        int dpToPx = Utils.dpToPx(context, 40);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(dpToPx, dpToPx);
        layoutParams.addRule(11);
        layoutParams.addRule(10);
        imageView.setLayoutParams(layoutParams);
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PopupToast.this.hide();
            }
        });
        this.promptText = new TextView(context);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, Utils.dpToPx(context, 50));
        layoutParams2.setMargins(Utils.dpToPx(context, 75), Utils.dpToPx(context, 10), Utils.dpToPx(context, 120), Utils.dpToPx(context, 10));
        this.promptText.setLayoutParams(layoutParams2);
        this.promptText.setGravity(16);
        this.promptText.setTextSize(12.0f);
        this.promptText.setTextColor(-1);
        this.promptText.setTypeface(Typeface.DEFAULT_BOLD);
        this.promptText.setShadowLayer(1.0f, 0.0f, -1.0f, Color.rgb(2, 56, 130));
        this.popupView.addView(imageView);
        this.popupView.addView(this.promptText);
        addView(this.popupView);
        this.popupView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PopupToast.this.onCheckinClick(view);
            }
        });
        updateLayout();
    }

    public static void hideOldPopup() {
        if (previousToastRef != null) {
            previousToastRef.hide();
        }
    }

    public int getAnimation(Context context) {
        if (this.windowAnimation != null) {
            return this.windowAnimation.intValue();
        }
        PopupWindow popupWindow = new PopupWindow(context);
        try {
            Field declaredField = PopupWindow.class.getDeclaredField("mIsDropdown");
            declaredField.setAccessible(true);
            declaredField.setBoolean(popupWindow, true);
            Field declaredField2 = PopupWindow.class.getDeclaredField("mAnimationStyle");
            declaredField2.setAccessible(true);
            declaredField2.setInt(popupWindow, -1);
            Field declaredField3 = PopupWindow.class.getDeclaredField("mAboveAnchor");
            declaredField3.setAccessible(true);
            declaredField3.setBoolean(popupWindow, false);
            Method declaredMethod = PopupWindow.class.getDeclaredMethod("computeAnimationResource", new Class[0]);
            declaredMethod.setAccessible(true);
            this.windowAnimation = (Integer) declaredMethod.invoke(popupWindow, new Object[0]);
        } catch (Exception e) {
        }
        if (this.windowAnimation == null) {
            this.windowAnimation = 16973828;
        }
        return this.windowAnimation.intValue();
    }

    public WindowManager.LayoutParams getWmParams() {
        WindowManager.LayoutParams wmParams = super.getWmParams();
        wmParams.gravity = 48;
        wmParams.windowAnimations = getAnimation(getContext());
        wmParams.flags |= 262400;
        return wmParams;
    }

    public void hide() {
        super.hide();
        previousToastRef = null;
    }

    public boolean isVertical() {
        int orientation = this.windowManager.getDefaultDisplay().getOrientation();
        return orientation == 0 || orientation == 2;
    }

    public void onCheckinClick(View view) {
        HeyzapAnalytics.trackEvent(getContext(), "popup-clicked");
        HeyzapLib.rawCheckin(getContext(), null);
        hide();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 4) {
            hide();
        }
        return super.onTouchEvent(motionEvent);
    }

    public void updateLayout() {
        boolean isVertical = isVertical();
        Drawables.setBackgroundDrawable(getContext(), this.popupView, isVertical ? "checkin_vert_popdown.png" : "checkin_popdown.png");
        this.promptText.setTextSize((float) ((isVertical || this.gameName.length() > 25) ? 12 : 14));
        this.promptText.setText("Welcome to" + ((!isVertical || this.gameName.length() >= 18) ? " " : "\n") + Utils.truncate(this.gameName, isVertical ? 25 : 40) + "!");
    }
}
