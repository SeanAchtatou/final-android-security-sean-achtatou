package com.heyzap.sdk;

import com.unity3d.player.UnityPlayer;

public class UnityHelper {
    public static void checkin(final String str) {
        UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
            public void run() {
                HeyzapLib.checkin(UnityPlayer.currentActivity, str);
            }
        });
    }

    public static void load(final boolean z) {
        UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
            public void run() {
                HeyzapLib.load(UnityPlayer.currentActivity, z);
            }
        });
    }

    public static void promptCheckin() {
        UnityPlayer.currentActivity.runOnUiThread(new Runnable() {
            public void run() {
                HeyzapLib.promptCheckin(UnityPlayer.currentActivity);
            }
        });
    }
}
