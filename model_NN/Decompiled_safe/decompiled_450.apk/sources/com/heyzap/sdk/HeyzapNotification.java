package com.heyzap.sdk;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

class HeyzapNotification {
    private static final int NOTIFICATION_ID = 100101001;

    HeyzapNotification() {
    }

    public static void send(Context context, String str) {
        Context applicationContext = context.getApplicationContext();
        String str2 = "Get more from " + str;
        Notification notification = new Notification(17301516, str2 + "\n" + "Install Heyzap to play with your friends!", System.currentTimeMillis());
        notification.flags |= 16;
        notification.setLatestEventInfo(applicationContext, str2, "Install Heyzap to play with your friends!", PendingIntent.getActivity(context, 0, new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.heyzap.android&referrer=" + HeyzapAnalytics.getAnalyticsReferrer(context, "notification=true"))), 0));
        ((NotificationManager) context.getSystemService("notification")).notify(NOTIFICATION_ID, notification);
        HeyzapAnalytics.trackEvent(context, "notification-sent");
    }
}
