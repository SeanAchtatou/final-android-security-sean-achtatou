package softkos.untanglemeextreme;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int ad_moveme = 2130837504;
        public static final int aleft = 2130837505;
        public static final int aright = 2130837506;
        public static final int arrow_left = 2130837507;
        public static final int arrow_right = 2130837508;
        public static final int back_0 = 2130837509;
        public static final int back_1 = 2130837510;
        public static final int back_2 = 2130837511;
        public static final int back_3 = 2130837512;
        public static final int birdy_ad = 2130837513;
        public static final int boxit_ad = 2130837514;
        public static final int bug = 2130837515;
        public static final int bug_1 = 2130837516;
        public static final int bug_2 = 2130837517;
        public static final int bug_3 = 2130837518;
        public static final int bug_locked = 2130837519;
        public static final int button_off = 2130837520;
        public static final int button_on = 2130837521;
        public static final int connect_ad = 2130837522;
        public static final int cubix_ad = 2130837523;
        public static final int frogs_jump_ad = 2130837524;
        public static final int heyzap_game = 2130837525;
        public static final int heyzap_menu = 2130837526;
        public static final int icon = 2130837527;
        public static final int menuimage = 2130837528;
        public static final int menuimage_inv = 2130837529;
        public static final int other_button_off = 2130837530;
        public static final int other_button_on = 2130837531;
        public static final int rotateme_ad = 2130837532;
        public static final int solved = 2130837533;
        public static final int tripeaks_ad = 2130837534;
        public static final int turnmeon_ad = 2130837535;
        public static final int zoom_in = 2130837536;
        public static final int zoom_out = 2130837537;
    }

    public static final class id {
        public static final int back_image = 2131034139;
        public static final int background_textview = 2131034136;
        public static final int cancel = 2131034130;
        public static final int close_dlg = 2131034121;
        public static final int empty2 = 2131034115;
        public static final int empty3 = 2131034117;
        public static final int empty4 = 2131034118;
        public static final int empty_5 = 2131034120;
        public static final int empty_textview = 2131034133;
        public static final int how_to_play_text1 = 2131034113;
        public static final int how_to_play_text2 = 2131034114;
        public static final int how_to_play_text3 = 2131034116;
        public static final int how_to_play_text5 = 2131034119;
        public static final int is_solved_level = 2131034122;
        public static final int layout_gameback = 2131034137;
        public static final int layout_gamepoints = 2131034142;
        public static final int layout_root = 2131034112;
        public static final int layout_root3 = 2131034128;
        public static final int layout_root_settings = 2131034131;
        public static final int next10_level = 2131034127;
        public static final int next_back = 2131034140;
        public static final int next_level = 2131034125;
        public static final int next_point = 2131034145;
        public static final int ok = 2131034129;
        public static final int point_image = 2131034144;
        public static final int point_textview = 2131034141;
        public static final int prev10_level = 2131034126;
        public static final int prev_back = 2131034138;
        public static final int prev_level = 2131034123;
        public static final int prev_point = 2131034143;
        public static final int reset_all_levels = 2131034134;
        public static final int selected_level = 2131034124;
        public static final int settings_textview = 2131034132;
        public static final int vibration_setting = 2131034135;
    }

    public static final class layout {
        public static final int how_to_play = 2130903040;
        public static final int level_selection = 2130903041;
        public static final int main = 2130903042;
        public static final int settings = 2130903043;
    }

    public static final class string {
        public static final int app_name = 2130968576;
    }
}
