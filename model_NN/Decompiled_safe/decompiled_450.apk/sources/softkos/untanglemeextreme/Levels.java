package softkos.untanglemeextreme;

import android.content.Context;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Levels {
    static Levels instance = null;
    int currLevel = 0;
    int file_pos = 0;
    ArrayList<Line> lineList = new ArrayList<>();
    ArrayList<GamePoint> pointList = new ArrayList<>();
    Random random;
    int screenH = 0;
    int screenW = 0;
    int[] solved_levels = new int[1000];

    public Levels() {
        for (int i = 0; i < this.solved_levels.length; i++) {
            this.solved_levels[i] = 0;
        }
        this.random = new Random();
    }

    public void setScreenSize(int w, int h) {
        this.screenW = w;
        this.screenH = h;
    }

    public int getLevelCount() {
        return 160;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public GamePoint getPoint(int index) {
        if (index < 0 || index >= this.pointList.size()) {
            return null;
        }
        return this.pointList.get(index);
    }

    public int getPointCount() {
        return this.pointList.size();
    }

    public void addPoint(GamePoint p) {
        this.pointList.add(p);
    }

    public boolean isLevelSolved() {
        if (getLinesCount() == 0) {
            return false;
        }
        for (int i = 0; i < getLinesCount(); i++) {
            if (getLine(i).isIntersected()) {
                return false;
            }
        }
        int prev = this.solved_levels[this.currLevel];
        this.solved_levels[this.currLevel] = 1;
        if (prev == 0) {
            FileWR.getInstance().saveGame();
        }
        return true;
    }

    public void clearSolved() {
        Context c = null;
        Vars.getInstance().resetLevel();
        if (MainActivity.getInstance() != null) {
            c = MainActivity.getInstance().getApplicationContext();
        }
        for (int i = 0; i < this.solved_levels.length; i++) {
            this.solved_levels[i] = 0;
            try {
                c.deleteFile("level_" + i);
            } catch (Exception e) {
            }
        }
    }

    public int isSolved(int i) {
        if (i < 0) {
            return 0;
        }
        return this.solved_levels[i];
    }

    public void setSolved(int i) {
        this.solved_levels[i] = 1;
    }

    public void resetSolved(int i) {
        this.solved_levels[i] = 0;
    }

    public int getSolvedLevel(int lev) {
        return this.solved_levels[lev];
    }

    public void addLine(Line l) {
        this.lineList.add(l);
    }

    public int getLinesCount() {
        return this.lineList.size();
    }

    public Line getLine(int index) {
        return this.lineList.get(index);
    }

    public double dist(double x1, double y1, double x2, double y2) {
        return Math.sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)));
    }

    public void checkIntersection() {
        int i = 0;
        while (i < getLinesCount()) {
            try {
                getLine(i).setIntersected(false);
                i++;
            } catch (Exception e) {
                return;
            }
        }
        for (int i2 = 0; i2 < getLinesCount(); i2++) {
            double x1 = (double) getPoint(getLine(i2).start_index).x;
            double y1 = (double) getPoint(getLine(i2).start_index).y;
            double x2 = (double) getPoint(getLine(i2).end_index).x;
            double y2 = (double) getPoint(getLine(i2).end_index).y;
            if (x1 == x2) {
                x1 += 1.0d;
            }
            if (y1 == y2) {
                y1 += 1.0d;
            }
            for (int k = 0; k < getLinesCount(); k++) {
                if (k != i2) {
                    double u1 = (double) getPoint(getLine(k).start_index).x;
                    double v1 = (double) getPoint(getLine(k).start_index).y;
                    double u2 = (double) getPoint(getLine(k).end_index).x;
                    double v2 = (double) getPoint(getLine(k).end_index).y;
                    double b1 = (y2 - y1) / (x2 - x1);
                    double b2 = (v2 - v1) / (u2 - u1);
                    double a1 = y1 - (b1 * x1);
                    double xi = (-(a1 - (v1 - (b2 * u1)))) / (b1 - b2);
                    double yi = a1 + (b1 * xi);
                    if (dist(xi, yi, x1, y1) > 1.0d && dist(xi, yi, x2, y2) > 1.0d && dist(xi, yi, u1, v1) > 1.0d && dist(xi, yi, u2, v2) > 1.0d && (x1 - xi) * (xi - x2) >= 0.0d && (u1 - xi) * (xi - u2) >= 0.0d && (y1 - yi) * (yi - y2) >= 0.0d && (v1 - yi) * (yi - v2) >= 0.0d) {
                        getLine(i2).setIntersected(true);
                        getLine(k).setIntersected(true);
                    }
                }
            }
        }
        for (int i3 = 0; i3 < this.pointList.size(); i3++) {
            int x12 = this.pointList.get(i3).x;
            int y12 = this.pointList.get(i3).y;
            for (int j = i3 + 1; j < this.pointList.size(); j++) {
                if (dist((double) x12, (double) y12, (double) this.pointList.get(j).x, (double) this.pointList.get(j).y) <= 2.0d) {
                    for (int l = 0; l < this.lineList.size(); l++) {
                        if (this.lineList.get(l).start_index == i3) {
                            this.lineList.get(l).setIntersected(true);
                        }
                        if (this.lineList.get(l).end_index == i3) {
                            this.lineList.get(l).setIntersected(true);
                        }
                        if (this.lineList.get(l).start_index == j) {
                            this.lineList.get(l).setIntersected(true);
                        }
                        if (this.lineList.get(l).end_index == j) {
                            this.lineList.get(l).setIntersected(true);
                        }
                    }
                }
            }
        }
    }

    public void initLevel(int lev) {
        this.pointList.clear();
        this.lineList.clear();
        this.screenW = 320;
        this.screenH = 480;
        this.screenW = Vars.getInstance().getScreenWidth();
        this.screenH = Vars.getInstance().getScreenHeight();
        if (lev < 0) {
            lev = 0;
        }
        if (lev == 0) {
            addLevelData(getPoints(0, 4), new Line[]{new Line(0, 1), new Line(0, 2), new Line(1, 2), new Line(3, 2), new Line(1, 3)});
            this.pointList.get(0).setLocked();
        }
        if (lev == 1) {
            addLevelData(getPoints(0, 5), new Line[]{new Line(0, 2), new Line(0, 3), new Line(0, 1), new Line(2, 4), new Line(2, 3), new Line(1, 4), new Line(1, 3), new Line(3, 4)});
        }
        if (lev == 2) {
            addLevelData(getPoints(0, 6), new Line[]{new Line(0, 1), new Line(0, 2), new Line(0, 3), new Line(0, 4), new Line(0, 5), new Line(1, 4), new Line(1, 5), new Line(2, 4), new Line(2, 3), new Line(3, 5)});
            this.pointList.get(2).setLocked();
        }
        if (lev == 3) {
            addLevelData(getPoints(0, 7), new Line[]{new Line(0, 2), new Line(0, 5), new Line(0, 6), new Line(1, 3), new Line(1, 4), new Line(1, 6), new Line(2, 4), new Line(2, 6), new Line(3, 5), new Line(3, 6), new Line(4, 6), new Line(5, 6)});
        }
        if (lev == 4) {
            addLevelData(getPoints(0, 7), new Line[]{new Line(0, 1), new Line(0, 2), new Line(0, 4), new Line(0, 5), new Line(0, 6), new Line(1, 3), new Line(1, 4), new Line(1, 5), new Line(2, 5), new Line(2, 6), new Line(3, 4), new Line(3, 5)});
            this.pointList.get(1).setLocked();
        }
        if (lev == 5) {
            addLevelData(getPoints(0, 8), new Line[]{new Line(0, 4), new Line(0, 2), new Line(0, 5), new Line(1, 3), new Line(1, 4), new Line(2, 5), new Line(2, 6), new Line(2, 7), new Line(3, 4), new Line(3, 5), new Line(3, 6), new Line(3, 7), new Line(4, 5), new Line(5, 6), new Line(6, 7)});
        }
        if (lev == 6) {
            addLevelData(getPoints(0, 8), new Line[]{new Line(0, 1), new Line(0, 3), new Line(0, 5), new Line(0, 6), new Line(0, 7), new Line(1, 3), new Line(1, 6), new Line(2, 3), new Line(2, 4), new Line(2, 5), new Line(2, 7), new Line(6, 7), new Line(3, 4), new Line(4, 5), new Line(3, 5), new Line(5, 7)});
            this.pointList.get(5).setLocked();
        }
        if (lev == 7) {
            addLevelData(getPoints(0, 8), new Line[]{new Line(0, 3), new Line(0, 4), new Line(0, 5), new Line(0, 6), new Line(1, 3), new Line(1, 5), new Line(1, 7), new Line(3, 6), new Line(3, 5), new Line(2, 4), new Line(2, 5), new Line(2, 7), new Line(4, 7), new Line(5, 7), new Line(2, 5)});
            this.pointList.get(0).setLocked();
            this.pointList.get(4).setLocked();
        }
        if (lev == 8) {
            addLevelData(getPoints(0, 9), new Line[]{new Line(0, 5), new Line(0, 7), new Line(0, 4), new Line(0, 8), new Line(0, 2), new Line(2, 6), new Line(2, 4), new Line(2, 8), new Line(1, 3), new Line(1, 6), new Line(3, 8), new Line(3, 6), new Line(1, 7), new Line(1, 8), new Line(0, 1), new Line(4, 5), new Line(5, 7), new Line(6, 8)});
        }
        if (lev == 9) {
            addLevelData(getPoints(0, 9), new Line[]{new Line(8, 6), new Line(8, 3), new Line(3, 6), new Line(1, 6), new Line(5, 1), new Line(8, 5), new Line(5, 3), new Line(3, 1), new Line(6, 4), new Line(1, 4), new Line(0, 8), new Line(0, 5), new Line(5, 7), new Line(7, 1), new Line(2, 1), new Line(0, 7), new Line(0, 2), new Line(2, 4), new Line(7, 2)});
            this.pointList.get(1).setLocked();
            this.pointList.get(5).setLocked();
        }
        if (lev == 10) {
            addLevelData(getPoints(0, 9), new Line[]{new Line(0, 7), new Line(7, 3), new Line(3, 1), new Line(7, 1), new Line(7, 5), new Line(1, 5), new Line(8, 5), new Line(3, 8), new Line(8, 2), new Line(5, 2), new Line(6, 8), new Line(4, 2), new Line(8, 4), new Line(6, 4), new Line(3, 6), new Line(0, 4), new Line(0, 3), new Line(0, 6)});
            this.pointList.get(5).setLocked();
            this.pointList.get(8).setLocked();
            this.pointList.get(3).setLocked();
        }
        if (lev == 11) {
            addLevelData(getPoints(0, 10), new Line[]{new Line(2, 4), new Line(5, 4), new Line(4, 0), new Line(4, 8), new Line(8, 3), new Line(0, 8), new Line(0, 3), new Line(5, 0), new Line(8, 6), new Line(9, 3), new Line(3, 6), new Line(0, 9), new Line(9, 6), new Line(2, 9), new Line(5, 2), new Line(2, 0), new Line(2, 7), new Line(7, 9), new Line(7, 6), new Line(1, 6), new Line(2, 1), new Line(7, 1)});
        }
        if (lev == 12) {
            addLevelData(getPoints(0, 10), new Line[]{new Line(3, 6), new Line(1, 6), new Line(6, 4), new Line(4, 2), new Line(9, 2), new Line(2, 0), new Line(2, 7), new Line(9, 4), new Line(6, 2), new Line(0, 7), new Line(5, 7), new Line(0, 5), new Line(8, 0), new Line(8, 5), new Line(3, 0), new Line(3, 2), new Line(1, 9), new Line(1, 4), new Line(3, 9), new Line(3, 8), new Line(3, 5), new Line(1, 3)});
            this.pointList.get(1).setLocked();
            this.pointList.get(9).setLocked();
        }
        if (lev == 13) {
            addLevelData(getPoints(0, 10), new Line[]{new Line(8, 5), new Line(5, 2), new Line(8, 0), new Line(8, 2), new Line(2, 6), new Line(2, 0), new Line(0, 6), new Line(3, 6), new Line(1, 3), new Line(8, 3), new Line(8, 1), new Line(7, 8), new Line(1, 4), new Line(4, 3), new Line(4, 6), new Line(9, 6), new Line(4, 9), new Line(7, 9), new Line(7, 4), new Line(1, 7), new Line(3, 0)});
            this.pointList.get(3).setLocked();
            this.pointList.get(8).setLocked();
        }
        if (lev == 14) {
            addLevelData(getPoints(0, 11), new Line[]{new Line(10, 5), new Line(8, 6), new Line(10, 8), new Line(10, 3), new Line(3, 8), new Line(7, 10), new Line(3, 1), new Line(7, 1), new Line(7, 3), new Line(1, 8), new Line(7, 6), new Line(1, 6), new Line(5, 6), new Line(8, 5), new Line(5, 0), new Line(0, 6), new Line(0, 2), new Line(2, 4), new Line(5, 2), new Line(6, 4), new Line(0, 4), new Line(4, 9), new Line(6, 9), new Line(2, 9)});
            this.pointList.get(0).setLocked();
            this.pointList.get(3).setLocked();
            this.pointList.get(8).setLocked();
        }
        if (lev == 15) {
            addLevelData(getPoints(0, 11), new Line[]{new Line(0, 3), new Line(0, 8), new Line(0, 7), new Line(0, 10), new Line(3, 6), new Line(3, 10), new Line(1, 3), new Line(3, 8), new Line(1, 7), new Line(1, 8), new Line(1, 5), new Line(1, 9), new Line(2, 7), new Line(2, 9), new Line(2, 5), new Line(5, 9), new Line(7, 8), new Line(7, 10), new Line(4, 6), new Line(4, 10), new Line(6, 10), new Line(7, 9)});
        }
        if (lev == 16) {
            addLevelData(getPoints(0, 11), new Line[]{new Line(0, 5), new Line(0, 6), new Line(0, 8), new Line(0, 3), new Line(1, 8), new Line(1, 10), new Line(1, 4), new Line(1, 5), new Line(4, 9), new Line(4, 10), new Line(2, 4), new Line(2, 9), new Line(6, 9), new Line(6, 10), new Line(2, 6), new Line(2, 3), new Line(3, 10), new Line(3, 6), new Line(5, 7), new Line(5, 8), new Line(3, 7), new Line(3, 8), new Line(8, 10), new Line(9, 10)});
            this.pointList.get(10).setLocked();
        }
        if (lev == 17) {
            addLevelData(getPoints(4, 12), new Line[]{new Line(0, 1), new Line(0, 4), new Line(1, 4), new Line(2, 5), new Line(2, 7), new Line(3, 5), new Line(3, 6), new Line(3, 9), new Line(4, 5), new Line(5, 9), new Line(6, 9), new Line(7, 8), new Line(9, 0), new Line(11, 10), new Line(10, 4), new Line(11, 3), new Line(10, 6), new Line(8, 0)});
        }
        if (lev == 18) {
            addLevelData(getPoints(0, 12), new Line[]{new Line(0, 5), new Line(0, 3), new Line(0, 10), new Line(0, 11), new Line(3, 5), new Line(3, 11), new Line(5, 7), new Line(5, 10), new Line(2, 7), new Line(2, 8), new Line(1, 2), new Line(1, 4), new Line(1, 9), new Line(1, 10), new Line(1, 6), new Line(6, 10), new Line(2, 4), new Line(2, 9), new Line(7, 9), new Line(7, 10), new Line(6, 4), new Line(6, 8), new Line(10, 11), new Line(9, 10), new Line(4, 8)});
            this.pointList.get(4).setLocked();
        }
        if (lev == 19) {
            addLevelData(getPoints(0, 12), new Line[]{new Line(0, 4), new Line(0, 8), new Line(0, 6), new Line(0, 9), new Line(6, 9), new Line(6, 10), new Line(2, 5), new Line(2, 8), new Line(2, 6), new Line(5, 6), new Line(2, 4), new Line(4, 8), new Line(1, 5), new Line(1, 11), new Line(1, 9), new Line(1, 3), new Line(1, 10), new Line(5, 10), new Line(3, 9), new Line(3, 11), new Line(3, 7), new Line(7, 9), new Line(7, 11), new Line(6, 8), new Line(9, 10), new Line(5, 11)});
            this.pointList.get(1).setLocked();
            this.pointList.get(5).setLocked();
        }
        if (lev == 20) {
            addLevelData(getPoints(4, 13), new Line[]{new Line(0, 1), new Line(0, 4), new Line(1, 4), new Line(2, 5), new Line(2, 7), new Line(3, 5), new Line(3, 6), new Line(3, 9), new Line(4, 5), new Line(5, 9), new Line(6, 9), new Line(7, 8), new Line(9, 0), new Line(10, 9), new Line(10, 0), new Line(11, 7), new Line(11, 5), new Line(12, 0), new Line(12, 2), new Line(8, 11)});
        }
        if (lev == 21) {
            addLevelData(getPoints(0, 13), new Line[]{new Line(0, 3), new Line(0, 7), new Line(0, 9), new Line(3, 7), new Line(3, 10), new Line(1, 4), new Line(1, 7), new Line(4, 7), new Line(4, 8), new Line(4, 9), new Line(7, 9), new Line(2, 6), new Line(2, 11), new Line(2, 5), new Line(2, 12), new Line(6, 12), new Line(6, 11), new Line(1, 6), new Line(1, 10), new Line(1, 8), new Line(6, 8), new Line(5, 6), new Line(5, 11), new Line(5, 8), new Line(6, 10), new Line(10, 12), new Line(7, 10)});
            this.pointList.get(1).setLocked();
        }
        if (lev == 22) {
            addLevelData(getPoints(0, 13), new Line[]{new Line(0, 6), new Line(0, 7), new Line(0, 9), new Line(4, 8), new Line(4, 10), new Line(4, 5), new Line(4, 12), new Line(1, 6), new Line(1, 3), new Line(1, 5), new Line(1, 10), new Line(1, 11), new Line(1, 9), new Line(2, 3), new Line(2, 8), new Line(2, 11), new Line(3, 8), new Line(3, 11), new Line(3, 6), new Line(6, 9), new Line(7, 9), new Line(7, 12), new Line(5, 12), new Line(5, 9), new Line(9, 12), new Line(5, 10), new Line(8, 10), new Line(8, 11), new Line(10, 11)});
            this.pointList.get(5).setLocked();
            this.pointList.get(9).setLocked();
        }
        if (lev == 23) {
            addLevelData(getPoints(4, 14), new Line[]{new Line(0, 1), new Line(0, 4), new Line(1, 4), new Line(2, 5), new Line(2, 7), new Line(3, 5), new Line(3, 6), new Line(3, 9), new Line(4, 5), new Line(5, 9), new Line(6, 9), new Line(7, 8), new Line(9, 0), new Line(10, 5), new Line(10, 8), new Line(11, 9), new Line(11, 0), new Line(12, 0), new Line(12, 3), new Line(12, 5), new Line(13, 7), new Line(13, 4)});
        }
        if (lev == 24) {
            addLevelData(getPoints(0, 14), new Line[]{new Line(0, 6), new Line(0, 12), new Line(0, 4), new Line(0, 13), new Line(0, 3), new Line(0, 8), new Line(3, 6), new Line(3, 8), new Line(3, 10), new Line(6, 12), new Line(1, 9), new Line(1, 4), new Line(1, 7), new Line(4, 12), new Line(4, 9), new Line(9, 12), new Line(4, 13), new Line(4, 7), new Line(8, 13), new Line(8, 10), new Line(2, 13), new Line(2, 11), new Line(5, 7), new Line(5, 11), new Line(2, 10), new Line(2, 5), new Line(5, 10), new Line(11, 13), new Line(7, 11), new Line(7, 13), new Line(10, 13)});
            this.pointList.get(13).setLocked();
        }
        if (lev == 25) {
            addLevelData(getPoints(0, 14), new Line[]{new Line(0, 10), new Line(0, 8), new Line(0, 6), new Line(0, 7), new Line(1, 9), new Line(1, 13), new Line(1, 4), new Line(1, 11), new Line(3, 5), new Line(3, 12), new Line(3, 9), new Line(5, 9), new Line(2, 3), new Line(2, 12), new Line(2, 4), new Line(2, 5), new Line(2, 10), new Line(4, 10), new Line(4, 5), new Line(4, 9), new Line(4, 8), new Line(4, 11), new Line(6, 7), new Line(6, 13), new Line(7, 13), new Line(9, 12), new Line(6, 8), new Line(8, 11), new Line(11, 13)});
            this.pointList.get(11).setLocked();
            this.pointList.get(13).setLocked();
        }
        if (lev == 26) {
            addLevelData(getPoints(4, 15), new Line[]{new Line(0, 1), new Line(0, 4), new Line(1, 4), new Line(2, 5), new Line(2, 7), new Line(3, 5), new Line(3, 6), new Line(3, 9), new Line(5, 9), new Line(6, 9), new Line(7, 8), new Line(9, 0), new Line(10, 5), new Line(10, 8), new Line(11, 9), new Line(11, 0), new Line(12, 0), new Line(12, 3), new Line(12, 5), new Line(13, 7), new Line(13, 4), new Line(0, 14), new Line(3, 14)});
        }
        if (lev == 27) {
            addLevelData(getPoints(0, 15), new Line[]{new Line(0, 8), new Line(0, 13), new Line(0, 5), new Line(0, 10), new Line(1, 5), new Line(1, 13), new Line(1, 4), new Line(1, 11), new Line(1, 9), new Line(2, 3), new Line(2, 12), new Line(2, 9), new Line(2, 14), new Line(2, 4), new Line(2, 11), new Line(3, 11), new Line(3, 12), new Line(3, 8), new Line(6, 7), new Line(6, 12), new Line(4, 9), new Line(4, 11), new Line(7, 9), new Line(7, 14), new Line(8, 11), new Line(8, 13), new Line(5, 10), new Line(5, 13), new Line(8, 10), new Line(8, 12), new Line(11, 13), new Line(9, 14), new Line(12, 14)});
            this.pointList.get(13).setLocked();
        }
        if (lev == 28) {
            addLevelData(getPoints(1, 15), new Line[]{new Line(0, 7), new Line(0, 8), new Line(0, 4), new Line(0, 14), new Line(0, 11), new Line(0, 3), new Line(0, 13), new Line(2, 9), new Line(2, 10), new Line(1, 9), new Line(1, 10), new Line(1, 5), new Line(1, 7), new Line(1, 12), new Line(4, 12), new Line(4, 14), new Line(4, 6), new Line(4, 7), new Line(3, 5), new Line(3, 13), new Line(3, 6), new Line(3, 11), new Line(6, 14), new Line(6, 11), new Line(5, 7), new Line(5, 10), new Line(5, 8), new Line(5, 13), new Line(8, 13), new Line(7, 8), new Line(7, 12), new Line(9, 10), new Line(9, 12), new Line(2, 12), new Line(11, 14)});
            this.pointList.get(0).setLocked();
            this.pointList.get(7).setLocked();
        }
        if (lev == 29) {
            addLevelData(getPoints(4, 16), new Line[]{new Line(2, 8), new Line(2, 10), new Line(2, 13), new Line(5, 8), new Line(8, 9), new Line(5, 10), new Line(1, 5), new Line(1, 9), new Line(1, 11), new Line(6, 9), new Line(6, 11), new Line(0, 1), new Line(0, 11), new Line(7, 10), new Line(0, 7), new Line(0, 14), new Line(7, 13), new Line(7, 12), new Line(7, 14), new Line(3, 13), new Line(3, 12), new Line(12, 14), new Line(4, 6), new Line(4, 15), new Line(14, 15), new Line(3, 4), new Line(4, 14)});
        }
        if (lev == 30) {
            addLevelData(getPoints(3, 16), new Line[]{new Line(0, 8), new Line(0, 12), new Line(0, 3), new Line(0, 6), new Line(0, 10), new Line(3, 10), new Line(3, 6), new Line(6, 10), new Line(6, 12), new Line(6, 14), new Line(4, 6), new Line(4, 14), new Line(4, 9), new Line(4, 10), new Line(4, 7), new Line(7, 9), new Line(1, 9), new Line(9, 14), new Line(1, 7), new Line(1, 11), new Line(1, 8), new Line(1, 12), new Line(1, 14), new Line(1, 13), new Line(2, 13), new Line(2, 8), new Line(2, 5), new Line(2, 15), new Line(2, 11), new Line(5, 11), new Line(7, 11), new Line(7, 15), new Line(8, 13), new Line(8, 12), new Line(12, 14), new Line(11, 13), new Line(5, 15)});
            this.pointList.get(7).setLocked();
            this.pointList.get(15).setLocked();
        }
        if (lev == 31) {
            addLevelData(getPoints(3, 16), new Line[]{new Line(0, 8), new Line(0, 12), new Line(0, 3), new Line(0, 6), new Line(0, 10), new Line(3, 10), new Line(3, 6), new Line(6, 10), new Line(6, 12), new Line(6, 14), new Line(4, 6), new Line(4, 14), new Line(4, 9), new Line(4, 10), new Line(4, 7), new Line(7, 9), new Line(1, 9), new Line(9, 14), new Line(1, 7), new Line(1, 11), new Line(1, 8), new Line(1, 12), new Line(1, 14), new Line(1, 13), new Line(2, 13), new Line(2, 8), new Line(2, 5), new Line(2, 15), new Line(2, 11), new Line(5, 11), new Line(7, 11), new Line(7, 15), new Line(8, 13), new Line(8, 12), new Line(12, 14), new Line(11, 13), new Line(5, 15)});
            this.pointList.get(6).setLocked();
        }
        if (lev == 32) {
            addLevelData(getPoints(4, 17), new Line[]{new Line(0, 10), new Line(0, 5), new Line(0, 4), new Line(0, 12), new Line(7, 10), new Line(7, 12), new Line(2, 7), new Line(2, 12), new Line(2, 16), new Line(4, 12), new Line(4, 16), new Line(1, 4), new Line(1, 8), new Line(4, 8), new Line(4, 11), new Line(8, 11), new Line(6, 16), new Line(6, 11), new Line(3, 11), new Line(3, 6), new Line(6, 9), new Line(9, 14), new Line(14, 15), new Line(5, 15), new Line(5, 13), new Line(3, 13), new Line(3, 15)});
        }
        if (lev == 33) {
            addLevelData(getPoints(0, 17), new Line[]{new Line(0, 5), new Line(0, 13), new Line(0, 1), new Line(0, 10), new Line(0, 4), new Line(0, 16), new Line(1, 3), new Line(1, 13), new Line(1, 7), new Line(1, 11), new Line(1, 16), new Line(3, 7), new Line(3, 4), new Line(3, 16), new Line(2, 8), new Line(2, 12), new Line(2, 11), new Line(2, 6), new Line(2, 7), new Line(2, 14), new Line(8, 9), new Line(8, 12), new Line(9, 12), new Line(9, 14), new Line(3, 14), new Line(4, 14), new Line(4, 9), new Line(4, 16), new Line(7, 11), new Line(7, 14), new Line(6, 15), new Line(6, 13), new Line(5, 10), new Line(5, 15), new Line(5, 13), new Line(13, 15), new Line(11, 13), new Line(10, 15), new Line(6, 11)});
            this.pointList.get(13).setLocked();
        }
        if (lev == 34) {
            addLevelData(getPoints(3, 17), new Line[]{new Line(0, 5), new Line(0, 13), new Line(0, 1), new Line(0, 10), new Line(0, 4), new Line(0, 16), new Line(1, 3), new Line(1, 13), new Line(1, 7), new Line(1, 11), new Line(1, 16), new Line(3, 7), new Line(3, 4), new Line(3, 16), new Line(2, 8), new Line(2, 12), new Line(2, 11), new Line(2, 6), new Line(2, 7), new Line(2, 14), new Line(8, 9), new Line(8, 12), new Line(9, 12), new Line(9, 14), new Line(3, 14), new Line(4, 14), new Line(4, 9), new Line(4, 16), new Line(7, 11), new Line(7, 14), new Line(6, 15), new Line(6, 13), new Line(5, 10), new Line(5, 15), new Line(5, 13), new Line(13, 15), new Line(11, 13), new Line(10, 15), new Line(6, 11)});
            this.pointList.get(11).setLocked();
            this.pointList.get(7).setLocked();
        }
        if (lev == 35) {
            addLevelData(getPoints(4, 18), new Line[]{new Line(1, 7), new Line(1, 10), new Line(0, 1), new Line(0, 10), new Line(0, 11), new Line(5, 11), new Line(5, 10), new Line(10, 12), new Line(4, 10), new Line(4, 12), new Line(4, 7), new Line(7, 14), new Line(13, 14), new Line(7, 13), new Line(2, 13), new Line(12, 13), new Line(2, 6), new Line(2, 15), new Line(2, 16), new Line(6, 15), new Line(15, 16), new Line(3, 16), new Line(9, 16), new Line(16, 17), new Line(13, 16), new Line(12, 17), new Line(5, 12), new Line(5, 17), new Line(5, 9), new Line(9, 17), new Line(8, 9), new Line(3, 8), new Line(8, 11)});
        }
        if (lev == 36) {
            addLevelData(getPoints(3, 18), new Line[]{new Line(0, 6), new Line(0, 14), new Line(0, 11), new Line(0, 17), new Line(0, 9), new Line(1, 14), new Line(1, 15), new Line(1, 7), new Line(1, 10), new Line(1, 6), new Line(1, 13), new Line(2, 8), new Line(2, 7), new Line(2, 5), new Line(2, 15), new Line(5, 8), new Line(5, 12), new Line(5, 16), new Line(5, 17), new Line(5, 15), new Line(15, 17), new Line(7, 8), new Line(8, 12), new Line(12, 16), new Line(7, 15), new Line(11, 15), new Line(11, 15), new Line(6, 9), new Line(6, 13), new Line(3, 10), new Line(3, 4), new Line(3, 9), new Line(3, 13), new Line(4, 13), new Line(4, 9), new Line(3, 7), new Line(7, 10), new Line(9, 13), new Line(10, 13), new Line(11, 17), new Line(16, 17), new Line(6, 14), new Line(14, 15)});
            this.pointList.get(2).setLocked();
        }
        if (lev == 37) {
            addLevelData(getPoints(0, 18), new Line[]{new Line(0, 6), new Line(0, 14), new Line(0, 11), new Line(0, 17), new Line(0, 9), new Line(1, 14), new Line(1, 15), new Line(1, 7), new Line(1, 10), new Line(1, 6), new Line(1, 13), new Line(2, 8), new Line(2, 7), new Line(2, 5), new Line(2, 15), new Line(5, 8), new Line(5, 12), new Line(5, 16), new Line(5, 17), new Line(5, 15), new Line(15, 17), new Line(7, 8), new Line(8, 12), new Line(12, 16), new Line(7, 15), new Line(11, 15), new Line(11, 15), new Line(6, 9), new Line(6, 13), new Line(3, 10), new Line(3, 4), new Line(3, 9), new Line(3, 13), new Line(4, 13), new Line(4, 9), new Line(3, 7), new Line(7, 10), new Line(9, 13), new Line(10, 13), new Line(11, 17), new Line(16, 17), new Line(6, 14), new Line(14, 15)});
            this.pointList.get(6).setLocked();
            this.pointList.get(9).setLocked();
        }
        if (lev == 38) {
            addLevelData(getPoints(4, 19), new Line[]{new Line(7, 12), new Line(6, 7), new Line(6, 12), new Line(1, 6), new Line(6, 10), new Line(7, 14), new Line(10, 14), new Line(1, 10), new Line(1, 13), new Line(0, 1), new Line(1, 18), new Line(10, 13), new Line(0, 13), new Line(13, 14), new Line(0, 11), new Line(0, 14), new Line(11, 14), new Line(2, 11), new Line(0, 2), new Line(0, 18), new Line(2, 18), new Line(2, 17), new Line(2, 8), new Line(8, 17), new Line(5, 8), new Line(5, 17), new Line(3, 5), new Line(17, 18), new Line(3, 9), new Line(3, 18), new Line(9, 18), new Line(9, 16), new Line(3, 16), new Line(4, 16), new Line(3, 4), new Line(4, 5), new Line(5, 15), new Line(4, 15), new Line(15, 16), new Line(3, 17)});
        }
        if (lev == 39) {
            addLevelData(getPoints(0, 19), new Line[]{new Line(7, 12), new Line(6, 7), new Line(6, 12), new Line(1, 6), new Line(6, 10), new Line(7, 14), new Line(10, 14), new Line(1, 10), new Line(1, 13), new Line(0, 1), new Line(1, 18), new Line(10, 13), new Line(0, 13), new Line(13, 14), new Line(0, 11), new Line(0, 14), new Line(11, 14), new Line(2, 11), new Line(0, 2), new Line(0, 18), new Line(2, 18), new Line(2, 17), new Line(2, 8), new Line(8, 17), new Line(5, 8), new Line(5, 17), new Line(3, 5), new Line(17, 18), new Line(3, 9), new Line(3, 18), new Line(9, 18), new Line(9, 16), new Line(3, 16), new Line(4, 16), new Line(3, 4), new Line(4, 5), new Line(5, 15), new Line(4, 15), new Line(15, 16), new Line(3, 17)});
            this.pointList.get(10).setLocked();
        }
        if (lev == 40) {
            addLevelData(getPoints(0, 19), new Line[]{new Line(0, 5), new Line(0, 10), new Line(0, 3), new Line(0, 8), new Line(1, 7), new Line(1, 9), new Line(1, 4), new Line(1, 14), new Line(4, 9), new Line(4, 15), new Line(4, 17), new Line(4, 11), new Line(1, 18), new Line(9, 18), new Line(7, 12), new Line(7, 18), new Line(2, 15), new Line(2, 4), new Line(2, 6), new Line(2, 17), new Line(6, 17), new Line(6, 11), new Line(2, 16), new Line(2, 18), new Line(2, 13), new Line(6, 13), new Line(3, 13), new Line(3, 16), new Line(3, 8), new Line(8, 13), new Line(5, 10), new Line(5, 14), new Line(5, 12), new Line(12, 14), new Line(8, 10), new Line(10, 14), new Line(0, 12), new Line(7, 14), new Line(3, 12), new Line(12, 16), new Line(12, 18), new Line(13, 16), new Line(16, 18), new Line(15, 18), new Line(9, 15), new Line(11, 17)});
            this.pointList.get(12).setLocked();
            this.pointList.get(18).setLocked();
        }
        if (lev == 41) {
            addLevelData(getPoints(4, 19), new Line[]{new Line(0, 5), new Line(0, 10), new Line(0, 3), new Line(0, 8), new Line(1, 7), new Line(1, 9), new Line(1, 4), new Line(1, 14), new Line(4, 9), new Line(4, 15), new Line(4, 17), new Line(4, 11), new Line(1, 18), new Line(9, 18), new Line(7, 12), new Line(7, 18), new Line(2, 15), new Line(2, 4), new Line(2, 6), new Line(2, 17), new Line(6, 17), new Line(6, 11), new Line(2, 16), new Line(2, 18), new Line(2, 13), new Line(6, 13), new Line(3, 13), new Line(3, 16), new Line(3, 8), new Line(8, 13), new Line(5, 10), new Line(5, 14), new Line(5, 12), new Line(12, 14), new Line(8, 10), new Line(10, 14), new Line(0, 12), new Line(7, 14), new Line(3, 12), new Line(12, 16), new Line(12, 18), new Line(13, 16), new Line(16, 18), new Line(15, 18), new Line(9, 15), new Line(11, 17)});
            this.pointList.get(2).setLocked();
            this.pointList.get(6).setLocked();
        }
        if (lev == 42) {
            addLevelData(getPoints(4, 20), new Line[]{new Line(5, 17), new Line(5, 12), new Line(5, 19), new Line(2, 5), new Line(7, 12), new Line(7, 9), new Line(9, 12), new Line(9, 15), new Line(3, 9), new Line(3, 15), new Line(3, 8), new Line(3, 14), new Line(8, 15), new Line(8, 14), new Line(4, 14), new Line(4, 18), new Line(4, 11), new Line(11, 14), new Line(8, 11), new Line(8, 13), new Line(1, 12), new Line(1, 13), new Line(12, 13), new Line(2, 12), new Line(2, 13), new Line(1, 7), new Line(1, 8), new Line(7, 15), new Line(2, 6), new Line(2, 19), new Line(6, 13), new Line(6, 17), new Line(6, 19), new Line(0, 17), new Line(0, 16), new Line(0, 6), new Line(6, 16), new Line(6, 10), new Line(10, 16), new Line(10, 18), new Line(6, 18), new Line(6, 11), new Line(11, 18), new Line(11, 13), new Line(17, 19), new Line(1, 15)});
        }
        if (lev == 43) {
            addLevelData(getPoints(2, 20), new Line[]{new Line(5, 17), new Line(5, 12), new Line(5, 19), new Line(2, 5), new Line(7, 12), new Line(7, 9), new Line(9, 12), new Line(9, 15), new Line(3, 9), new Line(3, 15), new Line(3, 8), new Line(3, 14), new Line(8, 15), new Line(8, 14), new Line(4, 14), new Line(4, 18), new Line(4, 11), new Line(11, 14), new Line(8, 11), new Line(8, 13), new Line(1, 12), new Line(1, 13), new Line(12, 13), new Line(2, 12), new Line(2, 13), new Line(1, 7), new Line(1, 8), new Line(7, 15), new Line(2, 6), new Line(2, 19), new Line(6, 13), new Line(6, 17), new Line(6, 19), new Line(0, 17), new Line(0, 16), new Line(0, 6), new Line(6, 16), new Line(6, 10), new Line(10, 16), new Line(10, 18), new Line(6, 18), new Line(6, 11), new Line(11, 18), new Line(11, 13), new Line(17, 19), new Line(1, 15)});
            this.pointList.get(10).setLocked();
        }
        if (lev == 44) {
            addLevelData(getPoints(0, 20), new Line[]{new Line(1, 18), new Line(1, 4), new Line(1, 15), new Line(1, 16), new Line(1, 19), new Line(15, 19), new Line(4, 18), new Line(4, 7), new Line(4, 15), new Line(4, 10), new Line(2, 18), new Line(2, 11), new Line(2, 8), new Line(2, 14), new Line(2, 16), new Line(8, 16), new Line(6, 16), new Line(6, 8), new Line(6, 17), new Line(6, 19), new Line(0, 6), new Line(6, 12), new Line(0, 5), new Line(0, 9), new Line(0, 12), new Line(5, 9), new Line(5, 3), new Line(5, 12), new Line(5, 15), new Line(3, 9), new Line(3, 13), new Line(3, 10), new Line(3, 4), new Line(5, 10), new Line(5, 13), new Line(12, 15), new Line(12, 17), new Line(10, 13), new Line(10, 15), new Line(7, 11), new Line(7, 18), new Line(11, 18), new Line(11, 14), new Line(16, 18), new Line(15, 17), new Line(17, 19), new Line(16, 19), new Line(8, 14)});
            this.pointList.get(1).setLocked();
            this.pointList.get(4).setLocked();
        }
        if (lev == 45) {
            addLevelData(getPoints(4, 20), new Line[]{new Line(1, 18), new Line(1, 4), new Line(1, 15), new Line(1, 16), new Line(1, 19), new Line(15, 19), new Line(4, 18), new Line(4, 7), new Line(4, 15), new Line(4, 10), new Line(2, 18), new Line(2, 11), new Line(2, 8), new Line(2, 14), new Line(2, 16), new Line(8, 16), new Line(6, 16), new Line(6, 8), new Line(6, 17), new Line(6, 19), new Line(0, 6), new Line(6, 12), new Line(0, 5), new Line(0, 9), new Line(0, 12), new Line(5, 9), new Line(5, 3), new Line(5, 12), new Line(5, 15), new Line(3, 9), new Line(3, 13), new Line(3, 10), new Line(3, 4), new Line(5, 10), new Line(5, 13), new Line(12, 15), new Line(12, 17), new Line(10, 13), new Line(10, 15), new Line(7, 11), new Line(7, 18), new Line(11, 18), new Line(11, 14), new Line(16, 18), new Line(15, 17), new Line(17, 19), new Line(16, 19), new Line(8, 14)});
            this.pointList.get(0).setLocked();
            this.pointList.get(3).setLocked();
            this.pointList.get(5).setLocked();
        }
        if (lev == 46) {
            addLevelData(getPoints(3, 21), new Line[]{new Line(0, 2), new Line(0, 7), new Line(0, 8), new Line(0, 15), new Line(0, 17), new Line(1, 10), new Line(1, 13), new Line(1, 16), new Line(1, 16), new Line(6, 16), new Line(2, 6), new Line(2, 15), new Line(2, 7), new Line(2, 11), new Line(2, 12), new Line(7, 12), new Line(3, 9), new Line(3, 20), new Line(3, 11), new Line(3, 7), new Line(3, 12), new Line(11, 12), new Line(7, 8), new Line(7, 20), new Line(8, 19), new Line(8, 20), new Line(9, 19), new Line(9, 20), new Line(4, 9), new Line(4, 19), new Line(6, 11), new Line(6, 15), new Line(6, 17), new Line(5, 10), new Line(5, 18), new Line(5, 14), new Line(5, 17), new Line(10, 18), new Line(10, 13), new Line(13, 15), new Line(16, 17), new Line(15, 17), new Line(13, 17), new Line(10, 17), new Line(14, 17), new Line(4, 14), new Line(4, 17), new Line(4, 18), new Line(14, 18), new Line(17, 19), new Line(8, 17), new Line(19, 20)});
        }
        if (lev == 47) {
            addLevelData(getPoints(4, 21), new Line[]{new Line(0, 2), new Line(0, 7), new Line(0, 8), new Line(0, 15), new Line(0, 17), new Line(1, 10), new Line(1, 13), new Line(1, 16), new Line(1, 16), new Line(6, 16), new Line(2, 6), new Line(2, 15), new Line(2, 7), new Line(2, 11), new Line(2, 12), new Line(7, 12), new Line(3, 9), new Line(3, 20), new Line(3, 11), new Line(3, 7), new Line(3, 12), new Line(11, 12), new Line(7, 8), new Line(7, 20), new Line(8, 19), new Line(8, 20), new Line(9, 19), new Line(9, 20), new Line(4, 9), new Line(4, 19), new Line(6, 11), new Line(6, 15), new Line(6, 17), new Line(5, 10), new Line(5, 18), new Line(5, 14), new Line(5, 17), new Line(10, 18), new Line(10, 13), new Line(13, 15), new Line(16, 17), new Line(15, 17), new Line(13, 17), new Line(10, 17), new Line(14, 17), new Line(4, 14), new Line(4, 17), new Line(4, 18), new Line(14, 18), new Line(17, 19), new Line(8, 17), new Line(19, 20)});
            this.pointList.get(20).setLocked();
        }
        if (lev == 48) {
            addLevelData(getPoints(0, 21), new Line[]{new Line(0, 11), new Line(0, 5), new Line(0, 4), new Line(0, 7), new Line(0, 17), new Line(0, 19), new Line(4, 10), new Line(4, 19), new Line(4, 7), new Line(4, 11), new Line(1, 4), new Line(1, 20), new Line(1, 10), new Line(1, 8), new Line(1, 16), new Line(1, 12), new Line(1, 2), new Line(1, 13), new Line(1, 9), new Line(1, 17), new Line(1, 19), new Line(17, 19), new Line(5, 9), new Line(5, 13), new Line(5, 17), new Line(5, 18), new Line(5, 2), new Line(5, 15), new Line(2, 14), new Line(2, 15), new Line(2, 12), new Line(2, 13), new Line(3, 16), new Line(3, 6), new Line(3, 14), new Line(3, 15), new Line(3, 18), new Line(14, 15), new Line(6, 14), new Line(6, 12), new Line(6, 16), new Line(12, 16), new Line(8, 10), new Line(8, 16), new Line(4, 20), new Line(10, 20), new Line(15, 18), new Line(9, 17), new Line(9, 13)});
            this.pointList.get(6).setLocked();
            this.pointList.get(16).setLocked();
        }
        if (lev == 49) {
            addLevelData(getPoints(4, 21), new Line[]{new Line(0, 11), new Line(0, 5), new Line(0, 4), new Line(0, 7), new Line(0, 17), new Line(0, 19), new Line(4, 10), new Line(4, 19), new Line(4, 7), new Line(4, 11), new Line(1, 4), new Line(1, 20), new Line(1, 10), new Line(1, 8), new Line(1, 16), new Line(1, 12), new Line(1, 2), new Line(1, 13), new Line(1, 9), new Line(1, 17), new Line(1, 19), new Line(17, 19), new Line(5, 9), new Line(5, 13), new Line(5, 17), new Line(5, 18), new Line(5, 2), new Line(5, 15), new Line(2, 14), new Line(2, 15), new Line(2, 12), new Line(2, 13), new Line(3, 16), new Line(3, 6), new Line(3, 14), new Line(3, 15), new Line(3, 18), new Line(14, 15), new Line(6, 14), new Line(6, 12), new Line(6, 16), new Line(12, 16), new Line(8, 10), new Line(8, 16), new Line(4, 20), new Line(10, 20), new Line(15, 18), new Line(9, 17), new Line(9, 13)});
            this.pointList.get(1).setLocked();
            this.pointList.get(2).setLocked();
            this.pointList.get(4).setLocked();
        }
        if (lev == 50) {
            addLevelData(getPoints(4, 22), new Line[]{new Line(0, 4), new Line(0, 16), new Line(0, 11), new Line(0, 18), new Line(0, 19), new Line(16, 19), new Line(1, 5), new Line(1, 16), new Line(1, 10), new Line(1, 6), new Line(1, 17), new Line(1, 19), new Line(2, 14), new Line(2, 17), new Line(2, 3), new Line(2, 13), new Line(2, 9), new Line(2, 21), new Line(3, 8), new Line(3, 13), new Line(3, 20), new Line(3, 21), new Line(4, 11), new Line(4, 18), new Line(4, 5), new Line(4, 16), new Line(5, 6), new Line(5, 16), new Line(5, 10), new Line(6, 10), new Line(6, 15), new Line(1, 15), new Line(7, 14), new Line(7, 15), new Line(7, 17), new Line(14, 17), new Line(8, 13), new Line(8, 17), new Line(13, 17), new Line(8, 12), new Line(8, 17), new Line(8, 20), new Line(12, 18), new Line(12, 19), new Line(12, 20), new Line(11, 18), new Line(9, 18), new Line(9, 20), new Line(9, 21), new Line(20, 21), new Line(18, 20), new Line(17, 19), new Line(15, 17), new Line(18, 19), new Line(14, 15)});
        }
        if (lev == 51) {
            addLevelData(getPoints(3, 22), new Line[]{new Line(0, 4), new Line(0, 16), new Line(0, 11), new Line(0, 18), new Line(0, 19), new Line(16, 19), new Line(1, 5), new Line(1, 16), new Line(1, 10), new Line(1, 6), new Line(1, 17), new Line(1, 19), new Line(2, 14), new Line(2, 17), new Line(2, 3), new Line(2, 13), new Line(2, 9), new Line(2, 21), new Line(3, 8), new Line(3, 13), new Line(3, 20), new Line(3, 21), new Line(4, 11), new Line(4, 18), new Line(4, 5), new Line(4, 16), new Line(5, 6), new Line(5, 16), new Line(5, 10), new Line(6, 10), new Line(6, 15), new Line(1, 15), new Line(7, 14), new Line(7, 15), new Line(7, 17), new Line(14, 17), new Line(8, 13), new Line(8, 17), new Line(13, 17), new Line(8, 12), new Line(8, 17), new Line(8, 20), new Line(12, 18), new Line(12, 19), new Line(12, 20), new Line(11, 18), new Line(9, 18), new Line(9, 20), new Line(9, 21), new Line(20, 21), new Line(18, 20), new Line(17, 19), new Line(15, 17), new Line(18, 19), new Line(14, 15)});
        }
        if (lev == 52) {
            addLevelData(getPoints(0, 22), new Line[]{new Line(0, 4), new Line(0, 16), new Line(0, 11), new Line(0, 18), new Line(0, 19), new Line(16, 19), new Line(1, 5), new Line(1, 16), new Line(1, 10), new Line(1, 6), new Line(1, 17), new Line(1, 19), new Line(2, 14), new Line(2, 17), new Line(2, 3), new Line(2, 13), new Line(2, 9), new Line(2, 21), new Line(3, 8), new Line(3, 13), new Line(3, 20), new Line(3, 21), new Line(4, 11), new Line(4, 18), new Line(4, 5), new Line(4, 16), new Line(5, 6), new Line(5, 16), new Line(5, 10), new Line(6, 10), new Line(6, 15), new Line(1, 15), new Line(7, 14), new Line(7, 15), new Line(7, 17), new Line(14, 17), new Line(8, 13), new Line(8, 17), new Line(13, 17), new Line(8, 12), new Line(8, 17), new Line(8, 20), new Line(12, 18), new Line(12, 19), new Line(12, 20), new Line(11, 18), new Line(9, 18), new Line(9, 20), new Line(9, 21), new Line(20, 21), new Line(18, 20), new Line(17, 19), new Line(15, 17), new Line(18, 19), new Line(14, 15)});
            this.pointList.get(11).setLocked();
        }
        if (lev == 53) {
            addLevelData(getPoints(0, 22), new Line[]{new Line(0, 8), new Line(0, 20), new Line(0, 18), new Line(0, 19), new Line(0, 5), new Line(0, 21), new Line(2, 8), new Line(2, 20), new Line(4, 9), new Line(4, 12), new Line(4, 6), new Line(4, 15), new Line(9, 12), new Line(9, 12), new Line(9, 10), new Line(9, 14), new Line(9, 15), new Line(1, 11), new Line(1, 16), new Line(1, 3), new Line(1, 15), new Line(1, 6), new Line(6, 15), new Line(6, 11), new Line(6, 12), new Line(2, 12), new Line(5, 7), new Line(5, 17), new Line(5, 14), new Line(5, 18), new Line(5, 13), new Line(5, 21), new Line(2, 19), new Line(8, 19), new Line(3, 16), new Line(3, 7), new Line(3, 10), new Line(3, 17), new Line(3, 15), new Line(10, 15), new Line(10, 13), new Line(10, 17), new Line(10, 14), new Line(14, 20), new Line(12, 20), new Line(18, 20), new Line(19, 21), new Line(8, 20), new Line(11, 16), new Line(7, 16), new Line(7, 17), new Line(13, 17), new Line(13, 14), new Line(14, 18)});
            this.pointList.get(15).setLocked();
        }
        if (lev == 54) {
            addLevelData(getPoints(4, 22), new Line[]{new Line(0, 8), new Line(0, 20), new Line(0, 18), new Line(0, 19), new Line(0, 5), new Line(0, 21), new Line(2, 8), new Line(2, 20), new Line(4, 9), new Line(4, 12), new Line(4, 6), new Line(4, 15), new Line(9, 12), new Line(9, 12), new Line(9, 10), new Line(9, 14), new Line(9, 15), new Line(1, 11), new Line(1, 16), new Line(1, 3), new Line(1, 15), new Line(1, 6), new Line(6, 15), new Line(6, 11), new Line(6, 12), new Line(2, 12), new Line(5, 7), new Line(5, 17), new Line(5, 14), new Line(5, 18), new Line(5, 13), new Line(5, 21), new Line(2, 19), new Line(8, 19), new Line(3, 16), new Line(3, 7), new Line(3, 10), new Line(3, 17), new Line(3, 15), new Line(10, 15), new Line(10, 13), new Line(10, 17), new Line(10, 14), new Line(14, 20), new Line(12, 20), new Line(18, 20), new Line(19, 21), new Line(8, 20), new Line(11, 16), new Line(7, 16), new Line(7, 17), new Line(13, 17), new Line(13, 14), new Line(14, 18)});
            this.pointList.get(5).setLocked();
        }
        if (lev == 55) {
            addLevelData(getPoints(3, 22), new Line[]{new Line(0, 8), new Line(0, 20), new Line(0, 18), new Line(0, 19), new Line(0, 5), new Line(0, 21), new Line(2, 8), new Line(2, 20), new Line(4, 9), new Line(4, 12), new Line(4, 6), new Line(4, 15), new Line(9, 12), new Line(9, 12), new Line(9, 10), new Line(9, 14), new Line(9, 15), new Line(1, 11), new Line(1, 16), new Line(1, 3), new Line(1, 15), new Line(1, 6), new Line(6, 15), new Line(6, 11), new Line(6, 12), new Line(2, 12), new Line(5, 7), new Line(5, 17), new Line(5, 14), new Line(5, 18), new Line(5, 13), new Line(5, 21), new Line(2, 19), new Line(8, 19), new Line(3, 16), new Line(3, 7), new Line(3, 10), new Line(3, 17), new Line(3, 15), new Line(10, 15), new Line(10, 13), new Line(10, 17), new Line(10, 14), new Line(14, 20), new Line(12, 20), new Line(18, 20), new Line(19, 21), new Line(8, 20), new Line(11, 16), new Line(7, 16), new Line(7, 17), new Line(13, 17), new Line(13, 14), new Line(14, 18)});
            this.pointList.get(1).setLocked();
            this.pointList.get(6).setLocked();
        }
        if (lev == 56) {
            addLevelData(getPoints(3, 23), new Line[]{new Line(0, 9), new Line(0, 5), new Line(0, 12), new Line(0, 17), new Line(0, 19), new Line(5, 9), new Line(9, 12), new Line(9, 17), new Line(9, 22), new Line(3, 9), new Line(3, 20), new Line(3, 22), new Line(3, 7), new Line(20, 22), new Line(7, 20), new Line(7, 21), new Line(7, 10), new Line(7, 11), new Line(2, 13), new Line(2, 11), new Line(2, 10), new Line(1, 2), new Line(1, 11), new Line(1, 13), new Line(1, 6), new Line(1, 14), new Line(10, 11), new Line(11, 13), new Line(6, 11), new Line(6, 14), new Line(6, 7), new Line(4, 5), new Line(4, 19), new Line(4, 17), new Line(4, 22), new Line(4, 18), new Line(4, 15), new Line(4, 8), new Line(5, 19), new Line(5, 8), new Line(8, 14), new Line(8, 15), new Line(14, 15), new Line(14, 21), new Line(16, 20), new Line(16, 21), new Line(16, 22), new Line(16, 18), new Line(18, 22), new Line(17, 22), new Line(12, 17), new Line(17, 19), new Line(15, 18), new Line(18, 21), new Line(15, 21), new Line(20, 21)});
        }
        if (lev == 57) {
            addLevelData(getPoints(4, 23), new Line[]{new Line(0, 9), new Line(0, 5), new Line(0, 12), new Line(0, 17), new Line(0, 19), new Line(5, 9), new Line(9, 12), new Line(9, 17), new Line(9, 22), new Line(3, 9), new Line(3, 20), new Line(3, 22), new Line(3, 7), new Line(20, 22), new Line(7, 20), new Line(7, 21), new Line(7, 10), new Line(7, 11), new Line(2, 13), new Line(2, 11), new Line(2, 10), new Line(1, 2), new Line(1, 11), new Line(1, 13), new Line(1, 6), new Line(1, 14), new Line(10, 11), new Line(11, 13), new Line(6, 11), new Line(6, 14), new Line(6, 7), new Line(4, 5), new Line(4, 19), new Line(4, 17), new Line(4, 22), new Line(4, 18), new Line(4, 15), new Line(4, 8), new Line(5, 19), new Line(5, 8), new Line(8, 14), new Line(8, 15), new Line(14, 15), new Line(14, 21), new Line(16, 20), new Line(16, 21), new Line(16, 22), new Line(16, 18), new Line(18, 22), new Line(17, 22), new Line(12, 17), new Line(17, 19), new Line(15, 18), new Line(18, 21), new Line(15, 21), new Line(20, 21)});
        }
        if (lev == 58) {
            addLevelData(getPoints(3, 23), new Line[]{new Line(0, 9), new Line(0, 10), new Line(0, 6), new Line(0, 14), new Line(0, 15), new Line(0, 21), new Line(0, 22), new Line(0, 19), new Line(1, 5), new Line(1, 12), new Line(1, 9), new Line(1, 10), new Line(5, 13), new Line(5, 14), new Line(5, 10), new Line(5, 12), new Line(6, 13), new Line(6, 14), new Line(6, 19), new Line(6, 20), new Line(4, 6), new Line(6, 11), new Line(4, 11), new Line(4, 8), new Line(4, 16), new Line(3, 4), new Line(3, 8), new Line(3, 16), new Line(3, 11), new Line(3, 20), new Line(3, 19), new Line(3, 22), new Line(8, 16), new Line(8, 22), new Line(8, 17), new Line(11, 20), new Line(19, 20), new Line(19, 22), new Line(2, 15), new Line(2, 21), new Line(2, 17), new Line(2, 18), new Line(15, 17), new Line(17, 22), new Line(15, 22), new Line(15, 21), new Line(17, 18), new Line(18, 21), new Line(7, 18), new Line(7, 9), new Line(7, 12), new Line(9, 12), new Line(7, 21), new Line(9, 10), new Line(13, 14), new Line(10, 14), new Line(12, 18), new Line(9, 21)});
            this.pointList.get(20).setLocked();
        }
        if (lev == 59) {
            addLevelData(getPoints(4, 23), new Line[]{new Line(0, 9), new Line(0, 10), new Line(0, 6), new Line(0, 14), new Line(0, 15), new Line(0, 21), new Line(0, 22), new Line(0, 19), new Line(1, 5), new Line(1, 12), new Line(1, 9), new Line(1, 10), new Line(5, 13), new Line(5, 14), new Line(5, 10), new Line(5, 12), new Line(6, 13), new Line(6, 14), new Line(6, 19), new Line(6, 20), new Line(4, 6), new Line(6, 11), new Line(4, 11), new Line(4, 8), new Line(4, 16), new Line(3, 4), new Line(3, 8), new Line(3, 16), new Line(3, 11), new Line(3, 20), new Line(3, 19), new Line(3, 22), new Line(8, 16), new Line(8, 22), new Line(8, 17), new Line(11, 20), new Line(19, 20), new Line(19, 22), new Line(2, 15), new Line(2, 21), new Line(2, 17), new Line(2, 18), new Line(15, 17), new Line(17, 22), new Line(15, 22), new Line(15, 21), new Line(17, 18), new Line(18, 21), new Line(7, 18), new Line(7, 9), new Line(7, 12), new Line(9, 12), new Line(7, 21), new Line(9, 10), new Line(13, 14), new Line(10, 14), new Line(12, 18), new Line(9, 21)});
            this.pointList.get(2).setLocked();
        }
        if (lev == 60) {
            addLevelData(getPoints(0, 23), new Line[]{new Line(0, 14), new Line(0, 1), new Line(0, 2), new Line(0, 3), new Line(1, 14), new Line(1, 9), new Line(1, 2), new Line(1, 18), new Line(9, 14), new Line(9, 18), new Line(9, 19), new Line(6, 15), new Line(6, 19), new Line(6, 18), new Line(6, 10), new Line(6, 5), new Line(6, 20), new Line(5, 15), new Line(5, 11), new Line(11, 15), new Line(11, 21), new Line(5, 8), new Line(5, 20), new Line(4, 20), new Line(4, 16), new Line(4, 22), new Line(4, 13), new Line(4, 12), new Line(4, 17), new Line(4, 10), new Line(2, 3), new Line(2, 18), new Line(3, 7), new Line(3, 17), new Line(7, 12), new Line(7, 18), new Line(7, 17), new Line(12, 17), new Line(13, 17), new Line(13, 22), new Line(8, 11), new Line(8, 21), new Line(8, 16), new Line(8, 22), new Line(8, 20), new Line(16, 20), new Line(10, 12), new Line(10, 18), new Line(12, 18), new Line(18, 19), new Line(15, 19), new Line(16, 22), new Line(10, 20), new Line(21, 22)});
            this.pointList.get(14).setLocked();
        }
        if (lev == 61) {
            addLevelData(getPoints(4, 23), new Line[]{new Line(0, 14), new Line(0, 1), new Line(0, 2), new Line(0, 3), new Line(1, 14), new Line(1, 9), new Line(1, 2), new Line(1, 18), new Line(9, 14), new Line(9, 18), new Line(9, 19), new Line(6, 15), new Line(6, 19), new Line(6, 18), new Line(6, 10), new Line(6, 5), new Line(6, 20), new Line(5, 15), new Line(5, 11), new Line(11, 15), new Line(11, 21), new Line(5, 8), new Line(5, 20), new Line(4, 20), new Line(4, 16), new Line(4, 22), new Line(4, 13), new Line(4, 12), new Line(4, 17), new Line(4, 10), new Line(2, 3), new Line(2, 18), new Line(3, 7), new Line(3, 17), new Line(7, 12), new Line(7, 18), new Line(7, 17), new Line(12, 17), new Line(13, 17), new Line(13, 22), new Line(8, 11), new Line(8, 21), new Line(8, 16), new Line(8, 22), new Line(8, 20), new Line(16, 20), new Line(10, 12), new Line(10, 18), new Line(12, 18), new Line(18, 19), new Line(15, 19), new Line(16, 22), new Line(10, 20), new Line(21, 22)});
            this.pointList.get(1).setLocked();
            this.pointList.get(2).setLocked();
        }
        if (lev == 62) {
            addLevelData(getPoints(4, 24), new Line[]{new Line(0, 18), new Line(0, 13), new Line(0, 21), new Line(0, 13), new Line(0, 7), new Line(0, 4), new Line(0, 9), new Line(1, 5), new Line(1, 6), new Line(1, 8), new Line(1, 21), new Line(13, 21), new Line(1, 13), new Line(13, 18), new Line(1, 18), new Line(1, 20), new Line(2, 22), new Line(2, 19), new Line(2, 16), new Line(2, 23), new Line(2, 17), new Line(3, 4), new Line(3, 12), new Line(3, 7), new Line(3, 15), new Line(4, 7), new Line(4, 12), new Line(4, 9), new Line(5, 6), new Line(5, 14), new Line(5, 20), new Line(6, 8), new Line(6, 14), new Line(6, 16), new Line(6, 23), new Line(7, 15), new Line(7, 10), new Line(8, 21), new Line(8, 19), new Line(8, 16), new Line(9, 18), new Line(9, 12), new Line(19, 21), new Line(16, 19), new Line(16, 23), new Line(11, 17), new Line(11, 23), new Line(11, 14), new Line(11, 20), new Line(17, 20), new Line(17, 22), new Line(10, 19), new Line(10, 15), new Line(12, 15), new Line(10, 22), new Line(15, 22), new Line(19, 22), new Line(7, 19), new Line(14, 23), new Line(17, 23)});
        }
        if (lev == 63) {
            addLevelData(getPoints(0, 24), new Line[]{new Line(0, 18), new Line(0, 13), new Line(0, 21), new Line(0, 13), new Line(0, 7), new Line(0, 4), new Line(0, 9), new Line(1, 5), new Line(1, 6), new Line(1, 8), new Line(1, 21), new Line(13, 21), new Line(1, 13), new Line(13, 18), new Line(1, 18), new Line(1, 20), new Line(2, 22), new Line(2, 19), new Line(2, 16), new Line(2, 23), new Line(2, 17), new Line(3, 4), new Line(3, 12), new Line(3, 7), new Line(3, 15), new Line(4, 7), new Line(4, 12), new Line(4, 9), new Line(5, 6), new Line(5, 14), new Line(5, 20), new Line(6, 8), new Line(6, 14), new Line(6, 16), new Line(6, 23), new Line(7, 15), new Line(7, 10), new Line(8, 21), new Line(8, 19), new Line(8, 16), new Line(9, 18), new Line(9, 12), new Line(19, 21), new Line(16, 19), new Line(16, 23), new Line(11, 17), new Line(11, 23), new Line(11, 14), new Line(11, 20), new Line(17, 20), new Line(17, 22), new Line(10, 19), new Line(10, 15), new Line(12, 15), new Line(10, 22), new Line(15, 22), new Line(19, 22), new Line(7, 19), new Line(14, 23), new Line(17, 23)});
            this.pointList.get(19).setLocked();
        }
        if (lev == 64) {
            addLevelData(getPoints(0, 24), new Line[]{new Line(0, 8), new Line(0, 10), new Line(0, 7), new Line(0, 23), new Line(3, 7), new Line(3, 23), new Line(7, 19), new Line(7, 15), new Line(1, 17), new Line(1, 9), new Line(1, 21), new Line(1, 23), new Line(2, 9), new Line(2, 14), new Line(2, 17), new Line(2, 8), new Line(8, 17), new Line(8, 19), new Line(8, 10), new Line(10, 17), new Line(9, 14), new Line(9, 21), new Line(4, 5), new Line(4, 11), new Line(4, 23), new Line(4, 21), new Line(4, 13), new Line(4, 18), new Line(3, 15), new Line(3, 13), new Line(3, 11), new Line(11, 15), new Line(5, 6), new Line(5, 21), new Line(6, 18), new Line(6, 20), new Line(7, 23), new Line(11, 13), new Line(11, 18), new Line(6, 21), new Line(6, 12), new Line(11, 22), new Line(16, 20), new Line(16, 22), new Line(10, 23), new Line(0, 19), new Line(15, 19), new Line(17, 23), new Line(9, 17), new Line(14, 21), new Line(12, 14), new Line(12, 20), new Line(5, 18), new Line(21, 23), new Line(16, 18), new Line(18, 20), new Line(18, 22), new Line(13, 23), new Line(12, 21)});
        }
        if (lev == 65) {
            addLevelData(getPoints(4, 24), new Line[]{new Line(0, 8), new Line(0, 10), new Line(0, 7), new Line(0, 23), new Line(3, 7), new Line(3, 23), new Line(7, 19), new Line(7, 15), new Line(1, 17), new Line(1, 9), new Line(1, 21), new Line(1, 23), new Line(2, 9), new Line(2, 14), new Line(2, 17), new Line(2, 8), new Line(8, 17), new Line(8, 19), new Line(8, 10), new Line(10, 17), new Line(9, 14), new Line(9, 21), new Line(4, 5), new Line(4, 11), new Line(4, 23), new Line(4, 21), new Line(4, 13), new Line(4, 18), new Line(3, 15), new Line(3, 13), new Line(3, 11), new Line(11, 15), new Line(5, 6), new Line(5, 21), new Line(6, 18), new Line(6, 20), new Line(7, 23), new Line(11, 13), new Line(11, 18), new Line(6, 21), new Line(6, 12), new Line(11, 22), new Line(16, 20), new Line(16, 22), new Line(10, 23), new Line(0, 19), new Line(15, 19), new Line(17, 23), new Line(9, 17), new Line(14, 21), new Line(12, 14), new Line(12, 20), new Line(5, 18), new Line(21, 23), new Line(16, 18), new Line(18, 20), new Line(18, 22), new Line(13, 23), new Line(12, 21)});
            this.pointList.get(22).setLocked();
        }
        if (lev == 66) {
            addLevelData(getPoints(0, 24), Levels2.getInstance().getLinesForLevel(lev));
            this.pointList.get(14).setLocked();
        }
        if (lev == 67) {
            addLevelData(getPoints(4, 24), Levels2.getInstance().getLinesForLevel(lev - 1));
            this.pointList.get(0).setLocked();
            this.pointList.get(6).setLocked();
        }
        if (lev == 68) {
            addLevelData(getPoints(4, 25), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 69) {
            addLevelData(getPoints(0, 25), Levels2.getInstance().getLinesForLevel(lev - 1));
        }
        if (lev == 70) {
            addLevelData(getPoints(3, 25), Levels2.getInstance().getLinesForLevel(lev - 2));
            this.pointList.get(10).setLocked();
        }
        if (lev == 71) {
            addLevelData(getPoints(4, 25), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 72) {
            addLevelData(getPoints(3, 25), Levels2.getInstance().getLinesForLevel(lev - 1));
            this.pointList.get(15).setLocked();
        }
        if (lev == 73) {
            addLevelData(getPoints(0, 25), Levels2.getInstance().getLinesForLevel(lev - 2));
            this.pointList.get(6).setLocked();
            this.pointList.get(9).setLocked();
        }
        if (lev == 74 || lev == 75) {
            addLevelData(getPoints(0, 26), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 76 || lev == 77) {
            addLevelData(getPoints(4, 26), Levels2.getInstance().getLinesForLevel(lev - 2));
        }
        if (lev == 78 || lev == 79) {
            addLevelData(getPoints(3, 26), Levels2.getInstance().getLinesForLevel(lev - 4));
            this.pointList.get(10).setLocked();
        }
        if (lev == 80) {
            addLevelData(getPoints(4, 26), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 81) {
            addLevelData(getPoints(3, 26), Levels2.getInstance().getLinesForLevel(lev - 1));
            this.pointList.get(0).setLocked();
            this.pointList.get(3).setLocked();
        }
        if (lev == 82) {
            addLevelData(getPoints(0, 26), Levels2.getInstance().getLinesForLevel(lev - 2));
            this.pointList.get(5).setLocked();
        }
        if (lev == 83 || lev == 84) {
            addLevelData(getPoints(0, 27), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 85 || lev == 86) {
            addLevelData(getPoints(3, 27), Levels2.getInstance().getLinesForLevel(lev - 2));
        }
        if (lev == 87 || lev == 88) {
            addLevelData(getPoints(1, 27), Levels2.getInstance().getLinesForLevel(lev - 4));
            this.pointList.get(23).setLocked();
        }
        if (lev == 89) {
            addLevelData(getPoints(1, 27), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 90) {
            addLevelData(getPoints(3, 27), Levels2.getInstance().getLinesForLevel(lev - 1));
            this.pointList.get(3).setLocked();
        }
        if (lev == 91) {
            addLevelData(getPoints(4, 27), Levels2.getInstance().getLinesForLevel(lev - 2));
            this.pointList.get(2).setLocked();
            this.pointList.get(5).setLocked();
        }
        if (lev == 92 || lev == 93) {
            addLevelData(getPoints(3, 28), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 94 || lev == 95) {
            addLevelData(getPoints(4, 28), Levels2.getInstance().getLinesForLevel(lev - 2));
            this.pointList.get(13).setLocked();
        }
        if (lev == 96) {
            addLevelData(getPoints(4, 28), Levels2.getInstance().getLinesForLevel(lev));
            this.pointList.get(10).setLocked();
        }
        if (lev == 97) {
            addLevelData(getPoints(0, 28), Levels2.getInstance().getLinesForLevel(lev - 1));
            this.pointList.get(1).setLocked();
            this.pointList.get(8).setLocked();
        }
        if (lev == 98 || lev == 99) {
            addLevelData(getPoints(0, 29), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 100 || lev == 101) {
            addLevelData(getPoints(4, 29), Levels2.getInstance().getLinesForLevel(lev - 2));
        }
        if (lev == 102 || lev == 103) {
            addLevelData(getPoints(3, 29), Levels2.getInstance().getLinesForLevel(lev - 4));
            this.pointList.get((110 - lev) * 2).setLocked();
        }
        if (lev == 104) {
            addLevelData(getPoints(3, 29), Levels2.getInstance().getLinesForLevel(lev));
            this.pointList.get(10).setLocked();
        }
        if (lev == 105) {
            addLevelData(getPoints(4, 29), Levels2.getInstance().getLinesForLevel(lev - 1));
            this.pointList.get(0).setLocked();
            this.pointList.get(8).setLocked();
        }
        if (lev == 106 || lev == 107) {
            addLevelData(getPoints(3, 30), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 108 || lev == 109) {
            addLevelData(getPoints(4, 30), Levels2.getInstance().getLinesForLevel(lev - 2));
            this.pointList.get(4).setLocked();
        }
        if (lev == 110) {
            addLevelData(getPoints(2, 30), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 111) {
            addLevelData(getPoints(4, 30), Levels2.getInstance().getLinesForLevel(lev - 1));
            this.pointList.get(5).setLocked();
            this.pointList.get(0).setLocked();
        }
        if (lev == 112 || lev == 113) {
            addLevelData(getPoints(4, 31), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 114 || lev == 115) {
            addLevelData(getPoints(3, 31), Levels2.getInstance().getLinesForLevel(lev - 2));
            if (lev == 114) {
                this.pointList.get(5).setLocked();
            }
            if (lev == 115) {
                this.pointList.get(15).setLocked();
            }
        }
        if (lev == 116) {
            addLevelData(getPoints(3, 31), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 117) {
            addLevelData(getPoints(2, 31), Levels2.getInstance().getLinesForLevel(lev - 1));
            this.pointList.get(0).setLocked();
            this.pointList.get(7).setLocked();
        }
        if (lev == 118 || lev == 119) {
            addLevelData(getPoints(2, 32), Levels2.getInstance().getLinesForLevel(lev));
            this.pointList.get(23).setLocked();
        }
        if (lev == 120 || lev == 121) {
            addLevelData(getPoints(4, 32), Levels2.getInstance().getLinesForLevel(lev - 2));
            this.pointList.get(13).setLocked();
        }
        if (lev == 122) {
            addLevelData(getPoints(4, 32), Levels2.getInstance().getLinesForLevel(lev));
            this.pointList.get(4).setLocked();
        }
        if (lev == 123) {
            addLevelData(getPoints(2, 32), Levels2.getInstance().getLinesForLevel(lev - 1));
            this.pointList.get(2).setLocked();
            this.pointList.get(6).setLocked();
        }
        if (lev == 124 || lev == 125) {
            addLevelData(getPoints(2, 33), Levels2.getInstance().getLinesForLevel(lev));
            this.pointList.get(21).setLocked();
        }
        if (lev == 126 || lev == 127) {
            addLevelData(getPoints(4, 33), Levels2.getInstance().getLinesForLevel(lev - 2));
            this.pointList.get(10).setLocked();
        }
        if (lev == 128) {
            addLevelData(getPoints(4, 33), Levels2.getInstance().getLinesForLevel(lev));
            this.pointList.get(9).setLocked();
            this.pointList.get(13).setLocked();
        }
        if (lev == 129) {
            addLevelData(getPoints(2, 33), Levels2.getInstance().getLinesForLevel(lev - 1));
            this.pointList.get(4).setLocked();
            this.pointList.get(10).setLocked();
        }
        if (lev == 130 || lev == 131 || lev == 132) {
            addLevelData(getPoints(2, 34), Levels2.getInstance().getLinesForLevel(lev));
            if (lev == 131) {
                this.pointList.get(15).setLocked();
            }
        }
        if (lev == 133 || lev == 134 || lev == 135) {
            addLevelData(getPoints(3, 34), Levels2.getInstance().getLinesForLevel(lev - 3));
            if (lev == 134) {
                this.pointList.get(10).setLocked();
            }
            if (lev == 135) {
                this.pointList.get(1).setLocked();
                this.pointList.get(18).setLocked();
            }
        }
        if (lev == 136 || lev == 137 || lev == 138) {
            addLevelData(getPoints(4, 34), Levels2.getInstance().getLinesForLevel(lev - 6));
            if (lev == 136) {
                this.pointList.get(20).setLocked();
            }
            if (lev == 137) {
                this.pointList.get(8).setLocked();
                this.pointList.get(10).setLocked();
            }
            if (lev == 138) {
                this.pointList.get(4).setLocked();
                this.pointList.get(7).setLocked();
                this.pointList.get(0).setLocked();
            }
        }
        if (lev == 139 || lev == 140) {
            addLevelData(getPoints(4, 35), Levels2.getInstance().getLinesForLevel(lev));
            if (lev == 140) {
                this.pointList.get(1).setLocked();
                this.pointList.get(5).setLocked();
            }
        }
        if (lev == 141 || lev == 142) {
            addLevelData(getPoints(3, 35), Levels2.getInstance().getLinesForLevel(lev - 2));
            if (lev == 141) {
                this.pointList.get(1).setLocked();
                this.pointList.get(8).setLocked();
                this.pointList.get(13).setLocked();
            }
        }
        if (lev == 143 || lev == 144) {
            addLevelData(getPoints(4, 36), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 145 || lev == 146) {
            addLevelData(getPoints(3, 36), Levels2.getInstance().getLinesForLevel(lev - 2));
            if (lev == 145) {
                this.pointList.get(0).setLocked();
                this.pointList.get(10).setLocked();
                this.pointList.get(11).setLocked();
            }
            if (lev == 146) {
                this.pointList.get(0).setLocked();
                this.pointList.get(6).setLocked();
                this.pointList.get(1).setLocked();
            }
        }
        if (lev == 147 || lev == 148) {
            addLevelData(getPoints(2, 36), Levels2.getInstance().getLinesForLevel(lev - 4));
            if (lev == 147) {
                this.pointList.get(1).setLocked();
                this.pointList.get(5).setLocked();
                this.pointList.get(9).setLocked();
            }
            if (lev == 148) {
                this.pointList.get(8).setLocked();
                this.pointList.get(10).setLocked();
                this.pointList.get(14).setLocked();
            }
        }
        if (lev == 149 || lev == 150) {
            addLevelData(getPoints(2, 37), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 151 || lev == 152) {
            addLevelData(getPoints(3, 37), Levels2.getInstance().getLinesForLevel(lev - 2));
            if (lev == 151) {
                this.pointList.get(1).setLocked();
                this.pointList.get(3).setLocked();
                this.pointList.get(6).setLocked();
            }
            if (lev == 152) {
                this.pointList.get(20).setLocked();
                this.pointList.get(22).setLocked();
                this.pointList.get(25).setLocked();
            }
        }
        if (lev == 153 || lev == 154) {
            addLevelData(getPoints(4, 37), Levels2.getInstance().getLinesForLevel(lev - 4));
            if (lev == 153) {
                this.pointList.get(2).setLocked();
                this.pointList.get(7).setLocked();
                this.pointList.get(8).setLocked();
            }
            if (lev == 154) {
                this.pointList.get(1).setLocked();
                this.pointList.get(7).setLocked();
                this.pointList.get(9).setLocked();
            }
        }
        if (lev == 155 || lev == 156) {
            addLevelData(getPoints(4, 38), Levels2.getInstance().getLinesForLevel(lev));
        }
        if (lev == 157 || lev == 158) {
            addLevelData(getPoints(3, 38), Levels2.getInstance().getLinesForLevel(lev - 2));
            if (lev == 157) {
                this.pointList.get(1).setLocked();
                this.pointList.get(6).setLocked();
                this.pointList.get(11).setLocked();
            }
            if (lev == 158) {
                this.pointList.get(1).setLocked();
                this.pointList.get(8).setLocked();
                this.pointList.get(9).setLocked();
            }
        }
        if (lev == 159 || lev == 160) {
            addLevelData(getPoints(2, 38), Levels2.getInstance().getLinesForLevel(lev - 4));
            if (lev == 159) {
                this.pointList.get(3).setLocked();
                this.pointList.get(10).setLocked();
                this.pointList.get(12).setLocked();
            }
            if (lev == 160) {
                this.pointList.get(2).setLocked();
                this.pointList.get(4).setLocked();
                this.pointList.get(7).setLocked();
            }
        }
        this.currLevel = lev;
        checkIntersection();
    }

    public GamePoint[] getPoints(int type, int count) {
        GamePoint[] p = new GamePoint[count];
        if (type == 0) {
            int angle = 360 / count;
            int curr_angle = 0;
            int r = this.screenW / 3;
            int xc = this.screenW / 2;
            int yc = this.screenH / 2;
            for (int i = 0; i < count; i++) {
                double rad = (((double) curr_angle) * 3.141592653589793d) / 180.0d;
                p[i] = new GamePoint(((int) (((double) r) * Math.cos(rad))) + xc, ((int) (((double) r) * Math.sin(rad))) + yc);
                curr_angle += angle;
            }
        }
        if (type == 1) {
            int columns = (int) Math.sqrt((double) count);
            int xspacing = this.screenW / (columns + 1);
            int xstart = xspacing;
            int ystart = xspacing;
            for (int i2 = 0; i2 < count; i2++) {
                p[i2] = new GamePoint(xstart + ((i2 % columns) * xspacing), ystart + ((i2 / columns) * xspacing));
            }
        }
        if (type == 2) {
            int angle2 = 360 / count;
            int curr_angle2 = 0;
            int r2 = (int) (((double) this.screenW) / 2.7d);
            int ry = (int) (((double) this.screenW) / 2.1d);
            int xc2 = this.screenW / 2;
            int yc2 = (int) (((double) this.screenH) * 0.45d);
            for (int i3 = 0; i3 < count; i3++) {
                double rad2 = (((double) curr_angle2) * 3.141592653589793d) / 180.0d;
                p[i3] = new GamePoint(((int) (((double) r2) * Math.cos(rad2))) + xc2, ((int) (((double) ry) * Math.sin(rad2))) + yc2);
                curr_angle2 += angle2;
            }
        }
        if (type == 3) {
            int columns2 = (int) Math.sqrt((double) count);
            int xspacing2 = this.screenW / (columns2 + 1);
            int yspacing = this.screenH / (columns2 + 2);
            int xstart2 = xspacing2;
            int ystart2 = xspacing2;
            for (int i4 = 0; i4 < count; i4++) {
                int xoff = this.random.nextInt(20) - 10;
                int x = ((i4 % columns2) * xspacing2) + xstart2 + xoff;
                p[i4] = new GamePoint(x, ((i4 / columns2) * yspacing) + ystart2 + (this.random.nextInt(20) - 10));
            }
        }
        if (type == 4) {
            int r1 = (int) (((double) this.screenW) / 2.5d);
            int ry1 = (int) (((double) this.screenW) / 1.9d);
            int r22 = (int) (((double) this.screenW) / 3.8d);
            int ry2 = (int) (((double) this.screenW) / 3.3d);
            int xc3 = this.screenW / 2;
            int yc3 = (int) (((double) this.screenH) * 0.45d);
            int cnt1 = count / 3;
            int cnt2 = count - cnt1;
            double angle3 = (double) (360 / cnt1);
            double curr_angle3 = 0.0d;
            for (int i5 = 0; i5 < cnt1; i5++) {
                double rad3 = (3.141592653589793d * curr_angle3) / 180.0d;
                p[i5] = new GamePoint(((int) (((double) r22) * Math.cos(rad3))) + xc3, ((int) (((double) ry2) * Math.sin(rad3))) + yc3);
                curr_angle3 += angle3;
            }
            double angle4 = (double) (360 / cnt2);
            double curr_angle4 = 0.0d;
            for (int i6 = 0; i6 < cnt2; i6++) {
                double rad4 = (3.141592653589793d * curr_angle4) / 180.0d;
                p[i6 + cnt1] = new GamePoint(((int) (((double) r1) * Math.cos(rad4))) + xc3, ((int) (((double) ry1) * Math.sin(rad4))) + yc3);
                curr_angle4 += angle4;
            }
        }
        return p;
    }

    public void addLevelData(GamePoint[] p, Line[] l) {
        for (GamePoint addPoint : p) {
            addPoint(addPoint);
        }
        for (Line addLine : l) {
            addLine(addLine);
        }
    }

    public static Levels getInstance() {
        if (instance == null) {
            instance = new Levels();
            instance.initLevel(0);
        }
        return instance;
    }

    public static final byte[] intToByteArray(int value) {
        return new byte[]{(byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value};
    }

    public static final int byteArrayToInt(byte[] b) {
        return (b[0] << 24) + ((b[1] & 255) << 16) + ((b[2] & 255) << 8) + (b[3] & 255);
    }

    public void writeInt(FileOutputStream fos, int val) {
        try {
            fos.write(intToByteArray(val));
        } catch (Exception e) {
        }
    }

    public int getInt(byte[] buffer) {
        if (this.file_pos + 3 >= buffer.length) {
            return 0;
        }
        byte[] int_buf = {buffer[this.file_pos], buffer[this.file_pos + 1], buffer[this.file_pos + 2], buffer[this.file_pos + 3]};
        this.file_pos += 4;
        return byteArrayToInt(int_buf);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.FileNotFoundException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void saveLevel() {
        try {
            FileOutputStream fos = MainActivity.getInstance().getApplicationContext().openFileOutput("level_" + this.currLevel, 0);
            int i = 0;
            while (i < this.pointList.size()) {
                try {
                    writeInt(fos, this.pointList.get(i).x);
                    writeInt(fos, this.pointList.get(i).y);
                    i++;
                } catch (IOException e) {
                    Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e);
                    return;
                }
            }
            fos.close();
        } catch (FileNotFoundException e2) {
            Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e2);
        }
    }

    public void loadLevel() {
        this.file_pos = 0;
        try {
            FileInputStream fis = MainActivity.getInstance().getApplicationContext().openFileInput("level_" + this.currLevel);
            try {
                byte[] buffer = new byte[fis.available()];
                fis.read(buffer);
                for (int i = 0; i < this.pointList.size(); i++) {
                    this.pointList.get(i).x = getInt(buffer);
                    this.pointList.get(i).y = getInt(buffer);
                }
                fis.close();
            } catch (IOException e) {
            }
        } catch (FileNotFoundException e2) {
        }
    }
}
