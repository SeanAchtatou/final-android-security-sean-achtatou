package softkos.untanglemeextreme;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import softkos.untanglemeextreme.Vars;

public class MainActivity extends Activity {
    static MainActivity instance = null;
    ImageView backImageView = null;
    Button closeDlgButton = null;
    GameCanvas gameCanvas = null;
    RelativeLayout gameLayout;
    Dialog howToPlayDlg = null;
    Vars.AppState lastAppState;
    MenuCanvas menuCanvas = null;
    ImageButton nextBackBtn = null;
    ImageButton nextPointsBtn = null;
    ImageView pointsImageView = null;
    ImageButton prevBackBtn = null;
    ImageButton prevPointsBtn = null;
    Button resetLevelsBtn = null;
    MenuItem undoMenuItem;
    Button vibrationSettingBtn = null;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        instance = this;
        requestWindowFeature(1);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        getWindow().setFlags(1024, 1024);
        Vars.getInstance().setScreenSize(dm.widthPixels, dm.heightPixels);
        Vars.getInstance();
        FileWR.getInstance().loadGame();
        Levels.getInstance().initLevel(Vars.getInstance().getLevel() - 1);
        Levels.getInstance().loadLevel();
        Vars.getInstance().nextLevel();
        Levels.getInstance().loadLevel();
        Vars.getInstance().prevLevel();
        Settings.getInstnace();
        showMenu();
    }

    public void vibrate(int ms) {
        if (Settings.getInstnace().getIntSettings(Settings.VIBRATION_DISABLED) == 0) {
            try {
                ((Vibrator) getSystemService("vibrator")).vibrate((long) ms);
            } catch (Exception e) {
            }
        }
    }

    public void showMenu() {
        if (this.menuCanvas == null) {
            this.menuCanvas = new MenuCanvas(this);
        }
        OtherApp.getInstance().random();
        this.menuCanvas.updateButtonText();
        Vars.getInstance().setAppState(Vars.AppState.Menu);
        this.menuCanvas.showUI(true);
        setContentView(this.menuCanvas);
    }

    public void showGame() {
        if (this.gameCanvas == null) {
            this.gameLayout = new RelativeLayout(this);
            this.gameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            this.gameCanvas = new GameCanvas(this);
            AdView adView = new AdView(this, AdSize.BANNER, "a14e0e342fcfb1b");
            RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(-1, -2);
            lparams.addRule(12);
            this.gameLayout.addView(this.gameCanvas);
            this.gameLayout.addView(adView, lparams);
            adView.loadAd(new AdRequest());
            adView.bringToFront();
        }
        Vars.getInstance().setAppState(Vars.AppState.Game);
        if (this.menuCanvas != null) {
            this.menuCanvas.showUI(false);
        }
        this.gameCanvas.showUI(true);
        this.gameCanvas.resetVp();
        setContentView(this.gameLayout);
    }

    public void showSettings() {
        this.lastAppState = Vars.getInstance().getAppState();
        Vars.getInstance().setAppState(Vars.AppState.Settings);
        setContentView((int) R.layout.settings);
        Settings instnace = Settings.getInstnace();
        Settings.getInstnace();
        int skin = instnace.getIntSettings(Settings.GAME_BACK_INDEX);
        this.backImageView = (ImageView) findViewById(R.id.back_image);
        this.pointsImageView = (ImageView) findViewById(R.id.point_image);
        if (skin == 0) {
            this.backImageView.setImageResource(R.drawable.back_0);
        }
        if (skin == 1) {
            this.backImageView.setImageResource(R.drawable.back_1);
        }
        if (skin == 2) {
            this.backImageView.setImageResource(R.drawable.back_2);
        }
        if (skin == 3) {
            this.backImageView.setImageResource(R.drawable.back_3);
        }
        Settings instnace2 = Settings.getInstnace();
        Settings.getInstnace();
        int val = instnace2.getIntSettings(Settings.GAME_POINTS_INDEX);
        if (val == 0) {
            this.pointsImageView.setImageResource(R.drawable.bug);
        }
        if (val == 1) {
            this.pointsImageView.setImageResource(R.drawable.bug_1);
        }
        if (val == 2) {
            this.pointsImageView.setImageResource(R.drawable.bug_2);
        }
        if (val == 3) {
            this.pointsImageView.setImageResource(R.drawable.bug_3);
        }
        this.nextBackBtn = (ImageButton) findViewById(R.id.next_back);
        this.nextBackBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Settings instnace = Settings.getInstnace();
                Settings.getInstnace();
                int val = instnace.getIntSettings(Settings.GAME_BACK_INDEX) + 1;
                if (val > 3) {
                    val = 0;
                }
                if (val == 0) {
                    MainActivity.this.backImageView.setImageResource(R.drawable.back_0);
                }
                if (val == 1) {
                    MainActivity.this.backImageView.setImageResource(R.drawable.back_1);
                }
                if (val == 2) {
                    MainActivity.this.backImageView.setImageResource(R.drawable.back_2);
                }
                if (val == 3) {
                    MainActivity.this.backImageView.setImageResource(R.drawable.back_3);
                }
                Settings instnace2 = Settings.getInstnace();
                Settings.getInstnace();
                instnace2.setIntSettings(Settings.GAME_BACK_INDEX, val);
            }
        });
        this.prevBackBtn = (ImageButton) findViewById(R.id.prev_back);
        this.prevBackBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Settings instnace = Settings.getInstnace();
                Settings.getInstnace();
                int val = instnace.getIntSettings(Settings.GAME_BACK_INDEX) - 1;
                if (val < 0) {
                    val = 3;
                }
                if (val == 0) {
                    MainActivity.this.backImageView.setImageResource(R.drawable.back_0);
                }
                if (val == 1) {
                    MainActivity.this.backImageView.setImageResource(R.drawable.back_1);
                }
                if (val == 2) {
                    MainActivity.this.backImageView.setImageResource(R.drawable.back_2);
                }
                if (val == 3) {
                    MainActivity.this.backImageView.setImageResource(R.drawable.back_3);
                }
                Settings instnace2 = Settings.getInstnace();
                Settings.getInstnace();
                instnace2.setIntSettings(Settings.GAME_BACK_INDEX, val);
            }
        });
        this.nextPointsBtn = (ImageButton) findViewById(R.id.next_point);
        this.nextPointsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Settings instnace = Settings.getInstnace();
                Settings.getInstnace();
                int val = instnace.getIntSettings(Settings.GAME_POINTS_INDEX) + 1;
                if (val > 3) {
                    val = 0;
                }
                if (val == 0) {
                    MainActivity.this.pointsImageView.setImageResource(R.drawable.bug);
                }
                if (val == 1) {
                    MainActivity.this.pointsImageView.setImageResource(R.drawable.bug_1);
                }
                if (val == 2) {
                    MainActivity.this.pointsImageView.setImageResource(R.drawable.bug_2);
                }
                if (val == 3) {
                    MainActivity.this.pointsImageView.setImageResource(R.drawable.bug_3);
                }
                Settings instnace2 = Settings.getInstnace();
                Settings.getInstnace();
                instnace2.setIntSettings(Settings.GAME_POINTS_INDEX, val);
            }
        });
        this.prevPointsBtn = (ImageButton) findViewById(R.id.prev_point);
        this.prevPointsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Settings instnace = Settings.getInstnace();
                Settings.getInstnace();
                int val = instnace.getIntSettings(Settings.GAME_POINTS_INDEX) - 1;
                if (val < 0) {
                    val = 3;
                }
                if (val == 0) {
                    MainActivity.this.pointsImageView.setImageResource(R.drawable.bug);
                }
                if (val == 1) {
                    MainActivity.this.pointsImageView.setImageResource(R.drawable.bug_1);
                }
                if (val == 2) {
                    MainActivity.this.pointsImageView.setImageResource(R.drawable.bug_2);
                }
                if (val == 3) {
                    MainActivity.this.pointsImageView.setImageResource(R.drawable.bug_3);
                }
                Settings instnace2 = Settings.getInstnace();
                Settings.getInstnace();
                instnace2.setIntSettings(Settings.GAME_POINTS_INDEX, val);
            }
        });
        this.resetLevelsBtn = (Button) findViewById(R.id.reset_all_levels);
        this.resetLevelsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MainActivity.getInstance().resetAllLevels();
            }
        });
        this.vibrationSettingBtn = (Button) findViewById(R.id.vibration_setting);
        if (Settings.getInstnace().getIntSettings(Settings.VIBRATION_DISABLED) != 0) {
            this.vibrationSettingBtn.setText("Vibration: OFF");
        } else {
            this.vibrationSettingBtn.setText("Vibration: ON");
        }
        this.vibrationSettingBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (Settings.getInstnace().getIntSettings(Settings.VIBRATION_DISABLED) != 0) {
                    MainActivity.this.vibrationSettingBtn.setText("Vibration: ON");
                    Settings.getInstnace().setIntSettings(Settings.VIBRATION_DISABLED, 0);
                    return;
                }
                MainActivity.this.vibrationSettingBtn.setText("Vibration: OFF");
                Settings.getInstnace().setIntSettings(Settings.VIBRATION_DISABLED, 1);
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && (Vars.getInstance().getAppState() == Vars.AppState.Game || Vars.getInstance().getAppState() == Vars.AppState.HowToPlay || Vars.getInstance().getAppState() == Vars.AppState.Settings)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyUp(keyCode, event);
        }
        if (Vars.getInstance().getAppState() == Vars.AppState.Game) {
            if (GameCanvas.getInstance() == null || GameCanvas.getInstance().getLevelSelectionDialog() == null) {
                Levels.getInstance().saveLevel();
                showMenu();
            } else {
                GameCanvas.getInstance().dismissDialogs();
                return true;
            }
        }
        if (Vars.getInstance().getAppState() == Vars.AppState.HowToPlay) {
            showMenu();
        }
        if (Vars.getInstance().getAppState() == Vars.AppState.Settings) {
            Settings.getInstnace().saveSettings();
            if (this.lastAppState == Vars.AppState.Game) {
                showGame();
            } else {
                showMenu();
            }
        }
        return true;
    }

    public static MainActivity getInstance() {
        return instance;
    }

    public MenuItem getUndoMenuItem() {
        return this.undoMenuItem;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2001, 0, "Prev level").setIcon((int) R.drawable.arrow_left);
        menu.add(0, 2002, 0, "Next level").setIcon((int) R.drawable.arrow_right);
        this.undoMenuItem = menu.add(0, 2004, 1, "Undo");
        MenuItem add = menu.add(0, 2000, 1, "Reset level");
        MenuItem add2 = menu.add(0, 2003, 1, "Select level");
        MenuItem add3 = menu.add(1, 2005, 4, "Settings");
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onOptionsItemSelected(android.view.MenuItem r4) {
        /*
            r3 = this;
            r2 = 1
            int r0 = r4.getItemId()
            switch(r0) {
                case 2000: goto L_0x000a;
                case 2001: goto L_0x0024;
                case 2002: goto L_0x003e;
                case 2003: goto L_0x0058;
                case 2004: goto L_0x006b;
                case 2005: goto L_0x0074;
                default: goto L_0x0008;
            }
        L_0x0008:
            r0 = 0
        L_0x0009:
            return r0
        L_0x000a:
            softkos.untanglemeextreme.Vars r0 = softkos.untanglemeextreme.Vars.getInstance()
            softkos.untanglemeextreme.Vars$AppState r0 = r0.appState
            softkos.untanglemeextreme.Vars$AppState r1 = softkos.untanglemeextreme.Vars.AppState.Game
            if (r0 != r1) goto L_0x0024
            softkos.untanglemeextreme.Vars r0 = softkos.untanglemeextreme.Vars.getInstance()
            r0.resetLevel()
            softkos.untanglemeextreme.GameCanvas r0 = softkos.untanglemeextreme.GameCanvas.getInstance()
            r0.resetVp()
            r0 = r2
            goto L_0x0009
        L_0x0024:
            softkos.untanglemeextreme.Vars r0 = softkos.untanglemeextreme.Vars.getInstance()
            softkos.untanglemeextreme.Vars$AppState r0 = r0.appState
            softkos.untanglemeextreme.Vars$AppState r1 = softkos.untanglemeextreme.Vars.AppState.Game
            if (r0 != r1) goto L_0x003e
            softkos.untanglemeextreme.Vars r0 = softkos.untanglemeextreme.Vars.getInstance()
            r0.prevLevel()
            softkos.untanglemeextreme.GameCanvas r0 = softkos.untanglemeextreme.GameCanvas.getInstance()
            r0.resetVp()
            r0 = r2
            goto L_0x0009
        L_0x003e:
            softkos.untanglemeextreme.Vars r0 = softkos.untanglemeextreme.Vars.getInstance()
            softkos.untanglemeextreme.Vars$AppState r0 = r0.appState
            softkos.untanglemeextreme.Vars$AppState r1 = softkos.untanglemeextreme.Vars.AppState.Game
            if (r0 != r1) goto L_0x0058
            softkos.untanglemeextreme.Vars r0 = softkos.untanglemeextreme.Vars.getInstance()
            r0.nextLevel()
            softkos.untanglemeextreme.GameCanvas r0 = softkos.untanglemeextreme.GameCanvas.getInstance()
            r0.resetVp()
            r0 = r2
            goto L_0x0009
        L_0x0058:
            softkos.untanglemeextreme.Vars r0 = softkos.untanglemeextreme.Vars.getInstance()
            softkos.untanglemeextreme.Vars$AppState r0 = r0.appState
            softkos.untanglemeextreme.Vars$AppState r1 = softkos.untanglemeextreme.Vars.AppState.Game
            if (r0 != r1) goto L_0x0069
            softkos.untanglemeextreme.GameCanvas r0 = softkos.untanglemeextreme.GameCanvas.getInstance()
            r0.selectLevelDialog()
        L_0x0069:
            r0 = r2
            goto L_0x0009
        L_0x006b:
            softkos.untanglemeextreme.Vars r0 = softkos.untanglemeextreme.Vars.getInstance()
            r0.undoMove()
            r0 = r2
            goto L_0x0009
        L_0x0074:
            r3.showSettings()
            r0 = r2
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: softkos.untanglemeextreme.MainActivity.onOptionsItemSelected(android.view.MenuItem):boolean");
    }

    public void resetAllLevels() {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        alertbox.setTitle("Reset all levels");
        alertbox.setMessage("Do you want reset all solved levels?");
        alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Levels.getInstance().clearSolved();
                FileWR.getInstance().saveGame();
                if (Vars.getInstance().appState == Vars.AppState.Game && GameCanvas.getInstance() != null) {
                    GameCanvas.getInstance().resetVp();
                }
            }
        });
        alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        alertbox.show();
    }

    public Dialog getHowToPlayDlg() {
        return this.howToPlayDlg;
    }

    public void showHowToPlay() {
        this.howToPlayDlg = new Dialog(this);
        this.howToPlayDlg.requestWindowFeature(1);
        this.howToPlayDlg.setContentView((int) R.layout.how_to_play);
        this.howToPlayDlg.show();
        this.closeDlgButton = (Button) this.howToPlayDlg.findViewById(R.id.close_dlg);
        this.closeDlgButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MainActivity.getInstance().getHowToPlayDlg().dismiss();
            }
        });
    }
}
