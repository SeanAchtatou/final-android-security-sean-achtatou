package softkos.untanglemeextreme;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;
import com.heyzap.sdk.HeyzapLib;

public class GameCanvas extends View {
    static GameCanvas instance = null;
    int bugDragged = -1;
    int bug_size = 32;
    int cnt = 0;
    gButton heyzapBtn = null;
    int inter_cnt = 0;
    AlertDialog lvlSelectDlg = null;
    gButton nextLevelBtn = null;
    Paint paint = new Paint();
    PaintManager paintMgr;
    int scrollStartX = -1;
    int scrollStartY = -1;
    Vars vars;
    int vpx;
    int vpy;
    gButton zoomInBtn = null;
    gButton zoomOutBtn = null;

    public GameCanvas(Context c) {
        super(c);
        instance = this;
        this.vars = Vars.getInstance();
        this.paintMgr = PaintManager.getInstance();
        initUI();
        layoutUI();
    }

    public void initUI() {
        int btn_w = 268;
        int btn_h = 50;
        int btn_s = 45;
        if (this.vars.getScreenWidth() > 400) {
            btn_w = 388;
            btn_h = 70;
            btn_s = 60;
        }
        this.nextLevelBtn = new gButton();
        this.nextLevelBtn.setSize(btn_w, btn_h);
        this.nextLevelBtn.setId(Vars.getInstance().NEXT_LEVEL);
        this.nextLevelBtn.setText("Next level");
        Vars.getInstance().addButton(this.nextLevelBtn);
        this.nextLevelBtn.hide();
        this.heyzapBtn = new gButton();
        this.heyzapBtn.setSize(btn_w, btn_h);
        this.heyzapBtn.setId(Vars.getInstance().HEYZAP_GAME);
        this.heyzapBtn.setImage("heyzap_game");
        this.heyzapBtn.setText("Share with friend      ");
        Vars.getInstance().addButton(this.heyzapBtn);
        this.heyzapBtn.hide();
        this.zoomInBtn = new gButton();
        this.zoomInBtn.setSize(btn_s, btn_s);
        this.zoomInBtn.setId(Vars.getInstance().ZOOM_IN);
        this.zoomInBtn.setImage("zoom_in");
        this.zoomInBtn.disableBackImages();
        Vars.getInstance().addButton(this.zoomInBtn);
        this.zoomOutBtn = new gButton();
        this.zoomOutBtn.setSize(btn_s, btn_s);
        this.zoomOutBtn.setId(Vars.getInstance().ZOOM_OUT);
        this.zoomOutBtn.setImage("zoom_out");
        this.zoomOutBtn.disableBackImages();
        Vars.getInstance().addButton(this.zoomOutBtn);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Levels.getInstance().setScreenSize(getWidth(), getHeight());
        if (getWidth() > 400) {
            this.bug_size = 60;
        }
        if (getWidth() < 300) {
            this.bug_size = 30;
        }
        layoutUI();
    }

    public void showUI(boolean s) {
        Vars.getInstance().hideAllButtons();
        if (s) {
            this.zoomOutBtn.show();
            this.zoomInBtn.show();
        }
    }

    public void layoutUI() {
        if (this.nextLevelBtn != null) {
            this.nextLevelBtn.setPosition((getWidth() / 2) - (this.nextLevelBtn.getWidth() / 2), ((getHeight() / 2) + (getWidth() / 2)) - this.nextLevelBtn.getHeight());
            this.heyzapBtn.setPosition((getWidth() / 2) - (this.heyzapBtn.getWidth() / 2), this.nextLevelBtn.getPy() - ((int) (((double) this.nextLevelBtn.getHeight()) * 1.05d)));
            this.zoomInBtn.setPosition(getWidth() - this.zoomInBtn.getWidth(), 1);
            this.zoomOutBtn.setPosition(0, 1);
        }
    }

    public void mouseDown(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                this.scrollStartX = x;
                this.scrollStartY = y;
                return;
            }
        }
        int cl = this.vars.getLevel();
        this.bugDragged = -1;
        if (Levels.getInstance().isSolved(cl) == 0) {
            Levels lev = Levels.getInstance();
            int click_bug_size = ((int) (((double) this.bug_size) * this.vars.getZoomLevel())) * 2;
            int i2 = 0;
            while (true) {
                if (i2 >= lev.getPointCount()) {
                    break;
                }
                GamePoint p = new GamePoint(lev.getPoint(i2).x, lev.getPoint(i2).y);
                if (!lev.getPoint(i2).isLocked()) {
                    p.x = (int) (((double) p.x) * this.vars.getZoomLevel());
                    p.y = (int) (((double) p.y) * this.vars.getZoomLevel());
                    if (p.x - this.vpx > x - (click_bug_size / 2) && p.x - this.vpx < (click_bug_size / 2) + x && p.y - this.vpy > y - (click_bug_size / 2) && p.y - this.vpy < (click_bug_size / 2) + y) {
                        this.bugDragged = i2;
                        this.vars.saveLastMove(i2);
                        MainActivity.getInstance().vibrate(50);
                        break;
                    }
                }
                i2++;
            }
            if (this.bugDragged < 0) {
                this.scrollStartX = x;
                this.scrollStartY = y;
            }
        }
    }

    public void resetVp() {
        this.vpy = 0;
        this.vpx = 0;
    }

    public void mouseUp(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
        if (this.bugDragged >= 0) {
            MainActivity.getInstance().vibrate(50);
        }
        this.bugDragged = -1;
        Levels.getInstance().checkIntersection();
        Levels.getInstance().isLevelSolved();
    }

    public void mouseDrag(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
        if (Levels.getInstance().isSolved(this.vars.getLevel()) == 0) {
            if (this.bugDragged <= -1) {
                this.vpx -= x - this.scrollStartX;
                this.vpy -= y - this.scrollStartY;
                this.scrollStartX = x;
                this.scrollStartY = y;
            } else if (Levels.getInstance().getPoint(this.bugDragged) != null) {
                Levels.getInstance().getPoint(this.bugDragged).x = ((int) (((double) (x * 1)) / this.vars.getZoomLevel())) + ((int) (((double) (this.vpx * 1)) / this.vars.getZoomLevel()));
                Levels.getInstance().getPoint(this.bugDragged).y = ((int) (((double) (y * 1)) / this.vars.getZoomLevel())) + ((int) (((double) (this.vpy * 1)) / this.vars.getZoomLevel()));
            }
        }
        if (this.inter_cnt > 5) {
            Levels.getInstance().checkIntersection();
            this.inter_cnt = 0;
        }
        this.inter_cnt++;
    }

    public void handleCommand(int id) {
        if (id == Vars.getInstance().NEXT_LEVEL) {
            resetVp();
            this.vars.nextLevel();
        }
        if (id == Vars.getInstance().ZOOM_IN) {
            this.vars.zoomIn();
        }
        if (id == Vars.getInstance().ZOOM_OUT) {
            this.vars.zoomOut();
        }
        if (id == Vars.getInstance().HEYZAP_GAME) {
            HeyzapLib.checkin(MainActivity.getInstance(), "I just finished level " + (this.vars.getLevel() + 1) + " in great Untangle Me Extreme game on Android! :)");
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
            invalidate();
        }
        return true;
    }

    public void drawBack(Canvas canvas) {
        int c1 = Color.rgb(33, 49, 74);
        int c2 = Color.rgb(43, 59, 84);
        if (Settings.getInstnace().getIntSettings(Settings.GAME_BACK_INDEX) == 1) {
            c1 = Color.rgb(122, 89, 63);
            c2 = Color.rgb(163, 121, 91);
        }
        if (Settings.getInstnace().getIntSettings(Settings.GAME_BACK_INDEX) == 2) {
            c1 = Color.rgb(87, 135, 86);
            c2 = Color.rgb(62, 97, 62);
        }
        if (Settings.getInstnace().getIntSettings(Settings.GAME_BACK_INDEX) == 3) {
            c1 = Color.rgb(200, 200, 200);
            c2 = Color.rgb(240, 240, 240);
        }
        int size = getWidth() / 3;
        int curr_col = c1;
        int bx = this.vpx % (size * 2);
        int by = this.vpy % (size * 2);
        for (int y = -2; y < 12; y++) {
            for (int x = -2; x < 5; x++) {
                if (curr_col == c1) {
                    curr_col = c2;
                } else {
                    curr_col = c1;
                }
                this.paintMgr.setColor(curr_col);
                this.paintMgr.fillRectangle(canvas, (x * size) - bx, (y * size) - by, size, size);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paint.setAntiAlias(true);
        drawBack(canvas);
        Levels lev = Levels.getInstance();
        int color1 = Color.rgb(140, 250, 40);
        int color2 = Color.argb(100, 50, 200, 50);
        for (int i = 0; i < lev.getLinesCount(); i++) {
            if (!lev.getLine(i).isIntersected()) {
                int start_index = lev.getLine(i).start_index;
                int end_index = lev.getLine(i).end_index;
                Point point = new Point(lev.getPoint(start_index).x, lev.getPoint(start_index).y);
                Point point2 = new Point(lev.getPoint(end_index).x, lev.getPoint(end_index).y);
                point.x = (int) (((double) point.x) * this.vars.getZoomLevel());
                point.y = (int) (((double) point.y) * this.vars.getZoomLevel());
                point2.x = (int) (((double) point2.x) * this.vars.getZoomLevel());
                point2.y = (int) (((double) point2.y) * this.vars.getZoomLevel());
                if (this.bugDragged < 0 || !(start_index == this.bugDragged || end_index == this.bugDragged)) {
                    this.paintMgr.setLineWidth(3);
                    this.paintMgr.setColor(color2);
                    this.paintMgr.drawLine(canvas, point.x - this.vpx, point.y - this.vpy, point2.x - this.vpx, point2.y - this.vpy);
                    this.paintMgr.setColor(-16711936);
                    this.paintMgr.setLineWidth(1);
                    this.paintMgr.drawLine(canvas, point.x - this.vpx, point.y - this.vpy, point2.x - this.vpx, point2.y - this.vpy);
                } else {
                    this.paintMgr.setLineWidth(5);
                    this.paintMgr.setColor(color1);
                    this.paintMgr.drawLine(canvas, point.x - this.vpx, point.y - this.vpy, point2.x - this.vpx, point2.y - this.vpy);
                    this.paintMgr.setColor(-16711936);
                    this.paintMgr.setLineWidth(2);
                    this.paintMgr.drawLine(canvas, point.x - this.vpx, point.y - this.vpy, point2.x - this.vpx, point2.y - this.vpy);
                }
            }
        }
        int color3 = Color.argb(170, 160, 40, 40);
        int color4 = Color.argb(100, 200, 50, 50);
        for (int i2 = 0; i2 < lev.getLinesCount(); i2++) {
            if (lev.getLine(i2).isIntersected()) {
                int start_index2 = lev.getLine(i2).start_index;
                int end_index2 = lev.getLine(i2).end_index;
                Point point3 = new Point(lev.getPoint(start_index2).x, lev.getPoint(start_index2).y);
                Point point4 = new Point(lev.getPoint(end_index2).x, lev.getPoint(end_index2).y);
                point3.x = (int) (((double) point3.x) * this.vars.getZoomLevel());
                point3.y = (int) (((double) point3.y) * this.vars.getZoomLevel());
                point4.x = (int) (((double) point4.x) * this.vars.getZoomLevel());
                point4.y = (int) (((double) point4.y) * this.vars.getZoomLevel());
                if (this.bugDragged < 0 || !(start_index2 == this.bugDragged || end_index2 == this.bugDragged)) {
                    this.paintMgr.setLineWidth(3);
                    this.paintMgr.setColor(color4);
                    this.paintMgr.drawLine(canvas, point3.x - this.vpx, point3.y - this.vpy, point4.x - this.vpx, point4.y - this.vpy);
                    this.paintMgr.setLineWidth(1);
                    this.paintMgr.setColor(-65536);
                    this.paintMgr.drawLine(canvas, point3.x - this.vpx, point3.y - this.vpy, point4.x - this.vpx, point4.y - this.vpy);
                } else {
                    this.paintMgr.setLineWidth(5);
                    this.paintMgr.setColor(color3);
                    this.paintMgr.drawLine(canvas, point3.x - this.vpx, point3.y - this.vpy, point4.x - this.vpx, point4.y - this.vpy);
                    this.paintMgr.setLineWidth(2);
                    this.paintMgr.setColor(-65536);
                    this.paintMgr.drawLine(canvas, point3.x - this.vpx, point3.y - this.vpy, point4.x - this.vpx, point4.y - this.vpy);
                }
            }
        }
        int val = Settings.getInstnace().getIntSettings(Settings.GAME_POINTS_INDEX);
        for (int i3 = 0; i3 < lev.getPointCount(); i3++) {
            GamePoint p = lev.getPoint(i3);
            int x = (int) ((((double) (p.x - (this.bug_size / 2))) * this.vars.getZoomLevel()) - ((double) this.vpx));
            int y = (int) ((((double) (p.y - (this.bug_size / 2))) * this.vars.getZoomLevel()) - ((double) this.vpy));
            int bs = (int) (((double) this.bug_size) * this.vars.getZoomLevel());
            String bug_img = "bug_" + val;
            if (p.isLocked()) {
                bug_img = "bug_locked";
            }
            this.paintMgr.drawImage(canvas, bug_img, x, y, bs, bs);
        }
        this.nextLevelBtn.hide();
        this.heyzapBtn.hide();
        if (Levels.getInstance().getSolvedLevel(this.vars.getLevel()) > 0) {
            this.paintMgr.drawImage(canvas, "solved", 0, (getHeight() / 2) - (getWidth() / 2), getWidth(), getWidth());
            this.nextLevelBtn.show();
            this.heyzapBtn.show();
        }
        if (Settings.getInstnace().getIntSettings(Settings.GAME_BACK_INDEX) == 3) {
            this.paintMgr.setColor(-16777216);
        } else {
            this.paintMgr.setColor(-1);
        }
        this.paintMgr.setTextSize(20);
        int h = this.paintMgr.getTextSize();
        this.paintMgr.drawString(canvas, "Level: " + (this.vars.getLevel() + 1), this.zoomOutBtn.getWidth() + 10, (h * 2) + 6, getWidth(), 40, PaintManager.STR_LEFT);
        this.paintMgr.drawString(canvas, "Press menu for options", this.zoomOutBtn.getWidth() + 10, h + 3, getWidth(), 40, PaintManager.STR_LEFT);
        for (int i4 = 0; i4 < Vars.getInstance().getButtonList().size(); i4++) {
            Vars.getInstance().getButton(i4).draw(canvas, this.paint);
        }
    }

    public static GameCanvas getInstance() {
        return instance;
    }

    public AlertDialog getLevelSelectionDialog() {
        return this.lvlSelectDlg;
    }

    public void dismissDialogs() {
        this.lvlSelectDlg = null;
    }

    /* access modifiers changed from: package-private */
    public void selectLevelDialog() {
        String[] items = new String[Levels.getInstance().getLevelCount()];
        for (int i = 0; i < Levels.getInstance().getLevelCount(); i++) {
            items[i] = "Level " + (i + 1);
            if (Levels.getInstance().isSolved(i) != 0) {
                items[i] = String.valueOf(items[i]) + ": solved";
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.getInstance());
        builder.setTitle("Select level");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Levels.getInstance().saveLevel();
                Vars.getInstance().setLevel(item);
                Levels.getInstance().initLevel(item);
                GameCanvas.getInstance().resetVp();
                Levels.getInstance().loadLevel();
                Levels.getInstance().checkIntersection();
                GameCanvas.this.lvlSelectDlg.dismiss();
                GameCanvas.this.lvlSelectDlg = null;
            }
        });
        this.lvlSelectDlg = builder.create();
        this.lvlSelectDlg.show();
    }
}
