package softkos.untanglemeextreme;

import java.util.Random;

public class OtherApp {
    static OtherApp instance = null;
    public String[] button_text = {"   Play Tripeaks!", "  Try Frogs Jump", "    Get Cubix game", "     Play BoxIt", "     Turn me on ;)", "    Play Move me"};
    public int current;
    public String[] images = {"tripeaks_ad", "frogs_jump_ad", "cubix_ad", "boxit_ad", "turnmeon_ad", "moveme_ad"};
    Random r = new Random();
    public String[] urls = {"market://details?id=softkos.tripeaks", "market://details?id=sofkos.frogsjump", "market://details?id=softkos.cubix", "market://details?id=softkos.boxit", "market://details?id=softkos.turnmeon", "market://details?id=softkos.moveme"};

    public OtherApp() {
        random();
    }

    public void random() {
        this.current = this.r.nextInt(this.button_text.length);
    }

    public static OtherApp getInstance() {
        if (instance == null) {
            instance = new OtherApp();
        }
        return instance;
    }
}
