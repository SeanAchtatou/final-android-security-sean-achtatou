package softkos.untanglemeextreme;

import android.graphics.drawable.Drawable;

public class ImageLoader {
    static ImageLoader instance = null;
    String[] image_names = {"button_off", "button_on", "bug_0", "bug_1", "bug_2", "bug_3", "bug_locked", "menuimage", "menuimage_inv", "solved", "arrow_right", "arrow_left", "other_button_off", "other_button_on", "zoom_in", "zoom_out", "heyzap_menu", "heyzap_game", "rotateme_ad", "frogs_jump_ad", "birdy_ad", "cubix_ad", "connect_ad", "boxit_ad", "turnmeon_ad", "moveme_ad", "tripeaks_ad"};
    Drawable[] images;
    int[] img_id = {R.drawable.button_off, R.drawable.button_on, R.drawable.bug, R.drawable.bug_1, R.drawable.bug_2, R.drawable.bug_3, R.drawable.bug_locked, R.drawable.menuimage, R.drawable.menuimage_inv, R.drawable.solved, R.drawable.arrow_right, R.drawable.arrow_left, R.drawable.other_button_off, R.drawable.other_button_on, R.drawable.zoom_in, R.drawable.zoom_out, R.drawable.heyzap_menu, R.drawable.heyzap_game, R.drawable.rotateme_ad, R.drawable.frogs_jump_ad, R.drawable.birdy_ad, R.drawable.cubix_ad, R.drawable.connect_ad, R.drawable.boxit_ad, R.drawable.turnmeon_ad, R.drawable.ad_moveme, R.drawable.tripeaks_ad};

    public static ImageLoader getInstance() {
        if (instance == null) {
            instance = new ImageLoader();
        }
        return instance;
    }

    public ImageLoader() {
        LoadImages();
    }

    public void LoadImages() {
        int len = this.img_id.length;
        this.images = new Drawable[this.image_names.length];
        for (int i = 0; i < len; i++) {
            this.images[i] = MainActivity.getInstance().getResources().getDrawable(this.img_id[i]);
        }
    }

    public Drawable getImage(String img) {
        for (int i = 0; i < this.image_names.length; i++) {
            if (img.equals(this.image_names[i])) {
                return this.images[i];
            }
        }
        return null;
    }
}
