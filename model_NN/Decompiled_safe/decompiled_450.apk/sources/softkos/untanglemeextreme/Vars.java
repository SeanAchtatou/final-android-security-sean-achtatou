package softkos.untanglemeextreme;

import java.util.ArrayList;
import java.util.Timer;

public class Vars {
    static Vars instance = null;
    public int APPSTORE_BUY = 105;
    public int FACEBOOK = 7;
    public int GO_TO_MOBILSOFT = 2;
    public int HEYZAP_GAME = 104;
    public int HEYZAP_MENU = 8;
    public int HOW_TO_PLAY = 6;
    public int NEXT_LEVEL = 101;
    public int OTHER_APP = 4;
    public int RESET_LEVEL = 100;
    public int RESET_LEVELS = 3;
    public int START_GAME = 1;
    public int TWITTER = 5;
    public int ZOOM_IN = 102;
    public int ZOOM_OUT = 103;
    AppState appState;
    ArrayList<gButton> buttonList = null;
    int currLevel = 0;
    ArrayList<PointMove> lastMoves = null;
    int screenHeight;
    int screenWidth;
    Timer updateTimer = new Timer();
    double zoomLevel = 100.0d;

    public enum AppState {
        Menu,
        About,
        Game,
        HowToPlay,
        Settings
    }

    public void setLevel(int l) {
        this.currLevel = l;
    }

    public int getLevel() {
        return this.currLevel;
    }

    public void nextLevel() {
        if (this.currLevel < Levels.getInstance().getLevelCount() - 1) {
            Levels.getInstance().saveLevel();
            this.currLevel++;
            this.lastMoves.clear();
            updateMenu();
            Levels.getInstance().initLevel(this.currLevel);
            Levels.getInstance().loadLevel();
            FileWR.getInstance().saveGame();
            try {
                Levels.getInstance().checkIntersection();
            } catch (Exception e) {
            }
        }
    }

    public void prevLevel() {
        if (this.currLevel > 0) {
            Levels.getInstance().saveLevel();
            this.currLevel--;
            this.lastMoves.clear();
            updateMenu();
            Levels.getInstance().initLevel(this.currLevel);
            Levels.getInstance().loadLevel();
            FileWR.getInstance().saveGame();
            try {
                Levels.getInstance().checkIntersection();
            } catch (Exception e) {
            }
        }
    }

    public void resetLevel() {
        if (this.currLevel < Levels.getInstance().getLevelCount() - 1) {
            Levels.getInstance().resetSolved(this.currLevel);
            this.lastMoves.clear();
            this.currLevel++;
            Levels.getInstance().initLevel(this.currLevel);
            this.currLevel--;
            Levels.getInstance().initLevel(this.currLevel);
            updateMenu();
            Levels.getInstance().saveLevel();
            FileWR.getInstance().saveGame();
            try {
                Levels.getInstance().checkIntersection();
            } catch (Exception e) {
            }
        }
    }

    public void setAppState(AppState as) {
        this.appState = as;
    }

    public AppState getAppState() {
        return this.appState;
    }

    public void setScreenSize(int w, int h) {
        this.screenWidth = w;
        this.screenHeight = h;
    }

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public Vars() {
        this.updateTimer.schedule(new UpdateTimer(), 10, 50);
        this.buttonList = new ArrayList<>();
        this.buttonList.clear();
        this.lastMoves = new ArrayList<>();
        this.lastMoves.clear();
        this.zoomLevel = 100.0d;
    }

    public void addButton(gButton b) {
        this.buttonList.add(b);
    }

    public ArrayList<gButton> getButtonList() {
        return this.buttonList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public gButton getButton(int b) {
        if (b < 0 || b >= this.buttonList.size()) {
            return null;
        }
        return this.buttonList.get(b);
    }

    public void zoomIn() {
        this.zoomLevel += 10.0d;
        if (this.zoomLevel > 120.0d) {
            this.zoomLevel = 120.0d;
        }
    }

    public void zoomOut() {
        this.zoomLevel -= 10.0d;
        if (this.zoomLevel < 40.0d) {
            this.zoomLevel = 40.0d;
        }
    }

    public double getZoomLevel() {
        return this.zoomLevel / 100.0d;
    }

    public void hideAllButtons() {
        for (int i = 0; i < this.buttonList.size(); i++) {
            getButton(i).hide();
        }
    }

    public void undoMove() {
        PointMove pointMove = new PointMove();
        int index = this.lastMoves.size() - 1;
        if (index >= 0) {
            pointMove.pointIndex = this.lastMoves.get(index).pointIndex;
            pointMove.lastX = this.lastMoves.get(index).lastX;
            pointMove.lastY = this.lastMoves.get(index).lastY;
            this.lastMoves.remove(index);
            if (pointMove.pointIndex >= 0) {
                if (Levels.getInstance().getPoint(pointMove.pointIndex) != null) {
                    Levels.getInstance().getPoint(pointMove.pointIndex).x = pointMove.lastX;
                    Levels.getInstance().getPoint(pointMove.pointIndex).y = pointMove.lastY;
                }
                pointMove.pointIndex = -1;
                updateMenu();
                Levels.getInstance().checkIntersection();
            }
        }
    }

    public void updateMenu() {
        if (MainActivity.getInstance().getUndoMenuItem() != null) {
            MainActivity.getInstance().getUndoMenuItem().setTitle("Undo (" + this.lastMoves.size() + ")");
        }
    }

    public void saveLastMove(int i) {
        PointMove p = new PointMove();
        p.pointIndex = i;
        p.lastX = Levels.getInstance().getPoint(i).x;
        p.lastY = Levels.getInstance().getPoint(i).y;
        this.lastMoves.add(p);
        updateMenu();
    }

    public static Vars getInstance() {
        if (instance == null) {
            instance = new Vars();
        }
        return instance;
    }
}
