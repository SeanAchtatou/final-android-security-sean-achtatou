package softkos.untanglemeextreme;

import android.content.Context;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileWR {
    static FileWR instance = null;
    String FILENAME = "untangleme4";
    int file_pos = 0;
    String lastLevelFilename = "untangle_last_level";

    public static final byte[] intToByteArray(int value) {
        return new byte[]{(byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value};
    }

    public static final int byteArrayToInt(byte[] b) {
        return (b[0] << 24) + ((b[1] & 255) << 16) + ((b[2] & 255) << 8) + (b[3] & 255);
    }

    public void writeInt(FileOutputStream fos, int val) {
        try {
            fos.write(intToByteArray(val));
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.FileNotFoundException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void saveGame() {
        Context context = MainActivity.getInstance().getApplicationContext();
        try {
            FileOutputStream fos = context.openFileOutput(this.FILENAME, 0);
            for (int i = 0; i < 600; i++) {
                writeInt(fos, Levels.getInstance().isSolved(i));
            }
            try {
                fos.close();
            } catch (IOException e) {
                Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e);
            }
        } catch (FileNotFoundException e2) {
            Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e2);
        }
        try {
            FileOutputStream fos1 = context.openFileOutput(this.lastLevelFilename, 0);
            try {
                writeInt(fos1, Vars.getInstance().getLevel());
                fos1.close();
            } catch (IOException e3) {
            }
        } catch (FileNotFoundException e4) {
        }
    }

    public int getInt(byte[] buffer) {
        if (this.file_pos + 3 >= buffer.length) {
            return 0;
        }
        byte[] int_buf = {buffer[this.file_pos], buffer[this.file_pos + 1], buffer[this.file_pos + 2], buffer[this.file_pos + 3]};
        this.file_pos += 4;
        return byteArrayToInt(int_buf);
    }

    public String getString(byte[] buffer, int len) {
        byte[] tmp = new byte[len];
        for (int i = 0; i < len; i++) {
            tmp[i] = buffer[this.file_pos + i];
        }
        this.file_pos += len;
        return new String(tmp);
    }

    public void loadGame() {
        Context context = MainActivity.getInstance().getApplicationContext();
        try {
            FileInputStream fis = context.openFileInput(this.FILENAME);
            try {
                byte[] buffer = new byte[fis.available()];
                fis.read(buffer);
                if (buffer.length == 2400) {
                    for (int i = 0; i < 500; i++) {
                        if (getInt(buffer) == 1) {
                            Levels.getInstance().setSolved(i);
                        }
                    }
                }
                fis.close();
            } catch (IOException e) {
            }
            this.file_pos = 0;
            try {
                FileInputStream fis1 = context.openFileInput(this.lastLevelFilename);
                try {
                    byte[] buffer2 = new byte[fis1.available()];
                    fis1.read(buffer2);
                    Vars.getInstance().setLevel(getInt(buffer2));
                    fis1.close();
                } catch (IOException e2) {
                }
            } catch (FileNotFoundException e3) {
            }
        } catch (FileNotFoundException e4) {
        }
    }

    public static FileWR getInstance() {
        if (instance == null) {
            instance = new FileWR();
        }
        return instance;
    }
}
