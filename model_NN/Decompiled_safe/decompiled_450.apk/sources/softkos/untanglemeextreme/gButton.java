package softkos.untanglemeextreme;

import android.graphics.Canvas;
import android.graphics.Paint;

public class gButton {
    boolean back_images_disabled;
    boolean enabled;
    boolean hasKeyboardFocus;
    int height;
    int id;
    String image;
    ImageLoader imgInst;
    boolean is_pressed;
    boolean mouseDownVal;
    PaintManager paintMgr;
    int px;
    int py;
    String text;
    boolean visible;
    int width;

    public void setEnabled(boolean e) {
        this.enabled = e;
    }

    public void disableBackImages() {
        this.back_images_disabled = true;
    }

    public void setKeyboardFocus(boolean f) {
        this.hasKeyboardFocus = f;
    }

    public boolean getKeyboardFocus() {
        return this.hasKeyboardFocus;
    }

    public void setImage(String img) {
        this.image = img;
    }

    public gButton() {
        this.imgInst = null;
        this.image = null;
        this.enabled = true;
        this.back_images_disabled = false;
        this.is_pressed = false;
        this.mouseDownVal = false;
        this.text = "";
        this.id = -1;
        this.image = null;
        this.imgInst = ImageLoader.getInstance();
        this.paintMgr = PaintManager.getInstance();
    }

    public void setText(String t) {
        this.text = t;
    }

    public String getText() {
        return this.text;
    }

    public void setId(int i) {
        this.id = i;
    }

    public int getId() {
        return this.id;
    }

    public void show() {
        this.visible = true;
    }

    public void hide() {
        this.visible = false;
    }

    public void setPosition(int x, int y) {
        this.px = x;
        this.py = y;
    }

    public void setSize(int w, int h) {
        this.width = w;
        this.height = h;
    }

    public int getPx() {
        return this.px;
    }

    public int getPy() {
        return this.py;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public void draw(Canvas g, Paint p) {
        if (this.visible) {
            if (!this.back_images_disabled) {
                if (!this.is_pressed || !this.mouseDownVal) {
                    this.paintMgr.drawImage(g, "button_off", this.px, this.py, this.width, this.height);
                } else {
                    this.paintMgr.drawImage(g, "button_on", this.px, this.py, this.width, this.height);
                }
            }
            if (this.image != null) {
                this.paintMgr.drawImage(g, this.image, this.px, this.py, this.width, this.height);
            }
            p.setAntiAlias(true);
            p.setTextSize(24.0f);
            if (!this.is_pressed || !this.mouseDownVal) {
                p.setColor(-16777216);
            } else {
                p.setColor(-1);
            }
            g.drawText(this.text, ((float) (this.px + (this.width / 2))) - (p.measureText(this.text) / 2.0f), (((float) (this.py + (this.height / 2))) + (p.getTextSize() / 2.0f)) - 2.0f, p);
        }
    }

    public int mouseDrag(int x, int y) {
        if (!this.visible || !this.enabled) {
            return 0;
        }
        if (x <= this.px || y <= this.py || x >= this.px + this.width || y >= this.py + this.height) {
            this.is_pressed = false;
            return 0;
        }
        this.is_pressed = true;
        return 1;
    }

    public int mouseUp(int x, int y) {
        if (!this.visible || !this.enabled) {
            return 0;
        }
        int val = 0;
        if (x > this.px && y > this.py && x < this.px + this.width && y < this.py + this.height && this.is_pressed && this.mouseDownVal) {
            val = 1;
        }
        this.is_pressed = false;
        this.mouseDownVal = false;
        return val;
    }

    public int mouseDown(int x, int y) {
        if (!this.visible || !this.enabled) {
            return 0;
        }
        if (x <= this.px || y <= this.py || x >= this.px + this.width || y >= this.py + this.height) {
            return 0;
        }
        this.is_pressed = true;
        this.mouseDownVal = true;
        return 1;
    }
}
