package softkos.untanglemeextreme;

public class Line {
    public int end_index;
    boolean intersected = false;
    public int start_index;

    public Line(int i1, int i2) {
        this.start_index = i1;
        this.end_index = i2;
    }

    public void setIntersected(boolean i) {
        this.intersected = i;
    }

    public boolean isIntersected() {
        return this.intersected;
    }
}
