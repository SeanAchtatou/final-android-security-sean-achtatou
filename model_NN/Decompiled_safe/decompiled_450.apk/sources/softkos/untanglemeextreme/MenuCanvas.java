package softkos.untanglemeextreme;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import com.heyzap.sdk.HeyzapLib;

public class MenuCanvas extends View {
    static MenuCanvas instance = null;
    gButton facebookBtn = null;
    gButton heyzapBtn = null;
    gButton howToPlayBtn = null;
    gButton mobilsoftBtn = null;
    gButton otherAppBtn = null;
    Paint paint = new Paint();
    PaintManager paintMgr;
    gButton startGame = null;
    gButton twitterBtn = null;
    Vars vars;

    public MenuCanvas(Context c) {
        super(c);
        instance = this;
        this.vars = Vars.getInstance();
        this.paintMgr = PaintManager.getInstance();
        initUI();
    }

    public void initUI() {
        int btn_w = 248;
        int btn_h = 48;
        if (this.vars.getScreenWidth() > 400) {
            btn_w = 340;
            btn_h = 68;
        }
        this.startGame = new gButton();
        this.startGame.setSize(btn_w, btn_h);
        this.startGame.setId(Vars.getInstance().START_GAME);
        this.startGame.setText("Start");
        Vars.getInstance().addButton(this.startGame);
        this.howToPlayBtn = new gButton();
        this.howToPlayBtn.setSize(btn_w, btn_h);
        this.howToPlayBtn.setId(Vars.getInstance().HOW_TO_PLAY);
        this.howToPlayBtn.setText("How to play");
        Vars.getInstance().addButton(this.howToPlayBtn);
        this.otherAppBtn = new gButton();
        this.otherAppBtn.setSize(btn_w, btn_h);
        this.otherAppBtn.setId(Vars.getInstance().OTHER_APP);
        this.otherAppBtn.setText("Try Frogs Jump");
        Vars.getInstance().addButton(this.otherAppBtn);
        updateButtonText();
        this.mobilsoftBtn = new gButton();
        this.mobilsoftBtn.setSize(btn_w, btn_h);
        this.mobilsoftBtn.setId(Vars.getInstance().GO_TO_MOBILSOFT);
        this.mobilsoftBtn.setText("More free apps");
        Vars.getInstance().addButton(this.mobilsoftBtn);
        this.twitterBtn = new gButton();
        this.twitterBtn.setSize(btn_w, btn_h);
        this.twitterBtn.setId(Vars.getInstance().TWITTER);
        this.twitterBtn.setText("Share with friend!");
        Vars.getInstance().addButton(this.twitterBtn);
        this.heyzapBtn = new gButton();
        this.heyzapBtn.setSize(btn_w, btn_h);
        this.heyzapBtn.setId(Vars.getInstance().HEYZAP_MENU);
        this.heyzapBtn.setText("Checkin           ");
        this.heyzapBtn.setImage("heyzap_menu");
        Vars.getInstance().addButton(this.heyzapBtn);
    }

    public void updateButtonText() {
        this.otherAppBtn.setText(OtherApp.getInstance().button_text[OtherApp.getInstance().current]);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        layoutUI();
    }

    public void showUI(boolean s) {
        Vars.getInstance().hideAllButtons();
        if (s) {
            this.mobilsoftBtn.show();
            this.otherAppBtn.show();
            this.startGame.show();
            this.twitterBtn.show();
            this.howToPlayBtn.show();
            this.heyzapBtn.show();
            return;
        }
        this.mobilsoftBtn.hide();
        this.otherAppBtn.hide();
        this.startGame.hide();
        this.twitterBtn.hide();
        this.howToPlayBtn.hide();
        this.heyzapBtn.hide();
    }

    public void layoutUI() {
        int y = (int) (((double) getHeight()) - (((double) this.startGame.getHeight()) * 7.2d));
        int x = (getWidth() / 2) - (this.startGame.getWidth() / 2);
        int spacing = (int) (((double) this.mobilsoftBtn.getHeight()) * 1.02d);
        int y2 = y + spacing;
        this.startGame.setPosition(x, y2);
        int y3 = y2 + spacing;
        this.otherAppBtn.setPosition(x, y3);
        int y4 = y3 + spacing;
        this.howToPlayBtn.setPosition(x, y4);
        int y5 = y4 + spacing;
        this.twitterBtn.setPosition(x, y5);
        int y6 = y5 + spacing;
        this.heyzapBtn.setPosition(x, y6);
        this.mobilsoftBtn.setPosition(x, y6 + spacing);
    }

    public void mouseDown(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                invalidate();
                return;
            }
        }
    }

    public void mouseDrag(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
            if (pressed != 0) {
                invalidate();
                return;
            }
        }
    }

    public void handleCommand(int id) {
        if (id == Vars.getInstance().START_GAME) {
            MainActivity.getInstance().showGame();
        } else if (id == Vars.getInstance().GO_TO_MOBILSOFT) {
            MainActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.mobilsoft.pl")));
        } else if (id == Vars.getInstance().HOW_TO_PLAY) {
            MainActivity.getInstance().showHowToPlay();
        } else if (id == Vars.getInstance().OTHER_APP) {
            try {
                MainActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(OtherApp.getInstance().urls[OtherApp.getInstance().current])));
            } catch (Exception e) {
            }
        } else if (id == Vars.getInstance().TWITTER) {
            try {
                Intent defineIntent = new Intent("android.intent.action.VIEW");
                defineIntent.putExtra("sms_body", "Checkout great android game! Link: http://market.android.com/details?id=softkos.untanglemeextreme");
                defineIntent.setData(Uri.parse("content://mms-sms/conversations/" + -1L));
                MainActivity.getInstance().startActivity(defineIntent);
            } catch (Exception e2) {
            }
        } else if (id == Vars.getInstance().HEYZAP_MENU) {
            HeyzapLib.checkin(MainActivity.getInstance());
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        invalidate();
        return true;
    }

    public void drawBack(Canvas canvas) {
        int c1 = Color.rgb(33, 49, 74);
        int c2 = Color.rgb(43, 59, 84);
        if (Settings.getInstnace().getIntSettings(Settings.GAME_BACK_INDEX) == 1) {
            c1 = Color.rgb(122, 89, 63);
            c2 = Color.rgb(163, 121, 91);
        }
        if (Settings.getInstnace().getIntSettings(Settings.GAME_BACK_INDEX) == 2) {
            c1 = Color.rgb(87, 135, 86);
            c2 = Color.rgb(62, 97, 62);
        }
        if (Settings.getInstnace().getIntSettings(Settings.GAME_BACK_INDEX) == 3) {
            c1 = Color.rgb(200, 200, 200);
            c2 = Color.rgb(240, 240, 240);
        }
        int size = getWidth() / 5;
        int curr_col = c1;
        for (int y = 0; y < getHeight(); y += size) {
            for (int x = 0; x < getWidth(); x += size) {
                if (curr_col == c1) {
                    curr_col = c2;
                } else {
                    curr_col = c1;
                }
                this.paintMgr.setColor(curr_col);
                this.paintMgr.fillRectangle(canvas, x, y, size, size);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paint.setAntiAlias(true);
        drawBack(canvas);
        if (Settings.getInstnace().getIntSettings(Settings.GAME_BACK_INDEX) == 3) {
            this.paintMgr.drawImage(canvas, "menuimage_inv", 0, 0, getWidth(), getWidth());
        } else {
            this.paintMgr.drawImage(canvas, "menuimage", 0, 0, getWidth(), getWidth());
        }
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            Vars.getInstance().getButton(i).draw(canvas, this.paint);
        }
        String img = OtherApp.getInstance().images[OtherApp.getInstance().current];
        this.paintMgr.drawImage(canvas, img, this.otherAppBtn.getPx() + 5, this.otherAppBtn.getPy() + 6, this.otherAppBtn.getHeight() - 12, this.otherAppBtn.getHeight() - 12);
    }

    public static MenuCanvas getInstance() {
        return instance;
    }
}
