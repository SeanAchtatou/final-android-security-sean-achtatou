package softkos.untanglemeextreme;

import android.graphics.Point;

public class GamePoint extends Point {
    boolean locked;

    public GamePoint() {
        this.locked = false;
    }

    public GamePoint(int xx, int yy) {
        this.x = xx;
        this.y = yy;
    }

    public boolean isLocked() {
        return this.locked;
    }

    public void setLocked() {
        this.locked = true;
    }
}
