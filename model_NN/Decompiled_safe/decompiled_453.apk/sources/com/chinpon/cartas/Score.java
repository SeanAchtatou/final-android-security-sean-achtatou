package com.chinpon.cartas;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;

public class Score extends Graphic {
    private static Score score = null;
    int puntuacion = 0;

    public static Score getInstance(Bitmap bitmap) {
        if (score == null) {
            score = new Score(bitmap);
        }
        score.reset();
        return score;
    }

    private Score(Bitmap bitmap) {
        super(bitmap);
    }

    public void pintar(Canvas canvas) {
        Paint paint = new Paint();
        paint.setTextSize(30.0f);
        paint.setAntiAlias(true);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3.0f);
        paint.setColor(-16777216);
        canvas.drawText(Integer.toString(this.puntuacion), 219.0f, 100.0f, paint);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-16711936);
        canvas.drawText(Integer.toString(this.puntuacion), 219.0f, 100.0f, paint);
        canvas.drawBitmap(getBitmap(), 175.0f, 68.0f, (Paint) null);
    }

    public void incScore(int inc) {
        this.puntuacion += inc;
    }

    public void reset() {
        this.puntuacion = 0;
    }

    public int getScore() {
        return this.puntuacion;
    }
}
