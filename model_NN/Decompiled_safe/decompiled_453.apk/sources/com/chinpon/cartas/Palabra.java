package com.chinpon.cartas;

public class Palabra {
    private String equivalencia;
    private boolean idiomaAAprender;
    private String palabra;

    public Palabra(String palabra2, String equivalencia2, boolean idiomaAAprender2) {
        this.palabra = palabra2;
        this.equivalencia = equivalencia2;
        this.idiomaAAprender = idiomaAAprender2;
    }

    public boolean isEquivalente(Palabra palabra2) {
        if (this.palabra.equals(palabra2.equivalencia)) {
            return true;
        }
        return false;
    }

    public String getPalabra() {
        return this.palabra;
    }

    public boolean isIdiomaAAprender() {
        return this.idiomaAAprender;
    }
}
