package com.chinpon.cartas;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;

public class Cronometro extends Graphic {
    private static Cronometro cronometro = null;
    private int aux = 0;
    private int retardo = 5;
    private boolean stop = false;
    private int tiempo;
    private int tiempoTotal;

    public static Cronometro getInstance(Bitmap bitmap, int aTiempo) {
        if (cronometro == null) {
            cronometro = new Cronometro(bitmap, aTiempo);
        }
        cronometro.reset();
        return cronometro;
    }

    private Cronometro(Bitmap bitmap, int aTiempo) {
        super(bitmap);
        play();
        this.tiempoTotal = aTiempo;
        this.tiempo = this.tiempoTotal;
    }

    public void pintar(Canvas canvas) {
        if (!this.stop) {
            descScore(1);
        }
        Paint paint = new Paint();
        paint.setTextSize(30.0f);
        paint.setAntiAlias(true);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setColor(-16777216);
        paint.setStrokeWidth(3.0f);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawText(Integer.toString(this.tiempo), 65.0f, 100.0f, paint);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-65536);
        canvas.drawText(Integer.toString(this.tiempo), 65.0f, 100.0f, paint);
        canvas.drawBitmap(getBitmap(), 20.0f, 65.0f, (Paint) null);
    }

    public void descScore(int dec) {
        if (this.tiempo > 0) {
            if (this.aux > this.retardo) {
                this.aux = 0;
                this.tiempo -= dec;
            }
            this.aux++;
        }
    }

    public void incTiempo(int inc) {
        this.tiempo += inc;
    }

    public void stop() {
        this.stop = true;
    }

    public void play() {
        this.stop = false;
    }

    public void reset() {
        this.tiempo = this.tiempoTotal;
    }

    public int getTiempo() {
        return this.tiempo;
    }
}
