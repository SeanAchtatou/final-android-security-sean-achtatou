package com.chinpon.cartas;

import android.graphics.Bitmap;

public class Graphic {
    /* access modifiers changed from: private */
    public Bitmap _bitmap;
    private Coordinates _coordinates;
    private int _explosionStep = 0;
    private Speed _speed;
    private String _type;

    public class Speed {
        private int _x = 1;
        private int _y = 1;

        public Speed() {
        }

        public int getX() {
            return this._x;
        }

        public void setX(int speed) {
            this._x = speed;
        }

        public int getY() {
            return this._y;
        }

        public void setY(int speed) {
            this._y = speed;
        }

        public String toString() {
            return "Speed: x: " + this._x + " | y: " + this._y;
        }
    }

    public class Coordinates {
        private int _x = 0;
        private int _y = 0;

        public Coordinates() {
        }

        public int getX() {
            return this._x;
        }

        public int getTouchedX() {
            return this._x + (Graphic.this._bitmap.getWidth() / 2);
        }

        public void setX(int value) {
            this._x = value;
        }

        public void setTouchedX(int value) {
            this._x = value - (Graphic.this._bitmap.getWidth() / 2);
        }

        public int getY() {
            return this._y;
        }

        public int getTouchedY() {
            return this._y + (Graphic.this._bitmap.getHeight() / 2);
        }

        public void setY(int value) {
            this._y = value;
        }

        public void setTouchedY(int value) {
            this._y = value - (Graphic.this._bitmap.getHeight() / 2);
        }

        public String toString() {
            return "Coordinates: (" + this._x + "/" + this._y + ")";
        }
    }

    public Graphic(Bitmap bitmap) {
        this._bitmap = bitmap;
        this._coordinates = new Coordinates();
        this._speed = new Speed();
    }

    public void setBitmap(Bitmap bitmap) {
        this._bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return this._bitmap;
    }

    public Speed getSpeed() {
        return this._speed;
    }

    public Coordinates getCoordinates() {
        return this._coordinates;
    }

    public void setType(String type) {
        this._type = type;
    }

    public String getType() {
        return this._type;
    }

    public void setExplosionStep(int step) {
        this._explosionStep = step;
    }

    public int getExplosionStep() {
        return this._explosionStep;
    }
}
