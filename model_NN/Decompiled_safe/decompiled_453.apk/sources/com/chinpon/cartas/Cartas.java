package com.chinpon.cartas;

import android.app.Activity;
import android.os.Bundle;
import android.widget.RelativeLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.util.Observable;
import java.util.Observer;

public class Cartas extends Activity implements Observer {
    private String MY_AD_UNIT_ID = "a14e46993025d19";
    private AdView adView;
    private Panel panel = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.panel = new Panel(this);
        AdRequest adRequest = new AdRequest();
        this.adView = new AdView(this, AdSize.BANNER, this.MY_AD_UNIT_ID);
        this.adView.loadAd(adRequest);
        System.out.println(this.adView);
        if (this.adView != null) {
            this.adView.setVisibility(0);
        }
        RelativeLayout layout = new RelativeLayout(this);
        layout.addView(this.panel);
        layout.addView(this.adView);
        setContentView(layout);
    }

    public void onPause() {
        super.onPause();
        this.panel.pausarPartida();
    }

    public void onResume() {
        super.onResume();
        this.panel.restaurarPartida();
    }

    public void update(Observable arg0, Object object) {
        if (object instanceof Integer) {
            switch (((Integer) object).intValue()) {
                case Constantes.GLOBAL_CALL_SALIR_JUEGO:
                    setResult(-1);
                    finish();
                    return;
                case Constantes.GLOBAL_CALL_INICIAR_PARTIDA:
                    this.panel.comenzarPartida();
                    return;
                default:
                    return;
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        System.runFinalizersOnExit(true);
        System.exit(0);
    }
}
