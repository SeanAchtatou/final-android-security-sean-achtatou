package com.chinpon.cartas;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import com.chinpon.cartas.Graphic;

public class Carta extends Graphic {
    private final int ESTADO_ANIMACION_CUBRIENDO_CARTA = 5;
    private final int ESTADO_ANIMACION_DESCUBRIENDO_CARTA = 4;
    private final int ESTADO_CANDIDATA = 2;
    private final int ESTADO_ENCONTRADA = 3;
    private final int ESTADO_INICIAL = 1;
    private int anchura = 0;
    private int aux = 0;
    private Bitmap[] bitmap;
    private boolean descubriendose = true;
    private int estadoActual = 1;
    private boolean mostrarPalabra;
    Palabra palabra;
    private Partida partida;
    private int retardo = 1;
    private float tiempo = 1.0f;

    public Palabra getPalabra() {
        return this.palabra;
    }

    public void setPalabra(Palabra palabra2) {
        this.palabra = palabra2;
    }

    public Carta(Bitmap[] bitmap2, Palabra aPalabra, Partida aPartida) {
        super(null);
        this.bitmap = bitmap2;
        this.palabra = aPalabra;
        this.partida = aPartida;
        if (aPalabra.isIdiomaAAprender()) {
            setBitmap(bitmap2[1]);
        } else {
            setBitmap(bitmap2[0]);
        }
    }

    public void checkSelected(int x, int y) {
        if (x < getCoordinates().getX() + getBitmap().getWidth() && x > getCoordinates().getX() && y < getCoordinates().getY() + getBitmap().getHeight() && y > getCoordinates().getY()) {
            doEvent();
        }
    }

    public void doEvent() {
        switch (this.estadoActual) {
            case Constantes.GLOBAL_CALL_SALIR_JUEGO /*1*/:
                this.estadoActual = 4;
                repintar();
                return;
            default:
                return;
        }
    }

    public boolean isCandidata() {
        if (this.estadoActual == 2) {
            return true;
        }
        return false;
    }

    public boolean isCompletada() {
        if (this.estadoActual == 3) {
            return true;
        }
        return false;
    }

    public void completado() {
        this.estadoActual = 3;
        repintar();
    }

    public void candidata() {
        this.estadoActual = 2;
        repintar();
    }

    public void reset() {
        this.estadoActual = 5;
        repintar();
    }

    public void estadoInicial() {
        this.estadoActual = 1;
        repintar();
    }

    private Bitmap getImagenPorTipoCarta() {
        if (this.palabra.isIdiomaAAprender()) {
            return this.bitmap[1];
        }
        return this.bitmap[0];
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void repintar() {
        switch (this.estadoActual) {
            case Constantes.GLOBAL_CALL_SALIR_JUEGO /*1*/:
                setBitmap(getImagenPorTipoCarta());
                this.mostrarPalabra = false;
                return;
            case Constantes.GLOBAL_CALL_INICIAR_PARTIDA /*2*/:
                setBitmap(this.bitmap[2]);
                this.mostrarPalabra = true;
                this.partida.checkEquivalencias();
                return;
            case 3:
                setBitmap(this.bitmap[3]);
                this.mostrarPalabra = true;
                return;
            case 4:
                this.mostrarPalabra = false;
                break;
            case 5:
                break;
            default:
                return;
        }
        this.mostrarPalabra = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void pintar(Canvas canvas) {
        Graphic.Coordinates coords = getCoordinates();
        Paint paint = new Paint();
        Bitmap scaled = getBitmap();
        Matrix mat = new Matrix();
        switch (this.estadoActual) {
            case 3:
                Paint paint2 = new Paint();
                paint2.setColorFilter(new LightingColorFilter(-286331137, 286331391));
                canvas.drawBitmap(getBitmap(), (float) coords.getX(), (float) coords.getY(), paint2);
                paint.setColor(-16777216);
                paint.setTextSize(17.0f);
                paint.setAntiAlias(true);
                if (this.mostrarPalabra) {
                    paint.setTextAlign(Paint.Align.CENTER);
                    canvas.drawText(getPalabra().getPalabra(), (float) (coords.getX() + 55), (float) (coords.getY() + 44), paint);
                    return;
                }
                return;
            case 4:
                mat.setScale(this.tiempo, 1.0f, (float) (getBitmap().getWidth() / 2), (float) (getBitmap().getHeight() / 2));
                mat.postTranslate((float) coords.getX(), (float) coords.getY());
                canvas.drawBitmap(Bitmap.createBitmap(scaled, 0, 0, getBitmap().getWidth(), getBitmap().getHeight(), mat, true), mat, null);
                if (this.descubriendose) {
                    descTamano(0.25f);
                    if (this.tiempo == 0.25f) {
                        setBitmap(this.bitmap[2]);
                        this.descubriendose = false;
                        return;
                    }
                    return;
                }
                incTamano(0.25f);
                if (this.tiempo == 1.0f) {
                    this.descubriendose = true;
                    candidata();
                    return;
                }
                return;
            case 5:
                mat.setScale(this.tiempo, 1.0f, (float) (getBitmap().getWidth() / 2), (float) (getBitmap().getHeight() / 2));
                mat.postTranslate((float) coords.getX(), (float) coords.getY());
                canvas.drawBitmap(Bitmap.createBitmap(scaled, 0, 0, getBitmap().getWidth(), getBitmap().getHeight(), mat, true), mat, null);
                if (this.descubriendose) {
                    descTamano(0.25f);
                    if (this.tiempo == 0.25f) {
                        setBitmap(getImagenPorTipoCarta());
                        this.descubriendose = false;
                        return;
                    }
                    return;
                }
                incTamano(0.25f);
                if (this.tiempo == 1.0f) {
                    this.descubriendose = true;
                    estadoInicial();
                    return;
                }
                return;
            default:
                canvas.drawBitmap(getBitmap(), (float) coords.getX(), (float) coords.getY(), (Paint) null);
                paint.setColor(-16777216);
                paint.setTextSize(17.0f);
                paint.setAntiAlias(true);
                if (this.mostrarPalabra) {
                    paint.setTextAlign(Paint.Align.CENTER);
                    canvas.drawText(getPalabra().getPalabra(), (float) (coords.getX() + 55), (float) (coords.getY() + 44), paint);
                    return;
                }
                return;
        }
    }

    public void incTamano(float dec) {
        if (this.tiempo < 1.0f) {
            this.tiempo += dec;
        }
    }

    public void descTamano(float dec) {
        if (this.tiempo > 0.0f + dec) {
            this.tiempo -= dec;
        }
    }
}
