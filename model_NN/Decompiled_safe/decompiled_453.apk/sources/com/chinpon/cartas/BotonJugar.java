package com.chinpon.cartas;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import com.chinpon.cartas.Graphic;

public class BotonJugar extends Graphic {
    private Partida partida = null;
    private String texto = null;

    public BotonJugar(Bitmap bitmap, String texto2, Partida partida2) {
        super(bitmap);
        getCoordinates().setX(27);
        getCoordinates().setY(220);
        this.partida = partida2;
        this.texto = texto2;
    }

    public void pintar(Canvas canvas) {
        Graphic.Coordinates coords = getCoordinates();
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextSize(22.0f);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setColor(-1);
        paint.setTextAlign(Paint.Align.CENTER);
        canvas.drawBitmap(getBitmap(), (float) coords.getX(), (float) coords.getY(), paint);
        canvas.drawText(this.texto, (float) (coords.getX() + 137), (float) (coords.getY() + 43), paint);
    }

    public void checkSelected(int x, int y) {
        if (x < getCoordinates().getX() + getBitmap().getWidth() && x > getCoordinates().getX() && y < getCoordinates().getY() + getBitmap().getHeight() && y > getCoordinates().getY()) {
            doEvent();
        }
    }

    public void doEvent() {
        this.partida.iniciarJuego();
    }
}
