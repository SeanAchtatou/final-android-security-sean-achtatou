package com.chinpon.cartas;

import android.graphics.Canvas;

class MainThread extends Thread {
    private Panel _panel;
    private boolean _run = false;
    private long delay = 60;
    private long sleepTime;

    public MainThread(Panel panel) {
        this._panel = panel;
    }

    public void setRunning(boolean run) {
        this._run = run;
    }

    public boolean isRunning() {
        return this._run;
    }

    public void run() {
        while (this._run) {
            Canvas c = null;
            try {
                c = this._panel.getHolder().lockCanvas(null);
                synchronized (this._panel.getHolder()) {
                    long beforeTime = System.nanoTime();
                    this._panel.onDraw(c);
                    this.sleepTime = this.delay - ((System.nanoTime() - beforeTime) / 1000000);
                    try {
                        if (this.sleepTime > 0) {
                            Thread.sleep(this.sleepTime);
                        }
                    } catch (InterruptedException e) {
                    }
                }
            } finally {
                if (c != null) {
                    this._panel.getHolder().unlockCanvasAndPost(c);
                }
            }
        }
    }
}
