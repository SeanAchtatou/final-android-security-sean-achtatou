package com.chinpon.cartas;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import java.util.Map;

public class MenuInicio {
    private Map<Integer, Bitmap> _bitmapCache;
    private float animacion;
    private float animacion2;
    private BotonJugar botonJugar;
    private BotonSalir botonSalir;
    private Panel panel = null;
    private Partida partida = null;

    public MenuInicio(Panel aPanel, Partida aPartida) {
        this.panel = aPanel;
        this._bitmapCache = aPanel.getBitmapCache();
        this.partida = aPartida;
        this.botonJugar = new BotonJugar(this._bitmapCache.get(Integer.valueOf((int) R.drawable.boton_reintentar)), "JUGAR", this.partida);
        this.botonSalir = new BotonSalir(this._bitmapCache.get(Integer.valueOf((int) R.drawable.boton_salir)), this.partida);
    }

    public void activarMenuFin() {
        this.animacion = 1.0f;
        this.animacion2 = 0.0f;
    }

    public void pintar(Canvas canvas) {
        canvas.drawBitmap(this._bitmapCache.get(Integer.valueOf((int) R.drawable.fondo_inicio)), 0.0f, 0.0f, (Paint) null);
        this.botonJugar.pintar(canvas);
        this.botonSalir.pintar(canvas);
    }

    public void onTouchEvent(MotionEvent event) {
        if (event.getAction() == 0) {
            this.botonJugar.checkSelected((int) event.getX(), (int) event.getY());
            this.botonSalir.checkSelected((int) event.getX(), (int) event.getY());
        }
    }
}
