package com.chinpon.cartas;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.media.SoundPool;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.chinpon.cartas.Graphic;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Panel extends SurfaceView implements SurfaceHolder.Callback {
    private final int ESTADO_INICIO_PARTIDA = 1;
    private final int ESTADO_PRESENTACION = 2;
    private Map<Integer, Bitmap> _bitmapCache = new HashMap();
    private Graphic _currentGraphic;
    private ArrayList<Graphic> _explosions = new ArrayList<>();
    private ArrayList<Graphic> _graphics = new ArrayList<>();
    private Graphic.Coordinates _lastCoords;
    private int _playbackFile = 0;
    private SoundPool _soundPool;
    private MainThread _thread;
    private int estadoActual = 2;
    MenuInicio menuInicio = null;
    Partida partida = null;

    public Map<Integer, Bitmap> getBitmapCache() {
        return this._bitmapCache;
    }

    public Panel(Context context) {
        super(context);
        fillBitmapCache();
        this.partida = Partida.getInstance(this);
        this.menuInicio = new MenuInicio(this, this.partida);
        getHolder().addCallback(this);
        this._thread = new MainThread(this);
        setFocusable(true);
    }

    public void comenzarPartida() {
        this.estadoActual = 1;
        this.partida.comenzarPartida();
    }

    private void fillBitmapCache() {
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.carta_volteada), BitmapFactory.decodeResource(getResources(), R.drawable.carta_volteada));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.carta_volteada2), BitmapFactory.decodeResource(getResources(), R.drawable.carta_volteada2));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.fondo_tori), BitmapFactory.decodeResource(getResources(), R.drawable.fondo_tori));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.fondo_castillo), BitmapFactory.decodeResource(getResources(), R.drawable.fondo_castillo));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.fondo_inicio), BitmapFactory.decodeResource(getResources(), R.drawable.fondo_inicio));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.carta), BitmapFactory.decodeResource(getResources(), R.drawable.carta));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.clock_icon), BitmapFactory.decodeResource(getResources(), R.drawable.clock_icon));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.puntos_icon), BitmapFactory.decodeResource(getResources(), R.drawable.puntos_icon));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.boton_continuar), BitmapFactory.decodeResource(getResources(), R.drawable.boton_continuar));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.boton_reintentar), BitmapFactory.decodeResource(getResources(), R.drawable.boton_reintentar));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.boton_salir), BitmapFactory.decodeResource(getResources(), R.drawable.boton_salir));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.boton_coincide), BitmapFactory.decodeResource(getResources(), R.drawable.boton_coincide));
        this._bitmapCache.put(Integer.valueOf((int) R.drawable.boton_error), BitmapFactory.decodeResource(getResources(), R.drawable.boton_error));
    }

    public boolean onTouchEvent(MotionEvent event) {
        synchronized (getHolder()) {
            switch (this.estadoActual) {
                case Constantes.GLOBAL_CALL_SALIR_JUEGO:
                    this.partida.onTouchEvent(event);
                    break;
                case Constantes.GLOBAL_CALL_INICIAR_PARTIDA:
                    this.menuInicio.onTouchEvent(event);
                    break;
            }
        }
        return true;
    }

    public void onDraw(Canvas canvas) {
        switch (this.estadoActual) {
            case Constantes.GLOBAL_CALL_SALIR_JUEGO:
                this.partida.pintar(canvas);
                return;
            case Constantes.GLOBAL_CALL_INICIAR_PARTIDA:
                this.menuInicio.pintar(canvas);
                return;
            default:
                return;
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (!this._thread.isAlive()) {
            this._thread = new MainThread(this);
        }
        this._thread.setRunning(true);
        this._thread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        this._thread.setRunning(false);
        while (retry) {
            try {
                this._thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
        Log.i("thread", "Thread terminated...");
    }

    public int getEstadoActual() {
        return this.estadoActual;
    }

    public void setEstadoActual(int estadoActual2) {
        this.estadoActual = estadoActual2;
    }

    public void pausarPartida() {
        this.partida.pausarPartida();
    }

    public void restaurarPartida() {
        this.partida.restaurarPartida();
    }
}
