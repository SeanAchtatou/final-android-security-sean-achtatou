package com.chinpon.cartas;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import com.chinpon.cartas.Graphic;
import java.util.List;

public class BotonContinuar extends Graphic {
    private static BotonContinuar botonContinuar;
    private final int TIEMPO_DE_RESPUESTA = 3;
    private float animacion = 1.0f;
    private float animacion2 = 0.0f;
    private int aux = 0;
    List<Carta> cartasCandidatas = null;
    private Partida partida = null;
    private int retardo = 10;
    private int tiempo = 3;

    public static BotonContinuar getInstance(Bitmap bitmap, Partida partida2) {
        if (botonContinuar == null) {
            botonContinuar = new BotonContinuar(bitmap, partida2);
        }
        return botonContinuar;
    }

    private BotonContinuar(Bitmap bitmap, Partida partida2) {
        super(bitmap);
        this.partida = partida2;
        getCoordinates().setX(28);
        getCoordinates().setY(220);
    }

    public void pintar(Canvas canvas) {
        Graphic.Coordinates coords = getCoordinates();
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextSize(22.0f);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setColor(-1);
        canvas.drawBitmap(getBitmap(), (float) coords.getX(), (float) coords.getY(), paint);
        canvas.drawText("CONTINUAR", (float) (coords.getX() + 70), (float) (coords.getY() + 43), paint);
        paint.setTextSize(this.animacion);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        if (this.animacion < 60.0f) {
            this.animacion2 += 5.0f;
            this.animacion += 10.0f;
        }
        paint.setColor(-16777216);
        paint.setStrokeWidth(3.0f);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawText("Genial!!", 160.0f, this.animacion2 + 170.0f, paint);
        paint.setColor(-65281);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawText("Genial!!", 160.0f, this.animacion2 + 170.0f, paint);
    }

    public void checkSelected(int x, int y) {
        if (x < getCoordinates().getX() + getBitmap().getWidth() && x > getCoordinates().getX() && y < getCoordinates().getY() + getBitmap().getHeight() && y > getCoordinates().getY()) {
            doEvent();
        }
    }

    public void doEvent() {
        this.partida.continuarSiguienteFase();
    }
}
