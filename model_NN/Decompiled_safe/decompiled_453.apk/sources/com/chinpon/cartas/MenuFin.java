package com.chinpon.cartas;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;

public class MenuFin extends Graphic {
    private static MenuFin menuFin;
    private float animacion;
    private float animacion2;
    private Bitmap[] bitmaps;
    private BotonJugar botonJugar;
    private BotonSalir botonSalir;
    private Partida partida;

    public static MenuFin getInstance(Bitmap[] bitmaps2, Partida partida2) {
        if (menuFin == null) {
            menuFin = new MenuFin(bitmaps2, partida2);
        }
        return menuFin;
    }

    private MenuFin(Bitmap[] bitmaps2, Partida partida2) {
        super(bitmaps2[0]);
        this.botonJugar = new BotonJugar(bitmaps2[0], "REINTENTAR", partida2);
        this.botonSalir = new BotonSalir(bitmaps2[1], partida2);
        this.bitmaps = bitmaps2;
        this.partida = partida2;
        activarMenuFin();
    }

    public void activarMenuFin() {
        this.animacion = 1.0f;
        this.animacion2 = 0.0f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public void pintar(Canvas canvas) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(-65281);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setDither(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3.0f);
        paint.setTextSize(this.animacion);
        paint.setColor(-16777216);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawText("SE ACABÓ!!", 158.0f, 80.0f + this.animacion2, paint);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-16711936);
        canvas.drawText("SE ACABÓ!!", 158.0f, 80.0f + this.animacion2, paint);
        paint.setTextSize(30.0f);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(-16777216);
        canvas.drawText("PUNTUACIÓN", 161.0f, 140.0f, paint);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-16711936);
        canvas.drawText("PUNTUACIÓN", 161.0f, 140.0f, paint);
        paint.setTextSize(48.0f);
        paint.setColor(-16777216);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawRoundRect(new RectF(58.0f, 152.0f, 263.0f, 198.0f), 10.0f, 10.0f, paint);
        paint.setColor(-1);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRoundRect(new RectF(58.0f, 152.0f, 263.0f, 198.0f), 10.0f, 10.0f, paint);
        paint.setColor(-16776961);
        paint.setShadowLayer(0.0f, 0.0f, 0.0f, -16777216);
        canvas.drawText(Integer.toString(this.partida.getScore()), 161.0f, 192.0f, paint);
        if (this.animacion < 48.0f) {
            this.animacion2 += 5.0f;
            this.animacion += 10.0f;
        }
        this.botonJugar.pintar(canvas);
        this.botonSalir.pintar(canvas);
    }

    public void checkSelected(int x, int y) {
        this.botonJugar.checkSelected(x, y);
        this.botonSalir.checkSelected(x, y);
    }
}
