package com.chinpon.cartas;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import com.chinpon.cartas.Graphic;
import java.util.List;

public class BotonConfirmar extends Graphic {
    private static BotonConfirmar botonConfirmar = null;
    private final int ESTADO_ACTIVO = 4;
    private final int ESTADO_ANIMACION_APARECIENDO = 1;
    private final int ESTADO_ANIMACION_CORRECTO = 3;
    private final int ESTADO_ANIMACION_ERROR = 2;
    private final int ESTADO_INACTIVO = 5;
    int TIEMPO_CONFIRMACION = 15;
    private final int TIEMPO_DE_RESPUESTA = 1;
    float animacion = 0.0f;
    private int aux = 0;
    Bitmap[] bitmaps = null;
    List<Carta> cartasCandidatas = null;
    int direccion = 0;
    private int estadoActual = 5;
    private Partida partida = null;
    private int retardo = 0;
    private int tiempo = 1;
    int tiempoAnimacion = 0;
    int tiempoAnimacionTotal = this.TIEMPO_CONFIRMACION;

    public static BotonConfirmar getInstance(Bitmap[] bitmaps2, Partida partida2) {
        if (botonConfirmar == null) {
            botonConfirmar = new BotonConfirmar(bitmaps2, partida2);
        }
        return botonConfirmar;
    }

    public BotonConfirmar(Bitmap[] bitmaps2, Partida partida2) {
        super(bitmaps2[0]);
        this.partida = partida2;
        getCoordinates().setX(35);
        getCoordinates().setY(190);
        this.bitmaps = bitmaps2;
        this.estadoActual = 4;
    }

    public void activarBoton() {
        estadoActivo();
    }

    public void respuestaInvalida() {
        this.animacion = 0.0f;
        this.direccion = 0;
        this.tiempoAnimacionTotal = this.TIEMPO_CONFIRMACION;
        this.tiempoAnimacion = 0;
        setBitmap(this.bitmaps[1]);
        this.estadoActual = 2;
    }

    public void respuestaCorrecta() {
        this.estadoActual = 3;
    }

    public void estadoActivo() {
        this.tiempo = 1;
        this.retardo = 10;
        this.aux = 0;
        this.animacion = 0.0f;
        this.direccion = 0;
        this.tiempoAnimacion = 0;
        setBitmap(this.bitmaps[0]);
        this.estadoActual = 4;
    }

    public void estadoInactivo() {
        this.estadoActual = 5;
        this.partida.setPartidaEnCurso();
        this.partida.checkFinalizarPartida();
    }

    public void setCartasComprobar(List<Carta> cartasCandidatas2) {
        this.cartasCandidatas = cartasCandidatas2;
    }

    public void pintar(Canvas canvas) {
        Graphic.Coordinates coords = getCoordinates();
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(-1);
        Matrix matrix = new Matrix();
        switch (this.estadoActual) {
            case Constantes.GLOBAL_CALL_INICIAR_PARTIDA /*2*/:
                matrix.setTranslate(((float) coords.getX()) + this.animacion, ((float) coords.getY()) + this.animacion);
                canvas.drawBitmap(getBitmap(), matrix, paint);
                paint.setTextSize(27.0f);
                paint.setTypeface(Typeface.DEFAULT_BOLD);
                canvas.drawText("ERROR", ((float) (coords.getX() + 83)) + this.animacion, ((float) (coords.getY() + 54)) + this.animacion, paint);
                if (this.direccion == 0) {
                    this.animacion += 2.0f;
                    if (this.animacion >= 1.0f) {
                        this.direccion = 1;
                    }
                } else {
                    this.animacion -= 2.0f;
                    if (this.animacion <= -1.0f) {
                        this.direccion = 0;
                    }
                }
                this.tiempoAnimacion++;
                if (this.tiempoAnimacion >= this.tiempoAnimacionTotal) {
                    estadoInactivo();
                    break;
                }
                break;
            case 4:
                matrix.setScale(this.animacion + 1.0f, this.animacion + 1.0f, (float) (getBitmap().getWidth() / 2), (float) (getBitmap().getHeight() / 2));
                matrix.postTranslate((float) coords.getX(), (float) coords.getY());
                Paint paint2 = new Paint();
                paint2.setAlpha(185);
                canvas.drawBitmap(getBitmap(), matrix, paint2);
                paint.setTextSize(27.0f);
                paint.setTypeface(Typeface.DEFAULT_BOLD);
                canvas.drawText("COINCIDE", (float) (coords.getX() + 60), (float) (coords.getY() + 54), paint);
                if (this.direccion == 0) {
                    this.animacion = (float) (((double) this.animacion) + 0.003d);
                    if (((double) this.animacion) >= 0.02d) {
                        this.direccion = 1;
                    }
                } else {
                    this.animacion = (float) (((double) this.animacion) - 0.003d);
                    if (this.animacion <= 0.0f) {
                        this.direccion = 0;
                    }
                }
                descTiempo(1);
                break;
        }
        paint.setColor(-1);
    }

    public void descTiempo(int dec) {
        if (this.tiempo > 0) {
            if (this.aux > this.retardo) {
                this.aux = 0;
                this.tiempo -= dec;
            }
            this.aux++;
            return;
        }
        this.tiempo = 1;
        this.cartasCandidatas.get(0).reset();
        this.cartasCandidatas.get(1).reset();
        this.partida.setPartidaEnCurso();
    }

    public void checkSelected(int x, int y) {
        if (x < getCoordinates().getX() + getBitmap().getWidth() && x > getCoordinates().getX() && y < getCoordinates().getY() + getBitmap().getHeight() && y > getCoordinates().getY()) {
            doEvent();
        }
    }

    public void doEvent() {
        if (this.estadoActual == 4) {
            Carta cartaAux1 = this.cartasCandidatas.get(0);
            Carta cartaAux2 = this.cartasCandidatas.get(1);
            if (cartaAux1.getPalabra().isEquivalente(cartaAux2.getPalabra())) {
                cartaAux1.completado();
                cartaAux2.completado();
                this.partida.updateScore();
                estadoInactivo();
                return;
            }
            respuestaInvalida();
            cartaAux1.reset();
            cartaAux2.reset();
        }
    }
}
