package com.chinpon.cartas;

public final class R {

    public static final class array {
        public static final int textos = 2131099648;
        public static final int textos2 = 2131099649;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int abstrakt = 2130837504;
        public static final int big = 2130837505;
        public static final int boton_coincide = 2130837506;
        public static final int boton_continuar = 2130837507;
        public static final int boton_error = 2130837508;
        public static final int boton_reintentar = 2130837509;
        public static final int boton_salir = 2130837510;
        public static final int carta = 2130837511;
        public static final int carta_volteada = 2130837512;
        public static final int carta_volteada2 = 2130837513;
        public static final int clock_icon = 2130837514;
        public static final int cutter_orig = 2130837515;
        public static final int fondo_castillo = 2130837516;
        public static final int fondo_inicio = 2130837517;
        public static final int fondo_tori = 2130837518;
        public static final int icon = 2130837519;
        public static final int paper = 2130837520;
        public static final int puntos_icon = 2130837521;
        public static final int rock = 2130837522;
        public static final int scissors = 2130837523;
        public static final int small = 2130837524;
        public static final int smaller = 2130837525;
        public static final int wallpaper = 2130837526;
    }

    public static final class id {
        public static final int adView = 2131165185;
        public static final int mainLayout = 2131165184;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int explosion = 2130968576;
    }

    public static final class string {
        public static final int admob_id = 2131034115;
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
        public static final int prueba = 2131034114;
    }
}
