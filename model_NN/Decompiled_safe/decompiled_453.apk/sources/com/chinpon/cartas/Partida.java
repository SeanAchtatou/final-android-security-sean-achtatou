package com.chinpon.cartas;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.MotionEvent;
import com.chinpon.cartas.Graphic;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Random;

public class Partida extends Observable {
    private static Partida partida;
    private final int ESTADO_EN_CURSO = 1;
    private final int ESTADO_ESPERANDO_CONFIRMACION = 4;
    private final int ESTADO_FINALIZADA_EXITO = 2;
    private final int ESTADO_FIN_TIMEOUT = 3;
    private final int ESTADO_INACTIVO = 0;
    private final int ESTADO_SALIR_JUEGO = 5;
    private final int INC_PUNTUACION = 10;
    private final int INC_TIEMPO = 50;
    private final int TIEMPO_PARTIDA = 100;
    private Map<Integer, Bitmap> _bitmapCache;
    private ArrayList<Carta> _cartas;
    private Graphic _currentGraphic;
    private Graphic.Coordinates _lastCoords;
    private BotonConfirmar botonConfirmar;
    private BotonContinuar botonContinuar;
    Bitmap[] botonesConf = null;
    Bitmap[] botonesMenuFin = null;
    Bitmap[] cartasBitmap = null;
    private Cronometro cronometro;
    private int estadoActual = 0;
    private int fondoActual = 1;
    private int[] fondos;
    String[] mTestArray = null;
    private MenuFin menuFin;
    Panel panel;
    private Score puntos;

    private Partida(Panel aPanel) {
        this.panel = aPanel;
        this._bitmapCache = aPanel.getBitmapCache();
        addObserver((Cartas) aPanel.getContext());
        this.cartasBitmap = new Bitmap[4];
        this.cartasBitmap[0] = this._bitmapCache.get(Integer.valueOf((int) R.drawable.carta_volteada));
        this.cartasBitmap[1] = this._bitmapCache.get(Integer.valueOf((int) R.drawable.carta_volteada2));
        this.cartasBitmap[2] = this._bitmapCache.get(Integer.valueOf((int) R.drawable.carta));
        this.cartasBitmap[3] = this._bitmapCache.get(Integer.valueOf((int) R.drawable.carta));
        this.botonesConf = new Bitmap[2];
        this.botonesConf[0] = this._bitmapCache.get(Integer.valueOf((int) R.drawable.boton_coincide));
        this.botonesConf[1] = this._bitmapCache.get(Integer.valueOf((int) R.drawable.boton_error));
        this.botonesMenuFin = new Bitmap[2];
        this.botonesMenuFin[0] = this._bitmapCache.get(Integer.valueOf((int) R.drawable.boton_reintentar));
        this.botonesMenuFin[1] = this._bitmapCache.get(Integer.valueOf((int) R.drawable.boton_salir));
        if (new Random((long) getRandomSeed(1)).nextInt(2) == 0) {
            this.mTestArray = this.panel.getResources().getStringArray(R.array.textos);
        } else {
            this.mTestArray = this.panel.getResources().getStringArray(R.array.textos2);
        }
    }

    public static Partida getInstance(Panel aPanel) {
        if (partida == null) {
            partida = new Partida(aPanel);
        }
        return partida;
    }

    public void comenzarPartida() {
        cargarParametrosInicio();
        generarPantallaJuego();
    }

    public void cargarParametrosInicio() {
        this.puntos = Score.getInstance(this._bitmapCache.get(Integer.valueOf((int) R.drawable.puntos_icon)));
        this.cronometro = Cronometro.getInstance(this._bitmapCache.get(Integer.valueOf((int) R.drawable.clock_icon)), 100);
        this.fondos = new int[2];
        this.fondos[0] = R.drawable.fondo_tori;
        this.fondos[1] = R.drawable.fondo_castillo;
    }

    public void pintar(Canvas canvas) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        switch (this.estadoActual) {
            case Constantes.GLOBAL_CALL_SALIR_JUEGO:
                canvas.drawBitmap(this._bitmapCache.get(Integer.valueOf(this.fondos[this.fondoActual])), 0.0f, 0.0f, (Paint) null);
                Iterator<Carta> it = this._cartas.iterator();
                while (it.hasNext()) {
                    it.next().pintar(canvas);
                }
                this.puntos.pintar(canvas);
                this.cronometro.pintar(canvas);
                break;
            case Constantes.GLOBAL_CALL_INICIAR_PARTIDA:
                canvas.drawBitmap(this._bitmapCache.get(Integer.valueOf(this.fondos[this.fondoActual])), 0.0f, 0.0f, (Paint) null);
                Iterator<Carta> it2 = this._cartas.iterator();
                while (it2.hasNext()) {
                    it2.next().pintar(canvas);
                }
                paint.setTextSize(30.0f);
                paint.setTypeface(Typeface.DEFAULT_BOLD);
                paint.setStyle(Paint.Style.STROKE);
                paint.setColor(-16777216);
                paint.setStrokeWidth(3.0f);
                canvas.drawText("+50", 120.0f, 100.0f, paint);
                paint.setStyle(Paint.Style.FILL);
                paint.setColor(-256);
                canvas.drawText("+50", 120.0f, 100.0f, paint);
                this.puntos.pintar(canvas);
                this.cronometro.pintar(canvas);
                this.botonContinuar.pintar(canvas);
                break;
            case 3:
                canvas.drawBitmap(this._bitmapCache.get(Integer.valueOf(this.fondos[this.fondoActual])), 0.0f, 0.0f, (Paint) null);
                Iterator<Carta> it3 = this._cartas.iterator();
                while (it3.hasNext()) {
                    it3.next().pintar(canvas);
                }
                this.menuFin.pintar(canvas);
                break;
            case 4:
                canvas.drawBitmap(this._bitmapCache.get(Integer.valueOf(this.fondos[this.fondoActual])), 0.0f, 0.0f, (Paint) null);
                Iterator<Carta> it4 = this._cartas.iterator();
                while (it4.hasNext()) {
                    it4.next().pintar(canvas);
                }
                this.puntos.pintar(canvas);
                this.cronometro.pintar(canvas);
                this.botonConfirmar.pintar(canvas);
                break;
        }
        checkFinalizarPorTimeOut();
    }

    public void continuarSiguienteFase() {
        this.cronometro.incTiempo(50);
        if (this.fondoActual < this.fondos.length - 1) {
            this.fondoActual++;
        } else {
            this.fondoActual = 0;
        }
        generarPantallaJuego();
    }

    public void generarPantallaJuego() {
        this.cronometro.play();
        this.botonConfirmar = BotonConfirmar.getInstance(this.botonesConf, this);
        this.botonContinuar = BotonContinuar.getInstance(this._bitmapCache.get(Integer.valueOf((int) R.drawable.boton_continuar)), this);
        this.menuFin = MenuFin.getInstance(this.botonesMenuFin, this);
        this.estadoActual = 1;
        Random rand = new Random((long) getRandomSeed(this.mTestArray.length - 1));
        Map<String, Palabra> palabrasCandidatas = new HashMap<>();
        while (palabrasCandidatas.size() < 8) {
            int value = rand.nextInt(this.mTestArray.length);
            if (value % 2 == 0 && value < this.mTestArray.length && !palabrasCandidatas.containsKey(this.mTestArray[value])) {
                palabrasCandidatas.put(this.mTestArray[value], new Palabra(this.mTestArray[value], this.mTestArray[value + 1], false));
                palabrasCandidatas.put(this.mTestArray[value + 1], new Palabra(this.mTestArray[value + 1], this.mTestArray[value], true));
            }
        }
        this._cartas = new ArrayList<>();
        Random rand2 = new Random((long) getRandomSeed(palabrasCandidatas.size()));
        while (palabrasCandidatas.size() > 0) {
            this._cartas.add(new Carta(this.cartasBitmap, (Palabra) palabrasCandidatas.remove((String) palabrasCandidatas.keySet().toArray()[rand2.nextInt(palabrasCandidatas.size())]), this));
        }
        int xaux = 35;
        int yaux = 142;
        Iterator<Carta> it = this._cartas.iterator();
        while (it.hasNext()) {
            Carta carta = it.next();
            carta.getCoordinates().setX(xaux);
            carta.getCoordinates().setY(yaux);
            if (xaux < 170) {
                xaux += 140;
            } else {
                xaux = 35;
                yaux += 85;
            }
        }
    }

    public int getRandomSeed(int seed) {
        return (int) (new Date().getTime() % ((long) (seed + 1)));
    }

    public boolean comprobarCoincidenciaCartas(Carta carta1, Carta carta2) {
        return true;
    }

    public void onTouchEvent(MotionEvent event) {
        if (event.getAction() == 0) {
            switch (this.estadoActual) {
                case Constantes.GLOBAL_CALL_SALIR_JUEGO:
                    Iterator<Carta> it = this._cartas.iterator();
                    while (it.hasNext()) {
                        it.next().checkSelected((int) event.getX(), (int) event.getY());
                    }
                    return;
                case Constantes.GLOBAL_CALL_INICIAR_PARTIDA:
                    this.botonContinuar.checkSelected((int) event.getX(), (int) event.getY());
                    return;
                case 3:
                    this.menuFin.checkSelected((int) event.getX(), (int) event.getY());
                    return;
                case 4:
                    this.botonConfirmar.checkSelected((int) event.getX(), (int) event.getY());
                    return;
                default:
                    return;
            }
        }
    }

    public void checkFinalizarPorTimeOut() {
        if (this.cronometro.getTiempo() == 0) {
            this.estadoActual = 3;
        }
    }

    public void checkFinalizarPartida() {
        boolean panelResuelto = true;
        Iterator<Carta> it = this._cartas.iterator();
        while (true) {
            if (it.hasNext()) {
                if (!it.next().isCompletada()) {
                    panelResuelto = false;
                    break;
                }
            } else {
                break;
            }
        }
        if (panelResuelto) {
            this.cronometro.stop();
            this.estadoActual = 2;
        }
    }

    public void checkEquivalencias() {
        List<Carta> cartasCandidatas = new ArrayList<>();
        Iterator<Carta> it = this._cartas.iterator();
        while (it.hasNext()) {
            Carta carta = it.next();
            if (carta.isCandidata()) {
                cartasCandidatas.add(carta);
            }
        }
        if (cartasCandidatas.size() == 2) {
            this.estadoActual = 4;
            this.botonConfirmar.setCartasComprobar(cartasCandidatas);
            this.botonConfirmar.estadoActivo();
        }
        checkFinalizarPartida();
    }

    public void updateScore() {
        this.puntos.incScore(10);
    }

    public void setPartidaEnCurso() {
        this.estadoActual = 1;
    }

    public int getScore() {
        if (this.puntos != null) {
            return this.puntos.getScore();
        }
        return 0;
    }

    public void salirJuego() {
        setChanged();
        notifyObservers(1);
    }

    public void iniciarJuego() {
        setChanged();
        notifyObservers(2);
    }

    public boolean isSalirJuego() {
        if (this.estadoActual == 5) {
            return true;
        }
        return false;
    }

    public void pausarPartida() {
        if (this.estadoActual == 1) {
            this.cronometro.stop();
        }
    }

    public void restaurarPartida() {
        if (this.estadoActual == 1) {
            this.cronometro.play();
        }
    }
}
