package net.sourceforge.zmanim;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import net.sourceforge.zmanim.util.AstronomicalCalculator;
import net.sourceforge.zmanim.util.GeoLocation;
import net.sourceforge.zmanim.util.ZmanimFormatter;

public class AstronomicalCalendar implements Cloneable {
    public static final double ASTRONOMICAL_ZENITH = 108.0d;
    public static final double CIVIL_ZENITH = 96.0d;
    public static final double GEOMETRIC_ZENITH = 90.0d;
    static final long HOUR_MILLIS = 3600000;
    static final long MINUTE_MILLIS = 60000;
    public static final double NAUTICAL_ZENITH = 102.0d;
    private static final long serialVersionUID = 1;
    private AstronomicalCalculator astronomicalCalculator;
    private Calendar calendar;
    private GeoLocation geoLocation;

    public Date getSunrise() {
        double sunrise = getUTCSunrise(90.0d);
        if (Double.isNaN(sunrise)) {
            return null;
        }
        return getDateFromTime(sunrise);
    }

    public Date getSeaLevelSunrise() {
        double sunrise = getUTCSeaLevelSunrise(90.0d);
        if (Double.isNaN(sunrise)) {
            return null;
        }
        return getDateFromTime(sunrise);
    }

    public Date getBeginCivilTwilight() {
        return getSunriseOffsetByDegrees(96.0d);
    }

    public Date getBeginNauticalTwilight() {
        return getSunriseOffsetByDegrees(102.0d);
    }

    public Date getBeginAstronomicalTwilight() {
        return getSunriseOffsetByDegrees(108.0d);
    }

    public Date getSunset() {
        double sunset = getUTCSunset(90.0d);
        if (Double.isNaN(sunset)) {
            return null;
        }
        return getAdjustedSunsetDate(getDateFromTime(sunset), getSunrise());
    }

    private Date getAdjustedSunsetDate(Date sunset, Date sunrise) {
        if (sunset == null || sunrise == null || sunrise.compareTo(sunset) < 0) {
            return sunset;
        }
        Calendar clonedCalendar = (Calendar) getCalendar().clone();
        clonedCalendar.setTime(sunset);
        clonedCalendar.add(5, 1);
        return clonedCalendar.getTime();
    }

    public Date getSeaLevelSunset() {
        double sunset = getUTCSeaLevelSunset(90.0d);
        if (Double.isNaN(sunset)) {
            return null;
        }
        return getAdjustedSunsetDate(getDateFromTime(sunset), getSeaLevelSunrise());
    }

    public Date getEndCivilTwilight() {
        return getSunsetOffsetByDegrees(96.0d);
    }

    public Date getEndNauticalTwilight() {
        return getSunsetOffsetByDegrees(102.0d);
    }

    public Date getEndAstronomicalTwilight() {
        return getSunsetOffsetByDegrees(108.0d);
    }

    public Date getTimeOffset(Date time, double offset) {
        return getTimeOffset(time, (long) offset);
    }

    public Date getTimeOffset(Date time, long offset) {
        if (time == null || offset == Long.MIN_VALUE) {
            return null;
        }
        return new Date(time.getTime() + offset);
    }

    public Date getSunriseOffsetByDegrees(double offsetZenith) {
        double alos = getUTCSunrise(offsetZenith);
        if (Double.isNaN(alos)) {
            return null;
        }
        return getDateFromTime(alos);
    }

    public Date getSunsetOffsetByDegrees(double offsetZenith) {
        double sunset = getUTCSunset(offsetZenith);
        if (Double.isNaN(sunset)) {
            return null;
        }
        return getAdjustedSunsetDate(getDateFromTime(sunset), getSunriseOffsetByDegrees(offsetZenith));
    }

    public AstronomicalCalendar() {
        this(new GeoLocation());
    }

    public AstronomicalCalendar(GeoLocation geoLocation2) {
        setCalendar(Calendar.getInstance(geoLocation2.getTimeZone()));
        setGeoLocation(geoLocation2);
        setAstronomicalCalculator(AstronomicalCalculator.getDefault());
    }

    public double getUTCSunrise(double zenith) {
        return getAstronomicalCalculator().getUTCSunrise(this, zenith, true);
    }

    public double getUTCSeaLevelSunrise(double zenith) {
        return getAstronomicalCalculator().getUTCSunrise(this, zenith, false);
    }

    public double getUTCSunset(double zenith) {
        return getAstronomicalCalculator().getUTCSunset(this, zenith, true);
    }

    public double getUTCSeaLevelSunset(double zenith) {
        return getAstronomicalCalculator().getUTCSunset(this, zenith, false);
    }

    private double getOffsetTime(double time) {
        boolean dst = getCalendar().getTimeZone().inDaylightTime(getCalendar().getTime());
        double dstOffset = 0.0d;
        double gmtOffset = (double) (((long) getCalendar().getTimeZone().getRawOffset()) / 3600000);
        if (dst) {
            dstOffset = (double) (((long) getCalendar().getTimeZone().getDSTSavings()) / 3600000);
        }
        return time + gmtOffset + dstOffset;
    }

    public long getTemporalHour() {
        return getTemporalHour(getSunrise(), getSunset());
    }

    public long getTemporalHour(Date sunrise, Date sunset) {
        if (sunrise == null || sunset == null) {
            return Long.MIN_VALUE;
        }
        return (sunset.getTime() - sunrise.getTime()) / 12;
    }

    public Date getSunTransit() {
        return getTimeOffset(getSunrise(), getTemporalHour() * 6);
    }

    /* access modifiers changed from: protected */
    public Date getDateFromTime(double time) {
        if (Double.isNaN(time)) {
            return null;
        }
        double time2 = (240.0d + getOffsetTime(time)) % 24.0d;
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(1, getCalendar().get(1));
        cal.set(2, getCalendar().get(2));
        cal.set(5, getCalendar().get(5));
        int hours = (int) time2;
        double time3 = (time2 - ((double) hours)) * 60.0d;
        int minutes = (int) time3;
        double time4 = (time3 - ((double) minutes)) * 60.0d;
        int seconds = (int) time4;
        cal.set(11, hours);
        cal.set(12, minutes);
        cal.set(13, seconds);
        cal.set(14, (int) (1000.0d * (time4 - ((double) seconds))));
        return cal.getTime();
    }

    public double getSunriseSolarDipFromOffset(double minutes) {
        Date offsetByDegrees = getSeaLevelSunrise();
        Date offsetByTime = getTimeOffset(getSeaLevelSunrise(), -(60000.0d * minutes));
        BigDecimal degrees = new BigDecimal(0);
        BigDecimal incrementor = new BigDecimal("0.0001");
        while (true) {
            if (offsetByDegrees != null && offsetByDegrees.getTime() <= offsetByTime.getTime()) {
                return degrees.doubleValue();
            }
            degrees = degrees.add(incrementor);
            offsetByDegrees = getSunriseOffsetByDegrees(90.0d + degrees.doubleValue());
        }
    }

    public double getSunsetSolarDipFromOffset(double minutes) {
        Date offsetByDegrees = getSeaLevelSunset();
        Date offsetByTime = getTimeOffset(getSeaLevelSunset(), 60000.0d * minutes);
        BigDecimal degrees = new BigDecimal(0);
        BigDecimal incrementor = new BigDecimal("0.0001");
        while (true) {
            if (offsetByDegrees != null && offsetByDegrees.getTime() >= offsetByTime.getTime()) {
                return degrees.doubleValue();
            }
            degrees = degrees.add(incrementor);
            offsetByDegrees = getSunsetOffsetByDegrees(90.0d + degrees.doubleValue());
        }
    }

    public String toString() {
        return ZmanimFormatter.toXML(this);
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof AstronomicalCalendar)) {
            return false;
        }
        AstronomicalCalendar aCal = (AstronomicalCalendar) object;
        return getCalendar().equals(aCal.getCalendar()) && getGeoLocation().equals(aCal.getGeoLocation()) && getAstronomicalCalculator().equals(aCal.getAstronomicalCalculator());
    }

    public int hashCode() {
        int i = 17 * 37;
        int result = getClass().hashCode() + 629;
        int result2 = result + (result * 37) + getCalendar().hashCode();
        int result3 = result2 + (result2 * 37) + getGeoLocation().hashCode();
        return result3 + (result3 * 37) + getAstronomicalCalculator().hashCode();
    }

    public GeoLocation getGeoLocation() {
        return this.geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation2) {
        this.geoLocation = geoLocation2;
        getCalendar().setTimeZone(geoLocation2.getTimeZone());
    }

    public AstronomicalCalculator getAstronomicalCalculator() {
        return this.astronomicalCalculator;
    }

    public void setAstronomicalCalculator(AstronomicalCalculator astronomicalCalculator2) {
        this.astronomicalCalculator = astronomicalCalculator2;
    }

    public Calendar getCalendar() {
        return this.calendar;
    }

    public void setCalendar(Calendar calendar2) {
        this.calendar = calendar2;
        if (getGeoLocation() != null) {
            getCalendar().setTimeZone(getGeoLocation().getTimeZone());
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v9, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: net.sourceforge.zmanim.AstronomicalCalendar} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object clone() {
        /*
            r5 = this;
            r1 = 0
            java.lang.Object r3 = super.clone()     // Catch:{ CloneNotSupportedException -> 0x0031 }
            r0 = r3
            net.sourceforge.zmanim.AstronomicalCalendar r0 = (net.sourceforge.zmanim.AstronomicalCalendar) r0     // Catch:{ CloneNotSupportedException -> 0x0031 }
            r1 = r0
        L_0x0009:
            net.sourceforge.zmanim.util.GeoLocation r3 = r5.getGeoLocation()
            java.lang.Object r3 = r3.clone()
            net.sourceforge.zmanim.util.GeoLocation r3 = (net.sourceforge.zmanim.util.GeoLocation) r3
            r1.setGeoLocation(r3)
            java.util.Calendar r3 = r5.getCalendar()
            java.lang.Object r3 = r3.clone()
            java.util.Calendar r3 = (java.util.Calendar) r3
            r1.setCalendar(r3)
            net.sourceforge.zmanim.util.AstronomicalCalculator r3 = r5.getAstronomicalCalculator()
            java.lang.Object r5 = r3.clone()
            net.sourceforge.zmanim.util.AstronomicalCalculator r5 = (net.sourceforge.zmanim.util.AstronomicalCalculator) r5
            r1.setAstronomicalCalculator(r5)
            return r1
        L_0x0031:
            r3 = move-exception
            r2 = r3
            java.io.PrintStream r3 = java.lang.System.out
            java.lang.String r4 = "Required by the compiler. Should never be reached since we implement clone()"
            r3.print(r4)
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: net.sourceforge.zmanim.AstronomicalCalendar.clone():java.lang.Object");
    }
}
