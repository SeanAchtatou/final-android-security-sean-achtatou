package net.sourceforge.zmanim.util;

public class GeoLocationUtils {
    private static int DISTANCE = 0;
    private static int FINAL_BEARING = 2;
    private static int INITIAL_BEARING = 1;

    public static double getGeodesicInitialBearing(GeoLocation location, GeoLocation destination) {
        return vincentyFormula(location, destination, INITIAL_BEARING);
    }

    public static double getGeodesicFinalBearing(GeoLocation location, GeoLocation destination) {
        return vincentyFormula(location, destination, FINAL_BEARING);
    }

    public static double getGeodesicDistance(GeoLocation location, GeoLocation destination) {
        return vincentyFormula(location, destination, DISTANCE);
    }

    /* JADX INFO: Multiple debug info for r52v11 double: [D('A' double), D('cosSqAlpha' double)] */
    /* JADX INFO: Multiple debug info for r4v11 double: [D('uSq' double), D('B' double)] */
    /* JADX INFO: Multiple debug info for r4v19 double: [D('deltaSigma' double), D('B' double)] */
    /* JADX INFO: Multiple debug info for r52v13 double: [D('A' double), D('distance' double)] */
    /* JADX INFO: Multiple debug info for r26v18 double: [D('lambda' double), D('lambdaP' double)] */
    /* JADX INFO: Multiple debug info for r52v50 double: [D('C' double), D('lambda' double)] */
    private static double vincentyFormula(GeoLocation location, GeoLocation destination, int formula) {
        double L = Math.toRadians(destination.getLongitude() - location.getLongitude());
        double U1 = Math.atan((1.0d - 0.0033528106647474805d) * Math.tan(Math.toRadians(location.getLatitude())));
        double U2 = Math.atan(Math.tan(Math.toRadians(destination.getLatitude())) * (1.0d - 0.0033528106647474805d));
        double sinU1 = Math.sin(U1);
        double cosU1 = Math.cos(U1);
        double sinU2 = Math.sin(U2);
        double cosU2 = Math.cos(U2);
        double cosSqAlpha = 20.0d;
        double sinLambda = 0.0d;
        double sinSigma = 0.0d;
        double sigma = 0.0d;
        double lambda = 0.0d;
        double cosSigma = 0.0d;
        double cosLambda = 0.0d;
        double cos2SigmaM = 0.0d;
        double lambdaP = L;
        double iterLimit = 6.283185307179586d;
        while (true) {
            if (Math.abs(lambdaP - iterLimit) <= 1.0E-12d) {
                break;
            }
            double iterLimit2 = cosSqAlpha - 1.0d;
            if (iterLimit2 <= 0.0d) {
                cosSqAlpha = iterLimit2;
                break;
            }
            sinLambda = Math.sin(lambdaP);
            cosLambda = Math.cos(lambdaP);
            sinSigma = Math.sqrt((cosU2 * sinLambda * cosU2 * sinLambda) + (((cosU1 * sinU2) - ((sinU1 * cosU2) * cosLambda)) * ((cosU1 * sinU2) - ((sinU1 * cosU2) * cosLambda))));
            if (sinSigma == 0.0d) {
                return 0.0d;
            }
            cosSigma = (sinU1 * sinU2) + (cosU1 * cosU2 * cosLambda);
            sigma = Math.atan2(sinSigma, cosSigma);
            double sinAlpha = ((cosU1 * cosU2) * sinLambda) / sinSigma;
            double cosSqAlpha2 = 1.0d - (sinAlpha * sinAlpha);
            double cos2SigmaM2 = cosSigma - (((2.0d * sinU1) * sinU2) / cosSqAlpha2);
            if (Double.isNaN(cos2SigmaM2)) {
                cos2SigmaM2 = 0.0d;
            }
            cos2SigmaM = cos2SigmaM2;
            double C = (0.0033528106647474805d / 16.0d) * cosSqAlpha2 * (4.0d + ((4.0d - (3.0d * cosSqAlpha2)) * 0.0033528106647474805d));
            double cosSqAlpha3 = cosSqAlpha2;
            cosSqAlpha = iterLimit2;
            iterLimit = lambdaP;
            lambdaP = (((((C * cosSigma * (-1.0d + (2.0d * cos2SigmaM * cos2SigmaM))) + cos2SigmaM) * C * sinSigma) + sigma) * (1.0d - C) * 0.0033528106647474805d * sinAlpha) + L;
            lambda = cosSqAlpha3;
        }
        if (cosSqAlpha == 0.0d) {
            return Double.NaN;
        }
        double uSq = (lambda * ((6378137.0d * 6378137.0d) - (6356752.3142d * 6356752.3142d))) / (6356752.3142d * 6356752.3142d);
        double cosSqAlpha4 = 1.0d + ((uSq / 16384.0d) * (4096.0d + ((-768.0d + ((320.0d - (175.0d * uSq)) * uSq)) * uSq)));
        double B = ((uSq * (-128.0d + ((74.0d - (47.0d * uSq)) * uSq))) + 256.0d) * (uSq / 1024.0d);
        double distance = cosSqAlpha4 * 6356752.3142d * (sigma - (((((cosSigma * (-1.0d + ((2.0d * cos2SigmaM) * cos2SigmaM))) - ((((B / 6.0d) * cos2SigmaM) * (-3.0d + ((4.0d * sinSigma) * sinSigma))) * (-3.0d + ((4.0d * cos2SigmaM) * cos2SigmaM)))) * (B / 4.0d)) + cos2SigmaM) * (B * sinSigma)));
        double fwdAz = Math.toDegrees(Math.atan2(cosU2 * sinLambda, (cosU1 * sinU2) - ((sinU1 * cosU2) * cosLambda)));
        double revAz = Math.toDegrees(Math.atan2(cosU1 * sinLambda, ((-sinU1) * cosU2) + (cosU1 * sinU2 * cosLambda)));
        if (formula == DISTANCE) {
            return distance;
        }
        if (formula == INITIAL_BEARING) {
            return fwdAz;
        }
        if (formula == FINAL_BEARING) {
            return revAz;
        }
        return Double.NaN;
    }

    public static double getRhumbLineBearing(GeoLocation location, GeoLocation destination) {
        double dLon = Math.toRadians(destination.getLongitude() - location.getLongitude());
        double dPhi = Math.log(Math.tan((Math.toRadians(destination.getLatitude()) / 2.0d) + 0.7853981633974483d) / Math.tan((Math.toRadians(location.getLatitude()) / 2.0d) + 0.7853981633974483d));
        if (Math.abs(dLon) > 3.141592653589793d) {
            dLon = dLon > 0.0d ? -(6.283185307179586d - dLon) : 6.283185307179586d + dLon;
        }
        return Math.toDegrees(Math.atan2(dLon, dPhi));
    }

    /* JADX INFO: Multiple debug info for r12v10 double: [D('d' double), D('dLon' double)] */
    public static double getRhumbLineDistance(GeoLocation location, GeoLocation destination) {
        double dLon;
        double dLat = Math.toRadians(destination.getLatitude() - location.getLatitude());
        double dLon2 = Math.toRadians(Math.abs(destination.getLongitude() - location.getLongitude()));
        double dPhi = Math.abs(dLat) > 1.0E-10d ? dLat / Math.log(Math.tan((Math.toRadians(destination.getLongitude()) / 2.0d) + 0.7853981633974483d) / Math.tan((Math.toRadians(location.getLatitude()) / 2.0d) + 0.7853981633974483d)) : Math.cos(Math.toRadians(location.getLatitude()));
        if (dLon2 > 3.141592653589793d) {
            dLon = 6.283185307179586d - dLon2;
        } else {
            dLon = dLon2;
        }
        return Math.sqrt((dLon * dPhi * dPhi * dLon) + (dLat * dLat)) * 6371.0d;
    }
}
