package net.sourceforge.zmanim.util;

import java.util.Calendar;
import net.sourceforge.zmanim.AstronomicalCalendar;

public class NOAACalculator extends AstronomicalCalculator {
    private String calculatorName = "US National Oceanic and Atmospheric Administration Algorithm";

    public String getCalculatorName() {
        return this.calculatorName;
    }

    public double getUTCSunrise(AstronomicalCalendar astronomicalCalendar, double zenith, boolean adjustForElevation) {
        double zenith2;
        if (adjustForElevation) {
            zenith2 = adjustZenith(zenith, astronomicalCalendar.getGeoLocation().getElevation());
        } else {
            zenith2 = adjustZenith(zenith, 0.0d);
        }
        return calcSunriseUTC(calcJD(astronomicalCalendar.getCalendar()), astronomicalCalendar.getGeoLocation().getLatitude(), -astronomicalCalendar.getGeoLocation().getLongitude(), zenith2) / 60.0d;
    }

    public double getUTCSunset(AstronomicalCalendar astronomicalCalendar, double zenith, boolean adjustForElevation) {
        double zenith2;
        if (adjustForElevation) {
            zenith2 = adjustZenith(zenith, astronomicalCalendar.getGeoLocation().getElevation());
        } else {
            zenith2 = adjustZenith(zenith, 0.0d);
        }
        return calcSunsetUTC(calcJD(astronomicalCalendar.getCalendar()), astronomicalCalendar.getGeoLocation().getLatitude(), -astronomicalCalendar.getGeoLocation().getLongitude(), zenith2) / 60.0d;
    }

    private static double calcJD(Calendar date) {
        int year = date.get(1);
        int month = date.get(2) + 1;
        int day = date.get(5);
        if (month <= 2) {
            year--;
            month += 12;
        }
        double A = Math.floor((double) (year / 100));
        return (((Math.floor(365.25d * ((double) (year + 4716))) + Math.floor(30.6001d * ((double) (month + 1)))) + ((double) day)) + ((2.0d - A) + Math.floor(A / 4.0d))) - 1524.5d;
    }

    private static double calcTimeJulianCent(double jd) {
        return (jd - 2451545.0d) / 36525.0d;
    }

    private static double calcJDFromJulianCent(double t) {
        return (36525.0d * t) + 2451545.0d;
    }

    private static double calcGeomMeanLongSun(double t) {
        double L0 = 280.46646d + ((36000.76983d + (3.032E-4d * t)) * t);
        while (L0 > 360.0d) {
            L0 -= 360.0d;
        }
        while (L0 < 0.0d) {
            L0 += 360.0d;
        }
        return L0;
    }

    private static double calcGeomMeanAnomalySun(double t) {
        return 357.52911d + ((35999.05029d - (1.537E-4d * t)) * t);
    }

    private static double calcEccentricityEarthOrbit(double t) {
        return 0.016708634d - ((4.2037E-5d + (1.267E-7d * t)) * t);
    }

    /* JADX INFO: Multiple debug info for r0v1 double: [D('m' double), D('mrad' double)] */
    /* JADX INFO: Multiple debug info for r0v3 double: [D('sin3m' double), D('mrad' double)] */
    /* JADX INFO: Multiple debug info for r12v5 double: [D('C' double), D('t' double)] */
    private static double calcSunEqOfCenter(double t) {
        double mrad = Math.toRadians(calcGeomMeanAnomalySun(t));
        double sinm = Math.sin(mrad);
        return ((0.019993d - (t * 1.01E-4d)) * Math.sin(mrad + mrad)) + (sinm * (1.914602d - ((0.004817d + (1.4E-5d * t)) * t))) + (Math.sin(mrad + mrad + mrad) * 2.89E-4d);
    }

    private static double calcSunTrueLong(double t) {
        return calcGeomMeanLongSun(t) + calcSunEqOfCenter(t);
    }

    private static double calcSunApparentLong(double t) {
        return (calcSunTrueLong(t) - 0.00569d) - (0.00478d * Math.sin(Math.toRadians(125.04d - (1934.136d * t))));
    }

    private static double calcMeanObliquityOfEcliptic(double t) {
        return 23.0d + ((26.0d + ((21.448d - ((46.815d + ((5.9E-4d - (0.001813d * t)) * t)) * t)) / 60.0d)) / 60.0d);
    }

    private static double calcObliquityCorrection(double t) {
        return calcMeanObliquityOfEcliptic(t) + (0.00256d * Math.cos(Math.toRadians(125.04d - (1934.136d * t))));
    }

    private static double calcSunDeclination(double t) {
        return Math.toDegrees(Math.asin(Math.sin(Math.toRadians(calcObliquityCorrection(t))) * Math.sin(Math.toRadians(calcSunApparentLong(t)))));
    }

    /* JADX INFO: Multiple debug info for r14v13 double: [D('Etime' double), D('cos2l0' double)] */
    private static double calcEquationOfTime(double t) {
        double epsilon = calcObliquityCorrection(t);
        double l0 = calcGeomMeanLongSun(t);
        double e = calcEccentricityEarthOrbit(t);
        double m = calcGeomMeanAnomalySun(t);
        double y = Math.tan(Math.toRadians(epsilon) / 2.0d);
        double y2 = y * y;
        double sin2l0 = Math.sin(2.0d * Math.toRadians(l0));
        double sinm = Math.sin(Math.toRadians(m));
        double sin2l02 = (sin2l0 * y2) - ((2.0d * e) * sinm);
        return Math.toDegrees((((Math.cos(2.0d * Math.toRadians(l0)) * (((4.0d * e) * y2) * sinm)) + sin2l02) - (((0.5d * y2) * y2) * Math.sin(Math.toRadians(l0) * 4.0d))) - ((e * (1.25d * e)) * Math.sin(2.0d * Math.toRadians(m)))) * 4.0d;
    }

    /* JADX INFO: Multiple debug info for r4v1 double: [D('lat' double), D('latRad' double)] */
    /* JADX INFO: Multiple debug info for r6v1 double: [D('solarDec' double), D('sdRad' double)] */
    private static double calcHourAngleSunrise(double lat, double solarDec, double zenith) {
        double latRad = Math.toRadians(lat);
        double sdRad = Math.toRadians(solarDec);
        return Math.acos((Math.cos(Math.toRadians(zenith)) / (Math.cos(latRad) * Math.cos(sdRad))) - (Math.tan(latRad) * Math.tan(sdRad)));
    }

    /* JADX INFO: Multiple debug info for r4v1 double: [D('lat' double), D('latRad' double)] */
    /* JADX INFO: Multiple debug info for r6v1 double: [D('solarDec' double), D('sdRad' double)] */
    private static double calcHourAngleSunset(double lat, double solarDec, double zenith) {
        double latRad = Math.toRadians(lat);
        double sdRad = Math.toRadians(solarDec);
        return -Math.acos((Math.cos(Math.toRadians(zenith)) / (Math.cos(latRad) * Math.cos(sdRad))) - (Math.tan(latRad) * Math.tan(sdRad)));
    }

    /* JADX INFO: Multiple debug info for r10v2 double: [D('JD' double), D('tnoon' double)] */
    /* JADX INFO: Multiple debug info for r10v3 double: [D('hourAngle' double), D('tnoon' double)] */
    /* JADX INFO: Multiple debug info for r10v5 double: [D('hourAngle' double), D('delta' double)] */
    /* JADX INFO: Multiple debug info for r10v6 double: [D('delta' double), D('timeDiff' double)] */
    /* JADX INFO: Multiple debug info for r10v8 double: [D('timeDiff' double), D('timeUTC' double)] */
    /* JADX INFO: Multiple debug info for r10v11 double: [D('newt' double), D('timeUTC' double)] */
    /* JADX INFO: Multiple debug info for r10v12 double: [D('hourAngle' double), D('newt' double)] */
    /* JADX INFO: Multiple debug info for r10v14 double: [D('hourAngle' double), D('delta' double)] */
    /* JADX INFO: Multiple debug info for r10v15 double: [D('delta' double), D('timeDiff' double)] */
    /* JADX INFO: Multiple debug info for r10v17 double: [D('timeDiff' double), D('timeUTC' double)] */
    private static double calcSunriseUTC(double JD, double latitude, double longitude, double zenith) {
        double t = calcTimeJulianCent(JD);
        double tnoon = calcTimeJulianCent(JD + (calcSolNoonUTC(t, longitude) / 1440.0d));
        double timeUTC = calcTimeJulianCent((((((longitude - Math.toDegrees(calcHourAngleSunrise(latitude, calcSunDeclination(tnoon), zenith))) * 4.0d) + 720.0d) - calcEquationOfTime(tnoon)) / 1440.0d) + calcJDFromJulianCent(t));
        return (((longitude - Math.toDegrees(calcHourAngleSunrise(latitude, calcSunDeclination(timeUTC), zenith))) * 4.0d) + 720.0d) - calcEquationOfTime(timeUTC);
    }

    /* JADX INFO: Multiple debug info for r0v3 double: [D('tnoon' double), D('eqTime' double)] */
    /* JADX INFO: Multiple debug info for r0v4 double: [D('solNoonUTC' double), D('eqTime' double)] */
    /* JADX INFO: Multiple debug info for r6v5 double: [D('newt' double), D('eqTime' double)] */
    private static double calcSolNoonUTC(double t, double longitude) {
        double d = 720.0d + (4.0d * longitude);
        return ((longitude * 4.0d) + 720.0d) - calcEquationOfTime(calcTimeJulianCent((calcJDFromJulianCent(t) - 0.5d) + ((d - calcEquationOfTime(calcTimeJulianCent(calcJDFromJulianCent(t) + (longitude / 360.0d)))) / 1440.0d)));
    }

    /* JADX INFO: Multiple debug info for r10v2 double: [D('JD' double), D('tnoon' double)] */
    /* JADX INFO: Multiple debug info for r10v3 double: [D('hourAngle' double), D('tnoon' double)] */
    /* JADX INFO: Multiple debug info for r10v5 double: [D('hourAngle' double), D('delta' double)] */
    /* JADX INFO: Multiple debug info for r10v6 double: [D('delta' double), D('timeDiff' double)] */
    /* JADX INFO: Multiple debug info for r10v8 double: [D('timeDiff' double), D('timeUTC' double)] */
    /* JADX INFO: Multiple debug info for r10v11 double: [D('newt' double), D('timeUTC' double)] */
    /* JADX INFO: Multiple debug info for r10v12 double: [D('hourAngle' double), D('newt' double)] */
    /* JADX INFO: Multiple debug info for r10v14 double: [D('hourAngle' double), D('delta' double)] */
    /* JADX INFO: Multiple debug info for r10v15 double: [D('delta' double), D('timeDiff' double)] */
    private static double calcSunsetUTC(double JD, double latitude, double longitude, double zenith) {
        double t = calcTimeJulianCent(JD);
        double tnoon = calcTimeJulianCent(JD + (calcSolNoonUTC(t, longitude) / 1440.0d));
        double timeUTC = calcTimeJulianCent((((((longitude - Math.toDegrees(calcHourAngleSunset(latitude, calcSunDeclination(tnoon), zenith))) * 4.0d) + 720.0d) - calcEquationOfTime(tnoon)) / 1440.0d) + calcJDFromJulianCent(t));
        return (((longitude - Math.toDegrees(calcHourAngleSunset(latitude, calcSunDeclination(timeUTC), zenith))) * 4.0d) + 720.0d) - calcEquationOfTime(timeUTC);
    }
}
