package net.sourceforge.zmanim.util;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import net.sourceforge.zmanim.AstronomicalCalendar;

public class ZmanimFormatter {
    public static final int DECIMAL_FORMAT = 1;
    public static final long HOUR_MILLIS = 3600000;
    static final long MINUTE_MILLIS = 60000;
    public static final int SEXAGESIMAL_FORMAT = 2;
    public static final int SEXAGESIMAL_MILLIS_FORMAT = 4;
    public static final int SEXAGESIMAL_SECONDS_FORMAT = 3;
    public static final int SEXAGESIMAL_XSD_FORMAT = 0;
    public static final int XSD_DURATION_FORMAT = 5;
    private static DecimalFormat milliNF = new DecimalFormat("000");
    private static DecimalFormat minuteSecondNF = new DecimalFormat("00");
    private SimpleDateFormat dateFormat;
    private DecimalFormat hourNF;
    private boolean prependZeroHours;
    private int timeFormat;
    boolean useDecimal;
    private boolean useMillis;
    private boolean useSeconds;

    public ZmanimFormatter() {
        this(0, new SimpleDateFormat("h:mm:ss"));
    }

    public ZmanimFormatter(int format, SimpleDateFormat dateFormat2) {
        this.timeFormat = 0;
        this.hourNF = new DecimalFormat(this.prependZeroHours ? "00" : "0");
        setTimeFormat(format);
        setDateFormat(dateFormat2);
    }

    public void setTimeFormat(int format) {
        this.timeFormat = format;
        switch (format) {
            case SEXAGESIMAL_XSD_FORMAT /*0*/:
                setSettings(true, true, true);
                return;
            case DECIMAL_FORMAT /*1*/:
            default:
                this.useDecimal = true;
                return;
            case SEXAGESIMAL_FORMAT /*2*/:
                setSettings(false, false, false);
                return;
            case SEXAGESIMAL_SECONDS_FORMAT /*3*/:
                setSettings(false, true, false);
                return;
            case SEXAGESIMAL_MILLIS_FORMAT /*4*/:
                setSettings(false, true, true);
                return;
        }
    }

    public void setDateFormat(SimpleDateFormat sdf) {
        this.dateFormat = sdf;
    }

    public SimpleDateFormat getDateFormat() {
        return this.dateFormat;
    }

    private void setSettings(boolean prependZeroHours2, boolean useSeconds2, boolean useMillis2) {
        this.prependZeroHours = prependZeroHours2;
        this.useSeconds = useSeconds2;
        this.useMillis = useMillis2;
    }

    public String format(double milliseconds) {
        return format((int) milliseconds);
    }

    public String format(int millis) {
        return format(new Time(millis));
    }

    public String format(Time time) {
        if (this.timeFormat == 5) {
            return formatXSDDurationTime(time);
        }
        StringBuffer sb = new StringBuffer();
        sb.append(this.hourNF.format((long) time.getHours()));
        sb.append(":");
        sb.append(minuteSecondNF.format((long) time.getMinutes()));
        if (this.useSeconds) {
            sb.append(":");
            sb.append(minuteSecondNF.format((long) time.getSeconds()));
        }
        if (this.useMillis) {
            sb.append(".");
            sb.append(milliNF.format((long) time.getMilliseconds()));
        }
        return sb.toString();
    }

    public String formatDateTime(Date dateTime, Calendar calendar) {
        this.dateFormat.setCalendar(calendar);
        if (this.dateFormat.toPattern().equals("yyyy-MM-dd'T'HH:mm:ss")) {
            return getXSDateTime(dateTime, calendar);
        }
        return this.dateFormat.format(dateTime);
    }

    public String getXSDateTime(Date dateTime, Calendar cal) {
        StringBuffer buff = new StringBuffer(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(dateTime));
        int offset = cal.get(15) + cal.get(16);
        if (offset == 0) {
            buff.append("Z");
        } else {
            int hrs = offset / 3600000;
            buff.append(new StringBuffer().append(hrs < 0 ? '-' : '+').append(formatDigits(hrs)).append(':').append(formatDigits(offset % 3600000)).toString());
        }
        return buff.toString();
    }

    private static String formatDigits(int digits) {
        String dd = String.valueOf(Math.abs(digits));
        return dd.length() == 1 ? new StringBuffer().append('0').append(dd).toString() : dd;
    }

    public String formatXSDDurationTime(long millis) {
        return formatXSDDurationTime(new Time((double) millis));
    }

    public String formatXSDDurationTime(Time time) {
        StringBuffer duration = new StringBuffer();
        duration.append("P");
        if (!(time.getHours() == 0 && time.getMinutes() == 0 && time.getSeconds() == 0 && time.getMilliseconds() == 0)) {
            duration.append("T");
            if (time.getHours() != 0) {
                duration.append(new StringBuffer().append(time.getHours()).append("H").toString());
            }
            if (time.getMinutes() != 0) {
                duration.append(new StringBuffer().append(time.getMinutes()).append("M").toString());
            }
            if (!(time.getSeconds() == 0 && time.getMilliseconds() == 0)) {
                duration.append(new StringBuffer().append(time.getSeconds()).append(".").append(milliNF.format((long) time.getMilliseconds())).toString());
                duration.append("S");
            }
            if (duration.length() == 1) {
                duration.append("T0S");
            }
            if (time.isNegative()) {
                duration.insert(0, "-");
            }
        }
        return duration.toString();
    }

    /* JADX INFO: Multiple debug info for r0v55 java.util.ArrayList: [D('dateList' java.util.List), D('value' java.lang.Object)] */
    public static String toXML(AstronomicalCalendar ac) {
        String tagName;
        ZmanimFormatter formatter = new ZmanimFormatter(5, new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"));
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String output = "<";
        if (ac.getClass().getName().endsWith("AstronomicalCalendar")) {
            output = new StringBuffer().append(output).append("AstronomicalTimes").toString();
        } else if (ac.getClass().getName().endsWith("ZmanimCalendar")) {
            output = new StringBuffer().append(output).append("Zmanim").toString();
        }
        String output2 = new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(output).append(" date=\"").append(df.format(ac.getCalendar().getTime())).append("\"").toString()).append(" type=\"").append(ac.getClass().getName()).append("\"").toString()).append(" algorithm=\"").append(ac.getAstronomicalCalculator().getCalculatorName()).append("\"").toString()).append(" location=\"").append(ac.getGeoLocation().getLocationName()).append("\"").toString()).append(" latitude=\"").append(ac.getGeoLocation().getLatitude()).append("\"").toString()).append(" longitude=\"").append(ac.getGeoLocation().getLongitude()).append("\"").toString()).append(" elevation=\"").append(ac.getGeoLocation().getElevation()).append("\"").toString()).append(" timeZoneName=\"").append(ac.getGeoLocation().getTimeZone().getDisplayName()).append("\"").toString()).append(" timeZoneID=\"").append(ac.getGeoLocation().getTimeZone().getID()).append("\"").toString()).append(" timeZoneOffset=\"").append(((double) ac.getGeoLocation().getTimeZone().getOffset(ac.getCalendar().getTimeInMillis())) / 3600000.0d).append("\"").toString()).append(">\n").toString();
        Method[] theMethods = ac.getClass().getMethods();
        List dateList = new ArrayList();
        List durationList = new ArrayList();
        List otherList = new ArrayList();
        String tagName2 = "";
        int i = 0;
        while (i < theMethods.length) {
            if (includeMethod(theMethods[i])) {
                tagName = theMethods[i].getName().substring(3);
                try {
                    Object value = theMethods[i].invoke(ac, null);
                    if (value == null) {
                        otherList.add(new StringBuffer().append("<").append(tagName).append(">N/A</").append(tagName).append(">").toString());
                    } else if (value instanceof Date) {
                        dateList.add(new Zman((Date) value, tagName));
                    } else if (value instanceof Long) {
                        durationList.add(new Zman((long) ((int) ((Long) value).longValue()), tagName));
                    } else {
                        otherList.add(new StringBuffer().append("<").append(tagName).append(">").append(value).append("</").append(tagName).append(">").toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                tagName = tagName2;
            }
            i++;
            tagName2 = tagName;
        }
        Collections.sort(dateList, Zman.DATE_ORDER);
        String output3 = output2;
        for (int i2 = 0; i2 < dateList.size(); i2++) {
            Zman zman = (Zman) dateList.get(i2);
            output3 = new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(output3).append("\t<").append(zman.getZmanLabel()).toString()).append(">").toString()).append(formatter.formatDateTime(zman.getZman(), ac.getCalendar())).append("</").append(zman.getZmanLabel()).append(">\n").toString();
        }
        Collections.sort(durationList, Zman.DURATION_ORDER);
        String output4 = output3;
        for (int i3 = 0; i3 < durationList.size(); i3++) {
            Zman zman2 = (Zman) durationList.get(i3);
            output4 = new StringBuffer().append(new StringBuffer().append(new StringBuffer().append(output4).append("\t<").append(zman2.getZmanLabel()).toString()).append(">").toString()).append(formatter.format((int) zman2.getDuration())).append("</").append(zman2.getZmanLabel()).append(">\n").toString();
        }
        String output5 = output4;
        for (int i4 = 0; i4 < otherList.size(); i4++) {
            output5 = new StringBuffer().append(output5).append("\t").append(otherList.get(i4)).append("\n").toString();
        }
        if (ac.getClass().getName().endsWith("AstronomicalCalendar")) {
            return new StringBuffer().append(output5).append("</AstronomicalTimes>").toString();
        }
        return ac.getClass().getName().endsWith("ZmanimCalendar") ? new StringBuffer().append(output5).append("</Zmanim>").toString() : output5;
    }

    private static boolean includeMethod(Method method) {
        List methodWhiteList = new ArrayList();
        List methodBlackList = new ArrayList();
        if (methodWhiteList.contains(method.getName())) {
            return true;
        }
        if (methodBlackList.contains(method.getName())) {
            return false;
        }
        if (method.getParameterTypes().length > 0) {
            return false;
        }
        if (!method.getName().startsWith("get")) {
            return false;
        }
        if (method.getReturnType().getName().endsWith("Date") || method.getReturnType().getName().endsWith("long")) {
            return true;
        }
        return false;
    }
}
