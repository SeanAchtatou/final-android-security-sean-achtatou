package net.sourceforge.zmanim.util;

import net.sourceforge.zmanim.AstronomicalCalendar;

public abstract class AstronomicalCalculator implements Cloneable {
    private double refraction = 0.5666666666666667d;
    private double solarRadius = 0.26666666666666666d;

    public abstract String getCalculatorName();

    public abstract double getUTCSunrise(AstronomicalCalendar astronomicalCalendar, double d, boolean z);

    public abstract double getUTCSunset(AstronomicalCalendar astronomicalCalendar, double d, boolean z);

    public static AstronomicalCalculator getDefault() {
        return new SunTimesCalculator();
    }

    /* access modifiers changed from: package-private */
    public double getElevationAdjustment(double elevation) {
        return Math.toDegrees(Math.acos(6356.9d / ((elevation / 1000.0d) + 6356.9d)));
    }

    /* access modifiers changed from: package-private */
    public double adjustZenith(double zenith, double elevation) {
        if (zenith == 90.0d) {
            return zenith + getSolarRadius() + getRefraction() + getElevationAdjustment(elevation);
        }
        return zenith;
    }

    /* access modifiers changed from: package-private */
    public double getRefraction() {
        return this.refraction;
    }

    public void setRefraction(double refraction2) {
        this.refraction = refraction2;
    }

    /* access modifiers changed from: package-private */
    public double getSolarRadius() {
        return this.solarRadius;
    }

    public void setSolarRadius(double solarRadius2) {
        this.solarRadius = solarRadius2;
    }

    public Object clone() {
        try {
            return (AstronomicalCalculator) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.print("Required by the compiler. Should never be reached since we implement clone()");
            return null;
        }
    }
}
