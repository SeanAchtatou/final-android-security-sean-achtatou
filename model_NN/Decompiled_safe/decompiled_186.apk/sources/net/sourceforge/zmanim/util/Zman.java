package net.sourceforge.zmanim.util;

import java.util.Comparator;
import java.util.Date;

public class Zman {
    public static final Comparator DATE_ORDER = new Comparator() {
        public int compare(Object o1, Object o2) {
            return ((Zman) o1).getZman().compareTo(((Zman) o2).getZman());
        }
    };
    public static final Comparator DURATION_ORDER = new Comparator() {
        public int compare(Object o1, Object o2) {
            Zman z1 = (Zman) o1;
            Zman z2 = (Zman) o2;
            if (z1.getDuration() == z2.getDuration()) {
                return 0;
            }
            return z1.getDuration() > z2.getDuration() ? 1 : -1;
        }
    };
    public static final Comparator NAME_ORDER = new Comparator() {
        public int compare(Object o1, Object o2) {
            return ((Zman) o1).getZmanLabel().compareTo(((Zman) o2).getZmanLabel());
        }
    };
    private long duration;
    private Date zman;
    private Date zmanDescription;
    private String zmanLabel;

    public Zman(Date date, String label) {
        this.zmanLabel = label;
        this.zman = date;
    }

    public Zman(long duration2, String label) {
        this.zmanLabel = label;
        this.duration = duration2;
    }

    public Date getZman() {
        return this.zman;
    }

    public void setZman(Date date) {
        this.zman = date;
    }

    public long getDuration() {
        return this.duration;
    }

    public void setDuration(long duration2) {
        this.duration = duration2;
    }

    public String getZmanLabel() {
        return this.zmanLabel;
    }

    public void setZmanLabel(String label) {
        this.zmanLabel = label;
    }

    public Date getZmanDescription() {
        return this.zmanDescription;
    }

    public void setZmanDescription(Date zmanDescription2) {
        this.zmanDescription = zmanDescription2;
    }
}
