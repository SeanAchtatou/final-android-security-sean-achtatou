package net.sourceforge.zmanim.util;

import net.sourceforge.zmanim.AstronomicalCalendar;

public class SunTimesCalculator extends AstronomicalCalculator {
    private static final double DEG_PER_HOUR = 15.0d;
    private static final int TYPE_SUNRISE = 0;
    private static final int TYPE_SUNSET = 1;
    public static final double ZENITH = 90.83333333333333d;
    private String calculatorName = "US Naval Almanac Algorithm";

    public String getCalculatorName() {
        return this.calculatorName;
    }

    public double getUTCSunrise(AstronomicalCalendar astronomicalCalendar, double zenith, boolean adjustForElevation) {
        double zenith2;
        if (adjustForElevation) {
            zenith2 = adjustZenith(zenith, astronomicalCalendar.getGeoLocation().getElevation());
        } else {
            zenith2 = adjustZenith(zenith, 0.0d);
        }
        return getTimeUTC(astronomicalCalendar.getCalendar().get(1), astronomicalCalendar.getCalendar().get(2) + 1, astronomicalCalendar.getCalendar().get(5), astronomicalCalendar.getGeoLocation().getLongitude(), astronomicalCalendar.getGeoLocation().getLatitude(), zenith2, 0);
    }

    public double getUTCSunset(AstronomicalCalendar astronomicalCalendar, double zenith, boolean adjustForElevation) {
        double zenith2;
        if (adjustForElevation) {
            zenith2 = adjustZenith(zenith, astronomicalCalendar.getGeoLocation().getElevation());
        } else {
            zenith2 = adjustZenith(zenith, 0.0d);
        }
        return getTimeUTC(astronomicalCalendar.getCalendar().get(1), astronomicalCalendar.getCalendar().get(2) + 1, astronomicalCalendar.getCalendar().get(5), astronomicalCalendar.getGeoLocation().getLongitude(), astronomicalCalendar.getGeoLocation().getLatitude(), zenith2, 1);
    }

    private static double sinDeg(double deg) {
        return Math.sin(((2.0d * deg) * 3.141592653589793d) / 360.0d);
    }

    private static double acosDeg(double x) {
        return (Math.acos(x) * 360.0d) / 6.283185307179586d;
    }

    private static double asinDeg(double x) {
        return (Math.asin(x) * 360.0d) / 6.283185307179586d;
    }

    private static double tanDeg(double deg) {
        return Math.tan(((2.0d * deg) * 3.141592653589793d) / 360.0d);
    }

    private static double cosDeg(double deg) {
        return Math.cos(((2.0d * deg) * 3.141592653589793d) / 360.0d);
    }

    private static int getDayOfYear(int year, int month, int day) {
        return ((((month * 275) / 9) - (((month + 9) / 12) * ((((year - ((year / 4) * 4)) + 2) / 3) + 1))) + day) - 30;
    }

    private static double getHoursFromMeridian(double longitude) {
        return longitude / DEG_PER_HOUR;
    }

    private static double getApproxTimeDays(int dayOfYear, double hoursFromMeridian, int type) {
        if (type == 0) {
            return ((double) dayOfYear) + ((6.0d - hoursFromMeridian) / 24.0d);
        }
        return ((double) dayOfYear) + ((18.0d - hoursFromMeridian) / 24.0d);
    }

    private static double getMeanAnomaly(int dayOfYear, double longitude, int type) {
        return (0.9856d * getApproxTimeDays(dayOfYear, getHoursFromMeridian(longitude), type)) - 3.289d;
    }

    private static double getSunTrueLongitude(double sunMeanAnomaly) {
        double l = (1.916d * sinDeg(sunMeanAnomaly)) + sunMeanAnomaly + (0.02d * sinDeg(2.0d * sunMeanAnomaly)) + 282.634d;
        if (l >= 360.0d) {
            l -= 360.0d;
        }
        if (l < 0.0d) {
            return l + 360.0d;
        }
        return l;
    }

    private static double getSunRightAscensionHours(double sunTrueLongitude) {
        double ra = 57.29577951308232d * Math.atan(0.91764d * tanDeg(sunTrueLongitude));
        return (ra + ((Math.floor(sunTrueLongitude / 90.0d) * 90.0d) - (Math.floor(ra / 90.0d) * 90.0d))) / DEG_PER_HOUR;
    }

    private static double getCosLocalHourAngle(double sunTrueLongitude, double latitude, double zenith) {
        double sinDec = 0.39782d * sinDeg(sunTrueLongitude);
        return (cosDeg(zenith) - (sinDeg(latitude) * sinDec)) / (cosDeg(latitude) * cosDeg(asinDeg(sinDec)));
    }

    private static double getLocalMeanTime(double localHour, double sunRightAscensionHours, double approxTimeDays) {
        return ((localHour + sunRightAscensionHours) - (0.06571d * approxTimeDays)) - 6.622d;
    }

    /* JADX INFO: Multiple debug info for r16v1 int: [D('day' int), D('dayOfYear' int)] */
    /* JADX INFO: Multiple debug info for r14v1 double: [D('sunMeanAnomaly' double), D('year' int)] */
    /* JADX INFO: Multiple debug info for r14v2 double: [D('cosLocalHourAngle' double), D('sunMeanAnomaly' double)] */
    private static double getTimeUTC(int year, int month, int day, double longitude, double latitude, double zenith, int type) {
        double cosLocalHourAngle;
        int dayOfYear = getDayOfYear(year, month, day);
        double sunTrueLong = getSunTrueLongitude(getMeanAnomaly(dayOfYear, longitude, type));
        double sunRightAscensionHours = getSunRightAscensionHours(sunTrueLong);
        double sunMeanAnomaly = getCosLocalHourAngle(sunTrueLong, latitude, zenith);
        if (type == 0) {
            if (sunMeanAnomaly > 1.0d) {
            }
            cosLocalHourAngle = 360.0d - acosDeg(sunMeanAnomaly);
        } else {
            if (sunMeanAnomaly < -1.0d) {
            }
            cosLocalHourAngle = acosDeg(sunMeanAnomaly);
        }
        double pocessedTime = getLocalMeanTime(cosLocalHourAngle / DEG_PER_HOUR, sunRightAscensionHours, getApproxTimeDays(dayOfYear, getHoursFromMeridian(longitude), type)) - getHoursFromMeridian(longitude);
        while (pocessedTime < 0.0d) {
            pocessedTime += 24.0d;
        }
        while (pocessedTime >= 24.0d) {
            pocessedTime -= 24.0d;
        }
        return pocessedTime;
    }
}
