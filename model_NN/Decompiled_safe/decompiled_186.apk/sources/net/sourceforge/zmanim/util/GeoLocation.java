package net.sourceforge.zmanim.util;

import java.util.TimeZone;

public class GeoLocation implements Cloneable {
    private static final long HOUR_MILLIS = 3600000;
    private static final long MINUTE_MILLIS = 60000;
    private int DISTANCE;
    private int FINAL_BEARING;
    private int INITIAL_BEARING;
    private double elevation;
    private double latitude;
    private String locationName;
    private double longitude;
    private TimeZone timeZone;

    public double getElevation() {
        return this.elevation;
    }

    public void setElevation(double elevation2) {
        if (elevation2 < 0.0d) {
            throw new IllegalArgumentException("Elevation cannot be negative");
        }
        this.elevation = elevation2;
    }

    public GeoLocation(String name, double latitude2, double longitude2, TimeZone timeZone2) {
        this(name, latitude2, longitude2, 0.0d, timeZone2);
    }

    public GeoLocation(String name, double latitude2, double longitude2, double elevation2, TimeZone timeZone2) {
        this.DISTANCE = 0;
        this.INITIAL_BEARING = 1;
        this.FINAL_BEARING = 2;
        setLocationName(name);
        setLatitude(latitude2);
        setLongitude(longitude2);
        setElevation(elevation2);
        setTimeZone(timeZone2);
    }

    public GeoLocation() {
        this.DISTANCE = 0;
        this.INITIAL_BEARING = 1;
        this.FINAL_BEARING = 2;
        setLocationName("Greenwich, England");
        setLongitude(0.0d);
        setLatitude(51.4772d);
        setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    public void setLatitude(double latitude2) {
        if (latitude2 > 90.0d || latitude2 < -90.0d) {
            throw new IllegalArgumentException("Latitude must be between -90 and  90");
        }
        this.latitude = latitude2;
    }

    public void setLatitude(int degrees, int minutes, double seconds, String direction) {
        double tempLat = ((double) degrees) + ((((double) minutes) + (seconds / 60.0d)) / 60.0d);
        if (tempLat > 90.0d || tempLat < 0.0d) {
            throw new IllegalArgumentException("Latitude must be between 0 and  90. Use direction of S instead of negative.");
        }
        if (direction.equals("S")) {
            tempLat *= -1.0d;
        } else if (!direction.equals("N")) {
            throw new IllegalArgumentException("Latitude direction must be N or S");
        }
        this.latitude = tempLat;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLongitude(double longitude2) {
        if (longitude2 > 180.0d || longitude2 < -180.0d) {
            throw new IllegalArgumentException("Longitude must be between -180 and  180");
        }
        this.longitude = longitude2;
    }

    public void setLongitude(int degrees, int minutes, double seconds, String direction) {
        double longTemp = ((double) degrees) + ((((double) minutes) + (seconds / 60.0d)) / 60.0d);
        if (longTemp > 180.0d || this.longitude < 0.0d) {
            throw new IllegalArgumentException("Longitude must be between 0 and  180. Use the ");
        }
        if (direction.equals("W")) {
            longTemp *= -1.0d;
        } else if (!direction.equals("E")) {
            throw new IllegalArgumentException("Longitude direction must be E or W");
        }
        this.longitude = longTemp;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public String getLocationName() {
        return this.locationName;
    }

    public void setLocationName(String name) {
        this.locationName = name;
    }

    public TimeZone getTimeZone() {
        return this.timeZone;
    }

    public void setTimeZone(TimeZone timeZone2) {
        this.timeZone = timeZone2;
    }

    public long getLocalMeanTimeOffset() {
        return (long) (((getLongitude() * 4.0d) * 60000.0d) - ((double) getTimeZone().getRawOffset()));
    }

    public double getGeodesicInitialBearing(GeoLocation location) {
        return vincentyFormula(location, this.INITIAL_BEARING);
    }

    public double getGeodesicFinalBearing(GeoLocation location) {
        return vincentyFormula(location, this.FINAL_BEARING);
    }

    public double getGeodesicDistance(GeoLocation location) {
        return vincentyFormula(location, this.DISTANCE);
    }

    private double vincentyFormula(GeoLocation location, int formula) {
        double L = Math.toRadians(location.getLongitude() - getLongitude());
        double U1 = Math.atan((1.0d - 0.0033528106647474805d) * Math.tan(Math.toRadians(getLatitude())));
        double U2 = Math.atan((1.0d - 0.0033528106647474805d) * Math.tan(Math.toRadians(location.getLatitude())));
        double sinU1 = Math.sin(U1);
        double cosU1 = Math.cos(U1);
        double sinU2 = Math.sin(U2);
        double cosU2 = Math.cos(U2);
        double lambda = L;
        double lambdaP = 6.283185307179586d;
        double iterLimit = 20.0d;
        double sinLambda = 0.0d;
        double cosLambda = 0.0d;
        double sinSigma = 0.0d;
        double cosSigma = 0.0d;
        double sigma = 0.0d;
        double cosSqAlpha = 0.0d;
        double cos2SigmaM = 0.0d;
        while (Math.abs(lambda - lambdaP) > 1.0E-12d) {
            iterLimit -= 1.0d;
            if (iterLimit <= 0.0d) {
                break;
            }
            sinLambda = Math.sin(lambda);
            cosLambda = Math.cos(lambda);
            sinSigma = Math.sqrt((cosU2 * sinLambda * cosU2 * sinLambda) + (((cosU1 * sinU2) - ((sinU1 * cosU2) * cosLambda)) * ((cosU1 * sinU2) - ((sinU1 * cosU2) * cosLambda))));
            if (sinSigma == 0.0d) {
                return 0.0d;
            }
            cosSigma = (sinU1 * sinU2) + (cosU1 * cosU2 * cosLambda);
            sigma = Math.atan2(sinSigma, cosSigma);
            double sinAlpha = ((cosU1 * cosU2) * sinLambda) / sinSigma;
            cosSqAlpha = 1.0d - (sinAlpha * sinAlpha);
            cos2SigmaM = cosSigma - (((2.0d * sinU1) * sinU2) / cosSqAlpha);
            if (Double.isNaN(cos2SigmaM)) {
                cos2SigmaM = 0.0d;
            }
            double C = (0.0033528106647474805d / 16.0d) * cosSqAlpha * (4.0d + ((4.0d - (3.0d * cosSqAlpha)) * 0.0033528106647474805d));
            lambdaP = lambda;
            lambda = L + ((1.0d - C) * 0.0033528106647474805d * sinAlpha * ((C * sinSigma * ((C * cosSigma * (-1.0d + (2.0d * cos2SigmaM * cos2SigmaM))) + cos2SigmaM)) + sigma));
        }
        if (iterLimit == 0.0d) {
            return Double.NaN;
        }
        double uSq = (((6378137.0d * 6378137.0d) - (6356752.3142d * 6356752.3142d)) * cosSqAlpha) / (6356752.3142d * 6356752.3142d);
        double B = (uSq / 1024.0d) * (256.0d + ((-128.0d + ((74.0d - (47.0d * uSq)) * uSq)) * uSq));
        double distance = 6356752.3142d * (1.0d + ((uSq / 16384.0d) * (4096.0d + ((-768.0d + ((320.0d - (175.0d * uSq)) * uSq)) * uSq)))) * (sigma - ((B * sinSigma) * (((B / 4.0d) * (((-1.0d + ((2.0d * cos2SigmaM) * cos2SigmaM)) * cosSigma) - ((((B / 6.0d) * cos2SigmaM) * (-3.0d + ((4.0d * sinSigma) * sinSigma))) * (-3.0d + ((4.0d * cos2SigmaM) * cos2SigmaM))))) + cos2SigmaM)));
        double fwdAz = Math.toDegrees(Math.atan2(cosU2 * sinLambda, (cosU1 * sinU2) - ((sinU1 * cosU2) * cosLambda)));
        double revAz = Math.toDegrees(Math.atan2(cosU1 * sinLambda, ((-sinU1) * cosU2) + (cosU1 * sinU2 * cosLambda)));
        if (formula == this.DISTANCE) {
            return distance;
        }
        if (formula == this.INITIAL_BEARING) {
            return fwdAz;
        }
        if (formula == this.FINAL_BEARING) {
            return revAz;
        }
        return Double.NaN;
    }

    public double getRhumbLineBearing(GeoLocation location) {
        double dLon = Math.toRadians(location.getLongitude() - getLongitude());
        double dPhi = Math.log(Math.tan((Math.toRadians(location.getLatitude()) / 2.0d) + 0.7853981633974483d) / Math.tan((Math.toRadians(getLatitude()) / 2.0d) + 0.7853981633974483d));
        if (Math.abs(dLon) > 3.141592653589793d) {
            dLon = dLon > 0.0d ? -(6.283185307179586d - dLon) : 6.283185307179586d + dLon;
        }
        return Math.toDegrees(Math.atan2(dLon, dPhi));
    }

    public double getRhumbLineDistance(GeoLocation location) {
        double dLat = Math.toRadians(location.getLatitude() - getLatitude());
        double dLon = Math.toRadians(Math.abs(location.getLongitude() - getLongitude()));
        double q = Math.abs(dLat) > 1.0E-10d ? dLat / Math.log(Math.tan((Math.toRadians(location.getLongitude()) / 2.0d) + 0.7853981633974483d) / Math.tan((Math.toRadians(getLatitude()) / 2.0d) + 0.7853981633974483d)) : Math.cos(Math.toRadians(getLatitude()));
        if (dLon > 3.141592653589793d) {
            dLon = 6.283185307179586d - dLon;
        }
        return Math.sqrt((dLat * dLat) + (q * q * dLon * dLon)) * 6371.0d;
    }

    public String toXML() {
        StringBuffer sb = new StringBuffer();
        sb.append("<GeoLocation>\n");
        sb.append("\t<LocationName>").append(getLocationName()).append("</LocationName>\n");
        sb.append("\t<Latitude>").append(getLatitude()).append("&deg;").append("</Latitude>\n");
        sb.append("\t<Longitude>").append(getLongitude()).append("&deg;").append("</Longitude>\n");
        sb.append("\t<Elevation>").append(getElevation()).append(" Meters").append("</Elevation>\n");
        sb.append("\t<TimezoneName>").append(getTimeZone().getID()).append("</TimezoneName>\n");
        sb.append("\t<TimeZoneDisplayName>").append(getTimeZone().getDisplayName()).append("</TimeZoneDisplayName>\n");
        sb.append("\t<TimezoneGMTOffset>").append(((long) getTimeZone().getRawOffset()) / 3600000).append("</TimezoneGMTOffset>\n");
        sb.append("\t<TimezoneDSTOffset>").append(((long) getTimeZone().getDSTSavings()) / 3600000).append("</TimezoneDSTOffset>\n");
        sb.append("</GeoLocation>");
        return sb.toString();
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof GeoLocation)) {
            return false;
        }
        GeoLocation geo = (GeoLocation) object;
        return Double.doubleToLongBits(this.latitude) == Double.doubleToLongBits(geo.latitude) && Double.doubleToLongBits(this.longitude) == Double.doubleToLongBits(geo.longitude) && this.elevation == geo.elevation && (this.locationName != null ? this.locationName.equals(geo.locationName) : geo.locationName == null) && (this.timeZone != null ? this.timeZone.equals(geo.timeZone) : geo.timeZone == null);
    }

    public int hashCode() {
        long latLong = Double.doubleToLongBits(this.latitude);
        long lonLong = Double.doubleToLongBits(this.longitude);
        long elevLong = Double.doubleToLongBits(this.elevation);
        int i = 17 * 37;
        int result = getClass().hashCode() + 629;
        int result2 = result + (result * 37) + ((int) ((latLong >>> 32) ^ latLong));
        int result3 = result2 + (result2 * 37) + ((int) ((lonLong >>> 32) ^ lonLong));
        int result4 = result3 + (result3 * 37) + ((int) ((elevLong >>> 32) ^ elevLong));
        int result5 = result4 + (result4 * 37) + (this.locationName == null ? 0 : this.locationName.hashCode());
        return result5 + (result5 * 37) + (this.timeZone == null ? 0 : this.timeZone.hashCode());
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("\nLocation Name:\t\t\t").append(getLocationName());
        sb.append("\nLatitude:\t\t\t").append(getLatitude()).append("&deg;");
        sb.append("\nLongitude:\t\t\t").append(getLongitude()).append("&deg;");
        sb.append("\nElevation:\t\t\t").append(getElevation()).append(" Meters");
        sb.append("\nTimezone Name:\t\t\t").append(getTimeZone().getID());
        sb.append("\nTimezone GMT Offset:\t\t").append(((long) getTimeZone().getRawOffset()) / 3600000);
        sb.append("\nTimezone DST Offset:\t\t").append(((long) getTimeZone().getDSTSavings()) / 3600000);
        return sb.toString();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: net.sourceforge.zmanim.util.GeoLocation} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object clone() {
        /*
            r5 = this;
            r1 = 0
            java.lang.Object r3 = super.clone()     // Catch:{ CloneNotSupportedException -> 0x001c }
            r0 = r3
            net.sourceforge.zmanim.util.GeoLocation r0 = (net.sourceforge.zmanim.util.GeoLocation) r0     // Catch:{ CloneNotSupportedException -> 0x001c }
            r1 = r0
        L_0x0009:
            java.util.TimeZone r3 = r5.getTimeZone()
            java.lang.Object r3 = r3.clone()
            java.util.TimeZone r3 = (java.util.TimeZone) r3
            r1.timeZone = r3
            java.lang.String r3 = r5.getLocationName()
            r1.locationName = r3
            return r1
        L_0x001c:
            r3 = move-exception
            r2 = r3
            java.io.PrintStream r3 = java.lang.System.out
            java.lang.String r4 = "Required by the compiler. Should never be reached since we implement clone()"
            r3.print(r4)
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: net.sourceforge.zmanim.util.GeoLocation.clone():java.lang.Object");
    }
}
