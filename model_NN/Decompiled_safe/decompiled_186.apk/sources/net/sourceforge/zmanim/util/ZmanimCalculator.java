package net.sourceforge.zmanim.util;

import net.sourceforge.zmanim.AstronomicalCalendar;

public class ZmanimCalculator extends AstronomicalCalculator {
    private String calculatorName = "US Naval Almanac Algorithm";

    public String getCalculatorName() {
        return this.calculatorName;
    }

    public double getUTCSunrise(AstronomicalCalendar astronomicalCalendar, double zenith, boolean adjustForElevation) {
        double zenith2;
        if (adjustForElevation) {
            zenith2 = adjustZenith(zenith, astronomicalCalendar.getGeoLocation().getElevation());
        } else {
            zenith2 = adjustZenith(zenith, 0.0d);
        }
        double lngHour = astronomicalCalendar.getGeoLocation().getLongitude() / 15.0d;
        double t = ((double) astronomicalCalendar.getCalendar().get(6)) + ((6.0d - lngHour) / 24.0d);
        double m = (0.9856d * t) - 3.289d;
        double l = (1.916d * Math.sin(Math.toRadians(m))) + m + (0.02d * Math.sin(Math.toRadians(2.0d * m))) + 282.634d;
        while (l < 0.0d) {
            l += 360.0d;
        }
        while (l >= 360.0d) {
            l -= 360.0d;
        }
        double RA = Math.toDegrees(Math.atan(0.91764d * Math.tan(Math.toRadians(l))));
        while (RA < 0.0d) {
            RA += 360.0d;
        }
        while (RA >= 360.0d) {
            RA -= 360.0d;
        }
        double sinDec = 0.39782d * Math.sin(Math.toRadians(l));
        double UT = (((((360.0d - Math.toDegrees(Math.acos((Math.cos(Math.toRadians(zenith2)) - (Math.sin(Math.toRadians(astronomicalCalendar.getGeoLocation().getLatitude())) * sinDec)) / (Math.cos(Math.toRadians(astronomicalCalendar.getGeoLocation().getLatitude())) * Math.cos(Math.asin(sinDec)))))) / 15.0d) + ((RA + ((Math.floor(l / 90.0d) * 90.0d) - (Math.floor(RA / 90.0d) * 90.0d))) / 15.0d)) - (0.06571d * t)) - 6.622d) - lngHour;
        while (UT < 0.0d) {
            UT += 24.0d;
        }
        while (UT >= 24.0d) {
            UT -= 24.0d;
        }
        return UT;
    }

    public double getUTCSunset(AstronomicalCalendar astronomicalCalendar, double zenith, boolean adjustForElevation) {
        double zenith2;
        if (adjustForElevation) {
            zenith2 = adjustZenith(zenith, astronomicalCalendar.getGeoLocation().getElevation());
        } else {
            zenith2 = adjustZenith(zenith, 0.0d);
        }
        int N = astronomicalCalendar.getCalendar().get(6);
        double lngHour = astronomicalCalendar.getGeoLocation().getLongitude() / 15.0d;
        double t = ((double) N) + ((18.0d - lngHour) / 24.0d);
        double M = (0.9856d * t) - 3.289d;
        double L = (1.916d * Math.sin(Math.toRadians(M))) + M + (0.02d * Math.sin(Math.toRadians(2.0d * M))) + 282.634d;
        while (L < 0.0d) {
            L += 360.0d;
        }
        while (L >= 360.0d) {
            L -= 360.0d;
        }
        double RA = Math.toDegrees(Math.atan(0.91764d * Math.tan(Math.toRadians(L))));
        while (RA < 0.0d) {
            RA += 360.0d;
        }
        while (RA >= 360.0d) {
            RA -= 360.0d;
        }
        double sinDec = 0.39782d * Math.sin(Math.toRadians(L));
        double UT = ((((Math.toDegrees(Math.acos((Math.cos(Math.toRadians(zenith2)) - (Math.sin(Math.toRadians(astronomicalCalendar.getGeoLocation().getLatitude())) * sinDec)) / (Math.cos(Math.toRadians(astronomicalCalendar.getGeoLocation().getLatitude())) * Math.cos(Math.asin(sinDec))))) / 15.0d) + ((RA + ((Math.floor(L / 90.0d) * 90.0d) - (Math.floor(RA / 90.0d) * 90.0d))) / 15.0d)) - (0.06571d * t)) - 6.622d) - lngHour;
        while (UT < 0.0d) {
            UT += 24.0d;
        }
        while (UT >= 24.0d) {
            UT -= 24.0d;
        }
        return UT;
    }
}
