package net.sourceforge.zmanim.util;

import java.util.Calendar;
import net.sourceforge.zmanim.AstronomicalCalendar;

public class JSuntimeCalculator extends AstronomicalCalculator {
    private String calculatorName = "US National Oceanic and Atmospheric Administration Algorithm";

    public String getCalculatorName() {
        return this.calculatorName;
    }

    public double getUTCSunrise(AstronomicalCalendar astronomicalCalendar, double zenith, boolean adjustForElevation) {
        double zenith2;
        if (adjustForElevation) {
            zenith2 = adjustZenith(zenith, astronomicalCalendar.getGeoLocation().getElevation());
        } else {
            zenith2 = adjustZenith(zenith, 0.0d);
        }
        return morningPhenomenon(dateToJulian(astronomicalCalendar.getCalendar()), astronomicalCalendar.getGeoLocation().getLatitude(), -astronomicalCalendar.getGeoLocation().getLongitude(), zenith2) / 60.0d;
    }

    public double getUTCSunset(AstronomicalCalendar astronomicalCalendar, double zenith, boolean adjustForElevation) {
        double zenith2;
        if (adjustForElevation) {
            zenith2 = adjustZenith(zenith, astronomicalCalendar.getGeoLocation().getElevation());
        } else {
            zenith2 = adjustZenith(zenith, 0.0d);
        }
        return eveningPhenomenon(dateToJulian(astronomicalCalendar.getCalendar()), astronomicalCalendar.getGeoLocation().getLatitude(), -astronomicalCalendar.getGeoLocation().getLongitude(), zenith2) / 60.0d;
    }

    /* JADX INFO: Multiple debug info for r10v1 double: [D('julian' double), D('hourangle' double)] */
    /* JADX INFO: Multiple debug info for r10v3 double: [D('delta' double), D('hourangle' double)] */
    /* JADX INFO: Multiple debug info for r10v4 double: [D('delta' double), D('timeDiff' double)] */
    /* JADX INFO: Multiple debug info for r10v6 double: [D('timeDiff' double), D('timeUTC' double)] */
    /* JADX INFO: Multiple debug info for r10v9 double: [D('newt' double), D('timeUTC' double)] */
    /* JADX INFO: Multiple debug info for r10v10 double: [D('newt' double), D('hourangle' double)] */
    /* JADX INFO: Multiple debug info for r10v12 double: [D('delta' double), D('hourangle' double)] */
    /* JADX INFO: Multiple debug info for r10v13 double: [D('delta' double), D('timeDiff' double)] */
    /* JADX INFO: Multiple debug info for r10v15 double: [D('timeDiff' double), D('morning' double)] */
    private static double morningPhenomenon(double julian, double latitude, double longitude, double zenithDistance) {
        double t = julianDayToJulianCenturies(julian);
        double timeUTC = julianDayToJulianCenturies((((((longitude - Math.toDegrees(hourAngleMorning(latitude, sunDeclination(t), zenithDistance))) * 4.0d) + 720.0d) - equationOfTime(t)) / 1440.0d) + julianCenturiesToJulianDay(t));
        return (((longitude - Math.toDegrees(hourAngleMorning(latitude, sunDeclination(timeUTC), zenithDistance))) * 4.0d) + 720.0d) - equationOfTime(timeUTC);
    }

    /* JADX INFO: Multiple debug info for r10v1 double: [D('julian' double), D('hourangle' double)] */
    /* JADX INFO: Multiple debug info for r10v3 double: [D('delta' double), D('hourangle' double)] */
    /* JADX INFO: Multiple debug info for r10v4 double: [D('delta' double), D('timeDiff' double)] */
    /* JADX INFO: Multiple debug info for r10v6 double: [D('timeDiff' double), D('timeUTC' double)] */
    /* JADX INFO: Multiple debug info for r10v9 double: [D('newt' double), D('timeUTC' double)] */
    /* JADX INFO: Multiple debug info for r10v10 double: [D('newt' double), D('hourangle' double)] */
    /* JADX INFO: Multiple debug info for r10v12 double: [D('delta' double), D('hourangle' double)] */
    /* JADX INFO: Multiple debug info for r10v13 double: [D('delta' double), D('timeDiff' double)] */
    /* JADX INFO: Multiple debug info for r10v15 double: [D('timeDiff' double), D('evening' double)] */
    private static double eveningPhenomenon(double julian, double latitude, double longitude, double zenithDistance) {
        double t = julianDayToJulianCenturies(julian);
        double timeUTC = julianDayToJulianCenturies((((((longitude - Math.toDegrees(hourAngleEvening(latitude, sunDeclination(t), zenithDistance))) * 4.0d) + 720.0d) - equationOfTime(t)) / 1440.0d) + julianCenturiesToJulianDay(t));
        return (((longitude - Math.toDegrees(hourAngleEvening(latitude, sunDeclination(timeUTC), zenithDistance))) * 4.0d) + 720.0d) - equationOfTime(timeUTC);
    }

    /* JADX INFO: Multiple debug info for r19v1 int: [D('date' java.util.Calendar), D('second' int)] */
    private static double dateToJulian(Calendar date) {
        int year = date.get(1);
        int month = date.get(2) + 1;
        int day = date.get(5);
        int hour = date.get(11);
        int minute = date.get(12);
        int second = date.get(13);
        double extra = ((100.0d * ((double) year)) + ((double) month)) - 190002.5d;
        return (((((((((double) minute) + (((double) second) / 60.0d)) / 60.0d) + ((double) hour)) / 24.0d) + ((((367.0d * ((double) year)) - Math.floor((7.0d * (((double) year) + Math.floor((((double) month) + 9.0d) / 12.0d))) / 4.0d)) + Math.floor((275.0d * ((double) month)) / 9.0d)) + ((double) day))) + 1721013.5d) - ((0.5d * extra) / Math.abs(extra))) + 0.5d;
    }

    private static double julianDayToJulianCenturies(double julian) {
        return (julian - 2451545.0d) / 36525.0d;
    }

    private static double julianCenturiesToJulianDay(double t) {
        return (36525.0d * t) + 2451545.0d;
    }

    /* JADX INFO: Multiple debug info for r14v1 double: [D('t' double), D('m' double)] */
    private static double equationOfTime(double t) {
        double epsilon = obliquityCorrection(t);
        double l0 = geomMeanLongSun(t);
        double e = eccentricityOfEarthsOrbit(t);
        double m = geometricMeanAnomalyOfSun(t);
        double y = Math.pow(Math.tan(Math.toRadians(epsilon) / 2.0d), 2.0d);
        return Math.toDegrees(((((Math.sin(2.0d * Math.toRadians(l0)) * y) - ((2.0d * e) * Math.sin(Math.toRadians(m)))) + ((((4.0d * e) * y) * Math.sin(Math.toRadians(m))) * Math.cos(2.0d * Math.toRadians(l0)))) - ((y * (0.5d * y)) * Math.sin(Math.toRadians(l0) * 4.0d))) - (Math.sin(Math.toRadians(m) * 2.0d) * (e * (1.25d * e)))) * 4.0d;
    }

    private static double sunDeclination(double t) {
        return Math.toDegrees(Math.asin(Math.sin(Math.toRadians(obliquityCorrection(t))) * Math.sin(Math.toRadians(sunsApparentLongitude(t)))));
    }

    private static double hourAngleMorning(double lat, double solarDec, double zenithDistance) {
        return Math.acos((Math.cos(Math.toRadians(zenithDistance)) / (Math.cos(Math.toRadians(lat)) * Math.cos(Math.toRadians(solarDec)))) - (Math.tan(Math.toRadians(lat)) * Math.tan(Math.toRadians(solarDec))));
    }

    private static double hourAngleEvening(double lat, double solarDec, double zenithDistance) {
        return -hourAngleMorning(lat, solarDec, zenithDistance);
    }

    private static double obliquityCorrection(double t) {
        return meanObliquityOfEcliptic(t) + (0.00256d * Math.cos(Math.toRadians(125.04d - (1934.136d * t))));
    }

    private static double meanObliquityOfEcliptic(double t) {
        return 23.0d + ((26.0d + (21.448d - (((46.815d + ((5.9E-4d - (0.001813d * t)) * t)) * t) / 60.0d))) / 60.0d);
    }

    private static double geomMeanLongSun(double t) {
        double l0 = 280.46646d + ((36000.76983d + (3.032E-4d * t)) * t);
        while (l0 >= 0.0d && l0 <= 360.0d) {
            if (l0 > 360.0d) {
                l0 -= 360.0d;
            }
            if (l0 < 0.0d) {
                l0 += 360.0d;
            }
        }
        return l0;
    }

    private static double eccentricityOfEarthsOrbit(double t) {
        return 0.016708634d - ((4.2037E-5d + (1.267E-7d * t)) * t);
    }

    private static double geometricMeanAnomalyOfSun(double t) {
        return 357.52911d + ((35999.05029d - (1.537E-4d * t)) * t);
    }

    private static double sunsApparentLongitude(double t) {
        return (sunsTrueLongitude(t) - 0.00569d) - (0.00478d * Math.sin(Math.toRadians(125.04d - (1934.136d * t))));
    }

    private static double sunsTrueLongitude(double t) {
        return geomMeanLongSun(t) + equationOfCentreForSun(t);
    }

    private static double equationOfCentreForSun(double t) {
        double m = geometricMeanAnomalyOfSun(t);
        return (Math.sin(Math.toRadians(m)) * (1.914602d - ((0.004817d + (1.4E-5d * t)) * t))) + (Math.sin(2.0d * Math.toRadians(m)) * (0.019993d - (1.01E-4d * t))) + (Math.sin(3.0d * Math.toRadians(m)) * 2.89E-4d);
    }
}
