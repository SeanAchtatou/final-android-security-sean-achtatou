package net.sourceforge.zmanim.util;

public class Time {
    private static final int HOUR_MILLIS = 3600000;
    private static final int MINUTE_MILLIS = 60000;
    private static final int SECOND_MILLIS = 1000;
    private int hours;
    private boolean isNegative;
    private int milliseconds;
    private int minutes;
    private int seconds;

    public Time(int hours2, int minutes2, int seconds2, int milliseconds2) {
        this.hours = 0;
        this.minutes = 0;
        this.seconds = 0;
        this.milliseconds = 0;
        this.isNegative = false;
        this.hours = hours2;
        this.minutes = minutes2;
        this.seconds = seconds2;
        this.milliseconds = milliseconds2;
    }

    public Time(double millis) {
        this((int) millis);
    }

    public Time(int millis) {
        this.hours = 0;
        this.minutes = 0;
        this.seconds = 0;
        this.milliseconds = 0;
        this.isNegative = false;
        if (millis < 0) {
            this.isNegative = true;
            millis = Math.abs(millis);
        }
        this.hours = millis / HOUR_MILLIS;
        int millis2 = millis - (this.hours * HOUR_MILLIS);
        this.minutes = millis2 / MINUTE_MILLIS;
        int millis3 = millis2 - (this.minutes * MINUTE_MILLIS);
        this.seconds = millis3 / SECOND_MILLIS;
        this.milliseconds = millis3 - (this.seconds * SECOND_MILLIS);
    }

    public boolean isNegative() {
        return this.isNegative;
    }

    public void setIsNegative(boolean isNegative2) {
        this.isNegative = isNegative2;
    }

    public int getHours() {
        return this.hours;
    }

    public void setHours(int hours2) {
        this.hours = hours2;
    }

    public int getMinutes() {
        return this.minutes;
    }

    public void setMinutes(int minutes2) {
        this.minutes = minutes2;
    }

    public int getSeconds() {
        return this.seconds;
    }

    public void setSeconds(int seconds2) {
        this.seconds = seconds2;
    }

    public int getMilliseconds() {
        return this.milliseconds;
    }

    public void setMilliseconds(int milliseconds2) {
        this.milliseconds = milliseconds2;
    }

    public double getTime() {
        return (double) ((this.hours * HOUR_MILLIS) + (this.minutes * MINUTE_MILLIS) + (this.seconds * SECOND_MILLIS) + this.milliseconds);
    }

    public String toString() {
        return new ZmanimFormatter().format(this);
    }
}
