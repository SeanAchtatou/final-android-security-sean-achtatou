package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Activity;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity_google extends Activity {
    private static String gtemp_c;
    private static String gtemp_f;
    private static ArrayList<String> nowCondition = new ArrayList<>();
    private static ArrayList<String> nowicon = new ArrayList<>();

    private static InputStream getWebWeatherSourceGoogle(String weatherlocationname, String tempcode) throws IOException {
        String[] items = weatherlocationname.split(",");
        String name = items[0].replaceAll(" ", "+");
        return new URL("http://www.google.com/ig/api?weather=" + name + "," + items[1].replace(" ", "")).openStream();
    }

    private InputStream getLocalWeatherSource() throws IOException {
        return getAssets().open("beijing_msn.xml");
    }

    /* JADX INFO: Multiple debug info for r6v15 'is'  java.io.InputStream: [D('tempcode' java.lang.String), D('is' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r5v26 org.xmlpull.v1.XmlPullParserFactory: [D('factory' org.xmlpull.v1.XmlPullParserFactory), D('weatherlocationname' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v33 java.lang.String: [D('tagName' java.lang.String), D('eventType' int)] */
    /* JADX INFO: Multiple debug info for r5v37 java.lang.String: [D('file' java.lang.String), D('url' java.lang.String)] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00d4 A[SYNTHETIC, Splitter:B:49:0x00d4] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00e6 A[SYNTHETIC, Splitter:B:57:0x00e6] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:36:0x00bb=Splitter:B:36:0x00bb, B:46:0x00cf=Splitter:B:46:0x00cf} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.lang.String[] parseWeatherGoogle(java.lang.String r5, java.lang.String r6) {
        /*
            r0 = 0
            java.io.InputStream r6 = getWebWeatherSourceGoogle(r5, r6)     // Catch:{ IOException -> 0x00b9, XmlPullParserException -> 0x00cd, all -> 0x00e1 }
            org.xmlpull.v1.XmlPullParserFactory r5 = org.xmlpull.v1.XmlPullParserFactory.newInstance()     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            r0 = 1
            r5.setNamespaceAware(r0)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            org.xmlpull.v1.XmlPullParser r0 = r5.newPullParser()     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            java.lang.String r5 = "UTF-8"
            r0.setInput(r6, r5)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            int r5 = r0.getEventType()     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
        L_0x001a:
            r1 = 1
            if (r5 != r1) goto L_0x0047
            if (r6 == 0) goto L_0x00f3
            r6.close()     // Catch:{ Exception -> 0x00ef }
            r5 = r6
        L_0x0023:
            java.util.ArrayList<java.lang.String> r5 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity_google.nowicon
            r6 = 0
            java.lang.Object r5 = r5.get(r6)
            java.lang.String r5 = (java.lang.String) r5
            int r5 = gWeathericon(r5)
            java.lang.String r6 = java.lang.Integer.toString(r5)
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]
            r0 = 0
            r5[r0] = r6
            r0 = 1
            java.util.ArrayList<java.lang.String> r6 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity_google.nowCondition
            r1 = 0
            java.lang.Object r6 = r6.get(r1)
            java.lang.String r6 = (java.lang.String) r6
            r5[r0] = r6
            return r5
        L_0x0047:
            if (r5 == 0) goto L_0x00af
            r1 = 1
            if (r5 == r1) goto L_0x00af
            r1 = 2
            if (r5 != r1) goto L_0x00b5
            java.lang.String r5 = r0.getName()     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            java.lang.String r1 = "condition"
            boolean r1 = r1.equals(r5)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            if (r1 == 0) goto L_0x0067
            java.util.ArrayList<java.lang.String> r1 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity_google.nowCondition     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            r2 = 0
            java.lang.String r3 = "data"
            java.lang.String r2 = r0.getAttributeValue(r2, r3)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            r1.add(r2)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
        L_0x0067:
            java.lang.String r1 = "temp_f"
            boolean r1 = r1.equals(r5)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            if (r1 == 0) goto L_0x0078
            r1 = 0
            java.lang.String r2 = "data"
            java.lang.String r1 = r0.getAttributeValue(r1, r2)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity_google.gtemp_f = r1     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
        L_0x0078:
            java.lang.String r1 = "temp_c"
            boolean r1 = r1.equals(r5)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            if (r1 == 0) goto L_0x0089
            r1 = 0
            java.lang.String r2 = "data"
            java.lang.String r1 = r0.getAttributeValue(r1, r2)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity_google.gtemp_c = r1     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
        L_0x0089:
            java.lang.String r1 = "icon"
            boolean r5 = r1.equals(r5)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            if (r5 == 0) goto L_0x00af
            r5 = 0
            java.lang.String r1 = "data"
            java.lang.String r5 = r0.getAttributeValue(r5, r1)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            r1 = 47
            int r1 = r5.lastIndexOf(r1)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            int r1 = r1 + 1
            r2 = 46
            int r2 = r5.lastIndexOf(r2)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            java.lang.String r5 = r5.substring(r1, r2)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            java.util.ArrayList<java.lang.String> r1 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity_google.nowicon     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            r1.add(r5)     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
        L_0x00af:
            int r5 = r0.next()     // Catch:{ IOException -> 0x00fd, XmlPullParserException -> 0x00fb }
            goto L_0x001a
        L_0x00b5:
            r1 = 3
            if (r5 == r1) goto L_0x00af
            goto L_0x00af
        L_0x00b9:
            r5 = move-exception
            r6 = r0
        L_0x00bb:
            r5.printStackTrace()     // Catch:{ all -> 0x00f6 }
            if (r6 == 0) goto L_0x00f3
            r6.close()     // Catch:{ Exception -> 0x00c6 }
            r5 = r6
            goto L_0x0023
        L_0x00c6:
            r5 = move-exception
            r5.printStackTrace()
            r5 = r6
            goto L_0x0023
        L_0x00cd:
            r5 = move-exception
            r6 = r0
        L_0x00cf:
            r5.printStackTrace()     // Catch:{ all -> 0x00f6 }
            if (r6 == 0) goto L_0x00f3
            r6.close()     // Catch:{ Exception -> 0x00da }
            r5 = r6
            goto L_0x0023
        L_0x00da:
            r5 = move-exception
            r5.printStackTrace()
            r5 = r6
            goto L_0x0023
        L_0x00e1:
            r5 = move-exception
            r6 = r5
            r5 = r0
        L_0x00e4:
            if (r5 == 0) goto L_0x00e9
            r5.close()     // Catch:{ Exception -> 0x00ea }
        L_0x00e9:
            throw r6
        L_0x00ea:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x00e9
        L_0x00ef:
            r5 = move-exception
            r5.printStackTrace()
        L_0x00f3:
            r5 = r6
            goto L_0x0023
        L_0x00f6:
            r5 = move-exception
            r4 = r5
            r5 = r6
            r6 = r4
            goto L_0x00e4
        L_0x00fb:
            r5 = move-exception
            goto L_0x00cf
        L_0x00fd:
            r5 = move-exception
            goto L_0x00bb
        */
        throw new UnsupportedOperationException("Method not decompiled: factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity_google.parseWeatherGoogle(java.lang.String, java.lang.String):java.lang.String[]");
    }

    public static int gWeathericon(String icon_name) {
        int gicon = 44;
        if (icon_name.equals("weather_cloudy-40")) {
            gicon = 26;
        }
        if (icon_name.equals("weather_drizzle-40")) {
            gicon = 11;
        }
        if (icon_name.equals("weather_dust-40")) {
            gicon = 19;
        }
        if (icon_name.equals("weather_fog-40")) {
            gicon = 20;
        }
        if (icon_name.equals("weather_haze-40")) {
            gicon = 21;
        }
        if (icon_name.equals("weather_heavyrain-40")) {
            gicon = 40;
        }
        if (icon_name.equals("weather_heavysnow-40")) {
            gicon = 42;
        }
        if (icon_name.equals("weather_icy-40")) {
            gicon = 25;
        }
        if (icon_name.equals("weather_mostlycloudy-40")) {
            gicon = 28;
        }
        if (icon_name.equals("weather_overcast-40")) {
            gicon = 26;
        }
        if (icon_name.equals("weather_partlycloudy-40")) {
            gicon = 30;
        }
        if (icon_name.equals("weather_rain-40")) {
            gicon = 12;
        }
        if (icon_name.equals("weather_rainsnow-40")) {
            gicon = 7;
        }
        if (icon_name.equals("weather_scatteredshowers-40")) {
            gicon = 39;
        }
        if (icon_name.equals("weather_scatteredthunderstorms-40")) {
            gicon = 38;
        }
        if (icon_name.equals("weather_sleet-40")) {
            gicon = 10;
        }
        if (icon_name.equals("weather_smoke-40")) {
            gicon = 22;
        }
        if (icon_name.equals("weather_snow-40")) {
            gicon = 14;
        }
        if (icon_name.equals("weather_snowflurries-40")) {
            gicon = 13;
        }
        if (icon_name.equals("weather_sunny-40")) {
            gicon = 32;
        }
        if (icon_name.equals("weather_thunderstorms-40")) {
            gicon = 0;
        }
        if (icon_name.equals("weather_windy-40")) {
            gicon = 24;
        }
        if (icon_name.equals("chance_of_rain")) {
            gicon = 39;
        }
        if (icon_name.equals("chance_of_snow")) {
            gicon = 41;
        }
        if (icon_name.equals("chance_of_storm")) {
            gicon = 39;
        }
        if (icon_name.equals("chance_of_tstorm")) {
            gicon = 38;
        }
        if (icon_name.equals("cloudy")) {
            gicon = 26;
        }
        if (icon_name.equals("dust")) {
            gicon = 19;
        }
        if (icon_name.equals("flurries")) {
            gicon = 13;
        }
        if (icon_name.equals("fog")) {
            gicon = 20;
        }
        if (icon_name.equals("haze")) {
            gicon = 21;
        }
        if (icon_name.equals("icy")) {
            gicon = 8;
        }
        if (icon_name.equals("mist")) {
            gicon = 11;
        }
        if (icon_name.equals("mostly_cloudy")) {
            gicon = 28;
        }
        if (icon_name.equals("mostly_sunny")) {
            gicon = 34;
        }
        if (icon_name.equals("partly_cloudy")) {
            gicon = 30;
        }
        if (icon_name.equals("rain")) {
            gicon = 12;
        }
        if (icon_name.equals("sleet")) {
            gicon = 5;
        }
        if (icon_name.equals("smoke")) {
            gicon = 22;
        }
        if (icon_name.equals("snow")) {
            gicon = 14;
        }
        if (icon_name.equals("storm")) {
            gicon = 0;
        }
        if (icon_name.equals("sunny")) {
            gicon = 32;
        }
        if (icon_name.equals("thunderstorm")) {
            return 0;
        }
        return gicon;
    }
}
