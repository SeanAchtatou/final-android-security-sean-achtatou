package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import java.util.Calendar;
import net.sourceforge.zmanim.util.ZmanimFormatter;

public class UberColorPickerDialog extends Dialog {
    private int mInitialColor;
    /* access modifiers changed from: private */
    public OnColorChangedListener mListener;
    private boolean mShowTitle;

    public interface OnColorChangedListener {
        void colorChanged(int i);
    }

    public UberColorPickerDialog(Context context, OnColorChangedListener listener, int initialColor, boolean showTitle) {
        super(context);
        this.mListener = listener;
        this.mInitialColor = initialColor;
        this.mShowTitle = showTitle;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OnColorChangedListener l = new OnColorChangedListener() {
            public void colorChanged(int color) {
                UberColorPickerDialog.this.mListener.colorChanged(color);
                UberColorPickerDialog.this.dismiss();
            }
        };
        DisplayMetrics dm = new DisplayMetrics();
        getWindow().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int screenWidth = dm.widthPixels;
        int screenHeight = dm.heightPixels;
        if (!this.mShowTitle) {
            getWindow().requestFeature(1);
            Toast.makeText(getContext(), "Pick a color (try the trackball)", 0).show();
        } else {
            setTitle("Pick a color (try the trackball)");
        }
        try {
            setContentView(new ColorPickerView(getContext(), l, screenWidth, screenHeight, this.mInitialColor));
        } catch (Exception e) {
            dismiss();
        }
    }

    public static boolean isGray(int color) {
        return ((color >> 16) & 255) == (color & 255) && ((color >> 8) & 255) == (color & 255);
    }

    public static boolean isGray(int[] rgb) {
        return rgb[1] == rgb[0] && rgb[2] == rgb[0];
    }

    private static class ColorPickerView extends View {
        private static final boolean[] ENABLED_METHODS = {true, true, true, true, true, true, true};
        private static final int FIRST_HOR_SLIDER_POS_Y = 20;
        private static final int METHOD_HSV_SLIDERS = 5;
        private static final int METHOD_HS_V_PALETTE = 0;
        private static final int METHOD_HV_S_PALETTE = 1;
        private static final int METHOD_RGB_SLIDERS = 4;
        private static int METHOD_SELECTOR_POS_X = (PALETTE_DIM + 10);
        private static final int METHOD_SELECTOR_SIZE = 40;
        private static final int METHOD_SELECTOR_SPACING = 10;
        private static final int METHOD_SV_H_PALETTE = 2;
        private static final int METHOD_UV_Y_PALETTE = 3;
        private static final int METHOD_YUV_SLIDERS = 6;
        private static int NUM_ENABLED_METHODS = ENABLED_METHODS.length;
        private static final int PALETTE_CENTER_X = PALETTE_RADIUS;
        private static final int PALETTE_CENTER_Y = PALETTE_RADIUS;
        private static final int PALETTE_DIM = (SWATCH_WIDTH * 2);
        private static int PALETTE_POS_X = 0;
        private static int PALETTE_POS_Y = 60;
        private static final int PALETTE_RADIUS = (PALETTE_DIM / 2);
        private static final float PI = 3.1415927f;
        private static final int SLIDER_THICKNESS = 40;
        private static final int SWATCH_HEIGHT = 60;
        private static int SWATCH_WIDTH = 95;
        private static final int TEXT_HALF_SIZE = 6;
        private static int[] TEXT_HEX_POS = new int[2];
        private static int[] TEXT_HSV_POS = new int[2];
        private static int[] TEXT_RGB_POS = new int[2];
        private static final int TEXT_SIZE = 12;
        private static int[] TEXT_YUV_POS = new int[2];
        private static final int TRACKED_NONE = -1;
        private static final int TRACK_B_SLIDER = 62;
        private static final int TRACK_G_SLIDER = 61;
        private static final int TRACK_HOR_VALUE_SLIDER = 72;
        private static final int TRACK_HOR_Y_SLIDER = 80;
        private static final int TRACK_HS_PALETTE = 30;
        private static final int TRACK_HV_PALETTE = 20;
        private static final int TRACK_H_SLIDER = 70;
        private static final int TRACK_R_SLIDER = 60;
        private static final int TRACK_SV_PALETTE = 40;
        private static final int TRACK_SWATCH_NEW = 11;
        private static final int TRACK_SWATCH_OLD = 10;
        private static final int TRACK_S_SLIDER = 71;
        private static final int TRACK_UV_PALETTE = 50;
        private static final int TRACK_U_SLIDER = 81;
        private static final int TRACK_VER_H_SLIDER = 41;
        private static final int TRACK_VER_S_SLIDER = 21;
        private static final int TRACK_VER_VALUE_SLIDER = 31;
        private static final int TRACK_VER_Y_SLIDER = 51;
        private static final int TRACK_V_SLIDER = 82;
        private static int VIEW_DIM_X = ((PALETTE_DIM + 10) + 40);
        private static int VIEW_DIM_Y = 280;
        private int[] mCoord = new int[3];
        private Shader mFadeInBottom;
        private Shader mFadeInLeft;
        private Shader mFadeInRight;
        private Shader mFadeInTop;
        private int mFocusedControl = TRACKED_NONE;
        private Paint[] mHSSmall = new Paint[2];
        private float[] mHSV = new float[3];
        private boolean mHSVenabled = true;
        private String mHexStr = "";
        private boolean mHexenabled = true;
        private Rect[] mHorSliderRects = new Rect[3];
        private Bitmap[] mHorSlidersBM = new Bitmap[3];
        private Canvas[] mHorSlidersCv = new Canvas[3];
        private OnColorChangedListener mListener;
        private int mMethod = 0;
        private int[] mMethodSelectRectMap = null;
        private Rect[] mMethodSelectorRects = null;
        private Rect mNewSwatchRect = new Rect();
        private Rect mOldSwatchRect = new Rect();
        private int mOriginalColor = 0;
        private Paint mOvalHueSat;
        private Paint mOvalHueSatSmall;
        private Paint mOvalHueVal;
        private Paint mOvalHueValSmall;
        private Rect mPaletteRect = new Rect();
        private Paint mPosMarker;
        private int[] mRGB = new int[3];
        private Paint[] mRGBSmall = new Paint[3];
        private boolean mRGBenabled = true;
        private Paint mSVSmall;
        private Paint mSatFader;
        private Shader mSatValMask;
        private Paint mSatValPalette;
        private boolean mShownYUVWarnedAlready = false;
        private int[] mSpectrumColors;
        private int[] mSpectrumColorsRev;
        private Paint mSwatchNew;
        private Paint mSwatchOld;
        private Paint mText;
        private long mTimeOfLastSliderSwitch = 0;
        private int mTracking = TRACKED_NONE;
        private Paint mUVPalette;
        private Paint mUVSmall;
        private Paint mValDimmer;
        private Bitmap mVerSliderBM;
        private Canvas mVerSliderCv;
        private Rect mVerSliderRect = new Rect();
        private float[] mYUV = new float[3];
        private Paint[] mYUVSmall = new Paint[3];
        private boolean mYUVenabled = true;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
         arg types: [int, int, float, int, int, int, android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
         arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
         arg types: [int, int, int, int, int, int, android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void}
         arg types: [int, int, int[], ?[OBJECT, ARRAY]]
         candidates:
          ClspMth{android.graphics.SweepGradient.<init>(float, float, long, long):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, int, int):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, long[], float[]):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void}
         arg types: [int, int, float, ?, int, android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void}
         arg types: [int, int, float, int, ?, android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
         arg types: [int, int, int, float, int, ?, android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void}
         arg types: [int, int, int, ?, int, android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void}
         arg types: [int, int, int, int, ?, android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.RadialGradient.<init>(float, float, float, int, int, android.graphics.Shader$TileMode):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
         arg types: [int, int, int, int, int, ?, android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
         arg types: [int, int, int, int, int[], ?[OBJECT, ARRAY], android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
         arg types: [int, int, int, int, ?, int, android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
        ColorPickerView(Context c, OnColorChangedListener l, int width, int height, int color) throws Exception {
            super(c);
            setFocusable(true);
            boolean isFocusable = isFocusable();
            boolean requestFocus = requestFocus();
            this.mListener = l;
            this.mOriginalColor = color;
            Color.colorToHSV(color, this.mHSV);
            if (UberColorPickerDialog.isGray(color)) {
                this.mHSV[1] = 0.0f;
            }
            updateAllFromHSV();
            NUM_ENABLED_METHODS = 0;
            for (int i = 0; i < ENABLED_METHODS.length; i++) {
                if (ENABLED_METHODS[i]) {
                    NUM_ENABLED_METHODS++;
                }
            }
            if (NUM_ENABLED_METHODS == 0) {
                Toast.makeText(getContext(), "No color picker methods enabled.", 0).show();
                throw new Exception("At least one method must be enabled");
            }
            this.mMethodSelectorRects = new Rect[NUM_ENABLED_METHODS];
            this.mMethodSelectRectMap = new int[NUM_ENABLED_METHODS];
            if (width <= height) {
                SWATCH_WIDTH = (PALETTE_DIM + 40) / 2;
                PALETTE_POS_X = 0;
                PALETTE_POS_Y = 108;
                METHOD_SELECTOR_POS_X = PALETTE_POS_X + PALETTE_DIM + 40 + 10;
                this.mHSVenabled = ENABLED_METHODS[0] || ENABLED_METHODS[1] || ENABLED_METHODS[2] || ENABLED_METHODS[5];
                this.mRGBenabled = ENABLED_METHODS[4];
                this.mYUVenabled = ENABLED_METHODS[3] || ENABLED_METHODS[6];
                this.mHexenabled = ENABLED_METHODS[4];
                int prevEnabledMethod = TRACKED_NONE;
                for (int i2 = 0; i2 < NUM_ENABLED_METHODS; i2++) {
                    this.mMethodSelectorRects[i2] = new Rect(METHOD_SELECTOR_POS_X, i2 * TRACK_UV_PALETTE, METHOD_SELECTOR_POS_X + 40, (i2 * TRACK_UV_PALETTE) + 40);
                    int j = prevEnabledMethod + 1;
                    while (true) {
                        if (j >= ENABLED_METHODS.length) {
                            break;
                        } else if (ENABLED_METHODS[j]) {
                            this.mMethodSelectRectMap[i2] = j;
                            prevEnabledMethod = j;
                            break;
                        } else {
                            j++;
                        }
                    }
                }
                this.mOldSwatchRect.set(0, 48, SWATCH_WIDTH, 108);
                this.mNewSwatchRect.set(SWATCH_WIDTH, 48, SWATCH_WIDTH * 2, 108);
                this.mPaletteRect.set(0, PALETTE_POS_Y, PALETTE_DIM, PALETTE_POS_Y + PALETTE_DIM);
                this.mVerSliderRect.set(PALETTE_DIM, PALETTE_POS_Y, PALETTE_DIM + 40, PALETTE_POS_Y + PALETTE_DIM);
                this.mHorSliderRects[0] = new Rect(0, PALETTE_POS_Y + 20, PALETTE_DIM, PALETTE_POS_Y + 20 + 40);
                this.mHorSliderRects[1] = new Rect(0, PALETTE_POS_Y + 20 + TRACK_UV_PALETTE, PALETTE_DIM, PALETTE_POS_Y + 20 + TRACK_UV_PALETTE + 40);
                this.mHorSliderRects[2] = new Rect(0, PALETTE_POS_Y + 20 + 100, PALETTE_DIM, PALETTE_POS_Y + 20 + 100 + 40);
                TEXT_HSV_POS[0] = 3;
                TEXT_HSV_POS[1] = 0;
                TEXT_RGB_POS[0] = TEXT_HSV_POS[0] + TRACK_UV_PALETTE;
                TEXT_RGB_POS[1] = TEXT_HSV_POS[1];
                TEXT_YUV_POS[0] = TEXT_HSV_POS[0] + 100;
                TEXT_YUV_POS[1] = TEXT_HSV_POS[1];
                TEXT_HEX_POS[0] = TEXT_HSV_POS[0] + 150;
                TEXT_HEX_POS[1] = TEXT_HSV_POS[1];
                VIEW_DIM_X = PALETTE_DIM + 40 + 10 + 40;
                VIEW_DIM_Y = Math.max(PALETTE_DIM + 60 + 48, (NUM_ENABLED_METHODS * 40) + ((NUM_ENABLED_METHODS - 1) * 10));
            } else {
                SWATCH_WIDTH = 110;
                PALETTE_POS_X = SWATCH_WIDTH;
                PALETTE_POS_Y = 0;
                METHOD_SELECTOR_POS_X = PALETTE_POS_X + PALETTE_DIM + 40 + 10;
                this.mHSVenabled = ENABLED_METHODS[0] || ENABLED_METHODS[1] || ENABLED_METHODS[2] || ENABLED_METHODS[5];
                this.mRGBenabled = ENABLED_METHODS[4];
                this.mYUVenabled = ENABLED_METHODS[3] || ENABLED_METHODS[6];
                this.mHexenabled = ENABLED_METHODS[4];
                int prevEnabledMethod2 = TRACKED_NONE;
                for (int i3 = 0; i3 < NUM_ENABLED_METHODS; i3++) {
                    int xOffset = (i3 / 4) * TRACK_UV_PALETTE;
                    this.mMethodSelectorRects[i3] = new Rect(METHOD_SELECTOR_POS_X + xOffset, (i3 % 4) * TRACK_UV_PALETTE, METHOD_SELECTOR_POS_X + xOffset + 40, ((i3 % 4) * TRACK_UV_PALETTE) + 40);
                    int j2 = prevEnabledMethod2 + 1;
                    while (true) {
                        if (j2 >= ENABLED_METHODS.length) {
                            break;
                        } else if (ENABLED_METHODS[j2]) {
                            this.mMethodSelectRectMap[i3] = j2;
                            prevEnabledMethod2 = j2;
                            break;
                        } else {
                            j2++;
                        }
                    }
                }
                int numMethodSelectorColumns = (int) Math.ceil((double) (((float) NUM_ENABLED_METHODS) / 4.0f));
                this.mOldSwatchRect.set(0, 84, SWATCH_WIDTH, 144);
                this.mNewSwatchRect.set(0, 144, SWATCH_WIDTH, 204);
                this.mPaletteRect.set(SWATCH_WIDTH, PALETTE_POS_Y, SWATCH_WIDTH + PALETTE_DIM, PALETTE_POS_Y + PALETTE_DIM);
                this.mVerSliderRect.set(SWATCH_WIDTH + PALETTE_DIM, PALETTE_POS_Y, SWATCH_WIDTH + PALETTE_DIM + 40, PALETTE_POS_Y + PALETTE_DIM);
                this.mHorSliderRects[0] = new Rect(SWATCH_WIDTH, 20, SWATCH_WIDTH + PALETTE_DIM, 60);
                this.mHorSliderRects[1] = new Rect(SWATCH_WIDTH, TRACK_H_SLIDER, SWATCH_WIDTH + PALETTE_DIM, 110);
                this.mHorSliderRects[2] = new Rect(SWATCH_WIDTH, 120, SWATCH_WIDTH + PALETTE_DIM, 160);
                TEXT_HSV_POS[0] = 3;
                TEXT_HSV_POS[1] = 0;
                TEXT_RGB_POS[0] = TEXT_HSV_POS[0];
                TEXT_RGB_POS[1] = (int) (((double) TEXT_HSV_POS[1]) + 42.0d);
                TEXT_YUV_POS[0] = TEXT_HSV_POS[0] + TRACK_UV_PALETTE;
                TEXT_YUV_POS[1] = (int) (((double) TEXT_HSV_POS[1]) + 42.0d);
                TEXT_HEX_POS[0] = TEXT_HSV_POS[0] + TRACK_UV_PALETTE;
                TEXT_HEX_POS[1] = TEXT_HSV_POS[1];
                VIEW_DIM_X = PALETTE_POS_X + PALETTE_DIM + 40 + (numMethodSelectorColumns * TRACK_UV_PALETTE);
                VIEW_DIM_Y = Math.max(this.mNewSwatchRect.bottom, Math.max(PALETTE_DIM, 190));
            }
            this.mSpectrumColors = new int[]{-65536, -256, -16711936, -16711681, -16776961, -65281, -65536};
            this.mSpectrumColorsRev = new int[]{-65536, -65281, -16776961, -16711681, -16711936, -256, -65536};
            this.mSwatchOld = new Paint(1);
            this.mSwatchOld.setStyle(Paint.Style.FILL);
            this.mSwatchOld.setColor(Color.HSVToColor(this.mHSV));
            this.mSwatchNew = new Paint(1);
            this.mSwatchNew.setStyle(Paint.Style.FILL);
            this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
            this.mFadeInLeft = new LinearGradient(0.0f, 0.0f, (float) PALETTE_DIM, 0.0f, -16777216, 0, Shader.TileMode.CLAMP);
            this.mFadeInRight = new LinearGradient(0.0f, 0.0f, (float) PALETTE_DIM, 0.0f, 0, -16777216, Shader.TileMode.CLAMP);
            this.mFadeInTop = new LinearGradient(0.0f, 0.0f, 0.0f, (float) PALETTE_DIM, -16777216, 0, Shader.TileMode.CLAMP);
            this.mFadeInBottom = new LinearGradient(0.0f, 0.0f, 0.0f, (float) PALETTE_DIM, 0, -16777216, Shader.TileMode.CLAMP);
            Shader fadeInTopSmall = new LinearGradient(0.0f, 0.0f, 0.0f, 40.0f, -16777216, 0, Shader.TileMode.CLAMP);
            Shader fadeInBottomSmall = new LinearGradient(0.0f, 0.0f, 0.0f, 40.0f, 0, -16777216, Shader.TileMode.CLAMP);
            new SweepGradient(0.0f, 0.0f, this.mSpectrumColorsRev, (float[]) null);
            SweepGradient sweepGradient = new SweepGradient(0.0f, 0.0f, this.mSpectrumColorsRev, (float[]) null);
            ComposeShader composeShader = new ComposeShader(sweepGradient, new RadialGradient(0.0f, 0.0f, (float) PALETTE_CENTER_X, (int) TRACKED_NONE, -16777216, Shader.TileMode.CLAMP), PorterDuff.Mode.SCREEN);
            this.mOvalHueSat = new Paint(1);
            this.mOvalHueSat.setShader(composeShader);
            this.mOvalHueSat.setStyle(Paint.Style.FILL);
            this.mOvalHueSat.setDither(true);
            ComposeShader composeShader2 = new ComposeShader(sweepGradient, new RadialGradient(0.0f, 0.0f, (float) PALETTE_CENTER_X, -16777216, (int) TRACKED_NONE, Shader.TileMode.CLAMP), PorterDuff.Mode.MULTIPLY);
            this.mOvalHueVal = new Paint(1);
            this.mOvalHueVal.setShader(composeShader2);
            this.mOvalHueVal.setStyle(Paint.Style.FILL);
            this.mOvalHueVal.setDither(true);
            this.mSatValMask = new ComposeShader(new LinearGradient(0.0f, 0.0f, 0.0f, (float) PALETTE_DIM, -16777216, (int) TRACKED_NONE, Shader.TileMode.CLAMP), this.mFadeInRight, PorterDuff.Mode.DST_IN);
            this.mSatValPalette = new Paint(1);
            this.mSatValPalette.setStyle(Paint.Style.FILL);
            this.mSatValPalette.setDither(true);
            this.mUVPalette = new Paint(1);
            this.mUVPalette.setStyle(Paint.Style.FILL);
            this.mUVPalette.setDither(true);
            this.mVerSliderBM = Bitmap.createBitmap(40, PALETTE_DIM, Bitmap.Config.RGB_565);
            this.mVerSliderCv = new Canvas(this.mVerSliderBM);
            for (int i4 = 0; i4 < 3; i4++) {
                this.mHorSlidersBM[i4] = Bitmap.createBitmap(PALETTE_DIM, 40, Bitmap.Config.RGB_565);
                this.mHorSlidersCv[i4] = new Canvas(this.mHorSlidersBM[i4]);
            }
            this.mSatFader = new Paint(1);
            this.mSatFader.setStyle(Paint.Style.FILL);
            this.mSatFader.setDither(true);
            this.mSatFader.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
            this.mValDimmer = new Paint(1);
            this.mValDimmer.setStyle(Paint.Style.FILL);
            this.mValDimmer.setDither(true);
            this.mValDimmer.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY));
            Shader shaderA = new SweepGradient(0.0f, 0.0f, this.mSpectrumColorsRev, (float[]) null);
            ComposeShader composeShader3 = new ComposeShader(shaderA, new RadialGradient(0.0f, 0.0f, 20.0f, (int) TRACKED_NONE, -16777216, Shader.TileMode.CLAMP), PorterDuff.Mode.SCREEN);
            this.mOvalHueSatSmall = new Paint(1);
            this.mOvalHueSatSmall.setShader(composeShader3);
            this.mOvalHueSatSmall.setStyle(Paint.Style.FILL);
            ComposeShader composeShader4 = new ComposeShader(shaderA, new RadialGradient(0.0f, 0.0f, 20.0f, -16777216, (int) TRACKED_NONE, Shader.TileMode.CLAMP), PorterDuff.Mode.MULTIPLY);
            this.mOvalHueValSmall = new Paint(1);
            this.mOvalHueValSmall.setShader(composeShader4);
            this.mOvalHueValSmall.setStyle(Paint.Style.FILL);
            ComposeShader composeShader5 = new ComposeShader(new LinearGradient(0.0f, 0.0f, 40.0f, 0.0f, -16777216, -65536, Shader.TileMode.CLAMP), new ComposeShader(new LinearGradient(0.0f, 0.0f, 0.0f, 40.0f, -16777216, (int) TRACKED_NONE, Shader.TileMode.CLAMP), new LinearGradient(0.0f, 0.0f, 40.0f, 0.0f, 0, -16777216, Shader.TileMode.CLAMP), PorterDuff.Mode.DST_IN), PorterDuff.Mode.SCREEN);
            this.mSVSmall = new Paint(1);
            this.mSVSmall.setShader(composeShader5);
            this.mSVSmall.setStyle(Paint.Style.FILL);
            ColorMatrix cm = new ColorMatrix();
            cm.setYUV2RGB();
            float[] a = cm.getArray();
            int[] rgb = new int[3];
            float[] yuv = {0.5f, -0.5f, 0.5f};
            matrixProductToByte(a, yuv, rgb);
            int col1 = Color.rgb(rgb[0], rgb[1], rgb[2]);
            yuv[1] = 0.5f;
            matrixProductToByte(a, yuv, rgb);
            ComposeShader composeShader6 = new ComposeShader(new LinearGradient(0.0f, 0.0f, 40.0f, 0.0f, col1, Color.rgb(rgb[0], rgb[1], rgb[2]), Shader.TileMode.CLAMP), fadeInTopSmall, PorterDuff.Mode.DST_IN);
            yuv[1] = -0.5f;
            yuv[2] = -0.5f;
            matrixProductToByte(a, yuv, rgb);
            int col12 = Color.rgb(rgb[0], rgb[1], rgb[2]);
            yuv[1] = 0.5f;
            matrixProductToByte(a, yuv, rgb);
            ComposeShader composeShader7 = new ComposeShader(composeShader6, new ComposeShader(new LinearGradient(0.0f, 0.0f, 40.0f, 0.0f, col12, Color.rgb(rgb[0], rgb[1], rgb[2]), Shader.TileMode.CLAMP), fadeInBottomSmall, PorterDuff.Mode.DST_IN), PorterDuff.Mode.SCREEN);
            this.mUVSmall = new Paint(1);
            this.mUVSmall.setShader(composeShader7);
            this.mUVSmall.setStyle(Paint.Style.FILL);
            LinearGradient linearGradient = new LinearGradient(0.0f, 0.0f, 40.0f, 0.0f, -16777216, -65536, Shader.TileMode.CLAMP);
            this.mRGBSmall[0] = new Paint(1);
            this.mRGBSmall[0].setShader(linearGradient);
            this.mRGBSmall[0].setStyle(Paint.Style.FILL);
            LinearGradient linearGradient2 = new LinearGradient(0.0f, 0.0f, 40.0f, 0.0f, -16777216, -16711936, Shader.TileMode.CLAMP);
            this.mRGBSmall[1] = new Paint(1);
            this.mRGBSmall[1].setShader(linearGradient2);
            this.mRGBSmall[1].setStyle(Paint.Style.FILL);
            LinearGradient linearGradient3 = new LinearGradient(0.0f, 0.0f, 40.0f, 0.0f, -16777216, -16776961, Shader.TileMode.CLAMP);
            this.mRGBSmall[2] = new Paint(1);
            this.mRGBSmall[2].setShader(linearGradient3);
            this.mRGBSmall[2].setStyle(Paint.Style.FILL);
            LinearGradient linearGradient4 = new LinearGradient(0.0f, 0.0f, 40.0f, 0.0f, this.mSpectrumColors, (float[]) null, Shader.TileMode.CLAMP);
            this.mHSSmall[0] = new Paint(1);
            this.mHSSmall[0].setShader(linearGradient4);
            this.mHSSmall[0].setStyle(Paint.Style.FILL);
            LinearGradient linearGradient5 = new LinearGradient(0.0f, 0.0f, 40.0f, 0.0f, (int) TRACKED_NONE, -65536, Shader.TileMode.CLAMP);
            this.mHSSmall[1] = new Paint(1);
            this.mHSSmall[1].setShader(linearGradient5);
            this.mHSSmall[1].setStyle(Paint.Style.FILL);
            yuv[0] = 0.0f;
            yuv[1] = 0.0f;
            yuv[2] = 0.0f;
            matrixProductToByte(a, yuv, rgb);
            int col13 = Color.rgb(rgb[0], rgb[1], rgb[2]);
            yuv[0] = 1.0f;
            matrixProductToByte(a, yuv, rgb);
            LinearGradient linearGradient6 = new LinearGradient(0.0f, 0.0f, 40.0f, 0.0f, col13, Color.rgb(rgb[0], rgb[1], rgb[2]), Shader.TileMode.CLAMP);
            this.mYUVSmall[0] = new Paint(1);
            this.mYUVSmall[0].setShader(linearGradient6);
            this.mYUVSmall[0].setStyle(Paint.Style.FILL);
            yuv[0] = 0.5f;
            yuv[1] = -0.5f;
            yuv[2] = 0.0f;
            matrixProductToByte(a, yuv, rgb);
            int col14 = Color.rgb(rgb[0], rgb[1], rgb[2]);
            yuv[1] = 0.5f;
            matrixProductToByte(a, yuv, rgb);
            LinearGradient linearGradient7 = new LinearGradient(0.0f, 0.0f, 40.0f, 0.0f, col14, Color.rgb(rgb[0], rgb[1], rgb[2]), Shader.TileMode.CLAMP);
            this.mYUVSmall[1] = new Paint(1);
            this.mYUVSmall[1].setShader(linearGradient7);
            this.mYUVSmall[1].setStyle(Paint.Style.FILL);
            yuv[0] = 0.5f;
            yuv[1] = 0.0f;
            yuv[2] = -0.5f;
            matrixProductToByte(a, yuv, rgb);
            int col15 = Color.rgb(rgb[0], rgb[1], rgb[2]);
            yuv[2] = 0.5f;
            matrixProductToByte(a, yuv, rgb);
            LinearGradient linearGradient8 = new LinearGradient(0.0f, 0.0f, 40.0f, 0.0f, col15, Color.rgb(rgb[0], rgb[1], rgb[2]), Shader.TileMode.CLAMP);
            this.mYUVSmall[2] = new Paint(1);
            this.mYUVSmall[2].setShader(linearGradient8);
            this.mYUVSmall[2].setStyle(Paint.Style.FILL);
            this.mPosMarker = new Paint(1);
            this.mPosMarker.setStyle(Paint.Style.STROKE);
            this.mPosMarker.setStrokeWidth(2.0f);
            this.mText = new Paint(1);
            this.mText.setTextSize(12.0f);
            this.mText.setColor((int) TRACKED_NONE);
            initUI();
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            drawSwatches(canvas);
            writeColorParams(canvas);
            if (this.mMethod == 0) {
                drawHSV1Palette(canvas);
            } else if (this.mMethod == 1) {
                drawHSV2Palette(canvas);
            } else if (this.mMethod == 2) {
                drawHSV3Palette(canvas);
            } else if (this.mMethod == 3) {
                drawYUVPalette(canvas);
            } else if (this.mMethod == 4) {
                drawRGBSliders(canvas);
            } else if (this.mMethod == 5) {
                drawHSVSliders(canvas);
            } else if (this.mMethod == 6) {
                drawYUVSliders(canvas);
            }
            drawMethodSelectors(canvas);
        }

        private void drawSwatches(Canvas canvas) {
            float[] hsv = new float[3];
            this.mText.setTextSize(16.0f);
            canvas.drawRect(this.mOldSwatchRect, this.mSwatchOld);
            Color.colorToHSV(this.mOriginalColor, hsv);
            if (((double) hsv[2]) > 0.5d) {
                this.mText.setColor(-16777216);
            }
            canvas.drawText("Revert", ((float) (this.mOldSwatchRect.left + (SWATCH_WIDTH / 2))) - (this.mText.measureText("Revert") / 2.0f), (float) (this.mOldSwatchRect.top + 16), this.mText);
            this.mText.setColor((int) TRACKED_NONE);
            canvas.drawRect(this.mNewSwatchRect, this.mSwatchNew);
            if (((double) this.mHSV[2]) > 0.5d) {
                this.mText.setColor(-16777216);
            }
            canvas.drawText("Accept", ((float) (this.mNewSwatchRect.left + (SWATCH_WIDTH / 2))) - (this.mText.measureText("Accept") / 2.0f), (float) (this.mNewSwatchRect.top + 16), this.mText);
            this.mText.setColor((int) TRACKED_NONE);
            this.mText.setTextSize(12.0f);
        }

        private void writeColorParams(Canvas canvas) {
            if (this.mHSVenabled) {
                canvas.drawText("H: " + Integer.toString((int) ((this.mHSV[0] / 360.0f) * 255.0f)), (float) TEXT_HSV_POS[0], (float) (TEXT_HSV_POS[1] + TEXT_SIZE), this.mText);
                canvas.drawText("S: " + Integer.toString((int) (this.mHSV[1] * 255.0f)), (float) TEXT_HSV_POS[0], (float) (TEXT_HSV_POS[1] + 24), this.mText);
                canvas.drawText("V: " + Integer.toString((int) (this.mHSV[2] * 255.0f)), (float) TEXT_HSV_POS[0], (float) (TEXT_HSV_POS[1] + 36), this.mText);
            }
            if (this.mRGBenabled) {
                canvas.drawText("R: " + this.mRGB[0], (float) TEXT_RGB_POS[0], (float) (TEXT_RGB_POS[1] + TEXT_SIZE), this.mText);
                canvas.drawText("G: " + this.mRGB[1], (float) TEXT_RGB_POS[0], (float) (TEXT_RGB_POS[1] + 24), this.mText);
                canvas.drawText("B: " + this.mRGB[2], (float) TEXT_RGB_POS[0], (float) (TEXT_RGB_POS[1] + 36), this.mText);
            }
            if (this.mYUVenabled) {
                canvas.drawText("Y: " + Integer.toString((int) (this.mYUV[0] * 255.0f)), (float) TEXT_YUV_POS[0], (float) (TEXT_YUV_POS[1] + TEXT_SIZE), this.mText);
                canvas.drawText("U: " + Integer.toString((int) ((this.mYUV[1] + 0.5f) * 255.0f)), (float) TEXT_YUV_POS[0], (float) (TEXT_YUV_POS[1] + 24), this.mText);
                canvas.drawText("V: " + Integer.toString((int) ((this.mYUV[2] + 0.5f) * 255.0f)), (float) TEXT_YUV_POS[0], (float) (TEXT_YUV_POS[1] + 36), this.mText);
            }
            if (this.mHexenabled) {
                canvas.drawText("#" + this.mHexStr, (float) TEXT_HEX_POS[0], (float) (TEXT_HEX_POS[1] + TEXT_SIZE), this.mText);
            }
        }

        private void mark2DPalette(Canvas canvas, int markerPosX, int markerPosY) {
            this.mPosMarker.setColor(-16777216);
            canvas.drawOval(new RectF((float) (markerPosX - 5), (float) (markerPosY - 5), (float) (markerPosX + 5), (float) (markerPosY + 5)), this.mPosMarker);
            this.mPosMarker.setColor((int) TRACKED_NONE);
            canvas.drawOval(new RectF((float) (markerPosX - 3), (float) (markerPosY - 3), (float) (markerPosX + 3), (float) (markerPosY + 3)), this.mPosMarker);
        }

        private void markVerSlider(Canvas canvas, int markerPos) {
            this.mPosMarker.setColor(-16777216);
            canvas.drawRect(new Rect(0, markerPos - 2, 40, markerPos + 3), this.mPosMarker);
            this.mPosMarker.setColor((int) TRACKED_NONE);
            canvas.drawRect(new Rect(0, markerPos, 40, markerPos + 1), this.mPosMarker);
        }

        private void markHorSlider(Canvas canvas, int markerPos) {
            this.mPosMarker.setColor(-16777216);
            canvas.drawRect(new Rect(markerPos - 2, 0, markerPos + 3, 40), this.mPosMarker);
            this.mPosMarker.setColor((int) TRACKED_NONE);
            canvas.drawRect(new Rect(markerPos, 0, markerPos + 1, 40), this.mPosMarker);
        }

        private void hilightFocusedVerSlider(Canvas canvas) {
            this.mPosMarker.setColor((int) TRACKED_NONE);
            canvas.drawRect(new Rect(0, 0, 40, PALETTE_DIM), this.mPosMarker);
            this.mPosMarker.setColor(-16777216);
            canvas.drawRect(new Rect(2, 2, 38, PALETTE_DIM - 2), this.mPosMarker);
        }

        private void hilightFocusedHorSlider(Canvas canvas) {
            this.mPosMarker.setColor((int) TRACKED_NONE);
            canvas.drawRect(new Rect(0, 0, PALETTE_DIM, 40), this.mPosMarker);
            this.mPosMarker.setColor(-16777216);
            canvas.drawRect(new Rect(2, 2, PALETTE_DIM - 2, 38), this.mPosMarker);
        }

        private void hilightFocusedOvalPalette(Canvas canvas) {
            this.mPosMarker.setColor((int) TRACKED_NONE);
            canvas.drawOval(new RectF((float) (-PALETTE_RADIUS), (float) (-PALETTE_RADIUS), (float) PALETTE_RADIUS, (float) PALETTE_RADIUS), this.mPosMarker);
            this.mPosMarker.setColor(-16777216);
            canvas.drawOval(new RectF((float) ((-PALETTE_RADIUS) + 2), (float) ((-PALETTE_RADIUS) + 2), (float) (PALETTE_RADIUS - 2), (float) (PALETTE_RADIUS - 2)), this.mPosMarker);
        }

        private void hilightFocusedSquarePalette(Canvas canvas) {
            this.mPosMarker.setColor((int) TRACKED_NONE);
            canvas.drawRect(new Rect(0, 0, PALETTE_DIM, PALETTE_DIM), this.mPosMarker);
            this.mPosMarker.setColor(-16777216);
            canvas.drawRect(new Rect(2, 2, PALETTE_DIM - 2, PALETTE_DIM - 2), this.mPosMarker);
        }

        private void hilightMethodSelectorOval(Canvas canvas) {
            this.mPosMarker.setColor((int) TRACKED_NONE);
            canvas.drawOval(new RectF(-20.0f, -20.0f, 20.0f, 20.0f), this.mPosMarker);
            this.mPosMarker.setColor(-16777216);
            canvas.drawOval(new RectF(-18.0f, -18.0f, 18.0f, 18.0f), this.mPosMarker);
        }

        private void hilightMethodSelectorRect(Canvas canvas) {
            this.mPosMarker.setColor((int) TRACKED_NONE);
            canvas.drawRect(new Rect(0, 0, 40, 40), this.mPosMarker);
            this.mPosMarker.setColor(-16777216);
            canvas.drawRect(new Rect(2, 2, 38, 38), this.mPosMarker);
        }

        private void drawHSV1Palette(Canvas canvas) {
            canvas.save();
            canvas.translate((float) PALETTE_POS_X, (float) PALETTE_POS_Y);
            canvas.translate((float) PALETTE_CENTER_X, (float) PALETTE_CENTER_Y);
            canvas.drawOval(new RectF((float) (-PALETTE_RADIUS), (float) (-PALETTE_RADIUS), (float) PALETTE_RADIUS, (float) PALETTE_RADIUS), this.mOvalHueSat);
            canvas.drawOval(new RectF((float) (-PALETTE_RADIUS), (float) (-PALETTE_RADIUS), (float) PALETTE_RADIUS, (float) PALETTE_RADIUS), this.mValDimmer);
            if (this.mFocusedControl == 0) {
                hilightFocusedOvalPalette(canvas);
            }
            mark2DPalette(canvas, this.mCoord[0], this.mCoord[1]);
            canvas.translate((float) (-PALETTE_CENTER_X), (float) (-PALETTE_CENTER_Y));
            canvas.translate((float) PALETTE_DIM, 0.0f);
            canvas.drawBitmap(this.mVerSliderBM, 0.0f, 0.0f, (Paint) null);
            if (this.mFocusedControl == 1) {
                hilightFocusedVerSlider(canvas);
            }
            markVerSlider(canvas, this.mCoord[2]);
            canvas.restore();
        }

        private void drawHSV2Palette(Canvas canvas) {
            canvas.save();
            canvas.translate((float) PALETTE_POS_X, (float) PALETTE_POS_Y);
            canvas.translate((float) PALETTE_CENTER_X, (float) PALETTE_CENTER_Y);
            canvas.drawOval(new RectF((float) (-PALETTE_RADIUS), (float) (-PALETTE_RADIUS), (float) PALETTE_RADIUS, (float) PALETTE_RADIUS), this.mOvalHueVal);
            canvas.drawOval(new RectF((float) (-PALETTE_RADIUS), (float) (-PALETTE_RADIUS), (float) PALETTE_RADIUS, (float) PALETTE_RADIUS), this.mSatFader);
            if (this.mFocusedControl == 0) {
                hilightFocusedOvalPalette(canvas);
            }
            mark2DPalette(canvas, this.mCoord[0], this.mCoord[2]);
            canvas.translate((float) (-PALETTE_CENTER_X), (float) (-PALETTE_CENTER_Y));
            canvas.translate((float) PALETTE_DIM, 0.0f);
            canvas.drawBitmap(this.mVerSliderBM, 0.0f, 0.0f, (Paint) null);
            if (this.mFocusedControl == 1) {
                hilightFocusedVerSlider(canvas);
            }
            if (this.mFocusedControl == 1) {
                hilightFocusedVerSlider(canvas);
            }
            markVerSlider(canvas, this.mCoord[1]);
            canvas.restore();
        }

        private void drawHSV3Palette(Canvas canvas) {
            canvas.save();
            canvas.translate((float) PALETTE_POS_X, (float) PALETTE_POS_Y);
            canvas.drawRect(new Rect(0, 0, PALETTE_DIM, PALETTE_DIM), this.mSatValPalette);
            if (this.mFocusedControl == 0) {
                hilightFocusedSquarePalette(canvas);
            }
            mark2DPalette(canvas, this.mCoord[2], this.mCoord[1]);
            canvas.translate((float) PALETTE_DIM, 0.0f);
            canvas.drawBitmap(this.mVerSliderBM, 0.0f, 0.0f, (Paint) null);
            if (this.mFocusedControl == 1) {
                hilightFocusedVerSlider(canvas);
            }
            canvas.drawRect(new Rect(0, 0, 40, PALETTE_DIM), this.mSatFader);
            canvas.drawRect(new Rect(0, 0, 40, PALETTE_DIM), this.mValDimmer);
            if (this.mFocusedControl == 1) {
                hilightFocusedVerSlider(canvas);
            }
            markVerSlider(canvas, this.mCoord[0]);
            canvas.restore();
        }

        private void drawYUVPalette(Canvas canvas) {
            canvas.save();
            canvas.translate((float) PALETTE_POS_X, (float) PALETTE_POS_Y);
            Paint black = new Paint(1);
            black.setStyle(Paint.Style.FILL);
            black.setColor(-16777216);
            canvas.drawRect(new Rect(0, 0, PALETTE_DIM, PALETTE_DIM), black);
            canvas.drawRect(new Rect(0, 0, PALETTE_DIM, PALETTE_DIM), this.mUVPalette);
            if (this.mFocusedControl == 0) {
                hilightFocusedSquarePalette(canvas);
            }
            mark2DPalette(canvas, this.mCoord[1], this.mCoord[2]);
            canvas.translate((float) PALETTE_DIM, 0.0f);
            canvas.drawBitmap(this.mVerSliderBM, 0.0f, 0.0f, (Paint) null);
            if (this.mFocusedControl == 1) {
                hilightFocusedVerSlider(canvas);
            }
            if (this.mFocusedControl == 1) {
                hilightFocusedVerSlider(canvas);
            }
            markVerSlider(canvas, this.mCoord[0]);
            canvas.restore();
        }

        private void drawRGBSliders(Canvas canvas) {
            canvas.save();
            canvas.translate((float) PALETTE_POS_X, (float) PALETTE_POS_Y);
            for (int i = 0; i < 3; i++) {
                if (i == 0) {
                    canvas.translate(0.0f, 20.0f);
                } else {
                    canvas.translate(0.0f, 50.0f);
                }
                canvas.drawBitmap(this.mHorSlidersBM[i], 0.0f, 0.0f, (Paint) null);
                if (this.mFocusedControl == i) {
                    hilightFocusedHorSlider(canvas);
                }
                markHorSlider(canvas, this.mCoord[i]);
                if (i == 0) {
                    canvas.drawText("R", (float) (PALETTE_DIM + 5), 26.0f, this.mText);
                } else if (i == 1) {
                    canvas.drawText("G", (float) (PALETTE_DIM + 5), 26.0f, this.mText);
                } else {
                    canvas.drawText("B", (float) (PALETTE_DIM + 5), 26.0f, this.mText);
                }
            }
            canvas.restore();
        }

        private void drawHSVSliders(Canvas canvas) {
            canvas.save();
            canvas.translate((float) PALETTE_POS_X, (float) PALETTE_POS_Y);
            for (int i = 0; i < 3; i++) {
                if (i == 0) {
                    canvas.translate(0.0f, 20.0f);
                } else {
                    canvas.translate(0.0f, 50.0f);
                }
                canvas.drawBitmap(this.mHorSlidersBM[i], 0.0f, 0.0f, (Paint) null);
                if (i == 0) {
                    canvas.drawRect(new Rect(0, 0, PALETTE_DIM, 40), this.mSatFader);
                    canvas.drawRect(new Rect(0, 0, PALETTE_DIM, 40), this.mValDimmer);
                }
                if (this.mFocusedControl == i) {
                    hilightFocusedHorSlider(canvas);
                }
                markHorSlider(canvas, this.mCoord[i]);
                if (i == 0) {
                    canvas.drawText("H", (float) (PALETTE_DIM + 5), 26.0f, this.mText);
                } else if (i == 1) {
                    canvas.drawText("S", (float) (PALETTE_DIM + 5), 26.0f, this.mText);
                } else {
                    canvas.drawText("V", (float) (PALETTE_DIM + 5), 26.0f, this.mText);
                }
            }
            canvas.restore();
        }

        private void drawYUVSliders(Canvas canvas) {
            canvas.save();
            canvas.translate((float) PALETTE_POS_X, (float) PALETTE_POS_Y);
            for (int i = 0; i < 3; i++) {
                if (i == 0) {
                    canvas.translate(0.0f, 20.0f);
                } else {
                    canvas.translate(0.0f, 50.0f);
                }
                canvas.drawBitmap(this.mHorSlidersBM[i], 0.0f, 0.0f, (Paint) null);
                if (this.mFocusedControl == i) {
                    hilightFocusedHorSlider(canvas);
                }
                markHorSlider(canvas, this.mCoord[i]);
                if (i == 0) {
                    canvas.drawText("Y", (float) (PALETTE_DIM + 5), 26.0f, this.mText);
                } else if (i == 1) {
                    canvas.drawText("U", (float) (PALETTE_DIM + 5), 26.0f, this.mText);
                } else {
                    canvas.drawText("V", (float) (PALETTE_DIM + 5), 26.0f, this.mText);
                }
            }
            canvas.restore();
        }

        private void drawMethodSelectors(Canvas canvas) {
            for (int i = 0; i < NUM_ENABLED_METHODS; i++) {
                canvas.save();
                switch (this.mMethodSelectRectMap[i]) {
                    case ZmanimFormatter.SEXAGESIMAL_XSD_FORMAT /*0*/:
                        canvas.translate((float) (this.mMethodSelectorRects[i].left + 20), (float) (this.mMethodSelectorRects[i].top + 20));
                        canvas.drawOval(new RectF(-20.0f, -20.0f, 20.0f, 20.0f), this.mOvalHueSatSmall);
                        if (this.mMethod != i) {
                            break;
                        } else {
                            hilightMethodSelectorOval(canvas);
                            break;
                        }
                    case 1:
                        canvas.translate((float) (this.mMethodSelectorRects[i].left + 20), (float) (this.mMethodSelectorRects[i].top + 20));
                        canvas.drawOval(new RectF(-20.0f, -20.0f, 20.0f, 20.0f), this.mOvalHueValSmall);
                        if (this.mMethod != i) {
                            break;
                        } else {
                            hilightMethodSelectorOval(canvas);
                            break;
                        }
                    case 2:
                        canvas.translate((float) this.mMethodSelectorRects[i].left, (float) this.mMethodSelectorRects[i].top);
                        canvas.drawRect(new RectF(0.0f, 0.0f, 40.0f, 40.0f), this.mSVSmall);
                        if (this.mMethod != i) {
                            break;
                        } else {
                            hilightMethodSelectorRect(canvas);
                            break;
                        }
                    case 3:
                        canvas.translate((float) this.mMethodSelectorRects[i].left, (float) this.mMethodSelectorRects[i].top);
                        canvas.drawRect(new RectF(0.0f, 0.0f, 40.0f, 40.0f), this.mUVSmall);
                        if (this.mMethod != i) {
                            break;
                        } else {
                            hilightMethodSelectorRect(canvas);
                            break;
                        }
                    case 4:
                        canvas.translate((float) this.mMethodSelectorRects[i].left, (float) (this.mMethodSelectorRects[i].top + 2));
                        canvas.drawRect(new RectF(0.0f, 0.0f, 40.0f, 10.0f), this.mRGBSmall[0]);
                        canvas.translate(0.0f, 13.0f);
                        canvas.drawRect(new RectF(0.0f, 0.0f, 40.0f, 10.0f), this.mRGBSmall[1]);
                        canvas.translate(0.0f, 13.0f);
                        canvas.drawRect(new RectF(0.0f, 0.0f, 40.0f, 10.0f), this.mRGBSmall[2]);
                        canvas.translate(0.0f, -28.0f);
                        if (this.mMethod != i) {
                            break;
                        } else {
                            hilightMethodSelectorRect(canvas);
                            break;
                        }
                    case 5:
                        canvas.translate((float) this.mMethodSelectorRects[i].left, (float) (this.mMethodSelectorRects[i].top + 2));
                        canvas.drawRect(new RectF(0.0f, 0.0f, 40.0f, 10.0f), this.mHSSmall[0]);
                        canvas.translate(0.0f, 13.0f);
                        canvas.drawRect(new RectF(0.0f, 0.0f, 40.0f, 10.0f), this.mHSSmall[1]);
                        canvas.translate(0.0f, 13.0f);
                        canvas.drawRect(new RectF(0.0f, 0.0f, 40.0f, 10.0f), this.mRGBSmall[0]);
                        canvas.translate(0.0f, -28.0f);
                        if (this.mMethod != i) {
                            break;
                        } else {
                            hilightMethodSelectorRect(canvas);
                            break;
                        }
                    case 6:
                        canvas.translate((float) this.mMethodSelectorRects[i].left, (float) (this.mMethodSelectorRects[i].top + 2));
                        canvas.drawRect(new RectF(0.0f, 0.0f, 40.0f, 10.0f), this.mYUVSmall[0]);
                        canvas.translate(0.0f, 13.0f);
                        canvas.drawRect(new RectF(0.0f, 0.0f, 40.0f, 10.0f), this.mYUVSmall[1]);
                        canvas.translate(0.0f, 13.0f);
                        canvas.drawRect(new RectF(0.0f, 0.0f, 40.0f, 10.0f), this.mYUVSmall[2]);
                        canvas.translate(0.0f, -28.0f);
                        if (this.mMethod != i) {
                            break;
                        } else {
                            hilightMethodSelectorRect(canvas);
                            break;
                        }
                }
                canvas.restore();
            }
        }

        private void initUI() {
            switch (this.mMethod) {
                case ZmanimFormatter.SEXAGESIMAL_XSD_FORMAT /*0*/:
                    initHSV1Palette();
                    break;
                case 1:
                    initHSV2Palette();
                    break;
                case 2:
                    initHSV3Palette();
                    break;
                case 3:
                    initYUVPalette();
                    break;
                case 4:
                    initRGBSliders();
                    break;
                case 5:
                    initHSVSliders();
                    break;
                case 6:
                    initYUVSliders();
                    break;
            }
            this.mFocusedControl = 0;
        }

        private void initHSV1Palette() {
            setOvalValDimmer();
            setVerValSlider();
            float angle = 6.2831855f - (this.mHSV[0] / 57.295776f);
            float radius = this.mHSV[1] * ((float) PALETTE_RADIUS);
            this.mCoord[0] = (int) (Math.cos((double) angle) * ((double) radius));
            this.mCoord[1] = (int) (Math.sin((double) angle) * ((double) radius));
            this.mCoord[2] = PALETTE_DIM - ((int) (this.mHSV[2] * ((float) PALETTE_DIM)));
        }

        private void initHSV2Palette() {
            setOvalSatFader();
            setVerSatSlider();
            float angle = 6.2831855f - (this.mHSV[0] / 57.295776f);
            float radius = this.mHSV[1] * ((float) PALETTE_RADIUS);
            this.mCoord[0] = (int) (Math.cos((double) angle) * ((double) radius));
            this.mCoord[2] = (int) (Math.sin((double) angle) * ((double) radius));
            this.mCoord[1] = PALETTE_DIM - ((int) (this.mHSV[1] * ((float) PALETTE_DIM)));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
         arg types: [int, float, int, int, int[], ?[OBJECT, ARRAY], android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
        private void initHSV3Palette() {
            new LinearGradient(0.0f, (float) PALETTE_DIM, 0.0f, 0.0f, this.mSpectrumColors, (float[]) null, Shader.TileMode.CLAMP);
            GradientDrawable gradDraw = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, this.mSpectrumColorsRev);
            gradDraw.setDither(true);
            gradDraw.setLevel(10000);
            gradDraw.setBounds(0, 0, 40, PALETTE_DIM);
            gradDraw.draw(this.mVerSliderCv);
            setHorSatFader();
            setHorValDimmer();
            setSatValPalette();
            setVerHueSlider();
            this.mCoord[1] = PALETTE_DIM - ((int) (this.mHSV[1] * ((float) PALETTE_DIM)));
            this.mCoord[2] = (int) (this.mHSV[2] * ((float) PALETTE_DIM));
            this.mCoord[0] = PALETTE_DIM - ((int) ((this.mHSV[0] / 360.0f) * ((float) PALETTE_DIM)));
        }

        private void initYUVPalette() {
            int color = Color.HSVToColor(this.mHSV);
            float r = ((float) Color.red(color)) / 255.0f;
            float g = ((float) Color.green(color)) / 255.0f;
            float b = ((float) Color.blue(color)) / 255.0f;
            ColorMatrix cm = new ColorMatrix();
            cm.setRGB2YUV();
            float[] a = cm.getArray();
            this.mYUV[0] = (a[0] * r) + (a[1] * g) + (a[2] * b);
            this.mYUV[0] = pinToUnit(this.mYUV[0]);
            this.mYUV[1] = (a[5] * r) + (a[6] * g) + (a[7] * b);
            this.mYUV[1] = pin(this.mYUV[1], -0.5f, 0.5f);
            this.mYUV[2] = (a[10] * r) + (a[TRACK_SWATCH_NEW] * g) + (a[TEXT_SIZE] * b);
            this.mYUV[2] = pin(this.mYUV[2], -0.5f, 0.5f);
            setUVPalette();
            setVerYSlider();
            this.mCoord[1] = (int) ((this.mYUV[1] + 0.5f) * ((float) PALETTE_DIM));
            this.mCoord[2] = PALETTE_DIM - ((int) ((this.mYUV[2] + 0.5f) * ((float) PALETTE_DIM)));
            this.mCoord[0] = PALETTE_DIM - ((int) (this.mYUV[0] * ((float) PALETTE_DIM)));
            if (!this.mShownYUVWarnedAlready) {
                Toast.makeText(getContext(), "Note that the UV 2D palette only shows an estimate but the swatch is correct.", 1).show();
            }
            this.mShownYUVWarnedAlready = true;
        }

        private void initRGBSliders() {
            int color = Color.HSVToColor(this.mHSV);
            this.mRGB[0] = Color.red(color);
            this.mRGB[1] = Color.green(color);
            this.mRGB[2] = Color.blue(color);
            setHorRSlider();
            setHorGSlider();
            setHorBSlider();
            int col = Color.HSVToColor(this.mHSV);
            this.mCoord[0] = (int) (((float) PALETTE_DIM) * (((float) Color.red(col)) / 255.0f));
            this.mCoord[1] = (int) (((float) PALETTE_DIM) * (((float) Color.green(col)) / 255.0f));
            this.mCoord[2] = (int) (((float) PALETTE_DIM) * (((float) Color.blue(col)) / 255.0f));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
         arg types: [int, int, float, int, int[], ?[OBJECT, ARRAY], android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
        private void initHSVSliders() {
            new LinearGradient(0.0f, 0.0f, (float) PALETTE_DIM, 0.0f, this.mSpectrumColors, (float[]) null, Shader.TileMode.CLAMP);
            GradientDrawable gradDraw = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, this.mSpectrumColors);
            gradDraw.setDither(true);
            gradDraw.setLevel(10000);
            gradDraw.setBounds(0, 0, PALETTE_DIM, 40);
            gradDraw.draw(this.mHorSlidersCv[0]);
            setHorSatFader();
            setHorValDimmer();
            setHorSatSlider();
            setHorValSlider();
            this.mCoord[0] = (int) ((this.mHSV[0] / 360.0f) * ((float) PALETTE_DIM));
            this.mCoord[1] = (int) (this.mHSV[1] * ((float) PALETTE_DIM));
            this.mCoord[2] = (int) (this.mHSV[2] * ((float) PALETTE_DIM));
        }

        private void initYUVSliders() {
            int color = Color.HSVToColor(this.mHSV);
            float r = ((float) Color.red(color)) / 255.0f;
            float g = ((float) Color.green(color)) / 255.0f;
            float b = ((float) Color.blue(color)) / 255.0f;
            ColorMatrix cm = new ColorMatrix();
            cm.setRGB2YUV();
            float[] a = cm.getArray();
            this.mYUV[0] = (a[0] * r) + (a[1] * g) + (a[2] * b);
            this.mYUV[0] = pinToUnit(this.mYUV[0]);
            this.mYUV[1] = (a[5] * r) + (a[6] * g) + (a[7] * b);
            this.mYUV[1] = pin(this.mYUV[1], -0.5f, 0.5f);
            this.mYUV[2] = (a[10] * r) + (a[TRACK_SWATCH_NEW] * g) + (a[TEXT_SIZE] * b);
            this.mYUV[2] = pin(this.mYUV[2], -0.5f, 0.5f);
            setHorYSlider();
            setHorUSlider();
            setHorVSlider();
            this.mCoord[0] = (int) (this.mYUV[0] * ((float) PALETTE_DIM));
            this.mCoord[1] = (int) ((this.mYUV[1] + 0.5f) * ((float) PALETTE_DIM));
            this.mCoord[2] = (int) ((this.mYUV[2] + 0.5f) * ((float) PALETTE_DIM));
        }

        private void setOvalValDimmer() {
            this.mValDimmer.setColor(Color.HSVToColor(new float[]{this.mHSV[0], 0.0f, this.mHSV[2]}));
        }

        private void setVerValSlider() {
            GradientDrawable gradDraw = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.HSVToColor(new float[]{this.mHSV[0], this.mHSV[1], 1.0f}), -16777216});
            gradDraw.setDither(true);
            gradDraw.setLevel(10000);
            gradDraw.setBounds(0, 0, 40, PALETTE_DIM);
            gradDraw.draw(this.mVerSliderCv);
        }

        private void setOvalSatFader() {
            this.mSatFader.setColor(Color.HSVToColor(new float[]{this.mHSV[0], 0.0f, (1.0f - this.mHSV[1]) * this.mHSV[2]}));
        }

        private void setVerSatSlider() {
            float[] hsv = {this.mHSV[0], 1.0f, this.mHSV[2]};
            int col1 = Color.HSVToColor(hsv);
            hsv[0] = this.mHSV[0];
            hsv[1] = 0.0f;
            hsv[2] = this.mHSV[2];
            GradientDrawable gradDraw = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{col1, Color.HSVToColor(hsv)});
            gradDraw.setDither(true);
            gradDraw.setLevel(10000);
            gradDraw.setBounds(0, 0, 40, PALETTE_DIM);
            gradDraw.draw(this.mVerSliderCv);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
         arg types: [int, int, float, int, int, int, android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
        private void setSatValPalette() {
            this.mSatValPalette.setShader(new ComposeShader(new LinearGradient(0.0f, 0.0f, (float) PALETTE_DIM, 0.0f, -16777216, Color.HSVToColor(new float[]{this.mHSV[0], 1.0f, 1.0f}), Shader.TileMode.CLAMP), this.mSatValMask, PorterDuff.Mode.SCREEN));
        }

        private void setVerHueSlider() {
        }

        private void setHorSatFader() {
            this.mSatFader.setColor(Color.HSVToColor(new float[]{this.mHSV[0], 0.0f, 1.0f - this.mHSV[1]}));
        }

        private void setHorValDimmer() {
            this.mValDimmer.setColor(Color.HSVToColor(new float[]{this.mHSV[0], 0.0f, this.mHSV[2]}));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
         arg types: [int, int, float, int, int, int, android.graphics.Shader$TileMode]
         candidates:
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
          ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
        private void setUVPalette() {
            ComposeShader composeShader;
            ColorMatrix cm = new ColorMatrix();
            cm.setYUV2RGB();
            float[] a = cm.getArray();
            int[] rgb = new int[3];
            float[] yuv = {this.mYUV[0], -0.5f, 0.5f};
            matrixProductToByte(a, yuv, rgb);
            int col1 = Color.rgb(rgb[0], rgb[1], rgb[2]);
            yuv[1] = 0.5f;
            matrixProductToByte(a, yuv, rgb);
            ComposeShader composeShader2 = new ComposeShader(new LinearGradient(0.0f, 0.0f, (float) PALETTE_DIM, 0.0f, col1, Color.rgb(rgb[0], rgb[1], rgb[2]), Shader.TileMode.CLAMP), this.mFadeInTop, PorterDuff.Mode.DST_IN);
            yuv[1] = -0.5f;
            yuv[2] = -0.5f;
            matrixProductToByte(a, yuv, rgb);
            int col12 = Color.rgb(rgb[0], rgb[1], rgb[2]);
            yuv[1] = 0.5f;
            matrixProductToByte(a, yuv, rgb);
            ComposeShader composeShader3 = new ComposeShader(composeShader2, new ComposeShader(new LinearGradient(0.0f, 0.0f, (float) PALETTE_DIM, 0.0f, col12, Color.rgb(rgb[0], rgb[1], rgb[2]), Shader.TileMode.CLAMP), this.mFadeInBottom, PorterDuff.Mode.DST_IN), PorterDuff.Mode.SCREEN);
            if (((double) this.mYUV[0]) >= 0.5d) {
                int gray = pinToByte((int) ((this.mYUV[0] - 0.5f) * 512.0f));
                composeShader = new ComposeShader(composeShader3, new RadialGradient((float) PALETTE_CENTER_X, (float) PALETTE_CENTER_Y, (float) PALETTE_RADIUS, Color.argb(pinToByte((int) ((this.mYUV[0] - 0.5f) * 480.0f)), gray, gray, gray), 0, Shader.TileMode.CLAMP), PorterDuff.Mode.SCREEN);
            } else {
                int gray2 = pinToByte((int) ((this.mYUV[0] + 0.5f) * 512.0f));
                composeShader = new ComposeShader(composeShader3, new RadialGradient((float) PALETTE_CENTER_X, (float) PALETTE_CENTER_Y, (float) PALETTE_RADIUS, Color.argb(pinToByte((int) ((1.0f - (this.mYUV[0] + 0.5f)) * 448.0f)), gray2, gray2, gray2), 0, Shader.TileMode.CLAMP), PorterDuff.Mode.DST_OUT);
            }
            this.mUVPalette.setShader(composeShader);
        }

        private void setVerYSlider() {
            ColorMatrix cm = new ColorMatrix();
            cm.setYUV2RGB();
            float[] a = cm.getArray();
            float[] yuv = new float[3];
            int[] rgb = new int[3];
            yuv[1] = this.mYUV[1];
            yuv[2] = this.mYUV[2];
            int[] colors = new int[TRACK_SWATCH_NEW];
            for (int i = 0; i <= 10; i++) {
                yuv[0] = ((float) i) / 10.0f;
                matrixProductToByte(a, yuv, rgb);
                colors[10 - i] = Color.rgb(rgb[0], rgb[1], rgb[2]);
            }
            GradientDrawable gradDraw = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, colors);
            gradDraw.setDither(true);
            gradDraw.setLevel(10000);
            gradDraw.setBounds(0, 0, 40, PALETTE_DIM);
            gradDraw.draw(this.mVerSliderCv);
        }

        private void setHorRSlider() {
            GradientDrawable gradDraw = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{Color.rgb(0, this.mRGB[1], this.mRGB[2]), Color.rgb(255, this.mRGB[1], this.mRGB[2])});
            gradDraw.setDither(true);
            gradDraw.setLevel(10000);
            gradDraw.setBounds(0, 0, PALETTE_DIM, 40);
            gradDraw.draw(this.mHorSlidersCv[0]);
        }

        private void setHorGSlider() {
            GradientDrawable gradDraw = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{Color.rgb(this.mRGB[0], 0, this.mRGB[2]), Color.rgb(this.mRGB[0], 255, this.mRGB[2])});
            gradDraw.setDither(true);
            gradDraw.setLevel(10000);
            gradDraw.setBounds(0, 0, PALETTE_DIM, 40);
            gradDraw.draw(this.mHorSlidersCv[1]);
        }

        private void setHorBSlider() {
            GradientDrawable gradDraw = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{Color.rgb(this.mRGB[0], this.mRGB[1], 0), Color.rgb(this.mRGB[0], this.mRGB[1], 255)});
            gradDraw.setDither(true);
            gradDraw.setLevel(10000);
            gradDraw.setBounds(0, 0, PALETTE_DIM, 40);
            gradDraw.draw(this.mHorSlidersCv[2]);
        }

        private void setHorSatSlider() {
            float[] hsv = {this.mHSV[0], 0.0f, this.mHSV[2]};
            int col1 = Color.HSVToColor(hsv);
            hsv[0] = this.mHSV[0];
            hsv[1] = 1.0f;
            hsv[2] = this.mHSV[2];
            GradientDrawable gradDraw = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{col1, Color.HSVToColor(hsv)});
            gradDraw.setDither(true);
            gradDraw.setLevel(10000);
            gradDraw.setBounds(0, 0, PALETTE_DIM, 40);
            gradDraw.draw(this.mHorSlidersCv[1]);
        }

        private void setHorValSlider() {
            GradientDrawable gradDraw = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{-16777216, Color.HSVToColor(new float[]{this.mHSV[0], this.mHSV[1], 1.0f})});
            gradDraw.setDither(true);
            gradDraw.setLevel(10000);
            gradDraw.setBounds(0, 0, PALETTE_DIM, 40);
            gradDraw.draw(this.mHorSlidersCv[2]);
        }

        private void setHorYSlider() {
            ColorMatrix cm = new ColorMatrix();
            cm.setYUV2RGB();
            float[] a = cm.getArray();
            float[] yuv = new float[3];
            int[] rgb = new int[3];
            yuv[1] = this.mYUV[1];
            yuv[2] = this.mYUV[2];
            int[] colors = new int[TRACK_SWATCH_NEW];
            for (int i = 0; i <= 10; i++) {
                yuv[0] = ((float) i) / 10.0f;
                matrixProductToByte(a, yuv, rgb);
                colors[i] = Color.rgb(rgb[0], rgb[1], rgb[2]);
            }
            GradientDrawable gradDraw = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
            gradDraw.setDither(true);
            gradDraw.setLevel(10000);
            gradDraw.setBounds(0, 0, PALETTE_DIM, 40);
            gradDraw.draw(this.mHorSlidersCv[0]);
        }

        private void setHorUSlider() {
            ColorMatrix cm = new ColorMatrix();
            cm.setYUV2RGB();
            float[] a = cm.getArray();
            float[] yuv = new float[3];
            int[] rgb = new int[3];
            yuv[0] = this.mYUV[0];
            yuv[2] = this.mYUV[2];
            int[] colors = new int[TRACK_SWATCH_NEW];
            for (int i = -5; i <= 5; i++) {
                yuv[1] = ((float) i) / 10.0f;
                matrixProductToByte(a, yuv, rgb);
                colors[i + 5] = Color.rgb(rgb[0], rgb[1], rgb[2]);
            }
            GradientDrawable gradDraw = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
            gradDraw.setDither(true);
            gradDraw.setLevel(10000);
            gradDraw.setBounds(0, 0, PALETTE_DIM, 40);
            gradDraw.draw(this.mHorSlidersCv[1]);
        }

        private void setHorVSlider() {
            ColorMatrix cm = new ColorMatrix();
            cm.setYUV2RGB();
            float[] a = cm.getArray();
            float[] yuv = new float[3];
            int[] rgb = new int[3];
            yuv[0] = this.mYUV[0];
            yuv[1] = this.mYUV[1];
            int[] colors = new int[TRACK_SWATCH_NEW];
            for (int i = -5; i <= 5; i++) {
                yuv[2] = ((float) i) / 10.0f;
                matrixProductToByte(a, yuv, rgb);
                colors[i + 5] = Color.rgb(rgb[0], rgb[1], rgb[2]);
            }
            GradientDrawable gradDraw = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, colors);
            gradDraw.setDither(true);
            gradDraw.setLevel(10000);
            gradDraw.setBounds(0, 0, PALETTE_DIM, 40);
            gradDraw.draw(this.mHorSlidersCv[2]);
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            setMeasuredDimension(VIEW_DIM_X, VIEW_DIM_Y);
        }

        public int sliderPosTo255(int sliderPos) {
            return pinToByte((int) (255.0f * (((float) sliderPos) / ((float) PALETTE_DIM))));
        }

        private int round(double x) {
            return (int) Math.round(x);
        }

        private int pinToByte(int n) {
            if (n < 0) {
                return 0;
            }
            if (n > 255) {
                return 255;
            }
            return n;
        }

        private float pinToUnit(float n) {
            if (n < 0.0f) {
                return 0.0f;
            }
            if (n > 1.0f) {
                return 1.0f;
            }
            return n;
        }

        private float pin(float n, float max) {
            if (n < 0.0f) {
                return 0.0f;
            }
            if (n > max) {
                return max;
            }
            return n;
        }

        private float pin(float n, float min, float max) {
            if (n < min) {
                return min;
            }
            if (n > max) {
                return max;
            }
            return n;
        }

        private void matrixProductToByte(float[] a, float[] in, int[] out) {
            out[0] = pinToByte(round((double) (((a[0] * in[0]) + (a[1] * in[1]) + (a[2] * in[2])) * 255.0f)));
            out[1] = pinToByte(round((double) (((a[5] * in[0]) + (a[6] * in[1]) + (a[7] * in[2])) * 255.0f)));
            out[2] = pinToByte(round((double) (((a[10] * in[0]) + (a[TRACK_SWATCH_NEW] * in[1]) + (a[TEXT_SIZE] * in[2])) * 255.0f)));
        }

        private int ave(int s, int d, float p) {
            return round((double) (((float) (d - s)) * p)) + s;
        }

        private int interpColor(int[] colors, float unit) {
            if (unit <= 0.0f) {
                return colors[0];
            }
            if (unit >= 1.0f) {
                return colors[colors.length - 1];
            }
            float p = unit * ((float) (colors.length - 1));
            int i = (int) p;
            float p2 = p - ((float) i);
            int c0 = colors[i];
            int c1 = colors[i + 1];
            return Color.argb(ave(Color.alpha(c0), Color.alpha(c1), p2), ave(Color.red(c0), Color.red(c1), p2), ave(Color.green(c0), Color.green(c1), p2), ave(Color.blue(c0), Color.blue(c1), p2));
        }

        private void HSV2RGB(float[] hsv, float[] rgb) {
            float f = (hsv[0] / 60.0f) - ((float) (((int) hsv[0]) / 60));
            float p = hsv[2] * (1.0f - hsv[1]);
            float q = hsv[2] * (1.0f - (hsv[1] * f));
            float t = hsv[2] * (1.0f - ((1.0f - f) * hsv[1]));
            switch ((((int) hsv[0]) / 60) % 6) {
                case ZmanimFormatter.SEXAGESIMAL_XSD_FORMAT /*0*/:
                    rgb[0] = hsv[2];
                    rgb[1] = t;
                    rgb[2] = p;
                    return;
                case 1:
                    rgb[0] = q;
                    rgb[1] = hsv[2];
                    rgb[2] = p;
                    return;
                case 2:
                    rgb[0] = p;
                    rgb[1] = hsv[2];
                    rgb[2] = t;
                    return;
                case 3:
                    rgb[0] = p;
                    rgb[1] = q;
                    rgb[2] = hsv[2];
                    return;
                case 4:
                    rgb[0] = t;
                    rgb[1] = p;
                    rgb[2] = hsv[2];
                    return;
                case 5:
                    rgb[0] = hsv[2];
                    rgb[1] = p;
                    rgb[2] = q;
                    return;
                default:
                    return;
            }
        }

        public boolean ptInRect(int x, int y, Rect r) {
            return x > r.left && x < r.right && y > r.top && y < r.bottom;
        }

        public boolean dispatchTrackballEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();
            long currTime = Calendar.getInstance().getTimeInMillis();
            int jump = event.getHistorySize() + 1;
            switch (event.getAction()) {
                case 2:
                    switch (this.mMethod) {
                        case ZmanimFormatter.SEXAGESIMAL_XSD_FORMAT /*0*/:
                            if (this.mFocusedControl != 0) {
                                if (this.mFocusedControl == 1) {
                                    if (y >= 0.0f) {
                                        if (y > 0.0f) {
                                            changeSlider(this.mFocusedControl, false, jump);
                                            break;
                                        }
                                    } else {
                                        changeSlider(this.mFocusedControl, true, jump);
                                        break;
                                    }
                                }
                            } else {
                                changeHSPalette(x, y, jump);
                                break;
                            }
                            break;
                        case 1:
                            if (this.mFocusedControl != 0) {
                                if (this.mFocusedControl == 1) {
                                    if (y >= 0.0f) {
                                        if (y > 0.0f) {
                                            changeSlider(this.mFocusedControl, false, jump);
                                            break;
                                        }
                                    } else {
                                        changeSlider(this.mFocusedControl, true, jump);
                                        break;
                                    }
                                }
                            } else {
                                changeHVPalette(x, y, jump);
                                break;
                            }
                            break;
                        case 2:
                            if (this.mFocusedControl != 0) {
                                if (this.mFocusedControl == 1) {
                                    if (y >= 0.0f) {
                                        if (y > 0.0f) {
                                            changeSlider(this.mFocusedControl, false, jump);
                                            break;
                                        }
                                    } else {
                                        changeSlider(this.mFocusedControl, true, jump);
                                        break;
                                    }
                                }
                            } else {
                                changeSVPalette(x, y, jump);
                                break;
                            }
                            break;
                        case 3:
                            if (this.mFocusedControl != 0) {
                                if (this.mFocusedControl == 1) {
                                    if (y >= 0.0f) {
                                        if (y > 0.0f) {
                                            changeSlider(this.mFocusedControl, false, jump);
                                            break;
                                        }
                                    } else {
                                        changeSlider(this.mFocusedControl, true, jump);
                                        break;
                                    }
                                }
                            } else {
                                changeUVPalette(x, y, jump);
                                break;
                            }
                            break;
                        case 4:
                        case 5:
                        case 6:
                            if (y >= 0.0f) {
                                if (y <= 0.0f) {
                                    if (x >= 0.0f || this.mFocusedControl == TRACKED_NONE) {
                                        if (x > 0.0f && this.mFocusedControl != TRACKED_NONE) {
                                            changeSlider(this.mFocusedControl, true, jump);
                                            break;
                                        }
                                    } else {
                                        changeSlider(this.mFocusedControl, false, jump);
                                        break;
                                    }
                                } else if (this.mFocusedControl != TRACKED_NONE) {
                                    if (this.mFocusedControl < 2 && currTime - this.mTimeOfLastSliderSwitch > 200) {
                                        this.mTimeOfLastSliderSwitch = currTime;
                                        this.mFocusedControl++;
                                        invalidate();
                                        break;
                                    }
                                } else {
                                    this.mFocusedControl = 0;
                                    invalidate();
                                    break;
                                }
                            } else if (this.mFocusedControl != TRACKED_NONE) {
                                if (this.mFocusedControl > 0 && currTime - this.mTimeOfLastSliderSwitch > 200) {
                                    this.mTimeOfLastSliderSwitch = currTime;
                                    this.mFocusedControl--;
                                    invalidate();
                                    break;
                                }
                            } else {
                                this.mFocusedControl = 2;
                                invalidate();
                                break;
                            }
                            break;
                    }
            }
            return true;
        }

        private void changeHSPalette(float x, float y, int jump) {
            int x2 = 0;
            int y2 = 0;
            if (x < 0.0f) {
                x2 = -jump;
            } else if (x > 0.0f) {
                x2 = jump;
            }
            if (y < 0.0f) {
                y2 = -jump;
            } else if (y > 0.0f) {
                y2 = jump;
            }
            int[] iArr = this.mCoord;
            iArr[0] = iArr[0] + x2;
            int[] iArr2 = this.mCoord;
            iArr2[1] = iArr2[1] + y2;
            if (this.mCoord[0] < (-PALETTE_RADIUS)) {
                this.mCoord[0] = -PALETTE_RADIUS;
            } else if (this.mCoord[0] > PALETTE_RADIUS) {
                this.mCoord[0] = PALETTE_RADIUS;
            }
            if (this.mCoord[1] < (-PALETTE_RADIUS)) {
                this.mCoord[1] = -PALETTE_RADIUS;
            } else if (this.mCoord[1] > PALETTE_RADIUS) {
                this.mCoord[1] = PALETTE_RADIUS;
            }
            float radius = (float) Math.sqrt((double) ((this.mCoord[0] * this.mCoord[0]) + (this.mCoord[1] * this.mCoord[1])));
            if (radius > ((float) PALETTE_RADIUS)) {
                radius = (float) PALETTE_RADIUS;
            }
            float angle = (float) Math.atan2((double) this.mCoord[1], (double) this.mCoord[0]);
            float unit = angle / 6.2831855f;
            if (unit < 0.0f) {
                unit += 1.0f;
            }
            this.mCoord[0] = round(Math.cos((double) angle) * ((double) radius));
            this.mCoord[1] = round(Math.sin((double) angle) * ((double) radius));
            float[] hsv = new float[3];
            Color.colorToHSV(interpColor(this.mSpectrumColorsRev, unit), hsv);
            this.mHSV[0] = hsv[0];
            this.mHSV[1] = radius / ((float) PALETTE_RADIUS);
            updateAllFromHSV();
            this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
            setVerValSlider();
            invalidate();
        }

        private void changeHVPalette(float x, float y, int jump) {
            int x2 = 0;
            int y2 = 0;
            if (x < 0.0f) {
                x2 = -jump;
            } else if (x > 0.0f) {
                x2 = jump;
            }
            if (y < 0.0f) {
                y2 = -jump;
            } else if (y > 0.0f) {
                y2 = jump;
            }
            int[] iArr = this.mCoord;
            iArr[0] = iArr[0] + x2;
            int[] iArr2 = this.mCoord;
            iArr2[2] = iArr2[2] + y2;
            if (this.mCoord[0] < (-PALETTE_RADIUS)) {
                this.mCoord[0] = -PALETTE_RADIUS;
            } else if (this.mCoord[0] > PALETTE_RADIUS) {
                this.mCoord[0] = PALETTE_RADIUS;
            }
            if (this.mCoord[2] < (-PALETTE_RADIUS)) {
                this.mCoord[2] = -PALETTE_RADIUS;
            } else if (this.mCoord[2] > PALETTE_RADIUS) {
                this.mCoord[2] = PALETTE_RADIUS;
            }
            float radius = (float) Math.sqrt((double) ((this.mCoord[0] * this.mCoord[0]) + (this.mCoord[2] * this.mCoord[2])));
            if (radius > ((float) PALETTE_RADIUS)) {
                radius = (float) PALETTE_RADIUS;
            }
            float angle = (float) Math.atan2((double) this.mCoord[2], (double) this.mCoord[0]);
            float unit = angle / 6.2831855f;
            if (unit < 0.0f) {
                unit += 1.0f;
            }
            this.mCoord[0] = round(Math.cos((double) angle) * ((double) radius));
            this.mCoord[2] = round(Math.sin((double) angle) * ((double) radius));
            float[] hsv = new float[3];
            Color.colorToHSV(interpColor(this.mSpectrumColorsRev, unit), hsv);
            this.mHSV[0] = hsv[0];
            this.mHSV[2] = radius / ((float) PALETTE_RADIUS);
            updateAllFromHSV();
            this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
            setOvalSatFader();
            setVerSatSlider();
            invalidate();
        }

        private void changeSVPalette(float x, float y, int jump) {
            int x2 = 0;
            int y2 = 0;
            if (x < 0.0f) {
                x2 = -jump;
            } else if (x > 0.0f) {
                x2 = jump;
            }
            if (y < 0.0f) {
                y2 = -jump;
            } else if (y > 0.0f) {
                y2 = jump;
            }
            int[] iArr = this.mCoord;
            iArr[1] = iArr[1] + y2;
            int[] iArr2 = this.mCoord;
            iArr2[2] = iArr2[2] + x2;
            this.mCoord[1] = (int) pin((float) this.mCoord[1], (float) PALETTE_DIM);
            this.mCoord[2] = (int) pin((float) this.mCoord[2], (float) PALETTE_DIM);
            this.mHSV[1] = ((float) (PALETTE_DIM - this.mCoord[1])) / ((float) PALETTE_DIM);
            this.mHSV[2] = ((float) this.mCoord[2]) / ((float) PALETTE_DIM);
            updateAllFromHSV();
            this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
            setHorSatFader();
            setHorValDimmer();
            invalidate();
        }

        private void changeUVPalette(float x, float y, int jump) {
            int x2 = 0;
            int y2 = 0;
            if (x < 0.0f) {
                x2 = -jump;
            } else if (x > 0.0f) {
                x2 = jump;
            }
            if (y < 0.0f) {
                y2 = -jump;
            } else if (y > 0.0f) {
                y2 = jump;
            }
            int[] iArr = this.mCoord;
            iArr[1] = iArr[1] + x2;
            int[] iArr2 = this.mCoord;
            iArr2[2] = iArr2[2] + y2;
            this.mCoord[1] = (int) pin((float) this.mCoord[1], (float) PALETTE_DIM);
            this.mCoord[2] = (int) pin((float) this.mCoord[2], (float) PALETTE_DIM);
            this.mYUV[1] = (((float) this.mCoord[1]) / ((float) PALETTE_DIM)) - 0.5f;
            this.mYUV[2] = (((float) (PALETTE_DIM - this.mCoord[2])) / ((float) PALETTE_DIM)) - 0.5f;
            updateAllFromYUV();
            ColorMatrix cm = new ColorMatrix();
            cm.setYUV2RGB();
            int[] rgb = new int[3];
            matrixProductToByte(cm.getArray(), this.mYUV, rgb);
            this.mSwatchNew.setColor(Color.rgb(rgb[0], rgb[1], rgb[2]));
            setVerYSlider();
            invalidate();
        }

        private void changeSlider(int slider, boolean increase, int jump) {
            if (this.mMethod == 0) {
                float[] fArr = this.mHSV;
                fArr[2] = fArr[2] + (((float) (increase ? jump : -jump)) / 256.0f);
                this.mHSV[2] = pinToUnit(this.mHSV[2]);
                updateAllFromHSV();
                this.mCoord[2] = PALETTE_DIM - ((int) (this.mHSV[2] * ((float) PALETTE_DIM)));
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setOvalValDimmer();
                invalidate();
            } else if (this.mMethod == 1) {
                float[] fArr2 = this.mHSV;
                fArr2[1] = fArr2[1] + (((float) (increase ? jump : -jump)) / 256.0f);
                this.mHSV[1] = pinToUnit(this.mHSV[1]);
                updateAllFromHSV();
                this.mCoord[1] = PALETTE_DIM - ((int) (this.mHSV[1] * ((float) PALETTE_DIM)));
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setOvalSatFader();
                invalidate();
            } else if (this.mMethod == 2) {
                float[] fArr3 = this.mHSV;
                fArr3[0] = fArr3[0] + ((((float) (increase ? jump : -jump)) / 256.0f) * 360.0f);
                this.mHSV[0] = pin(this.mHSV[0], 360.0f);
                updateAllFromHSV();
                this.mCoord[0] = PALETTE_DIM - ((int) ((this.mHSV[0] / 360.0f) * ((float) PALETTE_DIM)));
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setSatValPalette();
                invalidate();
            } else if (this.mMethod == 3) {
                float[] fArr4 = this.mYUV;
                fArr4[0] = fArr4[0] + (((float) (increase ? jump : -jump)) / 256.0f);
                this.mYUV[0] = pinToUnit(this.mYUV[0]);
                updateAllFromYUV();
                this.mCoord[0] = PALETTE_DIM - ((int) (this.mYUV[0] * ((float) PALETTE_DIM)));
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setUVPalette();
                invalidate();
            } else if (this.mMethod == 4) {
                int color = Color.HSVToColor(this.mHSV);
                if (slider == 0) {
                    this.mRGB[slider] = Color.red(color) + (increase ? jump : -jump);
                } else if (slider == 1) {
                    this.mRGB[slider] = Color.green(color) + (increase ? jump : -jump);
                } else {
                    this.mRGB[slider] = Color.blue(color) + (increase ? jump : -jump);
                }
                this.mRGB[slider] = pinToByte(this.mRGB[slider]);
                updateAllFromRGB();
                this.mCoord[slider] = (int) (((float) PALETTE_DIM) * (((float) this.mRGB[slider]) / 255.0f));
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                if (slider != 0) {
                    setHorRSlider();
                }
                if (slider != 1) {
                    setHorGSlider();
                }
                if (slider != 2) {
                    setHorBSlider();
                }
                invalidate();
            } else if (this.mMethod == 5) {
                if (slider == 0) {
                    float[] fArr5 = this.mHSV;
                    fArr5[slider] = fArr5[slider] + ((((float) (increase ? jump : -jump)) / 256.0f) * 360.0f);
                    this.mHSV[slider] = pin(this.mHSV[slider], 360.0f);
                    this.mCoord[slider] = (int) ((this.mHSV[slider] / 360.0f) * ((float) PALETTE_DIM));
                } else {
                    float[] fArr6 = this.mHSV;
                    fArr6[slider] = fArr6[slider] + (((float) (increase ? jump : -jump)) / 256.0f);
                    this.mHSV[slider] = pinToUnit(this.mHSV[slider]);
                    this.mCoord[slider] = (int) (this.mHSV[slider] * ((float) PALETTE_DIM));
                }
                updateAllFromHSV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                if (slider == 0) {
                    setHorSatSlider();
                    setHorValSlider();
                } else if (slider == 1) {
                    setHorSatFader();
                    setHorValSlider();
                } else if (slider == 2) {
                    setHorValDimmer();
                    setHorSatSlider();
                }
                invalidate();
            } else if (this.mMethod == 6) {
                float[] fArr7 = this.mYUV;
                fArr7[slider] = fArr7[slider] + (((float) (increase ? jump : -jump)) / 256.0f);
                if (slider == 0) {
                    this.mYUV[slider] = pinToUnit(this.mYUV[slider]);
                } else {
                    this.mYUV[slider] = pin(this.mYUV[slider], -0.5f, 0.5f);
                }
                updateAllFromYUV();
                if (slider == 0) {
                    this.mCoord[slider] = (int) (this.mYUV[slider] * ((float) PALETTE_DIM));
                } else {
                    this.mCoord[slider] = (int) ((this.mYUV[slider] + 0.5f) * ((float) PALETTE_DIM));
                }
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                if (slider != 0) {
                    setHorYSlider();
                }
                if (slider != 1) {
                    setHorUSlider();
                }
                if (slider != 2) {
                    setHorVSlider();
                }
                invalidate();
            }
        }

        private void updateHSVfromRGB() {
            Color.RGBToHSV(this.mRGB[0], this.mRGB[1], this.mRGB[2], this.mHSV);
            if (UberColorPickerDialog.isGray(this.mRGB)) {
                this.mHSV[1] = 0.0f;
            }
        }

        private void updateHSVfromYUV() {
            ColorMatrix cm = new ColorMatrix();
            cm.setYUV2RGB();
            matrixProductToByte(cm.getArray(), this.mYUV, this.mRGB);
            updateHSVfromRGB();
        }

        private void updateRGBfromHSV() {
            int color = Color.HSVToColor(this.mHSV);
            this.mRGB[0] = Color.red(color);
            this.mRGB[1] = Color.green(color);
            this.mRGB[2] = Color.blue(color);
        }

        private void updateRGBfromYUV() {
            ColorMatrix cm = new ColorMatrix();
            cm.setYUV2RGB();
            matrixProductToByte(cm.getArray(), this.mYUV, this.mRGB);
        }

        private void updateYUVfromRGB() {
            float r = ((float) this.mRGB[0]) / 255.0f;
            float g = ((float) this.mRGB[1]) / 255.0f;
            float b = ((float) this.mRGB[2]) / 255.0f;
            ColorMatrix cm = new ColorMatrix();
            cm.setRGB2YUV();
            float[] a = cm.getArray();
            this.mYUV[0] = (a[0] * r) + (a[1] * g) + (a[2] * b);
            this.mYUV[0] = pinToUnit(this.mYUV[0]);
            this.mYUV[1] = (a[5] * r) + (a[6] * g) + (a[7] * b);
            this.mYUV[1] = pin(this.mYUV[1], -0.5f, 0.5f);
            this.mYUV[2] = (a[10] * r) + (a[TRACK_SWATCH_NEW] * g) + (a[TEXT_SIZE] * b);
            this.mYUV[2] = pin(this.mYUV[2], -0.5f, 0.5f);
        }

        private void updateHexFromHSV() {
            this.mHexStr = Integer.toHexString(Color.HSVToColor(this.mHSV)).toUpperCase();
            this.mHexStr = this.mHexStr.substring(2, this.mHexStr.length());
        }

        private void updateAllFromHSV() {
            if (this.mRGBenabled || this.mYUVenabled) {
                updateRGBfromHSV();
            }
            if (this.mYUVenabled) {
                updateYUVfromRGB();
            }
            if (this.mRGBenabled) {
                updateHexFromHSV();
            }
        }

        private void updateAllFromRGB() {
            if (this.mHSVenabled || this.mHexenabled) {
                updateHSVfromRGB();
            }
            if (this.mYUVenabled) {
                updateYUVfromRGB();
            }
            if (this.mHexenabled) {
                updateHexFromHSV();
            }
        }

        private void updateAllFromYUV() {
            if (this.mRGBenabled || this.mHSVenabled || this.mHexenabled) {
                updateRGBfromYUV();
            }
            if (this.mHSVenabled) {
                updateHSVfromRGB();
            }
            if (this.mHexenabled) {
                updateHexFromHSV();
            }
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public boolean onTouchEvent(MotionEvent event) {
            boolean inOvalPalette;
            float x = event.getX();
            float y = event.getY();
            int x2 = (int) pin((float) round((double) (x - ((float) PALETTE_POS_X))), (float) PALETTE_DIM);
            int y2 = (int) pin((float) round((double) (y - ((float) PALETTE_POS_Y))), (float) PALETTE_DIM);
            float circlePinnedX = (x - ((float) PALETTE_POS_X)) - ((float) PALETTE_CENTER_X);
            float circlePinnedY = (y - ((float) PALETTE_POS_Y)) - ((float) PALETTE_CENTER_Y);
            boolean inSwatchOld = ptInRect(round((double) x), round((double) y), this.mOldSwatchRect);
            boolean inSwatchNew = ptInRect(round((double) x), round((double) y), this.mNewSwatchRect);
            boolean[] inMethodSelector = new boolean[NUM_ENABLED_METHODS];
            for (int i = 0; i < NUM_ENABLED_METHODS; i++) {
                inMethodSelector[i] = false;
            }
            int i2 = 0;
            while (true) {
                if (i2 >= NUM_ENABLED_METHODS) {
                    break;
                }
                if (ptInRect(round((double) x), round((double) y), this.mMethodSelectorRects[i2])) {
                    inMethodSelector[i2] = true;
                    break;
                }
                i2++;
            }
            float radius = (float) Math.sqrt((double) ((circlePinnedX * circlePinnedX) + (circlePinnedY * circlePinnedY)));
            if (radius <= ((float) PALETTE_RADIUS)) {
                inOvalPalette = true;
            } else {
                inOvalPalette = false;
            }
            if (radius > ((float) PALETTE_RADIUS)) {
                radius = (float) PALETTE_RADIUS;
            }
            boolean inSquarePalette = ptInRect(round((double) x), round((double) y), this.mPaletteRect);
            boolean inVerSlider = ptInRect(round((double) x), round((double) y), this.mVerSliderRect);
            boolean inFirstHorSlider = ptInRect(round((double) x), round((double) y), this.mHorSliderRects[0]);
            boolean inSecondHorSlider = ptInRect(round((double) x), round((double) y), this.mHorSliderRects[1]);
            boolean inThirdHorSlider = ptInRect(round((double) x), round((double) y), this.mHorSliderRects[2]);
            switch (event.getAction()) {
                case ZmanimFormatter.SEXAGESIMAL_XSD_FORMAT /*0*/:
                    this.mTracking = TRACKED_NONE;
                    if (!inSwatchOld) {
                        if (!inSwatchNew) {
                            if (NUM_ENABLED_METHODS <= 0 || !inMethodSelector[0]) {
                                if (NUM_ENABLED_METHODS <= 1 || !inMethodSelector[1]) {
                                    if (NUM_ENABLED_METHODS <= 2 || !inMethodSelector[2]) {
                                        if (NUM_ENABLED_METHODS <= 3 || !inMethodSelector[3]) {
                                            if (NUM_ENABLED_METHODS <= 4 || !inMethodSelector[4]) {
                                                if (NUM_ENABLED_METHODS <= 5 || !inMethodSelector[5]) {
                                                    if (NUM_ENABLED_METHODS <= 6 || !inMethodSelector[6]) {
                                                        if (this.mMethod != 0) {
                                                            if (this.mMethod != 1) {
                                                                if (this.mMethod != 2) {
                                                                    if (this.mMethod != 3) {
                                                                        if (this.mMethod != 4) {
                                                                            if (this.mMethod != 5) {
                                                                                if (this.mMethod == 6) {
                                                                                    if (!inFirstHorSlider) {
                                                                                        if (!inSecondHorSlider) {
                                                                                            if (inThirdHorSlider) {
                                                                                                this.mTracking = TRACK_V_SLIDER;
                                                                                                this.mFocusedControl = 2;
                                                                                                break;
                                                                                            }
                                                                                        } else {
                                                                                            this.mTracking = TRACK_U_SLIDER;
                                                                                            this.mFocusedControl = 1;
                                                                                            break;
                                                                                        }
                                                                                    } else {
                                                                                        this.mTracking = TRACK_HOR_Y_SLIDER;
                                                                                        this.mFocusedControl = 0;
                                                                                        break;
                                                                                    }
                                                                                }
                                                                            } else if (!inFirstHorSlider) {
                                                                                if (!inSecondHorSlider) {
                                                                                    if (inThirdHorSlider) {
                                                                                        this.mTracking = TRACK_HOR_VALUE_SLIDER;
                                                                                        this.mFocusedControl = 2;
                                                                                        break;
                                                                                    }
                                                                                } else {
                                                                                    this.mTracking = TRACK_S_SLIDER;
                                                                                    this.mFocusedControl = 1;
                                                                                    break;
                                                                                }
                                                                            } else {
                                                                                this.mTracking = TRACK_H_SLIDER;
                                                                                this.mFocusedControl = 0;
                                                                                break;
                                                                            }
                                                                        } else if (!inFirstHorSlider) {
                                                                            if (!inSecondHorSlider) {
                                                                                if (inThirdHorSlider) {
                                                                                    this.mTracking = TRACK_B_SLIDER;
                                                                                    this.mFocusedControl = 2;
                                                                                    break;
                                                                                }
                                                                            } else {
                                                                                this.mTracking = TRACK_G_SLIDER;
                                                                                this.mFocusedControl = 1;
                                                                                break;
                                                                            }
                                                                        } else {
                                                                            this.mTracking = 60;
                                                                            this.mFocusedControl = 0;
                                                                            break;
                                                                        }
                                                                    } else if (!inSquarePalette) {
                                                                        if (inVerSlider) {
                                                                            this.mTracking = TRACK_VER_Y_SLIDER;
                                                                            this.mFocusedControl = 1;
                                                                            break;
                                                                        }
                                                                    } else {
                                                                        this.mTracking = TRACK_UV_PALETTE;
                                                                        this.mFocusedControl = 0;
                                                                        break;
                                                                    }
                                                                } else if (!inSquarePalette) {
                                                                    if (inVerSlider) {
                                                                        this.mTracking = TRACK_VER_H_SLIDER;
                                                                        this.mFocusedControl = 1;
                                                                        break;
                                                                    }
                                                                } else {
                                                                    this.mTracking = 40;
                                                                    this.mFocusedControl = 0;
                                                                    break;
                                                                }
                                                            } else if (!inOvalPalette) {
                                                                if (inVerSlider) {
                                                                    this.mTracking = TRACK_VER_S_SLIDER;
                                                                    this.mFocusedControl = 1;
                                                                    break;
                                                                }
                                                            } else {
                                                                this.mTracking = 20;
                                                                this.mFocusedControl = 0;
                                                                break;
                                                            }
                                                        } else if (!inOvalPalette) {
                                                            if (inVerSlider) {
                                                                this.mTracking = TRACK_VER_VALUE_SLIDER;
                                                                this.mFocusedControl = 1;
                                                                break;
                                                            }
                                                        } else {
                                                            this.mTracking = TRACK_HS_PALETTE;
                                                            this.mFocusedControl = 0;
                                                            break;
                                                        }
                                                    } else {
                                                        this.mTracking = this.mMethodSelectRectMap[6];
                                                        break;
                                                    }
                                                } else {
                                                    this.mTracking = this.mMethodSelectRectMap[5];
                                                    break;
                                                }
                                            } else {
                                                this.mTracking = this.mMethodSelectRectMap[4];
                                                break;
                                            }
                                        } else {
                                            this.mTracking = this.mMethodSelectRectMap[3];
                                            break;
                                        }
                                    } else {
                                        this.mTracking = this.mMethodSelectRectMap[2];
                                        break;
                                    }
                                } else {
                                    this.mTracking = this.mMethodSelectRectMap[1];
                                    break;
                                }
                            } else {
                                this.mTracking = this.mMethodSelectRectMap[0];
                                break;
                            }
                        } else {
                            this.mTracking = TRACK_SWATCH_NEW;
                            break;
                        }
                    } else {
                        this.mTracking = 10;
                        break;
                    }
                    break;
                case 1:
                    if (this.mTracking == 10 && inSwatchOld) {
                        Color.colorToHSV(this.mOriginalColor, this.mHSV);
                        if (UberColorPickerDialog.isGray(this.mOriginalColor)) {
                            this.mHSV[1] = 0.0f;
                        }
                        this.mSwatchNew.setColor(this.mOriginalColor);
                        initUI();
                        invalidate();
                    } else if (this.mTracking == TRACK_SWATCH_NEW && inSwatchNew) {
                        this.mListener.colorChanged(this.mSwatchNew.getColor());
                        invalidate();
                    } else if (NUM_ENABLED_METHODS > 0 && this.mTracking == this.mMethodSelectRectMap[0] && inMethodSelector[0]) {
                        this.mMethod = this.mMethodSelectRectMap[0];
                        initUI();
                        invalidate();
                    } else if (NUM_ENABLED_METHODS > 1 && this.mTracking == this.mMethodSelectRectMap[1] && inMethodSelector[1]) {
                        this.mMethod = this.mMethodSelectRectMap[1];
                        initUI();
                        invalidate();
                    } else if (NUM_ENABLED_METHODS > 2 && this.mTracking == this.mMethodSelectRectMap[2] && inMethodSelector[2]) {
                        this.mMethod = this.mMethodSelectRectMap[2];
                        initUI();
                        invalidate();
                    } else if (NUM_ENABLED_METHODS > 3 && this.mTracking == this.mMethodSelectRectMap[3] && inMethodSelector[3]) {
                        this.mMethod = this.mMethodSelectRectMap[3];
                        initUI();
                        invalidate();
                    } else if (NUM_ENABLED_METHODS > 4 && this.mTracking == this.mMethodSelectRectMap[4] && inMethodSelector[4]) {
                        this.mMethod = this.mMethodSelectRectMap[4];
                        initUI();
                        invalidate();
                    } else if (NUM_ENABLED_METHODS > 5 && this.mTracking == this.mMethodSelectRectMap[5] && inMethodSelector[5]) {
                        this.mMethod = this.mMethodSelectRectMap[5];
                        initUI();
                        invalidate();
                    } else if (NUM_ENABLED_METHODS > 6 && this.mTracking == this.mMethodSelectRectMap[6] && inMethodSelector[6]) {
                        this.mMethod = this.mMethodSelectRectMap[6];
                        initUI();
                        invalidate();
                    }
                    this.mTracking = TRACKED_NONE;
                    return true;
                case 2:
                    break;
                default:
                    return true;
            }
            if (this.mTracking == TRACK_HS_PALETTE) {
                float angle = (float) Math.atan2((double) circlePinnedY, (double) circlePinnedX);
                float unit = angle / 6.2831855f;
                if (unit < 0.0f) {
                    unit += 1.0f;
                }
                this.mCoord[0] = round(Math.cos((double) angle) * ((double) radius));
                this.mCoord[1] = round(Math.sin((double) angle) * ((double) radius));
                float[] hsv = new float[3];
                Color.colorToHSV(interpColor(this.mSpectrumColorsRev, unit), hsv);
                this.mHSV[0] = hsv[0];
                this.mHSV[1] = radius / ((float) PALETTE_RADIUS);
                updateAllFromHSV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setVerValSlider();
                invalidate();
                return true;
            } else if (this.mTracking == TRACK_VER_VALUE_SLIDER) {
                if (this.mCoord[2] == y2) {
                    return true;
                }
                this.mCoord[2] = y2;
                this.mHSV[2] = 1.0f - (((float) y2) / ((float) PALETTE_DIM));
                updateAllFromHSV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setOvalValDimmer();
                invalidate();
                return true;
            } else if (this.mTracking == 20) {
                float angle2 = (float) Math.atan2((double) circlePinnedY, (double) circlePinnedX);
                float unit2 = angle2 / 6.2831855f;
                if (unit2 < 0.0f) {
                    unit2 += 1.0f;
                }
                this.mCoord[0] = round(Math.cos((double) angle2) * ((double) radius));
                this.mCoord[2] = round(Math.sin((double) angle2) * ((double) radius));
                float[] hsv2 = new float[3];
                Color.colorToHSV(interpColor(this.mSpectrumColorsRev, unit2), hsv2);
                this.mHSV[0] = hsv2[0];
                this.mHSV[2] = radius / ((float) PALETTE_RADIUS);
                updateAllFromHSV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setOvalSatFader();
                setVerSatSlider();
                invalidate();
                return true;
            } else if (this.mTracking == TRACK_VER_S_SLIDER) {
                if (this.mCoord[1] == y2) {
                    return true;
                }
                this.mCoord[1] = y2;
                this.mHSV[1] = 1.0f - (((float) y2) / ((float) PALETTE_DIM));
                updateAllFromHSV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setOvalSatFader();
                invalidate();
                return true;
            } else if (this.mTracking == 40) {
                if (this.mCoord[1] == y2 && this.mCoord[2] == x2) {
                    return true;
                }
                this.mCoord[1] = y2;
                this.mCoord[2] = x2;
                this.mHSV[1] = ((float) (PALETTE_DIM - this.mCoord[1])) / ((float) PALETTE_DIM);
                this.mHSV[2] = ((float) this.mCoord[2]) / ((float) PALETTE_DIM);
                updateAllFromHSV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setHorSatFader();
                setHorValDimmer();
                invalidate();
                return true;
            } else if (this.mTracking == TRACK_VER_H_SLIDER) {
                if (this.mCoord[0] == y2) {
                    return true;
                }
                this.mCoord[0] = y2;
                this.mHSV[0] = 360.0f - (360.0f * (((float) y2) / ((float) PALETTE_DIM)));
                updateAllFromHSV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setSatValPalette();
                invalidate();
                return true;
            } else if (this.mTracking == TRACK_UV_PALETTE) {
                if (this.mCoord[1] == y2 && this.mCoord[2] == x2) {
                    return true;
                }
                this.mCoord[1] = x2;
                this.mCoord[2] = y2;
                this.mYUV[1] = (((float) this.mCoord[1]) / ((float) PALETTE_DIM)) - 0.5f;
                this.mYUV[2] = (((float) (PALETTE_DIM - this.mCoord[2])) / ((float) PALETTE_DIM)) - 0.5f;
                updateAllFromYUV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setVerYSlider();
                invalidate();
                return true;
            } else if (this.mTracking == TRACK_VER_Y_SLIDER) {
                if (this.mCoord[0] == y2) {
                    return true;
                }
                this.mCoord[0] = y2;
                this.mYUV[0] = 1.0f - (((float) this.mCoord[0]) / ((float) PALETTE_DIM));
                updateAllFromYUV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setUVPalette();
                invalidate();
                return true;
            } else if (this.mTracking == 60) {
                if (this.mCoord[0] == x2) {
                    return true;
                }
                this.mCoord[0] = x2;
                this.mRGB[0] = sliderPosTo255(this.mCoord[0]);
                updateAllFromRGB();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setHorGSlider();
                setHorBSlider();
                invalidate();
                return true;
            } else if (this.mTracking == TRACK_G_SLIDER) {
                if (this.mCoord[1] == x2) {
                    return true;
                }
                this.mCoord[1] = x2;
                this.mRGB[1] = sliderPosTo255(this.mCoord[1]);
                updateAllFromRGB();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setHorRSlider();
                setHorBSlider();
                invalidate();
                return true;
            } else if (this.mTracking == TRACK_B_SLIDER) {
                if (this.mCoord[2] == x2) {
                    return true;
                }
                this.mCoord[2] = x2;
                this.mRGB[2] = sliderPosTo255(this.mCoord[2]);
                updateAllFromRGB();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setHorRSlider();
                setHorGSlider();
                invalidate();
                return true;
            } else if (this.mTracking == TRACK_H_SLIDER) {
                if (this.mCoord[0] == x2) {
                    return true;
                }
                this.mCoord[0] = x2;
                this.mHSV[0] = 360.0f * (((float) this.mCoord[0]) / ((float) PALETTE_DIM));
                updateAllFromHSV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setHorSatSlider();
                setHorValSlider();
                invalidate();
                return true;
            } else if (this.mTracking == TRACK_S_SLIDER) {
                if (this.mCoord[1] == x2) {
                    return true;
                }
                this.mCoord[1] = x2;
                this.mHSV[1] = ((float) this.mCoord[1]) / ((float) PALETTE_DIM);
                updateAllFromHSV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setHorSatFader();
                setHorValSlider();
                invalidate();
                return true;
            } else if (this.mTracking == TRACK_HOR_VALUE_SLIDER) {
                if (this.mCoord[2] == x2) {
                    return true;
                }
                this.mCoord[2] = x2;
                this.mHSV[2] = ((float) this.mCoord[2]) / ((float) PALETTE_DIM);
                updateAllFromHSV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setHorValDimmer();
                setHorSatSlider();
                invalidate();
                return true;
            } else if (this.mTracking == TRACK_HOR_Y_SLIDER) {
                if (this.mCoord[0] == x2) {
                    return true;
                }
                this.mCoord[0] = x2;
                this.mYUV[0] = ((float) this.mCoord[0]) / ((float) PALETTE_DIM);
                updateAllFromYUV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setHorUSlider();
                setHorVSlider();
                invalidate();
                return true;
            } else if (this.mTracking == TRACK_U_SLIDER) {
                if (this.mCoord[1] == x2) {
                    return true;
                }
                this.mCoord[1] = x2;
                this.mYUV[1] = (((float) this.mCoord[1]) / ((float) PALETTE_DIM)) - 0.5f;
                updateAllFromYUV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setHorYSlider();
                setHorVSlider();
                invalidate();
                return true;
            } else if (this.mTracking != TRACK_V_SLIDER || this.mCoord[2] == x2) {
                return true;
            } else {
                this.mCoord[2] = x2;
                this.mYUV[2] = (((float) this.mCoord[2]) / ((float) PALETTE_DIM)) - 0.5f;
                updateAllFromYUV();
                this.mSwatchNew.setColor(Color.HSVToColor(this.mHSV));
                setHorYSlider();
                setHorUSlider();
                invalidate();
                return true;
            }
        }
    }
}
