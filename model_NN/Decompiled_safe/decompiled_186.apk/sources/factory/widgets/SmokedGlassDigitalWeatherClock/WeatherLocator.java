package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Activity;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class WeatherLocator extends Activity {
    static ArrayList<String> al = new ArrayList<>();
    private static String weatherlocationcode;
    private static String weatherlocationname;
    private ProgressBar mProgressBar;
    private TextView mTextView;

    private static InputStream getWebWeatherSource(String searchcode) throws IOException {
        return new URL("http://weather.msn.com/find.aspx?weasearchstr=" + searchcode).openStream();
    }

    private InputStream getLocalWeatherSource() throws IOException {
        return getAssets().open("beijing_msn.xml");
    }

    public static ArrayList parseWeather(String searchcode) {
        InputStream is = null;
        try {
            is = getWebWeatherSource(searchcode);
            XmlPullParserFactory factory2 = XmlPullParserFactory.newInstance();
            factory2.setNamespaceAware(true);
            XmlPullParser xpp = factory2.newPullParser();
            xpp.setInput(is, "UTF-8");
            for (int eventType = xpp.getEventType(); eventType != 1; eventType = xpp.next()) {
                if (!(eventType == 0 || eventType == 1)) {
                    if (eventType == 2) {
                        String tagName = xpp.getName();
                        al.ensureCapacity(10);
                        if ("weather".equals(tagName)) {
                            weatherlocationcode = xpp.getAttributeValue(null, "weatherlocationcode");
                            weatherlocationname = xpp.getAttributeValue(null, "weatherlocationname");
                            al.add(String.valueOf(weatherlocationname) + "=" + weatherlocationcode);
                        }
                    } else if (eventType != 3) {
                    }
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e22) {
                    e22.printStackTrace();
                }
            }
        } catch (XmlPullParserException e3) {
            e3.printStackTrace();
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e23) {
                    e23.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e24) {
                    e24.printStackTrace();
                }
            }
            throw th;
        }
        return al;
    }
}
