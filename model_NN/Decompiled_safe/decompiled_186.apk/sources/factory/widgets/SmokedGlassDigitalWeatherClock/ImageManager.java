package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.os.Environment;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import org.apache.http.util.ByteArrayBuffer;

public class ImageManager {
    private static String PATH = (String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/factory.widgets.skins/large/");

    /* JADX INFO: Multiple debug info for r6v8 java.io.InputStream: [D('is' java.io.InputStream), D('ucon' java.net.URLConnection)] */
    public static void DownloadFromUrl(String imageURL, String fileName) {
        try {
            URL url = new URL(imageURL);
            File file = new File(fileName);
            long startTime = System.currentTimeMillis();
            Log.d("ImageManager", "download begining");
            Log.d("ImageManager", "download url:" + url);
            Log.d("ImageManager", "downloaded file name:" + fileName);
            BufferedInputStream bis = new BufferedInputStream(url.openConnection().getInputStream(), 8192);
            ByteArrayBuffer baf = new ByteArrayBuffer(50);
            while (true) {
                int current = bis.read();
                if (current == -1) {
                    FileOutputStream fos = new FileOutputStream(String.valueOf(PATH) + file);
                    fos.write(baf.toByteArray());
                    fos.close();
                    Log.d("ImageManager", "download ready in" + ((System.currentTimeMillis() - startTime) / 1000) + " sec");
                    return;
                }
                baf.append((byte) current);
            }
        } catch (IOException e) {
            Log.d("ImageManager", "Error: " + e);
        }
    }
}
