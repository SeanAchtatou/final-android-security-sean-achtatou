package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class ForecastActivity extends Activity {
    /* access modifiers changed from: private */
    public static String latitute;
    /* access modifiers changed from: private */
    public static String longtitute;
    /* access modifiers changed from: private */
    public static long sunsetrise;
    private static String timezone;
    /* access modifiers changed from: private */
    public static String weathercode;
    /* access modifiers changed from: private */
    public static String wsymbol;
    /* access modifiers changed from: private */
    public ProgressBar ProgressBar1;
    /* access modifiers changed from: private */
    public ProgressBar ProgressBar2;
    /* access modifiers changed from: private */
    public ProgressBar ProgressBar3;
    /* access modifiers changed from: private */
    public ProgressBar ProgressBar4;
    /* access modifiers changed from: private */
    public int appWidgetId;
    private String date;
    /* access modifiers changed from: private */
    public String feelslike;
    /* access modifiers changed from: private */
    public TextView high_day1;
    /* access modifiers changed from: private */
    public TextView high_day2;
    /* access modifiers changed from: private */
    public TextView high_day3;
    /* access modifiers changed from: private */
    public TextView high_day4;
    /* access modifiers changed from: private */
    public String humidity;
    final int[] imgSizeIds = {R.drawable.skycode0, R.drawable.skycode1, R.drawable.skycode2, R.drawable.skycode3, R.drawable.skycode4, R.drawable.skycode5, R.drawable.skycode6, R.drawable.skycode7, R.drawable.skycode8, R.drawable.skycode9, R.drawable.skycode10, R.drawable.skycode11, R.drawable.skycode12, R.drawable.skycode13, R.drawable.skycode14, R.drawable.skycode15, R.drawable.skycode16, R.drawable.skycode17, R.drawable.skycode18, R.drawable.skycode19, R.drawable.skycode20, R.drawable.skycode21, R.drawable.skycode22, R.drawable.skycode23, R.drawable.skycode24, R.drawable.skycode25, R.drawable.skycode26, R.drawable.skycode27, R.drawable.skycode28, R.drawable.skycode29, R.drawable.skycode30, R.drawable.skycode31, R.drawable.skycode32, R.drawable.skycode33, R.drawable.skycode34, R.drawable.skycode35, R.drawable.skycode36, R.drawable.skycode37, R.drawable.skycode38, R.drawable.skycode39, R.drawable.skycode40, R.drawable.skycode41, R.drawable.skycode42, R.drawable.skycode43, R.drawable.skycode44, R.drawable.skycode45, R.drawable.skycode46, R.drawable.skycode47};
    /* access modifiers changed from: private */
    public TextView low_day1;
    /* access modifiers changed from: private */
    public TextView low_day2;
    /* access modifiers changed from: private */
    public TextView low_day3;
    /* access modifiers changed from: private */
    public TextView low_day4;
    private Map<String, String> mCondition;
    /* access modifiers changed from: private */
    public List<Map<String, String>> mForecasts;
    /* access modifiers changed from: private */
    public ProgressBar mProgressBar;
    private String observationpoint;
    private String observationtime;
    private Context self = this;
    /* access modifiers changed from: private */
    public String skycode;
    /* access modifiers changed from: private */
    public String skytext;
    /* access modifiers changed from: private */
    public String temperature;
    /* access modifiers changed from: private */
    public TextView text2_current;
    /* access modifiers changed from: private */
    public TextView text_current;
    /* access modifiers changed from: private */
    public TextView text_day1;
    /* access modifiers changed from: private */
    public TextView text_day2;
    /* access modifiers changed from: private */
    public TextView text_day3;
    /* access modifiers changed from: private */
    public TextView text_day4;
    /* access modifiers changed from: private */
    public TextView title_day1;
    /* access modifiers changed from: private */
    public TextView title_day2;
    /* access modifiers changed from: private */
    public TextView title_day3;
    /* access modifiers changed from: private */
    public TextView title_day4;
    /* access modifiers changed from: private */
    public ImageView weather_current;
    /* access modifiers changed from: private */
    public ImageView weather_day1;
    /* access modifiers changed from: private */
    public ImageView weather_day2;
    /* access modifiers changed from: private */
    public ImageView weather_day3;
    /* access modifiers changed from: private */
    public ImageView weather_day4;
    /* access modifiers changed from: private */
    public String weatherlocationname;
    /* access modifiers changed from: private */
    public String windspeed;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        new RemoteViews(getApplicationContext().getPackageName(), (int) R.layout.countdownwidget);
        AdView adView = (AdView) findViewById(R.id.adView);
        if (adView != null) {
            adView.loadAd(new AdRequest());
        } else {
            finish();
        }
        this.text_current = (TextView) findViewById(R.id.text_current);
        this.text2_current = (TextView) findViewById(R.id.text2_current);
        this.weather_current = (ImageView) findViewById(R.id.weather_current);
        this.mProgressBar = (ProgressBar) findViewById(R.id.progress);
        this.ProgressBar1 = (ProgressBar) findViewById(R.id.progress1);
        this.ProgressBar2 = (ProgressBar) findViewById(R.id.progress2);
        this.ProgressBar3 = (ProgressBar) findViewById(R.id.progress3);
        this.ProgressBar4 = (ProgressBar) findViewById(R.id.progress4);
        this.weather_day1 = (ImageView) findViewById(R.id.weather_day1);
        this.title_day1 = (TextView) findViewById(R.id.title_day1);
        this.text_day1 = (TextView) findViewById(R.id.text_day1);
        this.high_day1 = (TextView) findViewById(R.id.high_day1);
        this.low_day1 = (TextView) findViewById(R.id.low_day1);
        this.title_day2 = (TextView) findViewById(R.id.title_day2);
        this.high_day2 = (TextView) findViewById(R.id.high_day2);
        this.low_day2 = (TextView) findViewById(R.id.low_day2);
        this.weather_day2 = (ImageView) findViewById(R.id.weather_day2);
        this.text_day2 = (TextView) findViewById(R.id.text_day2);
        this.title_day3 = (TextView) findViewById(R.id.title_day3);
        this.high_day3 = (TextView) findViewById(R.id.high_day3);
        this.low_day3 = (TextView) findViewById(R.id.low_day3);
        this.weather_day3 = (ImageView) findViewById(R.id.weather_day3);
        this.text_day3 = (TextView) findViewById(R.id.text_day3);
        this.title_day4 = (TextView) findViewById(R.id.title_day4);
        this.high_day4 = (TextView) findViewById(R.id.high_day4);
        this.low_day4 = (TextView) findViewById(R.id.low_day4);
        this.weather_day4 = (ImageView) findViewById(R.id.weather_day4);
        this.text_day4 = (TextView) findViewById(R.id.text_day4);
        Bundle bun = getIntent().getExtras();
        weathercode = bun.getString("weatherCode");
        wsymbol = bun.getString("wsymbol");
        this.appWidgetId = bun.getInt("appWidgetId");
        sunsetrise = bun.getLong("checkedsunsetrise");
        ((Button) findViewById(R.id.refreshforecast)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ForecastActivity.this.mProgressBar.setVisibility(0);
                ForecastActivity.this.ProgressBar1.setVisibility(0);
                ForecastActivity.this.ProgressBar2.setVisibility(0);
                ForecastActivity.this.ProgressBar3.setVisibility(0);
                ForecastActivity.this.ProgressBar4.setVisibility(0);
                ForecastActivity.this.weather_current.setVisibility(8);
                ForecastActivity.this.weather_day1.setVisibility(8);
                ForecastActivity.this.weather_day2.setVisibility(8);
                ForecastActivity.this.weather_day3.setVisibility(8);
                ForecastActivity.this.weather_day4.setVisibility(8);
                new Thread() {
                    public void run() {
                        ForecastActivity.this.parseWeather(ForecastActivity.weathercode, ForecastActivity.wsymbol);
                    }
                }.start();
            }
        });
        if (weathercode == null || weathercode == "WEATHER_CODE_EMPTY") {
            Toast.makeText(getApplicationContext(), "no weather code found", 0).show();
        } else {
            drawfirsttime();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    private static InputStream getWebWeatherSource(String code, String tempcode) throws IOException {
        return new URL("http://weather.msn.com/data.aspx?wealocations=wc:" + weathercode + "&weadegreetype=" + wsymbol).openStream();
    }

    private InputStream getLocalWeatherSource() throws IOException {
        return getAssets().open("beijing_msn.xml");
    }

    public String[] parseWeather(String code, final String tempcode) {
        this.mForecasts = new ArrayList();
        this.mCondition = new HashMap();
        InputStream is = null;
        try {
            is = getWebWeatherSource(weathercode, wsymbol);
            XmlPullParserFactory factory2 = XmlPullParserFactory.newInstance();
            factory2.setNamespaceAware(true);
            XmlPullParser xpp = factory2.newPullParser();
            xpp.setInput(is, "UTF-8");
            for (int eventType = xpp.getEventType(); eventType != 1; eventType = xpp.next()) {
                if (!(eventType == 0 || eventType == 1)) {
                    if (eventType == 2) {
                        String tagName = xpp.getName();
                        if ("weather".equals(tagName)) {
                            this.weatherlocationname = xpp.getAttributeValue(null, "weatherlocationname");
                            latitute = xpp.getAttributeValue(null, "lat");
                            longtitute = xpp.getAttributeValue(null, "long");
                            timezone = xpp.getAttributeValue(null, "timezone");
                        }
                        if ("current".equals(tagName)) {
                            this.temperature = xpp.getAttributeValue(null, "temperature");
                            this.skycode = xpp.getAttributeValue(null, "skycode");
                            this.skytext = xpp.getAttributeValue(null, "skytext");
                            this.date = xpp.getAttributeValue(null, "date");
                            this.observationtime = xpp.getAttributeValue(null, "observationtime");
                            this.observationpoint = xpp.getAttributeValue(null, "observationpoint");
                            this.feelslike = xpp.getAttributeValue(null, "feelslike");
                            this.humidity = xpp.getAttributeValue(null, "humidity");
                            this.windspeed = xpp.getAttributeValue(null, "winddisplay");
                        } else if ("forecast".equals(tagName)) {
                            Map<String, String> forecast = new HashMap<>();
                            forecast.put("low", xpp.getAttributeValue(null, "low"));
                            forecast.put("high", xpp.getAttributeValue(null, "high"));
                            forecast.put("skycodeday", xpp.getAttributeValue(null, "skycodeday"));
                            forecast.put("skytextday", xpp.getAttributeValue(null, "skytextday"));
                            forecast.put("date", xpp.getAttributeValue(null, "date"));
                            forecast.put("precip", xpp.getAttributeValue(null, "precip"));
                            forecast.put("shortday", xpp.getAttributeValue(null, "day"));
                            this.mForecasts.add(forecast);
                        }
                    } else if (eventType != 3) {
                    }
                }
            }
            runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        Map<String, String> day0 = (Map) ForecastActivity.this.mForecasts.get(0);
                        String d0_low = (String) day0.get("low");
                        String d0_high = (String) day0.get("high");
                        Map<String, String> day1 = (Map) ForecastActivity.this.mForecasts.get(1);
                        String d1_low = (String) day1.get("low");
                        String d1_high = (String) day1.get("high");
                        String d1_skycodeday = (String) day1.get("skycodeday");
                        String d1_skytextday = (String) day1.get("skytextday");
                        String d1_date = (String) day1.get("date");
                        String d1_precip = (String) day1.get("precip");
                        String d1_shortday = (String) day1.get("shortday");
                        Map<String, String> day2 = (Map) ForecastActivity.this.mForecasts.get(2);
                        String d2_low = (String) day2.get("low");
                        String d2_high = (String) day2.get("high");
                        String d2_skycodeday = (String) day2.get("skycodeday");
                        String d2_skytextday = (String) day2.get("skytextday");
                        String d2_date = (String) day2.get("date");
                        String d2_precip = (String) day2.get("precip");
                        String d2_shortday = (String) day2.get("shortday");
                        Map<String, String> day3 = (Map) ForecastActivity.this.mForecasts.get(3);
                        String d3_low = (String) day3.get("low");
                        String d3_high = (String) day3.get("high");
                        String d3_skycodeday = (String) day3.get("skycodeday");
                        String d3_skytextday = (String) day3.get("skytextday");
                        String d3_date = (String) day3.get("date");
                        String d3_precip = (String) day3.get("precip");
                        String d3_shortday = (String) day3.get("shortday");
                        Map<String, String> day4 = (Map) ForecastActivity.this.mForecasts.get(4);
                        String d4_low = (String) day4.get("low");
                        String d4_high = (String) day4.get("high");
                        String d4_skycodeday = (String) day4.get("skycodeday");
                        String d4_skytextday = (String) day4.get("skytextday");
                        String d4_date = (String) day4.get("date");
                        String d4_precip = (String) day4.get("precip");
                        String d4_shortday = (String) day4.get("shortday");
                        if (ForecastActivity.this.skycode.equals("44")) {
                            String[] g_arr = MainActivity_google.parseWeatherGoogle(ForecastActivity.this.weatherlocationname, tempcode);
                            ForecastActivity.this.skycode = g_arr[0];
                            ForecastActivity.this.skytext = g_arr[1];
                        }
                        if (ForecastActivity.sunsetrise == 1) {
                            if (iconsorter.SunSetRise(Double.valueOf(Double.parseDouble(ForecastActivity.latitute)).doubleValue(), Double.valueOf(Double.parseDouble(ForecastActivity.longtitute)).doubleValue()) == 0) {
                                ForecastActivity.this.skycode = Integer.toString(iconsorter.GetWeatherPic(Integer.parseInt(ForecastActivity.this.skycode)));
                                ForecastActivity.this.skytext = ForecastActivity.this.skytext.replaceAll("(?i)Sunny", "Clear");
                            }
                        }
                        ForecastActivity.this.mProgressBar.setVisibility(8);
                        ForecastActivity.this.ProgressBar1.setVisibility(8);
                        ForecastActivity.this.ProgressBar2.setVisibility(8);
                        ForecastActivity.this.ProgressBar3.setVisibility(8);
                        ForecastActivity.this.ProgressBar4.setVisibility(8);
                        ForecastActivity.this.text_current.setVisibility(0);
                        ForecastActivity.this.weather_current.setVisibility(0);
                        ForecastActivity.this.text_day1.setVisibility(0);
                        ForecastActivity.this.weather_day1.setVisibility(0);
                        ForecastActivity.this.text_day2.setVisibility(0);
                        ForecastActivity.this.weather_day2.setVisibility(0);
                        ForecastActivity.this.text_day3.setVisibility(0);
                        ForecastActivity.this.weather_day3.setVisibility(0);
                        ForecastActivity.this.text_day4.setVisibility(0);
                        ForecastActivity.this.weather_day4.setVisibility(0);
                        ForecastActivity.this.weather_current.setImageResource(ForecastActivity.this.imgSizeIds[Integer.parseInt(ForecastActivity.this.skycode)]);
                        ForecastActivity.this.text_current.setText(String.valueOf(ForecastActivity.this.weatherlocationname) + "\n" + ForecastActivity.this.skytext + " " + ForecastActivity.this.temperature + "°");
                        ForecastActivity.this.text2_current.setText("Feels like: " + ForecastActivity.this.feelslike + "°" + "\nHumidity: " + ForecastActivity.this.humidity + "%" + "\nWind: " + ForecastActivity.this.windspeed);
                        ForecastActivity.this.title_day1.setText(d1_shortday);
                        ForecastActivity.this.text_day1.setText(String.valueOf(d1_skytextday) + "\n" + d1_precip + "% precipitation");
                        ForecastActivity.this.high_day1.setText("Hi: " + d1_high + "°");
                        ForecastActivity.this.low_day1.setText("Lo: " + d1_low + "°");
                        if (d1_skycodeday.equals(null) || ForecastActivity.this.skycode.equals("")) {
                            d1_skycodeday = "44";
                        }
                        ForecastActivity.this.weather_day1.setImageResource(ForecastActivity.this.imgSizeIds[Integer.parseInt(d1_skycodeday)]);
                        ForecastActivity.this.title_day2.setText(d2_shortday);
                        ForecastActivity.this.text_day2.setText(String.valueOf(d2_skytextday) + "\n" + d2_precip + "% precipitation");
                        ForecastActivity.this.high_day2.setText("Hi: " + d2_high + "°");
                        ForecastActivity.this.low_day2.setText("Lo: " + d2_low + "°");
                        if (d2_skycodeday.equals(null) || ForecastActivity.this.skycode.equals("")) {
                            d2_skycodeday = "44";
                        }
                        ForecastActivity.this.weather_day2.setImageResource(ForecastActivity.this.imgSizeIds[Integer.parseInt(d2_skycodeday)]);
                        ForecastActivity.this.title_day3.setText(d3_shortday);
                        ForecastActivity.this.text_day3.setText(String.valueOf(d3_skytextday) + "\n" + d3_precip + "% precipitation");
                        ForecastActivity.this.high_day3.setText("Hi: " + d3_high + "°");
                        ForecastActivity.this.low_day3.setText("Lo: " + d3_low + "°");
                        if (d3_skycodeday.equals(null) || ForecastActivity.this.skycode.equals("")) {
                            d3_skycodeday = "44";
                        }
                        ForecastActivity.this.weather_day3.setImageResource(ForecastActivity.this.imgSizeIds[Integer.parseInt(d3_skycodeday)]);
                        ForecastActivity.this.title_day4.setText(d4_shortday);
                        ForecastActivity.this.text_day4.setText(String.valueOf(d4_skytextday) + "\n" + d4_precip + "% precipitation");
                        ForecastActivity.this.high_day4.setText("Hi: " + d4_high + "°");
                        ForecastActivity.this.low_day4.setText("Lo: " + d4_low + "°");
                        if (d4_skycodeday.equals(null) || ForecastActivity.this.skycode.equals("")) {
                            d4_skycodeday = "44";
                        }
                        ForecastActivity.this.weather_day4.setImageResource(ForecastActivity.this.imgSizeIds[Integer.parseInt(d4_skycodeday)]);
                        CountdownService.setWf(new String[]{ForecastActivity.this.temperature, ForecastActivity.this.skycode, ForecastActivity.this.skytext, ForecastActivity.this.humidity, ForecastActivity.this.windspeed, ForecastActivity.this.weatherlocationname, ForecastActivity.this.feelslike, d1_low, d1_high, d1_skycodeday, d1_skytextday, d1_date, d1_precip, d1_shortday, d2_low, d2_high, d2_skycodeday, d2_skytextday, d2_date, d2_precip, d2_shortday, d3_low, d3_high, d3_skycodeday, d3_skytextday, d3_date, d3_precip, d3_shortday, d4_low, d4_high, d4_skycodeday, d4_skytextday, d4_date, d4_precip, d4_shortday, d0_low, d0_high, ForecastActivity.latitute, ForecastActivity.longtitute});
                        CountdownService.setMissedit(1);
                        try {
                            CountdownWidget.makeControlPendingIntent(ForecastActivity.this.getBaseContext(), CountdownService.UPDATE, ForecastActivity.this.appWidgetId).send();
                        } catch (PendingIntent.CanceledException e) {
                            e.printStackTrace();
                        }
                    } catch (ArrayIndexOutOfBoundsException e2) {
                    } catch (IndexOutOfBoundsException e3) {
                        e3.printStackTrace();
                    }
                }
            });
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e22) {
                    e22.printStackTrace();
                }
            }
        } catch (XmlPullParserException e3) {
            e3.printStackTrace();
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e23) {
                    e23.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e24) {
                    e24.printStackTrace();
                }
            }
            throw th;
        }
        return null;
    }

    private void drawfirsttime() {
        String[] getfromservice = CountdownService.getWf();
        try {
            String temperature2 = getfromservice[0];
            String skycode2 = getfromservice[1];
            String skytext2 = getfromservice[2];
            String humidity2 = getfromservice[3];
            String windspeed2 = getfromservice[4];
            String weatherlocationname2 = getfromservice[5];
            String feelslike2 = getfromservice[6];
            String d1_low = getfromservice[7];
            String d1_high = getfromservice[8];
            String d1_skycodeday = getfromservice[9];
            String d1_skytextday = getfromservice[10];
            String str = getfromservice[11];
            String d1_precip = getfromservice[12];
            String d1_shortday = getfromservice[13];
            String d2_low = getfromservice[14];
            String d2_high = getfromservice[15];
            String d2_skycodeday = getfromservice[16];
            String d2_skytextday = getfromservice[17];
            String str2 = getfromservice[18];
            String d2_precip = getfromservice[19];
            String d2_shortday = getfromservice[20];
            String d3_low = getfromservice[21];
            String d3_high = getfromservice[22];
            String d3_skycodeday = getfromservice[23];
            String d3_skytextday = getfromservice[24];
            String str3 = getfromservice[25];
            String d3_precip = getfromservice[26];
            String d3_shortday = getfromservice[27];
            String d4_low = getfromservice[28];
            String d4_high = getfromservice[29];
            String d4_skycodeday = getfromservice[30];
            String d4_skytextday = getfromservice[31];
            String str4 = getfromservice[32];
            String d4_precip = getfromservice[33];
            String d4_shortday = getfromservice[34];
            this.mProgressBar.setVisibility(8);
            this.ProgressBar1.setVisibility(8);
            this.ProgressBar2.setVisibility(8);
            this.ProgressBar3.setVisibility(8);
            this.ProgressBar4.setVisibility(8);
            this.text_current.setVisibility(0);
            this.weather_current.setVisibility(0);
            this.text_day1.setVisibility(0);
            this.weather_day1.setVisibility(0);
            this.text_day2.setVisibility(0);
            this.weather_day2.setVisibility(0);
            this.text_day3.setVisibility(0);
            this.weather_day3.setVisibility(0);
            this.text_day4.setVisibility(0);
            this.weather_day4.setVisibility(0);
            if (skycode2.equals(null) || skycode2.equals("")) {
                skycode2 = "44";
            }
            this.weather_current.setImageResource(this.imgSizeIds[Integer.parseInt(skycode2)]);
            this.text_current.setText(String.valueOf(weatherlocationname2) + "\n" + skytext2 + " " + temperature2 + "°");
            this.text2_current.setText("Feels like: " + feelslike2 + "°" + "\nHumidity: " + humidity2 + "%" + "\nWind: " + windspeed2);
            this.title_day1.setText(d1_shortday);
            this.text_day1.setText(String.valueOf(d1_skytextday) + "\n" + d1_precip + "% precipitation");
            this.high_day1.setText("Hi: " + d1_high + "°");
            this.low_day1.setText("Lo: " + d1_low + "°");
            if (d1_skycodeday.equals(null) || d1_skycodeday.equals("")) {
                d1_skycodeday = "44";
            }
            this.weather_day1.setImageResource(this.imgSizeIds[Integer.parseInt(d1_skycodeday)]);
            this.title_day2.setText(d2_shortday);
            this.text_day2.setText(String.valueOf(d2_skytextday) + "\n" + d2_precip + "% precipitation");
            this.high_day2.setText("Hi: " + d2_high + "°");
            this.low_day2.setText("Lo: " + d2_low + "°");
            if (d2_skycodeday.equals(null) || d2_skycodeday.equals("")) {
                d2_skycodeday = "44";
            }
            this.weather_day2.setImageResource(this.imgSizeIds[Integer.parseInt(d2_skycodeday)]);
            this.title_day3.setText(d3_shortday);
            this.text_day3.setText(String.valueOf(d3_skytextday) + "\n" + d3_precip + "% precipitation");
            this.high_day3.setText("Hi: " + d3_high + "°");
            this.low_day3.setText("Lo: " + d3_low + "°");
            if (d3_skycodeday.equals(null) || d3_skycodeday.equals("")) {
                d3_skycodeday = "44";
            }
            this.weather_day3.setImageResource(this.imgSizeIds[Integer.parseInt(d3_skycodeday)]);
            this.title_day4.setText(d4_shortday);
            this.text_day4.setText(String.valueOf(d4_skytextday) + "\n" + d4_precip + "% precipitation");
            this.high_day4.setText("Hi: " + d4_high + "°");
            this.low_day4.setText("Lo: " + d4_low + "°");
            if (d4_skycodeday.equals(null) || d4_skycodeday.equals("")) {
                d4_skycodeday = "44";
            }
            this.weather_day4.setImageResource(this.imgSizeIds[Integer.parseInt(d4_skycodeday)]);
        } catch (ArrayIndexOutOfBoundsException e) {
        }
    }

    public static String[] stringToArray(String splitme) throws Exception {
        return splitme.split("=");
    }

    public void onFailedToReceiveAd(AdView adView) {
        Log.d("AdListener", "onFailedToReceiveAd");
    }

    public void onFailedToReceiveRefreshedAd(AdView adView) {
        Log.d("AdListener", "onFailedToReceiveRefreshedAd");
    }

    public void onReceiveAd(AdView adView) {
        Log.d("AdListener", "onReceiveAd");
    }

    public void onReceiveRefreshedAd(AdView adView) {
        Log.d("AdListener", "onReceiveRefreshedAd");
    }
}
