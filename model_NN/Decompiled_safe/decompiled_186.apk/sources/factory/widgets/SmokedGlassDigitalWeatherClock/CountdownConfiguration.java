package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import factory.widgets.SmokedGlassDigitalWeatherClock.UberColorPickerDialog;
import java.io.File;

public class CountdownConfiguration extends Activity implements View.OnClickListener, UberColorPickerDialog.OnColorChangedListener {
    protected static final int STATIC_INTEGER_VALUE = 0;
    private static int selection = 0;
    /* access modifiers changed from: private */
    public int appWidgetId;
    private TextView col1;
    private TextView col2;
    private TextView col3;
    private long colorother;
    private long colwheelhr;
    private long colwheelmin;
    private int mColor = -12566464;
    /* access modifiers changed from: private */
    public Context self = this;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configuration);
        Bundle config = getIntent().getExtras();
        CheckBox check1 = (CheckBox) findViewById(R.id.check1);
        if (Long.valueOf(config.getLong("checked")).longValue() == 1) {
            check1.setChecked(true);
        } else {
            check1.setChecked(false);
        }
        CheckBox check2 = (CheckBox) findViewById(R.id.checksys);
        if (Long.valueOf(config.getLong("checkedsys")).longValue() == 1) {
            check2.setChecked(true);
        } else {
            check2.setChecked(false);
        }
        CheckBox check4 = (CheckBox) findViewById(R.id.checkbattempf);
        if (Long.valueOf(config.getLong("checkedbattempf")).longValue() == 1) {
            check4.setChecked(true);
        } else {
            check4.setChecked(false);
        }
        CheckBox check5 = (CheckBox) findViewById(R.id.checknextalarm);
        if (Long.valueOf(config.getLong("checkednextalarm")).longValue() == 1) {
            check5.setChecked(true);
        } else {
            check5.setChecked(false);
        }
        CheckBox check6 = (CheckBox) findViewById(R.id.checkmoonphase);
        if (Long.valueOf(config.getLong("checkedmoonphase")).longValue() == 1) {
            check6.setChecked(true);
        } else {
            check6.setChecked(false);
        }
        CheckBox check7 = (CheckBox) findViewById(R.id.checkweatherenable);
        if (Long.valueOf(config.getLong("checkedweatherenabled")).longValue() == 1) {
            check7.setChecked(true);
        } else {
            check7.setChecked(false);
        }
        CheckBox check8 = (CheckBox) findViewById(R.id.checkweathertempf);
        if (Long.valueOf(config.getLong("checkedweathertempf")).longValue() == 1) {
            check8.setChecked(true);
        } else {
            check8.setChecked(false);
        }
        CheckBox check9 = (CheckBox) findViewById(R.id.noweatherdetail);
        if (Long.valueOf(config.getLong("checkednoweatherdetail")).longValue() == 1) {
            check9.setChecked(true);
        } else {
            check9.setChecked(false);
        }
        CheckBox check10 = (CheckBox) findViewById(R.id.weather_bottom);
        if (Long.valueOf(config.getLong("checkedweatherbottom")).longValue() == 1) {
            check10.setChecked(true);
        } else {
            check10.setChecked(false);
        }
        CheckBox check11 = (CheckBox) findViewById(R.id.checknoskin);
        if (Long.valueOf(config.getLong("checkednoskin")).longValue() == 1) {
            check11.setChecked(true);
        } else {
            check11.setChecked(false);
        }
        CheckBox check12 = (CheckBox) findViewById(R.id.checksamecolor);
        if (Long.valueOf(config.getLong("checkedsamecolor")).longValue() == 1) {
            check12.setChecked(true);
        } else {
            check12.setChecked(false);
        }
        CheckBox check13 = (CheckBox) findViewById(R.id.forecastalter);
        if (Long.valueOf(config.getLong("forecastalter")).longValue() == 1) {
            check13.setChecked(true);
        } else {
            check13.setChecked(false);
        }
        CheckBox check14 = (CheckBox) findViewById(R.id.sunsetrise);
        if (Long.valueOf(config.getLong("checkedsunsetrise")).longValue() == 1) {
            check14.setChecked(true);
        } else {
            check14.setChecked(false);
        }
        CheckBox check15 = (CheckBox) findViewById(R.id.checkbathrstab);
        if (Long.valueOf(config.getLong("checkedbathrstab")).longValue() == 1) {
            check15.setChecked(true);
        } else {
            check15.setChecked(false);
        }
        this.colwheelhr = config.getLong("colwheelhr");
        this.colwheelmin = config.getLong("colwheelmin");
        this.colorother = config.getLong("colorother");
        EditText weatherupdate = (EditText) findViewById(R.id.EditTextUpdateInterval);
        try {
            weatherupdate.setText(String.valueOf(config.getInt("weatherUpdatedtimer")));
        } catch (NullPointerException e) {
            weatherupdate.setText("");
            e.printStackTrace();
        }
        this.col1 = (TextView) findViewById(R.id.TextCol01);
        this.col2 = (TextView) findViewById(R.id.TextCol02);
        this.col3 = (TextView) findViewById(R.id.TextCol03);
        this.appWidgetId = getIntent().getExtras().getInt("appWidgetId", 0);
        Intent cancelResultValue = new Intent();
        cancelResultValue.putExtra("appWidgetId", this.appWidgetId);
        setResult(0, cancelResultValue);
        this.col1.setBackgroundColor((int) this.colwheelhr);
        this.col2.setBackgroundColor((int) this.colwheelmin);
        this.col3.setBackgroundColor((int) this.colorother);
        float[] hsv = new float[3];
        Color.colorToHSV(this.mColor, hsv);
        if (UberColorPickerDialog.isGray(this.mColor)) {
            hsv[1] = 0.0f;
        }
        if (((double) hsv[2]) < 0.5d) {
            this.col1.setTextColor(-1);
            this.col2.setTextColor(-1);
            this.col3.setTextColor(-1);
        } else {
            this.col1.setTextColor(-16777216);
            this.col2.setTextColor(-16777216);
            this.col3.setTextColor(-16777216);
        }
        ((Button) findViewById(R.id.Button01)).setOnClickListener(this);
        ((Button) findViewById(R.id.Button02)).setOnClickListener(this);
        ((Button) findViewById(R.id.Button03)).setOnClickListener(this);
        ((Button) findViewById(R.id.okbutton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String weatherCode;
                SharedPreferences prefs = CountdownConfiguration.this.self.getSharedPreferences("prefs", 0);
                SharedPreferences.Editor edit = prefs.edit();
                edit.commit();
                if (((CheckBox) CountdownConfiguration.this.findViewById(R.id.check1)).isChecked()) {
                    edit.putLong("checked" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checked" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                if (((CheckBox) CountdownConfiguration.this.findViewById(R.id.checksys)).isChecked()) {
                    edit.putLong("checkedsys" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checkedsys" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                if (((CheckBox) CountdownConfiguration.this.findViewById(R.id.checkbattempf)).isChecked()) {
                    edit.putLong("checkedbattempf" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checkedbattempf" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                if (((CheckBox) CountdownConfiguration.this.findViewById(R.id.checknextalarm)).isChecked()) {
                    edit.putLong("checkednextalarm" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checkednextalarm" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                if (((CheckBox) CountdownConfiguration.this.findViewById(R.id.checkmoonphase)).isChecked()) {
                    edit.putLong("checkedmoonphase" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checkedmoonphase" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                CheckBox check7 = (CheckBox) CountdownConfiguration.this.findViewById(R.id.checkweatherenable);
                if (check7.isChecked()) {
                    edit.putLong("checkedweatherenabled" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checkedweatherenabled" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                if (((CheckBox) CountdownConfiguration.this.findViewById(R.id.checkweathertempf)).isChecked()) {
                    edit.putLong("checkedweathertempf" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checkedweathertempf" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                if (((CheckBox) CountdownConfiguration.this.findViewById(R.id.noweatherdetail)).isChecked()) {
                    edit.putLong("checkednoweatherdetail" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checkednoweatherdetail" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                if (((CheckBox) CountdownConfiguration.this.findViewById(R.id.weather_bottom)).isChecked()) {
                    edit.putLong("checkedweatherbottom" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checkedweatherbottom" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                if (((CheckBox) CountdownConfiguration.this.findViewById(R.id.checknoskin)).isChecked()) {
                    edit.putLong("checkednoskin" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checkednoskin" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                if (((CheckBox) CountdownConfiguration.this.findViewById(R.id.checksamecolor)).isChecked()) {
                    edit.putLong("checkedsamecolor" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checkedsamecolor" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                if (((CheckBox) CountdownConfiguration.this.findViewById(R.id.forecastalter)).isChecked()) {
                    edit.putLong("checkedforecastalter" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checkedforecastalter" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                if (((CheckBox) CountdownConfiguration.this.findViewById(R.id.sunsetrise)).isChecked()) {
                    edit.putLong("checkedsunsetrise" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checkedsunsetrise" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                if (((CheckBox) CountdownConfiguration.this.findViewById(R.id.checkbathrstab)).isChecked()) {
                    edit.putLong("checkedbathrstab" + CountdownConfiguration.this.appWidgetId, 1);
                    edit.commit();
                } else {
                    edit.putLong("checkedbathrstab" + CountdownConfiguration.this.appWidgetId, 0);
                    edit.commit();
                }
                CountdownService.setMissedit(1);
                try {
                    EditText weatherupdate = (EditText) CountdownConfiguration.this.findViewById(R.id.EditTextUpdateInterval);
                    String weatherupdaterate = weatherupdate.getText().toString();
                    int weatherupdateratenum = Integer.parseInt(weatherupdate.getText().toString());
                    if (weatherupdaterate.equals("") || weatherupdaterate.equals(null) || (weatherupdateratenum == 0 && check7.isChecked())) {
                        Toast.makeText(CountdownConfiguration.this.getApplicationContext(), "no weather update frequency entered, setting 15 mins default", 1).show();
                        edit.putInt("weatherUpdatedtimer" + CountdownConfiguration.this.appWidgetId, 15);
                        edit.commit();
                        weatherCode = prefs.getString("weatherCode" + CountdownConfiguration.this.appWidgetId, "WEATHER_CODE_EMPTY");
                        if (weatherCode != null || weatherCode == "" || (weatherCode == "WEATHER_CODE_EMPTY" && check7.isChecked())) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(CountdownConfiguration.this.self);
                            builder.setCancelable(true);
                            builder.setMessage("You have enabled weather but did not setup a weather location.Press the button \"set weather location\"");
                            builder.setInverseBackgroundForced(true);
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builder.create().show();
                        }
                        try {
                            CountdownWidget.makeControlPendingIntent(CountdownConfiguration.this.self, CountdownService.UPDATE, CountdownConfiguration.this.appWidgetId).send();
                        } catch (PendingIntent.CanceledException e) {
                            e.printStackTrace();
                        }
                        Intent resultValue = new Intent();
                        resultValue.putExtra("appWidgetId", CountdownConfiguration.this.appWidgetId);
                        CountdownConfiguration.this.setResult(-1, resultValue);
                        CountdownConfiguration.this.finish();
                        return;
                    }
                    edit.putInt("weatherUpdatedtimer" + CountdownConfiguration.this.appWidgetId, Integer.parseInt(weatherupdate.getText().toString()));
                    edit.commit();
                    weatherCode = prefs.getString("weatherCode" + CountdownConfiguration.this.appWidgetId, "WEATHER_CODE_EMPTY");
                    if (weatherCode != null) {
                    }
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(CountdownConfiguration.this.self);
                    builder2.setCancelable(true);
                    builder2.setMessage("You have enabled weather but did not setup a weather location.Press the button \"set weather location\"");
                    builder2.setInverseBackgroundForced(true);
                    builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder2.create().show();
                } catch (NumberFormatException e2) {
                    System.out.println("could not parse" + e2);
                }
            }
        });
        ((Button) findViewById(R.id.cancelbutton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CountdownConfiguration.this.finish();
            }
        });
        ((Button) findViewById(R.id.btlauncher)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent applauncher = new Intent(CountdownConfiguration.this.getBaseContext(), AppLauncher.class);
                applauncher.setFlags(268435456);
                applauncher.putExtra("appWidgetId", CountdownConfiguration.this.appWidgetId);
                CountdownConfiguration.this.startActivity(applauncher);
            }
        });
        ((Button) findViewById(R.id.btlauncherMins)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent applauncher = new Intent(CountdownConfiguration.this.getBaseContext(), AppLauncherMins.class);
                applauncher.setFlags(268435456);
                applauncher.putExtra("appWidgetId", CountdownConfiguration.this.appWidgetId);
                CountdownConfiguration.this.startActivity(applauncher);
            }
        });
        ((Button) findViewById(R.id.themebutton)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent skinchooser = new Intent(CountdownConfiguration.this.getBaseContext(), HelloGallery.class);
                skinchooser.setFlags(268435456);
                skinchooser.putExtra("appWidgetId", CountdownConfiguration.this.appWidgetId);
                CountdownConfiguration.this.startActivity(skinchooser);
            }
        });
        ((Button) findViewById(R.id.skindownloader)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new File(Environment.getExternalStorageDirectory() + "/factory.widgets.skins/large/.downloadConfig").delete();
                Intent skindowloader = new Intent(CountdownConfiguration.this.getBaseContext(), DownloaderTest.class);
                skindowloader.setFlags(268435456);
                skindowloader.putExtra("appWidgetId", CountdownConfiguration.this.appWidgetId);
                CountdownConfiguration.this.startActivity(skindowloader);
            }
        });
        ((Button) findViewById(R.id.lazydownloader)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent skindowloader = new Intent(CountdownConfiguration.this.getBaseContext(), LazyLoadMainActivity.class);
                skindowloader.setFlags(268435456);
                skindowloader.putExtra("appWidgetId", CountdownConfiguration.this.appWidgetId);
                CountdownConfiguration.this.startActivity(skindowloader);
            }
        });
        final Bundle bundle = config;
        ((Button) findViewById(R.id.btnweatherlocation)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle weatherconfig = new Bundle();
                weatherconfig.putString("weatherCode", bundle.getString("weatherCode"));
                weatherconfig.putString("weatherName", bundle.getString("weatherName"));
                Intent weatherlocator = new Intent(CountdownConfiguration.this.getBaseContext(), FindWeatherLocation.class);
                weatherlocator.setFlags(268435456);
                weatherlocator.putExtra("appWidgetId", CountdownConfiguration.this.appWidgetId);
                weatherlocator.putExtras(weatherconfig);
                CountdownConfiguration.this.startActivity(weatherlocator);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v) {
        if (v.getId() == R.id.Button01) {
            selection = 1;
            new UberColorPickerDialog(this, this, (int) this.colwheelhr, true).show();
        }
        if (v.getId() == R.id.Button02) {
            selection = 2;
            new UberColorPickerDialog(this, this, (int) this.colwheelmin, true).show();
        }
        if (v.getId() == R.id.Button03) {
            selection = 3;
            new UberColorPickerDialog(this, this, (int) this.colorother, true).show();
        }
    }

    public void colorChanged(int color) {
        if (selection == 1) {
            TextView textView = this.col1;
            this.mColor = color;
            textView.setBackgroundColor(color);
            SharedPreferences.Editor edit = this.self.getSharedPreferences("prefs", 0).edit();
            edit.putInt("colorhr" + this.appWidgetId, color);
            edit.commit();
            float[] hsv = new float[3];
            Color.colorToHSV(this.mColor, hsv);
            if (((double) hsv[2]) < 0.5d) {
                this.col1.setTextColor(-1);
            } else {
                this.col1.setTextColor(-16777216);
            }
        }
        if (selection == 2) {
            TextView textView2 = this.col2;
            this.mColor = color;
            textView2.setBackgroundColor(color);
            SharedPreferences.Editor edit2 = this.self.getSharedPreferences("prefs", 0).edit();
            edit2.putInt("colormin" + this.appWidgetId, color);
            edit2.commit();
            float[] hsv2 = new float[3];
            Color.colorToHSV(this.mColor, hsv2);
            if (((double) hsv2[2]) < 0.5d) {
                this.col2.setTextColor(-1);
            } else {
                this.col2.setTextColor(-16777216);
            }
        }
        if (selection == 3) {
            TextView textView3 = this.col3;
            this.mColor = color;
            textView3.setBackgroundColor(color);
            SharedPreferences.Editor edit3 = this.self.getSharedPreferences("prefs", 0).edit();
            edit3.putInt("colorother" + this.appWidgetId, color);
            edit3.commit();
            float[] hsv3 = new float[3];
            Color.colorToHSV(this.mColor, hsv3);
            if (((double) hsv3[2]) < 0.5d) {
                this.col3.setTextColor(-1);
            } else {
                this.col3.setTextColor(-16777216);
            }
        }
    }
}
