package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.content.Context;
import android.net.ConnectivityManager;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class NetChecker {
    private static Context ctx;

    public enum NetStatus {
        NONET,
        FAULTYURL,
        NETOK,
        CONN_MISSING
    }

    public static boolean isOnline() {
        if (((ConnectivityManager) ctx.getSystemService("connectivity")).getActiveNetworkInfo() == null) {
            return false;
        }
        return true;
    }

    public static NetStatus checkNet() {
        if (!isOnline()) {
            return NetStatus.CONN_MISSING;
        }
        try {
            HttpURLConnection urlc = (HttpURLConnection) new URL("http://weather.msn.com").openConnection();
            urlc.setConnectTimeout(5000);
            urlc.connect();
            if (urlc.getResponseCode() == 200) {
                return NetStatus.NETOK;
            }
            return NetStatus.NETOK;
        } catch (MalformedURLException e) {
            return NetStatus.FAULTYURL;
        } catch (IOException e2) {
            return NetStatus.NONET;
        }
    }
}
