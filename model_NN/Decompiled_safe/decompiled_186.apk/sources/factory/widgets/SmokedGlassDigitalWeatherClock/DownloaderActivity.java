package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class DownloaderActivity extends Activity {
    private static final String EXTRA_CONFIG_VERSION = "DownloaderActivity_config_version";
    private static final String EXTRA_CUSTOM_TEXT = "DownloaderActivity_custom_text";
    private static final String EXTRA_DATA_PATH = "DownloaderActivity_data_path";
    private static final String EXTRA_FILE_CONFIG_URL = "DownloaderActivity_config_url";
    private static final String EXTRA_USER_AGENT = "DownloaderActivity_user_agent";
    private static final String LOCAL_CONFIG_FILE = ".downloadConfig";
    private static final String LOCAL_CONFIG_FILE_TEMP = ".downloadConfig_temp";
    private static final String LOCAL_FILTERED_FILE = ".downloadConfig_filtered";
    private static final String LOG_TAG = "Downloader";
    private static final int MSG_DOWNLOAD_FAILED = 1;
    private static final int MSG_DOWNLOAD_SUCCEEDED = 0;
    private static final int MSG_REPORT_PROGRESS = 2;
    private static final int MSG_REPORT_VERIFYING = 3;
    private static final long MS_PER_DAY = 86400000;
    private static final long MS_PER_HOUR = 3600000;
    private static final long MS_PER_MINUTE = 60000;
    private static final long MS_PER_SECOND = 1000;
    /* access modifiers changed from: private */
    public Thread mDownloadThread;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        /* Debug info: failed to restart local var, previous not found, register: 3 */
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    DownloaderActivity.this.onDownloadSucceeded();
                    return;
                case 1:
                    DownloaderActivity.this.onDownloadFailed((String) msg.obj);
                    return;
                case 2:
                    DownloaderActivity.this.onReportProgress(msg.arg1);
                    return;
                case 3:
                    DownloaderActivity.this.onReportVerifying();
                    return;
                default:
                    throw new IllegalArgumentException("Unknown message id " + msg.what);
            }
        }
    };
    private final DecimalFormat mPercentFormat = new DecimalFormat("0.00 %");
    private TextView mProgress;
    private long mStartTime;
    /* access modifiers changed from: private */
    public boolean mSuppressErrorMessages;
    private TextView mTimeRemaining;

    public static boolean ensureDownloaded(Activity activity, String customText, String fileConfigUrl, String configVersion, String dataPath, String userAgent) {
        File dest = new File(dataPath);
        if (!dest.exists() || !versionMatches(dest, configVersion)) {
            Intent intent = PreconditionActivityHelper.createPreconditionIntent(activity, DownloaderActivity.class);
            intent.putExtra(EXTRA_CUSTOM_TEXT, customText);
            intent.putExtra(EXTRA_FILE_CONFIG_URL, fileConfigUrl);
            intent.putExtra(EXTRA_CONFIG_VERSION, configVersion);
            intent.putExtra(EXTRA_DATA_PATH, dataPath);
            intent.putExtra(EXTRA_USER_AGENT, userAgent);
            PreconditionActivityHelper.startPreconditionActivityAndFinish(activity, intent);
            return false;
        }
        Log.i(LOG_TAG, "Versions match, no need to download.");
        return true;
    }

    public static boolean deleteData(String directory) {
        return deleteTree(new File(directory), true);
    }

    private static boolean deleteTree(File base, boolean deleteBase) {
        boolean result = true;
        if (base.isDirectory()) {
            for (File child : base.listFiles()) {
                result &= deleteTree(child, true);
            }
        }
        if (deleteBase) {
            return result & base.delete();
        }
        return result;
    }

    private static boolean versionMatches(File dest, String expectedVersion) {
        Config config = getLocalConfig(dest, LOCAL_CONFIG_FILE);
        if (config != null) {
            return config.version.equals(expectedVersion);
        }
        return false;
    }

    /* access modifiers changed from: private */
    public static Config getLocalConfig(File destPath, String configFilename) {
        try {
            FileInputStream is = new FileInputStream(new File(destPath, configFilename));
            try {
                Config config = ConfigHandler.parse(is);
                quietClose(is);
                return config;
            } catch (Exception e) {
                Log.e(LOG_TAG, "Unable to read local config file", e);
                quietClose(is);
                return null;
            } catch (Throwable th) {
                quietClose(is);
                throw th;
            }
        } catch (FileNotFoundException e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        requestWindowFeature(7);
        setContentView((int) R.layout.downloader);
        getWindow().setFeatureInt(7, R.layout.downloader_title);
        ((TextView) findViewById(R.id.customText)).setText(intent.getStringExtra(EXTRA_CUSTOM_TEXT));
        this.mProgress = (TextView) findViewById(R.id.progress);
        this.mTimeRemaining = (TextView) findViewById(R.id.time_remaining);
        ((Button) findViewById(R.id.cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (DownloaderActivity.this.mDownloadThread != null) {
                    DownloaderActivity.this.mSuppressErrorMessages = true;
                    DownloaderActivity.this.mDownloadThread.interrupt();
                }
            }
        });
        startDownloadThread();
    }

    /* access modifiers changed from: private */
    public void startDownloadThread() {
        this.mSuppressErrorMessages = false;
        this.mProgress.setText("");
        this.mTimeRemaining.setText("");
        this.mDownloadThread = new Thread(new Downloader(this, null), LOG_TAG);
        this.mDownloadThread.setPriority(4);
        this.mDownloadThread.start();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mSuppressErrorMessages = true;
        this.mDownloadThread.interrupt();
        try {
            this.mDownloadThread.join();
        } catch (InterruptedException e) {
        }
    }

    /* access modifiers changed from: private */
    public void onDownloadSucceeded() {
        Log.i(LOG_TAG, "Download succeeded");
        PreconditionActivityHelper.startOriginalActivityAndFinish(this);
    }

    /* access modifiers changed from: private */
    public void onDownloadFailed(String reason) {
        String shortReason;
        Log.e(LOG_TAG, "Download stopped: " + reason);
        int index = reason.indexOf(10);
        if (index >= 0) {
            shortReason = reason.substring(0, index);
        } else {
            shortReason = reason;
        }
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle((int) R.string.download_activity_download_stopped);
        if (!this.mSuppressErrorMessages) {
            alert.setMessage(shortReason);
        }
        alert.setButton(getString(R.string.download_activity_retry), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                DownloaderActivity.this.startDownloadThread();
            }
        });
        alert.setButton2(getString(R.string.download_activity_quit), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                DownloaderActivity.this.finish();
            }
        });
        try {
            alert.show();
        } catch (WindowManager.BadTokenException e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: private */
    public void onReportProgress(int progress) {
        this.mProgress.setText(this.mPercentFormat.format(((double) progress) / 10000.0d));
        long now = SystemClock.elapsedRealtime();
        if (this.mStartTime == 0) {
            this.mStartTime = now;
        }
        long delta = now - this.mStartTime;
        String timeRemaining = getString(R.string.download_activity_time_remaining_unknown);
        if (delta > 3000 && progress > 100) {
            long timeLeft = Math.max(0L, ((10000 * delta) / ((long) progress)) - delta);
            timeRemaining = timeLeft > 86400000 ? String.valueOf(Long.toString(((86400000 + timeLeft) - 1) / 86400000)) + " " + getString(R.string.download_activity_time_remaining_days) : timeLeft > 3600000 ? String.valueOf(Long.toString(((3600000 + timeLeft) - 1) / 3600000)) + " " + getString(R.string.download_activity_time_remaining_hours) : timeLeft > MS_PER_MINUTE ? String.valueOf(Long.toString(((MS_PER_MINUTE + timeLeft) - 1) / MS_PER_MINUTE)) + " " + getString(R.string.download_activity_time_remaining_minutes) : String.valueOf(Long.toString(((MS_PER_SECOND + timeLeft) - 1) / MS_PER_SECOND)) + " " + getString(R.string.download_activity_time_remaining_seconds);
        }
        this.mTimeRemaining.setText(timeRemaining);
    }

    /* access modifiers changed from: private */
    public void onReportVerifying() {
        this.mProgress.setText(getString(R.string.download_activity_verifying));
        this.mTimeRemaining.setText("");
    }

    /* access modifiers changed from: private */
    public static void quietClose(InputStream is) {
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
    }

    /* access modifiers changed from: private */
    public static void quietClose(OutputStream os) {
        if (os != null) {
            try {
                os.close();
            } catch (IOException e) {
            }
        }
    }

    private static class Config {
        ArrayList<File> mFiles;
        String version;

        private Config() {
            this.mFiles = new ArrayList<>();
        }

        /* synthetic */ Config(Config config) {
            this();
        }

        /* access modifiers changed from: package-private */
        public long getSize() {
            long result = 0;
            Iterator<File> it = this.mFiles.iterator();
            while (it.hasNext()) {
                result += it.next().getSize();
            }
            return result;
        }

        static class File {
            String dest;
            ArrayList<Part> mParts = new ArrayList<>();

            public File(String src, String dest2, String md5, long size) {
                if (src != null) {
                    this.mParts.add(new Part(src, md5, size));
                }
                this.dest = dest2;
            }

            static class Part {
                String md5;
                long size;
                String src;

                Part(String src2, String md52, long size2) {
                    this.src = src2;
                    this.md5 = md52;
                    this.size = size2;
                }
            }

            /* access modifiers changed from: package-private */
            public long getSize() {
                long result = 0;
                Iterator<Part> it = this.mParts.iterator();
                while (it.hasNext()) {
                    Part part = it.next();
                    if (part.size > 0) {
                        result += part.size;
                    }
                }
                return result;
            }
        }
    }

    private static class ConfigHandler extends DefaultHandler {
        public Config mConfig = new Config(null);

        public static Config parse(InputStream is) throws SAXException, UnsupportedEncodingException, IOException {
            ConfigHandler handler = new ConfigHandler();
            Xml.parse(is, Xml.findEncodingByName("UTF-8"), handler);
            return handler.mConfig;
        }

        private ConfigHandler() {
        }

        /* Debug info: failed to restart local var, previous not found, register: 10 */
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (localName.equals("config")) {
                this.mConfig.version = getRequiredString(attributes, "version");
            } else if (localName.equals("file")) {
                this.mConfig.mFiles.add(new Config.File(attributes.getValue("", "src"), getRequiredString(attributes, "dest"), attributes.getValue("", "md5"), getLong(attributes, "size", -1)));
            } else if (localName.equals("part")) {
                String src = getRequiredString(attributes, "src");
                String md5 = attributes.getValue("", "md5");
                long size = getLong(attributes, "size", -1);
                int length = this.mConfig.mFiles.size();
                if (length > 0) {
                    this.mConfig.mFiles.get(length - 1).mParts.add(new Config.File.Part(src, md5, size));
                }
            }
        }

        private static String getRequiredString(Attributes attributes, String localName) throws SAXException {
            String result = attributes.getValue("", localName);
            if (result != null) {
                return result;
            }
            throw new SAXException("Expected attribute " + localName);
        }

        private static long getLong(Attributes attributes, String localName, long defaultValue) {
            String value = attributes.getValue("", localName);
            if (value == null) {
                return defaultValue;
            }
            return Long.parseLong(value);
        }
    }

    private class DownloaderException extends Exception {
        public DownloaderException(String reason) {
            super(reason);
        }
    }

    private class Downloader implements Runnable {
        private static final int CHUNK_SIZE = 32768;
        private String mConfigVersion;
        private File mDataDir;
        private String mDataPath;
        private long mDownloadedSize;
        private String mFileConfigUrl;
        byte[] mFileIOBuffer;
        private DefaultHttpClient mHttpClient;
        private HttpGet mHttpGet;
        private int mReportedProgress;
        private long mTotalExpectedSize;
        private String mUserAgent;

        private Downloader() {
            this.mFileIOBuffer = new byte[CHUNK_SIZE];
        }

        /* synthetic */ Downloader(DownloaderActivity downloaderActivity, Downloader downloader) {
            this();
        }

        public void run() {
            Intent intent = DownloaderActivity.this.getIntent();
            this.mFileConfigUrl = intent.getStringExtra(DownloaderActivity.EXTRA_FILE_CONFIG_URL);
            this.mConfigVersion = intent.getStringExtra(DownloaderActivity.EXTRA_CONFIG_VERSION);
            this.mDataPath = intent.getStringExtra(DownloaderActivity.EXTRA_DATA_PATH);
            this.mUserAgent = intent.getStringExtra(DownloaderActivity.EXTRA_USER_AGENT);
            this.mDataDir = new File(this.mDataPath);
            try {
                this.mHttpClient = new DefaultHttpClient();
                Config config = getConfig();
                filter(config);
                persistantDownload(config);
                verify(config);
                cleanup();
                reportSuccess();
            } catch (Exception e) {
                Exception e2 = e;
                reportFailure(String.valueOf(e2.toString()) + "\n" + Log.getStackTraceString(e2));
            }
        }

        private void persistantDownload(Config config) throws ClientProtocolException, DownloaderException, IOException {
            while (true) {
                try {
                    download(config);
                    return;
                } catch (SocketException e) {
                    if (DownloaderActivity.this.mSuppressErrorMessages) {
                        throw e;
                    }
                } catch (SocketTimeoutException e2) {
                    if (DownloaderActivity.this.mSuppressErrorMessages) {
                        throw e2;
                    }
                }
                Log.i(DownloaderActivity.LOG_TAG, "Network connectivity issue, retrying.");
            }
        }

        private void filter(Config config) throws IOException, DownloaderException {
            File filteredFile = new File(this.mDataDir, DownloaderActivity.LOCAL_FILTERED_FILE);
            if (!filteredFile.exists()) {
                File localConfigFile = new File(this.mDataDir, DownloaderActivity.LOCAL_CONFIG_FILE_TEMP);
                HashSet<String> keepSet = new HashSet<>();
                keepSet.add(localConfigFile.getCanonicalPath());
                HashMap<String, Config.File> fileMap = new HashMap<>();
                Iterator<Config.File> it = config.mFiles.iterator();
                while (it.hasNext()) {
                    Config.File file = it.next();
                    fileMap.put(new File(this.mDataDir, file.dest).getCanonicalPath(), file);
                }
                recursiveFilter(this.mDataDir, fileMap, keepSet, false);
                touch(filteredFile);
            }
        }

        private void touch(File file) throws FileNotFoundException {
            DownloaderActivity.quietClose(new FileOutputStream(file));
        }

        private boolean recursiveFilter(File base, HashMap<String, Config.File> fileMap, HashSet<String> keepSet, boolean filterBase) throws IOException, DownloaderException {
            boolean result = true;
            if (base.isDirectory()) {
                for (File child : base.listFiles()) {
                    result &= recursiveFilter(child, fileMap, keepSet, true);
                }
            }
            if (!filterBase) {
                return result;
            }
            if (base.isDirectory()) {
                if (base.listFiles().length == 0) {
                    return result & base.delete();
                }
                return result;
            } else if (!shouldKeepFile(base, fileMap, keepSet)) {
                return result & base.delete();
            } else {
                return result;
            }
        }

        private boolean shouldKeepFile(File file, HashMap<String, Config.File> fileMap, HashSet<String> keepSet) throws IOException, DownloaderException {
            String canonicalPath = file.getCanonicalPath();
            if (keepSet.contains(canonicalPath)) {
                return true;
            }
            Config.File configFile = fileMap.get(canonicalPath);
            if (configFile == null) {
                return false;
            }
            return verifyFile(configFile, false);
        }

        private void reportSuccess() {
            DownloaderActivity.this.mHandler.sendMessage(Message.obtain(DownloaderActivity.this.mHandler, 0));
        }

        private void reportFailure(String reason) {
            DownloaderActivity.this.mHandler.sendMessage(Message.obtain(DownloaderActivity.this.mHandler, 1, reason));
        }

        private void reportProgress(int progress) {
            DownloaderActivity.this.mHandler.sendMessage(Message.obtain(DownloaderActivity.this.mHandler, 2, progress, 0));
        }

        private void reportVerifying() {
            DownloaderActivity.this.mHandler.sendMessage(Message.obtain(DownloaderActivity.this.mHandler, 3));
        }

        /* JADX INFO: finally extract failed */
        private Config getConfig() throws DownloaderException, ClientProtocolException, IOException, SAXException {
            Config config = null;
            if (this.mDataDir.exists()) {
                config = DownloaderActivity.getLocalConfig(this.mDataDir, DownloaderActivity.LOCAL_CONFIG_FILE_TEMP);
                if (config == null || !this.mConfigVersion.equals(config.version)) {
                    if (config == null) {
                        Log.i(DownloaderActivity.LOG_TAG, "Couldn't find local config.");
                    } else {
                        Log.i(DownloaderActivity.LOG_TAG, "Local version out of sync. Wanted " + this.mConfigVersion + " but have " + config.version);
                    }
                    config = null;
                }
            } else {
                Log.i(DownloaderActivity.LOG_TAG, "Creating directory " + this.mDataPath);
                this.mDataDir.mkdirs();
                this.mDataDir.mkdir();
                if (!this.mDataDir.exists()) {
                    throw new DownloaderException("Could not create the directory " + this.mDataPath);
                }
            }
            if (config == null) {
                InputStream is = new FileInputStream(download(this.mFileConfigUrl, DownloaderActivity.LOCAL_CONFIG_FILE_TEMP));
                try {
                    config = ConfigHandler.parse(is);
                    DownloaderActivity.quietClose(is);
                    if (!config.version.equals(this.mConfigVersion)) {
                        throw new DownloaderException("Configuration file version mismatch. Expected " + this.mConfigVersion + " received " + config.version);
                    }
                } catch (Throwable th) {
                    DownloaderActivity.quietClose(is);
                    throw th;
                }
            }
            return config;
        }

        private void noisyDelete(File file) throws IOException {
            if (!file.delete()) {
                throw new IOException("could not delete " + file);
            }
        }

        private void download(Config config) throws DownloaderException, ClientProtocolException, IOException {
            this.mDownloadedSize = 0;
            getSizes(config);
            Iterator<Config.File> it = config.mFiles.iterator();
            while (it.hasNext()) {
                downloadFile(it.next());
            }
        }

        private void downloadFile(Config.File file) throws DownloaderException, FileNotFoundException, IOException, ClientProtocolException {
            FileInputStream is;
            boolean append = false;
            File dest = new File(this.mDataDir, file.dest);
            long bytesToSkip = 0;
            if (dest.exists() && dest.isFile()) {
                append = true;
                bytesToSkip = dest.length();
                this.mDownloadedSize = this.mDownloadedSize + bytesToSkip;
            }
            FileOutputStream os = null;
            long offsetOfCurrentPart = 0;
            try {
                Iterator<Config.File.Part> it = file.mParts.iterator();
                while (it.hasNext()) {
                    Config.File.Part part = it.next();
                    if (part.size > bytesToSkip || part.size == 0) {
                        MessageDigest digest = null;
                        if (part.md5 != null) {
                            digest = createDigest();
                            if (bytesToSkip > 0) {
                                is = openInput(file.dest);
                                is.skip(offsetOfCurrentPart);
                                readIntoDigest(is, bytesToSkip, digest);
                                DownloaderActivity.quietClose(is);
                            }
                        }
                        if (os == null) {
                            os = openOutput(file.dest, append);
                        }
                        downloadPart(part.src, os, bytesToSkip, part.size, digest);
                        if (digest != null) {
                            String hash = getHash(digest);
                            if (!hash.equalsIgnoreCase(part.md5)) {
                                Log.e(DownloaderActivity.LOG_TAG, "web MD5 checksums don't match. " + part.src + "\nExpected " + part.md5 + "\n     got " + hash);
                                DownloaderActivity.quietClose(os);
                                dest.delete();
                                throw new DownloaderException("Received bad data from web server");
                            }
                            Log.i(DownloaderActivity.LOG_TAG, "web MD5 checksum matches.");
                        }
                    }
                    bytesToSkip -= Math.min(bytesToSkip, part.size);
                    offsetOfCurrentPart += part.size;
                }
                DownloaderActivity.quietClose(os);
            } catch (Throwable th) {
                DownloaderActivity.quietClose(os);
                throw th;
            }
        }

        private void cleanup() throws IOException {
            noisyDelete(new File(this.mDataDir, DownloaderActivity.LOCAL_FILTERED_FILE));
            new File(this.mDataDir, DownloaderActivity.LOCAL_CONFIG_FILE_TEMP).renameTo(new File(this.mDataDir, DownloaderActivity.LOCAL_CONFIG_FILE));
        }

        private void verify(Config config) throws DownloaderException, ClientProtocolException, IOException {
            Log.i(DownloaderActivity.LOG_TAG, "Verifying...");
            String failFiles = null;
            Iterator<Config.File> it = config.mFiles.iterator();
            while (it.hasNext()) {
                Config.File file = it.next();
                if (!verifyFile(file, true)) {
                    if (failFiles == null) {
                        failFiles = file.dest;
                    } else {
                        failFiles = String.valueOf(failFiles) + " " + file.dest;
                    }
                }
            }
            if (failFiles != null) {
                throw new DownloaderException("Possible bad SD-Card. MD5 sum incorrect for file(s) " + failFiles);
            }
        }

        /* JADX INFO: finally extract failed */
        private boolean verifyFile(Config.File file, boolean deleteInvalid) throws FileNotFoundException, DownloaderException, IOException {
            reportVerifying();
            File dest = new File(this.mDataDir, file.dest);
            if (!dest.exists()) {
                return false;
            }
            if (file.getSize() == dest.length() || !deleteInvalid) {
                FileInputStream is = new FileInputStream(dest);
                try {
                    Iterator<Config.File.Part> it = file.mParts.iterator();
                    while (it.hasNext()) {
                        Config.File.Part part = it.next();
                        if (part.md5 != null) {
                            MessageDigest digest = createDigest();
                            readIntoDigest(is, part.size, digest);
                            String hash = getHash(digest);
                            if (!hash.equalsIgnoreCase(part.md5)) {
                                Log.e(DownloaderActivity.LOG_TAG, "MD5 checksums don't match. " + part.src + " Expected " + part.md5 + " got " + hash);
                                if (deleteInvalid) {
                                    DownloaderActivity.quietClose(is);
                                    dest.delete();
                                }
                                DownloaderActivity.quietClose(is);
                                return false;
                            }
                        }
                    }
                    DownloaderActivity.quietClose(is);
                    return true;
                } catch (Throwable th) {
                    DownloaderActivity.quietClose(is);
                    throw th;
                }
            } else {
                dest.delete();
                return false;
            }
        }

        private void readIntoDigest(FileInputStream is, long bytesToRead, MessageDigest digest) throws IOException {
            int bytesRead;
            while (bytesToRead > 0 && (bytesRead = is.read(this.mFileIOBuffer, 0, (int) Math.min((long) this.mFileIOBuffer.length, bytesToRead))) >= 0) {
                updateDigest(digest, bytesRead);
                bytesToRead -= (long) bytesRead;
            }
        }

        private MessageDigest createDigest() throws DownloaderException {
            try {
                return MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                throw new DownloaderException("Couldn't create MD5 digest");
            }
        }

        private void updateDigest(MessageDigest digest, int bytesRead) {
            if (bytesRead == this.mFileIOBuffer.length) {
                digest.update(this.mFileIOBuffer);
                return;
            }
            byte[] temp = new byte[bytesRead];
            System.arraycopy(this.mFileIOBuffer, 0, temp, 0, bytesRead);
            digest.update(temp);
        }

        private String getHash(MessageDigest digest) {
            StringBuilder builder = new StringBuilder();
            for (byte b : digest.digest()) {
                builder.append(Integer.toHexString((b >> 4) & 15));
                builder.append(Integer.toHexString(b & 15));
            }
            return builder.toString();
        }

        private void getSizes(Config config) throws ClientProtocolException, IOException, DownloaderException {
            Iterator<Config.File> it = config.mFiles.iterator();
            while (it.hasNext()) {
                Iterator<Config.File.Part> it2 = it.next().mParts.iterator();
                while (it2.hasNext()) {
                    Config.File.Part part = it2.next();
                    if (part.size < 0) {
                        part.size = getSize(part.src);
                    }
                }
            }
            this.mTotalExpectedSize = config.getSize();
        }

        private long getSize(String url) throws ClientProtocolException, IOException {
            HttpResponse response = this.mHttpClient.execute(new HttpHead(normalizeUrl(url)));
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new IOException("Unexpected Http status code " + response.getStatusLine().getStatusCode());
            }
            Header[] clHeaders = response.getHeaders("Content-Length");
            if (clHeaders.length > 0) {
                return Long.parseLong(clHeaders[0].getValue());
            }
            return -1;
        }

        private String normalizeUrl(String url) throws MalformedURLException {
            return new URL(new URL(this.mFileConfigUrl), url).toString();
        }

        private InputStream get(String url, long startOffset, long expectedLength) throws ClientProtocolException, IOException {
            this.mHttpGet = new HttpGet(normalizeUrl(url));
            int expectedStatusCode = 200;
            if (startOffset > 0) {
                String range = "bytes=" + startOffset + "-";
                if (expectedLength >= 0) {
                    range = String.valueOf(range) + (expectedLength - 1);
                }
                Log.i(DownloaderActivity.LOG_TAG, "requesting byte range " + range);
                this.mHttpGet.addHeader("Range", range);
                expectedStatusCode = 206;
            }
            HttpResponse response = this.mHttpClient.execute(this.mHttpGet);
            long bytesToSkip = 0;
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != expectedStatusCode) {
                if (statusCode == 200 && expectedStatusCode == 206) {
                    Log.i(DownloaderActivity.LOG_TAG, "Byte range request ignored");
                    bytesToSkip = startOffset;
                } else {
                    throw new IOException("Unexpected Http status code " + statusCode + " expected " + expectedStatusCode);
                }
            }
            InputStream is = response.getEntity().getContent();
            if (bytesToSkip > 0) {
                is.skip(bytesToSkip);
            }
            return is;
        }

        private File download(String src, String dest) throws DownloaderException, ClientProtocolException, IOException {
            File destFile = new File(this.mDataDir, dest);
            FileOutputStream os = openOutput(dest, false);
            try {
                downloadPart(src, os, 0, -1, null);
                return destFile;
            } finally {
                os.close();
            }
        }

        private void downloadPart(String src, FileOutputStream os, long startOffset, long expectedLength, MessageDigest digest) throws ClientProtocolException, IOException, DownloaderException {
            boolean lengthIsKnown = expectedLength >= 0;
            if (startOffset < 0) {
                throw new IllegalArgumentException("Negative startOffset:" + startOffset);
            } else if (!lengthIsKnown || startOffset <= expectedLength) {
                InputStream is = get(src, startOffset, expectedLength);
                try {
                    long bytesRead = downloadStream(is, os, digest);
                    if (lengthIsKnown) {
                        long expectedBytesRead = expectedLength - startOffset;
                        if (expectedBytesRead != bytesRead) {
                            Log.e(DownloaderActivity.LOG_TAG, "Bad file transfer from server: " + src + " Expected " + expectedBytesRead + " Received " + bytesRead);
                            throw new DownloaderException("Incorrect number of bytes received from server");
                        }
                    }
                } finally {
                    is.close();
                    this.mHttpGet = null;
                }
            } else {
                throw new IllegalArgumentException("startOffset > expectedLength" + startOffset + " " + expectedLength);
            }
        }

        private FileOutputStream openOutput(String dest, boolean append) throws FileNotFoundException, DownloaderException {
            File destFile = new File(this.mDataDir, dest);
            File parent = destFile.getParentFile();
            if (!parent.exists()) {
                parent.mkdirs();
            }
            if (parent.exists()) {
                return new FileOutputStream(destFile, append);
            }
            throw new DownloaderException("Could not create directory " + parent.toString());
        }

        private FileInputStream openInput(String src) throws FileNotFoundException, DownloaderException {
            File srcFile = new File(this.mDataDir, src);
            File parent = srcFile.getParentFile();
            if (!parent.exists()) {
                parent.mkdirs();
            }
            if (parent.exists()) {
                return new FileInputStream(srcFile);
            }
            throw new DownloaderException("Could not create directory " + parent.toString());
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(long, long):long}
         arg types: [int, long]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(float, float):float}
          ClspMth{java.lang.Math.max(long, long):long} */
        private long downloadStream(InputStream is, FileOutputStream os, MessageDigest digest) throws DownloaderException, IOException {
            long totalBytesRead = 0;
            while (!Thread.interrupted()) {
                int bytesRead = is.read(this.mFileIOBuffer);
                if (bytesRead < 0) {
                    return totalBytesRead;
                }
                if (digest != null) {
                    updateDigest(digest, bytesRead);
                }
                totalBytesRead += (long) bytesRead;
                os.write(this.mFileIOBuffer, 0, bytesRead);
                this.mDownloadedSize += (long) bytesRead;
                int progress = (int) Math.min(this.mTotalExpectedSize, (this.mDownloadedSize * 10000) / Math.max(1L, this.mTotalExpectedSize));
                if (progress != this.mReportedProgress) {
                    this.mReportedProgress = progress;
                    reportProgress(progress);
                }
            }
            Log.i(DownloaderActivity.LOG_TAG, "downloader thread interrupted.");
            this.mHttpGet.abort();
            throw new DownloaderException("Thread interrupted");
        }
    }
}
