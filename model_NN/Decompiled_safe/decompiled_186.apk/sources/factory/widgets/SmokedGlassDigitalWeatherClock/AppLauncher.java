package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.Collections;
import java.util.List;

public class AppLauncher extends ListActivity {
    AppAdapter adapter = null;
    private int appWidgetId;
    /* access modifiers changed from: private */
    public Context self = this;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.applauncher);
        this.appWidgetId = getIntent().getExtras().getInt("appWidgetId", 0);
        PackageManager pm = getPackageManager();
        Intent main = new Intent("android.intent.action.MAIN", (Uri) null);
        main.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> launchables = pm.queryIntentActivities(main, 0);
        Collections.sort(launchables, new ResolveInfo.DisplayNameComparator(pm));
        this.adapter = new AppAdapter(pm, launchables);
        setListAdapter(this.adapter);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        ResolveInfo launchable = (ResolveInfo) this.adapter.getItem(position);
        ActivityInfo activity = launchable.activityInfo;
        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
        final Intent i = new Intent("android.intent.action.MAIN");
        i.addCategory("android.intent.category.LAUNCHER");
        i.setFlags(270532608);
        i.setComponent(name);
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Selected Item");
        adb.setMessage("Selected Item is \n" + ((String) launchable.activityInfo.applicationInfo.loadLabel(getPackageManager())));
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                SharedPreferences.Editor edit = AppLauncher.this.self.getSharedPreferences("prefs", 0).edit();
                edit.putString("applauncher", name.toString());
                edit.putString("launcherPackage1", name.getPackageName());
                edit.putString("launcherClass1", name.getClassName());
                edit.commit();
                AppLauncher.this.setResult(-1, new Intent().setAction(i.toString()));
                AppLauncher.this.finish();
            }
        });
        adb.show();
    }

    class AppAdapter extends ArrayAdapter<ResolveInfo> {
        private PackageManager pm = null;

        AppAdapter(PackageManager pm2, List<ResolveInfo> apps) {
            super(AppLauncher.this, (int) R.layout.row, apps);
            this.pm = pm2;
        }

        public String getItemAtPosition(int position) {
            return null;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = newView(parent);
            }
            bindView(position, convertView);
            return convertView;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        private View newView(ViewGroup parent) {
            return AppLauncher.this.getLayoutInflater().inflate((int) R.layout.row, parent, false);
        }

        private void bindView(int position, View row) {
            ((TextView) row.findViewById(R.id.label)).setText(((ResolveInfo) getItem(position)).loadLabel(this.pm));
            ((ImageView) row.findViewById(R.id.icon)).setImageDrawable(((ResolveInfo) getItem(position)).loadIcon(this.pm));
        }
    }
}
