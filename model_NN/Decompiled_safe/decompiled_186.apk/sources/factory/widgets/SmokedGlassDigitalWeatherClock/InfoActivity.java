package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class InfoActivity extends Activity {
    /* access modifiers changed from: private */
    public String applicationName;
    private Button closeButton;

    public void onCreate(Bundle savedInstanceState) {
        ApplicationInfo ai;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.infolayout);
        WebView mWebView = (WebView) findViewById(R.id.wv);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl("file:///android_asset/info.html");
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.setWebViewClient(new YourWebClient(this, null));
        this.closeButton = (Button) findViewById(R.id.bthelpfin);
        this.closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InfoActivity.this.finish();
            }
        });
        PackageManager pm = getPackageManager();
        try {
            ai = pm.getApplicationInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            ai = null;
        }
        this.applicationName = (String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)");
    }

    private class YourWebClient extends WebViewClient {
        private YourWebClient() {
        }

        /* synthetic */ YourWebClient(InfoActivity infoActivity, YourWebClient yourWebClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.contains("mailto")) {
                sendEmail("harisdotdot@gmail.com");
                return super.shouldOverrideUrlLoading(view, url);
            }
            view.loadUrl(url);
            if (url != null && url.startsWith("http://fatorywidgets.blogspot.com/2011/06/collection-of-our-clock-widgets.html")) {
                view.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                InfoActivity.this.finish();
                return true;
            } else if (url != null && url.startsWith("market://")) {
                view.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                InfoActivity.this.finish();
                return true;
            } else if (!url.startsWith("vnd.youtube:")) {
                return false;
            } else {
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("http://www.youtube.com/watch?v=DpMLF9b2aTQ"));
                InfoActivity.this.startActivity(i);
                InfoActivity.this.finish();
                return true;
            }
        }

        public void sendEmail(String email) {
            Intent emailIntent = new Intent("android.intent.action.SEND");
            emailIntent.setType("plain/text");
            emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{email});
            emailIntent.putExtra("android.intent.extra.SUBJECT", InfoActivity.this.applicationName);
            emailIntent.putExtra("android.intent.extra.TEXT", "");
            InfoActivity.this.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            InfoActivity.this.finish();
        }
    }
}
