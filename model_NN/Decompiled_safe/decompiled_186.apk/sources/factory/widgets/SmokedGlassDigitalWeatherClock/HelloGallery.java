package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class HelloGallery extends Activity {
    ImageView imageView;
    /* access modifiers changed from: private */
    public Context self = this;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.gallery_main);
        Gallery g = (Gallery) findViewById(R.id.Gallery01);
        final List<String> SD = ReadSDCard();
        g.setAdapter((SpinnerAdapter) new ImageAdapter(this, SD));
        this.imageView = (ImageView) findViewById(R.id.ImageView01);
        g.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                String myImagePath = (String) SD.get(position);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 1;
                HelloGallery.this.imageView.setImageBitmap(BitmapFactory.decodeFile(myImagePath, options));
                SharedPreferences.Editor edit = HelloGallery.this.self.getSharedPreferences("prefs", 0).edit();
                edit.putString("skinchoice", (String) SD.get(position));
                edit.commit();
                Toast.makeText(HelloGallery.this, "You selected " + HelloGallery.fileBaseName(new File(myImagePath)), 0).show();
            }
        });
    }

    private List<String> ReadSDCard() {
        List<String> tFileList = new ArrayList<>();
        File dir = new File(Environment.getExternalStorageDirectory() + "/factory.widgets.skins/");
        if (!dir.exists() && !dir.isDirectory()) {
            new File(String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/factory.widgets.skins/").mkdir();
        }
        File subdir = new File(Environment.getExternalStorageDirectory() + "/factory.widgets.skins2/large");
        if (!subdir.exists() && !subdir.isDirectory()) {
            new File(String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/factory.widgets.skins/large").mkdir();
        }
        File[] files = new File(Environment.getExternalStorageDirectory() + "/factory.widgets.skins/large/").listFiles();
        for (File file : files) {
            String curFile = file.getPath();
            String ext = curFile.substring(curFile.lastIndexOf(".") + 1, curFile.length()).toLowerCase();
            if (ext.equals("jpg") || ext.equals("gif") || ext.equals("png")) {
                tFileList.add(file.getPath());
            }
        }
        return tFileList;
    }

    public class ImageAdapter extends BaseAdapter {
        private List<String> FileList;
        private Context mContext;
        int mGalleryItemBackground;

        public ImageAdapter(Context c, List<String> fList) {
            this.mContext = c;
            this.FileList = fList;
            TypedArray a = HelloGallery.this.obtainStyledAttributes(R.styleable.Gallery1);
            this.mGalleryItemBackground = a.getResourceId(0, 0);
            a.recycle();
        }

        public int getCount() {
            return this.FileList.size();
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView i = new ImageView(this.mContext);
            i.setImageBitmap(BitmapFactory.decodeFile(this.FileList.get(position).toString()));
            i.setLayoutParams(new Gallery.LayoutParams(200, 133));
            i.setScaleType(ImageView.ScaleType.FIT_XY);
            i.setBackgroundResource(this.mGalleryItemBackground);
            return i;
        }
    }

    public static String fileBaseName(File f) {
        String name = f.getName();
        int idx = name.lastIndexOf(46);
        if (idx == -1) {
            return name;
        }
        if (idx == 0) {
            return new String("");
        }
        return name.substring(0, idx);
    }
}
