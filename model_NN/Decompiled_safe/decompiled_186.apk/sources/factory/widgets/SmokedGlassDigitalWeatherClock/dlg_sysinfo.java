package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.widget.TextView;
import java.text.DecimalFormat;

public class dlg_sysinfo extends Activity {
    private TextView SDfree;
    private TextView SDtotal;
    /* access modifiers changed from: private */
    public TextView batterLevel;
    /* access modifiers changed from: private */
    public TextView batteryTemp;
    private TextView internalfree;
    private TextView memfreeram;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.dlg_sysinfo);
        this.SDfree = (TextView) findViewById(R.id.textviewfree);
        this.internalfree = (TextView) findViewById(R.id.textviewintmem);
        this.memfreeram = (TextView) findViewById(R.id.textviewramfree);
        this.batterLevel = (TextView) findViewById(R.id.batteryLevel);
        this.batteryTemp = (TextView) findViewById(R.id.batteryTemp);
        getsdfree();
        getinternalfree();
        memfree();
        batteryLevel();
    }

    private void getsdfree() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        double megAvailable = (((double) (((long) stat.getBlockSize()) * ((long) stat.getAvailableBlocks()))) / 1048576.0d) / 1024.0d;
        if (megAvailable > 1.0d) {
            this.SDfree.setText("SD Card Storage free:\n" + (String.valueOf(new DecimalFormat("#.##").format(megAvailable)) + "GB"));
            return;
        }
        this.SDfree.setText("SD Card Storage free:\n" + (String.valueOf(new DecimalFormat("#").format(megAvailable * 1024.0d)) + "MB"));
    }

    private void getinternalfree() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        double intmem = (((double) ((long) stat.getAvailableBlocks())) * ((double) ((long) stat.getBlockSize()))) / 1048576.0d;
        if (intmem > 999.0d) {
            this.internalfree.setText("Internal Storage free:\n" + (String.valueOf(new DecimalFormat("#.00").format(intmem / 1024.0d)) + "GB"));
            return;
        }
        this.internalfree.setText("Internal Storage free:\n" + (String.valueOf(new DecimalFormat("#").format(intmem)) + "MB"));
    }

    private void memfree() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ((ActivityManager) getSystemService("activity")).getMemoryInfo(mi);
        this.memfreeram.setText("Internal Memory free:\n" + (mi.availMem / 1048576) + "Mb");
    }

    private void batteryLevel() {
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                context.unregisterReceiver(this);
                int rawlevel = intent.getIntExtra("level", 0);
                int scale = intent.getIntExtra("scale", 100);
                int level = -1;
                if (rawlevel >= 0 && scale > 0) {
                    level = (rawlevel * 100) / scale;
                }
                dlg_sysinfo.this.batterLevel.setText("Battery Level:\n" + level + "%");
                int temperature = intent.getIntExtra("temperature", 0);
                int tens = temperature / 10;
                String ct = String.valueOf(Integer.toString(tens)) + "." + (temperature - (tens * 10));
                int tens2 = (temperature * 18) / 100;
                dlg_sysinfo.this.batteryTemp.setText("Battery Temperature:\n" + (String.valueOf(ct) + "°C / " + (String.valueOf(Integer.toString(tens2 + 32)) + "." + ((temperature * 18) - (tens2 * 100))) + "°F"));
            }
        }, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    }
}
