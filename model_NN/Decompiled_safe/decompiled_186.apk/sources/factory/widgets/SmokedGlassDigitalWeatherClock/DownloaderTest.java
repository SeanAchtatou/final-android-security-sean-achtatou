package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;

public class DownloaderTest extends Activity {
    private static final String CONFIG_VERSION = "1.0";
    private static final String DATA_PATH = (Environment.getExternalStorageDirectory() + "/factory.widgets.skins/large");
    private static final String FILE_CONFIG_URL = "http://sites.google.com/site/factorywidgets/sense-clock-skins/download.config";
    private static final String USER_AGENT = "MyApp Downloader";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (DownloaderActivity.ensureDownloaded(this, getString(R.string.app_name), FILE_CONFIG_URL, CONFIG_VERSION, DATA_PATH, USER_AGENT)) {
            setContentView((int) R.layout.downloadermain);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = true;
        if (item.getItemId() == R.id.menu_main_download_again) {
            downloadAgain();
        } else {
            handled = false;
        }
        if (!handled) {
            return super.onOptionsItemSelected(item);
        }
        return handled;
    }

    private void downloadAgain() {
        DownloaderActivity.deleteData(DATA_PATH);
        startActivity(getIntent());
        finish();
    }
}
