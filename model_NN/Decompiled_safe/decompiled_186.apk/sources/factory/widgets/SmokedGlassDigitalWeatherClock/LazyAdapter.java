package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.net.MalformedURLException;
import java.net.URL;

public class LazyAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    private Activity activity;
    private String[] data;
    public ImageLoader imageLoader = new ImageLoader(this.activity.getApplicationContext());

    public static class ViewHolder {
        public ImageView image;
        public TextView text;
    }

    public LazyAdapter(Activity a, String[] d) {
        this.activity = a;
        this.data = d;
        inflater = (LayoutInflater) this.activity.getSystemService("layout_inflater");
    }

    public int getCount() {
        return this.data.length;
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate((int) R.layout.lazyitem, (ViewGroup) null);
            holder = new ViewHolder();
            holder.text = (TextView) vi.findViewById(R.id.text);
            holder.image = (ImageView) vi.findViewById(R.id.image);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }
        URL fullurl = null;
        try {
            fullurl = new URL(this.data[position]);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        String[] pathparts = fullurl.getPath().split("\\/");
        holder.text.setText(pathparts[pathparts.length - 1].split("\\.", 1)[0].replaceFirst("\\.png", "").replaceAll("_", " "));
        holder.image.setTag(this.data[position]);
        this.imageLoader.DisplayImage(this.data[position], this.activity, holder.image);
        return vi;
    }

    private String getFile(String fullurl) {
        return null;
    }
}
