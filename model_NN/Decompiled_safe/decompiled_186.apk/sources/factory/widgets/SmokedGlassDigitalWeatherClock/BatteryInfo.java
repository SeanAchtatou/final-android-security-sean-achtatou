package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class BatteryInfo extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        SharedPreferences settings;
        try {
            if (intent.getAction().equals("android.intent.action.BATTERY_CHANGED") && (settings = context.getSharedPreferences(CountdownService.PREFS_NAME, 0)) != null) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt(CountdownService.KEY_LEVEL, intent.getIntExtra("level", 0));
                editor.putInt(CountdownService.KEY_CHARGING, intent.getIntExtra("status", 1));
                editor.putInt(CountdownService.KEY_VOLTAGE, intent.getIntExtra("voltage", 0));
                editor.putInt(CountdownService.KEY_TEMP, intent.getIntExtra("temperature", 0));
                editor.commit();
            }
        } catch (Exception e) {
        }
    }
}
