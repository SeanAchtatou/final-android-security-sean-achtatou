package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class CountdownWidget extends AppWidgetProvider {
    public static int UPDATE_RATE = 60000;

    public void onEnabled(Context context) {
        for (int appWidgetId : AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, CountdownWidget.class))) {
            try {
                makeControlPendingIntent(context, CountdownService.UPDATE, appWidgetId).send();
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
        }
        super.onEnabled(context);
    }

    public void onDeleted(Context context, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            setAlarm(context, appWidgetId, -1);
            CountdownService.setMissedit(1);
        }
        super.onDeleted(context, appWidgetIds);
    }

    public void onDisabled(Context context) {
        context.stopService(new Intent(context, CountdownService.class));
        super.onDisabled(context);
        CountdownService.setMissedit(1);
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            setAlarm(context, appWidgetId, UPDATE_RATE);
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.TIME_SET".equals(intent.getAction()) || "android.intent.action.DATE_CHANGED".equals(intent.getAction()) || "android.intent.action.TIMEZONE_CHANGED".equals(intent.getAction())) {
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context.getPackageName(), CountdownWidget.class.getName()));
            try {
                onUpdate(context, appWidgetManager, appWidgetIds);
                for (int i = 0; i < appWidgetIds.length; i++) {
                    setAlarm(context, appWidgetIds[i], -1);
                    setAlarm(context, appWidgetIds[i], UPDATE_RATE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            for (int appWidgetId : appWidgetIds) {
                try {
                    makeControlPendingIntent(context, CountdownService.UPDATE, appWidgetId).send();
                } catch (PendingIntent.CanceledException e2) {
                    e2.printStackTrace();
                }
            }
        }
        super.onReceive(context, intent);
    }

    public static void setAlarm(Context context, int appWidgetId, int updateRate) {
        long firstTime = System.currentTimeMillis();
        long firstTime2 = firstTime + (60000 - (firstTime % 60000));
        PendingIntent newPending = makeControlPendingIntent(context, CountdownService.UPDATE, appWidgetId);
        AlarmManager alarms = (AlarmManager) context.getSystemService("alarm");
        if (updateRate >= 0) {
            alarms.setRepeating(1, firstTime2, (long) updateRate, newPending);
        } else {
            alarms.cancel(newPending);
        }
    }

    public static PendingIntent makeControlPendingIntent(Context context, String command, int appWidgetId) {
        Intent active = new Intent(context, CountdownService.class);
        active.setAction(command);
        active.putExtra("appWidgetId", appWidgetId);
        active.setData(Uri.withAppendedPath(Uri.parse("countdownwidget://widget/id/#" + command + appWidgetId), String.valueOf(appWidgetId)));
        return PendingIntent.getService(context, 0, active, 134217728);
    }
}
