package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;

public class FindWeatherLocation extends Activity {
    static ArrayList<String> alback = new ArrayList<>();
    String a;
    /* access modifiers changed from: private */
    public int appWidgetId;
    double b;
    Button button01;
    Button buttonclose;
    double c;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            Toast.makeText(FindWeatherLocation.this.getApplicationContext(), "Processing Done", 1).show();
            FindWeatherLocation.this.openMenu();
        }
    };
    private Object[] ia;
    ImageView iv1;
    TextView lc;
    TextView ln;
    private View.OnClickListener myClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            FindWeatherLocation.this.getWindow().setSoftInputMode(3);
            FindWeatherLocation.this.a = FindWeatherLocation.this.num1.getText().toString();
            FindWeatherLocation.this.tv1 = (TextView) FindWeatherLocation.this.findViewById(R.id.TextView01);
            FindWeatherLocation.this.pd = ProgressDialog.show(FindWeatherLocation.this, "Searching for weather", "please wait....", true);
            new Thread() {
                public void run() {
                    try {
                        FindWeatherLocation.alback = WeatherLocator.parseWeather(FindWeatherLocation.this.a);
                        sleep(1000);
                    } catch (Exception e) {
                    }
                    FindWeatherLocation.this.handler.sendEmptyMessage(0);
                    FindWeatherLocation.this.pd.dismiss();
                }
            }.start();
        }
    };
    EditText num1;
    /* access modifiers changed from: private */
    public ProgressDialog pd;
    /* access modifiers changed from: private */
    public Context self = this;
    TextView text;
    TextView tv1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.weatherlocate);
        Bundle weatherconfig = getIntent().getExtras();
        this.appWidgetId = getIntent().getExtras().getInt("appWidgetId", 0);
        this.tv1 = (TextView) findViewById(R.id.TextView01);
        this.button01 = (Button) findViewById(R.id.Button01);
        this.button01.setOnClickListener(this.myClickListener);
        this.num1 = (EditText) findViewById(R.id.EditText01);
        this.ln = (TextView) findViewById(R.id.TextViewLocation);
        this.lc = (TextView) findViewById(R.id.TextViewCode);
        try {
            String weatherName = weatherconfig.getString("weatherName");
            this.lc.setText(weatherconfig.getString("weatherCode").toString());
            this.ln.setText(weatherName.toString());
        } catch (NullPointerException e) {
            this.lc.setText("Location Code");
            this.lc.setText("Location Name");
            e.printStackTrace();
        }
        ((Button) findViewById(R.id.wlclose)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FindWeatherLocation.this.finish();
            }
        });
    }

    public void openMenu() {
        Object[] ia2 = alback.toArray();
        final String[] items = new String[ia2.length];
        for (int i = 0; i < ia2.length; i++) {
            items[i] = ((String) ia2[i]).replaceFirst("wc:", "");
        }
        alback.clear();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pick an item");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                String city;
                String[] splitResult = items[item].split("=");
                Toast.makeText(FindWeatherLocation.this.getApplicationContext(), "You selected\n" + splitResult[0], 1).show();
                FindWeatherLocation.this.ln.setText(splitResult[0]);
                FindWeatherLocation.this.lc.setText(splitResult[1]);
                String weatherName = splitResult[0];
                String weatherCode = splitResult[1];
                int i = weatherName.indexOf(",");
                if (i > 0) {
                    city = weatherName.substring(0, i).trim();
                    String trim = weatherName.substring(i + 1).trim();
                } else {
                    city = weatherName;
                }
                SharedPreferences.Editor edit = FindWeatherLocation.this.self.getSharedPreferences("prefs", 0).edit();
                edit.putString("weatherCode" + FindWeatherLocation.this.appWidgetId, weatherCode);
                edit.putString("weatherName" + FindWeatherLocation.this.appWidgetId, city);
                edit.commit();
            }
        });
        builder.create().show();
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(this.tv1.getWindowToken(), 0);
    }
}
