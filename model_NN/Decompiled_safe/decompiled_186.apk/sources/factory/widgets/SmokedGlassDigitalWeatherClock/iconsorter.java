package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import net.sourceforge.zmanim.AstronomicalCalendar;
import net.sourceforge.zmanim.util.GeoLocation;
import net.sourceforge.zmanim.util.ZmanimFormatter;

public class iconsorter {
    private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("EEEE, MMM d");
    private static SimpleDateFormat DIFF_FORMAT = new SimpleDateFormat("H:mm");
    private static SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("h:mm a");

    public static int GetWeatherPic(int skycode) {
        switch (skycode) {
            case 28:
                return 27;
            case 29:
            case 31:
            case 33:
            case 35:
            case 40:
            default:
                return skycode;
            case 30:
                return 29;
            case 32:
            case 36:
                return 31;
            case 34:
                return 33;
            case 37:
                return 47;
            case 38:
                return 47;
            case 39:
                return 45;
            case 41:
                return 46;
        }
    }

    /* JADX INFO: Multiple debug info for r11v1 net.sourceforge.zmanim.AstronomicalCalendar: [D('ac' net.sourceforge.zmanim.AstronomicalCalendar), D('latitude' double)] */
    /* JADX INFO: Multiple debug info for r11v2 java.util.Date: [D('riseTomorrow' java.util.Date), D('ac' net.sourceforge.zmanim.AstronomicalCalendar)] */
    /* JADX INFO: Multiple debug info for r0v13 int: [D('remainingMin' int), D('lengthHour' int)] */
    /* JADX INFO: Multiple debug info for r11v8 java.util.Date: [D('c' java.util.Calendar), D('lengthDate' java.util.Date)] */
    static int SunSetRise(double latitude, double longitude) {
        long remaining;
        long length;
        int celestial;
        Log.d("d2d", "timezone: " + TimeZone.getDefault());
        AstronomicalCalendar ac = new AstronomicalCalendar(new GeoLocation(null, latitude, longitude, 0.0d, TimeZone.getDefault()));
        Date riseDate = ac.getSunrise();
        if (riseDate == null) {
            Log.e("d2d", "rise date was null");
        }
        Date setDate = ac.getSunset();
        if (setDate == null) {
            Log.e("d2d", "rise date was null");
        }
        Date nowDate = new Date();
        if (nowDate.getTime() <= riseDate.getTime() || nowDate.getTime() >= setDate.getTime()) {
            ac.getCalendar().add(6, 1);
            Date riseTomorrow = ac.getSunrise();
            long length2 = riseTomorrow.getTime() - setDate.getTime();
            remaining = riseTomorrow.getTime() - nowDate.getTime();
            length = length2;
            celestial = 0;
        } else {
            long length3 = setDate.getTime() - riseDate.getTime();
            remaining = setDate.getTime() - nowDate.getTime();
            length = length3;
            celestial = 1;
        }
        int remainingHour = (int) (remaining / ZmanimFormatter.HOUR_MILLIS);
        Calendar c = Calendar.getInstance();
        c.set(11, remainingHour);
        c.set(12, (int) ((remaining / 60000) - ((long) (remainingHour * 60))));
        Date remainingDate = c.getTime();
        int lengthHour = (int) (length / ZmanimFormatter.HOUR_MILLIS);
        Calendar c2 = Calendar.getInstance();
        c2.set(11, lengthHour);
        c2.set(12, (int) ((length / 60000) - ((long) (lengthHour * 60))));
        Date lengthDate = c2.getTime();
        Log.d("d2d", "sunrise: " + TIME_FORMAT.format(riseDate));
        Log.d("d2d", "sunset: " + TIME_FORMAT.format(setDate));
        Log.d("d2d", "length: " + DIFF_FORMAT.format(lengthDate));
        Log.d("d2d", "remaining: " + DIFF_FORMAT.format(remainingDate));
        return celestial;
    }
}
