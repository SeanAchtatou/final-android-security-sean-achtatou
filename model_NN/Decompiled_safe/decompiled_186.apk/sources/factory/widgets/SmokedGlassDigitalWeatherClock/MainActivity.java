package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Activity;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity {
    private static String feelslike;
    public static String humidity;
    private static String latitute;
    private static String longtitute;
    private static List<Map<String, String>> mForecasts;
    public static String skycode;
    public static String skytext;
    public static String temperature;
    private static String timezone;
    private static String weatherlocationname;
    public static String winddisplay;

    private static InputStream getWebWeatherSource(String code, String tempcode) throws IOException {
        return new URL("http://weather.msn.com/data.aspx?wealocations=wc:" + code + "&weadegreetype=" + tempcode).openStream();
    }

    private InputStream getLocalWeatherSource() throws IOException {
        return getAssets().open("beijing_msn.xml");
    }

    /* JADX INFO: Multiple debug info for r35v48 org.xmlpull.v1.XmlPullParserFactory: [D('factory' org.xmlpull.v1.XmlPullParserFactory), D('code' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r35v55 java.lang.String: [D('tagName' java.lang.String), D('eventType' int)] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x01dd  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x01f5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String[] parseWeather(java.lang.String r35, java.lang.String r36, long r37) {
        /*
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.mForecasts = r4
            r4 = 0
            java.io.InputStream r4 = getWebWeatherSource(r35, r36)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            org.xmlpull.v1.XmlPullParserFactory r35 = org.xmlpull.v1.XmlPullParserFactory.newInstance()     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r5 = 1
            r0 = r35
            r1 = r5
            r0.setNamespaceAware(r1)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            org.xmlpull.v1.XmlPullParser r5 = r35.newPullParser()     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            java.lang.String r35 = "UTF-8"
            r0 = r5
            r1 = r4
            r2 = r35
            r0.setInput(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            int r35 = r5.getEventType()     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
        L_0x0028:
            r6 = 1
            r0 = r35
            r1 = r6
            if (r0 != r1) goto L_0x02ee
            if (r4 == 0) goto L_0x0464
            r4.close()     // Catch:{ Exception -> 0x0460 }
            r35 = r4
        L_0x0035:
            java.util.List<java.util.Map<java.lang.String, java.lang.String>> r35 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.mForecasts
            r4 = 0
            r0 = r35
            r1 = r4
            java.lang.Object r35 = r0.get(r1)
            java.util.Map r35 = (java.util.Map) r35
            java.lang.String r4 = "low"
            r0 = r35
            r1 = r4
            java.lang.Object r5 = r0.get(r1)
            java.lang.String r5 = (java.lang.String) r5
            java.lang.String r4 = "high"
            r0 = r35
            r1 = r4
            java.lang.Object r4 = r0.get(r1)
            java.lang.String r4 = (java.lang.String) r4
            java.util.List<java.util.Map<java.lang.String, java.lang.String>> r35 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.mForecasts
            r6 = 1
            r0 = r35
            r1 = r6
            java.lang.Object r35 = r0.get(r1)
            java.util.Map r35 = (java.util.Map) r35
            java.lang.String r6 = "low"
            r0 = r35
            r1 = r6
            java.lang.Object r8 = r0.get(r1)
            java.lang.String r8 = (java.lang.String) r8
            java.lang.String r6 = "high"
            r0 = r35
            r1 = r6
            java.lang.Object r7 = r0.get(r1)
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r6 = "skycodeday"
            r0 = r35
            r1 = r6
            java.lang.Object r11 = r0.get(r1)
            java.lang.String r11 = (java.lang.String) r11
            java.lang.String r6 = "skytextday"
            r0 = r35
            r1 = r6
            java.lang.Object r12 = r0.get(r1)
            java.lang.String r12 = (java.lang.String) r12
            java.lang.String r6 = "date"
            r0 = r35
            r1 = r6
            java.lang.Object r6 = r0.get(r1)
            java.lang.String r6 = (java.lang.String) r6
            java.lang.String r9 = "precip"
            r0 = r35
            r1 = r9
            java.lang.Object r9 = r0.get(r1)
            java.lang.String r9 = (java.lang.String) r9
            java.lang.String r10 = "shortday"
            r0 = r35
            r1 = r10
            java.lang.Object r10 = r0.get(r1)
            java.lang.String r10 = (java.lang.String) r10
            java.util.List<java.util.Map<java.lang.String, java.lang.String>> r35 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.mForecasts
            r13 = 2
            r0 = r35
            r1 = r13
            java.lang.Object r35 = r0.get(r1)
            java.util.Map r35 = (java.util.Map) r35
            java.lang.String r13 = "low"
            r0 = r35
            r1 = r13
            java.lang.Object r15 = r0.get(r1)
            java.lang.String r15 = (java.lang.String) r15
            java.lang.String r13 = "high"
            r0 = r35
            r1 = r13
            java.lang.Object r14 = r0.get(r1)
            java.lang.String r14 = (java.lang.String) r14
            java.lang.String r13 = "skycodeday"
            r0 = r35
            r1 = r13
            java.lang.Object r18 = r0.get(r1)
            java.lang.String r18 = (java.lang.String) r18
            java.lang.String r13 = "skytextday"
            r0 = r35
            r1 = r13
            java.lang.Object r19 = r0.get(r1)
            java.lang.String r19 = (java.lang.String) r19
            java.lang.String r13 = "date"
            r0 = r35
            r1 = r13
            java.lang.Object r13 = r0.get(r1)
            java.lang.String r13 = (java.lang.String) r13
            java.lang.String r16 = "precip"
            r0 = r35
            r1 = r16
            java.lang.Object r16 = r0.get(r1)
            java.lang.String r16 = (java.lang.String) r16
            java.lang.String r17 = "shortday"
            r0 = r35
            r1 = r17
            java.lang.Object r17 = r0.get(r1)
            java.lang.String r17 = (java.lang.String) r17
            java.util.List<java.util.Map<java.lang.String, java.lang.String>> r35 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.mForecasts
            r20 = 3
            r0 = r35
            r1 = r20
            java.lang.Object r35 = r0.get(r1)
            java.util.Map r35 = (java.util.Map) r35
            java.lang.String r20 = "low"
            r0 = r35
            r1 = r20
            java.lang.Object r22 = r0.get(r1)
            java.lang.String r22 = (java.lang.String) r22
            java.lang.String r20 = "high"
            r0 = r35
            r1 = r20
            java.lang.Object r21 = r0.get(r1)
            java.lang.String r21 = (java.lang.String) r21
            java.lang.String r20 = "skycodeday"
            r0 = r35
            r1 = r20
            java.lang.Object r25 = r0.get(r1)
            java.lang.String r25 = (java.lang.String) r25
            java.lang.String r20 = "skytextday"
            r0 = r35
            r1 = r20
            java.lang.Object r26 = r0.get(r1)
            java.lang.String r26 = (java.lang.String) r26
            java.lang.String r20 = "date"
            r0 = r35
            r1 = r20
            java.lang.Object r20 = r0.get(r1)
            java.lang.String r20 = (java.lang.String) r20
            java.lang.String r23 = "precip"
            r0 = r35
            r1 = r23
            java.lang.Object r23 = r0.get(r1)
            java.lang.String r23 = (java.lang.String) r23
            java.lang.String r24 = "shortday"
            r0 = r35
            r1 = r24
            java.lang.Object r24 = r0.get(r1)
            java.lang.String r24 = (java.lang.String) r24
            java.util.List<java.util.Map<java.lang.String, java.lang.String>> r35 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.mForecasts
            r27 = 4
            r0 = r35
            r1 = r27
            java.lang.Object r35 = r0.get(r1)
            java.util.Map r35 = (java.util.Map) r35
            java.lang.String r27 = "low"
            r0 = r35
            r1 = r27
            java.lang.Object r29 = r0.get(r1)
            java.lang.String r29 = (java.lang.String) r29
            java.lang.String r27 = "high"
            r0 = r35
            r1 = r27
            java.lang.Object r28 = r0.get(r1)
            java.lang.String r28 = (java.lang.String) r28
            java.lang.String r27 = "skycodeday"
            r0 = r35
            r1 = r27
            java.lang.Object r32 = r0.get(r1)
            java.lang.String r32 = (java.lang.String) r32
            java.lang.String r27 = "skytextday"
            r0 = r35
            r1 = r27
            java.lang.Object r33 = r0.get(r1)
            java.lang.String r33 = (java.lang.String) r33
            java.lang.String r27 = "date"
            r0 = r35
            r1 = r27
            java.lang.Object r27 = r0.get(r1)
            java.lang.String r27 = (java.lang.String) r27
            java.lang.String r30 = "precip"
            r0 = r35
            r1 = r30
            java.lang.Object r30 = r0.get(r1)
            java.lang.String r30 = (java.lang.String) r30
            java.lang.String r31 = "shortday"
            r0 = r35
            r1 = r31
            java.lang.Object r31 = r0.get(r1)
            java.lang.String r31 = (java.lang.String) r31
            java.lang.String r35 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.skycode
            java.lang.String r34 = "44"
            r0 = r35
            r1 = r34
            boolean r35 = r0.equals(r1)
            if (r35 == 0) goto L_0x01ef
            java.lang.String r35 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.weatherlocationname
            java.lang.String[] r35 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity_google.parseWeatherGoogle(r35, r36)
            r36 = 0
            r36 = r35[r36]
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.skycode = r36
            r36 = 1
            r35 = r35[r36]
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.skytext = r35
        L_0x01ef:
            r35 = 1
            int r35 = (r37 > r35 ? 1 : (r37 == r35 ? 0 : -1))
            if (r35 != 0) goto L_0x0237
            java.lang.String r35 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.latitute
            double r35 = java.lang.Double.parseDouble(r35)
            java.lang.Double r35 = java.lang.Double.valueOf(r35)
            java.lang.String r36 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.longtitute
            double r36 = java.lang.Double.parseDouble(r36)
            java.lang.Double r36 = java.lang.Double.valueOf(r36)
            double r37 = r35.doubleValue()
            double r35 = r36.doubleValue()
            r0 = r37
            r2 = r35
            int r35 = factory.widgets.SmokedGlassDigitalWeatherClock.iconsorter.SunSetRise(r0, r2)
            if (r35 != 0) goto L_0x0237
            java.lang.String r35 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.skycode
            int r35 = java.lang.Integer.parseInt(r35)
            int r35 = factory.widgets.SmokedGlassDigitalWeatherClock.iconsorter.GetWeatherPic(r35)
            java.lang.String r35 = java.lang.Integer.toString(r35)
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.skycode = r35
            java.lang.String r35 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.skytext
            java.lang.String r36 = "(?i)Sunny"
            java.lang.String r37 = "Clear"
            java.lang.String r35 = r35.replaceAll(r36, r37)
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.skytext = r35
        L_0x0237:
            r35 = 39
            r0 = r35
            java.lang.String[] r0 = new java.lang.String[r0]
            r35 = r0
            r36 = 0
            java.lang.String r37 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.temperature
            r35[r36] = r37
            r36 = 1
            java.lang.String r37 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.skycode
            r35[r36] = r37
            r36 = 2
            java.lang.String r37 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.skytext
            r35[r36] = r37
            r36 = 3
            java.lang.String r37 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.humidity
            r35[r36] = r37
            r36 = 4
            java.lang.String r37 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.winddisplay
            r35[r36] = r37
            r36 = 5
            java.lang.String r37 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.weatherlocationname
            r35[r36] = r37
            r36 = 6
            java.lang.String r37 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.feelslike
            r35[r36] = r37
            r36 = 7
            r35[r36] = r8
            r36 = 8
            r35[r36] = r7
            r36 = 9
            r35[r36] = r11
            r36 = 10
            r35[r36] = r12
            r36 = 11
            r35[r36] = r6
            r36 = 12
            r35[r36] = r9
            r36 = 13
            r35[r36] = r10
            r36 = 14
            r35[r36] = r15
            r36 = 15
            r35[r36] = r14
            r36 = 16
            r35[r36] = r18
            r36 = 17
            r35[r36] = r19
            r36 = 18
            r35[r36] = r13
            r36 = 19
            r35[r36] = r16
            r36 = 20
            r35[r36] = r17
            r36 = 21
            r35[r36] = r22
            r36 = 22
            r35[r36] = r21
            r36 = 23
            r35[r36] = r25
            r36 = 24
            r35[r36] = r26
            r36 = 25
            r35[r36] = r20
            r36 = 26
            r35[r36] = r23
            r36 = 27
            r35[r36] = r24
            r36 = 28
            r35[r36] = r29
            r36 = 29
            r35[r36] = r28
            r36 = 30
            r35[r36] = r32
            r36 = 31
            r35[r36] = r33
            r36 = 32
            r35[r36] = r27
            r36 = 33
            r35[r36] = r30
            r36 = 34
            r35[r36] = r31
            r36 = 35
            r35[r36] = r5
            r36 = 36
            r35[r36] = r4
            r36 = 37
            java.lang.String r37 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.latitute
            r35[r36] = r37
            r36 = 38
            java.lang.String r37 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.longtitute
            r35[r36] = r37
            return r35
        L_0x02ee:
            if (r35 == 0) goto L_0x038e
            r6 = 1
            r0 = r35
            r1 = r6
            if (r0 == r1) goto L_0x038e
            r6 = 2
            r0 = r35
            r1 = r6
            if (r0 != r1) goto L_0x042b
            java.lang.String r35 = r5.getName()     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            java.lang.String r6 = "weather"
            r0 = r6
            r1 = r35
            boolean r6 = r0.equals(r1)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            if (r6 == 0) goto L_0x032f
            r6 = 0
            java.lang.String r7 = "weatherlocationname"
            java.lang.String r6 = r5.getAttributeValue(r6, r7)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.weatherlocationname = r6     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r6 = 0
            java.lang.String r7 = "lat"
            java.lang.String r6 = r5.getAttributeValue(r6, r7)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.latitute = r6     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r6 = 0
            java.lang.String r7 = "long"
            java.lang.String r6 = r5.getAttributeValue(r6, r7)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.longtitute = r6     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r6 = 0
            java.lang.String r7 = "timezone"
            java.lang.String r6 = r5.getAttributeValue(r6, r7)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.timezone = r6     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
        L_0x032f:
            java.lang.String r6 = "current"
            r0 = r6
            r1 = r35
            boolean r6 = r0.equals(r1)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            if (r6 == 0) goto L_0x0394
            r35 = 0
            java.lang.String r6 = "temperature"
            r0 = r5
            r1 = r35
            r2 = r6
            java.lang.String r35 = r0.getAttributeValue(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.temperature = r35     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r35 = 0
            java.lang.String r6 = "skycode"
            r0 = r5
            r1 = r35
            r2 = r6
            java.lang.String r35 = r0.getAttributeValue(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.skycode = r35     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r35 = 0
            java.lang.String r6 = "skytext"
            r0 = r5
            r1 = r35
            r2 = r6
            java.lang.String r35 = r0.getAttributeValue(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.skytext = r35     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r35 = 0
            java.lang.String r6 = "humidity"
            r0 = r5
            r1 = r35
            r2 = r6
            java.lang.String r35 = r0.getAttributeValue(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.humidity = r35     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r35 = 0
            java.lang.String r6 = "winddisplay"
            r0 = r5
            r1 = r35
            r2 = r6
            java.lang.String r35 = r0.getAttributeValue(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.winddisplay = r35     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r35 = 0
            java.lang.String r6 = "feelslike"
            r0 = r5
            r1 = r35
            r2 = r6
            java.lang.String r35 = r0.getAttributeValue(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.feelslike = r35     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
        L_0x038e:
            int r35 = r5.next()     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            goto L_0x0028
        L_0x0394:
            java.lang.String r6 = "forecast"
            r0 = r6
            r1 = r35
            boolean r35 = r0.equals(r1)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            if (r35 == 0) goto L_0x038e
            java.util.HashMap r35 = new java.util.HashMap     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r35.<init>()     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            java.lang.String r6 = "low"
            r7 = 0
            java.lang.String r8 = "low"
            java.lang.String r7 = r5.getAttributeValue(r7, r8)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r0 = r35
            r1 = r6
            r2 = r7
            r0.put(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            java.lang.String r6 = "high"
            r7 = 0
            java.lang.String r8 = "high"
            java.lang.String r7 = r5.getAttributeValue(r7, r8)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r0 = r35
            r1 = r6
            r2 = r7
            r0.put(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            java.lang.String r6 = "skycodeday"
            r7 = 0
            java.lang.String r8 = "skycodeday"
            java.lang.String r7 = r5.getAttributeValue(r7, r8)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r0 = r35
            r1 = r6
            r2 = r7
            r0.put(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            java.lang.String r6 = "skytextday"
            r7 = 0
            java.lang.String r8 = "skytextday"
            java.lang.String r7 = r5.getAttributeValue(r7, r8)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r0 = r35
            r1 = r6
            r2 = r7
            r0.put(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            java.lang.String r6 = "date"
            r7 = 0
            java.lang.String r8 = "date"
            java.lang.String r7 = r5.getAttributeValue(r7, r8)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r0 = r35
            r1 = r6
            r2 = r7
            r0.put(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            java.lang.String r6 = "precip"
            r7 = 0
            java.lang.String r8 = "precip"
            java.lang.String r7 = r5.getAttributeValue(r7, r8)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r0 = r35
            r1 = r6
            r2 = r7
            r0.put(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            java.lang.String r6 = "shortday"
            r7 = 0
            java.lang.String r8 = "day"
            java.lang.String r7 = r5.getAttributeValue(r7, r8)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r0 = r35
            r1 = r6
            r2 = r7
            r0.put(r1, r2)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            java.util.List<java.util.Map<java.lang.String, java.lang.String>> r6 = factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.mForecasts     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            r0 = r6
            r1 = r35
            r0.add(r1)     // Catch:{ IOException -> 0x041e, XmlPullParserException -> 0x043b }
            goto L_0x038e
        L_0x041e:
            r35 = move-exception
            r35.printStackTrace()     // Catch:{ all -> 0x0450 }
            if (r4 == 0) goto L_0x0464
            r4.close()     // Catch:{ Exception -> 0x0433 }
            r35 = r4
            goto L_0x0035
        L_0x042b:
            r6 = 3
            r0 = r35
            r1 = r6
            if (r0 == r1) goto L_0x038e
            goto L_0x038e
        L_0x0433:
            r35 = move-exception
            r35.printStackTrace()
            r35 = r4
            goto L_0x0035
        L_0x043b:
            r35 = move-exception
            r35.printStackTrace()     // Catch:{ all -> 0x0450 }
            if (r4 == 0) goto L_0x0464
            r4.close()     // Catch:{ Exception -> 0x0448 }
            r35 = r4
            goto L_0x0035
        L_0x0448:
            r35 = move-exception
            r35.printStackTrace()
            r35 = r4
            goto L_0x0035
        L_0x0450:
            r35 = move-exception
            r36 = r35
            r35 = r4
            if (r35 == 0) goto L_0x045a
            r35.close()     // Catch:{ Exception -> 0x045b }
        L_0x045a:
            throw r36
        L_0x045b:
            r35 = move-exception
            r35.printStackTrace()
            goto L_0x045a
        L_0x0460:
            r35 = move-exception
            r35.printStackTrace()
        L_0x0464:
            r35 = r4
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: factory.widgets.SmokedGlassDigitalWeatherClock.MainActivity.parseWeather(java.lang.String, java.lang.String, long):java.lang.String[]");
    }
}
