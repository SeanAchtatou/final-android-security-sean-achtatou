package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class LazyLoadMainActivity extends Activity {
    LazyAdapter adapter;
    public String filepup;
    URL fullurl = null;
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
        }
    };
    ListView list;
    public View.OnClickListener listener = new View.OnClickListener() {
        public void onClick(View arg0) {
            LazyLoadMainActivity.this.adapter.imageLoader.clearCache();
            LazyLoadMainActivity.this.adapter.notifyDataSetChanged();
        }
    };
    public View.OnClickListener listener2 = new View.OnClickListener() {
        public void onClick(View arg0) {
            LazyLoadMainActivity.this.finish();
        }
    };
    private ProgressDialog pd;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lazymain);
        File file = new File(Environment.getExternalStorageDirectory() + "/factory.widgets.skins/");
        if (!file.exists() && !file.isDirectory()) {
            new File(String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/factory.widgets.skins/").mkdir();
        }
        File file2 = new File(Environment.getExternalStorageDirectory() + "/factory.widgets.skins/large");
        if (!file2.exists() && !file2.isDirectory()) {
            new File(String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/factory.widgets.skins/large").mkdir();
        }
        try {
            ImageManager.DownloadFromUrl("http://sites.google.com/site/factorywidgets/sense-clock-skins/down.conf", "down.conf");
        } catch (Exception e) {
        }
        ArrayList test = readFile(new File(Environment.getExternalStorageDirectory() + "/factory.widgets.skins/large/down.conf").toString());
        Object[] ia = test.toArray();
        String[] items = new String[ia.length];
        for (int i = 0; i < ia.length; i++) {
            items[i] = (String) ia[i];
        }
        test.clear();
        this.list = (ListView) findViewById(R.id.list);
        this.adapter = new LazyAdapter(this, items);
        this.list.setAdapter((ListAdapter) this.adapter);
        final String[] strArr = items;
        this.list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                try {
                    LazyLoadMainActivity.this.fullurl = new URL(strArr[arg2]);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                String[] pathparts = LazyLoadMainActivity.this.fullurl.getPath().split("\\/");
                String filename = pathparts[pathparts.length - 1].split("\\.", 1)[0];
                String displayname = filename.replaceFirst("\\.png", "").replaceAll("_", " ");
                LazyLoadMainActivity.this.filepup = filename;
                AlertDialog.Builder adb = new AlertDialog.Builder(LazyLoadMainActivity.this);
                adb.setTitle("Download Skin");
                adb.setMessage("Press 'OK' to download\n" + displayname);
                adb.setIcon((int) R.drawable.down32);
                adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Toast.makeText(LazyLoadMainActivity.this.getApplicationContext(), "Downloading...", 0).show();
                        ImageManager.DownloadFromUrl(LazyLoadMainActivity.this.fullurl.toString(), LazyLoadMainActivity.this.filepup);
                    }
                });
                adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Toast.makeText(LazyLoadMainActivity.this.getApplicationContext(), "Cancelled", 0).show();
                    }
                });
                adb.show();
            }
        });
        ((Button) findViewById(R.id.button1)).setOnClickListener(this.listener);
        ((Button) findViewById(R.id.button2)).setOnClickListener(this.listener2);
    }

    public void onDestroy() {
        this.adapter.imageLoader.stopThread();
        this.list.setAdapter((ListAdapter) null);
        super.onDestroy();
    }

    public static ArrayList readFile(String fileName) {
        ArrayList data = new ArrayList();
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName), 8192);
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                data.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            System.out.println(e2);
        }
        return data;
    }
}
