package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MyBroadcastReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        new CountdownWidget().onReceive(context, intent);
        System.out.println("time changed receiver");
    }
}
