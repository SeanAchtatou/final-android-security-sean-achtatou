package factory.widgets.SmokedGlassDigitalWeatherClock;

import android.app.ActivityManager;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.StatFs;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CountdownService extends Service {
    public static String APPWIDGET_CONFIGURE = "ConfigureWidget";
    private static final String COLOR_PREFERENCE_KEY = null;
    public static String KEY_CHARGING = "BATWIDG_CHARGING";
    public static String KEY_LEVEL = "BATWIDG_LEVEL";
    public static final String KEY_TEMP = "BATWIDG_TEMP";
    public static String KEY_VOLTAGE = "BATWIDG_VOLTAGE";
    public static final String LAUNCHDLG = "launchdlg";
    public static final String LAUNCHME = "launchme";
    public static final String LAUNCHMEMINS = "launchmemins";
    public static final String MINUS = "minus";
    public static final long MODIFY = 86400000;
    public static final String PLUS = "plus";
    public static final String PLUSPLUS = "plusplus";
    public static String PREFS_NAME = "BATWIDG_PREFS";
    public static final String UPDATE = "update";
    static String[] fbwf = {"n/a", "44", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "44", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "44", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "44", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "44", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a", "n/a"};
    static int missedit = 1;
    static String[] wf = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
    private TextView SDfree;
    private int baticon;
    private Date currentTime;
    BatteryInfo mBI = null;
    private int weathertimer = 0;
    private String wsymbol;

    public static void setMissedit(int missedit2) {
        missedit = missedit2;
    }

    public static void setWf(String[] wf2) {
        wf = wf2;
    }

    public static String[] getWf() {
        return wf;
    }

    public void onStart(Intent intent, int startId) {
        int skycode;
        int skycode2;
        int skycode3;
        int skycode4;
        String command = intent.getAction();
        int appWidgetId = intent.getExtras().getInt("appWidgetId");
        RemoteViews remoteViews = new RemoteViews(getApplicationContext().getPackageName(), R.layout.countdownwidget);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("prefs", 0);
        int colwheelmin = prefs.getInt("colormin" + appWidgetId, -1);
        int colwheelhr = prefs.getInt("colorhr" + appWidgetId, -1);
        int colorother = prefs.getInt("colorother" + appWidgetId, -1);
        long checked = prefs.getLong("checked" + appWidgetId, 0);
        long checkedsys = prefs.getLong("checkedsys" + appWidgetId, 0);
        long checkedbattempf = prefs.getLong("checkedbattempf" + appWidgetId, 0);
        long checkednextalarm = prefs.getLong("checkednextalarm" + appWidgetId, 0);
        long checkedmoonphase = prefs.getLong("checkedmoonphase" + appWidgetId, 0);
        long checkedweatherenabled = prefs.getLong("checkedweatherenabled" + appWidgetId, 0);
        long checkedweathertempf = prefs.getLong("checkedweathertempf" + appWidgetId, 0);
        int weatherUpdatedtimer = prefs.getInt("weatherUpdatedtimer" + appWidgetId, 1);
        String weatherCode = prefs.getString("weatherCode" + appWidgetId, "WEATHER_CODE_EMPTY");
        String weatherName = prefs.getString("weatherName" + appWidgetId, "WEATHER_CODE_NAME");
        String launcherPackage1 = prefs.getString("launcherPackage1", "LAUCHER_CODE_EMPTY");
        String launcherClass1 = prefs.getString("launcherClass1", "LAUCHER_CODE_EMPTY");
        String launcherPackage2 = prefs.getString("launcherPackage2", "LAUCHER_CODE_EMPTY");
        String launcherClass2 = prefs.getString("launcherClass2", "LAUCHER_CODE_EMPTY");
        String skinchoice = prefs.getString("skinchoice", "SKIN_CHOICE_EMPTY");
        long checkednoweatherdetail = prefs.getLong("checkednoweatherdetail" + appWidgetId, 0);
        long checkedweatherbottom = prefs.getLong("checkedweatherbottom" + appWidgetId, 0);
        long checkedsamecolor = prefs.getLong("checkedsamecolor" + appWidgetId, 0);
        long checkednoskin = prefs.getLong("checkednoskin" + appWidgetId, 0);
        long forecastalter = prefs.getLong("checkedforecastalter" + appWidgetId, 0);
        long checkedsunsetrise = prefs.getLong("checkedsunsetrise" + appWidgetId, 0);
        long checkedbathrstab = prefs.getLong("checkedbathrstab" + appWidgetId, 0);
        if (checkedsamecolor == 1) {
            colwheelmin = colwheelhr;
        }
        if (checkedweatherenabled == 1 && checkedweatherbottom == 1) {
            checkednoweatherdetail = 1;
            checkedsys = 0;
        }
        if (weatherCode == null || weatherCode == "WEATHER_CODE_EMPTY") {
            checkedweatherenabled = 0;
        }
        if (checkedweathertempf == 1) {
            this.wsymbol = "F";
        } else {
            this.wsymbol = "C";
        }
        if (command.equals(PLUS)) {
            if (forecastalter == 1) {
                Bundle bun = new Bundle();
                bun.putString("weatherCode", weatherCode);
                bun.putString("wsymbol", this.wsymbol);
                bun.putInt("appWidgetId", appWidgetId);
                bun.putLong("checkedsunsetrise", checkedsunsetrise);
                Intent intent2 = new Intent(this, ForecastActivityAlter.class);
                intent2.setFlags(268435456);
                intent2.putExtras(bun);
                intent2.putExtra("appWidgetId", appWidgetId);
                startActivity(intent2);
            } else {
                Bundle bun2 = new Bundle();
                bun2.putString("weatherCode", weatherCode);
                bun2.putString("wsymbol", this.wsymbol);
                bun2.putInt("appWidgetId", appWidgetId);
                bun2.putLong("checkedsunsetrise", checkedsunsetrise);
                Intent intent3 = new Intent(this, ForecastActivity.class);
                intent3.setFlags(268435456);
                intent3.putExtras(bun2);
                intent3.putExtra("appWidgetId", appWidgetId);
                startActivity(intent3);
            }
        } else if (command.equals(PLUSPLUS)) {
            if (forecastalter == 1) {
                Bundle bun3 = new Bundle();
                bun3.putString("weatherCode", weatherCode);
                bun3.putString("wsymbol", this.wsymbol);
                bun3.putInt("appWidgetId", appWidgetId);
                bun3.putLong("checkedsunsetrise", checkedsunsetrise);
                Intent intent4 = new Intent(this, ForecastActivityAlter.class);
                intent4.setFlags(268435456);
                intent4.putExtras(bun3);
                intent4.putExtra("appWidgetId", appWidgetId);
                startActivity(intent4);
            } else {
                Bundle bun4 = new Bundle();
                bun4.putString("weatherCode", weatherCode);
                bun4.putString("wsymbol", this.wsymbol);
                bun4.putInt("appWidgetId", appWidgetId);
                bun4.putLong("checkedsunsetrise", checkedsunsetrise);
                Intent intent5 = new Intent(this, ForecastActivity.class);
                intent5.setFlags(268435456);
                intent5.putExtras(bun4);
                intent5.putExtra("appWidgetId", appWidgetId);
                startActivity(intent5);
            }
        } else if (command.equals(MINUS)) {
            Bundle config = new Bundle();
            config.putLong("checkednextalarm", checkednextalarm);
            config.putLong("checked", checked);
            config.putLong("checkedsys", checkedsys);
            config.putLong("checkedbattempf", checkedbattempf);
            config.putLong("checkedmoonphase", checkedmoonphase);
            config.putLong("checkedweatherenabled", checkedweatherenabled);
            config.putLong("checkedweathertempf", checkedweathertempf);
            config.putString("weatherCode", weatherCode);
            config.putInt("weatherUpdatedtimer", weatherUpdatedtimer);
            config.putLong("colwheelmin", (long) colwheelmin);
            config.putLong("colwheelhr", (long) colwheelhr);
            config.putLong("colorother", (long) colorother);
            config.putLong("checkednoweatherdetail", checkednoweatherdetail);
            config.putString("weatherName", weatherName);
            config.putLong("checkedweatherbottom", checkedweatherbottom);
            config.putLong("checkedsamecolor", checkedsamecolor);
            config.putLong("checkednoskin", checkednoskin);
            config.putLong("forecastalter", forecastalter);
            config.putLong("checkedsunsetrise", checkedsunsetrise);
            config.putLong("checkedbathrstab", checkedbathrstab);
            Intent intent6 = new Intent(this, CountdownConfiguration.class);
            intent6.setFlags(268435456);
            intent6.putExtra("appWidgetId", appWidgetId);
            intent6.putExtras(config);
            startActivity(intent6);
        } else if (command.equals(LAUNCHME)) {
            if (launcherPackage1 != "LAUCHER_CODE_EMPTY") {
                Intent intent7 = new Intent("android.intent.action.MAIN");
                intent7.addCategory("android.intent.category.LAUNCHER");
                intent7.setFlags(270532608);
                intent7.setComponent(new ComponentName(launcherPackage1, launcherClass1));
                startActivity(intent7);
            } else if (launcherPackage1 == "LAUCHER_CODE_EMPTY") {
                Toast.makeText(getApplicationContext(), "no application configured to be launched", 0).show();
            }
        } else if (command.equals(LAUNCHMEMINS)) {
            if (launcherPackage2 != "LAUCHER_CODE_EMPTY") {
                Intent intent8 = new Intent("android.intent.action.MAIN");
                intent8.addCategory("android.intent.category.LAUNCHER");
                intent8.setFlags(270532608);
                intent8.setComponent(new ComponentName(launcherPackage2, launcherClass2));
                startActivity(intent8);
            } else if (launcherPackage2 == "LAUCHER_CODE_EMPTY") {
                Toast.makeText(getApplicationContext(), "no application configured to be launched", 0).show();
            }
        } else if (command.equals(LAUNCHDLG)) {
            Intent intent9 = new Intent(this, dlg_sysinfo.class);
            intent9.setFlags(268435456);
            intent9.putExtra("appWidgetId", appWidgetId);
            startActivity(intent9);
        }
        if (checkedweatherenabled == 1) {
            String wsymbol2 = "C";
            if (checkedweathertempf == 1) {
                wsymbol2 = "F";
            }
            this.weathertimer = this.weathertimer + 1;
            int[] imgWeatherIds = {R.drawable.skycode0, R.drawable.skycode1, R.drawable.skycode2, R.drawable.skycode3, R.drawable.skycode4, R.drawable.skycode5, R.drawable.skycode6, R.drawable.skycode7, R.drawable.skycode8, R.drawable.skycode9, R.drawable.skycode10, R.drawable.skycode11, R.drawable.skycode12, R.drawable.skycode13, R.drawable.skycode14, R.drawable.skycode15, R.drawable.skycode16, R.drawable.skycode17, R.drawable.skycode18, R.drawable.skycode19, R.drawable.skycode20, R.drawable.skycode21, R.drawable.skycode22, R.drawable.skycode23, R.drawable.skycode24, R.drawable.skycode25, R.drawable.skycode26, R.drawable.skycode27, R.drawable.skycode28, R.drawable.skycode29, R.drawable.skycode30, R.drawable.skycode31, R.drawable.skycode32, R.drawable.skycode33, R.drawable.skycode34, R.drawable.skycode35, R.drawable.skycode36, R.drawable.skycode37, R.drawable.skycode38, R.drawable.skycode39, R.drawable.skycode40, R.drawable.skycode41, R.drawable.skycode42, R.drawable.skycode43, R.drawable.skycode44, R.drawable.skycode45, R.drawable.skycode46, R.drawable.skycode47};
            if (!netCheckerHelper()) {
                if (this.weathertimer >= weatherUpdatedtimer) {
                    missedit = 1;
                }
                try {
                    wf = stringToArray(prefs.getString("weatherValues", "WEATHER_STRING_EMPTY"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (wf[1].equals(null) || wf[1].equals("")) {
                    skycode4 = 44;
                } else {
                    skycode4 = Integer.parseInt(wf[1]);
                }
                if (checkedweatherbottom == 1) {
                    remoteViews.setViewVisibility(R.id.weather_image, 4);
                    remoteViews.setViewVisibility(R.id.weather_text, 4);
                    remoteViews.setViewVisibility(R.id.weather_image_loc_bottom, 0);
                    remoteViews.setImageViewResource(R.id.weather_image_loc_bottom, imgWeatherIds[skycode4]);
                    remoteViews.setTextViewText(R.id.weather_loc_bottom, weatherName);
                    remoteViews.setTextColor(R.id.weather_loc_bottom, colorother);
                    remoteViews.setTextViewText(R.id.weather_text_bottom, wf[2]);
                    remoteViews.setTextColor(R.id.weather_text_bottom, colorother);
                    remoteViews.setTextViewText(R.id.weather_temp_now_bottom, String.valueOf(wf[0]) + "°");
                    remoteViews.setTextColor(R.id.weather_temp_now_bottom, colorother);
                    remoteViews.setTextViewText(R.id.weather_hl_bottom, "H:" + wf[36] + "°" + "\n" + "L:" + wf[35] + "°");
                    remoteViews.setTextColor(R.id.weather_hl_bottom, colorother);
                } else {
                    remoteViews.setViewVisibility(R.id.weather_image, 0);
                    remoteViews.setViewVisibility(R.id.weather_text, 0);
                    remoteViews.setViewVisibility(R.id.weather_image_loc_bottom, 4);
                    remoteViews.setImageViewResource(R.id.weather_image, imgWeatherIds[skycode4]);
                    remoteViews.setTextViewText(R.id.weather_text, String.valueOf(wf[2]) + " , " + wf[0] + "°" + wsymbol2 + " , H:" + wf[36] + "°" + " L:" + wf[35] + "°");
                    remoteViews.setTextColor(R.id.weather_text, colorother);
                }
            }
            if (netCheckerHelper()) {
                if (missedit == 1) {
                    try {
                        wf = MainActivity.parseWeather(weatherCode, wsymbol2, checkedsunsetrise);
                        if (!(wf == null && wf.length == 0)) {
                            String weatherValues = arrayToString(wf, "=");
                            SharedPreferences.Editor edit = prefs.edit();
                            edit.putString("weatherValues", weatherValues);
                            edit.commit();
                        }
                        if (wf[1].equals(null) || wf[1].equals("")) {
                            skycode3 = 44;
                        } else {
                            skycode3 = Integer.parseInt(wf[1]);
                        }
                        if (checkedweatherbottom == 1) {
                            remoteViews.setViewVisibility(R.id.weather_image, 4);
                            remoteViews.setViewVisibility(R.id.weather_text, 4);
                            remoteViews.setViewVisibility(R.id.weather_image_loc_bottom, 0);
                            remoteViews.setImageViewResource(R.id.weather_image_loc_bottom, imgWeatherIds[skycode3]);
                            remoteViews.setTextViewText(R.id.weather_loc_bottom, weatherName);
                            remoteViews.setTextColor(R.id.weather_loc_bottom, colorother);
                            remoteViews.setTextViewText(R.id.weather_text_bottom, wf[2]);
                            remoteViews.setTextColor(R.id.weather_text_bottom, colorother);
                            remoteViews.setTextViewText(R.id.weather_temp_now_bottom, String.valueOf(wf[0]) + "°");
                            remoteViews.setTextColor(R.id.weather_temp_now_bottom, colorother);
                            remoteViews.setTextViewText(R.id.weather_hl_bottom, "H:" + wf[36] + "°" + "\n" + "L:" + wf[35] + "°");
                            remoteViews.setTextColor(R.id.weather_hl_bottom, colorother);
                        } else {
                            remoteViews.setViewVisibility(R.id.weather_image, 0);
                            remoteViews.setViewVisibility(R.id.weather_text, 0);
                            remoteViews.setViewVisibility(R.id.weather_image_loc_bottom, 4);
                            remoteViews.setImageViewResource(R.id.weather_image, imgWeatherIds[skycode3]);
                            remoteViews.setTextViewText(R.id.weather_text, String.valueOf(wf[2]) + " , " + wf[0] + "°" + wsymbol2 + " , H:" + wf[36] + "°" + " L:" + wf[35] + "°");
                            remoteViews.setTextColor(R.id.weather_text, colorother);
                        }
                        this.weathertimer = 0;
                        missedit = 0;
                    } catch (Exception e2) {
                        Log.i("Exception", "No weather found found from missedit = " + e2);
                        missedit = 1;
                    }
                } else if (this.weathertimer >= weatherUpdatedtimer) {
                    try {
                        wf = MainActivity.parseWeather(weatherCode, wsymbol2, checkedsunsetrise);
                        if (!(wf == null && wf.length == 0)) {
                            String weatherValues2 = arrayToString(wf, "=");
                            SharedPreferences.Editor edit2 = prefs.edit();
                            edit2.putString("weatherValues", weatherValues2);
                            edit2.commit();
                        }
                        if (wf[1].equals(null) || wf[1].equals("")) {
                            skycode2 = 44;
                        } else {
                            skycode2 = Integer.parseInt(wf[1]);
                        }
                        if (checkedweatherbottom == 1) {
                            remoteViews.setViewVisibility(R.id.weather_image, 4);
                            remoteViews.setViewVisibility(R.id.weather_text, 4);
                            remoteViews.setViewVisibility(R.id.weather_image_loc_bottom, 0);
                            remoteViews.setImageViewResource(R.id.weather_image_loc_bottom, imgWeatherIds[skycode2]);
                            remoteViews.setTextViewText(R.id.weather_loc_bottom, wf[5]);
                            remoteViews.setTextColor(R.id.weather_loc_bottom, colorother);
                            remoteViews.setTextViewText(R.id.weather_text_bottom, wf[2]);
                            remoteViews.setTextColor(R.id.weather_text_bottom, colorother);
                            remoteViews.setTextViewText(R.id.weather_temp_now_bottom, String.valueOf(wf[0]) + "°");
                            remoteViews.setTextColor(R.id.weather_temp_now_bottom, colorother);
                            remoteViews.setTextViewText(R.id.weather_hl_bottom, "H:" + wf[36] + "°" + "\n" + "L:" + wf[35] + "°");
                            remoteViews.setTextColor(R.id.weather_hl_bottom, colorother);
                        } else {
                            remoteViews.setViewVisibility(R.id.weather_image, 0);
                            remoteViews.setViewVisibility(R.id.weather_text, 0);
                            remoteViews.setViewVisibility(R.id.weather_image_loc_bottom, 4);
                            remoteViews.setImageViewResource(R.id.weather_image, imgWeatherIds[skycode2]);
                            remoteViews.setTextViewText(R.id.weather_text, String.valueOf(wf[2]) + " , " + wf[0] + "°" + wsymbol2 + " , H:" + wf[36] + "°" + " L:" + wf[35] + "°");
                            remoteViews.setTextColor(R.id.weather_text, colorother);
                        }
                        this.weathertimer = 0;
                        missedit = 0;
                    } catch (Exception e3) {
                        Log.i("Exception", "No weather found found fro timer= " + e3);
                        missedit = 1;
                    }
                } else {
                    try {
                        wf = stringToArray(prefs.getString("weatherValues", "WEATHER_STRING_EMPTY"));
                        if (wf == null || wf.length == 0) {
                            wf = (String[]) fbwf.clone();
                        }
                        if (wf[1].equals(null) || wf[1].equals("")) {
                            skycode = 44;
                        } else {
                            skycode = Integer.parseInt(wf[1]);
                        }
                        if (checkedweatherbottom == 1) {
                            remoteViews.setViewVisibility(R.id.weather_image, 4);
                            remoteViews.setViewVisibility(R.id.weather_text, 4);
                            remoteViews.setViewVisibility(R.id.weather_image_loc_bottom, 0);
                            remoteViews.setImageViewResource(R.id.weather_image_loc_bottom, imgWeatherIds[skycode]);
                            remoteViews.setTextViewText(R.id.weather_loc_bottom, weatherName);
                            remoteViews.setTextColor(R.id.weather_loc_bottom, colorother);
                            remoteViews.setTextViewText(R.id.weather_text_bottom, wf[2]);
                            remoteViews.setTextColor(R.id.weather_text_bottom, colorother);
                            remoteViews.setTextViewText(R.id.weather_temp_now_bottom, String.valueOf(wf[0]) + "°");
                            remoteViews.setTextColor(R.id.weather_temp_now_bottom, colorother);
                            remoteViews.setTextViewText(R.id.weather_hl_bottom, "H:" + wf[36] + "°" + "\n" + "L:" + wf[35] + "°");
                            remoteViews.setTextColor(R.id.weather_hl_bottom, colorother);
                        } else {
                            remoteViews.setViewVisibility(R.id.weather_image, 0);
                            remoteViews.setViewVisibility(R.id.weather_text, 0);
                            remoteViews.setViewVisibility(R.id.weather_image_loc_bottom, 4);
                            remoteViews.setImageViewResource(R.id.weather_image, imgWeatherIds[skycode]);
                            remoteViews.setTextViewText(R.id.weather_text, String.valueOf(wf[2]) + " , " + wf[0] + "°" + wsymbol2 + " , H:" + wf[36] + "°" + " L:" + wf[35] + "°");
                            remoteViews.setTextColor(R.id.weather_text, colorother);
                        }
                    } catch (Exception e4) {
                        Log.i("Exception", "No weather found from else= " + e4);
                    }
                }
            }
        } else if (checkedweatherenabled == 0) {
            remoteViews.setViewVisibility(R.id.weather_image_loc_bottom, 4);
            remoteViews.setViewVisibility(R.id.weather_temp_now_bottom, 4);
            remoteViews.setViewVisibility(R.id.weather_loc_bottom, 4);
            remoteViews.setViewVisibility(R.id.weather_hl_bottom, 4);
            remoteViews.setViewVisibility(R.id.weather_text_bottom, 4);
            remoteViews.setViewVisibility(R.id.weather_image, 4);
            remoteViews.setViewVisibility(R.id.weather_text, 4);
        }
        if (this.mBI == null) {
            this.mBI = new BatteryInfo();
            IntentFilter mIntentFilter = new IntentFilter();
            mIntentFilter.addAction("android.intent.action.BATTERY_CHANGED");
            registerReceiver(this.mBI, mIntentFilter);
        }
        int level = 0;
        boolean charging = false;
        int battTemp = 0;
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        if (settings != null) {
            level = settings.getInt(KEY_LEVEL, 0);
            if (settings.getInt(KEY_CHARGING, 1) == 2) {
                charging = true;
            } else {
                charging = false;
            }
            battTemp = settings.getInt(KEY_TEMP, 0);
        }
        DecimalFormat decimalFormat = new DecimalFormat("#");
        int tens = battTemp / 10;
        String ct = String.valueOf(Integer.toString(tens)) + "." + (battTemp - (tens * 10));
        int tens2 = (battTemp * 18) / 100;
        String ft = String.valueOf(Integer.toString(tens2 + 32)) + "." + decimalFormat.format((long) (((battTemp * 18) - (tens2 * 100)) / 10));
        String cTemp = String.valueOf(ct) + "°C";
        String fTemp = String.valueOf(ft) + "°F";
        String levelText = String.valueOf(level) + "%";
        if (level == 0) {
            levelText = "0%";
        }
        String chargestatus = "";
        if (charging) {
            chargestatus = "+";
        }
        String sdfree = getsdfree();
        long memfree = getramfree();
        String intfree = getinternalfree();
        if (checkedbathrstab == 1) {
            this.baticon = R.drawable.bat000 + level;
            remoteViews.setViewVisibility(R.id.bathrstab, 0);
            remoteViews.setImageViewResource(R.id.bathrstab, this.baticon);
        } else if (checkedbathrstab == 0) {
            remoteViews.setViewVisibility(R.id.bathrstab, 4);
        }
        this.currentTime = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("a");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("h");
        SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("mm");
        SimpleDateFormat simpleDateFormat4 = new SimpleDateFormat("d");
        SimpleDateFormat simpleDateFormat5 = new SimpleDateFormat("HH");
        SimpleDateFormat yr = new SimpleDateFormat("yyyy");
        SimpleDateFormat simpleDateFormat6 = new SimpleDateFormat("M");
        if (checkedmoonphase == 1) {
            int[] imgSizeIds = {R.drawable.moon0, R.drawable.moon1, R.drawable.moon2, R.drawable.moon3, R.drawable.moon4, R.drawable.moon5, R.drawable.moon5, R.drawable.moon7, R.drawable.moon8, R.drawable.moon9, R.drawable.moon10, R.drawable.moon11, R.drawable.moon12, R.drawable.moon13, R.drawable.moon14, R.drawable.moon15, R.drawable.moon16, R.drawable.moon17, R.drawable.moon18, R.drawable.moon19, R.drawable.moon20, R.drawable.moon21, R.drawable.moon22, R.drawable.moon23, R.drawable.moon24, R.drawable.moon25, R.drawable.moon26, R.drawable.moon27, R.drawable.moon28, R.drawable.moon29, R.drawable.moon30};
            int moonPhase = conway(Integer.parseInt(yr.format(this.currentTime)), Integer.parseInt(simpleDateFormat6.format(this.currentTime)), Integer.parseInt(simpleDateFormat4.format(this.currentTime)));
            remoteViews.setViewVisibility(R.id.imgmoon, 0);
            remoteViews.setImageViewResource(R.id.imgmoon, imgSizeIds[moonPhase]);
        } else if (checkedmoonphase == 0) {
            remoteViews.setViewVisibility(R.id.imgmoon, 4);
        }
        if (checked == 1) {
            remoteViews.setTextViewText(R.id.TextViewHrs, simpleDateFormat5.format(this.currentTime));
            remoteViews.setTextViewText(R.id.TextViewAMPM, "  ");
        } else if (checked == 0) {
            remoteViews.setTextViewText(R.id.TextViewHrs, simpleDateFormat2.format(this.currentTime));
            remoteViews.setTextViewText(R.id.TextViewAMPM, simpleDateFormat.format(this.currentTime));
            remoteViews.setTextColor(R.id.TextViewAMPM, colwheelhr);
        }
        remoteViews.setTextColor(R.id.TextViewMins, colwheelmin);
        remoteViews.setTextColor(R.id.TextViewHrs, colwheelhr);
        remoteViews.setTextViewText(R.id.TextViewMins, simpleDateFormat3.format(this.currentTime));
        if (checkednextalarm == 1) {
            String Alarm1 = null;
            try {
                Alarm1 = Settings.System.getString(getContentResolver(), "next_alarm_formatted");
            } catch (Exception e5) {
                Log.i("Exception", "Exception next alarm not found = " + e5);
            }
            if (Alarm1 == null || TextUtils.isEmpty(Alarm1)) {
                remoteViews.setViewVisibility(R.id.imgalarm, 4);
                remoteViews.setTextViewText(R.id.TextViewNextAlarm, "");
            } else {
                remoteViews.setViewVisibility(R.id.imgalarm, 0);
                remoteViews.setTextViewText(R.id.TextViewNextAlarm, Alarm1);
                remoteViews.setTextColor(R.id.TextViewNextAlarm, colwheelmin);
            }
        } else if (checkednextalarm == 0) {
            remoteViews.setViewVisibility(R.id.imgalarm, 4);
            remoteViews.setTextViewText(R.id.TextViewNextAlarm, "");
        }
        if (checkedsys == 1) {
            remoteViews.setViewVisibility(R.id.imgram, 0);
            remoteViews.setViewVisibility(R.id.imgstorage, 0);
            remoteViews.setViewVisibility(R.id.imgbatt, 0);
            remoteViews.setViewVisibility(R.id.imgsd, 0);
            remoteViews.setViewVisibility(R.id.imgbattemp, 0);
            remoteViews.setViewVisibility(R.id.TextViewBatTemp, 0);
            remoteViews.setTextViewText(R.id.TextViewDateTop, String.valueOf(DateFormat.format("MMM", new Date()).toString()) + "  " + simpleDateFormat4.format(this.currentTime));
            remoteViews.setTextColor(R.id.TextViewDateTop, colwheelhr);
            remoteViews.setTextViewText(R.id.TextViewDayTop, DateFormat.format("EEE", new Date()).toString());
            remoteViews.setTextColor(R.id.TextViewDayTop, colwheelmin);
            remoteViews.setTextViewText(R.id.TextViewBat, String.valueOf(chargestatus) + levelText);
            remoteViews.setTextColor(R.id.TextViewBat, colorother);
            remoteViews.setTextViewText(R.id.TextViewSD, sdfree);
            remoteViews.setTextColor(R.id.TextViewSD, colorother);
            remoteViews.setTextViewText(R.id.TextViewMem, String.valueOf(memfree) + "MB");
            remoteViews.setTextColor(R.id.TextViewMem, colorother);
            remoteViews.setTextViewText(R.id.TextViewIntSt, intfree);
            remoteViews.setTextColor(R.id.TextViewIntSt, colorother);
            remoteViews.setViewVisibility(R.id.AnalogClock, 4);
            remoteViews.setTextViewText(R.id.TextViewDay, "");
            remoteViews.setTextViewText(R.id.TextViewDate, "");
            if (charging) {
                remoteViews.setViewVisibility(R.id.imgbattchg, 0);
                remoteViews.setViewVisibility(R.id.imgbatt, 4);
            } else {
                remoteViews.setViewVisibility(R.id.imgbattchg, 4);
                remoteViews.setViewVisibility(R.id.imgbatt, 0);
            }
            if (checkedbattempf == 0) {
                remoteViews.setTextViewText(R.id.TextViewBatTemp, cTemp);
                remoteViews.setTextColor(R.id.TextViewBatTemp, colorother);
            } else if (checkedbattempf == 1) {
                remoteViews.setTextViewText(R.id.TextViewBatTemp, fTemp);
                remoteViews.setTextColor(R.id.TextViewBatTemp, colorother);
            }
        } else if (checkedsys == 0) {
            remoteViews.setTextViewText(R.id.TextViewDate, String.valueOf(DateFormat.format("MMM", new Date()).toString()) + "  " + simpleDateFormat4.format(this.currentTime));
            remoteViews.setTextColor(R.id.TextViewDate, colorother);
            remoteViews.setTextViewText(R.id.TextViewDay, DateFormat.format("EEE", new Date()).toString());
            remoteViews.setTextColor(R.id.TextViewDay, colorother);
            remoteViews.setTextViewText(R.id.TextViewBat, "");
            remoteViews.setTextViewText(R.id.TextViewSD, "");
            remoteViews.setTextViewText(R.id.TextViewMem, "");
            remoteViews.setTextViewText(R.id.TextViewIntSt, "");
            remoteViews.setTextViewText(R.id.TextViewDateTop, "");
            remoteViews.setTextViewText(R.id.TextViewDayTop, "");
            remoteViews.setTextViewText(R.id.TextViewBatTemp, "");
            remoteViews.setViewVisibility(R.id.imgram, 4);
            remoteViews.setViewVisibility(R.id.imgstorage, 4);
            remoteViews.setViewVisibility(R.id.imgbatt, 4);
            remoteViews.setViewVisibility(R.id.imgbattchg, 4);
            remoteViews.setViewVisibility(R.id.imgsd, 4);
            remoteViews.setViewVisibility(R.id.imgbattemp, 4);
            remoteViews.setViewVisibility(R.id.TextViewBatTemp, 4);
            remoteViews.setViewVisibility(R.id.AnalogClock, 0);
        }
        if (checkedweatherenabled == 1) {
            remoteViews.setViewVisibility(R.id.AnalogClock, 4);
        }
        if (checkednoweatherdetail == 1 && checkedsys == 0) {
            remoteViews.setTextViewText(R.id.weather_text, "");
            remoteViews.setViewVisibility(R.id.AnalogClock, 0);
        }
        if (checkednoweatherdetail == 1 && checkedsys == 1) {
            remoteViews.setTextViewText(R.id.weather_text, "");
        }
        if (checkednoweatherdetail == 0 && checkedsys == 1) {
            remoteViews.setViewVisibility(R.id.weather_text, 0);
        }
        if (checkedweatherenabled == 1 && checkedweatherbottom == 1) {
            remoteViews.setTextViewText(R.id.TextViewDateTop, String.valueOf(DateFormat.format("MMM", new Date()).toString()) + "  " + simpleDateFormat4.format(this.currentTime));
            remoteViews.setTextColor(R.id.TextViewDateTop, colwheelhr);
            remoteViews.setTextViewText(R.id.TextViewDayTop, DateFormat.format("EEE", new Date()).toString());
            remoteViews.setTextColor(R.id.TextViewDayTop, colwheelmin);
            remoteViews.setTextViewText(R.id.TextViewDay, "");
            remoteViews.setTextViewText(R.id.TextViewDate, "");
            remoteViews.setViewVisibility(R.id.AnalogClock, 4);
            remoteViews.setViewVisibility(R.id.weather_image_loc_bottom, 0);
            remoteViews.setViewVisibility(R.id.weather_temp_now_bottom, 0);
            remoteViews.setViewVisibility(R.id.weather_loc_bottom, 0);
            remoteViews.setViewVisibility(R.id.weather_hl_bottom, 0);
            remoteViews.setViewVisibility(R.id.weather_text_bottom, 0);
        } else if (checkedweatherenabled == 1 && checkedweatherbottom == 0) {
            remoteViews.setViewVisibility(R.id.weather_image_loc_bottom, 4);
            remoteViews.setViewVisibility(R.id.weather_temp_now_bottom, 4);
            remoteViews.setViewVisibility(R.id.weather_loc_bottom, 4);
            remoteViews.setViewVisibility(R.id.weather_hl_bottom, 4);
            remoteViews.setViewVisibility(R.id.weather_text_bottom, 4);
        }
        if (!Environment.getExternalStorageState().equals("mounted")) {
            remoteViews.setImageViewResource(R.id.themeimage, R.drawable.clockback1);
        } else if (skinchoice == "SKIN_CHOICE_EMPTY" || checkednoskin == 1) {
            remoteViews.setImageViewResource(R.id.themeimage, R.drawable.clockback1);
        } else if (new File(skinchoice).exists()) {
            remoteViews.setImageViewBitmap(R.id.themeimage, BitmapFactory.decodeFile(skinchoice));
        }
        remoteViews.setOnClickPendingIntent(R.id.weather_image, CountdownWidget.makeControlPendingIntent(getApplicationContext(), PLUS, appWidgetId));
        remoteViews.setOnClickPendingIntent(R.id.weather_image_loc_bottom, CountdownWidget.makeControlPendingIntent(getApplicationContext(), PLUSPLUS, appWidgetId));
        remoteViews.setOnClickPendingIntent(R.id.TextViewBat, CountdownWidget.makeControlPendingIntent(getApplicationContext(), MINUS, appWidgetId));
        remoteViews.setOnClickPendingIntent(R.id.launchtrans, CountdownWidget.makeControlPendingIntent(getApplicationContext(), LAUNCHME, appWidgetId));
        remoteViews.setOnClickPendingIntent(R.id.launchtransmins, CountdownWidget.makeControlPendingIntent(getApplicationContext(), LAUNCHMEMINS, appWidgetId));
        remoteViews.setOnClickPendingIntent(R.id.launchdlg, CountdownWidget.makeControlPendingIntent(getApplicationContext(), LAUNCHDLG, appWidgetId));
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
        super.onStart(intent, startId);
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.mBI != null) {
                unregisterReceiver(this.mBI);
            }
        } catch (Exception e) {
            Log.e("Widget", "Failed to unregister", e);
        }
    }

    private String getsdfree() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        double megAvailable = (((double) (((long) stat.getBlockSize()) * ((long) stat.getAvailableBlocks()))) / 1048576.0d) / 1024.0d;
        if (megAvailable > 1.0d) {
            return String.valueOf(new DecimalFormat("#.##").format(megAvailable)) + "GB";
        }
        return String.valueOf(new DecimalFormat("#").format(megAvailable * 1024.0d)) + "MB";
    }

    private String getinternalfree() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        double intmem = (((double) ((long) stat.getAvailableBlocks())) * ((double) ((long) stat.getBlockSize()))) / 1048576.0d;
        if (intmem > 999.0d) {
            return String.valueOf(new DecimalFormat("#.00").format(intmem / 1024.0d)) + "GB";
        }
        return String.valueOf(new DecimalFormat("#").format(intmem)) + "MB";
    }

    private long getramfree() {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ((ActivityManager) getSystemService("activity")).getMemoryInfo(mi);
        return mi.availMem / 1048576;
    }

    private int conway(int year, int month, int day) {
        double d;
        double r = ((double) (year % 100)) % 19.0d;
        if (r > 9.0d) {
            r -= 19.0d;
        }
        double r2 = ((11.0d * r) % 30.0d) + ((double) month) + ((double) day);
        if (month < 3) {
            r2 += 2.0d;
        }
        double r3 = Math.floor(0.5d + (r2 - (year < 2000 ? 4.0d : 8.3d))) % 30.0d;
        if (r3 < 0.0d) {
            d = r3 + 30.0d;
        } else {
            d = r3;
        }
        return (int) d;
    }

    private boolean chkStatus() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService("connectivity");
        NetworkInfo wifi = connMgr.getNetworkInfo(1);
        NetworkInfo mobile = connMgr.getNetworkInfo(0);
        if (!wifi.isAvailable() && !mobile.isAvailable()) {
            return false;
        }
        return true;
    }

    private boolean netCheckerHelper() {
        NetworkInfo netInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public static String arrayToString(String[] a, String separator) {
        if (a == null || separator == null) {
            return null;
        }
        StringBuilder result = new StringBuilder();
        if (a.length > 0) {
            result.append(a[0]);
            for (int i = 1; i < a.length; i++) {
                result.append(separator);
                result.append(a[i]);
            }
        }
        return result.toString();
    }

    public static String[] stringToArray(String splitme) throws Exception {
        return splitme.split("=");
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }
}
