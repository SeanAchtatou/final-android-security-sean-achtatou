package jp.co.microad.smartphone.sdk.utils;

import android.app.Activity;
import android.content.res.AssetManager;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import jp.co.microad.smartphone.sdk.log.MLog;

public class ResourceUtil {
    public static InputStream getResourceAsStream(Activity activity, String fileName) {
        InputStream in = ResourceUtil.class.getResourceAsStream(fileName);
        if (in != null) {
            return in;
        }
        if (activity != null) {
            try {
                AssetManager man = activity.getAssets();
                if (man != null) {
                    return man.open(fileName);
                }
            } catch (FileNotFoundException e) {
                MLog.d("file not found " + fileName, e);
            } catch (IOException e2) {
                MLog.d("failed to read " + fileName, e2);
            }
        }
        return null;
    }
}
