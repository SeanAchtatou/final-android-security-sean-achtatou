package jp.co.microad.smartphone.sdk.utils;

import android.content.Context;
import jp.co.microad.smartphone.sdk.log.MLog;

public class SpotIdUtil {
    static final String MICROAD_KEY = "MICROAD_KEY";
    static String spotId = "";

    public static String getSpotId(Context context) {
        if (StringUtil.isNotEmpty(spotId)) {
            return spotId;
        }
        synchronized (spotId) {
            spotId = MetaDataUtil.getString(context, MICROAD_KEY);
        }
        return spotId;
    }

    public static String encryptSpotId(String spotId2, String seed) {
        if (StringUtil.isEmpty(spotId2)) {
            throw new IllegalStateException("枠IDをAndroidManifest.xmlに設定してください");
        } else if (StringUtil.isEmpty(seed)) {
            MLog.w("暗号化のキーが取得できませんでした");
            return null;
        } else {
            try {
                return CryptUtil.encrypt(spotId2, seed);
            } catch (Exception e) {
                MLog.e("暗号化に失敗しました", e);
                return null;
            }
        }
    }
}
