package jp.co.microad.smartphone.sdk.log;

import android.util.Log;
import jp.co.microad.smartphone.sdk.utils.SettingsUtil;

public abstract class MLog {
    public static final int ASSERT = 7;
    public static final int DEBUG = 3;
    public static final int ERROR = 6;
    public static final int INFO = 4;
    public static final String TAG = "MICROAD";
    public static final int VERBOSE = 2;
    public static final int WARN = 5;

    public static native boolean isLoggable(String str, int i);

    public static native int println(int i, String str, String str2);

    static boolean isNotAllLog() {
        return !SettingsUtil.isDebugg();
    }

    public static int v(String msg) {
        if (isNotAllLog()) {
            return -1;
        }
        return Log.v("MICROAD", msg);
    }

    public static int v(String msg, Throwable tr) {
        if (isNotAllLog()) {
            return -1;
        }
        return Log.v("MICROAD", msg, tr);
    }

    public static int d(String msg) {
        if (isNotAllLog()) {
            return -1;
        }
        return Log.d("MICROAD", msg);
    }

    public static int d(String msg, Throwable tr) {
        if (isNotAllLog()) {
            return -1;
        }
        return Log.d("MICROAD", msg, tr);
    }

    public static int i(String msg) {
        return Log.i("MICROAD", msg);
    }

    public static int i(String msg, Throwable tr) {
        return Log.i("MICROAD", msg, tr);
    }

    public static int w(String msg) {
        return Log.w("MICROAD", msg);
    }

    public static int w(String msg, Throwable tr) {
        return Log.w("MICROAD", msg, tr);
    }

    public static int w(Throwable tr) {
        return Log.w("MICROAD", tr);
    }

    public static int e(String msg) {
        return Log.e("MICROAD", msg);
    }

    public static int e(String msg, Throwable tr) {
        return Log.e("MICROAD", msg, tr);
    }

    public static String getStackTraceString(Throwable tr) {
        return Log.getStackTraceString(tr);
    }
}
