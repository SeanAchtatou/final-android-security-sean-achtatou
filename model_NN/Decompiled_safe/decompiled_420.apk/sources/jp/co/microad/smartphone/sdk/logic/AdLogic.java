package jp.co.microad.smartphone.sdk.logic;

import android.webkit.WebView;
import jp.co.microad.smartphone.sdk.MicroAdJs;
import jp.co.microad.smartphone.sdk.log.MLog;
import jp.co.microad.smartphone.sdk.utils.SettingsUtil;
import jp.co.microad.smartphone.sdk.utils.StringUtil;

public class AdLogic {
    HttpLogic httpLogic = new HttpLogic();

    public void loadAd(WebView wv, MicroAdJs mad) {
        String str;
        MLog.d("@@@ loading ad." + mad);
        if (wv == null) {
            MLog.w("WebViewが取得できませんでした");
        } else if (StringUtil.isEmpty(mad.spotId)) {
            MLog.e("AndroidManifest.xmlに枠IDを設定してください");
        } else if (StringUtil.isEmpty(mad.subscriberId)) {
            MLog.e("端末IDが取得できていません");
        } else {
            try {
                MLog.d(" spotId=" + mad.spotId);
                MLog.d(" subscriberId=" + mad.subscriberId);
                String url = String.format(SettingsUtil.get("adurl"), mad.subscriberId, mad.spotId);
                if (mad.haveLocation()) {
                    url = url + ("&lat=" + mad.latitude + "&lon=" + mad.longitude);
                }
                MLog.d(url);
                wv.addJavascriptInterface(mad, "mad");
                String response = this.httpLogic.doGet(url, "");
                if (StringUtil.isEmpty(response)) {
                    MLog.e("広告が取得できませんでした");
                    return;
                }
                wv.loadDataWithBaseURL(url, response, "text/html", "UTF-8", null);
                MLog.d("@@@ finish loading ad.");
            } catch (Exception e) {
                Exception e2 = e;
                MLog.e("広告取得中にエラーが発生しました");
                MLog.e(" stackgrace", e2);
            } finally {
                str = "@@@ finish loading ad.";
                MLog.d(str);
            }
        }
    }
}
