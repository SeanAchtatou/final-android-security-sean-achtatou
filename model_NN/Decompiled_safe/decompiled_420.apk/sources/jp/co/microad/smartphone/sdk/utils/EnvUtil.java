package jp.co.microad.smartphone.sdk.utils;

import android.app.Activity;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import jp.co.microad.smartphone.sdk.log.MLog;

public class EnvUtil {
    public static void setEnv(Activity activity) {
        String env = "";
        try {
            InputStream is = ResourceUtil.getResourceAsStream(activity, "env.txt");
            if (is != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                while (true) {
                    String line = reader.readLine();
                    if (line != null) {
                        if (StringUtil.isNotEmpty(line)) {
                            env = line.trim();
                            MLog.i("set env to '" + env + "'");
                            break;
                        }
                    } else {
                        break;
                    }
                }
                reader.close();
                is.close();
            }
        } catch (Exception e) {
            MLog.e("failed to load 'settings.xml'");
            MLog.d(" stacktrace", e);
        }
        SettingsUtil.env = env;
    }
}
