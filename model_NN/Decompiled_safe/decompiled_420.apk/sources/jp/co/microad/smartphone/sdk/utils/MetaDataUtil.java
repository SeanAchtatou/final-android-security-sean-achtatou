package jp.co.microad.smartphone.sdk.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;

public class MetaDataUtil {
    public static String getInt(Context context, String key) {
        String packageName = context.getPackageName();
        String activityName = context.getClass().getName();
        PackageManager pm = context.getPackageManager();
        try {
            Bundle bundle = pm.getActivityInfo(new ComponentName(packageName, activityName), 128).metaData;
            if (bundle != null && bundle.containsKey(key)) {
                return String.valueOf(bundle.getInt(key));
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        try {
            Bundle bundle2 = pm.getApplicationInfo(packageName, 128).metaData;
            if (bundle2 != null && bundle2.containsKey(key)) {
                return String.valueOf(bundle2.getInt(key));
            }
            throw new IllegalStateException("AndroidManifest.xmlに" + key + "を設定してください");
        } catch (PackageManager.NameNotFoundException e2) {
            throw new IllegalStateException("AndroidManifest.xmlに" + key + "を設定してください");
        }
    }

    public static boolean getBoolean(Context context, String key, boolean defaultValue) {
        String packageName = context.getPackageName();
        String activityName = context.getClass().getName();
        PackageManager pm = context.getPackageManager();
        try {
            Bundle bundle = pm.getActivityInfo(new ComponentName(packageName, activityName), 128).metaData;
            if (bundle != null && bundle.containsKey(key)) {
                return bundle.getBoolean(key, defaultValue);
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        try {
            Bundle bundle2 = pm.getApplicationInfo(packageName, 128).metaData;
            if (bundle2 != null && bundle2.containsKey(key)) {
                return bundle2.getBoolean(key, defaultValue);
            }
            throw new IllegalStateException("AndroidManifest.xmlに" + key + "を設定してください");
        } catch (PackageManager.NameNotFoundException e2) {
            throw new IllegalStateException("AndroidManifest.xmlに" + key + "を設定してください");
        }
    }

    public static String getString(Context context, String key) {
        String packageName = context.getPackageName();
        String activityName = context.getClass().getName();
        PackageManager pm = context.getPackageManager();
        try {
            Bundle bundle = pm.getActivityInfo(new ComponentName(packageName, activityName), 128).metaData;
            if (bundle != null) {
                String data = bundle.getString(key);
                if (StringUtil.isNotEmpty(data)) {
                    return data;
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        try {
            Bundle bundle2 = pm.getApplicationInfo(packageName, 128).metaData;
            if (bundle2 != null) {
                String data2 = bundle2.getString(key);
                if (StringUtil.isNotEmpty(data2)) {
                    return data2;
                }
            }
            throw new IllegalStateException("AndroidManifest.xmlに" + key + "を設定してください");
        } catch (PackageManager.NameNotFoundException e2) {
            throw new IllegalStateException("AndroidManifest.xmlに" + key + "を設定してください");
        }
    }
}
