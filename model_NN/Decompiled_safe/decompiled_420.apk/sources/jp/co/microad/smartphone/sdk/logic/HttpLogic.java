package jp.co.microad.smartphone.sdk.logic;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import jp.co.microad.smartphone.sdk.log.MLog;
import jp.co.microad.smartphone.sdk.utils.SettingsUtil;
import jp.co.microad.smartphone.sdk.utils.StringUtil;
import jp.co.microad.smartphone.sdk.utils.UserAgentUtil;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class HttpLogic {
    public String doGet(String uri, String params) {
        try {
            String url = uri + URLEncoder.encode(params, "UTF-8");
            MLog.d(url);
            HttpGet method = new HttpGet(url);
            DefaultHttpClient client = new DefaultHttpClient();
            if (SettingsUtil.isDebugg()) {
                String host = SettingsUtil.get("proxyHost");
                String port = SettingsUtil.get("proxyPort");
                if (StringUtil.isNotEmpty(host) || StringUtil.isNotEmpty(port)) {
                    HttpHost proxy = new HttpHost(host, Integer.valueOf(port).intValue());
                    client.getParams().setParameter("http.route.default-proxy", proxy);
                    MLog.d("proxy HostName=" + proxy.getHostName() + " Port=" + proxy.getPort());
                }
            }
            method.setHeader("User-Agent", UserAgentUtil.getUserAgent());
            try {
                HttpResponse response = client.execute(method);
                int status = response.getStatusLine().getStatusCode();
                if (status == 200) {
                    return EntityUtils.toString(response.getEntity(), "UTF-8");
                }
                MLog.e("status[" + status + "]" + response.getStatusLine().getReasonPhrase());
                return "";
            } catch (ClientProtocolException e) {
                MLog.e("ClientProtocolException", e);
            } catch (IOException e2) {
                MLog.e("IOException ネットワークに接続できません");
                MLog.d("", e2);
            } catch (Exception e3) {
                MLog.e("その他のエラー", e3);
            }
        } catch (UnsupportedEncodingException e4) {
            throw new RuntimeException(e4);
        }
    }
}
