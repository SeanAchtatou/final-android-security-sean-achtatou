package jp.co.microad.smartphone.sdk.logic;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import jp.co.microad.smartphone.sdk.log.MLog;

public abstract class BetterLocationManager {
    public static final long DEFAULT_DELAY = 0;
    public static final long DEFAULT_INTERVAL = 1000;
    public static final long DEFAULT_TIMEOUT = 15000;
    public static final int DEFAULT_UPDATE_LIMIT = 2;
    public static final boolean DEFAULT_USE_LAST_KNOWN_LOCATION = true;
    Location currentLocation;
    private long delay = 0;
    private LocationListener gpsLocationListener;
    long interval = 1000;
    private LocationManager locationManager;
    private Timer locationTimer;
    private float minDistance;
    private long minTime;
    private LocationListener networkLocationListener;
    private long significantlyNewer = 120000;
    long time;
    long timeout = DEFAULT_TIMEOUT;
    private int updateCount;
    private int updateLimit = 2;
    private boolean useLastKnownLocation = true;

    /* access modifiers changed from: protected */
    public abstract void onLocationProgress(long j);

    /* access modifiers changed from: protected */
    public abstract void onLocationProviderNotAvailable();

    /* access modifiers changed from: protected */
    public abstract void onLocationTimeout();

    /* access modifiers changed from: protected */
    public abstract void onUpdateLocation(Location location, int i);

    public BetterLocationManager(LocationManager locationManager2) {
        this.locationManager = locationManager2;
    }

    public Location getLastKnownLocation() {
        return getLastKnownLocation(new String[]{"gps", "network"});
    }

    public Location getLastKnownLocation(String[] providers) throws IllegalArgumentException {
        if (this.locationManager == null || providers == null) {
            return null;
        }
        Location result = null;
        for (String provider : providers) {
            if (provider != null && this.locationManager.isProviderEnabled(provider)) {
                Location lastKnownLocation = this.locationManager.getLastKnownLocation(provider);
                if (isBetterLocation(result, lastKnownLocation)) {
                    result = lastKnownLocation;
                }
            }
        }
        return result;
    }

    public boolean isUseLastKnownLocation() {
        return this.useLastKnownLocation;
    }

    public void setUseLastKnownLocation(boolean useLastKnownLocation2) {
        this.useLastKnownLocation = useLastKnownLocation2;
    }

    public long getSignificantlyNewer() {
        return this.significantlyNewer;
    }

    public void setSignificantlyNewer(long significantlyNewer2) {
        this.significantlyNewer = significantlyNewer2;
    }

    public long getDelay() {
        return this.delay;
    }

    public void setDelay(long delay2) {
        long j;
        if (delay2 < 0) {
            j = 0;
        } else {
            j = delay2;
        }
        this.delay = j;
    }

    public long getInterval() {
        return this.interval;
    }

    public void setInterval(long interval2) {
        if (this.locationTimer != null) {
            throw new IllegalStateException();
        }
        this.interval = interval2;
    }

    public long getTimeout() {
        return this.timeout;
    }

    public void setTimeout(long timeout2) {
        this.timeout = timeout2;
    }

    public Location getCurrentLocation() {
        return this.currentLocation;
    }

    public void start() {
        Location lastKnownLocation;
        try {
            stop();
            this.currentLocation = null;
            this.updateCount = 0;
            if (this.locationManager != null) {
                boolean gps = this.locationManager.isProviderEnabled("gps");
                boolean network = this.locationManager.isProviderEnabled("network");
                if (gps || network) {
                    if (this.useLastKnownLocation && (lastKnownLocation = getLastKnownLocation()) != null) {
                        updateLocation(lastKnownLocation, true);
                        if (new Date().getTime() - lastKnownLocation.getTime() < this.significantlyNewer) {
                            return;
                        }
                    }
                    if (this.timeout > 0) {
                        this.time = 0;
                        final Handler handler = new Handler();
                        this.locationTimer = new Timer(true);
                        this.locationTimer.scheduleAtFixedRate(new TimerTask() {
                            public void run() {
                                handler.post(new Runnable() {
                                    public void run() {
                                        if (BetterLocationManager.this.time > BetterLocationManager.this.timeout) {
                                            BetterLocationManager.this.stop();
                                            BetterLocationManager.this.onLocationTimeout();
                                            return;
                                        }
                                        BetterLocationManager.this.onLocationProgress(BetterLocationManager.this.time);
                                        BetterLocationManager.this.time += BetterLocationManager.this.interval;
                                    }
                                });
                            }
                        }, this.delay, this.interval);
                    }
                    if (gps) {
                        this.gpsLocationListener = new LocationListener() {
                            public void onLocationChanged(Location location) {
                                if (BetterLocationManager.this.isBetterLocation(BetterLocationManager.this.currentLocation, location)) {
                                    BetterLocationManager.this.updateLocation(location, false);
                                }
                            }

                            public void onProviderDisabled(String provider) {
                            }

                            public void onProviderEnabled(String provider) {
                            }

                            public void onStatusChanged(String provider, int status, Bundle extras) {
                            }
                        };
                        this.locationManager.requestLocationUpdates("gps", this.minTime, this.minDistance, this.gpsLocationListener);
                    }
                    if (network) {
                        this.networkLocationListener = new LocationListener() {
                            public void onLocationChanged(Location location) {
                                if (BetterLocationManager.this.isBetterLocation(BetterLocationManager.this.currentLocation, location)) {
                                    BetterLocationManager.this.updateLocation(location, false);
                                }
                            }

                            public void onProviderDisabled(String provider) {
                            }

                            public void onProviderEnabled(String provider) {
                            }

                            public void onStatusChanged(String provider, int status, Bundle extras) {
                            }
                        };
                        this.locationManager.requestLocationUpdates("network", this.minTime, this.minDistance, this.networkLocationListener);
                        return;
                    }
                    return;
                }
                onLocationProviderNotAvailable();
            }
        } catch (SecurityException e) {
            SecurityException e2 = e;
            MLog.e(e2.getMessage());
            MLog.d(" stacktrace", e2);
            stop();
        }
    }

    public void stop() {
        if (this.locationManager != null) {
            if (this.locationTimer != null) {
                this.locationTimer.cancel();
                this.locationTimer.purge();
                this.locationTimer = null;
            }
            if (this.networkLocationListener != null) {
                this.locationManager.removeUpdates(this.networkLocationListener);
                this.networkLocationListener = null;
            }
            if (this.gpsLocationListener != null) {
                this.locationManager.removeUpdates(this.gpsLocationListener);
                this.gpsLocationListener = null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void updateLocation(Location location, boolean lastKnownLocation) {
        if (!lastKnownLocation && this.updateLimit > 0) {
            this.updateCount++;
            if (this.updateCount >= this.updateLimit) {
                stop();
            }
        }
        this.currentLocation = location;
        onUpdateLocation(location, this.updateCount);
    }

    public boolean isBetterLocation(Location currentLocation2, Location newLocation) {
        if (newLocation == null) {
            return false;
        }
        if (currentLocation2 == null) {
            return true;
        }
        long timeDelta = newLocation.getTime() - currentLocation2.getTime();
        if (timeDelta > this.significantlyNewer) {
            return true;
        }
        if (timeDelta < this.significantlyNewer) {
            return false;
        }
        int accuracyDelta = (int) (newLocation.getAccuracy() - currentLocation2.getAccuracy());
        if (accuracyDelta < 0) {
            return true;
        }
        if (timeDelta > 0 && accuracyDelta <= 0) {
            return true;
        }
        if (timeDelta <= 0 || accuracyDelta > 200 || !isSameProvider(newLocation.getProvider(), currentLocation2.getProvider())) {
            return false;
        }
        return true;
    }

    public static boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}
