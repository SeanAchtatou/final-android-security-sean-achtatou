package jp.co.microad.smartphone.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import jp.co.microad.smartphone.sdk.entity.Settings;
import jp.co.microad.smartphone.sdk.log.MLog;
import jp.co.microad.smartphone.sdk.logic.AdLogic;
import jp.co.microad.smartphone.sdk.logic.LocationLogic;
import jp.co.microad.smartphone.sdk.logic.SettingsApiLogic;
import jp.co.microad.smartphone.sdk.task.AdTask;
import jp.co.microad.smartphone.sdk.utils.EnvUtil;
import jp.co.microad.smartphone.sdk.utils.SettingsUtil;
import jp.co.microad.smartphone.sdk.utils.StringUtil;
import jp.co.microad.smartphone.sdk.utils.SubscriberIdUtil;

public class MicroAdLayout extends RelativeLayout {
    public static final String TAG_WEBVIEW = "microad.webview";
    /* access modifiers changed from: private */
    public WeakReference<Activity> activityRef;
    AdLogic adLogic = new AdLogic();
    Settings apiSettings;
    ScheduledFuture future;
    boolean initialized = false;
    Boolean isScheduled = Boolean.FALSE;
    LocationLogic locationLogic;
    public MicroAdJs mad;
    int maxHeight = 0;
    public int maxWidth = 0;
    boolean permissionLocDisabled = false;
    ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private int widthSize;
    /* access modifiers changed from: private */
    public WebView wv;

    public MicroAdLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MicroAdLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MicroAdLayout(Context context) {
        super(context);
    }

    public void init(Activity activity) {
        if (activity == null) {
            MLog.e("Activityがnullのため初期化を中断します");
            return;
        }
        this.activityRef = new WeakReference<>(activity);
        EnvUtil.setEnv(activity);
        SettingsUtil.load(activity);
        this.mad = MicroAdJs.getInstance(activity, this.maxHeight, this.maxWidth);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        setFocusable(false);
        initWebView(activity);
        this.initialized = true;
    }

    /* access modifiers changed from: package-private */
    public void initWebView(Activity activity) {
        this.wv = new WebView(activity);
        this.wv.setHorizontalScrollBarEnabled(false);
        this.wv.setVerticalScrollBarEnabled(false);
        this.wv.getSettings().setJavaScriptEnabled(true);
        this.wv.setTag(TAG_WEBVIEW);
        this.wv.setFocusable(false);
        addView(this.wv);
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                MLog.d("Intercepted ACTION_DOWN event");
                String url = this.mad.getRedirectUrl();
                if (!StringUtil.isEmpty(url)) {
                    MLog.d("RedirectURL is " + url);
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
                    intent.addFlags(268435456);
                    try {
                        if (this.activityRef != null) {
                            Activity activity = this.activityRef.get();
                            if (activity != null) {
                                activity.startActivity(intent);
                                break;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } catch (Exception e) {
                        MLog.w("Could not handle click to " + url, e);
                        break;
                    }
                } else {
                    MLog.d("RedirectURL is empty.");
                    return false;
                }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize2 = View.MeasureSpec.getSize(widthMeasureSpec);
        if (this.maxWidth > 0 && this.maxWidth < widthSize2) {
            widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.maxWidth, Integer.MIN_VALUE);
        }
        int heightSize = View.MeasureSpec.getSize(heightMeasureSpec);
        if (this.maxHeight > 0 && this.maxHeight < heightSize) {
            heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.maxHeight, Integer.MIN_VALUE);
        }
        MLog.d("MicroAdLayout width=" + widthSize2 + " height=" + heightSize + " maxWidth=" + this.maxWidth + " maxHeight=" + this.maxHeight);
        if (this.widthSize > 0 && this.widthSize != widthSize2) {
            MLog.d("画面サイズが変更されたためリロードします");
            reload();
            this.widthSize = widthSize2;
        }
        if (this.widthSize <= 0) {
            this.widthSize = widthSize2;
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if (!this.initialized) {
            MLog.w("初期化されていません");
            return;
        }
        MLog.d("onWindowVisibilityChanged :" + (visibility == 0 ? "VISIBLE" : "NOT VISIBEL"));
        if (visibility != 0) {
            cancelSchedule();
            removeUpdate();
        } else if (this.apiSettings == null || !this.apiSettings.isLoaded()) {
            new SettingsApiTask().execute(this.mad.spotId);
        } else {
            requestUpdate();
            startSchedule();
        }
    }

    /* access modifiers changed from: package-private */
    public void cancelSchedule() {
        synchronized (this.isScheduled) {
            if (this.isScheduled.booleanValue()) {
                this.scheduler.schedule(new Runnable() {
                    public void run() {
                        if (MicroAdLayout.this.future == null) {
                            MLog.d("future is null.");
                            return;
                        }
                        MLog.d("@@@ scheduler canceld.");
                        MicroAdLayout.this.future.cancel(true);
                    }
                }, 0, TimeUnit.SECONDS);
                this.isScheduled = Boolean.FALSE;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void startSchedule() {
        synchronized (this.isScheduled) {
            if (this.isScheduled.booleanValue()) {
                MLog.d("@@@ already scheduled.");
            } else if (this.apiSettings == null || !this.apiSettings.isLoaded()) {
                this.scheduler.schedule(new Runnable() {
                    public void run() {
                        new SettingsApiTask().execute(MicroAdLayout.this.mad.spotId);
                    }
                }, 10, TimeUnit.SECONDS);
            } else {
                Runnable command = new Runnable() {
                    public void run() {
                        try {
                            String subscriberId = SubscriberIdUtil.get((Activity) MicroAdLayout.this.activityRef.get());
                            if (StringUtil.isEmpty(subscriberId)) {
                                MLog.d("端末IDが取得できないため終了します");
                                return;
                            }
                            MicroAdLayout.this.mad.setSubscriberId(subscriberId);
                            MicroAdLayout.this.adLogic.loadAd(MicroAdLayout.this.wv, MicroAdLayout.this.mad);
                        } catch (Exception e) {
                            MLog.e("広告取得時にエラー");
                            MLog.d(" stacktrace", e);
                        }
                    }
                };
                int interval = this.apiSettings.rotationInterval;
                if (interval < 0) {
                    MLog.d("ad loaded only once.");
                    this.future = this.scheduler.schedule(command, 0, TimeUnit.SECONDS);
                } else {
                    MLog.d("ad loaded every " + interval + "sec.");
                    this.future = this.scheduler.scheduleAtFixedRate(command, 0, (long) interval, TimeUnit.SECONDS);
                }
                this.isScheduled = Boolean.TRUE;
                MLog.d("@@@ start schedule. interval=" + interval + "s");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void removeUpdate() {
        if (this.apiSettings != null && this.apiSettings.gpsFlag && this.locationLogic != null) {
            this.locationLogic.removeUpdate();
        }
    }

    /* access modifiers changed from: package-private */
    public void requestUpdate() {
        if ((this.mad == null || !this.mad.haveLocation()) && this.apiSettings != null && this.apiSettings.gpsFlag) {
            if (this.locationLogic == null) {
                this.locationLogic = new LocationLogic(this);
            } else if (this.locationLogic.beTimeout) {
                return;
            }
            Activity activity = this.activityRef.get();
            if (activity != null) {
                this.locationLogic.requestUpdate(activity);
            }
        }
    }

    class SettingsApiTask extends AsyncTask<String, Integer, Void> {
        SettingsApiLogic logic = new SettingsApiLogic();

        SettingsApiTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... params) {
            if (MicroAdLayout.this.apiSettings == null) {
                MicroAdLayout.this.apiSettings = new Settings();
            }
            if (MicroAdLayout.this.apiSettings.isLoaded()) {
                return null;
            }
            String spotId = params[0];
            MicroAdLayout.this.apiSettings = this.logic.loadSettings((Activity) MicroAdLayout.this.activityRef.get(), spotId);
            MicroAdLayout.this.mad.color = MicroAdLayout.this.apiSettings.backgroundColor;
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void v) {
            MicroAdLayout.this.requestUpdate();
            MicroAdLayout.this.cancelSchedule();
            MicroAdLayout.this.startSchedule();
        }
    }

    public void reload() {
        if (!this.initialized) {
            MLog.w("初期化されていません");
        } else if (this.apiSettings == null || !this.apiSettings.isLoaded()) {
            MLog.d("設定情報が取得できていないため終了します");
        } else {
            String subscriberId = SubscriberIdUtil.get(this.activityRef.get());
            if (StringUtil.isEmpty(subscriberId)) {
                MLog.d("端末IDが取得できないため終了します");
                return;
            }
            this.mad.setSubscriberId(subscriberId);
            new AdTask(this.wv).execute(this.mad);
        }
    }
}
