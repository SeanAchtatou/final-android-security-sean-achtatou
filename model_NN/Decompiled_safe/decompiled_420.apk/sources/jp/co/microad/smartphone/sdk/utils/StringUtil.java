package jp.co.microad.smartphone.sdk.utils;

public class StringUtil {
    public static boolean isEmpty(String s) {
        if (s == null || "".equals(s)) {
            return true;
        }
        for (char c : s.toCharArray()) {
            if (c != ' ' && c != 12288) {
                return false;
            }
        }
        return true;
    }

    public static boolean isNotEmpty(String s) {
        return !isEmpty(s);
    }
}
