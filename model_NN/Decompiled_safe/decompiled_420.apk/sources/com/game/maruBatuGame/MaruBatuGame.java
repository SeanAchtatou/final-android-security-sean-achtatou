package com.game.maruBatuGame;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.crossfield.more.MoreActivity;
import jp.co.microad.smartphone.sdk.MicroAdLayout;

public class MaruBatuGame extends Activity implements View.OnClickListener {
    private static final String TAG = "MaruBatuGame";
    /* access modifiers changed from: private */
    public Intent intent;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/JohnHancockCP.otf");
        ((MicroAdLayout) findViewById(R.id.adview)).init(this);
        ((MicroAdLayout) findViewById(R.id.adview)).init(null);
        this.intent = new Intent(this, Game.class);
        View onePlayerButton = findViewById(R.id.one_player_button);
        ((TextView) onePlayerButton).setTypeface(tf);
        onePlayerButton.setOnClickListener(this);
        View settingButton = findViewById(R.id.setting_button);
        ((TextView) settingButton).setTypeface(tf);
        settingButton.setOnClickListener(this);
        View recordsButton = findViewById(R.id.records_button);
        ((TextView) recordsButton).setTypeface(tf);
        recordsButton.setOnClickListener(this);
        View moreGamesButton = findViewById(R.id.more_games_button);
        ((TextView) moreGamesButton).setTypeface(tf);
        moreGamesButton.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.one_player_button /*2131165185*/:
                Log.d(TAG, "clicked on one_player_button");
                this.intent.putExtra(Game.KEY_PAGE_MODE, 0);
                this.intent.putExtra(Game.KEY_PLAY_MODE, 1);
                this.intent.putExtra(Game.KEY_PLAYERS, 1);
                startGame();
                return;
            case R.id.setting_button /*2131165186*/:
                Log.d(TAG, "clicked on records_button");
                this.intent.putExtra(Game.KEY_PAGE_MODE, 2);
                startGame();
                return;
            case R.id.records_button /*2131165187*/:
                Log.d(TAG, "clicked on records_button");
                this.intent.putExtra(Game.KEY_PAGE_MODE, 1);
                startGame();
                return;
            case R.id.more_games_button /*2131165188*/:
                startActivity(new Intent(this, MoreActivity.class));
                return;
            default:
                return;
        }
    }

    private void openPlayDialog() {
        new AlertDialog.Builder(this).setTitle("Mode Select").setItems((int) R.array.size3, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int s) {
                if (s == 0 || s == 1 || s == 2) {
                    MaruBatuGame.this.intent.putExtra(Game.KEY_PAGE_MODE, 0);
                    MaruBatuGame.this.intent.putExtra(Game.KEY_BOARD_SIZE, s);
                } else {
                    MaruBatuGame.this.intent.putExtra(Game.KEY_PAGE_MODE, 1);
                }
                MaruBatuGame.this.startGame();
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void startGame() {
        this.intent.putExtra(Game.KEY_DIFFICULTY, 1);
        startActivity(this.intent);
    }
}
