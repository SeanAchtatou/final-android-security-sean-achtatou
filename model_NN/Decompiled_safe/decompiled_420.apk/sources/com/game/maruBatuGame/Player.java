package com.game.maruBatuGame;

import android.graphics.Color;

/* compiled from: Game */
class Player {
    Color color;
    String symbol;
    int type;

    public Player(int type2, String symbol2, Color color2) {
        this.type = type2;
        this.symbol = symbol2;
        this.color = color2;
    }

    private void setType(int type2) {
        this.type = type2;
    }

    private int getType() {
        return this.type;
    }

    private void setSymbol(String symbol2) {
        this.symbol = symbol2;
    }

    private String getSymbol() {
        return this.symbol;
    }

    private void setColor(Color color2) {
        this.color = color2;
    }

    private Color getColor() {
        return this.color;
    }
}
