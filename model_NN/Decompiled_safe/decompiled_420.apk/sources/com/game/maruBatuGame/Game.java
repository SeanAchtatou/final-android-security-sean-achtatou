package com.game.maruBatuGame;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.crossfield.more.MoreActivity;
import java.lang.reflect.Array;
import java.util.Random;
import jp.co.microad.smartphone.sdk.MicroAdLayout;

public class Game extends Activity implements View.OnClickListener {
    public static final String BOARDSIZE = "boardsize";
    public static final int BOARD_SIZE_3x3 = 0;
    public static final int BOARD_SIZE_6x6 = 1;
    public static final int BOARD_SIZE_9x9 = 2;
    public static final int COM_COM = 4;
    public static final int COM_PLAYER = 2;
    public static final String DIFFICULTY = "difficulty";
    public static final int DIFFICULTY_EASY = 1;
    public static final int DIFFICULTY_VERY_EASY = 0;
    public static final String DRAW33 = "draw33";
    public static final String DRAW66 = "draw66";
    public static final String DRAW99 = "draw99";
    public static final int FIVE_PLAYERS = 4;
    public static final int FOUR_PLAYERS = 3;
    public static final int GAME_PAGE = 0;
    public static final String KEY_BOARD_SIZE = "org.game.maruBatuGame.boardsize";
    public static final String KEY_DIFFICULTY = "org.game.maruBatuGame.difficulty";
    public static final String KEY_PAGE_MODE = "org.game.maruBatuGame.page";
    public static final String KEY_PLAYERS = "org.game.maruBatuGame.players";
    public static final String KEY_PLAY_MODE = "org.game.maruBatuGame.playMode";
    public static final String LOSE33 = "lose33";
    public static final String LOSE66 = "lose66";
    public static final String LOSE99 = "lose99";
    public static final String NDRAW33 = "ndraw33";
    public static final String NDRAW66 = "ndraw66";
    public static final String NDRAW99 = "ndraw99";
    public static final String NLOSE33 = "nlose33";
    public static final String NLOSE66 = "nlose66";
    public static final String NLOSE99 = "nlose99";
    public static final String NWIN33 = "nwin33";
    public static final String NWIN66 = "nwin66";
    public static final String NWIN99 = "nwin99";
    public static final int ONE_PLAYER = 0;
    public static final int PLAYER_COM = 1;
    public static final int PLAYER_PLAYER = 0;
    public static final int RECORD_PAGE = 1;
    public static final int ROTATION = 3;
    public static final int SETTING_PAGE = 2;
    public static final int SIX_PLAYERS = 5;
    private static final String TAG = "Game";
    public static final int THREE_PLAYERS = 2;
    public static final int TWO_PLAYERS = 1;
    public static final String WIN33 = "win33";
    public static final String WIN66 = "win66";
    public static final String WIN99 = "win99";
    private TextView Easy33View;
    private TextView Easy66View;
    private TextView Easy99View;
    private TextView Nomal33View;
    private TextView Nomal66View;
    private TextView Nomal99View;
    private int bSize;
    private int[][] board;
    private BoardView boardView;
    private int gDiff;
    public int maxTurn;
    private int nbPlayers;
    public int nbRow;
    public int nbTiles;
    public int nbTupple;
    private int pMode;
    private int page;
    public int playNow = 0;
    private Player[] players;
    public int priority;
    private int toAlign;
    public int turn;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        this.page = getIntent().getIntExtra(KEY_PAGE_MODE, 0);
        if (this.page == 0) {
            this.nbPlayers = getIntent().getIntExtra(KEY_PLAYERS, 0) + 1;
            this.gDiff = getPreferences(0).getInt(DIFFICULTY, 0);
            this.pMode = getIntent().getIntExtra(KEY_PLAY_MODE, 1);
            this.bSize = getPreferences(0).getInt(BOARDSIZE, 0);
            if (this.bSize == 0) {
                this.nbTupple = 3;
                this.nbRow = 3;
                this.toAlign = 3;
            } else if (this.bSize == 1) {
                this.nbTupple = 6;
                this.nbRow = 6;
                this.toAlign = 4;
            } else if (this.bSize == 2) {
                this.nbTupple = 9;
                this.nbRow = 9;
                this.toAlign = 5;
            }
            this.nbTiles = this.nbTupple * this.nbRow;
            this.board = (int[][]) Array.newInstance(Integer.TYPE, this.nbTiles + 1, this.nbTiles);
            for (int i = 0; i <= this.nbTiles; i++) {
                for (int j = 0; j < this.nbTiles; j++) {
                    this.board[i][j] = 0;
                }
            }
            this.boardView = new BoardView(this);
            this.boardView.requestFocus();
            this.players = new Player[this.nbPlayers];
            this.maxTurn = 0;
            this.turn = 0;
            this.priority = 1;
            this.playNow = 1;
            requestWindowFeature(1);
            getWindow().addFlags(1024);
            Display display = ((WindowManager) getSystemService("window")).getDefaultDisplay();
            float ah = ((float) display.getHeight()) / 10.0f;
            LinearLayout linearLayout = new LinearLayout(this);
            MicroAdLayout microAdLayout = new MicroAdLayout(this);
            microAdLayout.init(this);
            microAdLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, (int) ah));
            linearLayout.addView(microAdLayout);
            LinearLayout linearLayout2 = new LinearLayout(this);
            this.boardView.setLayoutParams(new LinearLayout.LayoutParams(-1, display.getHeight() - ((int) ah)));
            linearLayout2.addView(this.boardView);
            LinearLayout linearLayout3 = new LinearLayout(this);
            linearLayout3.setOrientation(1);
            linearLayout3.addView(linearLayout);
            linearLayout3.addView(linearLayout2);
            setContentView(linearLayout3);
        } else if (this.page == 1) {
            setTitle("Records");
            setContentView(R.layout.records);
            ((MicroAdLayout) findViewById(R.id.adview)).init(this);
            this.Easy33View = (TextView) findViewById(R.id.easy33);
            this.Easy33View.setText(String.valueOf(getPreferences(0).getString(WIN33, "0")) + " / " + getPreferences(0).getString(LOSE33, "0") + " / " + getPreferences(0).getString(DRAW33, "0"));
            this.Easy66View = (TextView) findViewById(R.id.easy66);
            this.Easy66View.setText(String.valueOf(getPreferences(0).getString(WIN66, "0")) + " / " + getPreferences(0).getString(LOSE66, "0") + " / " + getPreferences(0).getString(DRAW66, "0"));
            this.Easy99View = (TextView) findViewById(R.id.easy99);
            this.Easy99View.setText(String.valueOf(getPreferences(0).getString(WIN99, "0")) + " / " + getPreferences(0).getString(LOSE99, "0") + " / " + getPreferences(0).getString(DRAW99, "0"));
            this.Nomal33View = (TextView) findViewById(R.id.neasy33);
            this.Nomal33View.setText(String.valueOf(getPreferences(0).getString(NWIN33, "0")) + " / " + getPreferences(0).getString(NLOSE33, "0") + " / " + getPreferences(0).getString(NDRAW33, "0"));
            this.Nomal66View = (TextView) findViewById(R.id.neasy66);
            this.Nomal66View.setText(String.valueOf(getPreferences(0).getString(NWIN66, "0")) + " / " + getPreferences(0).getString(NLOSE66, "0") + " / " + getPreferences(0).getString(NDRAW66, "0"));
            this.Nomal99View = (TextView) findViewById(R.id.neasy99);
            this.Nomal99View.setText(String.valueOf(getPreferences(0).getString(NWIN99, "0")) + " / " + getPreferences(0).getString(NLOSE99, "0") + " / " + getPreferences(0).getString(NDRAW99, "0"));
            Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/JohnHancockCP.otf");
            View clearButton = findViewById(R.id.clear_button);
            ((TextView) clearButton).setTypeface(tf);
            clearButton.setOnClickListener(this);
            View returnButton = findViewById(R.id.return_title_button);
            ((TextView) returnButton).setTypeface(tf);
            returnButton.setOnClickListener(this);
            View moreButton = findViewById(R.id.more_game_button);
            ((TextView) moreButton).setTypeface(tf);
            moreButton.setOnClickListener(this);
        } else if (this.page == 2) {
            setTitle("Settings");
            setContentView(R.layout.setting);
            ((MicroAdLayout) findViewById(R.id.adview)).init(this);
            Typeface tf2 = Typeface.createFromAsset(getAssets(), "fonts/JohnHancockCP.otf");
            RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radiogroup);
            int bs = getPreferences(0).getInt(BOARDSIZE, 0);
            if (bs == 1) {
                radioGroup.check(R.id.radiobutton66);
            } else if (bs == 2) {
                radioGroup.check(R.id.radiobutton99);
            } else {
                radioGroup.check(R.id.radiobutton33);
            }
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId) {
                        case R.id.radiobutton33 /*2131165203*/:
                            Game.this.getPreferences(0).edit().putInt(Game.BOARDSIZE, 0).commit();
                            return;
                        case R.id.radiobutton66 /*2131165204*/:
                            Game.this.getPreferences(0).edit().putInt(Game.BOARDSIZE, 1).commit();
                            return;
                        case R.id.radiobutton99 /*2131165205*/:
                            Game.this.getPreferences(0).edit().putInt(Game.BOARDSIZE, 2).commit();
                            return;
                        default:
                            return;
                    }
                }
            });
            RadioGroup radioGroup2 = (RadioGroup) findViewById(R.id.radiogroup2);
            if (getPreferences(0).getInt(DIFFICULTY, 0) == 1) {
                radioGroup2.check(R.id.aiNormal);
            } else {
                radioGroup2.check(R.id.aiEasy);
            }
            radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId) {
                        case R.id.aiEasy /*2131165207*/:
                            Game.this.getPreferences(0).edit().putInt(Game.DIFFICULTY, 0).commit();
                            return;
                        case R.id.aiNormal /*2131165208*/:
                            Game.this.getPreferences(0).edit().putInt(Game.DIFFICULTY, 1).commit();
                            return;
                        default:
                            return;
                    }
                }
            });
            View onePlayerButton = findViewById(R.id.one_player_button);
            ((TextView) onePlayerButton).setTypeface(tf2);
            onePlayerButton.setOnClickListener(this);
            View recordsButton = findViewById(R.id.records_button);
            ((TextView) recordsButton).setTypeface(tf2);
            recordsButton.setOnClickListener(this);
            View returnButton2 = findViewById(R.id.return_title_button);
            ((TextView) returnButton2).setTypeface(tf2);
            returnButton2.setOnClickListener(this);
            View moreButton2 = findViewById(R.id.more_game_button);
            ((TextView) moreButton2).setTypeface(tf2);
            moreButton2.setOnClickListener(this);
        }
    }

    public void onClick(View v) {
        if (this.page == 1) {
            switch (v.getId()) {
                case R.id.return_title_button /*2131165199*/:
                    finish();
                    break;
                case R.id.more_game_button /*2131165200*/:
                    startActivity(new Intent(this, MoreActivity.class));
                    break;
                case R.id.clear_button /*2131165201*/:
                    openAlertDialog();
                    break;
            }
        }
        if (this.page == 2) {
            switch (v.getId()) {
                case R.id.one_player_button /*2131165185*/:
                    Intent intent = new Intent(this, Game.class);
                    intent.putExtra(KEY_PAGE_MODE, 0);
                    intent.putExtra(KEY_PLAY_MODE, 1);
                    intent.putExtra(KEY_PLAYERS, 1);
                    startActivity(intent);
                    return;
                case R.id.records_button /*2131165187*/:
                    Log.d(TAG, "clicked on records_button");
                    Intent intent2 = new Intent(this, Game.class);
                    intent2.putExtra(KEY_PAGE_MODE, 1);
                    startActivity(intent2);
                    return;
                case R.id.return_title_button /*2131165199*/:
                    finish();
                    return;
                case R.id.more_game_button /*2131165200*/:
                    startActivity(new Intent(this, MoreActivity.class));
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setTileValid(int x, int y) {
        this.turn++;
        this.maxTurn = this.turn;
        for (int i = 0; i < this.nbTiles; i++) {
            this.board[this.turn][i] = this.board[this.turn - 1][i];
        }
        this.board[this.turn][(this.nbTupple * y) + x] = this.priority;
        Log.d(TAG, "setTileValid:x" + x + ", y " + y);
        checkBoard();
        if (this.playNow == 1) {
            changePriority();
            if (this.priority != 1) {
                comTurn(this.gDiff);
            }
        }
    }

    private void comTurn(int diff) {
        switch (getPreferences(0).getInt(DIFFICULTY, 0)) {
            case 0:
                int aa = new Random().nextInt(100);
                if (aa >= 80 || aa <= 10) {
                    comVeryEasy(this.priority);
                    return;
                } else {
                    comNormal();
                    return;
                }
            case 1:
                comNormal();
                return;
            default:
                return;
        }
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 926 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void comNormal() {
        /*
            r15 = this;
            r14 = 4
            r13 = 3
            r12 = 1
            r2 = 0
            r4 = 0
            r0 = 0
            r8 = 0
            int r10 = r15.nbTiles
            int[] r9 = new int[r10]
            r7 = 0
            r8 = 2
            r2 = 0
        L_0x000e:
            int r10 = r15.nbTupple
            if (r2 < r10) goto L_0x0038
        L_0x0012:
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x011d
            java.lang.String r10 = "Game"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            java.lang.String r12 = "checkComTest1:x"
            r11.<init>(r12)
            java.lang.StringBuilder r11 = r11.append(r2)
            java.lang.String r12 = ", y "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r4)
            java.lang.String r11 = r11.toString()
            android.util.Log.d(r10, r11)
            r15.setTileValid(r2, r4)
        L_0x0037:
            return
        L_0x0038:
            r4 = 0
        L_0x0039:
            int r10 = r15.nbRow
            if (r4 < r10) goto L_0x0044
        L_0x003d:
            int r10 = r15.toAlign
            if (r7 >= r10) goto L_0x0012
            int r2 = r2 + 1
            goto L_0x000e
        L_0x0044:
            int r10 = r15.getTile(r2, r4)
            if (r10 != 0) goto L_0x0080
            r6 = 0
        L_0x004b:
            int r10 = r15.nbTiles
            if (r6 < r10) goto L_0x0087
            int r10 = r15.nbTupple
            int r10 = r10 * r4
            int r10 = r10 + r2
            r9[r10] = r8
            r3 = 0
        L_0x0056:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x005e
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x0094
        L_0x005e:
            int r10 = r15.nbRow
            int r3 = r10 * -1
        L_0x0062:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x006a
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x00b0
        L_0x006a:
            r3 = 0
        L_0x006b:
            int r10 = r15.nbRow
            if (r3 >= r10) goto L_0x0073
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x00d8
        L_0x0073:
            r3 = 0
        L_0x0074:
            int r10 = r15.nbTupple
            int r11 = r15.nbRow
            int r10 = r10 + r11
            int r10 = r10 - r12
            if (r3 >= r10) goto L_0x0080
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x00f4
        L_0x0080:
            int r10 = r15.toAlign
            if (r7 >= r10) goto L_0x003d
            int r4 = r4 + 1
            goto L_0x0039
        L_0x0087:
            int[][] r10 = r15.board
            int r11 = r15.turn
            r10 = r10[r11]
            r10 = r10[r6]
            r9[r6] = r10
            int r6 = r6 + 1
            goto L_0x004b
        L_0x0094:
            r7 = 0
            r5 = 0
        L_0x0096:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x009e
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x00a1
        L_0x009e:
            int r3 = r3 + 1
            goto L_0x0056
        L_0x00a1:
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r10 = r10 + r3
            r10 = r9[r10]
            if (r10 != r8) goto L_0x00ae
            int r7 = r7 + 1
        L_0x00ab:
            int r5 = r5 + 1
            goto L_0x0096
        L_0x00ae:
            r7 = 0
            goto L_0x00ab
        L_0x00b0:
            r7 = 0
            r5 = 0
        L_0x00b2:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x00ba
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x00bd
        L_0x00ba:
            int r3 = r3 + 1
            goto L_0x0062
        L_0x00bd:
            int r10 = r3 + r5
            if (r10 < 0) goto L_0x00d3
            int r10 = r3 + r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x00d3
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 + r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x00d6
            int r7 = r7 + 1
        L_0x00d3:
            int r5 = r5 + 1
            goto L_0x00b2
        L_0x00d6:
            r7 = 0
            goto L_0x00d3
        L_0x00d8:
            r7 = 0
            r5 = 0
        L_0x00da:
            int r10 = r15.nbTupple
            if (r5 >= r10) goto L_0x00e2
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x00e5
        L_0x00e2:
            int r3 = r3 + 1
            goto L_0x006b
        L_0x00e5:
            int r10 = r15.nbTupple
            int r10 = r10 * r3
            int r10 = r10 + r5
            r10 = r9[r10]
            if (r10 != r8) goto L_0x00f2
            int r7 = r7 + 1
        L_0x00ef:
            int r5 = r5 + 1
            goto L_0x00da
        L_0x00f2:
            r7 = 0
            goto L_0x00ef
        L_0x00f4:
            r7 = 0
            r5 = 0
        L_0x00f6:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x00fe
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x0102
        L_0x00fe:
            int r3 = r3 + 1
            goto L_0x0074
        L_0x0102:
            int r10 = r3 - r5
            if (r10 < 0) goto L_0x0118
            int r10 = r3 - r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x0118
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 - r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x011b
            int r7 = r7 + 1
        L_0x0118:
            int r5 = r5 + 1
            goto L_0x00f6
        L_0x011b:
            r7 = 0
            goto L_0x0118
        L_0x011d:
            r8 = 1
            r2 = 0
        L_0x011f:
            int r10 = r15.nbTupple
            if (r2 < r10) goto L_0x014a
        L_0x0123:
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x022f
            java.lang.String r10 = "Game"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            java.lang.String r12 = "checkComTest2:x"
            r11.<init>(r12)
            java.lang.StringBuilder r11 = r11.append(r2)
            java.lang.String r12 = ", y "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r4)
            java.lang.String r11 = r11.toString()
            android.util.Log.d(r10, r11)
            r15.setTileValid(r2, r4)
            goto L_0x0037
        L_0x014a:
            r4 = 0
        L_0x014b:
            int r10 = r15.nbRow
            if (r4 < r10) goto L_0x0156
        L_0x014f:
            int r10 = r15.toAlign
            if (r7 >= r10) goto L_0x0123
            int r2 = r2 + 1
            goto L_0x011f
        L_0x0156:
            int r10 = r15.getTile(r2, r4)
            if (r10 != 0) goto L_0x0192
            r6 = 0
        L_0x015d:
            int r10 = r15.nbTiles
            if (r6 < r10) goto L_0x0199
            int r10 = r15.nbTupple
            int r10 = r10 * r4
            int r10 = r10 + r2
            r9[r10] = r8
            r3 = 0
        L_0x0168:
            int r10 = r15.nbTupple
            int r11 = r15.nbRow
            int r10 = r10 + r11
            int r10 = r10 - r12
            if (r3 >= r10) goto L_0x0174
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x01a6
        L_0x0174:
            r3 = 0
        L_0x0175:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x017d
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x01ce
        L_0x017d:
            r3 = 0
        L_0x017e:
            int r10 = r15.nbRow
            if (r3 >= r10) goto L_0x0186
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x01ea
        L_0x0186:
            int r10 = r15.nbRow
            int r3 = r10 * -1
        L_0x018a:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x0192
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x0206
        L_0x0192:
            int r10 = r15.toAlign
            if (r7 >= r10) goto L_0x014f
            int r4 = r4 + 1
            goto L_0x014b
        L_0x0199:
            int[][] r10 = r15.board
            int r11 = r15.turn
            r10 = r10[r11]
            r10 = r10[r6]
            r9[r6] = r10
            int r6 = r6 + 1
            goto L_0x015d
        L_0x01a6:
            r7 = 0
            r5 = 0
        L_0x01a8:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x01b0
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x01b3
        L_0x01b0:
            int r3 = r3 + 1
            goto L_0x0168
        L_0x01b3:
            int r10 = r3 - r5
            if (r10 < 0) goto L_0x01c9
            int r10 = r3 - r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x01c9
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 - r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x01cc
            int r7 = r7 + 1
        L_0x01c9:
            int r5 = r5 + 1
            goto L_0x01a8
        L_0x01cc:
            r7 = 0
            goto L_0x01c9
        L_0x01ce:
            r7 = 0
            r5 = 0
        L_0x01d0:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x01d8
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x01db
        L_0x01d8:
            int r3 = r3 + 1
            goto L_0x0175
        L_0x01db:
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r10 = r10 + r3
            r10 = r9[r10]
            if (r10 != r8) goto L_0x01e8
            int r7 = r7 + 1
        L_0x01e5:
            int r5 = r5 + 1
            goto L_0x01d0
        L_0x01e8:
            r7 = 0
            goto L_0x01e5
        L_0x01ea:
            r7 = 0
            r5 = 0
        L_0x01ec:
            int r10 = r15.nbTupple
            if (r5 >= r10) goto L_0x01f4
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x01f7
        L_0x01f4:
            int r3 = r3 + 1
            goto L_0x017e
        L_0x01f7:
            int r10 = r15.nbTupple
            int r10 = r10 * r3
            int r10 = r10 + r5
            r10 = r9[r10]
            if (r10 != r8) goto L_0x0204
            int r7 = r7 + 1
        L_0x0201:
            int r5 = r5 + 1
            goto L_0x01ec
        L_0x0204:
            r7 = 0
            goto L_0x0201
        L_0x0206:
            r7 = 0
            r5 = 0
        L_0x0208:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x0210
            int r10 = r15.toAlign
            if (r7 < r10) goto L_0x0214
        L_0x0210:
            int r3 = r3 + 1
            goto L_0x018a
        L_0x0214:
            int r10 = r3 + r5
            if (r10 < 0) goto L_0x022a
            int r10 = r3 + r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x022a
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 + r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x022d
            int r7 = r7 + 1
        L_0x022a:
            int r5 = r5 + 1
            goto L_0x0208
        L_0x022d:
            r7 = 0
            goto L_0x022a
        L_0x022f:
            r1 = 0
            r8 = 1
            r2 = 0
        L_0x0232:
            int r10 = r15.nbTupple
            if (r2 < r10) goto L_0x026b
        L_0x0236:
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x023c
            r1 = 1
        L_0x023c:
            r2 = 0
        L_0x023d:
            int r10 = r15.nbTupple
            if (r2 < r10) goto L_0x0354
        L_0x0241:
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x0444
            if (r1 != 0) goto L_0x0444
            java.lang.String r10 = "Game"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            java.lang.String r12 = "checkComTest3:x"
            r11.<init>(r12)
            java.lang.StringBuilder r11 = r11.append(r2)
            java.lang.String r12 = ", y "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r4)
            java.lang.String r11 = r11.toString()
            android.util.Log.d(r10, r11)
            r15.setTileValid(r2, r4)
            goto L_0x0037
        L_0x026b:
            r4 = 0
        L_0x026c:
            int r10 = r15.nbRow
            if (r4 < r10) goto L_0x0278
        L_0x0270:
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 >= r10) goto L_0x0236
            int r2 = r2 + 1
            goto L_0x0232
        L_0x0278:
            int r10 = r15.getTile(r2, r4)
            if (r10 != 0) goto L_0x02b2
            r6 = 0
        L_0x027f:
            int r10 = r15.nbTiles
            if (r6 < r10) goto L_0x02ba
            r3 = 0
        L_0x0284:
            int r10 = r15.nbRow
            if (r3 >= r10) goto L_0x028d
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x02c7
        L_0x028d:
            r3 = 0
        L_0x028e:
            int r10 = r15.nbTupple
            int r11 = r15.nbRow
            int r10 = r10 + r11
            int r10 = r10 - r12
            if (r3 >= r10) goto L_0x029b
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x02e4
        L_0x029b:
            int r10 = r15.nbRow
            int r3 = r10 * -1
        L_0x029f:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x02a8
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x030d
        L_0x02a8:
            r3 = 0
        L_0x02a9:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x02b2
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x0336
        L_0x02b2:
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 >= r10) goto L_0x0270
            int r4 = r4 + 1
            goto L_0x026c
        L_0x02ba:
            int[][] r10 = r15.board
            int r11 = r15.turn
            r10 = r10[r11]
            r10 = r10[r6]
            r9[r6] = r10
            int r6 = r6 + 1
            goto L_0x027f
        L_0x02c7:
            r7 = 0
            r5 = 0
        L_0x02c9:
            int r10 = r15.nbTupple
            if (r5 >= r10) goto L_0x02d2
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x02d5
        L_0x02d2:
            int r3 = r3 + 1
            goto L_0x0284
        L_0x02d5:
            int r10 = r15.nbTupple
            int r10 = r10 * r3
            int r10 = r10 + r5
            r10 = r9[r10]
            if (r10 != r8) goto L_0x02e2
            int r7 = r7 + 1
        L_0x02df:
            int r5 = r5 + 1
            goto L_0x02c9
        L_0x02e2:
            r7 = 0
            goto L_0x02df
        L_0x02e4:
            r7 = 0
            r5 = 0
        L_0x02e6:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x02ef
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x02f2
        L_0x02ef:
            int r3 = r3 + 1
            goto L_0x028e
        L_0x02f2:
            int r10 = r3 - r5
            if (r10 < 0) goto L_0x0308
            int r10 = r3 - r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x0308
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 - r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x030b
            int r7 = r7 + 1
        L_0x0308:
            int r5 = r5 + 1
            goto L_0x02e6
        L_0x030b:
            r7 = 0
            goto L_0x0308
        L_0x030d:
            r7 = 0
            r5 = 0
        L_0x030f:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x0318
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x031b
        L_0x0318:
            int r3 = r3 + 1
            goto L_0x029f
        L_0x031b:
            int r10 = r3 + r5
            if (r10 < 0) goto L_0x0331
            int r10 = r3 + r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x0331
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 + r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x0334
            int r7 = r7 + 1
        L_0x0331:
            int r5 = r5 + 1
            goto L_0x030f
        L_0x0334:
            r7 = 0
            goto L_0x0331
        L_0x0336:
            r7 = 0
            r5 = 0
        L_0x0338:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x0341
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x0345
        L_0x0341:
            int r3 = r3 + 1
            goto L_0x02a9
        L_0x0345:
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r10 = r10 + r3
            r10 = r9[r10]
            if (r10 != r8) goto L_0x0352
            int r7 = r7 + 1
        L_0x034f:
            int r5 = r5 + 1
            goto L_0x0338
        L_0x0352:
            r7 = 0
            goto L_0x034f
        L_0x0354:
            r4 = 0
        L_0x0355:
            int r10 = r15.nbRow
            if (r4 < r10) goto L_0x0362
        L_0x0359:
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 >= r10) goto L_0x0241
            int r2 = r2 + 1
            goto L_0x023d
        L_0x0362:
            int r10 = r15.getTile(r2, r4)
            if (r10 != 0) goto L_0x03a2
            r6 = 0
        L_0x0369:
            int r10 = r15.nbTiles
            if (r6 < r10) goto L_0x03aa
            int r10 = r15.nbTupple
            int r10 = r10 * r4
            int r10 = r10 + r2
            r9[r10] = r8
            r3 = 0
        L_0x0374:
            int r10 = r15.nbRow
            if (r3 >= r10) goto L_0x037d
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x03b7
        L_0x037d:
            r3 = 0
        L_0x037e:
            int r10 = r15.nbTupple
            int r11 = r15.nbRow
            int r10 = r10 + r11
            int r10 = r10 - r12
            if (r3 >= r10) goto L_0x038b
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x03d4
        L_0x038b:
            int r10 = r15.nbRow
            int r3 = r10 * -1
        L_0x038f:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x0398
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x03fd
        L_0x0398:
            r3 = 0
        L_0x0399:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x03a2
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x0426
        L_0x03a2:
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 >= r10) goto L_0x0359
            int r4 = r4 + 1
            goto L_0x0355
        L_0x03aa:
            int[][] r10 = r15.board
            int r11 = r15.turn
            r10 = r10[r11]
            r10 = r10[r6]
            r9[r6] = r10
            int r6 = r6 + 1
            goto L_0x0369
        L_0x03b7:
            r7 = 0
            r5 = 0
        L_0x03b9:
            int r10 = r15.nbTupple
            if (r5 >= r10) goto L_0x03c2
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x03c5
        L_0x03c2:
            int r3 = r3 + 1
            goto L_0x0374
        L_0x03c5:
            int r10 = r15.nbTupple
            int r10 = r10 * r3
            int r10 = r10 + r5
            r10 = r9[r10]
            if (r10 != r8) goto L_0x03d2
            int r7 = r7 + 1
        L_0x03cf:
            int r5 = r5 + 1
            goto L_0x03b9
        L_0x03d2:
            r7 = 0
            goto L_0x03cf
        L_0x03d4:
            r7 = 0
            r5 = 0
        L_0x03d6:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x03df
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x03e2
        L_0x03df:
            int r3 = r3 + 1
            goto L_0x037e
        L_0x03e2:
            int r10 = r3 - r5
            if (r10 < 0) goto L_0x03f8
            int r10 = r3 - r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x03f8
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 - r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x03fb
            int r7 = r7 + 1
        L_0x03f8:
            int r5 = r5 + 1
            goto L_0x03d6
        L_0x03fb:
            r7 = 0
            goto L_0x03f8
        L_0x03fd:
            r7 = 0
            r5 = 0
        L_0x03ff:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x0408
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x040b
        L_0x0408:
            int r3 = r3 + 1
            goto L_0x038f
        L_0x040b:
            int r10 = r3 + r5
            if (r10 < 0) goto L_0x0421
            int r10 = r3 + r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x0421
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 + r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x0424
            int r7 = r7 + 1
        L_0x0421:
            int r5 = r5 + 1
            goto L_0x03ff
        L_0x0424:
            r7 = 0
            goto L_0x0421
        L_0x0426:
            r7 = 0
            r5 = 0
        L_0x0428:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x0431
            int r10 = r15.toAlign
            int r10 = r10 - r12
            if (r7 < r10) goto L_0x0435
        L_0x0431:
            int r3 = r3 + 1
            goto L_0x0399
        L_0x0435:
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r10 = r10 + r3
            r10 = r9[r10]
            if (r10 != r8) goto L_0x0442
            int r7 = r7 + 1
        L_0x043f:
            int r5 = r5 + 1
            goto L_0x0428
        L_0x0442:
            r7 = 0
            goto L_0x043f
        L_0x0444:
            r1 = 0
            r8 = 2
            r2 = 0
        L_0x0447:
            int r10 = r15.nbTupple
            if (r2 < r10) goto L_0x047a
        L_0x044b:
            if (r7 < r14) goto L_0x044e
            r1 = 1
        L_0x044e:
            r2 = 0
        L_0x044f:
            int r10 = r15.nbTupple
            if (r2 < r10) goto L_0x0545
        L_0x0453:
            if (r7 < r14) goto L_0x0617
            if (r1 != 0) goto L_0x0617
            java.lang.String r10 = "Game"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            java.lang.String r12 = "checkComTest4:x"
            r11.<init>(r12)
            java.lang.StringBuilder r11 = r11.append(r2)
            java.lang.String r12 = ", y "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r4)
            java.lang.String r11 = r11.toString()
            android.util.Log.d(r10, r11)
            r15.setTileValid(r2, r4)
            goto L_0x0037
        L_0x047a:
            r4 = 0
        L_0x047b:
            int r10 = r15.nbRow
            if (r4 < r10) goto L_0x0484
        L_0x047f:
            if (r7 >= r14) goto L_0x044b
            int r2 = r2 + 1
            goto L_0x0447
        L_0x0484:
            int r10 = r15.getTile(r2, r4)
            if (r10 != 0) goto L_0x04b2
            r6 = 0
        L_0x048b:
            int r10 = r15.nbTiles
            if (r6 < r10) goto L_0x04b7
            int r10 = r15.nbRow
            int r3 = r10 * -1
        L_0x0493:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x0499
            if (r7 < r14) goto L_0x04c4
        L_0x0499:
            r3 = 0
        L_0x049a:
            int r10 = r15.nbRow
            if (r3 >= r10) goto L_0x04a0
            if (r7 < r14) goto L_0x04ea
        L_0x04a0:
            r3 = 0
        L_0x04a1:
            int r10 = r15.nbTupple
            int r11 = r15.nbRow
            int r10 = r10 + r11
            int r10 = r10 - r12
            if (r3 >= r10) goto L_0x04ab
            if (r7 < r14) goto L_0x0504
        L_0x04ab:
            r3 = 0
        L_0x04ac:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x04b2
            if (r7 < r14) goto L_0x052a
        L_0x04b2:
            if (r7 >= r14) goto L_0x047f
            int r4 = r4 + 1
            goto L_0x047b
        L_0x04b7:
            int[][] r10 = r15.board
            int r11 = r15.turn
            r10 = r10[r11]
            r10 = r10[r6]
            r9[r6] = r10
            int r6 = r6 + 1
            goto L_0x048b
        L_0x04c4:
            r7 = 0
            r5 = 0
        L_0x04c6:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x04cc
            if (r7 < r14) goto L_0x04cf
        L_0x04cc:
            int r3 = r3 + 1
            goto L_0x0493
        L_0x04cf:
            int r10 = r3 + r5
            if (r10 < 0) goto L_0x04e5
            int r10 = r3 + r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x04e5
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 + r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x04e8
            int r7 = r7 + 1
        L_0x04e5:
            int r5 = r5 + 1
            goto L_0x04c6
        L_0x04e8:
            r7 = 0
            goto L_0x04e5
        L_0x04ea:
            r7 = 0
            r5 = 0
        L_0x04ec:
            int r10 = r15.nbTupple
            if (r5 >= r10) goto L_0x04f2
            if (r7 < r14) goto L_0x04f5
        L_0x04f2:
            int r3 = r3 + 1
            goto L_0x049a
        L_0x04f5:
            int r10 = r15.nbTupple
            int r10 = r10 * r3
            int r10 = r10 + r5
            r10 = r9[r10]
            if (r10 != r8) goto L_0x0502
            int r7 = r7 + 1
        L_0x04ff:
            int r5 = r5 + 1
            goto L_0x04ec
        L_0x0502:
            r7 = 0
            goto L_0x04ff
        L_0x0504:
            r7 = 0
            r5 = 0
        L_0x0506:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x050c
            if (r7 < r14) goto L_0x050f
        L_0x050c:
            int r3 = r3 + 1
            goto L_0x04a1
        L_0x050f:
            int r10 = r3 - r5
            if (r10 < 0) goto L_0x0525
            int r10 = r3 - r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x0525
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 - r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x0528
            int r7 = r7 + 1
        L_0x0525:
            int r5 = r5 + 1
            goto L_0x0506
        L_0x0528:
            r7 = 0
            goto L_0x0525
        L_0x052a:
            r7 = 0
            r5 = 0
        L_0x052c:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x0532
            if (r7 < r14) goto L_0x0536
        L_0x0532:
            int r3 = r3 + 1
            goto L_0x04ac
        L_0x0536:
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r10 = r10 + r3
            r10 = r9[r10]
            if (r10 != r8) goto L_0x0543
            int r7 = r7 + 1
        L_0x0540:
            int r5 = r5 + 1
            goto L_0x052c
        L_0x0543:
            r7 = 0
            goto L_0x0540
        L_0x0545:
            r4 = 0
        L_0x0546:
            int r10 = r15.nbRow
            if (r4 < r10) goto L_0x0550
        L_0x054a:
            if (r7 >= r14) goto L_0x0453
            int r2 = r2 + 1
            goto L_0x044f
        L_0x0550:
            int r10 = r15.getTile(r2, r4)
            if (r10 != 0) goto L_0x0584
            r6 = 0
        L_0x0557:
            int r10 = r15.nbTiles
            if (r6 < r10) goto L_0x0589
            int r10 = r15.nbTupple
            int r10 = r10 * r4
            int r10 = r10 + r2
            r9[r10] = r8
            int r10 = r15.nbRow
            int r3 = r10 * -1
        L_0x0565:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x056b
            if (r7 < r14) goto L_0x0596
        L_0x056b:
            r3 = 0
        L_0x056c:
            int r10 = r15.nbRow
            if (r3 >= r10) goto L_0x0572
            if (r7 < r14) goto L_0x05bc
        L_0x0572:
            r3 = 0
        L_0x0573:
            int r10 = r15.nbTupple
            int r11 = r15.nbRow
            int r10 = r10 + r11
            int r10 = r10 - r12
            if (r3 >= r10) goto L_0x057d
            if (r7 < r14) goto L_0x05d6
        L_0x057d:
            r3 = 0
        L_0x057e:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x0584
            if (r7 < r14) goto L_0x05fc
        L_0x0584:
            if (r7 >= r14) goto L_0x054a
            int r4 = r4 + 1
            goto L_0x0546
        L_0x0589:
            int[][] r10 = r15.board
            int r11 = r15.turn
            r10 = r10[r11]
            r10 = r10[r6]
            r9[r6] = r10
            int r6 = r6 + 1
            goto L_0x0557
        L_0x0596:
            r7 = 0
            r5 = 0
        L_0x0598:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x059e
            if (r7 < r14) goto L_0x05a1
        L_0x059e:
            int r3 = r3 + 1
            goto L_0x0565
        L_0x05a1:
            int r10 = r3 + r5
            if (r10 < 0) goto L_0x05b7
            int r10 = r3 + r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x05b7
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 + r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x05ba
            int r7 = r7 + 1
        L_0x05b7:
            int r5 = r5 + 1
            goto L_0x0598
        L_0x05ba:
            r7 = 0
            goto L_0x05b7
        L_0x05bc:
            r7 = 0
            r5 = 0
        L_0x05be:
            int r10 = r15.nbTupple
            if (r5 >= r10) goto L_0x05c4
            if (r7 < r14) goto L_0x05c7
        L_0x05c4:
            int r3 = r3 + 1
            goto L_0x056c
        L_0x05c7:
            int r10 = r15.nbTupple
            int r10 = r10 * r3
            int r10 = r10 + r5
            r10 = r9[r10]
            if (r10 != r8) goto L_0x05d4
            int r7 = r7 + 1
        L_0x05d1:
            int r5 = r5 + 1
            goto L_0x05be
        L_0x05d4:
            r7 = 0
            goto L_0x05d1
        L_0x05d6:
            r7 = 0
            r5 = 0
        L_0x05d8:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x05de
            if (r7 < r14) goto L_0x05e1
        L_0x05de:
            int r3 = r3 + 1
            goto L_0x0573
        L_0x05e1:
            int r10 = r3 - r5
            if (r10 < 0) goto L_0x05f7
            int r10 = r3 - r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x05f7
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 - r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x05fa
            int r7 = r7 + 1
        L_0x05f7:
            int r5 = r5 + 1
            goto L_0x05d8
        L_0x05fa:
            r7 = 0
            goto L_0x05f7
        L_0x05fc:
            r7 = 0
            r5 = 0
        L_0x05fe:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x0604
            if (r7 < r14) goto L_0x0608
        L_0x0604:
            int r3 = r3 + 1
            goto L_0x057e
        L_0x0608:
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r10 = r10 + r3
            r10 = r9[r10]
            if (r10 != r8) goto L_0x0615
            int r7 = r7 + 1
        L_0x0612:
            int r5 = r5 + 1
            goto L_0x05fe
        L_0x0615:
            r7 = 0
            goto L_0x0612
        L_0x0617:
            r8 = 2
            r1 = 0
            r2 = 0
        L_0x061a:
            int r10 = r15.nbTupple
            if (r2 < r10) goto L_0x064d
        L_0x061e:
            if (r7 < r13) goto L_0x0621
            r1 = 1
        L_0x0621:
            r2 = 0
        L_0x0622:
            int r10 = r15.nbTupple
            if (r2 < r10) goto L_0x0717
        L_0x0626:
            if (r7 < r13) goto L_0x07e8
            if (r1 != 0) goto L_0x07e8
            java.lang.String r10 = "Game"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            java.lang.String r12 = "checkComTest5:x"
            r11.<init>(r12)
            java.lang.StringBuilder r11 = r11.append(r2)
            java.lang.String r12 = ", y "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r4)
            java.lang.String r11 = r11.toString()
            android.util.Log.d(r10, r11)
            r15.setTileValid(r2, r4)
            goto L_0x0037
        L_0x064d:
            r4 = 0
        L_0x064e:
            int r10 = r15.nbRow
            if (r4 < r10) goto L_0x0657
        L_0x0652:
            if (r7 >= r13) goto L_0x061e
            int r2 = r2 + 1
            goto L_0x061a
        L_0x0657:
            int r10 = r15.getTile(r2, r4)
            if (r10 != 0) goto L_0x0685
            r6 = 0
        L_0x065e:
            int r10 = r15.nbTiles
            if (r6 < r10) goto L_0x068a
            r3 = 0
        L_0x0663:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x0669
            if (r7 < r13) goto L_0x0697
        L_0x0669:
            int r10 = r15.nbRow
            int r3 = r10 * -1
        L_0x066d:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x0673
            if (r7 < r13) goto L_0x06b1
        L_0x0673:
            r3 = 0
        L_0x0674:
            int r10 = r15.nbRow
            if (r3 >= r10) goto L_0x067a
            if (r7 < r13) goto L_0x06d7
        L_0x067a:
            r3 = 0
        L_0x067b:
            int r10 = r15.nbTupple
            int r11 = r15.nbRow
            int r10 = r10 + r11
            int r10 = r10 - r12
            if (r3 >= r10) goto L_0x0685
            if (r7 < r13) goto L_0x06f1
        L_0x0685:
            if (r7 >= r13) goto L_0x0652
            int r4 = r4 + 1
            goto L_0x064e
        L_0x068a:
            int[][] r10 = r15.board
            int r11 = r15.turn
            r10 = r10[r11]
            r10 = r10[r6]
            r9[r6] = r10
            int r6 = r6 + 1
            goto L_0x065e
        L_0x0697:
            r7 = 0
            r5 = 0
        L_0x0699:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x069f
            if (r7 < r13) goto L_0x06a2
        L_0x069f:
            int r3 = r3 + 1
            goto L_0x0663
        L_0x06a2:
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r10 = r10 + r3
            r10 = r9[r10]
            if (r10 != r8) goto L_0x06af
            int r7 = r7 + 1
        L_0x06ac:
            int r5 = r5 + 1
            goto L_0x0699
        L_0x06af:
            r7 = 0
            goto L_0x06ac
        L_0x06b1:
            r7 = 0
            r5 = 0
        L_0x06b3:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x06b9
            if (r7 < r13) goto L_0x06bc
        L_0x06b9:
            int r3 = r3 + 1
            goto L_0x066d
        L_0x06bc:
            int r10 = r3 + r5
            if (r10 < 0) goto L_0x06d2
            int r10 = r3 + r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x06d2
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 + r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x06d5
            int r7 = r7 + 1
        L_0x06d2:
            int r5 = r5 + 1
            goto L_0x06b3
        L_0x06d5:
            r7 = 0
            goto L_0x06d2
        L_0x06d7:
            r7 = 0
            r5 = 0
        L_0x06d9:
            int r10 = r15.nbTupple
            if (r5 >= r10) goto L_0x06df
            if (r7 < r13) goto L_0x06e2
        L_0x06df:
            int r3 = r3 + 1
            goto L_0x0674
        L_0x06e2:
            int r10 = r15.nbTupple
            int r10 = r10 * r3
            int r10 = r10 + r5
            r10 = r9[r10]
            if (r10 != r8) goto L_0x06ef
            int r7 = r7 + 1
        L_0x06ec:
            int r5 = r5 + 1
            goto L_0x06d9
        L_0x06ef:
            r7 = 0
            goto L_0x06ec
        L_0x06f1:
            r7 = 0
            r5 = 0
        L_0x06f3:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x06f9
            if (r7 < r13) goto L_0x06fc
        L_0x06f9:
            int r3 = r3 + 1
            goto L_0x067b
        L_0x06fc:
            int r10 = r3 - r5
            if (r10 < 0) goto L_0x0712
            int r10 = r3 - r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x0712
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 - r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x0715
            int r7 = r7 + 1
        L_0x0712:
            int r5 = r5 + 1
            goto L_0x06f3
        L_0x0715:
            r7 = 0
            goto L_0x0712
        L_0x0717:
            r4 = 0
        L_0x0718:
            int r10 = r15.nbRow
            if (r4 < r10) goto L_0x0722
        L_0x071c:
            if (r7 >= r13) goto L_0x0626
            int r2 = r2 + 1
            goto L_0x0622
        L_0x0722:
            int r10 = r15.getTile(r2, r4)
            if (r10 != 0) goto L_0x0756
            r6 = 0
        L_0x0729:
            int r10 = r15.nbTiles
            if (r6 < r10) goto L_0x075b
            int r10 = r15.nbTupple
            int r10 = r10 * r4
            int r10 = r10 + r2
            r9[r10] = r8
            r3 = 0
        L_0x0734:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x073a
            if (r7 < r13) goto L_0x0768
        L_0x073a:
            int r10 = r15.nbRow
            int r3 = r10 * -1
        L_0x073e:
            int r10 = r15.nbTupple
            if (r3 >= r10) goto L_0x0744
            if (r7 < r13) goto L_0x0782
        L_0x0744:
            r3 = 0
        L_0x0745:
            int r10 = r15.nbRow
            if (r3 >= r10) goto L_0x074b
            if (r7 < r13) goto L_0x07a8
        L_0x074b:
            r3 = 0
        L_0x074c:
            int r10 = r15.nbTupple
            int r11 = r15.nbRow
            int r10 = r10 + r11
            int r10 = r10 - r12
            if (r3 >= r10) goto L_0x0756
            if (r7 < r13) goto L_0x07c2
        L_0x0756:
            if (r7 >= r13) goto L_0x071c
            int r4 = r4 + 1
            goto L_0x0718
        L_0x075b:
            int[][] r10 = r15.board
            int r11 = r15.turn
            r10 = r10[r11]
            r10 = r10[r6]
            r9[r6] = r10
            int r6 = r6 + 1
            goto L_0x0729
        L_0x0768:
            r7 = 0
            r5 = 0
        L_0x076a:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x0770
            if (r7 < r13) goto L_0x0773
        L_0x0770:
            int r3 = r3 + 1
            goto L_0x0734
        L_0x0773:
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r10 = r10 + r3
            r10 = r9[r10]
            if (r10 != r8) goto L_0x0780
            int r7 = r7 + 1
        L_0x077d:
            int r5 = r5 + 1
            goto L_0x076a
        L_0x0780:
            r7 = 0
            goto L_0x077d
        L_0x0782:
            r7 = 0
            r5 = 0
        L_0x0784:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x078a
            if (r7 < r13) goto L_0x078d
        L_0x078a:
            int r3 = r3 + 1
            goto L_0x073e
        L_0x078d:
            int r10 = r3 + r5
            if (r10 < 0) goto L_0x07a3
            int r10 = r3 + r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x07a3
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 + r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x07a6
            int r7 = r7 + 1
        L_0x07a3:
            int r5 = r5 + 1
            goto L_0x0784
        L_0x07a6:
            r7 = 0
            goto L_0x07a3
        L_0x07a8:
            r7 = 0
            r5 = 0
        L_0x07aa:
            int r10 = r15.nbTupple
            if (r5 >= r10) goto L_0x07b0
            if (r7 < r13) goto L_0x07b3
        L_0x07b0:
            int r3 = r3 + 1
            goto L_0x0745
        L_0x07b3:
            int r10 = r15.nbTupple
            int r10 = r10 * r3
            int r10 = r10 + r5
            r10 = r9[r10]
            if (r10 != r8) goto L_0x07c0
            int r7 = r7 + 1
        L_0x07bd:
            int r5 = r5 + 1
            goto L_0x07aa
        L_0x07c0:
            r7 = 0
            goto L_0x07bd
        L_0x07c2:
            r7 = 0
            r5 = 0
        L_0x07c4:
            int r10 = r15.nbRow
            if (r5 >= r10) goto L_0x07ca
            if (r7 < r13) goto L_0x07cd
        L_0x07ca:
            int r3 = r3 + 1
            goto L_0x074c
        L_0x07cd:
            int r10 = r3 - r5
            if (r10 < 0) goto L_0x07e3
            int r10 = r3 - r5
            int r11 = r15.nbTupple
            if (r10 >= r11) goto L_0x07e3
            int r10 = r15.nbTupple
            int r10 = r10 * r5
            int r11 = r3 - r5
            int r10 = r10 + r11
            r10 = r9[r10]
            if (r10 != r8) goto L_0x07e6
            int r7 = r7 + 1
        L_0x07e3:
            int r5 = r5 + 1
            goto L_0x07c4
        L_0x07e6:
            r7 = 0
            goto L_0x07e3
        L_0x07e8:
            int r10 = r15.priority
            r15.comVeryEasy(r10)
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.game.maruBatuGame.Game.comNormal():void");
    }

    private void comVeryEasy(int p) {
        int x;
        int y;
        int f = 0;
        int g = 0;
        while (true) {
            x = new Random().nextInt(this.nbTupple);
            y = new Random().nextInt(this.nbRow);
            if (getTile(y, x) == 0) {
                if (y <= 0 || x <= 0 || getTile(y - 1, x - 1) != p) {
                    if (y <= 0 || x >= this.nbRow - 1 || getTile(y - 1, x + 1) != p) {
                        if (this.nbTupple - 1 <= y || x <= 0 || getTile(y + 1, x - 1) != p) {
                            if (this.nbTupple - 1 <= y || x >= this.nbRow - 1 || getTile(y + 1, x + 1) != p) {
                                if (y <= 0 || getTile(y - 1, x) != p) {
                                    if (x <= 0 || getTile(y, x - 1) != p) {
                                        if (this.nbTupple - 1 <= y || getTile(y + 1, x) != p) {
                                            if (this.nbTupple - 1 > x && getTile(y, x + 1) == p) {
                                                f = 1;
                                                break;
                                            }
                                        } else {
                                            f = 1;
                                            break;
                                        }
                                    } else {
                                        f = 1;
                                        break;
                                    }
                                } else {
                                    f = 1;
                                    break;
                                }
                            } else {
                                f = 1;
                                break;
                            }
                        } else {
                            f = 1;
                            break;
                        }
                    } else {
                        f = 1;
                        break;
                    }
                } else {
                    f = 1;
                    break;
                }
            }
            g++;
            if (g >= 1000) {
                break;
            }
        }
        if (f == 1) {
            Log.d(TAG, "checkComTest6:x" + y + ", y " + x);
            setTileValid(y, x);
            return;
        }
        comVeryEasy2();
    }

    private void comVeryEasy2() {
        int f = 0;
        while (true) {
            int x = new Random().nextInt(this.nbTupple);
            int y = new Random().nextInt(this.nbRow);
            if (getTile(y, x) == 0) {
                if (y > 0 && x > 0 && getTile(y - 1, x - 1) != 0) {
                    f = 1;
                }
                if (y > 0 && x < this.nbRow - 1 && getTile(y - 1, x + 1) != 0) {
                    f = 1;
                }
                if (this.nbTupple - 1 > y && x > 0 && getTile(y + 1, x - 1) != 0) {
                    f = 1;
                }
                if (this.nbTupple - 1 > y && x < this.nbRow - 1 && getTile(y + 1, x + 1) != 0) {
                    f = 1;
                }
                if (y > 0 && getTile(y - 1, x) != 0) {
                    f = 1;
                }
                if (x > 0 && getTile(y, x - 1) != 0) {
                    f = 1;
                }
                if (this.nbTupple - 1 > y && getTile(y + 1, x) != 0) {
                    f = 1;
                }
                if (this.nbTupple - 1 > x && getTile(y, x + 1) != 0) {
                    f = 1;
                }
                if (f == 1) {
                    Log.d(TAG, "checkComTest7:x" + y + ", y " + x);
                    setTileValid(y, x);
                    return;
                }
            }
        }
    }

    private void changePriority() {
        if (this.priority == this.nbPlayers) {
            this.priority = 1;
        } else {
            this.priority++;
        }
    }

    private void checkBoard() {
        int nbPrim = 0;
        for (int i = 0; i < this.nbTupple && nbPrim < this.toAlign; i++) {
            nbPrim = 0;
            for (int j = 0; j < this.nbRow && nbPrim < this.toAlign; j++) {
                if (getTile(i, j) == this.priority) {
                    nbPrim++;
                } else {
                    nbPrim = 0;
                }
            }
        }
        for (int i2 = 0; i2 < this.nbRow && nbPrim < this.toAlign; i2++) {
            int nbPrim2 = 0;
            for (int j2 = 0; j2 < this.nbTupple && nbPrim < this.toAlign; j2++) {
                if (getTile(j2, i2) == this.priority) {
                    nbPrim2 = nbPrim + 1;
                } else {
                    nbPrim2 = 0;
                }
            }
        }
        for (int i3 = this.nbRow * -1; i3 < this.nbTupple && nbPrim < this.toAlign; i3++) {
            nbPrim = 0;
            for (int j3 = 0; j3 < this.nbRow && nbPrim < this.toAlign; j3++) {
                if (i3 + j3 >= 0 && i3 + j3 < this.nbTupple) {
                    if (getTile(i3 + j3, j3) == this.priority) {
                        nbPrim++;
                    } else {
                        nbPrim = 0;
                    }
                }
            }
        }
        for (int i4 = 0; i4 < (this.nbTupple + this.nbRow) - 1 && nbPrim < this.toAlign; i4++) {
            nbPrim = 0;
            for (int j4 = 0; j4 < this.nbRow && nbPrim < this.toAlign; j4++) {
                if (i4 - j4 >= 0 && i4 - j4 < this.nbTupple) {
                    if (getTile(i4 - j4, j4) == this.priority) {
                        nbPrim++;
                    } else {
                        nbPrim = 0;
                    }
                }
            }
        }
        if (nbPrim >= this.toAlign) {
            Toast resultToast = Toast.makeText(this, String.valueOf(getSymbole(this.priority)) + " won", 0);
            resultToast.setGravity(17, 0, 0);
            resultToast.show();
            scoreRecord(this.priority);
            this.playNow = 0;
        } else if (this.turn >= this.nbTiles) {
            Toast resultToast2 = Toast.makeText(this, "tie", 0);
            resultToast2.setGravity(17, 0, 0);
            resultToast2.show();
            scoreRecord(0);
            this.playNow = 0;
        }
    }

    /* access modifiers changed from: protected */
    public String getTileString(int x, int y) {
        return getSymbole(getTile(x, y));
    }

    /* access modifiers changed from: protected */
    public String getSymbole(int i) {
        switch (i) {
            case 0:
                return " ";
            case 1:
                return "X";
            case 2:
                return "O";
            case 3:
                return "A";
            case 4:
                return "D";
            default:
                return "E";
        }
    }

    /* access modifiers changed from: protected */
    public int getTile(int i) {
        return this.board[this.turn][i];
    }

    /* access modifiers changed from: protected */
    public int getTile(int x, int y) {
        return this.board[this.turn][(this.nbTupple * y) + x];
    }

    public void doUndo() {
        if (this.turn == this.maxTurn && this.maxTurn % 2 == 1) {
            changePriority();
            this.turn--;
        } else if (this.turn > 0) {
            this.turn -= 2;
        }
        if (this.turn < 0) {
            this.turn = 0;
            changePriority();
        }
    }

    public void doRedo() {
        if (this.turn < this.maxTurn) {
            this.turn += 2;
        }
        if (this.turn > this.maxTurn) {
            this.turn = this.maxTurn;
            changePriority();
        }
    }

    public void resetGame() {
        for (int i = 0; i <= this.nbTiles; i++) {
            for (int j = 0; j < this.nbTiles; j++) {
                this.board[i][j] = 0;
            }
        }
        this.maxTurn = 0;
        this.turn = 0;
        this.priority = 1;
        this.playNow = 1;
    }

    public void toTitle() {
        finish();
    }

    private void scoreRecord(int result) {
        Log.d(TAG, String.valueOf(result) + "won");
        if (this.bSize == 0 && this.gDiff == 0) {
            switch (result) {
                case 0:
                    getPreferences(0).edit().putString(DRAW33, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(DRAW33, "0")) + 1)).toString()).commit();
                    return;
                case 1:
                    getPreferences(0).edit().putString(WIN33, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(WIN33, "0")) + 1)).toString()).commit();
                    return;
                case 2:
                    getPreferences(0).edit().putString(LOSE33, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(LOSE33, "0")) + 1)).toString()).commit();
                    return;
                default:
                    return;
            }
        } else if (this.bSize == 1 && this.gDiff == 0) {
            switch (result) {
                case 0:
                    getPreferences(0).edit().putString(DRAW66, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(DRAW66, "0")) + 1)).toString()).commit();
                    return;
                case 1:
                    getPreferences(0).edit().putString(WIN66, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(WIN66, "0")) + 1)).toString()).commit();
                    return;
                case 2:
                    getPreferences(0).edit().putString(LOSE66, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(LOSE66, "0")) + 1)).toString()).commit();
                    return;
                default:
                    return;
            }
        } else if (this.bSize == 2 && this.gDiff == 0) {
            switch (result) {
                case 0:
                    getPreferences(0).edit().putString(DRAW99, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(DRAW99, "0")) + 1)).toString()).commit();
                    return;
                case 1:
                    getPreferences(0).edit().putString(WIN99, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(WIN99, "0")) + 1)).toString()).commit();
                    return;
                case 2:
                    getPreferences(0).edit().putString(LOSE99, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(LOSE99, "0")) + 1)).toString()).commit();
                    return;
                default:
                    return;
            }
        } else if (this.bSize == 0 && this.gDiff == 1) {
            switch (result) {
                case 0:
                    getPreferences(0).edit().putString(NDRAW33, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(NDRAW33, "0")) + 1)).toString()).commit();
                    return;
                case 1:
                    getPreferences(0).edit().putString(NWIN33, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(NWIN33, "0")) + 1)).toString()).commit();
                    return;
                case 2:
                    getPreferences(0).edit().putString(NLOSE33, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(NLOSE33, "0")) + 1)).toString()).commit();
                    return;
                default:
                    return;
            }
        } else if (this.bSize == 1 && this.gDiff == 1) {
            switch (result) {
                case 0:
                    getPreferences(0).edit().putString(NDRAW66, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(NDRAW66, "0")) + 1)).toString()).commit();
                    return;
                case 1:
                    getPreferences(0).edit().putString(NWIN66, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(NWIN66, "0")) + 1)).toString()).commit();
                    return;
                case 2:
                    getPreferences(0).edit().putString(NLOSE66, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(NLOSE66, "0")) + 1)).toString()).commit();
                    return;
                default:
                    return;
            }
        } else if (this.bSize == 2 && this.gDiff == 1) {
            switch (result) {
                case 0:
                    getPreferences(0).edit().putString(NDRAW99, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(NDRAW99, "0")) + 1)).toString()).commit();
                    return;
                case 1:
                    getPreferences(0).edit().putString(NWIN99, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(NWIN99, "0")) + 1)).toString()).commit();
                    return;
                case 2:
                    getPreferences(0).edit().putString(NLOSE99, new StringBuilder(String.valueOf(Integer.parseInt(getPreferences(0).getString(NLOSE99, "0")) + 1)).toString()).commit();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void clearRecords() {
        getPreferences(0).edit().putString(WIN33, "0").commit();
        getPreferences(0).edit().putString(LOSE33, "0").commit();
        getPreferences(0).edit().putString(DRAW33, "0").commit();
        this.Easy33View = (TextView) findViewById(R.id.easy33);
        this.Easy33View.setText(String.valueOf(getPreferences(0).getString(WIN33, "0")) + " / " + getPreferences(0).getString(LOSE33, "0") + " / " + getPreferences(0).getString(DRAW33, "0"));
        getPreferences(0).edit().putString(WIN66, "0").commit();
        getPreferences(0).edit().putString(LOSE66, "0").commit();
        getPreferences(0).edit().putString(DRAW66, "0").commit();
        this.Easy66View = (TextView) findViewById(R.id.easy66);
        this.Easy66View.setText(String.valueOf(getPreferences(0).getString(WIN66, "0")) + " / " + getPreferences(0).getString(LOSE66, "0") + " / " + getPreferences(0).getString(DRAW66, "0"));
        getPreferences(0).edit().putString(WIN99, "0").commit();
        getPreferences(0).edit().putString(LOSE99, "0").commit();
        getPreferences(0).edit().putString(DRAW99, "0").commit();
        this.Easy99View = (TextView) findViewById(R.id.easy99);
        this.Easy99View.setText(String.valueOf(getPreferences(0).getString(WIN99, "0")) + " / " + getPreferences(0).getString(LOSE99, "0") + " / " + getPreferences(0).getString(DRAW99, "0"));
        getPreferences(0).edit().putString(NWIN33, "0").commit();
        getPreferences(0).edit().putString(NLOSE33, "0").commit();
        getPreferences(0).edit().putString(NDRAW33, "0").commit();
        this.Nomal33View = (TextView) findViewById(R.id.neasy33);
        this.Nomal33View.setText(String.valueOf(getPreferences(0).getString(NWIN33, "0")) + " / " + getPreferences(0).getString(NLOSE33, "0") + " / " + getPreferences(0).getString(NDRAW33, "0"));
        getPreferences(0).edit().putString(NWIN66, "0").commit();
        getPreferences(0).edit().putString(NLOSE66, "0").commit();
        getPreferences(0).edit().putString(NDRAW66, "0").commit();
        this.Nomal66View = (TextView) findViewById(R.id.neasy66);
        this.Nomal66View.setText(String.valueOf(getPreferences(0).getString(NWIN66, "0")) + " / " + getPreferences(0).getString(NLOSE66, "0") + " / " + getPreferences(0).getString(NDRAW66, "0"));
        getPreferences(0).edit().putString(NWIN99, "0").commit();
        getPreferences(0).edit().putString(NLOSE99, "0").commit();
        getPreferences(0).edit().putString(NDRAW99, "0").commit();
        this.Nomal99View = (TextView) findViewById(R.id.neasy99);
        this.Nomal99View.setText(String.valueOf(getPreferences(0).getString(NWIN99, "0")) + " / " + getPreferences(0).getString(NLOSE99, "0") + " / " + getPreferences(0).getString(NDRAW99, "0"));
    }

    private void openAlertDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Clear         ");
        alertDialogBuilder.setPositiveButton(" Ok ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Game.this.clearRecords();
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.create().show();
    }
}
