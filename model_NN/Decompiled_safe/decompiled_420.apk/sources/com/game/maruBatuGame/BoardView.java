package com.game.maruBatuGame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

public class BoardView extends View {
    private static final String TAG = "BoardView";
    private final Game game;
    private float hHeight;
    private float height;
    private float pWidth;
    private final Rect selRect = new Rect();
    private int selX;
    private int selY;
    private float width;

    public BoardView(Context context) {
        super(context);
        this.game = (Game) context;
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.width = (float) (Math.min(w, h) / this.game.nbTupple);
        this.height = (float) (Math.min(w, h) / this.game.nbRow);
        this.pWidth = (float) (Math.min(w, h) / 4);
        this.hHeight = (float) (h / 15);
        getRect(this.selX, this.selY, this.selRect);
        Log.d(TAG, "onSizeChanged: width " + this.width + ", height " + this.height);
        super.onSizeChanged(w, h, oldw, oldh);
    }

    private void getRect(int x, int y, Rect rect) {
        int tmp1 = (int) (this.hHeight * 2.0f);
        int i = (int) (this.hHeight + (this.height * ((float) this.game.nbRow)));
        rect.set((int) (((float) x) * this.width), ((int) ((((float) y) * this.height) + this.hHeight)) + tmp1, (int) ((((float) x) * this.width) + this.width), ((int) ((((float) y) * this.height) + this.height + this.hHeight)) + tmp1);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Log.d(TAG, "turn=" + this.game.turn + " maxTurn=" + this.game.maxTurn + " priority=" + this.game.priority);
        Paint background = new Paint();
        background.setColor(getResources().getColor(R.color.game_background));
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), background);
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/JohnHancockCP.otf");
        int tmp1 = (int) (this.hHeight * 2.0f);
        int tmp2 = (int) ((this.hHeight * 2.0f) + (this.height * ((float) this.game.nbRow)));
        this.hHeight = this.hHeight + ((float) tmp1);
        Paint gameBoard = new Paint();
        gameBoard.setColor(getResources().getColor(R.color.game_board));
        canvas.drawRect(0.0f, this.hHeight, (float) getWidth(), this.hHeight + (this.height * ((float) this.game.nbRow)), gameBoard);
        Paint light = new Paint();
        light.setColor(getResources().getColor(R.color.board_light));
        new Paint().setColor(getResources().getColor(R.color.board_dark));
        Paint hilite = new Paint();
        hilite.setColor(getResources().getColor(R.color.board_hilite));
        canvas.drawLine(0.0f, this.hHeight - 1.0f, (float) getWidth(), this.hHeight - 1.0f, hilite);
        canvas.drawLine(0.0f, this.hHeight, (float) getWidth(), this.hHeight, light);
        for (int i = 1; i <= this.game.nbRow; i++) {
            canvas.drawLine(0.0f, this.hHeight + (((float) i) * this.height), (float) getWidth(), this.hHeight + (((float) i) * this.height), light);
            canvas.drawLine(0.0f, this.hHeight + (((float) i) * this.height) + 1.0f, (float) getWidth(), this.hHeight + (((float) i) * this.height) + 1.0f, hilite);
        }
        for (int i2 = 1; i2 <= this.game.nbTupple - 1; i2++) {
            canvas.drawLine(((float) i2) * this.width, this.hHeight, ((float) i2) * this.width, this.hHeight + (this.height * ((float) this.game.nbRow)), hilite);
            canvas.drawLine((((float) i2) * this.width) + 1.0f, this.hHeight, (((float) i2) * this.width) + 1.0f, this.hHeight + (this.height * ((float) this.game.nbRow)), light);
        }
        this.hHeight = this.hHeight - ((float) tmp1);
        Paint controlPanel = new Paint();
        controlPanel.setColor(getResources().getColor(R.color.bbb));
        canvas.drawRect(0.0f, ((this.hHeight * 2.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2), (float) getWidth(), (((this.hHeight * 3.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2)) - ((this.hHeight / 3.0f) * 2.0f), controlPanel);
        controlPanel.setColor(getResources().getColor(R.color.ccc));
        canvas.drawRect(0.0f, (((this.hHeight * 2.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2)) + (this.hHeight / 3.0f), (float) getWidth(), (((this.hHeight * 3.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2)) - (this.hHeight / 3.0f), controlPanel);
        controlPanel.setColor(getResources().getColor(R.color.aaa));
        canvas.drawRect(0.0f, (((this.hHeight * 2.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2)) + ((this.hHeight / 3.0f) * 2.0f), (float) getWidth(), ((this.hHeight * 3.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2), controlPanel);
        Paint pLight = new Paint();
        pLight.setColor(getResources().getColor(R.color.board_light));
        new Paint().setColor(getResources().getColor(R.color.board_dark));
        Paint pHilite = new Paint();
        pHilite.setColor(getResources().getColor(R.color.board_hilite));
        canvas.drawLine(0.0f, (((this.hHeight * 2.0f) + (this.height * ((float) this.game.nbRow))) - 1.0f) - ((float) tmp2), (float) getWidth(), (((this.hHeight * 2.0f) + (this.height * ((float) this.game.nbRow))) - 1.0f) - ((float) tmp2), pHilite);
        canvas.drawLine(0.0f, ((this.hHeight * 2.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2), (float) getWidth(), ((this.hHeight * 2.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2), pLight);
        for (int i3 = 3; i3 <= 3; i3++) {
            canvas.drawLine(0.0f, ((this.hHeight * ((float) i3)) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2), (float) getWidth(), ((this.hHeight * ((float) i3)) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2), pLight);
            canvas.drawLine(0.0f, (((this.hHeight * ((float) i3)) + (this.height * ((float) this.game.nbRow))) + 1.0f) - ((float) tmp2), (float) getWidth(), (((this.hHeight * ((float) i3)) + (this.height * ((float) this.game.nbRow))) + 1.0f) - ((float) tmp2), pHilite);
        }
        for (int i4 = 1; i4 <= 3; i4++) {
            canvas.drawLine(((float) i4) * this.pWidth, ((this.hHeight * 2.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2), ((float) i4) * this.pWidth, ((this.hHeight * 3.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2), hilite);
            canvas.drawLine((((float) i4) * this.pWidth) + 1.0f, ((this.hHeight * 2.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2), (((float) i4) * this.pWidth) + 1.0f, ((this.hHeight * 3.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2), light);
        }
        Paint paint = new Paint(1);
        paint.setColor(getResources().getColor(R.color.board_foreground));
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize(this.hHeight * 0.8f);
        paint.setTextScaleX(this.width / this.height);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTypeface(tf);
        Paint.FontMetrics fm2 = paint.getFontMetrics();
        float x = this.pWidth / 2.0f;
        float y = ((((this.hHeight / 2.0f) - ((fm2.ascent + fm2.descent) / 2.0f)) + (this.height * ((float) this.game.nbRow))) + (2.0f * this.hHeight)) - ((float) tmp2);
        canvas.drawText("New", (0.0f * this.pWidth) + x, y, paint);
        canvas.drawText("Undo", (1.0f * this.pWidth) + x, y, paint);
        canvas.drawText("Redo", (2.0f * this.pWidth) + x, y, paint);
        canvas.drawText("Back", (3.0f * this.pWidth) + x, y, paint);
        this.hHeight = this.hHeight + ((float) tmp1);
        Paint paint2 = new Paint(1);
        paint2.setColor(getResources().getColor(R.color.board_foreground));
        paint2.setStyle(Paint.Style.FILL);
        paint2.setTextSize(this.height * 1.2f);
        paint2.setTextScaleX(this.width / this.height);
        paint2.setTextAlign(Paint.Align.CENTER);
        Paint.FontMetrics fm = paint2.getFontMetrics();
        float x2 = this.width / 2.0f;
        float y2 = ((this.height / 2.0f) - ((fm.ascent + fm.descent) / 2.0f)) + this.hHeight;
        for (int i5 = 0; i5 < this.game.nbTupple; i5++) {
            for (int j = 0; j < this.game.nbRow; j++) {
                if (this.game.getTile(i5, j) == 1) {
                    paint2.setColor(getResources().getColor(R.color.blue));
                    canvas.drawText(this.game.getTileString(i5, j), (((float) i5) * this.width) + x2, (((float) j) * this.height) + y2, paint2);
                } else if (this.game.getTile(i5, j) == 2) {
                    paint2.setColor(getResources().getColor(R.color.red));
                    canvas.drawText(this.game.getTileString(i5, j), (((float) i5) * this.width) + x2, (((float) j) * this.height) + y2, paint2);
                } else if (this.game.getTile(i5, j) == 3) {
                    paint2.setColor(getResources().getColor(R.color.green));
                    canvas.drawText(this.game.getTileString(i5, j), (((float) i5) * this.width) + x2, (((float) j) * this.height) + y2, paint2);
                } else if (this.game.getTile(i5, j) == 4) {
                    paint2.setColor(getResources().getColor(R.color.whight));
                    canvas.drawText(this.game.getTileString(i5, j), (((float) i5) * this.width) + x2, (((float) j) * this.height) + y2, paint2);
                }
            }
        }
        Log.d(TAG, "selRect=" + this.selRect);
        Paint selected = new Paint();
        selected.setColor(getResources().getColor(R.color.board_selected));
        canvas.drawRect(this.selRect, selected);
        this.hHeight = this.hHeight - ((float) tmp1);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown: Keycode=" + keyCode + ", event=" + event);
        switch (keyCode) {
            case 19:
                select(this.selX, this.selY - 1);
                break;
            case 20:
                select(this.selX, this.selY + 1);
                break;
            case 21:
                select(this.selX - 1, this.selY);
                break;
            case 22:
                select(this.selX + 1, this.selY);
                break;
            case 23:
            case 62:
            case 66:
                if (this.game.playNow == 1) {
                    this.game.setTileValid(this.selX, this.selY);
                    invalidate(this.selRect);
                    break;
                }
                break;
            default:
                return super.onKeyDown(keyCode, event);
        }
        return true;
    }

    private void select(int x, int y) {
        invalidate(this.selRect);
        if (x < 0) {
            this.selX = this.game.nbTupple - 1;
        } else if (x >= this.game.nbTupple) {
            this.selX = 0;
        } else {
            this.selX = x;
        }
        if (y < 0) {
            this.selY = this.game.nbRow - 1;
        } else if (y >= this.game.nbRow) {
            this.selY = 0;
        } else {
            this.selY = y;
        }
        getRect(this.selX, this.selY, this.selRect);
        invalidate(this.selRect);
    }

    public boolean onTouchEvent(MotionEvent event) {
        int tmp1 = (int) (this.hHeight * 2.0f);
        int tmp2 = (int) ((this.hHeight * 2.0f) + (this.height * ((float) this.game.nbRow)));
        int x = (int) (event.getX() / this.width);
        int y = (int) ((event.getY() - (this.hHeight + ((float) tmp1))) / this.height);
        if (0.0f > event.getY() - (this.hHeight + ((float) tmp1))) {
            y = -1;
        }
        if (event.getAction() != 0) {
            return super.onTouchEvent(event);
        }
        if (x >= 0 && x < this.game.nbTupple && y >= 0 && y < this.game.nbRow) {
            Log.d(TAG, "onTouchEvent:bX " + this.selX + ", bY " + this.selY);
            select(x, y);
            if (this.game.playNow == 1 && this.game.getTile(x, y) == 0) {
                this.game.setTileValid(this.selX, this.selY);
                invalidate();
            }
        }
        int x2 = (int) (event.getX() / this.pWidth);
        int y2 = (int) event.getY();
        if (((this.hHeight * 2.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2) <= ((float) y2) && ((float) y2) < ((this.hHeight * 3.0f) + (this.height * ((float) this.game.nbRow))) - ((float) tmp2)) {
            Log.d(TAG, "onTouchEvent:bX " + this.selX + ", bY " + this.selY);
            Log.d(TAG, "onTouchEvent:pX " + x2);
            switch (x2) {
                case 0:
                    this.game.resetGame();
                    invalidate();
                    break;
                case 1:
                    if (this.game.playNow == 1) {
                        this.game.doUndo();
                        invalidate();
                        break;
                    }
                    break;
                case 2:
                    if (this.game.playNow == 1) {
                        this.game.doRedo();
                        invalidate();
                        break;
                    }
                    break;
                case 3:
                    this.game.toTitle();
                    break;
            }
        }
        return true;
    }
}
