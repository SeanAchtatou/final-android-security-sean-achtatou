package com.game.maruBatuGame;

public final class R {

    public static final class array {
        public static final int difficulty2 = 2130968578;
        public static final int difficulty3 = 2130968579;
        public static final int difficulty5 = 2130968580;
        public static final int size2 = 2130968576;
        public static final int size3 = 2130968577;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int aaa = 2131034122;
        public static final int aliceblue = 2131034156;
        public static final int antiquewhite = 2131034141;
        public static final int aqua = 2131034178;
        public static final int aquamarine = 2131034194;
        public static final int azure = 2131034155;
        public static final int bbb = 2131034124;
        public static final int beige = 2131034211;
        public static final int bisque = 2131034144;
        public static final int black = 2131034128;
        public static final int blanchedalmond = 2131034143;
        public static final int blue = 2131034167;
        public static final int blueviolet = 2131034264;
        public static final int board_dark = 2131034117;
        public static final int board_foreground = 2131034119;
        public static final int board_hilite = 2131034118;
        public static final int board_light = 2131034116;
        public static final int board_selected = 2131034120;
        public static final int brown = 2131034232;
        public static final int burlywood = 2131034216;
        public static final int cadetblue = 2131034183;
        public static final int ccc = 2131034123;
        public static final int chartreuse = 2131034200;
        public static final int chocolate = 2131034227;
        public static final int coral = 2131034240;
        public static final int cornflowerblue = 2131034169;
        public static final int cornsilk = 2131034210;
        public static final int crimson = 2131034244;
        public static final int cyan = 2131034177;
        public static final int darkblue = 2131034165;
        public static final int darkcyan = 2131034184;
        public static final int darkgoldenrod = 2131034226;
        public static final int darkgray = 2131034131;
        public static final int darkgreen = 2131034187;
        public static final int darkkhaki = 2131034208;
        public static final int darkmagenta = 2131034260;
        public static final int darkolivegreen = 2131034205;
        public static final int darkorange = 2131034223;
        public static final int darkorchid = 2131034258;
        public static final int darkred = 2131034231;
        public static final int darksalmon = 2131034236;
        public static final int darkseagreen = 2131034193;
        public static final int darkslateblue = 2131034263;
        public static final int darkslategray = 2131034186;
        public static final int darkturquoise = 2131034181;
        public static final int darkviolet = 2131034259;
        public static final int deeppink = 2131034246;
        public static final int deepskyblue = 2131034170;
        public static final int dimgray = 2131034129;
        public static final int dodgerblue = 2131034168;
        public static final int firebrick = 2131034233;
        public static final int floralwhite = 2131034139;
        public static final int forestgreen = 2131034189;
        public static final int fuchsia = 2131034253;
        public static final int gainsboro = 2131034134;
        public static final int game_background = 2131034113;
        public static final int game_board = 2131034114;
        public static final int game_board2 = 2131034115;
        public static final int ghostwhite = 2131034138;
        public static final int gold = 2131034220;
        public static final int goldenrod = 2131034224;
        public static final int gray = 2131034130;
        public static final int green = 2131034188;
        public static final int greenyellow = 2131034201;
        public static final int honeydew = 2131034153;
        public static final int hotpink = 2131034247;
        public static final int indianred = 2131034234;
        public static final int indigo = 2131034262;
        public static final int ivory = 2131034152;
        public static final int khaki = 2131034218;
        public static final int lavender = 2131034157;
        public static final int lavenderblush = 2131034149;
        public static final int lawngreen = 2131034199;
        public static final int lemonchiffon = 2131034214;
        public static final int lightblue = 2131034173;
        public static final int lightcoral = 2131034237;
        public static final int lightcyan = 2131034176;
        public static final int lightgoldenrodyellow = 2131034213;
        public static final int lightgreen = 2131034196;
        public static final int lightgrey = 2131034133;
        public static final int lightpink = 2131034250;
        public static final int lightsalmon = 2131034239;
        public static final int lightseagreen = 2131034182;
        public static final int lightskyblue = 2131034171;
        public static final int lightslategray = 2131034159;
        public static final int lightsteelblue = 2131034158;
        public static final int lightyellow = 2131034212;
        public static final int lime = 2131034202;
        public static final int limegreen = 2131034203;
        public static final int linen = 2131034140;
        public static final int magenta = 2131034252;
        public static final int main_background = 2131034112;
        public static final int maroon = 2131034230;
        public static final int mediumaquamarine = 2131034192;
        public static final int mediumblue = 2131034166;
        public static final int mediumorchid = 2131034257;
        public static final int mediumpurple = 2131034265;
        public static final int mediumseagreen = 2131034191;
        public static final int mediumslateblue = 2131034267;
        public static final int mediumspringgreen = 2131034198;
        public static final int mediumturquoise = 2131034180;
        public static final int mediumvioletred = 2131034245;
        public static final int midnightblue = 2131034163;
        public static final int mintcream = 2131034154;
        public static final int mistyrose = 2131034148;
        public static final int moccasin = 2131034145;
        public static final int navajowhite = 2131034146;
        public static final int navy = 2131034164;
        public static final int oldlace = 2131034151;
        public static final int olive = 2131034207;
        public static final int olivedrab = 2131034206;
        public static final int orange = 2131034221;
        public static final int orangered = 2131034242;
        public static final int orchid = 2131034256;
        public static final int palegoldenrod = 2131034209;
        public static final int palegreen = 2131034195;
        public static final int paleturquoise = 2131034175;
        public static final int palevioletred = 2131034248;
        public static final int papayawhip = 2131034142;
        public static final int peachpuff = 2131034147;
        public static final int peru = 2131034225;
        public static final int pink = 2131034249;
        public static final int plum = 2131034255;
        public static final int powderblue = 2131034174;
        public static final int purple = 2131034261;
        public static final int puzzle_hint_0 = 2131034125;
        public static final int puzzle_hint_1 = 2131034126;
        public static final int puzzle_hint_2 = 2131034127;
        public static final int red = 2131034243;
        public static final int rosybrown = 2131034235;
        public static final int royalblue = 2131034162;
        public static final int saddlebrown = 2131034229;
        public static final int salmon = 2131034238;
        public static final int sandybrown = 2131034222;
        public static final int seagreen = 2131034190;
        public static final int seashell = 2131034150;
        public static final int sienna = 2131034228;
        public static final int silver = 2131034132;
        public static final int skyblue = 2131034172;
        public static final int slateblue = 2131034266;
        public static final int slategray = 2131034160;
        public static final int snow = 2131034137;
        public static final int springgreen = 2131034197;
        public static final int steelblue = 2131034161;
        public static final int tan = 2131034217;
        public static final int teal = 2131034185;
        public static final int thistle = 2131034251;
        public static final int tomato = 2131034241;
        public static final int turquoise = 2131034179;
        public static final int violet = 2131034254;
        public static final int wheat = 2131034215;
        public static final int whight = 2131034121;
        public static final int white = 2131034136;
        public static final int whitesmoke = 2131034135;
        public static final int yellow = 2131034219;
        public static final int yellowgreen = 2131034204;
    }

    public static final class drawable {
        public static final int bgtitle = 2130837504;
        public static final int icon = 2130837505;
    }

    public static final class id {
        public static final int adView = 2131165192;
        public static final int adview = 2131165184;
        public static final int aiEasy = 2131165207;
        public static final int aiNormal = 2131165208;
        public static final int boardView = 2131165210;
        public static final int clear_button = 2131165201;
        public static final int easy33 = 2131165193;
        public static final int easy66 = 2131165195;
        public static final int easy99 = 2131165197;
        public static final int linearLayout1 = 2131165189;
        public static final int linearLayout2 = 2131165191;
        public static final int lnearLayout_adview = 2131165211;
        public static final int lnearLayout_boardView = 2131165209;
        public static final int more_game_button = 2131165200;
        public static final int more_games_button = 2131165188;
        public static final int neasy33 = 2131165194;
        public static final int neasy66 = 2131165196;
        public static final int neasy99 = 2131165198;
        public static final int one_player_button = 2131165185;
        public static final int radiobutton33 = 2131165203;
        public static final int radiobutton66 = 2131165204;
        public static final int radiobutton99 = 2131165205;
        public static final int radiogroup = 2131165202;
        public static final int radiogroup2 = 2131165206;
        public static final int records_button = 2131165187;
        public static final int return_title_button = 2131165199;
        public static final int setting_button = 2131165186;
        public static final int webView_more = 2131165190;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int more = 2130903041;
        public static final int records = 2130903042;
        public static final int setting = 2130903043;
        public static final int stage = 2130903044;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int hello = 2131099648;
    }
}
