package com.appbyme.personal.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.appbyme.activity.constant.FinalConstant;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.fragment.WaterfallFragment;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.appbyme.personal.activity.fragment.AutogenUserReplyTopicFragment;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.forum.android.model.BoardModel;

public class AutogenPersonalCommentActivity extends BasePersonalActivity implements MCConstant {
    private int count = 0;
    private String ecComment;
    private Class<?> ecTab = null;
    private String forumComment;
    private Class<?> forumTab = null;
    private long userId = 0;
    private String userNickname;

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.userId = getIntent().getLongExtra("userId", 0);
        this.userNickname = getIntent().getStringExtra("nickname");
        this.forumTab = AutogenUserReplyTopicFragment.class;
        this.ecTab = WaterfallFragment.class;
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        this.ecComment = getResources().getString(this.mcResource.getStringId("mc_ec_comment_goods"));
        this.forumComment = getResources().getString(this.mcResource.getStringId("mc_forum_comment_forum"));
        if (this.isEc) {
            addTab(this.ecComment, this.ecTab);
            this.count++;
        }
        if (this.isForum) {
            addTab(this.forumComment, this.forumTab);
            this.count++;
        }
        if (this.count <= 1) {
            this.tabWidget.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void setTitleText() {
        this.titleText.setText(getResources().getString(this.mcResource.getStringId("mc_forum_user_reply_posts")));
    }

    /* access modifiers changed from: protected */
    public void sendParams(Fragment fragment) {
        if (fragment instanceof WaterfallFragment) {
            AutogenModuleModel moduleModel = new AutogenModuleModel();
            moduleModel.setModuleId(0);
            moduleModel.setDetailDisplay(1);
            moduleModel.setModuleType(9);
            BoardModel boardModel = new BoardModel();
            boardModel.setBoardId(-12);
            Bundle bundle = new Bundle();
            bundle.putSerializable(IntentConstant.SAVE_INSTANCE_BOARD_MODEL, boardModel);
            bundle.putSerializable(IntentConstant.SAVE_INSTANCE_CONFIG_MODEL, moduleModel);
            bundle.putInt(IntentConstant.INTENT_MODULE_MARKING, FinalConstant.GOODSCOMMENT_MARKING);
            bundle.putLong("boardId", -12);
            ((WaterfallFragment) fragment).setArguments(bundle);
        } else if (fragment instanceof AutogenUserReplyTopicFragment) {
            AutogenUserReplyTopicFragment autogenUserReplyTopicFragment = (AutogenUserReplyTopicFragment) fragment;
            autogenUserReplyTopicFragment.setUserId(this.userId);
            autogenUserReplyTopicFragment.setNickName(this.userNickname);
        }
    }
}
