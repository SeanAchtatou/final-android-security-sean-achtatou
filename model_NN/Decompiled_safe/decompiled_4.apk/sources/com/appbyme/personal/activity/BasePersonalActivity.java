package com.appbyme.personal.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import com.appbyme.activity.BaseFragmentActivity;
import com.appbyme.activity.constant.ConfigConstant;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.appbyme.android.service.ConfigService;
import com.appbyme.android.service.impl.ConfigServiceImpl;
import com.appbyme.widget.AutogenViewPager;
import com.mobcent.base.android.ui.activity.adapter.TabsAdapter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BasePersonalActivity extends BaseFragmentActivity {
    protected ImageButton backBtn;
    protected ConfigService configService;
    protected List<String> galleryModuleIdList = null;
    protected LayoutInflater inflater;
    boolean isEc = false;
    boolean isForum = false;
    boolean isSquare = false;
    protected TabHost mTabHost;
    protected PersonalFragmentAdapter personalAdapter;
    protected TabWidget tabWidget;
    protected TextView titleText;
    protected AutogenViewPager viewPager;

    /* access modifiers changed from: protected */
    public abstract void sendParams(Fragment fragment);

    /* access modifiers changed from: protected */
    public abstract void setTitleText();

    /* access modifiers changed from: protected */
    public void initData() {
        this.inflater = LayoutInflater.from(this);
        this.configService = new ConfigServiceImpl(this);
        getModuleIds();
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.mcResource.getLayoutId("personal_base_activity"));
        this.backBtn = (ImageButton) findViewById(this.mcResource.getViewId("back_btn"));
        this.titleText = (TextView) findViewById(this.mcResource.getViewId("title_text"));
        this.mTabHost = (TabHost) findViewById(16908306);
        this.tabWidget = (TabWidget) findViewById(16908307);
        this.mTabHost.setup();
        this.viewPager = (AutogenViewPager) findViewById(this.mcResource.getViewId("mc_ec_personal_publish_fragment_pager"));
        this.personalAdapter = new PersonalFragmentAdapter(this, this.mTabHost, this.viewPager);
        setTitleText();
    }

    private void getModuleIds() {
        for (Map.Entry<Integer, List<AutogenModuleModel>> value : this.application.getModuleList()) {
            List<AutogenModuleModel> mModuleList = value.getValue();
            int size = mModuleList.size();
            for (int j = 0; j < size; j++) {
                switch (((AutogenModuleModel) mModuleList.get(j)).getModuleType()) {
                    case 1:
                        this.isForum = true;
                        break;
                    case ConfigConstant.HOT_ID:
                        this.isEc = true;
                        break;
                    case 15:
                        this.isEc = true;
                        break;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePersonalActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public View createTabView(String string) {
        View v = this.inflater.inflate(this.mcResource.getLayoutId("personal_tabs_item"), (ViewGroup) null);
        ((TextView) v.findViewById(this.mcResource.getViewId("personal_tab_text"))).setText(string);
        showTab(this.tabWidget);
        return v;
    }

    public class PersonalFragmentAdapter extends TabsAdapter {
        private Map<Integer, Fragment> map = new HashMap();

        public PersonalFragmentAdapter(FragmentActivity activity, TabHost tabHost, ViewPager pager) {
            super(activity, tabHost, pager);
        }

        public Fragment getItem(int position) {
            Fragment fragment;
            if (this.map.get(Integer.valueOf(position)) == null) {
                fragment = super.getItem(position);
                this.map.put(Integer.valueOf(position), fragment);
            } else {
                fragment = this.map.get(Integer.valueOf(position));
            }
            BasePersonalActivity.this.sendParams(fragment);
            return fragment;
        }

        public void onTabChanged(String tabId) {
            BasePersonalActivity.this.viewPager.setCurrentItem(BasePersonalActivity.this.mTabHost.getCurrentTab());
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        public void onPageSelected(int position) {
            TabWidget widget = BasePersonalActivity.this.mTabHost.getTabWidget();
            int oldFocusability = widget.getDescendantFocusability();
            widget.setDescendantFocusability(393216);
            BasePersonalActivity.this.mTabHost.setCurrentTab(position);
            widget.setDescendantFocusability(oldFocusability);
        }

        public void onPageScrollStateChanged(int state) {
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            if (this.map.get(Integer.valueOf(position)) != null) {
                if (this.map.get(Integer.valueOf(position)) != null) {
                }
                this.map.remove(Integer.valueOf(position));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void showTab(TabWidget tabWidget2) {
    }

    /* access modifiers changed from: protected */
    public void addTab(String Tag, Class<?> object) {
        this.personalAdapter.addTab(this.mTabHost.newTabSpec(Tag).setIndicator(createTabView(Tag)), object, null);
    }
}
