package com.appbyme.personal.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.appbyme.activity.constant.FinalConstant;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.fragment.WaterfallFragment;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.appbyme.personal.activity.fragment.AutogenUserFavoriteTopicFragment;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.forum.android.model.BoardModel;

public class AutogenPersonalFavorActivity extends BasePersonalActivity implements MCConstant {
    private int count = 0;
    private String ecFavor;
    private Class<?> ecTab = null;
    private String forumFavor;
    private Class<?> forumTab = null;
    private long userId = 0;
    private String userNickname;

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.userId = getIntent().getLongExtra("userId", 0);
        this.userNickname = getIntent().getStringExtra("nickname");
        this.forumTab = AutogenUserFavoriteTopicFragment.class;
        this.ecTab = WaterfallFragment.class;
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        this.forumFavor = getResources().getString(this.mcResource.getStringId("mc_forum_comment_forum"));
        this.ecFavor = getResources().getString(this.mcResource.getStringId("mc_ec_comment_goods"));
        if (this.isEc) {
            addTab(this.ecFavor, this.ecTab);
            this.count++;
        }
        if (this.isForum) {
            addTab(this.forumFavor, this.forumTab);
            this.count++;
        }
        if (this.count <= 1) {
            this.tabWidget.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void setTitleText() {
        this.titleText.setText(getResources().getString(this.mcResource.getStringId("mc_forum_personal_favor")));
    }

    /* access modifiers changed from: protected */
    public void sendParams(Fragment fragment) {
        if (fragment instanceof WaterfallFragment) {
            AutogenModuleModel moduleModel = new AutogenModuleModel();
            moduleModel.setModuleId(0);
            moduleModel.setDetailDisplay(1);
            moduleModel.setModuleType(9);
            BoardModel boardModel = new BoardModel();
            boardModel.setBoardId(-12);
            Bundle bundle = new Bundle();
            bundle.putSerializable(IntentConstant.SAVE_INSTANCE_BOARD_MODEL, boardModel);
            bundle.putSerializable(IntentConstant.SAVE_INSTANCE_CONFIG_MODEL, moduleModel);
            bundle.putInt(IntentConstant.INTENT_MODULE_MARKING, FinalConstant.GOODSFAVOR_MARKING);
            bundle.putLong("boardId", -12);
            ((WaterfallFragment) fragment).setArguments(bundle);
        }
    }
}
