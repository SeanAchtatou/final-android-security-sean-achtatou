package com.appbyme.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class AutogenViewPager extends ViewPager {
    private boolean isScroll = true;

    public boolean isScroll() {
        return this.isScroll;
    }

    public void setScroll(boolean isScroll2) {
        this.isScroll = isScroll2;
    }

    public AutogenViewPager(Context context) {
        super(context);
    }

    public AutogenViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        try {
            if (isScroll()) {
                return super.onInterceptTouchEvent(event);
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
