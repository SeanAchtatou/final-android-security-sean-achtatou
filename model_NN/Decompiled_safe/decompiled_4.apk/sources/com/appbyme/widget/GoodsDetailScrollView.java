package com.appbyme.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

public class GoodsDetailScrollView extends ScrollView {
    private boolean LVIsTop = false;
    private View contentView;
    private boolean isBottom = false;
    int num = 0;
    private OnBorderListener onBorderListener;

    public interface OnBorderListener {
        void onBottom();

        void onTop();
    }

    public boolean isLVIsTop() {
        return this.LVIsTop;
    }

    public void setLVIsTop(boolean lVIsTop) {
        this.LVIsTop = lVIsTop;
    }

    public boolean isBottom() {
        return this.isBottom;
    }

    public void setBottom(boolean isBottom2) {
        this.isBottom = isBottom2;
    }

    public GoodsDetailScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public GoodsDetailScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GoodsDetailScrollView(Context context) {
        super(context);
    }

    public void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        doOnBorderListener();
    }

    private void doOnBorderListener() {
        int scrollY = getScrollY();
        if (this.contentView.getMeasuredHeight() - getHeight() == scrollY) {
            this.num++;
        }
        if (this.num > 1) {
            if (this.contentView == null || this.contentView.getMeasuredHeight() != getHeight() + scrollY) {
                this.onBorderListener.onTop();
            } else if (this.onBorderListener != null) {
                this.onBorderListener.onBottom();
            }
        }
    }

    public void setListener(OnBorderListener onBorderListener2, View contentView2) {
        this.onBorderListener = onBorderListener2;
        this.contentView = contentView2;
    }
}
