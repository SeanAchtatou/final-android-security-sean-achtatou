package com.appbyme.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.HorizontalScrollView;

public class SubNavScrollView extends HorizontalScrollView {
    private ScrollViewListener scrollViewListener;

    public interface ScrollViewListener {
        void onScrollChanged(SubNavScrollView subNavScrollView, int i, int i2, int i3, int i4);
    }

    public ScrollViewListener getScrollViewListener() {
        return this.scrollViewListener;
    }

    public void setScrollViewListener(ScrollViewListener scrollViewListener2) {
        this.scrollViewListener = scrollViewListener2;
    }

    public SubNavScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public SubNavScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SubNavScrollView(Context context) {
        super(context);
    }

    public void fling(int velocityY) {
        super.fling(velocityY);
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return super.onInterceptTouchEvent(ev);
    }

    public void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (this.scrollViewListener != null) {
            this.scrollViewListener.onScrollChanged(this, l, t, oldl, oldt);
        }
    }
}
