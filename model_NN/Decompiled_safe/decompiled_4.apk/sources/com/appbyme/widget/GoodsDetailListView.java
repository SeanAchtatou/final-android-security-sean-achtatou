package com.appbyme.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ListView;

public class GoodsDetailListView extends ListView {
    private boolean LVIsTop = false;
    private boolean SVIsBottom = false;
    private String TAG = "GoodsDetailListView";
    private GoodsDetailScrollView mScrollView;
    float y = 0.0f;

    public boolean isSVIsBottom() {
        return this.SVIsBottom;
    }

    public void setSVIsBottom(boolean sVIsBottom) {
        this.SVIsBottom = sVIsBottom;
    }

    public boolean isLVIsTop() {
        return this.LVIsTop;
    }

    public void setLVIsTop(boolean lVIsTop) {
        this.LVIsTop = lVIsTop;
    }

    public GoodsDetailScrollView getmScrollView() {
        return this.mScrollView;
    }

    public void setmScrollView(GoodsDetailScrollView mScrollView2) {
        this.mScrollView = mScrollView2;
    }

    public GoodsDetailListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public GoodsDetailListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GoodsDetailListView(Context context) {
        super(context);
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case 0:
                this.y = ev.getRawY();
                this.mScrollView.requestDisallowInterceptTouchEvent(true);
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case 0:
                this.y = ev.getRawY();
                this.mScrollView.requestDisallowInterceptTouchEvent(true);
                if (!this.SVIsBottom) {
                    this.mScrollView.requestDisallowInterceptTouchEvent(false);
                    break;
                }
                break;
            case 2:
                float desY = ev.getRawY() - this.y;
                if (this.SVIsBottom) {
                    if (desY > 0.0f && Math.abs(desY) > 2.0f && this.LVIsTop) {
                        this.mScrollView.requestDisallowInterceptTouchEvent(false);
                        break;
                    }
                } else {
                    this.mScrollView.requestDisallowInterceptTouchEvent(false);
                    break;
                }
        }
        return super.onTouchEvent(ev);
    }
}
