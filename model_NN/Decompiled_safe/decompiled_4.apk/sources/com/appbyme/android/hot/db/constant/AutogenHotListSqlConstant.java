package com.appbyme.android.hot.db.constant;

public interface AutogenHotListSqlConstant extends AutogenHotListTableConstant {
    public static final String HOT_LIST_SUM = "SELECT COUNT ( * )  FROM t_hot_list WHERE page = ? AND  board_id = ?";
    public static final String HOT_LIST_WHERE = "page = ? AND  board_id = ?";
    public static final String HOT_TOTAL_COUNT_WHERE = "board_id = ?";
    public static final String NEW_TABLE_HOT_LIST = "create table if not exists t_hot_list ( id integer primary key autoincrement, page INTEGER,board_id INTEGER,page_toal_num INTEGER,json_str TEXT);";
}
