package com.appbyme.android.hot.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import com.appbyme.android.hot.db.constant.AutogenHotListSqlConstant;
import com.appbyme.android.hot.db.constant.AutogenHotListTableConstant;

public class AutogenHotListDBUtil extends AutogenBaseHotListDBUtil implements AutogenHotListTableConstant {
    private static AutogenHotListDBUtil autogenHotListDBUtil;

    public AutogenHotListDBUtil(Context context) {
        super(context);
    }

    public static AutogenHotListDBUtil getInstance(Context context) {
        if (autogenHotListDBUtil == null) {
            autogenHotListDBUtil = new AutogenHotListDBUtil(context);
        }
        return autogenHotListDBUtil;
    }

    public synchronized boolean saveHotList(int page, int totalNum, long boardId, String jsonStr) {
        boolean z;
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("page", Integer.valueOf(page));
            values.put("page_toal_num", Integer.valueOf(totalNum));
            values.put("board_id", Long.valueOf(boardId));
            values.put("json_str", jsonStr);
            this.writableDatabase.insert("t_hot_list", null, values);
            closeWriteableDB();
            z = true;
        } catch (SQLException e) {
            e.printStackTrace();
            closeWriteableDB();
            z = false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
        return z;
    }

    public synchronized boolean cleanHotList() {
        return removeAllEntries("t_hot_list");
    }

    public synchronized String getHotList(int page, long hotBoardId) {
        String str;
        Cursor c = null;
        try {
            openReadableDB();
            Cursor c2 = this.readableDatabase.query("t_hot_list", null, AutogenHotListSqlConstant.HOT_LIST_WHERE, new String[]{new StringBuilder(String.valueOf(page)).toString(), new StringBuilder(String.valueOf(hotBoardId)).toString()}, null, null, null);
            if (c2.moveToFirst()) {
                String s = c2.getString(c2.getColumnIndex("json_str"));
                if (c2 != null) {
                    c2.close();
                }
                closeReadableDB();
                str = s;
            } else {
                if (c2 != null) {
                    c2.close();
                }
                closeReadableDB();
                str = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            closeReadableDB();
            throw th;
        }
        return str;
    }

    public synchronized boolean deleteHotBoard(long boardId) {
        return removeEntries(this.writableDatabase, "t_hot_list", boardId);
    }
}
