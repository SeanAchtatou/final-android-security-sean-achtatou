package com.appbyme.android.hot.db.constant;

public interface AutogenHotListTableConstant {
    public static final String HOT_LIST_BOARD_ID = "board_id";
    public static final String HOT_LIST_ID = "id";
    public static final String HOT_LIST_JSON_STR = "json_str";
    public static final String HOT_LIST_PAGE = "page";
    public static final String HOT_LIST_PAGE_TOTAL_NUM = "page_toal_num";
    public static final int SATABASES_VERSION = 1;
    public static final String T_HOT_LIST = "t_hot_list";
}
