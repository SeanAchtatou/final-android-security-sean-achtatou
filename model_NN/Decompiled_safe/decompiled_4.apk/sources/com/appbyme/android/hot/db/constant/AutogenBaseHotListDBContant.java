package com.appbyme.android.hot.db.constant;

public interface AutogenBaseHotListDBContant {
    public static final int HOT_DB_VERSION = 1;
    public static final String HOT_LIST_DATABASE_NAME = "mc_autogen_hot_list.db";
}
