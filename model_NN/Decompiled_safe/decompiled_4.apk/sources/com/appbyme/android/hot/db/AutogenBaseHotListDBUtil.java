package com.appbyme.android.hot.db;

import android.content.Context;
import com.appbyme.android.base.db.AutogenBaseDBUtil;
import com.appbyme.android.hot.db.constant.AutogenBaseHotListDBContant;

public class AutogenBaseHotListDBUtil extends AutogenBaseDBUtil implements AutogenBaseHotListDBContant {
    protected Context context;

    public AutogenBaseHotListDBUtil(Context context2) {
        initDataInConStructors(context2);
    }

    /* access modifiers changed from: protected */
    public void initDataInConStructors(Context context2) {
        this.autogenDBOpenHelper = new AutogenHotListDBOpenHelper(context2, AutogenBaseHotListDBContant.HOT_LIST_DATABASE_NAME, 1);
    }
}
