package com.appbyme.android.newest.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import com.appbyme.android.newest.db.constant.AutogenNewestListTableConstant;

public class AutogenNewestListDBUtil extends AutogenBaseNewestListDBUtil implements AutogenNewestListTableConstant {
    private static AutogenNewestListDBUtil autogenNewestListDBUtil;

    public AutogenNewestListDBUtil(Context context) {
        super(context);
    }

    public static AutogenNewestListDBUtil getInstance(Context context) {
        if (autogenNewestListDBUtil == null) {
            autogenNewestListDBUtil = new AutogenNewestListDBUtil(context);
        }
        return autogenNewestListDBUtil;
    }

    public synchronized boolean saveNewestList(int page, int totalNum, long boardId, String jsonStr) {
        boolean z;
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("page", Integer.valueOf(page));
            values.put("board_id", Long.valueOf(boardId));
            values.put("page_toal_num", Integer.valueOf(totalNum));
            values.put("json_str", jsonStr);
            this.writableDatabase.insert("t_hot_list", null, values);
            closeWriteableDB();
            z = true;
        } catch (SQLException e) {
            e.printStackTrace();
            closeWriteableDB();
            z = false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
        return z;
    }

    public synchronized boolean deleteNewestboard(long boardId) {
        openWriteableDB();
        return removeEntries(this.writableDatabase, "t_hot_list", boardId);
    }

    public synchronized String getNewestList(int page, long boardId) {
        String str;
        Cursor c = null;
        try {
            openReadableDB();
            Cursor c2 = this.readableDatabase.query("t_hot_list", null, "page = ? AND board_id = ?", new String[]{new StringBuilder(String.valueOf(page)).toString(), new StringBuilder(String.valueOf(boardId)).toString()}, null, null, null);
            if (c2.moveToFirst()) {
                String s = c2.getString(c2.getColumnIndex("json_str"));
                if (c2 != null) {
                    c2.close();
                }
                closeReadableDB();
                str = s;
            } else {
                if (c2 != null) {
                    c2.close();
                }
                closeReadableDB();
                str = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            closeReadableDB();
            throw th;
        }
        return str;
    }
}
