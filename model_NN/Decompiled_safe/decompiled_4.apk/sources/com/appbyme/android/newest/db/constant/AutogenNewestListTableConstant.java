package com.appbyme.android.newest.db.constant;

public interface AutogenNewestListTableConstant {
    public static final String NEWEST_LIST_BOARD_ID = "board_id";
    public static final String NEWEST_LIST_ID = "id";
    public static final String NEWEST_LIST_JSON_STR = "json_str";
    public static final String NEWEST_LIST_PAGE = "page";
    public static final String NEWEST_LIST_PAGE_TOTAL_NUM = "page_toal_num";
    public static final int SATABASES_VERSION = 1;
    public static final String T_NEWEST_LIST = "t_hot_list";
}
