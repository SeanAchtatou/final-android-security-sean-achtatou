package com.appbyme.android.newest.db.constant;

public interface AutogenBaseNewestListDBContant {
    public static final int NEWEST_DB_VERSION = 1;
    public static final String NEWEST_LIST_DATABASE_NAME = "mc_autogen_newest_list.db";
}
