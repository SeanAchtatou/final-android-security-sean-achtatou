package com.appbyme.android.newest.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.appbyme.android.hot.db.constant.AutogenHotListSqlConstant;

public class AutogenNewestListDBOpenHelper extends SQLiteOpenHelper {
    public AutogenNewestListDBOpenHelper(Context context, String databaseName, int version) {
        super(context, databaseName, (SQLiteDatabase.CursorFactory) null, version);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(AutogenHotListSqlConstant.NEW_TABLE_HOT_LIST);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.delete("t_hot_list", null, null);
        onCreate(db);
    }
}
