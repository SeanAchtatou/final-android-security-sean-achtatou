package com.appbyme.android.newest.db.constant;

public interface AutogenNewestListSqlConstant extends AutogenNewestListTableConstant {
    public static final String NEWEST_LIST_SUM = "SELECT SUM ( page_toal_num)  FROM t_hot_list WHERE ";
    public static final String NEWEST_LIST_WHERE = "page = ? AND board_id = ?";
    public static final String NEWEST_TOTAL_COUNT_WHERE = "board_id = ? ";
    public static final String NEW_TABLE_NEWEST_LIST = "create table if not exists t_hot_list ( id integer primary key autoincrement,board_id INTEGER, page INTEGER,page_toal_num INTEGER,json_str TEXT);";
}
