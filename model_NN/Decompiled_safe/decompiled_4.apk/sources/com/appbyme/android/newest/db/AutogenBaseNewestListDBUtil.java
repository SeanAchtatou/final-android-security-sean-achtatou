package com.appbyme.android.newest.db;

import android.content.Context;
import com.appbyme.android.base.db.AutogenBaseDBUtil;
import com.appbyme.android.newest.db.constant.AutogenBaseNewestListDBContant;

public class AutogenBaseNewestListDBUtil extends AutogenBaseDBUtil implements AutogenBaseNewestListDBContant {
    protected Context context;

    public AutogenBaseNewestListDBUtil(Context context2) {
        initDataInConStructors(context2);
    }

    /* access modifiers changed from: protected */
    public void initDataInConStructors(Context context2) {
        this.autogenDBOpenHelper = new AutogenNewestListDBOpenHelper(context2, AutogenBaseNewestListDBContant.NEWEST_LIST_DATABASE_NAME, 1);
    }
}
