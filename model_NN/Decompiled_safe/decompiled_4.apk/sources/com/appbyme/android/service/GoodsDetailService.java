package com.appbyme.android.service;

import com.appbyme.android.base.model.GoodsCommentsModel;
import com.appbyme.android.base.model.GoodsDetailModel;

public interface GoodsDetailService {
    int collectionGoods(long j);

    GoodsCommentsModel getGoodsCommentsByNet(long j);

    GoodsDetailModel getGoodsDetailListByNet(long j);

    void initData(int i);
}
