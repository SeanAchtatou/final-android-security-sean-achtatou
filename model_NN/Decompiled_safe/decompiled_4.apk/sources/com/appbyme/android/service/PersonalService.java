package com.appbyme.android.service;

import com.appbyme.android.base.model.GoodsModel;
import java.util.List;

public interface PersonalService {
    List<GoodsModel> getCommentList();

    List<GoodsModel> getCommentListByNet(int i, int i2);

    List<GoodsModel> getFavorList();

    List<GoodsModel> getFavorListByNet(int i, int i2);

    String getRefreshData();

    void initCacheDB(int i, boolean z);
}
