package com.appbyme.android.service.impl;

import android.content.Context;
import com.appbyme.android.api.NewestRestfulApiRequester;
import com.appbyme.android.base.db.ContentCacheDB;
import com.appbyme.android.base.db.RefreshCacheDB;
import com.appbyme.android.base.db.constant.BaseCacheDBCache;
import com.appbyme.android.base.model.GoodsModel;
import com.appbyme.android.newest.db.AutogenNewestListDBUtil;
import com.appbyme.android.service.NewestService;
import com.appbyme.android.service.impl.helper.NewestServiceImplHelper;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.util.DateUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class NewestServiceImpl implements NewestService {
    private String TAG = "NewestServiceImpl";
    protected long boardId;
    protected ContentCacheDB contentCacheDB;
    private Context context;
    protected boolean isLocal;
    protected boolean isRefresh;
    protected int lastPage;
    protected int page;
    protected int pageSize;
    protected RefreshCacheDB refreshCacheDB;

    public NewestServiceImpl(Context context2) {
        this.context = context2;
        this.contentCacheDB = ContentCacheDB.getInstance(context2);
        this.refreshCacheDB = RefreshCacheDB.getInstance(context2);
    }

    public void initCacheDB(int pageSize2, long boardId2, boolean isRefresh2) {
        this.page = 1;
        this.pageSize = pageSize2;
        this.isRefresh = isRefresh2;
        this.isLocal = true;
        this.boardId = boardId2;
    }

    public List<GoodsModel> getNewestList() {
        if (this.page == 1) {
            if (this.isRefresh) {
                return getNewestListByNet(this.page, this.pageSize, this.boardId);
            }
            List<GoodsModel> newestList = getNewestListsByLocal(this.page, this.pageSize, this.boardId);
            if (newestList == null || newestList.isEmpty()) {
                return getNewestListByNet(this.page, this.pageSize, this.boardId);
            }
            this.isLocal = this.contentCacheDB.getListReq(BaseCacheDBCache.NEWEST_TYPE);
            return newestList;
        } else if (this.isLocal) {
            return getNewestListsByLocal(this.page, this.pageSize, this.boardId);
        } else {
            return getNewestListByNet(this.page, this.pageSize, this.boardId);
        }
    }

    public List<GoodsModel> getNewestListByNet(int page2, int pageSize2, long boardId2) {
        return getNewestListByNet(page2, pageSize2, boardId2, false);
    }

    public List<GoodsModel> getNewestListByNet(int page2, int pageSize2, long boardId2, boolean isInitData) {
        setRefreshTime();
        String jsonStr = NewestRestfulApiRequester.getNewestList(this.context, page2);
        List<GoodsModel> goodsList = NewestServiceImplHelper.parseNewestList(jsonStr);
        if (goodsList == null || goodsList.isEmpty()) {
            if (goodsList == null) {
                goodsList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                GoodsModel goodsModel = new GoodsModel();
                goodsModel.setErrorCode(errorCode);
                goodsList.add(goodsModel);
            }
            if (this.page == 1 && !isInitData) {
                this.page = this.lastPage;
            }
        } else {
            if (this.page == 1) {
                if (AutogenNewestListDBUtil.getInstance(this.context).deleteNewestboard(boardId2)) {
                    AutogenNewestListDBUtil.getInstance(this.context).saveNewestList(page2, goodsList.size(), boardId2, jsonStr);
                }
                this.contentCacheDB.setListLocal(BaseCacheDBCache.NEWEST_TYPE, false);
                this.isLocal = false;
            } else {
                AutogenNewestListDBUtil.getInstance(this.context).saveNewestList(page2, goodsList.size(), boardId2, jsonStr);
            }
            this.page++;
        }
        return goodsList;
    }

    /* access modifiers changed from: protected */
    public void setRefreshTime() {
        this.refreshCacheDB.setRefreshTime(BaseCacheDBCache.NEWEST_TYPE, System.currentTimeMillis());
    }

    public boolean getNewestByLocal() {
        return this.contentCacheDB.getListReq(BaseCacheDBCache.NEWEST_TYPE);
    }

    public String getRefreshDate() {
        return DateUtil.getFormatTime(this.refreshCacheDB.getRefreshTime(BaseCacheDBCache.NEWEST_TYPE));
    }

    public List<GoodsModel> getNewestListsByLocal(int page2, int pageSize2, long boardId2) {
        List<GoodsModel> goodsList = new ArrayList<>();
        String jsonStr = AutogenNewestListDBUtil.getInstance(this.context).getNewestList(page2, boardId2);
        if (!(jsonStr == null || (goodsList = NewestServiceImplHelper.parseNewestList(jsonStr)) == null || goodsList.isEmpty() || goodsList == null || goodsList.isEmpty())) {
            this.page++;
        }
        this.contentCacheDB.setListLocal(BaseCacheDBCache.NEWEST_TYPE, true);
        return goodsList;
    }

    public int getCacheDB() {
        return this.contentCacheDB.getListPosition(BaseCacheDBCache.NEWEST_TYPE);
    }

    public void saveCacheDB(int position) {
        this.contentCacheDB.setListPage(BaseCacheDBCache.NEWEST_TYPE, this.page);
        this.contentCacheDB.setListItem(BaseCacheDBCache.NEWEST_TYPE, position);
    }
}
