package com.appbyme.android.service;

import com.appbyme.android.base.model.GoodsModel;
import java.util.List;

public interface ClassifyService {
    int getCacheDB();

    boolean getClassifyByLocal();

    List<GoodsModel> getClassifyList();

    List<GoodsModel> getClassifyListByLocal(int i, long j, int i2);

    List<GoodsModel> getClassifyListByNet(int i, int i2, long j);

    List<GoodsModel> getClassifyListByNet(int i, int i2, long j, boolean z);

    String getRefreshDate();

    void initCacheDB(int i, long j, boolean z);

    void saveCacheDB(int i);
}
