package com.appbyme.android.service.impl;

import android.content.Context;
import com.appbyme.android.api.PersonalRestfulApiRequester;
import com.appbyme.android.base.db.ContentCacheDB;
import com.appbyme.android.base.db.RefreshCacheDB;
import com.appbyme.android.base.db.constant.BaseCacheDBCache;
import com.appbyme.android.base.model.GoodsModel;
import com.appbyme.android.service.PersonalService;
import com.appbyme.android.service.impl.helper.PersonalServiceImplHelper;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.util.DateUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class PersonalServiceImpl implements PersonalService {
    private String TAG = "PersonalServiceImpl";
    protected ContentCacheDB contentCacheDB;
    private Context context;
    protected boolean isLocal;
    protected boolean isRefresh;
    protected int lastPage;
    protected int page;
    protected int pageSize;
    protected RefreshCacheDB refreshCacheDB;

    public PersonalServiceImpl(Context context2) {
        this.context = context2;
        this.contentCacheDB = ContentCacheDB.getInstance(context2);
        this.refreshCacheDB = RefreshCacheDB.getInstance(context2);
    }

    public void initCacheDB(int pageSize2, boolean isRefresh2) {
        this.page = 1;
        this.pageSize = pageSize2;
        this.isRefresh = isRefresh2;
        this.isLocal = true;
    }

    public List<GoodsModel> getFavorList() {
        if (this.isRefresh) {
            this.page = 1;
        }
        return getFavorListByNet(this.page, this.pageSize);
    }

    public List<GoodsModel> getFavorListByNet(int page2, int pageSize2) {
        String jsonStr = PersonalRestfulApiRequester.goodsFavor(this.context, page2, pageSize2);
        List<GoodsModel> goodsList = PersonalServiceImplHelper.parseFavorList(jsonStr);
        if (goodsList == null || goodsList.isEmpty()) {
            if (goodsList == null) {
                goodsList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                GoodsModel goodsModel = new GoodsModel();
                goodsModel.setErrorCode(errorCode);
                goodsList.add(goodsModel);
            }
        } else {
            this.page++;
        }
        return goodsList;
    }

    public void setRefreshData() {
        this.refreshCacheDB.setRefreshTime(BaseCacheDBCache.GOODSCOMMENT_TYPE, System.currentTimeMillis());
    }

    public String getRefreshData() {
        return DateUtil.getFormatTime(this.refreshCacheDB.getRefreshTime(BaseCacheDBCache.GOODSCOMMENT_TYPE));
    }

    public List<GoodsModel> getCommentList() {
        if (!this.isRefresh) {
            return getCommentListByNet(this.page, this.pageSize);
        }
        this.page = 1;
        return getCommentListByNet(this.page, this.pageSize);
    }

    public List<GoodsModel> getCommentListByNet(int page2, int pageSize2) {
        String jsonStr = PersonalRestfulApiRequester.goodsComment(this.context, page2, pageSize2);
        List<GoodsModel> goodsList = PersonalServiceImplHelper.parseCommentList(jsonStr);
        if (goodsList == null || goodsList.isEmpty()) {
            if (goodsList == null) {
                goodsList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                GoodsModel goodsModel = new GoodsModel();
                goodsModel.setErrorCode(errorCode);
                goodsList.add(goodsModel);
            }
        } else {
            this.page++;
        }
        return goodsList;
    }
}
