package com.appbyme.android.service.impl;

import android.content.Context;
import com.appbyme.android.api.HotRestfulApiRequester;
import com.appbyme.android.base.db.ContentCacheDB;
import com.appbyme.android.base.db.RefreshCacheDB;
import com.appbyme.android.base.model.GoodsModel;
import com.appbyme.android.hot.db.AutogenHotListDBUtil;
import com.appbyme.android.service.HotService;
import com.appbyme.android.service.impl.helper.HotServiceImplHelper;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.util.DateUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class HotServiceImpl implements HotService {
    private String TAG = "HotServiceImpl";
    protected long boradId;
    protected ContentCacheDB contentCacheDB;
    private Context context;
    protected boolean isLocal;
    protected boolean isRefresh;
    protected int lastPage;
    protected int page;
    protected int pageSize;
    protected RefreshCacheDB refreshCacheDB;

    public void initCacheDB(int pageSize2, long boardId, boolean isRefresh2) {
        this.page = 1;
        this.pageSize = pageSize2;
        this.isRefresh = isRefresh2;
        this.isLocal = true;
        this.boradId = boardId;
    }

    public HotServiceImpl(Context context2) {
        this.context = context2;
        this.contentCacheDB = ContentCacheDB.getInstance(context2);
        this.refreshCacheDB = RefreshCacheDB.getInstance(context2);
    }

    public List<GoodsModel> getHotList() {
        if (this.page == 1) {
            if (this.isRefresh) {
                return getHotListByNet(this.page, this.pageSize, this.boradId);
            }
            List<GoodsModel> goodsList = getHotListByLocal(this.page, this.pageSize, this.boradId);
            if (goodsList == null || goodsList.isEmpty()) {
                return getHotListByNet(this.page, this.pageSize, this.boradId);
            }
            this.isLocal = this.contentCacheDB.getListReq("hot");
            return goodsList;
        } else if (this.isLocal) {
            return getHotListByLocal(this.page, this.pageSize, this.boradId);
        } else {
            return getHotListByNet(this.page, this.pageSize, this.boradId);
        }
    }

    public List<GoodsModel> getHotListByNet(int page2, int pageSize2, long boradId2) {
        return getHotListByNet(page2, pageSize2, boradId2, false);
    }

    public List<GoodsModel> getHotListByNet(int page2, int pageSize2, long boradId2, boolean isInitData) {
        setRefreshTime();
        String jsonStr = HotRestfulApiRequester.getHotList(this.context, page2);
        List<GoodsModel> goodsList = HotServiceImplHelper.parseHotList(jsonStr);
        if (goodsList == null || goodsList.isEmpty()) {
            if (goodsList == null) {
                goodsList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                GoodsModel goodsModel = new GoodsModel();
                goodsModel.setErrorCode(errorCode);
                goodsList.add(goodsModel);
            }
        } else {
            if (this.page == 1) {
                if (AutogenHotListDBUtil.getInstance(this.context).deleteHotBoard(boradId2)) {
                    AutogenHotListDBUtil.getInstance(this.context).saveHotList(page2, goodsList.size(), boradId2, jsonStr);
                }
                this.contentCacheDB.setListLocal("hot", false);
                this.isLocal = false;
            } else {
                AutogenHotListDBUtil.getInstance(this.context).saveHotList(page2, goodsList.size(), boradId2, jsonStr);
            }
            this.page++;
        }
        return goodsList;
    }

    public List<GoodsModel> getHotListByLocal(int page2, int pageSize2, long boradId2) {
        List<GoodsModel> goodsList = new ArrayList<>();
        String jsonStr = AutogenHotListDBUtil.getInstance(this.context).getHotList(page2, boradId2);
        if (!(jsonStr == null || (goodsList = HotServiceImplHelper.parseHotList(jsonStr)) == null || goodsList.isEmpty())) {
            this.page++;
        }
        this.contentCacheDB.setListLocal("hot", true);
        return goodsList;
    }

    public boolean getHotByLocal() {
        return this.contentCacheDB.getListReq("hot");
    }

    /* access modifiers changed from: protected */
    public void setRefreshTime() {
        this.refreshCacheDB.setRefreshTime("hot", System.currentTimeMillis());
    }

    public String getRefreshDate() {
        return DateUtil.getFormatTime(this.refreshCacheDB.getRefreshTime("hot"));
    }

    public int getCacheDB() {
        return this.contentCacheDB.getListPosition("hot");
    }

    public void saveCacheDB(int position) {
        this.contentCacheDB.setListPage("hot", this.page);
        this.contentCacheDB.setListItem("hot", position);
    }
}
