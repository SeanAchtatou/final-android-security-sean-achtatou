package com.appbyme.android.service;

import com.appbyme.android.base.model.GoodsModel;
import java.util.List;

public interface HotService {
    int getCacheDB();

    boolean getHotByLocal();

    List<GoodsModel> getHotList();

    List<GoodsModel> getHotListByLocal(int i, int i2, long j);

    List<GoodsModel> getHotListByNet(int i, int i2, long j);

    List<GoodsModel> getHotListByNet(int i, int i2, long j, boolean z);

    String getRefreshDate();

    void initCacheDB(int i, long j, boolean z);

    void saveCacheDB(int i);
}
