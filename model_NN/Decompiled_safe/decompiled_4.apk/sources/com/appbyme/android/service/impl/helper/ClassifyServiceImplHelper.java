package com.appbyme.android.service.impl.helper;

import com.appbyme.android.api.constant.ClassifyApiConstant;
import com.appbyme.android.base.model.GoodsModel;
import com.mobcent.forum.android.constant.PostsApiConstant;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ClassifyServiceImplHelper implements ClassifyApiConstant, PostsApiConstant {
    /* JADX INFO: Multiple debug info for r11v4 int: [D('jsonStr' java.lang.String), D('rs' int)] */
    /* JADX INFO: Multiple debug info for r11v8 java.util.ArrayList<com.appbyme.android.base.model.GoodsModel>: [D('baseUrl' java.lang.String), D('rs' int)] */
    /* JADX INFO: Multiple debug info for r1v3 int: [D('goodsModel' com.appbyme.android.base.model.GoodsModel), D('i' int)] */
    public static List<GoodsModel> parseClassifyList(String jsonStr) {
        ArrayList<GoodsModel> goodsList = new ArrayList<>();
        try {
            JSONObject jsonRoot = new JSONObject(jsonStr);
            if (jsonRoot.optInt("rs") == 0) {
                return null;
            }
            long totalItems = jsonRoot.optLong("tbk_items_total");
            String baseUrl = jsonRoot.optString("base_url");
            JSONArray jsonArr = jsonRoot.optJSONArray("tbk_items");
            int len = jsonArr.length();
            for (int i = 0; i < len; i++) {
                JSONObject jsonObject = jsonArr.optJSONObject(i);
                GoodsModel goodsModel = new GoodsModel();
                goodsModel.setBoardId((long) jsonObject.optInt("board_id"));
                goodsModel.setBoardName(jsonObject.optString("board_name"));
                goodsModel.setEssence(jsonObject.optInt(PostsApiConstant.ESSENCE));
                goodsModel.setForumId(jsonObject.optInt("forumId"));
                goodsModel.setHits(jsonObject.optInt(PostsApiConstant.HITS));
                goodsModel.setHot(jsonObject.optInt("hot"));
                goodsModel.setLastReplyDate(jsonObject.optLong("last_reply_date"));
                if (!StringUtil.isEmpty(jsonObject.optString("pic_path"))) {
                    goodsModel.setPicPath(String.valueOf(baseUrl) + jsonObject.optString("pic_path"));
                }
                goodsModel.setReplies(jsonObject.optInt("replies"));
                goodsModel.setStatus(jsonObject.optInt("status"));
                goodsModel.setTitle(jsonObject.optString("title"));
                goodsModel.setTop(jsonObject.optInt("top"));
                goodsModel.setTopicId((long) jsonObject.optInt("topic_id"));
                goodsModel.setType(jsonObject.optInt("type"));
                goodsModel.setUploadType(jsonObject.optInt(PostsApiConstant.UPLOAD_TYPE));
                goodsModel.setUserId(jsonObject.optInt("user_id"));
                goodsModel.setUserNickName(jsonObject.optString("user_nick_name"));
                goodsModel.setRatio(Double.valueOf(jsonObject.optDouble("ratio")).floatValue());
                goodsModel.setPrice(jsonObject.optString("price"));
                goodsModel.setTbkItemsTotal(totalItems);
                goodsList.add(goodsModel);
            }
            return goodsList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
