package com.appbyme.android.service.impl.helper;

import com.appbyme.android.api.constant.AutogenConfigApiConstant;
import com.appbyme.android.base.model.AutogenBoardCategory;
import com.appbyme.android.base.model.AutogenBoardModel;
import com.appbyme.android.base.model.AutogenConfigModel;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.mobcent.forum.android.constant.BoardApiConstant;
import com.mobcent.forum.android.model.BoardModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class AutogenConfigServiceImplHelper implements AutogenConfigApiConstant, BoardApiConstant {
    /* JADX INFO: Multiple debug info for r9v7 int: [D('jsonStr' java.lang.String), D('rs' int)] */
    /* JADX INFO: Multiple debug info for r9v9 org.json.JSONObject: [D('dataObj' org.json.JSONObject), D('rs' int)] */
    /* JADX INFO: Multiple debug info for r0v5 boolean: [D('obj' org.json.JSONObject), D('isUpdate' boolean)] */
    /* JADX INFO: Multiple debug info for r9v12 com.appbyme.android.base.model.AutogenConfigModel: [D('jsonStr' java.lang.String), D('i' int)] */
    public static AutogenConfigModel parseJsonToConfigModel(String jsonStr) {
        Exception e;
        if (jsonStr == null) {
            return null;
        }
        try {
            AutogenConfigModel configModel = new AutogenConfigModel();
            try {
                JSONObject obj = new JSONObject(jsonStr);
                if (obj.optInt("rs") == 0) {
                    return null;
                }
                JSONObject dataObj = obj.optJSONObject("data");
                if (!obj.optBoolean(AutogenConfigApiConstant.IS_UPDATE, false)) {
                    return null;
                }
                configModel.setVersion(dataObj.optInt("version", 0));
                configModel.setForumId(dataObj.optInt("forum_id", 0));
                configModel.setStyle(dataObj.optInt(AutogenConfigApiConstant.STYLE, 0));
                JSONArray objArray = dataObj.optJSONArray(AutogenConfigApiConstant.CONFIG_LIST);
                if (objArray == null) {
                    return null;
                }
                List<AutogenModuleModel> moduleList = new ArrayList<>();
                int size = objArray.length();
                for (int i = 0; i < size; i++) {
                    JSONObject mObj = objArray.optJSONObject(i);
                    AutogenModuleModel moduleModel = new AutogenModuleModel();
                    moduleModel.setPosition(mObj.optInt("position", 0));
                    moduleModel.setType(mObj.optInt("type", 0));
                    moduleModel.setModuleType(mObj.optInt(AutogenConfigApiConstant.MODULE_TYPE, 0));
                    moduleModel.setModuleId((long) mObj.optInt(AutogenConfigApiConstant.MODULE_ID, 0));
                    moduleModel.setModuleName(mObj.optString(AutogenConfigApiConstant.MODULE_NAME));
                    moduleModel.setListDisplay(mObj.optInt(AutogenConfigApiConstant.LIST_DISPLAY, 0));
                    moduleModel.setModuleIcon(mObj.optString(AutogenConfigApiConstant.MODULE_ICON));
                    moduleModel.setBoardDisplay(mObj.optInt(AutogenConfigApiConstant.BOARD_DISPLAY, 0));
                    moduleModel.setDetailDisplay(mObj.optInt(AutogenConfigApiConstant.DETAIL_DISPLAY, 0));
                    moduleModel.setListOrBoard(mObj.optInt(AutogenConfigApiConstant.LIST_OR_BOARD, 0));
                    moduleModel.setBoardListKey(String.valueOf(moduleModel.getModuleId()) + "+" + moduleModel.getModuleType());
                    moduleList.add(moduleModel);
                }
                configModel.setModuleList(moduleList);
                return configModel;
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return null;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r3v9 org.json.JSONObject: [D('mObj' org.json.JSONObject), D('boardCategorylist' java.util.ArrayList<com.appbyme.android.base.model.AutogenBoardCategory>)] */
    /* JADX INFO: Multiple debug info for r5v11 int: [D('moduleId' int), D('moduleType' int)] */
    /* JADX INFO: Multiple debug info for r5v13 org.json.JSONArray: [D('moduleType' int), D('boardCatagoryArray' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r3v15 org.json.JSONArray: [D('categoryObj' org.json.JSONObject), D('boardArray' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r3v16 java.util.ArrayList: [D('boardArray' org.json.JSONArray), D('boardList' java.util.ArrayList<com.mobcent.forum.android.model.BoardModel>)] */
    /* JADX INFO: Multiple debug info for r3v17 int: [D('boardList' java.util.ArrayList<com.mobcent.forum.android.model.BoardModel>), D('t' int)] */
    /* JADX INFO: Multiple debug info for r10v5 int: [D('j' int), D('boardModel' com.mobcent.forum.android.model.BoardModel)] */
    public static AutogenBoardModel parseJsonToBoardModel(String jsonStr) {
        Exception e;
        HashMap hashMap;
        try {
            AutogenBoardModel autogenBoardModel = new AutogenBoardModel();
            try {
                JSONObject jSONObject = new JSONObject(jsonStr);
                if (jSONObject.optInt("rs") == 0) {
                    AutogenBoardModel autogenBoardModel2 = autogenBoardModel;
                    return null;
                }
                JSONArray listArray = jSONObject.optJSONArray("list");
                if (listArray == null) {
                    AutogenBoardModel autogenBoardModel3 = autogenBoardModel;
                    return null;
                }
                String baseUrl = jSONObject.optString("base_url");
                ArrayList<AutogenBoardCategory> boardCategorylist = new ArrayList<>();
                try {
                    hashMap = new HashMap();
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return null;
                }
                try {
                    int size = listArray.length();
                    for (int i = 0; i < size; i++) {
                        JSONObject mObj = listArray.optJSONObject(i);
                        int optInt = mObj.optInt(AutogenConfigApiConstant.MODULE_ID);
                        if (mObj.optInt(AutogenConfigApiConstant.MODULE_TYPE) == 16) {
                            JSONArray boardCatagoryArray = mObj.optJSONArray(AutogenConfigApiConstant.BOARD_ACTEGORIES);
                            if (boardCatagoryArray == null) {
                                ArrayList<AutogenBoardCategory> arrayList = boardCategorylist;
                                AutogenBoardModel autogenBoardModel4 = autogenBoardModel;
                                HashMap hashMap2 = hashMap;
                                return null;
                            }
                            int s = boardCatagoryArray.length();
                            for (int t = 0; t < s; t++) {
                                AutogenBoardCategory boardCategory = new AutogenBoardCategory();
                                JSONObject categoryObj = boardCatagoryArray.optJSONObject(t);
                                boardCategory.setBoardCategoryId(categoryObj.optInt("board_category_id"));
                                boardCategory.setBoardCategoryName(categoryObj.optString("board_category_name"));
                                boardCategory.setBoardCategorySort(categoryObj.optInt(AutogenConfigApiConstant.BOARD_CATEGORY_SORT));
                                boardCategory.setBoardCategoryType(categoryObj.optInt("board_category_type"));
                                boardCategory.setBoardCategoryImgUrl(String.valueOf(baseUrl) + categoryObj.optString(AutogenConfigApiConstant.BOARD_CATEGORY_IMG_URLC));
                                boardCategory.setBoardCategoryId(categoryObj.optInt("board_category_id"));
                                JSONArray boardArray = categoryObj.optJSONArray("board_list");
                                if (boardArray == null || boardArray.length() <= 0) {
                                    ArrayList<BoardModel> boardList = new ArrayList<>();
                                    boardList.add(new BoardModel());
                                    boardCategory.setBoardList(boardList);
                                } else {
                                    ArrayList<BoardModel> boardList2 = new ArrayList<>();
                                    int length = boardArray.length();
                                    for (int j = 0; j < length; j++) {
                                        JSONObject boardObj = boardArray.optJSONObject(j);
                                        BoardModel boardModel = new BoardModel();
                                        boardModel.setBoardDesc(boardObj.optString(BoardApiConstant.BOARD_DESC));
                                        boardModel.setBoardId(boardObj.optLong("board_id"));
                                        boardModel.setBoardName(boardObj.optString("board_name"));
                                        boardModel.setPermission(boardObj.optInt(BoardApiConstant.BOARD_PERMISSION));
                                        boardModel.setLastPostsDate(boardObj.optLong(BoardApiConstant.LAST_POSTS_DATE));
                                        boardModel.setPicPath(boardObj.optString(String.valueOf(baseUrl) + "pic_path"));
                                        boardModel.setPostsTotalNum(boardObj.optInt(BoardApiConstant.POSTS_TOTAL_NUM));
                                        boardModel.setTodayPostsNum(boardObj.optInt(BoardApiConstant.TD_POSTS_NUM));
                                        boardModel.setTopicTotalNum(boardObj.optInt(BoardApiConstant.TOPIC_TOTAL_NUM));
                                        boardModel.setBaseUrl(baseUrl);
                                        boardList2.add(boardModel);
                                    }
                                    boardCategory.setBoardList(boardList2);
                                    ArrayList<BoardModel> arrayList2 = boardList2;
                                }
                                boardCategorylist.add(boardCategory);
                            }
                            hashMap.put("10003", boardCategorylist);
                        }
                    }
                    autogenBoardModel.setCategoryMaps(hashMap);
                    ArrayList<AutogenBoardCategory> arrayList3 = boardCategorylist;
                    AutogenBoardModel autogenBoardModel5 = autogenBoardModel;
                    AutogenBoardModel autogenBoardModel6 = autogenBoardModel;
                    HashMap hashMap3 = hashMap;
                    return autogenBoardModel6;
                } catch (Exception e3) {
                    e = e3;
                    e.printStackTrace();
                    return null;
                }
            } catch (Exception e4) {
                e = e4;
                e.printStackTrace();
                return null;
            }
        } catch (Exception e5) {
            e = e5;
            e.printStackTrace();
            return null;
        }
    }
}
