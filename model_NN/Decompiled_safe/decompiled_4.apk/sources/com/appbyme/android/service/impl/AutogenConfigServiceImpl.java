package com.appbyme.android.service.impl;

import android.content.Context;
import com.appbyme.android.base.db.SharedPreferencesDB;
import com.appbyme.android.base.model.AutogenBoardModel;
import com.appbyme.android.base.model.AutogenConfigModel;
import com.appbyme.android.constant.AutogenConfigConstant;
import com.appbyme.android.service.AutogenConfigService;
import com.appbyme.android.service.impl.helper.AutogenConfigServiceImplHelper;
import com.mobcent.forum.android.util.MCLogUtil;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class AutogenConfigServiceImpl implements AutogenConfigService, AutogenConfigConstant {
    private String TAG = "AutogenConfigServiceImpl";
    private String boardPath;
    private Context context;
    private String modulePath;

    public AutogenConfigServiceImpl(Context context2) {
        this.context = context2;
        this.modulePath = String.valueOf(context2.getFilesDir().getAbsolutePath()) + MODULE_CONFIG_PATH;
        this.boardPath = String.valueOf(context2.getFilesDir().getAbsolutePath()) + BOARD_CONFIG_PATH;
        File moduleFile = new File(this.modulePath);
        if (!moduleFile.exists()) {
            moduleFile.mkdir();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public AutogenConfigModel getConfigModel() {
        String jsonStr = getConfigFromAccest();
        if (jsonStr == null) {
            jsonStr = getModuleModelByNet();
        }
        AutogenConfigModel configModel = AutogenConfigServiceImplHelper.parseJsonToConfigModel(jsonStr);
        if (configModel == null) {
            return null;
        }
        SharedPreferencesDB.getInstance(this.context).setConfigVersion(configModel.getVersion());
        int size = configModel.getModuleList().size();
        for (int i = 0; i < size; i++) {
            MCLogUtil.i("AutogenConfigServiceImpl", "autogen-module =" + configModel.getModuleList().get(i).toString());
        }
        return configModel;
    }

    private String getConfigFromAccest() {
        IOException e;
        try {
            InputStream is = this.context.getAssets().open("module.json", 1);
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            try {
                byte[] buffer = new byte[1024];
                while (true) {
                    int length = is.read(buffer);
                    if (length == -1) {
                        String res = outStream.toString();
                        MCLogUtil.i("AutogenConfigServiceImpl", "from assets    res = " + res);
                        ByteArrayOutputStream byteArrayOutputStream = outStream;
                        return res;
                    }
                    outStream.write(buffer, 0, length);
                }
            } catch (IOException e2) {
                e = e2;
                e.printStackTrace();
                return null;
            }
        } catch (IOException e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
    }

    public AutogenBoardModel getBoardModel() {
        String jsonStr = getBoardModelByLocal();
        if (jsonStr == null) {
            jsonStr = getBoardModelByNet();
        }
        AutogenBoardModel autogenBoardModel = AutogenConfigServiceImplHelper.parseJsonToBoardModel(jsonStr);
        if (autogenBoardModel == null) {
            return null;
        }
        return autogenBoardModel;
    }

    /* JADX INFO: Multiple debug info for r1v2 java.io.IOException: [D('e' java.lang.Exception), D('e' java.io.IOException)] */
    /* JADX INFO: Multiple debug info for r1v6 java.io.IOException: [D('e' java.lang.Exception), D('e' java.io.IOException)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0054 A[SYNTHETIC, Splitter:B:21:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0063 A[SYNTHETIC, Splitter:B:29:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0082 A[SYNTHETIC, Splitter:B:45:0x0082] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getModuleModelByNet() {
        /*
            r9 = this;
            r8 = 0
            r4 = 0
            android.content.Context r6 = r9.context     // Catch:{ Exception -> 0x0070 }
            android.content.Context r7 = r9.context     // Catch:{ Exception -> 0x0070 }
            com.appbyme.android.base.db.SharedPreferencesDB r7 = com.appbyme.android.base.db.SharedPreferencesDB.getInstance(r7)     // Catch:{ Exception -> 0x0070 }
            int r7 = r7.getConfigVersion()     // Catch:{ Exception -> 0x0070 }
            java.lang.String r5 = com.appbyme.android.api.AutogenConfigRestfulApiRequester.getModuleInfo(r6, r7)     // Catch:{ Exception -> 0x0070 }
            com.appbyme.android.base.model.AutogenConfigModel r0 = com.appbyme.android.service.impl.helper.AutogenConfigServiceImplHelper.parseJsonToConfigModel(r5)     // Catch:{ Exception -> 0x0070 }
            if (r5 == 0) goto L_0x001a
            if (r0 != 0) goto L_0x002a
        L_0x001a:
            if (r4 == 0) goto L_0x0022
            r4.flush()     // Catch:{ IOException -> 0x0024 }
            r4.close()     // Catch:{ IOException -> 0x0024 }
        L_0x0022:
            r6 = r8
        L_0x0023:
            return r6
        L_0x0024:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()
            goto L_0x0022
        L_0x002a:
            r2 = 0
            java.io.FileWriter r3 = new java.io.FileWriter     // Catch:{ Exception -> 0x005c }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005c }
            java.lang.String r7 = r9.modulePath     // Catch:{ Exception -> 0x005c }
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x005c }
            r6.<init>(r7)     // Catch:{ Exception -> 0x005c }
            java.lang.String r7 = "module.json"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x005c }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x005c }
            r7 = 0
            r3.<init>(r6, r7)     // Catch:{ Exception -> 0x005c }
            r3.write(r5)     // Catch:{ Exception -> 0x00b5, all -> 0x00b2 }
            if (r3 == 0) goto L_0x009e
            r3.flush()     // Catch:{ IOException -> 0x0099 }
            r3.close()     // Catch:{ IOException -> 0x0099 }
            r2 = r3
        L_0x0052:
            if (r4 == 0) goto L_0x005a
            r4.flush()     // Catch:{ IOException -> 0x00a0 }
            r4.close()     // Catch:{ IOException -> 0x00a0 }
        L_0x005a:
            r6 = r5
            goto L_0x0023
        L_0x005c:
            r6 = move-exception
            r1 = r6
        L_0x005e:
            r1.printStackTrace()     // Catch:{ all -> 0x007f }
            if (r2 == 0) goto L_0x0052
            r2.flush()     // Catch:{ IOException -> 0x006a }
            r2.close()     // Catch:{ IOException -> 0x006a }
            goto L_0x0052
        L_0x006a:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()     // Catch:{ Exception -> 0x0070 }
            goto L_0x0052
        L_0x0070:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()     // Catch:{ all -> 0x0089 }
            if (r4 == 0) goto L_0x007d
            r4.flush()     // Catch:{ IOException -> 0x00a6 }
            r4.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x007d:
            r6 = r8
            goto L_0x0023
        L_0x007f:
            r6 = move-exception
        L_0x0080:
            if (r2 == 0) goto L_0x0088
            r2.flush()     // Catch:{ IOException -> 0x0093 }
            r2.close()     // Catch:{ IOException -> 0x0093 }
        L_0x0088:
            throw r6     // Catch:{ Exception -> 0x0070 }
        L_0x0089:
            r6 = move-exception
            if (r4 == 0) goto L_0x0092
            r4.flush()     // Catch:{ IOException -> 0x00ac }
            r4.close()     // Catch:{ IOException -> 0x00ac }
        L_0x0092:
            throw r6
        L_0x0093:
            r7 = move-exception
            r1 = r7
            r1.printStackTrace()     // Catch:{ Exception -> 0x0070 }
            goto L_0x0088
        L_0x0099:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()     // Catch:{ Exception -> 0x0070 }
        L_0x009e:
            r2 = r3
            goto L_0x0052
        L_0x00a0:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()
            goto L_0x005a
        L_0x00a6:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()
            goto L_0x007d
        L_0x00ac:
            r7 = move-exception
            r1 = r7
            r1.printStackTrace()
            goto L_0x0092
        L_0x00b2:
            r6 = move-exception
            r2 = r3
            goto L_0x0080
        L_0x00b5:
            r6 = move-exception
            r1 = r6
            r2 = r3
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appbyme.android.service.impl.AutogenConfigServiceImpl.getModuleModelByNet():java.lang.String");
    }

    /* JADX INFO: Multiple debug info for r1v3 java.io.IOException: [D('e' java.lang.Exception), D('e' java.io.IOException)] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x008f A[SYNTHETIC, Splitter:B:47:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0097 A[Catch:{ IOException -> 0x009b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String getModuleModelByLocal() {
        /*
            r12 = this;
            r11 = 0
            java.lang.String r8 = ""
            r3 = 0
            r6 = 0
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x00a8 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r10 = r12.modulePath     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Exception -> 0x00a8 }
            r9.<init>(r10)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r10 = "module.json"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x00a8 }
            r2.<init>(r9)     // Catch:{ Exception -> 0x00a8 }
            boolean r9 = r2.exists()     // Catch:{ Exception -> 0x00a8 }
            if (r9 != 0) goto L_0x003a
            if (r6 == 0) goto L_0x002d
            r6.flush()     // Catch:{ IOException -> 0x0034 }
            r6.close()     // Catch:{ IOException -> 0x0034 }
        L_0x002d:
            if (r3 == 0) goto L_0x0032
            r3.close()     // Catch:{ IOException -> 0x0034 }
        L_0x0032:
            r9 = r11
        L_0x0033:
            return r9
        L_0x0034:
            r9 = move-exception
            r1 = r9
            r1.printStackTrace()
            goto L_0x0032
        L_0x003a:
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00a8 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x00a8 }
            java.io.ByteArrayOutputStream r7 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x00ab, all -> 0x00a1 }
            r7.<init>()     // Catch:{ Exception -> 0x00ab, all -> 0x00a1 }
            r9 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r9]     // Catch:{ Exception -> 0x006a, all -> 0x00a4 }
            r5 = -1
        L_0x0049:
            int r5 = r4.read(r0)     // Catch:{ Exception -> 0x006a, all -> 0x00a4 }
            r9 = -1
            if (r5 != r9) goto L_0x0065
            java.lang.String r8 = r7.toString()     // Catch:{ Exception -> 0x006a, all -> 0x00a4 }
            if (r7 == 0) goto L_0x005c
            r7.flush()     // Catch:{ IOException -> 0x0080 }
            r7.close()     // Catch:{ IOException -> 0x0080 }
        L_0x005c:
            if (r4 == 0) goto L_0x0061
            r4.close()     // Catch:{ IOException -> 0x0080 }
        L_0x0061:
            r6 = r7
            r3 = r4
            r9 = r8
            goto L_0x0033
        L_0x0065:
            r9 = 0
            r7.write(r0, r9, r5)     // Catch:{ Exception -> 0x006a, all -> 0x00a4 }
            goto L_0x0049
        L_0x006a:
            r9 = move-exception
            r1 = r9
            r6 = r7
            r3 = r4
        L_0x006e:
            r1.printStackTrace()     // Catch:{ all -> 0x008c }
            if (r6 == 0) goto L_0x0079
            r6.flush()     // Catch:{ IOException -> 0x0086 }
            r6.close()     // Catch:{ IOException -> 0x0086 }
        L_0x0079:
            if (r3 == 0) goto L_0x007e
            r3.close()     // Catch:{ IOException -> 0x0086 }
        L_0x007e:
            r9 = r11
            goto L_0x0033
        L_0x0080:
            r9 = move-exception
            r1 = r9
            r1.printStackTrace()
            goto L_0x0061
        L_0x0086:
            r9 = move-exception
            r1 = r9
            r1.printStackTrace()
            goto L_0x007e
        L_0x008c:
            r9 = move-exception
        L_0x008d:
            if (r6 == 0) goto L_0x0095
            r6.flush()     // Catch:{ IOException -> 0x009b }
            r6.close()     // Catch:{ IOException -> 0x009b }
        L_0x0095:
            if (r3 == 0) goto L_0x009a
            r3.close()     // Catch:{ IOException -> 0x009b }
        L_0x009a:
            throw r9
        L_0x009b:
            r10 = move-exception
            r1 = r10
            r1.printStackTrace()
            goto L_0x009a
        L_0x00a1:
            r9 = move-exception
            r3 = r4
            goto L_0x008d
        L_0x00a4:
            r9 = move-exception
            r6 = r7
            r3 = r4
            goto L_0x008d
        L_0x00a8:
            r9 = move-exception
            r1 = r9
            goto L_0x006e
        L_0x00ab:
            r9 = move-exception
            r1 = r9
            r3 = r4
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appbyme.android.service.impl.AutogenConfigServiceImpl.getModuleModelByLocal():java.lang.String");
    }

    /* JADX INFO: Multiple debug info for r1v2 java.io.IOException: [D('e' java.lang.Exception), D('e' java.io.IOException)] */
    /* JADX INFO: Multiple debug info for r1v5 java.io.IOException: [D('e' java.lang.Exception), D('e' java.io.IOException)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x005a A[SYNTHETIC, Splitter:B:29:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0069 A[SYNTHETIC, Splitter:B:37:0x0069] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0088 A[SYNTHETIC, Splitter:B:53:0x0088] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getBoardModelByNet() {
        /*
            r9 = this;
            r8 = 0
            r4 = 0
            android.content.Context r6 = r9.context     // Catch:{ Exception -> 0x0076 }
            java.lang.String r5 = com.appbyme.android.api.AutogenConfigRestfulApiRequester.getBoardInfo(r6)     // Catch:{ Exception -> 0x0076 }
            if (r5 != 0) goto L_0x001a
            if (r4 == 0) goto L_0x0012
            r4.flush()     // Catch:{ IOException -> 0x0014 }
            r4.close()     // Catch:{ IOException -> 0x0014 }
        L_0x0012:
            r6 = r8
        L_0x0013:
            return r6
        L_0x0014:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()
            goto L_0x0012
        L_0x001a:
            com.appbyme.android.base.model.AutogenBoardModel r0 = com.appbyme.android.service.impl.helper.AutogenConfigServiceImplHelper.parseJsonToBoardModel(r5)     // Catch:{ Exception -> 0x0076 }
            if (r0 != 0) goto L_0x0030
            if (r4 == 0) goto L_0x0028
            r4.flush()     // Catch:{ IOException -> 0x002a }
            r4.close()     // Catch:{ IOException -> 0x002a }
        L_0x0028:
            r6 = r8
            goto L_0x0013
        L_0x002a:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()
            goto L_0x0028
        L_0x0030:
            r2 = 0
            java.io.FileWriter r3 = new java.io.FileWriter     // Catch:{ Exception -> 0x0062 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0062 }
            java.lang.String r7 = r9.boardPath     // Catch:{ Exception -> 0x0062 }
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ Exception -> 0x0062 }
            r6.<init>(r7)     // Catch:{ Exception -> 0x0062 }
            java.lang.String r7 = "board.json"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0062 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0062 }
            r7 = 0
            r3.<init>(r6, r7)     // Catch:{ Exception -> 0x0062 }
            r3.write(r5)     // Catch:{ Exception -> 0x00bb, all -> 0x00b8 }
            if (r3 == 0) goto L_0x00a4
            r3.flush()     // Catch:{ IOException -> 0x009f }
            r3.close()     // Catch:{ IOException -> 0x009f }
            r2 = r3
        L_0x0058:
            if (r4 == 0) goto L_0x0060
            r4.flush()     // Catch:{ IOException -> 0x00a6 }
            r4.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x0060:
            r6 = r5
            goto L_0x0013
        L_0x0062:
            r6 = move-exception
            r1 = r6
        L_0x0064:
            r1.printStackTrace()     // Catch:{ all -> 0x0085 }
            if (r2 == 0) goto L_0x0058
            r2.flush()     // Catch:{ IOException -> 0x0070 }
            r2.close()     // Catch:{ IOException -> 0x0070 }
            goto L_0x0058
        L_0x0070:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()     // Catch:{ Exception -> 0x0076 }
            goto L_0x0058
        L_0x0076:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()     // Catch:{ all -> 0x008f }
            if (r4 == 0) goto L_0x0083
            r4.flush()     // Catch:{ IOException -> 0x00ac }
            r4.close()     // Catch:{ IOException -> 0x00ac }
        L_0x0083:
            r6 = r8
            goto L_0x0013
        L_0x0085:
            r6 = move-exception
        L_0x0086:
            if (r2 == 0) goto L_0x008e
            r2.flush()     // Catch:{ IOException -> 0x0099 }
            r2.close()     // Catch:{ IOException -> 0x0099 }
        L_0x008e:
            throw r6     // Catch:{ Exception -> 0x0076 }
        L_0x008f:
            r6 = move-exception
            if (r4 == 0) goto L_0x0098
            r4.flush()     // Catch:{ IOException -> 0x00b2 }
            r4.close()     // Catch:{ IOException -> 0x00b2 }
        L_0x0098:
            throw r6
        L_0x0099:
            r7 = move-exception
            r1 = r7
            r1.printStackTrace()     // Catch:{ Exception -> 0x0076 }
            goto L_0x008e
        L_0x009f:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()     // Catch:{ Exception -> 0x0076 }
        L_0x00a4:
            r2 = r3
            goto L_0x0058
        L_0x00a6:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()
            goto L_0x0060
        L_0x00ac:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()
            goto L_0x0083
        L_0x00b2:
            r7 = move-exception
            r1 = r7
            r1.printStackTrace()
            goto L_0x0098
        L_0x00b8:
            r6 = move-exception
            r2 = r3
            goto L_0x0086
        L_0x00bb:
            r6 = move-exception
            r1 = r6
            r2 = r3
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appbyme.android.service.impl.AutogenConfigServiceImpl.getBoardModelByNet():java.lang.String");
    }

    /* JADX INFO: Multiple debug info for r1v3 java.io.IOException: [D('e' java.lang.Exception), D('e' java.io.IOException)] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x008f A[SYNTHETIC, Splitter:B:47:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0097 A[Catch:{ IOException -> 0x009b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String getBoardModelByLocal() {
        /*
            r12 = this;
            r11 = 0
            java.lang.String r8 = ""
            r3 = 0
            r6 = 0
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x00a8 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r10 = r12.boardPath     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ Exception -> 0x00a8 }
            r9.<init>(r10)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r10 = "board.json"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x00a8 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x00a8 }
            r2.<init>(r9)     // Catch:{ Exception -> 0x00a8 }
            boolean r9 = r2.exists()     // Catch:{ Exception -> 0x00a8 }
            if (r9 != 0) goto L_0x003a
            if (r6 == 0) goto L_0x002d
            r6.flush()     // Catch:{ IOException -> 0x0034 }
            r6.close()     // Catch:{ IOException -> 0x0034 }
        L_0x002d:
            if (r3 == 0) goto L_0x0032
            r3.close()     // Catch:{ IOException -> 0x0034 }
        L_0x0032:
            r9 = r11
        L_0x0033:
            return r9
        L_0x0034:
            r9 = move-exception
            r1 = r9
            r1.printStackTrace()
            goto L_0x0032
        L_0x003a:
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00a8 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x00a8 }
            java.io.ByteArrayOutputStream r7 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x00ab, all -> 0x00a1 }
            r7.<init>()     // Catch:{ Exception -> 0x00ab, all -> 0x00a1 }
            r9 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r9]     // Catch:{ Exception -> 0x006a, all -> 0x00a4 }
            r5 = -1
        L_0x0049:
            int r5 = r4.read(r0)     // Catch:{ Exception -> 0x006a, all -> 0x00a4 }
            r9 = -1
            if (r5 != r9) goto L_0x0065
            java.lang.String r8 = r7.toString()     // Catch:{ Exception -> 0x006a, all -> 0x00a4 }
            if (r7 == 0) goto L_0x005c
            r7.flush()     // Catch:{ IOException -> 0x0080 }
            r7.close()     // Catch:{ IOException -> 0x0080 }
        L_0x005c:
            if (r4 == 0) goto L_0x0061
            r4.close()     // Catch:{ IOException -> 0x0080 }
        L_0x0061:
            r6 = r7
            r3 = r4
            r9 = r8
            goto L_0x0033
        L_0x0065:
            r9 = 0
            r7.write(r0, r9, r5)     // Catch:{ Exception -> 0x006a, all -> 0x00a4 }
            goto L_0x0049
        L_0x006a:
            r9 = move-exception
            r1 = r9
            r6 = r7
            r3 = r4
        L_0x006e:
            r1.printStackTrace()     // Catch:{ all -> 0x008c }
            if (r6 == 0) goto L_0x0079
            r6.flush()     // Catch:{ IOException -> 0x0086 }
            r6.close()     // Catch:{ IOException -> 0x0086 }
        L_0x0079:
            if (r3 == 0) goto L_0x007e
            r3.close()     // Catch:{ IOException -> 0x0086 }
        L_0x007e:
            r9 = r11
            goto L_0x0033
        L_0x0080:
            r9 = move-exception
            r1 = r9
            r1.printStackTrace()
            goto L_0x0061
        L_0x0086:
            r9 = move-exception
            r1 = r9
            r1.printStackTrace()
            goto L_0x007e
        L_0x008c:
            r9 = move-exception
        L_0x008d:
            if (r6 == 0) goto L_0x0095
            r6.flush()     // Catch:{ IOException -> 0x009b }
            r6.close()     // Catch:{ IOException -> 0x009b }
        L_0x0095:
            if (r3 == 0) goto L_0x009a
            r3.close()     // Catch:{ IOException -> 0x009b }
        L_0x009a:
            throw r9
        L_0x009b:
            r10 = move-exception
            r1 = r10
            r1.printStackTrace()
            goto L_0x009a
        L_0x00a1:
            r9 = move-exception
            r3 = r4
            goto L_0x008d
        L_0x00a4:
            r9 = move-exception
            r6 = r7
            r3 = r4
            goto L_0x008d
        L_0x00a8:
            r9 = move-exception
            r1 = r9
            goto L_0x006e
        L_0x00ab:
            r9 = move-exception
            r1 = r9
            r3 = r4
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appbyme.android.service.impl.AutogenConfigServiceImpl.getBoardModelByLocal():java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x003e A[SYNTHETIC, Splitter:B:25:0x003e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String getAssetsConfig() {
        /*
            r8 = this;
            r2 = 0
            r0 = 0
            r5 = 0
            android.content.Context r6 = r8.context
            android.content.res.AssetManager r0 = r6.getAssets()
            java.lang.String r6 = "module.json"
            java.io.InputStream r5 = r0.open(r6)     // Catch:{ IOException -> 0x002b }
            int r6 = r5.available()     // Catch:{ IOException -> 0x002b }
            byte[] r1 = new byte[r6]     // Catch:{ IOException -> 0x002b }
            r3 = r2
        L_0x0016:
            int r6 = r5.read(r1)     // Catch:{ IOException -> 0x0050, all -> 0x004d }
            r7 = -1
            if (r6 != r7) goto L_0x0024
            if (r5 == 0) goto L_0x004b
            r5.close()     // Catch:{ IOException -> 0x0047 }
            r2 = r3
        L_0x0023:
            return r2
        L_0x0024:
            java.lang.String r2 = new java.lang.String     // Catch:{ IOException -> 0x0050, all -> 0x004d }
            r2.<init>(r1)     // Catch:{ IOException -> 0x0050, all -> 0x004d }
            r3 = r2
            goto L_0x0016
        L_0x002b:
            r6 = move-exception
            r4 = r6
        L_0x002d:
            r4.printStackTrace()     // Catch:{ all -> 0x003b }
            if (r5 == 0) goto L_0x0023
            r5.close()     // Catch:{ IOException -> 0x0036 }
            goto L_0x0023
        L_0x0036:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0023
        L_0x003b:
            r6 = move-exception
        L_0x003c:
            if (r5 == 0) goto L_0x0041
            r5.close()     // Catch:{ IOException -> 0x0042 }
        L_0x0041:
            throw r6
        L_0x0042:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0041
        L_0x0047:
            r4 = move-exception
            r4.printStackTrace()
        L_0x004b:
            r2 = r3
            goto L_0x0023
        L_0x004d:
            r6 = move-exception
            r2 = r3
            goto L_0x003c
        L_0x0050:
            r6 = move-exception
            r4 = r6
            r2 = r3
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appbyme.android.service.impl.AutogenConfigServiceImpl.getAssetsConfig():java.lang.String");
    }
}
