package com.appbyme.android.service.impl.helper;

import com.appbyme.android.base.model.ConfigModel;
import com.appbyme.android.constant.AutogenConfigConstant;
import org.json.JSONObject;

public class ConfigServiceImplHelper implements AutogenConfigConstant {
    public static ConfigModel getConfigModel(String jsonStr) {
        if (jsonStr == null) {
            return null;
        }
        try {
            ConfigModel configModel = new ConfigModel();
            configModel.setFormatId(new JSONObject(jsonStr).optInt(AutogenConfigConstant.CONFIG_FORMAT));
            return configModel;
        } catch (Exception e) {
            return null;
        }
    }

    public static ConfigModel getConfigModel2(String jsonStr) {
        if (jsonStr == null) {
            return null;
        }
        try {
            ConfigModel configModel = new ConfigModel();
            JSONObject jsonObj = new JSONObject(jsonStr);
            configModel.setModule1(jsonObj.optString(AutogenConfigConstant.MODULE1, null));
            configModel.setModule2(jsonObj.optString(AutogenConfigConstant.MODULE2, null));
            configModel.setModule3(jsonObj.optString(AutogenConfigConstant.MODULE3, null));
            configModel.setModule4(jsonObj.optString(AutogenConfigConstant.MODULE4, null));
            configModel.setModule0(jsonObj.optString(AutogenConfigConstant.MODULE0, null));
            configModel.setIsLocalApp(jsonObj.optInt(AutogenConfigConstant.IS_LOCAL_APP, 0));
            return configModel;
        } catch (Exception e) {
            return null;
        }
    }
}
