package com.appbyme.android.service.impl;

import android.content.Context;
import com.appbyme.android.base.model.ConfigModel;
import com.appbyme.android.constant.AutogenConfigConstant;
import com.appbyme.android.service.ConfigService;
import com.appbyme.android.service.impl.helper.ConfigServiceImplHelper;
import com.appbyme.android.util.AutogenStringUtil;
import java.util.ArrayList;
import java.util.List;

public class ConfigServiceImpl implements ConfigService, AutogenConfigConstant {
    private Context context;

    public ConfigServiceImpl(Context context2) {
        this.context = context2;
    }

    public ConfigModel getConfigModel() {
        return ConfigServiceImplHelper.getConfigModel(getConfigJsonStr());
    }

    public ConfigModel getConfigModel2() {
        return ConfigServiceImplHelper.getConfigModel2(getConfigJsonStr());
    }

    public List<Integer> getConfigIds(ConfigModel configModel) {
        List<Integer> integers = new ArrayList<>();
        integers.addAll(AutogenStringUtil.getStr(configModel.getModule1()));
        integers.addAll(AutogenStringUtil.getStr(configModel.getModule2()));
        integers.addAll(AutogenStringUtil.getStr(configModel.getModule3()));
        integers.addAll(AutogenStringUtil.getStr(configModel.getModule4()));
        integers.addAll(AutogenStringUtil.getStr(configModel.getModule0()));
        return integers;
    }

    public boolean isLocalApp(ConfigModel configModel) {
        if (configModel.getIsLocalApp() == 0) {
            return false;
        }
        return true;
    }

    public List<Integer> getConfigIds() {
        ConfigModel configModel = ConfigServiceImplHelper.getConfigModel2(getConfigJsonStr());
        List<Integer> integers = new ArrayList<>();
        integers.addAll(AutogenStringUtil.getStr(configModel.getModule1()));
        integers.addAll(AutogenStringUtil.getStr(configModel.getModule2()));
        integers.addAll(AutogenStringUtil.getStr(configModel.getModule3()));
        integers.addAll(AutogenStringUtil.getStr(configModel.getModule4()));
        return integers;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x003e A[SYNTHETIC, Splitter:B:25:0x003e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String getConfigJsonStr() {
        /*
            r8 = this;
            r2 = 0
            r0 = 0
            r5 = 0
            android.content.Context r6 = r8.context
            android.content.res.AssetManager r0 = r6.getAssets()
            java.lang.String r6 = "config/config.json"
            java.io.InputStream r5 = r0.open(r6)     // Catch:{ IOException -> 0x002b }
            int r6 = r5.available()     // Catch:{ IOException -> 0x002b }
            byte[] r1 = new byte[r6]     // Catch:{ IOException -> 0x002b }
            r3 = r2
        L_0x0016:
            int r6 = r5.read(r1)     // Catch:{ IOException -> 0x0050, all -> 0x004d }
            r7 = -1
            if (r6 != r7) goto L_0x0024
            if (r5 == 0) goto L_0x004b
            r5.close()     // Catch:{ IOException -> 0x0047 }
            r2 = r3
        L_0x0023:
            return r2
        L_0x0024:
            java.lang.String r2 = new java.lang.String     // Catch:{ IOException -> 0x0050, all -> 0x004d }
            r2.<init>(r1)     // Catch:{ IOException -> 0x0050, all -> 0x004d }
            r3 = r2
            goto L_0x0016
        L_0x002b:
            r6 = move-exception
            r4 = r6
        L_0x002d:
            r4.printStackTrace()     // Catch:{ all -> 0x003b }
            if (r5 == 0) goto L_0x0023
            r5.close()     // Catch:{ IOException -> 0x0036 }
            goto L_0x0023
        L_0x0036:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0023
        L_0x003b:
            r6 = move-exception
        L_0x003c:
            if (r5 == 0) goto L_0x0041
            r5.close()     // Catch:{ IOException -> 0x0042 }
        L_0x0041:
            throw r6
        L_0x0042:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0041
        L_0x0047:
            r4 = move-exception
            r4.printStackTrace()
        L_0x004b:
            r2 = r3
            goto L_0x0023
        L_0x004d:
            r6 = move-exception
            r2 = r3
            goto L_0x003c
        L_0x0050:
            r6 = move-exception
            r4 = r6
            r2 = r3
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appbyme.android.service.impl.ConfigServiceImpl.getConfigJsonStr():java.lang.String");
    }
}
