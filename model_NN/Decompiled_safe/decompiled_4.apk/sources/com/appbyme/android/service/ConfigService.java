package com.appbyme.android.service;

import com.appbyme.android.base.model.ConfigModel;
import java.util.List;

public interface ConfigService {
    List<Integer> getConfigIds();

    List<Integer> getConfigIds(ConfigModel configModel);

    ConfigModel getConfigModel();

    ConfigModel getConfigModel2();

    boolean isLocalApp(ConfigModel configModel);
}
