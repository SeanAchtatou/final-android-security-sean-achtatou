package com.appbyme.android.service.impl;

import android.content.Context;
import com.appbyme.android.api.GoodsDetailRestfulApiRequester;
import com.appbyme.android.base.model.GoodsCommentsModel;
import com.appbyme.android.base.model.GoodsDetailModel;
import com.appbyme.android.service.GoodsDetailService;
import com.appbyme.android.service.impl.helper.GoodsDetailImplHelper;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.util.StringUtil;

public class GoodsDetailServiceImpl implements GoodsDetailService {
    protected Context context;
    protected int page;
    protected int pageSize;
    protected int totalPage;

    public GoodsDetailServiceImpl(Context context2) {
        this.context = context2;
    }

    public void initData(int pageSize2) {
        this.page = 1;
        this.pageSize = pageSize2;
        this.totalPage = 1;
    }

    public int collectionGoods(long topicId) {
        return GoodsDetailImplHelper.parseGoodsCollect(GoodsDetailRestfulApiRequester.goodsFavor(this.context, topicId));
    }

    public GoodsDetailModel getGoodsDetailListByNet(long topicId) {
        return GoodsDetailImplHelper.parseGoodsDetail(GoodsDetailRestfulApiRequester.getGoodsDetail(this.context, topicId));
    }

    public GoodsCommentsModel getGoodsCommentsByNet(long numIid) {
        String jsonStr = GoodsDetailRestfulApiRequester.getGoodsCommentDetail(this.context, numIid, this.page);
        GoodsCommentsModel goodsCommentsModel = GoodsDetailImplHelper.parseGoodsCommetDetail(jsonStr);
        if (goodsCommentsModel == null) {
            goodsCommentsModel = new GoodsCommentsModel();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                goodsCommentsModel.setErrorCode(errorCode);
            }
        } else {
            this.page++;
            int total = goodsCommentsModel.getTotal();
            if (total % this.pageSize > 0) {
                this.totalPage = (total / this.pageSize) + 1;
            } else {
                this.totalPage = total / this.pageSize;
            }
        }
        return goodsCommentsModel;
    }
}
