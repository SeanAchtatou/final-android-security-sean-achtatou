package com.appbyme.android.service.impl;

import android.content.Context;
import com.appbyme.android.api.ClassifyRestfulApiRequester;
import com.appbyme.android.base.db.ContentCacheDB;
import com.appbyme.android.base.db.RefreshCacheDB;
import com.appbyme.android.base.db.constant.BaseCacheDBCache;
import com.appbyme.android.base.model.GoodsModel;
import com.appbyme.android.classify.db.AutogenClassifyListDBUtil;
import com.appbyme.android.service.ClassifyService;
import com.appbyme.android.service.impl.helper.ClassifyServiceImplHelper;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.util.DateUtil;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class ClassifyServiceImpl implements ClassifyService {
    private String TAG = "ClassifyServiceImpl";
    protected long boradId;
    protected ContentCacheDB contentCacheDB;
    private Context context;
    protected boolean isLocal;
    protected boolean isRefresh;
    protected int page;
    protected int pageSize;
    protected RefreshCacheDB refreshCacheDB;

    public ClassifyServiceImpl(Context context2) {
        this.context = context2;
        this.contentCacheDB = ContentCacheDB.getInstance(context2);
        this.refreshCacheDB = RefreshCacheDB.getInstance(context2);
    }

    public void initCacheDB(int pageSize2, long boardId, boolean isRefresh2) {
        this.page = 1;
        this.pageSize = pageSize2;
        this.isRefresh = isRefresh2;
        this.isLocal = true;
        this.boradId = boardId;
    }

    public List<GoodsModel> getClassifyList() {
        if (this.page == 1) {
            if (this.isRefresh) {
                return getClassifyListByNet(this.page, this.pageSize, this.boradId);
            }
            List<GoodsModel> goodsList = getClassifyListByLocal(this.page, this.boradId, this.pageSize);
            if (goodsList == null || goodsList.isEmpty()) {
                return getClassifyListByNet(this.page, this.pageSize, this.boradId);
            }
            this.isLocal = this.contentCacheDB.getListReq(BaseCacheDBCache.CLASSIFY_TYPE);
            return goodsList;
        } else if (this.isLocal) {
            return getClassifyListByLocal(this.page, this.boradId, this.pageSize);
        } else {
            return getClassifyListByNet(this.page, this.pageSize, this.boradId);
        }
    }

    public List<GoodsModel> getClassifyListByNet(int page2, int pageSize2, long boardId) {
        MCLogUtil.e(this.TAG, "网络加载数据");
        return getClassifyListByNet(page2, pageSize2, boardId, false);
    }

    public List<GoodsModel> getClassifyListByNet(int page2, int pageSize2, long classifyBoardId, boolean isInitData) {
        setRefreshTime();
        String jsonStr = ClassifyRestfulApiRequester.getClassifyList(this.context, page2, pageSize2, classifyBoardId);
        List<GoodsModel> goodsList = ClassifyServiceImplHelper.parseClassifyList(jsonStr);
        if (goodsList == null || goodsList.isEmpty()) {
            if (goodsList == null) {
                goodsList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                GoodsModel goodsModel = new GoodsModel();
                goodsModel.setErrorCode(errorCode);
                goodsList.add(goodsModel);
            }
        } else {
            if (this.page <= 1) {
                AutogenClassifyListDBUtil.getInstance(this.context).updataClassifyList(page2, goodsList.size(), classifyBoardId, jsonStr);
                this.contentCacheDB.setListLocal(BaseCacheDBCache.CLASSIFY_TYPE, false);
                this.isLocal = false;
            } else {
                AutogenClassifyListDBUtil.getInstance(this.context).saveClassifyList(page2, classifyBoardId, goodsList.size(), jsonStr);
            }
            this.page++;
        }
        return goodsList;
    }

    public List<GoodsModel> getClassifyListByLocal(int page2, long baordId, int pageSize2) {
        List<GoodsModel> goodsList = new ArrayList<>();
        String jsonStr = AutogenClassifyListDBUtil.getInstance(this.context).getClassifyList(page2, baordId);
        if (!(jsonStr == null || (goodsList = ClassifyServiceImplHelper.parseClassifyList(jsonStr)) == null || goodsList.isEmpty())) {
            this.page++;
        }
        this.contentCacheDB.setListLocal(BaseCacheDBCache.CLASSIFY_TYPE, true);
        return goodsList;
    }

    public boolean getClassifyByLocal() {
        return this.contentCacheDB.getListReq(BaseCacheDBCache.CLASSIFY_TYPE);
    }

    public String getRefreshDate() {
        return DateUtil.getFormatTime(this.refreshCacheDB.getRefreshTime(BaseCacheDBCache.CLASSIFY_TYPE));
    }

    public int getCacheDB() {
        return this.contentCacheDB.getListPosition(BaseCacheDBCache.CLASSIFY_TYPE);
    }

    public void saveCacheDB(int position) {
        this.contentCacheDB.setListPage(BaseCacheDBCache.CLASSIFY_TYPE, this.page);
        this.contentCacheDB.setListItem(BaseCacheDBCache.CLASSIFY_TYPE, position);
    }

    /* access modifiers changed from: protected */
    public void setRefreshTime() {
        this.refreshCacheDB.setRefreshTime(BaseCacheDBCache.CLASSIFY_TYPE, System.currentTimeMillis());
    }
}
