package com.appbyme.android.service;

import com.appbyme.android.base.model.AutogenBoardModel;
import com.appbyme.android.base.model.AutogenConfigModel;

public interface AutogenConfigService {
    AutogenBoardModel getBoardModel();

    String getBoardModelByNet();

    AutogenConfigModel getConfigModel();

    String getModuleModelByNet();
}
