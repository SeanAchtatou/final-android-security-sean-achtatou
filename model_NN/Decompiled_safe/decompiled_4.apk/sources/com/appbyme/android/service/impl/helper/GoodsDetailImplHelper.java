package com.appbyme.android.service.impl.helper;

import com.appbyme.android.base.model.GoodsCommentDetail;
import com.appbyme.android.base.model.GoodsCommentsModel;
import com.appbyme.android.base.model.GoodsDetailModel;
import com.appbyme.android.base.model.GoodsImgDetailModel;
import com.appbyme.android.base.model.GoodsShopModel;
import com.appbyme.android.base.model.ShopScore;
import com.appbyme.android.detail.db.constant.AutogenGoodsDetailConstant;
import com.mobcent.forum.android.constant.PostsApiConstant;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoodsDetailImplHelper implements AutogenGoodsDetailConstant, PostsApiConstant {
    public static int parseGoodsCollect(String jsonStr) {
        try {
            return new JSONObject(jsonStr).optInt("rs");
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /* JADX INFO: Multiple debug info for r10v2 com.appbyme.android.base.model.GoodsDetailModel: [D('goodsCommentsModel' com.appbyme.android.base.model.GoodsCommentsModel), D('rs' int)] */
    /* JADX INFO: Multiple debug info for r10v4 int: [D('jsonStr' java.lang.String), D('rs' int)] */
    /* JADX INFO: Multiple debug info for r10v8 boolean: [D('collectObj' org.json.JSONObject), D('isFavors' boolean)] */
    /* JADX INFO: Multiple debug info for r10v13 int: [D('clickUrl' java.lang.String), D('favorsNum' int)] */
    /* JADX INFO: Multiple debug info for r10v15 int: [D('favorsNum' int), D('volume' int)] */
    /* JADX INFO: Multiple debug info for r10v17 java.lang.String: [D('volume' int), D('goodsTitle' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r10v19 java.lang.String: [D('goodsTitle' java.lang.String), D('goodsPrice' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r10v24 org.json.JSONObject: [D('mainPic' org.json.JSONObject), D('tbDetailUrl' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v4 java.lang.String: [D('mainStr' java.lang.String), D('numIid' long)] */
    /* JADX INFO: Multiple debug info for r10v25 java.lang.String: [D('suffixPaht' java.lang.String), D('mainPic' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r10v42 java.lang.String: [D('position' int), D('url' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r10v46 java.lang.String: [D('shopPic' java.lang.String), D('nick' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r10v52 java.lang.String: [D('grade' int), D('localtion' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v5 com.appbyme.android.base.model.ShopScore: [D('shopScore' com.appbyme.android.base.model.ShopScore), D('shopObj' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r10v57 java.lang.String: [D('deliveryScore' java.lang.String), D('itemScore' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r10v59 java.lang.String: [D('itemScore' java.lang.String), D('serviceScore' java.lang.String)] */
    public static GoodsDetailModel parseGoodsDetail(String jsonStr) {
        GoodsDetailModel goodsDetailModel = new GoodsDetailModel();
        try {
            JSONObject obj = new JSONObject(jsonStr);
            if (obj.optInt("rs") == 0) {
                return null;
            }
            goodsDetailModel.setBoardId(obj.optLong("boardId"));
            goodsDetailModel.setFavors(obj.optJSONObject(AutogenGoodsDetailConstant.TBK_ITEM_ATTR).optBoolean(AutogenGoodsDetailConstant.ISFAVORS, false));
            JSONObject tbkObject = obj.optJSONObject(AutogenGoodsDetailConstant.TBK_ITEM);
            goodsDetailModel.setClickUrl(tbkObject.optString(AutogenGoodsDetailConstant.CLICKURL));
            goodsDetailModel.setFavorsNum(tbkObject.optInt(AutogenGoodsDetailConstant.FAVORSNUM));
            goodsDetailModel.setSaleNum(tbkObject.optInt(AutogenGoodsDetailConstant.VOLUME));
            goodsDetailModel.setGoodsTitle(tbkObject.optString("title"));
            goodsDetailModel.setPrice(tbkObject.optString("price"));
            goodsDetailModel.setNumIid(tbkObject.optLong("numIid"));
            goodsDetailModel.setTbDetailUrl(tbkObject.optString(AutogenGoodsDetailConstant.DETAILURL));
            JSONObject mainPic = tbkObject.optJSONObject(AutogenGoodsDetailConstant.MAINPICTURE);
            String mainStr = mainPic.optString("networkAccessUrl");
            goodsDetailModel.setGoodsThumb(String.valueOf(mainStr) + mainPic.optString("suffixPath"));
            GoodsShopModel goodsShopModel = new GoodsShopModel();
            JSONObject shopObj = obj.optJSONObject(AutogenGoodsDetailConstant.TBK_SHOP);
            if (shopObj != null) {
                goodsShopModel.setShopName(shopObj.optString(AutogenGoodsDetailConstant.NICK));
                goodsShopModel.setShopPic(AutogenGoodsDetailConstant.TB_HOST + shopObj.optString("picPath"));
                goodsShopModel.setGrade(tbkObject.optInt(AutogenGoodsDetailConstant.SELLERCREDITSCORE));
                goodsShopModel.setLocal(tbkObject.optString(AutogenGoodsDetailConstant.ITEMLOCATION));
                JSONObject shopScoreObj = shopObj.optJSONObject(AutogenGoodsDetailConstant.SHOPSCORE);
                ShopScore shopScore = new ShopScore();
                shopScore.setDeliveryScore(shopScoreObj.optString(AutogenGoodsDetailConstant.DELIVERYSCORE));
                shopScore.setItemScore(shopScoreObj.optString(AutogenGoodsDetailConstant.ITEMSCORE));
                shopScore.setServiceScore(shopScoreObj.optString(AutogenGoodsDetailConstant.SERVICESCORE));
                goodsShopModel.setShopScore(shopScore);
            }
            goodsDetailModel.setGoodsShopModel(goodsShopModel);
            JSONArray itemImgsArr = tbkObject.optJSONArray(AutogenGoodsDetailConstant.ITEMIMGS);
            int len = itemImgsArr.length();
            List<GoodsImgDetailModel> goodsImgDetailModelList = new ArrayList<>();
            for (int i = 0; i < len; i++) {
                GoodsImgDetailModel goodsImgDetailModel = new GoodsImgDetailModel();
                JSONObject itemImgsObj = itemImgsArr.optJSONObject(i);
                if (itemImgsObj != null) {
                    goodsImgDetailModel.setCreated(itemImgsObj.optString(AutogenGoodsDetailConstant.CREATED));
                    goodsImgDetailModel.setId(itemImgsObj.optLong("id"));
                    goodsImgDetailModel.setPosition(itemImgsObj.optInt("position"));
                    goodsImgDetailModel.setUrl(itemImgsObj.optString("url"));
                }
                goodsImgDetailModelList.add(goodsImgDetailModel);
            }
            goodsDetailModel.setGoodsImgDetailModelList(goodsImgDetailModelList);
            JSONObject tbkComments = obj.optJSONObject(AutogenGoodsDetailConstant.TBK_COMMENTS);
            GoodsCommentsModel goodsCommentsModel = new GoodsCommentsModel();
            if (tbkComments != null) {
                goodsCommentsModel = parserCommentCore(tbkComments);
            }
            goodsDetailModel.setGoodsCommentsModel(goodsCommentsModel);
            return goodsDetailModel;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static GoodsCommentsModel parseGoodsCommetDetail(String commetStr) {
        JSONObject obj = null;
        try {
            obj = new JSONObject(commetStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (obj == null) {
            return null;
        }
        if (obj.optInt("rs") == 0) {
            return null;
        }
        return parserCommentCore(obj.optJSONObject(AutogenGoodsDetailConstant.COMMENTS));
    }

    private static GoodsCommentsModel parserCommentCore(JSONObject obj) {
        GoodsCommentsModel goodsCommentsModel = new GoodsCommentsModel();
        goodsCommentsModel.setAllAppendCount(obj.optInt(AutogenGoodsDetailConstant.ALLAPPENDCOUNT));
        goodsCommentsModel.setAllBadCount(obj.optInt(AutogenGoodsDetailConstant.ALLBADCOUNT));
        goodsCommentsModel.setAllNormalCount(obj.optInt(AutogenGoodsDetailConstant.ALLNORMALCOUNT));
        goodsCommentsModel.setFeedGoodCount(obj.optInt(AutogenGoodsDetailConstant.FEEDGOODCOUNT));
        goodsCommentsModel.setIndex(obj.optInt(AutogenGoodsDetailConstant.INDEX));
        goodsCommentsModel.setTotal(obj.optInt(AutogenGoodsDetailConstant.TOTAL));
        JSONArray commentArr = obj.optJSONArray(AutogenGoodsDetailConstant.ITEMS);
        if (commentArr != null) {
            int len = commentArr.length();
            List<GoodsCommentDetail> goodsCommentDetaiList = new ArrayList<>();
            for (int i = 0; i < len; i++) {
                GoodsCommentDetail goodsCommentDetaiModel = new GoodsCommentDetail();
                JSONObject goodsCommentDetail = commentArr.optJSONObject(i);
                goodsCommentDetaiModel.setAnnoy(goodsCommentDetail.optInt(AutogenGoodsDetailConstant.ANNOY));
                goodsCommentDetaiModel.setBuyer(goodsCommentDetail.optString(AutogenGoodsDetailConstant.BUYER));
                goodsCommentDetaiModel.setCredit(goodsCommentDetail.optInt(AutogenGoodsDetailConstant.CREDIT));
                goodsCommentDetaiModel.setDeal(goodsCommentDetail.optString(AutogenGoodsDetailConstant.DEAL));
                goodsCommentDetaiModel.setRateId(goodsCommentDetail.optInt(AutogenGoodsDetailConstant.RATEID));
                goodsCommentDetaiModel.setText(goodsCommentDetail.optString("text"));
                goodsCommentDetaiModel.setType(goodsCommentDetail.optInt("type"));
                goodsCommentDetaiList.add(goodsCommentDetaiModel);
            }
            goodsCommentsModel.setGoodsCommentDetaiList(goodsCommentDetaiList);
        }
        return goodsCommentsModel;
    }
}
