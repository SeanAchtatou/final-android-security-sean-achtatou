package com.appbyme.android.service;

import com.appbyme.android.base.model.GoodsModel;
import java.util.List;

public interface NewestService {
    int getCacheDB();

    boolean getNewestByLocal();

    List<GoodsModel> getNewestList();

    List<GoodsModel> getNewestListByNet(int i, int i2, long j);

    List<GoodsModel> getNewestListByNet(int i, int i2, long j, boolean z);

    List<GoodsModel> getNewestListsByLocal(int i, int i2, long j);

    String getRefreshDate();

    void initCacheDB(int i, long j, boolean z);

    void saveCacheDB(int i);
}
