package com.appbyme.android.base.model;

import com.mobcent.forum.android.model.BaseModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AutogenBoardModel extends BaseModel {
    private static final long serialVersionUID = -2969121409059192494L;
    private Map<String, ArrayList<AutogenBoardCategory>> categoryMaps;

    public Map<String, ArrayList<AutogenBoardCategory>> getCategoryMaps() {
        if (this.categoryMaps == null) {
            this.categoryMaps = new HashMap();
        }
        return this.categoryMaps;
    }

    public void setCategoryMaps(Map<String, ArrayList<AutogenBoardCategory>> categoryMaps2) {
        this.categoryMaps = categoryMaps2;
    }
}
