package com.appbyme.android.base.db.constant;

public interface ContentDBConstant extends BaseCacheDBCache {
    public static final String CONTENT_FILE_NAME = "autogen_content_cache.prefs";
    public static final String LIST_ITEM_KEY = "_item_";
    public static final String LIST_PAGE_KEY = "_page_";
    public static final String LIST_REQ_KEY = "_req_";
    public static final String USER_KEY = "userId";
}
