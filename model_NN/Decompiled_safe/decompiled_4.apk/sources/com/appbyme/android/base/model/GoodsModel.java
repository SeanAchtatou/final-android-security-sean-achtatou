package com.appbyme.android.base.model;

import com.mobcent.forum.android.model.BaseModel;

public class GoodsModel extends BaseModel {
    private static final long serialVersionUID = -3536168411263277940L;
    private long boardId;
    private String boardName;
    private int essence;
    private int forumId;
    private int hits;
    private int hot;
    private long lastReplyDate;
    private String picPath;
    private String price;
    private float ratio;
    private int replies;
    private int status;
    private long tbkItemsTotal;
    private String title;
    private int top;
    private long topicId;
    private int type;
    private int uploadType;
    private int userId;
    private String userNickName;

    public long getTbkItemsTotal() {
        return this.tbkItemsTotal;
    }

    public void setTbkItemsTotal(long tbkItemsTotal2) {
        this.tbkItemsTotal = tbkItemsTotal2;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price2) {
        this.price = price2;
    }

    public float getRatio() {
        return this.ratio;
    }

    public void setRatio(float ratio2) {
        this.ratio = ratio2;
    }

    public long getBoardId() {
        return this.boardId;
    }

    public void setBoardId(long boardId2) {
        this.boardId = boardId2;
    }

    public String getBoardName() {
        return this.boardName;
    }

    public void setBoardName(String boardName2) {
        this.boardName = boardName2;
    }

    public int getEssence() {
        return this.essence;
    }

    public void setEssence(int essence2) {
        this.essence = essence2;
    }

    public int getForumId() {
        return this.forumId;
    }

    public void setForumId(int forumId2) {
        this.forumId = forumId2;
    }

    public int getHits() {
        return this.hits;
    }

    public void setHits(int hits2) {
        this.hits = hits2;
    }

    public int getHot() {
        return this.hot;
    }

    public void setHot(int hot2) {
        this.hot = hot2;
    }

    public long getLastReplyDate() {
        return this.lastReplyDate;
    }

    public void setLastReplyDate(long lastReplyDate2) {
        this.lastReplyDate = lastReplyDate2;
    }

    public String getPicPath() {
        return this.picPath;
    }

    public void setPicPath(String picPath2) {
        this.picPath = picPath2;
    }

    public int getReplies() {
        return this.replies;
    }

    public void setReplies(int replies2) {
        this.replies = replies2;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public int getTop() {
        return this.top;
    }

    public void setTop(int top2) {
        this.top = top2;
    }

    public long getTopicId() {
        return this.topicId;
    }

    public void setTopicId(long topicId2) {
        this.topicId = topicId2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public int getUploadType() {
        return this.uploadType;
    }

    public void setUploadType(int uploadType2) {
        this.uploadType = uploadType2;
    }

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId2) {
        this.userId = userId2;
    }

    public String getUserNickName() {
        return this.userNickName;
    }

    public void setUserNickName(String userNickName2) {
        this.userNickName = userNickName2;
    }

    public String toString() {
        return "HotModel [boardId=" + this.boardId + ", boardName=" + this.boardName + ", essence=" + this.essence + ", forumId=" + this.forumId + ", hits=" + this.hits + ", hot=" + this.hot + ", lastReplyDate=" + this.lastReplyDate + ", picPath=" + this.picPath + ", replies=" + this.replies + ", status=" + this.status + ", title=" + this.title + ", top=" + this.top + ", topicId=" + this.topicId + ", type=" + this.type + ", uploadType=" + this.uploadType + ", userId=" + this.userId + ", userNickName=" + this.userNickName + "]";
    }
}
