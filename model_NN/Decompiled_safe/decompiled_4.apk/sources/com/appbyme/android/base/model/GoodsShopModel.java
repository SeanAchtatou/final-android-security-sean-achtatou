package com.appbyme.android.base.model;

import com.mobcent.forum.android.model.BaseModel;

public class GoodsShopModel extends BaseModel {
    private static final long serialVersionUID = 7201806764721864206L;
    private int grade;
    private String local;
    private String shopName;
    private String shopPic;
    private ShopScore shopScore;

    public String getShopPic() {
        return this.shopPic;
    }

    public void setShopPic(String shopPic2) {
        this.shopPic = shopPic2;
    }

    public String getShopName() {
        return this.shopName;
    }

    public void setShopName(String shopName2) {
        this.shopName = shopName2;
    }

    public int getGrade() {
        return this.grade;
    }

    public void setGrade(int grade2) {
        this.grade = grade2;
    }

    public String getLocal() {
        return this.local;
    }

    public void setLocal(String local2) {
        this.local = local2;
    }

    public ShopScore getShopScore() {
        return this.shopScore;
    }

    public void setShopScore(ShopScore shopScore2) {
        this.shopScore = shopScore2;
    }
}
