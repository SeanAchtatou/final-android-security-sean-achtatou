package com.appbyme.android.base.model;

import com.mobcent.forum.android.model.BaseModel;

public class AutogenModuleModel extends BaseModel {
    private static final long serialVersionUID = 1;
    private int boardDisplay;
    private String boardListKey;
    private int detailDisplay;
    private boolean isSelected;
    private int listDisplay;
    private int listOrBoard;
    private String moduleIcon;
    private long moduleId;
    private String moduleName;
    private int moduleType;
    private int position;
    private int type;

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public long getModuleId() {
        return this.moduleId;
    }

    public void setModuleId(long moduleId2) {
        this.moduleId = moduleId2;
    }

    public String getModuleName() {
        return this.moduleName;
    }

    public void setModuleName(String moduleName2) {
        this.moduleName = moduleName2;
    }

    public int getListDisplay() {
        return this.listDisplay;
    }

    public void setListDisplay(int listDisplay2) {
        this.listDisplay = listDisplay2;
    }

    public int getBoardDisplay() {
        return this.boardDisplay;
    }

    public void setBoardDisplay(int boardDisplay2) {
        this.boardDisplay = boardDisplay2;
    }

    public int getDetailDisplay() {
        return this.detailDisplay;
    }

    public void setDetailDisplay(int detailDisplay2) {
        this.detailDisplay = detailDisplay2;
    }

    public int getListOrBoard() {
        return this.listOrBoard;
    }

    public void setListOrBoard(int listOrBoard2) {
        this.listOrBoard = listOrBoard2;
    }

    public String getModuleIcon() {
        return this.moduleIcon;
    }

    public void setModuleIcon(String moduleIcon2) {
        this.moduleIcon = moduleIcon2;
    }

    public String getBoardListKey() {
        return this.boardListKey;
    }

    public void setBoardListKey(String boardListKey2) {
        this.boardListKey = boardListKey2;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean isSelected2) {
        this.isSelected = isSelected2;
    }

    public void setPosition(int position2) {
        this.position = position2;
    }

    public void setModuleType(int moduleType2) {
        this.moduleType = moduleType2;
    }

    public int getModuleType() {
        return this.moduleType;
    }

    public int getPosition() {
        return this.position;
    }
}
