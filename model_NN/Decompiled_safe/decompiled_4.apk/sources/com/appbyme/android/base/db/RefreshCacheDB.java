package com.appbyme.android.base.db;

import android.content.Context;
import android.content.SharedPreferences;
import com.appbyme.android.base.db.constant.RefreshDBConstant;

public class RefreshCacheDB implements RefreshDBConstant {
    private static RefreshCacheDB refreshCacheDB = null;
    protected Context context;
    private SharedPreferences prefs = null;

    protected RefreshCacheDB(Context context2) {
        this.context = context2;
        this.prefs = context2.getSharedPreferences(RefreshDBConstant.REFRESH_FILE_NAME, 3);
    }

    public static RefreshCacheDB getInstance(Context context2) {
        if (refreshCacheDB == null) {
            refreshCacheDB = new RefreshCacheDB(context2);
        }
        return refreshCacheDB;
    }

    public void setRefreshTime(String type, long time) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putLong(String.valueOf(type) + RefreshDBConstant.LIST_TIME_KEY, time);
        editor.commit();
    }

    public long getRefreshTime(String type) {
        return this.prefs.getLong(String.valueOf(type) + RefreshDBConstant.LIST_TIME_KEY, 0);
    }
}
