package com.appbyme.android.base.model;

import com.mobcent.forum.android.model.BaseModel;

public class GoodsImgDetailModel extends BaseModel {
    private static final long serialVersionUID = -5815604705902871656L;
    private String created;
    private long id;
    private int position;
    private String url;

    public String getCreated() {
        return this.created;
    }

    public void setCreated(String created2) {
        this.created = created2;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public int getPosition() {
        return this.position;
    }

    public void setPosition(int position2) {
        this.position = position2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }
}
