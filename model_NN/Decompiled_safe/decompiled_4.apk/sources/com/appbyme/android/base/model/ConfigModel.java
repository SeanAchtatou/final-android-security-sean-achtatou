package com.appbyme.android.base.model;

import com.mobcent.forum.android.model.BaseModel;

public class ConfigModel extends BaseModel {
    private static final long serialVersionUID = 7879200132581894622L;
    private int formatId;
    private int isLocalApp;
    private String module0;
    private String module1;
    private String module2;
    private String module3;
    private String module4;

    public int getIsLocalApp() {
        return this.isLocalApp;
    }

    public void setIsLocalApp(int isLocalApp2) {
        this.isLocalApp = isLocalApp2;
    }

    public String getModule1() {
        return this.module1;
    }

    public void setModule1(String module12) {
        this.module1 = module12;
    }

    public String getModule2() {
        return this.module2;
    }

    public void setModule2(String module22) {
        this.module2 = module22;
    }

    public String getModule3() {
        return this.module3;
    }

    public void setModule3(String module32) {
        this.module3 = module32;
    }

    public String getModule4() {
        return this.module4;
    }

    public void setModule4(String module42) {
        this.module4 = module42;
    }

    public String getModule0() {
        return this.module0;
    }

    public void setModule0(String module02) {
        this.module0 = module02;
    }

    public int getFormatId() {
        return this.formatId;
    }

    public void setFormatId(int formatId2) {
        this.formatId = formatId2;
    }
}
