package com.appbyme.android.base.model;

import com.mobcent.forum.android.model.BaseModel;
import java.util.List;

public class GoodsCommentsModel extends BaseModel {
    private static final long serialVersionUID = 2840500962074856847L;
    private int allAppendCount;
    private int allBadCount;
    private int allNormalCount;
    private int feedGoodCount;
    private List<GoodsCommentDetail> goodsCommentDetaiList;
    private int index;
    private int page;
    private int total;

    public int getAllAppendCount() {
        return this.allAppendCount;
    }

    public void setAllAppendCount(int allAppendCount2) {
        this.allAppendCount = allAppendCount2;
    }

    public int getAllBadCount() {
        return this.allBadCount;
    }

    public void setAllBadCount(int allBadCount2) {
        this.allBadCount = allBadCount2;
    }

    public int getAllNormalCount() {
        return this.allNormalCount;
    }

    public void setAllNormalCount(int allNormalCount2) {
        this.allNormalCount = allNormalCount2;
    }

    public int getFeedGoodCount() {
        return this.feedGoodCount;
    }

    public void setFeedGoodCount(int feedGoodCount2) {
        this.feedGoodCount = feedGoodCount2;
    }

    public int getTotal() {
        return this.total;
    }

    public void setTotal(int total2) {
        this.total = total2;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int index2) {
        this.index = index2;
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(int page2) {
        this.page = page2;
    }

    public List<GoodsCommentDetail> getGoodsCommentDetaiList() {
        return this.goodsCommentDetaiList;
    }

    public void setGoodsCommentDetaiList(List<GoodsCommentDetail> goodsCommentDetaiList2) {
        this.goodsCommentDetaiList = goodsCommentDetaiList2;
    }
}
