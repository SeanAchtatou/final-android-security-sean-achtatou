package com.appbyme.android.base.model;

import com.mobcent.forum.android.model.BaseModel;
import com.mobcent.forum.android.model.BoardModel;
import java.util.ArrayList;

public class AutogenBoardCategory extends BaseModel {
    private static final long serialVersionUID = 2822672127497323789L;
    private ArrayList<BoardModel> BoardList;
    private int boardCategoryId;
    private String boardCategoryImgUrl;
    private String boardCategoryName;
    private int boardCategorySort;
    private int boardCategoryType;
    private int moduleId;

    public String getBoardCategoryImgUrl() {
        return this.boardCategoryImgUrl;
    }

    public void setBoardCategoryImgUrl(String boardCategoryImgUrl2) {
        this.boardCategoryImgUrl = boardCategoryImgUrl2;
    }

    public int getBoardCategoryId() {
        return this.boardCategoryId;
    }

    public void setBoardCategoryId(int boardCategoryId2) {
        this.boardCategoryId = boardCategoryId2;
    }

    public String getBoardCategoryName() {
        return this.boardCategoryName;
    }

    public void setBoardCategoryName(String boardCategoryName2) {
        this.boardCategoryName = boardCategoryName2;
    }

    public int getBoardCategorySort() {
        return this.boardCategorySort;
    }

    public void setBoardCategorySort(int boardCategorySort2) {
        this.boardCategorySort = boardCategorySort2;
    }

    public int getBoardCategoryType() {
        return this.boardCategoryType;
    }

    public void setBoardCategoryType(int boardCategoryType2) {
        this.boardCategoryType = boardCategoryType2;
    }

    public int getModuleId() {
        return this.moduleId;
    }

    public void setModuleId(int moduleId2) {
        this.moduleId = moduleId2;
    }

    public ArrayList<BoardModel> getBoardList() {
        return this.BoardList;
    }

    public void setBoardList(ArrayList<BoardModel> boardList) {
        this.BoardList = boardList;
    }
}
