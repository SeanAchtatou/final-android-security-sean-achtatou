package com.appbyme.android.base.db.constant;

public interface RefreshDBConstant extends BaseCacheDBCache {
    public static final String LIST_TIME_KEY = "_date_";
    public static final String REFRESH_FILE_NAME = "autogen_refresh_cache.prefs";
}
