package com.appbyme.android.base.model;

import com.mobcent.forum.android.model.BaseModel;

public class ShopScore extends BaseModel {
    private static final long serialVersionUID = 8620118569438435649L;
    private String deliveryScore;
    private String itemScore;
    private String serviceScore;

    public String getDeliveryScore() {
        return this.deliveryScore;
    }

    public void setDeliveryScore(String deliveryScore2) {
        this.deliveryScore = deliveryScore2;
    }

    public String getItemScore() {
        return this.itemScore;
    }

    public void setItemScore(String itemScore2) {
        this.itemScore = itemScore2;
    }

    public String getServiceScore() {
        return this.serviceScore;
    }

    public void setServiceScore(String serviceScore2) {
        this.serviceScore = serviceScore2;
    }
}
