package com.appbyme.android.base.db.constant;

public interface BaseCacheDBCache {
    public static final String CLASSIFY_TYPE = "classify";
    public static final String GOODSCOMMENT_TYPE = "goodscomment";
    public static final String HOT_TYPE = "hot";
    public static final String NEWEST_TYPE = "newest";
}
