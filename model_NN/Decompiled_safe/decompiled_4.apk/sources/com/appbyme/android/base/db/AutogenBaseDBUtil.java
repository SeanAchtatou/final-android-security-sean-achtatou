package com.appbyme.android.base.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class AutogenBaseDBUtil {
    private String TAG = "AutogenBaseDBUtil";
    protected SQLiteOpenHelper autogenDBOpenHelper;
    protected SQLiteDatabase readableDatabase;
    protected SQLiteDatabase writableDatabase;

    /* access modifiers changed from: protected */
    public abstract void initDataInConStructors(Context context);

    /* access modifiers changed from: protected */
    public boolean isRowExisted(SQLiteDatabase database, String table, String column, long id) {
        Cursor c = null;
        try {
            c = database.query(table, null, String.valueOf(column) + "=" + id, null, null, null, null);
            if (c.getCount() > 0) {
                return true;
            }
            if (c != null) {
                c.close();
            }
            return false;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean isRowExisted(SQLiteDatabase database, String table, String column, String id) {
        Cursor c = null;
        try {
            c = database.query(table, null, String.valueOf(column) + "='" + id + "'", null, null, null, null);
            if (c.getCount() > 0) {
                return true;
            }
            if (c != null) {
                c.close();
            }
            return false;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean isRowExisted(SQLiteDatabase database, String table, String selection, String[] selectArgs) {
        Cursor c = null;
        try {
            c = database.query(table, null, selection, selectArgs, null, null, null);
            if (c.getCount() > 0) {
                if (c != null) {
                    c.close();
                }
                return true;
            }
            if (c != null) {
                c.close();
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.close();
            }
            return false;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public boolean removeAllEntries(String table) {
        try {
            openWriteableDB();
            this.writableDatabase.delete(table, null, null);
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public boolean removeEntries(SQLiteDatabase database, String table, long boardId) {
        try {
            openWriteableDB();
            if (this.writableDatabase.delete(table, " board_id = ? ", new String[]{new StringBuilder(String.valueOf(boardId)).toString()}) > 0) {
                closeWriteableDB();
                return true;
            }
            closeWriteableDB();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public boolean tabbleIsExist(SQLiteDatabase database, String table) {
        boolean result = false;
        if (table == null) {
            return false;
        }
        Cursor c = null;
        try {
            Cursor c2 = database.rawQuery("select count(*) as c from Sqlite_master  where type ='table' and name ='" + table.trim() + "' ", null);
            if (c2.moveToNext() && c2.getInt(0) > 0) {
                result = true;
            }
            if (c2 != null) {
                c2.close();
            }
            return result;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            return false;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public SQLiteDatabase openWriteableDB() {
        if (this.writableDatabase == null || !this.writableDatabase.isOpen()) {
            this.writableDatabase = this.autogenDBOpenHelper.getWritableDatabase();
        }
        return this.writableDatabase;
    }

    /* access modifiers changed from: protected */
    public SQLiteDatabase openReadableDB() {
        if (this.readableDatabase == null || !this.readableDatabase.isOpen()) {
            this.readableDatabase = this.autogenDBOpenHelper.getReadableDatabase();
        }
        return this.readableDatabase;
    }

    /* access modifiers changed from: protected */
    public void closeWriteableDB() {
        try {
            this.autogenDBOpenHelper.close();
        } catch (Exception e) {
        }
        if (this.writableDatabase != null && this.writableDatabase.isOpen()) {
            try {
                this.writableDatabase.close();
                this.writableDatabase = null;
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void closeReadableDB() {
        try {
            this.autogenDBOpenHelper.close();
        } catch (Exception e) {
        }
        if (this.readableDatabase != null && this.readableDatabase.isOpen()) {
            try {
                this.readableDatabase.close();
                this.readableDatabase = null;
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public String getSelectionArgs(String selection, int page) {
        return String.valueOf("") + selection + "<=" + page;
    }
}
