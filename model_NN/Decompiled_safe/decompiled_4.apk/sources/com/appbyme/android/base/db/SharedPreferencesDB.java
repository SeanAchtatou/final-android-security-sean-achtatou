package com.appbyme.android.base.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import com.appbyme.android.base.db.constant.SharedPreferencesDBConstant;

public class SharedPreferencesDB implements SharedPreferencesDBConstant {
    private static SharedPreferencesDB sharedPreferencesDB = null;
    private Context context;
    private SharedPreferences prefs = null;

    private SharedPreferencesDB(Context cxt) {
        this.context = cxt;
        try {
            this.prefs = cxt.createPackageContext(cxt.getPackageName(), 2).getSharedPreferences(SharedPreferencesDBConstant.PREFS_FILE, 3);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static SharedPreferencesDB getInstance(Context context2) {
        if (sharedPreferencesDB == null) {
            sharedPreferencesDB = new SharedPreferencesDB(context2);
        }
        return sharedPreferencesDB;
    }

    public static SharedPreferencesDB newInstance(Context context2) {
        sharedPreferencesDB = new SharedPreferencesDB(context2);
        return sharedPreferencesDB;
    }

    public int getConfigVersion() {
        return this.prefs.getInt(SharedPreferencesDBConstant.CONFIG_VERSION, 0);
    }

    public void setConfigVersion(int version) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putInt(SharedPreferencesDBConstant.CONFIG_VERSION, version);
        editor.commit();
    }
}
