package com.appbyme.android.base.model;

import com.mobcent.forum.android.model.BaseModel;

public class GoodsCommentDetail extends BaseModel {
    private static final long serialVersionUID = -9110794833463894353L;
    private int annoy;
    private String buyer;
    private int credit;
    private String date;
    private String deal;
    private int rateId;
    private String text;
    private int type;

    public int getAnnoy() {
        return this.annoy;
    }

    public void setAnnoy(int annoy2) {
        this.annoy = annoy2;
    }

    public String getBuyer() {
        return this.buyer;
    }

    public void setBuyer(String buyer2) {
        this.buyer = buyer2;
    }

    public int getCredit() {
        return this.credit;
    }

    public void setCredit(int credit2) {
        this.credit = credit2;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date2) {
        this.date = date2;
    }

    public String getDeal() {
        return this.deal;
    }

    public void setDeal(String deal2) {
        this.deal = deal2;
    }

    public int getRateId() {
        return this.rateId;
    }

    public void setRateId(int rateId2) {
        this.rateId = rateId2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }
}
