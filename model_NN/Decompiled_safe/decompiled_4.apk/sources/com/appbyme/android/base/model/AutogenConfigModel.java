package com.appbyme.android.base.model;

import com.mobcent.forum.android.model.BaseModel;
import java.util.ArrayList;
import java.util.List;

public class AutogenConfigModel extends BaseModel {
    private static final long serialVersionUID = 7535503643007231296L;
    private int forumId;
    private boolean isUpdate;
    private boolean isWeather = false;
    private List<AutogenModuleModel> moduleList;
    private List<Integer> searchTypes;
    private int style;
    private int version;

    public int getForumId() {
        return this.forumId;
    }

    public void setForumId(int forumId2) {
        this.forumId = forumId2;
    }

    public boolean isUpdate() {
        return this.isUpdate;
    }

    public void setUpdate(boolean isUpdate2) {
        this.isUpdate = isUpdate2;
    }

    public List<AutogenModuleModel> getModuleList() {
        return this.moduleList;
    }

    public void setModuleList(List<AutogenModuleModel> moduleList2) {
        this.moduleList = moduleList2;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version2) {
        this.version = version2;
    }

    public int getStyle() {
        return this.style;
    }

    public void setStyle(int style2) {
        this.style = style2;
    }

    public List<Integer> getSearchTypes() {
        if (this.searchTypes == null) {
            this.searchTypes = new ArrayList();
        }
        return this.searchTypes;
    }

    public void setSearchTypes(List<Integer> searchTypes2) {
        this.searchTypes = searchTypes2;
    }

    public boolean isWeather() {
        return this.isWeather;
    }

    public void setWeather(boolean isWeather2) {
        this.isWeather = isWeather2;
    }

    public String toString() {
        return "AutogenConfigModel [moduleList=" + this.moduleList + ", version=" + this.version + ", style=" + this.style + ", isUpdate=" + this.isUpdate + ", forumId=" + this.forumId + ", searchTypes=" + this.searchTypes + ", isWeather=" + this.isWeather + "]";
    }
}
