package com.appbyme.android.base.db.constant;

public interface SharedPreferencesDBConstant {
    public static final String CONFIG_VERSION = "config_version";
    public static final String PREFS_FILE = "mc_info.prefs";
}
