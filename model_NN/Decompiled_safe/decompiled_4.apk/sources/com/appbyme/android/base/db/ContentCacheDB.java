package com.appbyme.android.base.db;

import android.content.Context;
import android.content.SharedPreferences;
import com.appbyme.android.base.db.constant.ContentDBConstant;

public class ContentCacheDB implements ContentDBConstant {
    private static ContentCacheDB contentCacheDB = null;
    protected Context context;
    private SharedPreferences prefs = null;

    protected ContentCacheDB(Context context2) {
        this.context = context2;
        this.prefs = context2.getSharedPreferences(ContentDBConstant.CONTENT_FILE_NAME, 3);
    }

    public static ContentCacheDB getInstance(Context context2) {
        if (contentCacheDB == null) {
            contentCacheDB = new ContentCacheDB(context2);
        }
        return contentCacheDB;
    }

    public int getListPosition(String type) {
        return this.prefs.getInt(String.valueOf(type) + ContentDBConstant.LIST_ITEM_KEY, 0);
    }

    public void setListLocal(String type, boolean isLocal) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putBoolean(String.valueOf(type) + ContentDBConstant.LIST_REQ_KEY, isLocal);
        editor.commit();
    }

    public boolean getListReq(String type) {
        return this.prefs.getBoolean(String.valueOf(type) + ContentDBConstant.LIST_REQ_KEY, true);
    }

    public int getListPage(String type) {
        return this.prefs.getInt(String.valueOf(type) + ContentDBConstant.LIST_PAGE_KEY, 0);
    }

    public void setListPage(String type, int page) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putInt(String.valueOf(type) + ContentDBConstant.LIST_PAGE_KEY, page);
        editor.commit();
    }

    public void setListItem(String type, int position) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putInt(String.valueOf(type) + ContentDBConstant.LIST_ITEM_KEY, position);
        editor.commit();
    }
}
