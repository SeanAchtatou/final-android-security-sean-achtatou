package com.appbyme.android.base.model;

import com.mobcent.forum.android.model.BaseModel;
import java.util.List;

public class GoodsDetailModel extends BaseModel {
    private static final long serialVersionUID = -5244884741696212598L;
    private long boardId;
    private String clickUrl;
    private int favorsNum;
    private GoodsCommentsModel goodsCommentsModel;
    private List<GoodsImgDetailModel> goodsImgDetailModelList;
    private GoodsShopModel goodsShopModel;
    private String goodsThumb;
    private String goodsTitle;
    private boolean isFavors;
    private long numIid;
    private String price;
    private int saleNum;
    private String tbDetailUrl;

    public String getClickUrl() {
        return this.clickUrl;
    }

    public void setClickUrl(String clickUrl2) {
        this.clickUrl = clickUrl2;
    }

    public long getNumIid() {
        return this.numIid;
    }

    public void setNumIid(long numIid2) {
        this.numIid = numIid2;
    }

    public List<GoodsImgDetailModel> getGoodsImgDetailModelList() {
        return this.goodsImgDetailModelList;
    }

    public void setGoodsImgDetailModelList(List<GoodsImgDetailModel> goodsImgDetailModelList2) {
        this.goodsImgDetailModelList = goodsImgDetailModelList2;
    }

    public GoodsCommentsModel getGoodsCommentsModel() {
        return this.goodsCommentsModel;
    }

    public void setGoodsCommentsModel(GoodsCommentsModel goodsCommentsModel2) {
        this.goodsCommentsModel = goodsCommentsModel2;
    }

    public long getBoardId() {
        return this.boardId;
    }

    public void setBoardId(long boardId2) {
        this.boardId = boardId2;
    }

    public boolean isFavors() {
        return this.isFavors;
    }

    public void setFavors(boolean isFavors2) {
        this.isFavors = isFavors2;
    }

    public int getFavorsNum() {
        return this.favorsNum;
    }

    public String getGoodsThumb() {
        return this.goodsThumb;
    }

    public void setGoodsThumb(String goodsThumb2) {
        this.goodsThumb = goodsThumb2;
    }

    public void setFavorsNum(int favorsNum2) {
        this.favorsNum = favorsNum2;
    }

    public int getSaleNum() {
        return this.saleNum;
    }

    public void setSaleNum(int saleNum2) {
        this.saleNum = saleNum2;
    }

    public String getGoodsTitle() {
        return this.goodsTitle;
    }

    public void setGoodsTitle(String goodsTitle2) {
        this.goodsTitle = goodsTitle2;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price2) {
        this.price = price2;
    }

    public String getTbDetailUrl() {
        return this.tbDetailUrl;
    }

    public void setTbDetailUrl(String tbDetailUrl2) {
        this.tbDetailUrl = tbDetailUrl2;
    }

    public GoodsShopModel getGoodsShopModel() {
        return this.goodsShopModel;
    }

    public void setGoodsShopModel(GoodsShopModel goodsShopModel2) {
        this.goodsShopModel = goodsShopModel2;
    }
}
