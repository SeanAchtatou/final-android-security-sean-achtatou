package com.appbyme.android.api.constant;

public interface HotApiConstant {
    public static final String BASEURL = "base_url";
    public static final int PAGECOUNT = 15;
    public static final String PRICE = "price";
    public static final String REPLIES = "replies";
    public static final String TBK_ITEMS = "tbk_items";
    public static final String TBK_ITEMS_TOTAL = "tbk_items_total";
}
