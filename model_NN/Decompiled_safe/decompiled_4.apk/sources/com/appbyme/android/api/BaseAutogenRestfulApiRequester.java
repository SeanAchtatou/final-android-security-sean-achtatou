package com.appbyme.android.api;

import android.content.Context;
import com.appbyme.android.api.constant.InfoApiConstant;
import com.mobcent.forum.android.api.BaseRestfulApiRequester;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import java.util.HashMap;

public class BaseAutogenRestfulApiRequester extends BaseRestfulApiRequester implements InfoApiConstant {
    public static String AUTOGEN_REQUEST_DOMAIN_URL = "";
    protected static String BOARD_SOCKET_TIME_OUT = "20000";
    protected static String BOARD_TIME_OUT = "5000";
    protected static String LIST_SOCKET_TIME_OUT = "20000";
    protected static String LIST_TIME_OUT = "5000";

    public static void updateForumUrl() {
    }

    public static String autogenDoPostRequest(String urlString, HashMap<String, String> params, Context context, int type) {
        params.put("baikeType", new StringBuilder(String.valueOf(type)).toString());
        return doPostRequest(urlString, params, context);
    }

    public static String autogenDoPostRequest(String urlString, HashMap<String, String> params, Context context) {
        params.put("forumId", new StringBuilder(String.valueOf(SharedPreferencesDB.getInstance(context).getForumId())).toString());
        return doPostRequest(urlString, params, context);
    }
}
