package com.appbyme.android.api;

import android.content.Context;
import com.appbyme.android.api.constant.GoodsApiConstant;
import com.mobcent.forum.android.api.util.HttpClientUtil;
import com.mobcent.forum.android.constant.PostsApiConstant;
import java.util.HashMap;

public class GoodsDetailRestfulApiRequester extends BaseAutogenRestfulApiRequester implements GoodsApiConstant, PostsApiConstant {
    public static String goodsFavor(Context context, long topicId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        params.put(HttpClientUtil.SET_CONNECTION_TIMEOUT_STR, BOARD_TIME_OUT);
        params.put(HttpClientUtil.SET_SOCKET_TIMEOUT_STR, BOARD_SOCKET_TIME_OUT);
        return doPostRequest("ecapi/item/collect", params, context);
    }

    public static String getGoodsDetail(Context context, long topicId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        params.put(HttpClientUtil.SET_CONNECTION_TIMEOUT_STR, BOARD_TIME_OUT);
        params.put(HttpClientUtil.SET_SOCKET_TIMEOUT_STR, BOARD_SOCKET_TIME_OUT);
        return doPostRequest("ecapi/item", params, context);
    }

    public static String getGoodsCommentDetail(Context context, long numIid, int page) {
        HashMap<String, String> params = new HashMap<>();
        params.put(GoodsApiConstant.NUM_IID, new StringBuilder(String.valueOf(numIid)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", "15");
        params.put(HttpClientUtil.SET_CONNECTION_TIMEOUT_STR, BOARD_TIME_OUT);
        params.put(HttpClientUtil.SET_SOCKET_TIMEOUT_STR, BOARD_SOCKET_TIME_OUT);
        return doPostRequest("ecapi/item/getTBComments", params, context);
    }
}
