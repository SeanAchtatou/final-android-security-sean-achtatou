package com.appbyme.android.api;

import android.content.Context;
import com.appbyme.android.api.constant.NewestApiConstant;
import com.mobcent.forum.android.api.util.HttpClientUtil;
import com.mobcent.forum.android.constant.PostsApiConstant;
import java.util.HashMap;

public class NewestRestfulApiRequester extends BaseAutogenRestfulApiRequester implements PostsApiConstant, NewestApiConstant {
    public static String getNewestList(Context context, int Page) {
        HashMap<String, String> params = new HashMap<>();
        params.put("page", new StringBuilder(String.valueOf(Page)).toString());
        params.put("pageSize", "15");
        params.put(HttpClientUtil.SET_CONNECTION_TIMEOUT_STR, BOARD_TIME_OUT);
        params.put(HttpClientUtil.SET_SOCKET_TIMEOUT_STR, BOARD_SOCKET_TIME_OUT);
        return doPostRequest("ecapi/items/newest", params, context);
    }
}
