package com.appbyme.android.api;

import android.content.Context;
import com.appbyme.android.api.constant.AutogenConfigApiConstant;
import com.mobcent.forum.android.api.util.HttpClientUtil;
import java.util.HashMap;

public class AutogenConfigRestfulApiRequester extends BaseAutogenRestfulApiRequester implements AutogenConfigApiConstant {
    public static String getModuleInfo(Context context, int version) {
        HashMap<String, String> params = new HashMap<>();
        params.put("version", new StringBuilder(String.valueOf(version)).toString());
        params.put(HttpClientUtil.SET_CONNECTION_TIMEOUT_STR, BOARD_TIME_OUT);
        params.put(HttpClientUtil.SET_SOCKET_TIMEOUT_STR, BOARD_SOCKET_TIME_OUT);
        return doPostRequest("config/configList.do", params, context);
    }

    public static String getBoardInfo(Context context) {
        HashMap<String, String> params = new HashMap<>();
        params.put(HttpClientUtil.SET_CONNECTION_TIMEOUT_STR, BOARD_TIME_OUT);
        params.put(HttpClientUtil.SET_SOCKET_TIMEOUT_STR, BOARD_SOCKET_TIME_OUT);
        return doPostRequest("config/ecBoardList.do", params, context);
    }
}
