package com.appbyme.android.api;

import android.content.Context;
import com.mobcent.forum.android.api.util.HttpClientUtil;
import com.mobcent.forum.android.constant.PostsApiConstant;
import java.util.HashMap;

public class PersonalRestfulApiRequester extends BaseAutogenRestfulApiRequester implements PostsApiConstant {
    public static String goodsFavor(Context context, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        params.put(HttpClientUtil.SET_CONNECTION_TIMEOUT_STR, BOARD_TIME_OUT);
        params.put(HttpClientUtil.SET_SOCKET_TIMEOUT_STR, BOARD_SOCKET_TIME_OUT);
        return doPostRequest("ecapi/item/getSelfCollects", params, context);
    }

    public static String goodsComment(Context context, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        params.put(HttpClientUtil.SET_CONNECTION_TIMEOUT_STR, BOARD_TIME_OUT);
        params.put(HttpClientUtil.SET_SOCKET_TIMEOUT_STR, BOARD_SOCKET_TIME_OUT);
        return doPostRequest("ecapi/item/getSelfComments", params, context);
    }
}
