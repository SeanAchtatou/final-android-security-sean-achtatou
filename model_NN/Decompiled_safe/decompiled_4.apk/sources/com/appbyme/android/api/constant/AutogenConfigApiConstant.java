package com.appbyme.android.api.constant;

public interface AutogenConfigApiConstant {
    public static final String BASE_URL = "base_url";
    public static final String BOARD_ACTEGORIES = "board_categories";
    public static final String BOARD_CATEGORY_ID = "board_category_id";
    public static final String BOARD_CATEGORY_IMG_URLC = "board_category_img_url";
    public static final String BOARD_CATEGORY_NAME = "board_category_name";
    public static final String BOARD_CATEGORY_SORT = "board_category_sort";
    public static final String BOARD_CATEGORY_TYPE = "board_category_type";
    public static final String BOARD_DISPLAY = "board_display";
    public static final String BOARD_LIST = "board_list";
    public static final String CATEGORY_ID = "category_id";
    public static final int CLASSIFY_ID = 16;
    public static final int CLASSIFY_MARKING = 10003;
    public static final String CONFIG_LIST = "config_list";
    public static final String DATA = "data";
    public static final String DETAIL_DISPLAY = "detail_display";
    public static final String IMG_URL = "img_url";
    public static final String IS_UPDATE = "is_update";
    public static final String LIST = "list";
    public static final String LIST_DISPLAY = "list_display";
    public static final String LIST_OR_BOARD = "list_or_board";
    public static final String MODULE_ICON = "module_icon";
    public static final String MODULE_ID = "module_id";
    public static final String MODULE_NAME = "module_name";
    public static final String MODULE_TYPE = "module_type";
    public static final String POSITION = "position";
    public static final String PUBLISH = "publish";
    public static final String REQ_VERSION = "version";
    public static final String RES_FORUM_ID = "forum_id";
    public static final String RES_VERSION = "version";
    public static final String STYLE = "style";
    public static final String TYPE = "type";
    public static final String WEATHER = "weather";
}
