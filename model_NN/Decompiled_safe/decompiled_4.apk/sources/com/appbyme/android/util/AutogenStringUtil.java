package com.appbyme.android.util;

import com.mobcent.ad.android.constant.AdApiConstant;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class AutogenStringUtil {
    public static List<Integer> getStr(String str) {
        List<Integer> integers = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(str, AdApiConstant.RES_SPLIT_COMMA);
        while (st.hasMoreTokens()) {
            integers.add(Integer.valueOf(Integer.parseInt(st.nextToken())));
        }
        return integers;
    }
}
