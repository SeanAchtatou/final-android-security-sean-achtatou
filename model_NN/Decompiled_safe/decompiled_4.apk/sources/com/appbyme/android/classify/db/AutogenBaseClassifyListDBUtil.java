package com.appbyme.android.classify.db;

import android.content.Context;
import com.appbyme.android.base.db.AutogenBaseDBUtil;
import com.appbyme.android.classify.db.constant.AutogenBaseClassifyListDBConstant;

public class AutogenBaseClassifyListDBUtil extends AutogenBaseDBUtil implements AutogenBaseClassifyListDBConstant {
    protected Context context;

    public AutogenBaseClassifyListDBUtil(Context context2) {
        initDataInConStructors(context2);
    }

    /* access modifiers changed from: protected */
    public void initDataInConStructors(Context context2) {
        this.autogenDBOpenHelper = new AutogenClassifyListDBOpenHelper(context2, AutogenBaseClassifyListDBConstant.CLASSIFY_LIST_DATABASE_NAME, 1);
    }
}
