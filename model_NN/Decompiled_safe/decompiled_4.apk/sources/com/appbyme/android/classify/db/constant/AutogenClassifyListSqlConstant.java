package com.appbyme.android.classify.db.constant;

public interface AutogenClassifyListSqlConstant extends AutogenClassifyListTableConstant {
    public static final String CLASSIFY_LIST_SUM = "SELECT COUNT ( * )  FROM t_classify_list WHERE board_id = ?";
    public static final String CLASSIFY_LIST_WHERE = "page = ? AND board_id = ?";
    public static final String CLASSIFY_TOTAL_COUNT_WHERE = "board_id = ?";
    public static final String NEW_TABLE_CLASSIFY_LIST = "create table if not exists t_classify_list ( id integer primary key autoincrement,board_id INTEGER,page INTEGER,page_toal_num INTEGER,json_str TEXT);";
}
