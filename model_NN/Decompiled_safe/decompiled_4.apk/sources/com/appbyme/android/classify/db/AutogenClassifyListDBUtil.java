package com.appbyme.android.classify.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import com.appbyme.android.classify.db.constant.AutogenClassifyListTableConstant;

public class AutogenClassifyListDBUtil extends AutogenBaseClassifyListDBUtil implements AutogenClassifyListTableConstant {
    private static AutogenClassifyListDBUtil autogenClassifyListDBUtil;

    public AutogenClassifyListDBUtil(Context context) {
        super(context);
    }

    public static AutogenClassifyListDBUtil getInstance(Context context) {
        if (autogenClassifyListDBUtil == null) {
            autogenClassifyListDBUtil = new AutogenClassifyListDBUtil(context);
        }
        return autogenClassifyListDBUtil;
    }

    public synchronized boolean saveClassifyList(int page, long classifyBoardId, int totalNum, String jsonStr) {
        boolean z;
        try {
            ContentValues values = new ContentValues();
            values.put("page", Integer.valueOf(page));
            values.put("board_id", Long.valueOf(classifyBoardId));
            values.put("page_toal_num", Integer.valueOf(totalNum));
            values.put("json_str", jsonStr);
            openWriteableDB();
            this.writableDatabase.insertOrThrow(AutogenClassifyListTableConstant.T_CLASSIFY_LIST, null, values);
            closeWriteableDB();
            z = true;
        } catch (SQLException e) {
            e.printStackTrace();
            closeWriteableDB();
            z = false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
        return z;
    }

    public synchronized boolean deleteClassifyBoard(long boardId) {
        return removeEntries(this.writableDatabase, AutogenClassifyListTableConstant.T_CLASSIFY_LIST, boardId);
    }

    public synchronized String getClassifyList(int page, long classifyBoardId) {
        String str;
        Cursor c = null;
        try {
            openReadableDB();
            Cursor c2 = this.readableDatabase.rawQuery("select json_str from t_classify_list where page=" + page + " and board_id=" + classifyBoardId, null);
            if (c2.moveToFirst()) {
                String s = c2.getString(c2.getColumnIndex("json_str"));
                if (c2 != null) {
                    c2.close();
                }
                closeReadableDB();
                str = s;
            } else {
                if (c2 != null) {
                    c2.close();
                }
                closeReadableDB();
                str = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            closeReadableDB();
            throw th;
        }
        return str;
    }

    public synchronized void updataClassifyList(int page, int totalNum, long boardId, String jsonStr) {
        openWriteableDB();
        this.writableDatabase.beginTransaction();
        boolean isRollBack = false;
        try {
            if (isRowExisted(this.writableDatabase, AutogenClassifyListTableConstant.T_CLASSIFY_LIST, "board_id", boardId)) {
                if (this.writableDatabase.delete(AutogenClassifyListTableConstant.T_CLASSIFY_LIST, " board_id = ? ", new String[]{new StringBuilder(String.valueOf(boardId)).toString()}) == 0) {
                    isRollBack = true;
                }
            }
            ContentValues values = new ContentValues();
            values.put("page", Integer.valueOf(page));
            values.put("board_id", Long.valueOf(boardId));
            values.put("page_toal_num", Integer.valueOf(totalNum));
            values.put("json_str", jsonStr);
            if (this.writableDatabase.insertOrThrow(AutogenClassifyListTableConstant.T_CLASSIFY_LIST, null, values) == -1) {
                isRollBack = true;
            }
            if (!isRollBack) {
                this.writableDatabase.setTransactionSuccessful();
            }
            this.writableDatabase.endTransaction();
            closeWriteableDB();
        } catch (Exception e) {
            e.printStackTrace();
            if (1 == 0) {
                this.writableDatabase.setTransactionSuccessful();
            }
            this.writableDatabase.endTransaction();
            closeWriteableDB();
        } catch (Throwable th) {
            if (0 == 0) {
                this.writableDatabase.setTransactionSuccessful();
            }
            this.writableDatabase.endTransaction();
            closeWriteableDB();
            throw th;
        }
    }
}
