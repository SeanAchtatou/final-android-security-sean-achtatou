package com.appbyme.android.classify.db.constant;

public interface AutogenBaseClassifyListDBConstant {
    public static final int CLASSIFY_DB_VERSION = 1;
    public static final String CLASSIFY_LIST_DATABASE_NAME = "mc_autogen_classify_list.db";
}
