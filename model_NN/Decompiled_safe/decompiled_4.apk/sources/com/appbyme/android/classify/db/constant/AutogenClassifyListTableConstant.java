package com.appbyme.android.classify.db.constant;

public interface AutogenClassifyListTableConstant {
    public static final String CLASSIFY_BOARD_ID = "board_id";
    public static final String CLASSIFY_LIST_ID = "id";
    public static final String CLASSIFY_LIST_JSON_STR = "json_str";
    public static final String CLASSIFY_LIST_PAGE = "page";
    public static final String CLASSIFY_LIST_PAGE_TOTAL_NUM = "page_toal_num";
    public static final int SATABASES_VERSION = 1;
    public static final String T_CLASSIFY_LIST = "t_classify_list";
}
