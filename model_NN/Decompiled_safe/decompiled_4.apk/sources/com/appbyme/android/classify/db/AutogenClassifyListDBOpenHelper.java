package com.appbyme.android.classify.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.appbyme.android.classify.db.constant.AutogenClassifyListSqlConstant;
import com.appbyme.android.classify.db.constant.AutogenClassifyListTableConstant;

public class AutogenClassifyListDBOpenHelper extends SQLiteOpenHelper {
    public AutogenClassifyListDBOpenHelper(Context context, String databaseName, int version) {
        super(context, databaseName, (SQLiteDatabase.CursorFactory) null, version);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(AutogenClassifyListSqlConstant.NEW_TABLE_CLASSIFY_LIST);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.delete(AutogenClassifyListTableConstant.T_CLASSIFY_LIST, null, null);
        onCreate(db);
    }
}
