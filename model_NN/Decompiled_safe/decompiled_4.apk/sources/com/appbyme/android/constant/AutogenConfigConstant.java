package com.appbyme.android.constant;

import java.io.File;

public interface AutogenConfigConstant {
    public static final String BOARD_CONFIG_NAME = "board.json";
    public static final String BOARD_CONFIG_PATH = (String.valueOf(File.separator) + "config" + File.separator);
    public static final String CONFIG_FORMAT = "formatId";
    public static final String CONFIG_PATH = "config/config.json";
    public static final String IS_LOCAL_APP = "isLocalApp";
    public static final String MODULE0 = "m0";
    public static final String MODULE1 = "m1";
    public static final String MODULE2 = "m2";
    public static final String MODULE3 = "m3";
    public static final String MODULE4 = "m4";
    public static final String MODULE_CONFIG_NAME = "module.json";
    public static final String MODULE_CONFIG_PATH = (String.valueOf(File.separator) + "config" + File.separator);
    public static final String MODULE_PATH = "module.json";
}
