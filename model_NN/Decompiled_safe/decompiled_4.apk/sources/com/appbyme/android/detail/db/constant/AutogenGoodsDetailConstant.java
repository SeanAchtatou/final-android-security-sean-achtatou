package com.appbyme.android.detail.db.constant;

public interface AutogenGoodsDetailConstant {
    public static final String ALLAPPENDCOUNT = "allAppendCount";
    public static final String ALLBADCOUNT = "allBadCount";
    public static final String ALLNORMALCOUNT = "allNormalCount";
    public static final String ANNOY = "annoy";
    public static final String BUYER = "buyer";
    public static final String CLICKURL = "clickUrl";
    public static final String COMMENTS = "comments";
    public static final String CREATED = "created";
    public static final String CREDIT = "credit";
    public static final String DATE = "date";
    public static final String DEAL = "deal";
    public static final String DELIVERYSCORE = "deliveryScore";
    public static final String DETAILURL = "detailUrl";
    public static final String FAVORSNUM = "favorsNum";
    public static final String FEEDGOODCOUNT = "feedGoodCount";
    public static final String ID = "id";
    public static final String INDEX = "index";
    public static final String ISFAVORS = "isFavors";
    public static final String ITEMIMGS = "itemImgs";
    public static final String ITEMLOCATION = "itemLocation";
    public static final String ITEMS = "items";
    public static final String ITEMSCORE = "itemScore";
    public static final String MAINPICTURE = "mainPicture";
    public static final String NETWORKACCESSURL = "networkAccessUrl";
    public static final String NICK = "nick";
    public static final String NUMIID = "numIid";
    public static final String PICPATH = "picPath";
    public static final String POSTIONT = "position";
    public static final String PRICE = "price";
    public static final String RATEID = "rateId";
    public static final String SELLERCREDITSCORE = "sellerCreditScore";
    public static final String SERVICESCORE = "serviceScore";
    public static final String SHOPSCORE = "shopScore";
    public static final String SUFFIXPATH = "suffixPath";
    public static final String TBK_COMMENTS = "tbk_comments";
    public static final String TBK_ITEM = "tbk_item";
    public static final String TBK_ITEM_ATTR = "tbk_item_attr";
    public static final String TBK_SHOP = "tbk_shop";
    public static final String TB_HOST = "http://logo.taobao.com/shop-logo";
    public static final String TEXT = "text";
    public static final String TOTAL = "total";
    public static final String URL = "url";
    public static final String VOLUME = "volume";
}
