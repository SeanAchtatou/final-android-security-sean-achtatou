package com.appbyme.comment.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.appbyme.activity.BaseFragmentActivity;
import com.appbyme.comment.activity.adapter.CommentListAdapter;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.ReplyTopicActivity;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.android.ui.activity.service.MediaService;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.model.PostsModel;
import com.mobcent.forum.android.model.ReplyModel;
import com.mobcent.forum.android.model.SoundModel;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class CommentListActivity extends BaseFragmentActivity implements MCConstant {
    /* access modifiers changed from: private */
    public long boardId;
    /* access modifiers changed from: private */
    public CommentListAdapter commentListAdapter;
    private Button commentsBackBtn;
    private Button commentsQuickreplyBtn;
    private Button commentsReplyBtn;
    /* access modifiers changed from: private */
    public EditText commentsReplyText;
    private TextView commentsTitle;
    private MediaPlayerBroadCastReceiver mediaPlayerBroadCastReceiver = null;
    /* access modifiers changed from: private */
    public String noRepliesContent;
    /* access modifiers changed from: private */
    public int page = 1;
    /* access modifiers changed from: private */
    public int pageSize = 10;
    /* access modifiers changed from: private */
    public PostsService postsService;
    /* access modifiers changed from: private */
    public PullToRefreshListView pullToRefreshListView;
    /* access modifiers changed from: private */
    public List<ReplyModel> replyModelList;
    private final char splitChar = 223;
    private final char tagImg = 225;
    /* access modifiers changed from: private */
    public long topicId;
    private String topicName;
    /* access modifiers changed from: private */
    public int totalNum = 0;

    static /* synthetic */ int access$1108(CommentListActivity x0) {
        int i = x0.page;
        x0.page = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.postsService = new PostsServiceImpl(getApplicationContext());
        this.replyModelList = new ArrayList();
        Intent intent = getIntent();
        this.boardId = intent.getLongExtra("boardId", 0);
        this.topicId = intent.getLongExtra("topicId", 0);
        this.topicName = intent.getStringExtra("topicName");
        this.noRepliesContent = getString(this.mcResource.getStringId("mc_forum_no_replies_content"));
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.mcResource.getLayoutId("comment_list_activity"));
        this.commentsBackBtn = (Button) findViewById(this.mcResource.getViewId("comments_back_btn"));
        this.commentsReplyBtn = (Button) findViewById(this.mcResource.getViewId("comments_reply_btn"));
        this.commentsQuickreplyBtn = (Button) findViewById(this.mcResource.getViewId("comments_quickreply_btn"));
        this.commentsTitle = (TextView) findViewById(this.mcResource.getViewId("comments_title"));
        this.commentsReplyText = (EditText) findViewById(this.mcResource.getViewId("comments_reply_text"));
        this.commentsTitle.setText(this.topicName);
        this.pullToRefreshListView = (PullToRefreshListView) findViewById(this.mcResource.getViewId("comments_listview"));
        this.commentListAdapter = new CommentListAdapter(this, this.replyModelList, this.boardId, this.topicId, null);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.commentListAdapter);
    }

    public class MediaPlayerBroadCastReceiver extends BroadcastReceiver {
        private SoundModel currSoundModel = null;
        private String tag;

        public MediaPlayerBroadCastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            this.tag = intent.getStringExtra(MediaService.SERVICE_TAG);
            if (this.tag != null && this.tag.equals(CommentListActivity.this.toString())) {
                this.currSoundModel = (SoundModel) intent.getSerializableExtra(MediaService.SERVICE_MODEL);
                CommentListActivity.this.commentListAdapter.updateReceivePlayImg(this.currSoundModel);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initActions() {
        this.commentsBackBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CommentListActivity.this.finish();
            }
        });
        this.commentsReplyBtn.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                if (LoginInterceptor.doInterceptor(CommentListActivity.this, null, null)) {
                    Intent intent = new Intent(CommentListActivity.this, ReplyTopicActivity.class);
                    intent.putExtra("boardId", CommentListActivity.this.boardId);
                    intent.putExtra("topicId", CommentListActivity.this.topicId);
                    intent.putExtra(MCConstant.IS_QUOTE_TOPIC, false);
                    CommentListActivity.this.startActivityForResult(intent, 11);
                }
            }
        });
        this.commentsQuickreplyBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (LoginInterceptor.doInterceptor(CommentListActivity.this, null, null)) {
                    String text = CommentListActivity.this.commentsReplyText.getText().toString();
                    if (!StringUtil.isEmpty(text)) {
                        CommentListActivity.this.addAsyncTask(new ReplyTextAsyncTask(CommentListActivity.this.postsService.createPublishTopicJson(text, "ß", "á")).execute(new Void[0]));
                    } else {
                        Toast.makeText(CommentListActivity.this.getApplicationContext(), CommentListActivity.this.mcResource.getStringId("mc_forum_publish_min_length_error"), 0).show();
                    }
                }
            }
        });
        this.pullToRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                CommentListActivity.this.clearAsyncTask();
                CommentListActivity.this.addAsyncTask(new PostsByDescAsyncTask(true).execute(new Void[0]));
            }
        });
        this.pullToRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                CommentListActivity.this.clearAsyncTask();
                CommentListActivity.this.addAsyncTask(new PostsByDescAsyncTask(false).execute(new Void[0]));
            }
        });
        this.pullToRefreshListView.onRefresh();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 11) {
            switch (resultCode) {
                case -1:
                    this.pullToRefreshListView.onRefresh();
                    return;
                default:
                    return;
            }
        }
    }

    private class PostsByDescAsyncTask extends AsyncTask<Void, Void, List<PostsModel>> {
        private boolean isPullToRefresh;

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<PostsModel>) ((List) x0));
        }

        public PostsByDescAsyncTask(boolean isPullToRefresh2) {
            this.isPullToRefresh = isPullToRefresh2;
        }

        /* access modifiers changed from: protected */
        public List<PostsModel> doInBackground(Void... params) {
            if (this.isPullToRefresh) {
                int unused = CommentListActivity.this.page = 1;
            }
            return CommentListActivity.this.postsService.getPostsByDesc(CommentListActivity.this.boardId, CommentListActivity.this.topicId, CommentListActivity.this.page, CommentListActivity.this.pageSize);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<PostsModel> postsModelList) {
            if (postsModelList == null) {
                CommentListActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else if (postsModelList.isEmpty()) {
                CommentListActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(postsModelList.get(0).getErrorCode())) {
                CommentListActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
                Toast.makeText(CommentListActivity.this.getApplicationContext(), MCForumErrorUtil.convertErrorCode(CommentListActivity.this.getApplicationContext(), postsModelList.get(0).getErrorCode()), 0).show();
            } else {
                if (this.isPullToRefresh) {
                    for (int i = 0; i < postsModelList.size(); i++) {
                        PostsModel postsMode = postsModelList.get(i);
                        if (postsMode.getPostType() == 1) {
                            int unused = CommentListActivity.this.totalNum = postsMode.getTotalNum();
                        }
                    }
                }
                if (this.isPullToRefresh) {
                    CommentListActivity.this.replyModelList.clear();
                }
                for (int i2 = 0; i2 < postsModelList.size(); i2++) {
                    PostsModel postsMode2 = postsModelList.get(i2);
                    if (postsMode2.getPostType() != 1) {
                        ReplyModel replyModel = postsMode2.getReply();
                        replyModel.setTotalNum(CommentListActivity.this.totalNum);
                        replyModel.setPage(CommentListActivity.this.page);
                        CommentListActivity.this.replyModelList.add(replyModel);
                    }
                }
                CommentListActivity.access$1108(CommentListActivity.this);
                CommentListActivity.this.commentListAdapter.setReplyModelList(CommentListActivity.this.replyModelList);
                CommentListActivity.this.commentListAdapter.notifyDataSetChanged();
                if (this.isPullToRefresh) {
                    CommentListActivity.this.pullToRefreshListView.onRefreshComplete();
                    if (CommentListActivity.this.replyModelList.size() >= CommentListActivity.this.totalNum) {
                        CommentListActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
                    } else {
                        CommentListActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
                    }
                } else if (CommentListActivity.this.replyModelList.size() < CommentListActivity.this.totalNum) {
                    CommentListActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    CommentListActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
                }
            }
            if (CommentListActivity.this.replyModelList.size() == 0) {
                CommentListActivity.this.pullToRefreshListView.onBottomRefreshComplete(2, CommentListActivity.this.noRepliesContent);
            }
        }
    }

    private class ReplyTextAsyncTask extends AsyncTask<Void, Void, String> {
        private String text = null;

        public ReplyTextAsyncTask(String text2) {
            this.text = text2;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... params) {
            return CommentListActivity.this.postsService.replyTopic(CommentListActivity.this.boardId, CommentListActivity.this.topicId, this.text, null, -1, false, -1);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String jsonStr) {
            if (jsonStr == null) {
                CommentListActivity.this.pullToRefreshListView.onRefresh();
                CommentListActivity.this.commentsReplyText.setText("");
                Toast.makeText(CommentListActivity.this.getApplicationContext(), CommentListActivity.this.mcResource.getStringId("mc_forum_reply_succ"), 0).show();
                return;
            }
            Toast.makeText(CommentListActivity.this.getApplicationContext(), CommentListActivity.this.mcResource.getStringId("mc_forum_msg_send_status"), 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.mediaPlayerBroadCastReceiver == null) {
            this.mediaPlayerBroadCastReceiver = new MediaPlayerBroadCastReceiver();
        }
        registerReceiver(this.mediaPlayerBroadCastReceiver, new IntentFilter(MediaService.INTENT_TAG + getPackageName()));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.mediaPlayerBroadCastReceiver != null) {
            unregisterReceiver(this.mediaPlayerBroadCastReceiver);
        }
    }
}
