package com.appbyme.comment.activity.adapter.holder;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.ad.android.ui.widget.AdView;

public class CommentListAdapterHolder {
    private AdView adView;
    private LinearLayout commentsContent;
    private TextView commentsFloor;
    private ImageView commentsHead;
    private LinearLayout commentsLevelBox;
    private RelativeLayout commentsLocationBox;
    private TextView commentsLocationText;
    private Button commentsManageItemBtn;
    private TextView commentsQuoteContent;
    private Button commentsReplyItemBtn;
    private TextView commentsTime;
    private TextView commentsUsername;

    public AdView getAdView() {
        return this.adView;
    }

    public void setAdView(AdView adView2) {
        this.adView = adView2;
    }

    public ImageView getCommentsHead() {
        return this.commentsHead;
    }

    public void setCommentsHead(ImageView commentsHead2) {
        this.commentsHead = commentsHead2;
    }

    public TextView getCommentsUsername() {
        return this.commentsUsername;
    }

    public void setCommentsUsername(TextView commentsUsername2) {
        this.commentsUsername = commentsUsername2;
    }

    public TextView getCommentsTime() {
        return this.commentsTime;
    }

    public void setCommentsTime(TextView commentsTime2) {
        this.commentsTime = commentsTime2;
    }

    public TextView getCommentsFloor() {
        return this.commentsFloor;
    }

    public void setCommentsFloor(TextView commentsFloor2) {
        this.commentsFloor = commentsFloor2;
    }

    public TextView getCommentsQuoteContent() {
        return this.commentsQuoteContent;
    }

    public void setCommentsQuoteContent(TextView commentsQuoteContent2) {
        this.commentsQuoteContent = commentsQuoteContent2;
    }

    public LinearLayout getCommentsContent() {
        return this.commentsContent;
    }

    public void setCommentsContent(LinearLayout commentsContent2) {
        this.commentsContent = commentsContent2;
    }

    public Button getCommentsReplyItemBtn() {
        return this.commentsReplyItemBtn;
    }

    public void setCommentsReplyItemBtn(Button commentsReplyItemBtn2) {
        this.commentsReplyItemBtn = commentsReplyItemBtn2;
    }

    public RelativeLayout getCommentsLocationBox() {
        return this.commentsLocationBox;
    }

    public void setCommentsLocationBox(RelativeLayout commentsLocationBox2) {
        this.commentsLocationBox = commentsLocationBox2;
    }

    public TextView getCommentsLocationText() {
        return this.commentsLocationText;
    }

    public void setCommentsLocationText(TextView commentsLocationText2) {
        this.commentsLocationText = commentsLocationText2;
    }

    public LinearLayout getCommentsLevelBox() {
        return this.commentsLevelBox;
    }

    public void setCommentsLevelBox(LinearLayout commentsLevelBox2) {
        this.commentsLevelBox = commentsLevelBox2;
    }

    public Button getCommentsManageItemBtn() {
        return this.commentsManageItemBtn;
    }

    public void setCommentsManageItemBtn(Button commentsManageItemBtn2) {
        this.commentsManageItemBtn = commentsManageItemBtn2;
    }
}
