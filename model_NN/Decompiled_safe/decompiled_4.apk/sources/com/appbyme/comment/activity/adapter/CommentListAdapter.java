package com.appbyme.comment.activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.appbyme.comment.activity.adapter.holder.CommentListAdapterHolder;
import com.mobcent.ad.android.ui.widget.AdView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.ReplyTopicActivity;
import com.mobcent.base.android.ui.activity.adapter.BaseSoundListAdapter;
import com.mobcent.base.android.ui.activity.delegate.TopicManageDelegate;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserHomeFragmentActivity;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.android.ui.activity.view.MCLevelView;
import com.mobcent.base.android.ui.activity.view.MCTopicManageDialog;
import com.mobcent.base.forum.android.util.ImageCache;
import com.mobcent.base.forum.android.util.MCDateUtil;
import com.mobcent.base.forum.android.util.MCFaceUtil;
import com.mobcent.base.forum.android.util.MCPhoneUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.model.ReplyModel;
import com.mobcent.forum.android.model.TopicContentModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class CommentListAdapter extends BaseSoundListAdapter {
    private String adTag;
    /* access modifiers changed from: private */
    public long boardId;
    /* access modifiers changed from: private */
    public Context context;
    private CommentListAdapterHolder holder;
    /* access modifiers changed from: private */
    public MCResource mcResource;
    private List<ReplyModel> replyModelList;
    /* access modifiers changed from: private */
    public Resources resources;
    /* access modifiers changed from: private */
    public long topicId;

    public CommentListAdapter(Context context2, List<ReplyModel> replyModelList2, long boardId2, long topicId2, String adTag2) {
        super(context2, context2.toString(), LayoutInflater.from(context2));
        this.mcResource = MCResource.getInstance(context2);
        this.context = context2;
        this.resources = context2.getResources();
        this.replyModelList = replyModelList2;
        this.boardId = boardId2;
        this.topicId = topicId2;
        this.adTag = adTag2;
    }

    public int getCount() {
        return this.replyModelList.size();
    }

    public Object getItem(int position) {
        return this.replyModelList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ReplyModel replyModel = (ReplyModel) getItem(position);
        int floor = replyModel.getTotalNum() - position;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate(this.mcResource.getLayoutId("comment_list_activity_item"), (ViewGroup) null);
            this.holder = new CommentListAdapterHolder();
            this.holder.setAdView((AdView) convertView.findViewById(this.mcResource.getViewId("ad_view")));
            this.holder.setCommentsHead((ImageView) convertView.findViewById(this.mcResource.getViewId("comments_head")));
            this.holder.setCommentsUsername((TextView) convertView.findViewById(this.mcResource.getViewId("comments_username")));
            this.holder.setCommentsTime((TextView) convertView.findViewById(this.mcResource.getViewId("comments_time")));
            this.holder.setCommentsFloor((TextView) convertView.findViewById(this.mcResource.getViewId("comments_floor")));
            this.holder.setCommentsContent((LinearLayout) convertView.findViewById(this.mcResource.getViewId("comments_content")));
            this.holder.setCommentsQuoteContent((TextView) convertView.findViewById(this.mcResource.getViewId("comments_quote_content")));
            this.holder.setCommentsReplyItemBtn((Button) convertView.findViewById(this.mcResource.getViewId("comments_reply_item_btn")));
            this.holder.setCommentsLocationBox((RelativeLayout) convertView.findViewById(this.mcResource.getViewId("comments_location_box")));
            this.holder.setCommentsLocationText((TextView) convertView.findViewById(this.mcResource.getViewId("comments_location_text")));
            this.holder.setCommentsLevelBox((LinearLayout) convertView.findViewById(this.mcResource.getViewId("comments_level_box")));
            this.holder.setCommentsManageItemBtn((Button) convertView.findViewById(this.mcResource.getViewId("comments_manage_item_btn")));
            convertView.setTag(this.holder);
        } else {
            this.holder = (CommentListAdapterHolder) convertView.getTag();
        }
        this.holder.getCommentsUsername().setText(replyModel.getUserNickName());
        this.holder.getCommentsLevelBox().removeAllViews();
        new MCLevelView(this.context, replyModel.getLevel(), this.holder.getCommentsLevelBox(), 0);
        this.holder.getCommentsFloor().setText(MCStringBundleUtil.resolveString(this.mcResource.getStringId("mc_forum_posts_reply_lab"), new String(floor + ""), this.context));
        this.holder.getCommentsTime().setText(MCDateUtil.convertTime(this.context, replyModel.getPostsDate(), this.mcResource));
        loadCommentsContent(replyModel.getReplyContentList(), this.holder.getCommentsContent(), replyModel.getStatus());
        if (replyModel.getLocation() == null || "".equals(replyModel.getLocation())) {
            this.holder.getCommentsLocationBox().setVisibility(8);
        } else {
            this.holder.getCommentsLocationBox().setVisibility(0);
            this.holder.getCommentsLocationText().setText(replyModel.getLocation() + "");
        }
        if (!replyModel.isQuote()) {
            this.holder.getCommentsQuoteContent().setVisibility(8);
        } else {
            this.holder.getCommentsQuoteContent().setVisibility(0);
            if (replyModel.getStatus() != 0) {
                this.holder.getCommentsQuoteContent().setText(replyModel.getQuoteContent());
                MCFaceUtil.setStrToFace(this.holder.getCommentsQuoteContent(), replyModel.getQuoteContent() + " ", this.context);
            }
        }
        String imageUrl = ImageCache.formatUrl(replyModel.getIconUrl() + replyModel.getIcon(), "100x100");
        final ImageView imageView = this.holder.getCommentsHead();
        imageView.setImageDrawable(null);
        AsyncTaskLoaderImage.getInstance(this.context).loadAsync(imageUrl, new AsyncTaskLoaderImage.BitmapImageCallback() {
            public void onImageLoaded(Bitmap bitmap, String url) {
                if (bitmap != null && !bitmap.isRecycled()) {
                    TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), new BitmapDrawable(CommentListAdapter.this.resources, bitmap)});
                    td.startTransition(350);
                    imageView.setImageDrawable(td);
                }
            }
        });
        this.holder.getCommentsReplyItemBtn().setOnClickListener(new View.OnClickListener() {
            /* Debug info: failed to restart local var, previous not found, register: 4 */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                if (LoginInterceptor.doInterceptor(CommentListAdapter.this.context, null, null)) {
                    Intent intent = new Intent(CommentListAdapter.this.context, ReplyTopicActivity.class);
                    intent.putExtra("boardId", CommentListAdapter.this.boardId);
                    intent.putExtra("topicId", CommentListAdapter.this.topicId);
                    intent.putExtra("toReplyId", replyModel.getReplyPostsId());
                    intent.putExtra(MCConstant.IS_QUOTE_TOPIC, true);
                    ((Activity) CommentListAdapter.this.context).startActivityForResult(intent, 11);
                }
            }
        });
        this.holder.getCommentsHead().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HashMap<String, Serializable> param = new HashMap<>();
                long userId = replyModel.getReplyUserId();
                param.put("userId", Long.valueOf(userId));
                if (LoginInterceptor.doInterceptorByDialog(CommentListAdapter.this.context, CommentListAdapter.this.mcResource, UserHomeFragmentActivity.class, param)) {
                    Intent intent = new Intent(CommentListAdapter.this.context, UserHomeFragmentActivity.class);
                    intent.putExtra("userId", userId);
                    CommentListAdapter.this.context.startActivity(intent);
                }
            }
        });
        if (replyModel.getStatus() == 0) {
            this.holder.getCommentsManageItemBtn().setVisibility(8);
            this.holder.getCommentsManageItemBtn().setOnClickListener(null);
        } else {
            this.holder.getCommentsManageItemBtn().setVisibility(0);
            this.holder.getCommentsManageItemBtn().setOnClickListener(new CommentsManageClickListener(this.holder.getCommentsManageItemBtn(), replyModel, this.holder.getCommentsContent()));
        }
        return convertView;
    }

    /* access modifiers changed from: private */
    public void loadCommentsContent(List<TopicContentModel> topicContentList, LinearLayout commentsContentLayout, int status) {
        if (topicContentList != null && topicContentList.size() > 0) {
            String str = "";
            commentsContentLayout.removeAllViews();
            if (status == 0) {
                TextView tv = new TextView(this.context);
                tv.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                tv.setTextColor(-16777216);
                tv.setText(this.mcResource.getStringId("mc_forum_topic_deleted"));
                commentsContentLayout.addView(tv);
                return;
            }
            for (int i = 0; i < topicContentList.size(); i++) {
                TopicContentModel topicContentModel = topicContentList.get(i);
                int type = topicContentModel.getType();
                if (type == 0) {
                    if (topicContentModel.getInfor() != null && !"".equals(topicContentModel.getInfor())) {
                        TextView tv2 = new TextView(this.context);
                        tv2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                        tv2.setTextColor(-16777216);
                        str = str + topicContentModel.getInfor();
                        tv2.setText(topicContentModel.getInfor());
                        MCFaceUtil.setStrToFace(tv2, topicContentModel.getInfor() + " ", this.context);
                        commentsContentLayout.addView(tv2);
                    }
                } else if (type == 1) {
                    final ImageView iv = new ImageView(this.context);
                    int height = MCPhoneUtil.getRawSize(this.context, 1, 200.0f);
                    commentsContentLayout.addView(iv, new LinearLayout.LayoutParams(height, height));
                    AsyncTaskLoaderImage.getInstance(this.context).loadAsync(ImageCache.formatUrl(topicContentModel.getBaseUrl() + topicContentModel.getInfor(), "320x480"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                        public void onImageLoaded(Bitmap bitmap, String url) {
                            if (bitmap != null && !bitmap.isRecycled()) {
                                TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), new BitmapDrawable(CommentListAdapter.this.resources, bitmap)});
                                td.startTransition(350);
                                iv.setImageDrawable(td);
                            }
                        }
                    });
                } else if (type == 5) {
                    commentsContentLayout.addView(getSoundView(topicContentModel.getSoundModel()), new LinearLayout.LayoutParams(-1, -2));
                }
            }
            commentsContentLayout.setTag(str);
        }
    }

    public List<ReplyModel> getReplyModelList() {
        return this.replyModelList;
    }

    public void setReplyModelList(List<ReplyModel> replyModelList2) {
        this.replyModelList = replyModelList2;
    }

    class CommentsManageClickListener implements View.OnClickListener {
        /* access modifiers changed from: private */
        public Button btn;
        /* access modifiers changed from: private */
        public LinearLayout commentsContentLayout;
        /* access modifiers changed from: private */
        public ReplyModel replyModel;

        public CommentsManageClickListener(Button btn2, ReplyModel replyModel2, LinearLayout commentsContentLayout2) {
            this.btn = btn2;
            this.replyModel = replyModel2;
            this.commentsContentLayout = commentsContentLayout2;
        }

        public void onClick(View v) {
            if (LoginInterceptor.doInterceptor(CommentListAdapter.this.context, null)) {
                new MCTopicManageDialog(CommentListAdapter.this.context, CommentListAdapter.this.boardId, "", null, this.replyModel, 2, new TopicManageDelegate() {
                    public void topicManageDelegate(int type) {
                        if (type == 1) {
                            CommentsManageClickListener.this.btn.setVisibility(8);
                            CommentsManageClickListener.this.btn.setOnClickListener(null);
                            CommentListAdapter.this.loadCommentsContent(CommentsManageClickListener.this.replyModel.getReplyContentList(), CommentsManageClickListener.this.commentsContentLayout, 0);
                        }
                    }
                }, false).show();
            }
        }
    }
}
