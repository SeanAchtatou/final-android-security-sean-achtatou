package com.appbyme.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.fragment.BaseFragment;
import com.appbyme.activity.fragment.GoodsCommentFragment;
import com.appbyme.activity.fragment.GoodsImgFragment;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.appbyme.android.base.model.GoodsDetailModel;
import com.appbyme.android.base.model.GoodsModel;
import com.appbyme.android.base.model.GoodsShopModel;
import com.appbyme.android.service.GoodsDetailService;
import com.appbyme.android.service.impl.GoodsDetailServiceImpl;
import com.appbyme.comment.activity.CommentListActivity;
import com.appbyme.widget.AutogenViewPager;
import com.appbyme.widget.GoodsDetailScrollView;
import com.mobcent.android.model.MCShareModel;
import com.mobcent.base.android.ui.activity.WebViewActivity;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.forum.android.constant.MCForumConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.PhoneUtil;
import com.mobcent.forum.android.util.StringUtil;
import com.mobcent.share.android.activity.helper.MCShareLaunchShareHelper;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class GoodsDetailActivity extends BaseListActivity {
    private String TAG = "GoodsDetailActivity";
    private Button buttomBuy;
    private Button buttomCollection;
    private Button buttomComment;
    private Button buttomShare;
    private TextView cityName;
    private TextView desEvaluateData;
    /* access modifiers changed from: private */
    public AutogenViewPager detailPager;
    private RelativeLayout detailTag;
    private TextView evaluate1Data;
    private TextView evaluate2Data;
    private TextView evaluate3Data;
    private ImageView favorIcon;
    private TextView favorNum;
    /* access modifiers changed from: private */
    public BaseFragment fragment = null;
    private Button goTB;
    /* access modifiers changed from: private */
    public GoodsDetailModel goodsDetailModel;
    /* access modifiers changed from: private */
    public GoodsDetailScrollView goodsDetailScrollView;
    /* access modifiers changed from: private */
    public GoodsDetailService goodsDetailService;
    /* access modifiers changed from: private */
    public GoodsModel goodsModel;
    private TextView goodsName;
    private TextView goodsPrice;
    private TextView goodsPriceSign;
    /* access modifiers changed from: private */
    public ImageView goodsThumb;
    private boolean isFavor = false;
    private boolean isGetData = false;
    private AutogenModuleModel moduleModel;
    private TextView saleNum;
    private TextView sendEvaluate3Data;
    private TextView serveEvaluateData;
    private TextView shopEvaluateRate;
    /* access modifiers changed from: private */
    public ImageView shopIcon;
    private TextView shopName;
    private TextView shopNameText;
    private LinearLayout shopRank;
    /* access modifiers changed from: private */
    public Button tag1;
    /* access modifiers changed from: private */
    public Button tag2;
    private TagAdapter tagAdapter;
    private RelativeLayout topbar;
    private View topbarLeft;
    private TextView topbarTitle;
    /* access modifiers changed from: private */
    public long topicId;

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        Bundle bundle = getIntent().getExtras();
        this.topicId = bundle.getLong("topicId");
        this.goodsModel = (GoodsModel) bundle.getSerializable(IntentConstant.INTENT_EC_GOODSMODEL);
        this.goodsDetailService = new GoodsDetailServiceImpl(this.application);
        new LoadDataAsyncTask().execute(Long.valueOf(this.topicId));
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.mcResource.getLayoutId("mc_ec_goods_detail_activity"));
        this.topbar = (RelativeLayout) findViewById(this.mcResource.getViewId("common_topbar"));
        this.topbarLeft = ((ViewStub) findViewById(this.mcResource.getViewId("ec_common_topbar_left"))).inflate();
        this.topbarLeft.setBackgroundDrawable(this.mcResource.getDrawable("mc_forum_top_bar_button1"));
        this.topbarTitle = (TextView) findViewById(this.mcResource.getViewId("ec_common_topbar_title"));
        this.topbarTitle.setText(this.mcResource.getString("mc_ec_goods_detail_title"));
        this.goodsDetailScrollView = (GoodsDetailScrollView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_scrollView"));
        this.goodsThumb = (ImageView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_goods_thumb"));
        this.favorNum = (TextView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_collect_num"));
        this.favorIcon = (ImageView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_collect_icon"));
        this.saleNum = (TextView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_sale_num"));
        this.goodsName = (TextView) findViewById(this.mcResource.getViewId("mc_ec_detail_goods_name"));
        this.goodsPriceSign = (TextView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_goods_price_sign"));
        this.goodsPriceSign.setVisibility(4);
        this.goodsPrice = (TextView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_goods_price"));
        this.goTB = (Button) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_go_tb"));
        this.shopIcon = (ImageView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_shop_icon"));
        this.shopName = (TextView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_shop_name"));
        this.shopNameText = (TextView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_shop_name_content"));
        this.shopRank = (LinearLayout) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_shop_rank"));
        this.cityName = (TextView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_shop_city_name"));
        this.shopEvaluateRate = (TextView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_shop_evaluate_rate"));
        this.evaluate1Data = (TextView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_shop_detail_evaluate1_data"));
        this.evaluate2Data = (TextView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_shop_detail_evaluate2_data"));
        this.evaluate3Data = (TextView) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_shop_detail_evaluate3_data"));
        this.detailTag = (RelativeLayout) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_tag"));
        this.tag1 = (Button) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_tag1"));
        this.tag2 = (Button) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_tag2"));
        this.detailPager = (AutogenViewPager) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_pager"));
        this.buttomBuy = (Button) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_buttom_buy"));
        this.buttomCollection = (Button) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_buttom_collection"));
        this.buttomShare = (Button) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_buttom_share"));
        this.buttomComment = (Button) findViewById(this.mcResource.getViewId("mc_ec_goods_detail_buttom_comment"));
        this.detailPager.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                GoodsDetailActivity.this.detailPager.getViewTreeObserver().removeOnPreDrawListener(this);
                int unused = GoodsDetailActivity.this.setPagerSize(GoodsDetailActivity.this.detailPager);
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    public int setPagerSize(AutogenViewPager detailPager2) {
        ViewGroup.LayoutParams lp = detailPager2.getLayoutParams();
        lp.height = (this.goodsDetailScrollView.getHeight() - this.detailTag.getHeight()) - 5;
        lp.width = -1;
        detailPager2.setLayoutParams(lp);
        return lp.height;
    }

    /* access modifiers changed from: protected */
    public void initActions() {
        super.initActions();
        this.goodsDetailScrollView.setListener(new MyOnBorderListener(), this.goodsDetailScrollView.getChildAt(0));
        this.goodsDetailScrollView.smoothScrollTo(0, 20);
        this.topbarLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GoodsDetailActivity.this.finish();
            }
        });
        this.buttomBuy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GoodsDetailActivity.this.openDetailInTB(GoodsDetailActivity.this.goodsDetailModel);
            }
        });
        this.buttomCollection.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GoodsDetailActivity.this.onClickFavor();
            }
        });
        this.favorIcon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GoodsDetailActivity.this.onClickFavor();
            }
        });
        this.favorIcon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GoodsDetailActivity.this.onClickFavor();
            }
        });
        this.buttomShare.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GoodsDetailActivity.this.share();
            }
        });
        this.buttomComment.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (GoodsDetailActivity.this.goodsModel != null) {
                    Intent intent = new Intent(GoodsDetailActivity.this, CommentListActivity.class);
                    intent.putExtra("boardId", GoodsDetailActivity.this.goodsModel.getBoardId());
                    intent.putExtra("topicId", GoodsDetailActivity.this.topicId);
                    intent.putExtra("topicName", GoodsDetailActivity.this.goodsModel.getTitle());
                    GoodsDetailActivity.this.startActivity(intent);
                }
            }
        });
        this.goTB.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GoodsDetailActivity.this.openDetailInTB(GoodsDetailActivity.this.goodsDetailModel);
            }
        });
        this.tag1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GoodsDetailActivity.this.tag1.setSelected(true);
                GoodsDetailActivity.this.tag2.setSelected(false);
                if (GoodsDetailActivity.this.detailPager != null) {
                    GoodsDetailActivity.this.detailPager.setCurrentItem(0, true);
                }
            }
        });
        this.tag2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GoodsDetailActivity.this.tag1.setSelected(false);
                GoodsDetailActivity.this.tag2.setSelected(true);
                if (GoodsDetailActivity.this.detailPager != null) {
                    GoodsDetailActivity.this.detailPager.setCurrentItem(1, true);
                }
            }
        });
        setTagClickable();
        this.detailPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int position) {
                GoodsDetailActivity.this.currentPosition = position;
                GoodsDetailActivity.this.detailTagNav(position);
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    private void setTagClickable() {
        if (this.isGetData) {
            this.tag1.setClickable(true);
            this.tag2.setClickable(true);
            return;
        }
        this.tag1.setClickable(false);
        this.tag2.setClickable(false);
    }

    /* access modifiers changed from: private */
    public void share() {
        if (this.isGetData || this.goodsDetailModel != null) {
            String shareText = this.goodsDetailModel.getGoodsTitle();
            String goodsImg = AsyncTaskLoaderImage.formatUrl(this.goodsDetailModel.getGoodsThumb(), MCForumConstant.RESOLUTION_640X480);
            String linkUrl = null;
            try {
                linkUrl = URLEncoder.encode(this.goodsDetailModel.getClickUrl(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            MCShareModel shareModel = new MCShareModel();
            shareModel.setContext(this);
            shareModel.setAppKey(SharedPreferencesDB.getInstance(this.context).getForumKey());
            shareModel.setContent(shareText);
            shareModel.setLinkUrl(linkUrl);
            shareModel.setPicUrl(goodsImg);
            shareModel.setSkipUrl(linkUrl);
            shareModel.setDownloadUrl(this.mcResource.getString("mc_share_download_url"));
            shareModel.setImageFilePath(AsyncTaskLoaderImage.getInstance(this.context).getImagePath(goodsImg));
            shareModel.setType(2);
            MCShareLaunchShareHelper.share(shareModel);
        }
    }

    /* access modifiers changed from: private */
    public void onClickFavor() {
        if (this.isGetData && LoginInterceptor.doInterceptorByDialog(this, this.mcResource, null, null)) {
            collectionGoods(this.topicId);
        }
    }

    private class LoadDataAsyncTask extends AsyncTask<Long, Void, GoodsDetailModel> {
        private long topicId;

        private LoadDataAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public GoodsDetailModel doInBackground(Long... arg0) {
            this.topicId = arg0[0].longValue();
            GoodsDetailModel unused = GoodsDetailActivity.this.goodsDetailModel = GoodsDetailActivity.this.application.getAutogenDataCache().getGoodsDetail(this.topicId);
            if (GoodsDetailActivity.this.goodsDetailModel == null) {
                GoodsDetailModel unused2 = GoodsDetailActivity.this.goodsDetailModel = GoodsDetailActivity.this.goodsDetailService.getGoodsDetailListByNet(this.topicId);
            }
            return GoodsDetailActivity.this.goodsDetailModel;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(GoodsDetailModel result) {
            GoodsDetailActivity.this.refreshView(this.topicId, result);
        }
    }

    /* access modifiers changed from: private */
    public void refreshView(long topicId2, GoodsDetailModel result) {
        if (result != null) {
            this.goodsDetailModel = result;
            this.isGetData = true;
            this.tagAdapter = new TagAdapter(getSupportFragmentManager(), this.goodsDetailModel);
            this.detailPager.setAdapter(this.tagAdapter);
            setTagClickable();
            this.tag1.setSelected(true);
            this.isFavor = result.isFavors();
            this.application.getAutogenDataCache().setGoodsDetail(topicId2, result);
            this.goodsThumb.setImageDrawable(this.mcResource.getDrawable("mc_forum_bg2"));
            AsyncTaskLoaderImage.getInstance(this.context).loadAsync(AsyncTaskLoaderImage.formatUrl(result.getGoodsThumb(), MCForumConstant.RESOLUTION_640X480), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap bitmap, String url) {
                    GoodsDetailActivity.this.handler.post(new Runnable() {
                        public void run() {
                            if (bitmap != null && !bitmap.isRecycled() && GoodsDetailActivity.this.goodsThumb.isShown()) {
                                TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), new BitmapDrawable(GoodsDetailActivity.this.getResources(), bitmap)});
                                td.startTransition(350);
                                GoodsDetailActivity.this.goodsThumb.setImageDrawable(td);
                            }
                        }
                    });
                }
            });
            this.favorNum.setText(result.getFavorsNum() + "");
            this.saleNum.setText(result.getSaleNum() + "");
            this.goodsName.setText(result.getGoodsTitle());
            String price = result.getPrice();
            if (StringUtil.isEmpty(price)) {
                this.goodsPriceSign.setVisibility(4);
            } else {
                this.goodsPriceSign.setVisibility(0);
                this.goodsPrice.setText(price);
            }
            this.shopIcon.setImageDrawable(this.mcResource.getDrawable("mc_forum_bg2"));
            GoodsShopModel GoodsShopModel = result.getGoodsShopModel();
            if (GoodsShopModel != null) {
                AsyncTaskLoaderImage.getInstance(this.context).loadAsync(AsyncTaskLoaderImage.formatUrl(GoodsShopModel.getShopPic(), "200x200"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                    public void onImageLoaded(final Bitmap bitmap, String url) {
                        GoodsDetailActivity.this.handler.post(new Runnable() {
                            public void run() {
                                if (bitmap != null && !bitmap.isRecycled() && GoodsDetailActivity.this.shopIcon.isShown()) {
                                    TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), new BitmapDrawable(GoodsDetailActivity.this.getResources(), bitmap)});
                                    td.startTransition(350);
                                    GoodsDetailActivity.this.shopIcon.setImageDrawable(td);
                                }
                            }
                        });
                    }
                });
            }
            GoodsShopModel goodsShopModel = result.getGoodsShopModel();
            if (goodsShopModel != null) {
                this.shopNameText.setText(goodsShopModel.getShopName());
                addGradeView(result.getGoodsShopModel().getGrade(), this.shopRank);
                this.cityName.setText(result.getGoodsShopModel().getLocal());
                this.evaluate1Data.setText(result.getGoodsShopModel().getShopScore().getItemScore());
                this.evaluate2Data.setText(result.getGoodsShopModel().getShopScore().getServiceScore());
                this.evaluate3Data.setText(result.getGoodsShopModel().getShopScore().getDeliveryScore());
            }
            setFavor(this.isFavor);
        }
    }

    private void setFavor(boolean isFavor2) {
        if (isFavor2) {
            this.favorIcon.setBackgroundDrawable(this.mcResource.getDrawable("mc_forum_shopping_commoditydetails1"));
            this.buttomCollection.setBackgroundDrawable(this.mcResource.getDrawable("mc_forum_tools_bar_button11"));
            return;
        }
        this.favorIcon.setBackgroundDrawable(this.mcResource.getDrawable("mc_forum_tools_bar_button11_n"));
        this.buttomCollection.setBackgroundDrawable(this.mcResource.getDrawable("mc_forum_tools_bar_button12"));
    }

    private void addGradeView(int grade, LinearLayout linearLayout) {
        String[] myGrade = calcGrade(grade);
        if (myGrade[0] != null || myGrade[1] != null || myGrade[2] != null) {
            int num = Integer.parseInt(myGrade[1]);
            String gradeName = myGrade[2];
            for (int i = 0; i < num; i++) {
                ImageView iv = new ImageView(this);
                iv.setImageDrawable(this.mcResource.getDrawable(gradeName));
                int width = (int) PhoneUtil.dip2px(this, 15.0f);
                linearLayout.addView(iv, width, width);
            }
        }
    }

    private String[] calcGrade(int grade) {
        String[] gradeArr = new String[3];
        for (int i = 1; i < grade; i++) {
            int s = grade / 5;
            gradeArr[0] = s + "";
            gradeArr[1] = (grade % 5) + "";
            switch (s) {
                case 0:
                    gradeArr[2] = "mc_forum_shopping_details2";
                    break;
                case 1:
                    gradeArr[2] = "mc_forum_shopping_details1";
                    break;
                case 2:
                    gradeArr[2] = "mc_forum_shopping_details5";
                    break;
                case 3:
                    gradeArr[2] = "mc_forum_shopping_details3";
                    break;
            }
        }
        return gradeArr;
    }

    private void collectionGoods(long topicId2) {
        new collectionAsyncTask().execute(Long.valueOf(topicId2));
    }

    private class collectionAsyncTask extends AsyncTask<Long, Void, Integer> {
        private collectionAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public Integer doInBackground(Long... params) {
            return Integer.valueOf(GoodsDetailActivity.this.goodsDetailService.collectionGoods(params[0].longValue()));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Integer result) {
            GoodsDetailActivity.this.handleCollectResult(result);
        }
    }

    /* access modifiers changed from: private */
    public void handleCollectResult(Integer rs) {
        if (rs.intValue() == 1) {
            if (this.isFavor) {
                this.isFavor = false;
                this.favorIcon.setBackgroundDrawable(this.mcResource.getDrawable("mc_forum_tools_bar_button11_n"));
                this.goodsDetailModel.setFavorsNum(this.goodsDetailModel.getFavorsNum() - 1);
                this.favorNum.setText(this.goodsDetailModel.getFavorsNum() + "");
                this.goodsDetailModel.setFavors(this.isFavor);
                this.application.getAutogenDataCache().clearGoodsDetail(this.topicId);
                this.application.getAutogenDataCache().setGoodsDetail(this.topicId, this.goodsDetailModel);
                this.buttomCollection.setBackgroundDrawable(this.mcResource.getDrawable("mc_forum_tools_bar_button12"));
                showMessage(this.mcResource.getString("mc_ec_goods_detail_cancle_favor_succ"));
                return;
            }
            this.isFavor = true;
            this.favorIcon.setBackgroundDrawable(this.mcResource.getDrawable("mc_forum_shopping_commoditydetails1"));
            this.goodsDetailModel.setFavorsNum(this.goodsDetailModel.getFavorsNum() + 1);
            this.favorNum.setText(this.goodsDetailModel.getFavorsNum() + "");
            this.goodsDetailModel.setFavors(this.isFavor);
            this.application.getAutogenDataCache().clearGoodsDetail(this.topicId);
            this.application.getAutogenDataCache().setGoodsDetail(this.topicId, this.goodsDetailModel);
            this.buttomCollection.setBackgroundResource(this.mcResource.getDrawableId("mc_forum_tools_bar_button11"));
            showMessage(this.mcResource.getString("mc_ec_goods_detail_collect_succ"));
        } else if (this.isFavor) {
            showMessage(this.mcResource.getString("mc_ec_goods_detail_cancle_favor_fail"));
        } else {
            showMessage(this.mcResource.getString("mc_ec_goods_detail_collect_fail"));
        }
    }

    /* access modifiers changed from: private */
    public void openDetailInTB(GoodsDetailModel goodsDetailModel2) {
        if (goodsDetailModel2 != null && !StringUtil.isEmpty(goodsDetailModel2.getClickUrl())) {
            Intent tbDetailIntent = new Intent(this, WebViewActivity.class);
            tbDetailIntent.putExtra("webViewUrl", goodsDetailModel2.getClickUrl());
            startActivity(tbDetailIntent);
        }
    }

    private class TagAdapter extends FragmentStatePagerAdapter {
        private Map<Integer, BaseFragment> fragmentMap = new HashMap();
        private GoodsDetailModel goodsDetailModel;

        public TagAdapter(FragmentManager fm, GoodsDetailModel goodsDetailModel2) {
            super(fm);
            this.goodsDetailModel = goodsDetailModel2;
        }

        public BaseFragment getItem(int position) {
            if (this.fragmentMap.get(Integer.valueOf(position)) == null) {
                if (position == 0) {
                    BaseFragment unused = GoodsDetailActivity.this.fragment = new GoodsImgFragment(GoodsDetailActivity.this, GoodsDetailActivity.this.goodsDetailScrollView);
                }
                if (position == 1) {
                    BaseFragment unused2 = GoodsDetailActivity.this.fragment = new GoodsCommentFragment(GoodsDetailActivity.this, GoodsDetailActivity.this.goodsDetailScrollView);
                }
                Bundle bundle = GoodsDetailActivity.this.getBundle(position);
                bundle.putSerializable(IntentConstant.INTENT_EC_GOODSDETAILMODEL, this.goodsDetailModel);
                GoodsDetailActivity.this.fragment.setArguments(bundle);
                this.fragmentMap.put(Integer.valueOf(position), GoodsDetailActivity.this.fragment);
            }
            return this.fragmentMap.get(Integer.valueOf(position));
        }

        public int getCount() {
            return 2;
        }
    }

    /* access modifiers changed from: private */
    public void detailTagNav(int position) {
        if (position == 0) {
            this.tag1.setSelected(true);
            this.tag2.setSelected(false);
        }
        if (position == 1) {
            this.tag2.setSelected(true);
            this.tag1.setSelected(false);
        }
    }

    class MyOnBorderListener implements GoodsDetailScrollView.OnBorderListener {
        MyOnBorderListener() {
        }

        public void onBottom() {
            if (GoodsDetailActivity.this.getCurrentFragment() != null) {
                GoodsDetailActivity.this.getCurrentFragment().setSVIsBottom(true);
            }
        }

        public void onTop() {
            if (GoodsDetailActivity.this.getCurrentFragment() != null) {
                GoodsDetailActivity.this.getCurrentFragment().setSVIsBottom(false);
            }
        }
    }

    /* access modifiers changed from: private */
    public BaseFragment getCurrentFragment() {
        if (this.tagAdapter == null) {
            return null;
        }
        return this.tagAdapter.getItem(this.currentPosition);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (getCurrentFragment() != null) {
            getCurrentFragment().recycleImage();
        }
        this.goodsThumb.setImageDrawable(null);
        this.shopIcon.setImageDrawable(null);
        this.application.getAutogenDataCache().clearGoodsDetail();
    }
}
