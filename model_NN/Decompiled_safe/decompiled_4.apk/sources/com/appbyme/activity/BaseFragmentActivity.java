package com.appbyme.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.widget.Toast;
import com.appbyme.activity.application.AutogenApplication;
import com.appbyme.activity.constant.ConfigConstant;
import com.appbyme.activity.constant.FinalConstant;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.observable.ActivityObserver;
import com.appbyme.android.base.model.AutogenBoardCategory;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.mobcent.base.android.ui.activity.receiver.MCForumReceiver;
import com.mobcent.base.forum.android.util.MCResource;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseFragmentActivity extends FragmentActivity implements FinalConstant, IntentConstant, ConfigConstant {
    protected String ACTIVITY_TAG;
    protected ActivityObserver activityObserver;
    protected AutogenApplication application;
    protected ArrayList<AutogenBoardCategory> boardCategory;
    protected Context context;
    private MCForumReceiver forumReceiver = new MCForumReceiver(this);
    protected Handler handler;
    protected LayoutInflater inflater;
    protected List<AsyncTask<?, ?, ?>> loadDataAsyncTasks;
    protected MCResource mcResource;
    protected AutogenModuleModel moduleModel;
    protected Bundle savedInstanceState;
    protected Toast toast;

    /* access modifiers changed from: protected */
    public abstract void initActions();

    /* access modifiers changed from: protected */
    public abstract void initData();

    /* access modifiers changed from: protected */
    public abstract void initViews();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState2) {
        super.onCreate(savedInstanceState2);
        this.savedInstanceState = savedInstanceState2;
        this.toast = Toast.makeText(getApplicationContext(), "", 0);
        this.loadDataAsyncTasks = new ArrayList();
        this.context = getApplicationContext();
        initBaseViews();
        initBaseData(savedInstanceState2);
        initData();
        initViews();
        initActions();
        overridePendingTransition(this.mcResource.getAnimId("right_in"), this.mcResource.getAnimId("left_out"));
    }

    /* access modifiers changed from: protected */
    public void initBaseViews() {
        requestWindowFeature(1);
    }

    /* access modifiers changed from: protected */
    public void initBaseData(Bundle savedInstanceState2) {
        this.application = (AutogenApplication) getApplication();
        this.application.addActivity(this);
        this.mcResource = MCResource.getInstance(getApplicationContext());
        this.handler = new Handler();
        this.ACTIVITY_TAG = getClass().getSimpleName();
        this.inflater = LayoutInflater.from(this);
        this.activityObserver = new ActivityObserver(this);
        this.application.getActivityObservable().registerObserver(this.activityObserver);
        this.application.getActivityObservable().notifyActivityCreate(this.activityObserver, savedInstanceState2);
    }

    public void showMessage(String str) {
        this.toast.setText(str);
        this.toast.show();
    }

    /* access modifiers changed from: protected */
    public void clearAsyncTask() {
        int size = this.loadDataAsyncTasks.size();
        for (int i = 0; i < size; i++) {
            this.loadDataAsyncTasks.get(i).cancel(true);
        }
        this.loadDataAsyncTasks.clear();
    }

    /* access modifiers changed from: protected */
    public void addAsyncTask(AsyncTask<?, ?, ?> task) {
        this.loadDataAsyncTasks.add(task);
    }

    /* access modifiers changed from: protected */
    public int getActivityDialogPattern() {
        return 0;
    }

    public void onBackPressed() {
        if (this.moduleModel == null || getActivityDialogPattern() != 0) {
            super.onBackPressed();
            overridePendingTransition(this.mcResource.getAnimId("left_in"), this.mcResource.getAnimId("right_out"));
        } else if (!this.application.getActivityObservable().notifyActivityDestory(this.activityObserver)) {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.forumReceiver.regBroadcastReceiver();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.forumReceiver.unregBroadcastReceiver();
    }

    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(this.mcResource.getAnimId("right_in"), this.mcResource.getAnimId("left_out"));
    }

    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(this.mcResource.getAnimId("right_in"), this.mcResource.getAnimId("left_out"));
    }

    public void finish() {
        super.finish();
        overridePendingTransition(this.mcResource.getAnimId("left_in"), this.mcResource.getAnimId("right_out"));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        clearAsyncTask();
        this.application.getActivityObservable().unregisterObserver(this.activityObserver);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        this.application.getActivityObservable().notifyActivitySaveInstanceState(this.activityObserver, outState);
    }
}
