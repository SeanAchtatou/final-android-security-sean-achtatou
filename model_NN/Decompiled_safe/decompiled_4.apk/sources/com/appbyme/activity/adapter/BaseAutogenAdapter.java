package com.appbyme.activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import com.appbyme.activity.application.AutogenApplication;
import com.appbyme.activity.constant.ConfigConstant;
import com.appbyme.activity.constant.FinalConstant;
import com.appbyme.activity.constant.IntentConstant;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.forum.android.util.MCResource;

public abstract class BaseAutogenAdapter extends BaseAdapter implements MCConstant, IntentConstant, FinalConstant, ConfigConstant {
    protected String ADAPTER_TAG;
    protected String adTag = null;
    protected AutogenApplication application;
    protected Context context;
    protected LayoutInflater inflater;
    protected Handler mHandler;
    protected MCResource mcResource;
    protected Resources resources;

    public BaseAutogenAdapter(Context context2) {
        this.mcResource = MCResource.getInstance(context2);
        this.ADAPTER_TAG = getClass().getSimpleName();
        this.mHandler = new Handler();
        this.context = context2;
        this.inflater = LayoutInflater.from(context2);
        this.resources = context2.getResources();
        this.application = (AutogenApplication) ((Activity) context2).getApplication();
    }
}
