package com.appbyme.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.widget.ImageView;
import com.appbyme.activity.GoodsDetailActivity;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.android.base.model.GoodsModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;

public abstract class BaseListFragmentAdapter extends BaseAutogenAdapter {
    public BaseListFragmentAdapter(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void startDetailActivity(long topicId, GoodsModel goodsModel) {
        Intent intent = new Intent(this.context, GoodsDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putLong("topicId", topicId);
        bundle.putSerializable(IntentConstant.INTENT_EC_GOODSMODEL, goodsModel);
        intent.putExtras(bundle);
        this.context.startActivity(intent);
    }

    public void loadImageByUrl(final ImageView img) {
        final String picUrl = img.getTag() == null ? "" : img.getTag().toString();
        AsyncTaskLoaderImage.getInstance(this.context).loadAsync(picUrl, new AsyncTaskLoaderImage.BitmapImageCallback() {
            public void onImageLoaded(final Bitmap bitmap, String url) {
                BaseListFragmentAdapter.this.mHandler.post(new Runnable() {
                    public void run() {
                        if (bitmap != null && !bitmap.isRecycled() && img.isShown()) {
                            TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), new BitmapDrawable(BaseListFragmentAdapter.this.resources, bitmap)});
                            td.startTransition(350);
                            if (img.getTag().equals(picUrl)) {
                                img.setImageDrawable(td);
                            }
                        }
                    }
                });
            }
        });
    }
}
