package com.appbyme.activity.constant;

public interface FinalConstant {
    public static final int AUTOGEN_STYLE1 = 1;
    public static final int AUTOGEN_STYLE2 = 2;
    public static final int AUTOGEN_STYLE3 = 3;
    public static final int CLASSIFY_MARKING = 10003;
    public static final int DEFAULT_PAGE_SIZE = 15;
    public static final long FAVOR_ID = -12;
    public static final int GOODSCOMMENT_MARKING = 10004;
    public static final int GOODSFAVOR_MARKING = 10005;
    public static final int HOT_MARKING = 1001;
    public static final int LOAD_ID = 3;
    public static final long MESSAGE_ID = -14;
    public static final int MORE_ID = 2;
    public static final int NEWEST_MARKING = 1002;
    public static final long PUBLISH_ID = -13;
    public static final int REFRESH_ID = 1;
    public static final long REPLY_ID = -11;
    public static final String RESOLUTION_400x600 = "400x600";
    public static final long SEARCH_ID = -10;
}
