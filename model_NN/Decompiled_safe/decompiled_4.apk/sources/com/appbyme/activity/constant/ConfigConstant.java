package com.appbyme.activity.constant;

public interface ConfigConstant {
    public static final int CLASSIFY_ID = 16;
    public static final int FORUM_ID = 1;
    public static final int HOT_ID = 14;
    public static final int NEWEST_ID = 15;
    public static final int PUBLISH_NAV_STYLE = 99;
    public static final int SQUARE_ID = 9;
}
