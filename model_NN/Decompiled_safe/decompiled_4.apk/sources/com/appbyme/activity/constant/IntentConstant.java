package com.appbyme.activity.constant;

public interface IntentConstant {
    public static final String INTENT_BOARD_NAME = "boardName";
    public static final String INTENT_CATEGORY_LIST = "categoryList";
    public static final String INTENT_CHANNEL_POSITION = "channelPosition";
    public static final String INTENT_COMMENT_BOARDID = "boardId";
    public static final String INTENT_COMMENT_REPLY_POSTSID = "topicName";
    public static final String INTENT_COMMENT_TOPICID = "topicId";
    public static final String INTENT_COMMENT_TOPICNAME = "topicName";
    public static final String INTENT_DETAIL_POSITION = "detailPosition";
    public static final String INTENT_EC_GOODSDETAILMODEL = "goodsDetailModel";
    public static final String INTENT_EC_GOODSMODEL = "goodsModel";
    public static final String INTENT_ISHIDE_PUBLISH = "isHidePublish";
    public static final String INTENT_LISTVIEW_POSITION = "position";
    public static final String INTENT_MODULEMODEL = "moduleModel";
    public static final String INTENT_MODULE_MARKING = "marking";
    public static final String INTENT_WHICH_CATEGORY = "whichCategory";
    public static final String NUMIID = "numIid";
    public static final String SAVE_INSTANCE_BOARD_ID = "boardId";
    public static final String SAVE_INSTANCE_BOARD_MODEL = "boardModel";
    public static final String SAVE_INSTANCE_CONFIG_MODEL = "configModel";
    public static final String SAVE_INSTANCE_DATACACHE_MODEL = "dateCache";
    public static final String SAVE_INSTANCE_USER_ID = "user_id";
    public static final String WEB_VIEW_URL = "webViewUrl";
}
