package com.appbyme.activity.exception;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Process;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.forum.android.service.impl.FeedBackServiceImpl;
import com.mobcent.forum.android.util.MCLogUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Properties;
import java.util.TreeSet;

public class CrashHandler implements Thread.UncaughtExceptionHandler {
    private static final String CRASH_REPORTER_EXTENSION = ".cr";
    public static final boolean DEBUG = true;
    private static final String STACK_TRACE = "STACK_TRACE";
    public static final String TAG = "CrashHandler";
    private static final String VERSION_CODE = "versionCode";
    private static final String VERSION_NAME = "versionName";
    private static CrashHandler instance;
    /* access modifiers changed from: private */
    public Context mContext;
    private Thread.UncaughtExceptionHandler mDefaultHandler;
    private Properties mDeviceCrashInfo = new Properties();

    private CrashHandler(Context context) {
        this.mContext = context;
        this.mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public static CrashHandler getInstance(Context context) {
        if (instance == null) {
            instance = new CrashHandler(context);
        }
        return instance;
    }

    public void uncaughtException(Thread thread, Throwable ex) {
        if (handleException(ex) || this.mDefaultHandler == null) {
            System.exit(10);
        } else {
            this.mDefaultHandler.uncaughtException(thread, ex);
        }
    }

    private boolean handleException(final Throwable ex) {
        if (ex == null) {
            return true;
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        final StringBuffer buffer = stringWriter.getBuffer();
        ex.printStackTrace(writer);
        new Thread() {
            public void run() {
                new FeedBackServiceImpl().feedBack(CrashHandler.this.mContext, ex.toString(), buffer.toString(), 16, MCConstant.VERSION);
                Process.killProcess(Process.myPid());
            }
        }.start();
        return false;
    }

    public void collectCrashDeviceInfo(Context ctx) {
        try {
            PackageInfo pi = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 1);
            if (pi != null) {
                this.mDeviceCrashInfo.put(VERSION_NAME, pi.versionName == null ? "not set" : pi.versionName);
                this.mDeviceCrashInfo.put("versionCode", Integer.valueOf(pi.versionCode));
            }
        } catch (PackageManager.NameNotFoundException e) {
            MCLogUtil.e("CrashHandler", "Error while collect package info");
        }
        for (Field field : Build.class.getDeclaredFields()) {
            try {
                field.setAccessible(true);
                this.mDeviceCrashInfo.put(field.getName(), field.get(null));
                MCLogUtil.d("CrashHandler", field.getName() + " : " + field.get(null));
            } catch (Exception e2) {
                MCLogUtil.e("CrashHandler", "Error while collect crash info");
            }
        }
    }

    private String saveCrashInfoToFile(Throwable ex) {
        Writer info = new StringWriter();
        PrintWriter printWriter = new PrintWriter(info);
        ex.printStackTrace(printWriter);
        for (Throwable cause = ex.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        String result = info.toString();
        printWriter.close();
        this.mDeviceCrashInfo.put(STACK_TRACE, result);
        try {
            String fileName = "crash-" + System.currentTimeMillis() + CRASH_REPORTER_EXTENSION;
            FileOutputStream trace = this.mContext.openFileOutput(fileName, 0);
            this.mDeviceCrashInfo.store(trace, "");
            trace.flush();
            trace.close();
            return fileName;
        } catch (Exception e) {
            MCLogUtil.e("CrashHandler", "an error occured while writing report file...");
            return null;
        }
    }

    private void sendCrashReportsToServer(Context ctx) {
        String[] crFiles = getCrashReportFiles(ctx);
        if (crFiles != null && crFiles.length > 0) {
            TreeSet<String> sortedFiles = new TreeSet<>();
            sortedFiles.addAll(Arrays.asList(crFiles));
            Iterator i$ = sortedFiles.iterator();
            while (i$.hasNext()) {
                File cr = new File(ctx.getFilesDir(), (String) i$.next());
                postReport(cr);
                cr.delete();
            }
        }
    }

    private String[] getCrashReportFiles(Context ctx) {
        return ctx.getFilesDir().list(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(CrashHandler.CRASH_REPORTER_EXTENSION);
            }
        });
    }

    private void postReport(File file) {
    }

    public void sendPreviousReportsToServer() {
    }
}
