package com.appbyme.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.fragment.BaseFragment;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.ForumServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.plaza.android.ui.activity.IntentPlazaModel;
import com.mobcent.plaza.android.ui.activity.PlazaSearchActivity;
import com.mobcent.plaza.android.ui.activity.delegate.PlazaConfig;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseListActivity extends BaseFragmentActivity {
    private String TAG = "BaseListActivity";
    protected int currentPosition = 0;
    protected FragmentStatePagerAdapter displayActivityAdapter;
    protected Intent intent;
    private List<Integer> positionList;

    public interface TopbarListener {
        void setTopbarTitle(String str);
    }

    /* access modifiers changed from: protected */
    public void getActivityArguments() {
        this.intent = getIntent();
        this.moduleModel = (AutogenModuleModel) this.intent.getSerializableExtra(IntentConstant.INTENT_MODULEMODEL);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        getActivityArguments();
        this.positionList = new ArrayList();
    }

    /* access modifiers changed from: protected */
    public void initActions() {
    }

    /* access modifiers changed from: protected */
    public Bundle getBundle(int position) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(IntentConstant.SAVE_INSTANCE_CONFIG_MODEL, this.moduleModel);
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void loadCurrentFragmentData() {
        BaseFragment baseFragment = (BaseFragment) this.displayActivityAdapter.getItem(this.currentPosition);
        if (!this.positionList.contains(Integer.valueOf(this.currentPosition))) {
            this.positionList.add(Integer.valueOf(this.currentPosition));
            baseFragment.loadDataByNet();
        }
    }

    /* access modifiers changed from: protected */
    public void goodsSearch() {
        if (PlazaConfig.getInstance().getPlazaDelegate() != null) {
            long userId = PlazaConfig.getInstance().getPlazaDelegate().getUserId(this);
            if (userId > 0) {
                ForumServiceImpl forumServiceImpl = new ForumServiceImpl();
                UserService userService = new UserServiceImpl(this.context);
                SharedPreferencesDB shareDB = SharedPreferencesDB.getInstance(this.context);
                IntentPlazaModel intentModel = new IntentPlazaModel();
                intentModel.setForumId(shareDB.getForumid());
                intentModel.setForumKey(forumServiceImpl.getForumKey(this.context));
                intentModel.setUserId(userService.getLoginUserId());
                List<Integer> searchTypeList = this.application.getConfigModel().getSearchTypes();
                int typeSize = searchTypeList.size();
                Integer[] array = new Integer[typeSize];
                searchTypeList.toArray(array);
                int[] typeArray = new int[typeSize];
                int len = array.length;
                for (int i = 0; i < len; i++) {
                    typeArray[i] = array[i].intValue();
                }
                intentModel.setSearchTypes(new int[]{16});
                intentModel.setUserId(userId);
                Intent intent2 = new Intent(this, PlazaSearchActivity.class);
                intent2.putExtra("plazaIntentModel", intentModel);
                startActivity(intent2);
            }
        }
    }
}
