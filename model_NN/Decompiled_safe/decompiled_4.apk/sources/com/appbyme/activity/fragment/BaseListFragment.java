package com.appbyme.activity.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.appbyme.activity.constant.FinalConstant;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.mobcent.forum.android.model.BoardModel;

public abstract class BaseListFragment extends BaseFragment implements IntentConstant, FinalConstant {
    protected Bundle args;
    protected BoardModel boardModel;
    protected boolean isRefresh = false;
    protected AutogenModuleModel moduleModel;

    public abstract void loadDataByNet();

    public abstract void loadMoreData();

    public void getActivityArguments() {
        this.args = getArguments();
        if (this.args != null) {
            this.moduleModel = (AutogenModuleModel) this.args.getSerializable(IntentConstant.SAVE_INSTANCE_CONFIG_MODEL);
            this.boardModel = (BoardModel) this.args.getSerializable(IntentConstant.SAVE_INSTANCE_BOARD_MODEL);
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        getActivityArguments();
    }

    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return null;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
    }
}
