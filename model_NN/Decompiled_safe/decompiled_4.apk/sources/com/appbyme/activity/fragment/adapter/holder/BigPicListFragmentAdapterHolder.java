package com.appbyme.activity.fragment.adapter.holder;

import android.widget.ImageView;
import android.widget.TextView;

public class BigPicListFragmentAdapterHolder {
    private TextView goodsName;
    private ImageView goodsPic;
    private TextView goodsPrice;
    private float ratio;

    public ImageView getGoodsPic() {
        return this.goodsPic;
    }

    public void setGoodsPic(ImageView goodsPic2) {
        this.goodsPic = goodsPic2;
    }

    public TextView getGoodsName() {
        return this.goodsName;
    }

    public void setGoodsName(TextView goodsName2) {
        this.goodsName = goodsName2;
    }

    public TextView getGoodsPrice() {
        return this.goodsPrice;
    }

    public void setGoodsPrice(TextView goodsPrice2) {
        this.goodsPrice = goodsPrice2;
    }

    public float getRatio() {
        return this.ratio;
    }

    public void setRatio(float ratio2) {
        this.ratio = ratio2;
    }
}
