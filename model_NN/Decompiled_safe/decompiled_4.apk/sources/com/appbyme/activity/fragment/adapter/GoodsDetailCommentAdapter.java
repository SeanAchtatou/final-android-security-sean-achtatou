package com.appbyme.activity.fragment.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.appbyme.activity.adapter.BaseListFragmentAdapter;
import com.appbyme.android.base.model.GoodsCommentDetail;
import com.mobcent.forum.android.util.StringUtil;
import java.util.List;

public class GoodsDetailCommentAdapter extends BaseListFragmentAdapter {
    private Context context;
    private List<GoodsCommentDetail> goodsCommentDetaiList;

    public GoodsDetailCommentAdapter(Context context2, List<GoodsCommentDetail> goodsCommentDetaiList2) {
        super(context2);
        this.context = context2;
        this.goodsCommentDetaiList = goodsCommentDetaiList2;
    }

    public int getCount() {
        return this.goodsCommentDetaiList.size();
    }

    public Object getItem(int position) {
        return this.goodsCommentDetaiList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        CommentHolder holder;
        GoodsCommentDetail goodsCommentDetai = (GoodsCommentDetail) getItem(position);
        if (convertView == null) {
            convertView = this.inflater.inflate(this.mcResource.getLayoutId("mc_ec_goods_detail_tag2_item"), (ViewGroup) null);
            holder = new CommentHolder();
            initCommentFragmentAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (CommentHolder) convertView.getTag();
        }
        updataCommentAdapterHodler(goodsCommentDetai, holder);
        return convertView;
    }

    private void updataCommentAdapterHodler(GoodsCommentDetail goodsCommentDetai, CommentHolder holder) {
        if (goodsCommentDetai != null) {
            if (!StringUtil.isEmpty(goodsCommentDetai.getBuyer())) {
                holder.getName().setText(goodsCommentDetai.getBuyer() + "");
            }
            if (!StringUtil.isEmpty(goodsCommentDetai.getText())) {
                holder.getContent().setText(goodsCommentDetai.getText());
            }
        }
    }

    private void initCommentFragmentAdapterHolder(View convertView, CommentHolder holder) {
        holder.setName((TextView) convertView.findViewById(this.mcResource.getViewId("mc_ec_goods_detail_comment_name")));
        holder.setContent((TextView) convertView.findViewById(this.mcResource.getViewId("mc_ec_goods_detail_comment_content")));
    }

    class CommentHolder {
        private TextView content;
        private TextView name;

        CommentHolder() {
        }

        public TextView getName() {
            return this.name;
        }

        public void setName(TextView name2) {
            this.name = name2;
        }

        public TextView getContent() {
            return this.content;
        }

        public void setContent(TextView content2) {
            this.content = content2;
        }
    }
}
