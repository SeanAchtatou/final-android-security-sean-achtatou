package com.appbyme.activity.fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.appbyme.activity.BaseListActivity;
import com.appbyme.activity.application.AutogenApplication;
import com.appbyme.activity.constant.FinalConstant;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.android.base.model.GoodsDetailModel;
import com.appbyme.android.service.ClassifyService;
import com.appbyme.android.service.HotService;
import com.appbyme.android.service.NewestService;
import com.appbyme.android.service.PersonalService;
import com.appbyme.android.service.impl.ClassifyServiceImpl;
import com.appbyme.android.service.impl.HotServiceImpl;
import com.appbyme.android.service.impl.NewestServiceImpl;
import com.appbyme.android.service.impl.PersonalServiceImpl;
import com.appbyme.widget.GoodsDetailListView;
import com.mobcent.ad.android.ui.widget.AdView;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.util.MCLogUtil;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseFragment extends Fragment implements IntentConstant, FinalConstant {
    protected String FRAGMENT_TAG;
    private String TAG = "BaseFragment";
    protected Activity activity;
    protected String adTag = null;
    protected AdView adView = null;
    protected AutogenApplication application;
    private Bundle bundle;
    private int cacheDb;
    protected ClassifyService classifyService;
    protected GoodsDetailListView detailListView;
    protected GoodsDetailModel goodsDetailModel;
    protected HotService hotService;
    protected LayoutInflater inflater;
    protected boolean isRefresh = false;
    protected List<AsyncTask<?, ?, ?>> loadDataAsyncTasks;
    protected Handler mHandler;
    protected int marking;
    protected MCResource mcResource;
    protected NewestService newestService;
    protected PersonalService personalService;
    protected Toast toast;
    protected BaseListActivity.TopbarListener topbarListener;

    /* access modifiers changed from: protected */
    public abstract void initData();

    /* access modifiers changed from: protected */
    public abstract View initViews(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle2);

    /* access modifiers changed from: protected */
    public abstract void initWidgetActions();

    public abstract void loadDataByNet();

    public abstract void loadMoreData();

    public void setSVIsBottom(boolean sVIsBottom) {
        this.detailListView.setSVIsBottom(sVIsBottom);
    }

    public void setLVIsTop(boolean isTop) {
        this.detailListView.setLVIsTop(isTop);
    }

    public void onAttach(Activity activity2) {
        super.onAttach(activity2);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.bundle = getArguments();
        this.goodsDetailModel = (GoodsDetailModel) this.bundle.getSerializable(IntentConstant.INTENT_EC_GOODSDETAILMODEL);
        this.activity = getActivity();
        this.application = (AutogenApplication) this.activity.getApplication();
        this.FRAGMENT_TAG = getClass().getSimpleName();
        this.mHandler = new Handler();
        this.inflater = LayoutInflater.from(this.activity);
        this.mcResource = MCResource.getInstance(this.activity);
        this.toast = Toast.makeText(this.activity, "", 0);
    }

    public View onCreateView(LayoutInflater inflater2, ViewGroup container, Bundle savedInstanceState) {
        initData();
        View view = initViews(inflater2, container, savedInstanceState);
        initWidgetActions();
        return view;
    }

    public void showMessage(String str) {
        this.toast.setText(str);
        this.toast.show();
    }

    /* access modifiers changed from: protected */
    public void clearAsyncTask() {
        if (this.loadDataAsyncTasks != null) {
            int size = this.loadDataAsyncTasks.size();
            for (int i = 0; i < size; i++) {
                this.loadDataAsyncTasks.get(i).cancel(true);
            }
            this.loadDataAsyncTasks.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void addAsyncTask(AsyncTask<?, ?, ?> asyncTask) {
        if (this.loadDataAsyncTasks == null) {
            this.loadDataAsyncTasks = new ArrayList();
        }
        this.loadDataAsyncTasks.add(asyncTask);
    }

    public void onDestroyView() {
        super.onDestroyView();
        clearAsyncTask();
    }

    public void recycleImage() {
    }

    /* access modifiers changed from: protected */
    public void initService(int marking2) {
        MCLogUtil.e(this.TAG + "---->initService", "marking = " + marking2);
        switch (marking2) {
            case FinalConstant.HOT_MARKING:
                this.hotService = new HotServiceImpl(this.activity);
                return;
            case 1002:
                this.newestService = new NewestServiceImpl(this.activity);
                return;
            case 10003:
                this.classifyService = new ClassifyServiceImpl(this.activity);
                return;
            case FinalConstant.GOODSCOMMENT_MARKING:
                this.personalService = new PersonalServiceImpl(this.activity);
                return;
            case FinalConstant.GOODSFAVOR_MARKING:
                this.personalService = new PersonalServiceImpl(this.activity);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public int getCacheDB() {
        switch (this.marking) {
            case FinalConstant.HOT_MARKING:
                this.cacheDb = this.hotService.getCacheDB();
                break;
            case 1002:
                this.cacheDb = this.newestService.getCacheDB();
                break;
            case 10003:
                this.cacheDb = this.classifyService.getCacheDB();
                break;
        }
        return this.cacheDb;
    }

    /* access modifiers changed from: protected */
    public boolean getLocalFlag() {
        switch (this.marking) {
            case FinalConstant.HOT_MARKING:
                return this.hotService.getHotByLocal();
            case 1002:
                return this.newestService.getNewestByLocal();
            case 10003:
                return this.classifyService.getClassifyByLocal();
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public String getRefreshDate() {
        switch (this.marking) {
            case FinalConstant.HOT_MARKING:
                return this.hotService.getRefreshDate();
            case 1002:
                return this.newestService.getRefreshDate();
            case 10003:
                return this.classifyService.getRefreshDate();
            case FinalConstant.GOODSCOMMENT_MARKING:
                return this.personalService.getRefreshData();
            case FinalConstant.GOODSFAVOR_MARKING:
                return this.personalService.getRefreshData();
            default:
                return null;
        }
    }
}
