package com.appbyme.activity.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import com.appbyme.activity.adapter.BaseListFragmentAdapter;
import com.appbyme.android.base.model.GoodsImgDetailModel;
import com.appbyme.widget.GoodsDetailListView;
import com.appbyme.widget.GoodsDetailScrollView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.fragmentActivity.ImageViewerFragmentActivity;
import com.mobcent.base.forum.android.util.MCPhoneUtil;
import com.mobcent.forum.android.constant.MCForumConstant;
import com.mobcent.forum.android.model.RichImageModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.PhoneUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class GoodsImgFragment extends BaseFragment {
    private String TAG = "GoodsImgFragment";
    private GoodsDetailScrollView detailScrollView;
    private List<GoodsImgDetailModel> goodsImgDetailModelList;
    /* access modifiers changed from: private */
    public int imageWidth = 0;
    private ImgAdapter imgAdapter;
    /* access modifiers changed from: private */
    public boolean isScrolling = false;
    /* access modifiers changed from: private */
    public Map<String, Float> ratioMaps;
    /* access modifiers changed from: private */
    public ArrayList<RichImageModel> richImageList;

    public GoodsImgFragment(Context context, GoodsDetailScrollView detailScrollView2) {
        this.detailScrollView = detailScrollView2;
        this.ratioMaps = new HashMap();
        this.imageWidth = MCPhoneUtil.getDisplayWidth((Activity) context);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.goodsImgDetailModelList = this.goodsDetailModel.getGoodsImgDetailModelList();
        if (this.goodsImgDetailModelList == null) {
            this.goodsImgDetailModelList = new ArrayList();
        }
        this.richImageList = new ArrayList<>();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(this.mcResource.getLayoutId("mc_ec_goods_detail_tag"), container, false);
        this.detailListView = (GoodsDetailListView) view.findViewById(this.mcResource.getViewId("mc_ec_goods_detail_tag_content"));
        this.detailListView.setmScrollView(this.detailScrollView);
        this.imgAdapter = new ImgAdapter(this.activity, this.goodsImgDetailModelList);
        this.detailListView.setAdapter((ListAdapter) this.imgAdapter);
        return view;
    }

    private class ImgAdapter extends BaseListFragmentAdapter {
        List<GoodsImgDetailModel> list;

        public ImgAdapter(Context context, List<GoodsImgDetailModel> list2) {
            super(context);
            Map<Integer, GoodsImgDetailModel> imgModelMap = new TreeMap<>();
            int num = list2.size();
            for (int i = 0; i < num; i++) {
                GoodsImgDetailModel goodsImgDetailModel = list2.get(i);
                if (goodsImgDetailModel == null) {
                    goodsImgDetailModel = new GoodsImgDetailModel();
                }
                imgModelMap.put(Integer.valueOf(goodsImgDetailModel.getPosition()), goodsImgDetailModel);
            }
            list2.clear();
            for (int j = 0; j < num; j++) {
                list2.add(j, imgModelMap.get(Integer.valueOf(j)));
            }
            list2.add(0, new GoodsImgDetailModel());
            this.list = list2;
        }

        public int getCount() {
            return this.list.size();
        }

        public Object getItem(int position) {
            return this.list.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            GoodsDetailItemImgAdapterHolder holder;
            GoodsImgDetailModel goodsImgDetailModel = (GoodsImgDetailModel) getItem(position);
            if (convertView == null) {
                convertView = this.inflater.inflate(this.mcResource.getLayoutId("mc_ec_goods_detail_tag1_item"), (ViewGroup) null);
                holder = new GoodsDetailItemImgAdapterHolder();
                initDetailImgFragmentAdapterHolder(convertView, holder, goodsImgDetailModel);
                convertView.setTag(holder);
            } else {
                holder = (GoodsDetailItemImgAdapterHolder) convertView.getTag();
            }
            updateDetailImgFragmentAdapterHolder(holder, this.list, position);
            onClickItem(holder, position);
            return convertView;
        }

        private void onClickItem(GoodsDetailItemImgAdapterHolder holder, final int position) {
            holder.getItemImg().setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ImgAdapter.this.clickImg(v, AsyncTaskLoaderImage.formatUrl(ImgAdapter.this.list.get(position).getUrl(), MCForumConstant.RESOLUTION_640X480));
                }
            });
        }

        /* access modifiers changed from: private */
        public void clickImg(View v, String whichClick) {
            GoodsImgFragment.this.richImageList.clear();
            int len = this.list.size();
            for (int i = 0; i < len; i++) {
                if (this.list.get(i) != null) {
                    String imgUrl = AsyncTaskLoaderImage.formatUrl(this.list.get(i).getUrl(), MCForumConstant.RESOLUTION_640X480);
                    if (!StringUtil.isEmpty(imgUrl)) {
                        RichImageModel richImageModel = new RichImageModel();
                        richImageModel.setImageUrl(imgUrl);
                        GoodsImgFragment.this.richImageList.add(richImageModel);
                    }
                }
            }
            Intent intent = new Intent(this.context, ImageViewerFragmentActivity.class);
            intent.putExtra(MCConstant.IMAGE_URL, whichClick);
            intent.putExtra(MCConstant.RICH_IMAGE_LIST, GoodsImgFragment.this.richImageList);
            intent.putExtra(MCConstant.IMAGE_SOURCE_TYPE, 1);
            this.context.startActivity(intent);
        }

        private void updateDetailImgFragmentAdapterHolder(GoodsDetailItemImgAdapterHolder holder, List<GoodsImgDetailModel> list2, int position) {
            GoodsImgDetailModel goodsImgDetailModel = list2.get(position);
            if (goodsImgDetailModel != null) {
                String imgUrl = goodsImgDetailModel.getUrl();
                if (position == 0) {
                    holder.getItemImg().setLayoutParams(new RelativeLayout.LayoutParams(PhoneUtil.getDisplayWidth(this.context), 1));
                } else {
                    int screenWidth = PhoneUtil.getDisplayWidth(this.context);
                    holder.getItemImg().setLayoutParams(new RelativeLayout.LayoutParams(screenWidth, screenWidth));
                }
                GoodsImgFragment.this.setImage(holder, imgUrl);
            }
        }

        private void initDetailImgFragmentAdapterHolder(View convertView, GoodsDetailItemImgAdapterHolder holder, GoodsImgDetailModel goodsImgDetailModel) {
            holder.setItemImg((ImageView) convertView.findViewById(this.mcResource.getViewId("mc_ec_goods_detail_img_item")));
        }
    }

    /* access modifiers changed from: private */
    public void setImage(final GoodsDetailItemImgAdapterHolder holder, String imgUrl) {
        AsyncTaskLoaderImage.getInstance(this.activity).loadAsync(imgUrl, new AsyncTaskLoaderImage.BitmapImageCallback() {
            public void onImageLoaded(final Bitmap bitmap, final String url) {
                GoodsImgFragment.this.mHandler.post(new Runnable() {
                    public void run() {
                        if (bitmap != null && !bitmap.isRecycled() && holder.getItemImg().isShown()) {
                            GoodsImgFragment.this.ratioMaps.put(url, Float.valueOf(Float.valueOf((float) bitmap.getWidth()).floatValue() / Float.valueOf((float) bitmap.getHeight()).floatValue()));
                            holder.getItemImg().setLayoutParams(new RelativeLayout.LayoutParams(GoodsImgFragment.this.imageWidth, (int) (((float) GoodsImgFragment.this.imageWidth) / ((Float) GoodsImgFragment.this.ratioMaps.get(url)).floatValue())));
                            TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), new BitmapDrawable(GoodsImgFragment.this.getResources(), bitmap)});
                            td.startTransition(350);
                            holder.getItemImg().setImageDrawable(td);
                        }
                    }
                });
            }
        });
    }

    public class GoodsDetailItemImgAdapterHolder {
        private ImageView itemImg;

        public GoodsDetailItemImgAdapterHolder() {
        }

        public ImageView getItemImg() {
            return this.itemImg;
        }

        public void setItemImg(ImageView itemImg2) {
            this.itemImg = itemImg2;
        }
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.detailListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case 0:
                        boolean unused = GoodsImgFragment.this.isScrolling = false;
                        return;
                    case 1:
                        boolean unused2 = GoodsImgFragment.this.isScrolling = false;
                        return;
                    case 2:
                        boolean unused3 = GoodsImgFragment.this.isScrolling = true;
                        return;
                    default:
                        return;
                }
            }

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem != 0 || view.getChildAt(0) == null || view.getChildAt(0).getTop() >= 3) {
                    GoodsImgFragment.this.setLVIsTop(false);
                } else {
                    GoodsImgFragment.this.setLVIsTop(true);
                }
            }
        });
    }

    public void recycleImage() {
        if (this.detailListView != null) {
            int count = this.detailListView.getCount();
            for (int i = 0; i < count; i++) {
                View view = this.detailListView.getChildAt(i);
                if (view != null && (view.getTag() instanceof GoodsDetailItemImgAdapterHolder)) {
                    GoodsDetailItemImgAdapterHolder holder = (GoodsDetailItemImgAdapterHolder) view.getTag();
                    holder.getItemImg().setImageBitmap(null);
                    holder.getItemImg().setImageDrawable(null);
                }
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        recycleImage();
    }

    public void loadDataByNet() {
    }

    public void loadMoreData() {
    }
}
