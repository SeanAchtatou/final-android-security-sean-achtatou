package com.appbyme.activity.fragment.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.appbyme.activity.adapter.BaseListFragmentAdapter;
import com.appbyme.activity.fragment.adapter.holder.BigPicListFragmentAdapterHolder;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.appbyme.android.base.model.GoodsModel;
import com.mobcent.forum.android.constant.MCForumConstant;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.PhoneUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.List;

public class BigPicListFragmentAdapter extends BaseListFragmentAdapter {
    public String TAG = "BigPicListFragmentAdapter";
    private List<GoodsModel> goodsList;
    private boolean isScrolling;
    private AutogenModuleModel moduleModel;

    public boolean isScrolling() {
        return this.isScrolling;
    }

    public void setScrolling(boolean isScrolling2) {
        this.isScrolling = isScrolling2;
    }

    public BigPicListFragmentAdapter(Context context, List<GoodsModel> goodsList2, AutogenModuleModel moduleModel2, String adTag) {
        super(context);
        this.goodsList = goodsList2;
        this.moduleModel = moduleModel2;
        this.adTag = adTag;
    }

    public void setGoodsList(List<GoodsModel> hotList) {
        this.goodsList = hotList;
    }

    public int getCount() {
        return this.goodsList.size();
    }

    public Object getItem(int position) {
        return this.goodsList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        BigPicListFragmentAdapterHolder holder;
        GoodsModel goodsModel = (GoodsModel) getItem(position);
        if (convertView == null) {
            convertView = this.inflater.inflate(this.mcResource.getLayoutId("mc_ec_big_pic_fragment_item"), (ViewGroup) null);
            holder = new BigPicListFragmentAdapterHolder();
            initHotListFragmentAdapterHolder(convertView, holder, goodsModel);
            convertView.setTag(holder);
        } else {
            holder = (BigPicListFragmentAdapterHolder) convertView.getTag();
        }
        updateHotListFragmentAdapterHolder(holder, goodsModel, position);
        onClickBigPicListFragmentAdapterHolder(holder, goodsModel);
        return convertView;
    }

    private void initHotListFragmentAdapterHolder(View convertView, BigPicListFragmentAdapterHolder holder, GoodsModel goodsModel) {
        ImageView goodsPic = (ImageView) convertView.findViewById(this.mcResource.getViewId("mc_ec_big_pic_goods_pic"));
        int[] imgParams = calcImgHight(goodsModel);
        ViewGroup.LayoutParams imgLp = goodsPic.getLayoutParams();
        imgLp.width = imgParams[0];
        imgLp.height = imgParams[1];
        goodsPic.setLayoutParams(imgLp);
        holder.setGoodsPic(goodsPic);
        holder.setGoodsName((TextView) convertView.findViewById(this.mcResource.getViewId("mc_ec_big_pic_goods_name")));
        holder.setGoodsPrice((TextView) convertView.findViewById(this.mcResource.getViewId("mc_ec_big_pic_goods_price")));
    }

    private int[] calcImgHight(GoodsModel goodsModel) {
        float ratio = goodsModel.getRatio();
        if (ratio == 0.0f) {
            ratio = 1.0f;
        }
        int imgWidth = PhoneUtil.getDisplayWidth(this.context);
        return new int[]{imgWidth, Float.valueOf(((float) imgWidth) * ratio).intValue()};
    }

    private void updateHotListFragmentAdapterHolder(BigPicListFragmentAdapterHolder holder, GoodsModel goodsModel, int position) {
        holder.getGoodsName().setText(goodsModel.getTitle());
        if (!StringUtil.isEmpty(goodsModel.getPrice())) {
            holder.getGoodsPrice().setText(this.mcResource.getString("mc_ec_comment_money_symbol") + goodsModel.getPrice());
        }
        holder.getGoodsPic().setBackgroundResource(this.mcResource.getRawId("mc_forum_x_img"));
        holder.getGoodsPic().setTag(AsyncTaskLoaderImage.formatUrl(goodsModel.getPicPath(), MCForumConstant.RESOLUTION_640X480));
        ImageView iv = holder.getGoodsPic();
        iv.setImageDrawable(null);
        if (!this.isScrolling) {
            loadImageByUrl(iv);
        }
    }

    private void onClickBigPicListFragmentAdapterHolder(BigPicListFragmentAdapterHolder holder, final GoodsModel goodsModel) {
        holder.getGoodsPic().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigPicListFragmentAdapter.this.startDetailActivity(goodsModel.getTopicId(), goodsModel);
            }
        });
        holder.getGoodsName().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BigPicListFragmentAdapter.this.startDetailActivity(goodsModel.getTopicId(), goodsModel);
            }
        });
    }
}
