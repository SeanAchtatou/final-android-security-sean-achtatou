package com.appbyme.activity.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.appbyme.activity.GoodsDetailCommentActivity;
import com.appbyme.activity.fragment.adapter.GoodsDetailCommentAdapter;
import com.appbyme.android.base.model.GoodsCommentDetail;
import com.appbyme.android.base.model.GoodsCommentsModel;
import com.appbyme.widget.GoodsDetailListView;
import com.appbyme.widget.GoodsDetailScrollView;
import java.util.ArrayList;
import java.util.List;

public class GoodsCommentFragment extends BaseFragment {
    private String TAG = "GoodsCommentFragment";
    private GoodsDetailCommentAdapter commentAdapter;
    /* access modifiers changed from: private */
    public Context context;
    private GoodsDetailScrollView detailScrollView;
    private TextView footerText;
    private LinearLayout footerView;
    private List<GoodsCommentDetail> list;

    public GoodsCommentFragment(Context context2, GoodsDetailScrollView detailScrollView2) {
        this.detailScrollView = detailScrollView2;
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        GoodsCommentsModel goodsCommentsModel = this.goodsDetailModel.getGoodsCommentsModel();
        if (goodsCommentsModel == null) {
            goodsCommentsModel = new GoodsCommentsModel();
        }
        this.list = goodsCommentsModel.getGoodsCommentDetaiList();
        if (this.list == null) {
            this.list = new ArrayList();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(this.mcResource.getLayoutId("mc_ec_goods_detail_tag"), container, false);
        this.detailListView = (GoodsDetailListView) view.findViewById(this.mcResource.getViewId("mc_ec_goods_detail_tag_content"));
        this.detailListView.setmScrollView(this.detailScrollView);
        this.detailListView.setDivider(this.mcResource.getDrawable("mc_forum_line1"));
        this.commentAdapter = new GoodsDetailCommentAdapter(this.activity, this.list);
        this.footerView = (LinearLayout) inflater.inflate(this.mcResource.getLayoutId("mc_ec_goods_detail_comment_footer"), (ViewGroup) null);
        this.footerText = (TextView) this.footerView.findViewById(this.mcResource.getViewId("mc_ec_goods_detail_footer_comment"));
        if (this.list.size() == 0) {
            this.footerText.setText(this.mcResource.getString("mc_ec_goods_detail_comment_footer_no"));
            this.footerView.setClickable(false);
        }
        this.detailListView.addFooterView(this.footerView);
        this.detailListView.setAdapter((ListAdapter) this.commentAdapter);
        return view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.detailListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState != 0) {
                    return;
                }
                if (view.getFirstVisiblePosition() == 0 && view.getChildAt(0) != null && view.getChildAt(0).getTop() == 0) {
                    GoodsCommentFragment.this.setLVIsTop(true);
                } else {
                    GoodsCommentFragment.this.setLVIsTop(false);
                }
            }

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0 && view.getChildAt(0) != null && view.getChildAt(0).getTop() == 0) {
                    GoodsCommentFragment.this.setLVIsTop(true);
                } else {
                    GoodsCommentFragment.this.setLVIsTop(false);
                }
            }
        });
        this.footerView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                long numIid = GoodsCommentFragment.this.goodsDetailModel.getNumIid();
                Intent intent = new Intent(GoodsCommentFragment.this.context, GoodsDetailCommentActivity.class);
                intent.putExtra("numIid", numIid);
                GoodsCommentFragment.this.context.startActivity(intent);
            }
        });
        if (this.list.size() == 0) {
            this.footerView.setClickable(false);
        } else {
            this.footerView.setClickable(true);
        }
    }

    public void loadMoreData() {
    }

    public void loadDataByNet() {
    }
}
