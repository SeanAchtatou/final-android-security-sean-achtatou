package com.appbyme.activity.fragment;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.appbyme.activity.GoodsDetailActivity;
import com.appbyme.activity.constant.FinalConstant;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.android.base.model.GoodsModel;
import com.appbyme.classify.activity.ClassifyActivity;
import com.appbyme.classify.activity.delegate.FallWallDelegate;
import com.appbyme.hot.activity.HotActivity;
import com.appbyme.newest.activity.NewestActivity;
import com.mobcent.base.android.ui.activity.helper.MCImageViewerHelper;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshBase;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshWaterFall;
import com.mobcent.base.forum.android.util.ImageCache;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.constant.MCForumConstant;
import com.mobcent.forum.android.model.FlowTag;
import com.mobcent.forum.android.model.RichImageModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WaterfallFragment extends BaseFragment implements FallWallDelegate {
    private String TAG = "WaterfallFragment";
    private long boardId = -10000;
    private int cacheDb;
    protected List<GoodsModel> goodsList;
    /* access modifiers changed from: private */
    public Map<String, ImageView> imageMap;
    private PullToRefreshWaterFall.OnLoadItemListener loadItemListener = new PullToRefreshWaterFall.OnLoadItemListener() {
        public void loadLayout(LinearLayout linearLayout, FlowTag flowTag) {
        }

        public void loadImage(String imageUrl, boolean isLoad) {
            WaterfallFragment.this.loadImageByUrl(imageUrl);
        }

        public void onItemClick(int currentPosition, FlowTag flowTag) {
            WaterfallFragment.this.startDetailActivity(flowTag.getTopicId(), WaterfallFragment.this.goodsList.get(currentPosition));
        }

        public void recycleImage(String imageUrl) {
            ImageView imageView = (ImageView) WaterfallFragment.this.imageMap.get(imageUrl);
            if (imageView != null) {
                imageView.setImageDrawable(null);
            }
        }

        public View getImgBox(FlowTag flowTag) {
            ImageView imageView = new ImageView(WaterfallFragment.this.activity);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            imageView.setBackgroundResource(WaterfallFragment.this.mcResource.getDrawableId("mc_forum_list9_li_bg"));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            RelativeLayout itemView = new RelativeLayout(WaterfallFragment.this.activity);
            itemView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            itemView.addView(imageView);
            TextView priceView = new TextView(WaterfallFragment.this.activity);
            priceView.setTextColor(WaterfallFragment.this.mcResource.getColor("mc_forum_img_mask_text"));
            priceView.setPadding(3, 1, 3, 1);
            priceView.setText(WaterfallFragment.this.mcResource.getString("mc_ec_comment_money_symbol") + flowTag.getTitle());
            priceView.setBackgroundResource(WaterfallFragment.this.mcResource.getDrawableId("mc_forum_shopping_commodity_listings1_picture_bg3"));
            RelativeLayout.LayoutParams lp1 = new RelativeLayout.LayoutParams(-2, -2);
            lp1.addRule(11);
            lp1.addRule(12);
            itemView.addView(priceView, lp1);
            WaterfallFragment.this.imageMap.put(flowTag.getThumbnailUrl(), imageView);
            return itemView;
        }
    };
    private int pageSize = 15;
    /* access modifiers changed from: private */
    public PullToRefreshWaterFall pullToRefreshWaterFall;
    /* access modifiers changed from: private */
    public Resources resources;
    private RelativeLayout subNav;

    public WaterfallFragment() {
    }

    public WaterfallFragment(RelativeLayout subNav2) {
        this.subNav = subNav2;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.marking = ((Integer) getArguments().get(IntentConstant.INTENT_MODULE_MARKING)).intValue();
        if (this.marking == 10003) {
            this.boardId = ((Long) getArguments().get("boardId")).longValue();
        }
        if (this.marking == 1001) {
            this.boardId = 1001;
        }
        if (this.marking == 1002) {
            this.boardId = 1002;
        }
        this.resources = this.activity.getResources();
        this.imageMap = new HashMap();
        this.goodsList = new ArrayList();
        initTopbar();
        initService(this.marking);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(this.mcResource.getLayoutId("waterfall_display_fragment"), container, false);
        this.pullToRefreshWaterFall = (PullToRefreshWaterFall) view.findViewById(this.mcResource.getViewId("mc_ec_display_content"));
        this.pullToRefreshWaterFall.setColumnCount(2);
        this.pullToRefreshWaterFall.initView(this.activity, this.adView);
        return view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.pullToRefreshWaterFall.setOnLoadItemListener(this.loadItemListener);
        this.pullToRefreshWaterFall.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            public void onRefresh() {
                if (WaterfallFragment.this.pullToRefreshWaterFall.isHand()) {
                    WaterfallFragment.this.onRefreshEvent();
                } else if (WaterfallFragment.this.isRefresh) {
                    WaterfallFragment.this.isRefresh = false;
                    WaterfallFragment.this.onRefreshEvent();
                } else {
                    WaterfallFragment.this.onInitEvent();
                }
            }
        });
        this.pullToRefreshWaterFall.setOnBottomRefreshListener(new PullToRefreshBase.OnBottomRefreshListener() {
            public void onRefresh() {
                WaterfallFragment.this.onMoreEvent();
            }
        });
        if (this.boardId != -100000) {
            this.pullToRefreshWaterFall.onRefresh(false);
        }
    }

    public void onMoreEvent() {
        LoadDataAsyncTask loadDataAsyncTask = new LoadDataAsyncTask();
        addAsyncTask(loadDataAsyncTask);
        loadDataAsyncTask.execute(2);
    }

    /* access modifiers changed from: protected */
    public void initTopbar() {
        switch (this.marking) {
            case FinalConstant.HOT_MARKING:
                this.topbarListener = new HotActivity.MyTopbarListrner();
                return;
            case 1002:
                this.topbarListener = new NewestActivity.MyTopbarListrner();
                return;
            case 10003:
                this.topbarListener = new ClassifyActivity.MyTopbarListrner();
                return;
            default:
                return;
        }
    }

    public void onInitEvent() {
        initServiceCacheDB(false);
        LoadDataAsyncTask loadDataAsyncTask = new LoadDataAsyncTask();
        addAsyncTask(loadDataAsyncTask);
        loadDataAsyncTask.execute(3);
    }

    /* access modifiers changed from: private */
    public void startDetailActivity(long topicId, GoodsModel goodsModel) {
        Intent intent = new Intent(this.activity, GoodsDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putLong("topicId", topicId);
        bundle.putSerializable(IntentConstant.INTENT_EC_GOODSMODEL, goodsModel);
        intent.putExtras(bundle);
        this.activity.startActivity(intent);
    }

    private class LoadDataAsyncTask extends AsyncTask<Integer, Void, List<GoodsModel>> {
        private int reqId;

        private LoadDataAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<GoodsModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public List<GoodsModel> doInBackground(Integer... params) {
            this.reqId = params[0].intValue();
            if (WaterfallFragment.this.marking == 1001) {
                return WaterfallFragment.this.hotService.getHotList();
            }
            if (WaterfallFragment.this.marking == 1002) {
                return WaterfallFragment.this.newestService.getNewestList();
            }
            if (WaterfallFragment.this.marking == 10004) {
                return WaterfallFragment.this.personalService.getCommentList();
            }
            if (WaterfallFragment.this.marking == 10005) {
                return WaterfallFragment.this.personalService.getFavorList();
            }
            return WaterfallFragment.this.classifyService.getClassifyList();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<GoodsModel> result) {
            if (result != null && !result.isEmpty() && result.get(0) != null && !StringUtil.isEmpty(result.get(0).getBoardName()) && WaterfallFragment.this.marking == 10003) {
                WaterfallFragment.this.setTopbarTitle(result.get(0).getBoardName());
            }
            switch (this.reqId) {
                case 1:
                    WaterfallFragment.this.onRefreshPostExecute(this.reqId, result);
                    return;
                case 2:
                    WaterfallFragment.this.onMorePostExecute(result);
                    return;
                case 3:
                    WaterfallFragment.this.onRefreshPostExecute(this.reqId, result);
                    return;
                default:
                    WaterfallFragment.this.onRefreshPostExecute(this.reqId, result);
                    return;
            }
        }
    }

    public void onRefreshPostExecute(int reqId, List<GoodsModel> result) {
        this.pullToRefreshWaterFall.onRefreshComplete();
        if (result != null) {
            boolean flag = getLocalFlag();
            if (result.size() == 0) {
                this.goodsList.clear();
                if (this.subNav != null) {
                    this.subNav.setVisibility(8);
                }
                this.pullToRefreshWaterFall.onBottomRefreshComplete(2);
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                showMessage(MCForumErrorUtil.convertErrorCode(this.activity, result.get(0).getErrorCode()));
                if (this.subNav != null) {
                    this.subNav.setVisibility(0);
                }
                this.pullToRefreshWaterFall.onBottomRefreshComplete(0);
            } else {
                long totalNum = result.get(0).getTbkItemsTotal();
                this.goodsList.clear();
                this.goodsList.addAll(result);
                this.imageMap.clear();
                if (this.subNav != null) {
                    this.subNav.setVisibility(0);
                }
                this.pullToRefreshWaterFall.onDrawWaterFall(goodsModels2FlowTags(result), 0);
                if (((long) this.goodsList.size()) < totalNum - 1) {
                    this.pullToRefreshWaterFall.onBottomRefreshComplete(0);
                } else if (flag) {
                    this.pullToRefreshWaterFall.onBottomRefreshComplete(5, MCStringBundleUtil.resolveString(this.mcResource.getStringId("mc_forum_cache_load_more"), getRefreshDate(), this.activity));
                } else {
                    this.pullToRefreshWaterFall.onBottomRefreshComplete(3);
                }
            }
            result.clear();
            this.pullToRefreshWaterFall.scrollTo(0, 0);
            return;
        }
        this.pullToRefreshWaterFall.onBottomRefreshComplete(0);
    }

    public void onMorePostExecute(List<GoodsModel> result) {
        if (result != null) {
            boolean flag = getLocalFlag();
            if (result.isEmpty()) {
                if (flag) {
                    this.pullToRefreshWaterFall.onBottomRefreshComplete(5, MCStringBundleUtil.resolveString(this.mcResource.getStringId("mc_forum_cache_load_more"), getRefreshDate(), this.activity));
                } else {
                    this.pullToRefreshWaterFall.onBottomRefreshComplete(3);
                }
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                showMessage(MCForumErrorUtil.convertErrorCode(this.activity, result.get(0).getErrorCode()));
                this.pullToRefreshWaterFall.onBottomRefreshComplete(0);
            } else {
                long totalNum = result.get(0).getTbkItemsTotal();
                this.goodsList.addAll(result);
                if (MCImageViewerHelper.getInstance().getOnViewSizeListener() != null) {
                    MCImageViewerHelper.getInstance().getOnViewSizeListener().onViewerSizeListener(parseRichModels(this.goodsList));
                }
                this.pullToRefreshWaterFall.onDrawWaterFall(goodsModels2FlowTags(result), 1);
                if (((long) this.goodsList.size()) < totalNum - 1) {
                    this.pullToRefreshWaterFall.onBottomRefreshComplete(0);
                } else if (flag) {
                    this.pullToRefreshWaterFall.onBottomRefreshComplete(5, MCStringBundleUtil.resolveString(this.mcResource.getStringId("mc_forum_cache_load_more"), getRefreshDate(), this.activity));
                } else {
                    this.pullToRefreshWaterFall.onBottomRefreshComplete(3);
                }
            }
            result.clear();
            return;
        }
        this.pullToRefreshWaterFall.onBottomRefreshComplete(0);
    }

    public void onRefreshEvent() {
        initServiceCacheDB(true);
        LoadDataAsyncTask loadDataAsyncTask = new LoadDataAsyncTask();
        addAsyncTask(loadDataAsyncTask);
        loadDataAsyncTask.execute(1);
    }

    private void initServiceCacheDB(boolean flag) {
        switch (this.marking) {
            case FinalConstant.HOT_MARKING:
                this.hotService.initCacheDB(this.pageSize, this.boardId, flag);
                return;
            case 1002:
                this.newestService.initCacheDB(this.pageSize, this.boardId, flag);
                return;
            case 10003:
                this.classifyService.initCacheDB(this.pageSize, this.boardId, flag);
                return;
            case FinalConstant.GOODSCOMMENT_MARKING:
                this.personalService.initCacheDB(this.pageSize, this.isRefresh);
                return;
            case FinalConstant.GOODSFAVOR_MARKING:
                this.personalService.initCacheDB(this.pageSize, this.isRefresh);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void loadImageByUrl(String imageUrl) {
        final ImageView imgView = this.imageMap.get(imageUrl);
        if (imgView.getDrawable() == null) {
            AsyncTaskLoaderImage.getInstance(this.activity).loadAsync(ImageCache.formatUrl(imageUrl, "200x200"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap bitmap, String url) {
                    WaterfallFragment.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (bitmap != null && !bitmap.isRecycled() && imgView.isShown()) {
                                TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), new BitmapDrawable(WaterfallFragment.this.resources, bitmap)});
                                td.startTransition(350);
                                ViewGroup.LayoutParams lp = imgView.getLayoutParams();
                                lp.width = -1;
                                lp.height = -1;
                                imgView.setLayoutParams(lp);
                                imgView.setImageDrawable(td);
                            }
                        }
                    });
                }
            });
        }
    }

    public void changePaddingTop(int subNavMoveHeight) {
        if (this.pullToRefreshWaterFall != null) {
            this.pullToRefreshWaterFall.getChildAt(0).setPadding(0, subNavMoveHeight, 0, 0);
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.pullToRefreshWaterFall.onDestroyView();
    }

    public void loadImageFallWall(int position) {
        if (this.pullToRefreshWaterFall != null) {
            this.pullToRefreshWaterFall.setFallWallImage();
        }
    }

    public void recycleImageFallWall(int position) {
        if (this.pullToRefreshWaterFall != null) {
            this.pullToRefreshWaterFall.setFallWallBlank();
        }
    }

    public static ArrayList<RichImageModel> parseRichModels(List<GoodsModel> goodsModels) {
        ArrayList<RichImageModel> richImageModels = new ArrayList<>();
        for (int i = 0; i < goodsModels.size(); i++) {
            RichImageModel model = new RichImageModel();
            model.setImageUrl(formatImgUrl(goodsModels.get(i).getPicPath()));
            richImageModels.add(model);
        }
        return richImageModels;
    }

    public static String formatImgUrl(String url) {
        return AsyncTaskLoaderImage.formatUrl(url, MCForumConstant.RESOLUTION_640X480);
    }

    private ArrayList<FlowTag> goodsModels2FlowTags(List<GoodsModel> goodsModels) {
        ArrayList<FlowTag> flowTags = new ArrayList<>();
        for (int i = 0; i < goodsModels.size(); i++) {
            GoodsModel goodsModel = goodsModels.get(i);
            FlowTag flowTag = new FlowTag();
            flowTag.setTopicId(goodsModel.getTopicId());
            flowTag.setRatio(goodsModel.getRatio());
            flowTag.setThumbnailUrl(goodsModel.getPicPath());
            flowTag.setTitle(goodsModel.getPrice());
            flowTags.add(flowTag);
        }
        return flowTags;
    }

    public void loadMoreData() {
        onMoreEvent();
    }

    public void loadDataByNet() {
        if (this.boardId != -100000) {
            this.isRefresh = true;
            if (this.pullToRefreshWaterFall != null) {
                this.pullToRefreshWaterFall.onRefresh();
            }
        }
    }

    /* access modifiers changed from: private */
    public void setTopbarTitle(String title) {
        if (this.topbarListener != null) {
            this.topbarListener.setTopbarTitle(title);
        }
    }

    /* access modifiers changed from: protected */
    public int getCacheDB() {
        switch (this.marking) {
            case FinalConstant.HOT_MARKING:
                this.cacheDb = this.hotService.getCacheDB();
                break;
            case 1002:
                this.cacheDb = this.newestService.getCacheDB();
                break;
            case 10003:
                this.cacheDb = this.classifyService.getCacheDB();
                break;
        }
        return this.cacheDb;
    }

    public void onPause() {
        super.onPause();
        int position = this.pullToRefreshWaterFall.getScrollY();
        switch (this.marking) {
            case FinalConstant.HOT_MARKING:
                this.hotService.saveCacheDB(position);
                return;
            case 1002:
                this.newestService.saveCacheDB(position);
                return;
            case 10003:
                this.classifyService.saveCacheDB(position);
                return;
            default:
                return;
        }
    }

    public void onStart() {
        super.onStart();
        loadImageFallWall(0);
    }
}
