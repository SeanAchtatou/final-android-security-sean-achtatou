package com.appbyme.activity.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import com.appbyme.activity.BaseListActivity;
import com.appbyme.activity.constant.FinalConstant;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.fragment.adapter.BigPicListFragmentAdapter;
import com.appbyme.activity.fragment.adapter.holder.BigPicListFragmentAdapterHolder;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.appbyme.android.base.model.GoodsModel;
import com.appbyme.classify.activity.ClassifyActivity;
import com.appbyme.classify.activity.delegate.FallWallDelegate;
import com.appbyme.hot.activity.HotActivity;
import com.appbyme.newest.activity.NewestActivity;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class BigPicFragment extends BaseFragment implements FallWallDelegate, IntentConstant, FinalConstant {
    private String TAG = "BigPicFragment";
    protected Bundle args;
    /* access modifiers changed from: private */
    public BigPicListFragmentAdapter bigPicListFragmentAdapter;
    private long boardId = -10000;
    protected BoardModel boardModel;
    private List<GoodsModel> goodsList;
    /* access modifiers changed from: private */
    public boolean isFoceNotify = false;
    private boolean isLoadMore = false;
    /* access modifiers changed from: private */
    public boolean isRefreshing = false;
    /* access modifiers changed from: private */
    public PullToRefreshListView mPullRefreshListView;
    protected AutogenModuleModel moduleModel;
    private int pageSize = 15;
    private RelativeLayout subNav;
    private BaseListActivity.TopbarListener topbarListener;

    public BigPicFragment() {
    }

    public BigPicFragment(RelativeLayout subNav2) {
        this.subNav = subNav2;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.args = getArguments();
        this.marking = ((Integer) this.args.get(IntentConstant.INTENT_MODULE_MARKING)).intValue();
        if (this.marking == 10003) {
            this.boardId = ((Long) getArguments().get("boardId")).longValue();
        }
        if (this.marking == 1001) {
            this.boardId = 1001;
        }
        if (this.marking == 1002) {
            this.boardId = 1002;
        }
        this.moduleModel = (AutogenModuleModel) this.args.getSerializable(IntentConstant.SAVE_INSTANCE_CONFIG_MODEL);
        this.goodsList = this.application.getAutogenDataCache().getGoodsList(this.moduleModel.getModuleId() + this.boardId);
        if (this.goodsList == null) {
            this.application.getAutogenDataCache().setGoodsList(this.moduleModel.getModuleId() + this.boardId, new ArrayList());
            this.goodsList = this.application.getAutogenDataCache().getGoodsList(this.moduleModel.getModuleId() + this.boardId);
        }
        initTopbar();
        initService(this.marking);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(this.mcResource.getLayoutId("big_pic_fragment"), container, false);
        this.mPullRefreshListView = (PullToRefreshListView) view.findViewById(this.mcResource.getViewId("mc_ec_list_content"));
        this.bigPicListFragmentAdapter = new BigPicListFragmentAdapter(this.activity, this.goodsList, this.moduleModel, this.adTag);
        this.mPullRefreshListView.setAdapter((ListAdapter) this.bigPicListFragmentAdapter);
        this.mPullRefreshListView.setScrollListener(new PullToRefreshListView.OnScrollListener() {
            public void onScrollStateChanged(AbsListView arg0, int scrollState) {
                switch (scrollState) {
                    case 0:
                        BigPicFragment.this.onImageLoaded();
                        return;
                    case 1:
                        BigPicFragment.this.bigPicListFragmentAdapter.setScrolling(false);
                        return;
                    case 2:
                        if (BigPicFragment.this.isRefreshing) {
                            BigPicFragment.this.bigPicListFragmentAdapter.setScrolling(false);
                            return;
                        } else {
                            BigPicFragment.this.bigPicListFragmentAdapter.setScrolling(true);
                            return;
                        }
                    default:
                        return;
                }
            }

            public void onScrollDirection(boolean isUp, int distance) {
            }

            public void onScroll(AbsListView arg0, int firstVisiableItem, int visibleItemCount, int totalItemCount) {
            }
        });
        return view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.mPullRefreshListView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
        this.mPullRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                boolean unused = BigPicFragment.this.isRefreshing = true;
                if (BigPicFragment.this.mPullRefreshListView.isHand()) {
                    boolean unused2 = BigPicFragment.this.isFoceNotify = true;
                    BigPicFragment.this.onRefreshEvent();
                } else if (BigPicFragment.this.isRefresh) {
                    boolean unused3 = BigPicFragment.this.isFoceNotify = true;
                    BigPicFragment.this.isRefresh = false;
                    BigPicFragment.this.onRefreshEvent();
                } else {
                    BigPicFragment.this.onInitEvent();
                }
            }
        });
        this.mPullRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                BigPicFragment.this.onMoreEvent();
            }
        });
        if (this.boardId != -100000) {
            this.mPullRefreshListView.onRefresh(false);
        } else {
            this.mPullRefreshListView.hideBottom();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        recycleImageFallWall(0);
    }

    public void onRefreshEvent() {
        initServiceCacheDB(true);
        LoadDataAsyncTask loadDataAsyncTask = new LoadDataAsyncTask();
        addAsyncTask(loadDataAsyncTask);
        loadDataAsyncTask.execute(1);
    }

    public void onInitEvent() {
        initServiceCacheDB(false);
        LoadDataAsyncTask loadDataAsyncTask = new LoadDataAsyncTask();
        addAsyncTask(loadDataAsyncTask);
        loadDataAsyncTask.execute(3);
    }

    public void onMoreEvent() {
        LoadDataAsyncTask loadDataAsyncTask = new LoadDataAsyncTask();
        addAsyncTask(loadDataAsyncTask);
        loadDataAsyncTask.execute(2);
    }

    private void initServiceCacheDB(boolean flag) {
        switch (this.marking) {
            case FinalConstant.HOT_MARKING:
                this.hotService.initCacheDB(this.pageSize, this.boardId, flag);
                return;
            case 1002:
                this.newestService.initCacheDB(this.pageSize, this.boardId, flag);
                return;
            case 10003:
                this.classifyService.initCacheDB(this.pageSize, this.boardId, flag);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void initTopbar() {
        switch (this.marking) {
            case FinalConstant.HOT_MARKING:
                this.topbarListener = new HotActivity.MyTopbarListrner();
                return;
            case 1002:
                this.topbarListener = new NewestActivity.MyTopbarListrner();
                return;
            case 10003:
                this.topbarListener = new ClassifyActivity.MyTopbarListrner();
                return;
            default:
                return;
        }
    }

    private class LoadDataAsyncTask extends AsyncTask<Integer, Void, List<GoodsModel>> {
        private int reqId;

        private LoadDataAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<GoodsModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public List<GoodsModel> doInBackground(Integer... params) {
            this.reqId = params[0].intValue();
            if (BigPicFragment.this.marking == 1001) {
                return BigPicFragment.this.hotService.getHotList();
            }
            if (BigPicFragment.this.marking == 1002) {
                return BigPicFragment.this.newestService.getNewestList();
            }
            return BigPicFragment.this.classifyService.getClassifyList();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<GoodsModel> result) {
            if (result != null && !result.isEmpty() && result.get(0) != null && !StringUtil.isEmpty(result.get(0).getBoardName()) && BigPicFragment.this.marking == 10003) {
                BigPicFragment.this.setTopbarTitle(result.get(0).getBoardName());
            }
            switch (this.reqId) {
                case 1:
                    BigPicFragment.this.onRefreshPostExecute(this.reqId, result);
                    return;
                case 2:
                    BigPicFragment.this.onMorePostExecute(result);
                    return;
                case 3:
                    BigPicFragment.this.onRefreshPostExecute(this.reqId, result);
                    return;
                default:
                    return;
            }
        }
    }

    public void onRefreshPostExecute(int reqId, List<GoodsModel> result) {
        this.mPullRefreshListView.onRefreshComplete();
        if (result != null) {
            if (result.size() == 0) {
                this.goodsList.clear();
                if (this.subNav != null) {
                    this.subNav.setVisibility(8);
                }
                this.mPullRefreshListView.onBottomRefreshComplete(2);
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                showMessage(MCForumErrorUtil.convertErrorCode(this.activity, result.get(0).getErrorCode()));
                if (this.subNav != null) {
                    this.subNav.setVisibility(0);
                }
                this.mPullRefreshListView.onBottomRefreshComplete(0);
            } else {
                long totalNum = result.get(0).getTbkItemsTotal();
                this.goodsList.clear();
                this.goodsList.addAll(result);
                this.mPullRefreshListView.setSelection(0);
                result.clear();
                if (((long) this.goodsList.size()) < totalNum - 1) {
                    this.mPullRefreshListView.onBottomRefreshComplete(0);
                } else if (getLocalFlag()) {
                    this.mPullRefreshListView.onBottomRefreshComplete(5, MCStringBundleUtil.resolveString(this.mcResource.getStringId("mc_forum_cache_load_more"), getRefreshDate(), this.activity));
                } else {
                    this.mPullRefreshListView.onBottomRefreshComplete(3);
                }
                if (this.subNav != null) {
                    this.subNav.setVisibility(0);
                }
            }
            if (this.isFoceNotify) {
                this.bigPicListFragmentAdapter.notifyDataSetChanged();
            }
        } else {
            this.mPullRefreshListView.onBottomRefreshComplete(0);
            if (this.subNav != null) {
                this.subNav.setVisibility(0);
            }
        }
        this.isFoceNotify = false;
    }

    public void onMorePostExecute(List<GoodsModel> result) {
        if (result != null) {
            boolean flag = getLocalFlag();
            if (result.isEmpty()) {
                if (flag) {
                    this.mPullRefreshListView.onBottomRefreshComplete(5, MCStringBundleUtil.resolveString(this.mcResource.getStringId("mc_forum_cache_load_more"), getRefreshDate(), this.activity));
                    return;
                }
                this.mPullRefreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                showMessage(MCForumErrorUtil.convertErrorCode(this.activity, result.get(0).getErrorCode()));
                this.mPullRefreshListView.onBottomRefreshComplete(0);
            } else {
                long totalNum = result.get(0).getTbkItemsTotal();
                this.goodsList.addAll(result);
                result.clear();
                this.bigPicListFragmentAdapter.setGoodsList(this.goodsList);
                this.application.getAutogenDataCache().setGoodsList(this.moduleModel.getModuleId() + this.boardId, this.goodsList);
                if (this.isLoadMore) {
                    this.isLoadMore = false;
                    this.bigPicListFragmentAdapter.notifyDataSetChanged();
                }
                if (((long) this.goodsList.size()) < totalNum - 1) {
                    this.mPullRefreshListView.onBottomRefreshComplete(0);
                } else if (flag) {
                    this.mPullRefreshListView.onBottomRefreshComplete(5, MCStringBundleUtil.resolveString(this.mcResource.getStringId("mc_forum_cache_load_more"), getRefreshDate(), this.activity));
                } else {
                    this.mPullRefreshListView.onBottomRefreshComplete(3);
                }
            }
        } else {
            this.mPullRefreshListView.onBottomRefreshComplete(0);
        }
    }

    public void loadMoreData() {
        this.isLoadMore = true;
        onMoreEvent();
    }

    public void loadDataByNet() {
        if (this.boardId != -100000) {
            this.isRefresh = true;
            if (this.mPullRefreshListView != null) {
                this.mPullRefreshListView.onRefresh();
            }
        }
    }

    public void onPause() {
        super.onPause();
        int position = this.mPullRefreshListView.getFirstVisiblePosition();
        switch (this.marking) {
            case FinalConstant.HOT_MARKING:
                this.hotService.saveCacheDB(position);
                return;
            case 1002:
                this.newestService.saveCacheDB(position);
                return;
            case 10003:
                this.classifyService.saveCacheDB(position);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void setTopbarTitle(String title) {
        if (this.topbarListener != null) {
            this.topbarListener.setTopbarTitle(title);
        }
    }

    public void loadImageFallWall(int position) {
        if (this.mPullRefreshListView != null) {
            int count = this.mPullRefreshListView.getCount();
            for (int i = 0; i < count; i++) {
                View view = this.mPullRefreshListView.getChildAt(i);
                if (view != null && (view.getTag() instanceof BigPicListFragmentAdapterHolder)) {
                    this.bigPicListFragmentAdapter.loadImageByUrl(((BigPicListFragmentAdapterHolder) view.getTag()).getGoodsPic());
                }
            }
        }
    }

    public void recycleImageFallWall(int position) {
        if (this.mPullRefreshListView != null) {
            int count = this.mPullRefreshListView.getCount();
            for (int i = 0; i < count; i++) {
                View view = this.mPullRefreshListView.getChildAt(i);
                if (view != null && (view.getTag() instanceof BigPicListFragmentAdapterHolder)) {
                    BigPicListFragmentAdapterHolder holder = (BigPicListFragmentAdapterHolder) view.getTag();
                    holder.getGoodsPic().setImageBitmap(null);
                    holder.getGoodsPic().setImageDrawable(null);
                }
            }
        }
    }

    public void onStart() {
        super.onStart();
        loadImageFallWall(0);
    }

    public void onImageLoaded() {
        if (this.mPullRefreshListView != null) {
            this.isRefreshing = false;
            this.bigPicListFragmentAdapter.setScrolling(false);
            int count = this.mPullRefreshListView.getChildCount();
            for (int i = 0; i <= count; i++) {
                View itemView = this.mPullRefreshListView.getChildAt(i);
                if (!(itemView == null || itemView.getTag() == null)) {
                    ImageView iv = ((BigPicListFragmentAdapterHolder) itemView.getTag()).getGoodsPic();
                    if (iv.getDrawable() == null) {
                        this.bigPicListFragmentAdapter.loadImageByUrl(iv);
                    }
                }
            }
        }
    }
}
