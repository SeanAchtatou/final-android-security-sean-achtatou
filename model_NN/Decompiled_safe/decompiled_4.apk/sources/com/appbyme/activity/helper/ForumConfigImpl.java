package com.appbyme.activity.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.appbyme.activity.application.AutogenApplication;
import com.appbyme.activity.helper.AutogenInitializeHelper;
import com.appbyme.activity.observable.ActivityObserver;
import com.appbyme.personal.activity.AutogenPersonalCommentActivity;
import com.appbyme.personal.activity.AutogenPersonalFavorActivity;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.PostsActivity;
import com.mobcent.base.android.ui.activity.ReplyTopicActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.MessageFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserHomeFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserLoginFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserTopicFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.ForumConfig;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCThemeResource;
import com.mobcent.forum.android.model.PostsNoticeModel;
import com.mobcent.forum.android.model.PushMessageModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.util.MCLogUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class ForumConfigImpl extends ForumConfig implements MCConstant {
    private static final int CLASSIFY = 4;
    private static final int FORUM = 1;
    private static final int HOT = 2;
    private static final int NEWEST = 3;
    private static final int NOBOARD = 0;

    public void initNav(Activity activity, MCResource resource) {
    }

    public void initNav(Activity activity, MCResource resource, MCThemeResource themeResource) {
    }

    public void gotoUserInfo(Activity activity, MCResource resource, long userId) {
        HashMap<String, Serializable> param = new HashMap<>();
        param.put("userId", Long.valueOf(userId));
        if (LoginInterceptor.doInterceptorByDialog(activity, resource, UserHomeFragmentActivity.class, param)) {
            Intent intent = new Intent(activity, UserHomeFragmentActivity.class);
            intent.putExtra("userId", userId);
            activity.startActivity(intent);
        }
    }

    public void onPostsClick(Activity activity, MCResource resource, View v, TopicModel topicModel, String boardName) {
        Intent intent = new Intent(activity, PostsActivity.class);
        intent.putExtra("boardId", topicModel.getBoardId());
        intent.putExtra("boardName", boardName);
        intent.putExtra("topicId", topicModel.getTopicId());
        intent.putExtra(MCConstant.TOPIC_USER_ID, topicModel.getUserId());
        intent.putExtra("baseUrl", topicModel.getBaseUrl());
        intent.putExtra(MCConstant.THUMBNAIL_IMAGE_URL, topicModel.getPicPath());
        intent.putExtra("type", topicModel.getType());
        activity.startActivity(intent);
    }

    public void onPostsClick(Activity activity, MCResource resource, View v, PostsNoticeModel model) {
        switch ((int) model.getModelType()) {
            case 0:
                Intent intent = new Intent(activity, PostsActivity.class);
                intent.putExtra("topicId", model.getTopicId());
                intent.putExtra("boardId", model.getBoardId());
                activity.startActivity(intent);
                return;
            case 1:
                Intent intent2 = new Intent(activity, PostsActivity.class);
                intent2.putExtra("topicId", model.getTopicId());
                intent2.putExtra("boardId", model.getBoardId());
                activity.startActivity(intent2);
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onReplyClick(Activity activity, MCResource resource, View v, PostsNoticeModel model, long pageFrom) {
        Intent intent = new Intent(activity, ReplyTopicActivity.class);
        intent.putExtra("boardId", model.getBoardId());
        intent.putExtra("topicId", model.getTopicId());
        intent.putExtra("toReplyId", model.getReplyPostsId());
        intent.putExtra(MCConstant.IS_QUOTE_TOPIC, true);
        intent.putExtra("pageFrom", pageFrom);
        intent.putExtra(MCConstant.POSTS_USER_LIST, new ArrayList());
        activity.startActivity(intent);
    }

    public void onMessageClick(Activity activity) {
        activity.startActivity(new Intent(activity, MessageFragmentActivity.class));
    }

    public Intent onBeatClickListener(Context context, int messageTab) {
        Intent intent = new Intent(context, MessageFragmentActivity.class);
        intent.putExtra(MCConstant.MESSAGE_TAP, messageTab);
        return intent;
    }

    public void onTopicClick(Activity activity, MCResource resource, View v, int TopicTab, long userId, UserInfoModel infoModel) {
        Intent intent = null;
        switch (TopicTab) {
            case 0:
                intent = new Intent(activity, AutogenPersonalCommentActivity.class);
                break;
            case 1:
                intent = new Intent(activity, UserTopicFragmentActivity.class);
                break;
            case 2:
                intent = new Intent(activity, AutogenPersonalFavorActivity.class);
                break;
        }
        intent.putExtra(MCConstant.USER_TOPIC, TopicTab);
        intent.putExtra("userId", userId);
        intent.putExtra("nickname", infoModel.getNickname());
        activity.startActivity(intent);
    }

    public void onLogoutClick(Activity activity) {
        activity.startActivity(new Intent(activity, UserLoginFragmentActivity.class));
        activity.finish();
    }

    public boolean setUserHomeRefreshBtn() {
        return false;
    }

    public boolean setUserHomePublishText(Activity activity) {
        return false;
    }

    public boolean onUserHomeSurroundTopic(Activity activity) {
        return true;
    }

    public boolean onUserHomeFindFriend(Activity activity) {
        return !getIsPublish(activity);
    }

    public boolean setAtReplyMessageFragment(Activity activity) {
        return getIsPublish(activity);
    }

    public boolean logoutApp() {
        return false;
    }

    public boolean isCopyright(Activity activity) {
        return true;
    }

    public boolean setNoPicModel(Context activity) {
        return true;
    }

    public void setAnimation(Activity activity, boolean isStart) {
        MCResource mcResource = MCResource.getInstance(activity);
        if (isStart) {
            activity.overridePendingTransition(mcResource.getAnimId("right_in"), mcResource.getAnimId("left_out"));
        } else {
            activity.overridePendingTransition(mcResource.getAnimId("left_in"), mcResource.getAnimId("right_out"));
        }
    }

    public void setBack(final Activity activity) {
        MCLogUtil.e("ForumConfigImpl", "setBack");
        AutogenInitializeHelper.getInstance().init(activity, new AutogenInitializeHelper.AutogenInitializeListener() {
            public void onGetData(String result) {
                activity.finish();
                ((AutogenApplication) activity.getApplication()).getActivityObservable().notifyStartHomeActivity(new ActivityObserver(activity));
            }
        }, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public Intent setPushMsgIntent(final Context activity, PushMessageModel messageModel) {
        Intent intent;
        if (messageModel.getPushType() == 1) {
            AutogenInitializeHelper.getInstance().init(activity, new AutogenInitializeHelper.AutogenInitializeListener() {
                public void onGetData(String result) {
                    AutogenApplication.getInstance().getActivityObservable().notifyStartHomeActivity(new ActivityObserver(activity));
                }
            }, true);
            return null;
        } else if (messageModel.getPushType() != 2) {
            return null;
        } else {
            MCLogUtil.e("messageModel.getPushDetailType()", messageModel.getPushDetailType() + "");
            switch (messageModel.getPushDetailType()) {
                case 1:
                    intent = new Intent(activity, PostsActivity.class);
                    intent.putExtra("topicId", messageModel.getTopicId());
                    intent.putExtra("push", true);
                    break;
                default:
                    intent = new Intent(activity, PostsActivity.class);
                    intent.putExtra("topicId", messageModel.getTopicId());
                    intent.putExtra("push", true);
                    break;
            }
            if (intent == null) {
                return null;
            }
            intent.setFlags(335544320);
            activity.startActivity(intent);
            return null;
        }
    }

    private boolean getIsPublish(Activity activity) {
        return true;
    }

    public Intent setLoginIntent(Context activity) {
        return new Intent(activity, UserLoginFragmentActivity.class);
    }
}
