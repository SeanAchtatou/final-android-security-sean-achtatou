package com.appbyme.activity.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.appbyme.activity.GoodsDetailActivity;
import com.appbyme.activity.application.AutogenApplication;
import com.appbyme.activity.constant.FinalConstant;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.observable.ActivityObserver;
import com.mobcent.base.android.ui.activity.AboutActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.SettingFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserHomeFragmentActivity;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.plaza.android.service.model.PlazaAppModel;
import com.mobcent.plaza.android.service.model.SearchModel;
import com.mobcent.plaza.android.ui.activity.delegate.PlazaConfig;
import java.util.ArrayList;

public class PlazaSearchConfigImpl implements PlazaConfig.PlazaDelegate, IntentConstant, FinalConstant {
    private final int SEARCH_ENCYCL = 7;
    private final int SEARCH_GALLERY = 2;
    private final int SEARCH_GOODS = 16;
    private final int SEARCH_INFO = 3;
    private final int SEARCH_MUSIC = 4;
    private final int SEARCH_VIDEO = 5;
    public String TAG = "PlazaSearchConfigImpl";
    private ActivityObserver activityObserver;
    private ArrayList<Object> data = new ArrayList<>();

    public void onSearchItemClick(Context context, SearchModel searchModel) {
        MCLogUtil.e(this.TAG, "" + searchModel.getBaikeType());
        Intent intent = null;
        switch (searchModel.getBaikeType()) {
            case 1:
                intent = new Intent(context, GoodsDetailActivity.class);
                break;
            case 7:
                intent = getEncyclIntent(context, searchModel);
                break;
        }
        if (intent != null) {
            context.startActivity(intent);
        }
    }

    public long getUserId(Context context) {
        if (!LoginInterceptor.doInterceptor(context, null)) {
            return 0;
        }
        return new UserServiceImpl(context).getLoginUserId();
    }

    public void onAppItemClick(Context context, PlazaAppModel paAppModel) {
    }

    public boolean onPlazaBackPressed(Context context) {
        if (!AutogenApplication.getInstance().isControlBack()) {
            return false;
        }
        Activity activity = (Activity) context;
        if (this.activityObserver == null) {
            this.activityObserver = new ActivityObserver(activity);
        }
        return ((AutogenApplication) activity.getApplication()).getActivityObservable().notifyActivityDestory(this.activityObserver);
    }

    private Intent getEncyclIntent(Context context, SearchModel searchModel) {
        return null;
    }

    public void onPersonalClick(Context context) {
        MCLogUtil.i(this.TAG, "onPersonalClick");
        if (LoginInterceptor.doInterceptor(context, null, null)) {
            context.startActivity(new Intent(context, UserHomeFragmentActivity.class));
        }
    }

    public void onSetClick(Context context) {
        MCLogUtil.i(this.TAG, "onSetClick");
        context.startActivity(new Intent(context, SettingFragmentActivity.class));
    }

    public void onAboutClick(Context context) {
        context.startActivity(new Intent(context, AboutActivity.class));
    }
}
