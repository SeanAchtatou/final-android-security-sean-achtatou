package com.appbyme.activity.helper;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import com.appbyme.activity.application.AutogenApplication;
import com.appbyme.android.base.model.AutogenBoardModel;
import com.appbyme.android.service.AutogenConfigService;
import com.appbyme.android.service.impl.AutogenConfigServiceImpl;
import com.mobcent.ad.android.ui.widget.manager.AdManager;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.PayStateModel;
import com.mobcent.forum.android.service.impl.PayStateServiceImpl;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public class AutogenInitializeHelper {
    private static final AutogenInitializeHelper mcAutogenInitializeHelper = new AutogenInitializeHelper();
    /* access modifiers changed from: private */
    public AutogenApplication application;
    /* access modifiers changed from: private */
    public AutogenInitializeListener autogenInitializeListener;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public PayStateModel payStateModel;

    public interface AutogenInitializeListener {
        void onGetData(String str);
    }

    public static AutogenInitializeHelper getInstance() {
        return mcAutogenInitializeHelper;
    }

    private AutogenInitializeHelper() {
    }

    public void init(Context context2, AutogenInitializeListener autogenInitializeListener2) {
        init(context2, autogenInitializeListener2, true);
    }

    public void init(Context context2, AutogenInitializeListener autogenInitializeListener2, boolean isLocal) {
        this.context = context2;
        this.application = AutogenApplication.getInstance();
        this.autogenInitializeListener = autogenInitializeListener2;
        AutogenBoardModel autogenBoardModel = this.application.getBoardModel();
        if (autogenBoardModel == null || autogenBoardModel.getCategoryMaps() == null || autogenBoardModel.getCategoryMaps().isEmpty()) {
            new LoadDataAsyncTask(isLocal).execute(new Void[0]);
            return;
        }
        try {
            ((Activity) context2).finish();
        } catch (ClassCastException e) {
            this.application.removeActivity();
            if (autogenInitializeListener2 != null) {
                autogenInitializeListener2.onGetData(null);
            }
        }
    }

    private class LoadDataAsyncTask extends AsyncTask<Void, Void, String> {
        private boolean isLocal;

        private LoadDataAsyncTask(boolean isLocal2) {
            this.isLocal = false;
            this.isLocal = isLocal2;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... params) {
            String appKey = SharedPreferencesDB.getInstance(AutogenInitializeHelper.this.context).getForumKey();
            long chanelNum = SharedPreferencesDB.getInstance(AutogenInitializeHelper.this.context).getChannelId();
            AdManager.getInstance().init(AutogenInitializeHelper.this.context, appKey, String.valueOf(chanelNum), SharedPreferencesDB.getInstance(AutogenInitializeHelper.this.context).getUserId());
            PayStateModel unused = AutogenInitializeHelper.this.payStateModel = new PayStateServiceImpl(AutogenInitializeHelper.this.context).controll(appKey);
            new PermServiceImpl(AutogenInitializeHelper.this.context).getPerm();
            AutogenConfigService configService = new AutogenConfigServiceImpl(AutogenInitializeHelper.this.context);
            if (!this.isLocal) {
                configService.getModuleModelByNet();
            }
            configService.getBoardModelByNet();
            AutogenInitializeHelper.this.application.saveConfigModel(configService.getConfigModel());
            AutogenInitializeHelper.this.application.saveBoardModel(configService.getBoardModel());
            AutogenInitializeHelper.this.application.getModuleList();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (AutogenInitializeHelper.this.payStateModel != null) {
                SharedPreferencesDB.getInstance(AutogenInitializeHelper.this.context).setPayState(AutogenInitializeHelper.this.payStateModel);
            }
            if (!StringUtil.isEmpty(SharedPreferencesDB.getInstance(AutogenInitializeHelper.this.context).getWeiXinAppKey())) {
                WXAPIFactory.createWXAPI(AutogenInitializeHelper.this.context, SharedPreferencesDB.getInstance(AutogenInitializeHelper.this.context).getWeiXinAppKey()).registerApp(SharedPreferencesDB.getInstance(AutogenInitializeHelper.this.context).getWeiXinAppKey());
            }
            if (AutogenInitializeHelper.this.autogenInitializeListener != null) {
                AutogenInitializeHelper.this.autogenInitializeListener.onGetData(result);
            }
        }
    }
}
