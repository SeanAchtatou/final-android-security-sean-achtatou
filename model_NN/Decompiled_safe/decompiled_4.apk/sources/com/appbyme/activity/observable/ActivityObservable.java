package com.appbyme.activity.observable;

import android.os.Bundle;

public class ActivityObservable extends Observable<ActivityObserver> {
    public boolean notifyActivityDestory(ActivityObserver observer) {
        return observer.onActivityDestory();
    }

    public void notifyActivityCreate(ActivityObserver observer, Bundle savedInstanceState) {
        observer.onActivityCreate(savedInstanceState);
    }

    public void notifyActivitySaveInstanceState(ActivityObserver observer, Bundle outState) {
        observer.onActivitySaveInstance(outState);
    }

    public void notifyStartHomeActivity(ActivityObserver observer) {
        observer.startHomeActivity();
    }
}
