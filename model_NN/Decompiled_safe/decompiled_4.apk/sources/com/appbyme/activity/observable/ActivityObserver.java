package com.appbyme.activity.observable;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;
import com.appbyme.activity.application.AutogenApplication;
import com.appbyme.activity.application.AutogenDataCache;
import com.appbyme.activity.constant.ConfigConstant;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.helper.ForumConfigImpl;
import com.appbyme.activity.helper.PlazaSearchConfigImpl;
import com.appbyme.android.base.model.AutogenBoardModel;
import com.appbyme.android.base.model.AutogenConfigModel;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.util.AppUtil;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.plaza.android.ui.activity.delegate.PlazaConfig;

public class ActivityObserver implements ConfigConstant {
    private static final int INTERVAL_TIME = 3000;
    private String LTAG = "ActivityObserver";
    protected String TAG;
    private Context context;
    private long endTime;
    private MCResource mcResource;
    private long startTime;

    public ActivityObserver(Context context2) {
        this.context = context2;
        this.mcResource = MCResource.getInstance(context2);
        this.TAG = context2.getClass().getName();
    }

    public boolean onActivityDestory() {
        MCLogUtil.i(this.LTAG, this.TAG + "destory");
        if (this.startTime == 0) {
            this.startTime = System.currentTimeMillis();
            Toast.makeText(this.context, this.mcResource.getString("mc_forum_app_exit_title"), (int) INTERVAL_TIME).show();
            return true;
        }
        this.endTime = System.currentTimeMillis();
        if (this.endTime - this.startTime < 3000) {
            AutogenApplication.getInstance().exitApplication();
            return false;
        }
        this.startTime = 0;
        this.endTime = 0;
        this.startTime = System.currentTimeMillis();
        Toast.makeText(this.context, this.mcResource.getString("mc_forum_app_exit_title"), (int) INTERVAL_TIME).show();
        return true;
    }

    public void startHomeActivity() {
        AutogenConfigModel configModel = AutogenApplication.getInstance().getConfigModel();
        if (configModel != null) {
            switch (configModel.getStyle()) {
                case 0:
                    Intent intent = new Intent();
                    intent.setAction(AppUtil.getPackageName(this.context) + ".com.define.appbyme.HomeActivity");
                    intent.setData(Uri.parse("myscheme://send data content!"));
                    intent.setFlags(268435456);
                    this.context.startActivity(intent);
                    return;
                default:
                    Intent intent1 = new Intent();
                    intent1.setAction(AppUtil.getPackageName(this.context) + ".com.define.appbyme.HomeActivity");
                    intent1.setData(Uri.parse("myscheme://send data content!"));
                    intent1.setFlags(268435456);
                    this.context.startActivity(intent1);
                    return;
            }
        }
    }

    public void onActivityCreate(Bundle savedInstanceState) {
        MCLogUtil.i(this.LTAG, this.TAG + " create");
        MCForumHelper.setForumConfig(new ForumConfigImpl());
        PlazaConfig.getInstance().setPlazaDelegate(new PlazaSearchConfigImpl());
        if (savedInstanceState != null) {
            AutogenApplication appliction = AutogenApplication.getInstance();
            MCLogUtil.i(this.LTAG, this.TAG + " create for savedInstantceState");
            appliction.saveBoardModel((AutogenBoardModel) savedInstanceState.getSerializable(IntentConstant.SAVE_INSTANCE_BOARD_MODEL));
            appliction.saveConfigModel((AutogenConfigModel) savedInstanceState.getSerializable(IntentConstant.SAVE_INSTANCE_CONFIG_MODEL));
            appliction.saveAutogenDataCache((AutogenDataCache) savedInstanceState.getSerializable(IntentConstant.SAVE_INSTANCE_DATACACHE_MODEL));
        }
    }

    public void onActivitySaveInstance(Bundle outState) {
        MCLogUtil.i(this.LTAG, this.TAG + " onSaveInstance");
        AutogenApplication application = AutogenApplication.getInstance();
        AutogenConfigModel configModel = application.getConfigModel();
        AutogenBoardModel boardModel = application.getBoardModel();
        AutogenDataCache autogenDataCache = application.getAutogenDataCache();
        outState.putSerializable(IntentConstant.SAVE_INSTANCE_CONFIG_MODEL, configModel);
        outState.putSerializable(IntentConstant.SAVE_INSTANCE_BOARD_MODEL, boardModel);
        outState.putSerializable(IntentConstant.SAVE_INSTANCE_DATACACHE_MODEL, autogenDataCache);
    }
}
