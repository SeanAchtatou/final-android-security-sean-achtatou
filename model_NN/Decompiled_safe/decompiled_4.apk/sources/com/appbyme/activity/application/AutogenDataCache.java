package com.appbyme.activity.application;

import com.appbyme.android.base.model.GoodsDetailModel;
import com.appbyme.android.base.model.GoodsModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AutogenDataCache implements Serializable {
    private static final long serialVersionUID = -3285694991711404612L;
    private Map<Long, GoodsDetailModel> goodsDetailMap;
    private Map<String, List<GoodsModel>> goodsMap;
    private Map<String, GoodsModel> goodsMaps;

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public GoodsDetailModel getGoodsDetail(long topicId) {
        if (this.goodsDetailMap != null) {
            return this.goodsDetailMap.get(Long.valueOf(topicId));
        }
        this.goodsDetailMap = new HashMap();
        return null;
    }

    public void setGoodsDetail(long topicId, GoodsDetailModel goodsDetailModel) {
        if (this.goodsDetailMap == null) {
            this.goodsDetailMap = new HashMap();
        }
        if (!this.goodsDetailMap.containsKey(Long.valueOf(topicId))) {
            this.goodsDetailMap.put(Long.valueOf(topicId), goodsDetailModel);
        }
    }

    public void clearGoodsDetail(long topicId) {
        if (this.goodsDetailMap != null) {
            this.goodsDetailMap.remove(Long.valueOf(topicId));
        }
    }

    public void clearGoodsDetail() {
        if (this.goodsDetailMap != null) {
            this.goodsDetailMap.clear();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public List<GoodsModel> getGoodsList(long moduleId) {
        String key = moduleId + "";
        if (this.goodsMap == null) {
            this.goodsMap = new HashMap();
            this.goodsMap.put(key, new ArrayList());
        } else if (this.goodsMap.get(key) == null) {
            this.goodsMap.put(key, new ArrayList());
        }
        return this.goodsMap.get(key);
    }

    public void setGoodsList(long moduleId, List<GoodsModel> goodsModelList) {
        String key = moduleId + "";
        if (this.goodsMap == null) {
            this.goodsMap = new HashMap();
        }
        this.goodsMap.put(key, goodsModelList);
    }

    public void removeGoodsList(String key) {
        if (this.goodsMap == null) {
            this.goodsMap = new HashMap();
        }
        this.goodsMap.remove(key);
    }

    public void clearGoodsList() {
        if (this.goodsMap != null) {
            this.goodsMap.clear();
            this.goodsMap = null;
        }
    }

    public GoodsModel getGoodsModel(long boardId, long topicId) {
        return this.goodsMaps.get(boardId + "+" + topicId);
    }

    public void setGoodsModel(long boardId, long topicId, GoodsModel goodsModel) {
        this.goodsMaps.put(boardId + "+" + topicId, goodsModel);
    }
}
