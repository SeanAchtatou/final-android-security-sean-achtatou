package com.appbyme.activity.application;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationManager;
import android.os.Process;
import com.appbyme.activity.constant.ConfigConstant;
import com.appbyme.activity.exception.CrashHandler;
import com.appbyme.activity.observable.ActivityObservable;
import com.appbyme.android.base.model.AutogenBoardModel;
import com.appbyme.android.base.model.AutogenConfigModel;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AutogenApplication extends Application implements ConfigConstant {
    private static AutogenApplication instance;
    private List<Activity> activities;
    private ActivityObservable activityObservable;
    private AutogenDataCache autogenDataCache;
    private AutogenBoardModel boardModel;
    private AutogenConfigModel configModel;

    public static AutogenApplication getInstance() {
        return instance;
    }

    public AutogenBoardModel getBoardModel() {
        if (this.boardModel == null) {
            this.boardModel = new AutogenBoardModel();
        }
        return this.boardModel;
    }

    public void saveBoardModel(AutogenBoardModel boardModel2) {
        this.boardModel = boardModel2;
    }

    public void onCreate() {
        super.onCreate();
        instance = this;
        this.activityObservable = new ActivityObservable();
        this.activities = new ArrayList();
        CrashHandler.getInstance(getApplicationContext());
    }

    public ActivityObservable getActivityObservable() {
        return this.activityObservable;
    }

    public void saveConfigModel(AutogenConfigModel configModel2) {
        this.configModel = configModel2;
    }

    public AutogenConfigModel getConfigModel() {
        if (this.configModel == null) {
            this.configModel = new AutogenConfigModel();
        }
        return this.configModel;
    }

    public void addActivity(Activity activity) {
        this.activities.add(activity);
    }

    public void removeActivity() {
        for (Activity activity : this.activities) {
            activity.finish();
        }
    }

    public void aa() {
    }

    public Map.Entry<Integer, List<AutogenModuleModel>>[] getModuleList() {
        if (this.configModel == null) {
            return null;
        }
        List<AutogenModuleModel> configList = this.configModel.getModuleList();
        Map<Integer, List<AutogenModuleModel>> configMap = new HashMap<>();
        if (configList != null) {
            int size = configList.size();
            for (int i = 0; i < size; i++) {
                AutogenModuleModel moduleModel = configList.get(i);
                int moduleType = moduleModel.getModuleType();
                int key = moduleModel.getPosition();
                if (configMap.containsKey(Integer.valueOf(key))) {
                    configMap.get(Integer.valueOf(key)).add(moduleModel);
                } else {
                    List<AutogenModuleModel> moduleList = new ArrayList<>();
                    moduleList.add(moduleModel);
                    configMap.put(Integer.valueOf(key), moduleList);
                }
            }
        }
        Set<Map.Entry<Integer, List<AutogenModuleModel>>> set = configMap.entrySet();
        Map.Entry<Integer, List<AutogenModuleModel>>[] entries = (Map.Entry[]) set.toArray(new Map.Entry[set.size()]);
        Arrays.sort(entries, new Comparator() {
            public int compare(Object lhs, Object rhs) {
                return ((Integer) ((Map.Entry) lhs).getKey()).compareTo((Integer) ((Map.Entry) rhs).getKey());
            }
        });
        return entries;
    }

    public AutogenDataCache getAutogenDataCache() {
        if (this.autogenDataCache == null) {
            this.autogenDataCache = new AutogenDataCache();
        }
        return this.autogenDataCache;
    }

    public void saveAutogenDataCache(AutogenDataCache autogenDataCache2) {
        this.autogenDataCache = autogenDataCache2;
    }

    public void exitApplication() {
        NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
        removeActivity();
        MCForumHelper.LogoutForum(this);
        Process.killProcess(Process.myPid());
    }

    public boolean isControlBack() {
        return true;
    }
}
