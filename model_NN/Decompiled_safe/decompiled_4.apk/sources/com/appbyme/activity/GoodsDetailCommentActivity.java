package com.appbyme.activity;

import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.appbyme.activity.fragment.adapter.GoodsDetailCommentAdapter;
import com.appbyme.android.base.model.GoodsCommentDetail;
import com.appbyme.android.base.model.GoodsCommentsModel;
import com.appbyme.android.service.GoodsDetailService;
import com.appbyme.android.service.impl.GoodsDetailServiceImpl;
import java.util.ArrayList;
import java.util.List;

public class GoodsDetailCommentActivity extends BaseListActivity {
    private String TAG = "GoodsDetailCommentActivity";
    private GoodsDetailCommentAdapter commentAdapter;
    /* access modifiers changed from: private */
    public TextView footerText;
    /* access modifiers changed from: private */
    public LinearLayout footerView;
    private List<GoodsCommentDetail> goodsCommentDetaiList;
    private ListView goodsDetailListView;
    /* access modifiers changed from: private */
    public GoodsDetailService goodsDetailService;
    /* access modifiers changed from: private */
    public long numIid;
    private RelativeLayout topbar;
    private View topbarLeft;
    private TextView topbarTitle;
    private int total;

    /* access modifiers changed from: protected */
    public void initData() {
        this.numIid = getIntent().getLongExtra("numIid", 0);
        this.goodsDetailService = new GoodsDetailServiceImpl(this.application);
        this.goodsDetailService.initData(15);
        this.goodsCommentDetaiList = new ArrayList();
        LoadCommentTask task = new LoadCommentTask();
        addAsyncTask(task);
        task.execute(Long.valueOf(this.numIid));
        super.initData();
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        LinearLayout rootView = (LinearLayout) this.inflater.inflate(this.mcResource.getLayoutId("mc_ec_goods_comment"), (ViewGroup) null);
        this.topbar = (RelativeLayout) rootView.findViewById(this.mcResource.getViewId("common_topbar"));
        this.topbarLeft = ((ViewStub) rootView.findViewById(this.mcResource.getViewId("ec_common_topbar_left"))).inflate();
        this.topbarLeft.setBackgroundDrawable(this.mcResource.getDrawable("mc_forum_top_bar_button1"));
        this.topbarTitle = (TextView) rootView.findViewById(this.mcResource.getViewId("ec_common_topbar_title"));
        this.topbarTitle.setText(this.mcResource.getString("mc_ec_goods_detail_tag_evaluate"));
        setContentView(rootView);
        this.goodsDetailListView = new ListView(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-1, -2);
        lp.setMargins(0, -10, 0, 0);
        this.goodsDetailListView.setLayoutParams(lp);
        this.goodsDetailListView.setDivider(this.mcResource.getDrawable("mc_forum_line1"));
        rootView.addView(this.goodsDetailListView);
        this.commentAdapter = new GoodsDetailCommentAdapter(this, this.goodsCommentDetaiList);
        this.footerView = (LinearLayout) this.inflater.inflate(this.mcResource.getLayoutId("mc_ec_goods_detail_comment_footer"), (ViewGroup) null);
        this.footerText = (TextView) this.footerView.findViewById(this.mcResource.getViewId("mc_ec_goods_detail_footer_comment"));
        this.footerText.setTextColor(this.mcResource.getColor("mc_forum_pull_down_refresh_text_color"));
        this.footerText.setText(this.mcResource.getString("mc_ec_goods_detail_longding_comment"));
        this.goodsDetailListView.addFooterView(this.footerView);
        this.goodsDetailListView.setAdapter((ListAdapter) this.commentAdapter);
    }

    /* access modifiers changed from: protected */
    public void initActions() {
        super.initActions();
        this.footerView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoadCommentTask task = new LoadCommentTask();
                GoodsDetailCommentActivity.this.addAsyncTask(task);
                task.execute(Long.valueOf(GoodsDetailCommentActivity.this.numIid));
                TextView unused = GoodsDetailCommentActivity.this.footerText = (TextView) GoodsDetailCommentActivity.this.footerView.findViewById(GoodsDetailCommentActivity.this.mcResource.getViewId("mc_ec_goods_detail_longding_comment"));
            }
        });
        this.topbarLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GoodsDetailCommentActivity.this.finish();
            }
        });
    }

    class LoadCommentTask extends AsyncTask<Long, Void, GoodsCommentsModel> {
        LoadCommentTask() {
        }

        /* access modifiers changed from: protected */
        public GoodsCommentsModel doInBackground(Long... params) {
            return GoodsDetailCommentActivity.this.goodsDetailService.getGoodsCommentsByNet(params[0].longValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(GoodsCommentsModel result) {
            GoodsDetailCommentActivity.this.onGetData(result);
        }
    }

    /* access modifiers changed from: private */
    public void onGetData(GoodsCommentsModel result) {
        this.total = result.getTotal();
        List<GoodsCommentDetail> list = result.getGoodsCommentDetaiList();
        if (list != null || !list.isEmpty()) {
            this.goodsCommentDetaiList.addAll(list);
            this.commentAdapter.notifyDataSetChanged();
            if (this.goodsCommentDetaiList.size() >= this.total - 1) {
                this.footerView.setClickable(false);
                this.footerText.setText(this.mcResource.getStringId("mc_ec_goods_detail_comment_over"));
                return;
            }
            this.footerText.setText(this.mcResource.getStringId("mc_ec_goods_detail_comment_footer_more"));
            return;
        }
        this.footerView.setClickable(false);
        this.footerText.setText(this.mcResource.getStringId("mc_ec_goods_detail_comment_footer_no"));
    }
}
