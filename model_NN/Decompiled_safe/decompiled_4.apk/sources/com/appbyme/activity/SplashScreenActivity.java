package com.appbyme.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.appbyme.activity.helper.AutogenInitializeHelper;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;
import com.mobcent.base.forum.android.util.ImageCache;
import com.mobcent.base.forum.android.util.MCPhoneUtil;
import com.mobcent.forum.android.constant.MCForumConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.PhoneUtil;

public class SplashScreenActivity extends BaseFragmentActivity {
    private String TAG = "SplashScreenActivity";
    private MCProgressBar progressBar;
    /* access modifiers changed from: private */
    public ImageView splashBackgroundImg;
    private RelativeLayout splashBottomLayout;

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.mcResource.getLayoutId("splash_screen"));
        this.progressBar = (MCProgressBar) findViewById(this.mcResource.getViewId("splash_loading_img"));
        this.progressBar.show();
        this.splashBackgroundImg = (ImageView) findViewById(this.mcResource.getViewId("splash_background_img"));
        this.splashBottomLayout = (RelativeLayout) findViewById(this.mcResource.getViewId("splash_bottom_layout"));
        fitSplashScreen();
    }

    /* access modifiers changed from: protected */
    public void initActions() {
        AutogenInitializeHelper.getInstance().init(this, new AutogenInitializeHelper.AutogenInitializeListener() {
            public void onGetData(String result) {
                if (SplashScreenActivity.this.application.getConfigModel() == null) {
                    SplashScreenActivity.this.handler.postDelayed(new Runnable() {
                        public void run() {
                            SplashScreenActivity.this.application.exitApplication();
                        }
                    }, 1000);
                } else {
                    SplashScreenActivity.this.handler.postDelayed(new Runnable() {
                        public void run() {
                            SplashScreenActivity.this.application.getActivityObservable().notifyStartHomeActivity(SplashScreenActivity.this.activityObserver);
                        }
                    }, 1000);
                }
                SplashScreenActivity.this.handler.postDelayed(new Runnable() {
                    public void run() {
                        if (SplashScreenActivity.this.splashBackgroundImg != null) {
                            SplashScreenActivity.this.splashBackgroundImg.setBackgroundDrawable(null);
                            SplashScreenActivity.this.splashBackgroundImg.setImageDrawable(null);
                        }
                        SplashScreenActivity.this.finish();
                    }
                }, 2000);
            }
        });
        AsyncTaskLoaderImage.getInstance(getApplicationContext()).loadAsync(ImageCache.formatUrl(SharedPreferencesDB.getInstance(getApplicationContext()).getPayStateImgUrl() + SharedPreferencesDB.getInstance(getApplicationContext()).getPayStateLoadingPageImage().split(AdApiConstant.RES_SPLIT_COMMA)[0], MCForumConstant.RESOLUTION_640X480), new AsyncTaskLoaderImage.BitmapImageCallback() {
            public void onImageLoaded(final Bitmap bitmap, String arg1) {
                if (SplashScreenActivity.this.splashBackgroundImg != null && bitmap != null) {
                    SplashScreenActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), new BitmapDrawable(SplashScreenActivity.this.getResources(), bitmap)});
                            td.startTransition(350);
                            SplashScreenActivity.this.splashBackgroundImg.setImageDrawable(td);
                            SplashScreenActivity.this.splashBackgroundImg.setBackgroundDrawable(null);
                        }
                    });
                }
            }
        });
    }

    private void fitSplashScreen() {
        int screenHeight = PhoneUtil.getDisplayHeight((Activity) this);
        int screenWidth = PhoneUtil.getDisplayWidth((Activity) this);
        RelativeLayout.LayoutParams imgLps = (RelativeLayout.LayoutParams) this.splashBackgroundImg.getLayoutParams();
        if (div((double) screenHeight, 16.0d) == div((double) screenWidth, 9.0d)) {
            imgLps.width = screenWidth;
            imgLps.height = screenHeight;
            this.splashBottomLayout.setVisibility(8);
            this.splashBackgroundImg.setBackgroundResource(this.mcResource.getDrawableId("mc_forum_loading_page2"));
            this.splashBottomLayout.setBackgroundDrawable(this.mcResource.getDrawable("mc_forum_dialog_and_menu_box"));
            this.splashBottomLayout.getLayoutParams().height = MCPhoneUtil.getRawSize(getApplicationContext(), 1, 40.0f);
            return;
        }
        int imgHeight = (int) (((double) screenWidth) * 1.5d);
        int bottomBoxHeight = screenHeight - imgHeight;
        imgLps.width = screenWidth;
        imgLps.height = imgHeight;
        this.splashBackgroundImg.setBackgroundResource(this.mcResource.getDrawableId("mc_forum_loading_page"));
        if (bottomBoxHeight == 0) {
            this.splashBottomLayout.setBackgroundDrawable(this.mcResource.getDrawable("mc_forum_dialog_and_menu_box"));
            this.splashBottomLayout.getLayoutParams().height = MCPhoneUtil.getRawSize(getApplicationContext(), 1, 40.0f);
            return;
        }
        this.splashBottomLayout.getLayoutParams().width = screenWidth;
        this.splashBottomLayout.getLayoutParams().height = bottomBoxHeight;
    }

    public double div(double v1, double v2) {
        return v1 / v2;
    }
}
