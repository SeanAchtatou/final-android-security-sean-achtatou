package com.appbyme.forum.activity;

import android.content.Intent;
import android.os.Bundle;
import com.appbyme.activity.application.AutogenApplication;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.observable.ActivityObserver;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.mobcent.base.android.ui.activity.fragmentActivity.HomeTopicFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.SlidingMenuControler;

public class AutogenHomeTopicFragmentActivity extends HomeTopicFragmentActivity {
    private ActivityObserver activityObserver;
    private AutogenApplication application;
    private AutogenModuleModel moduleModel;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.application = (AutogenApplication) getApplication();
        this.application.addActivity(this);
        this.activityObserver = new ActivityObserver(this);
        this.application.getActivityObservable().registerObserver(this.activityObserver);
        this.application.getActivityObservable().notifyActivityCreate(this.activityObserver, savedInstanceState);
        Intent intent = getIntent();
        this.moduleModel = (AutogenModuleModel) intent.getSerializableExtra(IntentConstant.INTENT_MODULEMODEL);
        this.titleText.setText(this.moduleModel.getModuleName());
        if (Boolean.valueOf(intent.getBooleanExtra(IntentConstant.INTENT_ISHIDE_PUBLISH, false)).booleanValue()) {
            this.publishBtn.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        SlidingMenuControler.SlidingMenuControlerListener listener;
        super.onResume();
        if (this.mViewPager != null && (listener = SlidingMenuControler.getInstance().getListener()) != null && listener.getSlidingMenu() != null) {
            if (this.mViewPager.getCurrentItem() == 0) {
                listener.getSlidingMenu().setTouchModeAbove(1);
            } else {
                listener.getSlidingMenu().setTouchModeAbove(0);
            }
        }
    }

    public void onBackPressed() {
        if (!this.application.isControlBack()) {
            super.onBackPressed();
        } else if (!this.application.getActivityObservable().notifyActivityDestory(this.activityObserver)) {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.application.getActivityObservable().unregisterObserver(this.activityObserver);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.application.getActivityObservable().notifyActivitySaveInstanceState(this.activityObserver, outState);
    }
}
