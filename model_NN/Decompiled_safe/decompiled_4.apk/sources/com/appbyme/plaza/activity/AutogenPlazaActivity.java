package com.appbyme.plaza.activity;

import com.appbyme.activity.helper.ForumConfigImpl;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.android.ui.activity.helper.SlidingMenuControler;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.plaza.android.ui.activity.PlazaActivity;

public class AutogenPlazaActivity extends PlazaActivity {
    public String TAG = "AutogenPlazaActivity";

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MCForumHelper.setForumConfig(new ForumConfigImpl());
        MCLogUtil.e(this.TAG, "AutogenPlazaActivity onResume");
        if (SlidingMenuControler.getInstance().getListener() != null) {
            SlidingMenuControler.getInstance().getListener().getSlidingMenu().setTouchModeAbove(1);
        }
    }
}
