package com.appbyme.hot.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.appbyme.activity.BaseListActivity;
import com.appbyme.activity.constant.FinalConstant;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.fragment.BaseFragment;
import com.appbyme.activity.fragment.BigPicFragment;
import com.appbyme.activity.fragment.WaterfallFragment;
import com.appbyme.classify.activity.delegate.FallWallDelegate;
import com.appbyme.widget.AutogenViewPager;
import com.mobcent.forum.android.util.StringUtil;
import java.util.HashMap;
import java.util.Map;

public class HotActivity extends BaseListActivity {
    private static RelativeLayout topbar;
    /* access modifiers changed from: private */
    public static TextView topbarTitle;
    private String TAG = "HotActivity";
    private AutogenViewPager fragmentViewPager;
    private View topbarRight;

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.mcResource.getLayoutId("base_hot_display_activity"));
        topbar = (RelativeLayout) findViewById(this.mcResource.getViewId("common_topbar"));
        this.fragmentViewPager = (AutogenViewPager) findViewById(this.mcResource.getViewId("fragment_pager"));
        topbarTitle = (TextView) findViewById(this.mcResource.getViewId("ec_common_topbar_title"));
        topbarTitle.setText(this.mcResource.getStringId("mc_ec_comment_hot_name"));
        this.topbarRight = ((ViewStub) findViewById(this.mcResource.getViewId("ec_common_topbar_right"))).inflate();
        this.displayActivityAdapter = new DisplayActivityAdapter(getSupportFragmentManager());
        this.fragmentViewPager.setAdapter(this.displayActivityAdapter);
        this.fragmentViewPager.setCurrentItem(this.currentPosition);
        this.handler.postDelayed(new Runnable() {
            public void run() {
                HotActivity.this.loadCurrentFragmentData();
            }
        }, 2000);
        onImageDeal(this.currentPosition);
    }

    private void onImageDeal(int position) {
        recycleFallWall(position);
        this.handler.postDelayed(new Runnable() {
            public void run() {
                HotActivity.this.loadFallWall();
            }
        }, 300);
    }

    /* access modifiers changed from: private */
    public void loadFallWall() {
        FallWallDelegate delegate = (FallWallDelegate) this.displayActivityAdapter.getItem(this.currentPosition);
        if (delegate != null) {
            delegate.loadImageFallWall(this.currentPosition);
        }
    }

    private void recycleFallWall(int position) {
    }

    /* access modifiers changed from: protected */
    public void initActions() {
        super.initActions();
        this.topbarRight.setBackgroundDrawable(this.mcResource.getDrawable("mc_forum_top_bar_button32"));
        this.topbarRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HotActivity.this.goodsSearch();
            }
        });
    }

    public class DisplayActivityAdapter extends FragmentStatePagerAdapter {
        private Map<Integer, BaseFragment> fragmentMap = new HashMap();

        public DisplayActivityAdapter(FragmentManager fm) {
            super(fm);
        }

        /* Debug info: failed to restart local var, previous not found, register: 5 */
        public BaseFragment getItem(int position) {
            BaseFragment fragment;
            Bundle bundle = HotActivity.this.getBundle(position);
            if (this.fragmentMap.get(Integer.valueOf(position)) != null) {
                return this.fragmentMap.get(Integer.valueOf(position));
            }
            switch (HotActivity.this.moduleModel.getListDisplay()) {
                case 0:
                    fragment = new WaterfallFragment();
                    break;
                case 1:
                    fragment = new BigPicFragment();
                    bundle.putInt("position", position);
                    break;
                default:
                    fragment = new WaterfallFragment();
                    break;
            }
            bundle.putInt(IntentConstant.INTENT_MODULE_MARKING, FinalConstant.HOT_MARKING);
            fragment.setArguments(bundle);
            this.fragmentMap.put(Integer.valueOf(position), fragment);
            return fragment;
        }

        public int getCount() {
            return 1;
        }

        public void destroyItem(View container, int position, Object object) {
            this.fragmentMap.remove(Integer.valueOf(position));
            super.destroyItem(container, position, object);
        }

        public int getItemPosition(Object object) {
            return -2;
        }
    }

    public static class MyTopbarListrner implements BaseListActivity.TopbarListener {
        public void setTopbarTitle(String title) {
            if (!StringUtil.isEmpty(title)) {
                HotActivity.topbarTitle.setText(title);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.application.getAutogenDataCache().clearGoodsList();
    }
}
