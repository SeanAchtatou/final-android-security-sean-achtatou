package com.appbyme.classify.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewStub;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.appbyme.activity.BaseListActivity;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.fragment.BaseFragment;
import com.appbyme.classify.activity.fragment.GridViewFragment;
import com.appbyme.widget.AutogenViewPager;
import com.mobcent.forum.android.util.StringUtil;
import java.util.HashMap;
import java.util.Map;

public class ClassifyActivity extends BaseListActivity {
    private static RelativeLayout topbar;
    /* access modifiers changed from: private */
    public static TextView topbarTitle;
    private AutogenViewPager fragmentViewPager;
    private View topbarLeft;
    private View topbarRight;

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.mcResource.getLayoutId("base_classify_display_activity"));
        topbar = (RelativeLayout) findViewById(this.mcResource.getViewId("common_topbar"));
        this.fragmentViewPager = (AutogenViewPager) findViewById(this.mcResource.getViewId("fragment_pager"));
        topbarTitle = (TextView) findViewById(this.mcResource.getViewId("ec_common_topbar_title"));
        topbarTitle.setText(this.mcResource.getStringId("mc_ec_comment_classify_name"));
        this.topbarRight = ((ViewStub) findViewById(this.mcResource.getViewId("ec_common_topbar_right"))).inflate();
        this.topbarRight.setBackgroundDrawable(this.mcResource.getDrawable("mc_forum_top_bar_button32"));
        this.displayActivityAdapter = new ClassifyActivityAdapter(getSupportFragmentManager());
        this.fragmentViewPager.setAdapter(this.displayActivityAdapter);
        this.fragmentViewPager.setCurrentItem(this.currentPosition);
        this.handler.postDelayed(new Runnable() {
            public void run() {
                ClassifyActivity.this.loadCurrentFragmentData();
            }
        }, 1000);
    }

    /* access modifiers changed from: protected */
    public void initActions() {
        this.topbarRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClassifyActivity.this.goodsSearch();
            }
        });
    }

    private class ClassifyActivityAdapter extends FragmentStatePagerAdapter {
        private Map<Integer, BaseFragment> fragmentMap = new HashMap();

        public ClassifyActivityAdapter(FragmentManager fm) {
            super(fm);
        }

        /* Debug info: failed to restart local var, previous not found, register: 5 */
        public Fragment getItem(int position) {
            BaseFragment fragment;
            if (this.fragmentMap.get(Integer.valueOf(position)) != null) {
                return this.fragmentMap.get(Integer.valueOf(position));
            }
            switch (ClassifyActivity.this.moduleModel.getListDisplay()) {
                case 0:
                    fragment = new GridViewFragment();
                    break;
                default:
                    fragment = new GridViewFragment();
                    break;
            }
            Bundle bundle = ClassifyActivity.this.getBundle(position);
            bundle.putInt("position", position);
            bundle.putInt(IntentConstant.INTENT_MODULE_MARKING, 10003);
            fragment.setArguments(bundle);
            this.fragmentMap.put(Integer.valueOf(position), fragment);
            return fragment;
        }

        public int getCount() {
            return 1;
        }
    }

    public static class MyTopbarListrner implements BaseListActivity.TopbarListener {
        public void setTopbarTitle(String title) {
            if (!StringUtil.isEmpty(title)) {
                ClassifyActivity.topbarTitle.setText(title);
            }
        }
    }
}
