package com.appbyme.classify.activity.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.appbyme.activity.constant.FinalConstant;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.fragment.BaseFragment;
import com.appbyme.android.base.model.AutogenBoardCategory;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.appbyme.classify.activity.fragment.adapter.GridViewFragmentAdapter;
import java.util.ArrayList;

public class GridViewFragment extends BaseFragment implements IntentConstant, FinalConstant {
    protected Bundle args;
    private GridView classifyGridView;
    protected ArrayList<AutogenBoardCategory> goodsCategoryList;
    private GridViewFragmentAdapter gridViewFragmentAdapter;
    protected AutogenModuleModel moduleModel;

    /* access modifiers changed from: protected */
    public void initData() {
        this.args = getArguments();
        if (this.args != null) {
            this.marking = ((Integer) this.args.get(IntentConstant.INTENT_MODULE_MARKING)).intValue();
            this.moduleModel = (AutogenModuleModel) this.args.getSerializable(IntentConstant.SAVE_INSTANCE_CONFIG_MODEL);
        }
        this.goodsCategoryList = this.application.getBoardModel().getCategoryMaps().get("10003");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.classifyGridView = (GridView) inflater.inflate(this.mcResource.getLayoutId("mc_ec_classify_gridview"), container, false).findViewById(this.mcResource.getViewId("mc_ec_classify_grid_view"));
        this.gridViewFragmentAdapter = new GridViewFragmentAdapter(this.activity, this.goodsCategoryList, this.moduleModel, null);
        this.classifyGridView.setAdapter((ListAdapter) this.gridViewFragmentAdapter);
        return this.classifyGridView;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
    }

    public void loadMoreData() {
    }

    public void loadDataByNet() {
    }
}
