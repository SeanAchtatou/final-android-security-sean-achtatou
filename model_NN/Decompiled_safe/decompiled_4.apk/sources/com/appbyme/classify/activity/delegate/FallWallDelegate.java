package com.appbyme.classify.activity.delegate;

public interface FallWallDelegate {
    void loadImageFallWall(int i);

    void recycleImageFallWall(int i);
}
