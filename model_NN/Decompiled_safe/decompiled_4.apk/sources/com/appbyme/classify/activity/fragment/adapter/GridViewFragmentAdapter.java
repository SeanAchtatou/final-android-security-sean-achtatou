package com.appbyme.classify.activity.fragment.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.appbyme.activity.adapter.BaseListFragmentAdapter;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.android.base.model.AutogenBoardCategory;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.appbyme.classify.activity.ClassifyDetailActivity;
import com.appbyme.classify.activity.fragment.adapter.holder.GridViewFragmentAdapterHolder;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import java.util.ArrayList;

public class GridViewFragmentAdapter extends BaseListFragmentAdapter {
    private ArrayList<AutogenBoardCategory> goodsCategoryList;
    private AutogenModuleModel moduleModel;

    public GridViewFragmentAdapter(Context context, ArrayList<AutogenBoardCategory> goodsCategoryList2, AutogenModuleModel moduleModel2, String adTag) {
        super(context);
        this.goodsCategoryList = goodsCategoryList2;
        this.moduleModel = moduleModel2;
        this.adTag = adTag;
    }

    public void setCategoryList(ArrayList<AutogenBoardCategory> goodsCategoryList2) {
        this.goodsCategoryList = goodsCategoryList2;
    }

    public int getCount() {
        if (this.goodsCategoryList == null) {
            return 0;
        }
        return this.goodsCategoryList.size();
    }

    public Object getItem(int position) {
        return this.goodsCategoryList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        GridViewFragmentAdapterHolder holder;
        AutogenBoardCategory autogenBoardCategory = this.goodsCategoryList.get(position);
        if (convertView == null) {
            convertView = this.inflater.inflate(this.mcResource.getLayoutId("mc_ec_classify_gridview_item"), (ViewGroup) null);
            holder = new GridViewFragmentAdapterHolder();
            initGridViewListFragmentAdapterHolder(convertView, holder, autogenBoardCategory);
            convertView.setTag(holder);
        } else {
            holder = (GridViewFragmentAdapterHolder) convertView.getTag();
        }
        updateClassifyCategoryFragmentAdapterHolder(holder, autogenBoardCategory, position);
        onClickCategoryListFragmentAdapterHolder(holder, position);
        convertView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        return convertView;
    }

    private void onClickCategoryListFragmentAdapterHolder(final GridViewFragmentAdapterHolder holder, final int position) {
        holder.getCategoryImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GridViewFragmentAdapter.this.openCategoryDetailActivity(position, ((Object) holder.getCategoryTitle().getText()) + "");
            }
        });
        holder.getCategoryTitle().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GridViewFragmentAdapter.this.openCategoryDetailActivity(position, ((Object) holder.getCategoryTitle().getText()) + "");
            }
        });
    }

    private void updateClassifyCategoryFragmentAdapterHolder(final GridViewFragmentAdapterHolder holder, AutogenBoardCategory autogenBoardCategory, int position) {
        holder.getCategoryTitle().setText(autogenBoardCategory.getBoardCategoryName());
        AsyncTaskLoaderImage.getInstance(this.context).loadAsync(AsyncTaskLoaderImage.formatUrl(autogenBoardCategory.getBoardCategoryImgUrl(), "200x200"), new AsyncTaskLoaderImage.BitmapImageCallback() {
            public void onImageLoaded(final Bitmap bitmap, String url) {
                GridViewFragmentAdapter.this.mHandler.post(new Runnable() {
                    public void run() {
                        if (bitmap != null && !bitmap.isRecycled() && holder.getCategoryImg().isShown()) {
                            TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), new BitmapDrawable(GridViewFragmentAdapter.this.resources, bitmap)});
                            td.startTransition(350);
                            holder.getCategoryImg().setImageDrawable(td);
                        }
                    }
                });
            }
        });
    }

    private void initGridViewListFragmentAdapterHolder(View convertView, GridViewFragmentAdapterHolder holder, AutogenBoardCategory autogenBoardCategory) {
        holder.setCategoryImg((ImageView) convertView.findViewById(this.mcResource.getViewId("mc_ec_classify_gridview_item_category_img")));
        holder.setCategoryTitle((TextView) convertView.findViewById(this.mcResource.getViewId("mc_ec_classify_gridview_item_category_title")));
    }

    /* access modifiers changed from: private */
    public void openCategoryDetailActivity(int position, String boardName) {
        Intent intent = new Intent(this.context, ClassifyDetailActivity.class);
        intent.putExtra(IntentConstant.INTENT_WHICH_CATEGORY, position);
        intent.putExtra(IntentConstant.INTENT_CATEGORY_LIST, this.application.getBoardModel().getCategoryMaps().get("10003"));
        intent.putExtra(IntentConstant.INTENT_MODULEMODEL, this.moduleModel);
        intent.putExtra("boardName", boardName);
        this.context.startActivity(intent);
    }
}
