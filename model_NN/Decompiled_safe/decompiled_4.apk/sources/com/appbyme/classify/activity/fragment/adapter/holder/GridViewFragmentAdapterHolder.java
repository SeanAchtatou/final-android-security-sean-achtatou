package com.appbyme.classify.activity.fragment.adapter.holder;

import android.widget.ImageView;
import android.widget.TextView;

public class GridViewFragmentAdapterHolder {
    private ImageView categoryImg;
    private TextView categoryTitle;

    public ImageView getCategoryImg() {
        return this.categoryImg;
    }

    public void setCategoryImg(ImageView categoryImg2) {
        this.categoryImg = categoryImg2;
    }

    public TextView getCategoryTitle() {
        return this.categoryTitle;
    }

    public void setCategoryTitle(TextView categoryTitle2) {
        this.categoryTitle = categoryTitle2;
    }
}
