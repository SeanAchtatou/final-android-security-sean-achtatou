package com.appbyme.classify.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.appbyme.activity.BaseListActivity;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.fragment.BaseFragment;
import com.appbyme.activity.fragment.BigPicFragment;
import com.appbyme.activity.fragment.WaterfallFragment;
import com.appbyme.android.base.model.AutogenBoardCategory;
import com.appbyme.classify.activity.delegate.FallWallDelegate;
import com.appbyme.widget.AutogenViewPager;
import com.appbyme.widget.SubNavScrollView;
import com.mobcent.base.android.ui.activity.delegate.OnMCPageChangeListener;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.util.PhoneUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClassifyDetailActivity extends BaseListActivity {
    private static TextView topbarTitle;
    private String TAG = "ClassifyDetailActivity";
    private ArrayList<AutogenBoardCategory> boardCategoryList;
    /* access modifiers changed from: private */
    public List<BoardModel> boardModelList;
    private int defaultCategory;
    private AutogenViewPager fragmentViewPager;
    /* access modifiers changed from: private */
    public boolean isClickSubNavItem = false;
    protected boolean isHandleSubNavBar = false;
    protected boolean isHasSubNavBar = true;
    protected float limitWidth;
    protected int subItemMaxCount = 4;
    protected int subNavMoveHeight = 0;
    protected RelativeLayout subNavView;
    protected Button subnavLeft;
    protected Button subnavRight;
    protected SubNavScrollView subnavScrollview;
    protected LinearLayout tabBox;
    private View topbarLeft;
    private String topbarName;
    private View topbarRight;

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.boardCategoryList = (ArrayList) this.intent.getSerializableExtra(IntentConstant.INTENT_CATEGORY_LIST);
        this.defaultCategory = ((Integer) this.intent.getSerializableExtra(IntentConstant.INTENT_WHICH_CATEGORY)).intValue();
        this.boardModelList = this.boardCategoryList.get(this.defaultCategory).getBoardList();
        this.topbarName = this.intent.getStringExtra("boardName");
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.mcResource.getLayoutId("base_classify_display_activity"));
        this.fragmentViewPager = (AutogenViewPager) findViewById(this.mcResource.getViewId("fragment_pager"));
        this.displayActivityAdapter = new ClassifyCategoryActivityAdapter(getSupportFragmentManager());
        this.fragmentViewPager.setAdapter(this.displayActivityAdapter);
        topbarTitle = (TextView) findViewById(this.mcResource.getViewId("ec_common_topbar_title"));
        topbarTitle.setText(this.topbarName);
        this.topbarRight = ((ViewStub) findViewById(this.mcResource.getViewId("ec_common_topbar_right"))).inflate();
        this.topbarLeft = ((ViewStub) findViewById(this.mcResource.getViewId("ec_common_topbar_left"))).inflate();
        this.topbarRight.setBackgroundResource(this.mcResource.getDrawableId("mc_forum_top_bar_button32"));
        this.topbarLeft.setBackgroundResource(this.mcResource.getDrawableId("mc_forum_top_bar_button1"));
        drawSubNavBar(0);
        this.handler.postDelayed(new Runnable() {
            public void run() {
                ClassifyDetailActivity.this.loadCurrentFragmentData();
            }
        }, 2000);
        onImageDeal(this.currentPosition);
    }

    /* access modifiers changed from: protected */
    public void initActions() {
        super.initActions();
        this.fragmentViewPager.setOnPageChangeListener(new OnMCPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
                super.onPageScrollStateChanged(state);
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            public void onPageSelected(int position) {
                super.onPageSelected(position);
                int unused = ClassifyDetailActivity.this.currentPosition = position;
                ClassifyDetailActivity.this.onSelectedSubNavItem(position);
                ClassifyDetailActivity.this.loadCurrentFragmentData();
                ClassifyDetailActivity.this.loadFallWall();
                ClassifyDetailActivity.this.recycleFallWall(position);
            }
        });
        this.topbarLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClassifyDetailActivity.this.finish();
            }
        });
        this.topbarRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClassifyDetailActivity.this.goodsSearch();
            }
        });
    }

    private void onImageDeal(int position) {
        recycleFallWall(position);
        this.handler.postDelayed(new Runnable() {
            public void run() {
                ClassifyDetailActivity.this.loadFallWall();
            }
        }, 300);
    }

    /* access modifiers changed from: private */
    public void loadFallWall() {
        FallWallDelegate delegate = (FallWallDelegate) this.displayActivityAdapter.getItem(this.currentPosition);
        if (delegate != null) {
            delegate.loadImageFallWall(this.currentPosition);
        }
    }

    /* access modifiers changed from: private */
    public void recycleFallWall(int position) {
        ClassifyCategoryActivityAdapter adapter = (ClassifyCategoryActivityAdapter) this.displayActivityAdapter;
        FallWallDelegate delegate = adapter.getLeftItem(position);
        if (delegate != null) {
            delegate.recycleImageFallWall(position - 1);
        }
        FallWallDelegate delegate2 = adapter.getRightItem(position);
        if (delegate2 != null) {
            delegate2.recycleImageFallWall(position + 1);
        }
    }

    private class ClassifyCategoryActivityAdapter extends FragmentStatePagerAdapter {
        private Map<Integer, BaseFragment> fragmentMap = new HashMap();

        public ClassifyCategoryActivityAdapter(FragmentManager fm) {
            super(fm);
        }

        /* Debug info: failed to restart local var, previous not found, register: 7 */
        public Fragment getItem(int position) {
            BaseFragment fragment;
            if (this.fragmentMap.get(Integer.valueOf(position)) != null) {
                return this.fragmentMap.get(Integer.valueOf(position));
            }
            switch (ClassifyDetailActivity.this.moduleModel.getListDisplay()) {
                case 0:
                    fragment = new WaterfallFragment(ClassifyDetailActivity.this.subNavView);
                    break;
                case 1:
                    fragment = new BigPicFragment(ClassifyDetailActivity.this.subNavView);
                    break;
                default:
                    fragment = new WaterfallFragment(ClassifyDetailActivity.this.subNavView);
                    break;
            }
            Bundle bundle = ClassifyDetailActivity.this.getBundle(position);
            bundle.putInt("position", position);
            bundle.putInt(IntentConstant.INTENT_MODULE_MARKING, 10003);
            bundle.putLong("boardId", ((BoardModel) ClassifyDetailActivity.this.boardModelList.get(position)).getBoardId());
            fragment.setArguments(bundle);
            this.fragmentMap.put(Integer.valueOf(position), fragment);
            return fragment;
        }

        public FallWallDelegate getLeftItem(int position) {
            BaseFragment fragment = this.fragmentMap.get(Integer.valueOf(position - 1));
            if (fragment == null) {
                return null;
            }
            return (FallWallDelegate) fragment;
        }

        public FallWallDelegate getRightItem(int position) {
            BaseFragment fragment = this.fragmentMap.get(Integer.valueOf(position + 1));
            if (fragment == null) {
                return null;
            }
            return (FallWallDelegate) fragment;
        }

        public int getCount() {
            return ClassifyDetailActivity.this.boardModelList.size();
        }
    }

    /* access modifiers changed from: protected */
    public void drawSubNavBar(int subNavPosition) {
        int categorySize;
        int i;
        if (this.isHasSubNavBar && (categorySize = this.boardCategoryList.get(this.defaultCategory).getBoardList().size()) > 1) {
            this.subItemMaxCount = this.subItemMaxCount == 0 ? 1 : this.subItemMaxCount;
            if (this.subNavView == null) {
                this.subNavView = (RelativeLayout) findViewById(this.mcResource.getViewId("subnav"));
                this.subNavView.setVisibility(8);
                this.subnavScrollview = (SubNavScrollView) findViewById(this.mcResource.getViewId("subnav_scrollview"));
                this.tabBox = (LinearLayout) findViewById(this.mcResource.getViewId("tab_box"));
                this.subnavLeft = (Button) findViewById(this.mcResource.getViewId("subnav_left"));
                this.subnavRight = (Button) findViewById(this.mcResource.getViewId("subnav_right"));
                this.subNavMoveHeight = PhoneUtil.getRawSize(getApplicationContext(), 1, 40.0f);
            }
            float subNavWidth = ((float) PhoneUtil.getDisplayWidth(getApplicationContext())) - (PhoneUtil.getDisplayDensity(getApplicationContext()) * 10.0f);
            if (categorySize > this.subItemMaxCount) {
                this.subnavRight.setVisibility(0);
                this.subnavScrollview.setScrollViewListener(new MyScrollViewListener());
            }
            this.limitWidth = (float) ((categorySize - this.subItemMaxCount) * ((int) ((subNavWidth / ((float) this.subItemMaxCount)) - (PhoneUtil.getDisplayDensity(getApplicationContext()) * 10.0f))));
            if (!this.isClickSubNavItem) {
                this.subnavScrollview.scrollTo((int) (((float) ((subNavPosition - this.subItemMaxCount) + 1)) * (subNavWidth / ((float) this.subItemMaxCount))), 0);
            }
            if (categorySize > this.subItemMaxCount) {
                i = this.subItemMaxCount;
            } else {
                i = categorySize;
            }
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((int) (subNavWidth / ((float) i)), -1);
            if (this.tabBox.getChildCount() == 0) {
                for (int i2 = 0; i2 < categorySize; i2++) {
                    if (this.tabBox.getChildAt(i2) == null) {
                        View view = this.inflater.inflate(this.mcResource.getLayoutId("subnav_item"), (ViewGroup) null);
                        view.setOnClickListener(new OnSubNavItemListener(i2));
                        this.tabBox.addView(view, lp);
                    }
                }
            }
            for (int j = 0; j < categorySize; j++) {
                View view2 = this.tabBox.getChildAt(j);
                TextView tabName = (TextView) view2.findViewById(this.mcResource.getViewId("tab_name"));
                if (subNavPosition == j) {
                    tabName.setText(this.boardCategoryList.get(this.defaultCategory).getBoardList().get(j).getBoardName());
                    tabName.setSelected(true);
                    view2.setSelected(true);
                } else {
                    tabName.setText(this.boardCategoryList.get(this.defaultCategory).getBoardList().get(j).getBoardName());
                    tabName.setSelected(false);
                    view2.setSelected(false);
                }
            }
            this.isClickSubNavItem = false;
        }
    }

    private class MyScrollViewListener implements SubNavScrollView.ScrollViewListener {
        private MyScrollViewListener() {
        }

        public void onScrollChanged(SubNavScrollView scrollView, int x, int y, int oldx, int oldy) {
            int scrollx = scrollView.getScrollX();
            if (scrollx > 0) {
                ClassifyDetailActivity.this.subnavLeft.setVisibility(0);
            }
            if (scrollx == 0) {
                ClassifyDetailActivity.this.subnavLeft.setVisibility(4);
            }
            if (ClassifyDetailActivity.this.tabBox.getMeasuredWidth() <= scrollView.getScrollX() + scrollView.getWidth()) {
                ClassifyDetailActivity.this.subnavRight.setVisibility(4);
            } else {
                ClassifyDetailActivity.this.subnavRight.setVisibility(0);
            }
        }
    }

    private class OnSubNavItemListener implements View.OnClickListener {
        private int position;

        public OnSubNavItemListener(int position2) {
            this.position = position2;
        }

        public void onClick(View v) {
            boolean unused = ClassifyDetailActivity.this.isClickSubNavItem = true;
            ClassifyDetailActivity.this.onClickSubNavItem(this.position);
        }
    }

    /* access modifiers changed from: protected */
    public void onClickSubNavItem(int position) {
        if (this.fragmentViewPager != null) {
            this.fragmentViewPager.setCurrentItem(position);
        }
    }

    /* access modifiers changed from: protected */
    public void onSelectedSubNavItem(int position) {
        boolean isChangePosition = false;
        if (this.currentPosition != position) {
            this.currentPosition = position;
            isChangePosition = true;
        }
        if (this.subNavView != null && this.displayActivityAdapter != null && ((BaseFragment) this.displayActivityAdapter.getItem(position)) != null) {
            if (isChangePosition && this.subNavView != null && this.subNavView.getVisibility() == 8) {
                Animation animationDown = new TranslateAnimation(0.0f, 0.0f, (float) (-this.subNavMoveHeight), 0.0f);
                animationDown.setDuration(300);
                animationDown.setFillAfter(true);
                this.subNavView.startAnimation(animationDown);
                animationDown.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                    }
                });
                this.subNavView.setVisibility(0);
            }
            if (this.subNavView != null) {
                drawSubNavBar(position);
            }
        }
    }

    /* access modifiers changed from: protected */
    public int getActivityDialogPattern() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.application.getAutogenDataCache().clearGoodsList();
    }
}
