package com.baidu.location;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.baidu.location.c;
import com.baidu.location.e;
import java.io.File;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.UUID;

class j {
    public static String A = "no";
    public static final boolean B = false;
    public static float C = 10.0f;
    public static float D = 2.0f;
    private static boolean E = false;
    public static float F = 2.3f;
    public static boolean G = true;
    public static boolean H = false;
    public static int I = 0;
    public static double J = 0.0d;
    public static int K = 10;
    public static int L = LocationClientOption.MIN_SCAN_SPAN;
    public static long M = 1200000;
    public static String N = "http://loc.map.baidu.com/sdk_ep.php";
    public static boolean O = true;
    public static boolean P = true;
    public static float Q = 200.0f;
    public static boolean R = true;
    public static int S = 5000;
    private static final int T = 128;
    public static float U = 3.8f;
    public static int V = 20;
    public static float W = 10.0f;
    public static int X = 2;
    public static int Y = 16;
    public static double Z = 0.0d;
    public static float a = 0.5f;
    public static int aa = 300;
    public static boolean ab = false;
    public static long ac = 20;
    public static int ad = 70;
    public static int ae = 70;
    public static int af = 0;
    public static boolean ag = false;
    public static String ah = "gcj02";
    public static float ai = 50.0f;
    private static Process aj = null;
    public static String ak = null;
    public static long al = 300000;
    public static float am = 1.1f;
    public static String b = null;

    /* renamed from: byte  reason: not valid java name */
    public static float f196byte = 0.9f;
    public static float c = 2.2f;

    /* renamed from: case  reason: not valid java name */
    public static final boolean f197case = false;

    /* renamed from: char  reason: not valid java name */
    private static String f198char = "http://loc.map.baidu.com/sdk.php";
    public static boolean d = false;

    /* renamed from: do  reason: not valid java name */
    public static String f199do = null;
    public static int e = 0;

    /* renamed from: else  reason: not valid java name */
    private static String f200else = "[baidu_location_service]";
    private static String f = f.v;

    /* renamed from: for  reason: not valid java name */
    public static int f201for = 20;
    public static int g = 0;

    /* renamed from: goto  reason: not valid java name */
    public static int f202goto = 6;
    public static int h = 0;
    private static final String i = (f.ac + "/con.dat");

    /* renamed from: if  reason: not valid java name */
    public static double f203if = 0.0d;

    /* renamed from: int  reason: not valid java name */
    public static int f204int = 7;
    public static byte[] j = null;
    private static boolean k = true;
    public static int l = 3;

    /* renamed from: long  reason: not valid java name */
    public static int f205long = 120;
    public static boolean m = true;
    public static boolean n = false;

    /* renamed from: new  reason: not valid java name */
    public static int f206new = -1;
    public static double o = 0.0d;
    public static int p = 3;
    public static int q = 100;
    public static int r = 30;
    public static float s = 0.1f;
    public static int t = -1;

    /* renamed from: try  reason: not valid java name */
    public static boolean f207try = true;
    public static float u = 0.0f;
    public static int v = 60;

    /* renamed from: void  reason: not valid java name */
    public static boolean f208void = true;
    public static float w = 6.0f;
    public static boolean x = true;
    public static final boolean y = false;
    public static boolean z = true;

    public static class a {
        private static final boolean a = false;

        /* renamed from: if  reason: not valid java name */
        private static final String f209if = a.class.getSimpleName();

        private static String a(Context context) {
            return b.a(context);
        }

        /* renamed from: if  reason: not valid java name */
        public static String m254if(Context context) {
            String a2 = a(context);
            String str = b.m255do(context);
            if (TextUtils.isEmpty(str)) {
                str = "0";
            }
            return a2 + "|" + new StringBuffer(str).reverse().toString();
        }
    }

    public static class b {
        private static final String a = "a";

        /* renamed from: do  reason: not valid java name */
        private static final String f210do = "bids";

        /* renamed from: for  reason: not valid java name */
        private static final String f211for = "i";

        /* renamed from: if  reason: not valid java name */
        private static final String f212if = "DeviceId";

        private b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.j.a(byte[], boolean):java.lang.String
         arg types: [byte[], int]
         candidates:
          com.baidu.location.j.a(java.lang.String, java.lang.String):void
          com.baidu.location.j.a(byte[], boolean):java.lang.String */
        public static String a(Context context) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(f210do, 0);
            String string = sharedPreferences.getString(f211for, null);
            if (string == null) {
                string = m255do(context);
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString(f211for, string);
                edit.commit();
            }
            String string2 = sharedPreferences.getString(a, null);
            if (string2 == null) {
                string2 = m256if(context);
                SharedPreferences.Editor edit2 = sharedPreferences.edit();
                edit2.putString(a, string2);
                edit2.commit();
            }
            String str = string2;
            String a2 = j.a(("com.baidu" + string + str).getBytes(), true);
            String string3 = Settings.System.getString(context.getContentResolver(), a2);
            if (!TextUtils.isEmpty(string3)) {
                return string3;
            }
            String a3 = j.a((string + str + UUID.randomUUID().toString()).getBytes(), true);
            Settings.System.putString(context.getContentResolver(), a2, a3);
            return !a3.equals(Settings.System.getString(context.getContentResolver(), a2)) ? a2 : a3;
        }

        /* renamed from: do  reason: not valid java name */
        public static String m255do(Context context) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager == null) {
                return "";
            }
            String deviceId = telephonyManager.getDeviceId();
            return TextUtils.isEmpty(deviceId) ? "" : deviceId;
        }

        /* renamed from: if  reason: not valid java name */
        public static String m256if(Context context) {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            return TextUtils.isEmpty(string) ? "" : string;
        }
    }

    j() {
    }

    static float a(String str, String str2, String str3) {
        if (str == null || str.equals("")) {
            return Float.MIN_VALUE;
        }
        int indexOf = str.indexOf(str2);
        if (indexOf == -1) {
            return Float.MIN_VALUE;
        }
        int length = indexOf + str2.length();
        int indexOf2 = str.indexOf(str3, length);
        if (indexOf2 == -1) {
            return Float.MIN_VALUE;
        }
        String substring = str.substring(length, indexOf2);
        if (substring == null || substring.equals("")) {
            return Float.MIN_VALUE;
        }
        try {
            return Float.parseFloat(substring);
        } catch (NumberFormatException e2) {
            m250if(f, "util numberFormatException, intStr : " + substring);
            e2.printStackTrace();
            return Float.MIN_VALUE;
        }
    }

    static String a() {
        Calendar instance = Calendar.getInstance();
        int i2 = instance.get(5);
        return String.format("%d_%d_%d_%d_%d_%d", Integer.valueOf(instance.get(1)), Integer.valueOf(instance.get(2) + 1), Integer.valueOf(i2), Integer.valueOf(instance.get(11)), Integer.valueOf(instance.get(12)), Integer.valueOf(instance.get(13)));
    }

    public static String a(c.a aVar, e.c cVar, Location location, String str, int i2) {
        String str2;
        String aVar2;
        StringBuffer stringBuffer = new StringBuffer();
        if (!(aVar == null || (aVar2 = aVar.toString()) == null)) {
            stringBuffer.append(aVar2);
        }
        if (cVar != null) {
            String str3 = i2 == 0 ? cVar.m141char() : cVar.m139byte();
            if (str3 != null) {
                stringBuffer.append(str3);
            }
        }
        if (location != null) {
            String str4 = (h == 0 || i2 == 0) ? b.m58do(location) : b.m71if(location);
            if (str4 != null) {
                stringBuffer.append(str4);
            }
        }
        boolean z2 = false;
        if (i2 == 0) {
            z2 = true;
        }
        String a2 = c.a(z2);
        if (a2 != null) {
            stringBuffer.append(a2);
        }
        if (str != null) {
            stringBuffer.append(str);
        }
        if (!(aVar == null || (str2 = aVar.m123int()) == null || str2.length() + stringBuffer.length() >= 750)) {
            stringBuffer.append(str2);
        }
        String stringBuffer2 = stringBuffer.toString();
        m250if(f, "util format : " + stringBuffer2);
        if (location == null || cVar == null) {
            l = 3;
        } else {
            try {
                float speed = location.getSpeed();
                int i3 = h;
                int i4 = cVar.m142do();
                int i5 = cVar.m151try();
                boolean z3 = cVar.m140case();
                if (speed < w && ((i3 == 1 || i3 == 0) && (i4 < v || z3))) {
                    l = 1;
                } else if (speed >= W || (!(i3 == 1 || i3 == 0 || i3 == 3) || (i4 >= ae && i5 <= f202goto))) {
                    l = 3;
                } else {
                    l = 2;
                }
            } catch (Exception e2) {
                l = 3;
            }
        }
        return stringBuffer2;
    }

    static String a(String str, String str2, String str3, double d2) {
        if (str == null || str.equals("")) {
            return null;
        }
        int indexOf = str.indexOf(str2);
        if (indexOf == -1) {
            return null;
        }
        int length = indexOf + str2.length();
        int indexOf2 = str.indexOf(str3, length);
        if (indexOf2 == -1) {
            return null;
        }
        String str4 = str.substring(0, length) + String.format("%.7f", Double.valueOf(d2)) + str.substring(indexOf2);
        m250if(f, "NEW:" + str4);
        return str4;
    }

    public static String a(byte[] bArr, String str, boolean z2) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            String hexString = Integer.toHexString(b2 & 255);
            if (z2) {
                hexString = hexString.toUpperCase();
            }
            if (hexString.length() == 1) {
                sb.append("0");
            }
            sb.append(hexString).append(str);
        }
        return sb.toString();
    }

    public static String a(byte[] bArr, boolean z2) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(bArr);
            return a(instance.digest(), "", z2);
        } catch (NoSuchAlgorithmException e2) {
            throw new RuntimeException(e2);
        }
    }

    public static void a(int i2) {
        O = (i2 & 1) == 1;
        P = (i2 & 2) == 2;
        n = (i2 & 4) == 4;
        x = (i2 & 8) == 8;
        z = (i2 & 65536) == 65536;
        m = (i2 & 131072) == 131072;
        m250if(f, "A1~C3:" + O + P + n + x + z + m);
    }

    public static void a(String str) {
        if (k) {
            Log.d(f200else, str);
        }
    }

    public static void a(String str, String str2) {
        if (E) {
            Log.d(str, str2);
        }
    }

    /* renamed from: do  reason: not valid java name */
    static double m242do(String str, String str2, String str3) {
        if (str == null || str.equals("")) {
            return Double.MIN_VALUE;
        }
        int indexOf = str.indexOf(str2);
        if (indexOf == -1) {
            return Double.MIN_VALUE;
        }
        int length = indexOf + str2.length();
        int indexOf2 = str.indexOf(str3, length);
        if (indexOf2 == -1) {
            return Double.MIN_VALUE;
        }
        String substring = str.substring(length, indexOf2);
        if (substring == null || substring.equals("")) {
            return Double.MIN_VALUE;
        }
        try {
            return Double.parseDouble(substring);
        } catch (NumberFormatException e2) {
            m250if(f, "util numberFormatException, doubleStr : " + substring);
            e2.printStackTrace();
            return Double.MIN_VALUE;
        }
    }

    /* renamed from: do  reason: not valid java name */
    public static String m243do() {
        return f198char;
    }

    /* renamed from: do  reason: not valid java name */
    public static boolean m244do(String str) {
        int i2 = m246if(str, "error\":\"", "\"");
        return i2 > 100 && i2 < 200;
    }

    /* renamed from: for  reason: not valid java name */
    static String m245for() {
        Calendar instance = Calendar.getInstance();
        int i2 = instance.get(5);
        return String.format("%d-%d-%d %d:%d:%d", Integer.valueOf(instance.get(1)), Integer.valueOf(instance.get(2) + 1), Integer.valueOf(i2), Integer.valueOf(instance.get(11)), Integer.valueOf(instance.get(12)), Integer.valueOf(instance.get(13)));
    }

    /* renamed from: if  reason: not valid java name */
    static int m246if(String str, String str2, String str3) {
        if (str == null || str.equals("")) {
            return Integer.MIN_VALUE;
        }
        int indexOf = str.indexOf(str2);
        if (indexOf == -1) {
            return Integer.MIN_VALUE;
        }
        int length = indexOf + str2.length();
        int indexOf2 = str.indexOf(str3, length);
        if (indexOf2 == -1) {
            return Integer.MIN_VALUE;
        }
        String substring = str.substring(length, indexOf2);
        if (substring == null || substring.equals("")) {
            return Integer.MIN_VALUE;
        }
        try {
            return Integer.parseInt(substring);
        } catch (NumberFormatException e2) {
            m250if(f, "util numberFormatException, intStr : " + substring);
            e2.printStackTrace();
            return Integer.MIN_VALUE;
        }
    }

    /* renamed from: if  reason: not valid java name */
    public static void m247if() {
        try {
            File file = new File(i);
            if (file.exists()) {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                randomAccessFile.seek(4);
                int readInt = randomAccessFile.readInt();
                int readInt2 = randomAccessFile.readInt();
                randomAccessFile.seek(128);
                byte[] bArr = new byte[readInt];
                int i2 = 0;
                while (true) {
                    if (i2 >= readInt2) {
                        break;
                    }
                    randomAccessFile.seek((long) ((readInt * i2) + 128));
                    int readInt3 = randomAccessFile.readInt();
                    if (readInt3 > 0 && readInt3 < readInt) {
                        randomAccessFile.read(bArr, 0, readInt3);
                        if (bArr[readInt3 - 1] == 0) {
                            String str = new String(bArr, 0, readInt3 - 1);
                            m250if(f, "a:" + str);
                            if (str.equals(ak)) {
                                f206new = randomAccessFile.readInt();
                                e = i2;
                                m250if(f, str + f206new);
                                break;
                            }
                        } else {
                            continue;
                        }
                    }
                    i2++;
                }
                if (i2 == readInt2) {
                    e = readInt2;
                }
                randomAccessFile.close();
            }
        } catch (Exception e2) {
        }
    }

    /* renamed from: if  reason: not valid java name */
    public static void m248if(int i2) {
        File file = new File(i);
        if (!file.exists()) {
            m251int();
        }
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            randomAccessFile.seek(4);
            int readInt = randomAccessFile.readInt();
            int readInt2 = randomAccessFile.readInt();
            randomAccessFile.seek((long) ((readInt * e) + 128));
            byte[] bytes = (ak + 0).getBytes();
            randomAccessFile.writeInt(bytes.length);
            randomAccessFile.write(bytes, 0, bytes.length);
            randomAccessFile.writeInt(i2);
            if (readInt2 == e) {
                randomAccessFile.seek(8);
                randomAccessFile.writeInt(readInt2 + 1);
            }
            randomAccessFile.close();
        } catch (Exception e2) {
        }
    }

    /* renamed from: if  reason: not valid java name */
    public static void m249if(String str) {
        if (E && str != null) {
            f198char = str;
        }
    }

    /* renamed from: if  reason: not valid java name */
    public static void m250if(String str, String str2) {
    }

    /* renamed from: int  reason: not valid java name */
    public static void m251int() {
        try {
            File file = new File(i);
            if (!file.exists()) {
                File file2 = new File(f.ac);
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                if (!file.createNewFile()) {
                    file = null;
                }
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                randomAccessFile.seek(0);
                randomAccessFile.writeInt(0);
                randomAccessFile.writeInt(128);
                randomAccessFile.writeInt(0);
                randomAccessFile.close();
            }
        } catch (Exception e2) {
        }
    }

    /* renamed from: new  reason: not valid java name */
    public static void m252new() {
        if (aj != null) {
            try {
                m250if(f, "logcat stop...");
                aj.destroy();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* renamed from: try  reason: not valid java name */
    public static void m253try() {
        if (E) {
            try {
                if (aj != null) {
                    aj.destroy();
                    aj = null;
                }
                File file = new File(f.ac);
                if (file.exists()) {
                    m250if("sdkdemo_applocation", "directory already exists...");
                } else {
                    file.mkdirs();
                    m250if("sdkdemo_applocation", "directory not exists, make dirs...");
                }
                m250if(f, "logcat start ...");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
