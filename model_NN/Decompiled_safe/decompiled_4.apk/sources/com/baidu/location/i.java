package com.baidu.location;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.forum.android.os.service.HeartMsgOSService;
import java.util.ArrayList;
import java.util.Iterator;

public class i {

    /* renamed from: new  reason: not valid java name */
    public static final String f183new = "android.com.baidu.location.TIMER.NOTIFY";
    private int a = 0;
    private long b = 0;
    /* access modifiers changed from: private */

    /* renamed from: byte  reason: not valid java name */
    public ArrayList f184byte = null;
    private boolean c = false;

    /* renamed from: case  reason: not valid java name */
    private BDLocation f185case = null;

    /* renamed from: char  reason: not valid java name */
    private long f186char = 0;
    /* access modifiers changed from: private */
    public LocationClient d = null;
    /* access modifiers changed from: private */

    /* renamed from: do  reason: not valid java name */
    public String f187do = f.v;

    /* renamed from: else  reason: not valid java name */
    private b f188else = null;

    /* renamed from: for  reason: not valid java name */
    private AlarmManager f189for = null;

    /* renamed from: goto  reason: not valid java name */
    private float f190goto = Float.MAX_VALUE;

    /* renamed from: if  reason: not valid java name */
    private Context f191if = null;

    /* renamed from: int  reason: not valid java name */
    private a f192int = new a();

    /* renamed from: long  reason: not valid java name */
    private boolean f193long = false;

    /* renamed from: try  reason: not valid java name */
    private PendingIntent f194try = null;

    /* renamed from: void  reason: not valid java name */
    private boolean f195void = false;

    public class a implements BDLocationListener {
        public a() {
        }

        public void onReceiveLocation(BDLocation bDLocation) {
            i.this.a(bDLocation);
        }

        public void onReceivePoi(BDLocation bDLocation) {
        }
    }

    public class b extends BroadcastReceiver {
        public b() {
        }

        public void onReceive(Context context, Intent intent) {
            j.m250if(i.this.f187do, "timer expire,request location...");
            if (i.this.f184byte != null && !i.this.f184byte.isEmpty()) {
                i.this.d.requestNotifyLocation();
            }
        }
    }

    public i(Context context, LocationClient locationClient) {
        this.f191if = context;
        this.d = locationClient;
        this.d.registerNotifyLocationListener(this.f192int);
        this.f189for = (AlarmManager) this.f191if.getSystemService("alarm");
        this.f188else = new b();
        this.c = false;
    }

    private void a() {
        boolean z = true;
        if (m237do()) {
            int i = this.f190goto > 5000.0f ? HeartMsgOSService.HEART_BEAT_TIME_OUT : this.f190goto > 1000.0f ? 120000 : this.f190goto > 500.0f ? 60000 : 10000;
            if (this.f193long) {
                this.f193long = false;
                i = 10000;
            }
            if (this.a != 0) {
                if (((long) i) > (this.f186char + ((long) this.a)) - System.currentTimeMillis()) {
                    z = false;
                }
            }
            if (z) {
                this.a = i;
                this.f186char = System.currentTimeMillis();
                a((long) this.a);
            }
        }
    }

    private void a(long j) {
        if (this.f195void) {
            this.f189for.cancel(this.f194try);
        }
        this.f194try = PendingIntent.getBroadcast(this.f191if, 0, new Intent(f183new), 134217728);
        this.f189for.set(0, System.currentTimeMillis() + j, this.f194try);
        j.m250if(this.f187do, "timer start:" + j);
    }

    /* access modifiers changed from: private */
    public void a(BDLocation bDLocation) {
        float f;
        j.m250if(this.f187do, "notify new loation");
        this.f195void = false;
        if (bDLocation.getLocType() != 61 && bDLocation.getLocType() != 161 && bDLocation.getLocType() != 65) {
            a(120000);
        } else if (System.currentTimeMillis() - this.b >= 5000 && this.f184byte != null) {
            this.f185case = bDLocation;
            this.b = System.currentTimeMillis();
            float[] fArr = new float[1];
            float f2 = Float.MAX_VALUE;
            Iterator it = this.f184byte.iterator();
            while (true) {
                f = f2;
                if (!it.hasNext()) {
                    break;
                }
                BDNotifyListener bDNotifyListener = (BDNotifyListener) it.next();
                Location.distanceBetween(bDLocation.getLatitude(), bDLocation.getLongitude(), bDNotifyListener.mLatitudeC, bDNotifyListener.mLongitudeC, fArr);
                f2 = (fArr[0] - bDNotifyListener.mRadius) - bDLocation.getRadius();
                j.m250if(this.f187do, "distance:" + f2);
                if (f2 > 0.0f) {
                    if (f2 < f) {
                    }
                    f2 = f;
                } else {
                    if (bDNotifyListener.Notified < 3) {
                        bDNotifyListener.Notified++;
                        bDNotifyListener.onNotify(bDLocation, fArr[0]);
                        if (bDNotifyListener.Notified < 3) {
                            this.f193long = true;
                        }
                    }
                    f2 = f;
                }
            }
            if (f < this.f190goto) {
                this.f190goto = f;
            }
            this.a = 0;
            a();
        }
    }

    /* renamed from: do  reason: not valid java name */
    private boolean m237do() {
        boolean z = false;
        if (this.f184byte == null || this.f184byte.isEmpty()) {
            return false;
        }
        Iterator it = this.f184byte.iterator();
        while (it.hasNext()) {
            if (((BDNotifyListener) it.next()).Notified < 3) {
                z = true;
            }
        }
        return z;
    }

    public void a(BDNotifyListener bDNotifyListener) {
        j.m250if(this.f187do, bDNotifyListener.mCoorType + "2gcj");
        if (bDNotifyListener.mCoorType != null) {
            if (!bDNotifyListener.mCoorType.equals("gcj02")) {
                double[] dArr = Jni.m1if(bDNotifyListener.mLongitude, bDNotifyListener.mLatitude, bDNotifyListener.mCoorType + "2gcj");
                bDNotifyListener.mLongitudeC = dArr[0];
                bDNotifyListener.mLatitudeC = dArr[1];
                j.m250if(this.f187do, bDNotifyListener.mCoorType + "2gcj");
                j.m250if(this.f187do, "coor:" + bDNotifyListener.mLongitude + AdApiConstant.RES_SPLIT_COMMA + bDNotifyListener.mLatitude + ":" + bDNotifyListener.mLongitudeC + AdApiConstant.RES_SPLIT_COMMA + bDNotifyListener.mLatitudeC);
            }
            if (this.f185case == null || System.currentTimeMillis() - this.b > 300000) {
                this.d.requestNotifyLocation();
            } else {
                float[] fArr = new float[1];
                Location.distanceBetween(this.f185case.getLatitude(), this.f185case.getLongitude(), bDNotifyListener.mLatitudeC, bDNotifyListener.mLongitudeC, fArr);
                float radius = (fArr[0] - bDNotifyListener.mRadius) - this.f185case.getRadius();
                if (radius > 0.0f) {
                    if (radius < this.f190goto) {
                        this.f190goto = radius;
                    }
                } else if (bDNotifyListener.Notified < 3) {
                    bDNotifyListener.Notified++;
                    bDNotifyListener.onNotify(this.f185case, fArr[0]);
                    if (bDNotifyListener.Notified < 3) {
                        this.f193long = true;
                    }
                }
            }
            a();
        }
    }

    /* renamed from: do  reason: not valid java name */
    public int m239do(BDNotifyListener bDNotifyListener) {
        if (this.f184byte == null) {
            return 0;
        }
        if (this.f184byte.contains(bDNotifyListener)) {
            this.f184byte.remove(bDNotifyListener);
        }
        if (this.f184byte.size() == 0 && this.f195void) {
            this.f189for.cancel(this.f194try);
        }
        return 1;
    }

    /* renamed from: if  reason: not valid java name */
    public int m240if(BDNotifyListener bDNotifyListener) {
        if (this.f184byte == null) {
            this.f184byte = new ArrayList();
        }
        this.f184byte.add(bDNotifyListener);
        bDNotifyListener.isAdded = true;
        bDNotifyListener.mNotifyCache = this;
        if (!this.c) {
            this.f191if.registerReceiver(this.f188else, new IntentFilter(f183new));
            this.c = true;
        }
        if (bDNotifyListener.mCoorType == null) {
            return 1;
        }
        if (!bDNotifyListener.mCoorType.equals("gcj02")) {
            double[] dArr = Jni.m1if(bDNotifyListener.mLongitude, bDNotifyListener.mLatitude, bDNotifyListener.mCoorType + "2gcj");
            bDNotifyListener.mLongitudeC = dArr[0];
            bDNotifyListener.mLatitudeC = dArr[1];
            j.m250if(this.f187do, bDNotifyListener.mCoorType + "2gcj");
            j.m250if(this.f187do, "coor:" + bDNotifyListener.mLongitude + AdApiConstant.RES_SPLIT_COMMA + bDNotifyListener.mLatitude + ":" + bDNotifyListener.mLongitudeC + AdApiConstant.RES_SPLIT_COMMA + bDNotifyListener.mLatitudeC);
        }
        if (this.f185case == null || System.currentTimeMillis() - this.b > 30000) {
            this.d.requestNotifyLocation();
        } else {
            float[] fArr = new float[1];
            Location.distanceBetween(this.f185case.getLatitude(), this.f185case.getLongitude(), bDNotifyListener.mLatitudeC, bDNotifyListener.mLongitudeC, fArr);
            float radius = (fArr[0] - bDNotifyListener.mRadius) - this.f185case.getRadius();
            if (radius > 0.0f) {
                if (radius < this.f190goto) {
                    this.f190goto = radius;
                }
            } else if (bDNotifyListener.Notified < 3) {
                bDNotifyListener.Notified++;
                bDNotifyListener.onNotify(this.f185case, fArr[0]);
                if (bDNotifyListener.Notified < 3) {
                    this.f193long = true;
                }
            }
        }
        a();
        return 1;
    }

    /* renamed from: if  reason: not valid java name */
    public void m241if() {
        if (this.f195void) {
            this.f189for.cancel(this.f194try);
        }
        this.f185case = null;
        this.b = 0;
        if (this.c) {
            j.m250if(this.f187do, "unregister...");
            this.f191if.unregisterReceiver(this.f188else);
        }
        this.c = false;
    }
}
