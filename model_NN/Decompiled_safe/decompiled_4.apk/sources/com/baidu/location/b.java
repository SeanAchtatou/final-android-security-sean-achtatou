package com.baidu.location;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import com.mobcent.ad.android.constant.AdApiConstant;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class b {
    /* access modifiers changed from: private */
    public static File A = new File(f.ac, N);
    private static final int B = 750;
    private static final int G = 5;
    /* access modifiers changed from: private */
    public static int I = 0;
    /* access modifiers changed from: private */
    public static long J = 0;
    /* access modifiers changed from: private */
    public static long L = 0;
    private static final int M = 5;
    private static String N = "Temp_in.dat";
    /* access modifiers changed from: private */
    public static int b = 0;
    /* access modifiers changed from: private */

    /* renamed from: byte  reason: not valid java name */
    public static long f62byte = 0;
    /* access modifiers changed from: private */
    public static int c = 0;
    /* access modifiers changed from: private */

    /* renamed from: case  reason: not valid java name */
    public static int f63case = 0;
    /* access modifiers changed from: private */

    /* renamed from: char  reason: not valid java name */
    public static boolean f64char = true;

    /* renamed from: else  reason: not valid java name */
    private static final double f65else = 1.0E-5d;

    /* renamed from: goto  reason: not valid java name */
    private static final int f66goto = 3000;
    private static final int h = 1024;
    private static final int i = 1000;

    /* renamed from: if  reason: not valid java name */
    private static final String f67if = "baidu_location_service";
    /* access modifiers changed from: private */
    public static int j = 0;
    /* access modifiers changed from: private */
    public static int k = 0;
    private static final int l = 12;
    /* access modifiers changed from: private */
    public static double m = 0.0d;
    /* access modifiers changed from: private */
    public static double o = 0.0d;
    private static final int p = 1;
    /* access modifiers changed from: private */
    public static String r = null;
    private static final int s = 3;
    private static final int v = 100;

    /* renamed from: void  reason: not valid java name */
    private static final int f68void = 3600;
    /* access modifiers changed from: private */
    public static StringBuffer y = null;
    private static final int z = 5;
    /* access modifiers changed from: private */
    public boolean C = false;
    private String D = null;
    /* access modifiers changed from: private */
    public long E = 0;
    private Location F;
    private Handler H = null;
    private final int K = 400;
    /* access modifiers changed from: private */
    public GpsStatus a;
    /* access modifiers changed from: private */
    public long d = 0;
    /* access modifiers changed from: private */

    /* renamed from: do  reason: not valid java name */
    public LocationManager f69do = null;
    /* access modifiers changed from: private */
    public boolean e = false;
    private Context f;

    /* renamed from: for  reason: not valid java name */
    private d f70for = null;
    /* access modifiers changed from: private */
    public String g = null;
    /* access modifiers changed from: private */

    /* renamed from: int  reason: not valid java name */
    public boolean f71int = false;
    /* access modifiers changed from: private */

    /* renamed from: long  reason: not valid java name */
    public long f72long = 0;
    /* access modifiers changed from: private */
    public String n = null;

    /* renamed from: new  reason: not valid java name */
    private a f73new = null;
    private final long q = 1000;
    private boolean t = false;
    /* access modifiers changed from: private */

    /* renamed from: try  reason: not valid java name */
    public String f74try = null;
    /* access modifiers changed from: private */
    public List u = new ArrayList();
    private boolean w = true;
    private C0001b x = null;

    private class a implements GpsStatus.NmeaListener, GpsStatus.Listener {
        private a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.if(com.baidu.location.b, boolean):void
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.if(com.baidu.location.b, long):long
          com.baidu.location.b.if(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.if(com.baidu.location.b, boolean):void */
        public void onGpsStatusChanged(int i) {
            if (b.this.f69do != null) {
                switch (i) {
                    case 2:
                        b.this.a((Location) null);
                        b.this.a(false);
                        int unused = b.k = 0;
                        return;
                    case 3:
                    default:
                        return;
                    case 4:
                        j.m250if("baidu_location_service", "gps status change");
                        if (b.this.a == null) {
                            GpsStatus unused2 = b.this.a = b.this.f69do.getGpsStatus(null);
                        } else {
                            b.this.f69do.getGpsStatus(b.this.a);
                        }
                        int i2 = 0;
                        for (GpsSatellite usedInFix : b.this.a.getSatellites()) {
                            i2 = usedInFix.usedInFix() ? i2 + 1 : i2;
                        }
                        j.m250if("baidu_location_service", "gps nunmber in count:" + i2);
                        if (b.k >= 3 && i2 < 3) {
                            long unused3 = b.this.d = System.currentTimeMillis();
                        }
                        if (i2 < 3) {
                            b.this.a(false);
                        }
                        if (b.k <= 3 && i2 > 3) {
                            b.this.a(true);
                        }
                        int unused4 = b.k = i2;
                        return;
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.do(com.baidu.location.b, boolean):boolean
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.do(com.baidu.location.b, long):long
          com.baidu.location.b.do(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.do(com.baidu.location.b, boolean):boolean */
        public void onNmeaReceived(long j, String str) {
            if (!j.m) {
                j.h = 0;
            } else if (str != null && !str.equals("") && str.length() >= 9 && str.length() <= 150 && b.this.m88new()) {
                long currentTimeMillis = System.currentTimeMillis();
                j.m250if("baidu_location_service", "gps manager onNmeaReceived : " + currentTimeMillis + " " + str);
                if (currentTimeMillis - b.this.E > 400 && b.this.e && b.this.u.size() > 0) {
                    try {
                        c cVar = new c(b.this.u, b.this.g, b.this.f74try, b.this.n);
                        if (cVar.m97if()) {
                            j.h = cVar.m91case();
                            if (j.h > 0) {
                                String unused = b.r = String.format("&nmea=%.1f|%.1f&g_tp=%d", Double.valueOf(cVar.a()), Double.valueOf(cVar.m90byte()), Integer.valueOf(j.h));
                            }
                        } else {
                            j.h = 0;
                            j.m250if("baidu_location_service", "nmea invalid");
                        }
                    } catch (Exception e) {
                        j.h = 0;
                    }
                    b.this.u.clear();
                    String unused2 = b.this.g = b.this.f74try = b.this.n = (String) null;
                    boolean unused3 = b.this.e = false;
                }
                if (str.startsWith("$GPGGA")) {
                    boolean unused4 = b.this.e = true;
                    String unused5 = b.this.g = str.trim();
                } else if (str.startsWith("$GPGSV")) {
                    b.this.u.add(str.trim());
                } else if (str.startsWith("$GPGSA")) {
                    String unused6 = b.this.n = str.trim();
                }
                long unused7 = b.this.E = System.currentTimeMillis();
            }
        }
    }

    /* renamed from: com.baidu.location.b$b  reason: collision with other inner class name */
    private class C0001b implements LocationListener {
        private C0001b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.a(com.baidu.location.b, boolean):boolean
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.a(com.baidu.location.b, long):long
          com.baidu.location.b.a(com.baidu.location.b, android.location.GpsStatus):android.location.GpsStatus
          com.baidu.location.b.a(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.a(com.baidu.location.b, android.location.Location):void
          com.baidu.location.b.a(com.baidu.location.b, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.if(com.baidu.location.b, boolean):void
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.if(com.baidu.location.b, long):long
          com.baidu.location.b.if(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.if(com.baidu.location.b, boolean):void */
        public void onLocationChanged(Location location) {
            b.this.a(location);
            boolean unused = b.this.f71int = false;
            if (b.this.C) {
                b.this.a(true);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.if(com.baidu.location.b, boolean):void
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.if(com.baidu.location.b, long):long
          com.baidu.location.b.if(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.if(com.baidu.location.b, boolean):void */
        public void onProviderDisabled(String str) {
            b.this.a((Location) null);
            b.this.a(false);
        }

        public void onProviderEnabled(String str) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.if(com.baidu.location.b, boolean):void
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.if(com.baidu.location.b, long):long
          com.baidu.location.b.if(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.if(com.baidu.location.b, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.b.a(com.baidu.location.b, boolean):boolean
         arg types: [com.baidu.location.b, int]
         candidates:
          com.baidu.location.b.a(com.baidu.location.b, long):long
          com.baidu.location.b.a(com.baidu.location.b, android.location.GpsStatus):android.location.GpsStatus
          com.baidu.location.b.a(com.baidu.location.b, java.lang.String):java.lang.String
          com.baidu.location.b.a(com.baidu.location.b, android.location.Location):void
          com.baidu.location.b.a(com.baidu.location.b, boolean):boolean */
        public void onStatusChanged(String str, int i, Bundle bundle) {
            switch (i) {
                case 0:
                    b.this.a((Location) null);
                    b.this.a(false);
                    return;
                case 1:
                    long unused = b.this.f72long = System.currentTimeMillis();
                    boolean unused2 = b.this.f71int = true;
                    b.this.a(false);
                    return;
                case 2:
                    boolean unused3 = b.this.f71int = false;
                    return;
                default:
                    return;
            }
        }
    }

    private class c {
        public int a;
        private String b;

        /* renamed from: byte  reason: not valid java name */
        private double f75byte;

        /* renamed from: case  reason: not valid java name */
        private char f76case;

        /* renamed from: char  reason: not valid java name */
        private String f77char;
        private int d;

        /* renamed from: do  reason: not valid java name */
        private List f78do;
        private int e;

        /* renamed from: else  reason: not valid java name */
        private List f79else;
        private String f;

        /* renamed from: for  reason: not valid java name */
        private double f80for;
        private String g;

        /* renamed from: goto  reason: not valid java name */
        private boolean f81goto;
        private boolean h;

        /* renamed from: if  reason: not valid java name */
        private int f82if;

        /* renamed from: int  reason: not valid java name */
        private String f83int;

        /* renamed from: long  reason: not valid java name */
        private int f84long;

        /* renamed from: new  reason: not valid java name */
        private boolean f85new;

        /* renamed from: try  reason: not valid java name */
        private boolean f86try;

        /* renamed from: void  reason: not valid java name */
        private boolean f87void;

        private class a {
            private int a = 0;

            /* renamed from: do  reason: not valid java name */
            private int f88do = 0;

            /* renamed from: if  reason: not valid java name */
            private int f90if = 0;

            /* renamed from: int  reason: not valid java name */
            private int f91int = 0;

            public a(int i, int i2, int i3, int i4) {
                this.f91int = i;
                this.a = i2;
                this.f90if = i3;
                this.f88do = i4;
            }

            public int a() {
                return this.a;
            }

            /* renamed from: do  reason: not valid java name */
            public int m107do() {
                return this.f88do;
            }

            /* renamed from: if  reason: not valid java name */
            public int m108if() {
                return this.f90if;
            }
        }

        private c(List list, String str, String str2, String str3) {
            this.f81goto = false;
            this.f77char = "";
            this.h = false;
            this.f83int = "";
            this.f84long = 0;
            this.d = 0;
            this.f = "";
            this.f85new = false;
            this.b = "";
            this.f76case = 'N';
            this.g = "";
            this.f87void = false;
            this.f82if = 1;
            this.f75byte = 0.0d;
            this.f80for = 0.0d;
            this.f78do = null;
            this.f86try = false;
            this.f79else = null;
            this.e = 0;
            this.a = 0;
            this.f78do = list;
            this.f77char = str;
            this.f = str2;
            this.g = str3;
            this.f79else = new ArrayList();
            m103try();
        }

        /* access modifiers changed from: private */
        public double a() {
            return this.f80for;
        }

        private int a(boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
            double[] a2;
            if (!this.f81goto) {
                return 0;
            }
            if (z && this.h) {
                this.a = 1;
                if (this.d >= j.K) {
                    return 1;
                }
                if (this.d <= j.p) {
                    return 4;
                }
            }
            if (z2 && this.f87void) {
                this.a = 2;
                if (this.f80for <= ((double) j.am)) {
                    return 1;
                }
                if (this.f80for >= ((double) j.c)) {
                    return 4;
                }
            }
            if (z3 && this.f87void) {
                this.a = 3;
                if (this.f75byte <= ((double) j.F)) {
                    return 1;
                }
                if (this.f75byte >= ((double) j.U)) {
                    return 4;
                }
            }
            if (!this.f86try) {
                return 0;
            }
            if (z4) {
                this.a = 4;
                int i = 0;
                for (a aVar : this.f79else) {
                    if (aVar.m107do() >= j.f201for) {
                        i++;
                    }
                }
                if (i >= j.f204int) {
                    return 1;
                }
                if (i <= j.X) {
                    return 4;
                }
            }
            if (z5) {
                this.a = 5;
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                for (int i2 = 0; i2 < 10; i2++) {
                    arrayList.add(new ArrayList());
                }
                int i3 = 0;
                for (a aVar2 : this.f79else) {
                    if (aVar2.m107do() >= 10 && aVar2.m108if() >= 1) {
                        ((List) arrayList.get((aVar2.m107do() - 10) / 5)).add(aVar2);
                        i3++;
                    }
                }
                if (i3 < 4) {
                    return 4;
                }
                for (int i4 = 0; i4 < arrayList.size(); i4++) {
                    if (!(((List) arrayList.get(i4)).size() == 0 || (a2 = a((List) arrayList.get(i4))) == null)) {
                        arrayList2.add(a2);
                        arrayList3.add(Integer.valueOf(i4));
                    }
                }
                if (arrayList2 == null || arrayList2.size() <= 0) {
                    return 4;
                }
                double[] dArr = (double[]) arrayList2.get(0);
                dArr[0] = dArr[0] * ((double) ((Integer) arrayList3.get(0)).intValue());
                dArr[1] = dArr[1] * ((double) ((Integer) arrayList3.get(0)).intValue());
                if (arrayList2.size() > 1) {
                    for (int i5 = 1; i5 < arrayList2.size(); i5++) {
                        double[] dArr2 = (double[]) arrayList2.get(i5);
                        dArr2[0] = dArr2[0] * ((double) ((Integer) arrayList3.get(i5)).intValue());
                        dArr2[1] = dArr2[1] * ((double) ((Integer) arrayList3.get(i5)).intValue());
                        dArr[0] = dArr[0] + dArr2[0];
                        dArr[1] = dArr[1] + dArr2[1];
                    }
                    dArr[0] = dArr[0] / ((double) arrayList2.size());
                    dArr[1] = dArr[1] / ((double) arrayList2.size());
                }
                double[] a3 = a(dArr[0], dArr[1]);
                if (a3[0] <= ((double) j.ad)) {
                    return 1;
                }
                if (a3[0] >= ((double) j.f205long)) {
                    return 4;
                }
            }
            this.a = 0;
            return 3;
        }

        private boolean a(String str) {
            if (str != null && str.length() > 8) {
                char c2 = 0;
                for (int i = 1; i < str.length() - 3; i++) {
                    c2 ^= str.charAt(i);
                }
                if (Integer.toHexString(c2).equalsIgnoreCase(str.substring(str.length() - 2, str.length()))) {
                    return true;
                }
            }
            return false;
        }

        private double[] a(double d2, double d3) {
            return new double[]{Math.sqrt((d2 * d2) + (d3 * d3)), d3 == 0.0d ? d2 > 0.0d ? 90.0d : d2 < 0.0d ? 270.0d : 0.0d : Math.toDegrees(Math.atan(d2 / d3))};
        }

        private double[] a(List list) {
            if (list == null || list.size() <= 0) {
                return null;
            }
            double[] dArr = m99if((double) (90 - ((a) list.get(0)).m108if()), (double) ((a) list.get(0)).a());
            if (list.size() > 1) {
                for (int i = 1; i < list.size(); i++) {
                    double[] dArr2 = m99if((double) (90 - ((a) list.get(i)).m108if()), (double) ((a) list.get(i)).a());
                    dArr[0] = dArr[0] + dArr2[0];
                    dArr[1] = dArr[1] + dArr2[1];
                }
                dArr[0] = dArr[0] / ((double) list.size());
                dArr[1] = dArr[1] / ((double) list.size());
            }
            return dArr;
        }

        /* access modifiers changed from: private */
        /* renamed from: byte  reason: not valid java name */
        public double m90byte() {
            return this.f75byte;
        }

        /* access modifiers changed from: private */
        /* renamed from: case  reason: not valid java name */
        public int m91case() {
            return a(true, true, true, true, true);
        }

        /* renamed from: char  reason: not valid java name */
        private String m92char() {
            return this.g;
        }

        /* renamed from: for  reason: not valid java name */
        private boolean m95for() {
            return this.h;
        }

        /* renamed from: goto  reason: not valid java name */
        private boolean m96goto() {
            return this.f87void;
        }

        /* access modifiers changed from: private */
        /* renamed from: if  reason: not valid java name */
        public boolean m97if() {
            return this.f81goto;
        }

        /* renamed from: if  reason: not valid java name */
        private double[] m99if(double d2, double d3) {
            return new double[]{Math.sin(Math.toRadians(d3)) * d2, Math.cos(Math.toRadians(d3)) * d2};
        }

        /* renamed from: int  reason: not valid java name */
        private List m100int() {
            return this.f78do;
        }

        /* renamed from: long  reason: not valid java name */
        private boolean m101long() {
            return this.f85new;
        }

        /* renamed from: new  reason: not valid java name */
        private boolean m102new() {
            return this.f86try;
        }

        /* renamed from: try  reason: not valid java name */
        private void m103try() {
            if (a(this.f77char)) {
                String[] split = this.f77char.split(AdApiConstant.RES_SPLIT_COMMA);
                if (split.length == 15) {
                    if (!split[6].equals("") && !split[7].equals("")) {
                        this.f84long = Integer.valueOf(split[6]).intValue();
                        this.d = Integer.valueOf(split[7]).intValue();
                        this.h = true;
                    }
                } else {
                    return;
                }
            }
            if (a(this.g)) {
                String substring = this.g.substring(0, this.g.length() - 3);
                int i = 0;
                for (int i2 = 0; i2 < substring.length(); i2++) {
                    if (substring.charAt(i2) == ',') {
                        i++;
                    }
                }
                String[] split2 = substring.split(AdApiConstant.RES_SPLIT_COMMA, i + 1);
                if (split2.length < 6) {
                    return;
                }
                if (!split2[2].equals("") && !split2[split2.length - 3].equals("") && !split2[split2.length - 2].equals("") && !split2[split2.length - 1].equals("")) {
                    this.f82if = Integer.valueOf(split2[2]).intValue();
                    this.f75byte = Double.valueOf(split2[split2.length - 3]).doubleValue();
                    this.f80for = Double.valueOf(split2[split2.length - 2]).doubleValue();
                    this.f87void = true;
                }
            }
            if (this.f78do != null && this.f78do.size() > 0) {
                Iterator it = this.f78do.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    String str = (String) it.next();
                    if (!a(str)) {
                        this.f86try = false;
                        break;
                    }
                    String substring2 = str.substring(0, str.length() - 3);
                    int i3 = 0;
                    for (int i4 = 0; i4 < substring2.length(); i4++) {
                        if (substring2.charAt(i4) == ',') {
                            i3++;
                        }
                    }
                    String[] split3 = substring2.split(AdApiConstant.RES_SPLIT_COMMA, i3 + 1);
                    this.f86try = Integer.valueOf(split3[1]).intValue() == this.f78do.size() && split3.length > 8;
                    if (!this.f86try) {
                        break;
                    }
                    int i5 = 4;
                    while (true) {
                        int i6 = i5;
                        if (i6 < split3.length) {
                            if (!split3[i6].equals("") && !split3[i6 + 1].equals("") && !split3[i6 + 2].equals("")) {
                                this.e++;
                                this.f79else.add(new a(Integer.valueOf(split3[i6]).intValue(), Integer.valueOf(split3[i6 + 2]).intValue(), Integer.valueOf(split3[i6 + 1]).intValue(), split3[i6 + 3].equals("") ? 0 : Integer.valueOf(split3[i6 + 3]).intValue()));
                            }
                            i5 = i6 + 4;
                        }
                    }
                }
            } else {
                this.f86try = false;
            }
            this.f81goto = this.h && this.f87void;
        }

        /* renamed from: void  reason: not valid java name */
        private String m104void() {
            return this.f;
        }

        /* renamed from: do  reason: not valid java name */
        public String m105do() {
            return this.f83int;
        }

        /* renamed from: else  reason: not valid java name */
        public int m106else() {
            return this.f84long;
        }
    }

    public static class d {
        private boolean a = true;

        /* renamed from: if  reason: not valid java name */
        private String f92if = null;

        public d(String str) {
            this.f92if = str != null ? str.length() > b.v ? str.substring(0, b.v) : str : "";
        }

        private String a(int i) {
            if (!b.A.exists()) {
                return null;
            }
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(b.A, "rw");
                randomAccessFile.seek(0);
                int readInt = randomAccessFile.readInt();
                if (!b.m75if(readInt, randomAccessFile.readInt(), randomAccessFile.readInt())) {
                    randomAccessFile.close();
                    boolean unused = b.h();
                    return null;
                } else if (i == 0 || i == readInt + 1) {
                    randomAccessFile.close();
                    return null;
                } else {
                    long j = 12 + 0 + ((long) ((i - 1) * 1024));
                    randomAccessFile.seek(j);
                    int readInt2 = randomAccessFile.readInt();
                    byte[] bArr = new byte[readInt2];
                    randomAccessFile.seek(j + 4);
                    for (int i2 = 0; i2 < readInt2; i2++) {
                        bArr[i2] = randomAccessFile.readByte();
                    }
                    randomAccessFile.close();
                    return new String(bArr);
                }
            } catch (IOException e) {
                return null;
            }
        }

        /* access modifiers changed from: private */
        public void a() {
            if (b.y != null && b.y.length() >= b.v) {
                a(b.y.toString());
            }
            b.f();
        }

        /* access modifiers changed from: private */
        public boolean a(Location location) {
            return a(location, j.V, j.aa);
        }

        private boolean a(Location location, int i, int i2) {
            if (location == null || !j.f208void || !this.a || !j.P) {
                return false;
            }
            if (j.V < 5) {
                j.V = 5;
            } else if (j.V > 1000) {
                j.V = 1000;
            }
            if (j.aa < 5) {
                j.aa = 5;
            } else if (j.aa > b.f68void) {
                j.aa = b.f68void;
            }
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            long time = location.getTime() / 1000;
            if (b.f64char) {
                int unused = b.f63case = 1;
                StringBuffer unused2 = b.y = new StringBuffer("");
                b.y.append(String.format("&nr=%s&traj=%d,%.5f,%.5f|", this.f92if, Long.valueOf(time), Double.valueOf(longitude), Double.valueOf(latitude)));
                int unused3 = b.c = b.y.length();
                long unused4 = b.f62byte = time;
                double unused5 = b.o = longitude;
                double unused6 = b.m = latitude;
                long unused7 = b.J = (long) Math.floor((longitude * 100000.0d) + 0.5d);
                long unused8 = b.L = (long) Math.floor((latitude * 100000.0d) + 0.5d);
                boolean unused9 = b.f64char = false;
                return true;
            }
            float[] fArr = new float[1];
            Location.distanceBetween(latitude, longitude, b.m, b.o, fArr);
            long g = time - b.f62byte;
            if (fArr[0] < ((float) j.V) && g < ((long) j.aa)) {
                return false;
            }
            if (b.y == null) {
                b.i();
                int unused10 = b.c = 0;
                StringBuffer unused11 = b.y = new StringBuffer("");
                b.y.append(String.format("&nr=%s&traj=%d,%.5f,%.5f|", this.f92if, Long.valueOf(time), Double.valueOf(longitude), Double.valueOf(latitude)));
                int unused12 = b.c = b.y.length();
                long unused13 = b.f62byte = time;
                double unused14 = b.o = longitude;
                double unused15 = b.m = latitude;
                long unused16 = b.J = (long) Math.floor((longitude * 100000.0d) + 0.5d);
                long unused17 = b.L = (long) Math.floor((latitude * 100000.0d) + 0.5d);
            } else {
                double unused18 = b.o = longitude;
                double unused19 = b.m = latitude;
                long floor = (long) Math.floor((longitude * 100000.0d) + 0.5d);
                long floor2 = (long) Math.floor((latitude * 100000.0d) + 0.5d);
                int unused20 = b.j = (int) (time - b.f62byte);
                int unused21 = b.I = (int) (floor - b.J);
                int unused22 = b.b = (int) (floor2 - b.L);
                b.y.append(String.format("%d,%d,%d|", Integer.valueOf(b.j), Integer.valueOf(b.I), Integer.valueOf(b.b)));
                int unused23 = b.c = b.y.length();
                long unused24 = b.f62byte = time;
                long unused25 = b.J = floor;
                long unused26 = b.L = floor2;
            }
            if (b.c + 15 > b.B) {
                a(b.y.toString());
                StringBuffer unused27 = b.y = (StringBuffer) null;
            }
            if (b.f63case >= j.L) {
                this.a = false;
            }
            return true;
        }

        private boolean a(String str) {
            if (str == null || !str.startsWith("&nr")) {
                return false;
            }
            if (!b.A.exists() && !b.h()) {
                return false;
            }
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(b.A, "rw");
                randomAccessFile.seek(0);
                int readInt = randomAccessFile.readInt();
                int readInt2 = randomAccessFile.readInt();
                int readInt3 = randomAccessFile.readInt();
                if (!b.m75if(readInt, readInt2, readInt3)) {
                    randomAccessFile.close();
                    boolean unused = b.h();
                    return false;
                }
                if (j.f207try) {
                    if (readInt == j.L) {
                        if (str.equals(a(readInt3 == 1 ? j.L : readInt3 - 1))) {
                            randomAccessFile.close();
                            return false;
                        }
                    } else if (readInt3 > 1 && str.equals(a(readInt3 - 1))) {
                        randomAccessFile.close();
                        return false;
                    }
                }
                randomAccessFile.seek(((long) (((readInt3 - 1) * 1024) + 12)) + 0);
                if (str.length() > b.B) {
                    randomAccessFile.close();
                    return false;
                }
                String str2 = Jni.m0if(str);
                int length = str2.length();
                if (length > 1020) {
                    randomAccessFile.close();
                    return false;
                }
                randomAccessFile.writeInt(length);
                randomAccessFile.writeBytes(str2);
                if (readInt == 0) {
                    randomAccessFile.seek(0);
                    randomAccessFile.writeInt(1);
                    randomAccessFile.writeInt(1);
                    randomAccessFile.writeInt(2);
                } else if (readInt < j.L - 1) {
                    randomAccessFile.seek(0);
                    randomAccessFile.writeInt(readInt + 1);
                    randomAccessFile.seek(8);
                    randomAccessFile.writeInt(readInt + 2);
                } else if (readInt == j.L - 1) {
                    randomAccessFile.seek(0);
                    randomAccessFile.writeInt(j.L);
                    if (readInt2 == 0 || readInt2 == 1) {
                        randomAccessFile.writeInt(2);
                    }
                    randomAccessFile.seek(8);
                    randomAccessFile.writeInt(1);
                } else if (readInt3 == readInt2) {
                    int i = readInt3 == j.L ? 1 : readInt3 + 1;
                    int i2 = i == j.L ? 1 : i + 1;
                    randomAccessFile.seek(4);
                    randomAccessFile.writeInt(i2);
                    randomAccessFile.writeInt(i);
                } else {
                    int i3 = readInt3 == j.L ? 1 : readInt3 + 1;
                    if (i3 == readInt2) {
                        int i4 = i3 == j.L ? 1 : i3 + 1;
                        randomAccessFile.seek(4);
                        randomAccessFile.writeInt(i4);
                    }
                    randomAccessFile.seek(8);
                    randomAccessFile.writeInt(i3);
                }
                randomAccessFile.close();
                return true;
            } catch (IOException e) {
                return false;
            }
        }

        /* renamed from: if  reason: not valid java name */
        private boolean m109if() {
            if (b.A.exists()) {
                b.A.delete();
            }
            b.f();
            return !b.A.exists();
        }
    }

    public b(Context context, Handler handler) {
        this.f = context;
        this.H = handler;
        try {
            if (j.f199do != null) {
                this.f70for = new d(j.f199do);
            } else {
                this.f70for = new d("NULL");
            }
        } catch (Exception e2) {
            this.f70for = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.g.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.baidu.location.g.a(android.content.Context, android.net.NetworkInfo):int
      com.baidu.location.g.a(java.lang.String, android.os.Handler):boolean
      com.baidu.location.g.a(java.lang.String, boolean):void */
    private void a(double d2, double d3, float f2) {
        int i2;
        j.m250if("baidu_location_service", "check...gps ...");
        if (j.z) {
            if (d2 < 73.146973d || d2 > 135.252686d || d3 > 54.258807d || d3 < 14.604847d || f2 > 18.0f) {
                i2 = 0;
            } else {
                j.m250if("baidu_location_service", "check...gps2 ...");
                int i3 = (int) ((d2 - j.f203if) * 1000.0d);
                int i4 = (int) ((j.o - d3) * 1000.0d);
                j.m250if("baidu_location_service", "check...gps ..." + i3 + i4);
                if (i3 <= 0 || i3 >= 50 || i4 <= 0 || i4 >= 50) {
                    j.J = d2;
                    j.Z = d3;
                    g.a(String.format("&ll=%.5f|%.5f", Double.valueOf(d2), Double.valueOf(d3)) + "&im=" + j.f199do, true);
                } else {
                    j.m250if("baidu_location_service", "check...gps ..." + i3 + i4);
                    int i5 = i3 + (i4 * 50);
                    int i6 = i5 >> 2;
                    int i7 = i5 & 3;
                    if (j.ag) {
                        i2 = (j.j[i6] >> (i7 * 2)) & 3;
                        j.m250if("baidu_location_service", "check gps scacity..." + i2);
                    }
                }
                i2 = 0;
            }
            if (j.I != i2) {
                j.I = i2;
                try {
                    if (j.I == 3) {
                        this.f69do.removeUpdates(this.x);
                        this.f69do.requestLocationUpdates("gps", 1000, 1.0f, this.x);
                        return;
                    }
                    this.f69do.removeUpdates(this.x);
                    this.f69do.requestLocationUpdates("gps", 1000, 5.0f, this.x);
                } catch (Exception e2) {
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Location location) {
        j.m250if("baidu_location_service", "set new gpsLocation ...");
        this.F = location;
        if (this.F == null) {
            this.D = null;
        } else {
            long currentTimeMillis = System.currentTimeMillis();
            this.F.setTime(currentTimeMillis);
            float speed = (float) (((double) this.F.getSpeed()) * 3.6d);
            this.D = String.format("&ll=%.5f|%.5f&s=%.1f&d=%.1f&ll_n=%d&ll_t=%d", Double.valueOf(this.F.getLongitude()), Double.valueOf(this.F.getLatitude()), Float.valueOf(speed), Float.valueOf(this.F.getBearing()), Integer.valueOf(k), Long.valueOf(currentTimeMillis));
            a(this.F.getLongitude(), this.F.getLatitude(), speed);
        }
        if (this.f70for != null) {
            try {
                boolean unused = this.f70for.a(this.F);
            } catch (Exception e2) {
            }
        }
        this.H.obtainMessage(51).sendToTarget();
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        this.C = z2;
        if ((!z2 || m88new()) && j.ab != z2) {
            j.ab = z2;
            if (j.H) {
                this.H.obtainMessage(53).sendToTarget();
            }
        }
    }

    public static boolean a(Location location, Location location2, boolean z2) {
        if (location == location2) {
            return false;
        }
        if (location == null || location2 == null) {
            return true;
        }
        float speed = location2.getSpeed();
        if (z2 && j.I == 3 && speed < 5.0f) {
            return true;
        }
        float distanceTo = location2.distanceTo(location);
        return speed > j.C ? distanceTo > j.Q : speed > j.D ? distanceTo > j.ai : distanceTo > 5.0f;
    }

    /* renamed from: do  reason: not valid java name */
    public static String m58do(Location location) {
        String str = m64for(location);
        return str != null ? str + "&g_tp=0" : str;
    }

    /* access modifiers changed from: private */
    public static void f() {
        f64char = true;
        y = null;
        f63case = 0;
        c = 0;
        f62byte = 0;
        J = 0;
        L = 0;
        o = 0.0d;
        m = 0.0d;
        j = 0;
        I = 0;
        b = 0;
    }

    /* renamed from: for  reason: not valid java name */
    public static String m64for(Location location) {
        if (location == null) {
            return null;
        }
        return String.format("&ll=%.5f|%.5f&s=%.1f&d=%.1f&ll_r=%d&ll_n=%d&ll_h=%.2f&ll_t=%d", Double.valueOf(location.getLongitude()), Double.valueOf(location.getLatitude()), Float.valueOf((float) (((double) location.getSpeed()) * 3.6d)), Float.valueOf(location.getBearing()), Integer.valueOf((int) (location.hasAccuracy() ? location.getAccuracy() : -1.0f)), Integer.valueOf(k), Double.valueOf(location.hasAltitude() ? location.getAltitude() : 555.0d), Long.valueOf(location.getTime() / 1000));
    }

    /* access modifiers changed from: private */
    public static boolean h() {
        if (A.exists()) {
            A.delete();
        }
        if (!A.getParentFile().exists()) {
            A.getParentFile().mkdirs();
        }
        try {
            A.createNewFile();
            RandomAccessFile randomAccessFile = new RandomAccessFile(A, "rw");
            randomAccessFile.seek(0);
            randomAccessFile.writeInt(0);
            randomAccessFile.writeInt(0);
            randomAccessFile.writeInt(1);
            randomAccessFile.close();
            f();
            return A.exists();
        } catch (IOException e2) {
            return false;
        }
    }

    static /* synthetic */ int i() {
        int i2 = f63case + 1;
        f63case = i2;
        return i2;
    }

    /* renamed from: if  reason: not valid java name */
    public static String m71if(Location location) {
        String str = m64for(location);
        return str != null ? str + r : str;
    }

    /* access modifiers changed from: private */
    /* renamed from: if  reason: not valid java name */
    public static boolean m75if(int i2, int i3, int i4) {
        if (i2 < 0 || i2 > j.L) {
            return false;
        }
        if (i3 < 0 || i3 > i2 + 1) {
            return false;
        }
        return i4 >= 1 && i4 <= i2 + 1 && i4 <= j.L;
    }

    public static String j() {
        j.m250if("baidu_location_service", "GPS readline...");
        if (A == null) {
            return null;
        }
        if (!A.exists()) {
            return null;
        }
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(A, "rw");
            randomAccessFile.seek(0);
            int readInt = randomAccessFile.readInt();
            int readInt2 = randomAccessFile.readInt();
            int readInt3 = randomAccessFile.readInt();
            if (!m75if(readInt, readInt2, readInt3)) {
                randomAccessFile.close();
                h();
                return null;
            }
            j.m250if("baidu_location_service", "GPS readline1...");
            if (readInt2 == 0 || readInt2 == readInt3) {
                randomAccessFile.close();
                return null;
            }
            j.m250if("baidu_location_service", "GPS readline2...");
            long j2 = 0 + ((long) (((readInt2 - 1) * 1024) + 12));
            randomAccessFile.seek(j2);
            int readInt4 = randomAccessFile.readInt();
            byte[] bArr = new byte[readInt4];
            randomAccessFile.seek(j2 + 4);
            for (int i2 = 0; i2 < readInt4; i2++) {
                bArr[i2] = randomAccessFile.readByte();
            }
            String str = new String(bArr);
            int i3 = readInt < j.L ? readInt2 + 1 : readInt2 == j.L ? 1 : readInt2 + 1;
            randomAccessFile.seek(4);
            randomAccessFile.writeInt(i3);
            randomAccessFile.close();
            return str;
        } catch (IOException e2) {
            return null;
        }
    }

    /* renamed from: case  reason: not valid java name */
    public boolean m84case() {
        return (this.F == null || this.F.getLatitude() == 0.0d || this.F.getLongitude() == 0.0d) ? false : true;
    }

    /* renamed from: char  reason: not valid java name */
    public String m85char() {
        return this.D;
    }

    /* renamed from: goto  reason: not valid java name */
    public void m86goto() {
    }

    /* renamed from: int  reason: not valid java name */
    public String m87int() {
        if (this.F != null) {
            String str = "{\"result\":{\"time\":\"" + j.m245for() + "\",\"error\":\"61\"},\"content\":{\"point\":{\"x\":" + "\"%f\",\"y\":\"%f\"},\"radius\":\"%d\",\"d\":\"%f\"," + "\"s\":\"%f\",\"n\":\"%d\"}}";
            int accuracy = (int) (this.F.hasAccuracy() ? this.F.getAccuracy() : 10.0f);
            float speed = (float) (((double) this.F.getSpeed()) * 3.6d);
            double[] dArr = Jni.m1if(this.F.getLongitude(), this.F.getLatitude(), "gps2gcj");
            if (dArr[0] <= 0.0d && dArr[1] <= 0.0d) {
                dArr[0] = this.F.getLongitude();
                dArr[1] = this.F.getLatitude();
            }
            String format = String.format(str, Double.valueOf(dArr[0]), Double.valueOf(dArr[1]), Integer.valueOf(accuracy), Float.valueOf(this.F.getBearing()), Float.valueOf(speed), Integer.valueOf(k));
            j.m250if("baidu_location_service", "wgs84: " + this.F.getLongitude() + " " + this.F.getLatitude() + " gcj02: " + dArr[0] + " " + dArr[1]);
            return format;
        }
        j.m250if("baidu_location_service", "gps man getGpsJson but gpslocation is null");
        return null;
    }

    public void k() {
        if (!this.t) {
            try {
                this.f69do = (LocationManager) this.f.getSystemService("location");
                this.x = new C0001b();
                this.f73new = new a();
                this.f69do.requestLocationUpdates("gps", 1000, 5.0f, this.x);
                this.f69do.addGpsStatusListener(this.f73new);
                this.f69do.addNmeaListener(this.f73new);
                this.t = true;
            } catch (Exception e2) {
            }
        }
    }

    public void l() {
        if (this.t) {
            if (this.f69do != null) {
                try {
                    if (this.x != null) {
                        this.f69do.removeUpdates(this.x);
                    }
                    if (this.f73new != null) {
                        this.f69do.removeGpsStatusListener(this.f73new);
                        this.f69do.removeNmeaListener(this.f73new);
                    }
                    if (this.f70for != null) {
                        this.f70for.a();
                    }
                } catch (Exception e2) {
                }
            }
            this.x = null;
            this.f73new = null;
            this.f69do = null;
            this.t = false;
            a(false);
        }
    }

    /* renamed from: new  reason: not valid java name */
    public boolean m88new() {
        if (!m84case()) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (this.f71int && currentTimeMillis - this.f72long > 3000) {
            return false;
        }
        if (k >= 3) {
            return true;
        }
        return currentTimeMillis - this.d < 3000;
    }

    /* renamed from: try  reason: not valid java name */
    public Location m89try() {
        return this.F;
    }
}
