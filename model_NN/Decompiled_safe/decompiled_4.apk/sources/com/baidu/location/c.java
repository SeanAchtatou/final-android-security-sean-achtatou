package com.baidu.location;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.baidu.location.j;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

class c {

    /* renamed from: byte  reason: not valid java name */
    private static String f93byte = null;
    private static int c = 3;

    /* renamed from: case  reason: not valid java name */
    private static Method f94case = null;

    /* renamed from: char  reason: not valid java name */
    private static boolean f95char = false;
    private static Class d = null;

    /* renamed from: for  reason: not valid java name */
    private static Method f96for = null;

    /* renamed from: goto  reason: not valid java name */
    private static String f97goto = null;

    /* renamed from: long  reason: not valid java name */
    private static Method f98long = null;
    /* access modifiers changed from: private */

    /* renamed from: void  reason: not valid java name */
    public static long f99void = 3000;
    /* access modifiers changed from: private */
    public a a = new a();
    private boolean b = false;
    /* access modifiers changed from: private */

    /* renamed from: do  reason: not valid java name */
    public Handler f100do = null;

    /* renamed from: else  reason: not valid java name */
    private final String f101else = f.v;

    /* renamed from: if  reason: not valid java name */
    private Context f102if = null;

    /* renamed from: int  reason: not valid java name */
    private b f103int = null;
    /* access modifiers changed from: private */

    /* renamed from: new  reason: not valid java name */
    public List f104new = null;
    /* access modifiers changed from: private */

    /* renamed from: try  reason: not valid java name */
    public TelephonyManager f105try = null;

    public class a {

        /* renamed from: byte  reason: not valid java name */
        public long f106byte;

        /* renamed from: do  reason: not valid java name */
        public int f107do;

        /* renamed from: for  reason: not valid java name */
        public int f108for;

        /* renamed from: if  reason: not valid java name */
        public int f109if;

        /* renamed from: int  reason: not valid java name */
        public int f110int;

        /* renamed from: new  reason: not valid java name */
        public char f111new;

        /* renamed from: try  reason: not valid java name */
        public int f112try;

        public a() {
            this.f108for = -1;
            this.f112try = -1;
            this.f107do = -1;
            this.f109if = -1;
            this.f106byte = 0;
            this.f110int = -1;
            this.f111new = 0;
            this.f106byte = System.currentTimeMillis();
        }

        public a(int i, int i2, int i3, int i4, char c) {
            this.f108for = -1;
            this.f112try = -1;
            this.f107do = -1;
            this.f109if = -1;
            this.f106byte = 0;
            this.f110int = -1;
            this.f111new = 0;
            this.f108for = i;
            this.f112try = i2;
            this.f107do = i3;
            this.f109if = i4;
            this.f111new = c;
            this.f106byte = System.currentTimeMillis() / 1000;
        }

        public String a() {
            StringBuffer stringBuffer = new StringBuffer((int) AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER);
            stringBuffer.append(this.f112try + 23);
            stringBuffer.append("H");
            stringBuffer.append(this.f108for + 45);
            stringBuffer.append("K");
            stringBuffer.append(this.f109if + 54);
            stringBuffer.append("Q");
            stringBuffer.append(this.f107do + 203);
            return stringBuffer.toString();
        }

        public boolean a(a aVar) {
            return this.f108for == aVar.f108for && this.f112try == aVar.f112try && this.f109if == aVar.f109if;
        }

        /* renamed from: do  reason: not valid java name */
        public boolean m120do() {
            return System.currentTimeMillis() - this.f106byte < c.f99void;
        }

        /* renamed from: for  reason: not valid java name */
        public boolean m121for() {
            return this.f108for > -1 && this.f112try > 0;
        }

        /* renamed from: if  reason: not valid java name */
        public String m122if() {
            StringBuffer stringBuffer = new StringBuffer(64);
            stringBuffer.append(String.format("cell=%d|%d|%d|%d:%d", Integer.valueOf(this.f107do), Integer.valueOf(this.f109if), Integer.valueOf(this.f108for), Integer.valueOf(this.f112try), Integer.valueOf(this.f110int)));
            return stringBuffer.toString();
        }

        /* renamed from: int  reason: not valid java name */
        public String m123int() {
            String str;
            String str2;
            try {
                List<NeighboringCellInfo> neighboringCellInfo = c.this.f105try.getNeighboringCellInfo();
                if (neighboringCellInfo == null || neighboringCellInfo.isEmpty()) {
                    str = null;
                    j.m250if(f.v, "Neighbour:" + str);
                    return str;
                }
                String str3 = "&nc=";
                int i = 0;
                for (NeighboringCellInfo neighboringCellInfo2 : neighboringCellInfo) {
                    if (i != 0) {
                        if (i >= 8) {
                            break;
                        }
                        str2 = neighboringCellInfo2.getLac() != this.f108for ? str3 + ";" + neighboringCellInfo2.getLac() + "|" + neighboringCellInfo2.getCid() + "|" + neighboringCellInfo2.getRssi() : str3 + ";" + "|" + neighboringCellInfo2.getCid() + "|" + neighboringCellInfo2.getRssi();
                    } else {
                        str2 = neighboringCellInfo2.getLac() != this.f108for ? str3 + neighboringCellInfo2.getLac() + "|" + neighboringCellInfo2.getCid() + "|" + neighboringCellInfo2.getRssi() : str3 + "|" + neighboringCellInfo2.getCid() + "|" + neighboringCellInfo2.getRssi();
                    }
                    i++;
                    str3 = str2;
                }
                str = str3;
                j.m250if(f.v, "Neighbour:" + str);
                return str;
            } catch (Exception e) {
                str = null;
            }
        }

        public String toString() {
            StringBuffer stringBuffer = new StringBuffer((int) AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER);
            stringBuffer.append("&nw=");
            stringBuffer.append(c.this.a.f111new);
            stringBuffer.append(String.format("&cl=%d|%d|%d|%d&cl_s=%d", Integer.valueOf(this.f107do), Integer.valueOf(this.f109if), Integer.valueOf(this.f108for), Integer.valueOf(this.f112try), Integer.valueOf(this.f110int)));
            stringBuffer.append("&cl_t=");
            stringBuffer.append(this.f106byte);
            if (c.this.f104new != null && c.this.f104new.size() > 0) {
                int size = c.this.f104new.size();
                stringBuffer.append("&clt=");
                for (int i = 0; i < size; i++) {
                    a aVar = (a) c.this.f104new.get(i);
                    if (aVar.f107do != this.f107do) {
                        stringBuffer.append(aVar.f107do);
                    }
                    stringBuffer.append("|");
                    if (aVar.f109if != this.f109if) {
                        stringBuffer.append(aVar.f109if);
                    }
                    stringBuffer.append("|");
                    if (aVar.f108for != this.f108for) {
                        stringBuffer.append(aVar.f108for);
                    }
                    stringBuffer.append("|");
                    if (aVar.f112try != this.f112try) {
                        stringBuffer.append(aVar.f112try);
                    }
                    stringBuffer.append("|");
                    if (i != size - 1) {
                        stringBuffer.append(aVar.f106byte / 1000);
                    } else {
                        stringBuffer.append((System.currentTimeMillis() - aVar.f106byte) / 1000);
                    }
                    stringBuffer.append(";");
                }
            }
            return stringBuffer.toString();
        }
    }

    private class b extends PhoneStateListener {
        public b() {
        }

        public void onCellLocationChanged(CellLocation cellLocation) {
            if (cellLocation != null) {
                try {
                    c.this.a(c.this.f105try.getCellLocation());
                } catch (Exception e) {
                }
            }
        }

        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            if (c.this.a != null) {
                if (c.this.a.f111new == 'g') {
                    c.this.a.f110int = signalStrength.getGsmSignalStrength();
                } else if (c.this.a.f111new == 'c') {
                    c.this.a.f110int = signalStrength.getCdmaDbm();
                }
                j.m250if("cell strength", "===== cell singal strength changed : " + c.this.a.f110int);
                if (c.this.f100do != null) {
                    c.this.f100do.obtainMessage(31).sendToTarget();
                }
            }
        }
    }

    public c(Context context, Handler handler) {
        this.f102if = context;
        this.f100do = handler;
    }

    public static String a(boolean z) {
        StringBuffer stringBuffer = new StringBuffer((int) AccessibilityEventCompat.TYPE_VIEW_HOVER_EXIT);
        stringBuffer.append("&sdk=");
        stringBuffer.append(3.3f);
        if (z && j.A.equals("all")) {
            stringBuffer.append("&addr=all");
        }
        if (z) {
            stringBuffer.append("&coor=gcj02");
        }
        if (f97goto == null) {
            stringBuffer.append("&im=");
            stringBuffer.append(f93byte);
        } else {
            stringBuffer.append("&cu=");
            stringBuffer.append(f97goto);
        }
        stringBuffer.append("&mb=");
        stringBuffer.append(Build.MODEL);
        stringBuffer.append("&resid=");
        stringBuffer.append("12");
        stringBuffer.append("&os=A");
        stringBuffer.append(Build.VERSION.SDK);
        if (z) {
            stringBuffer.append("&sv=");
            String str = Build.VERSION.RELEASE;
            if (str != null && str.length() > 5) {
                str = str.substring(0, 5);
            }
            stringBuffer.append(str);
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: private */
    public void a(CellLocation cellLocation) {
        int i;
        if (cellLocation != null && this.f105try != null) {
            if (!f95char) {
                f93byte = this.f105try.getDeviceId();
                f95char = m113if();
            }
            j.m250if(f.v, "set cell info..");
            a aVar = new a();
            aVar.f106byte = System.currentTimeMillis();
            String networkOperator = this.f105try.getNetworkOperator();
            if (networkOperator != null && networkOperator.length() > 0) {
                try {
                    if (networkOperator.length() >= 3) {
                        int intValue = Integer.valueOf(networkOperator.substring(0, 3)).intValue();
                        if (intValue < 0) {
                            intValue = this.a.f107do;
                        }
                        aVar.f107do = intValue;
                    }
                    String substring = networkOperator.substring(3);
                    if (substring != null) {
                        char[] charArray = substring.toCharArray();
                        int i2 = 0;
                        while (true) {
                            if (i2 >= charArray.length) {
                                i = i2;
                                break;
                            } else if (!Character.isDigit(charArray[i2])) {
                                i = i2;
                                break;
                            } else {
                                i2++;
                            }
                        }
                    } else {
                        i = 0;
                    }
                    int intValue2 = Integer.valueOf(substring.substring(0, i)).intValue();
                    if (intValue2 < 0) {
                        intValue2 = this.a.f109if;
                    }
                    aVar.f109if = intValue2;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (cellLocation instanceof GsmCellLocation) {
                aVar.f108for = ((GsmCellLocation) cellLocation).getLac();
                aVar.f112try = ((GsmCellLocation) cellLocation).getCid();
                aVar.f111new = 'g';
            } else if (cellLocation instanceof CdmaCellLocation) {
                aVar.f111new = 'c';
                if (Integer.parseInt(Build.VERSION.SDK) >= 5) {
                    if (d == null) {
                        try {
                            d = Class.forName("android.telephony.cdma.CdmaCellLocation");
                            f98long = d.getMethod("getBaseStationId", new Class[0]);
                            f94case = d.getMethod("getNetworkId", new Class[0]);
                            f96for = d.getMethod("getSystemId", new Class[0]);
                        } catch (Exception e2) {
                            d = null;
                            e2.printStackTrace();
                            return;
                        }
                    }
                    if (d != null && d.isInstance(cellLocation)) {
                        try {
                            int intValue3 = ((Integer) f96for.invoke(cellLocation, new Object[0])).intValue();
                            if (intValue3 < 0) {
                                intValue3 = this.a.f109if;
                            }
                            aVar.f109if = intValue3;
                            aVar.f112try = ((Integer) f98long.invoke(cellLocation, new Object[0])).intValue();
                            aVar.f108for = ((Integer) f94case.invoke(cellLocation, new Object[0])).intValue();
                        } catch (Exception e3) {
                            e3.printStackTrace();
                            return;
                        }
                    }
                } else {
                    return;
                }
            }
            if (!aVar.m121for()) {
                return;
            }
            if (this.a == null || !this.a.a(aVar)) {
                this.a = aVar;
                this.f100do.obtainMessage(31).sendToTarget();
                if (aVar.m121for()) {
                    if (this.f104new == null) {
                        this.f104new = new LinkedList();
                    }
                    int size = this.f104new.size();
                    a aVar2 = size == 0 ? null : (a) this.f104new.get(size - 1);
                    if (aVar2 == null || aVar2.f112try != this.a.f112try || aVar2.f108for != this.a.f108for) {
                        if (aVar2 != null) {
                            aVar2.f106byte = this.a.f106byte - aVar2.f106byte;
                        }
                        this.f104new.add(this.a);
                        if (this.f104new.size() > c) {
                            this.f104new.remove(0);
                        }
                    }
                } else if (this.f104new != null) {
                    this.f104new.clear();
                }
            }
        }
    }

    /* renamed from: if  reason: not valid java name */
    private boolean m113if() {
        if (f93byte == null || f93byte.length() < 10) {
            return false;
        }
        try {
            char[] charArray = f93byte.toCharArray();
            for (int i = 0; i < 10; i++) {
                if (charArray[i] > '9' || charArray[i] < '0') {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public a a() {
        if ((this.a == null || !this.a.m120do() || !this.a.m121for()) && this.f105try != null) {
            try {
                a(this.f105try.getCellLocation());
            } catch (Exception e) {
            }
        }
        return this.a;
    }

    /* renamed from: byte  reason: not valid java name */
    public void m115byte() {
        if (this.b) {
            if (!(this.f103int == null || this.f105try == null)) {
                this.f105try.listen(this.f103int, 0);
            }
            this.f103int = null;
            this.f105try = null;
            this.f104new.clear();
            this.f104new = null;
            j.m250if(f.v, "cell manager stop ...");
            this.b = false;
        }
    }

    /* renamed from: do  reason: not valid java name */
    public void m116do() {
        if (!this.b) {
            this.f105try = (TelephonyManager) this.f102if.getSystemService("phone");
            this.f104new = new LinkedList();
            this.f103int = new b();
            if (this.f105try != null && this.f103int != null) {
                try {
                    this.f105try.listen(this.f103int, 272);
                    f93byte = this.f105try.getDeviceId();
                    j.f199do = "v3.3" + f93byte + "|" + Build.MODEL;
                } catch (Exception e) {
                }
                try {
                    f97goto = j.a.m254if(this.f102if);
                    j.m250if(f.v, "CUID:" + f97goto);
                } catch (Exception e2) {
                    f97goto = null;
                }
                try {
                    if (f97goto != null) {
                        j.f199do = "v3.3|" + f97goto + "|" + Build.MODEL;
                    }
                    j.m250if(f.v, "CUID:" + j.f199do);
                } catch (Exception e3) {
                }
                f95char = m113if();
                j.a(f.v, "i:" + f93byte);
                j.m250if(f.v, "cell manager start...");
                this.b = true;
            }
        }
    }

    /* renamed from: for  reason: not valid java name */
    public String m117for() {
        if (this.f105try == null) {
            this.f105try = (TelephonyManager) this.f102if.getSystemService("phone");
        }
        try {
            a(this.f105try.getCellLocation());
        } catch (Exception e) {
        }
        return this.a.toString();
    }

    /* renamed from: int  reason: not valid java name */
    public String m118int() {
        return f93byte;
    }

    /* renamed from: new  reason: not valid java name */
    public int m119new() {
        return this.f105try.getNetworkType();
    }
}
