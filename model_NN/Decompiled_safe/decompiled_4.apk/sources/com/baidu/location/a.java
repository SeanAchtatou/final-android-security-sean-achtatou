package com.baidu.location;

import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.mobcent.forum.android.constant.LocationConstant;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class a {
    /* access modifiers changed from: private */

    /* renamed from: if  reason: not valid java name */
    public static String f54if = f.v;
    private boolean a = false;

    /* renamed from: do  reason: not valid java name */
    private ArrayList f55do = null;

    /* renamed from: for  reason: not valid java name */
    private boolean f56for = false;

    /* renamed from: int  reason: not valid java name */
    private Handler f57int = null;

    /* renamed from: com.baidu.location.a$a  reason: collision with other inner class name */
    private class C0000a {

        /* renamed from: do  reason: not valid java name */
        public LocationClientOption f58do = new LocationClientOption();

        /* renamed from: for  reason: not valid java name */
        public Messenger f59for = null;

        /* renamed from: if  reason: not valid java name */
        public int f60if = 0;

        /* renamed from: int  reason: not valid java name */
        public String f61int = null;

        public C0000a(Message message) {
            this.f59for = message.replyTo;
            this.f61int = message.getData().getString("packName");
            this.f58do.f51new = message.getData().getString("prodName");
            j.ak = this.f61int;
            j.b = this.f58do.f51new;
            this.f58do.f52try = message.getData().getString("coorType");
            this.f58do.f43char = message.getData().getString("addrType");
            j.A = this.f58do.f43char;
            this.f58do.f42case = message.getData().getBoolean("openGPS");
            this.f58do.f49int = message.getData().getInt("scanSpan");
            this.f58do.f50long = message.getData().getInt("timeOut");
            this.f58do.f47goto = message.getData().getInt("priority");
            this.f58do.f53void = message.getData().getBoolean("location_change_notify");
        }

        /* access modifiers changed from: private */
        public void a(int i) {
            Message obtain = Message.obtain((Handler) null, i);
            try {
                if (this.f59for != null) {
                    this.f59for.send(obtain);
                }
                this.f60if = 0;
            } catch (Exception e) {
                if (e instanceof DeadObjectException) {
                    this.f60if++;
                }
            }
        }

        private void a(int i, String str, String str2) {
            Bundle bundle = new Bundle();
            bundle.putString(str, str2);
            Message obtain = Message.obtain((Handler) null, i);
            obtain.setData(bundle);
            try {
                if (this.f59for != null) {
                    this.f59for.send(obtain);
                }
                this.f60if = 0;
            } catch (Exception e) {
                if (e instanceof DeadObjectException) {
                    this.f60if++;
                }
            }
        }

        public void a() {
            a(23);
        }

        public void a(String str) {
            if (this.f58do.f53void) {
                m49if(str);
            }
        }

        public void a(String str, int i) {
            String str2;
            j.m250if(a.f54if, "decode...");
            if (str != null) {
                if (i == 21) {
                    a(27, "locStr", str);
                }
                if (this.f58do.f52try == null || this.f58do.f52try.equals("gcj02")) {
                    str2 = str;
                } else {
                    double d = j.m242do(str, "x\":\"", "\"");
                    double d2 = j.m242do(str, "y\":\"", "\"");
                    j.m250if(a.f54if, "decode..." + d + ":" + d2);
                    if (d == Double.MIN_VALUE || d2 == Double.MIN_VALUE) {
                        str2 = str;
                    } else {
                        double[] dArr = Jni.m1if(d, d2, this.f58do.f52try);
                        String a2 = j.a(j.a(str, "x\":\"", "\"", dArr[0]), "y\":\"", "\"", dArr[1]);
                        j.m250if(a.f54if, "decode2 ..." + dArr[0] + ":" + dArr[1]);
                        j.m250if(a.f54if, "decode3 ..." + a2);
                        str2 = a2;
                    }
                    if (this.f58do.f45else) {
                        try {
                            JSONObject jSONObject = new JSONObject(str2);
                            JSONObject jSONObject2 = jSONObject.getJSONObject("result");
                            JSONObject jSONObject3 = jSONObject.getJSONObject("content");
                            if (Integer.parseInt(jSONObject2.getString("error")) == 161) {
                                JSONObject jSONObject4 = jSONObject3.getJSONObject("poi");
                                JSONArray jSONArray = jSONObject4.getJSONArray(LocationConstant.POI);
                                int i2 = 0;
                                while (i2 < jSONArray.length()) {
                                    JSONObject jSONObject5 = jSONArray.getJSONObject(i2);
                                    double parseDouble = Double.parseDouble(jSONObject5.getString("x"));
                                    double parseDouble2 = Double.parseDouble(jSONObject5.getString("y"));
                                    if (!(parseDouble == Double.MIN_VALUE || parseDouble2 == Double.MIN_VALUE)) {
                                        double[] dArr2 = Jni.m1if(parseDouble, parseDouble2, this.f58do.f52try);
                                        jSONObject5.put("x", String.valueOf(dArr2[0]));
                                        jSONObject5.put("y", String.valueOf(dArr2[1]));
                                        jSONArray.put(i2, jSONObject5);
                                        i2++;
                                    }
                                }
                                jSONObject4.put(LocationConstant.POI, jSONArray);
                                jSONObject3.put("poi", jSONObject4);
                                jSONObject.put("content", jSONObject3);
                                str2 = jSONObject.toString();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                a(i, "locStr", str2);
            }
        }

        /* renamed from: if  reason: not valid java name */
        public void m48if() {
            if (!this.f58do.f53void) {
                return;
            }
            if (j.ab) {
                a(54);
            } else {
                a(55);
            }
        }

        /* renamed from: if  reason: not valid java name */
        public void m49if(String str) {
            String str2;
            if (str != null) {
                a(27, "locStr", str);
                if (this.f58do.f52try != null && !this.f58do.f52try.equals("gcj02")) {
                    double d = j.m242do(str, "x\":\"", "\"");
                    double d2 = j.m242do(str, "y\":\"", "\"");
                    if (!(d == Double.MIN_VALUE || d2 == Double.MIN_VALUE)) {
                        double[] dArr = Jni.m1if(d, d2, this.f58do.f52try);
                        if (dArr[0] > 0.0d || dArr[1] > 0.0d) {
                            str2 = j.a(j.a(str, "x\":\"", "\"", dArr[0]), "y\":\"", "\"", dArr[1]);
                            a(21, "locStr", str2);
                        }
                    }
                }
                str2 = str;
                a(21, "locStr", str2);
            }
        }
    }

    public a(Handler handler) {
        this.f57int = handler;
        this.f55do = new ArrayList();
    }

    private C0000a a(Messenger messenger) {
        if (this.f55do == null) {
            return null;
        }
        Iterator it = this.f55do.iterator();
        while (it.hasNext()) {
            C0000a aVar = (C0000a) it.next();
            if (aVar.f59for.equals(messenger)) {
                return aVar;
            }
        }
        return null;
    }

    private void a() {
        boolean z;
        boolean z2 = false;
        Iterator it = this.f55do.iterator();
        while (true) {
            z = z2;
            if (!it.hasNext()) {
                break;
            }
            z2 = ((C0000a) it.next()).f58do.f51new.equals("kuikedefancaiburudashahaochi") ? true : z;
        }
        if (this.a != z) {
            this.a = z;
            this.f57int.obtainMessage(81).sendToTarget();
        }
    }

    private void a(C0000a aVar) {
        if (aVar != null) {
            if (a(aVar.f59for) != null) {
                aVar.a(14);
                return;
            }
            this.f55do.add(aVar);
            j.m250if(f54if, aVar.f61int + " registered ");
            aVar.a(13);
        }
    }

    /* renamed from: do  reason: not valid java name */
    private void m36do() {
        m37int();
        a();
        m47new();
    }

    /* renamed from: int  reason: not valid java name */
    private void m37int() {
        boolean z = false;
        Iterator it = this.f55do.iterator();
        boolean z2 = false;
        while (it.hasNext()) {
            C0000a aVar = (C0000a) it.next();
            if (aVar.f58do.f42case) {
                z2 = true;
            }
            z = aVar.f58do.f53void ? true : z;
        }
        j.H = z;
        if (this.f56for != z2) {
            this.f56for = z2;
            this.f57int.obtainMessage(52).sendToTarget();
        }
    }

    public String a(Message message) {
        if (message == null || message.replyTo == null) {
            j.m250if(f54if, "invalid Poirequest");
            return null;
        }
        C0000a a2 = a(message.replyTo);
        if (a2 == null) {
            return null;
        }
        a2.f58do.a = message.getData().getInt("num", a2.f58do.a);
        a2.f58do.f44do = message.getData().getFloat("distance", a2.f58do.f44do);
        a2.f58do.f48if = message.getData().getBoolean("extraInfo", a2.f58do.f48if);
        a2.f58do.f45else = true;
        String format = String.format("&poi=%.1f|%d", Float.valueOf(a2.f58do.f44do), Integer.valueOf(a2.f58do.a));
        return a2.f58do.f48if ? format + "|1" : format;
    }

    public void a(String str) {
        ArrayList arrayList = new ArrayList();
        Iterator it = this.f55do.iterator();
        while (it.hasNext()) {
            C0000a aVar = (C0000a) it.next();
            aVar.m49if(str);
            if (aVar.f60if > 4) {
                arrayList.add(aVar);
            }
        }
        if (arrayList != null && arrayList.size() > 0) {
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                j.m250if(f54if, "remove dead object...");
                this.f55do.remove((C0000a) it2.next());
            }
        }
    }

    public void a(String str, int i) {
        ArrayList arrayList = new ArrayList();
        Iterator it = this.f55do.iterator();
        while (it.hasNext()) {
            C0000a aVar = (C0000a) it.next();
            aVar.a(str, i);
            if (aVar.f60if > 4) {
                arrayList.add(aVar);
            }
        }
        if (arrayList != null && arrayList.size() > 0) {
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                j.m250if(f54if, "remove dead object...");
                this.f55do.remove((C0000a) it2.next());
            }
        }
    }

    public void a(String str, Message message) {
        if (str != null && message != null) {
            C0000a a2 = a(message.replyTo);
            if (a2 != null) {
                a2.m49if(str);
                if (a2.f60if > 4) {
                    this.f55do.remove(a2);
                    return;
                }
                return;
            }
            j.m250if(f54if, "not found the client messener...");
        }
    }

    /* renamed from: byte  reason: not valid java name */
    public String m39byte() {
        StringBuffer stringBuffer = new StringBuffer((int) AccessibilityEventCompat.TYPE_VIEW_HOVER_EXIT);
        C0000a aVar = (C0000a) this.f55do.get(0);
        if (aVar.f58do.f51new != null) {
            stringBuffer.append(aVar.f58do.f51new);
        }
        if (aVar.f61int != null) {
            stringBuffer.append(":");
            stringBuffer.append(aVar.f61int);
            stringBuffer.append("|");
        }
        String stringBuffer2 = stringBuffer.toString();
        if (stringBuffer2 == null || stringBuffer2.equals("")) {
            return null;
        }
        return "&prod=" + stringBuffer2;
    }

    /* renamed from: do  reason: not valid java name */
    public int m40do(Message message) {
        if (message == null || message.replyTo == null) {
            return 1;
        }
        C0000a a2 = a(message.replyTo);
        if (a2 == null || a2.f58do == null) {
            return 1;
        }
        return a2.f58do.f47goto;
    }

    /* renamed from: for  reason: not valid java name */
    public boolean m41for() {
        return this.f56for;
    }

    /* renamed from: for  reason: not valid java name */
    public boolean m42for(Message message) {
        C0000a a2 = a(message.replyTo);
        if (a2 == null) {
            return false;
        }
        int i = a2.f58do.f49int;
        a2.f58do.f49int = message.getData().getInt("scanSpan", a2.f58do.f49int);
        if (a2.f58do.f49int < 1000) {
            j.R = false;
        } else {
            j.R = true;
        }
        boolean z = a2.f58do.f49int > 999 && i < 1000;
        a2.f58do.f42case = message.getData().getBoolean("openGPS", a2.f58do.f42case);
        String string = message.getData().getString("coorType");
        LocationClientOption locationClientOption = a2.f58do;
        if (string == null || string.equals("")) {
            string = a2.f58do.f52try;
        }
        locationClientOption.f52try = string;
        String string2 = message.getData().getString("addrType");
        LocationClientOption locationClientOption2 = a2.f58do;
        if (string2 == null || string2.equals("")) {
            string2 = a2.f58do.f43char;
        }
        locationClientOption2.f43char = string2;
        j.A = a2.f58do.f43char;
        a2.f58do.f50long = message.getData().getInt("timeOut", a2.f58do.f50long);
        a2.f58do.f53void = message.getData().getBoolean("location_change_notify", a2.f58do.f53void);
        a2.f58do.f47goto = message.getData().getInt("priority", a2.f58do.f47goto);
        m36do();
        return z;
    }

    /* renamed from: if  reason: not valid java name */
    public void m43if() {
        Iterator it = this.f55do.iterator();
        while (it.hasNext()) {
            ((C0000a) it.next()).a();
        }
    }

    /* renamed from: if  reason: not valid java name */
    public void m44if(Message message) {
        C0000a a2 = a(message.replyTo);
        if (a2 != null) {
            j.m250if(f54if, a2.f61int + " unregistered");
            this.f55do.remove(a2);
        }
        m36do();
    }

    /* renamed from: if  reason: not valid java name */
    public void m45if(String str) {
        Iterator it = this.f55do.iterator();
        while (it.hasNext()) {
            ((C0000a) it.next()).a(str);
        }
    }

    /* renamed from: int  reason: not valid java name */
    public void m46int(Message message) {
        if (message == null || message.replyTo == null) {
            j.m250if(f54if, "invalid regist client");
            return;
        }
        a(new C0000a(message));
        m36do();
    }

    /* renamed from: new  reason: not valid java name */
    public void m47new() {
        Iterator it = this.f55do.iterator();
        while (it.hasNext()) {
            ((C0000a) it.next()).m48if();
        }
    }
}
