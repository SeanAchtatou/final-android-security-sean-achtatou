package com.baidu.location;

import android.location.Location;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.baidu.location.c;
import com.baidu.location.e;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

class k {
    private static Location a = null;
    private static int b = 8;

    /* renamed from: byte  reason: not valid java name */
    private static int f213byte = 8;
    private static int c = 0;

    /* renamed from: case  reason: not valid java name */
    private static int f214case = 1024;

    /* renamed from: char  reason: not valid java name */
    private static int f215char = 64;
    private static final int d = 1040;

    /* renamed from: do  reason: not valid java name */
    private static String f216do = f.v;
    private static Location e = null;

    /* renamed from: else  reason: not valid java name */
    private static File f217else = null;
    private static final int f = 32;

    /* renamed from: for  reason: not valid java name */
    private static final int f218for = 2048;
    private static e.c g = null;

    /* renamed from: goto  reason: not valid java name */
    private static final int f219goto = 128;
    private static ArrayList h = new ArrayList();
    private static int i = 5;

    /* renamed from: if  reason: not valid java name */
    private static final int f220if = 2048;

    /* renamed from: int  reason: not valid java name */
    private static double f221int = 100.0d;
    private static ArrayList j = new ArrayList();
    private static final String k = (f.ac + "/yol.dat");
    private static int l = AccessibilityEventCompat.TYPE_VIEW_HOVER_EXIT;

    /* renamed from: long  reason: not valid java name */
    private static ArrayList f222long = new ArrayList();
    private static ArrayList m = new ArrayList();
    private static double n = 0.1d;

    /* renamed from: new  reason: not valid java name */
    private static double f223new = 0.0d;
    private static int o = 1024;
    private static final String p = (f.ac + "/yor.dat");
    private static Location q = null;
    private static ArrayList r = new ArrayList();
    private static final int s = 2048;
    private static final String t = (f.ac + "/yom.dat");

    /* renamed from: try  reason: not valid java name */
    private static int f224try = 16;
    private static final String u = (f.ac + "/yoh.dat");
    private static int v = 128;

    /* renamed from: void  reason: not valid java name */
    private static double f225void = 30.0d;
    private static int w = 512;
    private static ArrayList x = new ArrayList();
    private static String y = (f.ac + "/yo.dat");

    k() {
    }

    private static int a(List list, int i2) {
        if (list == null || i2 > 256 || i2 < 0) {
            return -1;
        }
        try {
            if (f217else == null) {
                f217else = new File(y);
                if (!f217else.exists()) {
                    j.m250if(f216do, "upload man readfile does not exist...");
                    f217else = null;
                    return -2;
                }
            }
            RandomAccessFile randomAccessFile = new RandomAccessFile(f217else, "rw");
            if (randomAccessFile.length() < 1) {
                randomAccessFile.close();
                return -3;
            }
            randomAccessFile.seek((long) i2);
            int readInt = randomAccessFile.readInt();
            int readInt2 = randomAccessFile.readInt();
            int readInt3 = randomAccessFile.readInt();
            int readInt4 = randomAccessFile.readInt();
            long readLong = randomAccessFile.readLong();
            if (!a(readInt, readInt2, readInt3, readInt4, readLong) || readInt2 < 1) {
                randomAccessFile.close();
                return -4;
            }
            byte[] bArr = new byte[o];
            int i3 = readInt2;
            int i4 = f213byte;
            while (i4 > 0 && i3 > 0) {
                randomAccessFile.seek(((long) ((((readInt + i3) - 1) % readInt3) * readInt4)) + readLong);
                int readInt5 = randomAccessFile.readInt();
                if (readInt5 > 0 && readInt5 < readInt4) {
                    randomAccessFile.read(bArr, 0, readInt5);
                    if (bArr[readInt5 - 1] == 0) {
                        list.add(new String(bArr, 0, readInt5 - 1));
                    }
                }
                i4--;
                i3--;
            }
            randomAccessFile.seek((long) i2);
            randomAccessFile.writeInt(readInt);
            randomAccessFile.writeInt(i3);
            randomAccessFile.writeInt(readInt3);
            randomAccessFile.writeInt(readInt4);
            randomAccessFile.writeLong(readLong);
            randomAccessFile.close();
            return f213byte - i4;
        } catch (Exception e2) {
            return -5;
        }
    }

    public static String a(int i2) {
        String str;
        ArrayList arrayList;
        if (i2 == 1) {
            String str2 = u;
            str = str2;
            arrayList = x;
        } else if (i2 == 2) {
            String str3 = t;
            str = str3;
            arrayList = j;
        } else if (i2 == 3) {
            String str4 = k;
            str = str4;
            arrayList = r;
        } else if (i2 != 4) {
            return null;
        } else {
            String str5 = p;
            str = str5;
            arrayList = r;
        }
        if (arrayList == null) {
            return null;
        }
        if (arrayList.size() < 1) {
            j.m250if(f216do, str + ":get data from sd file...");
            a(str, arrayList);
        }
        if (arrayList.size() <= 0) {
            return null;
        }
        arrayList.remove(0);
        return (String) arrayList.get(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.k.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.location.k.a(java.util.List, int):int
      com.baidu.location.k.a(int, int):void
      com.baidu.location.k.a(java.lang.String, int):void
      com.baidu.location.k.a(android.location.Location, com.baidu.location.e$c):boolean
      com.baidu.location.k.a(android.location.Location, boolean):boolean
      com.baidu.location.k.a(java.lang.String, java.util.List):boolean
      com.baidu.location.k.a(int, boolean):void */
    public static void a() {
        b = 0;
        j.m250if(f216do, "flush data...");
        a(1, false);
        a(2, false);
        a(3, false);
        b = 8;
    }

    public static void a(double d2, double d3, double d4, double d5) {
        f223new = d2 > 0.0d ? d2 : f223new;
        n = d3;
        f225void = d4 > 20.0d ? d4 : f225void;
        f221int = d5;
    }

    public static void a(int i2, int i3) {
    }

    public static void a(int i2, int i3, boolean z) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.k.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.location.k.a(java.util.List, int):int
      com.baidu.location.k.a(int, int):void
      com.baidu.location.k.a(java.lang.String, int):void
      com.baidu.location.k.a(android.location.Location, com.baidu.location.e$c):boolean
      com.baidu.location.k.a(android.location.Location, boolean):boolean
      com.baidu.location.k.a(java.lang.String, java.util.List):boolean
      com.baidu.location.k.a(int, boolean):void */
    public static void a(int i2, boolean z) {
        String str;
        ArrayList arrayList;
        int i3;
        boolean z2;
        int i4;
        int i5;
        if (i2 == 1) {
            str = u;
            if (!z) {
                arrayList = x;
            } else {
                return;
            }
        } else if (i2 == 2) {
            str = t;
            arrayList = z ? x : j;
        } else if (i2 == 3) {
            str = k;
            arrayList = z ? j : r;
        } else if (i2 == 4) {
            str = p;
            if (z) {
                arrayList = r;
            } else {
                return;
            }
        } else {
            return;
        }
        File file = new File(str);
        if (!file.exists()) {
            a(str);
        }
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            randomAccessFile.seek(4);
            int readInt = randomAccessFile.readInt();
            int readInt2 = randomAccessFile.readInt();
            int readInt3 = randomAccessFile.readInt();
            int readInt4 = randomAccessFile.readInt();
            int i6 = readInt3;
            int readInt5 = randomAccessFile.readInt();
            int i7 = readInt4;
            int size = arrayList.size();
            while (true) {
                if (size <= b) {
                    i3 = readInt5;
                    z2 = false;
                    break;
                }
                j.m250if(f216do, "write new data...");
                int i8 = z ? readInt5 + 1 : readInt5;
                if (i6 >= readInt) {
                    if (!z) {
                        z2 = true;
                        i3 = i8;
                        break;
                    }
                    randomAccessFile.seek((long) ((i7 * readInt2) + 128));
                    byte[] bytes = (((String) arrayList.get(0)) + 0).getBytes();
                    randomAccessFile.writeInt(bytes.length);
                    randomAccessFile.write(bytes, 0, bytes.length);
                    arrayList.remove(0);
                    i4 = i7 + 1;
                    if (i4 > i6) {
                        i4 = 0;
                    }
                    i5 = i6;
                } else {
                    randomAccessFile.seek((long) ((readInt2 * i6) + 128));
                    byte[] bytes2 = (((String) arrayList.get(0)) + 0).getBytes();
                    randomAccessFile.writeInt(bytes2.length);
                    randomAccessFile.write(bytes2, 0, bytes2.length);
                    arrayList.remove(0);
                    int i9 = i7;
                    i5 = i6 + 1;
                    i4 = i9;
                }
                size--;
                i6 = i5;
                i7 = i4;
                readInt5 = i8;
            }
            randomAccessFile.seek(12);
            randomAccessFile.writeInt(i6);
            randomAccessFile.writeInt(i7);
            randomAccessFile.writeInt(i3);
            randomAccessFile.close();
            if (z2 && i2 < 4) {
                a(i2 + 1, true);
            }
        } catch (Exception e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.k.a(android.location.Location, boolean):boolean
     arg types: [android.location.Location, int]
     candidates:
      com.baidu.location.k.a(java.util.List, int):int
      com.baidu.location.k.a(int, int):void
      com.baidu.location.k.a(int, boolean):void
      com.baidu.location.k.a(java.lang.String, int):void
      com.baidu.location.k.a(android.location.Location, com.baidu.location.e$c):boolean
      com.baidu.location.k.a(java.lang.String, java.util.List):boolean
      com.baidu.location.k.a(android.location.Location, boolean):boolean */
    public static void a(c.a aVar, e.c cVar, Location location, String str) {
        String a2;
        if (j.O) {
            if (j.I == 3 && !a(location, cVar) && !a(location, false)) {
                return;
            }
            if (aVar != null && aVar.m120do()) {
                e.c cVar2 = !a(location, cVar) ? null : cVar;
                String a3 = j.a(aVar, cVar2, location, str, 1);
                if (a3 != null) {
                    m260for(Jni.m0if(a3));
                    q = location;
                    e = location;
                    if (cVar2 != null) {
                        g = cVar2;
                    }
                }
            } else if (cVar == null || !cVar.m147if() || !a(location, cVar)) {
                c.a aVar2 = !a(location) ? null : aVar;
                e.c cVar3 = !a(location, cVar) ? null : cVar;
                if ((aVar2 != null || cVar3 != null) && (a2 = j.a(aVar2, cVar3, location, str, 3)) != null) {
                    m264int(Jni.m0if(a2));
                    e = location;
                    if (cVar3 != null) {
                        g = cVar3;
                    }
                }
            } else {
                String a4 = j.a(!a(location) ? null : aVar, cVar, location, str, 2);
                if (a4 != null) {
                    String str2 = Jni.m0if(a4);
                    j.m250if(f216do, "upload size:" + str2.length());
                    m258do(str2);
                    a = location;
                    e = location;
                    if (cVar != null) {
                        g = cVar;
                    }
                }
            }
        }
    }

    public static void a(String str) {
        try {
            File file = new File(str);
            if (!file.exists()) {
                File file2 = new File(f.ac);
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                if (!file.createNewFile()) {
                    file = null;
                }
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                randomAccessFile.seek(0);
                randomAccessFile.writeInt(32);
                randomAccessFile.writeInt(2048);
                randomAccessFile.writeInt(d);
                randomAccessFile.writeInt(0);
                randomAccessFile.writeInt(0);
                randomAccessFile.writeInt(0);
                randomAccessFile.close();
            }
        } catch (Exception e2) {
        }
    }

    public static void a(String str, int i2) {
    }

    public static void a(String str, int i2, boolean z) {
    }

    private static boolean a(int i2, int i3, int i4, int i5, long j2) {
        if (i2 < 0 || i2 >= i4) {
            return false;
        }
        if (i3 < 0 || i3 > i4) {
            return false;
        }
        if (i4 < 0 || i4 > 1024) {
            return false;
        }
        return i5 >= 128 && i5 <= 1024;
    }

    private static boolean a(Location location) {
        if (location == null) {
            return false;
        }
        if (q == null || e == null) {
            q = location;
            return true;
        }
        double distanceTo = (double) location.distanceTo(q);
        return ((double) location.distanceTo(e)) > ((distanceTo * ((double) j.s)) + ((((double) j.u) * distanceTo) * distanceTo)) + ((double) j.r);
    }

    private static boolean a(Location location, e.c cVar) {
        if (location == null || cVar == null || cVar.f128for == null || cVar.f128for.isEmpty()) {
            return false;
        }
        if (cVar.m143do(g)) {
            return false;
        }
        if (a != null) {
            return true;
        }
        a = location;
        return true;
    }

    public static boolean a(Location location, boolean z) {
        return b.a(e, location, z);
    }

    public static boolean a(String str, List list) {
        File file = new File(str);
        if (!file.exists()) {
            return false;
        }
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            randomAccessFile.seek(8);
            int readInt = randomAccessFile.readInt();
            int readInt2 = randomAccessFile.readInt();
            int readInt3 = randomAccessFile.readInt();
            byte[] bArr = new byte[o];
            boolean z = false;
            int i2 = readInt2;
            int i3 = b + 1;
            int i4 = i2;
            while (i3 > 0 && i4 > 0) {
                if (i4 < readInt3) {
                    readInt3 = 0;
                }
                try {
                    randomAccessFile.seek((long) (((i4 - 1) * readInt) + 128));
                    int readInt4 = randomAccessFile.readInt();
                    if (readInt4 > 0 && readInt4 < readInt) {
                        randomAccessFile.read(bArr, 0, readInt4);
                        if (bArr[readInt4 - 1] == 0) {
                            list.add(new String(bArr, 0, readInt4 - 1));
                            z = true;
                        }
                    }
                    i3--;
                    i4--;
                } catch (Exception e2) {
                    return z;
                }
            }
            randomAccessFile.seek(12);
            randomAccessFile.writeInt(i4);
            randomAccessFile.writeInt(readInt3);
            randomAccessFile.close();
            return z;
        } catch (Exception e3) {
            return false;
        }
    }

    /* renamed from: do  reason: not valid java name */
    public static String m257do() {
        return m263int();
    }

    /* renamed from: do  reason: not valid java name */
    private static void m258do(String str) {
        m262if(str);
    }

    /* renamed from: for  reason: not valid java name */
    public static void m259for() {
    }

    /* renamed from: for  reason: not valid java name */
    private static void m260for(String str) {
        m262if(str);
    }

    /* renamed from: if  reason: not valid java name */
    public static String m261if() {
        File file = new File(t);
        if (file.exists()) {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                randomAccessFile.seek(20);
                int readInt = randomAccessFile.readInt();
                if (readInt > 128) {
                    String str = "&p1=" + readInt;
                    randomAccessFile.seek(20);
                    randomAccessFile.writeInt(0);
                    randomAccessFile.close();
                    return str;
                }
            } catch (Exception e2) {
            }
        }
        File file2 = new File(k);
        if (file2.exists()) {
            try {
                RandomAccessFile randomAccessFile2 = new RandomAccessFile(file2, "rw");
                randomAccessFile2.seek(20);
                int readInt2 = randomAccessFile2.readInt();
                if (readInt2 > 256) {
                    String str2 = "&p2=" + readInt2;
                    randomAccessFile2.seek(20);
                    randomAccessFile2.writeInt(0);
                    randomAccessFile2.close();
                    return str2;
                }
            } catch (Exception e3) {
            }
        }
        File file3 = new File(p);
        if (!file3.exists()) {
            return null;
        }
        try {
            RandomAccessFile randomAccessFile3 = new RandomAccessFile(file3, "rw");
            randomAccessFile3.seek(20);
            int readInt3 = randomAccessFile3.readInt();
            if (readInt3 <= 512) {
                return null;
            }
            String str3 = "&p3=" + readInt3;
            randomAccessFile3.seek(20);
            randomAccessFile3.writeInt(0);
            randomAccessFile3.close();
            return str3;
        } catch (Exception e4) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.k.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.baidu.location.k.a(java.util.List, int):int
      com.baidu.location.k.a(int, int):void
      com.baidu.location.k.a(java.lang.String, int):void
      com.baidu.location.k.a(android.location.Location, com.baidu.location.e$c):boolean
      com.baidu.location.k.a(android.location.Location, boolean):boolean
      com.baidu.location.k.a(java.lang.String, java.util.List):boolean
      com.baidu.location.k.a(int, boolean):void */
    /* renamed from: if  reason: not valid java name */
    public static void m262if(String str) {
        ArrayList arrayList;
        int i2 = j.l;
        if (i2 == 1) {
            arrayList = x;
        } else if (i2 == 2) {
            arrayList = j;
        } else if (i2 == 3) {
            arrayList = r;
        } else {
            return;
        }
        if (arrayList != null) {
            j.m250if(f216do, "insert2HighPriorityQueue...");
            if (arrayList.size() <= f224try) {
                arrayList.add(str);
            }
            if (arrayList.size() >= f224try) {
                a(i2, false);
            }
            while (arrayList.size() > f224try) {
                arrayList.remove(0);
            }
        }
    }

    /* renamed from: int  reason: not valid java name */
    public static String m263int() {
        String str;
        String str2 = null;
        for (int i2 = 1; i2 < 5; i2++) {
            str2 = a(i2);
            if (str2 != null) {
                return str2;
            }
        }
        j.m250if(f216do, "read the old file data...");
        a(r, f215char);
        if (r.size() > 0) {
            str = (String) r.get(0);
            r.remove(0);
        } else {
            str = str2;
        }
        if (str != null) {
            return str;
        }
        a(r, c);
        if (r.size() > 0) {
            str = (String) r.get(0);
            r.remove(0);
        }
        if (str != null) {
            return str;
        }
        a(r, v);
        if (r.size() <= 0) {
            return str;
        }
        String str3 = (String) r.get(0);
        r.remove(0);
        return str3;
    }

    /* renamed from: int  reason: not valid java name */
    private static void m264int(String str) {
        m262if(str);
    }

    /* renamed from: new  reason: not valid java name */
    public static void m265new() {
    }
}
