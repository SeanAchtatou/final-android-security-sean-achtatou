package com.tencent.mm.sdk.platformtools;

import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class KVConfig {
    private static boolean aa = false;

    private static void a(Map<String, String> map) {
        if (map == null || map.size() <= 0) {
            Log.v("MicroMsg.SDK.KVConfig", "empty values");
            return;
        }
        for (Map.Entry next : map.entrySet()) {
            Log.v("MicroMsg.SDK.KVConfig", "key=" + ((String) next.getKey()) + " value=" + ((String) next.getValue()));
        }
    }

    private static void a(Map<String, String> map, String str, Node node, int i) {
        if (node.getNodeName().equals("#text")) {
            map.put(str, node.getNodeValue());
        } else if (node.getNodeName().equals("#cdata-section")) {
            map.put(str, node.getNodeValue());
        } else {
            String str2 = str + "." + node.getNodeName() + (i > 0 ? Integer.valueOf(i) : "");
            map.put(str2, node.getNodeValue());
            NamedNodeMap attributes = node.getAttributes();
            if (attributes != null) {
                for (int i2 = 0; i2 < attributes.getLength(); i2++) {
                    Node item = attributes.item(i2);
                    map.put(str2 + ".$" + item.getNodeName(), item.getNodeValue());
                }
            }
            HashMap hashMap = new HashMap();
            NodeList childNodes = node.getChildNodes();
            for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                Node item2 = childNodes.item(i3);
                int nullAsNil = Util.nullAsNil((Integer) hashMap.get(item2.getNodeName()));
                a(map, str2, item2, nullAsNil);
                hashMap.put(item2.getNodeName(), Integer.valueOf(nullAsNil + 1));
            }
        }
    }

    public static Map<String, String> parseIni(String str) {
        String[] split;
        if (str == null || str.length() <= 0) {
            return null;
        }
        HashMap hashMap = new HashMap();
        for (String str2 : str.split("\n")) {
            if (str2 != null && str2.length() > 0 && (split = str2.trim().split("=", 2)) != null && split.length >= 2) {
                String str3 = split[0];
                String str4 = split[1];
                if (str3 != null && str3.length() > 0 && str3.matches("^[a-zA-Z0-9_]*")) {
                    hashMap.put(str3, str4);
                }
            }
        }
        if (!aa) {
            return hashMap;
        }
        a(hashMap);
        return hashMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0076, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0077, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x007d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007e, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0083, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0084, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0089, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008a, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007d A[ExcHandler: SAXException (r0v11 'e' org.xml.sax.SAXException A[CUSTOM_DECLARE]), Splitter:B:16:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0083 A[ExcHandler: IOException (r0v9 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:16:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0089 A[ExcHandler: Exception (r0v7 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:16:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map<java.lang.String, java.lang.String> parseXml(java.lang.String r9, java.lang.String r10, java.lang.String r11) {
        /*
            r7 = 1
            r6 = 0
            r5 = 0
            r0 = 60
            int r0 = r9.indexOf(r0)
            if (r0 >= 0) goto L_0x0014
            java.lang.String r0 = "MicroMsg.SDK.KVConfig"
            java.lang.String r1 = "text not in xml format"
            com.tencent.mm.sdk.platformtools.Log.e(r0, r1)
            r0 = r5
        L_0x0013:
            return r0
        L_0x0014:
            if (r0 <= 0) goto L_0x00ea
            java.lang.String r1 = "MicroMsg.SDK.KVConfig"
            java.lang.String r2 = "fix xml header from + %d"
            java.lang.Object[] r3 = new java.lang.Object[r7]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)
            r3[r6] = r4
            com.tencent.mm.sdk.platformtools.Log.w(r1, r2, r3)
            java.lang.String r0 = r9.substring(r0)
        L_0x0029:
            if (r0 == 0) goto L_0x0031
            int r1 = r0.length()
            if (r1 > 0) goto L_0x0033
        L_0x0031:
            r0 = r5
            goto L_0x0013
        L_0x0033:
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            javax.xml.parsers.DocumentBuilderFactory r2 = javax.xml.parsers.DocumentBuilderFactory.newInstance()
            javax.xml.parsers.DocumentBuilder r2 = r2.newDocumentBuilder()     // Catch:{ ParserConfigurationException -> 0x004b }
            if (r2 != 0) goto L_0x0051
            java.lang.String r0 = "MicroMsg.SDK.KVConfig"
            java.lang.String r1 = "new Document Builder failed"
            com.tencent.mm.sdk.platformtools.Log.e(r0, r1)
            r0 = r5
            goto L_0x0013
        L_0x004b:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r5
            goto L_0x0013
        L_0x0051:
            org.xml.sax.InputSource r3 = new org.xml.sax.InputSource     // Catch:{ DOMException -> 0x0076, SAXException -> 0x007d, IOException -> 0x0083, Exception -> 0x0089 }
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ DOMException -> 0x0076, SAXException -> 0x007d, IOException -> 0x0083, Exception -> 0x0089 }
            byte[] r0 = r0.getBytes()     // Catch:{ DOMException -> 0x0076, SAXException -> 0x007d, IOException -> 0x0083, Exception -> 0x0089 }
            r4.<init>(r0)     // Catch:{ DOMException -> 0x0076, SAXException -> 0x007d, IOException -> 0x0083, Exception -> 0x0089 }
            r3.<init>(r4)     // Catch:{ DOMException -> 0x0076, SAXException -> 0x007d, IOException -> 0x0083, Exception -> 0x0089 }
            if (r11 == 0) goto L_0x0064
            r3.setEncoding(r11)     // Catch:{ DOMException -> 0x0076, SAXException -> 0x007d, IOException -> 0x0083, Exception -> 0x0089 }
        L_0x0064:
            org.w3c.dom.Document r0 = r2.parse(r3)     // Catch:{ DOMException -> 0x0076, SAXException -> 0x007d, IOException -> 0x0083, Exception -> 0x0089 }
            r0.normalize()     // Catch:{ DOMException -> 0x00e5, SAXException -> 0x007d, IOException -> 0x0083, Exception -> 0x0089 }
        L_0x006b:
            if (r0 != 0) goto L_0x008f
            java.lang.String r0 = "MicroMsg.SDK.KVConfig"
            java.lang.String r1 = "new Document failed"
            com.tencent.mm.sdk.platformtools.Log.e(r0, r1)
            r0 = r5
            goto L_0x0013
        L_0x0076:
            r0 = move-exception
            r2 = r5
        L_0x0078:
            r0.printStackTrace()
            r0 = r2
            goto L_0x006b
        L_0x007d:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r5
            goto L_0x0013
        L_0x0083:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r5
            goto L_0x0013
        L_0x0089:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r5
            goto L_0x0013
        L_0x008f:
            org.w3c.dom.Element r0 = r0.getDocumentElement()
            if (r0 != 0) goto L_0x009f
            java.lang.String r0 = "MicroMsg.SDK.KVConfig"
            java.lang.String r1 = "getDocumentElement failed"
            com.tencent.mm.sdk.platformtools.Log.e(r0, r1)
            r0 = r5
            goto L_0x0013
        L_0x009f:
            if (r10 == 0) goto L_0x00ba
            java.lang.String r2 = r0.getNodeName()
            boolean r2 = r10.equals(r2)
            if (r2 == 0) goto L_0x00ba
            java.lang.String r2 = ""
            a(r1, r2, r0, r6)
        L_0x00b0:
            boolean r0 = com.tencent.mm.sdk.platformtools.KVConfig.aa
            if (r0 == 0) goto L_0x00b7
            a(r1)
        L_0x00b7:
            r0 = r1
            goto L_0x0013
        L_0x00ba:
            org.w3c.dom.NodeList r0 = r0.getElementsByTagName(r10)
            int r2 = r0.getLength()
            if (r2 > 0) goto L_0x00ce
            java.lang.String r0 = "MicroMsg.SDK.KVConfig"
            java.lang.String r1 = "parse item null"
            com.tencent.mm.sdk.platformtools.Log.e(r0, r1)
            r0 = r5
            goto L_0x0013
        L_0x00ce:
            int r2 = r0.getLength()
            if (r2 <= r7) goto L_0x00db
            java.lang.String r2 = "MicroMsg.SDK.KVConfig"
            java.lang.String r3 = "parse items more than one"
            com.tencent.mm.sdk.platformtools.Log.w(r2, r3)
        L_0x00db:
            java.lang.String r2 = ""
            org.w3c.dom.Node r0 = r0.item(r6)
            a(r1, r2, r0, r6)
            goto L_0x00b0
        L_0x00e5:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            goto L_0x0078
        L_0x00ea:
            r0 = r9
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mm.sdk.platformtools.KVConfig.parseXml(java.lang.String, java.lang.String, java.lang.String):java.util.Map");
    }
}
