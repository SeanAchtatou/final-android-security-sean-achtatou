package com.tencent.mm.sdk.storage;

import android.os.Looper;

public abstract class MStorage {
    private final MStorageEvent<IOnStorageChange, String> bM = new MStorageEvent<IOnStorageChange, String>() {
        /* access modifiers changed from: protected */
        public /* synthetic */ void processEvent(Object obj, Object obj2) {
            MStorage mStorage = MStorage.this;
            ((IOnStorageChange) obj).onNotifyChange((String) obj2);
        }
    };
    private final MStorageEvent<IOnStorageLoaded, String> bN = new MStorageEvent<IOnStorageLoaded, String>() {
        /* access modifiers changed from: protected */
        public /* synthetic */ void processEvent(Object obj, Object obj2) {
            MStorage mStorage = MStorage.this;
            ((IOnStorageLoaded) obj).onNotifyLoaded();
        }
    };

    public interface IOnStorageChange {
        void onNotifyChange(String str);
    }

    public interface IOnStorageLoaded {
        void onNotifyLoaded();
    }

    public void add(IOnStorageChange iOnStorageChange) {
        this.bM.add(iOnStorageChange, Looper.getMainLooper());
    }

    public void addLoadedListener(IOnStorageLoaded iOnStorageLoaded) {
        this.bN.add(iOnStorageLoaded, Looper.getMainLooper());
    }

    public void doNotify() {
        this.bM.event("*");
        this.bM.doNotify();
    }

    public void doNotify(String str) {
        this.bM.event(str);
        this.bM.doNotify();
    }

    public void lock() {
        this.bM.lock();
    }

    public void remove(IOnStorageChange iOnStorageChange) {
        this.bM.remove(iOnStorageChange);
    }

    public void removeLoadedListener(IOnStorageLoaded iOnStorageLoaded) {
        this.bN.remove(iOnStorageLoaded);
    }

    public void unlock() {
        this.bM.unlock();
    }
}
