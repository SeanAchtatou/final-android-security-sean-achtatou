package com.tencent.mm.sdk.platformtools;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import android.os.Vibrator;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.tencent.mm.algorithm.MD5;
import com.tencent.mm.sdk.plugin.MMPluginProviderConstants;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.Character;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import junit.framework.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public final class Util {
    public static final int BEGIN_TIME = 22;
    public static final int BIT_OF_KB = 10;
    public static final int BIT_OF_MB = 20;
    public static final int BYTE_OF_KB = 1024;
    public static final int BYTE_OF_MB = 1048576;
    public static final String CHINA = "zh_CN";
    public static final int DAY = 0;
    public static final int END_TIME = 8;
    public static final String ENGLISH = "en";
    private static final TimeZone GMT = TimeZone.getTimeZone("GMT");
    public static final String HONGKONG = "zh_HK";
    public static final String LANGUAGE_DEFAULT = "language_default";
    public static final int MASK_16BIT = 65535;
    public static final int MASK_32BIT = -1;
    public static final int MASK_4BIT = 15;
    public static final int MASK_8BIT = 255;
    public static final long MAX_32BIT_VALUE = 4294967295L;
    public static final int MAX_ACCOUNT_LENGTH = 20;
    public static final int MAX_DECODE_PICTURE_SIZE = 2764800;
    public static final int MAX_PASSWORD_LENGTH = 9;
    public static final long MILLSECONDS_OF_DAY = 86400000;
    public static final long MILLSECONDS_OF_HOUR = 3600000;
    public static final long MILLSECONDS_OF_MINUTE = 60000;
    public static final long MILLSECONDS_OF_SECOND = 1000;
    public static final long MINUTE_OF_HOUR = 60;
    public static final int MIN_ACCOUNT_LENGTH = 6;
    public static final int MIN_PASSWORD_LENGTH = 4;
    public static final String PHOTO_DEFAULT_EXT = ".jpg";
    public static final long SECOND_OF_MINUTE = 60;
    public static final String TAIWAN = "zh_TW";
    private static final long[] bt = {300, 200, 300, 200};
    private static final char[] bu = {9, 10, 13};
    private static final char[] bv = {'<', '>', '\"', '\'', '&'};
    private static final String[] bw = {"&lt;", "&gt;", "&quot;", "&apos;", "&amp;"};

    private Util() {
    }

    public static String GetHostIp() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress();
                        }
                    }
                }
            }
        } catch (Exception | SocketException e) {
        }
        return null;
    }

    public static int UnZipFolder(String str, String str2) {
        try {
            Log.v("XZip", "UnZipFolder(String, String)");
            ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(str));
            while (true) {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry != null) {
                    String name = nextEntry.getName();
                    if (nextEntry.isDirectory()) {
                        new File(str2 + File.separator + name.substring(0, name.length() - 1)).mkdirs();
                    } else {
                        File file = new File(str2 + File.separator + name);
                        file.createNewFile();
                        FileOutputStream fileOutputStream = new FileOutputStream(file);
                        byte[] bArr = new byte[1024];
                        while (true) {
                            int read = zipInputStream.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(bArr, 0, read);
                            fileOutputStream.flush();
                        }
                        fileOutputStream.close();
                    }
                } else {
                    zipInputStream.close();
                    return 0;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return -1;
        } catch (IOException e2) {
            e2.printStackTrace();
            return -2;
        }
    }

    private static int a(char[] cArr, int i, int i2) {
        if (i2 <= 0) {
            return 0;
        }
        if (cArr[i] != '#') {
            new String(cArr, i, i2);
            return 0;
        } else if (i2 <= 1 || !(cArr[i + 1] == 'x' || cArr[i + 1] == 'X')) {
            try {
                return Integer.parseInt(new String(cArr, i + 1, i2 - 1), 10);
            } catch (NumberFormatException e) {
                return 0;
            }
        } else {
            try {
                return Integer.parseInt(new String(cArr, i + 2, i2 - 2), 16);
            } catch (NumberFormatException e2) {
                return 0;
            }
        }
    }

    private static void a(Map<String, String> map, String str, Node node, int i) {
        if (node.getNodeName().equals("#text")) {
            map.put(str, node.getNodeValue());
        } else if (node.getNodeName().equals("#cdata-section")) {
            map.put(str, node.getNodeValue());
        } else {
            String str2 = str + "." + node.getNodeName() + (i > 0 ? Integer.valueOf(i) : "");
            map.put(str2, node.getNodeValue());
            NamedNodeMap attributes = node.getAttributes();
            if (attributes != null) {
                for (int i2 = 0; i2 < attributes.getLength(); i2++) {
                    Node item = attributes.item(i2);
                    map.put(str2 + ".$" + item.getNodeName(), item.getNodeValue());
                }
            }
            HashMap hashMap = new HashMap();
            NodeList childNodes = node.getChildNodes();
            for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                Node item2 = childNodes.item(i3);
                int nullAsNil = nullAsNil((Integer) hashMap.get(item2.getNodeName()));
                a(map, str2, item2, nullAsNil);
                hashMap.put(item2.getNodeName(), Integer.valueOf(nullAsNil + 1));
            }
        }
    }

    public static byte[] bmpToByteArray(Bitmap bitmap, boolean z) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        if (z) {
            bitmap.recycle();
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        try {
            byteArrayOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return byteArray;
    }

    public static boolean checkPermission(Context context, String str) {
        Assert.assertNotNull(context);
        String packageName = context.getPackageName();
        boolean z = context.getPackageManager().checkPermission(str, packageName) == 0;
        Log.d("MicroMsg.Util", packageName + " has " + (z ? "permission " : "no permission ") + str);
        return z;
    }

    public static boolean checkSDCardFull() {
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return false;
        }
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long blockCount = (long) statFs.getBlockCount();
        long availableBlocks = (long) statFs.getAvailableBlocks();
        if (blockCount <= 0) {
            return false;
        }
        if (blockCount - availableBlocks < 0) {
            return false;
        }
        int i = (int) (((blockCount - availableBlocks) * 100) / blockCount);
        long blockSize = ((long) statFs.getBlockSize()) * ((long) statFs.getFreeBlocks());
        Log.d("MicroMsg.Util", "checkSDCardFull per:" + i + " blockCount:" + blockCount + " availCount:" + availableBlocks + " availSize:" + blockSize);
        if (95 > i) {
            return false;
        }
        return blockSize <= 52428800;
    }

    public static String convertStreamToString(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine + "\n");
                } else {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        inputStream.close();
        return sb.toString();
    }

    public static long currentDayInMills() {
        return (nowMilliSecond() / 86400000) * 86400000;
    }

    public static long currentMonthInMills() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        GregorianCalendar gregorianCalendar2 = new GregorianCalendar(gregorianCalendar.get(1), gregorianCalendar.get(2), 1);
        gregorianCalendar2.setTimeZone(GMT);
        return gregorianCalendar2.getTimeInMillis();
    }

    public static long currentTicks() {
        return SystemClock.elapsedRealtime();
    }

    public static long currentWeekInMills() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        GregorianCalendar gregorianCalendar2 = new GregorianCalendar(gregorianCalendar.get(1), gregorianCalendar.get(2), gregorianCalendar.get(5));
        gregorianCalendar2.setTimeZone(GMT);
        gregorianCalendar2.add(6, -(gregorianCalendar.get(7) - gregorianCalendar.getFirstDayOfWeek()));
        return gregorianCalendar2.getTimeInMillis();
    }

    public static long currentYearInMills() {
        GregorianCalendar gregorianCalendar = new GregorianCalendar(new GregorianCalendar().get(1), 1, 1);
        gregorianCalendar.setTimeZone(GMT);
        return gregorianCalendar.getTimeInMillis();
    }

    public static byte[] decodeHexString(String str) {
        if (str == null || str.length() <= 0) {
            return new byte[0];
        }
        try {
            byte[] bArr = new byte[(str.length() / 2)];
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = (byte) (Integer.parseInt(str.substring(i * 2, (i * 2) + 2), 16) & 255);
            }
            return bArr;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    public static String dumpArray(Object[] objArr) {
        StringBuilder sb = new StringBuilder();
        for (Object append : objArr) {
            sb.append(append);
            sb.append(AdApiConstant.RES_SPLIT_COMMA);
        }
        return sb.toString();
    }

    public static String dumpHex(byte[] bArr) {
        int i = 0;
        if (bArr == null) {
            return "(null)";
        }
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        int length = bArr.length;
        char[] cArr2 = new char[((length * 3) + (length / 16))];
        for (int i2 = 0; i2 < length; i2++) {
            byte b = bArr[i2];
            int i3 = i + 1;
            cArr2[i] = ' ';
            int i4 = i3 + 1;
            cArr2[i3] = cArr[(b >>> 4) & 15];
            int i5 = i4 + 1;
            cArr2[i4] = cArr[b & 15];
            if (i2 % 16 != 0 || i2 <= 0) {
                i = i5;
            } else {
                i = i5 + 1;
                cArr2[i5] = 10;
            }
        }
        return new String(cArr2);
    }

    public static String encodeHexString(byte[] bArr) {
        StringBuilder sb = new StringBuilder("");
        if (bArr != null) {
            for (int i = 0; i < bArr.length; i++) {
                sb.append(String.format("%02x", Integer.valueOf(bArr[i] & 255)));
            }
        }
        return sb.toString();
    }

    public static String escapeSqlValue(String str) {
        return str != null ? str.replace("\\[", "[[]").replace("%", "").replace("\\^", "").replace("'", "").replace("\\{", "").replace("\\}", "").replace("\"", "") : str;
    }

    public static String escapeStringForXml(String str) {
        boolean z;
        if (str == null) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if ((charAt >= ' ' || charAt == bu[0] || charAt == bu[1] || charAt == bu[2]) && charAt <= 127) {
                int length2 = bv.length - 1;
                while (true) {
                    if (length2 < 0) {
                        z = true;
                        break;
                    } else if (bv[length2] == charAt) {
                        stringBuffer.append(bw[length2]);
                        z = false;
                        break;
                    } else {
                        length2--;
                    }
                }
                if (z) {
                    stringBuffer.append(charAt);
                }
            } else {
                stringBuffer.append("&#");
                stringBuffer.append(Integer.toString(charAt));
                stringBuffer.append(';');
            }
        }
        return stringBuffer.toString();
    }

    public static String expandEntities(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = 0;
        int i2 = -1;
        for (int i3 = 0; i3 < length; i3++) {
            char charAt = str.charAt(i3);
            int i4 = i + 1;
            cArr[i] = charAt;
            if (charAt == '&' && i2 == -1) {
                i = i4;
                i2 = i4;
            } else if (i2 == -1 || Character.isLetter(charAt) || Character.isDigit(charAt) || charAt == '#') {
                i = i4;
            } else if (charAt == ';') {
                int a = a(cArr, i2, (i4 - i2) - 1);
                if (a > 65535) {
                    int i5 = a - 65536;
                    cArr[i2 - 1] = (char) ((i5 >> 10) + 55296);
                    cArr[i2] = (char) ((i5 & 1023) + 56320);
                    i = i2 + 1;
                } else if (a != 0) {
                    cArr[i2 - 1] = (char) a;
                    i = i2;
                } else {
                    i = i4;
                }
                i2 = -1;
            } else {
                i = i4;
                i2 = -1;
            }
        }
        return new String(cArr, 0, i);
    }

    public static String formatSecToMin(int i) {
        return String.format("%d:%02d", Long.valueOf(((long) i) / 60), Long.valueOf(((long) i) % 60));
    }

    public static String formatUnixTime(long j) {
        return new SimpleDateFormat("[yy-MM-dd HH:mm:ss]").format(new Date(1000 * j));
    }

    public static void freeBitmapMap(Map<String, Bitmap> map) {
        for (Map.Entry<String, Bitmap> value : map.entrySet()) {
            Bitmap bitmap = (Bitmap) value.getValue();
            if (bitmap != null) {
                bitmap.recycle();
            }
        }
        map.clear();
    }

    public static String getCutPasswordMD5(String str) {
        String str2 = str == null ? "" : str;
        return str2.length() <= 16 ? getFullPasswordMD5(str2) : getFullPasswordMD5(str2.substring(0, 16));
    }

    public static String getDeviceId(Context context) {
        if (context == null) {
            return null;
        }
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager == null) {
                return null;
            }
            String deviceId = telephonyManager.getDeviceId();
            if (deviceId == null) {
                return null;
            }
            return deviceId.trim();
        } catch (SecurityException e) {
            Log.e("MicroMsg.Util", "getDeviceId failed, security exception");
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String getFullPasswordMD5(String str) {
        return MD5.getMessageDigest(str.getBytes());
    }

    public static int getHex(String str, int i) {
        if (str == null) {
            return i;
        }
        try {
            return (int) (Long.decode(str).longValue() & MAX_32BIT_VALUE);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return i;
        }
    }

    public static BitmapFactory.Options getImageOptions(String str) {
        Assert.assertTrue(str != null && !str.equals(""));
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
            if (decodeFile != null) {
                decodeFile.recycle();
            }
        } catch (OutOfMemoryError e) {
            Log.e("MicroMsg.Util", "decode bitmap failed: " + e.getMessage());
        }
        return options;
    }

    public static Intent getInstallPackIntent(String str, Context context) {
        Assert.assertTrue(str != null && !str.equals(""));
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), "application/vnd.android.package-archive");
        return intent;
    }

    public static int getInt(String str, int i) {
        if (str == null) {
            return i;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return i;
        }
    }

    public static int getIntRandom(int i, int i2) {
        Assert.assertTrue(i > i2);
        return new Random(System.currentTimeMillis()).nextInt((i - i2) + 1) + i2;
    }

    public static String getLine1Number(Context context) {
        if (context == null) {
            return null;
        }
        try {
            if (((TelephonyManager) context.getSystemService("phone")) == null) {
                Log.e("MicroMsg.Util", "get line1 number failed, null tm");
                return null;
            }
        } catch (SecurityException e) {
            Log.e("MicroMsg.Util", "getLine1Number failed, security exception");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public static long getLong(String str, long j) {
        if (str == null) {
            return j;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return j;
        }
    }

    public static Element getRootElementFromXML(byte[] bArr) {
        try {
            DocumentBuilder newDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            if (newDocumentBuilder == null) {
                Log.e("MicroMsg.Util", "new Document Builder failed");
                return null;
            }
            try {
                Document parse = newDocumentBuilder.parse(new ByteArrayInputStream(bArr));
                if (parse != null) {
                    return parse.getDocumentElement();
                }
                Log.e("MicroMsg.Util", "new Document failed");
                return null;
            } catch (SAXException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e2) {
                e2.printStackTrace();
                return null;
            }
        } catch (ParserConfigurationException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, boolean z, float f) {
        Assert.assertNotNull(bitmap);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setFilterBitmap(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-4144960);
        canvas.drawRoundRect(rectF, f, f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        if (z) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    public static String getSizeKB(long j) {
        if ((j >> 20) > 0) {
            return getSizeMB(j);
        }
        if ((j >> 9) <= 0) {
            return j + "B";
        }
        return (((float) Math.round((((float) j) * 10.0f) / 1024.0f)) / 10.0f) + "KB";
    }

    public static String getSizeMB(long j) {
        return (((float) Math.round((((float) j) * 10.0f) / 1048576.0f)) / 10.0f) + "MB";
    }

    public static String getStack() {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        if (stackTrace == null || stackTrace.length < 2) {
            return "";
        }
        String str = "";
        int i = 1;
        while (i < stackTrace.length && stackTrace[i].getClassName().contains(MMPluginProviderConstants.PluginIntent.APP_PACKAGE_PATTERN)) {
            str = str + "[" + stackTrace[i].getClassName().substring(15) + ":" + stackTrace[i].getMethodName() + "]";
            i++;
        }
        return str;
    }

    public static int getSystemVersion(Context context, int i) {
        return context == null ? i : Settings.System.getInt(context.getContentResolver(), "sys.settings_system_version", i);
    }

    public static String getTimeZone() {
        String timeZoneDef = getTimeZoneDef();
        int indexOf = timeZoneDef.indexOf(43);
        if (indexOf == -1) {
            indexOf = timeZoneDef.indexOf(45);
        }
        if (indexOf == -1) {
            return "";
        }
        String substring = timeZoneDef.substring(indexOf, indexOf + 3);
        return substring.charAt(1) == '0' ? substring.substring(0, 1) + substring.substring(2, 3) : substring;
    }

    public static String getTimeZoneDef() {
        int i;
        char c;
        int rawOffset = (int) (((long) TimeZone.getDefault().getRawOffset()) / MILLSECONDS_OF_MINUTE);
        if (rawOffset < 0) {
            i = -rawOffset;
            c = '-';
        } else {
            i = rawOffset;
            c = '+';
        }
        return String.format("GMT%s%02d:%02d", Character.valueOf(c), Long.valueOf(((long) i) / 60), Long.valueOf(((long) i) % 60));
    }

    public static String getTimeZoneOffset() {
        TimeZone timeZone = TimeZone.getDefault();
        return String.format("%.2f", Double.valueOf((((double) (((long) (timeZone.getRawOffset() * 100)) / MILLSECONDS_OF_HOUR)) / 100.0d) + ((double) (timeZone.useDaylightTime() ? 1 : 0))));
    }

    public static String getTopActivityName(Context context) {
        try {
            return ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getClassName();
        } catch (Exception e) {
            e.printStackTrace();
            return "(null)";
        }
    }

    public static int guessHttpContinueRecvLength(int i) {
        return ((((i - 1) / 1462) + 1) * 52) + 52 + i;
    }

    public static int guessHttpRecvLength(int i) {
        return ((((i - 1) / 1462) + 1) * 52) + 208 + i;
    }

    public static int guessHttpSendLength(int i) {
        return ((((i - 1) / 1462) + 1) * 52) + 224 + i;
    }

    public static int guessTcpConnectLength() {
        return 172;
    }

    public static int guessTcpDisconnectLength() {
        return 156;
    }

    public static int guessTcpRecvLength(int i) {
        return ((((i - 1) / 1462) + 1) * 52) + 40 + i;
    }

    public static int guessTcpSendLength(int i) {
        return ((((i - 1) / 1462) + 1) * 52) + 40 + i;
    }

    public static void installPack(String str, Context context) {
        context.startActivity(getInstallPackIntent(str, context));
    }

    public static boolean isAlpha(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }

    public static boolean isChinese(char c) {
        Character.UnicodeBlock of = Character.UnicodeBlock.of(c);
        return of == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || of == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS || of == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || of == Character.UnicodeBlock.GENERAL_PUNCTUATION || of == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || of == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS;
    }

    public static boolean isDayTimeNow() {
        int i = new GregorianCalendar().get(11);
        return ((long) i) >= 6 && ((long) i) < 18;
    }

    public static boolean isImgFile(String str) {
        if (str == null || str.length() == 0) {
            Log.e("MicroMsg.Util", "isImgFile, invalid argument");
            return false;
        } else if (str.length() < 3) {
            return false;
        } else {
            if (!new File(str).exists()) {
                return false;
            }
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(str, options);
            return options.outWidth > 0 && options.outHeight > 0;
        }
    }

    public static boolean isIntentAvailable(Context context, Intent intent) {
        return context.getPackageManager().queryIntentActivities(intent, 65536).size() > 0;
    }

    public static boolean isLockScreen(Context context) {
        try {
            return ((KeyguardManager) context.getSystemService("keyguard")).inKeyguardRestrictedInputMode();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isNightTime(int i, int i2, int i3) {
        if (i2 > i3) {
            return i >= i2 || i <= i3;
        }
        if (i2 < i3) {
            return i <= i3 && i >= i2;
        }
        return true;
    }

    public static boolean isNullOrNil(String str) {
        return str == null || str.length() <= 0;
    }

    public static boolean isNullOrNil(byte[] bArr) {
        return bArr == null || bArr.length <= 0;
    }

    public static boolean isNum(char c) {
        return c >= '0' && c <= '9';
    }

    public static boolean isProcessRunning(Context context, String str) {
        for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (next != null && next.processName != null && next.processName.equals(str)) {
                Log.w("MicroMsg.Util", "process " + str + " is running");
                return true;
            }
        }
        Log.w("MicroMsg.Util", "process " + str + " is not running");
        return false;
    }

    public static boolean isServiceRunning(Context context, String str) {
        for (ActivityManager.RunningServiceInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningServices(Integer.MAX_VALUE)) {
            if (next != null && next.service != null && next.service.getClassName().toString().equals(str)) {
                Log.w("MicroMsg.Util", "service " + str + " is running");
                return true;
            }
        }
        Log.w("MicroMsg.Util", "service " + str + " is not running");
        return false;
    }

    public static boolean isTopActivity(Context context) {
        String name = context.getClass().getName();
        String topActivityName = getTopActivityName(context);
        Log.d("MicroMsg.Util", "top activity=" + topActivityName + ", context=" + name);
        return topActivityName.equalsIgnoreCase(name);
    }

    public static boolean isTopApplication(Context context) {
        try {
            String className = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getClassName();
            String packageName = context.getPackageName();
            Log.d("MicroMsg.Util", "top activity=" + className + ", context=" + packageName);
            return className.contains(packageName);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isValidAccount(String str) {
        if (str == null) {
            return false;
        }
        String trim = str.trim();
        if (trim.length() < 6 || trim.length() > 20) {
            return false;
        }
        if (!isAlpha(trim.charAt(0))) {
            return false;
        }
        for (int i = 0; i < trim.length(); i++) {
            char charAt = trim.charAt(i);
            if (!isAlpha(charAt) && !isNum(charAt) && charAt != '-' && charAt != '_') {
                return false;
            }
        }
        return true;
    }

    public static boolean isValidEmail(String str) {
        if (str == null || str.length() <= 0) {
            return false;
        }
        return str.trim().matches("^[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$");
    }

    public static boolean isValidPassword(String str) {
        if (str == null) {
            return false;
        }
        if (str.length() < 4) {
            return false;
        }
        if (str.length() >= 9) {
            return true;
        }
        try {
            Integer.parseInt(str);
            return false;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    public static boolean isValidQQNum(String str) {
        if (str == null || str.length() <= 0) {
            return false;
        }
        try {
            long longValue = Long.valueOf(str.trim()).longValue();
            return longValue > 0 && longValue <= MAX_32BIT_VALUE;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean jump(Context context, String str) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        if (!isIntentAvailable(context, intent)) {
            Log.e("MicroMsg.Util", "jump to url failed, " + str);
            return false;
        }
        context.startActivity(intent);
        return true;
    }

    public static String listToString(List<String> list, String str) {
        if (list == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder("");
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return sb.toString();
            }
            if (i2 == list.size() - 1) {
                sb.append(list.get(i2).trim());
            } else {
                sb.append(list.get(i2).trim() + str);
            }
            i = i2 + 1;
        }
    }

    public static String mapToXml(String str, LinkedHashMap<String, String> linkedHashMap) {
        StringBuilder sb = new StringBuilder();
        sb.append("<key>");
        for (Map.Entry next : linkedHashMap.entrySet()) {
            Object key = next.getKey();
            Object value = next.getValue();
            if (key == null) {
                key = "unknow";
            }
            if (value == null) {
                value = "unknow";
            }
            sb.append("<" + key + ">");
            sb.append(value);
            sb.append("</" + key + ">");
        }
        sb.append("</key>");
        return sb.toString();
    }

    public static long milliSecondsToNow(long j) {
        return System.currentTimeMillis() - j;
    }

    public static long nowMilliSecond() {
        return System.currentTimeMillis();
    }

    public static long nowSecond() {
        return System.currentTimeMillis() / 1000;
    }

    public static int nullAs(Integer num, int i) {
        return num == null ? i : num.intValue();
    }

    public static long nullAs(Long l, long j) {
        return l == null ? j : l.longValue();
    }

    public static String nullAs(String str, String str2) {
        return str == null ? str2 : str;
    }

    public static boolean nullAs(Boolean bool, boolean z) {
        return bool == null ? z : bool.booleanValue();
    }

    public static boolean nullAsFalse(Boolean bool) {
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public static int nullAsInt(Object obj, int i) {
        return obj == null ? i : obj instanceof Integer ? ((Integer) obj).intValue() : obj instanceof Long ? ((Long) obj).intValue() : i;
    }

    public static int nullAsNil(Integer num) {
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    public static long nullAsNil(Long l) {
        if (l == null) {
            return 0;
        }
        return l.longValue();
    }

    public static String nullAsNil(String str) {
        return str == null ? "" : str;
    }

    public static boolean nullAsTrue(Boolean bool) {
        if (bool == null) {
            return true;
        }
        return bool.booleanValue();
    }

    public static Map<String, String> parseIni(String str) {
        String[] split;
        if (str == null || str.length() <= 0) {
            return null;
        }
        HashMap hashMap = new HashMap();
        for (String str2 : str.split("\n")) {
            if (str2 != null && str2.length() > 0 && (split = str2.trim().split("=", 2)) != null && split.length >= 2) {
                String str3 = split[0];
                String str4 = split[1];
                if (str3 != null && str3.length() > 0 && str3.matches("^[a-zA-Z0-9_]*")) {
                    hashMap.put(str3, str4);
                }
            }
        }
        return hashMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004f, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0050, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0056, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0057, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005d, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0062, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0063, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0056 A[ExcHandler: SAXException (r1v10 'e' org.xml.sax.SAXException A[CUSTOM_DECLARE]), Splitter:B:12:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005c A[ExcHandler: IOException (r1v8 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:12:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0062 A[ExcHandler: Exception (r1v6 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:12:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0068  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Map<java.lang.String, java.lang.String> parseXml(java.lang.String r9, java.lang.String r10, java.lang.String r11) {
        /*
            r7 = 0
            r6 = 0
            if (r9 == 0) goto L_0x000a
            int r1 = r9.length()
            if (r1 > 0) goto L_0x000c
        L_0x000a:
            r1 = r6
        L_0x000b:
            return r1
        L_0x000c:
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            javax.xml.parsers.DocumentBuilderFactory r1 = javax.xml.parsers.DocumentBuilderFactory.newInstance()
            javax.xml.parsers.DocumentBuilder r1 = r1.newDocumentBuilder()     // Catch:{ ParserConfigurationException -> 0x0024 }
            if (r1 != 0) goto L_0x002a
            java.lang.String r1 = "MicroMsg.Util"
            java.lang.String r2 = "new Document Builder failed"
            com.tencent.mm.sdk.platformtools.Log.e(r1, r2)
            r1 = r6
            goto L_0x000b
        L_0x0024:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r6
            goto L_0x000b
        L_0x002a:
            org.xml.sax.InputSource r3 = new org.xml.sax.InputSource     // Catch:{ DOMException -> 0x004f, SAXException -> 0x0056, IOException -> 0x005c, Exception -> 0x0062 }
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ DOMException -> 0x004f, SAXException -> 0x0056, IOException -> 0x005c, Exception -> 0x0062 }
            byte[] r5 = r9.getBytes()     // Catch:{ DOMException -> 0x004f, SAXException -> 0x0056, IOException -> 0x005c, Exception -> 0x0062 }
            r4.<init>(r5)     // Catch:{ DOMException -> 0x004f, SAXException -> 0x0056, IOException -> 0x005c, Exception -> 0x0062 }
            r3.<init>(r4)     // Catch:{ DOMException -> 0x004f, SAXException -> 0x0056, IOException -> 0x005c, Exception -> 0x0062 }
            if (r11 == 0) goto L_0x003d
            r3.setEncoding(r11)     // Catch:{ DOMException -> 0x004f, SAXException -> 0x0056, IOException -> 0x005c, Exception -> 0x0062 }
        L_0x003d:
            org.w3c.dom.Document r1 = r1.parse(r3)     // Catch:{ DOMException -> 0x004f, SAXException -> 0x0056, IOException -> 0x005c, Exception -> 0x0062 }
            r1.normalize()     // Catch:{ DOMException -> 0x00f8, SAXException -> 0x0056, IOException -> 0x005c, Exception -> 0x0062 }
        L_0x0044:
            if (r1 != 0) goto L_0x0068
            java.lang.String r1 = "MicroMsg.Util"
            java.lang.String r2 = "new Document failed"
            com.tencent.mm.sdk.platformtools.Log.e(r1, r2)
            r1 = r6
            goto L_0x000b
        L_0x004f:
            r1 = move-exception
            r3 = r6
        L_0x0051:
            r1.printStackTrace()
            r1 = r3
            goto L_0x0044
        L_0x0056:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r6
            goto L_0x000b
        L_0x005c:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r6
            goto L_0x000b
        L_0x0062:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r6
            goto L_0x000b
        L_0x0068:
            org.w3c.dom.Element r1 = r1.getDocumentElement()
            if (r1 != 0) goto L_0x0077
            java.lang.String r1 = "MicroMsg.Util"
            java.lang.String r2 = "getDocumentElement failed"
            com.tencent.mm.sdk.platformtools.Log.e(r1, r2)
            r1 = r6
            goto L_0x000b
        L_0x0077:
            if (r10 == 0) goto L_0x00c9
            java.lang.String r3 = r1.getNodeName()
            boolean r3 = r10.equals(r3)
            if (r3 == 0) goto L_0x00c9
            java.lang.String r3 = ""
            a(r2, r3, r1, r7)
        L_0x0088:
            java.util.Set r1 = r2.entrySet()
            java.util.Iterator r3 = r1.iterator()
        L_0x0090:
            boolean r1 = r3.hasNext()
            if (r1 == 0) goto L_0x00f5
            java.lang.Object r9 = r3.next()
            r0 = r9
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            r1 = r0
            java.lang.String r4 = "MicroMsg.Util"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "key="
            r5.<init>(r6)
            java.lang.Object r9 = r1.getKey()
            java.lang.String r9 = (java.lang.String) r9
            java.lang.StringBuilder r5 = r5.append(r9)
            java.lang.String r6 = " value="
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.Object r9 = r1.getValue()
            java.lang.String r9 = (java.lang.String) r9
            java.lang.StringBuilder r1 = r5.append(r9)
            java.lang.String r1 = r1.toString()
            com.tencent.mm.sdk.platformtools.Log.v(r4, r1)
            goto L_0x0090
        L_0x00c9:
            org.w3c.dom.NodeList r1 = r1.getElementsByTagName(r10)
            int r3 = r1.getLength()
            if (r3 > 0) goto L_0x00dd
            java.lang.String r1 = "MicroMsg.Util"
            java.lang.String r2 = "parse item null"
            com.tencent.mm.sdk.platformtools.Log.e(r1, r2)
            r1 = r6
            goto L_0x000b
        L_0x00dd:
            int r3 = r1.getLength()
            r4 = 1
            if (r3 <= r4) goto L_0x00eb
            java.lang.String r3 = "MicroMsg.Util"
            java.lang.String r4 = "parse items more than one"
            com.tencent.mm.sdk.platformtools.Log.w(r3, r4)
        L_0x00eb:
            java.lang.String r3 = ""
            org.w3c.dom.Node r1 = r1.item(r7)
            a(r2, r3, r1, r7)
            goto L_0x0088
        L_0x00f5:
            r1 = r2
            goto L_0x000b
        L_0x00f8:
            r3 = move-exception
            r8 = r3
            r3 = r1
            r1 = r8
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mm.sdk.platformtools.Util.parseXml(java.lang.String, java.lang.String, java.lang.String):java.util.Map");
    }

    public static MediaPlayer playSound(Context context, int i, MediaPlayer.OnCompletionListener onCompletionListener) {
        try {
            AssetFileDescriptor openFd = context.getAssets().openFd(context.getString(i));
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(openFd.getFileDescriptor(), openFd.getStartOffset(), openFd.getLength());
            openFd.close();
            mediaPlayer.prepare();
            mediaPlayer.setLooping(false);
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(onCompletionListener);
            return mediaPlayer;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void playSound(Context context, int i) {
        playSound(context, i, new MediaPlayer.OnCompletionListener() {
            public final void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
            }
        });
    }

    public static String processXml(String str) {
        return (str == null || str.length() == 0) ? str : Build.VERSION.SDK_INT < 8 ? expandEntities(str) : str;
    }

    public static void saveBitmapToImage(Bitmap bitmap, int i, Bitmap.CompressFormat compressFormat, String str, String str2, boolean z) {
        Assert.assertTrue((str == null || str2 == null) ? false : true);
        Log.d("MicroMsg.Util", "saving to " + str + str2);
        File file = new File(str + str2);
        file.createNewFile();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            bitmap.compress(compressFormat, i, fileOutputStream);
            fileOutputStream.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void saveBitmapToImage(Bitmap bitmap, int i, Bitmap.CompressFormat compressFormat, String str, boolean z) {
        Assert.assertTrue(!isNullOrNil(str));
        Log.d("MicroMsg.Util", "saving to " + str);
        File file = new File(str);
        file.createNewFile();
        FileOutputStream fileOutputStream = null;
        try {
            FileOutputStream fileOutputStream2 = new FileOutputStream(file);
            try {
                bitmap.compress(compressFormat, i, fileOutputStream2);
                fileOutputStream2.flush();
                fileOutputStream2.close();
            } catch (FileNotFoundException e) {
                e = e;
                fileOutputStream = fileOutputStream2;
                try {
                    e.printStackTrace();
                    fileOutputStream.close();
                } catch (Throwable th) {
                    th = th;
                    fileOutputStream.close();
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = fileOutputStream2;
                fileOutputStream.close();
                throw th;
            }
        } catch (FileNotFoundException e2) {
            e = e2;
            e.printStackTrace();
            fileOutputStream.close();
        }
    }

    public static long secondsToNow(long j) {
        return (System.currentTimeMillis() / 1000) - j;
    }

    public static void selectPicture(Context context, int i) {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        ((Activity) context).startActivityForResult(Intent.createChooser(intent, null), i);
    }

    public static void shake(Context context, boolean z) {
        Vibrator vibrator = (Vibrator) context.getSystemService("vibrator");
        if (vibrator != null) {
            if (z) {
                vibrator.vibrate(bt, -1);
            } else {
                vibrator.cancel();
            }
        }
    }

    public static int[] splitToIntArray(String str) {
        if (str == null) {
            return null;
        }
        String[] split = str.split(":");
        ArrayList arrayList = new ArrayList();
        for (String str2 : split) {
            if (str2 != null && str2.length() > 0) {
                try {
                    arrayList.add(Integer.valueOf(Integer.valueOf(str2).intValue()));
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("MicroMsg.Util", "invalid port num, ignore");
                }
            }
        }
        int[] iArr = new int[arrayList.size()];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = ((Integer) arrayList.get(i)).intValue();
        }
        return iArr;
    }

    public static List<String> stringsToList(String[] strArr) {
        if (strArr == null || strArr.length == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (String add : strArr) {
            arrayList.add(add);
        }
        return arrayList;
    }

    public static long ticksToNow(long j) {
        return SystemClock.elapsedRealtime() - j;
    }

    public static void transClickToSelect(final View view, final View view2) {
        view.setOnTouchListener(new View.OnTouchListener() {
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                        view2.setSelected(true);
                        break;
                    case 1:
                    case 3:
                    case 4:
                        view2.setSelected(false);
                        break;
                    case 2:
                        view2.setSelected(view.isPressed());
                        break;
                }
                return false;
            }
        });
    }
}
