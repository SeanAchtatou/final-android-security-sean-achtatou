package com.tencent.mm.sdk.platformtools;

public class CharSequences {
    static void a(int i, int i2, int i3) {
        if (i < 0) {
            throw new IndexOutOfBoundsException();
        } else if (i2 < 0) {
            throw new IndexOutOfBoundsException();
        } else if (i2 > i3) {
            throw new IndexOutOfBoundsException();
        } else if (i > i2) {
            throw new IndexOutOfBoundsException();
        }
    }

    public static int compareToIgnoreCase(CharSequence charSequence, CharSequence charSequence2) {
        int i = 0;
        int length = charSequence.length();
        int length2 = charSequence2.length();
        int i2 = length < length2 ? length : length2;
        int i3 = 0;
        while (i3 < i2) {
            int i4 = i3 + 1;
            int i5 = i + 1;
            int lowerCase = Character.toLowerCase(charSequence.charAt(i3)) - Character.toLowerCase(charSequence2.charAt(i));
            if (lowerCase != 0) {
                return lowerCase;
            }
            i = i5;
            i3 = i4;
        }
        return length - length2;
    }

    public static boolean equals(CharSequence charSequence, CharSequence charSequence2) {
        if (charSequence.length() != charSequence2.length()) {
            return false;
        }
        int length = charSequence.length();
        for (int i = 0; i < length; i++) {
            if (charSequence.charAt(i) != charSequence2.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    public static CharSequence forAsciiBytes(final byte[] bArr) {
        return new CharSequence() {
            public final char charAt(int i) {
                return (char) bArr[i];
            }

            public final int length() {
                return bArr.length;
            }

            public final CharSequence subSequence(int i, int i2) {
                return CharSequences.forAsciiBytes(bArr, i, i2);
            }

            public final String toString() {
                return new String(bArr);
            }
        };
    }

    public static CharSequence forAsciiBytes(final byte[] bArr, final int i, final int i2) {
        a(i, i2, bArr.length);
        return new CharSequence() {
            public final char charAt(int i) {
                return (char) bArr[i + i];
            }

            public final int length() {
                return i2 - i;
            }

            public final CharSequence subSequence(int i, int i2) {
                int i3 = i - i;
                int i4 = i2 - i;
                CharSequences.a(i3, i4, length());
                return CharSequences.forAsciiBytes(bArr, i3, i4);
            }

            public final String toString() {
                return new String(bArr, i, length());
            }
        };
    }
}
