package com.tencent.mm.algorithm;

import java.util.HashMap;
import java.util.Map;

public class LRUMap<K, O> {
    private Map<K, LRUMap<K, O>.TimeVal<O>> c;
    private int d;
    private int e;
    private PreRemoveCallback<K, O> f;

    public interface OnClearListener<K, O> {
        void onClear(K k, O o);
    }

    public interface PreRemoveCallback<K, O> {
        void preRemoveCallback(K k, O o);
    }

    public class TimeVal<OO> {
        public OO obj;
        public Long t;

        public TimeVal(OO oo) {
            this.obj = oo;
            UpTime();
        }

        public void UpTime() {
            this.t = Long.valueOf(System.currentTimeMillis());
        }
    }

    public LRUMap(int i) {
        this(i, null);
    }

    public LRUMap(int i, PreRemoveCallback<K, O> preRemoveCallback) {
        this.c = null;
        this.f = null;
        this.d = i;
        this.e = 0;
        this.f = preRemoveCallback;
        this.c = new HashMap();
    }

    public boolean check(K k) {
        return this.c.containsKey(k);
    }

    public boolean checkAndUpTime(K k) {
        if (!this.c.containsKey(k)) {
            return false;
        }
        this.c.get(k).UpTime();
        return true;
    }

    public void clear() {
        this.c.clear();
    }

    public void clear(OnClearListener<K, O> onClearListener) {
        if (this.c != null) {
            if (onClearListener != null) {
                for (Map.Entry next : this.c.entrySet()) {
                    onClearListener.onClear(next.getKey(), ((TimeVal) next.getValue()).obj);
                }
            }
            this.c.clear();
        }
    }

    public O get(K k) {
        TimeVal timeVal = this.c.get(k);
        if (timeVal == null) {
            return null;
        }
        return timeVal.obj;
    }

    public O getAndUptime(K k) {
        TimeVal timeVal = this.c.get(k);
        if (timeVal == null) {
            return null;
        }
        this.c.get(k).UpTime();
        return timeVal.obj;
    }

    public void remove(K k) {
        if (this.c.containsKey(k)) {
            if (this.f != null) {
                this.f.preRemoveCallback(k, this.c.get(k).obj);
            }
            this.c.remove(k);
        }
    }

    public void setMaxSize(int i) {
        if (i > 0) {
            this.d = i;
        }
    }

    public void setPerDeleteSize(int i) {
        if (i > 0) {
            this.e = i;
        }
    }

    public int size() {
        return this.c.size();
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [OO, java.lang.Object, O] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void update(K r4, O r5) {
        /*
            r3 = this;
            java.util.Map<K, com.tencent.mm.algorithm.LRUMap<K, O>$TimeVal<O>> r0 = r3.c
            java.lang.Object r0 = r0.get(r4)
            com.tencent.mm.algorithm.LRUMap$TimeVal r0 = (com.tencent.mm.algorithm.LRUMap.TimeVal) r0
            if (r0 != 0) goto L_0x005b
            com.tencent.mm.algorithm.LRUMap$TimeVal r0 = new com.tencent.mm.algorithm.LRUMap$TimeVal
            r0.<init>(r5)
            java.util.Map<K, com.tencent.mm.algorithm.LRUMap<K, O>$TimeVal<O>> r1 = r3.c
            r1.put(r4, r0)
            java.util.Map<K, com.tencent.mm.algorithm.LRUMap<K, O>$TimeVal<O>> r0 = r3.c
            int r0 = r0.size()
            int r1 = r3.d
            if (r0 <= r1) goto L_0x0057
            java.util.ArrayList r0 = new java.util.ArrayList
            java.util.Map<K, com.tencent.mm.algorithm.LRUMap<K, O>$TimeVal<O>> r1 = r3.c
            java.util.Set r1 = r1.entrySet()
            r0.<init>(r1)
            com.tencent.mm.algorithm.LRUMap$1 r1 = new com.tencent.mm.algorithm.LRUMap$1
            r1.<init>()
            java.util.Collections.sort(r0, r1)
            int r1 = r3.e
            if (r1 > 0) goto L_0x0058
            int r1 = r3.d
            int r1 = r1 / 10
            if (r1 > 0) goto L_0x003c
            r1 = 1
        L_0x003c:
            java.util.Iterator r2 = r0.iterator()
        L_0x0040:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0057
            java.lang.Object r0 = r2.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r0 = r0.getKey()
            r3.remove(r0)
            int r0 = r1 + -1
            if (r0 > 0) goto L_0x0071
        L_0x0057:
            return
        L_0x0058:
            int r1 = r3.e
            goto L_0x003c
        L_0x005b:
            java.util.Map<K, com.tencent.mm.algorithm.LRUMap<K, O>$TimeVal<O>> r0 = r3.c
            java.lang.Object r0 = r0.get(r4)
            com.tencent.mm.algorithm.LRUMap$TimeVal r0 = (com.tencent.mm.algorithm.LRUMap.TimeVal) r0
            r0.UpTime()
            java.util.Map<K, com.tencent.mm.algorithm.LRUMap<K, O>$TimeVal<O>> r0 = r3.c
            java.lang.Object r3 = r0.get(r4)
            com.tencent.mm.algorithm.LRUMap$TimeVal r3 = (com.tencent.mm.algorithm.LRUMap.TimeVal) r3
            r3.obj = r5
            goto L_0x0057
        L_0x0071:
            r1 = r0
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mm.algorithm.LRUMap.update(java.lang.Object, java.lang.Object):void");
    }
}
