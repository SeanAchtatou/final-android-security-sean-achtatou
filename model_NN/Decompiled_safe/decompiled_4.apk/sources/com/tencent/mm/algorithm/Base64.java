package com.tencent.mm.algorithm;

import android.support.v4.view.accessibility.AccessibilityEventCompat;
import java.util.Arrays;

public class Base64 {
    private static final char[] a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();
    private static final int[] b;

    static {
        int[] iArr = new int[AccessibilityEventCompat.TYPE_VIEW_HOVER_EXIT];
        b = iArr;
        Arrays.fill(iArr, -1);
        int length = a.length;
        for (int i = 0; i < length; i++) {
            b[a[i]] = i;
        }
        b[61] = 0;
    }

    public static final byte[] decode(String str) {
        int length = str != null ? str.length() : 0;
        if (length == 0) {
            return new byte[0];
        }
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (b[str.charAt(i2)] < 0) {
                i++;
            }
        }
        if ((length - i) % 4 != 0) {
            return null;
        }
        int i3 = length;
        int i4 = 0;
        while (i3 > 1) {
            i3--;
            if (b[str.charAt(i3)] > 0) {
                break;
            } else if (str.charAt(i3) == '=') {
                i4++;
            }
        }
        int i5 = (((length - i) * 6) >> 3) - i4;
        byte[] bArr = new byte[i5];
        int i6 = 0;
        int i7 = 0;
        while (i6 < i5) {
            int i8 = 0;
            int i9 = i7;
            int i10 = 0;
            while (i10 < 4) {
                int i11 = i9 + 1;
                int i12 = b[str.charAt(i9)];
                if (i12 >= 0) {
                    i8 |= i12 << (18 - (i10 * 6));
                } else {
                    i10--;
                }
                i10++;
                i9 = i11;
            }
            int i13 = i6 + 1;
            bArr[i6] = (byte) (i8 >> 16);
            if (i13 < i5) {
                i6 = i13 + 1;
                bArr[i13] = (byte) (i8 >> 8);
                if (i6 < i5) {
                    i13 = i6 + 1;
                    bArr[i6] = (byte) i8;
                } else {
                    i7 = i9;
                }
            }
            i6 = i13;
            i7 = i9;
        }
        return bArr;
    }

    public static final byte[] decode(byte[] bArr) {
        int i = 0;
        for (byte b2 : bArr) {
            if (b[b2 & 255] < 0) {
                i++;
            }
        }
        if ((r0 - i) % 4 != 0) {
            return null;
        }
        int i2 = r0;
        int i3 = 0;
        while (i2 > 1) {
            i2--;
            if (b[bArr[i2] & 255] > 0) {
                break;
            } else if (bArr[i2] == 61) {
                i3++;
            }
        }
        int i4 = (((r0 - i) * 6) >> 3) - i3;
        byte[] bArr2 = new byte[i4];
        int i5 = 0;
        int i6 = 0;
        while (i5 < i4) {
            int i7 = 0;
            int i8 = i6;
            int i9 = 0;
            while (i9 < 4) {
                int i10 = i8 + 1;
                int i11 = b[bArr[i8] & 255];
                if (i11 >= 0) {
                    i7 |= i11 << (18 - (i9 * 6));
                } else {
                    i9--;
                }
                i9++;
                i8 = i10;
            }
            int i12 = i5 + 1;
            bArr2[i5] = (byte) (i7 >> 16);
            if (i12 < i4) {
                i5 = i12 + 1;
                bArr2[i12] = (byte) (i7 >> 8);
                if (i5 < i4) {
                    i12 = i5 + 1;
                    bArr2[i5] = (byte) i7;
                } else {
                    i6 = i8;
                }
            }
            i5 = i12;
            i6 = i8;
        }
        return bArr2;
    }

    public static final byte[] decode(char[] cArr) {
        int length = cArr != null ? cArr.length : 0;
        if (length == 0) {
            return new byte[0];
        }
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            if (b[cArr[i2]] < 0) {
                i++;
            }
        }
        if ((length - i) % 4 != 0) {
            return null;
        }
        int i3 = length;
        int i4 = 0;
        while (i3 > 1) {
            i3--;
            if (b[cArr[i3]] > 0) {
                break;
            } else if (cArr[i3] == '=') {
                i4++;
            }
        }
        int i5 = (((length - i) * 6) >> 3) - i4;
        byte[] bArr = new byte[i5];
        int i6 = 0;
        int i7 = 0;
        while (i6 < i5) {
            int i8 = 0;
            int i9 = i7;
            int i10 = 0;
            while (i10 < 4) {
                int i11 = i9 + 1;
                int i12 = b[cArr[i9]];
                if (i12 >= 0) {
                    i8 |= i12 << (18 - (i10 * 6));
                } else {
                    i10--;
                }
                i10++;
                i9 = i11;
            }
            int i13 = i6 + 1;
            bArr[i6] = (byte) (i8 >> 16);
            if (i13 < i5) {
                i6 = i13 + 1;
                bArr[i13] = (byte) (i8 >> 8);
                if (i6 < i5) {
                    i13 = i6 + 1;
                    bArr[i6] = (byte) i8;
                } else {
                    i7 = i9;
                }
            }
            i6 = i13;
            i7 = i9;
        }
        return bArr;
    }

    public static final byte[] decodeFast(String str) {
        int i;
        int i2;
        int length = str.length();
        if (length == 0) {
            return new byte[0];
        }
        int i3 = length - 1;
        int i4 = 0;
        while (i4 < i3 && b[str.charAt(i4) & 255] < 0) {
            i4++;
        }
        while (i3 > 0 && b[str.charAt(i3) & 255] < 0) {
            i3--;
        }
        int i5 = str.charAt(i3) == '=' ? str.charAt(i3 - 1) == '=' ? 2 : 1 : 0;
        int i6 = (i3 - i4) + 1;
        if (length > 76) {
            i = (str.charAt(76) == 13 ? i6 / 78 : 0) << 1;
        } else {
            i = 0;
        }
        int i7 = (((i6 - i) * 6) >> 3) - i5;
        byte[] bArr = new byte[i7];
        int i8 = (i7 / 3) * 3;
        int i9 = 0;
        int i10 = i4;
        int i11 = 0;
        while (i9 < i8) {
            int i12 = i10 + 1;
            int i13 = i12 + 1;
            int i14 = i13 + 1;
            int i15 = i14 + 1;
            int i16 = (b[str.charAt(i10)] << 18) | (b[str.charAt(i12)] << 12) | (b[str.charAt(i13)] << 6) | b[str.charAt(i14)];
            int i17 = i9 + 1;
            bArr[i9] = (byte) (i16 >> 16);
            int i18 = i17 + 1;
            bArr[i17] = (byte) (i16 >> 8);
            int i19 = i18 + 1;
            bArr[i18] = (byte) i16;
            if (i <= 0 || (i11 = i11 + 1) != 19) {
                i2 = i15;
            } else {
                i2 = i15 + 2;
                i11 = 0;
            }
            i10 = i2;
            i9 = i19;
        }
        if (i9 < i7) {
            int i20 = 0;
            int i21 = 0;
            for (int i22 = i10; i22 <= i3 - i5; i22++) {
                i21 |= b[str.charAt(i22)] << (18 - (i20 * 6));
                i20++;
            }
            int i23 = 16;
            for (int i24 = i9; i24 < i7; i24++) {
                bArr[i24] = (byte) (i21 >> i23);
                i23 -= 8;
            }
        }
        return bArr;
    }

    public static final byte[] decodeFast(byte[] bArr) {
        int i;
        int i2;
        int length = bArr.length;
        if (length == 0) {
            return new byte[0];
        }
        int i3 = length - 1;
        int i4 = 0;
        while (i4 < i3 && b[bArr[i4] & 255] < 0) {
            i4++;
        }
        while (i3 > 0 && b[bArr[i3] & 255] < 0) {
            i3--;
        }
        int i5 = bArr[i3] == 61 ? bArr[i3 - 1] == 61 ? 2 : 1 : 0;
        int i6 = (i3 - i4) + 1;
        if (length > 76) {
            i = (bArr[76] == 13 ? i6 / 78 : 0) << 1;
        } else {
            i = 0;
        }
        int i7 = (((i6 - i) * 6) >> 3) - i5;
        byte[] bArr2 = new byte[i7];
        int i8 = (i7 / 3) * 3;
        int i9 = 0;
        int i10 = i4;
        int i11 = 0;
        while (i9 < i8) {
            int i12 = i10 + 1;
            int i13 = i12 + 1;
            int i14 = i13 + 1;
            int i15 = i14 + 1;
            int i16 = (b[bArr[i10]] << 18) | (b[bArr[i12]] << 12) | (b[bArr[i13]] << 6) | b[bArr[i14]];
            int i17 = i9 + 1;
            bArr2[i9] = (byte) (i16 >> 16);
            int i18 = i17 + 1;
            bArr2[i17] = (byte) (i16 >> 8);
            int i19 = i18 + 1;
            bArr2[i18] = (byte) i16;
            if (i <= 0 || (i11 = i11 + 1) != 19) {
                i2 = i15;
            } else {
                i2 = i15 + 2;
                i11 = 0;
            }
            i10 = i2;
            i9 = i19;
        }
        if (i9 < i7) {
            int i20 = 0;
            int i21 = 0;
            for (int i22 = i10; i22 <= i3 - i5; i22++) {
                i21 |= b[bArr[i22]] << (18 - (i20 * 6));
                i20++;
            }
            int i23 = 16;
            for (int i24 = i9; i24 < i7; i24++) {
                bArr2[i24] = (byte) (i21 >> i23);
                i23 -= 8;
            }
        }
        return bArr2;
    }

    public static final byte[] decodeFast(char[] cArr) {
        int i;
        int i2;
        int length = cArr.length;
        if (length == 0) {
            return new byte[0];
        }
        int i3 = length - 1;
        int i4 = 0;
        while (i4 < i3 && b[cArr[i4]] < 0) {
            i4++;
        }
        while (i3 > 0 && b[cArr[i3]] < 0) {
            i3--;
        }
        int i5 = cArr[i3] == '=' ? cArr[i3 - 1] == '=' ? 2 : 1 : 0;
        int i6 = (i3 - i4) + 1;
        if (length > 76) {
            i = (cArr[76] == 13 ? i6 / 78 : 0) << 1;
        } else {
            i = 0;
        }
        int i7 = (((i6 - i) * 6) >> 3) - i5;
        byte[] bArr = new byte[i7];
        int i8 = (i7 / 3) * 3;
        int i9 = 0;
        int i10 = i4;
        int i11 = 0;
        while (i9 < i8) {
            int i12 = i10 + 1;
            int i13 = i12 + 1;
            int i14 = i13 + 1;
            int i15 = i14 + 1;
            int i16 = (b[cArr[i10]] << 18) | (b[cArr[i12]] << 12) | (b[cArr[i13]] << 6) | b[cArr[i14]];
            int i17 = i9 + 1;
            bArr[i9] = (byte) (i16 >> 16);
            int i18 = i17 + 1;
            bArr[i17] = (byte) (i16 >> 8);
            int i19 = i18 + 1;
            bArr[i18] = (byte) i16;
            if (i <= 0 || (i11 = i11 + 1) != 19) {
                i2 = i15;
            } else {
                i2 = i15 + 2;
                i11 = 0;
            }
            i10 = i2;
            i9 = i19;
        }
        if (i9 < i7) {
            int i20 = 0;
            int i21 = 0;
            for (int i22 = i10; i22 <= i3 - i5; i22++) {
                i21 |= b[cArr[i22]] << (18 - (i20 * 6));
                i20++;
            }
            int i23 = 16;
            for (int i24 = i9; i24 < i7; i24++) {
                bArr[i24] = (byte) (i21 >> i23);
                i23 -= 8;
            }
        }
        return bArr;
    }

    public static final byte[] encodeToByte(byte[] bArr, boolean z) {
        int length = bArr != null ? bArr.length : 0;
        if (length == 0) {
            return new byte[0];
        }
        int i = (length / 3) * 3;
        int i2 = (((length - 1) / 3) + 1) << 2;
        int i3 = i2 + (z ? ((i2 - 1) / 76) << 1 : 0);
        byte[] bArr2 = new byte[i3];
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        while (i5 < i) {
            int i7 = i5 + 1;
            int i8 = i7 + 1;
            byte b2 = ((bArr[i5] & 255) << 16) | ((bArr[i7] & 255) << 8);
            int i9 = i8 + 1;
            byte b3 = b2 | (bArr[i8] & 255);
            int i10 = i4 + 1;
            bArr2[i4] = (byte) a[(b3 >>> 18) & 63];
            int i11 = i10 + 1;
            bArr2[i10] = (byte) a[(b3 >>> 12) & 63];
            int i12 = i11 + 1;
            bArr2[i11] = (byte) a[(b3 >>> 6) & 63];
            i4 = i12 + 1;
            bArr2[i12] = (byte) a[b3 & 63];
            if (z && (i6 = i6 + 1) == 19 && i4 < i3 - 2) {
                int i13 = i4 + 1;
                bArr2[i4] = 13;
                i4 = i13 + 1;
                bArr2[i13] = 10;
                i6 = 0;
            }
            i5 = i9;
        }
        int i14 = length - i;
        if (i14 > 0) {
            int i15 = (i14 == 2 ? (bArr[length - 1] & 255) << 2 : 0) | ((bArr[i] & 255) << 10);
            bArr2[i3 - 4] = (byte) a[i15 >> 12];
            bArr2[i3 - 3] = (byte) a[(i15 >>> 6) & 63];
            bArr2[i3 - 2] = i14 == 2 ? (byte) a[i15 & 63] : 61;
            bArr2[i3 - 1] = 61;
        }
        return bArr2;
    }

    public static final char[] encodeToChar(byte[] bArr, boolean z) {
        int length = bArr != null ? bArr.length : 0;
        if (length == 0) {
            return new char[0];
        }
        int i = (length / 3) * 3;
        int i2 = (((length - 1) / 3) + 1) << 2;
        int i3 = i2 + (z ? ((i2 - 1) / 76) << 1 : 0);
        char[] cArr = new char[i3];
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        while (i5 < i) {
            int i7 = i5 + 1;
            int i8 = i7 + 1;
            byte b2 = ((bArr[i5] & 255) << 16) | ((bArr[i7] & 255) << 8);
            int i9 = i8 + 1;
            byte b3 = b2 | (bArr[i8] & 255);
            int i10 = i4 + 1;
            cArr[i4] = a[(b3 >>> 18) & 63];
            int i11 = i10 + 1;
            cArr[i10] = a[(b3 >>> 12) & 63];
            int i12 = i11 + 1;
            cArr[i11] = a[(b3 >>> 6) & 63];
            i4 = i12 + 1;
            cArr[i12] = a[b3 & 63];
            if (z && (i6 = i6 + 1) == 19 && i4 < i3 - 2) {
                int i13 = i4 + 1;
                cArr[i4] = 13;
                i4 = i13 + 1;
                cArr[i13] = 10;
                i6 = 0;
            }
            i5 = i9;
        }
        int i14 = length - i;
        if (i14 > 0) {
            int i15 = (i14 == 2 ? (bArr[length - 1] & 255) << 2 : 0) | ((bArr[i] & 255) << 10);
            cArr[i3 - 4] = a[i15 >> 12];
            cArr[i3 - 3] = a[(i15 >>> 6) & 63];
            cArr[i3 - 2] = i14 == 2 ? a[i15 & 63] : '=';
            cArr[i3 - 1] = '=';
        }
        return cArr;
    }

    public static final String encodeToString(byte[] bArr, boolean z) {
        return new String(encodeToChar(bArr, z));
    }
}
