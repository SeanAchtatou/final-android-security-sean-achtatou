package com.mobcent.update.android.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.io.File;

public class ApkUtil {
    public static void installApk(Context context, String url) {
        if (url.contains(".apk")) {
            File file = new File(String.valueOf(MCLibIOUtil.getUpdateFile(context).getAbsolutePath()) + MCLibIOUtil.FS + url);
            Intent intent = new Intent();
            intent.addFlags(268435456);
            intent.setAction("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            context.startActivity(intent);
        }
    }

    public static void launchApk(Context context, String packageName) {
        context.startActivity(context.getPackageManager().getLaunchIntentForPackage(packageName));
    }
}
