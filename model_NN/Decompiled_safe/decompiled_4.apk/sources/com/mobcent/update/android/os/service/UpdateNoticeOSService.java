package com.mobcent.update.android.os.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.mobcent.update.android.constant.MCUpdateConstant;
import com.mobcent.update.android.model.UpdateModel;
import com.mobcent.update.android.service.UpdateService;
import com.mobcent.update.android.service.impl.UpdateServiceImpl;

public class UpdateNoticeOSService extends Service {
    private static UpdateCallBack updateCallBack;
    private boolean isUpdate;
    private UpdateService service;
    /* access modifiers changed from: private */
    public UpdateModel updateModel;

    public interface UpdateCallBack {
        void updateResult(boolean z);
    }

    public static void setUpdateCallBack(UpdateCallBack updateCallBack2) {
        updateCallBack = updateCallBack2;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        new Thread(new Runnable() {
            public void run() {
                UpdateNoticeOSService.this.updateModel = UpdateNoticeOSService.this.getUpdateModel();
                if (UpdateNoticeOSService.this.updateModel != null) {
                    try {
                        Thread.sleep(900);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    UpdateNoticeOSService.this.boardcastToReceiver(UpdateNoticeOSService.this.updateModel);
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public UpdateModel getUpdateModel() {
        if (this.service == null) {
            this.service = new UpdateServiceImpl(this);
        }
        this.updateModel = this.service.getUpdateInfo(this);
        if (this.updateModel != null) {
            this.isUpdate = true;
            if (updateCallBack != null) {
                updateCallBack.updateResult(this.isUpdate);
            }
            return this.updateModel;
        }
        this.isUpdate = false;
        if (updateCallBack != null) {
            updateCallBack.updateResult(this.isUpdate);
            stopSelf();
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void boardcastToReceiver(UpdateModel updateModel2) {
        Intent intent = new Intent(String.valueOf(getPackageName()) + MCUpdateConstant.UPDATE_NOTICE_SERVER_MSG);
        if (updateModel2 != null) {
            intent.putExtra(MCUpdateConstant.RETURN_UPDATE_NOTICE_MODEL, updateModel2);
        }
        sendBroadcast(intent);
    }
}
