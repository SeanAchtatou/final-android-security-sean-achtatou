package com.mobcent.update.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.update.android.db.constant.UpdateDBConstant;
import com.mobcent.update.android.model.UpdateModel;

public class UpdateStatusDBUtil implements UpdateDBConstant {
    private UpdateDBHelper updateDBHelper;

    public UpdateStatusDBUtil(Context context) {
        this.updateDBHelper = new UpdateDBHelper(context);
    }

    public void saveStatus(UpdateModel model, int status) {
        SQLiteDatabase db = null;
        try {
            db = this.updateDBHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(UpdateDBConstant.UPDATE_ID, Integer.valueOf(model.getId()));
            values.put("update_status", Integer.valueOf(status));
            db.insertOrThrow("update_status", null, values);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void updateStatus(UpdateModel model, int status) {
        SQLiteDatabase db = null;
        try {
            db = this.updateDBHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(UpdateDBConstant.UPDATE_ID, Integer.valueOf(model.getId()));
            values.put("update_status", Integer.valueOf(status));
            db.update("update_status", values, "updateId = " + model.getId(), null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public int getUpdateStatus(UpdateModel updateModel) {
        int status = 0;
        try {
            SQLiteDatabase db = this.updateDBHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select *  from update_status where updateId=?", new String[]{new StringBuilder(String.valueOf(updateModel.getId())).toString()});
            while (cursor.moveToNext()) {
                status = cursor.getInt(1);
            }
            cursor.close();
            db.close();
            return status;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public UpdateModel getUpdateModel(UpdateModel updateModel) {
        Exception e;
        SQLiteDatabase db = null;
        UpdateModel model = null;
        try {
            db = this.updateDBHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select *  from update_status where updateId=?", new String[]{new StringBuilder(String.valueOf(updateModel.getId())).toString()});
            while (true) {
                try {
                    UpdateModel model2 = model;
                    if (!cursor.moveToNext()) {
                        cursor.close();
                        db.close();
                        return model2;
                    }
                    model = new UpdateModel();
                    model.setId(cursor.getInt(0));
                    model.setStatus(cursor.getInt(1));
                } catch (Exception e2) {
                    e = e2;
                    try {
                        e.printStackTrace();
                        db.close();
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        db.close();
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    db.close();
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
        }
    }

    public boolean saveOrUpdate(UpdateModel model, int status) {
        SQLiteDatabase db = null;
        try {
            db = this.updateDBHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(UpdateDBConstant.UPDATE_ID, Integer.valueOf(model.getId()));
            values.put("update_status", Integer.valueOf(status));
            if (!isRowExisted(db, "update_status", UpdateDBConstant.UPDATE_ID, (long) model.getId())) {
                db.insertOrThrow("update_status", null, values);
            } else {
                db.update("update_status", values, "updateId = '" + model.getId() + "'", null);
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean isRowExisted(SQLiteDatabase database, String table, String column, long id) {
        try {
            if (database.query(table, null, String.valueOf(column) + "=" + id, null, null, null, null).getCount() > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
