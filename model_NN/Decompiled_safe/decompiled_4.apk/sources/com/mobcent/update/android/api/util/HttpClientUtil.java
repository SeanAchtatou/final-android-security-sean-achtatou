package com.mobcent.update.android.api.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiManager;
import com.mobcent.update.android.util.MCLogUtil;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class HttpClientUtil {
    private static final int SET_CONNECTION_TIMEOUT = 50000;
    private static final int SET_SOCKET_TIMEOUT = 200000;
    private static final String TAG = "updateLog";

    /* JADX INFO: Multiple debug info for r11v1 org.apache.http.client.HttpClient: [D('context' android.content.Context), D('client' org.apache.http.client.HttpClient)] */
    /* JADX INFO: Multiple debug info for r11v6 org.apache.http.StatusLine: [D('client' org.apache.http.client.HttpClient), D('status' org.apache.http.StatusLine)] */
    /* JADX INFO: Multiple debug info for r11v7 int: [D('statusCode' int), D('status' org.apache.http.StatusLine)] */
    /* JADX INFO: Multiple debug info for r10v13 java.lang.String: [D('response' org.apache.http.HttpResponse), D('result' java.lang.String)] */
    public static String doPostRequest(String urlString, HashMap<String, String> params, Context context) {
        String result;
        HttpPost httpPost;
        String tempURL;
        String str;
        String tempURL2 = String.valueOf(urlString) + "?";
        HttpClient client = getNewHttpClient(context);
        HttpPost httpPost2 = new HttpPost(urlString);
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            if (params != null && !params.isEmpty()) {
                for (String key : params.keySet()) {
                    nameValuePairs.add(new BasicNameValuePair(key, params.get(key)));
                    tempURL2 = String.valueOf(tempURL2) + key + "=" + params.get(key) + "&";
                }
            }
            MCLogUtil.i(TAG, "tempURL = " + tempURL2);
            httpPost2.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
            HttpPost httpPost3 = httpPost2;
            try {
                HttpResponse response = client.execute(httpPost3);
                if (response.getStatusLine().getStatusCode() != 200) {
                    return "connection_fail";
                }
                String result2 = read(response);
                try {
                    MCLogUtil.i(TAG, "result = " + result2);
                    if (result2 == null) {
                        str = "{}";
                    } else {
                        str = result2;
                    }
                    return str;
                } catch (Exception e) {
                    tempURL = tempURL2;
                    HttpPost httpPost4 = httpPost3;
                    request = e;
                    result = result2;
                    httpPost = httpPost4;
                    request.printStackTrace();
                    return "connection_fail";
                }
            } catch (Exception e2) {
                tempURL = tempURL2;
                result = "";
                HttpUriRequest httpUriRequest = e2;
                httpPost = httpPost3;
                request = httpUriRequest;
                request.printStackTrace();
                return "connection_fail";
            }
        } catch (Exception e3) {
            request = e3;
            httpPost = null;
            tempURL = tempURL2;
            result = "";
            request.printStackTrace();
            return "connection_fail";
        }
    }

    public static synchronized byte[] getFileInByte(String url, Context context) {
        byte[] bArr;
        synchronized (HttpClientUtil.class) {
            try {
                byte[] imgData = null;
                try {
                    HttpURLConnection connection = null;
                    InputStream is = null;
                    try {
                        connection = getNewHttpURLConnection(new URL(url), context);
                        connection.setDoInput(true);
                        connection.connect();
                        is = connection.getInputStream();
                        int length = connection.getContentLength();
                        if (length != -1) {
                            imgData = new byte[length];
                            byte[] temp = new byte[512];
                            int destPos = 0;
                            while (true) {
                                int readLen = is.read(temp);
                                if (readLen <= 0) {
                                    break;
                                }
                                System.arraycopy(temp, 0, imgData, destPos, readLen);
                                destPos += readLen;
                            }
                            byte[] bArr2 = null;
                        }
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (connection != null) {
                            connection.disconnect();
                        }
                        bArr = imgData;
                    } catch (IOException e2) {
                        if (is != null) {
                            is.close();
                        }
                        if (connection != null) {
                            connection.disconnect();
                        }
                        bArr = null;
                        return bArr;
                    } catch (IOException e3) {
                        e3.printStackTrace();
                        bArr = null;
                        return bArr;
                    } catch (Throwable th) {
                        th = th;
                    }
                } catch (Exception e4) {
                    bArr = null;
                }
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    /* JADX INFO: Multiple debug info for r2v5 java.io.IOException: [D('e' java.net.MalformedURLException), D('e' java.io.IOException)] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ae A[SYNTHETIC, Splitter:B:47:0x00ae] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00b6 A[Catch:{ IOException -> 0x00bf }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00bb A[Catch:{ IOException -> 0x00bf }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00c8 A[SYNTHETIC, Splitter:B:57:0x00c8] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00d0 A[Catch:{ IOException -> 0x00d9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00d5 A[Catch:{ IOException -> 0x00d9 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void downloadFile(java.lang.String r12, java.io.File r13, android.content.Context r14) {
        /*
            r6 = 0
            java.lang.String r9 = "updateLog"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0056 }
            java.lang.String r11 = "Image url is "
            r10.<init>(r11)     // Catch:{ Exception -> 0x0056 }
            java.lang.StringBuilder r10 = r10.append(r12)     // Catch:{ Exception -> 0x0056 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x0056 }
            com.mobcent.update.android.util.MCLogUtil.i(r9, r10)     // Catch:{ Exception -> 0x0056 }
            java.net.URL r7 = new java.net.URL     // Catch:{ Exception -> 0x0056 }
            r7.<init>(r12)     // Catch:{ Exception -> 0x0056 }
            r1 = 0
            r5 = 0
            r3 = 0
            java.net.HttpURLConnection r1 = getNewHttpURLConnection(r7, r14)     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            r9 = 1
            r1.setDoInput(r9)     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            r1.connect()     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            java.io.InputStream r5 = r1.getInputStream()     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            boolean r9 = r13.createNewFile()     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            if (r9 == 0) goto L_0x0042
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            r4.<init>(r13)     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            r9 = 256(0x100, float:3.59E-43)
            byte[] r0 = new byte[r9]     // Catch:{ MalformedURLException -> 0x005e, IOException -> 0x00e9, all -> 0x00e6 }
        L_0x003b:
            int r8 = r5.read(r0)     // Catch:{ MalformedURLException -> 0x005e, IOException -> 0x00e9, all -> 0x00e6 }
            if (r8 > 0) goto L_0x0059
            r3 = r4
        L_0x0042:
            if (r3 == 0) goto L_0x004a
            r3.flush()     // Catch:{ IOException -> 0x00df }
            r3.close()     // Catch:{ IOException -> 0x00df }
        L_0x004a:
            if (r5 == 0) goto L_0x004f
            r5.close()     // Catch:{ IOException -> 0x00df }
        L_0x004f:
            if (r1 == 0) goto L_0x0054
            r1.disconnect()     // Catch:{ IOException -> 0x00df }
        L_0x0054:
            r6 = r7
        L_0x0055:
            return
        L_0x0056:
            r9 = move-exception
            r2 = r9
            goto L_0x0055
        L_0x0059:
            r9 = 0
            r4.write(r0, r9, r8)     // Catch:{ MalformedURLException -> 0x005e, IOException -> 0x00e9, all -> 0x00e6 }
            goto L_0x003b
        L_0x005e:
            r9 = move-exception
            r2 = r9
            r3 = r4
        L_0x0061:
            java.lang.String r9 = "updateLog"
            java.lang.String r10 = "URL is format error"
            com.mobcent.update.android.util.MCLogUtil.i(r9, r10)     // Catch:{ all -> 0x00c5 }
            if (r3 == 0) goto L_0x0070
            r3.flush()     // Catch:{ IOException -> 0x007b }
            r3.close()     // Catch:{ IOException -> 0x007b }
        L_0x0070:
            if (r5 == 0) goto L_0x0075
            r5.close()     // Catch:{ IOException -> 0x007b }
        L_0x0075:
            if (r1 == 0) goto L_0x0054
            r1.disconnect()     // Catch:{ IOException -> 0x007b }
            goto L_0x0054
        L_0x007b:
            r9 = move-exception
            r2 = r9
            r2.printStackTrace()
            goto L_0x0054
        L_0x0081:
            r9 = move-exception
            r2 = r9
        L_0x0083:
            java.lang.String r9 = "updateLog"
            java.lang.String r10 = "IO error when download file"
            com.mobcent.update.android.util.MCLogUtil.i(r9, r10)     // Catch:{ all -> 0x00c5 }
            java.lang.String r9 = "updateLog"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c5 }
            java.lang.String r11 = "The URL is "
            r10.<init>(r11)     // Catch:{ all -> 0x00c5 }
            java.lang.StringBuilder r10 = r10.append(r12)     // Catch:{ all -> 0x00c5 }
            java.lang.String r11 = ";the file name "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x00c5 }
            java.lang.String r11 = r13.getName()     // Catch:{ all -> 0x00c5 }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x00c5 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x00c5 }
            com.mobcent.update.android.util.MCLogUtil.i(r9, r10)     // Catch:{ all -> 0x00c5 }
            if (r3 == 0) goto L_0x00b4
            r3.flush()     // Catch:{ IOException -> 0x00bf }
            r3.close()     // Catch:{ IOException -> 0x00bf }
        L_0x00b4:
            if (r5 == 0) goto L_0x00b9
            r5.close()     // Catch:{ IOException -> 0x00bf }
        L_0x00b9:
            if (r1 == 0) goto L_0x0054
            r1.disconnect()     // Catch:{ IOException -> 0x00bf }
            goto L_0x0054
        L_0x00bf:
            r9 = move-exception
            r2 = r9
            r2.printStackTrace()
            goto L_0x0054
        L_0x00c5:
            r9 = move-exception
        L_0x00c6:
            if (r3 == 0) goto L_0x00ce
            r3.flush()     // Catch:{ IOException -> 0x00d9 }
            r3.close()     // Catch:{ IOException -> 0x00d9 }
        L_0x00ce:
            if (r5 == 0) goto L_0x00d3
            r5.close()     // Catch:{ IOException -> 0x00d9 }
        L_0x00d3:
            if (r1 == 0) goto L_0x00d8
            r1.disconnect()     // Catch:{ IOException -> 0x00d9 }
        L_0x00d8:
            throw r9
        L_0x00d9:
            r10 = move-exception
            r2 = r10
            r2.printStackTrace()
            goto L_0x00d8
        L_0x00df:
            r9 = move-exception
            r2 = r9
            r2.printStackTrace()
            goto L_0x0054
        L_0x00e6:
            r9 = move-exception
            r3 = r4
            goto L_0x00c6
        L_0x00e9:
            r9 = move-exception
            r2 = r9
            r3 = r4
            goto L_0x0083
        L_0x00ed:
            r9 = move-exception
            r2 = r9
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.update.android.api.util.HttpClientUtil.downloadFile(java.lang.String, java.io.File, android.content.Context):void");
    }

    /* JADX INFO: Multiple debug info for r14v24 java.net.HttpURLConnection: [D('context' android.content.Context), D('con' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r12v68 byte[]: [D('bufferSize' int), D('buffer' byte[])] */
    /* JADX INFO: Multiple debug info for r0v39 java.lang.String: [D('jsonStr' java.lang.String), D('boundary' java.lang.String)] */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01ff A[SYNTHETIC, Splitter:B:104:0x01ff] */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0204 A[Catch:{ IOException -> 0x0215 }] */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0209 A[Catch:{ IOException -> 0x0215 }] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0211 A[Catch:{ IOException -> 0x0215 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0147 A[SYNTHETIC, Splitter:B:41:0x0147] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x014c A[Catch:{ IOException -> 0x0169 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0151 A[Catch:{ IOException -> 0x0169 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0159 A[Catch:{ IOException -> 0x0169 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x017d A[SYNTHETIC, Splitter:B:60:0x017d] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0182 A[Catch:{ IOException -> 0x019a }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0187 A[Catch:{ IOException -> 0x019a }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x018f A[Catch:{ IOException -> 0x019a }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01aa A[SYNTHETIC, Splitter:B:76:0x01aa] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01af A[Catch:{ IOException -> 0x01c7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01b4 A[Catch:{ IOException -> 0x01c7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01bc A[Catch:{ IOException -> 0x01c7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01d4 A[SYNTHETIC, Splitter:B:90:0x01d4] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01d9 A[Catch:{ IOException -> 0x01f1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x01de A[Catch:{ IOException -> 0x01f1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01e6 A[Catch:{ IOException -> 0x01f1 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:38:0x0142=Splitter:B:38:0x0142, B:73:0x01a5=Splitter:B:73:0x01a5} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String uploadFile(java.lang.String r12, java.lang.String r13, android.content.Context r14) {
        /*
            r1 = 0
            r6 = 0
            r2 = 0
            r4 = 0
            java.lang.String r0 = "updateLog"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = "uploadUrl = "
            r3.<init>(r5)
            java.lang.StringBuilder r3 = r3.append(r12)
            java.lang.String r5 = ";uploadFile="
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.StringBuilder r3 = r3.append(r13)
            java.lang.String r3 = r3.toString()
            com.mobcent.update.android.util.MCLogUtil.i(r0, r3)
            boolean r0 = com.mobcent.update.android.util.PhoneConnectionUtil.isNetworkAvailable(r14)
            if (r0 != 0) goto L_0x0038
            java.lang.String r12 = "updateLog"
            java.lang.String r13 = "connnect fail"
            com.mobcent.update.android.util.MCLogUtil.i(r12, r13)
            java.lang.String r12 = "connection_fail"
            r14 = r4
            r13 = r2
            r0 = r6
            r10 = r1
            r1 = r12
            r12 = r10
        L_0x0037:
            return r1
        L_0x0038:
            java.lang.String r3 = "\r\n"
            java.lang.String r7 = "--"
            java.lang.String r0 = "*****"
            java.lang.String r5 = ""
            if (r13 == 0) goto L_0x0057
            java.lang.String r8 = com.mobcent.update.android.util.MCLibIOUtil.FS
            int r8 = r13.lastIndexOf(r8)
            r9 = -1
            if (r8 <= r9) goto L_0x0057
            java.lang.String r5 = com.mobcent.update.android.util.MCLibIOUtil.FS
            int r5 = r13.lastIndexOf(r5)
            int r5 = r5 + 1
            java.lang.String r5 = r13.substring(r5)
        L_0x0057:
            java.net.URL r8 = new java.net.URL     // Catch:{ ClientProtocolException -> 0x02a9, SocketTimeoutException -> 0x016e, IOException -> 0x019f, Exception -> 0x01cc, all -> 0x01f6 }
            r8.<init>(r12)     // Catch:{ ClientProtocolException -> 0x02a9, SocketTimeoutException -> 0x016e, IOException -> 0x019f, Exception -> 0x01cc, all -> 0x01f6 }
            java.net.HttpURLConnection r14 = getNewHttpURLConnection(r8, r14)     // Catch:{ ClientProtocolException -> 0x02a9, SocketTimeoutException -> 0x016e, IOException -> 0x019f, Exception -> 0x01cc, all -> 0x01f6 }
            r12 = 1
            r14.setDoInput(r12)     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            r12 = 1
            r14.setDoOutput(r12)     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            r12 = 0
            r14.setUseCaches(r12)     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            r12 = 180000(0x2bf20, float:2.52234E-40)
            r14.setConnectTimeout(r12)     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            r12 = 180000(0x2bf20, float:2.52234E-40)
            r14.setReadTimeout(r12)     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            java.lang.String r12 = "POST"
            r14.setRequestMethod(r12)     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            java.lang.String r12 = "Connection"
            java.lang.String r1 = "Keep-Alive"
            r14.setRequestProperty(r12, r1)     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            java.lang.String r12 = "Charset"
            java.lang.String r1 = "UTF-8"
            r14.setRequestProperty(r12, r1)     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            java.lang.String r12 = "Content-Type"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            java.lang.String r8 = "multipart/form-data;boundary="
            r1.<init>(r8)     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            java.lang.String r1 = r1.toString()     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            r14.setRequestProperty(r12, r1)     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            java.io.OutputStream r12 = r14.getOutputStream()     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            r1.<init>(r12)     // Catch:{ ClientProtocolException -> 0x02b1, SocketTimeoutException -> 0x0284, IOException -> 0x025f, Exception -> 0x023c, all -> 0x021a }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            java.lang.String r2 = java.lang.String.valueOf(r7)     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            r12.<init>(r2)     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            java.lang.StringBuilder r12 = r12.append(r0)     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            java.lang.StringBuilder r12 = r12.append(r3)     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            java.lang.String r12 = r12.toString()     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            r1.writeBytes(r12)     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            java.lang.String r2 = "Content-Disposition: form-data; name=\"file1\";filename=\""
            r12.<init>(r2)     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            java.lang.StringBuilder r12 = r12.append(r5)     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            java.lang.String r2 = "\""
            java.lang.StringBuilder r12 = r12.append(r2)     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            java.lang.StringBuilder r12 = r12.append(r3)     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            java.lang.String r12 = r12.toString()     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            r1.writeBytes(r12)     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            r1.writeBytes(r3)     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            r2.<init>(r13)     // Catch:{ ClientProtocolException -> 0x02ba, SocketTimeoutException -> 0x028d, IOException -> 0x0268, Exception -> 0x0244, all -> 0x0221 }
            r12 = 1024(0x400, float:1.435E-42)
            byte[] r12 = new byte[r12]     // Catch:{ ClientProtocolException -> 0x013b, SocketTimeoutException -> 0x0296, IOException -> 0x0271, Exception -> 0x024c, all -> 0x0228 }
            r13 = -1
        L_0x00e9:
            int r13 = r2.read(r12)     // Catch:{ ClientProtocolException -> 0x013b, SocketTimeoutException -> 0x0296, IOException -> 0x0271, Exception -> 0x024c, all -> 0x0228 }
            r4 = -1
            if (r13 != r4) goto L_0x0136
            r1.writeBytes(r3)     // Catch:{ ClientProtocolException -> 0x013b, SocketTimeoutException -> 0x0296, IOException -> 0x0271, Exception -> 0x024c, all -> 0x0228 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x013b, SocketTimeoutException -> 0x0296, IOException -> 0x0271, Exception -> 0x024c, all -> 0x0228 }
            java.lang.String r13 = java.lang.String.valueOf(r7)     // Catch:{ ClientProtocolException -> 0x013b, SocketTimeoutException -> 0x0296, IOException -> 0x0271, Exception -> 0x024c, all -> 0x0228 }
            r12.<init>(r13)     // Catch:{ ClientProtocolException -> 0x013b, SocketTimeoutException -> 0x0296, IOException -> 0x0271, Exception -> 0x024c, all -> 0x0228 }
            java.lang.StringBuilder r12 = r12.append(r0)     // Catch:{ ClientProtocolException -> 0x013b, SocketTimeoutException -> 0x0296, IOException -> 0x0271, Exception -> 0x024c, all -> 0x0228 }
            java.lang.StringBuilder r12 = r12.append(r7)     // Catch:{ ClientProtocolException -> 0x013b, SocketTimeoutException -> 0x0296, IOException -> 0x0271, Exception -> 0x024c, all -> 0x0228 }
            java.lang.StringBuilder r12 = r12.append(r3)     // Catch:{ ClientProtocolException -> 0x013b, SocketTimeoutException -> 0x0296, IOException -> 0x0271, Exception -> 0x024c, all -> 0x0228 }
            java.lang.String r12 = r12.toString()     // Catch:{ ClientProtocolException -> 0x013b, SocketTimeoutException -> 0x0296, IOException -> 0x0271, Exception -> 0x024c, all -> 0x0228 }
            r1.writeBytes(r12)     // Catch:{ ClientProtocolException -> 0x013b, SocketTimeoutException -> 0x0296, IOException -> 0x0271, Exception -> 0x024c, all -> 0x0228 }
            java.io.InputStream r13 = r14.getInputStream()     // Catch:{ ClientProtocolException -> 0x013b, SocketTimeoutException -> 0x0296, IOException -> 0x0271, Exception -> 0x024c, all -> 0x0228 }
            java.lang.String r0 = com.mobcent.update.android.util.MCLibIOUtil.convertStreamToString(r13)     // Catch:{ ClientProtocolException -> 0x02c3, SocketTimeoutException -> 0x029f, IOException -> 0x027a, Exception -> 0x0255, all -> 0x022f }
            if (r13 == 0) goto L_0x011c
            r13.close()     // Catch:{ IOException -> 0x0164 }
        L_0x011c:
            if (r2 == 0) goto L_0x0121
            r2.close()     // Catch:{ IOException -> 0x0164 }
        L_0x0121:
            if (r1 == 0) goto L_0x0129
            r1.flush()     // Catch:{ IOException -> 0x0164 }
            r1.close()     // Catch:{ IOException -> 0x0164 }
        L_0x0129:
            if (r14 == 0) goto L_0x012e
            r14.disconnect()     // Catch:{ IOException -> 0x0164 }
        L_0x012e:
            r12 = r14
            r14 = r2
            r10 = r13
            r13 = r1
            r1 = r0
            r0 = r10
            goto L_0x0037
        L_0x0136:
            r4 = 0
            r1.write(r12, r4, r13)     // Catch:{ ClientProtocolException -> 0x013b, SocketTimeoutException -> 0x0296, IOException -> 0x0271, Exception -> 0x024c, all -> 0x0228 }
            goto L_0x00e9
        L_0x013b:
            r12 = move-exception
            r0 = r2
            r13 = r1
            r1 = r6
            r10 = r12
            r12 = r14
            r14 = r10
        L_0x0142:
            r14.printStackTrace()     // Catch:{ all -> 0x0236 }
            if (r1 == 0) goto L_0x014a
            r1.close()     // Catch:{ IOException -> 0x0169 }
        L_0x014a:
            if (r0 == 0) goto L_0x014f
            r0.close()     // Catch:{ IOException -> 0x0169 }
        L_0x014f:
            if (r13 == 0) goto L_0x0157
            r13.flush()     // Catch:{ IOException -> 0x0169 }
            r13.close()     // Catch:{ IOException -> 0x0169 }
        L_0x0157:
            if (r12 == 0) goto L_0x015c
            r12.disconnect()     // Catch:{ IOException -> 0x0169 }
        L_0x015c:
            java.lang.String r14 = "connection_fail"
            r10 = r0
            r0 = r1
            r1 = r14
            r14 = r10
            goto L_0x0037
        L_0x0164:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x012e
        L_0x0169:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x015c
        L_0x016e:
            r12 = move-exception
            r14 = r12
            r0 = r4
            r13 = r2
            r12 = r1
            r1 = r6
        L_0x0174:
            java.lang.String r14 = "updateLog"
            java.lang.String r2 = "SocketTimeoutException"
            com.mobcent.update.android.util.MCLogUtil.i(r14, r2)     // Catch:{ all -> 0x0236 }
            if (r1 == 0) goto L_0x0180
            r1.close()     // Catch:{ IOException -> 0x019a }
        L_0x0180:
            if (r0 == 0) goto L_0x0185
            r0.close()     // Catch:{ IOException -> 0x019a }
        L_0x0185:
            if (r13 == 0) goto L_0x018d
            r13.flush()     // Catch:{ IOException -> 0x019a }
            r13.close()     // Catch:{ IOException -> 0x019a }
        L_0x018d:
            if (r12 == 0) goto L_0x0192
            r12.disconnect()     // Catch:{ IOException -> 0x019a }
        L_0x0192:
            java.lang.String r14 = "connection_fail"
            r10 = r0
            r0 = r1
            r1 = r14
            r14 = r10
            goto L_0x0037
        L_0x019a:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x0192
        L_0x019f:
            r12 = move-exception
            r14 = r12
            r0 = r4
            r13 = r2
            r12 = r1
            r1 = r6
        L_0x01a5:
            r14.printStackTrace()     // Catch:{ all -> 0x0236 }
            if (r1 == 0) goto L_0x01ad
            r1.close()     // Catch:{ IOException -> 0x01c7 }
        L_0x01ad:
            if (r0 == 0) goto L_0x01b2
            r0.close()     // Catch:{ IOException -> 0x01c7 }
        L_0x01b2:
            if (r13 == 0) goto L_0x01ba
            r13.flush()     // Catch:{ IOException -> 0x01c7 }
            r13.close()     // Catch:{ IOException -> 0x01c7 }
        L_0x01ba:
            if (r12 == 0) goto L_0x01bf
            r12.disconnect()     // Catch:{ IOException -> 0x01c7 }
        L_0x01bf:
            java.lang.String r14 = "connection_fail"
            r10 = r0
            r0 = r1
            r1 = r14
            r14 = r10
            goto L_0x0037
        L_0x01c7:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x01bf
        L_0x01cc:
            r12 = move-exception
            r14 = r12
            r0 = r4
            r13 = r2
            r12 = r1
            r1 = r6
        L_0x01d2:
            if (r1 == 0) goto L_0x01d7
            r1.close()     // Catch:{ IOException -> 0x01f1 }
        L_0x01d7:
            if (r0 == 0) goto L_0x01dc
            r0.close()     // Catch:{ IOException -> 0x01f1 }
        L_0x01dc:
            if (r13 == 0) goto L_0x01e4
            r13.flush()     // Catch:{ IOException -> 0x01f1 }
            r13.close()     // Catch:{ IOException -> 0x01f1 }
        L_0x01e4:
            if (r12 == 0) goto L_0x01e9
            r12.disconnect()     // Catch:{ IOException -> 0x01f1 }
        L_0x01e9:
            java.lang.String r14 = "upload_images_fail"
            r10 = r0
            r0 = r1
            r1 = r14
            r14 = r10
            goto L_0x0037
        L_0x01f1:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x01e9
        L_0x01f6:
            r12 = move-exception
            r14 = r4
            r13 = r2
            r0 = r6
            r10 = r12
            r12 = r1
            r1 = r10
        L_0x01fd:
            if (r0 == 0) goto L_0x0202
            r0.close()     // Catch:{ IOException -> 0x0215 }
        L_0x0202:
            if (r14 == 0) goto L_0x0207
            r14.close()     // Catch:{ IOException -> 0x0215 }
        L_0x0207:
            if (r13 == 0) goto L_0x020f
            r13.flush()     // Catch:{ IOException -> 0x0215 }
            r13.close()     // Catch:{ IOException -> 0x0215 }
        L_0x020f:
            if (r12 == 0) goto L_0x0214
            r12.disconnect()     // Catch:{ IOException -> 0x0215 }
        L_0x0214:
            throw r1
        L_0x0215:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x0214
        L_0x021a:
            r12 = move-exception
            r1 = r12
            r13 = r2
            r0 = r6
            r12 = r14
            r14 = r4
            goto L_0x01fd
        L_0x0221:
            r12 = move-exception
            r13 = r1
            r0 = r6
            r1 = r12
            r12 = r14
            r14 = r4
            goto L_0x01fd
        L_0x0228:
            r12 = move-exception
            r13 = r1
            r0 = r6
            r1 = r12
            r12 = r14
            r14 = r2
            goto L_0x01fd
        L_0x022f:
            r12 = move-exception
            r0 = r13
            r13 = r1
            r1 = r12
            r12 = r14
            r14 = r2
            goto L_0x01fd
        L_0x0236:
            r14 = move-exception
            r10 = r14
            r14 = r0
            r0 = r1
            r1 = r10
            goto L_0x01fd
        L_0x023c:
            r12 = move-exception
            r0 = r4
            r13 = r2
            r1 = r6
            r10 = r12
            r12 = r14
            r14 = r10
            goto L_0x01d2
        L_0x0244:
            r12 = move-exception
            r0 = r4
            r13 = r1
            r1 = r6
            r10 = r12
            r12 = r14
            r14 = r10
            goto L_0x01d2
        L_0x024c:
            r12 = move-exception
            r0 = r2
            r13 = r1
            r1 = r6
            r10 = r12
            r12 = r14
            r14 = r10
            goto L_0x01d2
        L_0x0255:
            r12 = move-exception
            r0 = r2
            r10 = r12
            r12 = r14
            r14 = r10
            r11 = r13
            r13 = r1
            r1 = r11
            goto L_0x01d2
        L_0x025f:
            r12 = move-exception
            r0 = r4
            r13 = r2
            r1 = r6
            r10 = r12
            r12 = r14
            r14 = r10
            goto L_0x01a5
        L_0x0268:
            r12 = move-exception
            r0 = r4
            r13 = r1
            r1 = r6
            r10 = r12
            r12 = r14
            r14 = r10
            goto L_0x01a5
        L_0x0271:
            r12 = move-exception
            r0 = r2
            r13 = r1
            r1 = r6
            r10 = r12
            r12 = r14
            r14 = r10
            goto L_0x01a5
        L_0x027a:
            r12 = move-exception
            r0 = r2
            r10 = r12
            r12 = r14
            r14 = r10
            r11 = r13
            r13 = r1
            r1 = r11
            goto L_0x01a5
        L_0x0284:
            r12 = move-exception
            r0 = r4
            r13 = r2
            r1 = r6
            r10 = r12
            r12 = r14
            r14 = r10
            goto L_0x0174
        L_0x028d:
            r12 = move-exception
            r0 = r4
            r13 = r1
            r1 = r6
            r10 = r12
            r12 = r14
            r14 = r10
            goto L_0x0174
        L_0x0296:
            r12 = move-exception
            r0 = r2
            r13 = r1
            r1 = r6
            r10 = r12
            r12 = r14
            r14 = r10
            goto L_0x0174
        L_0x029f:
            r12 = move-exception
            r0 = r2
            r10 = r12
            r12 = r14
            r14 = r10
            r11 = r13
            r13 = r1
            r1 = r11
            goto L_0x0174
        L_0x02a9:
            r12 = move-exception
            r14 = r12
            r0 = r4
            r13 = r2
            r12 = r1
            r1 = r6
            goto L_0x0142
        L_0x02b1:
            r12 = move-exception
            r0 = r4
            r13 = r2
            r1 = r6
            r10 = r12
            r12 = r14
            r14 = r10
            goto L_0x0142
        L_0x02ba:
            r12 = move-exception
            r0 = r4
            r13 = r1
            r1 = r6
            r10 = r12
            r12 = r14
            r14 = r10
            goto L_0x0142
        L_0x02c3:
            r12 = move-exception
            r0 = r2
            r10 = r12
            r12 = r14
            r14 = r10
            r11 = r13
            r13 = r1
            r1 = r11
            goto L_0x0142
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.update.android.api.util.HttpClientUtil.uploadFile(java.lang.String, java.lang.String, android.content.Context):java.lang.String");
    }

    public static HttpClient getNewHttpClient(Context context) {
        String proxyStr;
        Cursor mCursor = null;
        try {
            KeyStore.getInstance(KeyStore.getDefaultType()).load(null, null);
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, SET_CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, SET_SOCKET_TIMEOUT);
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, "UTF-8");
            HttpClient client = new DefaultHttpClient(params);
            if (!((WifiManager) context.getSystemService("wifi")).isWifiEnabled() && (mCursor = context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null)) != null && mCursor.moveToFirst() && (proxyStr = mCursor.getString(mCursor.getColumnIndex("proxy"))) != null && proxyStr.trim().length() > 0) {
                client.getParams().setParameter("http.route.default-proxy", new HttpHost(proxyStr, 80));
            }
            if (mCursor != null) {
                mCursor.close();
            }
            return client;
        } catch (Exception e) {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            if (mCursor == null) {
                return defaultHttpClient;
            }
            mCursor.close();
            return defaultHttpClient;
        } catch (Throwable th) {
            if (mCursor != null) {
                mCursor.close();
            }
            throw th;
        }
    }

    /* JADX INFO: Multiple debug info for r11v14 'mCursor'  android.database.Cursor: [D('mCursor' android.database.Cursor), D('context' android.content.Context)] */
    /* JADX INFO: Multiple debug info for r2v5 java.lang.String: [D('proxyStr' java.lang.String), D('uri' android.net.Uri)] */
    /* JADX WARN: Type inference failed for: r1v25, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.net.HttpURLConnection getNewHttpURLConnection(java.net.URL r10, android.content.Context r11) {
        /*
            r1 = 0
            r8 = 0
            java.net.URLConnection r7 = r10.openConnection()     // Catch:{ Exception -> 0x0063, all -> 0x0088 }
            java.net.HttpURLConnection r7 = (java.net.HttpURLConnection) r7     // Catch:{ Exception -> 0x0063, all -> 0x0088 }
            java.lang.String r1 = "wifi"
            java.lang.Object r1 = r11.getSystemService(r1)     // Catch:{ Exception -> 0x00a1, all -> 0x0093 }
            android.net.wifi.WifiManager r1 = (android.net.wifi.WifiManager) r1     // Catch:{ Exception -> 0x00a1, all -> 0x0093 }
            boolean r1 = r1.isWifiEnabled()     // Catch:{ Exception -> 0x00a1, all -> 0x0093 }
            if (r1 != 0) goto L_0x00ac
            java.lang.String r1 = "content://telephony/carriers/preferapn"
            android.net.Uri r2 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x00a1, all -> 0x0093 }
            android.content.ContentResolver r1 = r11.getContentResolver()     // Catch:{ Exception -> 0x00a1, all -> 0x0093 }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            android.database.Cursor r11 = r1.query(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x00a1, all -> 0x0093 }
            if (r11 == 0) goto L_0x00aa
            boolean r1 = r11.moveToFirst()     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            if (r1 == 0) goto L_0x00aa
            java.lang.String r1 = "proxy"
            int r1 = r11.getColumnIndex(r1)     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            java.lang.String r2 = r11.getString(r1)     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            if (r2 == 0) goto L_0x00aa
            java.lang.String r1 = r2.trim()     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            int r1 = r1.length()     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            if (r1 <= 0) goto L_0x00aa
            java.net.Proxy r1 = new java.net.Proxy     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            java.net.Proxy$Type r3 = java.net.Proxy.Type.HTTP     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            r5 = 80
            r4.<init>(r2, r5)     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            r1.<init>(r3, r4)     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            java.net.URLConnection r1 = r10.openConnection(r1)     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            r0 = r1
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            r10 = r0
        L_0x005c:
            if (r11 == 0) goto L_0x0061
            r11.close()
        L_0x0061:
            r1 = r10
        L_0x0062:
            return r1
        L_0x0063:
            r11 = move-exception
            r2 = r8
            r9 = r11
            r11 = r1
            r1 = r9
        L_0x0068:
            r1.printStackTrace()     // Catch:{ all -> 0x009c }
            java.net.URLConnection r10 = r10.openConnection()     // Catch:{ IOException -> 0x007a }
            java.net.HttpURLConnection r10 = (java.net.HttpURLConnection) r10     // Catch:{ IOException -> 0x007a }
            if (r2 == 0) goto L_0x0076
            r2.close()
        L_0x0076:
            r1 = r10
            r10 = r11
            r11 = r2
            goto L_0x0062
        L_0x007a:
            r10 = move-exception
            r10.printStackTrace()     // Catch:{ all -> 0x009c }
            if (r2 == 0) goto L_0x0083
            r2.close()
        L_0x0083:
            r10 = 0
            r1 = r10
            r10 = r11
            r11 = r2
            goto L_0x0062
        L_0x0088:
            r10 = move-exception
            r11 = r8
            r9 = r10
            r10 = r1
            r1 = r9
        L_0x008d:
            if (r11 == 0) goto L_0x0092
            r11.close()
        L_0x0092:
            throw r1
        L_0x0093:
            r10 = move-exception
            r1 = r10
            r11 = r8
            r10 = r7
            goto L_0x008d
        L_0x0098:
            r10 = move-exception
            r1 = r10
            r10 = r7
            goto L_0x008d
        L_0x009c:
            r10 = move-exception
            r1 = r10
            r10 = r11
            r11 = r2
            goto L_0x008d
        L_0x00a1:
            r11 = move-exception
            r1 = r11
            r2 = r8
            r11 = r7
            goto L_0x0068
        L_0x00a6:
            r1 = move-exception
            r2 = r11
            r11 = r7
            goto L_0x0068
        L_0x00aa:
            r10 = r7
            goto L_0x005c
        L_0x00ac:
            r11 = r8
            r10 = r7
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.update.android.api.util.HttpClientUtil.getNewHttpURLConnection(java.net.URL, android.content.Context):java.net.HttpURLConnection");
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006b  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:35:0x0071=Splitter:B:35:0x0071, B:24:0x005a=Splitter:B:24:0x005a} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String read(org.apache.http.HttpResponse r13) throws java.lang.Exception {
        /*
            r12 = -1
            java.lang.String r8 = ""
            org.apache.http.HttpEntity r3 = r13.getEntity()
            r5 = 0
            r0 = 0
            java.io.InputStream r5 = r3.getContent()     // Catch:{ IllegalStateException -> 0x007e, IOException -> 0x006f }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ IllegalStateException -> 0x007e, IOException -> 0x006f }
            r1.<init>()     // Catch:{ IllegalStateException -> 0x007e, IOException -> 0x006f }
            java.lang.String r10 = "Content-Encoding"
            org.apache.http.Header r4 = r13.getFirstHeader(r10)     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            if (r4 == 0) goto L_0x0030
            java.lang.String r10 = r4.getValue()     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            java.lang.String r10 = r10.toLowerCase()     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            java.lang.String r11 = "gzip"
            int r10 = r10.indexOf(r11)     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            if (r10 <= r12) goto L_0x0030
            java.util.zip.GZIPInputStream r6 = new java.util.zip.GZIPInputStream     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            r6.<init>(r5)     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            r5 = r6
        L_0x0030:
            r7 = 0
            r10 = 512(0x200, float:7.175E-43)
            byte[] r9 = new byte[r10]     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
        L_0x0035:
            int r7 = r5.read(r9)     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            if (r7 != r12) goto L_0x0052
            java.lang.String r8 = new java.lang.String     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            byte[] r10 = r1.toByteArray()     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            r8.<init>(r10)     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            if (r1 == 0) goto L_0x004c
            r1.flush()
            r1.close()
        L_0x004c:
            if (r5 == 0) goto L_0x0051
            r5.close()
        L_0x0051:
            return r8
        L_0x0052:
            r10 = 0
            r1.write(r9, r10, r7)     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            goto L_0x0035
        L_0x0057:
            r10 = move-exception
            r2 = r10
            r0 = r1
        L_0x005a:
            java.lang.Exception r10 = new java.lang.Exception     // Catch:{ all -> 0x0060 }
            r10.<init>(r2)     // Catch:{ all -> 0x0060 }
            throw r10     // Catch:{ all -> 0x0060 }
        L_0x0060:
            r10 = move-exception
        L_0x0061:
            if (r0 == 0) goto L_0x0069
            r0.flush()
            r0.close()
        L_0x0069:
            if (r5 == 0) goto L_0x006e
            r5.close()
        L_0x006e:
            throw r10
        L_0x006f:
            r10 = move-exception
            r2 = r10
        L_0x0071:
            java.lang.Exception r10 = new java.lang.Exception     // Catch:{ all -> 0x0060 }
            r10.<init>(r2)     // Catch:{ all -> 0x0060 }
            throw r10     // Catch:{ all -> 0x0060 }
        L_0x0077:
            r10 = move-exception
            r0 = r1
            goto L_0x0061
        L_0x007a:
            r10 = move-exception
            r2 = r10
            r0 = r1
            goto L_0x0071
        L_0x007e:
            r10 = move-exception
            r2 = r10
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.update.android.api.util.HttpClientUtil.read(org.apache.http.HttpResponse):java.lang.String");
    }
}
