package com.mobcent.update.android.util;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MCLibIOUtil {
    public static final String CACHE = "cache";
    public static final String FS = File.separator;
    public static final String MOBCENT = "mobcent";
    public static final String UPDATE = "update";

    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                Log.e("convertStreamToString", "convertStreamToString error");
                try {
                    is.close();
                } catch (IOException e3) {
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    public static boolean getExternalStorageState() {
        String state = Environment.getExternalStorageState();
        if ("mounted".equals(state)) {
            return true;
        }
        if ("mounted_ro".equals(state)) {
            return false;
        }
        return false;
    }

    public static String getSDCardPath() {
        return Environment.getExternalStorageDirectory().toString();
    }

    public static boolean isFileExist(String path) {
        File file = new File(path);
        return file.exists() && file.isFile();
    }

    public static boolean isDirExist(String path) {
        File file = new File(path);
        return file.exists() && file.isDirectory();
    }

    public static boolean makeDirs(String path) {
        return new File(path).mkdirs();
    }

    public static String getBaseLocalLocation(Context context) {
        if (getExternalStorageState()) {
            return getSDCardPath();
        }
        return context.getFilesDir().getAbsolutePath();
    }

    public static int getFileSize(File file) {
        IOException e;
        FileNotFoundException e2;
        int size = 0;
        try {
            try {
                size = new FileInputStream(file).available();
            } catch (FileNotFoundException e3) {
                e2 = e3;
                e2.printStackTrace();
                return size;
            } catch (IOException e4) {
                e = e4;
                e.printStackTrace();
                return size;
            }
        } catch (FileNotFoundException e5) {
            e2 = e5;
            e2.printStackTrace();
            return size;
        } catch (IOException e6) {
            e = e6;
            e.printStackTrace();
            return size;
        }
        return size;
    }

    public static File getUpdateFile(Context context) {
        return createDirInCache(context, "update");
    }

    private static File createDirInCache(Context context, String dirName) {
        return createFileDir(String.valueOf(getCachePath(context)) + dirName + FS);
    }

    private static String getCachePath(Context context) {
        String basePath = getBaseLocalLocation(context);
        return String.valueOf(basePath) + FS + "mobcent" + FS + getPackageName(context) + FS + "cache" + FS;
    }

    private static File createFileDir(String path) {
        if (isDirExist(path) || makeDirs(path)) {
            return new File(path);
        }
        return null;
    }

    private static String getPackageName(Context context) {
        return context.getPackageName();
    }
}
