package com.mobcent.update.android.util;

import android.content.Context;

public class MCStringUtil {
    public static String resolveString(int stringSource, String[] args, Context context) {
        String string = context.getResources().getString(stringSource);
        for (int i = 0; i < args.length; i++) {
            string = string.replace("{" + i + "}", args[i]);
        }
        return string;
    }

    public static String resolveString(int stringSource, String arg, Context context) {
        return context.getResources().getString(stringSource).replace("{0}", arg);
    }
}
