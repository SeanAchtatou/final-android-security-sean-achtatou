package com.mobcent.update.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.update.android.db.constant.UpdateDBConstant;
import com.mobcent.update.android.model.DownloadModel;

public class DownloadDBUtil implements UpdateDBConstant {
    private static UpdateDBHelper dbHelper;

    public DownloadDBUtil(Context context) {
        dbHelper = new UpdateDBHelper(context);
    }

    public void delete() {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.delete(UpdateDBConstant.TABLE_DOWNLOAD, null, null);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void save(java.lang.String r6, int r7, int r8) {
        /*
            r5 = this;
            r0 = 0
            com.mobcent.update.android.db.UpdateDBHelper r3 = com.mobcent.update.android.db.DownloadDBUtil.dbHelper     // Catch:{ Exception -> 0x003b }
            android.database.sqlite.SQLiteDatabase r0 = r3.getWritableDatabase()     // Catch:{ Exception -> 0x003b }
            r0.beginTransaction()     // Catch:{ Exception -> 0x003b }
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ all -> 0x0036 }
            r2.<init>()     // Catch:{ all -> 0x0036 }
            java.lang.String r3 = "downpath"
            r2.put(r3, r6)     // Catch:{ all -> 0x0036 }
            java.lang.String r3 = "postionid"
            java.lang.Integer r4 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x0036 }
            r2.put(r3, r4)     // Catch:{ all -> 0x0036 }
            java.lang.String r3 = "downlength"
            java.lang.Integer r4 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0036 }
            r2.put(r3, r4)     // Catch:{ all -> 0x0036 }
            java.lang.String r3 = "download_table"
            r4 = 0
            r0.insertOrThrow(r3, r4, r2)     // Catch:{ all -> 0x0036 }
            r0.setTransactionSuccessful()     // Catch:{ all -> 0x0036 }
            r0.endTransaction()     // Catch:{ Exception -> 0x003b }
            r0.close()     // Catch:{ Exception -> 0x003b }
        L_0x0035:
            return
        L_0x0036:
            r3 = move-exception
            r0.endTransaction()     // Catch:{ Exception -> 0x003b }
            throw r3     // Catch:{ Exception -> 0x003b }
        L_0x003b:
            r3 = move-exception
            r1 = r3
            r1.printStackTrace()
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.update.android.db.DownloadDBUtil.save(java.lang.String, int, int):void");
    }

    public DownloadModel getData(String downloadUrl) {
        Exception e;
        DownloadModel model = null;
        try {
            SQLiteDatabase db = dbHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery("select *  from download_table where downpath=?", new String[]{downloadUrl});
            while (true) {
                try {
                    DownloadModel model2 = model;
                    if (!cursor.moveToNext()) {
                        cursor.close();
                        db.close();
                        DownloadModel downloadModel = model2;
                        return model2;
                    }
                    model = new DownloadModel();
                    model.setDownloadUrl(cursor.getString(1));
                    model.setLastPostion(cursor.getInt(2));
                    model.setDownloadLength(cursor.getInt(3));
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return null;
                }
            }
        } catch (Exception e3) {
            e = e3;
        }
    }

    public void update(int downloadSize, String url, int fileSize) {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(UpdateDBConstant.POSTION_ID, Integer.valueOf(downloadSize));
            values.put(UpdateDBConstant.DOWN_PATH, url);
            values.put(UpdateDBConstant.DOWN_LENGTH, Integer.valueOf(fileSize));
            db.update(UpdateDBConstant.TABLE_DOWNLOAD, values, "downpath = '" + url + "'", null);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateOrSave(int downloadSize, String url, int fileSize) {
        if (getData(url) != null) {
            update(downloadSize, url, fileSize);
        } else {
            save(url, downloadSize, fileSize);
        }
    }
}
