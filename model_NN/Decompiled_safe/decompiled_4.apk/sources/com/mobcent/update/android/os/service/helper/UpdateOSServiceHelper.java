package com.mobcent.update.android.os.service.helper;

import android.content.Context;
import android.content.Intent;
import com.mobcent.update.android.constant.MCUpdateConstant;
import com.mobcent.update.android.model.UpdateModel;
import com.mobcent.update.android.os.service.DownloadNoticeOSService;
import com.mobcent.update.android.os.service.UpdateNoticeOSService;

public class UpdateOSServiceHelper {
    public static void startUpdateNoticeService(Context context) {
        UpdateNoticeOSService.setUpdateCallBack(null);
        context.startService(new Intent(context, UpdateNoticeOSService.class));
    }

    public static void startUpdateNoticeService(Context context, UpdateNoticeOSService.UpdateCallBack updateCallBack) {
        UpdateNoticeOSService.setUpdateCallBack(updateCallBack);
        context.startService(new Intent(context, UpdateNoticeOSService.class));
    }

    public static void stopUpdateNoticeService(Context context) {
        UpdateNoticeOSService.setUpdateCallBack(null);
        context.stopService(new Intent(context, UpdateNoticeOSService.class));
    }

    public static void startDownloadService(Context context, UpdateModel updateModel) {
        Intent intent = new Intent(context, DownloadNoticeOSService.class);
        intent.setAction(MCUpdateConstant.DOWNLOAD_SERVICE_ACTION);
        if (updateModel != null) {
            intent.putExtra(MCUpdateConstant.UPDATE_NOTICE_MODEL, updateModel);
        }
        context.startService(intent);
    }

    public static void stopDownloadService(Context context) {
        context.stopService(new Intent(context, DownloadNoticeOSService.class));
    }

    public static void setUpdateCallBack(UpdateNoticeOSService.UpdateCallBack updateCallBack) {
        UpdateNoticeOSService.setUpdateCallBack(updateCallBack);
    }
}
