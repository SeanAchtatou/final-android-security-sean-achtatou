package com.mobcent.update.android.model;

import java.io.Serializable;

public class UpdateModel implements Serializable {
    private static final long serialVersionUID = 1;
    private String appName;
    private String desc;
    private String icon;
    private int id;
    private String link;
    private int showDetail;
    private float size;
    private int status;
    private long time;
    private String type;
    private String ver_name;

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public int getShowDetail() {
        return this.showDetail;
    }

    public void setShowDetail(int showDetail2) {
        this.showDetail = showDetail2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String appName2) {
        this.appName = appName2;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getVer_name() {
        return this.ver_name;
    }

    public void setVer_name(String ver_name2) {
        this.ver_name = ver_name2;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link2) {
        this.link = link2;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc2) {
        this.desc = desc2;
    }

    public float getSize() {
        return this.size;
    }

    public void setSize(float size2) {
        this.size = size2;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time2) {
        this.time = time2;
    }
}
