package com.mobcent.update.android.api;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.mobcent.update.android.api.util.HttpClientUtil;
import com.mobcent.update.android.constant.MCUpdateConstant;
import java.util.HashMap;

public abstract class BaseRestfulApiRequester implements MCUpdateConstant {
    public static String doPostRequest(String urlString, HashMap<String, String> params, Context context) {
        try {
            String forumKey = context.getPackageManager().getApplicationInfo(context.getPackageName(), AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER).metaData.getString("mc_forum_key");
            int version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            if (forumKey != null) {
                params.put("forumKey", forumKey);
            }
            if (version != 0) {
                params.put("versionCode", new StringBuilder(String.valueOf(version)).toString());
            }
            params.put("platType", "1");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return HttpClientUtil.doPostRequest(urlString, params, context);
    }
}
