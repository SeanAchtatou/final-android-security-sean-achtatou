package com.mobcent.update.android.download;

public interface DownloadProgressListener {
    void onDownloadSize(int i, int i2);
}
