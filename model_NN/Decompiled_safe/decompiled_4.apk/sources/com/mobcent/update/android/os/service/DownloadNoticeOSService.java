package com.mobcent.update.android.os.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.mobcent.update.android.constant.MCUpdateConstant;
import com.mobcent.update.android.db.UpdateStatusDBUtil;
import com.mobcent.update.android.download.DownloadProgressListener;
import com.mobcent.update.android.download.FileDownloader;
import com.mobcent.update.android.model.UpdateModel;
import com.mobcent.update.android.util.ApkUtil;
import com.mobcent.update.android.util.MCLibIOUtil;
import com.mobcent.update.android.util.MCStringUtil;
import com.mobcent.update.android.util.MCUpdateResource;
import java.io.File;

public class DownloadNoticeOSService extends Service {
    /* access modifiers changed from: private */
    public String apkName;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg != null) {
                switch (msg.what) {
                    case -1:
                        Toast.makeText(DownloadNoticeOSService.this.getApplicationContext(), DownloadNoticeOSService.this.getResources().getString(DownloadNoticeOSService.this.resource.getStringId("mc_update_download_check")), 0).show();
                        DownloadNoticeOSService.this.nm.cancel(12);
                        return;
                    case 0:
                    case 1:
                    case 3:
                    default:
                        return;
                    case 2:
                        Integer precent = (Integer) msg.obj;
                        DownloadNoticeOSService.this.notification.contentView.setProgressBar(DownloadNoticeOSService.this.resource.getViewId("mc_update_download_progressBar"), 100, precent.intValue(), false);
                        DownloadNoticeOSService.this.notification.contentView.setTextViewText(DownloadNoticeOSService.this.resource.getViewId("mc_update_download_progress_txt"), MCStringUtil.resolveString(DownloadNoticeOSService.this.resource.getStringId("mc_update_download_progress"), new StringBuilder().append(precent).toString(), DownloadNoticeOSService.this.getApplicationContext()));
                        DownloadNoticeOSService.this.nm.notify(12, DownloadNoticeOSService.this.notification);
                        return;
                    case 4:
                        new UpdateStatusDBUtil(DownloadNoticeOSService.this.getApplicationContext()).saveOrUpdate(DownloadNoticeOSService.this.updateModel, 3);
                        ApkUtil.installApk(DownloadNoticeOSService.this.getApplicationContext(), DownloadNoticeOSService.this.apkName);
                        DownloadNoticeOSService.this.nm.cancel(12);
                        return;
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public NotificationManager nm;
    /* access modifiers changed from: private */
    public Notification notification;
    private final int notificationId = 12;
    /* access modifiers changed from: private */
    public MCUpdateResource resource;
    /* access modifiers changed from: private */
    public UpdateModel updateModel;
    private RemoteViews views;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        if (intent != null) {
            this.resource = MCUpdateResource.getInstance(this);
            this.updateModel = (UpdateModel) intent.getSerializableExtra(MCUpdateConstant.UPDATE_NOTICE_MODEL);
            getNotification();
            download(this.updateModel.getLink(), MCLibIOUtil.getUpdateFile(this));
        }
    }

    private void getNotification() {
        this.nm = (NotificationManager) getApplicationContext().getSystemService("notification");
        this.notification = new Notification(0, this.updateModel.getAppName(), System.currentTimeMillis());
        this.notification.icon = 17301633;
        this.notification.flags = 2;
        this.notification.when = System.currentTimeMillis();
        this.notification.defaults = 4;
        this.views = new RemoteViews(getApplicationContext().getPackageName(), this.resource.getLayoutId("mc_update_download_notification"));
        this.notification.setLatestEventInfo(getApplicationContext(), "", "", PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), 134217728));
        this.notification.contentView.setProgressBar(this.resource.getViewId("mc_update_download_progressBar"), 100, 0, false);
        this.notification.contentView = this.views;
        this.nm.notify(12, this.notification);
    }

    private void download(String path, File savedir) {
        if (!savedir.exists()) {
            savedir.mkdirs();
        }
        if (!MCLibIOUtil.getExternalStorageState()) {
            Toast.makeText(getApplicationContext(), this.resource.getStringId("mc_update_download_no_sd"), 1).show();
            return;
        }
        FileDownloader downloader = new FileDownloader(getApplicationContext(), path, savedir, new DownloadProgressListener() {
            public void onDownloadSize(int status, int precent) {
                DownloadNoticeOSService.this.handler.sendMessage(DownloadNoticeOSService.this.handler.obtainMessage(status, Integer.valueOf(precent)));
            }
        });
        downloader.start();
        this.apkName = downloader.getApkName();
    }
}
