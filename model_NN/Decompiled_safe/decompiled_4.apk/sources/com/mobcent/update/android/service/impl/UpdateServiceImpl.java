package com.mobcent.update.android.service.impl;

import android.content.Context;
import com.mobcent.update.android.api.UpdateRestfulApiRequester;
import com.mobcent.update.android.db.UpdateStatusDBUtil;
import com.mobcent.update.android.model.UpdateModel;
import com.mobcent.update.android.service.UpdateService;
import com.mobcent.update.android.service.impl.helper.UpdateServiceImplHelper;

public class UpdateServiceImpl implements UpdateService {
    private UpdateStatusDBUtil dbUtil;
    private Context mContext;

    public UpdateServiceImpl(Context context) {
        this.mContext = context;
        this.dbUtil = new UpdateStatusDBUtil(context);
    }

    public UpdateModel getUpdateInfo(Context context) {
        new UpdateModel();
        UpdateModel updateModel = UpdateServiceImplHelper.getUpdateInfo(UpdateRestfulApiRequester.getUpdateInfo(this.mContext));
        if (updateModel == null) {
            return null;
        }
        int existedState = this.dbUtil.getUpdateStatus(updateModel);
        if (existedState == 0) {
            this.dbUtil.saveOrUpdate(updateModel, 0);
        }
        if (existedState == 2 || existedState == 0 || existedState == 1) {
            return updateModel;
        }
        return null;
    }
}
