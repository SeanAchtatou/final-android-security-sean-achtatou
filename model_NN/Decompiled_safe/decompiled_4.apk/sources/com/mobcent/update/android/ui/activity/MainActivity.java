package com.mobcent.update.android.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import com.mobcent.update.android.os.service.helper.UpdateOSServiceHelper;

public class MainActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        UpdateOSServiceHelper.startUpdateNoticeService(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        UpdateOSServiceHelper.stopUpdateNoticeService(this);
    }
}
