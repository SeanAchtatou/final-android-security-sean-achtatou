package com.mobcent.update.android.ui.activity.receiver;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.update.android.constant.MCUpdateConstant;
import com.mobcent.update.android.db.UpdateStatusDBUtil;
import com.mobcent.update.android.model.UpdateModel;
import com.mobcent.update.android.os.service.helper.UpdateOSServiceHelper;
import com.mobcent.update.android.util.MCLogUtil;
import com.mobcent.update.android.util.MCUpdateResource;

public class UpdateNoticeReceiver extends BroadcastReceiver implements MCUpdateConstant {
    public static final int UPDATE_NOTICE_ID = 11;

    public void onReceive(final Context context, Intent intent) {
        if (intent != null) {
            try {
                if ((String.valueOf(context.getPackageName()) + MCUpdateConstant.UPDATE_NOTICE_SERVER_MSG).equals(intent.getAction())) {
                    MCLogUtil.i("UpdateNoticeReceiver", "onReceive");
                    final UpdateStatusDBUtil dbUtil = new UpdateStatusDBUtil(context);
                    final UpdateModel updateModel = (UpdateModel) intent.getSerializableExtra(MCUpdateConstant.RETURN_UPDATE_NOTICE_MODEL);
                    if (updateModel != null && !updateModel.getAppName().equals("") && updateModel.getAppName() != null && !updateModel.getVer_name().equals("") && updateModel.getVer_name() != null) {
                        MCUpdateResource resource = MCUpdateResource.getInstance(context);
                        String title = context.getResources().getString(resource.getStringId("mc_update_notificaion_title"));
                        String content = String.valueOf(context.getResources().getString(resource.getStringId("mc_update_dialog_new"))) + updateModel.getAppName() + AdApiConstant.RES_SPLIT_COMMA + context.getResources().getString(resource.getStringId("mc_update_dialog_version")) + updateModel.getVer_name() + AdApiConstant.RES_SPLIT_COMMA + context.getResources().getString(resource.getStringId("mc_update_dialog_install"));
                        if (updateModel.getDesc() != null && !updateModel.getDesc().equals("")) {
                            content = String.valueOf(content) + "\n" + context.getResources().getString(resource.getStringId("mc_update_dialog_desc")) + "\n" + updateModel.getDesc();
                        }
                        AlertDialog dialog = new AlertDialog.Builder(context.getApplicationContext()).setIcon(17301659).setTitle(title).setMessage(content).setPositiveButton(context.getResources().getString(resource.getStringId("mc_update_dialog_confirm")), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dbUtil.saveOrUpdate(updateModel, 1);
                                UpdateOSServiceHelper.startDownloadService(context, updateModel);
                                dialog.dismiss();
                            }
                        }).setNegativeButton(context.getResources().getString(resource.getStringId("mc_update_dialog_cancel")), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dbUtil.saveOrUpdate(updateModel, 2);
                                dialog.dismiss();
                            }
                        }).create();
                        dialog.getWindow().setType(2003);
                        dialog.setCancelable(false);
                        dialog.show();
                    }
                }
            } catch (Exception e) {
            }
        }
    }
}
