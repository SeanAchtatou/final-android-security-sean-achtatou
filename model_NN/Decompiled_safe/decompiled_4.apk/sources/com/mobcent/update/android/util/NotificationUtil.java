package com.mobcent.update.android.util;

import com.mobcent.update.android.constant.MCUpdateConstant;
import java.util.LinkedHashMap;

public class NotificationUtil implements MCUpdateConstant {
    private static LinkedHashMap<String, Integer> nfHashMap;
    private static NotificationUtil notificationUtil;
    private int notificationId = 1;

    public NotificationUtil() {
        nfHashMap = new LinkedHashMap<>();
    }

    public static NotificationUtil getNotification() {
        if (notificationUtil == null) {
            notificationUtil = new NotificationUtil();
        }
        return notificationUtil;
    }

    public void remove(String url) {
        nfHashMap.remove(url);
    }

    public int add(String url) {
        if (this.notificationId > 1000000) {
            this.notificationId = 1;
        }
        nfHashMap.put(url, Integer.valueOf(this.notificationId));
        int tNotificationId = this.notificationId;
        this.notificationId++;
        return tNotificationId;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public int get(String url) {
        if (nfHashMap.containsKey(url)) {
            return nfHashMap.get(url).intValue();
        }
        return -1;
    }
}
