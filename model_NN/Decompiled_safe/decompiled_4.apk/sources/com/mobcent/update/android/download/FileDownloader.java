package com.mobcent.update.android.download;

import android.content.Context;
import com.mobcent.update.android.constant.MCUpdateConstant;
import com.mobcent.update.android.db.DownloadDBUtil;
import com.mobcent.update.android.model.DownloadModel;
import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.http.Header;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

public class FileDownloader extends Thread implements MCUpdateConstant {
    private String apkName = "";
    private int downloadPercent = 0;
    private int downloadSize = 0;
    private String downloadUrl;
    private File fileSaveDir;
    private int fileSize = 0;
    private int lastPosition = 0;
    private DownloadProgressListener listener;
    private DownloadModel model;
    private File saveFile;
    private int status = 1;
    private DownloadDBUtil updateDBUtil;

    public FileDownloader(Context context, String downloadUrl2, File fileSaveDir2, DownloadProgressListener listener2) {
        this.downloadUrl = downloadUrl2;
        this.updateDBUtil = new DownloadDBUtil(context);
        this.fileSaveDir = fileSaveDir2;
        this.listener = listener2;
        this.model = new DownloadModel();
    }

    public int getLastPosition() {
        this.model = this.updateDBUtil.getData(this.downloadUrl);
        if (this.model != null) {
            return this.model.getLastPostion();
        }
        this.model = new DownloadModel();
        this.model.setDownloadLength(this.downloadSize);
        this.model.setDownloadUrl(this.downloadUrl);
        this.model.setLastPostion(this.lastPosition);
        return 0;
    }

    /* access modifiers changed from: protected */
    public synchronized void append(int size) {
        this.downloadSize += size;
    }

    public void run() {
        super.run();
        this.lastPosition = getLastPosition();
        init();
        download();
    }

    public String getApkName() {
        try {
            for (Header h : new DefaultHttpClient(new BasicHttpParams()).execute(new HttpPost(this.downloadUrl)).getAllHeaders()) {
                if ("Content-Disposition".equals(h.getName())) {
                    this.apkName = h.getValue();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.apkName.substring(this.apkName.lastIndexOf("=") + 1);
    }

    private void init() {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(this.downloadUrl).openConnection();
            conn.setConnectTimeout(5000);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*");
            conn.setRequestProperty("Accept-Language", "zh-CN");
            conn.setRequestProperty("Referer", this.downloadUrl);
            conn.setRequestProperty("Charset", "UTF-8");
            conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.connect();
            if (conn.getResponseCode() == 200) {
                this.fileSize = conn.getContentLength();
                this.saveFile = new File(this.fileSaveDir, "/" + getApkName());
                return;
            }
            throw new RuntimeException("server no response ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int download() {
        try {
            this.downloadSize = this.lastPosition;
            URL downUrl = new URL(this.downloadUrl);
            HttpURLConnection http = (HttpURLConnection) downUrl.openConnection();
            http.setConnectTimeout(5000);
            http.setRequestMethod("GET");
            http.setRequestProperty("Accept", "image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*");
            http.setRequestProperty("Accept-Language", "zh-CN");
            http.setRequestProperty("Referer", downUrl.toString());
            http.setRequestProperty("Charset", "UTF-8");
            http.setRequestProperty("Range", "bytes=" + this.downloadSize + "-" + this.fileSize);
            http.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)");
            InputStream inStream = http.getInputStream();
            byte[] buffer = new byte[10240];
            RandomAccessFile threadfile = new RandomAccessFile(this.saveFile, "rwd");
            threadfile.seek((long) this.lastPosition);
            this.status = 2;
            while (true) {
                int offset = inStream.read(buffer);
                if (offset == -1) {
                    break;
                }
                threadfile.write(buffer, 0, offset);
                this.downloadSize += offset;
                if (!(this.listener == null || this.fileSize == 0)) {
                    int precent = (int) ((((double) this.downloadSize) / ((double) this.fileSize)) * 100.0d);
                    if (precent - this.downloadPercent >= 3 || precent == 100) {
                        if (precent == 100) {
                            this.status = 4;
                        }
                        this.downloadPercent = precent;
                        this.listener.onDownloadSize(this.status, precent);
                    }
                }
            }
            if (this.status == 4) {
                this.updateDBUtil.delete();
            }
            threadfile.close();
            inStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            this.status = -1;
            this.listener.onDownloadSize(this.status, -1);
        }
        return this.downloadSize;
    }
}
