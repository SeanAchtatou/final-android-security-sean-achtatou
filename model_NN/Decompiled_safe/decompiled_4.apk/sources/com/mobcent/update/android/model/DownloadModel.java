package com.mobcent.update.android.model;

public class DownloadModel {
    private int downloadLength;
    private String downloadUrl;
    private int lastPostion;

    public String getDownloadUrl() {
        return this.downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl2) {
        this.downloadUrl = downloadUrl2;
    }

    public int getLastPostion() {
        return this.lastPostion;
    }

    public void setLastPostion(int lastPostion2) {
        this.lastPostion = lastPostion2;
    }

    public int getDownloadLength() {
        return this.downloadLength;
    }

    public void setDownloadLength(int downloadLength2) {
        this.downloadLength = downloadLength2;
    }
}
