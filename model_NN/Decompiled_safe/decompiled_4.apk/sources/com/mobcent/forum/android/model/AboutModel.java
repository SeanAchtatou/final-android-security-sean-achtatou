package com.mobcent.forum.android.model;

public class AboutModel extends BaseModel {
    private static final long serialVersionUID = -2771477010129613265L;
    private String desc;
    private String email;
    private String qq;
    private String tel;
    private String website;
    private String weibo_qq;
    private String weibo_sina;

    public String getDesc() {
        return this.desc;
    }

    public void setDesc(String desc2) {
        this.desc = desc2;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public String getTel() {
        return this.tel;
    }

    public void setTel(String tel2) {
        this.tel = tel2;
    }

    public String getWebsite() {
        return this.website;
    }

    public void setWebsite(String website2) {
        this.website = website2;
    }

    public String getWeibo_qq() {
        return this.weibo_qq;
    }

    public void setWeibo_qq(String weibo_qq2) {
        this.weibo_qq = weibo_qq2;
    }

    public String getWeibo_sina() {
        return this.weibo_sina;
    }

    public void setWeibo_sina(String weibo_sina2) {
        this.weibo_sina = weibo_sina2;
    }

    public String getQq() {
        return this.qq;
    }

    public void setQq(String qq2) {
        this.qq = qq2;
    }
}
