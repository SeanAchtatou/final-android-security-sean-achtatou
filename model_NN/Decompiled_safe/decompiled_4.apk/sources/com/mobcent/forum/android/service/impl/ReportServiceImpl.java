package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.ReportRestfulApiRequester;
import com.mobcent.forum.android.model.ReportModel;
import com.mobcent.forum.android.service.ReportService;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.service.impl.helper.ReportServiceImplHelper;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class ReportServiceImpl implements ReportService {
    private Context context;

    public ReportServiceImpl(Context context2) {
        this.context = context2;
    }

    public String report(int type, long oid, String reason) {
        return BaseJsonHelper.formJsonRS(ReportRestfulApiRequester.report(this.context, type, oid, reason));
    }

    public List<ReportModel> getReportTopicList(int page, int pageSize) {
        String jsonStr = ReportRestfulApiRequester.getReportTopicManage(this.context, page, pageSize);
        List<ReportModel> reportList = ReportServiceImplHelper.getReportTopicList(jsonStr);
        if (reportList == null || reportList.size() == 0) {
            if (reportList == null) {
                reportList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                ReportModel reportModel = new ReportModel();
                reportModel.setErrorCode(errorCode);
                reportList.add(reportModel);
            }
        }
        return reportList;
    }

    public String cancelReportTopic(long reportId) {
        return BaseJsonHelper.formJsonRS(ReportRestfulApiRequester.cancelReportTopic(this.context, reportId));
    }

    public List<ReportModel> getReportUserList(int page, int pageSize) {
        String jsonStr = ReportRestfulApiRequester.getReportUserManage(this.context, page, pageSize);
        List<ReportModel> reportList = ReportServiceImplHelper.getReportTopicList(jsonStr);
        if (reportList == null || reportList.size() == 0) {
            if (reportList == null) {
                reportList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                ReportModel reportModel = new ReportModel();
                reportModel.setErrorCode(errorCode);
                reportList.add(reportModel);
            }
        }
        return reportList;
    }

    public String cancelReportUser(long reportId) {
        return BaseJsonHelper.formJsonRS(ReportRestfulApiRequester.cancelReportUser(this.context, reportId));
    }
}
