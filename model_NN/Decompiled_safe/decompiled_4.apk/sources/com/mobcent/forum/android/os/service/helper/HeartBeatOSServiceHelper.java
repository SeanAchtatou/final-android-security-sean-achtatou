package com.mobcent.forum.android.os.service.helper;

import android.content.Context;
import android.content.Intent;
import com.mobcent.forum.android.os.service.HeartMsgOSService;

public class HeartBeatOSServiceHelper {
    public static boolean heartBeatLive = false;

    public static void startHeartBeatService(Context context, int icon) {
        heartBeatLive = true;
        Intent intent = new Intent(context, HeartMsgOSService.class);
        intent.putExtra(HeartMsgOSService.APP_ICON_GET_INTENT, icon);
        context.startService(intent);
    }

    public static void stopHeartBeatService(Context context) {
        heartBeatLive = false;
        context.stopService(new Intent(context, HeartMsgOSService.class));
    }
}
