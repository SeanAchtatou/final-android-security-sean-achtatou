package com.mobcent.forum.android.db.helper;

import android.database.Cursor;
import com.mobcent.forum.android.model.UserInfoModel;

public class UserDBUtilHelper {
    public static UserInfoModel buildUserInfoModel(Cursor c) {
        UserInfoModel model = new UserInfoModel();
        model.setUserId(c.getLong(0));
        model.setEmail(c.getString(1));
        model.setPwd(c.getString(2));
        model.setNickname(c.getString(3));
        model.setUas(c.getString(4));
        model.setUat(c.getString(5));
        model.setIcon(c.getString(6));
        model.setGender(c.getInt(7));
        model.setScore(c.getInt(8));
        model.setCurrUser(c.getInt(9));
        model.setCredits(c.getInt(10));
        model.setSignature(c.getString(11));
        return model;
    }
}
