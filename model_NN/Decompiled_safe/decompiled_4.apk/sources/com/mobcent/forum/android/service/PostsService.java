package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.AnnoModel;
import com.mobcent.forum.android.model.BaseModel;
import com.mobcent.forum.android.model.PollItemModel;
import com.mobcent.forum.android.model.PostsModel;
import com.mobcent.forum.android.model.PostsNoticeModel;
import com.mobcent.forum.android.model.TopicContentModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.List;

public interface PostsService {
    String PhysicalDelTopic(long j);

    String createPublishPollTopicJson(List<String> list);

    String createPublishTopicJson(String str, String str2, String str3);

    String createPublishTopicJson(String str, String str2, String str3, String str4);

    String createPublishTopicJson(String str, String str2, String str3, List<UserInfoModel> list, String str4, int i);

    String createRepostTopicJson(TopicModel topicModel, String str, long j, long j2, String str2, String str3, String str4, List<UserInfoModel> list, String str5, int i);

    String deleteReply(long j, long j2);

    String deleteTopic(long j, long j2);

    AnnoModel getAnnoDetail(long j, long j2);

    List<PostsNoticeModel> getAtPostsNoticeList(int i, int i2);

    List<TopicModel> getEssenceTopicList(long j, int i, int i2);

    List<TopicModel> getHotTopicList(long j, int i, int i2);

    List<TopicModel> getHotTopicList(long j, int i, int i2, boolean z);

    List<TopicModel> getHotTopicListByNet(long j, int i, int i2);

    long getLastReplyRemindId();

    List<TopicModel> getLocalSurroudTopic();

    BaseModel getLongPic(String str, String str2, String str3);

    boolean getMentionFriendNotificationFlag();

    boolean getMessageVoiceFlag();

    List<TopicModel> getNetSurroudTopic(double d, double d2, int i, int i2, int i3);

    List<TopicModel> getPicTopicList(int i, int i2, boolean z);

    List<TopicModel> getPostTimeTopicList(long j, int i, int i2);

    List<TopicModel> getPostTimeTopicList(long j, int i, int i2, boolean z);

    List<TopicModel> getPostTimeTopicListByNet(long j, int i, int i2);

    List<PostsModel> getPosts(long j, long j2, int i, int i2);

    List<PostsModel> getPostsByDesc(long j, long j2, int i, int i2);

    List<PostsNoticeModel> getPostsNoticeList(long j, int i, int i2);

    List<TopicModel> getRelatedPosts(long j, int i, int i2);

    boolean getReplyNotificationFlag();

    List<TopicContentModel> getTopicContent(long j, long j2);

    List<TopicContentModel> getTopicContent(long j, long j2, long j3);

    List<TopicModel> getTopicList(long j, int i, int i2);

    List<TopicModel> getUserEssenceTopicList(long j, int i, int i2);

    List<PollItemModel> getUserPolls(long j, long j2, String str);

    List<PostsModel> getUserPosts(long j, long j2, long j3, int i, int i2);

    List<TopicModel> getUserReplyList(long j, int i, int i2);

    List<TopicModel> getUserTopicList(long j, int i, int i2);

    boolean isContainsPic(String str, String str2, String str3, String str4, int i);

    String publishAnnounce(long j, int i, String str, String str2, String str3, String str4, int i2);

    String publishPollTopic(long j, String str, String str2, int i, int i2, String str3, long j2);

    String publishPollTopic(long j, String str, String str2, int i, int i2, String str3, long j2, double d, double d2, String str4, int i3, int i4);

    String publishTopic(long j, String str, String str2, boolean z);

    String publishTopic(long j, String str, String str2, boolean z, double d, double d2, String str3, int i, int i2);

    String replyTopic(long j, long j2, String str, String str2, long j3);

    String replyTopic(long j, long j2, String str, String str2, long j3, boolean z, long j4);

    String replyTopic(long j, long j2, String str, String str2, long j3, boolean z, long j4, double d, double d2, String str3, int i, int i2);

    List<TopicModel> searchTopicList(long j, long j2, String str, int i, int i2);

    String setCloseTopic(long j, long j2, int i);

    String setEssenceTopic(long j, long j2, int i);

    void setMentionFriendNotificationFlag(boolean z);

    void setMessageVoiceFlag(boolean z);

    void setReplyNotificationFlag(boolean z);

    String setTopTopic(long j, long j2, int i);

    boolean shareTopic(long j, long j2);

    boolean updateAtReplyRemindState(long j, String str);

    void updateLastReplyRemindId(long j);

    String updateReply(long j, long j2, String str);

    boolean updateReplyRemindState(long j, String str);

    String updateTopic(long j, long j2, String str);

    String uploadAudio(String str);

    String uploadImage(String str, String str2);
}
