package com.mobcent.forum.android.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.GZIPInputStream;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class MCLibIOUtil {
    public static final String AUDIO = "audio";
    public static final String CACHE = "cache";
    public static final String DOWNLOAD = "download";
    public static final String FS = File.separator;
    public static final String IMAGE = "image";
    public static final String MOBCENT = "mobcent";
    public static final String UPDATE = "update";

    public static String getUrlResponse(String url) {
        try {
            return convertStreamToString(new DefaultHttpClient().execute(new HttpGet(url)).getEntity().getContent());
        } catch (Exception e) {
            return null;
        }
    }

    public static String unGzip(HttpEntity compressedEntity) throws IOException {
        if (compressedEntity == null) {
            return "";
        }
        String results = convertStreamToString(new GZIPInputStream(compressedEntity.getContent()));
        if (results == null) {
            return results;
        }
        if (results.trim().startsWith("{") && results.trim().endsWith("}")) {
            return results;
        }
        if (results.trim().startsWith("[") && results.trim().endsWith("]")) {
            return results;
        }
        try {
            return convertStreamToString(compressedEntity.getContent());
        } catch (IllegalStateException e) {
            return results;
        }
    }

    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                Log.e("convertStreamToString", "convertStreamToString error");
                try {
                    is.close();
                } catch (IOException e3) {
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    public static Bitmap getBitmapFromUrl(URL url) {
        InputStream in = null;
        OutputStream out = null;
        try {
            InputStream in2 = new BufferedInputStream(url.openStream(), 4096);
            try {
                ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
                OutputStream out2 = new BufferedOutputStream(dataStream, 4096);
                try {
                    copy(in2, out2);
                    out2.flush();
                    byte[] data = dataStream.toByteArray();
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, new BitmapFactory.Options());
                    closeStream(in2);
                    closeStream(out2);
                    return bitmap;
                } catch (IOException e) {
                    out = out2;
                    in = in2;
                    closeStream(in);
                    closeStream(out);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    out = out2;
                    in = in2;
                    closeStream(in);
                    closeStream(out);
                    throw th;
                }
            } catch (IOException e2) {
                in = in2;
                closeStream(in);
                closeStream(out);
                return null;
            } catch (Throwable th2) {
                th = th2;
                in = in2;
                closeStream(in);
                closeStream(out);
                throw th;
            }
        } catch (IOException e3) {
            closeStream(in);
            closeStream(out);
            return null;
        } catch (Throwable th3) {
            th = th3;
            closeStream(in);
            closeStream(out);
            throw th;
        }
    }

    public static Drawable getDrawableFromUrl(URL url) {
        try {
            return Drawable.createFromStream(url.openStream(), "src");
        } catch (IOException | MalformedURLException e) {
            return null;
        }
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] b = new byte[4096];
        while (true) {
            int read = in.read(b);
            if (read != -1) {
                out.write(b, 0, read);
            } else {
                return;
            }
        }
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }

    public static boolean saveFile(byte[] fileStream, String relativePath, String basePath) {
        try {
            ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
            int i = 0;
            while (i < fileStream.length) {
                try {
                    byteOutputStream.write(fileStream[i]);
                    i++;
                } catch (Exception e) {
                    System.gc();
                    return false;
                } catch (Throwable th) {
                    th = th;
                    System.gc();
                    throw th;
                }
            }
            FileOutputStream fileOutputStream = new FileOutputStream(String.valueOf(basePath) + FS + relativePath);
            try {
                fileOutputStream.write(byteOutputStream.toByteArray());
                byteOutputStream.close();
                fileOutputStream.close();
                System.gc();
                return true;
            } catch (Exception e2) {
                System.gc();
                return false;
            } catch (Throwable th2) {
                th = th2;
                System.gc();
                throw th;
            }
        } catch (Exception e3) {
            System.gc();
            return false;
        } catch (Throwable th3) {
            th = th3;
            System.gc();
            throw th;
        }
    }

    public static boolean saveFile(byte[] fileStream, String path) {
        try {
            ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
            int i = 0;
            while (i < fileStream.length) {
                try {
                    byteOutputStream.write(fileStream[i]);
                    i++;
                } catch (Exception e) {
                    return false;
                }
            }
            FileOutputStream fileOutputStream = new FileOutputStream(path);
            try {
                fileOutputStream.write(byteOutputStream.toByteArray());
                byteOutputStream.close();
                fileOutputStream.close();
                return true;
            } catch (Exception e2) {
                return false;
            }
        } catch (Exception e3) {
            return false;
        }
    }

    public static boolean saveImageFile(Context activity, Bitmap bitmap, Bitmap.CompressFormat compressFormat, String relativePath, String basePath) {
        if (bitmap == null) {
            return false;
        }
        File file = new File(String.valueOf(basePath) + FS + relativePath);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            try {
                bitmap.compress(compressFormat, 100, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e2) {
            return false;
        }
    }

    public static boolean saveImageFile(Context activity, Bitmap bitmap, Bitmap.CompressFormat compressFormat, String relativePath, String basePath, int quality) {
        if (bitmap == null) {
            return false;
        }
        File file = new File(String.valueOf(basePath) + FS + relativePath);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            try {
                bitmap.compress(compressFormat, quality, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e2) {
            return false;
        }
    }

    public static boolean getExternalStorageState() {
        String state = Environment.getExternalStorageState();
        if ("mounted".equals(state)) {
            return true;
        }
        if ("mounted_ro".equals(state)) {
            return false;
        }
        return false;
    }

    public static String getSDCardPath() {
        return Environment.getExternalStorageDirectory().toString();
    }

    public static boolean isFileExist(String path) {
        File file = new File(path);
        return file.exists() && file.isFile();
    }

    public static boolean isDirExist(String path) {
        File file = new File(path);
        return file.exists() && file.isDirectory();
    }

    public static boolean makeDirs(String path) {
        return new File(path).mkdirs();
    }

    public static String getBaseLocalLocation(Context context) {
        if (getExternalStorageState()) {
            return getSDCardPath();
        }
        return context.getFilesDir().getAbsolutePath();
    }

    public static int getFileSize(File file) {
        IOException e;
        FileNotFoundException e2;
        int size = 0;
        try {
            try {
                size = new FileInputStream(file).available();
            } catch (FileNotFoundException e3) {
                e2 = e3;
                e2.printStackTrace();
                return size;
            } catch (IOException e4) {
                e = e4;
                e.printStackTrace();
                return size;
            }
        } catch (FileNotFoundException e5) {
            e2 = e5;
            e2.printStackTrace();
            return size;
        } catch (IOException e6) {
            e = e6;
            e.printStackTrace();
            return size;
        }
        return size;
    }

    public static boolean addToSysGallery(Activity activity, String fileAbsolutePath, String fileName) {
        if (activity == null) {
            return false;
        }
        try {
            MediaStore.Images.Media.insertImage(activity.getContentResolver(), fileAbsolutePath, fileName, fileName);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String createFileDir(String path) {
        if (isDirExist(path) || makeDirs(path)) {
            return path;
        }
        return null;
    }

    public static String getCachePath(Context context) {
        String basePath = getBaseLocalLocation(context);
        String path = String.valueOf(basePath) + FS + "mobcent" + FS + AppUtil.getPackageName(context) + FS + "cache" + FS;
        createFileDir(path);
        return path;
    }

    public static String getDownPath(Context context) {
        String basePath = getBaseLocalLocation(context);
        String path = String.valueOf(basePath) + FS + "mobcent" + FS + AppUtil.getPackageName(context) + FS + "download" + FS;
        createFileDir(path);
        return path;
    }

    public static String createDirInCache(Context context, String dirName) {
        String path = String.valueOf(getCachePath(context)) + dirName + FS;
        createFileDir(path);
        return path;
    }

    public static String createDirInDown(Context context, String dirName) {
        String path = String.valueOf(getDownPath(context)) + dirName + FS;
        createFileDir(path);
        return path;
    }

    public static String getImageCachePath(Context context) {
        return createDirInCache(context, "image");
    }

    public static String getImageCachePathByApp(Context context) {
        String basePath = context.getFilesDir().getAbsolutePath();
        return String.valueOf(basePath) + FS + "mobcent" + FS + AppUtil.getPackageName(context) + FS + "cache" + FS + "image" + FS;
    }

    public static String getAudioCachePath(Context context) {
        return createDirInCache(context, "audio");
    }

    public static String getUpdatePath(Context context) {
        return createDirInCache(context, "update");
    }

    public static boolean moveFile(String srcPath, String destPath) {
        return new File(srcPath).renameTo(new File(destPath));
    }
}
