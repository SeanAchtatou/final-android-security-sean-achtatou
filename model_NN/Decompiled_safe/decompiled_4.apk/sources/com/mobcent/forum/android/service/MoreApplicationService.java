package com.mobcent.forum.android.service;

import android.content.Context;
import com.mobcent.forum.android.model.MoreApplicationModel;
import java.util.List;

public interface MoreApplicationService {
    List<MoreApplicationModel> getMoreApplicationList(Context context, long j);
}
