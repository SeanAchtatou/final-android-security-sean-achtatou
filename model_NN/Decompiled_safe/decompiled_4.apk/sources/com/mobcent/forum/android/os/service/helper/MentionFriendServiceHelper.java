package com.mobcent.forum.android.os.service.helper;

import android.content.Context;
import android.content.Intent;
import com.mobcent.forum.android.os.service.MentionFriendsOSService;

public class MentionFriendServiceHelper {
    public static void startMentionFriendService(Context context) {
        try {
            context.stopService(new Intent(context, MentionFriendsOSService.class));
        } catch (Exception e) {
        }
        context.startService(new Intent(context, MentionFriendsOSService.class));
    }

    public static void stopMentionFriendService(Context context) {
        context.stopService(new Intent(context, MentionFriendsOSService.class));
    }
}
