package com.mobcent.forum.android.application;

import android.app.Activity;
import android.app.Application;
import android.os.Process;
import com.mobcent.forum.android.exception.CrashHandler;
import java.util.ArrayList;
import java.util.List;

public class McForumApplication extends Application {
    private static McForumApplication instance;
    private List<Activity> listActivities;

    public static McForumApplication getInstance() {
        return instance;
    }

    public void onCreate() {
        super.onCreate();
        CrashHandler.getInstance().init(getApplicationContext());
        instance = this;
        this.listActivities = new ArrayList();
    }

    public List<Activity> getList() {
        return this.listActivities;
    }

    public void addAcitvity(Activity activity) {
        this.listActivities.add(activity);
    }

    public void remove(Activity activity) {
        this.listActivities.remove(activity);
    }

    public void finishAllActivity() {
        for (Activity activity : this.listActivities) {
            activity.finish();
        }
        Process.killProcess(Process.myPid());
    }
}
