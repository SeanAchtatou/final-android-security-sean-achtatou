package com.mobcent.forum.android.os.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.mobcent.forum.android.service.MentionFriendService;
import com.mobcent.forum.android.service.ModeratorService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.MentionFriendServiceImpl;
import com.mobcent.forum.android.service.impl.ModeratorServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;

public class MentionFriendsOSService extends Service {
    private MentionFriendService mentionFriendService;
    private ModeratorService moderatorService;
    private UserService userService;

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        new Thread(new Runnable() {
            public void run() {
                MentionFriendsOSService.this.mentionFriend();
                MentionFriendsOSService.this.stopSelf();
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public synchronized void mentionFriend() {
        this.mentionFriendService = new MentionFriendServiceImpl(this);
        this.moderatorService = new ModeratorServiceImpl(this);
        this.userService = new UserServiceImpl(this);
        if (this.userService.isLogin()) {
            this.mentionFriendService.getForumMentionFriendByNet(this.userService.getLoginUserId());
            this.moderatorService.getModeratorBoardList(this.userService.getLoginUserId(), false);
        }
    }
}
