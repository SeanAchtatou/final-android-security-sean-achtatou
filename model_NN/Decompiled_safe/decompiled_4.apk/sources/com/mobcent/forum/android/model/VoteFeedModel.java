package com.mobcent.forum.android.model;

public class VoteFeedModel extends BaseModel {
    private static final long serialVersionUID = 3909384665802284537L;
    private String icon;
    private long userId;
    private String userNickName;
    private String[] votes;

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
    }

    public String getUserNickName() {
        return this.userNickName;
    }

    public void setUserNickName(String userNickName2) {
        this.userNickName = userNickName2;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public String[] getVotes() {
        return this.votes;
    }

    public void setVotes(String[] votes2) {
        this.votes = votes2;
    }
}
