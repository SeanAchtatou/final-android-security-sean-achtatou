package com.mobcent.forum.android.service.impl.helper;

import android.content.Context;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.db.PermDBUtil;
import com.mobcent.forum.android.model.PermModel;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class PermServiceImplHelper extends BaseJsonHelper implements PermConstant {
    public static PermModel getPermission(Context context, String jsonStr) {
        Exception e;
        PermModel permModel = null;
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            if (!jsonStr.contains("rs")) {
                return null;
            }
            String errorCode = formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                PermModel permModel2 = new PermModel();
                try {
                    permModel2.setErrorCode(errorCode);
                    permModel = permModel2;
                } catch (Exception e2) {
                    e = e2;
                    permModel = permModel2;
                    e.printStackTrace();
                    return permModel;
                }
            } else if (1 != jsonObject.optInt("rs")) {
                return null;
            } else {
                JSONObject permObject = jsonObject.optJSONObject("perm");
                if (permObject == null) {
                    return null;
                }
                PermDBUtil.getInstance(context).updatePermJsonString(permObject.toString());
                permModel = getPermissionModel(permObject);
            }
            return permModel;
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return permModel;
        }
    }

    public static PermModel getPermissionModel(JSONObject permObject) {
        PermModel permModel = new PermModel();
        try {
            JSONObject userGroupObject = permObject.optJSONObject(PermConstant.USER_GROUP);
            JSONArray boardsArray = permObject.optJSONArray(PermConstant.BOARDS);
            if (userGroupObject != null) {
                PermModel userGroupModel = new PermModel();
                userGroupModel.setGroupId(userGroupObject.optInt(PermConstant.GROUP_ID));
                userGroupModel.setGroupType(userGroupObject.optString(PermConstant.GROUP_TYPE));
                userGroupModel.setVisit(userGroupObject.optInt(PermConstant.VISIT));
                userGroupModel.setPost(userGroupObject.optInt(PermConstant.POST));
                userGroupModel.setRead(userGroupObject.optInt(PermConstant.READ));
                userGroupModel.setReply(userGroupObject.optInt("reply"));
                userGroupModel.setUpload(userGroupObject.optInt(PermConstant.UPLOAD));
                userGroupModel.setDownload(userGroupObject.optInt("download"));
                permModel.setUserGroup(userGroupModel);
            }
            if (boardsArray != null) {
                List<PermModel> boards = new ArrayList<>();
                for (int i = 0; i < boardsArray.length(); i++) {
                    PermModel boardModel = new PermModel();
                    JSONObject boardObject = boardsArray.optJSONObject(i);
                    boardModel.setBoardId(boardObject.optLong("board_id"));
                    boardModel.setVisit(boardObject.optInt(PermConstant.VISIT));
                    boardModel.setPost(boardObject.optInt(PermConstant.POST));
                    boardModel.setRead(boardObject.optInt(PermConstant.READ));
                    boardModel.setReply(boardObject.optInt("reply"));
                    boardModel.setUpload(boardObject.optInt(PermConstant.UPLOAD));
                    boardModel.setDownload(boardObject.optInt("download"));
                    boards.add(boardModel);
                }
                permModel.setBoards(boards);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return permModel;
    }
}
