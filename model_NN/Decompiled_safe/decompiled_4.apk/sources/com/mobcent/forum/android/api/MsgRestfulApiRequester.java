package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.constant.MsgConstant;
import java.util.HashMap;

public class MsgRestfulApiRequester extends BaseRestfulApiRequester implements MsgConstant {
    public static String getMsgUserList(Context context, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("message/getMsgUserList.do", params, context);
    }

    public static String updateMessage(Context context, String messageRelationIds, long currUserId) {
        HashMap<String, String> params = new HashMap<>();
        params.put(MsgConstant.SEND_MESSAGE_RELATION_IDS, messageRelationIds);
        params.put("userId", new StringBuilder(String.valueOf(currUserId)).toString());
        return doPostRequest("message/updateMessage.do", params, context);
    }

    public static String getUnReadMessage(Context context, long fromUserId) {
        HashMap<String, String> params = new HashMap<>();
        params.put(MsgConstant.SEND_FROM_USER_ID, new StringBuilder(String.valueOf(fromUserId)).toString());
        return doPostRequest("message/UnMessage.do", params, context);
    }

    public static String getHistoryMessage(Context context, long fromUserId, int page, int pageSize, long unMessageRelationId) {
        HashMap<String, String> params = new HashMap<>();
        params.put(MsgConstant.SEND_FROM_USER_ID, new StringBuilder(String.valueOf(fromUserId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        params.put(MsgConstant.SEND_UN_MESSAGE_RELATION_ID, new StringBuilder(String.valueOf(unMessageRelationId)).toString());
        return doPostRequest("message/historyMessage.do", params, context);
    }

    public static String sendMessage(Context context, long toUserId, String content, int type, long audioTme) {
        HashMap<String, String> params = new HashMap<>();
        params.put(MsgConstant.SEND_TO_USER_ID, new StringBuilder(String.valueOf(toUserId)).toString());
        params.put("content", new StringBuilder(String.valueOf(content)).toString());
        params.put("type", new StringBuilder(String.valueOf(type)).toString());
        if (audioTme > 0) {
            params.put("time", new StringBuilder(String.valueOf(audioTme)).toString());
        }
        return doPostRequest("message/sendMessage.do", params, context);
    }

    public static String setUserBlackList(Context context, long userId, long toUserId, int state) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MsgConstant.BLACK_TO_USER_ID, new StringBuilder(String.valueOf(toUserId)).toString());
        params.put("state", new StringBuilder(String.valueOf(state)).toString());
        return doPostRequest("user/setUserBlackList.do", params, context);
    }
}
