package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.forum.android.db.constant.SurrroundUserDBConstant;
import com.mobcent.forum.android.util.DateUtil;

public class SurroundUserDBUtil extends BaseDBUtil implements SurrroundUserDBConstant {
    private static SurroundUserDBUtil surroundFriendsDBUtil;

    protected SurroundUserDBUtil(Context ctx) {
        super(ctx);
    }

    public static SurroundUserDBUtil getInstance(Context context) {
        if (surroundFriendsDBUtil == null) {
            surroundFriendsDBUtil = new SurroundUserDBUtil(context);
        }
        return surroundFriendsDBUtil;
    }

    public String getSurroundUserJsonString(long userId) {
        String jsonStr = null;
        Cursor cursor = null;
        try {
            openReadableDB();
            cursor = this.readableDatabase.query(SurrroundUserDBConstant.TABLE_SURROUND_FRIENDS, null, "id=" + userId, null, null, null, null);
            while (cursor.moveToNext()) {
                jsonStr = cursor.getString(1);
            }
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            jsonStr = null;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
            throw th;
        }
        return jsonStr;
    }

    public boolean updateSurroundUserJsonString(String jsonStr, long userId) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("id", Long.valueOf(userId));
            values.put("jsonStr", jsonStr);
            values.put("update_time", Long.valueOf(DateUtil.getCurrentTime()));
            if (!isRowExisted(this.writableDatabase, SurrroundUserDBConstant.TABLE_SURROUND_FRIENDS, "id", userId)) {
                this.writableDatabase.insertOrThrow(SurrroundUserDBConstant.TABLE_SURROUND_FRIENDS, null, values);
            } else {
                this.writableDatabase.update(SurrroundUserDBConstant.TABLE_SURROUND_FRIENDS, values, "id=" + userId, null);
            }
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean deleteSurroundUserList() {
        return removeAllEntries(SurrroundUserDBConstant.TABLE_SURROUND_FRIENDS);
    }
}
