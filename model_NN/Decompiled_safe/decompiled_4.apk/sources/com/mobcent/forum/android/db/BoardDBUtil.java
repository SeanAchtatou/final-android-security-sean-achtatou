package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.forum.android.db.constant.BoardDBConstant;

public class BoardDBUtil extends BaseDBUtil implements BoardDBConstant {
    private static final int BOARD_ID = 1;
    private static String SQL_SELECT_BOARD = "SELECT * FROM board";
    private static BoardDBUtil boardDBUtil;

    public BoardDBUtil(Context context) {
        super(context);
    }

    public static BoardDBUtil getInstance(Context context) {
        if (boardDBUtil == null) {
            boardDBUtil = new BoardDBUtil(context);
        }
        return boardDBUtil;
    }

    public String getBoardJsonString() {
        String jsonStr = null;
        Cursor cursor = null;
        try {
            openReadableDB();
            cursor = this.readableDatabase.rawQuery(SQL_SELECT_BOARD, null);
            while (cursor.moveToNext()) {
                jsonStr = cursor.getString(1);
            }
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
            throw th;
        }
        return jsonStr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean updateBoardJsonString(String jsonStr) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("jsonStr", jsonStr);
            values.put("id", (Integer) 1);
            if (!isRowExisted(this.writableDatabase, BoardDBConstant.TABLE_BOARD, "id", 1)) {
                this.writableDatabase.insertOrThrow(BoardDBConstant.TABLE_BOARD, null, values);
            } else {
                this.writableDatabase.update(BoardDBConstant.TABLE_BOARD, values, "id=1", null);
            }
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean delteBoardList() {
        return removeAllEntries(BoardDBConstant.TABLE_BOARD);
    }
}
