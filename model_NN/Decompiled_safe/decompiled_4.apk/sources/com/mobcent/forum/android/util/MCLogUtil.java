package com.mobcent.forum.android.util;

import android.os.Build;
import android.util.Log;

public class MCLogUtil {
    public static int DEBUG = 2;
    public static int ERROR = 5;
    public static int INFO = 3;
    public static int VERBOSE = 1;
    public static int WARN = 4;
    private static boolean isLog = false;
    private static int level = DEBUG;

    public static void i(String tag, String msg) {
        if (isPrintLog() && level <= INFO) {
            Log.i(tag, String.valueOf(getMsgTag(tag)) + msg);
        }
    }

    public static void d(String tag, String msg) {
        if (isPrintLog() && level <= DEBUG) {
            Log.d(tag, String.valueOf(getMsgTag(tag)) + msg);
        }
    }

    public static void w(String tag, String msg) {
        if (isPrintLog() && level <= WARN) {
            Log.w(tag, String.valueOf(getMsgTag(tag)) + msg);
        }
    }

    public static void e(String tag, String msg) {
        if (isPrintLog() && level <= ERROR) {
            Log.e(tag, String.valueOf(getMsgTag(tag)) + msg);
        }
    }

    public static void v(String tag, String msg) {
        if (isPrintLog() && level <= VERBOSE) {
            Log.v(tag, String.valueOf(getMsgTag(tag)) + msg);
        }
    }

    public static boolean isPrintLog() {
        return isLog;
    }

    private static String getMsgTag(String tag) {
        if (Build.VERSION.SDK_INT > 15) {
            return "mclog >> " + tag + " >> ";
        }
        return "";
    }
}
