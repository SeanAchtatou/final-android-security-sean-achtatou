package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.MentionFriendRestfulApiRequester;
import com.mobcent.forum.android.db.MentionFriendDBUtil;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.MentionFriendService;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.service.impl.helper.MentionFriendServiceImplHelper;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class MentionFriendServiceImpl implements MentionFriendService {
    /* access modifiers changed from: private */
    public Context context;

    public MentionFriendServiceImpl(Context context2) {
        this.context = context2;
    }

    public List<UserInfoModel> getForumMentionFriendByNet(long userId) {
        final String jsonStr = MentionFriendRestfulApiRequester.getForumMentionFriend(this.context, userId);
        List<UserInfoModel> mentionFriendList = MentionFriendServiceImplHelper.parseMentionFriendListJson(jsonStr);
        if (mentionFriendList == null) {
            mentionFriendList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                UserInfoModel userFriendModel = new UserInfoModel();
                userFriendModel.setErrorCode(errorCode);
                mentionFriendList.add(userFriendModel);
            }
        } else {
            final long lastUpdateTime = Calendar.getInstance().getTimeInMillis();
            final long user = userId;
            new Thread() {
                public void run() {
                    MentionFriendDBUtil.getInstance(MentionFriendServiceImpl.this.context).updateMentionFriendJsonString(jsonStr, 0, user, lastUpdateTime);
                }
            }.start();
        }
        return mentionFriendList;
    }

    public List<UserInfoModel> getFroumMentionFriend(long userId) {
        String jsonStr;
        try {
            jsonStr = MentionFriendDBUtil.getInstance(this.context).getMentionFriendJsonString(userId);
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
        }
        if (StringUtil.isEmpty(jsonStr)) {
            return getForumMentionFriendByNet(userId);
        }
        return MentionFriendServiceImplHelper.parseMentionFriendListJson(jsonStr);
    }

    public List<UserInfoModel> getOpenPlatformMentionFriendByNet(long platformId, long userId) {
        final String jsonStr = MentionFriendRestfulApiRequester.getOpenPlatformMentionFriend(this.context, platformId, userId);
        List<UserInfoModel> mentionFriendList = MentionFriendServiceImplHelper.parseMentionFriendListJson(jsonStr);
        if (mentionFriendList == null) {
            mentionFriendList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                UserInfoModel userFriendModel = new UserInfoModel();
                userFriendModel.setErrorCode(errorCode);
                mentionFriendList.add(userFriendModel);
            }
        } else {
            final long lastUpdateTime = Calendar.getInstance().getTimeInMillis();
            final long j = platformId;
            final long j2 = userId;
            new Thread() {
                public void run() {
                    MentionFriendDBUtil.getInstance(MentionFriendServiceImpl.this.context).updateMentionFriendJsonString(jsonStr, j, j2, lastUpdateTime);
                }
            }.start();
        }
        return mentionFriendList;
    }

    public List<UserInfoModel> getOpenPlatformMentionFriend(long platformId, long userId) {
        String jsonStr;
        try {
            jsonStr = MentionFriendDBUtil.getInstance(this.context).getMentionFriendJsonString(userId);
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
        }
        if (StringUtil.isEmpty(jsonStr)) {
            return getOpenPlatformMentionFriendByNet(platformId, userId);
        }
        return MentionFriendServiceImplHelper.parseMentionFriendListJson(jsonStr);
    }

    public boolean addLocalForumMentionFriend(long userId, UserInfoModel userInfo) {
        try {
            JSONArray array = new JSONObject(MentionFriendDBUtil.getInstance(this.context).getMentionFriendJsonString(userId)).getJSONArray("list");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", userInfo.getNickname());
            jsonObject.put("uid", userInfo.getUserId());
            jsonObject.put("role_num", 2);
            array.put(jsonObject);
            JSONObject jsonObject2 = new JSONObject();
            jsonObject2.put("list", array);
            MentionFriendDBUtil.getInstance(this.context).updateMentionFriendJsonString(jsonObject2.toString(), 0, userId, Calendar.getInstance().getTimeInMillis());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean deletedLocalForumMentionFriend(long userId, UserInfoModel userInfo) {
        try {
            JSONArray array = new JSONObject(MentionFriendDBUtil.getInstance(this.context).getMentionFriendJsonString(userId)).getJSONArray("list");
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = (JSONObject) array.get(i);
                if (Long.valueOf(jsonObject.optLong("uid")).longValue() != userInfo.getUserId()) {
                    jsonArray.put(jsonObject);
                }
            }
            JSONObject jsonObject2 = new JSONObject();
            jsonObject2.put("list", jsonArray);
            MentionFriendDBUtil.getInstance(this.context).updateMentionFriendJsonString(jsonObject2.toString(), 0, userId, Calendar.getInstance().getTimeInMillis());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
