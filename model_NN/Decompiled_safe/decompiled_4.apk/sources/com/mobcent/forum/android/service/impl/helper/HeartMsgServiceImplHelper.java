package com.mobcent.forum.android.service.impl.helper;

import android.content.Context;
import com.baidu.location.LocationClientOption;
import com.mobcent.forum.android.constant.BaseRestfulApiConstant;
import com.mobcent.forum.android.constant.HeartMsgConstant;
import com.mobcent.forum.android.constant.PostsApiConstant;
import com.mobcent.forum.android.constant.UserConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.HeartMsgModel;
import com.mobcent.forum.android.model.PushMessageModel;
import com.mobcent.forum.android.model.SoundModel;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HeartMsgServiceImplHelper implements UserConstant, HeartMsgConstant, BaseRestfulApiConstant {
    /* JADX INFO: Multiple debug info for r4v4 int: [D('heartMsgModel' com.mobcent.forum.android.model.HeartMsgModel), D('i' int)] */
    public static List<HeartMsgModel> parseHeartMsgJson(Context context, String jsonStr, long currUserId) {
        List<HeartMsgModel> heartMsgList;
        List<HeartMsgModel> arrayList = new ArrayList<>();
        try {
            JSONObject jSONObject = new JSONObject(jsonStr);
            if (jSONObject.optInt("rs") == 0) {
                List<HeartMsgModel> list = arrayList;
                return null;
            }
            int replyNoticeNum = jSONObject.optInt("reply_notice_num");
            int msgTotalNum = jSONObject.optInt("msg_total_num");
            int relationalNoticeNum = jSONObject.optInt("relational_notice_num");
            String iconUrl = jSONObject.optString("icon_url");
            int hbTime = jSONObject.optInt(HeartMsgConstant.HB_TIME) * LocationClientOption.MIN_SCAN_SPAN;
            JSONArray jsonArray = jSONObject.optJSONArray("list");
            int len = jsonArray.length();
            for (int i = 0; i < len; i++) {
                JSONObject obj = jsonArray.optJSONObject(i);
                HeartMsgModel heartMsgModel = new HeartMsgModel();
                heartMsgModel.setIconUrl(iconUrl);
                heartMsgModel.setFormUserId(obj.optLong(HeartMsgConstant.FROM_USER_ID));
                heartMsgModel.setFromUserNickname(obj.optString(HeartMsgConstant.FROM_USER_NICKNAME));
                heartMsgModel.setFromUserIcon(obj.optString("icon"));
                heartMsgModel.setToUserId(currUserId);
                heartMsgModel.setToUserIcon("");
                heartMsgModel.setContent(obj.optString("content"));
                heartMsgModel.setCreateDate(obj.optLong("create_date"));
                heartMsgModel.setMsgId(obj.optLong(HeartMsgConstant.MESSAGE_ID));
                heartMsgModel.setType(obj.optInt("type"));
                if (obj.optInt("type") == 3) {
                    SoundModel soundModel = new SoundModel();
                    soundModel.setSoundTime((long) obj.optInt("time"));
                    soundModel.setSoundPath(obj.optString("content"));
                    heartMsgModel.setSoundModel(soundModel);
                }
                arrayList.add(heartMsgModel);
            }
            int pushMsgNum = 0;
            if (jSONObject.optString("push") != null && !jSONObject.optString("push").equals("")) {
                SharedPreferencesDB.getInstance(context).setPushMsgListJson(jSONObject.optString("push"));
                pushMsgNum = 1;
            }
            if (arrayList.size() == 0) {
                HeartMsgModel heartBeatModel = new HeartMsgModel();
                heartBeatModel.setHbTime(hbTime);
                heartBeatModel.setMsgTotalNum(msgTotalNum);
                heartBeatModel.setReplyNoticeNum(replyNoticeNum);
                heartBeatModel.setRelationalNoticeNum(relationalNoticeNum);
                heartBeatModel.setPushMessageJson(jSONObject.optString("push"));
                heartBeatModel.setPushMsgNum(pushMsgNum);
                arrayList.add(heartBeatModel);
                heartMsgList = arrayList;
            } else if (arrayList.size() > 0) {
                HeartMsgModel heartBeatModel2 = (HeartMsgModel) arrayList.get(0);
                heartBeatModel2.setHbTime(hbTime);
                heartBeatModel2.setMsgTotalNum(msgTotalNum);
                heartBeatModel2.setReplyNoticeNum(replyNoticeNum);
                heartBeatModel2.setRelationalNoticeNum(relationalNoticeNum);
                heartBeatModel2.setPushMsgNum(pushMsgNum);
                heartBeatModel2.setPushMessageJson(jSONObject.optString("push"));
                heartMsgList = arrayList;
            } else {
                heartMsgList = arrayList;
            }
            return heartMsgList;
        } catch (Exception e) {
            e.printStackTrace();
            heartMsgList = null;
        }
    }

    public static List<PushMessageModel> getPushMsgList(String json) {
        List<PushMessageModel> pushMessageList = new ArrayList<>();
        try {
            JSONArray pushArray = new JSONArray(json);
            for (int i = 0; i < pushArray.length(); i++) {
                JSONObject jsonObject = pushArray.optJSONObject(i);
                PushMessageModel pushMessageModel = new PushMessageModel();
                pushMessageModel.setPushMsgId(jsonObject.optLong(HeartMsgConstant.PUSH_MSG_ID));
                pushMessageModel.setPushType(jsonObject.optInt("type"));
                pushMessageModel.setTitle(jsonObject.optString("title"));
                pushMessageModel.setPushDesc(jsonObject.optString("desc"));
                pushMessageModel.setTopicId(jsonObject.optLong("topic_id"));
                pushMessageModel.setPushDetailType(jsonObject.optInt(HeartMsgConstant.PUSH_DETAIL_TYPE));
                pushMessageList.add(pushMessageModel);
            }
            return pushMessageList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static HeartMsgModel parseHeatJson(String jsonStr) {
        Exception e;
        HeartMsgModel heartMsgModel;
        try {
            if (jsonStr.equals("")) {
                return null;
            }
            HeartMsgModel heartMsgModel2 = new HeartMsgModel();
            try {
                JSONObject jsonObj = new JSONObject(jsonStr);
                heartMsgModel2.setIconUrl(jsonObj.optString("icon_url"));
                heartMsgModel2.setFormUserId(jsonObj.optLong(HeartMsgConstant.FROM_USER_ID));
                heartMsgModel2.setFromUserNickname(jsonObj.optString(HeartMsgConstant.FROM_USER_NICKNAME));
                heartMsgModel2.setFromUserIcon(jsonObj.optString("icon"));
                heartMsgModel2.setToUserId(jsonObj.optLong("to_user_id"));
                heartMsgModel2.setToUserIcon(jsonObj.optString("icon"));
                heartMsgModel2.setContent(jsonObj.optString("content"));
                heartMsgModel2.setCreateDate(jsonObj.optLong("create_date"));
                heartMsgModel2.setMsgId(jsonObj.optLong(HeartMsgConstant.MESSAGE_ID));
                heartMsgModel2.setType(jsonObj.optInt("type"));
                if (jsonObj.optInt("type") == 3) {
                    SoundModel soundModel = new SoundModel();
                    soundModel.setSoundTime((long) jsonObj.optInt("time"));
                    soundModel.setSoundPath(jsonObj.optString("content"));
                    heartMsgModel2.setSoundModel(soundModel);
                    heartMsgModel = heartMsgModel2;
                } else {
                    heartMsgModel = heartMsgModel2;
                }
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                heartMsgModel = null;
                return heartMsgModel;
            }
            return heartMsgModel;
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            heartMsgModel = null;
            return heartMsgModel;
        }
    }

    public static String createHeartMsgJson(HeartMsgModel heartMsgModel) {
        if (heartMsgModel == null) {
            return null;
        }
        try {
            JSONObject jsonObj = new JSONObject();
            try {
                jsonObj.put("icon_url", heartMsgModel.getIconUrl());
                jsonObj.put(HeartMsgConstant.FROM_USER_ID, heartMsgModel.getFormUserId());
                jsonObj.put(HeartMsgConstant.FROM_USER_NICKNAME, heartMsgModel.getFromUserNickname());
                jsonObj.put("icon", heartMsgModel.getFromUserIcon());
                jsonObj.put("to_user_id", heartMsgModel.getToUserId());
                jsonObj.put("icon", heartMsgModel.getToUserIcon());
                jsonObj.put("content", heartMsgModel.getContent());
                jsonObj.put("create_date", heartMsgModel.getCreateDate());
                jsonObj.put(HeartMsgConstant.MESSAGE_ID, heartMsgModel.getMsgId());
                jsonObj.put("type", heartMsgModel.getType());
                if (heartMsgModel.getSoundModel() != null) {
                    jsonObj.put("time", heartMsgModel.getSoundModel().getSoundTime());
                } else {
                    jsonObj.put("time", "");
                }
                return jsonObj.toString();
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r13v1 int: [D('pageSize' int), D('j' int)] */
    public static List<UserInfoModel> getMsgUserList(String jsonStr, int page, int pageSize) {
        int totalNum;
        int totalNum2;
        int totalNum3 = page * pageSize;
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            List<UserInfoModel> userInfoList = new ArrayList<>();
            String iconUrl = jsonObject.optString("icon_url");
            int hasNextPage = jsonObject.optInt("has_next");
            if (hasNextPage > 0) {
                totalNum2 = (page + 1) * pageSize;
            } else {
                totalNum2 = totalNum3;
            }
            try {
                JSONArray jsonArray = jsonObject.optJSONArray("list");
                int j = jsonArray.length();
                for (int i = 0; i < j; i++) {
                    JSONObject jobj = jsonArray.optJSONObject(i);
                    UserInfoModel userInfoModel = new UserInfoModel();
                    userInfoModel.setUserId(jobj.optLong("uid"));
                    userInfoModel.setBlackStatus(jobj.optInt(UserConstant.IS_BLACK_USER));
                    userInfoModel.setNickname(jobj.optString("name"));
                    userInfoModel.setIcon(String.valueOf(iconUrl) + jobj.optString("icon"));
                    userInfoList.add(userInfoModel);
                }
                if (userInfoList.size() > 0) {
                    UserInfoModel firstUserInfoModel = (UserInfoModel) userInfoList.get(0);
                    if (page == 1 && hasNextPage == 0) {
                        firstUserInfoModel.setTotalNum(userInfoList.size());
                    } else {
                        firstUserInfoModel.setTotalNum(totalNum2);
                    }
                    firstUserInfoModel.setPage(page);
                    firstUserInfoModel.setRs(jsonObject.optInt("rs"));
                    userInfoList.set(0, firstUserInfoModel);
                }
                int i2 = totalNum2;
                return userInfoList;
            } catch (Exception e) {
                totalNum = totalNum2;
                return null;
            }
        } catch (Exception e2) {
            totalNum = totalNum3;
        }
    }

    public static boolean updateMessage(String jsonStr) {
        try {
            if (new JSONObject(jsonStr).optInt("rs") == 0) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean sendMessage(String jsonStr) {
        try {
            if (new JSONObject(jsonStr).optInt("rs") == 0) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean parseSetUserBlack(String jsonStr) {
        try {
            if (new JSONObject(jsonStr).optInt("rs") == 0) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String parseUploadAudioJson(String jsonStr) {
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.optInt("rs") == 0) {
                return null;
            }
            return String.valueOf(jsonObj.optString(BaseRestfulApiConstant.SOUND_URL)) + jsonObj.optString(PostsApiConstant.SOUND_PATH);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
