package com.mobcent.forum.android.util;

public class MD5Util {
    static final char[] Hex = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    static final byte[] PADDING;
    static final int S11 = 7;
    static final int S12 = 12;
    static final int S13 = 17;
    static final int S14 = 22;
    static final int S21 = 5;
    static final int S22 = 9;
    static final int S23 = 14;
    static final int S24 = 20;
    static final int S31 = 4;
    static final int S32 = 11;
    static final int S33 = 16;
    static final int S34 = 23;
    static final int S41 = 6;
    static final int S42 = 10;
    static final int S43 = 15;
    static final int S44 = 21;
    private byte[] buffer = new byte[64];
    private long[] count = new long[2];
    private byte[] digest = new byte[16];
    public String digestHexStr;
    private long[] state = new long[4];

    static {
        byte[] bArr = new byte[64];
        bArr[0] = Byte.MIN_VALUE;
        PADDING = bArr;
    }

    public String getMD5ofStr(String s) {
        md5Init();
        md5Update(s.getBytes(), s.length());
        md5Final();
        this.digestHexStr = "";
        for (int i = 0; i < 16; i++) {
            this.digestHexStr = String.valueOf(this.digestHexStr) + byteHEX(this.digest[i]);
        }
        return this.digestHexStr;
    }

    public MD5Util() {
        md5Init();
    }

    private void md5Init() {
        this.count[0] = 0;
        this.count[1] = 0;
        this.state[0] = 1732584193;
        this.state[1] = 4023233417L;
        this.state[2] = 2562383102L;
        this.state[3] = 271733878;
    }

    private long F(long l, long l1, long l2) {
        return (l & l1) | ((-1 ^ l) & l2);
    }

    private long G(long l, long l1, long l2) {
        return (l & l2) | ((-1 ^ l2) & l1);
    }

    private long H(long l, long l1, long l2) {
        return (l ^ l1) ^ l2;
    }

    private long I(long l, long l1, long l2) {
        return ((-1 ^ l2) | l) ^ l1;
    }

    private long FF(long l, long l1, long l2, long l3, long l4, long l5, long l6) {
        long l7 = l + F(l1, l2, l3) + l4 + l6;
        return ((long) ((((int) l7) << ((int) l5)) | (((int) l7) >>> ((int) (32 - l5))))) + l1;
    }

    private long GG(long l, long l1, long l2, long l3, long l4, long l5, long l6) {
        long l7 = l + G(l1, l2, l3) + l4 + l6;
        return ((long) ((((int) l7) << ((int) l5)) | (((int) l7) >>> ((int) (32 - l5))))) + l1;
    }

    private long HH(long l, long l1, long l2, long l3, long l4, long l5, long l6) {
        long l7 = l + H(l1, l2, l3) + l4 + l6;
        return ((long) ((((int) l7) << ((int) l5)) | (((int) l7) >>> ((int) (32 - l5))))) + l1;
    }

    private long II(long l, long l1, long l2, long l3, long l4, long l5, long l6) {
        long l7 = l + I(l1, l2, l3) + l4 + l6;
        return ((long) ((((int) l7) << ((int) l5)) | (((int) l7) >>> ((int) (32 - l5))))) + l1;
    }

    private void md5Update(byte[] abyte0, int i) {
        int j;
        byte[] abyte1 = new byte[64];
        int k = ((int) (this.count[0] >>> 3)) & 63;
        long[] jArr = this.count;
        long j2 = jArr[0] + ((long) (i << 3));
        jArr[0] = j2;
        if (j2 < ((long) (i << 3))) {
            long[] jArr2 = this.count;
            jArr2[1] = jArr2[1] + 1;
        }
        long[] jArr3 = this.count;
        jArr3[1] = jArr3[1] + ((long) (i >>> 29));
        int l = 64 - k;
        if (i >= l) {
            md5Memcpy(this.buffer, abyte0, k, 0, l);
            md5Transform(this.buffer);
            j = l;
            while (j + 63 < i) {
                md5Memcpy(abyte1, abyte0, 0, j, 64);
                md5Transform(abyte1);
                j += 64;
            }
            k = 0;
        } else {
            j = 0;
        }
        md5Memcpy(this.buffer, abyte0, k, j, i - j);
    }

    private void md5Final() {
        byte[] abyte0 = new byte[8];
        Encode(abyte0, this.count, 8);
        int i = ((int) (this.count[0] >>> 3)) & 63;
        md5Update(PADDING, i >= 56 ? 120 - i : 56 - i);
        md5Update(abyte0, 8);
        Encode(this.digest, this.state, 16);
    }

    private void md5Memcpy(byte[] abyte0, byte[] abyte1, int i, int j, int k) {
        for (int l = 0; l < k; l++) {
            abyte0[i + l] = abyte1[j + l];
        }
    }

    private void md5Transform(byte[] abyte0) {
        long l = this.state[0];
        long l1 = this.state[1];
        long l2 = this.state[2];
        long l3 = this.state[3];
        long[] al = new long[16];
        Decode(al, abyte0, 64);
        long l4 = FF(l, l1, l2, l3, al[0], 7, 3614090360L);
        long l32 = FF(l3, l4, l1, l2, al[1], 12, 3905402710L);
        long l22 = FF(l2, l32, l4, l1, al[2], 17, 606105819);
        long l12 = FF(l1, l22, l32, l4, al[3], 22, 3250441966L);
        long l5 = FF(l4, l12, l22, l32, al[4], 7, 4118548399L);
        long l33 = FF(l32, l5, l12, l22, al[5], 12, 1200080426);
        long l23 = FF(l22, l33, l5, l12, al[6], 17, 2821735955L);
        long l13 = FF(l12, l23, l33, l5, al[7], 22, 4249261313L);
        long l6 = FF(l5, l13, l23, l33, al[8], 7, 1770035416);
        long l34 = FF(l33, l6, l13, l23, al[9], 12, 2336552879L);
        long l24 = FF(l23, l34, l6, l13, al[10], 17, 4294925233L);
        long l14 = FF(l13, l24, l34, l6, al[11], 22, 2304563134L);
        long l7 = FF(l6, l14, l24, l34, al[12], 7, 1804603682);
        long l35 = FF(l34, l7, l14, l24, al[13], 12, 4254626195L);
        long l25 = FF(l24, l35, l7, l14, al[14], 17, 2792965006L);
        long l15 = FF(l14, l25, l35, l7, al[15], 22, 1236535329);
        long l8 = GG(l7, l15, l25, l35, al[1], 5, 4129170786L);
        long l36 = GG(l35, l8, l15, l25, al[6], 9, 3225465664L);
        long l26 = GG(l25, l36, l8, l15, al[11], 14, 643717713);
        long l16 = GG(l15, l26, l36, l8, al[0], 20, 3921069994L);
        long l9 = GG(l8, l16, l26, l36, al[5], 5, 3593408605L);
        long l37 = GG(l36, l9, l16, l26, al[10], 9, 38016083);
        long l27 = GG(l26, l37, l9, l16, al[15], 14, 3634488961L);
        long l17 = GG(l16, l27, l37, l9, al[4], 20, 3889429448L);
        long l10 = GG(l9, l17, l27, l37, al[9], 5, 568446438);
        long l38 = GG(l37, l10, l17, l27, al[14], 9, 3275163606L);
        long l28 = GG(l27, l38, l10, l17, al[3], 14, 4107603335L);
        long l18 = GG(l17, l28, l38, l10, al[8], 20, 1163531501);
        long l11 = GG(l10, l18, l28, l38, al[13], 5, 2850285829L);
        long l39 = GG(l38, l11, l18, l28, al[2], 9, 4243563512L);
        long l29 = GG(l28, l39, l11, l18, al[7], 14, 1735328473);
        long l19 = GG(l18, l29, l39, l11, al[12], 20, 2368359562L);
        long l20 = HH(l11, l19, l29, l39, al[5], 4, 4294588738L);
        long l310 = HH(l39, l20, l19, l29, al[8], 11, 2272392833L);
        long l210 = HH(l29, l310, l20, l19, al[11], 16, 1839030562);
        long l110 = HH(l19, l210, l310, l20, al[14], 23, 4259657740L);
        long l21 = HH(l20, l110, l210, l310, al[1], 4, 2763975236L);
        long l311 = HH(l310, l21, l110, l210, al[4], 11, 1272893353);
        long l211 = HH(l210, l311, l21, l110, al[7], 16, 4139469664L);
        long l111 = HH(l110, l211, l311, l21, al[10], 23, 3200236656L);
        long l30 = HH(l21, l111, l211, l311, al[13], 4, 681279174);
        long l312 = HH(l311, l30, l111, l211, al[0], 11, 3936430074L);
        long l212 = HH(l211, l312, l30, l111, al[3], 16, 3572445317L);
        long l112 = HH(l111, l212, l312, l30, al[6], 23, 76029189);
        long l31 = HH(l30, l112, l212, l312, al[9], 4, 3654602809L);
        long l313 = HH(l312, l31, l112, l212, al[12], 11, 3873151461L);
        long l213 = HH(l212, l313, l31, l112, al[15], 16, 530742520);
        long l113 = HH(l112, l213, l313, l31, al[2], 23, 3299628645L);
        long l40 = II(l31, l113, l213, l313, al[0], 6, 4096336452L);
        long l314 = II(l313, l40, l113, l213, al[7], 10, 1126891415);
        long l214 = II(l213, l314, l40, l113, al[14], 15, 2878612391L);
        long l114 = II(l113, l214, l314, l40, al[5], 21, 4237533241L);
        long l41 = II(l40, l114, l214, l314, al[12], 6, 1700485571);
        long l315 = II(l314, l41, l114, l214, al[3], 10, 2399980690L);
        long l215 = II(l214, l315, l41, l114, al[10], 15, 4293915773L);
        long l115 = II(l114, l215, l315, l41, al[1], 21, 2240044497L);
        long l42 = II(l41, l115, l215, l315, al[8], 6, 1873313359);
        long l316 = II(l315, l42, l115, l215, al[15], 10, 4264355552L);
        long l216 = II(l215, l316, l42, l115, al[6], 15, 2734768916L);
        long l116 = II(l115, l216, l316, l42, al[13], 21, 1309151649);
        long l43 = II(l42, l116, l216, l316, al[4], 6, 4149444226L);
        long l317 = II(l316, l43, l116, l216, al[11], 10, 3174756917L);
        long l217 = II(l216, l317, l43, l116, al[2], 15, 718787259);
        long l117 = II(l116, l217, l317, l43, al[9], 21, 3951481745L);
        long[] jArr = this.state;
        jArr[0] = jArr[0] + l43;
        long[] jArr2 = this.state;
        jArr2[1] = jArr2[1] + l117;
        long[] jArr3 = this.state;
        jArr3[2] = jArr3[2] + l217;
        long[] jArr4 = this.state;
        jArr4[3] = jArr4[3] + l317;
    }

    private void Encode(byte[] abyte0, long[] al, int i) {
        int j = 0;
        for (int k = 0; k < i; k += 4) {
            abyte0[k] = (byte) ((int) (al[j] & 255));
            abyte0[k + 1] = (byte) ((int) ((al[j] >>> 8) & 255));
            abyte0[k + 2] = (byte) ((int) ((al[j] >>> 16) & 255));
            abyte0[k + 3] = (byte) ((int) ((al[j] >>> 24) & 255));
            j++;
        }
    }

    private void Decode(long[] al, byte[] abyte0, int i) {
        int j = 0;
        for (int k = 0; k < i; k += 4) {
            al[j] = b2iu(abyte0[k]) | (b2iu(abyte0[k + 1]) << 8) | (b2iu(abyte0[k + 2]) << 16) | (b2iu(abyte0[k + 3]) << 24);
            j++;
        }
    }

    public static long b2iu(byte byte0) {
        return (long) (byte0 >= 0 ? byte0 : byte0 & 255);
    }

    public static String byteHEX(byte byte0) {
        char[] ac = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        return new String(new char[]{ac[(byte0 >>> 4) & 15], ac[byte0 & 15]});
    }

    public static String toMD5(String s) {
        return new MD5Util().getMD5ofStr(s);
    }

    public static void main(String[] args) {
        System.out.println(toMD5("123456"));
    }
}
