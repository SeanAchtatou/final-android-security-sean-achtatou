package com.mobcent.forum.android.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.forum.android.application.McForumApplication;

public abstract class BaseHeartDBUtil {
    public static final String DATABASE_NAME = "mcForum2_heart.db";
    int DEFAULT_VERSION = 1;
    protected Context context;
    protected HeartMsgDBOpenHelper heartMsgDBOpenHelper;
    protected SQLiteDatabase readableDatabase;
    protected SQLiteDatabase writableDatabase;

    public BaseHeartDBUtil(Context ctx) {
        Context appContext;
        this.context = ctx;
        long userId = SharedPreferencesDB.getInstance(ctx).getUserId();
        McForumApplication app = McForumApplication.getInstance();
        if (app == null) {
            appContext = this.context.getApplicationContext();
        } else {
            appContext = app.getApplicationContext();
        }
        this.context = appContext;
        this.heartMsgDBOpenHelper = new HeartMsgDBOpenHelper(this.context, "mcForum2_heart.db" + userId, this.DEFAULT_VERSION);
    }

    public boolean isRowExisted(SQLiteDatabase database, String table, String column, long id) {
        Cursor c = null;
        try {
            c = database.query(table, null, String.valueOf(column) + "=" + id, null, null, null, null);
            if (c.getCount() > 0) {
                c.close();
                return true;
            }
            c.close();
            return false;
        } catch (Exception e) {
            c.close();
            return false;
        } catch (Throwable th) {
            c.close();
            throw th;
        }
    }

    public boolean isRowExisted(SQLiteDatabase database, String table, String column, String id) {
        Cursor c = null;
        try {
            c = database.query(table, null, String.valueOf(column) + "='" + id + "'", null, null, null, null);
            if (c.getCount() > 0) {
                c.close();
                return true;
            }
            c.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            c.close();
            return false;
        } catch (Throwable th) {
            c.close();
            throw th;
        }
    }

    public boolean removeAllEntries(String tableName) {
        try {
            openWriteableDB();
            this.writableDatabase.delete(tableName, null, null);
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean tabbleIsExist(SQLiteDatabase database, String tableName) {
        boolean result = false;
        if (tableName == null) {
            return false;
        }
        Cursor c = null;
        try {
            Cursor c2 = database.rawQuery("select count(*) as c from Sqlite_master  where type ='table' and name ='" + tableName.trim() + "' ", null);
            if (c2.moveToNext() && c2.getInt(0) > 0) {
                result = true;
            }
            if (c2 != null) {
                c2.close();
            }
            return result;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            return false;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            throw th;
        }
    }

    public synchronized void openWriteableDB() {
        this.writableDatabase = this.heartMsgDBOpenHelper.getWritableDatabase();
    }

    public synchronized void openReadableDB() {
        this.readableDatabase = this.heartMsgDBOpenHelper.getReadableDatabase();
    }

    public void closeWriteableDB() {
        try {
            this.heartMsgDBOpenHelper.close();
        } catch (Exception e) {
        }
        if (this.writableDatabase != null && this.writableDatabase.isOpen()) {
            try {
                this.writableDatabase.close();
                this.writableDatabase = null;
            } catch (Exception e2) {
            }
        }
    }

    public void closeReadableDB() {
        try {
            this.heartMsgDBOpenHelper.close();
        } catch (Exception e) {
        }
        if (this.readableDatabase != null && this.readableDatabase.isOpen()) {
            try {
                this.readableDatabase.close();
                this.readableDatabase = null;
            } catch (Exception e2) {
            }
        }
    }
}
