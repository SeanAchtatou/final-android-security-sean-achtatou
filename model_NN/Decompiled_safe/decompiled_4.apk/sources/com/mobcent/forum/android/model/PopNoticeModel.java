package com.mobcent.forum.android.model;

public class PopNoticeModel extends BaseModel {
    private static final long serialVersionUID = 3571735724323823734L;
    private String content;
    private String jsonStr;
    private long noticeId;
    private int noticeState;
    private int type;
    private String url;
    private int version;

    public int getNoticeState() {
        return this.noticeState;
    }

    public void setNoticeState(int noticeState2) {
        this.noticeState = noticeState2;
    }

    public String getJsonStr() {
        return this.jsonStr;
    }

    public void setJsonStr(String jsonStr2) {
        this.jsonStr = jsonStr2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version2) {
        this.version = version2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public long getNoticeId() {
        return this.noticeId;
    }

    public void setNoticeId(long noticeId2) {
        this.noticeId = noticeId2;
    }
}
