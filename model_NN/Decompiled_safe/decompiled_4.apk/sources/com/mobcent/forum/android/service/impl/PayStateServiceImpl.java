package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.PayStateRestfulApiRequester;
import com.mobcent.forum.android.model.PayStateModel;
import com.mobcent.forum.android.service.PayStateService;
import com.mobcent.forum.android.service.impl.helper.PayStateServiceHelper;

public class PayStateServiceImpl implements PayStateService {
    private Context context;

    public PayStateServiceImpl(Context context2) {
        this.context = context2;
    }

    public PayStateModel controll(String forumKey) {
        return PayStateServiceHelper.controll(PayStateRestfulApiRequester.controll(this.context, forumKey));
    }
}
