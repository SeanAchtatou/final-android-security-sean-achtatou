package com.mobcent.forum.android.model;

public class MoreApplicationModel extends BaseModel {
    private static final long serialVersionUID = 5790731264703346849L;
    private String AppName;
    private int appState;
    private String downloadUrl;
    private String iconUrl;
    private String packageName;
    private String shortDescription;

    public int getAppState() {
        return this.appState;
    }

    public void setAppState(int appState2) {
        this.appState = appState2;
    }

    public String getAppName() {
        return this.AppName;
    }

    public void setAppName(String appName) {
        this.AppName = appName;
    }

    public String getIconUrl() {
        return this.iconUrl;
    }

    public void setIconUrl(String iconUrl2) {
        this.iconUrl = iconUrl2;
    }

    public String getDownloadUrl() {
        return this.downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl2) {
        this.downloadUrl = downloadUrl2;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String packageName2) {
        this.packageName = packageName2;
    }

    public String getShortDescription() {
        return this.shortDescription;
    }

    public void setShortDescription(String shortDescription2) {
        this.shortDescription = shortDescription2;
    }
}
