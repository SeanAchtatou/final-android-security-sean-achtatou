package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.ShareRestfulApiRequester;
import com.mobcent.forum.android.service.ShareService;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.service.impl.helper.ShareServiceImplHelper;
import com.mobcent.forum.android.util.StringUtil;

public class ShareServiceImpl implements ShareService {
    private Context context;

    public ShareServiceImpl(Context context2) {
        this.context = context2;
    }

    public String addShareNum(long boardId, long topicId) {
        String jsonStr = ShareRestfulApiRequester.addShareNum(this.context, boardId, topicId);
        if (ShareServiceImplHelper.parseSuccessModel(jsonStr)) {
            return null;
        }
        String error = BaseJsonHelper.formJsonRS(jsonStr);
        if (!StringUtil.isEmpty(error)) {
            return error;
        }
        return null;
    }
}
