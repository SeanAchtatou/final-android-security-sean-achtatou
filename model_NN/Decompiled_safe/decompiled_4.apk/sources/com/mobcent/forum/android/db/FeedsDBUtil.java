package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.forum.android.db.constant.FeedsDBConstant;
import com.mobcent.forum.android.util.DateUtil;

public class FeedsDBUtil extends BaseDBUtil implements FeedsDBConstant {
    private static final int FEEDS_ID = 1;
    private static FeedsDBUtil feedsDBUtil;

    public FeedsDBUtil(Context ctx) {
        super(ctx);
    }

    public static FeedsDBUtil getInstance(Context context) {
        if (feedsDBUtil == null) {
            feedsDBUtil = new FeedsDBUtil(context);
        }
        return feedsDBUtil;
    }

    public String getFeedsTopicJsonString() {
        String jsonStr = null;
        Cursor cursor = null;
        try {
            openReadableDB();
            cursor = this.readableDatabase.rawQuery(FeedsDBConstant.SQL_SELECT_FEEDS_TOPTIC, null);
            while (cursor.moveToNext()) {
                jsonStr = cursor.getString(1);
            }
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            jsonStr = null;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
            throw th;
        }
        return jsonStr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean updateFeedsTopicJsonString(String jsonStr) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("id", (Integer) 1);
            values.put("jsonStr", jsonStr);
            values.put("update_time", Long.valueOf(DateUtil.getCurrentTime()));
            if (!isRowExisted(this.writableDatabase, FeedsDBConstant.TABLE_FEEDS, "id", 1)) {
                this.writableDatabase.insertOrThrow(FeedsDBConstant.TABLE_FEEDS, null, values);
            } else {
                this.writableDatabase.update(FeedsDBConstant.TABLE_FEEDS, values, "id=1", null);
            }
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean delteFeedsList() {
        return removeAllEntries(FeedsDBConstant.TABLE_FEEDS);
    }
}
