package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.api.util.HttpClientUtil;
import com.mobcent.forum.android.constant.FeedBackConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.util.AppUtil;
import com.mobcent.forum.android.util.PhoneUtil;
import java.util.HashMap;

public class FeedBackRestfulApiRequester extends BaseRestfulApiRequester implements FeedBackConstant {
    private static final String BASE_URL = "http://www.mobcent.com/";

    public static String feedBack(Context context, String mainInfo, String stackTraceInfo, int projectType, String projectVersion) {
        HashMap<String, String> params = new HashMap<>();
        SharedPreferencesDB shareDB = SharedPreferencesDB.getInstance(context);
        params.put(FeedBackConstant.MAIN_INFO, mainInfo);
        params.put(FeedBackConstant.STACK_TRACE_INFO, stackTraceInfo);
        params.put(FeedBackConstant.PHONE_TYPE, PhoneUtil.getPhoneType());
        params.put(FeedBackConstant.PHONE_OS_VERSION, PhoneUtil.getSDKVersionName());
        params.put("platType", "1");
        params.put(FeedBackConstant.APP_ID, new StringBuilder(String.valueOf(shareDB.getForumId())).toString());
        params.put("appKey", shareDB.getForumKey());
        params.put("appName", AppUtil.getAppName(context));
        params.put(FeedBackConstant.APP_VERSION, AppUtil.getVersionName(context));
        String serviceType = PhoneUtil.getServiceName(context);
        if (serviceType.equals("wifi")) {
            params.put(FeedBackConstant.NETWORK_TYPE, "1");
            params.put(FeedBackConstant.SERVICE_TYPE, "");
        } else if (serviceType.equals("mobile")) {
            params.put(FeedBackConstant.NETWORK_TYPE, getNetWorkType(context));
            params.put(FeedBackConstant.SERVICE_TYPE, "1");
        } else if (serviceType.equals("unicom")) {
            params.put(FeedBackConstant.NETWORK_TYPE, getNetWorkType(context));
            params.put(FeedBackConstant.SERVICE_TYPE, "2");
        } else if (serviceType.equals("telecom")) {
            params.put(FeedBackConstant.NETWORK_TYPE, getNetWorkType(context));
            params.put(FeedBackConstant.SERVICE_TYPE, "3");
        }
        params.put("userId", new StringBuilder(String.valueOf(shareDB.getUserId())).toString());
        params.put(FeedBackConstant.PROJECT_TYPE, new StringBuilder(String.valueOf(projectType)).toString());
        params.put(FeedBackConstant.PROJECT_VERSION, projectVersion);
        return HttpClientUtil.doPostRequest("http://www.mobcent.com/count_query/mobcent/feedback.do", params, context);
    }

    public static String getNetWorkType(Context context) {
        String netWorkType = PhoneUtil.getNetWorkName(context);
        if (netWorkType.equals("cmwap") || netWorkType.equals("cmnet") || netWorkType.equals("ctwap") || netWorkType.equals("ctnet") || netWorkType.equals("uniwap") || netWorkType.equals("uninet")) {
            return "2";
        }
        if (!netWorkType.equals("3gwap") && !netWorkType.equals("3gnet")) {
            return "1";
        }
        return "3";
    }
}
