package com.mobcent.forum.android.model;

public class InformantModel extends BaseModel {
    private static final long serialVersionUID = 1;
    private String nickName;
    private String reason;
    private long reportTime;
    private long userId;

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(String nickName2) {
        this.nickName = nickName2;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason2) {
        this.reason = reason2;
    }

    public long getReportTime() {
        return this.reportTime;
    }

    public void setReportTime(long reportTime2) {
        this.reportTime = reportTime2;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
    }
}
