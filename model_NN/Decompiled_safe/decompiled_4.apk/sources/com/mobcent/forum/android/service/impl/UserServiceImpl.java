package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.UserRestfulApiRequester;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.db.ModeratorBoardDBUtil;
import com.mobcent.forum.android.db.PermDBUtil;
import com.mobcent.forum.android.db.RecomUserDBUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.db.SurroundUserDBUtil;
import com.mobcent.forum.android.db.UserFriendsDBUtil;
import com.mobcent.forum.android.db.UserJsonDBUtil;
import com.mobcent.forum.android.db.helper.UserJsonDBHelper;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.service.impl.helper.UserBoardInfoServiceHelper;
import com.mobcent.forum.android.service.impl.helper.UserServiceImplHelper;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {
    /* access modifiers changed from: private */
    public Context context;

    public UserServiceImpl(Context context2) {
        this.context = context2;
    }

    public UserInfoModel regUser(String email, String password) {
        String jsonStr = UserRestfulApiRequester.regUser(this.context, email, password);
        UserInfoModel userInfoModel = UserServiceImplHelper.parseRegUserJson(jsonStr);
        if (userInfoModel == null) {
            UserInfoModel userInfoModel2 = new UserInfoModel();
            userInfoModel2.setErrorCode(BaseJsonHelper.formJsonRS(jsonStr));
            return userInfoModel2;
        }
        PermDBUtil.getInstance(this.context).updatePermJsonString(userInfoModel.getPermModelStr());
        userInfoModel.setEmail(email);
        userInfoModel.setPwd(password);
        SharedPreferencesDB.getInstance(this.context).updateUserInfo(userInfoModel.getUserId(), userInfoModel.getForumId(), userInfoModel.getUat(), userInfoModel.getUas(), userInfoModel.getRoleNum());
        SharedPreferencesDB.getInstance(this.context).setNickName(email);
        saveCurrUserInfoInDB(userInfoModel);
        return userInfoModel;
    }

    public UserInfoModel regUserByOpenPlatform(String email, String icon, String password, int gender, String nickName, long platformId, String platformUserId) {
        String jsonStr = UserRestfulApiRequester.regUserByOpenPlatform(this.context, email, icon, password, gender, nickName, platformId, platformUserId);
        UserInfoModel userInfoModel = UserServiceImplHelper.parseRegUserByOpenPlatform(jsonStr);
        if (userInfoModel == null) {
            UserInfoModel userInfoModel2 = new UserInfoModel();
            userInfoModel2.setErrorCode(BaseJsonHelper.formJsonRS(jsonStr));
            return userInfoModel2;
        }
        userInfoModel.setEmail(email);
        userInfoModel.setPwd(password);
        SharedPreferencesDB.getInstance(this.context).updateUserInfo(userInfoModel.getUserId(), userInfoModel.getForumId(), userInfoModel.getUat(), userInfoModel.getUas(), userInfoModel.getRoleNum());
        SharedPreferencesDB.getInstance(this.context).setNickName(nickName);
        saveCurrUserInfoInDB(userInfoModel);
        return userInfoModel;
    }

    public List<UserInfoModel> getAllUsers() {
        return UserJsonDBUtil.getInstance(this.context).getAllUsers();
    }

    public UserInfoModel loginUser(String email, String password) {
        final String jsonStr = UserRestfulApiRequester.loginUser(this.context, email, password);
        UserInfoModel userInfoModel = UserServiceImplHelper.parseLoginUserJson(jsonStr);
        if (userInfoModel == null) {
            UserInfoModel userInfoModel2 = new UserInfoModel();
            userInfoModel2.setErrorCode(BaseJsonHelper.formJsonRS(jsonStr));
            return userInfoModel2;
        }
        PermDBUtil.getInstance(this.context).updatePermJsonString(userInfoModel.getPermModelStr());
        userInfoModel.setEmail(email);
        userInfoModel.setPwd(password);
        final long userId = userInfoModel.getUserId();
        SharedPreferencesDB.getInstance(this.context).updateUserInfo(userInfoModel.getUserId(), userInfoModel.getForumId(), userInfoModel.getUat(), userInfoModel.getUas(), userInfoModel.getRoleNum());
        saveCurrUserInfoInDB(userInfoModel);
        new Thread() {
            public void run() {
                ModeratorBoardDBUtil.getInstance(UserServiceImpl.this.context).updateModeratorBoardString(jsonStr, userId);
            }
        }.start();
        return userInfoModel;
    }

    public UserInfoModel getCurrUser() {
        return UserJsonDBUtil.getInstance(this.context).getCurrentUserInfo();
    }

    public UserInfoModel changeUserPwd(String oldPassword, String newPassword) {
        String jsonStr = UserRestfulApiRequester.changeUserPwd(this.context, oldPassword, newPassword);
        UserInfoModel userInfoModel = new UserInfoModel();
        if (UserServiceImplHelper.parseChangePwdJson(jsonStr)) {
            userInfoModel.setUserId(SharedPreferencesDB.getInstance(this.context).getUserId());
            userInfoModel.setPwd(newPassword);
            UserInfoModel user = UserJsonDBUtil.getInstance(this.context).getCurrentUserInfo();
            if (user != null) {
                user.setPwd(newPassword);
                UserJsonDBUtil.getInstance(this.context).updateUserInfo(user.getUserId(), UserJsonDBHelper.buildUserInfo(user));
            }
        } else {
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                userInfoModel.setErrorCode(errorCode);
            }
        }
        return userInfoModel;
    }

    public String uploadIcon(String uploadFile) {
        String jsonStr = UserRestfulApiRequester.uploadIcon(this.context, uploadFile);
        String iconPath = UserServiceImplHelper.parseUploadIconJson(jsonStr);
        if (iconPath == null) {
            return BaseReturnCodeConstant.ERROR_CODE + BaseJsonHelper.formJsonRS(jsonStr);
        }
        return iconPath;
    }

    public String updateUser(String nickName, String icon, int gender, String signature, long pageFrom) {
        String jsonStr = UserRestfulApiRequester.updateUser(this.context, nickName, icon, gender, signature, pageFrom);
        if (!UserServiceImplHelper.parseUpdateUserJson(jsonStr)) {
            return BaseJsonHelper.formJsonRS(jsonStr);
        }
        UserInfoModel user = UserJsonDBUtil.getInstance(this.context).getCurrentUserInfo();
        if (user == null) {
            return null;
        }
        user.setNickname(nickName);
        user.setIcon(icon);
        user.setGender(gender);
        user.setSignature(signature);
        user.setPageFrom(pageFrom);
        UserJsonDBUtil.getInstance(this.context).updateUserInfo(user.getUserId(), UserJsonDBHelper.buildUserInfo(user));
        return null;
    }

    public UserInfoModel getUserInfo(long userId) {
        String jsonStr = UserRestfulApiRequester.getUserInfo(this.context, userId);
        UserInfoModel userInfoModel = UserServiceImplHelper.parseGetUserInfoJson(jsonStr);
        if (userInfoModel == null) {
            UserInfoModel userInfoModel2 = new UserInfoModel();
            userInfoModel2.setErrorCode(BaseJsonHelper.formJsonRS(jsonStr));
            return userInfoModel2;
        }
        PermDBUtil.getInstance(this.context).updatePermJsonString(userInfoModel.getPermModelStr());
        return userInfoModel;
    }

    public String followUser(long currentUserId, long focusId) {
        return BaseJsonHelper.formJsonRS(UserRestfulApiRequester.followUser(this.context, currentUserId, focusId));
    }

    public String unfollowUser(long currentUserId, long focusId) {
        return BaseJsonHelper.formJsonRS(UserRestfulApiRequester.unfollowUser(this.context, currentUserId, focusId));
    }

    public UserInfoModel getUser() {
        String jsonStr = UserRestfulApiRequester.getUser(this.context);
        UserInfoModel userInfoModel = UserServiceImplHelper.parseGetUserJson(jsonStr);
        if (userInfoModel != null) {
            return userInfoModel;
        }
        UserInfoModel userInfoModel2 = new UserInfoModel();
        userInfoModel2.setErrorCode(BaseJsonHelper.formJsonRS(jsonStr));
        return userInfoModel2;
    }

    public String logout() {
        new Thread() {
            public void run() {
                UserServiceImplHelper.parseLogoutJson(UserRestfulApiRequester.logout(UserServiceImpl.this.context));
            }
        }.start();
        return null;
    }

    public boolean isLogin() {
        String accessToken = SharedPreferencesDB.getInstance(this.context).getAccessToken();
        String accessSecret = SharedPreferencesDB.getInstance(this.context).getAccessSecret();
        if (accessToken == null || accessSecret == null) {
            return false;
        }
        return true;
    }

    public long getLoginUserId() {
        return SharedPreferencesDB.getInstance(this.context).getUserId();
    }

    public String getNickname() {
        return SharedPreferencesDB.getInstance(this.context).getNickName();
    }

    public List<UserInfoModel> getUserFriendList(long userId, int page, int pageSize) {
        String jsonStr = UserRestfulApiRequester.getUserFriendList(this.context, userId, page, pageSize);
        List<UserInfoModel> userFriendList = UserServiceImplHelper.parseUserFriendListJson(jsonStr, page, pageSize);
        if (userFriendList == null) {
            userFriendList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                UserInfoModel userFriendModel = new UserInfoModel();
                userFriendModel.setErrorCode(errorCode);
                userFriendList.add(userFriendModel);
            }
        }
        return userFriendList;
    }

    public List<UserInfoModel> getSurroudUser(long userId, double longitude, double latitude, int radius, int page, int pageSize) {
        String jsonStr = UserRestfulApiRequester.getSurroudUser(this.context, longitude, latitude, radius, page, pageSize);
        List<UserInfoModel> surroundUserList = UserServiceImplHelper.parseSurroundUserJson(jsonStr, page, pageSize);
        if (surroundUserList == null) {
            surroundUserList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                UserInfoModel userInfoModel = new UserInfoModel();
                userInfoModel.setErrorCode(errorCode);
                surroundUserList.add(userInfoModel);
            }
        } else {
            SurroundUserDBUtil.getInstance(this.context).updateSurroundUserJsonString(jsonStr, userId);
        }
        return surroundUserList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public List<UserInfoModel> getLocalSurroudUser(long userId, int page, int pageSize) {
        try {
            String jsonStr = SurroundUserDBUtil.getInstance(this.context).getSurroundUserJsonString(userId);
            MCLogUtil.i("UserServiceImpl", "jsonStr = " + jsonStr);
            List<UserInfoModel> surroundUserList = UserServiceImplHelper.parseSurroundUserJson(jsonStr, page, pageSize);
            if (surroundUserList == null) {
                ArrayList arrayList = new ArrayList();
                try {
                    String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
                    if (!StringUtil.isEmpty(errorCode)) {
                        UserInfoModel userInfoModel = new UserInfoModel();
                        userInfoModel.setErrorCode(errorCode);
                        arrayList.add(userInfoModel);
                        surroundUserList = arrayList;
                    } else {
                        surroundUserList = arrayList;
                    }
                } catch (Exception e) {
                    return null;
                }
            } else {
                surroundUserList.get(0).setHasNext(0);
            }
            return surroundUserList;
        } catch (Exception e2) {
        }
    }

    public UserInfoModel parseOpenplatformUserInfo(String jsonStr) {
        UserInfoModel userInfoModel = UserServiceImplHelper.parseOpenplatformUserInfo(jsonStr);
        if (userInfoModel != null) {
            SharedPreferencesDB.getInstance(this.context).updateUserInfo(userInfoModel.getUserId(), userInfoModel.getForumId(), userInfoModel.getUat(), userInfoModel.getUas(), userInfoModel.getRoleNum());
            PermDBUtil.getInstance(this.context).updatePermJsonString(userInfoModel.getPermModelStr());
            return userInfoModel;
        }
        UserInfoModel userInfoModel2 = new UserInfoModel();
        userInfoModel2.setErrorCode(BaseJsonHelper.formJsonRS(jsonStr));
        return userInfoModel2;
    }

    public List<UserInfoModel> getRecommendUser(long userId, int page, int pageSize) {
        String jsonStr = UserRestfulApiRequester.getRecommendUser(this.context, userId, page, pageSize);
        List<UserInfoModel> recommendUserList = UserServiceImplHelper.parseUserFriendListJson(jsonStr, page, pageSize);
        if (recommendUserList == null) {
            recommendUserList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                UserInfoModel userFriendModel = new UserInfoModel();
                userFriendModel.setErrorCode(errorCode);
                recommendUserList.add(userFriendModel);
            }
        } else {
            RecomUserDBUtil.getInstance(this.context).updateRecomUserJsonString(jsonStr);
        }
        return recommendUserList;
    }

    private void saveCurrUserInfoInDB(UserInfoModel user) {
        UserJsonDBUtil.getInstance(this.context).saveUserInfoModel(user.getUserId(), 1, UserJsonDBHelper.buildUserInfo(user));
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public List<UserInfoModel> getRecommendUser(long userId, int page, int pageSize, boolean isLocal) {
        String jsonStr;
        if (!isLocal) {
            return getRecommendUser(userId, page, pageSize);
        }
        try {
            jsonStr = RecomUserDBUtil.getInstance(this.context).getRecomUserJsonString();
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
        }
        if (StringUtil.isEmpty(jsonStr)) {
            return getRecommendUser(userId, page, pageSize);
        }
        List<UserInfoModel> recomUsersList = UserServiceImplHelper.parseUserFriendListJson(jsonStr, page, pageSize);
        if (recomUsersList == null || recomUsersList.size() <= 0) {
            return recomUsersList;
        }
        recomUsersList.get(0).setHasNext(0);
        return recomUsersList;
    }

    public List<UserInfoModel> getUserFriendListByNet(long userId, int page, int pageSize) {
        String jsonStr = UserRestfulApiRequester.getUserFriendList(this.context, userId, page, pageSize);
        List<UserInfoModel> userFriendList = UserServiceImplHelper.parseUserFriendListJson(jsonStr, page, pageSize);
        if (userFriendList == null) {
            userFriendList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                UserInfoModel userFriendModel = new UserInfoModel();
                userFriendModel.setErrorCode(errorCode);
                userFriendList.add(userFriendModel);
            }
        } else {
            UserFriendsDBUtil.getInstance(this.context).updateUserFriendsJsonString(jsonStr);
        }
        return userFriendList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public List<UserInfoModel> getUserFriendList(long userId, int page, int pageSize, boolean isLocal) {
        String jsonStr;
        if (!isLocal) {
            return getUserFriendListByNet(userId, page, pageSize);
        }
        try {
            jsonStr = UserFriendsDBUtil.getInstance(this.context).getUserFriendsJsonString();
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
        }
        if (StringUtil.isEmpty(jsonStr)) {
            return getUserFriendListByNet(userId, page, pageSize);
        }
        List<UserInfoModel> userFriendsList = UserServiceImplHelper.parseUserFriendListJson(jsonStr, page, pageSize);
        if (userFriendsList == null || userFriendsList.size() <= 0) {
            return userFriendsList;
        }
        userFriendsList.get(0).setHasNext(0);
        return userFriendsList;
    }

    public List<UserInfoModel> getUserFanList(long userId, int page, int pageSize) {
        String jsonStr = UserRestfulApiRequester.getUserFanList(this.context, userId, page, pageSize);
        List<UserInfoModel> userFriendList = UserServiceImplHelper.parseUserFriendListJson(jsonStr, page, pageSize);
        if (userFriendList == null) {
            userFriendList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                UserInfoModel userFriendModel = new UserInfoModel();
                userFriendModel.setErrorCode(errorCode);
                userFriendList.add(userFriendModel);
            }
        }
        return userFriendList;
    }

    public List<UserInfoModel> getRegUserList() {
        String jsonStr = UserRestfulApiRequester.getRegUser(this.context);
        List<UserInfoModel> userList = UserServiceImplHelper.parseGetRegUserInfoJson(jsonStr);
        if (userList == null) {
            userList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                UserInfoModel userFriendModel = new UserInfoModel();
                userFriendModel.setErrorCode(errorCode);
                userList.add(userFriendModel);
            }
        }
        return userList;
    }

    public String getResetPassword(long userId, String password) {
        return BaseJsonHelper.formJsonRS(UserRestfulApiRequester.getResetPassword(this.context, userId, password));
    }

    public int getRoleNum() {
        return SharedPreferencesDB.getInstance(this.context).getRoleNum();
    }

    public boolean currentUserIsAdmin() {
        if (SharedPreferencesDB.getInstance(this.context).getRoleNum() == 8) {
            return true;
        }
        return false;
    }

    public boolean currentUserIsSuperAdmin() {
        if (SharedPreferencesDB.getInstance(this.context).getRoleNum() > 8) {
            return true;
        }
        return false;
    }

    public boolean currentUserIsBoardowner(long boardId) {
        if (SharedPreferencesDB.getInstance(this.context).getRoleNum() != 4 || !new ModeratorServiceImpl(this.context).BoardPermission(boardId, getLoginUserId())) {
            return false;
        }
        return true;
    }

    public UserInfoModel getUserBoardInfoList(long otherUserId) {
        String jsonStr = UserRestfulApiRequester.getUserBoardInfoList(this.context, otherUserId);
        UserInfoModel userInfoModel = UserBoardInfoServiceHelper.getUserBoardInfoList(jsonStr);
        if (userInfoModel != null) {
            return userInfoModel;
        }
        UserInfoModel userInfoModel2 = new UserInfoModel();
        userInfoModel2.setErrorCode(BaseJsonHelper.formJsonRS(jsonStr));
        return userInfoModel2;
    }
}
