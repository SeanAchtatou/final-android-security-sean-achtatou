package com.mobcent.forum.android.service;

import android.graphics.Bitmap;
import com.mobcent.forum.android.service.delegate.DownProgressDelegate;
import java.io.File;

public interface FileTransferService {
    void downloadFile(String str, File file);

    Bitmap getBitmapInsExtMedia(String str, float f, int i, String str2, String str3, boolean z);

    Bitmap getBitmapInsExtMediaProBar(String str, float f, int i, String str2, String str3, DownProgressDelegate downProgressDelegate, boolean z);

    Bitmap getBitmapUnInsExtMedia(String str);

    Bitmap getBitmapUnInsExtMedia(String str, float f, int i, boolean z);

    Bitmap getBitmapUnInsExtMediaProBar(String str, float f, int i, DownProgressDelegate downProgressDelegate, boolean z);

    Bitmap getBitmapUnInsExtMediaProBar(String str, DownProgressDelegate downProgressDelegate);

    byte[] getImageStream(String str);

    byte[] getImageStreamProBar(String str, DownProgressDelegate downProgressDelegate);
}
