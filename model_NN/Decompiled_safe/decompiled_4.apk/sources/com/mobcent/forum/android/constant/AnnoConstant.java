package com.mobcent.forum.android.constant;

public interface AnnoConstant {
    public static final String ANNOUNCE_ID = "announce_id";
    public static final String ANNO_LIST = "anno_list";
    public static final String AUTHOR = "author";
    public static final String AUTHOR_ID = "author_id";
    public static final String A_ID = "aid";
    public static final String BOARD_ID = "board_id";
    public static final String CONTENT = "content";
    public static final int CONTENT_IMG = 1;
    public static final int CONTENT_TEXT = 0;
    public static final String CREATE_DATE = "create_date";
    public static final String DESC = "desc";
    public static final String END_DATE = "end_date";
    public static final String FORUM_ID = "forum_id";
    public static final String FORWARD = "forward";
    public static final String FRIEND_LIST = "friendList";
    public static final String HAS_NEXT = "has_next";
    public static final String ICON = "icon";
    public static final String ICON_URL = "icon_url";
    public static final String IMG_URL = "img_url";
    public static final String INFOR = "infor";
    public static final String IS_REPLY = "is_reply";
    public static final String PARAM_ANNOUNCE_ID = "announceId";
    public static final String START_DATE = "start_date";
    public static final String SUBJECT = "subject";
    public static final String TYPE = "type";
    public static final String VERIFY = "verify";
    public static final int VERIFY_ALREADY = 1;
    public static final int VERIFY_ING = 2;
    public static final int VERIFY_NOT = 0;
}
