package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.forum.android.db.constant.BaseDBConstant;
import com.mobcent.forum.android.db.constant.HeartMsgDBConstant;
import java.util.ArrayList;
import java.util.List;

public class HeartMsgDBUtil extends BaseHeartDBUtil implements BaseDBConstant, HeartMsgDBConstant {
    public static final String DATABASE_NAME = "mcForum2_heart.db";
    private static HeartMsgDBUtil heartMsgDBUtil;
    int DEFAULT_VERSION = 1;
    protected Context context;

    protected HeartMsgDBUtil(Context ctx) {
        super(ctx);
    }

    public static HeartMsgDBUtil getInstance(Context context2) {
        if (heartMsgDBUtil == null) {
            heartMsgDBUtil = new HeartMsgDBUtil(context2);
        }
        return heartMsgDBUtil;
    }

    public static HeartMsgDBUtil getNewInstance(Context context2) {
        heartMsgDBUtil = new HeartMsgDBUtil(context2);
        return heartMsgDBUtil;
    }

    public boolean saveHeartBeatModel(long chatUserId, String jsonStr) {
        if (jsonStr == null) {
            return true;
        }
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("userId", Long.valueOf(chatUserId));
            values.put("jsonStr", jsonStr);
            this.writableDatabase.insertOrThrow(HeartMsgDBConstant.TABLE_HEART_BEAT, null, values);
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean saveMessageModel(long chatUserId, String jsonStr) {
        if (jsonStr == null) {
            return true;
        }
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("userId", Long.valueOf(chatUserId));
            values.put("jsonStr", jsonStr);
            this.writableDatabase.insertOrThrow(HeartMsgDBConstant.TABLE_MESSAGE, null, values);
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean deleteHeartBeatList() {
        try {
            openWriteableDB();
            this.writableDatabase.delete(HeartMsgDBConstant.TABLE_HEART_BEAT, null, null);
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean deleteHeartBeatList(long chatUserId) {
        try {
            openWriteableDB();
            this.writableDatabase.delete(HeartMsgDBConstant.TABLE_HEART_BEAT, HeartMsgDBConstant.DELETE_HEART_BEAT_WHERE_CASE, new String[]{new StringBuilder(String.valueOf(chatUserId)).toString()});
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public List<String> getHeartBeatList() {
        List<String> jsonStrs = new ArrayList<>();
        Cursor c = null;
        try {
            openReadableDB();
            c = this.readableDatabase.rawQuery(HeartMsgDBConstant.SELECT_ALL_HEART_BEAT, null);
            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToPosition(i);
                    jsonStrs.add(c.getString(2));
                }
            }
            if (c != null) {
                c.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            closeReadableDB();
            throw th;
        }
        return jsonStrs;
    }

    public List<String> getHeartBeatList(long userId) {
        Cursor c = null;
        List<String> jsonStrs = new ArrayList<>();
        try {
            openReadableDB();
            c = this.readableDatabase.rawQuery(HeartMsgDBConstant.SELECT_SOME_HEART_BEAT, new String[]{new StringBuilder(String.valueOf(userId)).toString()});
            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToPosition(i);
                    jsonStrs.add(c.getString(2));
                }
            }
            if (c != null) {
                c.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            closeReadableDB();
            throw th;
        }
        return jsonStrs;
    }

    public List<String> getMessages(long userId, int start, int len) {
        Cursor c = null;
        List<String> jsonStrs = new ArrayList<>();
        try {
            openReadableDB();
            c = this.readableDatabase.rawQuery(HeartMsgDBConstant.SELECT_TABLE_MESSAGE, new String[]{new StringBuilder(String.valueOf(userId)).toString(), new StringBuilder(String.valueOf(start)).toString(), new StringBuilder(String.valueOf(len)).toString()});
            if (c.getCount() > 0) {
                for (int i = 0; i < c.getCount(); i++) {
                    c.moveToPosition(i);
                    jsonStrs.add(c.getString(2));
                }
            }
            if (c != null) {
                c.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            e.printStackTrace();
            if (c != null) {
                c.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            closeReadableDB();
            throw th;
        }
        return jsonStrs;
    }

    public static void changeUser(Context context2) {
        getNewInstance(context2);
    }
}
