package com.mobcent.forum.android.model;

import java.util.Set;

public class NewFeedsModel extends BaseModel {
    private static final long serialVersionUID = -7705950942528071525L;
    private AnnoModel annoModel;
    private long endTime;
    private int hasNext;
    private Set<String> imgSet;
    private String lastUpdate;
    private long time;
    private TopicFeedModel topicFeed;
    private long type;
    private UserFeedModel userFeed;

    public String getLastUpdate() {
        return this.lastUpdate;
    }

    public void setLastUpdate(String lastUpdate2) {
        this.lastUpdate = lastUpdate2;
    }

    public long getType() {
        return this.type;
    }

    public void setType(long type2) {
        this.type = type2;
    }

    public long getEndTime() {
        return this.endTime;
    }

    public void setEndTime(long endTime2) {
        this.endTime = endTime2;
    }

    public UserFeedModel getUserFeed() {
        return this.userFeed;
    }

    public void setUserFeed(UserFeedModel userFeed2) {
        this.userFeed = userFeed2;
    }

    public TopicFeedModel getTopicFeed() {
        return this.topicFeed;
    }

    public void setTopicFeed(TopicFeedModel topicFeed2) {
        this.topicFeed = topicFeed2;
    }

    public int getHasNext() {
        return this.hasNext;
    }

    public void setHasNext(int hasNext2) {
        this.hasNext = hasNext2;
    }

    public Set<String> getImgSet() {
        return this.imgSet;
    }

    public void setImgSet(Set<String> imgSet2) {
        this.imgSet = imgSet2;
    }

    public AnnoModel getAnnoModel() {
        return this.annoModel;
    }

    public void setAnnoModel(AnnoModel annoModel2) {
        this.annoModel = annoModel2;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time2) {
        this.time = time2;
    }
}
