package com.mobcent.forum.android.model;

public class SoundModel extends BaseModel {
    public static final int SOUND_DOWNING = 8;
    public static final int SOUND_FAIL = 5;
    public static final int SOUND_FINISH = 4;
    public static final int SOUND_INIT = 1;
    public static final int SOUND_NONE = 0;
    public static final int SOUND_OS_NO_PAUSE = 7;
    public static final int SOUND_OS_PAUSE = 6;
    public static final int SOUND_PAUSE = 3;
    public static final int SOUND_PLAY = 2;
    private static final long serialVersionUID = 1;
    private int currentPosition;
    private int palyStatus = 0;
    private int playProgress;
    private String soundPath;
    private long soundTime;
    private int type;

    public long getSoundTime() {
        return this.soundTime;
    }

    public void setSoundTime(long soundTime2) {
        this.soundTime = soundTime2;
    }

    public int getCurrentPosition() {
        return this.currentPosition;
    }

    public void setCurrentPosition(int currentPosition2) {
        this.currentPosition = currentPosition2;
    }

    public String getSoundPath() {
        return this.soundPath;
    }

    public void setSoundPath(String soundPath2) {
        this.soundPath = soundPath2;
    }

    public int getPalyStatus() {
        return this.palyStatus;
    }

    public void setPalyStatus(int palyStatus2) {
        this.palyStatus = palyStatus2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public int getPlayProgress() {
        return this.playProgress;
    }

    public void setPlayProgress(int playProgress2) {
        this.playProgress = playProgress2;
    }

    public String toString() {
        return "SoundModel [palyStatus=" + this.palyStatus + ", soundTime=" + this.soundTime + ", currentPosition=" + this.currentPosition + ", soundPath=" + this.soundPath + "]";
    }
}
