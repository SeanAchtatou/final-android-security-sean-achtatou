package com.mobcent.forum.android.db;

import android.content.Context;

public class DBManagerUtil {
    public boolean clearCache(Context context) {
        return BoardDBUtil.getInstance(context).delteBoardList() && HeartMsgDBUtil.getInstance(context).deleteHeartBeatList() && MentionFriendDBUtil.getInstance(context).delteMentionFriendList() && PopNoticeDBUtil.getInstance(context).deletePopNotice() && UserJsonDBUtil.getInstance(context).delteData() && LocationDBUtil.getInstance(context).deleteLocationInfo() && PollPostsDBUtil.getInstance(context).deleteAllPoll() && SurroundTopicDBUtil.getInstance(context).deleteSurroundTopicList() && SurroundUserDBUtil.getInstance(context).deleteSurroundUserList() && FeedsDBUtil.getInstance(context).delteFeedsList() && HotTopicDBUtil.getInstance(context).delteHotTopicList() && NewTopicDBUtil.getInstance(context).delteNewTopicList() && RecomUserDBUtil.getInstance(context).delteRecomUserList() && UserFriendsDBUtil.getInstance(context).delteUserFriendsList() && PicTopicDBUtil.getInstance(context).deltePicTopicList() && ModeratorBoardDBUtil.getInstance(context).delteModeratorBoardList() && NewFeedsDBUtil.getInstance(context).delteFeedsList() && PermDBUtil.getInstance(context).deltePermList();
    }
}
