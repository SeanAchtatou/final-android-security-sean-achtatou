package com.mobcent.forum.android.model;

public class RichImageModel extends BaseModel {
    private static final long serialVersionUID = -156085826382659818L;
    private int downloadLen;
    private String imageDesc;
    private String imageUrl;
    private int maxLen;

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public String getImageDesc() {
        return this.imageDesc;
    }

    public void setImageDesc(String imageDesc2) {
        this.imageDesc = imageDesc2;
    }

    public int getMaxLen() {
        return this.maxLen;
    }

    public void setMaxLen(int maxLen2) {
        this.maxLen = maxLen2;
    }

    public int getDownloadLen() {
        return this.downloadLen;
    }

    public void setDownloadLen(int downloadLen2) {
        this.downloadLen = downloadLen2;
    }
}
