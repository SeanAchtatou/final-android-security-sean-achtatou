package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.PermModel;

public interface PermService {
    String getGroupType();

    PermModel getPerm();

    int getPermNum(String str, String str2, long j);
}
