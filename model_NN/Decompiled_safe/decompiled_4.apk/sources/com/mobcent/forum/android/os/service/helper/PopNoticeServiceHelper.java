package com.mobcent.forum.android.os.service.helper;

import android.content.Context;
import android.content.Intent;
import com.mobcent.forum.android.os.service.PopNoticeOSService;

public class PopNoticeServiceHelper {
    public static void startPopNoticeService(Context context) {
        context.startService(new Intent(context, PopNoticeOSService.class));
    }

    public static void stopPopNoticeService(Context context) {
        context.stopService(new Intent(context, PopNoticeOSService.class));
    }
}
