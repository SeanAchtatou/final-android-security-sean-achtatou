package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.constant.HeartMsgConstant;
import java.util.HashMap;

public class HeartBeatRestfulApiRequester extends BaseRestfulApiRequester implements HeartMsgConstant {
    public static final String HEART_URL = "user/heart.do";

    public static String getHeartBeat(Context context) {
        return doPostRequest(HEART_URL, new HashMap<>(), context);
    }
}
