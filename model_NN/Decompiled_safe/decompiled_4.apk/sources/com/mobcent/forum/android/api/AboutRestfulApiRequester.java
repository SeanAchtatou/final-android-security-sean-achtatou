package com.mobcent.forum.android.api;

import android.content.Context;
import java.util.HashMap;

public abstract class AboutRestfulApiRequester extends BaseRestfulApiRequester {
    public static String ABOUT_DOMAIN_URL = "about/getAboutInfo.do";

    public static String getAboutInfo(Context context) {
        return doPostRequest(ABOUT_DOMAIN_URL, new HashMap<>(), context);
    }
}
