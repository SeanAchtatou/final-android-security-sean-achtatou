package com.mobcent.forum.android.os.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import com.mobcent.forum.android.model.PopNoticeModel;
import com.mobcent.forum.android.service.PopNoticeService;
import com.mobcent.forum.android.service.impl.PopNoticeServiceImpl;

public class PopNoticeOSService extends Service {
    public static final String POP_NOTICE_SERVER_MSG = ".forum.service.pop.notice.notify";
    public static final String RETURN_POP_NOTICE_MODEL = "returnPopNoticeModel";
    Handler handler = new Handler();
    /* access modifiers changed from: private */
    public PopNoticeModel popNoticeModel;
    private PopNoticeService popNoticeService;

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        new Thread(new Runnable() {
            public void run() {
                PopNoticeOSService.this.popNoticeModel = PopNoticeOSService.this.getPopModel();
                if (PopNoticeOSService.this.popNoticeModel != null) {
                    try {
                        Thread.sleep(3000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    PopNoticeOSService.this.boardcastToReceiver(PopNoticeOSService.this.popNoticeModel);
                }
                PopNoticeOSService.this.stopSelf();
            }
        }).start();
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    /* access modifiers changed from: private */
    public PopNoticeModel getPopModel() {
        if (this.popNoticeService == null) {
            this.popNoticeService = new PopNoticeServiceImpl(this);
        }
        this.popNoticeModel = this.popNoticeService.getPopNoticeModel();
        if (this.popNoticeModel != null) {
            return this.popNoticeModel;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void boardcastToReceiver(PopNoticeModel popNoticeModel2) {
        Intent intent = new Intent(String.valueOf(getPackageName()) + POP_NOTICE_SERVER_MSG);
        intent.putExtra(RETURN_POP_NOTICE_MODEL, popNoticeModel2);
        sendBroadcast(intent);
    }
}
