package com.mobcent.forum.android.constant;

public interface UserManageConstant {
    public static final String BANNED_SHIELDED_TYPE = "type";
    public static final String BOARD_ID = "board_id";
    public static final String BOARD_NAME = "board_name";
    public static final String HAS_NEXT = "has_next";
    public static final String ICON = "icon";
    public static final String ICON_URL = "icon_url";
    public static final String NICK_NAME = "nickname";
    public static final String PARAM_BANNED_SHIELDED_USER_ID = "shieldedUserId";
    public static final String PARAM_BOARD_ID = "boardId";
    public static final String PARAM_OTHER_USER_ID = "otherUserId";
    public static final String PARAM_SHIELDED_DATE = "shieldedDate";
    public static final String PARAM_SHIELDED_REASON = "shieldedReason";
    public static final String SHIELDED_DATE = "shielded_date";
    public static final String SHIELDED_LIST = "shielded_list";
    public static final String SHIELDED_REASON = "shielded_reason";
    public static final String START_DATE = "start_date";
    public static final long TOTAL = 0;
    public static final String USERID = "userId";
    public static final String USER_ID = "user_id";
}
