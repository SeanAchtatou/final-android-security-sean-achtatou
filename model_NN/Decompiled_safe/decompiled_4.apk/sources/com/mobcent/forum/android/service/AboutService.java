package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.AboutModel;

public interface AboutService {
    AboutModel getAoutInfo();
}
