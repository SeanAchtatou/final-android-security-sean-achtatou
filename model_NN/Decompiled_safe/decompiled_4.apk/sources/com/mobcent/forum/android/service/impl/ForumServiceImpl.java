package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.service.ForumService;

public class ForumServiceImpl implements ForumService {
    public String getForumKey(Context context) {
        return SharedPreferencesDB.getInstance(context).getForumKey();
    }
}
