package com.mobcent.forum.android.model;

import java.util.List;

public class BoardModel extends BaseModel {
    private static final long serialVersionUID = -6179520966646266556L;
    private String boardDesc;
    private long boardId;
    private String boardName;
    private List<BoardModel> boards;
    private long categoryId;
    private boolean isAd;
    private boolean isSelected = false;
    private long lastPostsDate;
    private int permission;
    private String picPath;
    private int postsTotalNum;
    private int todayPostsNum;
    private int topicTotalNum;

    public List<BoardModel> getBoards() {
        return this.boards;
    }

    public void setBoards(List<BoardModel> boards2) {
        this.boards = boards2;
    }

    public long getBoardId() {
        return this.boardId;
    }

    public void setBoardId(long boardId2) {
        this.boardId = boardId2;
    }

    public String getBoardName() {
        return this.boardName;
    }

    public void setBoardName(String boardName2) {
        this.boardName = boardName2;
    }

    public long getLastPostsDate() {
        return this.lastPostsDate;
    }

    public void setLastPostsDate(long lastPostsDate2) {
        this.lastPostsDate = lastPostsDate2;
    }

    public int getPostsTotalNum() {
        return this.postsTotalNum;
    }

    public void setPostsTotalNum(int postsTotalNum2) {
        this.postsTotalNum = postsTotalNum2;
    }

    public int getTodayPostsNum() {
        return this.todayPostsNum;
    }

    public void setTodayPostsNum(int todayPostsNum2) {
        this.todayPostsNum = todayPostsNum2;
    }

    public int getTopicTotalNum() {
        return this.topicTotalNum;
    }

    public void setTopicTotalNum(int topicTotalNum2) {
        this.topicTotalNum = topicTotalNum2;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean isSelected2) {
        this.isSelected = isSelected2;
    }

    public String getPicPath() {
        return this.picPath;
    }

    public void setPicPath(String picPath2) {
        this.picPath = picPath2;
    }

    public int getPermission() {
        return this.permission;
    }

    public void setPermission(int permission2) {
        this.permission = permission2;
    }

    public String getBoardDesc() {
        return this.boardDesc;
    }

    public void setBoardDesc(String boardDesc2) {
        this.boardDesc = boardDesc2;
    }

    public long getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(long categoryId2) {
        this.categoryId = categoryId2;
    }

    public boolean isAd() {
        return this.isAd;
    }

    public void setAd(boolean ad) {
        this.isAd = ad;
    }
}
