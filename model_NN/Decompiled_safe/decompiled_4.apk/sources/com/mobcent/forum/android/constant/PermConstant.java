package com.mobcent.forum.android.constant;

public interface PermConstant {
    public static final String BOARDS = "boards";
    public static final String BOARD_ID = "board_id";
    public static final String DOWNLOAD = "download";
    public static final int FLAG_DOWNLOAD = 1;
    public static final int FLAG_POST = 1;
    public static final int FLAG_POST_AFTER_VERIFY = 2;
    public static final int FLAG_READ = 1;
    public static final int FLAG_REPLY = 1;
    public static final int FLAG_UNDOWNLOAD = 0;
    public static final int FLAG_UNPOST = 0;
    public static final int FLAG_UNREAD = 0;
    public static final int FLAG_UNREPLY = 0;
    public static final int FLAG_UNUPLOAD = 0;
    public static final int FLAG_UNVISIT = 0;
    public static final int FLAG_UPLOAD = 1;
    public static final int FLAG_VISIT = 1;
    public static final String GROUP_ID = "group_id";
    public static final String GROUP_TYPE = "group_type";
    public static final String MEMBER = "member";
    public static final String PERM = "perm";
    public static final String POST = "post";
    public static final String READ = "read";
    public static final String REPLY = "reply";
    public static final String SYSTEM = "system";
    public static final String UPLOAD = "upload";
    public static final String USER_GROUP = "usergroup";
    public static final String VISIT = "visit";
}
