package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.AnnoModel;
import java.util.List;

public interface AnnounceService {
    List<AnnoModel> getAnnoList(int i, int i2);

    String verifyAnno(int i, int i2);
}
