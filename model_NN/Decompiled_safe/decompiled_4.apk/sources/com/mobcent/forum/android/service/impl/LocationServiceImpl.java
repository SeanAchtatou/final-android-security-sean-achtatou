package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.LocationRestfulApiRequester;
import com.mobcent.forum.android.db.LocationDBUtil;
import com.mobcent.forum.android.model.LocationModel;
import com.mobcent.forum.android.service.LocationService;
import org.json.JSONException;
import org.json.JSONObject;

public class LocationServiceImpl implements LocationService {
    private Context context;

    public LocationServiceImpl(Context context2) {
        this.context = context2;
    }

    public boolean saveLocation(double longitude, double latitude, String location) {
        try {
            if (new JSONObject(LocationRestfulApiRequester.saveLocation(this.context, longitude, latitude, location)).optInt("rs") == 0) {
                return false;
            }
            return true;
        } catch (JSONException e) {
            return false;
        }
    }

    public LocationModel getLocalLocationInfo(long userId) {
        try {
            return LocationDBUtil.getInstance(this.context).getLocationInfo(userId);
        } catch (Exception e) {
            return null;
        }
    }
}
