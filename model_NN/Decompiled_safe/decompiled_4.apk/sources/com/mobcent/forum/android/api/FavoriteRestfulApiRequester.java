package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.constant.PostsApiConstant;
import com.mobcent.forum.android.util.MCResource;
import java.util.HashMap;

public class FavoriteRestfulApiRequester extends BaseRestfulApiRequester implements PostsApiConstant {
    public static String getMyFavoriteTopicList(Context context, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest(MCResource.getInstance(context).getString("mc_forum_my_favor_topic_list_url"), params, context);
    }

    public static String addFavoriteTopic(Context context, long topicId, long boardId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        return doPostRequest("favors/addFavorite.do", params, context);
    }

    public static String delFavoriteTopic(Context context, long topicId, long boardId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        return doPostRequest("favors/delFavorite.do", params, context);
    }
}
