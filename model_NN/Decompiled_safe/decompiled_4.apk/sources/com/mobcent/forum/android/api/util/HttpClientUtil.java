package com.mobcent.forum.android.api.util;

import android.content.Context;
import com.mobcent.forum.android.service.delegate.DownProgressDelegate;
import com.mobcent.forum.android.util.MCLogUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.message.BasicNameValuePair;

public class HttpClientUtil {
    private static final int SET_CONNECTION_TIMEOUT = 30000;
    public static final String SET_CONNECTION_TIMEOUT_STR = "connection_timeout";
    private static final int SET_SOCKET_TIMEOUT = 100000;
    public static final String SET_SOCKET_TIMEOUT_STR = "socket_timeout";
    private static final String TAG = "HttpClientUtil";

    /* JADX INFO: Multiple debug info for r11v1 org.apache.http.client.HttpClient: [D('context' android.content.Context), D('client' org.apache.http.client.HttpClient)] */
    /* JADX INFO: Multiple debug info for r0v4 org.apache.http.client.methods.HttpPost: [D('httpPost' org.apache.http.client.methods.HttpPost), D('connectionTimeout' int)] */
    /* JADX INFO: Multiple debug info for r1v4 java.util.ArrayList: [D('nameValuePairs' java.util.List<org.apache.http.NameValuePair>), D('socketTimeout' int)] */
    /* JADX INFO: Multiple debug info for r11v6 org.apache.http.StatusLine: [D('client' org.apache.http.client.HttpClient), D('status' org.apache.http.StatusLine)] */
    /* JADX INFO: Multiple debug info for r11v7 int: [D('statusCode' int), D('status' org.apache.http.StatusLine)] */
    /* JADX INFO: Multiple debug info for r10v13 java.lang.String: [D('response' org.apache.http.HttpResponse), D('result' java.lang.String)] */
    public static String doPostRequest(String urlString, HashMap<String, String> params, Context context) {
        String result;
        HttpPost httpPost;
        String tempURL;
        String str;
        String tempURL2 = String.valueOf(urlString) + "?";
        int connectionTimeout = -1;
        int socketTimeout = -1;
        if (params != null && !params.isEmpty()) {
            if (params.get(SET_CONNECTION_TIMEOUT_STR) != null) {
                connectionTimeout = Integer.parseInt(params.get(SET_CONNECTION_TIMEOUT_STR));
            }
            if (params.get(SET_SOCKET_TIMEOUT_STR) != null) {
                socketTimeout = Integer.parseInt(params.get(SET_SOCKET_TIMEOUT_STR));
            }
        }
        HttpClient client = getNewHttpClient(context, connectionTimeout, socketTimeout);
        HttpPost httpPost2 = new HttpPost(urlString);
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            if (params != null && !params.isEmpty()) {
                for (String key : params.keySet()) {
                    nameValuePairs.add(new BasicNameValuePair(key, params.get(key)));
                    tempURL2 = String.valueOf(tempURL2) + key + "=" + params.get(key) + "&";
                }
            }
            MCLogUtil.i(TAG, "tempURL = " + tempURL2);
            httpPost2.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
            HttpPost httpPost3 = httpPost2;
            try {
                HttpResponse response = client.execute(httpPost3);
                if (response.getStatusLine().getStatusCode() != 200) {
                    return "connection_fail";
                }
                String result2 = read(response);
                try {
                    MCLogUtil.i(TAG, "result = " + result2);
                    if (result2 == null) {
                        str = "{}";
                    } else {
                        str = result2;
                    }
                    return str;
                } catch (Exception e) {
                    tempURL = tempURL2;
                    HttpPost httpPost4 = httpPost3;
                    request = e;
                    result = result2;
                    httpPost = httpPost4;
                    request.printStackTrace();
                    return "connection_fail";
                }
            } catch (Exception e2) {
                tempURL = tempURL2;
                result = "";
                HttpUriRequest httpUriRequest = e2;
                httpPost = httpPost3;
                request = httpUriRequest;
                request.printStackTrace();
                return "connection_fail";
            }
        } catch (Exception e3) {
            request = e3;
            httpPost = null;
            tempURL = tempURL2;
            result = "";
            request.printStackTrace();
            return "connection_fail";
        }
    }

    public static synchronized byte[] getFileInByte(String url, Context context) {
        byte[] bArr;
        synchronized (HttpClientUtil.class) {
            try {
                byte[] imgData = null;
                try {
                    HttpURLConnection connection = null;
                    InputStream is = null;
                    try {
                        connection = getNewHttpURLConnection(new URL(url), context);
                        connection.setDoInput(true);
                        connection.connect();
                        is = connection.getInputStream();
                        int length = connection.getContentLength();
                        if (length != -1) {
                            imgData = new byte[length];
                            byte[] temp = new byte[512];
                            int destPos = 0;
                            while (true) {
                                int readLen = is.read(temp);
                                if (readLen <= 0) {
                                    break;
                                }
                                System.arraycopy(temp, 0, imgData, destPos, readLen);
                                destPos += readLen;
                            }
                            byte[] bArr2 = null;
                        }
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (connection != null) {
                            connection.disconnect();
                        }
                        bArr = imgData;
                    } catch (IOException e2) {
                        if (is != null) {
                            is.close();
                        }
                        if (connection != null) {
                            connection.disconnect();
                        }
                        bArr = null;
                        return bArr;
                    } catch (IOException e3) {
                        e3.printStackTrace();
                        bArr = null;
                        return bArr;
                    } catch (Throwable th) {
                        th = th;
                    }
                } catch (Exception e4) {
                    bArr = null;
                }
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    /* JADX INFO: Multiple debug info for r2v5 java.io.IOException: [D('e' java.net.MalformedURLException), D('e' java.io.IOException)] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ae A[SYNTHETIC, Splitter:B:47:0x00ae] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00b6 A[Catch:{ IOException -> 0x00bf }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00bb A[Catch:{ IOException -> 0x00bf }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00c8 A[SYNTHETIC, Splitter:B:57:0x00c8] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00d0 A[Catch:{ IOException -> 0x00d9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00d5 A[Catch:{ IOException -> 0x00d9 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void downloadFile(java.lang.String r12, java.io.File r13, android.content.Context r14) {
        /*
            r6 = 0
            java.lang.String r9 = "HttpClientUtil"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0056 }
            java.lang.String r11 = "Image url is "
            r10.<init>(r11)     // Catch:{ Exception -> 0x0056 }
            java.lang.StringBuilder r10 = r10.append(r12)     // Catch:{ Exception -> 0x0056 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x0056 }
            com.mobcent.forum.android.util.MCLogUtil.i(r9, r10)     // Catch:{ Exception -> 0x0056 }
            java.net.URL r7 = new java.net.URL     // Catch:{ Exception -> 0x0056 }
            r7.<init>(r12)     // Catch:{ Exception -> 0x0056 }
            r1 = 0
            r5 = 0
            r3 = 0
            java.net.HttpURLConnection r1 = getNewHttpURLConnection(r7, r14)     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            r9 = 1
            r1.setDoInput(r9)     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            r1.connect()     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            java.io.InputStream r5 = r1.getInputStream()     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            boolean r9 = r13.createNewFile()     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            if (r9 == 0) goto L_0x0042
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            r4.<init>(r13)     // Catch:{ MalformedURLException -> 0x00ed, IOException -> 0x0081 }
            r9 = 256(0x100, float:3.59E-43)
            byte[] r0 = new byte[r9]     // Catch:{ MalformedURLException -> 0x005e, IOException -> 0x00e9, all -> 0x00e6 }
        L_0x003b:
            int r8 = r5.read(r0)     // Catch:{ MalformedURLException -> 0x005e, IOException -> 0x00e9, all -> 0x00e6 }
            if (r8 > 0) goto L_0x0059
            r3 = r4
        L_0x0042:
            if (r3 == 0) goto L_0x004a
            r3.flush()     // Catch:{ IOException -> 0x00df }
            r3.close()     // Catch:{ IOException -> 0x00df }
        L_0x004a:
            if (r5 == 0) goto L_0x004f
            r5.close()     // Catch:{ IOException -> 0x00df }
        L_0x004f:
            if (r1 == 0) goto L_0x0054
            r1.disconnect()     // Catch:{ IOException -> 0x00df }
        L_0x0054:
            r6 = r7
        L_0x0055:
            return
        L_0x0056:
            r9 = move-exception
            r2 = r9
            goto L_0x0055
        L_0x0059:
            r9 = 0
            r4.write(r0, r9, r8)     // Catch:{ MalformedURLException -> 0x005e, IOException -> 0x00e9, all -> 0x00e6 }
            goto L_0x003b
        L_0x005e:
            r9 = move-exception
            r2 = r9
            r3 = r4
        L_0x0061:
            java.lang.String r9 = "HttpClientUtil"
            java.lang.String r10 = "URL is format error"
            com.mobcent.forum.android.util.MCLogUtil.i(r9, r10)     // Catch:{ all -> 0x00c5 }
            if (r3 == 0) goto L_0x0070
            r3.flush()     // Catch:{ IOException -> 0x007b }
            r3.close()     // Catch:{ IOException -> 0x007b }
        L_0x0070:
            if (r5 == 0) goto L_0x0075
            r5.close()     // Catch:{ IOException -> 0x007b }
        L_0x0075:
            if (r1 == 0) goto L_0x0054
            r1.disconnect()     // Catch:{ IOException -> 0x007b }
            goto L_0x0054
        L_0x007b:
            r9 = move-exception
            r2 = r9
            r2.printStackTrace()
            goto L_0x0054
        L_0x0081:
            r9 = move-exception
            r2 = r9
        L_0x0083:
            java.lang.String r9 = "HttpClientUtil"
            java.lang.String r10 = "IO error when download file"
            com.mobcent.forum.android.util.MCLogUtil.i(r9, r10)     // Catch:{ all -> 0x00c5 }
            java.lang.String r9 = "HttpClientUtil"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c5 }
            java.lang.String r11 = "The URL is "
            r10.<init>(r11)     // Catch:{ all -> 0x00c5 }
            java.lang.StringBuilder r10 = r10.append(r12)     // Catch:{ all -> 0x00c5 }
            java.lang.String r11 = ";the file name "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x00c5 }
            java.lang.String r11 = r13.getName()     // Catch:{ all -> 0x00c5 }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x00c5 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x00c5 }
            com.mobcent.forum.android.util.MCLogUtil.i(r9, r10)     // Catch:{ all -> 0x00c5 }
            if (r3 == 0) goto L_0x00b4
            r3.flush()     // Catch:{ IOException -> 0x00bf }
            r3.close()     // Catch:{ IOException -> 0x00bf }
        L_0x00b4:
            if (r5 == 0) goto L_0x00b9
            r5.close()     // Catch:{ IOException -> 0x00bf }
        L_0x00b9:
            if (r1 == 0) goto L_0x0054
            r1.disconnect()     // Catch:{ IOException -> 0x00bf }
            goto L_0x0054
        L_0x00bf:
            r9 = move-exception
            r2 = r9
            r2.printStackTrace()
            goto L_0x0054
        L_0x00c5:
            r9 = move-exception
        L_0x00c6:
            if (r3 == 0) goto L_0x00ce
            r3.flush()     // Catch:{ IOException -> 0x00d9 }
            r3.close()     // Catch:{ IOException -> 0x00d9 }
        L_0x00ce:
            if (r5 == 0) goto L_0x00d3
            r5.close()     // Catch:{ IOException -> 0x00d9 }
        L_0x00d3:
            if (r1 == 0) goto L_0x00d8
            r1.disconnect()     // Catch:{ IOException -> 0x00d9 }
        L_0x00d8:
            throw r9
        L_0x00d9:
            r10 = move-exception
            r2 = r10
            r2.printStackTrace()
            goto L_0x00d8
        L_0x00df:
            r9 = move-exception
            r2 = r9
            r2.printStackTrace()
            goto L_0x0054
        L_0x00e6:
            r9 = move-exception
            r3 = r4
            goto L_0x00c6
        L_0x00e9:
            r9 = move-exception
            r2 = r9
            r3 = r4
            goto L_0x0083
        L_0x00ed:
            r9 = move-exception
            r2 = r9
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.forum.android.api.util.HttpClientUtil.downloadFile(java.lang.String, java.io.File, android.content.Context):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x00c4 A[SYNTHETIC, Splitter:B:51:0x00c4] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00cc A[Catch:{ IOException -> 0x00d8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00d1 A[Catch:{ IOException -> 0x00d8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00e8 A[SYNTHETIC, Splitter:B:63:0x00e8] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00f0 A[Catch:{ IOException -> 0x00f9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00f5 A[Catch:{ IOException -> 0x00f9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x013a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void downloadFile(java.lang.String r8, java.io.File r9, com.mobcent.forum.android.service.delegate.DownProgressDelegate r10, android.content.Context r11) {
        /*
            r0 = 0
            java.lang.String r1 = "HttpClientUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0060 }
            java.lang.String r3 = "Image url is "
            r2.<init>(r3)     // Catch:{ Exception -> 0x0060 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ Exception -> 0x0060 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0060 }
            com.mobcent.forum.android.util.MCLogUtil.i(r1, r2)     // Catch:{ Exception -> 0x0060 }
            java.net.URL r4 = new java.net.URL     // Catch:{ Exception -> 0x0060 }
            r4.<init>(r8)     // Catch:{ Exception -> 0x0060 }
            r0 = 0
            r2 = 0
            r1 = 0
            java.net.HttpURLConnection r0 = getNewHttpURLConnection(r4, r11)     // Catch:{ MalformedURLException -> 0x012c, IOException -> 0x0094, all -> 0x00e1 }
            r11 = 1
            r0.setDoInput(r11)     // Catch:{ MalformedURLException -> 0x012c, IOException -> 0x0094, all -> 0x00e1 }
            r0.connect()     // Catch:{ MalformedURLException -> 0x012c, IOException -> 0x0094, all -> 0x00e1 }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ MalformedURLException -> 0x012c, IOException -> 0x0094, all -> 0x00e1 }
            int r11 = r0.getContentLength()     // Catch:{ MalformedURLException -> 0x0133, IOException -> 0x011e, all -> 0x0106 }
            r10.setMax(r11)     // Catch:{ MalformedURLException -> 0x0133, IOException -> 0x011e, all -> 0x0106 }
            boolean r11 = r9.createNewFile()     // Catch:{ MalformedURLException -> 0x0133, IOException -> 0x011e, all -> 0x0106 }
            if (r11 == 0) goto L_0x0143
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ MalformedURLException -> 0x0133, IOException -> 0x011e, all -> 0x0106 }
            r2.<init>(r9)     // Catch:{ MalformedURLException -> 0x0133, IOException -> 0x011e, all -> 0x0106 }
            r11 = 256(0x100, float:3.59E-43)
            byte[] r11 = new byte[r11]     // Catch:{ MalformedURLException -> 0x006c, IOException -> 0x0125, all -> 0x010c }
            r1 = 0
        L_0x0043:
            int r5 = r3.read(r11)     // Catch:{ MalformedURLException -> 0x006c, IOException -> 0x0125, all -> 0x010c }
            if (r5 > 0) goto L_0x0063
            r9 = r2
        L_0x004a:
            if (r9 == 0) goto L_0x0052
            r9.flush()     // Catch:{ IOException -> 0x00fe }
            r9.close()     // Catch:{ IOException -> 0x00fe }
        L_0x0052:
            if (r3 == 0) goto L_0x0057
            r3.close()     // Catch:{ IOException -> 0x00fe }
        L_0x0057:
            if (r0 == 0) goto L_0x0102
            r0.disconnect()     // Catch:{ IOException -> 0x00fe }
            r10 = r3
            r8 = r0
        L_0x005e:
            r8 = r4
        L_0x005f:
            return
        L_0x0060:
            r8 = move-exception
            r8 = r0
            goto L_0x005f
        L_0x0063:
            r6 = 0
            r2.write(r11, r6, r5)     // Catch:{ MalformedURLException -> 0x006c, IOException -> 0x0125, all -> 0x010c }
            int r1 = r1 + r5
            r10.setProgress(r1)     // Catch:{ MalformedURLException -> 0x006c, IOException -> 0x0125, all -> 0x010c }
            goto L_0x0043
        L_0x006c:
            r8 = move-exception
            r9 = r8
            r10 = r2
            r11 = r3
            r8 = r0
        L_0x0071:
            java.lang.String r9 = "HttpClientUtil"
            java.lang.String r0 = "URL is format error"
            com.mobcent.forum.android.util.MCLogUtil.i(r9, r0)     // Catch:{ all -> 0x0112 }
            if (r10 == 0) goto L_0x0080
            r10.flush()     // Catch:{ IOException -> 0x008d }
            r10.close()     // Catch:{ IOException -> 0x008d }
        L_0x0080:
            if (r11 == 0) goto L_0x0085
            r11.close()     // Catch:{ IOException -> 0x008d }
        L_0x0085:
            if (r8 == 0) goto L_0x013f
            r8.disconnect()     // Catch:{ IOException -> 0x008d }
            r9 = r10
            r10 = r11
            goto L_0x005e
        L_0x008d:
            r9 = move-exception
            r9.printStackTrace()
            r9 = r10
            r10 = r11
            goto L_0x005e
        L_0x0094:
            r10 = move-exception
            r11 = r10
            r10 = r0
            r0 = r1
            r1 = r2
        L_0x0099:
            java.lang.String r11 = "HttpClientUtil"
            java.lang.String r2 = "IO error when download file"
            com.mobcent.forum.android.util.MCLogUtil.i(r11, r2)     // Catch:{ all -> 0x0118 }
            java.lang.String r11 = "HttpClientUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0118 }
            java.lang.String r3 = "The URL is "
            r2.<init>(r3)     // Catch:{ all -> 0x0118 }
            java.lang.StringBuilder r8 = r2.append(r8)     // Catch:{ all -> 0x0118 }
            java.lang.String r2 = ";the file name "
            java.lang.StringBuilder r8 = r8.append(r2)     // Catch:{ all -> 0x0118 }
            java.lang.String r9 = r9.getName()     // Catch:{ all -> 0x0118 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x0118 }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0118 }
            com.mobcent.forum.android.util.MCLogUtil.i(r11, r8)     // Catch:{ all -> 0x0118 }
            if (r0 == 0) goto L_0x00ca
            r0.flush()     // Catch:{ IOException -> 0x00d8 }
            r0.close()     // Catch:{ IOException -> 0x00d8 }
        L_0x00ca:
            if (r1 == 0) goto L_0x00cf
            r1.close()     // Catch:{ IOException -> 0x00d8 }
        L_0x00cf:
            if (r10 == 0) goto L_0x013a
            r10.disconnect()     // Catch:{ IOException -> 0x00d8 }
            r9 = r0
            r8 = r10
            r10 = r1
            goto L_0x005e
        L_0x00d8:
            r8 = move-exception
            r8.printStackTrace()
            r9 = r0
            r8 = r10
            r10 = r1
            goto L_0x005e
        L_0x00e1:
            r8 = move-exception
            r11 = r8
            r9 = r1
            r10 = r2
            r8 = r0
        L_0x00e6:
            if (r9 == 0) goto L_0x00ee
            r9.flush()     // Catch:{ IOException -> 0x00f9 }
            r9.close()     // Catch:{ IOException -> 0x00f9 }
        L_0x00ee:
            if (r10 == 0) goto L_0x00f3
            r10.close()     // Catch:{ IOException -> 0x00f9 }
        L_0x00f3:
            if (r8 == 0) goto L_0x00f8
            r8.disconnect()     // Catch:{ IOException -> 0x00f9 }
        L_0x00f8:
            throw r11
        L_0x00f9:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x00f8
        L_0x00fe:
            r8 = move-exception
            r8.printStackTrace()
        L_0x0102:
            r10 = r3
            r8 = r0
            goto L_0x005e
        L_0x0106:
            r8 = move-exception
            r11 = r8
            r9 = r1
            r10 = r3
            r8 = r0
            goto L_0x00e6
        L_0x010c:
            r8 = move-exception
            r11 = r8
            r9 = r2
            r10 = r3
            r8 = r0
            goto L_0x00e6
        L_0x0112:
            r9 = move-exception
            r7 = r9
            r9 = r10
            r10 = r11
            r11 = r7
            goto L_0x00e6
        L_0x0118:
            r8 = move-exception
            r11 = r8
            r9 = r0
            r8 = r10
            r10 = r1
            goto L_0x00e6
        L_0x011e:
            r10 = move-exception
            r11 = r10
            r10 = r0
            r0 = r1
            r1 = r3
            goto L_0x0099
        L_0x0125:
            r10 = move-exception
            r11 = r10
            r1 = r3
            r10 = r0
            r0 = r2
            goto L_0x0099
        L_0x012c:
            r8 = move-exception
            r9 = r8
            r10 = r1
            r11 = r2
            r8 = r0
            goto L_0x0071
        L_0x0133:
            r8 = move-exception
            r9 = r8
            r10 = r1
            r11 = r3
            r8 = r0
            goto L_0x0071
        L_0x013a:
            r9 = r0
            r8 = r10
            r10 = r1
            goto L_0x005e
        L_0x013f:
            r9 = r10
            r10 = r11
            goto L_0x005e
        L_0x0143:
            r9 = r1
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.forum.android.api.util.HttpClientUtil.downloadFile(java.lang.String, java.io.File, com.mobcent.forum.android.service.delegate.DownProgressDelegate, android.content.Context):void");
    }

    public static synchronized byte[] getFileInByteByProgress(String url, Context context, DownProgressDelegate downProgress) {
        byte[] bArr;
        synchronized (HttpClientUtil.class) {
            try {
                byte[] imgData = null;
                try {
                    HttpURLConnection connection = null;
                    InputStream is = null;
                    try {
                        connection = getNewHttpURLConnection(new URL(url), context);
                        connection.setDoInput(true);
                        connection.connect();
                        is = connection.getInputStream();
                        int length = connection.getContentLength();
                        downProgress.setMax(length);
                        if (length != -1) {
                            imgData = new byte[length];
                            byte[] temp = new byte[512];
                            int destPos = 0;
                            while (true) {
                                int readLen = is.read(temp);
                                if (readLen <= 0) {
                                    break;
                                }
                                System.arraycopy(temp, 0, imgData, destPos, readLen);
                                destPos += readLen;
                                downProgress.setProgress(destPos);
                            }
                            byte[] bArr2 = null;
                        }
                        if (is != null) {
                            try {
                                is.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (connection != null) {
                            connection.disconnect();
                        }
                        bArr = imgData;
                    } catch (IOException e2) {
                        if (is != null) {
                            is.close();
                        }
                        if (connection != null) {
                            connection.disconnect();
                        }
                        bArr = null;
                        return bArr;
                    } catch (IOException e3) {
                        e3.printStackTrace();
                        bArr = null;
                        return bArr;
                    } catch (Throwable th) {
                        th = th;
                    }
                } catch (Exception e4) {
                    bArr = null;
                }
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
    }

    /* JADX INFO: Multiple debug info for r13v24 java.net.HttpURLConnection: [D('context' android.content.Context), D('con' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r11v73 byte[]: [D('bufferSize' int), D('buffer' byte[])] */
    /* JADX INFO: Multiple debug info for r14v40 java.lang.String: [D('jsonStr' java.lang.String), D('boundary' java.lang.String)] */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x022a A[SYNTHETIC, Splitter:B:104:0x022a] */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x022f A[Catch:{ IOException -> 0x0240 }] */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0234 A[Catch:{ IOException -> 0x0240 }] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x023c A[Catch:{ IOException -> 0x0240 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0172 A[SYNTHETIC, Splitter:B:41:0x0172] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0177 A[Catch:{ IOException -> 0x0194 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x017c A[Catch:{ IOException -> 0x0194 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0184 A[Catch:{ IOException -> 0x0194 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01a8 A[SYNTHETIC, Splitter:B:60:0x01a8] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x01ad A[Catch:{ IOException -> 0x01c5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01b2 A[Catch:{ IOException -> 0x01c5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01ba A[Catch:{ IOException -> 0x01c5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01d5 A[SYNTHETIC, Splitter:B:76:0x01d5] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01da A[Catch:{ IOException -> 0x01f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01df A[Catch:{ IOException -> 0x01f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01e7 A[Catch:{ IOException -> 0x01f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x01ff A[SYNTHETIC, Splitter:B:90:0x01ff] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0204 A[Catch:{ IOException -> 0x021c }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0209 A[Catch:{ IOException -> 0x021c }] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0211 A[Catch:{ IOException -> 0x021c }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:73:0x01d0=Splitter:B:73:0x01d0, B:38:0x016d=Splitter:B:38:0x016d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String uploadFile(java.lang.String r11, java.lang.String r12, android.content.Context r13, java.lang.String r14) {
        /*
            r0 = 0
            r5 = 0
            r1 = 0
            r3 = 0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r11 = java.lang.String.valueOf(r11)
            r2.<init>(r11)
            java.lang.String r11 = "?"
            java.lang.StringBuilder r11 = r2.append(r11)
            java.lang.StringBuilder r11 = r11.append(r14)
            java.lang.String r11 = r11.toString()
            java.lang.String r14 = "HttpClientUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r4 = "uploadUrl = "
            r2.<init>(r4)
            java.lang.StringBuilder r2 = r2.append(r11)
            java.lang.String r4 = ";uploadFile="
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r12)
            java.lang.String r2 = r2.toString()
            com.mobcent.forum.android.util.MCLogUtil.i(r14, r2)
            boolean r14 = com.mobcent.forum.android.util.PhoneConnectionUtil.isNetworkAvailable(r13)
            if (r14 != 0) goto L_0x004f
            java.lang.String r11 = "HttpClientUtil"
            java.lang.String r12 = "connnect fail"
            com.mobcent.forum.android.util.MCLogUtil.i(r11, r12)
            java.lang.String r11 = "connection_fail"
            r13 = r3
            r12 = r1
            r14 = r5
            r9 = r0
            r0 = r11
            r11 = r9
        L_0x004e:
            return r0
        L_0x004f:
            java.lang.String r2 = "\r\n"
            java.lang.String r6 = "--"
            java.lang.String r14 = "*****"
            java.lang.String r4 = ""
            if (r12 == 0) goto L_0x006e
            java.lang.String r7 = com.mobcent.forum.android.util.MCLibIOUtil.FS
            int r7 = r12.lastIndexOf(r7)
            r8 = -1
            if (r7 <= r8) goto L_0x006e
            java.lang.String r4 = com.mobcent.forum.android.util.MCLibIOUtil.FS
            int r4 = r12.lastIndexOf(r4)
            int r4 = r4 + 1
            java.lang.String r4 = r12.substring(r4)
        L_0x006e:
            java.net.URL r7 = new java.net.URL     // Catch:{ ClientProtocolException -> 0x02d4, SocketTimeoutException -> 0x0199, IOException -> 0x01ca, Exception -> 0x01f7, all -> 0x0221 }
            r7.<init>(r11)     // Catch:{ ClientProtocolException -> 0x02d4, SocketTimeoutException -> 0x0199, IOException -> 0x01ca, Exception -> 0x01f7, all -> 0x0221 }
            java.net.HttpURLConnection r13 = getNewHttpURLConnection(r7, r13)     // Catch:{ ClientProtocolException -> 0x02d4, SocketTimeoutException -> 0x0199, IOException -> 0x01ca, Exception -> 0x01f7, all -> 0x0221 }
            r11 = 1
            r13.setDoInput(r11)     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            r11 = 1
            r13.setDoOutput(r11)     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            r11 = 0
            r13.setUseCaches(r11)     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            r11 = 180000(0x2bf20, float:2.52234E-40)
            r13.setConnectTimeout(r11)     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            r11 = 180000(0x2bf20, float:2.52234E-40)
            r13.setReadTimeout(r11)     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            java.lang.String r11 = "POST"
            r13.setRequestMethod(r11)     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            java.lang.String r11 = "Connection"
            java.lang.String r0 = "Keep-Alive"
            r13.setRequestProperty(r11, r0)     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            java.lang.String r11 = "Charset"
            java.lang.String r0 = "UTF-8"
            r13.setRequestProperty(r11, r0)     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            java.lang.String r11 = "Content-Type"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            java.lang.String r7 = "multipart/form-data;boundary="
            r0.<init>(r7)     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            java.lang.StringBuilder r0 = r0.append(r14)     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            java.lang.String r0 = r0.toString()     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            r13.setRequestProperty(r11, r0)     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            java.io.DataOutputStream r0 = new java.io.DataOutputStream     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            java.io.OutputStream r11 = r13.getOutputStream()     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            r0.<init>(r11)     // Catch:{ ClientProtocolException -> 0x02dc, SocketTimeoutException -> 0x02af, IOException -> 0x028a, Exception -> 0x0267, all -> 0x0245 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            java.lang.String r1 = java.lang.String.valueOf(r6)     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            r11.<init>(r1)     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            java.lang.StringBuilder r11 = r11.append(r14)     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            java.lang.StringBuilder r11 = r11.append(r2)     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            java.lang.String r11 = r11.toString()     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            r0.writeBytes(r11)     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            java.lang.String r1 = "Content-Disposition: form-data; name=\"file1\";filename=\""
            r11.<init>(r1)     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            java.lang.StringBuilder r11 = r11.append(r4)     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            java.lang.String r1 = "\""
            java.lang.StringBuilder r11 = r11.append(r1)     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            java.lang.StringBuilder r11 = r11.append(r2)     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            java.lang.String r11 = r11.toString()     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            r0.writeBytes(r11)     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            r0.writeBytes(r2)     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            r1.<init>(r12)     // Catch:{ ClientProtocolException -> 0x02e5, SocketTimeoutException -> 0x02b8, IOException -> 0x0293, Exception -> 0x026f, all -> 0x024c }
            r11 = 1024(0x400, float:1.435E-42)
            byte[] r11 = new byte[r11]     // Catch:{ ClientProtocolException -> 0x0166, SocketTimeoutException -> 0x02c1, IOException -> 0x029c, Exception -> 0x0277, all -> 0x0253 }
            r12 = -1
        L_0x0100:
            int r12 = r1.read(r11)     // Catch:{ ClientProtocolException -> 0x0166, SocketTimeoutException -> 0x02c1, IOException -> 0x029c, Exception -> 0x0277, all -> 0x0253 }
            r3 = -1
            if (r12 != r3) goto L_0x0161
            r0.writeBytes(r2)     // Catch:{ ClientProtocolException -> 0x0166, SocketTimeoutException -> 0x02c1, IOException -> 0x029c, Exception -> 0x0277, all -> 0x0253 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x0166, SocketTimeoutException -> 0x02c1, IOException -> 0x029c, Exception -> 0x0277, all -> 0x0253 }
            java.lang.String r12 = java.lang.String.valueOf(r6)     // Catch:{ ClientProtocolException -> 0x0166, SocketTimeoutException -> 0x02c1, IOException -> 0x029c, Exception -> 0x0277, all -> 0x0253 }
            r11.<init>(r12)     // Catch:{ ClientProtocolException -> 0x0166, SocketTimeoutException -> 0x02c1, IOException -> 0x029c, Exception -> 0x0277, all -> 0x0253 }
            java.lang.StringBuilder r11 = r11.append(r14)     // Catch:{ ClientProtocolException -> 0x0166, SocketTimeoutException -> 0x02c1, IOException -> 0x029c, Exception -> 0x0277, all -> 0x0253 }
            java.lang.StringBuilder r11 = r11.append(r6)     // Catch:{ ClientProtocolException -> 0x0166, SocketTimeoutException -> 0x02c1, IOException -> 0x029c, Exception -> 0x0277, all -> 0x0253 }
            java.lang.StringBuilder r11 = r11.append(r2)     // Catch:{ ClientProtocolException -> 0x0166, SocketTimeoutException -> 0x02c1, IOException -> 0x029c, Exception -> 0x0277, all -> 0x0253 }
            java.lang.String r11 = r11.toString()     // Catch:{ ClientProtocolException -> 0x0166, SocketTimeoutException -> 0x02c1, IOException -> 0x029c, Exception -> 0x0277, all -> 0x0253 }
            r0.writeBytes(r11)     // Catch:{ ClientProtocolException -> 0x0166, SocketTimeoutException -> 0x02c1, IOException -> 0x029c, Exception -> 0x0277, all -> 0x0253 }
            java.io.InputStream r12 = r13.getInputStream()     // Catch:{ ClientProtocolException -> 0x0166, SocketTimeoutException -> 0x02c1, IOException -> 0x029c, Exception -> 0x0277, all -> 0x0253 }
            java.lang.String r14 = com.mobcent.forum.android.util.MCLibIOUtil.convertStreamToString(r12)     // Catch:{ ClientProtocolException -> 0x02ee, SocketTimeoutException -> 0x02ca, IOException -> 0x02a5, Exception -> 0x0280, all -> 0x025a }
            java.lang.String r11 = "HttpClientUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x02ee, SocketTimeoutException -> 0x02ca, IOException -> 0x02a5, Exception -> 0x0280, all -> 0x025a }
            java.lang.String r3 = "jsonStr="
            r2.<init>(r3)     // Catch:{ ClientProtocolException -> 0x02ee, SocketTimeoutException -> 0x02ca, IOException -> 0x02a5, Exception -> 0x0280, all -> 0x025a }
            java.lang.StringBuilder r2 = r2.append(r14)     // Catch:{ ClientProtocolException -> 0x02ee, SocketTimeoutException -> 0x02ca, IOException -> 0x02a5, Exception -> 0x0280, all -> 0x025a }
            java.lang.String r2 = r2.toString()     // Catch:{ ClientProtocolException -> 0x02ee, SocketTimeoutException -> 0x02ca, IOException -> 0x02a5, Exception -> 0x0280, all -> 0x025a }
            com.mobcent.forum.android.util.MCLogUtil.i(r11, r2)     // Catch:{ ClientProtocolException -> 0x02ee, SocketTimeoutException -> 0x02ca, IOException -> 0x02a5, Exception -> 0x0280, all -> 0x025a }
            if (r12 == 0) goto L_0x0147
            r12.close()     // Catch:{ IOException -> 0x018f }
        L_0x0147:
            if (r1 == 0) goto L_0x014c
            r1.close()     // Catch:{ IOException -> 0x018f }
        L_0x014c:
            if (r0 == 0) goto L_0x0154
            r0.flush()     // Catch:{ IOException -> 0x018f }
            r0.close()     // Catch:{ IOException -> 0x018f }
        L_0x0154:
            if (r13 == 0) goto L_0x0159
            r13.disconnect()     // Catch:{ IOException -> 0x018f }
        L_0x0159:
            r11 = r13
            r13 = r1
            r9 = r12
            r12 = r0
            r0 = r14
            r14 = r9
            goto L_0x004e
        L_0x0161:
            r3 = 0
            r0.write(r11, r3, r12)     // Catch:{ ClientProtocolException -> 0x0166, SocketTimeoutException -> 0x02c1, IOException -> 0x029c, Exception -> 0x0277, all -> 0x0253 }
            goto L_0x0100
        L_0x0166:
            r11 = move-exception
            r14 = r1
            r12 = r0
            r0 = r5
            r9 = r11
            r11 = r13
            r13 = r9
        L_0x016d:
            r13.printStackTrace()     // Catch:{ all -> 0x0261 }
            if (r0 == 0) goto L_0x0175
            r0.close()     // Catch:{ IOException -> 0x0194 }
        L_0x0175:
            if (r14 == 0) goto L_0x017a
            r14.close()     // Catch:{ IOException -> 0x0194 }
        L_0x017a:
            if (r12 == 0) goto L_0x0182
            r12.flush()     // Catch:{ IOException -> 0x0194 }
            r12.close()     // Catch:{ IOException -> 0x0194 }
        L_0x0182:
            if (r11 == 0) goto L_0x0187
            r11.disconnect()     // Catch:{ IOException -> 0x0194 }
        L_0x0187:
            java.lang.String r13 = "connection_fail"
            r9 = r14
            r14 = r0
            r0 = r13
            r13 = r9
            goto L_0x004e
        L_0x018f:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x0159
        L_0x0194:
            r13 = move-exception
            r13.printStackTrace()
            goto L_0x0187
        L_0x0199:
            r11 = move-exception
            r13 = r11
            r14 = r3
            r12 = r1
            r11 = r0
            r0 = r5
        L_0x019f:
            java.lang.String r13 = "HttpClientUtil"
            java.lang.String r1 = "SocketTimeoutException"
            com.mobcent.forum.android.util.MCLogUtil.i(r13, r1)     // Catch:{ all -> 0x0261 }
            if (r0 == 0) goto L_0x01ab
            r0.close()     // Catch:{ IOException -> 0x01c5 }
        L_0x01ab:
            if (r14 == 0) goto L_0x01b0
            r14.close()     // Catch:{ IOException -> 0x01c5 }
        L_0x01b0:
            if (r12 == 0) goto L_0x01b8
            r12.flush()     // Catch:{ IOException -> 0x01c5 }
            r12.close()     // Catch:{ IOException -> 0x01c5 }
        L_0x01b8:
            if (r11 == 0) goto L_0x01bd
            r11.disconnect()     // Catch:{ IOException -> 0x01c5 }
        L_0x01bd:
            java.lang.String r13 = "connection_fail"
            r9 = r14
            r14 = r0
            r0 = r13
            r13 = r9
            goto L_0x004e
        L_0x01c5:
            r13 = move-exception
            r13.printStackTrace()
            goto L_0x01bd
        L_0x01ca:
            r11 = move-exception
            r13 = r11
            r14 = r3
            r12 = r1
            r11 = r0
            r0 = r5
        L_0x01d0:
            r13.printStackTrace()     // Catch:{ all -> 0x0261 }
            if (r0 == 0) goto L_0x01d8
            r0.close()     // Catch:{ IOException -> 0x01f2 }
        L_0x01d8:
            if (r14 == 0) goto L_0x01dd
            r14.close()     // Catch:{ IOException -> 0x01f2 }
        L_0x01dd:
            if (r12 == 0) goto L_0x01e5
            r12.flush()     // Catch:{ IOException -> 0x01f2 }
            r12.close()     // Catch:{ IOException -> 0x01f2 }
        L_0x01e5:
            if (r11 == 0) goto L_0x01ea
            r11.disconnect()     // Catch:{ IOException -> 0x01f2 }
        L_0x01ea:
            java.lang.String r13 = "connection_fail"
            r9 = r14
            r14 = r0
            r0 = r13
            r13 = r9
            goto L_0x004e
        L_0x01f2:
            r13 = move-exception
            r13.printStackTrace()
            goto L_0x01ea
        L_0x01f7:
            r11 = move-exception
            r13 = r11
            r14 = r3
            r12 = r1
            r11 = r0
            r0 = r5
        L_0x01fd:
            if (r0 == 0) goto L_0x0202
            r0.close()     // Catch:{ IOException -> 0x021c }
        L_0x0202:
            if (r14 == 0) goto L_0x0207
            r14.close()     // Catch:{ IOException -> 0x021c }
        L_0x0207:
            if (r12 == 0) goto L_0x020f
            r12.flush()     // Catch:{ IOException -> 0x021c }
            r12.close()     // Catch:{ IOException -> 0x021c }
        L_0x020f:
            if (r11 == 0) goto L_0x0214
            r11.disconnect()     // Catch:{ IOException -> 0x021c }
        L_0x0214:
            java.lang.String r13 = "upload_images_fail"
            r9 = r14
            r14 = r0
            r0 = r13
            r13 = r9
            goto L_0x004e
        L_0x021c:
            r13 = move-exception
            r13.printStackTrace()
            goto L_0x0214
        L_0x0221:
            r11 = move-exception
            r13 = r3
            r12 = r1
            r14 = r5
            r9 = r11
            r11 = r0
            r0 = r9
        L_0x0228:
            if (r14 == 0) goto L_0x022d
            r14.close()     // Catch:{ IOException -> 0x0240 }
        L_0x022d:
            if (r13 == 0) goto L_0x0232
            r13.close()     // Catch:{ IOException -> 0x0240 }
        L_0x0232:
            if (r12 == 0) goto L_0x023a
            r12.flush()     // Catch:{ IOException -> 0x0240 }
            r12.close()     // Catch:{ IOException -> 0x0240 }
        L_0x023a:
            if (r11 == 0) goto L_0x023f
            r11.disconnect()     // Catch:{ IOException -> 0x0240 }
        L_0x023f:
            throw r0
        L_0x0240:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x023f
        L_0x0245:
            r11 = move-exception
            r0 = r11
            r12 = r1
            r14 = r5
            r11 = r13
            r13 = r3
            goto L_0x0228
        L_0x024c:
            r11 = move-exception
            r12 = r0
            r14 = r5
            r0 = r11
            r11 = r13
            r13 = r3
            goto L_0x0228
        L_0x0253:
            r11 = move-exception
            r12 = r0
            r14 = r5
            r0 = r11
            r11 = r13
            r13 = r1
            goto L_0x0228
        L_0x025a:
            r11 = move-exception
            r14 = r12
            r12 = r0
            r0 = r11
            r11 = r13
            r13 = r1
            goto L_0x0228
        L_0x0261:
            r13 = move-exception
            r9 = r13
            r13 = r14
            r14 = r0
            r0 = r9
            goto L_0x0228
        L_0x0267:
            r11 = move-exception
            r14 = r3
            r12 = r1
            r0 = r5
            r9 = r11
            r11 = r13
            r13 = r9
            goto L_0x01fd
        L_0x026f:
            r11 = move-exception
            r14 = r3
            r12 = r0
            r0 = r5
            r9 = r11
            r11 = r13
            r13 = r9
            goto L_0x01fd
        L_0x0277:
            r11 = move-exception
            r14 = r1
            r12 = r0
            r0 = r5
            r9 = r11
            r11 = r13
            r13 = r9
            goto L_0x01fd
        L_0x0280:
            r11 = move-exception
            r14 = r1
            r9 = r11
            r11 = r13
            r13 = r9
            r10 = r12
            r12 = r0
            r0 = r10
            goto L_0x01fd
        L_0x028a:
            r11 = move-exception
            r14 = r3
            r12 = r1
            r0 = r5
            r9 = r11
            r11 = r13
            r13 = r9
            goto L_0x01d0
        L_0x0293:
            r11 = move-exception
            r14 = r3
            r12 = r0
            r0 = r5
            r9 = r11
            r11 = r13
            r13 = r9
            goto L_0x01d0
        L_0x029c:
            r11 = move-exception
            r14 = r1
            r12 = r0
            r0 = r5
            r9 = r11
            r11 = r13
            r13 = r9
            goto L_0x01d0
        L_0x02a5:
            r11 = move-exception
            r14 = r1
            r9 = r11
            r11 = r13
            r13 = r9
            r10 = r12
            r12 = r0
            r0 = r10
            goto L_0x01d0
        L_0x02af:
            r11 = move-exception
            r14 = r3
            r12 = r1
            r0 = r5
            r9 = r11
            r11 = r13
            r13 = r9
            goto L_0x019f
        L_0x02b8:
            r11 = move-exception
            r14 = r3
            r12 = r0
            r0 = r5
            r9 = r11
            r11 = r13
            r13 = r9
            goto L_0x019f
        L_0x02c1:
            r11 = move-exception
            r14 = r1
            r12 = r0
            r0 = r5
            r9 = r11
            r11 = r13
            r13 = r9
            goto L_0x019f
        L_0x02ca:
            r11 = move-exception
            r14 = r1
            r9 = r11
            r11 = r13
            r13 = r9
            r10 = r12
            r12 = r0
            r0 = r10
            goto L_0x019f
        L_0x02d4:
            r11 = move-exception
            r13 = r11
            r14 = r3
            r12 = r1
            r11 = r0
            r0 = r5
            goto L_0x016d
        L_0x02dc:
            r11 = move-exception
            r14 = r3
            r12 = r1
            r0 = r5
            r9 = r11
            r11 = r13
            r13 = r9
            goto L_0x016d
        L_0x02e5:
            r11 = move-exception
            r14 = r3
            r12 = r0
            r0 = r5
            r9 = r11
            r11 = r13
            r13 = r9
            goto L_0x016d
        L_0x02ee:
            r11 = move-exception
            r14 = r1
            r9 = r11
            r11 = r13
            r13 = r9
            r10 = r12
            r12 = r0
            r0 = r10
            goto L_0x016d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.forum.android.api.util.HttpClientUtil.uploadFile(java.lang.String, java.lang.String, android.content.Context, java.lang.String):java.lang.String");
    }

    /* JADX INFO: Multiple debug info for r9v5 java.lang.Exception: [D('client' org.apache.http.client.HttpClient), D('mCursor' android.database.Cursor)] */
    /* JADX INFO: Multiple debug info for r0v2 org.apache.http.params.BasicHttpParams: [D('trustStore' java.security.KeyStore), D('params' org.apache.http.params.HttpParams)] */
    /* JADX INFO: Multiple debug info for r8v11 'mCursor'  android.database.Cursor: [D('mCursor' android.database.Cursor), D('context' android.content.Context)] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.apache.http.client.HttpClient getNewHttpClient(android.content.Context r8, int r9, int r10) {
        /*
            r6 = 0
            java.lang.String r0 = java.security.KeyStore.getDefaultType()     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            java.security.KeyStore r0 = java.security.KeyStore.getInstance(r0)     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            r1 = 0
            r2 = 0
            r0.load(r1, r2)     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            org.apache.http.params.BasicHttpParams r0 = new org.apache.http.params.BasicHttpParams     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            r0.<init>()     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            if (r9 <= 0) goto L_0x0080
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r0, r9)     // Catch:{ Exception -> 0x0086, all -> 0x009d }
        L_0x0018:
            if (r10 <= 0) goto L_0x0096
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r0, r10)     // Catch:{ Exception -> 0x0086, all -> 0x009d }
        L_0x001d:
            org.apache.http.HttpVersion r9 = org.apache.http.HttpVersion.HTTP_1_1     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            org.apache.http.params.HttpProtocolParams.setVersion(r0, r9)     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            java.lang.String r9 = "UTF-8"
            org.apache.http.params.HttpProtocolParams.setContentCharset(r0, r9)     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            org.apache.http.impl.client.DefaultHttpClient r9 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            r9.<init>(r0)     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            java.lang.String r10 = "wifi"
            java.lang.Object r10 = r8.getSystemService(r10)     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            android.net.wifi.WifiManager r10 = (android.net.wifi.WifiManager) r10     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            boolean r10 = r10.isWifiEnabled()     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            if (r10 != 0) goto L_0x00b2
            java.lang.String r10 = "content://telephony/carriers/preferapn"
            android.net.Uri r1 = android.net.Uri.parse(r10)     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            android.content.ContentResolver r0 = r8.getContentResolver()     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            if (r8 == 0) goto L_0x007a
            boolean r10 = r8.moveToFirst()     // Catch:{ Exception -> 0x00ad, all -> 0x00a6 }
            if (r10 == 0) goto L_0x007a
            java.lang.String r10 = "proxy"
            int r10 = r8.getColumnIndex(r10)     // Catch:{ Exception -> 0x00ad, all -> 0x00a6 }
            java.lang.String r0 = r8.getString(r10)     // Catch:{ Exception -> 0x00ad, all -> 0x00a6 }
            if (r0 == 0) goto L_0x007a
            java.lang.String r10 = r0.trim()     // Catch:{ Exception -> 0x00ad, all -> 0x00a6 }
            int r10 = r10.length()     // Catch:{ Exception -> 0x00ad, all -> 0x00a6 }
            if (r10 <= 0) goto L_0x007a
            org.apache.http.HttpHost r10 = new org.apache.http.HttpHost     // Catch:{ Exception -> 0x00ad, all -> 0x00a6 }
            r1 = 80
            r10.<init>(r0, r1)     // Catch:{ Exception -> 0x00ad, all -> 0x00a6 }
            org.apache.http.params.HttpParams r0 = r9.getParams()     // Catch:{ Exception -> 0x00ad, all -> 0x00a6 }
            java.lang.String r1 = "http.route.default-proxy"
            r0.setParameter(r1, r10)     // Catch:{ Exception -> 0x00ad, all -> 0x00a6 }
        L_0x007a:
            if (r8 == 0) goto L_0x007f
            r8.close()
        L_0x007f:
            return r9
        L_0x0080:
            r9 = 30000(0x7530, float:4.2039E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r0, r9)     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            goto L_0x0018
        L_0x0086:
            r8 = move-exception
            r9 = r6
        L_0x0088:
            org.apache.http.impl.client.DefaultHttpClient r8 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ all -> 0x00a8 }
            r8.<init>()     // Catch:{ all -> 0x00a8 }
            if (r9 == 0) goto L_0x0092
            r9.close()
        L_0x0092:
            r7 = r9
            r9 = r8
            r8 = r7
            goto L_0x007f
        L_0x0096:
            r9 = 100000(0x186a0, float:1.4013E-40)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r0, r9)     // Catch:{ Exception -> 0x0086, all -> 0x009d }
            goto L_0x001d
        L_0x009d:
            r8 = move-exception
            r9 = r8
            r8 = r6
        L_0x00a0:
            if (r8 == 0) goto L_0x00a5
            r8.close()
        L_0x00a5:
            throw r9
        L_0x00a6:
            r9 = move-exception
            goto L_0x00a0
        L_0x00a8:
            r8 = move-exception
            r7 = r8
            r8 = r9
            r9 = r7
            goto L_0x00a0
        L_0x00ad:
            r9 = move-exception
            r7 = r9
            r9 = r8
            r8 = r7
            goto L_0x0088
        L_0x00b2:
            r8 = r6
            goto L_0x007a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.forum.android.api.util.HttpClientUtil.getNewHttpClient(android.content.Context, int, int):org.apache.http.client.HttpClient");
    }

    public static HttpURLConnection getNewHttpURLConnection1(Context context) {
        MalformedURLException e;
        IOException e2;
        try {
            URL url = new URL("http://192.168.1.44:8080/test3/time");
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(url.openConnection().getInputStream()));
                while (true) {
                    String line = br.readLine();
                    if (line == null) {
                        MCLogUtil.e("test", url.toString());
                        URL url2 = url;
                        return null;
                    }
                    System.out.println(line);
                }
            } catch (MalformedURLException e3) {
                e = e3;
                e.printStackTrace();
                return null;
            } catch (IOException e4) {
                e2 = e4;
                e2.printStackTrace();
                return null;
            }
        } catch (MalformedURLException e5) {
            e = e5;
            e.printStackTrace();
            return null;
        } catch (IOException e6) {
            e2 = e6;
            e2.printStackTrace();
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r11v14 'mCursor'  android.database.Cursor: [D('mCursor' android.database.Cursor), D('context' android.content.Context)] */
    /* JADX INFO: Multiple debug info for r2v5 java.lang.String: [D('proxyStr' java.lang.String), D('uri' android.net.Uri)] */
    /* JADX WARN: Type inference failed for: r1v25, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.net.HttpURLConnection getNewHttpURLConnection(java.net.URL r10, android.content.Context r11) {
        /*
            r1 = 0
            r8 = 0
            java.net.URLConnection r7 = r10.openConnection()     // Catch:{ Exception -> 0x0063, all -> 0x0088 }
            java.net.HttpURLConnection r7 = (java.net.HttpURLConnection) r7     // Catch:{ Exception -> 0x0063, all -> 0x0088 }
            java.lang.String r1 = "wifi"
            java.lang.Object r1 = r11.getSystemService(r1)     // Catch:{ Exception -> 0x00a1, all -> 0x0093 }
            android.net.wifi.WifiManager r1 = (android.net.wifi.WifiManager) r1     // Catch:{ Exception -> 0x00a1, all -> 0x0093 }
            boolean r1 = r1.isWifiEnabled()     // Catch:{ Exception -> 0x00a1, all -> 0x0093 }
            if (r1 != 0) goto L_0x00ac
            java.lang.String r1 = "content://telephony/carriers/preferapn"
            android.net.Uri r2 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x00a1, all -> 0x0093 }
            android.content.ContentResolver r1 = r11.getContentResolver()     // Catch:{ Exception -> 0x00a1, all -> 0x0093 }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            android.database.Cursor r11 = r1.query(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x00a1, all -> 0x0093 }
            if (r11 == 0) goto L_0x00aa
            boolean r1 = r11.moveToFirst()     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            if (r1 == 0) goto L_0x00aa
            java.lang.String r1 = "proxy"
            int r1 = r11.getColumnIndex(r1)     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            java.lang.String r2 = r11.getString(r1)     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            if (r2 == 0) goto L_0x00aa
            java.lang.String r1 = r2.trim()     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            int r1 = r1.length()     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            if (r1 <= 0) goto L_0x00aa
            java.net.Proxy r1 = new java.net.Proxy     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            java.net.Proxy$Type r3 = java.net.Proxy.Type.HTTP     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            r5 = 80
            r4.<init>(r2, r5)     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            r1.<init>(r3, r4)     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            java.net.URLConnection r1 = r10.openConnection(r1)     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            r0 = r1
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00a6, all -> 0x0098 }
            r10 = r0
        L_0x005c:
            if (r11 == 0) goto L_0x0061
            r11.close()
        L_0x0061:
            r1 = r10
        L_0x0062:
            return r1
        L_0x0063:
            r11 = move-exception
            r2 = r8
            r9 = r11
            r11 = r1
            r1 = r9
        L_0x0068:
            r1.printStackTrace()     // Catch:{ all -> 0x009c }
            java.net.URLConnection r10 = r10.openConnection()     // Catch:{ IOException -> 0x007a }
            java.net.HttpURLConnection r10 = (java.net.HttpURLConnection) r10     // Catch:{ IOException -> 0x007a }
            if (r2 == 0) goto L_0x0076
            r2.close()
        L_0x0076:
            r1 = r10
            r10 = r11
            r11 = r2
            goto L_0x0062
        L_0x007a:
            r10 = move-exception
            r10.printStackTrace()     // Catch:{ all -> 0x009c }
            if (r2 == 0) goto L_0x0083
            r2.close()
        L_0x0083:
            r10 = 0
            r1 = r10
            r10 = r11
            r11 = r2
            goto L_0x0062
        L_0x0088:
            r10 = move-exception
            r11 = r8
            r9 = r10
            r10 = r1
            r1 = r9
        L_0x008d:
            if (r11 == 0) goto L_0x0092
            r11.close()
        L_0x0092:
            throw r1
        L_0x0093:
            r10 = move-exception
            r1 = r10
            r11 = r8
            r10 = r7
            goto L_0x008d
        L_0x0098:
            r10 = move-exception
            r1 = r10
            r10 = r7
            goto L_0x008d
        L_0x009c:
            r10 = move-exception
            r1 = r10
            r10 = r11
            r11 = r2
            goto L_0x008d
        L_0x00a1:
            r11 = move-exception
            r1 = r11
            r2 = r8
            r11 = r7
            goto L_0x0068
        L_0x00a6:
            r1 = move-exception
            r2 = r11
            r11 = r7
            goto L_0x0068
        L_0x00aa:
            r10 = r7
            goto L_0x005c
        L_0x00ac:
            r11 = r8
            r10 = r7
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.forum.android.api.util.HttpClientUtil.getNewHttpURLConnection(java.net.URL, android.content.Context):java.net.HttpURLConnection");
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006b  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:35:0x0071=Splitter:B:35:0x0071, B:24:0x005a=Splitter:B:24:0x005a} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String read(org.apache.http.HttpResponse r13) throws java.lang.Exception {
        /*
            r12 = -1
            java.lang.String r8 = ""
            org.apache.http.HttpEntity r3 = r13.getEntity()
            r5 = 0
            r0 = 0
            java.io.InputStream r5 = r3.getContent()     // Catch:{ IllegalStateException -> 0x007e, IOException -> 0x006f }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ IllegalStateException -> 0x007e, IOException -> 0x006f }
            r1.<init>()     // Catch:{ IllegalStateException -> 0x007e, IOException -> 0x006f }
            java.lang.String r10 = "Content-Encoding"
            org.apache.http.Header r4 = r13.getFirstHeader(r10)     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            if (r4 == 0) goto L_0x0030
            java.lang.String r10 = r4.getValue()     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            java.lang.String r10 = r10.toLowerCase()     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            java.lang.String r11 = "gzip"
            int r10 = r10.indexOf(r11)     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            if (r10 <= r12) goto L_0x0030
            java.util.zip.GZIPInputStream r6 = new java.util.zip.GZIPInputStream     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            r6.<init>(r5)     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            r5 = r6
        L_0x0030:
            r7 = 0
            r10 = 512(0x200, float:7.175E-43)
            byte[] r9 = new byte[r10]     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
        L_0x0035:
            int r7 = r5.read(r9)     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            if (r7 != r12) goto L_0x0052
            java.lang.String r8 = new java.lang.String     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            byte[] r10 = r1.toByteArray()     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            r8.<init>(r10)     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            if (r1 == 0) goto L_0x004c
            r1.flush()
            r1.close()
        L_0x004c:
            if (r5 == 0) goto L_0x0051
            r5.close()
        L_0x0051:
            return r8
        L_0x0052:
            r10 = 0
            r1.write(r9, r10, r7)     // Catch:{ IllegalStateException -> 0x0057, IOException -> 0x007a, all -> 0x0077 }
            goto L_0x0035
        L_0x0057:
            r10 = move-exception
            r2 = r10
            r0 = r1
        L_0x005a:
            java.lang.Exception r10 = new java.lang.Exception     // Catch:{ all -> 0x0060 }
            r10.<init>(r2)     // Catch:{ all -> 0x0060 }
            throw r10     // Catch:{ all -> 0x0060 }
        L_0x0060:
            r10 = move-exception
        L_0x0061:
            if (r0 == 0) goto L_0x0069
            r0.flush()
            r0.close()
        L_0x0069:
            if (r5 == 0) goto L_0x006e
            r5.close()
        L_0x006e:
            throw r10
        L_0x006f:
            r10 = move-exception
            r2 = r10
        L_0x0071:
            java.lang.Exception r10 = new java.lang.Exception     // Catch:{ all -> 0x0060 }
            r10.<init>(r2)     // Catch:{ all -> 0x0060 }
            throw r10     // Catch:{ all -> 0x0060 }
        L_0x0077:
            r10 = move-exception
            r0 = r1
            goto L_0x0061
        L_0x007a:
            r10 = move-exception
            r2 = r10
            r0 = r1
            goto L_0x0071
        L_0x007e:
            r10 = move-exception
            r2 = r10
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.forum.android.api.util.HttpClientUtil.read(org.apache.http.HttpResponse):java.lang.String");
    }
}
