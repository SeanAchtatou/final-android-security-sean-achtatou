package com.mobcent.forum.android.constant;

public interface ModeratorConstant extends BaseRestfulApiConstant {
    public static final String BOADR_ID = "board_id";
    public static final String BOADR_NAME = "board_name";
    public static final String BOARD_IDS = "boardIds";
    public static final String ICON = "icon";
    public static final String IS_MODERATOR = "is_moderator";
    public static final String LIST = "list";
    public static final String MBS = "mbs";
    public static final int MODERATOR = 1;
    public static final String ModeratorToUser = "moderatorUserId";
    public static final String REPLY_POSTS_ID = "replyPostId";
    public static final String TOPIC_ID = "topicId";
    public static final String USER_ID = "user_id";
    public static final String USER_NICKNAME = "nickname";
}
