package com.mobcent.forum.android.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.forum.android.application.McForumApplication;

public abstract class BaseDBUtil {
    public static final String DATABASE_NAME = "mcForum2.db";
    public static final int DEFAULT_VERSION = 1;
    protected Context context;
    protected DBOpenHelper myDBOpenHelper;
    protected SQLiteDatabase readableDatabase;
    protected SQLiteDatabase writableDatabase;

    public BaseDBUtil(Context context2) {
        Context appContext;
        McForumApplication app = McForumApplication.getInstance();
        if (app == null) {
            appContext = context2.getApplicationContext();
        } else {
            appContext = app.getApplicationContext();
        }
        this.context = appContext;
        this.myDBOpenHelper = new DBOpenHelper(context2, DATABASE_NAME, 1);
    }

    public boolean isRowExisted(SQLiteDatabase database, String table, String column, long id) {
        Cursor c = null;
        try {
            c = database.query(table, null, String.valueOf(column) + "=" + id, null, null, null, null);
            if (c.getCount() > 0) {
                c.close();
                return true;
            }
            c.close();
            return false;
        } catch (Exception e) {
            c.close();
            return false;
        } catch (Throwable th) {
            c.close();
            throw th;
        }
    }

    public boolean isRowExisted(SQLiteDatabase database, String table, String column, String id) {
        Cursor c = null;
        try {
            c = database.query(table, null, String.valueOf(column) + "='" + id + "'", null, null, null, null);
            if (c.getCount() > 0) {
                c.close();
                return true;
            }
            c.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            c.close();
            return false;
        } catch (Throwable th) {
            c.close();
            throw th;
        }
    }

    public boolean removeAllEntries(String tableName) {
        try {
            openWriteableDB();
            this.writableDatabase.delete(tableName, null, null);
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean tabbleIsExist(SQLiteDatabase database, String tableName) {
        boolean result = false;
        if (tableName == null) {
            return false;
        }
        Cursor c = null;
        try {
            Cursor c2 = database.rawQuery("select count(*) as c from Sqlite_master  where type ='table' and name ='" + tableName.trim() + "' ", null);
            if (c2.moveToNext() && c2.getInt(0) > 0) {
                result = true;
            }
            if (c2 != null) {
                c2.close();
            }
            return result;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            return false;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            throw th;
        }
    }

    public synchronized void openWriteableDB() {
        this.writableDatabase = this.myDBOpenHelper.getWritableDatabase();
    }

    public synchronized void openReadableDB() {
        this.readableDatabase = this.myDBOpenHelper.getReadableDatabase();
    }

    public void closeWriteableDB() {
        if (this.writableDatabase != null) {
            this.writableDatabase.close();
        }
    }

    public void closeReadableDB() {
        if (this.readableDatabase != null) {
            this.readableDatabase.close();
        }
    }
}
