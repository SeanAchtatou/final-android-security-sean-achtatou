package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.api.util.HttpClientUtil;
import com.mobcent.forum.android.application.McForumApplication;
import com.mobcent.forum.android.constant.BaseRestfulApiConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.os.service.helper.HeartBeatOSServiceHelper;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.MCResource;
import com.mobcent.forum.android.util.PhoneUtil;
import java.util.HashMap;

public abstract class BaseRestfulApiRequester implements BaseRestfulApiConstant {
    /* JADX INFO: Multiple debug info for r7v1 android.content.Context: [D('appContext' android.content.Context), D('context' android.content.Context)] */
    /* JADX INFO: Multiple debug info for r0v1 java.lang.String: [D('app' com.mobcent.forum.android.application.McForumApplication), D('accessToken' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r2v1 int: [D('forumId' int), D('shareDB' com.mobcent.forum.android.db.SharedPreferencesDB)] */
    /* JADX INFO: Multiple debug info for r7v17 android.content.Context: [D('appContext' android.content.Context), D('context' android.content.Context)] */
    public static String doPostRequest(String urlString, HashMap<String, String> params, Context context) {
        Context appContext;
        String urlString2;
        McForumApplication app = McForumApplication.getInstance();
        if (app == null) {
            appContext = context.getApplicationContext();
        } else {
            appContext = app.getApplicationContext();
        }
        SharedPreferencesDB shareDB = SharedPreferencesDB.getInstance(appContext);
        String forumKey = shareDB.getForumKey();
        String accessToken = shareDB.getAccessToken();
        String accessSecret = shareDB.getAccessSecret();
        int forumId = shareDB.getForumId();
        if (forumKey != null) {
            params.put("forumKey", forumKey);
        }
        if (accessToken != null) {
            params.put("accessToken", accessToken);
        }
        if (accessSecret != null) {
            params.put("accessSecret", accessSecret);
        }
        if (forumId != -1) {
            params.put("forumId", new StringBuilder(String.valueOf(forumId)).toString());
        }
        params.put("imei", PhoneUtil.getIMEI(appContext));
        params.put("imsi", PhoneUtil.getIMSI(appContext));
        String packageName = appContext.getPackageName();
        String appName = appContext.getApplicationInfo().loadLabel(appContext.getPackageManager()).toString();
        params.put("packageName", packageName);
        params.put("appName", appName);
        params.put(BaseRestfulApiConstant.SDK_TYPE, "");
        params.put("sdkVersion", BaseRestfulApiConstant.SDK_VERSION_VALUE);
        params.put("platType", "1");
        if (!urlString.contains("http")) {
            urlString2 = String.valueOf(appContext.getResources().getString(MCResource.getInstance(appContext).getStringId("mc_forum_base_request_domain_url"))) + urlString;
        } else {
            urlString2 = urlString;
        }
        checkHeartBeat(urlString2, appContext);
        return HttpClientUtil.doPostRequest(urlString2, params, appContext);
    }

    public static String uploadFile(String urlString, String uploadFile, Context context, String action) {
        Context appContext;
        String power;
        McForumApplication app = McForumApplication.getInstance();
        if (app == null) {
            appContext = context.getApplicationContext();
        } else {
            appContext = app.getApplicationContext();
        }
        String url = String.valueOf(MCResource.getInstance(appContext).getString("mc_forum_request_img_domain_url")) + urlString;
        checkHeartBeat(url, appContext);
        SharedPreferencesDB sharedPreferencesDB = SharedPreferencesDB.getInstance(context);
        boolean powerBy = sharedPreferencesDB.getPayStatePowerBy();
        boolean waterImage = sharedPreferencesDB.getPayStateWaterMark();
        String power2 = "";
        String img = "";
        if (!action.equals(BaseRestfulApiConstant.FORUM_HEADER_LOCAL_ACTION)) {
            img = sharedPreferencesDB.getPayStateWaterMarkImage();
            if (powerBy) {
                power = "1";
            } else {
                power = "0";
            }
            if (waterImage) {
                power2 = String.valueOf(power) + "1";
            } else {
                power2 = String.valueOf(power) + "0";
            }
        }
        return HttpClientUtil.uploadFile(url, uploadFile, appContext, "action=" + action + "&powerBy=" + power2 + "&waterImagePath=" + img);
    }

    private static void checkHeartBeat(String urlString, Context context) {
        Context appContext;
        McForumApplication app = McForumApplication.getInstance();
        if (app == null) {
            appContext = context.getApplicationContext();
        } else {
            appContext = app.getApplicationContext();
        }
        if (urlString.indexOf(HeartBeatRestfulApiRequester.HEART_URL) <= -1) {
            MCLogUtil.i("HeartMsgOSService", "resert heart beat");
            HeartBeatOSServiceHelper.startHeartBeatService(appContext, 0);
        }
    }
}
