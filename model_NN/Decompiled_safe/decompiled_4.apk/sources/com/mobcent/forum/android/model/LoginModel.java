package com.mobcent.forum.android.model;

public class LoginModel extends BaseModel {
    private static final long serialVersionUID = -2627739804982191191L;
    private String icon;
    private long id;
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }
}
