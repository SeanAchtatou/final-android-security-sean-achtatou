package com.mobcent.forum.android.model;

public class PlatformModel {
    private long platformId;
    private String platformImage;

    public long getPlatformId() {
        return this.platformId;
    }

    public void setPlatformId(long platformId2) {
        this.platformId = platformId2;
    }

    public String getPlatformImage() {
        return this.platformImage;
    }

    public void setPlatformImage(String platformImage2) {
        this.platformImage = platformImage2;
    }
}
