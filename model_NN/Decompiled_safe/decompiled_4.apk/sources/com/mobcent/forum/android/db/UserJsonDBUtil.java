package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.forum.android.db.constant.BaseDBConstant;
import com.mobcent.forum.android.db.constant.UserJsonDBConstant;
import com.mobcent.forum.android.db.helper.UserJsonDBHelper;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.ArrayList;
import java.util.List;

public class UserJsonDBUtil extends BaseDBUtil implements BaseDBConstant, UserJsonDBConstant {
    private static UserJsonDBUtil userDBUtil;

    protected UserJsonDBUtil(Context ctx) {
        super(ctx);
    }

    public static UserJsonDBUtil getInstance(Context context) {
        if (userDBUtil == null) {
            userDBUtil = new UserJsonDBUtil(context);
        }
        return userDBUtil;
    }

    public boolean saveUserInfoModel(long userId, int currUser, String userInfo) {
        return save(UserJsonDBConstant.TABLE_JSON_USER, userId, currUser, userInfo);
    }

    public boolean updateUserInfo(long userId, String userInfo) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("userId", Long.valueOf(userId));
            values.put("userInfo", userInfo);
            if (isRowExisted(this.writableDatabase, UserJsonDBConstant.TABLE_JSON_USER, "userId", userId)) {
                this.writableDatabase.update(UserJsonDBConstant.TABLE_JSON_USER, values, "userId='" + userId + "'", null);
                closeWriteableDB();
                return true;
            }
            this.writableDatabase.insertOrThrow(UserJsonDBConstant.TABLE_JSON_USER, null, values);
            closeWriteableDB();
            return false;
        } catch (Exception e) {
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean updateCurrentInfo(long userId) {
        try {
            openWriteableDB();
            this.writableDatabase.execSQL(" update t_json_user set currUser = 1 where  userId = '" + userId + "'");
            this.writableDatabase.execSQL(" update t_json_user set currUser = 0 where  userId != '" + userId + "'");
            closeWriteableDB();
            return false;
        } catch (Exception e) {
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public UserInfoModel getCurrentUserInfo() {
        return get(UserJsonDBConstant.TABLE_JSON_USER);
    }

    public List<UserInfoModel> getAllUsers() {
        Cursor c = null;
        try {
            openWriteableDB();
            c = this.writableDatabase.query(UserJsonDBConstant.TABLE_JSON_USER, null, null, null, null, null, null);
            if (c.getCount() == 0) {
                if (c != null) {
                    c.close();
                }
                closeWriteableDB();
                return null;
            }
            List<UserInfoModel> userInfoModels = new ArrayList<>();
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                userInfoModels.add(UserJsonDBHelper.formUserInfo(c.getString(2)));
            }
            if (c != null) {
                c.close();
            }
            closeWriteableDB();
            return userInfoModels;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            closeWriteableDB();
            return null;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            closeWriteableDB();
            throw th;
        }
    }

    public boolean delteData() {
        return removeAllEntries(UserJsonDBConstant.TABLE_JSON_USER);
    }

    public boolean savePersonalInfo(long userId, int currUser, String userInfo) {
        return save(UserJsonDBConstant.TABLE_JSON_PERSONAL, userId, currUser, userInfo);
    }

    public UserInfoModel getPersonalInfo() {
        return get(UserJsonDBConstant.TABLE_JSON_PERSONAL);
    }

    private boolean save(String table, long userId, int currUser, String userInfo) {
        if (userId == 0) {
            return true;
        }
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("userId", Long.valueOf(userId));
            values.put("currUser", Integer.valueOf(currUser));
            values.put("userInfo", userInfo);
            if (!isRowExisted(this.writableDatabase, table, "userId", userId)) {
                this.writableDatabase.insertOrThrow(table, null, values);
            } else {
                this.writableDatabase.update(table, values, "userId='" + userId + "'", null);
            }
            this.writableDatabase.execSQL(" update " + table + " set " + "currUser" + " = " + 0 + " where  " + "userId" + " != '" + userId + "'");
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    private UserInfoModel get(String table) {
        Cursor c = null;
        try {
            openWriteableDB();
            Cursor c2 = this.writableDatabase.query(table, null, "currUser=1", null, null, null, null);
            if (c2.moveToFirst()) {
                UserInfoModel fromPersonalInfo = UserJsonDBHelper.fromPersonalInfo(c2.getString(2));
                if (c2 != null) {
                    c2.close();
                }
                closeWriteableDB();
                return fromPersonalInfo;
            }
            if (c2 != null) {
                c2.close();
            }
            closeWriteableDB();
            return null;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            closeWriteableDB();
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            closeWriteableDB();
            throw th;
        }
    }
}
