package com.mobcent.forum.android.constant;

public interface ReportConstant extends BaseRestfulApiConstant {
    public static final String BOARD_NAME = "board_name";
    public static final String CONTENT = "content_abbr";
    public static final String COUNT = "count";
    public static final String GENDER = "gender";
    public static final String INFORMANTS = "informants";
    public static final String LIST = "list";
    public static final String NICK_NAME = "nickname";
    public static final String POST_ID = "post_id";
    public static final String PROCESSED = "is_processed";
    public static final String REASON = "reason";
    public static final String REPORT_ID = "report_id";
    public static final String REPORT_OID = "oid";
    public static final String REPORT_REASON = "reason";
    public static final String REPORT_TIME = "report_time";
    public static final String REPORT_TOPIC_ID = "reportId";
    public static final String REPORT_Type = "type";
    public static final String STATUS = "status";
    public static final String TITLE = "title";
    public static final String TOPIC_ID = "topic_id";
    public static final String TOTAL_NUM = "total_num";
    public static final String TYPE = "type";
    public static final String USER_ID = "user_id";
}
