package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.PopNoticeModel;

public interface PopNoticeService {
    PopNoticeModel getPopNoticeModel();
}
