package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.forum.android.constant.UserManageConstant;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.util.StringUtil;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class UserManageApiRequester extends BaseRestfulApiRequester implements UserManageConstant {
    public static String bannedShielded(Context context, int type, long currentUserId, long bannedShieldedUserId, int boardId, Date bannedDate, String reason) {
        HashMap<String, String> params = new HashMap<>();
        params.put("type", new StringBuilder(String.valueOf(type)).toString());
        params.put("userId", new StringBuilder(String.valueOf(currentUserId)).toString());
        params.put("shieldedUserId", new StringBuilder(String.valueOf(bannedShieldedUserId)).toString());
        params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        if (bannedDate != null) {
            params.put(UserManageConstant.PARAM_SHIELDED_DATE, new SimpleDateFormat("yyyy-MM-dd").format(bannedDate));
        }
        if (!reason.equals("")) {
            params.put(UserManageConstant.PARAM_SHIELDED_REASON, reason);
        }
        return doPostRequest("user/addShieldedUser.do", params, context);
    }

    /* JADX INFO: Multiple debug info for r0v1 java.util.HashMap: [D('url' java.lang.String), D('params' java.util.HashMap<java.lang.String, java.lang.String>)] */
    public static String bannedShielded(Context context, int type, long currentUserId, long bannedShieldedUserId, List<BoardModel> selectBoardList, Date bannedDate, String reason) {
        HashMap<String, String> params = new HashMap<>();
        params.put("type", new StringBuilder(String.valueOf(type)).toString());
        params.put("userId", new StringBuilder(String.valueOf(currentUserId)).toString());
        params.put("shieldedUserId", new StringBuilder(String.valueOf(bannedShieldedUserId)).toString());
        String boardId = "";
        boolean isTotal = false;
        if (selectBoardList.size() > 0) {
            StringBuffer sb = new StringBuffer();
            int i = 0;
            while (true) {
                if (i >= selectBoardList.size()) {
                    break;
                } else if (selectBoardList.get(i).getBoardId() == 0) {
                    isTotal = true;
                    break;
                } else {
                    if (selectBoardList.get(i).getBoardId() > 0) {
                        sb.append(String.valueOf(selectBoardList.get(i).getBoardId()) + AdApiConstant.RES_SPLIT_COMMA);
                    }
                    i++;
                }
            }
            if (!isTotal) {
                boardId = sb.toString().substring(0, sb.toString().length() - 1);
            } else {
                boardId = "0";
            }
        }
        params.put("boardId", boardId);
        if (bannedDate != null) {
            params.put(UserManageConstant.PARAM_SHIELDED_DATE, new SimpleDateFormat("yyyy-MM-dd").format(bannedDate));
        }
        if (!StringUtil.isEmpty(reason)) {
            params.put(UserManageConstant.PARAM_SHIELDED_REASON, reason);
        }
        return doPostRequest("user/setShieldedUser.do", params, context);
    }

    public static String cancelBannedShielded(Context context, int type, long bannedShieldedUserId, int boardId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("type", new StringBuilder(String.valueOf(type)).toString());
        params.put("shieldedUserId", new StringBuilder(String.valueOf(bannedShieldedUserId)).toString());
        params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        return doPostRequest("user/cancelShieldedUser.do", params, context);
    }

    public static String getBannedShieldedUser(Context context, int type, int boardId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("type", new StringBuilder(String.valueOf(type)).toString());
        params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("user/getShieldedUserList.do", params, context);
    }

    public static String searchBannedShieldedUser(Context context, int type, int boardId, int page, int pageSize) {
        return null;
    }
}
