package com.mobcent.forum.android.model;

public class PostsModel extends BaseModel {
    private static final long serialVersionUID = 7881817952633540401L;
    private int postType;
    private ReplyModel reply;
    private TopicModel topic;

    public TopicModel getTopic() {
        return this.topic;
    }

    public void setTopic(TopicModel topic2) {
        this.topic = topic2;
    }

    public ReplyModel getReply() {
        return this.reply;
    }

    public void setReply(ReplyModel reply2) {
        this.reply = reply2;
    }

    public int getPostType() {
        return this.postType;
    }

    public void setPostType(int postType2) {
        this.postType = postType2;
    }
}
