package com.mobcent.forum.android.model;

public class UserBoardInfoModel {
    private long boardId;
    private String boardName;
    private int isBanned;
    private int isModerator;
    private int isShielded;

    public long getBoardId() {
        return this.boardId;
    }

    public void setBoardId(long boardId2) {
        this.boardId = boardId2;
    }

    public String getBoardName() {
        return this.boardName;
    }

    public void setBoardName(String boardName2) {
        this.boardName = boardName2;
    }

    public int getIsModerator() {
        return this.isModerator;
    }

    public void setIsModerator(int isModerator2) {
        this.isModerator = isModerator2;
    }

    public int getIsBanned() {
        return this.isBanned;
    }

    public void setIsBanned(int isBanned2) {
        this.isBanned = isBanned2;
    }

    public int getIsShielded() {
        return this.isShielded;
    }

    public void setIsShielded(int isShielded2) {
        this.isShielded = isShielded2;
    }
}
