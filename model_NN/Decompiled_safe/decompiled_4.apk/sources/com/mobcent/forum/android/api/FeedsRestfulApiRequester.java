package com.mobcent.forum.android.api;

import android.content.Context;
import java.util.HashMap;

public class FeedsRestfulApiRequester extends BaseRestfulApiRequester {
    public static String getFeedsList(Context context, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("feed/getFeeds.do", params, context);
    }

    public static String getNewFeedsList(Context context, long endTime) {
        HashMap<String, String> params = new HashMap<>();
        if (endTime > 0) {
            params.put("endTime", new StringBuilder(String.valueOf(endTime)).toString());
        }
        return doPostRequest("feed/getFeedsV2.do", params, context);
    }

    public static String getUserFeed(Context context, long userId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("/feed/userFeeds.do", params, context);
    }
}
