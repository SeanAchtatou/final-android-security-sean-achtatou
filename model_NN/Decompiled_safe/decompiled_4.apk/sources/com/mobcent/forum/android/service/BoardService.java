package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.model.ForumModel;
import java.util.List;

public interface BoardService {
    List<BoardModel> getBoadListByLocal();

    List<BoardModel> getBoadListByNet();

    ForumModel getForumInfo();

    ForumModel getForumInfoByNet();

    boolean updateForumInfo();
}
