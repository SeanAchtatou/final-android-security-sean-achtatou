package com.mobcent.forum.android.constant;

public interface MentionFriendConstant {
    public static final String LIST = "list";
    public static final String NICK_NAME = "name";
    public static final String PLAT_FORUM_ID = "platformId";
    public static final int ROLE_ADMIN = 8;
    public static final int ROLE_BOARD_OWNER = 4;
    public static final int ROLE_GENERAL_USER = 2;
    public static final int ROLE_GUEST = 1;
    public static final String ROLE_NUM = "role_num";
    public static final int ROLE_SUPER_ADMIN = 16;
    public static final int ROLE_UNKNOW = -1;
    public static final long UNKNOW_USER_ID = -1;
    public static final String USER_ID = "uid";
    public static final String USER_INFO_ID = "userId";
}
