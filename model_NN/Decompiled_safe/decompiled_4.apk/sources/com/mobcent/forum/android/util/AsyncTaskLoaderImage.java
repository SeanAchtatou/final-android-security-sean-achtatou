package com.mobcent.forum.android.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import com.mobcent.base.forum.android.util.AsyncImageLoader;
import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AsyncTaskLoaderImage {
    private static Map<String, AsyncTaskLoaderImage> _instanceMap = new HashMap();
    private static AsyncTaskLoaderImage asyncTaskLoadImage = null;
    public static ExecutorService originalThreadPool = Executors.newFixedThreadPool(8);
    private final String TAG = "AsyncTaskLoaderImage";
    public LruCache<String, Bitmap> _cache;
    /* access modifiers changed from: private */
    public HashMap<String, List<BitmapImageCallback>> _callbacks;
    /* access modifiers changed from: private */
    public final Object _lock = new Object();
    private Context context;
    private String imagePath = null;
    private HashMap<String, LoaderImageThread> loaderImageThreads;

    public interface BitmapImageCallback {
        void onImageLoaded(Bitmap bitmap, String str);
    }

    public static synchronized AsyncTaskLoaderImage getInstance(Context context2, String tag) {
        AsyncTaskLoaderImage asyncTaskLoaderImage;
        synchronized (AsyncTaskLoaderImage.class) {
            if (_instanceMap == null) {
                _instanceMap = new HashMap();
            }
            if (_instanceMap.get(tag) == null) {
                _instanceMap.put(tag, new AsyncTaskLoaderImage(context2));
            }
            asyncTaskLoaderImage = _instanceMap.get(tag);
        }
        return asyncTaskLoaderImage;
    }

    public static void clearInstanceMap() {
        if (_instanceMap == null) {
            _instanceMap = new HashMap();
        } else {
            _instanceMap.clear();
        }
    }

    public static synchronized AsyncTaskLoaderImage getInstance(Context context2) {
        AsyncTaskLoaderImage asyncTaskLoaderImage;
        synchronized (AsyncTaskLoaderImage.class) {
            if (asyncTaskLoadImage == null) {
                asyncTaskLoadImage = new AsyncTaskLoaderImage(context2);
            }
            asyncTaskLoaderImage = asyncTaskLoadImage;
        }
        return asyncTaskLoaderImage;
    }

    private AsyncTaskLoaderImage(Context context2) {
        this._cache = new LruCache<String, Bitmap>(PhoneUtil.getCacheSize(context2)) {
            /* access modifiers changed from: protected */
            public int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getRowBytes() * bitmap.getHeight();
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: android.support.v4.util.LruCache.entryRemoved(boolean, java.lang.Object, java.lang.Object, java.lang.Object):void
             arg types: [boolean, java.lang.String, android.graphics.Bitmap, android.graphics.Bitmap]
             candidates:
              com.mobcent.forum.android.util.AsyncTaskLoaderImage.1.entryRemoved(boolean, java.lang.String, android.graphics.Bitmap, android.graphics.Bitmap):void
              android.support.v4.util.LruCache.entryRemoved(boolean, java.lang.Object, java.lang.Object, java.lang.Object):void */
            /* access modifiers changed from: protected */
            public void entryRemoved(boolean evicted, String key, Bitmap oldValue, Bitmap newValue) {
                super.entryRemoved(evicted, (Object) key, (Object) oldValue, (Object) newValue);
                MCLogUtil.e("AsyncTaskLoaderImage", "oldValue" + oldValue);
            }
        };
        this.context = context2;
        this._callbacks = new HashMap<>();
        this.loaderImageThreads = new HashMap<>();
        this.imagePath = MCLibIOUtil.getImageCachePath(context2);
    }

    public static String getHash(String url) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(url.getBytes());
            return new BigInteger(digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            return url;
        }
    }

    public void addBitmapToMemoryCache(String hash, Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            this._cache.put(hash, bitmap);
        }
    }

    private Bitmap getbitmapFromCache(String url, String hash) {
        Bitmap b;
        synchronized (this._lock) {
            b = this._cache.get(hash);
            if (b != null && b.isRecycled()) {
                this._cache.remove(hash);
                b = null;
            }
        }
        return b;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap loadSync(java.lang.String r8, java.lang.String r9, com.mobcent.forum.android.util.AsyncTaskLoaderImage.LoaderImageThread r10) {
        /*
            r7 = this;
            r0 = 0
            java.lang.String r4 = "AsyncTaskLoaderImage"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008f }
            java.lang.String r6 = "url="
            r5.<init>(r6)     // Catch:{ Exception -> 0x008f }
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ Exception -> 0x008f }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x008f }
            com.mobcent.forum.android.util.MCLogUtil.i(r4, r5)     // Catch:{ Exception -> 0x008f }
            android.graphics.Bitmap r0 = r7.getbitmapFromCache(r8, r9)     // Catch:{ Exception -> 0x008f }
            java.lang.String r4 = r7.imagePath     // Catch:{ Exception -> 0x008f }
            if (r4 != 0) goto L_0x0055
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x008f }
            android.content.Context r4 = r7.context     // Catch:{ Exception -> 0x008f }
            java.io.File r4 = r4.getCacheDir()     // Catch:{ Exception -> 0x008f }
            r2.<init>(r4, r9)     // Catch:{ Exception -> 0x008f }
        L_0x0028:
            if (r0 != 0) goto L_0x008a
            boolean r4 = r2.exists()     // Catch:{ Exception -> 0x008f }
            if (r4 == 0) goto L_0x003d
            java.lang.String r4 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x008f }
            android.graphics.Bitmap r0 = r7.getBitmap(r4)     // Catch:{ Exception -> 0x008f }
            if (r0 != 0) goto L_0x003d
            r2.delete()     // Catch:{ Exception -> 0x008f }
        L_0x003d:
            boolean r4 = r2.exists()     // Catch:{ Exception -> 0x008f }
            if (r4 != 0) goto L_0x004d
            com.mobcent.forum.android.service.impl.FileTransferServiceImpl r3 = new com.mobcent.forum.android.service.impl.FileTransferServiceImpl     // Catch:{ Exception -> 0x008f }
            android.content.Context r4 = r7.context     // Catch:{ Exception -> 0x008f }
            r3.<init>(r4)     // Catch:{ Exception -> 0x008f }
            r3.downloadFile(r8, r2)     // Catch:{ Exception -> 0x008f }
        L_0x004d:
            boolean r4 = r10.isInterrupted()     // Catch:{ Exception -> 0x008f }
            if (r4 == 0) goto L_0x006e
            r4 = 0
        L_0x0054:
            return r4
        L_0x0055:
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x008f }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008f }
            java.lang.String r5 = r7.imagePath     // Catch:{ Exception -> 0x008f }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x008f }
            r4.<init>(r5)     // Catch:{ Exception -> 0x008f }
            java.lang.StringBuilder r4 = r4.append(r9)     // Catch:{ Exception -> 0x008f }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x008f }
            r2.<init>(r4)     // Catch:{ Exception -> 0x008f }
            goto L_0x0028
        L_0x006e:
            boolean r4 = r2.exists()     // Catch:{ Exception -> 0x008f }
            if (r4 == 0) goto L_0x0083
            if (r0 != 0) goto L_0x0083
            java.lang.String r4 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x008f }
            android.graphics.Bitmap r0 = r7.getBitmap(r4)     // Catch:{ Exception -> 0x008f }
            if (r0 != 0) goto L_0x0083
            r2.delete()     // Catch:{ Exception -> 0x008f }
        L_0x0083:
            java.lang.Object r4 = r7._lock     // Catch:{ Exception -> 0x008f }
            monitor-enter(r4)     // Catch:{ Exception -> 0x008f }
            r7.addBitmapToMemoryCache(r9, r0)     // Catch:{ all -> 0x008c }
            monitor-exit(r4)     // Catch:{ all -> 0x008c }
        L_0x008a:
            r4 = r0
            goto L_0x0054
        L_0x008c:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x008c }
            throw r5     // Catch:{ Exception -> 0x008f }
        L_0x008f:
            r4 = move-exception
            r1 = r4
            r1.printStackTrace()
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.forum.android.util.AsyncTaskLoaderImage.loadSync(java.lang.String, java.lang.String, com.mobcent.forum.android.util.AsyncTaskLoaderImage$LoaderImageThread):android.graphics.Bitmap");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadAsync(java.lang.String r7, com.mobcent.forum.android.util.AsyncTaskLoaderImage.BitmapImageCallback r8) {
        /*
            r6 = this;
            if (r7 == 0) goto L_0x000a
            java.lang.String r4 = ""
            boolean r4 = r7.equals(r4)
            if (r4 == 0) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            java.lang.String r2 = getHash(r7)
            android.graphics.Bitmap r0 = r6.getbitmapFromCache(r7, r2)
            if (r0 == 0) goto L_0x001f
            boolean r4 = r0.isRecycled()
            if (r4 != 0) goto L_0x001f
            r8.onImageLoaded(r0, r7)
            goto L_0x000a
        L_0x001f:
            java.lang.Object r4 = r6._lock
            monitor-enter(r4)
            java.util.HashMap<java.lang.String, java.util.List<com.mobcent.forum.android.util.AsyncTaskLoaderImage$BitmapImageCallback>> r5 = r6._callbacks     // Catch:{ all -> 0x0033 }
            java.lang.Object r1 = r5.get(r2)     // Catch:{ all -> 0x0033 }
            java.util.List r1 = (java.util.List) r1     // Catch:{ all -> 0x0033 }
            if (r1 == 0) goto L_0x0036
            if (r8 == 0) goto L_0x0031
            r1.add(r8)     // Catch:{ all -> 0x0033 }
        L_0x0031:
            monitor-exit(r4)     // Catch:{ all -> 0x0033 }
            goto L_0x000a
        L_0x0033:
            r5 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0033 }
            throw r5
        L_0x0036:
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0033 }
            r1.<init>()     // Catch:{ all -> 0x0033 }
            if (r8 == 0) goto L_0x0040
            r1.add(r8)     // Catch:{ all -> 0x0033 }
        L_0x0040:
            java.util.HashMap<java.lang.String, java.util.List<com.mobcent.forum.android.util.AsyncTaskLoaderImage$BitmapImageCallback>> r5 = r6._callbacks     // Catch:{ all -> 0x0033 }
            r5.put(r2, r1)     // Catch:{ all -> 0x0033 }
            monitor-exit(r4)     // Catch:{ all -> 0x0033 }
            com.mobcent.forum.android.util.AsyncTaskLoaderImage$LoaderImageThread r3 = new com.mobcent.forum.android.util.AsyncTaskLoaderImage$LoaderImageThread
            r3.<init>(r7, r2)
            java.util.HashMap<java.lang.String, com.mobcent.forum.android.util.AsyncTaskLoaderImage$LoaderImageThread> r4 = r6.loaderImageThreads
            r4.put(r2, r3)
            java.util.concurrent.ExecutorService r4 = com.mobcent.forum.android.util.AsyncTaskLoaderImage.originalThreadPool
            r4.execute(r3)
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.forum.android.util.AsyncTaskLoaderImage.loadAsync(java.lang.String, com.mobcent.forum.android.util.AsyncTaskLoaderImage$BitmapImageCallback):void");
    }

    public void loadAsyncByLocal(String url, int width, BitmapImageCallback callback) {
        if (url != null && !url.equals("")) {
            String hash = getHash(url);
            Bitmap b = getbitmapFromCache(url, hash);
            if (b == null || b.isRecycled()) {
                synchronized (this._lock) {
                    List<BitmapImageCallback> callbacks = this._callbacks.get(hash);
                    if (callbacks == null) {
                        callbacks = new ArrayList<>();
                        this._callbacks.put(hash, callbacks);
                    }
                    callbacks.add(callback);
                }
                LoaderLocalImageThread loaderImageThread = new LoaderLocalImageThread(url, hash, width);
                this.loaderImageThreads.put(hash, loaderImageThread);
                originalThreadPool.execute(loaderImageThread);
                return;
            }
            callback.onImageLoaded(b, url);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobcent.forum.android.util.ImageUtil.getBitmapFromMedia(android.content.Context, java.lang.String, float, int):android.graphics.Bitmap
     arg types: [android.content.Context, java.lang.String, int, ?]
     candidates:
      com.mobcent.forum.android.util.ImageUtil.getBitmapFromMedia(android.content.Context, java.lang.String, int, int):android.graphics.Bitmap
      com.mobcent.forum.android.util.ImageUtil.getBitmapFromMedia(android.content.Context, java.lang.String, float, int):android.graphics.Bitmap */
    private Bitmap getBitmap(String fileName) {
        return ImageUtil.getBitmapFromMedia(this.context, fileName, 0.8f, (int) AsyncImageLoader.defaultMaxWidth);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobcent.forum.android.util.ImageUtil.getBitmapFromMedia(android.content.Context, java.lang.String, float, int):android.graphics.Bitmap
     arg types: [android.content.Context, java.lang.String, int, int]
     candidates:
      com.mobcent.forum.android.util.ImageUtil.getBitmapFromMedia(android.content.Context, java.lang.String, int, int):android.graphics.Bitmap
      com.mobcent.forum.android.util.ImageUtil.getBitmapFromMedia(android.content.Context, java.lang.String, float, int):android.graphics.Bitmap */
    /* access modifiers changed from: private */
    public Bitmap getBitmapByWidth(String path, int width) {
        return ImageUtil.getBitmapFromMedia(this.context, path, 0.8f, width);
    }

    private class LoaderImageThread extends Thread {
        private String hash;
        private String url;

        public LoaderImageThread(String url2, String hash2) {
            this.url = url2;
            this.hash = hash2;
        }

        public void run() {
            List<BitmapImageCallback> callbacks;
            try {
                Bitmap d = AsyncTaskLoaderImage.this.loadSync(this.url, this.hash, this);
                synchronized (AsyncTaskLoaderImage.this._lock) {
                    callbacks = (List) AsyncTaskLoaderImage.this._callbacks.remove(this.hash);
                }
                if (callbacks == null) {
                    return;
                }
                if (!callbacks.isEmpty()) {
                    for (BitmapImageCallback iter : callbacks) {
                        if (iter != null) {
                            iter.onImageLoaded(d, this.url);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class LoaderLocalImageThread extends LoaderImageThread {
        private String hash;
        private String url;
        private int width;

        public LoaderLocalImageThread(String url2, String hash2, int width2) {
            super(url2, hash2);
            this.url = url2;
            this.hash = hash2;
            this.width = width2;
        }

        public void run() {
            List<BitmapImageCallback> callbacks;
            try {
                Bitmap d = AsyncTaskLoaderImage.this.getBitmapByWidth(this.url, this.width);
                synchronized (AsyncTaskLoaderImage.this._lock) {
                    callbacks = (List) AsyncTaskLoaderImage.this._callbacks.remove(this.hash);
                }
                if (callbacks == null) {
                    return;
                }
                if (!callbacks.isEmpty()) {
                    for (BitmapImageCallback iter : callbacks) {
                        if (iter != null) {
                            iter.onImageLoaded(d, this.url);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getImagePath(String url) {
        String hash = getHash(url);
        if (this.imagePath == null) {
            if (new File(this.context.getCacheDir() + hash).exists()) {
                return this.context.getCacheDir() + hash;
            }
            return null;
        } else if (new File(String.valueOf(this.imagePath) + hash).exists()) {
            return String.valueOf(this.imagePath) + hash;
        } else {
            return null;
        }
    }

    public void interruptLoadImageThread(String urlStr) {
        String hash = getHash(urlStr);
        this.loaderImageThreads.remove(hash);
        this._callbacks.remove(hash);
    }

    public void recycleBitmap(String urlStr) {
        String hash = getHash(urlStr);
        Bitmap bitmap = this._cache.get(hash);
        if (bitmap != null) {
            if (!bitmap.isRecycled()) {
                bitmap.recycle();
                MCLogUtil.i("AsyncTaskLoaderImage", "recycleBitmap imageUrl = " + urlStr);
            }
            this._cache.remove(hash);
        }
        this.loaderImageThreads.remove(hash);
        this._callbacks.remove(hash);
    }

    public void recycleBitmaps(Collection<String> urlStrs) {
        if (urlStrs != null && !urlStrs.isEmpty()) {
            for (String imageUrl : urlStrs) {
                recycleBitmap(imageUrl);
            }
        }
    }

    public static String formatUrl(String url, String resolution) {
        if (url == null) {
            return "";
        }
        if (resolution == null) {
            return url;
        }
        return url.contains("xgsize") ? url.replace("xgsize", resolution) : url;
    }

    public void addCache(String imgUrl, Bitmap bitmap) {
        if (this._cache != null) {
            this._cache.put(getHash(imgUrl), bitmap);
        }
    }
}
