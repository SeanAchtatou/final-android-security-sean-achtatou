package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.forum.android.db.constant.UserFriendsDBConstant;
import com.mobcent.forum.android.util.DateUtil;

public class UserFriendsDBUtil extends BaseDBUtil implements UserFriendsDBConstant {
    private static final int USER_FRIEND_ID = 1;
    private static UserFriendsDBUtil userFriendsDBUtil;

    protected UserFriendsDBUtil(Context ctx) {
        super(ctx);
    }

    public static UserFriendsDBUtil getInstance(Context context) {
        if (userFriendsDBUtil == null) {
            userFriendsDBUtil = new UserFriendsDBUtil(context);
        }
        return userFriendsDBUtil;
    }

    public String getUserFriendsJsonString() {
        String jsonStr = null;
        Cursor cursor = null;
        try {
            openReadableDB();
            cursor = this.readableDatabase.rawQuery(UserFriendsDBConstant.SQL_SELECT_USER_FRIEND, null);
            while (cursor.moveToNext()) {
                jsonStr = cursor.getString(1);
            }
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            jsonStr = null;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
            throw th;
        }
        return jsonStr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean updateUserFriendsJsonString(String jsonStr) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("id", (Integer) 1);
            values.put("jsonStr", jsonStr);
            values.put("update_time", Long.valueOf(DateUtil.getCurrentTime()));
            if (!isRowExisted(this.writableDatabase, UserFriendsDBConstant.TABLE_USER_FRIEND, "id", 1)) {
                this.writableDatabase.insertOrThrow(UserFriendsDBConstant.TABLE_USER_FRIEND, null, values);
            } else {
                this.writableDatabase.update(UserFriendsDBConstant.TABLE_USER_FRIEND, values, "id=1", null);
            }
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean delteUserFriendsList() {
        return removeAllEntries(UserFriendsDBConstant.TABLE_USER_FRIEND);
    }
}
