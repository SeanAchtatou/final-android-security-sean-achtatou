package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.constant.MoreApplicationConstant;
import java.util.HashMap;

public class MoreApplicationRestfulApiRequester extends BaseRestfulApiRequester implements MoreApplicationConstant {
    public static String getMoreApplications(Context context, long appSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("platType", "1");
        params.put("appSize", new StringBuilder(String.valueOf(appSize)).toString());
        return doPostRequest("app/getMoreForum.do", params, context);
    }
}
