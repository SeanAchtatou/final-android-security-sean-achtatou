package com.mobcent.forum.android.model;

import java.util.List;

public class TopicModel extends FlowTag {
    private static final long serialVersionUID = -2587580302418486037L;
    private long boardId;
    private String boardName;
    private String content;
    private String contentAll;
    private long createDate;
    private int distance = -1;
    private int essence;
    private boolean follow;
    private boolean hasImg = false;
    private int hasNext;
    private int hasVoice;
    private int hitCount;
    private int hot;
    private String icon;
    private boolean isAd;
    private boolean isBannedUser;
    private int isFavor;
    private boolean isShieldedUser;
    private long lastReplyDate;
    private String lastUpdate;
    private int level;
    private String location;
    private String pic;
    private String picPath;
    private String poll;
    private int replieCount;
    private RepostTopicModel repostTopicModel;
    private int roleNum;
    private int status;
    private String thumbnail;
    private String title;
    private int top;
    private List<TopicContentModel> topicContentList;
    private long topicId;
    private int type;
    private int uploadType;
    private long userId;
    private String userNickName;
    private int visible;

    public String getContentAll() {
        return this.contentAll;
    }

    public void setContentAll(String contentAll2) {
        this.contentAll = contentAll2;
    }

    public boolean isAd() {
        return this.isAd;
    }

    public void setAd(boolean isAd2) {
        this.isAd = isAd2;
    }

    public int getHasVoice() {
        return this.hasVoice;
    }

    public void setHasVoice(int hasVoice2) {
        this.hasVoice = hasVoice2;
    }

    public String getLastUpdate() {
        return this.lastUpdate;
    }

    public void setLastUpdate(String lastUpdate2) {
        this.lastUpdate = lastUpdate2;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public RepostTopicModel getRepostTopicModel() {
        return this.repostTopicModel;
    }

    public void setRepostTopicModel(RepostTopicModel repostTopicModel2) {
        this.repostTopicModel = repostTopicModel2;
    }

    public long getBoardId() {
        return this.boardId;
    }

    public String getBoardName() {
        return this.boardName;
    }

    public String getContent() {
        return this.content;
    }

    public long getCreateDate() {
        return this.createDate;
    }

    public int getDistance() {
        return this.distance;
    }

    public int getEssence() {
        return this.essence;
    }

    public int getHasNext() {
        return this.hasNext;
    }

    public int getHitCount() {
        return this.hitCount;
    }

    public int getHot() {
        return this.hot;
    }

    public String getIcon() {
        return this.icon;
    }

    public int getIsFavor() {
        return this.isFavor;
    }

    public long getLastReplyDate() {
        return this.lastReplyDate;
    }

    public String getLocation() {
        return this.location;
    }

    public String getPicPath() {
        return this.picPath;
    }

    public String getPoll() {
        return this.poll;
    }

    public int getReplieCount() {
        return this.replieCount;
    }

    public int getRoleNum() {
        return this.roleNum;
    }

    public int getStatus() {
        return this.status;
    }

    public String getTitle() {
        return this.title;
    }

    public int getTop() {
        return this.top;
    }

    public List<TopicContentModel> getTopicContentList() {
        return this.topicContentList;
    }

    public long getTopicId() {
        return this.topicId;
    }

    public int getType() {
        return this.type;
    }

    public long getUserId() {
        return this.userId;
    }

    public String getUserNickName() {
        return this.userNickName;
    }

    public boolean isFollow() {
        return this.follow;
    }

    public boolean isHasImg() {
        return this.hasImg;
    }

    public void setBoardId(long boardId2) {
        this.boardId = boardId2;
    }

    public void setBoardName(String boardName2) {
        this.boardName = boardName2;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public void setCreateDate(long createDate2) {
        this.createDate = createDate2;
    }

    public void setDistance(int distance2) {
        this.distance = distance2;
    }

    public void setEssence(int essence2) {
        this.essence = essence2;
    }

    public void setFollow(boolean follow2) {
        this.follow = follow2;
    }

    public void setHasImg(boolean hasImg2) {
        this.hasImg = hasImg2;
    }

    public void setHasNext(int hasNext2) {
        this.hasNext = hasNext2;
    }

    public void setHitCount(int hitCount2) {
        this.hitCount = hitCount2;
    }

    public void setHot(int hot2) {
        this.hot = hot2;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public void setIsFavor(int isFavor2) {
        this.isFavor = isFavor2;
    }

    public void setLastReplyDate(long lastReplyDate2) {
        this.lastReplyDate = lastReplyDate2;
    }

    public void setLocation(String location2) {
        this.location = location2;
    }

    public void setPicPath(String picPath2) {
        this.picPath = picPath2;
    }

    public void setPoll(String poll2) {
        this.poll = poll2;
    }

    public void setReplieCount(int replieCount2) {
        this.replieCount = replieCount2;
    }

    public void setRoleNum(int roleNum2) {
        this.roleNum = roleNum2;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public void setTop(int top2) {
        this.top = top2;
    }

    public void setTopicContentList(List<TopicContentModel> topicContentList2) {
        this.topicContentList = topicContentList2;
    }

    public void setTopicId(long topicId2) {
        this.topicId = topicId2;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
    }

    public void setUserNickName(String userNickName2) {
        this.userNickName = userNickName2;
    }

    public String getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(String thumbnail2) {
        this.thumbnail = thumbnail2;
    }

    public String getPic() {
        return this.pic;
    }

    public void setPic(String pic2) {
        this.pic = pic2;
    }

    public boolean getBannedUser() {
        return this.isBannedUser;
    }

    public void setBannedUser(boolean isBannedUser2) {
        this.isBannedUser = isBannedUser2;
    }

    public boolean getShieldedUser() {
        return this.isShieldedUser;
    }

    public void setShieldedUser(boolean isShieldedUser2) {
        this.isShieldedUser = isShieldedUser2;
    }

    public int getUploadType() {
        return this.uploadType;
    }

    public void setUploadType(int uploadType2) {
        this.uploadType = uploadType2;
    }

    public int getVisible() {
        return this.visible;
    }

    public void setVisible(int visible2) {
        this.visible = visible2;
    }
}
