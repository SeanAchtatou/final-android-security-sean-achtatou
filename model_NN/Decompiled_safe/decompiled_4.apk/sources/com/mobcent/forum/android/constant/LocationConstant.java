package com.mobcent.forum.android.constant;

public interface LocationConstant {
    public static final String ACTION = "action";
    public static final String LOGIN = "login";
    public static final String NAME = "name";
    public static final String POI = "p";
}
