package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.PermRestfulApiRequester;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.db.PermDBUtil;
import com.mobcent.forum.android.model.PermModel;
import com.mobcent.forum.android.service.PermService;
import com.mobcent.forum.android.service.impl.helper.PermServiceImplHelper;
import com.mobcent.forum.android.util.StringUtil;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class PermServiceImpl implements PermService, PermConstant {
    private Context context;

    public PermServiceImpl(Context context2) {
        this.context = context2;
    }

    public PermModel getPerm() {
        return PermServiceImplHelper.getPermission(this.context, PermRestfulApiRequester.getPerm(this.context));
    }

    public int getPermNum(String groupOrBoards, String flag, long boardId) {
        List<PermModel> permList;
        if (!StringUtil.isEmpty(PermDBUtil.getInstance(this.context).getPermJsonString())) {
            try {
                PermModel permModel = PermServiceImplHelper.getPermissionModel(new JSONObject(PermDBUtil.getInstance(this.context).getPermJsonString()));
                if (permModel != null) {
                    if (PermConstant.USER_GROUP.equals(groupOrBoards)) {
                        if (permModel.getUserGroup() != null) {
                            return getNum(permModel.getUserGroup(), flag);
                        }
                    } else if (PermConstant.BOARDS.equals(groupOrBoards) && (permList = permModel.getBoards()) != null) {
                        for (int i = 0; i < permList.size(); i++) {
                            if (permList.get(i).getBoardId() == boardId) {
                                return getNum(permList.get(i), flag);
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return 1;
    }

    private int getNum(PermModel permModel, String flag) {
        if (PermConstant.VISIT.equals(flag)) {
            return permModel.getVisit();
        }
        if (PermConstant.POST.equals(flag)) {
            return permModel.getPost();
        }
        if (PermConstant.READ.equals(flag)) {
            return permModel.getRead();
        }
        if ("reply".equals(flag)) {
            return permModel.getReply();
        }
        if (PermConstant.UPLOAD.equals(flag)) {
            return permModel.getUpload();
        }
        if ("download".equals(flag)) {
            return permModel.getDownload();
        }
        return -1;
    }

    public String getGroupType() {
        if (!StringUtil.isEmpty(PermDBUtil.getInstance(this.context).getPermJsonString())) {
            try {
                PermModel permModel = PermServiceImplHelper.getPermissionModel(new JSONObject(PermDBUtil.getInstance(this.context).getPermJsonString()));
                if (!(permModel == null || permModel.getUserGroup() == null)) {
                    return permModel.getUserGroup().getGroupType();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return "";
    }
}
