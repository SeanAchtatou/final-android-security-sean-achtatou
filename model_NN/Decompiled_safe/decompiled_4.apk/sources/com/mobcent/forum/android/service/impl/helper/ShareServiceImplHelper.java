package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.PostsApiConstant;
import org.json.JSONObject;

public class ShareServiceImplHelper implements PostsApiConstant {
    public static boolean parseSuccessModel(String jsonStr) {
        try {
            if (new JSONObject(jsonStr).optInt("rs") == 0) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
