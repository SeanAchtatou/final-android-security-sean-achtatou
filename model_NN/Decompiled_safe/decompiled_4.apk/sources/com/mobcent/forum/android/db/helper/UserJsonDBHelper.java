package com.mobcent.forum.android.db.helper;

import com.mobcent.forum.android.constant.UserConstant;
import com.mobcent.forum.android.model.UserInfoModel;
import org.json.JSONException;
import org.json.JSONObject;

public class UserJsonDBHelper implements UserConstant {
    public static String buildUserInfo(UserInfoModel user) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("uid", user.getUserId());
            jsonObj.put("email", user.getEmail());
            jsonObj.put("password", user.getPwd());
            jsonObj.put("nickname", user.getNickname());
            jsonObj.put("secret", user.getUas());
            jsonObj.put("token", user.getUat());
            jsonObj.put("icon", user.getIcon());
            jsonObj.put("icon_url", user.getIconUrl());
            jsonObj.put("gender", user.getGender());
            jsonObj.put("score", user.getScore());
            jsonObj.put("credits", user.getCredits());
            jsonObj.put("signature", user.getSignature());
            jsonObj.put("platformId", user.getPlatformId());
            jsonObj.put(UserConstant.PLATFORM_USER_ID, user.getPlatformUserId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj.toString();
    }

    public static UserInfoModel formUserInfo(String jsonStr) {
        UserInfoModel user = new UserInfoModel();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            user.setUserId(jsonObj.optLong("uid"));
            user.setEmail(jsonObj.optString("email"));
            user.setPwd(jsonObj.optString("password"));
            user.setNickname(jsonObj.optString("nickname"));
            user.setUas(jsonObj.optString("secret"));
            user.setUat(jsonObj.optString("token"));
            user.setIcon(jsonObj.optString("icon"));
            user.setIconUrl(jsonObj.optString("icon_url"));
            user.setGender(jsonObj.optInt("gender"));
            user.setScore(jsonObj.optInt("score"));
            user.setCredits(jsonObj.optInt("credits"));
            user.setSignature(jsonObj.optString("signature"));
            user.setPlatformId((long) jsonObj.optInt("platformId"));
            user.setPlatformUserId(jsonObj.optString(UserConstant.PLATFORM_USER_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }

    public static String buildPersonalInfo(UserInfoModel user) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("uid", user.getUserId());
            jsonObj.put("icon", user.getIcon());
            jsonObj.put("name", user.getNickname());
            jsonObj.put("gender", user.getGender());
            jsonObj.put(UserConstant.IS_FOLLOW, user.getIsFollow());
            jsonObj.put("status", user.getStatus());
            jsonObj.put("credits", user.getCredits());
            jsonObj.put(UserConstant.GOLD_NUM, user.getGoldNum());
            jsonObj.put("level", user.getLevel());
            jsonObj.put(UserConstant.TOPIC_NUM, user.getTopicNum());
            jsonObj.put(UserConstant.REPLY_POSTS_NUM, user.getReplyPostsNum());
            jsonObj.put(UserConstant.ESSENCE_NUM, user.getEssenceNum());
            jsonObj.put("favor_num", user.getUserFavoriteNum());
            jsonObj.put(UserConstant.USER_RELATE_POSTS_NUM, user.getRelatePostsNum());
            jsonObj.put(UserConstant.USER_FRIEND_NUM, user.getUserFriendsNum());
            jsonObj.put(UserConstant.LAST_LOGIN_TIME, user.getLastLoginTime());
            jsonObj.put(UserConstant.IS_BLACK_USER, user.getBlackStatus());
            jsonObj.put("level", user.getLevel());
            jsonObj.put(UserConstant.USER_FOLLOW_NUM, user.getFollowNum());
            jsonObj.put(UserConstant.GAG_BOARD_NUM, user.getBannedBoardNum());
            jsonObj.put(UserConstant.SHIELD_BOARD_NUM, user.getShieldBoardNum());
            jsonObj.put(UserConstant.IS_DELE_ACCOUNT, user.getIsDelAccounts());
            jsonObj.put(UserConstant.IS_DELE_IP, user.getIsDelIp());
            jsonObj.put("role_num", user.getRoleNum());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj.toString();
    }

    public static UserInfoModel fromPersonalInfo(String jsonStr) {
        UserInfoModel userInfoModel = new UserInfoModel();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            userInfoModel.setUserId(jsonObject.optLong("uid"));
            userInfoModel.setIcon(jsonObject.optString("icon"));
            userInfoModel.setNickname(jsonObject.optString("name"));
            userInfoModel.setGender(jsonObject.optInt("gender"));
            userInfoModel.setIsFollow(jsonObject.optInt(UserConstant.IS_FOLLOW));
            userInfoModel.setStatus(jsonObject.optInt("status"));
            userInfoModel.setCredits(jsonObject.optInt("credits"));
            userInfoModel.setGoldNum(jsonObject.optInt(UserConstant.GOLD_NUM));
            userInfoModel.setLevel(jsonObject.optInt("level"));
            userInfoModel.setTopicNum(jsonObject.optInt(UserConstant.TOPIC_NUM));
            userInfoModel.setReplyPostsNum(jsonObject.optInt(UserConstant.REPLY_POSTS_NUM));
            userInfoModel.setEssenceNum(jsonObject.optInt(UserConstant.ESSENCE_NUM));
            userInfoModel.setUserFavoriteNum(jsonObject.optInt("favor_num"));
            userInfoModel.setRelatePostsNum(jsonObject.optInt(UserConstant.USER_RELATE_POSTS_NUM));
            userInfoModel.setUserFriendsNum(jsonObject.optInt(UserConstant.USER_FRIEND_NUM));
            userInfoModel.setLastLoginTime(jsonObject.optLong(UserConstant.LAST_LOGIN_TIME));
            userInfoModel.setBlackStatus(jsonObject.optInt(UserConstant.IS_BLACK_USER));
            userInfoModel.setLevel(jsonObject.optInt("level"));
            userInfoModel.setFollowNum(jsonObject.optInt(UserConstant.USER_FOLLOW_NUM));
            userInfoModel.setBannedBoardNum(jsonObject.optInt(UserConstant.GAG_BOARD_NUM));
            userInfoModel.setShieldBoardNum(jsonObject.optInt(UserConstant.SHIELD_BOARD_NUM));
            userInfoModel.setIsDelAccounts(jsonObject.optInt(UserConstant.IS_DELE_ACCOUNT));
            userInfoModel.setIsDelIp(jsonObject.optInt(UserConstant.IS_DELE_IP));
            userInfoModel.setRoleNum(jsonObject.optInt("role_num"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return userInfoModel;
    }
}
