package com.mobcent.forum.android.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.WindowManager;
import com.tencent.mm.sdk.platformtools.Util;
import java.util.Locale;

public class PhoneUtil {
    public static String getUserAgent() {
        return getPhoneType();
    }

    public static String getIMEI(Context context) {
        String imei = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (imei == null) {
            return "";
        }
        return imei;
    }

    public static String getIMSI(Context context) {
        String imsi = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        if (imsi == null) {
            return "";
        }
        return imsi;
    }

    public static String getNetworkOperatorName(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
    }

    public static String getNetworkOperatorName(Activity activity) {
        return ((TelephonyManager) activity.getSystemService("phone")).getNetworkOperatorName();
    }

    public static String getPhoneType() {
        String type = Build.MODEL;
        if (type != null) {
            type = type.replace(" ", "");
        }
        return type.trim();
    }

    public static String getDevice() {
        return Build.DEVICE;
    }

    public static String getProduct() {
        return Build.PRODUCT;
    }

    public static String getType() {
        return Build.TYPE;
    }

    public static String getSDKVersionName() {
        return Build.VERSION.RELEASE;
    }

    public static String getSDKVersion() {
        return Build.VERSION.SDK;
    }

    public static int getAndroidSDKVersion() {
        return Build.VERSION.SDK_INT;
    }

    public static String getResolution(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(dm);
        return String.valueOf(dm.widthPixels) + "x" + dm.heightPixels;
    }

    public static String getResolution(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return String.valueOf(dm.widthPixels) + "x" + dm.heightPixels;
    }

    public static int getDisplayWidth(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public static int getDisplayWidth(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public static int getDisplayHeight(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    public static int getDisplayHeight(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    public static float getDisplayDensity(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(dm);
        return dm.density;
    }

    public static float getDisplayDensity(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.density;
    }

    public static String getPhoneLanguage() {
        String language = Locale.getDefault().getLanguage();
        if (language == null) {
            return "";
        }
        return language;
    }

    public static String getLocalMacAddress(Context context) {
        WifiInfo info = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo();
        if (info == null) {
            return "";
        }
        return info.getMacAddress();
    }

    public static int getCacheSize(Context context) {
        return (Util.BYTE_OF_MB * ((ActivityManager) context.getSystemService("activity")).getMemoryClass()) / 8;
    }

    public static int getRawSize(Context context, int unit, float size) {
        Resources resources;
        if (context == null) {
            resources = Resources.getSystem();
        } else {
            resources = context.getResources();
        }
        return (int) TypedValue.applyDimension(unit, size, resources.getDisplayMetrics());
    }

    public static String getNetWorkName(Context context) {
        return PhoneConnectionUtil.getNetworkType(context).toLowerCase();
    }

    public static String getServiceName(Context context) {
        if (PhoneConnectionUtil.getNetworkType(context).equals("wifi")) {
            return "wifi";
        }
        if (PhoneConnectionUtil.isConnectChinaMobile(context)) {
            return "mobile";
        }
        if (PhoneConnectionUtil.isConnectChinaUnicom(context)) {
            return "unicom";
        }
        if (PhoneConnectionUtil.isConnectChinaTelecom(context)) {
            return "telecom";
        }
        return "";
    }

    public static float dip2px(Context context, float dipValue) {
        return TypedValue.applyDimension(1, dipValue, Resources.getSystem().getDisplayMetrics());
    }
}
