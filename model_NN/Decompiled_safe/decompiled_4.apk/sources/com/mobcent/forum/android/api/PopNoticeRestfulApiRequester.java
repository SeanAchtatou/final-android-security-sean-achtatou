package com.mobcent.forum.android.api;

import android.content.Context;
import android.content.pm.PackageManager;
import com.mobcent.forum.android.constant.PopNoticeConstant;
import java.util.HashMap;

public class PopNoticeRestfulApiRequester extends BaseRestfulApiRequester implements PopNoticeConstant {
    private static int currentVersionCode;

    public static String getPopNoticeModel(Context context) {
        try {
            currentVersionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        HashMap<String, String> params = new HashMap<>();
        params.put(PopNoticeConstant.TYPE_DEVICE_REQ_PARAMS, "1");
        params.put("versionCode", new StringBuilder(String.valueOf(currentVersionCode)).toString());
        return doPostRequest("forum/notice.do", params, context);
    }
}
