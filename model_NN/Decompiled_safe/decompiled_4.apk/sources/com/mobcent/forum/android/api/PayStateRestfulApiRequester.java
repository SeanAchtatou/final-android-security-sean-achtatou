package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.api.util.HttpClientUtil;
import com.mobcent.forum.android.constant.PayStateConstant;
import com.mobcent.forum.android.util.MCResource;
import java.util.HashMap;

public class PayStateRestfulApiRequester implements PayStateConstant {
    public static String controll(Context context, String forumKey) {
        String url = String.valueOf(MCResource.getInstance(context).getString("mc_forum_base_request_domain_url")) + "pay/payState.do";
        HashMap<String, String> params = new HashMap<>();
        params.put("forumKey", forumKey);
        params.put(HttpClientUtil.SET_CONNECTION_TIMEOUT_STR, "1000");
        params.put(HttpClientUtil.SET_SOCKET_TIMEOUT_STR, "2000");
        return HttpClientUtil.doPostRequest(url, params, context);
    }
}
