package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.constant.ModeratorConstant;
import java.util.HashMap;

public class ModeratorRestfulApiRequester extends BaseRestfulApiRequester implements ModeratorConstant {
    public static String addModerator(Context context, long moderatorUserId, String boardIds, long topicId) {
        HashMap<String, String> params = new HashMap<>();
        if (!boardIds.equals("")) {
            params.put(ModeratorConstant.BOARD_IDS, boardIds);
        }
        params.put(ModeratorConstant.ModeratorToUser, new StringBuilder(String.valueOf(moderatorUserId)).toString());
        if (topicId > 0) {
            params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        }
        return doPostRequest("admin/addModerator.do", params, context);
    }

    public static String cancelModerator(Context context, long moderatorUserId, String boardIds, long topicId) {
        HashMap<String, String> params = new HashMap<>();
        if (!boardIds.equals("")) {
            params.put(ModeratorConstant.BOARD_IDS, boardIds);
        }
        params.put(ModeratorConstant.ModeratorToUser, new StringBuilder(String.valueOf(moderatorUserId)).toString());
        if (topicId > 0) {
            params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        }
        return doPostRequest("admin/cancelModerator.do", params, context);
    }

    public static String getModeratorList(Context context) {
        return doPostRequest("admin/moderatorList.do", new HashMap<>(), context);
    }

    public static String getModeratorBoardList(Context context) {
        return doPostRequest("admin/moderatorBoardList.do", new HashMap<>(), context);
    }

    public static String setModerator(Context context, long moderatorUserId, String boardIds, long topicId, long ReplyId) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ModeratorConstant.BOARD_IDS, boardIds);
        params.put(ModeratorConstant.ModeratorToUser, new StringBuilder(String.valueOf(moderatorUserId)).toString());
        if (topicId > 0) {
            params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        }
        if (ReplyId > 0) {
            params.put(ModeratorConstant.REPLY_POSTS_ID, new StringBuilder(String.valueOf(ReplyId)).toString());
        }
        return doPostRequest("admin/setModerator.do", params, context);
    }
}
