package com.mobcent.forum.android.model;

public class TopicContentModel extends BaseModel {
    private static final long serialVersionUID = 7205015692078660032L;
    private int imageIndex;
    private String infor;
    private PollTopicModel pollTopicModel;
    private RepostTopicModel repostTopicModel;
    private SoundModel soundModel;
    private int type;

    public RepostTopicModel getRepostTopicModel() {
        return this.repostTopicModel;
    }

    public void setRepostTopicModel(RepostTopicModel repostTopicModel2) {
        this.repostTopicModel = repostTopicModel2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public String getInfor() {
        return this.infor;
    }

    public void setInfor(String infor2) {
        this.infor = infor2;
    }

    public int getImageIndex() {
        return this.imageIndex;
    }

    public void setImageIndex(int imageIndex2) {
        this.imageIndex = imageIndex2;
    }

    public PollTopicModel getPollTopicModel() {
        return this.pollTopicModel;
    }

    public void setPollTopicModel(PollTopicModel pollTopicModel2) {
        this.pollTopicModel = pollTopicModel2;
    }

    public SoundModel getSoundModel() {
        return this.soundModel;
    }

    public void setSoundModel(SoundModel soundModel2) {
        this.soundModel = soundModel2;
    }
}
