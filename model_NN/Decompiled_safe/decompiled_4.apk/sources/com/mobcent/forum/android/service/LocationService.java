package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.LocationModel;

public interface LocationService {
    LocationModel getLocalLocationInfo(long j);

    boolean saveLocation(double d, double d2, String str);
}
