package com.mobcent.forum.android.db.constant;

public interface FeedsDBConstant extends BaseDBConstant {
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_JSON_STR = "jsonStr";
    public static final String COLUMN_UPDATE_TIME = "update_time";
    public static final String SQL_CREATE_TABLE_FEEDS = "CREATE TABLE IF NOT EXISTS feeds_table(id LONG PRIMARY KEY,jsonStr TEXT , update_time LONG );";
    public static final String SQL_CREATE_TABLE_NEW_FEEDS = "CREATE TABLE IF NOT EXISTS new_feeds_table(id LONG PRIMARY KEY,jsonStr TEXT , update_time LONG );";
    public static final String SQL_SELECT_FEEDS_TOPTIC = "SELECT * FROM feeds_table";
    public static final String SQL_SELECT_NEW_FEEDS_TOPTIC = "SELECT * FROM new_feeds_table";
    public static final String TABLE_FEEDS = "feeds_table";
    public static final String TABLE_NEW_FEEDS = "new_feeds_table";
}
