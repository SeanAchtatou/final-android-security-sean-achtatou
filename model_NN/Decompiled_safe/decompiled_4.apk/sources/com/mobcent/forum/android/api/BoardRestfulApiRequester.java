package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.util.MCResource;
import java.util.HashMap;

public abstract class BoardRestfulApiRequester extends BaseRestfulApiRequester {
    public static String getBoards(Context context) {
        return doPostRequest(MCResource.getInstance(context).getString("mc_forum_board_domain_url"), new HashMap<>(), context);
    }
}
