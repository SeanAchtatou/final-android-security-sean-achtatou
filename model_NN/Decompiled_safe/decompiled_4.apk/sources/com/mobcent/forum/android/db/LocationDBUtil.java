package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import com.mobcent.forum.android.db.constant.LocationDBConstant;
import com.mobcent.forum.android.model.LocationModel;

public class LocationDBUtil extends BaseDBUtil implements LocationDBConstant {
    private static LocationDBUtil locationDBUtil;

    public LocationDBUtil(Context ctx) {
        super(ctx);
    }

    public static LocationDBUtil getInstance(Context context) {
        if (locationDBUtil == null) {
            locationDBUtil = new LocationDBUtil(context);
        }
        return locationDBUtil;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0064  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.mobcent.forum.android.model.LocationModel getLocationInfo(long r13) {
        /*
            r12 = this;
            com.mobcent.forum.android.model.LocationModel r10 = new com.mobcent.forum.android.model.LocationModel
            r10.<init>()
            r8 = 0
            r12.openReadableDB()     // Catch:{ Exception -> 0x004d }
            android.database.sqlite.SQLiteDatabase r0 = r12.readableDatabase     // Catch:{ Exception -> 0x004d }
            java.lang.String r1 = "location"
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004d }
            java.lang.String r4 = "id="
            r3.<init>(r4)     // Catch:{ Exception -> 0x004d }
            java.lang.StringBuilder r3 = r3.append(r13)     // Catch:{ Exception -> 0x004d }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x004d }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x004d }
        L_0x0025:
            boolean r0 = r8.moveToNext()     // Catch:{ Exception -> 0x004d }
            if (r0 != 0) goto L_0x0034
            if (r8 == 0) goto L_0x0030
            r8.close()
        L_0x0030:
            r12.closeReadableDB()
        L_0x0033:
            return r10
        L_0x0034:
            r0 = 1
            double r0 = r8.getDouble(r0)     // Catch:{ Exception -> 0x004d }
            r10.setLatitude(r0)     // Catch:{ Exception -> 0x004d }
            r0 = 2
            double r0 = r8.getDouble(r0)     // Catch:{ Exception -> 0x004d }
            r10.setLongitude(r0)     // Catch:{ Exception -> 0x004d }
            r0 = 3
            java.lang.String r0 = r8.getString(r0)     // Catch:{ Exception -> 0x004d }
            r10.setAddrStr(r0)     // Catch:{ Exception -> 0x004d }
            goto L_0x0025
        L_0x004d:
            r0 = move-exception
            r9 = r0
            com.mobcent.forum.android.model.LocationModel r11 = new com.mobcent.forum.android.model.LocationModel     // Catch:{ all -> 0x0061 }
            r11.<init>()     // Catch:{ all -> 0x0061 }
            r9.printStackTrace()     // Catch:{ all -> 0x006b }
            if (r8 == 0) goto L_0x005c
            r8.close()
        L_0x005c:
            r12.closeReadableDB()
            r10 = r11
            goto L_0x0033
        L_0x0061:
            r0 = move-exception
        L_0x0062:
            if (r8 == 0) goto L_0x0067
            r8.close()
        L_0x0067:
            r12.closeReadableDB()
            throw r0
        L_0x006b:
            r0 = move-exception
            r10 = r11
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.forum.android.db.LocationDBUtil.getLocationInfo(long):com.mobcent.forum.android.model.LocationModel");
    }

    public boolean updateLocation(LocationModel locationModel) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("latitude", Double.valueOf(locationModel.getLatitude()));
            values.put("longitude", Double.valueOf(locationModel.getLongitude()));
            values.put(LocationDBConstant.COLUMN_ADDR, locationModel.getAddrStr());
            values.put("id", Long.valueOf(locationModel.getUserId()));
            if (!isRowExisted(this.writableDatabase, "location", "id", locationModel.getUserId())) {
                this.writableDatabase.insertOrThrow("location", null, values);
            } else {
                this.writableDatabase.update("location", values, "id = " + locationModel.getUserId(), null);
            }
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean deleteLocationInfo() {
        return removeAllEntries("location");
    }
}
