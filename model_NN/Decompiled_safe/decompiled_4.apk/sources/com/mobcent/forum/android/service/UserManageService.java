package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.Date;
import java.util.List;

public interface UserManageService {
    String bannedShielded(int i, long j, long j2, int i2, Date date, String str);

    String bannedShielded(int i, long j, long j2, List<BoardModel> list, Date date, String str);

    String cancelBannedShielded(int i, long j, int i2);

    List<UserInfoModel> getBannedShieldedUser(int i, int i2, int i3, int i4);

    List<UserInfoModel> searchBannedShieldedUser(int i, int i2, String str, int i3, int i4);
}
