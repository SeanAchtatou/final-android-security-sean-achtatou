package com.mobcent.forum.android.util;

import android.content.Context;
import android.os.Vibrator;

public class PhoneVibratorUtil {
    public static void shotVibratePhone(Context context) {
        ((Vibrator) context.getSystemService("vibrator")).vibrate(new long[]{800, 50, 400, 30}, -1);
    }
}
