package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.UserConstant;
import com.mobcent.forum.android.model.UserBoardInfoModel;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class UserBoardInfoServiceHelper extends BaseJsonHelper implements UserConstant {
    public static UserInfoModel getUserBoardInfoList(String jsonStr) {
        UserInfoModel userInfoModel = new UserInfoModel();
        List<UserBoardInfoModel> userBoardInfoList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            userInfoModel.setRoleNum(jsonObject.optInt("role_num"));
            try {
                JSONArray jsonArray = jsonObject.optJSONArray("list");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonModel = jsonArray.getJSONObject(i);
                    UserBoardInfoModel userBoardInfoModel = new UserBoardInfoModel();
                    userBoardInfoModel.setBoardId((long) jsonModel.optInt("board_id"));
                    userBoardInfoModel.setBoardName(jsonModel.optString("board_name"));
                    userBoardInfoModel.setIsModerator(jsonModel.optInt("is_moderator"));
                    userBoardInfoModel.setIsBanned(jsonModel.optInt("is_gag"));
                    userBoardInfoModel.setIsShielded(jsonModel.optInt("is_shield"));
                    userBoardInfoList.add(userBoardInfoModel);
                }
                userInfoModel.setUserBoardInfoList(userBoardInfoList);
            } catch (Exception e) {
                userInfoModel = null;
            }
            return userInfoModel;
        } catch (Exception e2) {
            Exception exc = e2;
            return null;
        }
    }
}
