package com.mobcent.forum.android.constant;

public interface PopNoticeConstant {
    public static final String REQUEST_VERSIONCODE = "versionCode";
    public static final String RETURN_CONTENT = "content";
    public static final String RETURN_NOTICE = "notice";
    public static final String RETURN_NOTICE_ID = "nid";
    public static final String RETURN_TYPE = "type";
    public static final String RETURN_URL = "url";
    public static final String RETURN_VERSION = "version";
    public static final String TYPE_DEVICE_REQ_PARAMS = "device";
}
