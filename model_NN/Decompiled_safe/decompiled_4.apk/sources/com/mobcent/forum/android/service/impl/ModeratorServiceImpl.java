package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.ModeratorRestfulApiRequester;
import com.mobcent.forum.android.db.ModeratorBoardDBUtil;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.model.ModeratorModel;
import com.mobcent.forum.android.service.ModeratorService;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.service.impl.helper.ModeratorServiceImplHelper;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class ModeratorServiceImpl implements ModeratorService {
    /* access modifiers changed from: private */
    public Context context;

    public ModeratorServiceImpl(Context context2) {
        this.context = context2;
    }

    public List<ModeratorModel> getModeratorList() {
        new ArrayList();
        String jsonStr = ModeratorRestfulApiRequester.getModeratorList(this.context);
        List<ModeratorModel> moderatorList = ModeratorServiceImplHelper.getModeratorList(jsonStr);
        if (moderatorList == null || moderatorList.size() == 0) {
            if (moderatorList == null) {
                moderatorList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                ModeratorModel topic = new ModeratorModel();
                topic.setErrorCode(errorCode);
                moderatorList.add(topic);
            }
        }
        return moderatorList;
    }

    public String addModerator(long ModeratorToUser, String boardIds, long topicId) {
        return BaseJsonHelper.formJsonRS(ModeratorRestfulApiRequester.addModerator(this.context, ModeratorToUser, boardIds, topicId));
    }

    public String delModerator(long ModeratorToUser, String boardIds, long topicId) {
        return BaseJsonHelper.formJsonRS(ModeratorRestfulApiRequester.cancelModerator(this.context, ModeratorToUser, boardIds, topicId));
    }

    public ModeratorModel getModeratorBoardList(long userId, boolean isLocal) {
        if (!isLocal) {
            return getModeratorBoardListNet(userId);
        }
        ModeratorModel moderatorModel = getModeratorBoardByLocal(userId);
        if (moderatorModel == null) {
            return getModeratorBoardListNet(userId);
        }
        return moderatorModel;
    }

    public ModeratorModel getModeratorBoardByLocal(long userId) {
        String jsonStr;
        try {
            jsonStr = ModeratorBoardDBUtil.getInstance(this.context).getModeratorBoardJsonString(userId);
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
        }
        if (!StringUtil.isEmpty(jsonStr)) {
            return ModeratorServiceImplHelper.getModeratorModel(jsonStr);
        }
        return null;
    }

    public ModeratorModel getModeratorBoardListNet(final long userId) {
        final String jsonStr = ModeratorRestfulApiRequester.getModeratorBoardList(this.context);
        ModeratorModel moderatorModel = ModeratorServiceImplHelper.getModeratorModel(jsonStr);
        if (moderatorModel == null) {
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (StringUtil.isEmpty(errorCode)) {
                return moderatorModel;
            }
            ModeratorModel moderatorModel2 = new ModeratorModel();
            moderatorModel2.setErrorCode(errorCode);
            return moderatorModel2;
        }
        new Thread() {
            public void run() {
                ModeratorBoardDBUtil.getInstance(ModeratorServiceImpl.this.context).updateModeratorBoardString(jsonStr, userId);
            }
        }.start();
        return moderatorModel;
    }

    public boolean BoardPermission(long boardId, long userId) {
        ModeratorModel moderatorModel;
        List<BoardModel> list;
        if (!(!new UserServiceImpl(this.context).isLogin() || (moderatorModel = getModeratorBoardList(userId, true)) == null || (list = moderatorModel.getBoardList()) == null)) {
            for (int i = 0; i < list.size(); i++) {
                if (boardId == list.get(i).getBoardId()) {
                    return true;
                }
            }
        }
        return false;
    }

    public String setModerator(long ModeratorToUser, String boardIds, long topicId, long ReplyId) {
        return BaseJsonHelper.formJsonRS(ModeratorRestfulApiRequester.setModerator(this.context, ModeratorToUser, boardIds, topicId, ReplyId));
    }
}
