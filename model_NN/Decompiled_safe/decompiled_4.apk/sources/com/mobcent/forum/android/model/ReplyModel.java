package com.mobcent.forum.android.model;

import java.util.List;

public class ReplyModel extends BaseModel {
    private static final long serialVersionUID = 6472536821625859659L;
    private String[] dialogFunction;
    private int floor;
    private boolean follow;
    private boolean hasImg;
    private String icon;
    private boolean isAd;
    private boolean isBannedUser = false;
    private boolean isQuote;
    private boolean isShieldedUser = false;
    private int level;
    private String location;
    private String pic;
    private long postsDate;
    private String quoteContent;
    private String quoteUserName;
    private String replyContent;
    private List<TopicContentModel> replyContentList;
    private long replyPostsId;
    private long replyUserId;
    private int roleNum;
    private String shortContent;
    private int status;
    private String thumbnail;
    private String title;
    private String userNickName;

    public String getPic() {
        return this.pic;
    }

    public void setPic(String pic2) {
        this.pic = pic2;
    }

    public int getFloor() {
        return this.floor;
    }

    public void setFloor(int floor2) {
        this.floor = floor2;
    }

    public boolean isAd() {
        return this.isAd;
    }

    public void setAd(boolean isAd2) {
        this.isAd = isAd2;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location2) {
        this.location = location2;
    }

    public int getRoleNum() {
        return this.roleNum;
    }

    public void setRoleNum(int roleNum2) {
        this.roleNum = roleNum2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public long getReplyUserId() {
        return this.replyUserId;
    }

    public void setReplyUserId(long replyUserId2) {
        this.replyUserId = replyUserId2;
    }

    public long getReplyPostsId() {
        return this.replyPostsId;
    }

    public void setReplyPostsId(long replyPostsId2) {
        this.replyPostsId = replyPostsId2;
    }

    public String getReplyContent() {
        return this.replyContent;
    }

    public void setReplyContent(String replyContent2) {
        this.replyContent = replyContent2;
    }

    public String getUserNickName() {
        return this.userNickName;
    }

    public void setUserNickName(String userNickName2) {
        this.userNickName = userNickName2;
    }

    public long getPostsDate() {
        return this.postsDate;
    }

    public void setPostsDate(long postsDate2) {
        this.postsDate = postsDate2;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public boolean isQuote() {
        return this.isQuote;
    }

    public void setQuote(boolean isQuote2) {
        this.isQuote = isQuote2;
    }

    public String getQuoteContent() {
        return this.quoteContent;
    }

    public void setQuoteContent(String quoteContent2) {
        this.quoteContent = quoteContent2;
    }

    public String getQuoteUserName() {
        return this.quoteUserName;
    }

    public void setQuoteUserName(String quoteUserName2) {
        this.quoteUserName = quoteUserName2;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public List<TopicContentModel> getReplyContentList() {
        return this.replyContentList;
    }

    public void setReplyContentList(List<TopicContentModel> replyContentList2) {
        this.replyContentList = replyContentList2;
    }

    public String getShortContent() {
        return this.shortContent;
    }

    public void setShortContent(String shortContent2) {
        this.shortContent = shortContent2;
    }

    public boolean isHasImg() {
        return this.hasImg;
    }

    public void setHasImg(boolean hasImg2) {
        this.hasImg = hasImg2;
    }

    public boolean isFollow() {
        return this.follow;
    }

    public void setFollow(boolean follow2) {
        this.follow = follow2;
    }

    public String[] getDialogFunction() {
        return this.dialogFunction;
    }

    public void setDialogFunction(String[] dialogFunction2) {
        this.dialogFunction = dialogFunction2;
    }

    public String getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(String thumbnail2) {
        this.thumbnail = thumbnail2;
    }

    public boolean getBannedUser() {
        return this.isBannedUser;
    }

    public void setBannedUser(boolean isBannedUser2) {
        this.isBannedUser = isBannedUser2;
    }

    public boolean getShieldedUser() {
        return this.isShieldedUser;
    }

    public void setShieldedUser(boolean isShieldedUser2) {
        this.isShieldedUser = isShieldedUser2;
    }
}
