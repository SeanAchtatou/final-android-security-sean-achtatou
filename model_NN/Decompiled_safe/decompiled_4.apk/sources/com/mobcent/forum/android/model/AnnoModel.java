package com.mobcent.forum.android.model;

public class AnnoModel extends TopicModel {
    private static final long serialVersionUID = 7205015692078660032L;
    private long annoCreateDate;
    private long annoEndDate;
    private long annoId;
    private long annoStartDate;
    private String author;
    private long authorId;
    private long boardId;
    private long forumId;
    private String icon;
    private int isReply;
    private String subject;
    private int verify;

    public long getAnnoStartDate() {
        return this.annoStartDate;
    }

    public long getAnnoId() {
        return this.annoId;
    }

    public void setAnnoId(long annoId2) {
        this.annoId = annoId2;
    }

    public void setAnnoStartDate(long annoStartDate2) {
        this.annoStartDate = annoStartDate2;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author2) {
        this.author = author2;
    }

    public long getForumId() {
        return this.forumId;
    }

    public void setForumId(long forumId2) {
        this.forumId = forumId2;
    }

    public long getAnnoEndDate() {
        return this.annoEndDate;
    }

    public void setAnnoEndDate(long annoEndDate2) {
        this.annoEndDate = annoEndDate2;
    }

    public long getAuthorId() {
        return this.authorId;
    }

    public void setAuthorId(long authorId2) {
        this.authorId = authorId2;
    }

    public long getBoardId() {
        return this.boardId;
    }

    public void setBoardId(long boardId2) {
        this.boardId = boardId2;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject2) {
        this.subject = subject2;
    }

    public int getVerify() {
        return this.verify;
    }

    public void setVerify(int verify2) {
        this.verify = verify2;
    }

    public long getAnnoCreateDate() {
        return this.annoCreateDate;
    }

    public void setAnnoCreateDate(long annoCreateDate2) {
        this.annoCreateDate = annoCreateDate2;
    }

    public int getIsReply() {
        return this.isReply;
    }

    public void setIsReply(int isReply2) {
        this.isReply = isReply2;
    }
}
