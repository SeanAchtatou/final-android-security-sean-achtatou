package com.mobcent.forum.android.model;

import java.util.List;
import java.util.Set;

public class UserFeedModel extends BaseModel {
    private static final long serialVersionUID = 2598947967313524711L;
    private int hasNext;
    private Set<String> imgSet;
    private UserInfoModel userInfo;
    private List<UserTopicFeedModel> userTopicList;

    public UserInfoModel getUserInfo() {
        return this.userInfo;
    }

    public void setUserInfo(UserInfoModel userInfo2) {
        this.userInfo = userInfo2;
    }

    public List<UserTopicFeedModel> getUserTopicList() {
        return this.userTopicList;
    }

    public void setUserTopicList(List<UserTopicFeedModel> userTopicList2) {
        this.userTopicList = userTopicList2;
    }

    public int getHasNext() {
        return this.hasNext;
    }

    public void setHasNext(int hasNext2) {
        this.hasNext = hasNext2;
    }

    public Set<String> getImgSet() {
        return this.imgSet;
    }

    public void setImgSet(Set<String> imgSet2) {
        this.imgSet = imgSet2;
    }
}
