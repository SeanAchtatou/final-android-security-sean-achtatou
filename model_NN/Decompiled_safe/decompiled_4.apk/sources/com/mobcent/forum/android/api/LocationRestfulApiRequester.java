package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.constant.LocationConstant;
import java.util.HashMap;

public class LocationRestfulApiRequester extends BaseRestfulApiRequester implements LocationConstant {
    public static String saveLocation(Context context, double longitude, double latitude, String location) {
        HashMap<String, String> params = new HashMap<>();
        params.put(LocationConstant.ACTION, LocationConstant.LOGIN);
        params.put("longitude", new StringBuilder(String.valueOf(longitude)).toString());
        params.put("latitude", new StringBuilder(String.valueOf(latitude)).toString());
        params.put("location", location);
        return doPostRequest("lbs/saveLoc.do", params, context);
    }
}
