package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.constant.MentionFriendConstant;
import java.util.HashMap;

public class MentionFriendRestfulApiRequester extends BaseRestfulApiRequester implements MentionFriendConstant {
    public static String getForumMentionFriend(Context context, long userId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        return doPostRequest("user/getFriends.do", params, context);
    }

    public static String getOpenPlatformMentionFriend(Context context, long platformId, long userId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("platformId", new StringBuilder(String.valueOf(platformId)).toString());
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        return doPostRequest("friend/getFriendsByOP.do", params, context);
    }
}
