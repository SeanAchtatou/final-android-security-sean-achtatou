package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.BaseRestfulApiConstant;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import org.json.JSONObject;

public class BaseJsonHelper implements BaseReturnCodeConstant {
    public static String formJsonRS(String jsonStr) {
        String errorCode;
        if (jsonStr.trim().equals("{}")) {
            return BaseReturnCodeConstant.ERROR_JSON_WRONG;
        }
        if (jsonStr.equals("connection_fail") || jsonStr.equals("upload_images_fail")) {
            return jsonStr;
        }
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            int rs = jsonObj.optInt("rs");
            if (rs == 1) {
                errorCode = null;
            } else if (rs == 0) {
                errorCode = jsonObj.optString(BaseRestfulApiConstant.ERROR_CODE);
            } else {
                errorCode = BaseReturnCodeConstant.ERROR_OTHER_WRONG;
            }
        } catch (Exception e) {
            errorCode = BaseReturnCodeConstant.ERROR_JSON_WRONG;
        }
        return errorCode;
    }
}
