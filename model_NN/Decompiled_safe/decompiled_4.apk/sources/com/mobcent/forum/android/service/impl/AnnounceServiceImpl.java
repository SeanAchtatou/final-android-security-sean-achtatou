package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.AnnounceApiRequester;
import com.mobcent.forum.android.model.AnnoModel;
import com.mobcent.forum.android.service.AnnounceService;
import com.mobcent.forum.android.service.impl.helper.AnnounceServiceImplHelper;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class AnnounceServiceImpl implements AnnounceService {
    private Context context;

    public AnnounceServiceImpl(Context context2) {
        this.context = context2;
    }

    public List<AnnoModel> getAnnoList(int page, int pageSize) {
        new ArrayList();
        String jsonStr = AnnounceApiRequester.getAnnoList(this.context, page, pageSize);
        List<AnnoModel> annoList = AnnounceServiceImplHelper.getAnnounceList(jsonStr);
        if (annoList == null || annoList.size() == 0) {
            if (annoList == null) {
                annoList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                AnnoModel annoModel = new AnnoModel();
                annoModel.setErrorCode(errorCode);
                annoList.add(annoModel);
            }
        }
        return annoList;
    }

    public String verifyAnno(int verify, int announceId) {
        return BaseJsonHelper.formJsonRS(AnnounceApiRequester.verifyAnno(this.context, verify, announceId));
    }
}
