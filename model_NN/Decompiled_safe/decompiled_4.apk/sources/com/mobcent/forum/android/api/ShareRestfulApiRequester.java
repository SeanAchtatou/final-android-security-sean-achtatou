package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.constant.PostsApiConstant;
import java.util.HashMap;

public class ShareRestfulApiRequester extends BaseRestfulApiRequester implements PostsApiConstant {
    public static String addShareNum(Context context, long boardId, long topicId) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        return doPostRequest("forum/shareTopic.do", params, context);
    }
}
