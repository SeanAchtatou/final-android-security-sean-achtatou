package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.UserManageApiRequester;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.UserManageService;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.service.impl.helper.UserManageServiceImplHelper;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserManageServiceImpl implements UserManageService {
    private Context context;

    public UserManageServiceImpl(Context context2) {
        this.context = context2;
    }

    public String bannedShielded(int type, long currentUserId, long bannedShieldedUserId, int boardId, Date bannedDate, String reason) {
        return BaseJsonHelper.formJsonRS(UserManageApiRequester.bannedShielded(this.context, type, currentUserId, bannedShieldedUserId, boardId, bannedDate, reason));
    }

    public String bannedShielded(int type, long currentUserId, long bannedShieldedUserId, List<BoardModel> selectBoardList, Date bannedDate, String reason) {
        return BaseJsonHelper.formJsonRS(UserManageApiRequester.bannedShielded(this.context, type, currentUserId, bannedShieldedUserId, selectBoardList, bannedDate, reason));
    }

    public String cancelBannedShielded(int type, long bannedShieldedUserId, int boardId) {
        return BaseJsonHelper.formJsonRS(UserManageApiRequester.cancelBannedShielded(this.context, type, bannedShieldedUserId, boardId));
    }

    public List<UserInfoModel> getBannedShieldedUser(int type, int boardId, int page, int pageSize) {
        new ArrayList();
        String jsonStr = UserManageApiRequester.getBannedShieldedUser(this.context, type, boardId, page, pageSize);
        List<UserInfoModel> userInfoList = UserManageServiceImplHelper.getUserInfoList(jsonStr);
        if (userInfoList == null || userInfoList.size() == 0) {
            if (userInfoList == null) {
                userInfoList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                UserInfoModel userInfoModel = new UserInfoModel();
                userInfoModel.setErrorCode(errorCode);
                userInfoList.add(userInfoModel);
            }
        }
        return userInfoList;
    }

    public List<UserInfoModel> searchBannedShieldedUser(int type, int boardId, String searchKeyWord, int page, int pageSize) {
        new ArrayList();
        String jsonStr = UserManageApiRequester.searchBannedShieldedUser(this.context, type, boardId, page, pageSize);
        List<UserInfoModel> userInfoList = UserManageServiceImplHelper.getUserInfoList(jsonStr);
        if (userInfoList == null || userInfoList.size() == 0) {
            if (userInfoList == null) {
                userInfoList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                UserInfoModel userInfoModel = new UserInfoModel();
                userInfoModel.setErrorCode(errorCode);
                userInfoList.add(userInfoModel);
            }
        }
        return userInfoList;
    }
}
