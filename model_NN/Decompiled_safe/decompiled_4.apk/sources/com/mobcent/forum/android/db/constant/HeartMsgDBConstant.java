package com.mobcent.forum.android.db.constant;

public interface HeartMsgDBConstant {
    public static final String COLUMN_HEART_BEAT_ID = "heatBeatId";
    public static final String COLUMN_HEART_BEAT_JSON = "jsonStr";
    public static final String COLUMN_HEART_BEAT_USER_ID = "userId";
    public static final String COLUMN_MESSAGE_ID = "msgId";
    public static final String COLUMN_MESSAGE_JSON = "jsonStr";
    public static final String COLUMN_MESSAGE_USER_ID = "userId";
    public static final String CREATE_TABLE_HEART_BEAT = "CREATE TABLE IF NOT EXISTS t_heart_beat(heatBeatId integer primary key autoincrement,userId LONG,jsonStr TEXT);";
    public static final String CREATE_TABLE_MESSAGE = "CREATE TABLE IF NOT EXISTS t_message(msgId integer primary key autoincrement,userId LONG,jsonStr TEXT);";
    public static final String DELETE_HEART_BEAT_WHERE_CASE = "userId =?";
    public static final String SELECT_ALL_HEART_BEAT = "SELECT * FROM t_heart_beat";
    public static final String SELECT_SOME_HEART_BEAT = "SELECT * FROM t_heart_beat WHERE userId=? ORDER BY heatBeatId DESC";
    public static final String SELECT_TABLE_MESSAGE = "SELECT * FROM t_message WHERE userId=? ORDER BY msgId DESC LIMIT ?,?";
    public static final String TABLE_HEART_BEAT = "t_heart_beat";
    public static final String TABLE_MESSAGE = "t_message";
}
