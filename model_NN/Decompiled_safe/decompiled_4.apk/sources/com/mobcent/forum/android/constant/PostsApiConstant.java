package com.mobcent.forum.android.constant;

public interface PostsApiConstant extends BaseRestfulApiConstant {
    public static final String ANNOUNCE_CONTENT = "announce_content";
    public static final String ANNOUNCE_DETAIL = "announce_detail";
    public static final String ANNO_ID = "announce_id";
    public static final String ANNO_LIST = "anno_list";
    public static final String ANNO_START_DATE = "start_date";
    public static final String AUTHOR = "author";
    public static final String BOARD_ID = "board_id";
    public static final String BOARD_NAME = "board_name";
    public static final String CONTENT = "content";
    public static final String CONTENT_ALL = "content_all";
    public static final int CONTENT_AUDIO = 5;
    public static final int CONTENT_IMG = 1;
    public static final int CONTENT_TEXT = 0;
    public static final String CREATE_DATE = "create_date";
    public static final String CREATE_TIME = "create_time";
    public static final String DEADLINE = "deadline";
    public static final String DESC = "desc";
    public static final String DISTANCE = "distance";
    public static final String ESSENCE = "essence";
    public static final String FLAT_UD = "platId";
    public static final String FORWARD = "forward";
    public static final String FORWARD_TIME = "forward_time";
    public static final String FRIEND_LIST = "friendList";
    public static final String FROM = "from";
    public static final String FROM_DATE = "fromDate";
    public static final String HAS_NEXT = "has_next";
    public static final String HITS = "hits";
    public static final String HOT = "hot";
    public static final String ICON = "icon";
    public static final String INFOR = "infor";
    public static final String ISREPLY = "isReply";
    public static final int IS_BOARD_ID = 0;
    public static final int IS_ESSENCE_TOPIC = 1;
    public static final String IS_FAVOR = "is_favor";
    public static final String IS_GAG = "is_gag";
    public static final int IS_HOT_TOPIC = 1;
    public static final String IS_QUOTE = "is_quote";
    public static final String IS_READ = "is_read";
    public static final String IS_REPLY = "is_reply";
    public static final String IS_SHIELD = "is_shield";
    public static final int IS_TOP_TOPIC = 1;
    public static final int IS_UNESSENCE_TOPIC = 0;
    public static final int IS_UNHOT_TOPIC = 0;
    public static final int IS_UNTOP_TOPIC = 0;
    public static final int IS_USER_ID = 0;
    public static final String IS_VISIBLE = "isVisible";
    public static final String ITEM_NAME = "itemName";
    public static final String LAST_REPLY_DATE = "last_reply_date";
    public static final String LIST = "list";
    public static final String MODEL_TYPE = "model_type";
    public static final String NICK_NAME = "nickname";
    public static final String PARAM_ANNOUNCE_ID = "announceId";
    public static final String PARAM_BASE_URL = "baseUrl";
    public static final String PARAM_BOARD_ID = "boardId";
    public static final String PARAM_FORUM_ID = "forum_id";
    public static final String PARAM_IS_QUOTE = "isQuote";
    public static final String PARAM_POI = "poi";
    public static final String PARAM_RCONTENT = "rContent";
    public static final String PARAM_RELATIONAL_CONTENT_IDS = "relationalContentIds";
    public static final String PARAM_RELPY_REMIND_IDS = "relpyRemindIds";
    public static final String PARAM_REPLY_POSTS_ID = "replyPostsId";
    public static final String PARAM_RTITLE = "rTitle";
    public static final String PARAM_TOPIC_ID = "topicId";
    public static final String PARAM_TO_REPLYID = "toReplyId";
    public static final String PARAM_USER_ID = "userId";
    public static final String PIC_PATH = "pic_path";
    public static final String POI_LIST = "pois";
    public static final String POI_TOPIC = "topic";
    public static final String POLL = "poll";
    public static final String POLLITEM = "pollItem";
    public static final String POLLS_ID = "poll_id";
    public static final String POLL_ID = "pollItemId";
    public static final String POLL_INFO = "poll_info";
    public static final String POLL_ITEM_ID = "itemId";
    public static final String POLL_ITEM_LIST = "poll_item_list";
    public static final String POLL_LATITUDE = "latitude";
    public static final String POLL_LIST = "vote_rs";
    public static final String POLL_LOCATION = "location";
    public static final String POLL_LONGITUDE = "longitude";
    public static final String POLL_NAME_ID = "poll_item_id";
    public static final String POLL_STATUS = "poll_status";
    public static final String POLL_TOTAL_NUM = "totalNum";
    public static final int POLL_TYPE = 1;
    public static final String POSTS_DATE = "posts_date";
    public static final int POSTS_NOTICE_READED = 1;
    public static final int POSTS_NOTICE_REPLYED = 1;
    public static final int POSTS_NOTICE_UNREAD = 0;
    public static final int POSTS_NOTICE_UNREPLY = 0;
    public static final int POSTS_STATUS_CLOSE = 2;
    public static final int POSTS_STATUS_DELETED = 0;
    public static final int POSTS_STATUS_NORMAL = 1;
    public static final int POSTS_TYPE_REPLY = 0;
    public static final int POSTS_TYPE_TOPIC = 1;
    public static final String QUOTE_CONTENT = "quote_content";
    public static final String QUOTE_USER_NAME = "quote_user_name";
    public static final String RATIO = "ratio";
    public static final String RELATIONAL_CONTENT_ID = "relational_content_id";
    public static final String REPLYES = "replies";
    public static final String REPLY_AT_NICK_NAME = "reply_nickname";
    public static final String REPLY_CONTENT = "reply_content";
    public static final String REPLY_DATE = "replied_date";
    public static final String REPLY_ID = "reply_id";
    public static final String REPLY_NAME = "reply_name";
    public static final String REPLY_NICK_NAME = "reply_nick_name";
    public static final String REPLY_POSTS_ID = "reply_posts_id";
    public static final String REPLY_REMIND_ID = "reply_remind_id";
    public static final String REPLY_USER_ID = "reply_id";
    public static final String ROLE_NUM = "role_num";
    public static final String RS = "rs";
    public static final String SORT = "sortby";
    public static final String SORT_VALUE = "publish";
    public static final String SOUND_PATH = "sound_path";
    public static final String STATUS = "status";
    public static final String THUMBNAIL = "thumbnail";
    public static final String TITLE = "title";
    public static final String TITlE = "title";
    public static final String TOP = "top";
    public static final String TOPIC = "topic";
    public static final String TOPIC_CONTENT = "topic_content";
    public static final int TOPIC_FAVORED = 1;
    public static final String TOPIC_ID = "topic_id";
    public static final String TOPIC_NAME = "name";
    public static final String TOPIC_SUBJECT = "topic_subject";
    public static final String TOPIC_TITLE = "topic_title";
    public static final String TOPIC_TYPE = "type";
    public static final int TOPIC_UNFAVOR = 0;
    public static final String TOTAL_NUM = "total_num";
    public static final String TO_DATE = "toDate";
    public static final String TYPE = "type";
    public static final int UPLOAD_GENERAL = 0;
    public static final String UPLOAD_TYPE = "upload_type";
    public static final int UPLOAD_VIDEO = 3;
    public static final int UPLOAD_VOICE = 2;
    public static final String USER_ID = "user_id";
    public static final String USER_LEVEL = "level";
    public static final String USER_NICK_NAME = "user_nick_name";
    public static final String VISIBLE = "visible";
    public static final int VISIBLE_MODERATOR = 3;
    public static final int VISIBLE_NORMAL = 1;
    public static final int VISIBLE_REPLY = 2;
}
