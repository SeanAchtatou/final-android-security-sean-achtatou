package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.constant.AnnoConstant;
import java.util.HashMap;

public class AnnounceApiRequester extends BaseRestfulApiRequester implements AnnoConstant {
    public static String getAnnoList(Context context, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("announce/getVerifyAnnounceList.do", params, context);
    }

    public static String verifyAnno(Context context, int verify, int announceId) {
        HashMap<String, String> params = new HashMap<>();
        params.put(AnnoConstant.VERIFY, new StringBuilder(String.valueOf(verify)).toString());
        params.put("announceId", new StringBuilder(String.valueOf(announceId)).toString());
        return doPostRequest("announce/verifyAnnounce.do", params, context);
    }
}
