package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.ReportConstant;
import com.mobcent.forum.android.model.InformantModel;
import com.mobcent.forum.android.model.ReportModel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ReportServiceImplHelper extends BaseJsonHelper implements ReportConstant {
    /* JADX INFO: Multiple debug info for r11v12 java.util.List<com.mobcent.forum.android.model.ReportModel>: [D('firstReportModel' com.mobcent.forum.android.model.ReportModel), D('reportlList' java.util.List<com.mobcent.forum.android.model.ReportModel>)] */
    /* JADX INFO: Multiple debug info for r11v17 org.json.JSONArray: [D('object' org.json.JSONObject), D('array' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r11v18 int: [D('array' org.json.JSONArray), D('i' int)] */
    public static List<ReportModel> getReportTopicList(String jsonStr) {
        try {
            List<ReportModel> reportlList = new ArrayList<>();
            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                if (jsonObject.optInt("rs") == 1) {
                    JSONArray jsonArray = jsonObject.optJSONArray("list");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ReportModel reportModel = new ReportModel();
                        JSONObject object = jsonArray.optJSONObject(i);
                        reportModel.setContent(object.optString(ReportConstant.CONTENT));
                        reportModel.setCount(object.optInt(ReportConstant.COUNT));
                        reportModel.setProcessed(object.optInt(ReportConstant.PROCESSED));
                        reportModel.setNickName(object.optString("nickname"));
                        reportModel.setPostId(object.optLong(ReportConstant.POST_ID));
                        reportModel.setReportId(object.optInt(ReportConstant.REPORT_ID));
                        reportModel.setReprtTime(object.optLong(ReportConstant.REPORT_TIME));
                        reportModel.setStatus(object.optInt("status"));
                        reportModel.setGender(object.optInt("gender"));
                        reportModel.setTitle(object.optString("title"));
                        reportModel.setType(object.optInt("type"));
                        reportModel.setBoardName(object.optString("board_name"));
                        reportModel.setUserId(object.optLong("user_id"));
                        reportModel.setTopicId(object.optLong("topic_id"));
                        List<InformantModel> informantList = new ArrayList<>();
                        JSONArray array = object.optJSONArray(ReportConstant.INFORMANTS);
                        for (int j = 0; j < array.length(); j++) {
                            InformantModel model = new InformantModel();
                            JSONObject object2 = array.optJSONObject(j);
                            model.setNickName(object2.optString("nickname"));
                            model.setReason(object2.optString("reason"));
                            model.setReportTime(object2.optLong(ReportConstant.REPORT_TIME));
                            model.setUserId(object2.optLong("user_id"));
                            informantList.add(model);
                        }
                        reportModel.setInformantList(informantList);
                        reportlList.add(reportModel);
                    }
                }
                if (reportlList.size() <= 0) {
                    return reportlList;
                }
                ReportModel firstReportModel = (ReportModel) reportlList.get(0);
                firstReportModel.setRs(jsonObject.optInt("rs"));
                firstReportModel.setHasNext(jsonObject.optInt("has_next"));
                firstReportModel.setTotalNum(jsonObject.optInt("total_num"));
                reportlList.set(0, firstReportModel);
                return reportlList;
            } catch (Exception e) {
                e = e;
                e.printStackTrace();
                return null;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return null;
        }
    }
}
