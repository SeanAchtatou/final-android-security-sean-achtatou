package com.mobcent.forum.android.model;

import java.io.Serializable;

public class TopicDraftModel implements Serializable {
    private static final long serialVersionUID = 3735826464935778778L;
    private long boardId;
    private String boardName;
    private String content;
    private long id;
    private String title;
    private String voteString = "";

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public long getBoardId() {
        return this.boardId;
    }

    public void setBoardId(long boardId2) {
        this.boardId = boardId2;
    }

    public String getBoardName() {
        return this.boardName;
    }

    public void setBoardName(String boardName2) {
        this.boardName = boardName2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getVoteString() {
        return this.voteString;
    }

    public void setVoteString(String voteString2) {
        this.voteString = voteString2;
    }
}
