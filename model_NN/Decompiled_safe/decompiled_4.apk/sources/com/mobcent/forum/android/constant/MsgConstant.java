package com.mobcent.forum.android.constant;

public interface MsgConstant {
    public static final String BLACK_STATE = "state";
    public static final String BLACK_TO_USER_ID = "bUserId";
    public static final String BLACK_USER_ID = "userId";
    public static final String GET_CONTENT = "content";
    public static final String GET_CREATEED_DATE = "created_date";
    public static final String GET_LIST = "list";
    public static final String GET_MSG_RELATION_ID = "msg_relation_id";
    public static final String GET_NICK_NAME = "nick_name";
    public static final String GET_TO_NICK_NAME = "to_nick_name";
    public static final String GET_TO_USER_ID = "to_user_id";
    public static final String GET_USER_ID = "user_id";
    public static final String SEND_CONTENT = "content";
    public static final String SEND_FROM_USER_ID = "fromUserId";
    public static final String SEND_MESSAGE_RELATION_IDS = "messageRelationIds";
    public static final String SEND_TO_USER_ID = "toUserId";
    public static final String SEND_UN_MESSAGE_RELATION_ID = "unMessageRelationId";
    public static final String SEND_USER_ID = "userId";
    public static final String TIME = "time";
    public static final String TYPE = "type";
}
