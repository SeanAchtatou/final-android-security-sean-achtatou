package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.LocationConstant;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class LocationServiceImplHelper extends BaseJsonHelper implements LocationConstant {
    public static List<String> parsePoi(String json) {
        List<String> list = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONObject(json).optJSONArray(LocationConstant.POI);
            int j = jsonArray.length();
            for (int i = 0; i < j; i++) {
                list.add(((JSONObject) jsonArray.opt(i)).optString("name"));
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }
}
