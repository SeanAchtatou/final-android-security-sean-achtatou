package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.AboutRestfulApiRequester;
import com.mobcent.forum.android.model.AboutModel;
import com.mobcent.forum.android.service.AboutService;
import com.mobcent.forum.android.service.impl.helper.AboutServiceImplHelper;

public class AboutServiceImpl implements AboutService {
    private Context context;

    public AboutServiceImpl(Context context2) {
        this.context = context2;
    }

    public AboutModel getAoutInfo() {
        try {
            return AboutServiceImplHelper.getAboutInfo(AboutRestfulApiRequester.getAboutInfo(this.context));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
