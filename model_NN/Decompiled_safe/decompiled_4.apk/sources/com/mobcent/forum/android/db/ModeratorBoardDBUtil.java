package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.forum.android.db.constant.ModeratorBoardDBConstant;
import com.mobcent.forum.android.util.DateUtil;

public class ModeratorBoardDBUtil extends BaseDBUtil implements ModeratorBoardDBConstant {
    private static ModeratorBoardDBUtil moderatorBoardDBUtil;

    public ModeratorBoardDBUtil(Context ctx) {
        super(ctx);
    }

    public static ModeratorBoardDBUtil getInstance(Context context) {
        if (moderatorBoardDBUtil == null) {
            moderatorBoardDBUtil = new ModeratorBoardDBUtil(context);
        }
        return moderatorBoardDBUtil;
    }

    public String getModeratorBoardJsonString(long userId) {
        String jsonStr = null;
        Cursor cursor = null;
        try {
            openReadableDB();
            Cursor cursor2 = this.readableDatabase.rawQuery(ModeratorBoardDBConstant.SQL_SELECT_MODERATOR_BOARD, null);
            cursor = this.readableDatabase.query(ModeratorBoardDBConstant.TABLE_MODERATOR_BOARD, null, "id=" + userId, null, null, null, null);
            while (cursor.moveToNext()) {
                jsonStr = cursor.getString(1);
            }
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            jsonStr = null;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
            throw th;
        }
        return jsonStr;
    }

    public boolean updateModeratorBoardString(String jsonStr, long userId) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("id", Long.valueOf(userId));
            values.put("jsonStr", jsonStr);
            values.put("update_time", Long.valueOf(DateUtil.getCurrentTime()));
            if (!isRowExisted(this.writableDatabase, ModeratorBoardDBConstant.TABLE_MODERATOR_BOARD, "id", userId)) {
                this.writableDatabase.insertOrThrow(ModeratorBoardDBConstant.TABLE_MODERATOR_BOARD, null, values);
            } else {
                this.writableDatabase.update(ModeratorBoardDBConstant.TABLE_MODERATOR_BOARD, values, "id=" + userId, null);
            }
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean delteModeratorBoardList() {
        return removeAllEntries(ModeratorBoardDBConstant.TABLE_MODERATOR_BOARD);
    }
}
