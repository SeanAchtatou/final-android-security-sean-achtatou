package com.mobcent.forum.android.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
    public static boolean isEmpty(String s) {
        if (s == null || s.trim().equals("")) {
            return true;
        }
        return false;
    }

    public static boolean isEmail(String strEmail) {
        return Pattern.compile("^([a-z0-9A-Z_]+[-\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$").matcher(strEmail).matches();
    }

    public static boolean isNumeric(String str) {
        return Pattern.compile("[0-9]*").matcher(str).matches();
    }

    public static boolean isPwdMatchRule(String pwd) {
        if (pwd == null) {
            return false;
        }
        return Pattern.compile("[A-Za-z0-9]+").matcher(pwd).matches();
    }

    public static int getChineseCount(String str) {
        int count = 0;
        Matcher m = Pattern.compile("[\\u4e00-\\u9fa5]").matcher(str);
        while (m.find()) {
            for (int i = 0; i <= m.groupCount(); i++) {
                count++;
            }
        }
        return count;
    }

    public static boolean isChinese(String str) {
        return Pattern.matches("[一-龥]", str);
    }

    public static boolean isNameMatchRule(String name) {
        if (name == null) {
            return false;
        }
        return Pattern.compile("[A-Za-z0-9_]+").matcher(name).matches();
    }

    public static boolean isNickNameMatchRule(String nickName) {
        int len = nickName.length();
        for (int i = 0; i < len; i++) {
            String str = nickName.substring(i, i + 1);
            if (!isChinese(str) && !isNameMatchRule(str)) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkNickNameMinLen(String str, int len) {
        int chinaCount = getChineseCount(str);
        if ((chinaCount * 2) + (str.length() - chinaCount) < len) {
            return false;
        }
        return true;
    }

    public static boolean checkNickNameMaxLen(String str, int len) {
        int chinaCount = getChineseCount(str);
        if ((chinaCount * 2) + (str.length() - chinaCount) > len) {
            return false;
        }
        return true;
    }

    public static String getExtensionName(String filename) {
        int dot;
        if (filename == null || filename.length() <= 0 || (dot = filename.lastIndexOf(46)) <= -1 || dot >= filename.length() - 1) {
            return filename;
        }
        return filename.substring(dot + 1);
    }

    public static String getMD5Str(String url) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(url.getBytes());
            return new BigInteger(digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            return url;
        }
    }

    public static List<String> getTokenizer(String str, String split) {
        if (str == null || str.isEmpty()) {
            return new ArrayList();
        }
        List<String> strs = new ArrayList<>();
        StringTokenizer strToken = new StringTokenizer(str, split);
        while (strToken.hasMoreTokens()) {
            strs.add(strToken.nextToken());
        }
        return strs;
    }

    public static String formatImgUrl(String url, String resolution) {
        if (url == null) {
            return "";
        }
        if (resolution == null) {
            return url;
        }
        return url.contains("xgsize") ? url.replace("xgsize", resolution) : url;
    }
}
