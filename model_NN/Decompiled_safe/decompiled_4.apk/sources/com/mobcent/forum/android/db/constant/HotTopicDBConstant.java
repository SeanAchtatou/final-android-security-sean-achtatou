package com.mobcent.forum.android.db.constant;

public interface HotTopicDBConstant extends BaseDBConstant {
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_JSON_STR = "jsonStr";
    public static final String COLUMN_UPDATE_TIME = "update_time";
    public static final String SQL_CREATE_TABLE_HOT_TOPIC = "CREATE TABLE IF NOT EXISTS hot_topic(id LONG PRIMARY KEY,jsonStr TEXT , update_time LONG );";
    public static final String SQL_SELECT_HOT_TOPTIC = "SELECT * FROM hot_topic";
    public static final String TABLE_HOT_TOPIC = "hot_topic";
}
