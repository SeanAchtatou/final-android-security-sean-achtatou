package com.mobcent.forum.android.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.mobcent.forum.android.service.impl.FileTransferServiceImpl;

public class ImageLoaderUtil {
    private ImageLoaderUtil(Context context) {
    }

    public static String getPathName(String url) {
        return url.substring(url.lastIndexOf("/") + 1);
    }

    public static String getExtentionName(String url) {
        int dot;
        if (url == null || url.length() <= 0 || (dot = url.lastIndexOf(46)) <= -1 || dot >= url.length() - 1) {
            return null;
        }
        return url.substring(dot + 1);
    }

    public Bitmap.CompressFormat getImageType(String imageType) {
        if (imageType.equalsIgnoreCase("png")) {
            return Bitmap.CompressFormat.PNG;
        }
        if (imageType.equalsIgnoreCase("jpg")) {
            return Bitmap.CompressFormat.JPEG;
        }
        return Bitmap.CompressFormat.PNG;
    }

    public static boolean downloadGif(String urlString, String size, Context context) {
        if (urlString == null || size == null || urlString.equals("")) {
            return false;
        }
        if (urlString.indexOf("xgsize") > -1) {
            urlString = urlString.replace("xgsize", size);
        }
        String imagePath = MCLibIOUtil.getImageCachePath(context);
        if (!MCLibIOUtil.isDirExist(imagePath) && !MCLibIOUtil.makeDirs(imagePath)) {
            return false;
        }
        String fileName = getPathName(urlString);
        if (MCLibIOUtil.isFileExist(String.valueOf(imagePath) + fileName)) {
            return true;
        }
        return MCLibIOUtil.saveFile(new FileTransferServiceImpl(context).getImageStream(urlString), fileName, imagePath);
    }

    public static Bitmap getBitmap(String filePath, int sampleSize) {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = sampleSize;
        return BitmapFactory.decodeFile(filePath, bitmapOptions);
    }
}
