package com.mobcent.forum.android.model;

import java.util.List;

public class HeartMsgModel extends BaseModel {
    private static final long serialVersionUID = -6525988241097258223L;
    private String content;
    private long createDate;
    private int dbSaveNum;
    private long formUserId;
    private String fromUserIcon;
    private String fromUserNickname;
    private int hbTime;
    private long msgId;
    private int msgTotalNum;
    private String pushMessageJson;
    private List<PushMessageModel> pushMsgList;
    private int pushMsgNum;
    private int relationalNoticeNum;
    private int replyNoticeNum;
    private int sendStatus;
    private SoundModel soundModel;
    private String toUserIcon;
    private long toUserId;
    private String toUserNickName;
    private int type;

    public SoundModel getSoundModel() {
        return this.soundModel;
    }

    public void setSoundModel(SoundModel soundModel2) {
        this.soundModel = soundModel2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public int getReplyNoticeNum() {
        return this.replyNoticeNum;
    }

    public void setReplyNoticeNum(int replyNoticeNum2) {
        this.replyNoticeNum = replyNoticeNum2;
    }

    public int getMsgTotalNum() {
        return this.msgTotalNum;
    }

    public void setMsgTotalNum(int msgTotalNum2) {
        this.msgTotalNum = msgTotalNum2;
    }

    public int getHbTime() {
        return this.hbTime;
    }

    public void setHbTime(int hbTime2) {
        this.hbTime = hbTime2;
    }

    public long getFormUserId() {
        return this.formUserId;
    }

    public void setFormUserId(long formUserId2) {
        this.formUserId = formUserId2;
    }

    public String getFromUserNickname() {
        return this.fromUserNickname;
    }

    public void setFromUserNickname(String fromUserNickname2) {
        this.fromUserNickname = fromUserNickname2;
    }

    public String getFromUserIcon() {
        return this.fromUserIcon;
    }

    public void setFromUserIcon(String fromUserIcon2) {
        this.fromUserIcon = fromUserIcon2;
    }

    public long getToUserId() {
        return this.toUserId;
    }

    public void setToUserId(long toUserId2) {
        this.toUserId = toUserId2;
    }

    public String getToUserNickName() {
        return this.toUserNickName;
    }

    public void setToUserNickName(String toUserNickName2) {
        this.toUserNickName = toUserNickName2;
    }

    public String getToUserIcon() {
        return this.toUserIcon;
    }

    public void setToUserIcon(String toUserIcon2) {
        this.toUserIcon = toUserIcon2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public long getCreateDate() {
        return this.createDate;
    }

    public void setCreateDate(long createDate2) {
        this.createDate = createDate2;
    }

    public long getMsgId() {
        return this.msgId;
    }

    public void setMsgId(long msgId2) {
        this.msgId = msgId2;
    }

    public int getDBSaveNum() {
        return this.dbSaveNum;
    }

    public void setDBSaveNum(int dbSaveNum2) {
        this.dbSaveNum = dbSaveNum2;
    }

    public int getSendStatus() {
        return this.sendStatus;
    }

    public void setSendStatus(int sendStatus2) {
        this.sendStatus = sendStatus2;
    }

    public int getRelationalNoticeNum() {
        return this.relationalNoticeNum;
    }

    public void setRelationalNoticeNum(int relationalNoticeNum2) {
        this.relationalNoticeNum = relationalNoticeNum2;
    }

    public List<PushMessageModel> getPushMsgList() {
        return this.pushMsgList;
    }

    public void setPushMsgList(List<PushMessageModel> pushMsgList2) {
        this.pushMsgList = pushMsgList2;
    }

    public int getPushMsgNum() {
        return this.pushMsgNum;
    }

    public void setPushMsgNum(int pushMsgNum2) {
        this.pushMsgNum = pushMsgNum2;
    }

    public String getPushMessageJson() {
        return this.pushMessageJson;
    }

    public void setPushMessageJson(String pushMessageJson2) {
        this.pushMessageJson = pushMessageJson2;
    }
}
