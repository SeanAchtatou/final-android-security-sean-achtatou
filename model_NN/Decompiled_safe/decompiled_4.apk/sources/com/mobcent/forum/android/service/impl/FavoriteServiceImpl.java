package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.FavoriteRestfulApiRequester;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.FavoriteService;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.service.impl.helper.PostsServiceImplHelper;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class FavoriteServiceImpl implements FavoriteService {
    private Context context;

    public FavoriteServiceImpl(Context context2) {
        this.context = context2;
    }

    public List<TopicModel> getUserFavoriteList(int page, int pageSize) {
        new ArrayList();
        String jsonStr = FavoriteRestfulApiRequester.getMyFavoriteTopicList(this.context, page, pageSize);
        List<TopicModel> topicList = PostsServiceImplHelper.getFavoriteTopicList(jsonStr);
        if (topicList == null || topicList.isEmpty()) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topic = new TopicModel();
                topic.setErrorCode(errorCode);
                topicList.add(topic);
            }
        }
        return topicList;
    }

    public String addFavoriteTopic(long topicId, long boardId) {
        return BaseJsonHelper.formJsonRS(FavoriteRestfulApiRequester.addFavoriteTopic(this.context, topicId, boardId));
    }

    public String delFavoriteTopic(long topicId, long boardId) {
        return BaseJsonHelper.formJsonRS(FavoriteRestfulApiRequester.delFavoriteTopic(this.context, topicId, boardId));
    }
}
