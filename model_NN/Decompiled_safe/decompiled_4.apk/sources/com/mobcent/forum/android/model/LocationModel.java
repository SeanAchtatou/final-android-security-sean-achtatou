package com.mobcent.forum.android.model;

import java.io.Serializable;

public class LocationModel implements Serializable {
    private static final long serialVersionUID = -2824079131788643543L;
    private String addrStr;
    private double latitude;
    private double longitude;
    private String str;
    private long userId;

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude2) {
        this.latitude = latitude2;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude2) {
        this.longitude = longitude2;
    }

    public String getAddrStr() {
        return this.addrStr;
    }

    public void setAddrStr(String addrStr2) {
        this.addrStr = addrStr2;
    }

    public String getStr() {
        return this.str;
    }

    public void setStr(String str2) {
        this.str = str2;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
    }
}
