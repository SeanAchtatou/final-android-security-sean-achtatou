package com.mobcent.forum.android.db.constant;

public interface PollDBConstant {
    public static final String COLUMN_DEADTIME = "deadtime";
    public static final String COLUMN_POLL_TIME = "pollTime";
    public static final String COLUMN_TOPIC_ID = "topicId";
    public static final String COLUMN_USER_ID = "userId";
    public static final String SQL_CREATE_TABLE_POLL = "CREATE TABLE IF NOT EXISTS pollPosts(userId LONG,topicId LONG,pollTime LONG,deadtime LONG,PRIMARY KEY(userId,topicId));";
    public static final String TABLE_POLL_POSTS = "pollPosts";
}
