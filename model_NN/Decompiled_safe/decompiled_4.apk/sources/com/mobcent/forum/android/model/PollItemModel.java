package com.mobcent.forum.android.model;

import java.text.DecimalFormat;

public class PollItemModel extends BaseModel {
    private static final long serialVersionUID = 1;
    private int num;
    private String percent;
    private int pollItemId;
    private String pollName;
    private double ratio;
    private int totalNum;

    public String getPollName() {
        return this.pollName;
    }

    public void setPollName(String poll_name) {
        this.pollName = poll_name;
    }

    public int getPollItemId() {
        return this.pollItemId;
    }

    public void setPollItemId(int poll_item_id) {
        this.pollItemId = poll_item_id;
    }

    public int getTotalNum() {
        return this.totalNum;
    }

    public void setTotalNum(int total_num) {
        this.totalNum = total_num;
    }

    public double getRatio() {
        return this.ratio;
    }

    public void setRatio(double ratio2) {
        this.ratio = ratio2;
    }

    public String getPercent() {
        this.percent = new DecimalFormat("0.00%").format(this.ratio);
        return this.percent;
    }

    public void setPercent(String percent2) {
        this.percent = percent2;
    }

    public int getNum() {
        return this.num;
    }

    public void setNum(int num2) {
        this.num = num2;
    }
}
