package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import com.mobcent.forum.android.db.constant.TopicDraftDBConstant;
import com.mobcent.forum.android.model.TopicDraftModel;

public class TopicDraftDBUtil extends BaseDBUtil implements TopicDraftDBConstant {
    private static TopicDraftDBUtil topicDraftDBUtil;

    protected TopicDraftDBUtil(Context ctx) {
        super(ctx);
    }

    public static TopicDraftDBUtil getInstance(Context context) {
        if (topicDraftDBUtil == null) {
            topicDraftDBUtil = new TopicDraftDBUtil(context);
        }
        return topicDraftDBUtil;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0080  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.mobcent.forum.android.model.TopicDraftModel getTopicDraft(long r13) {
        /*
            r12 = this;
            r10 = 0
            r8 = 0
            r12.openReadableDB()     // Catch:{ Exception -> 0x006e }
            android.database.sqlite.SQLiteDatabase r0 = r12.readableDatabase     // Catch:{ Exception -> 0x006e }
            java.lang.String r1 = "topic_draft"
            r2 = 0
            java.lang.String r3 = "id = ? "
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x006e }
            r5 = 0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006e }
            java.lang.String r7 = java.lang.String.valueOf(r13)     // Catch:{ Exception -> 0x006e }
            r6.<init>(r7)     // Catch:{ Exception -> 0x006e }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x006e }
            r4[r5] = r6     // Catch:{ Exception -> 0x006e }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x006e }
            r11 = r10
        L_0x0027:
            boolean r0 = r8.moveToNext()     // Catch:{ Exception -> 0x008a, all -> 0x0087 }
            if (r0 != 0) goto L_0x0037
            if (r8 == 0) goto L_0x0032
            r8.close()
        L_0x0032:
            r12.closeReadableDB()
            r10 = r11
        L_0x0036:
            return r10
        L_0x0037:
            com.mobcent.forum.android.model.TopicDraftModel r10 = new com.mobcent.forum.android.model.TopicDraftModel     // Catch:{ Exception -> 0x008a, all -> 0x0087 }
            r10.<init>()     // Catch:{ Exception -> 0x008a, all -> 0x0087 }
            r0 = 0
            long r0 = r8.getLong(r0)     // Catch:{ Exception -> 0x006e }
            r10.setId(r0)     // Catch:{ Exception -> 0x006e }
            r0 = 1
            long r0 = r8.getLong(r0)     // Catch:{ Exception -> 0x006e }
            r10.setBoardId(r0)     // Catch:{ Exception -> 0x006e }
            r0 = 2
            java.lang.String r0 = r8.getString(r0)     // Catch:{ Exception -> 0x006e }
            r10.setBoardName(r0)     // Catch:{ Exception -> 0x006e }
            r0 = 3
            java.lang.String r0 = r8.getString(r0)     // Catch:{ Exception -> 0x006e }
            r10.setTitle(r0)     // Catch:{ Exception -> 0x006e }
            r0 = 4
            java.lang.String r0 = r8.getString(r0)     // Catch:{ Exception -> 0x006e }
            r10.setContent(r0)     // Catch:{ Exception -> 0x006e }
            r0 = 5
            java.lang.String r0 = r8.getString(r0)     // Catch:{ Exception -> 0x006e }
            r10.setVoteString(r0)     // Catch:{ Exception -> 0x006e }
            r11 = r10
            goto L_0x0027
        L_0x006e:
            r0 = move-exception
            r9 = r0
        L_0x0070:
            r10 = 0
            r9.printStackTrace()     // Catch:{ all -> 0x007d }
            if (r8 == 0) goto L_0x0079
            r8.close()
        L_0x0079:
            r12.closeReadableDB()
            goto L_0x0036
        L_0x007d:
            r0 = move-exception
        L_0x007e:
            if (r8 == 0) goto L_0x0083
            r8.close()
        L_0x0083:
            r12.closeReadableDB()
            throw r0
        L_0x0087:
            r0 = move-exception
            r10 = r11
            goto L_0x007e
        L_0x008a:
            r0 = move-exception
            r9 = r0
            r10 = r11
            goto L_0x0070
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.forum.android.db.TopicDraftDBUtil.getTopicDraft(long):com.mobcent.forum.android.model.TopicDraftModel");
    }

    public boolean updateDraft(TopicDraftModel topicDraftModel) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("id", Long.valueOf(topicDraftModel.getId()));
            values.put("boardId", Long.valueOf(topicDraftModel.getBoardId()));
            values.put("boardName", topicDraftModel.getBoardName());
            values.put("title", topicDraftModel.getTitle());
            values.put("content", topicDraftModel.getContent());
            values.put("vote", topicDraftModel.getVoteString());
            if (!isRowExisted(this.writableDatabase, TopicDraftDBConstant.TABLE_TOPIC_DRAFT, "id", topicDraftModel.getId())) {
                this.writableDatabase.insertOrThrow(TopicDraftDBConstant.TABLE_TOPIC_DRAFT, null, values);
            } else {
                this.writableDatabase.update(TopicDraftDBConstant.TABLE_TOPIC_DRAFT, values, "id=" + topicDraftModel.getId(), null);
            }
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean deleteDraft(long id) {
        try {
            openWriteableDB();
            this.writableDatabase.delete(TopicDraftDBConstant.TABLE_TOPIC_DRAFT, "id= ?", new String[]{new StringBuilder(String.valueOf(id)).toString()});
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }
}
