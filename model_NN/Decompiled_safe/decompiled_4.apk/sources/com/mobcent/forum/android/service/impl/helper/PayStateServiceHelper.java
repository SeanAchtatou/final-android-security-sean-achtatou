package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.PayStateConstant;
import com.mobcent.forum.android.model.PayStateModel;
import org.json.JSONException;
import org.json.JSONObject;

public class PayStateServiceHelper implements PayStateConstant {
    public static PayStateModel controll(String jsonStr) {
        PayStateModel payStateModel = null;
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj != null && jsonObj.optInt("rs") == 1) {
                PayStateModel payStateModel2 = new PayStateModel();
                try {
                    payStateModel2.setImgUrl(jsonObj.optString("img_url"));
                    JSONObject dataObj = jsonObj.optJSONObject("data");
                    if (dataObj != null) {
                        payStateModel2.setLoadingPageImage(dataObj.optString("loadingpage_image"));
                        payStateModel2.setWaterMarkImage(dataObj.optString("watermark_image"));
                        payStateModel2.setWeiXinAppKey(dataObj.optString("weixin_appkey"));
                        JSONObject payStateObj = dataObj.optJSONObject(PayStateConstant.PAYSTATE);
                        if (payStateObj != null) {
                            if (payStateObj.optInt("adv") == 1) {
                                payStateModel2.setAdv(true);
                            } else {
                                payStateModel2.setAdv(false);
                            }
                            if (payStateObj.optInt("client_manager") == 1) {
                                payStateModel2.setClientManager(true);
                            } else {
                                payStateModel2.setClientManager(false);
                            }
                            if (payStateObj.optInt("loadingpage") == 1) {
                                payStateModel2.setLoadingPage(true);
                            } else {
                                payStateModel2.setLoadingPage(false);
                            }
                            if (payStateObj.optInt("msg_push") == 1) {
                                payStateModel2.setMsgPush(true);
                            } else {
                                payStateModel2.setMsgPush(false);
                            }
                            if (payStateObj.optInt("powerby") == 1) {
                                payStateModel2.setPowerBy(true);
                            } else {
                                payStateModel2.setPowerBy(false);
                            }
                            if (payStateObj.optInt("share_key") == 1) {
                                payStateModel2.setShareKey(true);
                            } else {
                                payStateModel2.setShareKey(false);
                            }
                            if (payStateObj.optInt("square") == 1) {
                                payStateModel2.setSquare(true);
                            } else {
                                payStateModel2.setSquare(false);
                            }
                            if (payStateObj.optInt("watermark") == 1) {
                                payStateModel2.setWaterMark(true);
                                payStateModel = payStateModel2;
                            } else {
                                payStateModel2.setWaterMark(false);
                            }
                        }
                    }
                    payStateModel = payStateModel2;
                } catch (JSONException e) {
                    JSONException jSONException = e;
                    payStateModel = payStateModel2;
                    return payStateModel;
                }
            }
            return payStateModel;
        } catch (JSONException e2) {
            return payStateModel;
        }
    }
}
