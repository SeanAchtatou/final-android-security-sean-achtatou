package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.constant.ReportConstant;
import java.util.HashMap;

public class ReportRestfulApiRequester extends BaseRestfulApiRequester implements ReportConstant {
    public static String report(Context context, int type, long oid, String reason) {
        HashMap<String, String> params = new HashMap<>();
        params.put("type", new StringBuilder(String.valueOf(type)).toString());
        params.put("oid", new StringBuilder(String.valueOf(oid)).toString());
        params.put("reason", reason);
        return doPostRequest("report/report.do", params, context);
    }

    public static String getReportTopicManage(Context context, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("report/reportTopicList.do", params, context);
    }

    public static String cancelReportTopic(Context context, long reportId) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ReportConstant.REPORT_TOPIC_ID, new StringBuilder(String.valueOf(reportId)).toString());
        return doPostRequest("report/cancelReportTopic.do", params, context);
    }

    public static String getReportUserManage(Context context, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("report/reportUserList.do", params, context);
    }

    public static String cancelReportUser(Context context, long reportId) {
        HashMap<String, String> params = new HashMap<>();
        params.put(ReportConstant.REPORT_TOPIC_ID, new StringBuilder(String.valueOf(reportId)).toString());
        return doPostRequest("report/cancelReportUser.do", params, context);
    }
}
