package com.mobcent.forum.android.service.impl.helper;

import android.content.Context;
import android.content.pm.PackageInfo;
import com.mobcent.forum.android.constant.MoreApplicationConstant;
import com.mobcent.forum.android.model.MoreApplicationModel;
import com.mobcent.forum.android.util.MCLibIOUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class MoreApplicationServiceImplHelper extends BaseJsonHelper implements MoreApplicationConstant {
    /* JADX INFO: Multiple debug info for r8v14 int: [D('applicationModel' com.mobcent.forum.android.model.MoreApplicationModel), D('i' int)] */
    public static List<MoreApplicationModel> parseMoreApplicationListJson(String jsonStr, Context context) {
        List<MoreApplicationModel> moreApplicationList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String url = jsonObject.optString(MoreApplicationConstant.APP_ICON_URL);
            JSONArray jsonArray = new JSONArray(jsonObject.optString(MoreApplicationConstant.APP_LIST));
            List<PackageInfo> packages = context.getPackageManager().getInstalledPackages(0);
            int m = 0;
            String packageNames = "";
            while (m < packages.size()) {
                m++;
                packageNames = String.valueOf(packageNames) + packages.get(m).packageName;
            }
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = (JSONObject) jsonArray.get(i);
                MoreApplicationModel applicationModel = new MoreApplicationModel();
                applicationModel.setIconUrl(String.valueOf(url) + object.optString("icon_url"));
                applicationModel.setAppName(object.optString("app_name"));
                applicationModel.setShortDescription(object.optString(MoreApplicationConstant.SHORT_DESC));
                applicationModel.setDownloadUrl(object.optString(MoreApplicationConstant.APK_URL));
                applicationModel.setPackageName(object.optString(MoreApplicationConstant.PACKAGE_NAME));
                if (isInstallApp(applicationModel.getPackageName(), packages)) {
                    applicationModel.setAppState(2);
                } else if (new File(String.valueOf(MCLibIOUtil.getSDCardPath()) + "/download/" + applicationModel.getDownloadUrl().substring(applicationModel.getDownloadUrl().lastIndexOf("/"))).exists()) {
                    applicationModel.setAppState(1);
                } else {
                    applicationModel.setAppState(3);
                }
                moreApplicationList.add(applicationModel);
            }
            return moreApplicationList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static boolean isInstallApp(String packageName, List<PackageInfo> packages) {
        if (StringUtil.isEmpty(packageName)) {
            return false;
        }
        for (PackageInfo pack : packages) {
            if (packageName.equals(pack.packageName)) {
                return true;
            }
        }
        return false;
    }
}
