package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.forum.android.db.constant.PollDBConstant;

public class PollPostsDBUtil extends BaseDBUtil implements PollDBConstant {
    private static PollPostsDBUtil pollPostsDBUtil;

    public PollPostsDBUtil(Context ctx) {
        super(ctx);
    }

    public static PollPostsDBUtil getInstance(Context context) {
        if (pollPostsDBUtil == null) {
            pollPostsDBUtil = new PollPostsDBUtil(context);
        }
        return pollPostsDBUtil;
    }

    public long getposts(long topicId, long userId) {
        long deadTime = 0;
        Cursor cursor = null;
        try {
            openReadableDB();
            cursor = this.readableDatabase.query(PollDBConstant.TABLE_POLL_POSTS, null, "userId = " + userId + " and " + "topicId" + " = " + topicId, null, null, null, null);
            while (cursor.moveToNext()) {
                deadTime = cursor.getLong(3);
            }
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            e.printStackTrace();
            deadTime = 0;
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
            throw th;
        }
        return deadTime;
    }

    public boolean updatePosts(long userId, long topicId, long deadtime, long time) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("userId", Long.valueOf(userId));
            values.put("topicId", Long.valueOf(topicId));
            values.put(PollDBConstant.COLUMN_POLL_TIME, Long.valueOf(time));
            values.put(PollDBConstant.COLUMN_DEADTIME, Long.valueOf(deadtime));
            if (getposts(topicId, userId) <= 0) {
                this.writableDatabase.insertOrThrow(PollDBConstant.TABLE_POLL_POSTS, null, values);
            } else {
                this.writableDatabase.update(PollDBConstant.TABLE_POLL_POSTS, values, "userId = " + userId + " and " + "topicId" + " = " + topicId, null);
            }
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean deleteAllPoll() {
        return removeAllEntries(PollDBConstant.TABLE_POLL_POSTS);
    }
}
