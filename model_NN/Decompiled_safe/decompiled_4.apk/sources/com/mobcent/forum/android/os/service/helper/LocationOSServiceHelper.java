package com.mobcent.forum.android.os.service.helper;

import android.content.Context;
import com.baidu.location.BDLocation;
import com.mobcent.forum.android.service.LocationService;
import com.mobcent.forum.android.service.impl.LocationServiceImpl;
import com.mobcent.forum.android.util.LocationUtil;
import java.util.List;

public class LocationOSServiceHelper {
    private static LocationUtil locationUtil = null;

    public static void startLocationOSService(Context context) {
        initLocationUtil(context);
        locationUtil.startLocation();
    }

    public static void stopLocationOSService(Context context) {
        if (locationUtil != null) {
            locationUtil.stopLocation();
        }
    }

    public static void initLocationUtil(Context context) {
        final LocationService saveLocationService = new LocationServiceImpl(context);
        locationUtil = new LocationUtil(context, false, new LocationUtil.LocationDelegate() {
            public void locationResults(final BDLocation location) {
                if (location.getLocType() == 161 || location.getLocType() == 61) {
                    final LocationService locationService = LocationService.this;
                    new Thread(new Runnable() {
                        public void run() {
                            locationService.saveLocation(location.getLongitude(), location.getLatitude(), location.getAddrStr());
                        }
                    }).start();
                } else if (location.getLocType() != 62 && location.getLocType() != 167) {
                    location.getLocType();
                }
            }

            public void onReceivePoi(boolean hasPoi, List<String> list) {
            }
        });
    }
}
