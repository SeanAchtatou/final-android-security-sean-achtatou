package com.mobcent.forum.android.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.mobcent.forum.android.db.constant.BoardDBConstant;
import com.mobcent.forum.android.db.constant.FeedsDBConstant;
import com.mobcent.forum.android.db.constant.HeartMsgDBConstant;
import com.mobcent.forum.android.db.constant.HotTopicDBConstant;
import com.mobcent.forum.android.db.constant.LocationDBConstant;
import com.mobcent.forum.android.db.constant.MentionFriendsDBConstant;
import com.mobcent.forum.android.db.constant.ModeratorBoardDBConstant;
import com.mobcent.forum.android.db.constant.NewsTopicDBConstant;
import com.mobcent.forum.android.db.constant.PermDBConstant;
import com.mobcent.forum.android.db.constant.PicTopicDBConstant;
import com.mobcent.forum.android.db.constant.PollDBConstant;
import com.mobcent.forum.android.db.constant.PopNoticeDBConstant;
import com.mobcent.forum.android.db.constant.RecomUserDBConstant;
import com.mobcent.forum.android.db.constant.SurrroundTopicDBConstant;
import com.mobcent.forum.android.db.constant.SurrroundUserDBConstant;
import com.mobcent.forum.android.db.constant.TopicDraftDBConstant;
import com.mobcent.forum.android.db.constant.UserFriendsDBConstant;
import com.mobcent.forum.android.db.constant.UserJsonDBConstant;

public class DBOpenHelper extends SQLiteOpenHelper {
    public DBOpenHelper(Context context, String databaseName, int version) {
        super(context, databaseName, (SQLiteDatabase.CursorFactory) null, version);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PermDBConstant.SQL_CREATE_TABLE_PERM);
        db.execSQL(BoardDBConstant.SQL_CREATE_TABLE_BOARD);
        db.execSQL(FeedsDBConstant.SQL_CREATE_TABLE_FEEDS);
        db.execSQL(HeartMsgDBConstant.CREATE_TABLE_HEART_BEAT);
        db.execSQL(HeartMsgDBConstant.CREATE_TABLE_MESSAGE);
        db.execSQL(HotTopicDBConstant.SQL_CREATE_TABLE_HOT_TOPIC);
        db.execSQL(LocationDBConstant.SQL_CREATE_TABLE_LOCATION);
        db.execSQL(MentionFriendsDBConstant.SQL_CREATE_TABLE_FRIEND);
        db.execSQL(NewsTopicDBConstant.SQL_CREATE_TABLE_NEWS_TOPIC);
        db.execSQL(PollDBConstant.SQL_CREATE_TABLE_POLL);
        db.execSQL(PopNoticeDBConstant.CREATE_TABLE_NOTICE);
        db.execSQL(RecomUserDBConstant.SQL_CREATE_TABLE_RECOM_USER);
        db.execSQL(SurrroundTopicDBConstant.SQL_CREATE_TABLE_LOCATION);
        db.execSQL(SurrroundUserDBConstant.SQL_CREATE_TABLE_LOCATION);
        db.execSQL(TopicDraftDBConstant.SQL_CREATE_TABLE_TOPIC_DRAFT);
        db.execSQL(TopicDraftDBConstant.SQL_CREATE_TABLE_TOPIC_DRAFT);
        db.execSQL(UserFriendsDBConstant.SQL_CREATE_TABLE_USER_FRIEND);
        db.execSQL(UserJsonDBConstant.CREATE_TABLE_USER);
        db.execSQL(UserJsonDBConstant.CREATE_TABLE_PERSONAL);
        db.execSQL(PicTopicDBConstant.SQL_CREATE_TABLE_PIC_TOPIC);
        db.execSQL(ModeratorBoardDBConstant.SQL_CREATE_TABLE_MODERATOR_BOARD);
        db.execSQL(FeedsDBConstant.SQL_CREATE_TABLE_NEW_FEEDS);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.delete(BoardDBConstant.TABLE_BOARD, null, null);
        db.delete(FeedsDBConstant.TABLE_FEEDS, null, null);
        db.delete(HeartMsgDBConstant.TABLE_HEART_BEAT, null, null);
        db.delete(HeartMsgDBConstant.TABLE_MESSAGE, null, null);
        db.delete(HotTopicDBConstant.TABLE_HOT_TOPIC, null, null);
        db.delete("location", null, null);
        db.delete(MentionFriendsDBConstant.TABLE_MENTION_FRIEND, null, null);
        db.delete(NewsTopicDBConstant.TABLE_NEWS_TOPIC, null, null);
        db.delete(PollDBConstant.TABLE_POLL_POSTS, null, null);
        db.delete(PopNoticeDBConstant.TABLE_NOTICE, null, null);
        db.delete(RecomUserDBConstant.TABLE_RECOM_USER, null, null);
        db.delete(SurrroundTopicDBConstant.TABLE_SURROUND_TOPIC, null, null);
        db.delete(SurrroundUserDBConstant.TABLE_SURROUND_FRIENDS, null, null);
        db.delete(TopicDraftDBConstant.TABLE_TOPIC_DRAFT, null, null);
        db.delete(UserFriendsDBConstant.TABLE_USER_FRIEND, null, null);
        db.delete(UserJsonDBConstant.TABLE_JSON_USER, null, null);
        db.delete(UserJsonDBConstant.TABLE_JSON_PERSONAL, null, null);
        db.delete(PicTopicDBConstant.TABLE_PIC_TOPIC, null, null);
        db.delete(ModeratorBoardDBConstant.TABLE_MODERATOR_BOARD, null, null);
        db.delete(FeedsDBConstant.TABLE_NEW_FEEDS, null, null);
        onCreate(db);
    }
}
