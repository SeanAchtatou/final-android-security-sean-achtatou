package com.mobcent.forum.android.db.constant;

public interface PopNoticeDBConstant {
    public static final String COLUMN_JSON_STR = "jsonStr";
    public static final String COLUMN_NOTICE_ID = "noticeId";
    public static final String COLUMN_NOTICE_STATE = "noticeState";
    public static final String CREATE_TABLE_NOTICE = "CREATE TABLE IF NOT EXISTS t_notice ( noticeId LONG PRIMARY KEY,noticeState INTEGER,jsonStr TEXT );";
    public static final String DELETE_TABLE_WHERE_NOTICE_ID = "noticeId =?";
    public static final String SELECT_ALL_NOTICE = "SELECT * FROM t_notice";
    public static final String SELECT_SOME_NOTICE = "SELECT * FROM t_notice WHERE noticeId=? ORDER BY noticeId DESC";
    public static final int STATE_DB_EXCEPTION = 0;
    public static final int STATE_IS_CHECK = 1;
    public static final int STATE_NOT_CHECK = -1;
    public static final String TABLE_NOTICE = "t_notice";
}
