package com.mobcent.forum.android.model;

import java.util.List;

public class ModeratorModel extends BaseModel {
    private static final long serialVersionUID = -5142826605342208917L;
    private List<BoardModel> boardList;
    private String icon;
    private int moderator;
    private String nickname;
    private long userId;

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public int getModerator() {
        return this.moderator;
    }

    public void setModerator(int isModerator) {
        this.moderator = isModerator;
    }

    public String getNickname() {
        return this.nickname;
    }

    public void setNickname(String nickname2) {
        this.nickname = nickname2;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
    }

    public List<BoardModel> getBoardList() {
        return this.boardList;
    }

    public void setBoardList(List<BoardModel> boardList2) {
        this.boardList = boardList2;
    }
}
