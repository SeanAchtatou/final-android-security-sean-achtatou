package com.mobcent.forum.android.model;

public class FeedsModel extends BaseModel {
    private static final long serialVersionUID = -6571795433880473152L;
    private long aId;
    private long ftype;
    private int hasNext;
    private long oId;
    private String oName;
    private int objectLength;
    private long sId;
    private String sName;
    private int subjectLength;
    private long time;
    private int typeLenth;

    public long getaId() {
        return this.aId;
    }

    public long getFtype() {
        return this.ftype;
    }

    public int getHasNext() {
        return this.hasNext;
    }

    public int getObjectLength() {
        return this.objectLength;
    }

    public long getoId() {
        return this.oId;
    }

    public String getoName() {
        return this.oName;
    }

    public long getsId() {
        return this.sId;
    }

    public String getsName() {
        return this.sName;
    }

    public int getSubjectLength() {
        return this.subjectLength;
    }

    public long getTime() {
        return this.time;
    }

    public int getTypeLenth() {
        return this.typeLenth;
    }

    public void setaId(long aId2) {
        this.aId = aId2;
    }

    public void setFtype(long ftype2) {
        this.ftype = ftype2;
    }

    public void setHasNext(int hasNext2) {
        this.hasNext = hasNext2;
    }

    public void setObjectLength(int sLength) {
        this.objectLength = sLength;
    }

    public void setoId(long oId2) {
        this.oId = oId2;
    }

    public void setoName(String oName2) {
        this.oName = oName2;
    }

    public void setsId(long sId2) {
        this.sId = sId2;
    }

    public void setsName(String sName2) {
        this.sName = sName2;
    }

    public void setSubjectLength(int subjectLength2) {
        this.subjectLength = subjectLength2;
    }

    public void setTime(long time2) {
        this.time = time2;
    }

    public void setTypeLenth(int wayLenth) {
        this.typeLenth = wayLenth;
    }
}
