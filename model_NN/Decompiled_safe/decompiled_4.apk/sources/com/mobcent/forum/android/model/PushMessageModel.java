package com.mobcent.forum.android.model;

public class PushMessageModel extends BaseModel {
    private static final long serialVersionUID = -4130566338947229624L;
    private String pushDesc;
    private int pushDetailType;
    private long pushMsgId;
    private int pushType;
    private String title;
    private long topicId;

    public long getPushMsgId() {
        return this.pushMsgId;
    }

    public void setPushMsgId(long pushMsgId2) {
        this.pushMsgId = pushMsgId2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getPushDesc() {
        return this.pushDesc;
    }

    public void setPushDesc(String pushDesc2) {
        this.pushDesc = pushDesc2;
    }

    public long getTopicId() {
        return this.topicId;
    }

    public void setTopicId(long topicId2) {
        this.topicId = topicId2;
    }

    public int getPushType() {
        return this.pushType;
    }

    public void setPushType(int pushType2) {
        this.pushType = pushType2;
    }

    public int getPushDetailType() {
        return this.pushDetailType;
    }

    public void setPushDetailType(int pushDetailType2) {
        this.pushDetailType = pushDetailType2;
    }
}
