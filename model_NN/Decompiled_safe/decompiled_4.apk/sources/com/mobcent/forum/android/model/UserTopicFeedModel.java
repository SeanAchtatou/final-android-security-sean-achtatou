package com.mobcent.forum.android.model;

public class UserTopicFeedModel extends BaseModel {
    private static final long serialVersionUID = 552859095231131889L;
    private AnnoModel anno;
    private int ftype;
    private ReplyModel reply;
    private ReplyModel replyQuote;
    private long time;
    private UserInfoModel toUser;
    private TopicModel topic;
    private VoteFeedModel vote;

    public ReplyModel getReplyQuote() {
        return this.replyQuote;
    }

    public void setReplyQuote(ReplyModel replyQuote2) {
        this.replyQuote = replyQuote2;
    }

    public int getFtype() {
        return this.ftype;
    }

    public void setFtype(int ftype2) {
        this.ftype = ftype2;
    }

    public TopicModel getTopic() {
        return this.topic;
    }

    public void setTopic(TopicModel topic2) {
        this.topic = topic2;
    }

    public ReplyModel getReply() {
        return this.reply;
    }

    public void setReply(ReplyModel reply2) {
        this.reply = reply2;
    }

    public long getTime() {
        return this.time;
    }

    public void setTime(long time2) {
        this.time = time2;
    }

    public VoteFeedModel getVote() {
        return this.vote;
    }

    public void setVote(VoteFeedModel vote2) {
        this.vote = vote2;
    }

    public UserInfoModel getToUser() {
        return this.toUser;
    }

    public void setToUser(UserInfoModel toUser2) {
        this.toUser = toUser2;
    }

    public AnnoModel getAnno() {
        return this.anno;
    }

    public void setAnno(AnnoModel anno2) {
        this.anno = anno2;
    }
}
