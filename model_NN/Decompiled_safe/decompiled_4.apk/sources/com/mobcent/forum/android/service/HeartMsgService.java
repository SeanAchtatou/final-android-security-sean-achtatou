package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.HeartMsgModel;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.List;

public interface HeartMsgService {
    boolean deleteHeartBeatList(long j);

    List<HeartMsgModel> getAllMessages(long j, int i, int i2);

    List<HeartMsgModel> getHeartBeatListByNet();

    List<HeartMsgModel> getHeartBeatListLocally();

    List<HeartMsgModel> getMessageList(long j);

    List<UserInfoModel> getMsgUserList(int i, int i2);

    String sendMessage(HeartMsgModel heartMsgModel);

    String setUserBlack(int i, long j, long j2);

    void updateMessage(List<HeartMsgModel> list);

    String uploadAudio(String str);
}
