package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.BaseRestfulApiConstant;
import com.mobcent.forum.android.constant.PopNoticeConstant;
import com.mobcent.forum.android.model.PopNoticeModel;
import org.json.JSONObject;

public class PopNoticeServiceImplHelper implements PopNoticeConstant, BaseRestfulApiConstant {
    public static PopNoticeModel parsePopNoticeJson(String jsonStr) {
        PopNoticeModel popNoticeModel = new PopNoticeModel();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.optInt("rs") == 0) {
                return null;
            }
            JSONObject jobj = jsonObj.optJSONObject(PopNoticeConstant.RETURN_NOTICE);
            if (jobj != null) {
                popNoticeModel.setNoticeId(jobj.optLong(PopNoticeConstant.RETURN_NOTICE_ID));
                popNoticeModel.setType(jobj.optInt("type"));
                popNoticeModel.setVersion(jobj.optInt("version"));
                popNoticeModel.setContent(jobj.optString("content"));
                popNoticeModel.setUrl(jobj.optString("url"));
            }
            return popNoticeModel;
        } catch (Exception e) {
            popNoticeModel = null;
        }
    }
}
