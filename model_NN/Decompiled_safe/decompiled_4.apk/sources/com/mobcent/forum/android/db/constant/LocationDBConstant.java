package com.mobcent.forum.android.db.constant;

public interface LocationDBConstant {
    public static final String COLUMN_ADDR = "addrStr";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_STRING = "str";
    public static final String SQL_CREATE_TABLE_LOCATION = "CREATE TABLE IF NOT EXISTS location(id LONG PRIMARY KEY,latitude DOUBLE , longitude DOUBLE , addrStr TEXT , str TEXT );";
    public static final String TABLE_LOCATION = "location";
}
