package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.forum.android.db.constant.NewsTopicDBConstant;
import com.mobcent.forum.android.util.DateUtil;

public class NewTopicDBUtil extends BaseDBUtil implements NewsTopicDBConstant {
    private static final int NEW_TOPIC_ID = 1;
    private static NewTopicDBUtil newTopicDBUtil;

    public NewTopicDBUtil(Context ctx) {
        super(ctx);
    }

    public static NewTopicDBUtil getInstance(Context context) {
        if (newTopicDBUtil == null) {
            newTopicDBUtil = new NewTopicDBUtil(context);
        }
        return newTopicDBUtil;
    }

    public String[] getNewsTopicJsonString() {
        String[] result = new String[2];
        String jsonStr = null;
        Cursor cursor = null;
        long time = 0;
        try {
            openReadableDB();
            cursor = this.readableDatabase.rawQuery(NewsTopicDBConstant.SQL_SELECT_NEWS_TOPTIC, null);
            while (cursor.moveToNext()) {
                jsonStr = cursor.getString(1);
                time = cursor.getLong(2);
            }
            result[0] = jsonStr;
            result[1] = new StringBuilder(String.valueOf(time)).toString();
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
            throw th;
        }
        return result;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean updateNewsTopicJsonString(String jsonStr) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("id", (Integer) 1);
            values.put("jsonStr", jsonStr);
            values.put("update_time", Long.valueOf(DateUtil.getCurrentTime()));
            if (!isRowExisted(this.writableDatabase, NewsTopicDBConstant.TABLE_NEWS_TOPIC, "id", 1)) {
                this.writableDatabase.insertOrThrow(NewsTopicDBConstant.TABLE_NEWS_TOPIC, null, values);
            } else {
                this.writableDatabase.update(NewsTopicDBConstant.TABLE_NEWS_TOPIC, values, "id=1", null);
            }
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean delteNewTopicList() {
        return removeAllEntries(NewsTopicDBConstant.TABLE_NEWS_TOPIC);
    }
}
