package com.mobcent.forum.android.service.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.mobcent.forum.android.api.util.HttpClientUtil;
import com.mobcent.forum.android.service.FileTransferService;
import com.mobcent.forum.android.service.delegate.DownProgressDelegate;
import com.mobcent.forum.android.util.ImageUtil;
import com.mobcent.forum.android.util.MCLibIOUtil;
import java.io.File;

public class FileTransferServiceImpl implements FileTransferService {
    private Context context;

    public FileTransferServiceImpl(Context context2) {
        this.context = context2;
    }

    public Bitmap getBitmapUnInsExtMedia(String url) {
        byte[] imgData = HttpClientUtil.getFileInByte(url, this.context);
        if (imgData == null) {
            return null;
        }
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = 1;
        return BitmapFactory.decodeByteArray(imgData, 0, imgData.length, bitmapOptions);
    }

    public Bitmap getBitmapUnInsExtMedia(String url, float scale, int maxWidth, boolean isMatrix) {
        byte[] imgData = getImageStream(url);
        if (imgData == null) {
            return null;
        }
        return ImageUtil.compressBitmap(this.context, imgData, imgData.length, maxWidth, scale, isMatrix);
    }

    public Bitmap getBitmapInsExtMedia(String url, float scale, int maxWidth, String relativePath, String basePath, boolean isMatrix) {
        byte[] imgData = getImageStream(url);
        if (imgData == null) {
            return null;
        }
        MCLibIOUtil.saveFile(imgData, relativePath, basePath);
        return ImageUtil.compressBitmap(this.context, imgData, imgData.length, maxWidth, scale, isMatrix);
    }

    public byte[] getImageStream(String url) {
        return HttpClientUtil.getFileInByte(url, this.context);
    }

    public Bitmap getBitmapUnInsExtMediaProBar(String url, float scale, int maxWidth, DownProgressDelegate downProgress, boolean isMatrix) {
        byte[] imgData = getImageStreamProBar(url, downProgress);
        if (imgData == null) {
            return null;
        }
        return ImageUtil.compressBitmap(this.context, imgData, imgData.length, maxWidth, scale, isMatrix);
    }

    public Bitmap getBitmapInsExtMediaProBar(String url, float scale, int maxWidth, String relativePath, String basePath, DownProgressDelegate downProgress, boolean isMatrix) {
        byte[] imgData = getImageStreamProBar(url, downProgress);
        if (imgData == null) {
            return null;
        }
        MCLibIOUtil.saveFile(imgData, relativePath, basePath);
        return ImageUtil.compressBitmap(this.context, imgData, imgData.length, maxWidth, scale, isMatrix);
    }

    public byte[] getImageStreamProBar(String url, DownProgressDelegate downProgress) {
        return HttpClientUtil.getFileInByteByProgress(url, this.context, downProgress);
    }

    public Bitmap getBitmapUnInsExtMediaProBar(String url, DownProgressDelegate downProgress) {
        byte[] imgData = HttpClientUtil.getFileInByteByProgress(url, this.context, downProgress);
        if (imgData == null) {
            return null;
        }
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = 1;
        return BitmapFactory.decodeByteArray(imgData, 0, imgData.length, bitmapOptions);
    }

    public void downloadFile(String url, File targetFile) {
        HttpClientUtil.downloadFile(url, targetFile, this.context);
    }
}
