package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.BaseRestfulApiConstant;
import com.mobcent.forum.android.constant.BoardApiConstant;
import com.mobcent.forum.android.model.BoardCategoryModel;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.model.ForumModel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class BoardServiceImplHelper implements BoardApiConstant {
    /* JADX INFO: Multiple debug info for r21v11 org.json.JSONObject: [D('forumModel' com.mobcent.forum.android.model.ForumModel), D('jobj' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r6v4 com.mobcent.forum.android.model.BoardModel: [D('boardModel' com.mobcent.forum.android.model.BoardModel), D('boardModel2' com.mobcent.forum.android.model.BoardModel)] */
    /* JADX INFO: Multiple debug info for r21v17 int: [D('bl' java.util.List<com.mobcent.forum.android.model.BoardModel>), D('k' int)] */
    /* JADX INFO: Multiple debug info for r21v20 int: [D('boardModel' com.mobcent.forum.android.model.BoardModel), D('k' int)] */
    public static ForumModel getForumInfo(String jsonStr) {
        ForumModel forumModel = new ForumModel();
        try {
            JSONObject jSONObject = new JSONObject(jsonStr);
            forumModel.setOnlineUserNum(jSONObject.optInt(BoardApiConstant.ONLINE_USER_NUM));
            forumModel.setTdVisitors(jSONObject.optInt(BoardApiConstant.TD_VISITORS));
            forumModel.setLastUpdateTime(jSONObject.optLong(BaseRestfulApiConstant.LAST_UPDATE_TIME, 0));
            ArrayList arrayList = new ArrayList();
            JSONArray jsonArray = jSONObject.optJSONArray("list");
            int j = jsonArray.length();
            for (int i = 0; i < j; i++) {
                JSONObject jobj = jsonArray.optJSONObject(i);
                BoardCategoryModel boardCategoryModel = new BoardCategoryModel();
                boardCategoryModel.setBoardCategoryId(jobj.optLong("board_category_id"));
                boardCategoryModel.setBoardCategoryName(jobj.optString("board_category_name"));
                boardCategoryModel.setBoardCategoryType(jobj.optInt("board_category_type"));
                ArrayList arrayList2 = new ArrayList();
                JSONArray jsonBoardArray = jobj.optJSONArray("board_list");
                if (boardCategoryModel.getBoardCategoryType() == 1) {
                    int s = jsonBoardArray.length();
                    for (int k = 0; k < s; k++) {
                        BoardModel boardModel = new BoardModel();
                        JSONObject boardjson = jsonBoardArray.optJSONObject(k);
                        boardModel.setBoardId(boardjson.optLong("board_id"));
                        boardModel.setBoardName(boardjson.optString("board_name"));
                        boardModel.setLastPostsDate(boardjson.optLong(BoardApiConstant.LAST_POSTS_DATE));
                        boardModel.setPostsTotalNum(boardjson.optInt(BoardApiConstant.POSTS_TOTAL_NUM));
                        boardModel.setTodayPostsNum(boardjson.optInt(BoardApiConstant.TD_POSTS_NUM));
                        boardModel.setTopicTotalNum(boardjson.optInt(BoardApiConstant.TOPIC_TOTAL_NUM));
                        boardModel.setPicPath(boardjson.optString("pic_path"));
                        boardModel.setBoardDesc(boardjson.optString(BoardApiConstant.BOARD_DESC));
                        boardModel.setBaseUrl(jSONObject.getString("img_url"));
                        arrayList2.add(boardModel);
                    }
                }
                if (boardCategoryModel.getBoardCategoryType() == 2) {
                    int s2 = jsonBoardArray.length();
                    for (int k2 = 0; k2 < s2; k2 += 2) {
                        BoardModel model = new BoardModel();
                        ArrayList arrayList3 = new ArrayList();
                        BoardModel boardModel2 = new BoardModel();
                        JSONObject boardjson2 = jsonBoardArray.optJSONObject(k2);
                        boardModel2.setBoardId(boardjson2.optLong("board_id"));
                        boardModel2.setBoardName(boardjson2.optString("board_name"));
                        boardModel2.setLastPostsDate(boardjson2.optLong(BoardApiConstant.LAST_POSTS_DATE));
                        boardModel2.setPostsTotalNum(boardjson2.optInt(BoardApiConstant.POSTS_TOTAL_NUM));
                        boardModel2.setTodayPostsNum(boardjson2.optInt(BoardApiConstant.TD_POSTS_NUM));
                        boardModel2.setTopicTotalNum(boardjson2.optInt(BoardApiConstant.TOPIC_TOTAL_NUM));
                        boardModel2.setPicPath(boardjson2.optString("pic_path"));
                        boardModel2.setBoardDesc(boardjson2.optString(BoardApiConstant.BOARD_DESC));
                        boardModel2.setBaseUrl(jSONObject.getString("img_url"));
                        arrayList3.add(boardModel2);
                        BoardModel boardModel22 = new BoardModel();
                        if (k2 + 1 < s2) {
                            JSONObject boardjson22 = jsonBoardArray.optJSONObject(k2 + 1);
                            boardModel22.setBoardId(boardjson22.optLong("board_id"));
                            boardModel22.setBoardName(boardjson22.optString("board_name"));
                            boardModel22.setLastPostsDate(boardjson22.optLong(BoardApiConstant.LAST_POSTS_DATE));
                            boardModel22.setPostsTotalNum(boardjson22.optInt(BoardApiConstant.POSTS_TOTAL_NUM));
                            boardModel22.setTodayPostsNum(boardjson22.optInt(BoardApiConstant.TD_POSTS_NUM));
                            boardModel22.setTopicTotalNum(boardjson22.optInt(BoardApiConstant.TOPIC_TOTAL_NUM));
                            boardModel22.setPicPath(boardjson2.optString("pic_path"));
                            boardModel22.setBoardDesc(boardjson2.optString(BoardApiConstant.BOARD_DESC));
                            boardModel22.setBaseUrl(jSONObject.getString("img_url"));
                            arrayList3.add(boardModel22);
                        } else {
                            arrayList3.add(null);
                        }
                        model.setBoards(arrayList3);
                        arrayList2.add(model);
                    }
                }
                boardCategoryModel.setBoardList(arrayList2);
                arrayList.add(boardCategoryModel);
            }
            forumModel.setBoardCategoryList(arrayList);
            return forumModel;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getForumInfoStrWithLastUpdateTime(String jsonStr, long time) {
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            jsonObject.put(BaseRestfulApiConstant.LAST_UPDATE_TIME, time);
            return jsonObject.toString();
        } catch (Exception e) {
            return jsonStr;
        }
    }

    /* JADX INFO: Multiple debug info for r1v1 java.util.ArrayList: [D('jsonObject' org.json.JSONObject), D('boardList' java.util.List<com.mobcent.forum.android.model.BoardModel>)] */
    /* JADX INFO: Multiple debug info for r11v9 org.json.JSONObject: [D('boardList' java.util.List<com.mobcent.forum.android.model.BoardModel>), D('jobj' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r11v10 java.lang.String: [D('jobj' org.json.JSONObject), D('board' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r11v14 int: [D('boardModel' com.mobcent.forum.android.model.BoardModel), D('k' int)] */
    public static List<BoardModel> getForumBoardInfo(String jsonStr) {
        try {
            JSONArray jsonArray = new JSONObject(jsonStr).optJSONArray("list");
            List<BoardModel> boardList = new ArrayList<>();
            try {
                int j = jsonArray.length();
                for (int i = 0; i < j; i++) {
                    JSONArray jsonBoardArray = new JSONArray(jsonArray.optJSONObject(i).optString("board_list"));
                    int s = jsonBoardArray.length();
                    for (int k = 0; k < s; k++) {
                        BoardModel boardModel = new BoardModel();
                        JSONObject boardjson = jsonBoardArray.optJSONObject(k);
                        boardModel.setBoardId(boardjson.optLong("board_id"));
                        boardModel.setBoardName(boardjson.optString("board_name"));
                        boardModel.setLastPostsDate(boardjson.optLong(BoardApiConstant.LAST_POSTS_DATE));
                        boardModel.setPostsTotalNum(boardjson.optInt(BoardApiConstant.POSTS_TOTAL_NUM));
                        boardModel.setTodayPostsNum(boardjson.optInt(BoardApiConstant.TD_POSTS_NUM));
                        boardModel.setTopicTotalNum(boardjson.optInt(BoardApiConstant.TOPIC_TOTAL_NUM));
                        boardList.add(boardModel);
                    }
                }
                return boardList;
            } catch (Exception e) {
                return null;
            }
        } catch (Exception e2) {
            return null;
        }
    }
}
