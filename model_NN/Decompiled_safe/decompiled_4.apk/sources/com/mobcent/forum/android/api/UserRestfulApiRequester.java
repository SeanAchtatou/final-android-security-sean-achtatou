package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.api.util.HttpClientUtil;
import com.mobcent.forum.android.constant.BaseRestfulApiConstant;
import com.mobcent.forum.android.constant.UserConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.util.MCResource;
import com.mobcent.forum.android.util.PhoneUtil;
import java.util.HashMap;

public class UserRestfulApiRequester extends BaseRestfulApiRequester implements UserConstant {
    public static String regUser(Context context, String email, String password) {
        HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", password);
        return regLogin(context, "user/regUser.do", params);
    }

    public static String regUserByOpenPlatform(Context context, String email, String icon, String password, int gender, String nickName, long platformId, String platformUserId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("icon", icon);
        params.put("password", password);
        params.put("gender", new StringBuilder(String.valueOf(gender)).toString());
        params.put("nickname", nickName);
        params.put("platformId", new StringBuilder(String.valueOf(platformId)).toString());
        params.put(UserConstant.PLATFORM_USER_ID, platformUserId);
        return regLogin(context, "user/regUserByOpenPlatform.do", params);
    }

    public static String loginUser(Context context, String email, String password) {
        HashMap<String, String> params = new HashMap<>();
        params.put("email", email);
        params.put("password", password);
        return regLogin(context, "user/login.do", params);
    }

    public static String changeUserPwd(Context context, String oldPwd, String newPwd) {
        HashMap<String, String> params = new HashMap<>();
        params.put(UserConstant.USER_OLDPASSWORD, oldPwd);
        params.put("password", newPwd);
        return doPostRequest("user/updateUserPassword.do", params, context);
    }

    public static String getUserInfo(Context context, long userId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        return doPostRequest("user/getUserInfo.do", params, context);
    }

    public static String getSurroudUser(Context context, double longitude, double latitude, int radius, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("poi", "user");
        params.put("longitude", new StringBuilder(String.valueOf(longitude)).toString());
        params.put("latitude", new StringBuilder(String.valueOf(latitude)).toString());
        if (radius > 0) {
            params.put(BaseRestfulApiConstant.RADIUS, new StringBuilder(String.valueOf(radius)).toString());
        }
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("lbs/lookAround.do", params, context);
    }

    public static String followUser(Context context, long currentUserId, long focusId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(currentUserId)).toString());
        params.put(UserConstant.FOLLOW_ID, new StringBuilder(String.valueOf(focusId)).toString());
        return doPostRequest("user/followUser.do", params, context);
    }

    public static String unfollowUser(Context context, long currentUserId, long focusId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(currentUserId)).toString());
        params.put(UserConstant.FOLLOW_ID, new StringBuilder(String.valueOf(focusId)).toString());
        return doPostRequest("user/unfollowUser.do", params, context);
    }

    public static String getUserFriendList(Context context, long userId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("user/getFollowedUsers.do", params, context);
    }

    public static String regLogin(Context context, String urlString, HashMap<String, String> params) {
        String forumKey = SharedPreferencesDB.getInstance(context).getForumKey();
        if (forumKey != null) {
            params.put("forumKey", forumKey);
        }
        params.put("imei", PhoneUtil.getIMEI(context));
        params.put("imsi", PhoneUtil.getIMSI(context));
        String packageName = context.getPackageName();
        String appName = context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
        params.put("packageName", packageName);
        params.put("appName", appName);
        params.put(BaseRestfulApiConstant.SDK_TYPE, "");
        params.put("sdkVersion", BaseRestfulApiConstant.SDK_VERSION_VALUE);
        params.put("platType", "1");
        return HttpClientUtil.doPostRequest(String.valueOf(context.getResources().getString(MCResource.getInstance(context).getStringId("mc_forum_base_request_domain_url"))) + urlString, params, context);
    }

    public static String uploadIcon(Context context, String uploadFile) {
        return uploadFile("mobcentFile/servlet/UploadFileServlet", uploadFile, context, BaseRestfulApiConstant.FORUM_HEADER_LOCAL_ACTION);
    }

    public static String updateUser(Context context, String nickName, String icon, int gender, String signature, long pageFrom) {
        HashMap<String, String> params = new HashMap<>();
        params.put("nickname", nickName);
        if (icon != null) {
            params.put("icon", icon);
        }
        params.put("signature", signature);
        params.put("gender", new StringBuilder(String.valueOf(gender)).toString());
        params.put("pageFrom", new StringBuilder(String.valueOf(pageFrom)).toString());
        return doPostRequest("user/updateUser.do", params, context);
    }

    public static String getUser(Context context) {
        return doPostRequest("user/getMyUserInfo.do", new HashMap<>(), context);
    }

    public static String logout(Context context) {
        HashMap<String, String> params = new HashMap<>();
        SharedPreferencesDB shareDB = SharedPreferencesDB.getInstance(context);
        String accessToken = shareDB.getAccessToken();
        String accessSecret = shareDB.getAccessSecret();
        if (accessToken != null) {
            params.put("accessToken", accessToken);
        }
        if (accessSecret != null) {
            params.put("accessSecret", accessSecret);
        }
        shareDB.removeUserInfo();
        return doPostRequest("user/loginOut.do", params, context);
    }

    public static String getRecommendUser(Context context, long userId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("user/getRecommendUsers.do", params, context);
    }

    public static String getUserFanList(Context context, long userId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("user/getUserFollow.do", params, context);
    }

    public static String getRegUser(Context context) {
        return doPostRequest("user/getUserInfoByCode.do", new HashMap<>(), context);
    }

    public static String getResetPassword(Context context, long userId, String password) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("password", password);
        return doPostRequest("user/resetUserPassword.do", params, context);
    }

    public static String getUserBoardInfoList(Context context, long userId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("otherUserId", new StringBuilder(String.valueOf(userId)).toString());
        return doPostRequest("admin/boardList.do", params, context);
    }
}
