package com.mobcent.forum.android.model;

import java.util.List;

public class TopicFeedModel extends BaseModel {
    private static final long serialVersionUID = -7247901443883797699L;
    private String[] nicknames;
    private List<ReplyModel> replyList;
    private TopicModel topic;
    private UserInfoModel userInfo;

    public TopicModel getTopic() {
        return this.topic;
    }

    public void setTopic(TopicModel topic2) {
        this.topic = topic2;
    }

    public List<ReplyModel> getReplyList() {
        return this.replyList;
    }

    public void setReplyList(List<ReplyModel> replyList2) {
        this.replyList = replyList2;
    }

    public String[] getNicknames() {
        return this.nicknames;
    }

    public void setNicknames(String[] nicknames2) {
        this.nicknames = nicknames2;
    }

    public UserInfoModel getUserInfo() {
        return this.userInfo;
    }

    public void setUserInfo(UserInfoModel userInfo2) {
        this.userInfo = userInfo2;
    }
}
