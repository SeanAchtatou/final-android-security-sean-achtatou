package com.mobcent.forum.android.model;

import java.util.List;

public class RepostTopicModel extends BaseModel {
    private static final long serialVersionUID = 8252981057998154377L;
    private long boardId;
    private String boardName;
    private String nickName;
    private List<TopicContentModel> repostContentList;
    private long repostTime;

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(String nickName2) {
        this.nickName = nickName2;
    }

    public String getBoardName() {
        return this.boardName;
    }

    public void setBoardName(String boardName2) {
        this.boardName = boardName2;
    }

    public long getRepostTime() {
        return this.repostTime;
    }

    public void setRepostTime(long repostTime2) {
        this.repostTime = repostTime2;
    }

    public long getBoardId() {
        return this.boardId;
    }

    public void setBoardId(long boardId2) {
        this.boardId = boardId2;
    }

    public List<TopicContentModel> getRepostContentList() {
        return this.repostContentList;
    }

    public void setRepostContentList(List<TopicContentModel> repostContentList2) {
        this.repostContentList = repostContentList2;
    }
}
