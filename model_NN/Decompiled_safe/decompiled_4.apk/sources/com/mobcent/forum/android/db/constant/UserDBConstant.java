package com.mobcent.forum.android.db.constant;

public interface UserDBConstant {
    public static final String COLUMN_CURRENT_USER = "currUser";
    public static final String COLUMN_SIGNATURE = "signature";
    public static final String COLUMN_USER_ACCESS_SECERT = "uas";
    public static final String COLUMN_USER_ACCESS_TOKEN = "uat";
    public static final String COLUMN_USER_CREDITS = "credits";
    public static final String COLUMN_USER_EMAIl = "email";
    public static final String COLUMN_USER_GENDER = "gender";
    public static final String COLUMN_USER_ICON = "icon";
    public static final String COLUMN_USER_ID = "userId";
    public static final String COLUMN_USER_NICKNAME = "nickname";
    public static final String COLUMN_USER_PWD = "pwd";
    public static final String COLUMN_USER_ROLE_NUM = "role_num";
    public static final String COLUMN_USER_SCORE = "score";
    public static final String CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS t_user(userId LONG PRIMARY KEY,email VARCHAR(64),pwd VARCHAR(32),nickname VARCHAR(50),uas VARCHAR(36),uat VARCHAR(36),icon TEXT,gender INTEGER,score INTEGER,currUser INTEGER,credits INTEGER,signature TEXT);";
    public static final int GENDER_FEMALE = 2;
    public static final int GENDER_MALE = 1;
    public static final int IS_CURR_USER = 1;
    public static final int IS_NOT_CURR_USER = 0;
    public static final String TABLE_USER = "t_user";
}
