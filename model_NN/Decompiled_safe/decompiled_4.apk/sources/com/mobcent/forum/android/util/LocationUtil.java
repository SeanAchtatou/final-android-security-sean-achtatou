package com.mobcent.forum.android.util;

import android.content.Context;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.mobcent.forum.android.db.LocationDBUtil;
import com.mobcent.forum.android.model.LocationModel;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.service.impl.helper.LocationServiceImplHelper;
import java.util.ArrayList;
import java.util.List;

public class LocationUtil {
    /* access modifiers changed from: private */
    public LocationModel location;
    /* access modifiers changed from: private */
    public LocationClient mLocationClient = null;
    /* access modifiers changed from: private */
    public boolean requestPoi = false;
    /* access modifiers changed from: private */
    public boolean saveLocal = false;

    public interface LocationDelegate {
        void locationResults(BDLocation bDLocation);

        void onReceivePoi(boolean z, List<String> list);
    }

    public LocationUtil(final Context context, boolean openGps, final LocationDelegate locationDelegate) {
        this.mLocationClient = new LocationClient(context);
        LocationClientOption option = new LocationClientOption();
        option.setOpenGps(openGps);
        option.setCoorType("bd09ll");
        option.setPriority(1);
        option.setProdName("mobcent");
        option.setPoiExtraInfo(true);
        option.setScanSpan(LocationClientOption.MIN_SCAN_SPAN);
        option.setTimeOut(5000);
        option.setAddrType("all");
        this.mLocationClient.setLocOption(option);
        this.mLocationClient.registerLocationListener(new BDLocationListener() {
            public void onReceiveLocation(BDLocation dbLocation) {
                MCLogUtil.i("LocationUtil", "onReceive");
                if (dbLocation != null) {
                    MCLogUtil.i("LocationUtil", "AddrStr = " + dbLocation.getAddrStr());
                    if (dbLocation.getLocType() == 161 || dbLocation.getLocType() == 61) {
                        if (!LocationUtil.this.saveLocal) {
                            LocationUtil.this.location = LocationUtil.this.saveLocationToDB(context, dbLocation);
                            LocationUtil.this.saveLocal = true;
                        }
                        if (!LocationUtil.this.requestPoi) {
                            LocationUtil.this.stopLocation();
                        } else if (LocationUtil.this.mLocationClient == null || !LocationUtil.this.mLocationClient.isStarted()) {
                            if (locationDelegate != null) {
                                locationDelegate.onReceivePoi(false, null);
                            }
                            LocationUtil.this.stopLocation();
                        } else {
                            LocationUtil.this.mLocationClient.requestPoi();
                        }
                    } else if (dbLocation.getLocType() == 62) {
                        LocationUtil.this.stopLocation();
                    } else if (dbLocation.getLocType() == 167) {
                        LocationUtil.this.stopLocation();
                    } else if (dbLocation.getLocType() == 63) {
                        LocationUtil.this.stopLocation();
                    }
                    if (locationDelegate != null) {
                        locationDelegate.locationResults(dbLocation);
                    }
                }
            }

            public void onReceivePoi(BDLocation poiLocation) {
                List<String> poi = new ArrayList<>();
                boolean hasPoi = false;
                if (poiLocation != null && poiLocation.hasPoi()) {
                    poi = LocationServiceImplHelper.parsePoi(poiLocation.getPoi());
                    hasPoi = true;
                }
                if (locationDelegate != null) {
                    locationDelegate.onReceivePoi(hasPoi, poi);
                }
                LocationUtil.this.stopLocation();
            }
        });
    }

    public void startLocation() {
        if (this.mLocationClient != null && !this.mLocationClient.isStarted()) {
            MCLogUtil.e("LocationUtil", new StringBuilder(String.valueOf(this.mLocationClient.isStarted())).toString());
            MCLogUtil.i("LocationUtil", "startLocation1");
            this.mLocationClient.start();
            this.mLocationClient.requestLocation();
            this.mLocationClient.requestPoi();
        }
        if (this.mLocationClient != null && this.mLocationClient.isStarted()) {
            MCLogUtil.i("LocationUtil", "requestLocation");
            this.mLocationClient.requestLocation();
            this.mLocationClient.requestPoi();
        }
    }

    public void stopLocation() {
        if (this.mLocationClient != null && this.mLocationClient.isStarted()) {
            this.mLocationClient.stop();
        }
    }

    /* access modifiers changed from: private */
    public LocationModel saveLocationToDB(Context context, BDLocation location2) {
        if (location2 == null) {
            return null;
        }
        LocationModel locationModel = new LocationModel();
        locationModel.setLatitude(location2.getLatitude());
        locationModel.setLongitude(location2.getLongitude());
        locationModel.setAddrStr(location2.getAddrStr());
        locationModel.setUserId(new UserServiceImpl(context).getLoginUserId());
        LocationDBUtil.getInstance(context).updateLocation(locationModel);
        return locationModel;
    }

    public LocationClient getLocationClient() {
        return this.mLocationClient;
    }

    public void setLocationClient(LocationClient mLocationClient2) {
        this.mLocationClient = mLocationClient2;
    }

    public LocationModel getLocation() {
        return this.location;
    }

    public void setLocation(LocationModel location2) {
        this.location = location2;
    }

    public boolean isRequestPoi() {
        return this.requestPoi;
    }

    public void setRequestPoi(boolean requestPoi2) {
        this.requestPoi = requestPoi2;
    }
}
