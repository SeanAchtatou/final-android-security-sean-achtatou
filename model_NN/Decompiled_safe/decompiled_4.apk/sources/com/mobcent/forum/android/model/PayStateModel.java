package com.mobcent.forum.android.model;

public class PayStateModel {
    private boolean adv;
    private boolean clientManager;
    private String imgUrl;
    private boolean loadingPage;
    private String loadingPageImage;
    private boolean msgPush;
    private boolean powerBy;
    private boolean shareKey;
    private boolean square;
    private boolean waterMark;
    private String waterMarkImage;
    private String weiXinAppKey;

    public String getLoadingPageImage() {
        return this.loadingPageImage;
    }

    public void setLoadingPageImage(String loadingPageImage2) {
        this.loadingPageImage = loadingPageImage2;
    }

    public boolean getAdv() {
        return this.adv;
    }

    public void setAdv(boolean adv2) {
        this.adv = adv2;
    }

    public boolean getClientManager() {
        return this.clientManager;
    }

    public void setClientManager(boolean clientManager2) {
        this.clientManager = clientManager2;
    }

    public boolean getLoadingPage() {
        return this.loadingPage;
    }

    public void setLoadingPage(boolean loadingPage2) {
        this.loadingPage = loadingPage2;
    }

    public boolean getMsgPush() {
        return this.msgPush;
    }

    public void setMsgPush(boolean msgPush2) {
        this.msgPush = msgPush2;
    }

    public boolean getPowerBy() {
        return this.powerBy;
    }

    public void setPowerBy(boolean powerBy2) {
        this.powerBy = powerBy2;
    }

    public boolean getShareKey() {
        return this.shareKey;
    }

    public void setShareKey(boolean shareKey2) {
        this.shareKey = shareKey2;
    }

    public boolean getSquare() {
        return this.square;
    }

    public void setSquare(boolean square2) {
        this.square = square2;
    }

    public boolean getWaterMark() {
        return this.waterMark;
    }

    public void setWaterMark(boolean waterMark2) {
        this.waterMark = waterMark2;
    }

    public String getWaterMarkImage() {
        return this.waterMarkImage;
    }

    public void setWaterMarkImage(String waterMarkImage2) {
        this.waterMarkImage = waterMarkImage2;
    }

    public String getImgUrl() {
        return this.imgUrl;
    }

    public void setImgUrl(String imgUrl2) {
        this.imgUrl = imgUrl2;
    }

    public String getWeiXinAppKey() {
        return this.weiXinAppKey;
    }

    public void setWeiXinAppKey(String weiXinAppKey2) {
        this.weiXinAppKey = weiXinAppKey2;
    }
}
