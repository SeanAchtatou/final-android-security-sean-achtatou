package com.mobcent.forum.android.model;

import java.util.List;

public class PollTopicModel extends BaseModel {
    private static final long serialVersionUID = -5162029968955435535L;
    private long deadline;
    private boolean is_visible;
    private int[] pollId;
    private int pollStatus;
    private List<PollItemModel> pooList;
    private int poolType;

    public int getPollStatus() {
        return this.pollStatus;
    }

    public void setPollStatus(int pollStatus2) {
        this.pollStatus = pollStatus2;
    }

    public int[] getPollId() {
        return this.pollId;
    }

    public void setPollId(int[] pollId2) {
        this.pollId = pollId2;
    }

    public long getDeadline() {
        return this.deadline;
    }

    public void setDeadline(long deadline2) {
        this.deadline = deadline2;
    }

    public boolean isIs_visible() {
        return this.is_visible;
    }

    public void setIs_visible(boolean is_visible2) {
        this.is_visible = is_visible2;
    }

    public int getPoolType() {
        return this.poolType;
    }

    public void setPoolType(int poolType2) {
        this.poolType = poolType2;
    }

    public List<PollItemModel> getPooList() {
        return this.pooList;
    }

    public void setPooList(List<PollItemModel> pooList2) {
        this.pooList = pooList2;
    }
}
