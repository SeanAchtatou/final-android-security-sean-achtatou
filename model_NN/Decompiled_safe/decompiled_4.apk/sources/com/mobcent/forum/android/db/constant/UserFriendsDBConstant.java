package com.mobcent.forum.android.db.constant;

public interface UserFriendsDBConstant extends BaseDBConstant {
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_JSON_STR = "jsonStr";
    public static final String COLUMN_UPDATE_TIME = "update_time";
    public static final String SQL_CREATE_TABLE_USER_FRIEND = "CREATE TABLE IF NOT EXISTS user_friends_table(id LONG PRIMARY KEY,jsonStr TEXT , update_time LONG );";
    public static final String SQL_SELECT_USER_FRIEND = "SELECT * FROM user_friends_table";
    public static final String TABLE_USER_FRIEND = "user_friends_table";
}
