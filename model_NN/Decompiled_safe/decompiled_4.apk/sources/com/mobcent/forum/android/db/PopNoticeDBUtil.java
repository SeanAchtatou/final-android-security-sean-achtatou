package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import com.mobcent.forum.android.db.constant.PopNoticeDBConstant;
import com.mobcent.forum.android.model.PopNoticeModel;

public class PopNoticeDBUtil extends BaseDBUtil implements PopNoticeDBConstant {
    private static PopNoticeDBUtil popNoticeDBUtil;

    public PopNoticeDBUtil(Context ctx) {
        super(ctx);
    }

    public static PopNoticeDBUtil getInstance(Context context) {
        if (popNoticeDBUtil == null) {
            popNoticeDBUtil = new PopNoticeDBUtil(context);
        }
        return popNoticeDBUtil;
    }

    public static PopNoticeDBUtil getNewInstance(Context context) {
        popNoticeDBUtil = new PopNoticeDBUtil(context);
        return popNoticeDBUtil;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public int existedState(PopNoticeModel popNoticeModel) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put(PopNoticeDBConstant.COLUMN_NOTICE_ID, Long.valueOf(popNoticeModel.getNoticeId()));
            values.put("jsonStr", popNoticeModel.getJsonStr());
            if (!isRowExisted(this.writableDatabase, PopNoticeDBConstant.TABLE_NOTICE, PopNoticeDBConstant.COLUMN_NOTICE_ID, popNoticeModel.getNoticeId())) {
                values.put(PopNoticeDBConstant.COLUMN_NOTICE_STATE, (Integer) 1);
                this.writableDatabase.insertOrThrow(PopNoticeDBConstant.TABLE_NOTICE, null, values);
                closeWriteableDB();
                return -1;
            }
            closeWriteableDB();
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return 0;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean deletePopNotice() {
        return removeAllEntries(PopNoticeDBConstant.TABLE_NOTICE);
    }

    public boolean deleteHeartBeatList(long noticeId) {
        try {
            openWriteableDB();
            this.writableDatabase.delete(PopNoticeDBConstant.TABLE_NOTICE, PopNoticeDBConstant.DELETE_TABLE_WHERE_NOTICE_ID, new String[]{new StringBuilder(String.valueOf(noticeId)).toString()});
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public static void changeUser(Context context) {
        getNewInstance(context);
    }
}
