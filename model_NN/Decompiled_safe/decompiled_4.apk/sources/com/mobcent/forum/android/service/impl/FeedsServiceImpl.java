package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.FeedsRestfulApiRequester;
import com.mobcent.forum.android.db.FeedsDBUtil;
import com.mobcent.forum.android.db.NewFeedsDBUtil;
import com.mobcent.forum.android.model.FeedsModel;
import com.mobcent.forum.android.model.NewFeedsModel;
import com.mobcent.forum.android.model.UserFeedModel;
import com.mobcent.forum.android.service.FeedsService;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.service.impl.helper.FeedsServiceImplHelper;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class FeedsServiceImpl implements FeedsService {
    private Context context;

    public FeedsServiceImpl(Context context2) {
        this.context = context2;
    }

    public List<FeedsModel> getFeedsByNet(int page, int pageSize) {
        String jsonStr = FeedsRestfulApiRequester.getFeedsList(this.context, page, pageSize);
        List<FeedsModel> trendsList = FeedsServiceImplHelper.getFeeds(jsonStr);
        if (trendsList == null) {
            trendsList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                FeedsModel feed = new FeedsModel();
                feed.setErrorCode(errorCode);
                trendsList.add(feed);
            }
        } else {
            FeedsDBUtil.getInstance(this.context).updateFeedsTopicJsonString(jsonStr);
        }
        return trendsList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public List<FeedsModel> getFeeds(int page, int pageSize, boolean isLocal) {
        String jsonStr;
        if (!isLocal) {
            return getFeedsByNet(page, pageSize);
        }
        try {
            jsonStr = FeedsDBUtil.getInstance(this.context).getFeedsTopicJsonString();
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
        }
        if (StringUtil.isEmpty(jsonStr)) {
            return getFeedsByNet(page, pageSize);
        }
        List<FeedsModel> feedsList = FeedsServiceImplHelper.getFeeds(jsonStr);
        if (feedsList == null || feedsList.size() <= 0) {
            return feedsList;
        }
        feedsList.get(0).setHasNext(0);
        return feedsList;
    }

    public List<NewFeedsModel> getNewFeedsByNet(long endTime) {
        String jsonStr = FeedsRestfulApiRequester.getNewFeedsList(this.context, endTime);
        List<NewFeedsModel> list = FeedsServiceImplHelper.getNewFeeds(jsonStr);
        if (list == null) {
            list = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                NewFeedsModel feed = new NewFeedsModel();
                feed.setErrorCode(errorCode);
                list.add(feed);
            }
        } else {
            NewFeedsDBUtil.getInstance(this.context).updateFeedsTopicJsonString(jsonStr);
        }
        return list;
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    public List<NewFeedsModel> getNewFeeds(long endTime, boolean isLocal) {
        String jsonStr;
        String[] result = null;
        if (!isLocal) {
            return getNewFeedsByNet(endTime);
        }
        try {
            result = NewFeedsDBUtil.getInstance(this.context).getFeedsTopicJsonString();
            jsonStr = result[0];
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
        }
        if (StringUtil.isEmpty(jsonStr)) {
            return getNewFeedsByNet(0);
        }
        List<NewFeedsModel> feedsList = FeedsServiceImplHelper.getNewFeeds(jsonStr);
        if (feedsList == null || feedsList.size() <= 0) {
            return feedsList;
        }
        feedsList.get(0).setTotalNum(0);
        feedsList.get(0).setHasNext(-1);
        feedsList.get(0).setLastUpdate(result[1]);
        return feedsList;
    }

    public UserFeedModel getUserFeed(long userId, int page, int pageSize) {
        String jsonStr = FeedsRestfulApiRequester.getUserFeed(this.context, userId, page, pageSize);
        UserFeedModel userFeedModel = FeedsServiceImplHelper.getUserFeeds(jsonStr);
        if (userFeedModel != null) {
            return userFeedModel;
        }
        String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
        if (StringUtil.isEmpty(errorCode)) {
            return userFeedModel;
        }
        UserFeedModel userFeedModel2 = new UserFeedModel();
        userFeedModel2.setErrorCode(errorCode);
        return userFeedModel2;
    }
}
