package com.mobcent.forum.android.db.constant;

public interface PermDBConstant {
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_JSON_STR = "jsonStr";
    public static final String SQL_CREATE_TABLE_PERM = "CREATE TABLE IF NOT EXISTS perm(id LONG PRIMARY KEY,jsonStr TEXT);";
    public static final String TABLE_PERM = "perm";
}
