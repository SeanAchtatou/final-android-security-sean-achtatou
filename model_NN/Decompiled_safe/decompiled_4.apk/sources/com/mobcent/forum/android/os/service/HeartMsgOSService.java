package com.mobcent.forum.android.os.service;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.IBinder;
import com.mobcent.forum.android.model.HeartMsgModel;
import com.mobcent.forum.android.service.HeartMsgService;
import com.mobcent.forum.android.service.impl.HeartMsgServiceImpl;
import com.mobcent.forum.android.util.DateUtil;
import com.mobcent.forum.android.util.MCLogUtil;
import java.util.List;

public class HeartMsgOSService extends Service {
    public static final String APP_ICON_GET_INTENT = "service_app_icon";
    public static final String APP_ICON_SEND_INTENT = "receiver_app_icon";
    public static final String APP_LAST_REQUEST_TIME_INTENT = "last_request_time";
    public static int HEART_BEAT_INTERVAL = 15000;
    public static final int HEART_BEAT_TIME_OUT = 600000;
    public static final int HEART_BEAT_TIME_OUT_45 = 2700000;
    public static final String HEART_MSG_LIST = "heartMsgList";
    public static boolean IS_HEART_BEAT_TIME_OUT = false;
    public static final String MOBILE_NET = "mobileNet";
    public static final String MSG_TOTAL_NUM = "msg_total_num";
    public static final String NET_CHECK = "netCheck";
    public static final String PUSH_NOTICE_NUM = "push_notice_num";
    public static final String RELATIONAL_NOTICE_NUM = "relational_notice_num";
    public static final String REPLY_NOTICE_NUM = "reply_notice_num";
    public static final String SERVER_NOTIFICATION_MSG = ".forum.service.heart.beat.notify";
    public String TAG = "HeartMsgOSService";
    private List<HeartMsgModel> heartMsgList;
    private HeartMsgService heartMsgService;
    /* access modifiers changed from: private */
    public HeartBeatThread heartMsgThread;
    private int icon = 0;
    private boolean isStopMonitorServer = false;
    /* access modifiers changed from: private */
    public long lastRequestTime = 0;
    /* access modifiers changed from: private */
    public int netTips = 0;

    public boolean isStopMonitorServer() {
        return this.isStopMonitorServer;
    }

    public void setStopMonitorServer(boolean isStopMonitorServer2) {
        this.isStopMonitorServer = isStopMonitorServer2;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        initService();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        if (intent != null) {
            this.icon = intent.getIntExtra(APP_ICON_GET_INTENT, 0);
        }
        startService();
    }

    public void onDestroy() {
        super.onDestroy();
        stopService();
    }

    private void initService() {
        this.heartMsgThread = new HeartBeatThread(this, null);
        this.heartMsgService = new HeartMsgServiceImpl(this);
    }

    private void startService() {
        if (HEART_BEAT_INTERVAL == 2700000 && this.heartMsgThread.isAlive()) {
            this.heartMsgThread.interrupt();
            this.heartMsgThread = new HeartBeatThread(this, null);
            this.heartMsgThread.start();
        } else if (!this.heartMsgThread.isAlive()) {
            try {
                this.heartMsgThread.start();
            } catch (Exception e) {
            }
        }
    }

    private void stopService() {
        setStopMonitorServer(true);
        this.heartMsgThread = null;
    }

    private class HeartBeatThread extends Thread {
        private HeartBeatThread() {
        }

        /* synthetic */ HeartBeatThread(HeartMsgOSService heartMsgOSService, HeartBeatThread heartBeatThread) {
            this();
        }

        public void run() {
            try {
                ConnectivityManager connectivityManager = (ConnectivityManager) HeartMsgOSService.this.getSystemService("connectivity");
                HeartMsgOSService.this.netTips = 0;
                while (!HeartMsgOSService.this.isStopMonitorServer() && HeartMsgOSService.this.heartMsgThread == Thread.currentThread()) {
                    if (HeartMsgOSService.this.isAction()) {
                        HeartMsgOSService.this.lastRequestTime = 0;
                        HeartMsgOSService.HEART_BEAT_INTERVAL = 15000;
                    } else if (HeartMsgOSService.this.lastRequestTime == 0) {
                        HeartMsgOSService.this.lastRequestTime = DateUtil.getCurrentTime();
                    }
                    if (HeartMsgOSService.this.lastRequestTime <= 0 || DateUtil.getCurrentTime() - HeartMsgOSService.this.lastRequestTime < 600000) {
                        HeartMsgOSService.IS_HEART_BEAT_TIME_OUT = false;
                    } else {
                        HeartMsgOSService.HEART_BEAT_INTERVAL = HeartMsgOSService.HEART_BEAT_TIME_OUT_45;
                        HeartMsgOSService.IS_HEART_BEAT_TIME_OUT = true;
                    }
                    HeartMsgOSService.this.getHeartBeatList();
                    Thread.sleep((long) HeartMsgOSService.HEART_BEAT_INTERVAL);
                }
            } catch (Exception e) {
                Exception e2 = e;
                e2.printStackTrace();
                MCLogUtil.e("e.printStackTrace();", e2.toString());
            }
        }
    }

    /* access modifiers changed from: private */
    public void getHeartBeatList() {
        int heartBeatTime;
        this.heartMsgList = this.heartMsgService.getHeartBeatListByNet();
        if (this.heartMsgList != null && !this.heartMsgList.isEmpty()) {
            if (!IS_HEART_BEAT_TIME_OUT && (heartBeatTime = this.heartMsgList.get(0).getHbTime()) > 0) {
                HEART_BEAT_INTERVAL = heartBeatTime;
            }
            int replyNoticeNum = this.heartMsgList.get(0).getReplyNoticeNum();
            int msgTotalNum = this.heartMsgList.get(0).getMsgTotalNum();
            int relationalNoticeNum = this.heartMsgList.get(0).getRelationalNoticeNum();
            int pushMsgNum = this.heartMsgList.get(0).getPushMsgNum();
            if (replyNoticeNum > 0 || msgTotalNum > 0 || relationalNoticeNum > 0 || pushMsgNum > 0) {
                boardcastToReceiver(replyNoticeNum, msgTotalNum, relationalNoticeNum, pushMsgNum);
            }
        }
    }

    private void boardcastToReceiver(int replyNoticeNum, int msgTotalNum, int relationalNoticeNum, int pushMsgNum) {
        Intent intent = new Intent(String.valueOf(getPackageName()) + SERVER_NOTIFICATION_MSG);
        intent.putExtra("reply_notice_num", replyNoticeNum);
        intent.putExtra("msg_total_num", msgTotalNum);
        intent.putExtra("relational_notice_num", relationalNoticeNum);
        intent.putExtra(PUSH_NOTICE_NUM, pushMsgNum);
        intent.putExtra(APP_ICON_SEND_INTENT, this.icon);
        sendBroadcast(intent);
    }

    public boolean isAction() {
        List<ActivityManager.RunningTaskInfo> tasks = ((ActivityManager) getApplicationContext().getSystemService("activity")).getRunningTasks(1);
        if (tasks == null || tasks.isEmpty() || !tasks.get(0).topActivity.getPackageName().equals(getApplicationContext().getPackageName())) {
            return false;
        }
        return true;
    }
}
