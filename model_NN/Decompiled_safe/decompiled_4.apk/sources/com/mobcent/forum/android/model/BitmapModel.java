package com.mobcent.forum.android.model;

public class BitmapModel extends BaseModel {
    private static final long serialVersionUID = 1488473808286348794L;
    private int height;
    private int width;

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width2) {
        this.width = width2;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height2) {
        this.height = height2;
    }
}
