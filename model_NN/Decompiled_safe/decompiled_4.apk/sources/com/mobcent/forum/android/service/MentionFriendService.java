package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.UserInfoModel;
import java.util.List;

public interface MentionFriendService {
    boolean addLocalForumMentionFriend(long j, UserInfoModel userInfoModel);

    boolean deletedLocalForumMentionFriend(long j, UserInfoModel userInfoModel);

    List<UserInfoModel> getForumMentionFriendByNet(long j);

    List<UserInfoModel> getFroumMentionFriend(long j);

    List<UserInfoModel> getOpenPlatformMentionFriend(long j, long j2);

    List<UserInfoModel> getOpenPlatformMentionFriendByNet(long j, long j2);
}
