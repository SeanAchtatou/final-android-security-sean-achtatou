package com.mobcent.forum.android.constant;

public interface MoreApplicationConstant extends BaseRestfulApiConstant {
    public static final String APK_URL = "apk_url";
    public static final int APP_DOWNLOAD = 3;
    public static final String APP_ICON_URL = "app_icon_url";
    public static final int APP_INSTALLED = 1;
    public static final String APP_LIST = "app_list";
    public static final String APP_NAME = "app_name";
    public static final int APP_OPEN = 2;
    public static final String APP_SIZE = "appSize";
    public static final String PACKAGE_NAME = "package_name";
    public static final String SHORT_DESC = "short_desc";
}
