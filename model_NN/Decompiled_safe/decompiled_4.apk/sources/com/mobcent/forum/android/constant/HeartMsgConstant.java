package com.mobcent.forum.android.constant;

public interface HeartMsgConstant {
    public static final String FROM_USER_ICON = "icon";
    public static final String FROM_USER_ID = "from_user_id";
    public static final String FROM_USER_NICKNAME = "from_user_nickname";
    public static final String HB_TIME = "hb_time";
    public static final String MESSAEG_LIST = "list";
    public static final String MESSAGE_CONTENT = "content";
    public static final String MESSAGE_CREATE_DATE = "create_date";
    public static final String MESSAGE_ID = "msg_id";
    public static final int MSG_AUDIO = 3;
    public static final int MSG_IMAGE = 2;
    public static final int MSG_TEXT = 1;
    public static final String MSG_TOTAL_NUM = "msg_total_num";
    public static final String PUSH = "push";
    public static final String PUSH_DETAIL_TYPE = "detail_type";
    public static final String PUSH_MSG_DESC = "desc";
    public static final String PUSH_MSG_ID = "push_msg_id";
    public static final String PUSH_MSG_TIELE = "title";
    public static final String PUSH_MSG_TYPE = "type";
    public static final String PUSH_TIPIC_ID = "topic_id";
    public static final String RELATIONAL_NOTICE_NUM = "relational_notice_num";
    public static final String REPLY_NOTICE_NUM = "reply_notice_num";
    public static final String SOUND_MODEL = "soundmodel";
    public static final String TIME = "time";
    public static final String TO_USER_ICON = "icon";
    public static final String TO_USER_ID = "to_user_id";
    public static final String TO_USER_NICKNAME = "to_user_nickname";
    public static final String TYPE = "type";
}
