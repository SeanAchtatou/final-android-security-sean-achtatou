package com.mobcent.forum.android.model;

import java.util.List;

public class BoardCategoryModel extends BaseModel {
    private static final long serialVersionUID = -5055264636829393501L;
    private long boardCategoryId;
    private String boardCategoryName;
    private int boardCategoryType;
    private List<BoardModel> boardList;
    private boolean isAd;

    public long getBoardCategoryId() {
        return this.boardCategoryId;
    }

    public void setBoardCategoryId(long boardCategoryId2) {
        this.boardCategoryId = boardCategoryId2;
    }

    public String getBoardCategoryName() {
        return this.boardCategoryName;
    }

    public void setBoardCategoryName(String boardCategoryName2) {
        this.boardCategoryName = boardCategoryName2;
    }

    public List<BoardModel> getBoardList() {
        return this.boardList;
    }

    public void setBoardList(List<BoardModel> boardList2) {
        this.boardList = boardList2;
    }

    public int getBoardCategoryType() {
        return this.boardCategoryType;
    }

    public void setBoardCategoryType(int boardCategoryType2) {
        this.boardCategoryType = boardCategoryType2;
    }

    public boolean isAd() {
        return this.isAd;
    }

    public void setAd(boolean isAd2) {
        this.isAd = isAd2;
    }
}
