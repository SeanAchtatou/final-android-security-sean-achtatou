package com.mobcent.forum.android.service.delegate;

public interface DownProgressDelegate {
    void setMax(int i);

    void setProgress(int i);
}
