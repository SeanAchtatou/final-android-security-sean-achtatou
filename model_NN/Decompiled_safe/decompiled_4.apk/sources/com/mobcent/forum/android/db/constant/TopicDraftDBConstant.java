package com.mobcent.forum.android.db.constant;

public interface TopicDraftDBConstant {
    public static final String COLOMN_BOARD_NAME = "boardName";
    public static final String COLOMN_CONTENT = "content";
    public static final String COLOMN_TITLE = "title";
    public static final String COLOMN_VOTE = "vote";
    public static final String COLUMN_BOARD_ID = "boardId";
    public static final String COLUMN_ID = "id";
    public static final long PUBLIC_ANNOUNCE_ID = 4;
    public static final long PUBLIC_TOPIC_ID = 1;
    public static final long PUBLIC_VOTE_ID = 2;
    public static final long REPLY_ID = 3;
    public static final String SQL_CREATE_TABLE_TOPIC_DRAFT = "CREATE TABLE IF NOT EXISTS topic_draft(id LONG PRIMARY KEY, boardId LONG , boardName TEXT , title TEXT , content TEXT , vote TEXT  );";
    public static final String TABLE_TOPIC_DRAFT = "topic_draft";
}
