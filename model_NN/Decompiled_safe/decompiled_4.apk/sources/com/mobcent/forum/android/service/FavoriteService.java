package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.TopicModel;
import java.util.List;

public interface FavoriteService {
    String addFavoriteTopic(long j, long j2);

    String delFavoriteTopic(long j, long j2);

    List<TopicModel> getUserFavoriteList(int i, int i2);
}
