package com.mobcent.forum.android.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.mobcent.forum.android.db.constant.SharedPreferencesDBConstant;
import com.mobcent.forum.android.model.HeartMsgModel;
import com.mobcent.forum.android.model.PayStateModel;
import com.mobcent.forum.android.util.StringUtil;

public class SharedPreferencesDB implements SharedPreferencesDBConstant {
    private static SharedPreferencesDB sharedPreferencesDB = null;
    private Context context;
    private SharedPreferences prefs = null;

    protected SharedPreferencesDB(Context cxt) {
        this.context = cxt;
        try {
            this.prefs = cxt.createPackageContext(cxt.getPackageName(), 2).getSharedPreferences(SharedPreferencesDBConstant.PREFS_FILE, 3);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static SharedPreferencesDB getInstance(Context context2) {
        if (sharedPreferencesDB == null) {
            sharedPreferencesDB = new SharedPreferencesDB(context2);
        }
        return sharedPreferencesDB;
    }

    public static void newInstance(Context context2) {
        sharedPreferencesDB = new SharedPreferencesDB(context2);
    }

    public String getForumKey() {
        String forumKey = this.prefs.getString("mc_forum_key", null);
        if (forumKey != null && !StringUtil.isEmpty(forumKey)) {
            return forumKey;
        }
        try {
            String forumKey2 = this.context.getPackageManager().getApplicationInfo(this.context.getPackageName(), AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER).metaData.getString("mc_forum_key");
            this.prefs.edit().putString("mc_forum_key", forumKey2).commit();
            return forumKey2;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    public long getChannelId() {
        try {
            return (long) this.context.getPackageManager().getApplicationInfo(this.context.getPackageName(), AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER).metaData.getInt(SharedPreferencesDBConstant.MC_CHANNEL_ID);
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }

    public int getForumid() {
        int forumId = this.prefs.getInt(SharedPreferencesDBConstant.MC_FORUM_ID, 0);
        if (forumId != 0) {
            return forumId;
        }
        try {
            int forumId2 = this.context.getPackageManager().getApplicationInfo(this.context.getPackageName(), AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER).metaData.getInt(SharedPreferencesDBConstant.MC_FORUM_ID);
            this.prefs.edit().putInt(SharedPreferencesDBConstant.MC_FORUM_ID, forumId2).commit();
            return forumId2;
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }

    public long getUserId() {
        return this.prefs.getLong("userId", 0);
    }

    public String getAccessToken() {
        return this.prefs.getString("accessToken", null);
    }

    public String getAccessSecret() {
        return this.prefs.getString("accessSecret", null);
    }

    public String getNickName() {
        return this.prefs.getString("nickname", "");
    }

    public int getRoleNum() {
        return this.prefs.getInt("role_num", 1);
    }

    public int getForumId() {
        return this.prefs.getInt("forumId", -1);
    }

    public void updateUserInfo(long userId, int forumId, String accessToken, String accessSecret, int roleNum) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putLong("userId", userId);
        editor.putInt("forumId", forumId);
        editor.putString("accessToken", accessToken);
        editor.putString("accessSecret", accessSecret);
        editor.putInt("role_num", roleNum);
        editor.commit();
        HeartMsgDBUtil.changeUser(this.context);
    }

    public void setNickName(String nickname) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString("nickname", nickname);
        editor.commit();
    }

    public void removeUserInfo() {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.remove("userId");
        editor.remove("nickname");
        editor.remove("accessToken");
        editor.remove("accessSecret");
        editor.commit();
    }

    public void removeUserAccess() {
        SharedPreferences.Editor editor = this.prefs.edit();
        if (this.prefs.getString("accessToken", null) != null) {
            editor.remove("accessToken");
        }
        if (this.prefs.getString("accessSecret", null) != null) {
            editor.remove("accessSecret");
        }
        editor.commit();
    }

    public boolean getReplyNotificationFlag(long userId) {
        return this.prefs.getBoolean("isNotifyReply_" + userId, true);
    }

    public void setReplyNotificationFlag(boolean isNotifyReply, long userId) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putBoolean("isNotifyReply_" + userId, isNotifyReply);
        editor.commit();
    }

    public boolean getMentionFriendNotificationFlag(long userId) {
        return this.prefs.getBoolean("isMentionFriendNotifyReply_" + userId, true);
    }

    public void setMentionFriendNotificationFlag(boolean isNotifyReply, long userId) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putBoolean("isMentionFriendNotifyReply_" + userId, isNotifyReply);
        editor.commit();
    }

    public boolean getMessageVoiceFlag(long userId) {
        return this.prefs.getBoolean("isMessageVoice_" + userId, true);
    }

    public void setMessageVoiceFlag(boolean isNotifyReply, long userId) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putBoolean("isMessageVoice_" + userId, isNotifyReply);
        editor.commit();
    }

    public long getListLastUpdateDate(String name) {
        return this.prefs.getLong(name, 0);
    }

    public void updateListLastUpdateDate(String name, long value) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putLong(name, value);
        editor.commit();
    }

    public long getLastReplyRemindId() {
        return this.prefs.getLong(SharedPreferencesDBConstant.LAST_REPLY_REMIND_ID, 0);
    }

    public void updateLastReplyRemindId(long value) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putLong(SharedPreferencesDBConstant.LAST_REPLY_REMIND_ID, value);
        editor.commit();
    }

    public void updateIsUserDBUpgrade(boolean b) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putBoolean(SharedPreferencesDBConstant.IS_USER_DB_UPGRADE, b);
        editor.commit();
    }

    public boolean isUserDBUpgrade() {
        return this.prefs.getBoolean(SharedPreferencesDBConstant.IS_USER_DB_UPGRADE, false);
    }

    public void saveHeartInfo(HeartMsgModel heartMsgModel, long currentUserId) {
        SharedPreferences.Editor editor = this.prefs.edit();
        HeartMsgModel heartModel = getHeartInfo(currentUserId);
        if (heartModel.getReplyNoticeNum() != heartMsgModel.getReplyNoticeNum()) {
            editor.putInt("replyNoticeNum_" + currentUserId, heartMsgModel.getReplyNoticeNum());
        }
        if (heartModel.getMsgTotalNum() != heartMsgModel.getMsgTotalNum()) {
            editor.putInt("msgTotalNum_" + currentUserId, heartMsgModel.getMsgTotalNum());
        }
        if (heartModel.getRelationalNoticeNum() != heartMsgModel.getRelationalNoticeNum()) {
            editor.putInt("relationalNoticeNum_" + currentUserId, heartMsgModel.getRelationalNoticeNum());
        }
        editor.commit();
    }

    public HeartMsgModel getHeartInfo(long currentUserId) {
        HeartMsgModel heartMsgModel = new HeartMsgModel();
        heartMsgModel.setReplyNoticeNum(this.prefs.getInt("replyNoticeNum_" + currentUserId, 0));
        heartMsgModel.setMsgTotalNum(this.prefs.getInt("msgTotalNum_" + currentUserId, 0));
        heartMsgModel.setRelationalNoticeNum(this.prefs.getInt("relationalNoticeNum_" + currentUserId, 0));
        return heartMsgModel;
    }

    public boolean getPicModeFlag() {
        if (((ConnectivityManager) this.context.getSystemService("connectivity")).getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        return this.prefs.getBoolean(SharedPreferencesDBConstant.PIC_MODE_TAG, true);
    }

    public void setPicModeFlag(boolean b) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putBoolean(SharedPreferencesDBConstant.PIC_MODE_TAG, b);
        editor.commit();
    }

    public void saveTopicDraftAudioTime(String audioName, int time) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putInt(audioName, time);
        editor.commit();
    }

    public int getTopicDraftAudioTime(String audioName) {
        return this.prefs.getInt(audioName, 0);
    }

    public void setPayState(PayStateModel payStateModel) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString("loadingpage_image", payStateModel.getLoadingPageImage());
        editor.putBoolean("adv", payStateModel.getAdv());
        editor.putBoolean("client_manager", payStateModel.getClientManager());
        editor.putBoolean("loadingpage", payStateModel.getLoadingPage());
        editor.putBoolean("msg_push", payStateModel.getMsgPush());
        editor.putBoolean("powerby", payStateModel.getPowerBy());
        editor.putBoolean("share_key", payStateModel.getShareKey());
        editor.putBoolean("square", payStateModel.getSquare());
        editor.putBoolean("watermark", payStateModel.getWaterMark());
        editor.putString("watermark_image", payStateModel.getWaterMarkImage());
        editor.putString("img_url", payStateModel.getImgUrl());
        editor.putString("weixin_appkey", payStateModel.getWeiXinAppKey());
        editor.commit();
    }

    public int getVersionCode() {
        try {
            return this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 16384).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return -1;
        }
    }

    public void setLeadViewVersionCode(int versionCode) {
        this.prefs.edit().putInt(SharedPreferencesDBConstant.VERSION_CODE, versionCode).commit();
    }

    public int getLeadViewVersionCode() {
        return this.prefs.getInt(SharedPreferencesDBConstant.VERSION_CODE, -1);
    }

    public boolean getPayStateAdv() {
        return this.prefs.getBoolean("adv", false);
    }

    public boolean getPayStateClientManager() {
        return this.prefs.getBoolean("client_manager", false);
    }

    public boolean getPayStateLoadingPage() {
        return this.prefs.getBoolean("loadingpage", false);
    }

    public String getPayStateLoadingPageStr() {
        if (this.prefs.getBoolean("loadingpage", false)) {
            return getPayStateLoadingPageImage();
        }
        return "";
    }

    public boolean getPayStateMsgPush() {
        return this.prefs.getBoolean("msg_push", false);
    }

    public boolean getPayStatePowerBy() {
        return this.prefs.getBoolean("powerby", false);
    }

    public boolean getPayStateShareKey() {
        return this.prefs.getBoolean("share_key", false);
    }

    public boolean getPayStateSquare() {
        return this.prefs.getBoolean("square", false);
    }

    public boolean getPayStateWaterMark() {
        return this.prefs.getBoolean("watermark", false);
    }

    public String getPayStateLoadingPageImage() {
        return this.prefs.getString("loadingpage_image", "");
    }

    public String getPayStateWaterMarkImage() {
        return this.prefs.getString("watermark_image", "");
    }

    public String getPayStateImgUrl() {
        return this.prefs.getString("img_url", "");
    }

    public void setPushMsgListJson(String json) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString(SharedPreferencesDBConstant.PUSH_LIST_JSON, json);
        editor.commit();
    }

    public String getPushMsgListJson() {
        return this.prefs.getString(SharedPreferencesDBConstant.PUSH_LIST_JSON, "");
    }

    public String getWeiXinAppKey() {
        return this.prefs.getString("weixin_appkey", "");
    }
}
