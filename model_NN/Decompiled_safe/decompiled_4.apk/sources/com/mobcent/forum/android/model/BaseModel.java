package com.mobcent.forum.android.model;

import java.io.Serializable;

public class BaseModel implements Serializable {
    private static final long serialVersionUID = 1;
    private String baseUrl;
    private String errorCode;
    private boolean hasNext;
    private String iconUrl;
    private long lastUpdateTime;
    private int page;
    private String pathOrContent;
    private int rs = 1;
    private int totalNum;

    public String getPathOrContent() {
        return this.pathOrContent;
    }

    public void setPathOrContent(String pathOrContent2) {
        this.pathOrContent = pathOrContent2;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode2) {
        this.errorCode = errorCode2;
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    public void setBaseUrl(String baseUrl2) {
        this.baseUrl = baseUrl2;
    }

    public int getTotalNum() {
        return this.totalNum;
    }

    public void setTotalNum(int totalNum2) {
        this.totalNum = totalNum2;
    }

    public int getRs() {
        return this.rs;
    }

    public void setRs(int rs2) {
        this.rs = rs2;
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(int page2) {
        this.page = page2;
    }

    public String getIconUrl() {
        return this.iconUrl;
    }

    public void setIconUrl(String iconUrl2) {
        this.iconUrl = iconUrl2;
    }

    public long getLastUpdateTime() {
        return this.lastUpdateTime;
    }

    public void setLastUpdateTime(long lastUpdateTime2) {
        this.lastUpdateTime = lastUpdateTime2;
    }

    public boolean isHasNext() {
        return this.hasNext;
    }

    public void setHasNext(boolean hasNext2) {
        this.hasNext = hasNext2;
    }
}
