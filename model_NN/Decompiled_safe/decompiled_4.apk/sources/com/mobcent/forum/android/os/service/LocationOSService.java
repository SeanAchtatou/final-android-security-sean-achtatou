package com.mobcent.forum.android.os.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.baidu.location.BDLocation;
import com.mobcent.forum.android.service.LocationService;
import com.mobcent.forum.android.service.impl.LocationServiceImpl;
import com.mobcent.forum.android.util.LocationUtil;
import java.util.List;

public class LocationOSService extends Service {
    private LocationUtil locationUtil = null;

    public void onCreate() {
        super.onCreate();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        initLocationUtil(this);
        this.locationUtil.startLocation();
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.locationUtil != null) {
            this.locationUtil.stopLocation();
        }
    }

    public void initLocationUtil(Context context) {
        final LocationService saveLocationService = new LocationServiceImpl(context);
        this.locationUtil = new LocationUtil(context, false, new LocationUtil.LocationDelegate() {
            public void locationResults(final BDLocation location) {
                if (location.getLocType() == 161 || location.getLocType() == 61) {
                    final LocationService locationService = saveLocationService;
                    new Thread(new Runnable() {
                        public void run() {
                            locationService.saveLocation(location.getLongitude(), location.getLatitude(), location.getAddrStr());
                        }
                    }).start();
                } else if (location.getLocType() != 62 && location.getLocType() != 167) {
                    location.getLocType();
                }
            }

            public void onReceivePoi(boolean hasPoi, List<String> list) {
            }
        });
    }
}
