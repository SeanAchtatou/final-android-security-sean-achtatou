package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.forum.android.constant.BaseRestfulApiConstant;
import com.mobcent.forum.android.constant.PostsApiConstant;
import com.mobcent.forum.android.model.AnnoModel;
import com.mobcent.forum.android.model.BaseModel;
import com.mobcent.forum.android.model.PollItemModel;
import com.mobcent.forum.android.model.PollTopicModel;
import com.mobcent.forum.android.model.PostsNoticeModel;
import com.mobcent.forum.android.model.RepostTopicModel;
import com.mobcent.forum.android.model.SoundModel;
import com.mobcent.forum.android.model.TopicContentModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PostsServiceImplHelper implements PostsApiConstant {
    /* JADX INFO: Multiple debug info for r3v0 org.json.JSONObject: [D('jobj' org.json.JSONObject), D('j' int)] */
    /* JADX INFO: Multiple debug info for r11v21 int: [D('annoModel' com.mobcent.forum.android.model.TopicModel), D('i' int)] */
    public static List<TopicModel> getTopicList(String jsonStr, long boardId) {
        ArrayList arrayList;
        ArrayList arrayList2 = new ArrayList();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String baseUrl = jsonObject.optString("img_url");
            try {
                JSONArray jsonArrayAnno = jsonObject.optJSONArray("anno_list");
                int j = jsonArrayAnno.length();
                for (int i = 0; i < j; i++) {
                    JSONObject jobjAnno = jsonArrayAnno.optJSONObject(i);
                    AnnoModel annoModel = new AnnoModel();
                    annoModel.setAnnoId((long) jobjAnno.optInt("announce_id"));
                    annoModel.setAuthor(jobjAnno.optString("author"));
                    annoModel.setBoardId(jobjAnno.optLong("board_id", boardId));
                    annoModel.setForumId((long) jobjAnno.optInt("forum_id"));
                    annoModel.setTitle(jobjAnno.optString("title"));
                    annoModel.setAnnoStartDate(jobjAnno.optLong("start_date"));
                    arrayList2.add(annoModel);
                }
            } catch (Exception e) {
            }
            try {
                JSONArray jsonArrayTopic = jsonObject.optJSONArray("list");
                int j2 = jsonArrayTopic.length();
                for (int i2 = 0; i2 < j2; i2++) {
                    JSONObject jobj = jsonArrayTopic.optJSONObject(i2);
                    TopicModel topicModel = new TopicModel();
                    topicModel.setBaseUrl(baseUrl);
                    topicModel.setTopicId(jobj.optLong("topic_id"));
                    topicModel.setTitle(jobj.optString("title"));
                    topicModel.setLastReplyDate(jobj.optLong("last_reply_date"));
                    topicModel.setHitCount(jobj.optInt(PostsApiConstant.HITS));
                    topicModel.setReplieCount(jobj.optInt("replies"));
                    topicModel.setUserId((long) jobj.optInt("user_id"));
                    topicModel.setUserNickName(jobj.optString("user_nick_name"));
                    topicModel.setType(jobj.optInt("type"));
                    topicModel.setPicPath(jobj.optString("pic_path"));
                    topicModel.setBoardId(jobj.optLong("board_id", boardId));
                    topicModel.setTop(jobj.optInt("top"));
                    topicModel.setBoardName(jobj.optString("board_name"));
                    topicModel.setHot(jobj.optInt("hot"));
                    topicModel.setEssence(jobj.optInt(PostsApiConstant.ESSENCE));
                    topicModel.setPoll(jobj.optString(PostsApiConstant.POLL));
                    topicModel.setStatus(jobj.optInt("status"));
                    topicModel.setUploadType(jobj.optInt(PostsApiConstant.UPLOAD_TYPE));
                    topicModel.setVisible(jobj.optInt(PostsApiConstant.VISIBLE));
                    topicModel.setRatio(1.0f / ((float) jobj.optDouble("ratio")));
                    topicModel.setThumbnailUrl(String.valueOf(baseUrl) + topicModel.getPicPath());
                    arrayList2.add(topicModel);
                }
                TopicModel firstTopicModel = (TopicModel) arrayList2.get(0);
                firstTopicModel.setTotalNum(jsonObject.optInt("total_num"));
                firstTopicModel.setPage(jsonObject.optInt("page"));
                firstTopicModel.setRs(jsonObject.optInt("rs"));
                firstTopicModel.setHasNext(jsonObject.optInt("has_next"));
                arrayList2.set(0, firstTopicModel);
                arrayList = arrayList2;
            } catch (Exception e2) {
                arrayList = null;
            }
            return arrayList;
        } catch (Exception e3) {
            return null;
        }
    }

    public static List<TopicModel> getUserTopicList(String jsonStr, long boardId) {
        List<TopicModel> topicList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String baseUrl = jsonObject.optString("img_url");
            try {
                JSONArray jsonArrayTopic = jsonObject.optJSONArray("list");
                int j = jsonArrayTopic.length();
                for (int i = 0; i < j; i++) {
                    JSONObject jobj = jsonArrayTopic.optJSONObject(i);
                    TopicModel topicModel = new TopicModel();
                    topicModel.setBaseUrl(baseUrl);
                    topicModel.setTopicId(jobj.optLong("topic_id"));
                    topicModel.setTitle(jobj.optString("title"));
                    topicModel.setLastReplyDate(jobj.optLong("last_reply_date"));
                    topicModel.setHitCount(jobj.optInt(PostsApiConstant.HITS));
                    topicModel.setReplieCount(jobj.optInt("replies"));
                    topicModel.setUserId((long) jobj.optInt("user_id"));
                    topicModel.setUserNickName(jobj.optString("user_nick_name"));
                    topicModel.setType(jobj.optInt("type"));
                    topicModel.setPicPath(jobj.optString("pic_path"));
                    topicModel.setBoardId(jobj.optLong("board_id", boardId));
                    topicModel.setTop(jobj.optInt("top"));
                    topicModel.setHot(jobj.optInt("hot"));
                    topicModel.setEssence(jobj.optInt(PostsApiConstant.ESSENCE));
                    topicModel.setUploadType(jobj.optInt(PostsApiConstant.UPLOAD_TYPE));
                    topicModel.setStatus(jobj.optInt("status"));
                    topicList.add(topicModel);
                }
                TopicModel firstTopicModel = (TopicModel) topicList.get(0);
                firstTopicModel.setHasNext(jsonObject.optInt("has_next"));
                firstTopicModel.setPage(jsonObject.optInt("page"));
                firstTopicModel.setRs(jsonObject.optInt("rs"));
                topicList.set(0, firstTopicModel);
            } catch (Exception e) {
                topicList = null;
            }
            return topicList;
        } catch (Exception e2) {
            Exception exc = e2;
            return null;
        }
    }

    public static List<TopicModel> searchTopicList(String jsonStr) {
        List<TopicModel> topicList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String baseUrl = jsonObject.optString("img_url");
            JSONArray jsonArray = jsonObject.optJSONArray("list");
            int j = jsonArray.length();
            for (int i = 0; i < j; i++) {
                JSONObject jsonObj = jsonArray.optJSONObject(i);
                TopicModel topicModel = new TopicModel();
                topicModel.setBaseUrl(baseUrl);
                topicModel.setTopicId(jsonObj.optLong("topic_id"));
                topicModel.setTitle(jsonObj.optString("title"));
                topicModel.setLastReplyDate(jsonObj.optLong("last_reply_date"));
                topicModel.setHitCount(jsonObj.optInt(PostsApiConstant.HITS));
                topicModel.setReplieCount(jsonObj.optInt("replies"));
                topicModel.setUserId((long) jsonObj.optInt("user_id"));
                topicModel.setUserNickName(jsonObj.optString("user_nick_name"));
                topicModel.setType(jsonObj.optInt("type"));
                topicModel.setPicPath(jsonObj.optString("pic_path"));
                topicModel.setBoardId(jsonObj.optLong("board_id"));
                topicModel.setStatus(jsonObj.optInt("status"));
                topicList.add(topicModel);
            }
            if (topicList.size() > 0) {
                TopicModel firstTopicModel = (TopicModel) topicList.get(0);
                firstTopicModel.setTotalNum(jsonObject.optInt("total_num"));
                firstTopicModel.setPage(jsonObject.optInt("page"));
                firstTopicModel.setRs(jsonObject.optInt("rs"));
                topicList.set(0, firstTopicModel);
            }
            return topicList;
        } catch (Exception e) {
            return null;
        }
    }

    public static List<TopicModel> getFavoriteTopicList(String jsonStr) {
        List<TopicModel> topicList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String baseUrl = jsonObject.optString("img_url");
            JSONArray jsonArray = jsonObject.optJSONArray("list");
            int j = jsonArray.length();
            for (int i = 0; i < j; i++) {
                JSONObject jobj = jsonArray.optJSONObject(i);
                TopicModel topicModel = new TopicModel();
                topicModel.setBaseUrl(baseUrl);
                topicModel.setTopicId(jobj.optLong("topic_id"));
                topicModel.setTitle(jobj.optString("title"));
                topicModel.setLastReplyDate(-1);
                topicModel.setHitCount(-1);
                topicModel.setReplieCount(-1);
                topicModel.setUserId((long) jobj.optInt("user_id"));
                topicModel.setUserNickName(jobj.optString("user_nick_name"));
                topicModel.setType(jobj.optInt("type"));
                topicModel.setPicPath(jobj.optString("pic_path"));
                topicModel.setBoardId(jobj.optLong("board_id"));
                topicModel.setUploadType(jobj.optInt(PostsApiConstant.UPLOAD_TYPE));
                topicModel.setStatus(jobj.optInt("status"));
                topicList.add(topicModel);
            }
            TopicModel firstTopicModel = (TopicModel) topicList.get(0);
            firstTopicModel.setHasNext(jsonObject.optInt("has_next"));
            firstTopicModel.setPage(jsonObject.optInt("page"));
            firstTopicModel.setRs(jsonObject.optInt("rs"));
            topicList.set(0, firstTopicModel);
            return topicList;
        } catch (Exception e) {
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r0v51 java.util.List: [D('jsonArray' org.json.JSONArray), D('list' java.util.List<com.mobcent.forum.android.model.TopicContentModel>)] */
    /* JADX INFO: Multiple debug info for r2v7 org.json.JSONObject: [D('pollJsonObject' org.json.JSONObject), D('jsonObject' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r2v11 com.mobcent.forum.android.model.TopicContentModel: [D('pollJsonObject' org.json.JSONObject), D('model' com.mobcent.forum.android.model.TopicContentModel)] */
    /*  JADX ERROR: NullPointerException in pass: CodeShrinkVisitor
        java.lang.NullPointerException
        	at jadx.core.dex.instructions.args.InsnArg.wrapInstruction(InsnArg.java:119)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.inline(CodeShrinkVisitor.java:145)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.shrinkBlock(CodeShrinkVisitor.java:71)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.shrinkMethod(CodeShrinkVisitor.java:43)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.visit(CodeShrinkVisitor.java:35)
        */
    public static java.util.List<com.mobcent.forum.android.model.PostsModel> getPosts(java.lang.String r15) {
        /*
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            r8 = 0
            r2 = 0
            java.lang.String r0 = ""
            java.lang.String r1 = ""
            java.lang.String r6 = ""
            r4 = 0
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x014f }
            r3.<init>(r15)     // Catch:{ Exception -> 0x014f }
            java.lang.String r15 = "img_url"
            java.lang.String r15 = r3.optString(r15)     // Catch:{ Exception -> 0x0251 }
            java.lang.String r0 = "icon_url"
            java.lang.String r0 = r3.optString(r0)     // Catch:{ Exception -> 0x0259 }
            java.lang.String r1 = "sound_url"
            java.lang.String r2 = r3.optString(r1)     // Catch:{ Exception -> 0x025e }
            java.lang.String r1 = "page"
            int r1 = r3.optInt(r1)     // Catch:{ Exception -> 0x0266 }
            r6 = r1
            r10 = r2
            r7 = r5
            r1 = r0
        L_0x002f:
            java.lang.String r0 = "topic"
            org.json.JSONObject r2 = r3.getJSONObject(r0)     // Catch:{ Exception -> 0x024d }
            com.mobcent.forum.android.model.TopicModel r5 = new com.mobcent.forum.android.model.TopicModel     // Catch:{ Exception -> 0x024d }
            r5.<init>()     // Catch:{ Exception -> 0x024d }
            java.lang.String r0 = "content"
            java.lang.String r0 = r2.optString(r0)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = r0.trim()     // Catch:{ Exception -> 0x0161 }
            r5.setContent(r0)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = "content_all"
            java.lang.String r0 = r2.optString(r0)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = r0.trim()     // Catch:{ Exception -> 0x0161 }
            r5.setContentAll(r0)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = "create_date"
            long r8 = r2.optLong(r0)     // Catch:{ Exception -> 0x0161 }
            r5.setCreateDate(r8)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = "title"
            java.lang.String r0 = r2.optString(r0)     // Catch:{ Exception -> 0x0161 }
            r5.setTitle(r0)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = "topic_id"
            long r8 = r2.optLong(r0)     // Catch:{ Exception -> 0x0161 }
            r5.setTopicId(r8)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = "user_id"
            long r8 = r2.optLong(r0)     // Catch:{ Exception -> 0x0161 }
            r5.setUserId(r8)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = "user_nick_name"
            java.lang.String r0 = r2.optString(r0)     // Catch:{ Exception -> 0x0161 }
            r5.setUserNickName(r0)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = "role_num"
            int r0 = r2.optInt(r0)     // Catch:{ Exception -> 0x0161 }
            r5.setRoleNum(r0)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = "icon"
            java.lang.String r0 = r2.optString(r0)     // Catch:{ Exception -> 0x0161 }
            r5.setIcon(r0)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = "is_favor"
            int r0 = r2.optInt(r0)     // Catch:{ Exception -> 0x0161 }
            r5.setIsFavor(r0)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = "status"
            int r0 = r2.optInt(r0)     // Catch:{ Exception -> 0x0161 }
            r5.setStatus(r0)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = "level"
            int r0 = r2.optInt(r0)     // Catch:{ Exception -> 0x0161 }
            r5.setLevel(r0)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = "is_shield"
            int r0 = r2.optInt(r0)     // Catch:{ Exception -> 0x0161 }
            r4 = 1
            if (r0 != r4) goto L_0x015b
            r0 = 1
            r5.setShieldedUser(r0)     // Catch:{ Exception -> 0x0161 }
        L_0x00bb:
            java.lang.String r0 = "is_gag"
            int r0 = r2.optInt(r0)     // Catch:{ Exception -> 0x0161 }
            r4 = 1
            if (r0 != r4) goto L_0x0168
            r0 = 1
            r5.setBannedUser(r0)     // Catch:{ Exception -> 0x0161 }
        L_0x00c8:
            java.lang.String r0 = "location"
            java.lang.String r0 = r2.optString(r0)     // Catch:{ Exception -> 0x0161 }
            r5.setLocation(r0)     // Catch:{ Exception -> 0x0161 }
            r5.setBaseUrl(r15)     // Catch:{ Exception -> 0x0161 }
            r5.setIconUrl(r1)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r0 = "content_all"
            org.json.JSONArray r0 = r2.optJSONArray(r0)     // Catch:{ Exception -> 0x0161 }
            java.util.List r0 = getTopicContent(r0, r15, r10)     // Catch:{ Exception -> 0x0161 }
            java.lang.String r4 = "poll_info"
            org.json.JSONObject r2 = r2.optJSONObject(r4)     // Catch:{ Exception -> 0x0161 }
            com.mobcent.forum.android.model.PollTopicModel r4 = getTopicPollInfo(r2)     // Catch:{ Exception -> 0x0161 }
            if (r4 == 0) goto L_0x010f
            if (r0 != 0) goto L_0x0105
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x0161 }
            r0.<init>()     // Catch:{ Exception -> 0x0161 }
            com.mobcent.forum.android.model.TopicContentModel r2 = new com.mobcent.forum.android.model.TopicContentModel     // Catch:{ Exception -> 0x0161 }
            r2.<init>()     // Catch:{ Exception -> 0x0161 }
            java.lang.String r8 = ""
            r2.setInfor(r8)     // Catch:{ Exception -> 0x0161 }
            r8 = 0
            r2.setType(r8)     // Catch:{ Exception -> 0x0161 }
            r0.add(r2)     // Catch:{ Exception -> 0x0161 }
        L_0x0105:
            r2 = 0
            java.lang.Object r2 = r0.get(r2)     // Catch:{ Exception -> 0x0161 }
            com.mobcent.forum.android.model.TopicContentModel r2 = (com.mobcent.forum.android.model.TopicContentModel) r2     // Catch:{ Exception -> 0x0161 }
            r2.setPollTopicModel(r4)     // Catch:{ Exception -> 0x0161 }
        L_0x010f:
            r5.setTopicContentList(r0)     // Catch:{ Exception -> 0x0161 }
            r0 = r5
        L_0x0113:
            if (r0 == 0) goto L_0x0124
            com.mobcent.forum.android.model.PostsModel r2 = new com.mobcent.forum.android.model.PostsModel
            r2.<init>()
            r2.setTopic(r0)
            r0 = 1
            r2.setPostType(r0)
            r7.add(r2)
        L_0x0124:
            java.lang.String r0 = "list"
            org.json.JSONArray r5 = r3.optJSONArray(r0)     // Catch:{ Exception -> 0x0244 }
            r0 = 0
            int r2 = r5.length()     // Catch:{ Exception -> 0x0244 }
        L_0x012f:
            if (r0 < r2) goto L_0x016e
            r15 = 0
            java.lang.Object r15 = r7.get(r15)     // Catch:{ Exception -> 0x0244 }
            com.mobcent.forum.android.model.PostsModel r15 = (com.mobcent.forum.android.model.PostsModel) r15     // Catch:{ Exception -> 0x0244 }
            java.lang.String r0 = "total_num"
            int r0 = r3.optInt(r0)     // Catch:{ Exception -> 0x0244 }
            r15.setTotalNum(r0)     // Catch:{ Exception -> 0x0244 }
            r15.setPage(r6)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r0 = "rs"
            int r0 = r3.optInt(r0)     // Catch:{ Exception -> 0x0244 }
            r15.setRs(r0)     // Catch:{ Exception -> 0x0244 }
            r15 = r7
        L_0x014e:
            return r15
        L_0x014f:
            r15 = move-exception
            r3 = r6
            r13 = r15
            r15 = r0
            r0 = r13
        L_0x0154:
            r0 = 0
            r6 = r4
            r10 = r3
            r7 = r0
            r3 = r2
            goto L_0x002f
        L_0x015b:
            r0 = 0
            r5.setShieldedUser(r0)     // Catch:{ Exception -> 0x0161 }
            goto L_0x00bb
        L_0x0161:
            r0 = move-exception
            r2 = r5
        L_0x0163:
            r0.printStackTrace()
            r0 = 0
            goto L_0x0113
        L_0x0168:
            r0 = 0
            r5.setBannedUser(r0)     // Catch:{ Exception -> 0x0161 }
            goto L_0x00c8
        L_0x016e:
            org.json.JSONObject r4 = r5.optJSONObject(r0)     // Catch:{ Exception -> 0x0244 }
            com.mobcent.forum.android.model.ReplyModel r9 = new com.mobcent.forum.android.model.ReplyModel     // Catch:{ Exception -> 0x0244 }
            r9.<init>()     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "posts_date"
            long r11 = r4.optLong(r8)     // Catch:{ Exception -> 0x0244 }
            r9.setPostsDate(r11)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "reply_content"
            java.lang.String r8 = r4.optString(r8)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = r8.trim()     // Catch:{ Exception -> 0x0244 }
            r9.setReplyContent(r8)     // Catch:{ Exception -> 0x0244 }
            java.util.List r8 = getReplyContent(r8, r15, r10, r9)     // Catch:{ Exception -> 0x0244 }
            r9.setReplyContentList(r8)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "reply_id"
            long r11 = r4.optLong(r8)     // Catch:{ Exception -> 0x0244 }
            r9.setReplyUserId(r11)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "reply_name"
            java.lang.String r8 = r4.optString(r8)     // Catch:{ Exception -> 0x0244 }
            r9.setUserNickName(r8)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "role_num"
            int r8 = r4.optInt(r8)     // Catch:{ Exception -> 0x0244 }
            r9.setRoleNum(r8)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "reply_posts_id"
            int r8 = r4.optInt(r8)     // Catch:{ Exception -> 0x0244 }
            long r11 = (long) r8     // Catch:{ Exception -> 0x0244 }
            r9.setReplyPostsId(r11)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "title"
            java.lang.String r8 = r4.optString(r8)     // Catch:{ Exception -> 0x0244 }
            r9.setTitle(r8)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "icon"
            java.lang.String r8 = r4.optString(r8)     // Catch:{ Exception -> 0x0244 }
            r9.setIcon(r8)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "is_quote"
            boolean r8 = r4.optBoolean(r8)     // Catch:{ Exception -> 0x0244 }
            r9.setQuote(r8)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "quote_content"
            java.lang.String r8 = r4.optString(r8)     // Catch:{ Exception -> 0x0244 }
            r9.setQuoteContent(r8)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "quote_user_name"
            java.lang.String r8 = r4.optString(r8)     // Catch:{ Exception -> 0x0244 }
            r9.setQuoteUserName(r8)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "status"
            int r8 = r4.optInt(r8)     // Catch:{ Exception -> 0x0244 }
            r9.setStatus(r8)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "level"
            int r8 = r4.optInt(r8)     // Catch:{ Exception -> 0x0244 }
            r9.setLevel(r8)     // Catch:{ Exception -> 0x0244 }
            r8 = 1
            int r8 = r6 - r8
            int r8 = r8 * 20
            int r8 = r8 + r0
            int r8 = r8 + 1
            r9.setFloor(r8)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "is_shield"
            int r8 = r4.optInt(r8)     // Catch:{ Exception -> 0x0244 }
            r11 = 1
            if (r8 != r11) goto L_0x023f
            r8 = 1
            r9.setShieldedUser(r8)     // Catch:{ Exception -> 0x0244 }
        L_0x0210:
            java.lang.String r8 = "is_gag"
            int r8 = r4.optInt(r8)     // Catch:{ Exception -> 0x0244 }
            r11 = 1
            if (r8 != r11) goto L_0x0248
            r8 = 1
            r9.setBannedUser(r8)     // Catch:{ Exception -> 0x0244 }
        L_0x021d:
            r9.setBaseUrl(r15)     // Catch:{ Exception -> 0x0244 }
            r9.setIconUrl(r1)     // Catch:{ Exception -> 0x0244 }
            java.lang.String r8 = "location"
            java.lang.String r4 = r4.optString(r8)     // Catch:{ Exception -> 0x0244 }
            r9.setLocation(r4)     // Catch:{ Exception -> 0x0244 }
            com.mobcent.forum.android.model.PostsModel r4 = new com.mobcent.forum.android.model.PostsModel     // Catch:{ Exception -> 0x0244 }
            r4.<init>()     // Catch:{ Exception -> 0x0244 }
            r4.setReply(r9)     // Catch:{ Exception -> 0x0244 }
            r8 = 0
            r4.setPostType(r8)     // Catch:{ Exception -> 0x0244 }
            r7.add(r4)     // Catch:{ Exception -> 0x0244 }
            int r0 = r0 + 1
            goto L_0x012f
        L_0x023f:
            r8 = 0
            r9.setShieldedUser(r8)     // Catch:{ Exception -> 0x0244 }
            goto L_0x0210
        L_0x0244:
            r15 = move-exception
            r15 = 0
            goto L_0x014e
        L_0x0248:
            r8 = 0
            r9.setBannedUser(r8)     // Catch:{ Exception -> 0x0244 }
            goto L_0x021d
        L_0x024d:
            r0 = move-exception
            r2 = r8
            goto L_0x0163
        L_0x0251:
            r15 = move-exception
            r2 = r3
            r3 = r6
            r13 = r0
            r0 = r15
            r15 = r13
            goto L_0x0154
        L_0x0259:
            r0 = move-exception
            r2 = r3
            r3 = r6
            goto L_0x0154
        L_0x025e:
            r1 = move-exception
            r2 = r3
            r3 = r6
            r13 = r0
            r0 = r1
            r1 = r13
            goto L_0x0154
        L_0x0266:
            r1 = move-exception
            r13 = r1
            r1 = r0
            r0 = r13
            r14 = r2
            r2 = r3
            r3 = r14
            goto L_0x0154
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.forum.android.service.impl.helper.PostsServiceImplHelper.getPosts(java.lang.String):java.util.List");
    }

    private static RepostTopicModel getRepostTopic(JSONObject object, String baseURL, String soundURL) {
        JSONException e;
        try {
            RepostTopicModel repostTopicModel = new RepostTopicModel();
            try {
                repostTopicModel.setBoardId(object.optLong("board_id"));
                repostTopicModel.setBoardName(object.optString("board_name"));
                repostTopicModel.setNickName(object.optString("nickname"));
                repostTopicModel.setRepostTime(object.optLong(PostsApiConstant.FORWARD_TIME));
                JSONArray array = object.getJSONArray("content");
                List<TopicContentModel> list = new ArrayList<>();
                for (int i = 0; i < array.length(); i++) {
                    TopicContentModel contentModel = new TopicContentModel();
                    JSONObject object2 = (JSONObject) array.get(i);
                    contentModel.setInfor(object2.optString("infor").trim());
                    contentModel.setType(object2.optInt("type"));
                    if (contentModel.getType() == 1) {
                        if (contentModel.getInfor().indexOf("http://") == -1) {
                            contentModel.setBaseUrl(baseURL);
                        }
                    } else if (contentModel.getType() == 5) {
                        SoundModel soundModel = new SoundModel();
                        soundModel.setSoundPath(String.valueOf(soundURL) + contentModel.getInfor());
                        soundModel.setSoundTime((long) object2.optInt("desc"));
                        contentModel.setSoundModel(soundModel);
                    }
                    list.add(contentModel);
                }
                repostTopicModel.setRepostContentList(list);
                return repostTopicModel;
            } catch (JSONException e2) {
                e = e2;
                e.printStackTrace();
                return null;
            }
        } catch (JSONException e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r4v48 java.util.List: [D('jsonArray' org.json.JSONArray), D('list' java.util.List<com.mobcent.forum.android.model.TopicContentModel>)] */
    /* JADX INFO: Multiple debug info for r6v7 org.json.JSONObject: [D('pollJsonObject' org.json.JSONObject), D('jsonObject' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r6v11 com.mobcent.forum.android.model.TopicContentModel: [D('pollJsonObject' org.json.JSONObject), D('model' com.mobcent.forum.android.model.TopicContentModel)] */
    /*  JADX ERROR: NullPointerException in pass: CodeShrinkVisitor
        java.lang.NullPointerException
        	at jadx.core.dex.instructions.args.InsnArg.wrapInstruction(InsnArg.java:119)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.inline(CodeShrinkVisitor.java:145)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.shrinkBlock(CodeShrinkVisitor.java:71)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.shrinkMethod(CodeShrinkVisitor.java:43)
        	at jadx.core.dex.visitors.shrink.CodeShrinkVisitor.visit(CodeShrinkVisitor.java:35)
        */
    public static java.util.List<com.mobcent.forum.android.model.PostsModel> getPostsWithFriend(java.lang.String r19, java.util.List<com.mobcent.forum.android.model.UserInfoModel> r20) {
        /*
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            r12 = 0
            r6 = 0
            java.lang.String r4 = ""
            java.lang.String r5 = ""
            java.lang.String r10 = ""
            r8 = 0
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ Exception -> 0x0178 }
            r0 = r7
            r1 = r19
            r0.<init>(r1)     // Catch:{ Exception -> 0x0178 }
            java.lang.String r19 = "img_url"
            r0 = r7
            r1 = r19
            java.lang.String r19 = r0.optString(r1)     // Catch:{ Exception -> 0x029e }
            java.lang.String r4 = "icon_url"
            java.lang.String r4 = r7.optString(r4)     // Catch:{ Exception -> 0x02a9 }
            java.lang.String r5 = "sound_url"
            java.lang.String r6 = r7.optString(r5)     // Catch:{ Exception -> 0x02ae }
            java.lang.String r5 = "page"
            int r5 = r7.optInt(r5)     // Catch:{ Exception -> 0x02b8 }
            r10 = r5
            r14 = r6
            r11 = r9
            r5 = r4
        L_0x0035:
            java.lang.String r4 = "topic"
            org.json.JSONObject r6 = r7.getJSONObject(r4)     // Catch:{ Exception -> 0x029a }
            com.mobcent.forum.android.model.TopicModel r9 = new com.mobcent.forum.android.model.TopicModel     // Catch:{ Exception -> 0x029a }
            r9.<init>()     // Catch:{ Exception -> 0x029a }
            java.lang.String r4 = "content"
            java.lang.String r4 = r6.optString(r4)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = r4.trim()     // Catch:{ Exception -> 0x018d }
            r9.setContent(r4)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "content_all"
            java.lang.String r4 = r6.optString(r4)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = r4.trim()     // Catch:{ Exception -> 0x018d }
            r9.setContentAll(r4)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "create_date"
            long r12 = r6.optLong(r4)     // Catch:{ Exception -> 0x018d }
            r9.setCreateDate(r12)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "title"
            java.lang.String r4 = r6.optString(r4)     // Catch:{ Exception -> 0x018d }
            r9.setTitle(r4)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "topic_id"
            long r12 = r6.optLong(r4)     // Catch:{ Exception -> 0x018d }
            r9.setTopicId(r12)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "user_id"
            long r12 = r6.optLong(r4)     // Catch:{ Exception -> 0x018d }
            r9.setUserId(r12)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "user_nick_name"
            java.lang.String r4 = r6.optString(r4)     // Catch:{ Exception -> 0x018d }
            r9.setUserNickName(r4)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "role_num"
            int r4 = r6.optInt(r4)     // Catch:{ Exception -> 0x018d }
            r9.setRoleNum(r4)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "icon"
            java.lang.String r4 = r6.optString(r4)     // Catch:{ Exception -> 0x018d }
            r9.setIcon(r4)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "is_favor"
            int r4 = r6.optInt(r4)     // Catch:{ Exception -> 0x018d }
            r9.setIsFavor(r4)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "level"
            int r4 = r6.optInt(r4)     // Catch:{ Exception -> 0x018d }
            r9.setLevel(r4)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "status"
            int r4 = r6.optInt(r4)     // Catch:{ Exception -> 0x018d }
            r9.setStatus(r4)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "is_shield"
            int r4 = r6.optInt(r4)     // Catch:{ Exception -> 0x018d }
            r8 = 1
            if (r4 != r8) goto L_0x0187
            r4 = 1
            r9.setShieldedUser(r4)     // Catch:{ Exception -> 0x018d }
        L_0x00c1:
            java.lang.String r4 = "is_gag"
            int r4 = r6.optInt(r4)     // Catch:{ Exception -> 0x018d }
            r8 = 1
            if (r4 != r8) goto L_0x0194
            r4 = 1
            r9.setBannedUser(r4)     // Catch:{ Exception -> 0x018d }
        L_0x00ce:
            r0 = r9
            r1 = r19
            r0.setBaseUrl(r1)     // Catch:{ Exception -> 0x018d }
            r9.setIconUrl(r5)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "location"
            java.lang.String r4 = r6.optString(r4)     // Catch:{ Exception -> 0x018d }
            r9.setLocation(r4)     // Catch:{ Exception -> 0x018d }
            long r12 = r9.getUserId()     // Catch:{ Exception -> 0x018d }
            r0 = r20
            r1 = r12
            boolean r4 = isUserFollow(r0, r1)     // Catch:{ Exception -> 0x018d }
            r9.setFollow(r4)     // Catch:{ Exception -> 0x018d }
            java.lang.String r4 = "content_all"
            org.json.JSONArray r4 = r6.optJSONArray(r4)     // Catch:{ Exception -> 0x018d }
            r0 = r4
            r1 = r19
            r2 = r14
            java.util.List r4 = getTopicContent(r0, r1, r2)     // Catch:{ Exception -> 0x018d }
            java.lang.String r8 = "poll_info"
            org.json.JSONObject r6 = r6.optJSONObject(r8)     // Catch:{ Exception -> 0x018d }
            com.mobcent.forum.android.model.PollTopicModel r8 = getTopicPollInfo(r6)     // Catch:{ Exception -> 0x018d }
            if (r8 == 0) goto L_0x012a
            if (r4 != 0) goto L_0x0120
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ Exception -> 0x018d }
            r4.<init>()     // Catch:{ Exception -> 0x018d }
            com.mobcent.forum.android.model.TopicContentModel r6 = new com.mobcent.forum.android.model.TopicContentModel     // Catch:{ Exception -> 0x018d }
            r6.<init>()     // Catch:{ Exception -> 0x018d }
            java.lang.String r12 = ""
            r6.setInfor(r12)     // Catch:{ Exception -> 0x018d }
            r12 = 0
            r6.setType(r12)     // Catch:{ Exception -> 0x018d }
            r4.add(r6)     // Catch:{ Exception -> 0x018d }
        L_0x0120:
            r6 = 0
            java.lang.Object r6 = r4.get(r6)     // Catch:{ Exception -> 0x018d }
            com.mobcent.forum.android.model.TopicContentModel r6 = (com.mobcent.forum.android.model.TopicContentModel) r6     // Catch:{ Exception -> 0x018d }
            r6.setPollTopicModel(r8)     // Catch:{ Exception -> 0x018d }
        L_0x012a:
            r9.setTopicContentList(r4)     // Catch:{ Exception -> 0x018d }
            r4 = r9
        L_0x012e:
            if (r4 == 0) goto L_0x013f
            com.mobcent.forum.android.model.PostsModel r6 = new com.mobcent.forum.android.model.PostsModel
            r6.<init>()
            r6.setTopic(r4)
            r4 = 1
            r6.setPostType(r4)
            r11.add(r6)
        L_0x013f:
            java.lang.String r4 = "list"
            org.json.JSONArray r9 = r7.optJSONArray(r4)     // Catch:{ Exception -> 0x0290 }
            r4 = 0
            int r6 = r9.length()     // Catch:{ Exception -> 0x0290 }
        L_0x014a:
            if (r4 < r6) goto L_0x019a
            r19 = 0
            r0 = r11
            r1 = r19
            java.lang.Object r19 = r0.get(r1)     // Catch:{ Exception -> 0x0290 }
            com.mobcent.forum.android.model.PostsModel r19 = (com.mobcent.forum.android.model.PostsModel) r19     // Catch:{ Exception -> 0x0290 }
            java.lang.String r20 = "total_num"
            r0 = r7
            r1 = r20
            int r20 = r0.optInt(r1)     // Catch:{ Exception -> 0x0290 }
            r19.setTotalNum(r20)     // Catch:{ Exception -> 0x0290 }
            r0 = r19
            r1 = r10
            r0.setPage(r1)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r20 = "rs"
            r0 = r7
            r1 = r20
            int r20 = r0.optInt(r1)     // Catch:{ Exception -> 0x0290 }
            r19.setRs(r20)     // Catch:{ Exception -> 0x0290 }
            r19 = r11
        L_0x0177:
            return r19
        L_0x0178:
            r19 = move-exception
            r7 = r10
            r17 = r19
            r19 = r4
            r4 = r17
        L_0x0180:
            r4 = 0
            r10 = r8
            r14 = r7
            r11 = r4
            r7 = r6
            goto L_0x0035
        L_0x0187:
            r4 = 0
            r9.setShieldedUser(r4)     // Catch:{ Exception -> 0x018d }
            goto L_0x00c1
        L_0x018d:
            r4 = move-exception
            r6 = r9
        L_0x018f:
            r4.printStackTrace()
            r4 = 0
            goto L_0x012e
        L_0x0194:
            r4 = 0
            r9.setBannedUser(r4)     // Catch:{ Exception -> 0x018d }
            goto L_0x00ce
        L_0x019a:
            org.json.JSONObject r8 = r9.optJSONObject(r4)     // Catch:{ Exception -> 0x0290 }
            com.mobcent.forum.android.model.ReplyModel r13 = new com.mobcent.forum.android.model.ReplyModel     // Catch:{ Exception -> 0x0290 }
            r13.<init>()     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "posts_date"
            long r15 = r8.optLong(r12)     // Catch:{ Exception -> 0x0290 }
            r0 = r13
            r1 = r15
            r0.setPostsDate(r1)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "reply_content"
            java.lang.String r12 = r8.optString(r12)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = r12.trim()     // Catch:{ Exception -> 0x0290 }
            r13.setReplyContent(r12)     // Catch:{ Exception -> 0x0290 }
            r0 = r12
            r1 = r19
            r2 = r14
            r3 = r13
            java.util.List r12 = getReplyContent(r0, r1, r2, r3)     // Catch:{ Exception -> 0x0290 }
            r13.setReplyContentList(r12)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "reply_id"
            long r15 = r8.optLong(r12)     // Catch:{ Exception -> 0x0290 }
            r0 = r13
            r1 = r15
            r0.setReplyUserId(r1)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "reply_name"
            java.lang.String r12 = r8.optString(r12)     // Catch:{ Exception -> 0x0290 }
            r13.setUserNickName(r12)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "role_num"
            int r12 = r8.optInt(r12)     // Catch:{ Exception -> 0x0290 }
            r13.setRoleNum(r12)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "reply_posts_id"
            int r12 = r8.optInt(r12)     // Catch:{ Exception -> 0x0290 }
            long r15 = (long) r12     // Catch:{ Exception -> 0x0290 }
            r0 = r13
            r1 = r15
            r0.setReplyPostsId(r1)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "title"
            java.lang.String r12 = r8.optString(r12)     // Catch:{ Exception -> 0x0290 }
            r13.setTitle(r12)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "icon"
            java.lang.String r12 = r8.optString(r12)     // Catch:{ Exception -> 0x0290 }
            r13.setIcon(r12)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "is_quote"
            boolean r12 = r8.optBoolean(r12)     // Catch:{ Exception -> 0x0290 }
            r13.setQuote(r12)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "quote_content"
            java.lang.String r12 = r8.optString(r12)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = r12.trim()     // Catch:{ Exception -> 0x0290 }
            r13.setQuoteContent(r12)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "quote_user_name"
            java.lang.String r12 = r8.optString(r12)     // Catch:{ Exception -> 0x0290 }
            r13.setQuoteUserName(r12)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "status"
            int r12 = r8.optInt(r12)     // Catch:{ Exception -> 0x0290 }
            r13.setStatus(r12)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "level"
            int r12 = r8.optInt(r12)     // Catch:{ Exception -> 0x0290 }
            r13.setLevel(r12)     // Catch:{ Exception -> 0x0290 }
            r12 = 1
            int r12 = r10 - r12
            int r12 = r12 * 20
            int r12 = r12 + r4
            int r12 = r12 + 1
            r13.setFloor(r12)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "is_shield"
            int r12 = r8.optInt(r12)     // Catch:{ Exception -> 0x0290 }
            r15 = 1
            if (r12 != r15) goto L_0x028b
            r12 = 1
            r13.setShieldedUser(r12)     // Catch:{ Exception -> 0x0290 }
        L_0x024b:
            java.lang.String r12 = "is_gag"
            int r12 = r8.optInt(r12)     // Catch:{ Exception -> 0x0290 }
            r15 = 1
            if (r12 != r15) goto L_0x0295
            r12 = 1
            r13.setBannedUser(r12)     // Catch:{ Exception -> 0x0290 }
        L_0x0258:
            r0 = r13
            r1 = r19
            r0.setBaseUrl(r1)     // Catch:{ Exception -> 0x0290 }
            r13.setIconUrl(r5)     // Catch:{ Exception -> 0x0290 }
            java.lang.String r12 = "location"
            java.lang.String r8 = r8.optString(r12)     // Catch:{ Exception -> 0x0290 }
            r13.setLocation(r8)     // Catch:{ Exception -> 0x0290 }
            long r15 = r13.getReplyUserId()     // Catch:{ Exception -> 0x0290 }
            r0 = r20
            r1 = r15
            boolean r8 = isUserFollow(r0, r1)     // Catch:{ Exception -> 0x0290 }
            r13.setFollow(r8)     // Catch:{ Exception -> 0x0290 }
            com.mobcent.forum.android.model.PostsModel r8 = new com.mobcent.forum.android.model.PostsModel     // Catch:{ Exception -> 0x0290 }
            r8.<init>()     // Catch:{ Exception -> 0x0290 }
            r8.setReply(r13)     // Catch:{ Exception -> 0x0290 }
            r12 = 0
            r8.setPostType(r12)     // Catch:{ Exception -> 0x0290 }
            r11.add(r8)     // Catch:{ Exception -> 0x0290 }
            int r4 = r4 + 1
            goto L_0x014a
        L_0x028b:
            r12 = 0
            r13.setShieldedUser(r12)     // Catch:{ Exception -> 0x0290 }
            goto L_0x024b
        L_0x0290:
            r19 = move-exception
            r19 = 0
            goto L_0x0177
        L_0x0295:
            r12 = 0
            r13.setBannedUser(r12)     // Catch:{ Exception -> 0x0290 }
            goto L_0x0258
        L_0x029a:
            r4 = move-exception
            r6 = r12
            goto L_0x018f
        L_0x029e:
            r19 = move-exception
            r6 = r7
            r7 = r10
            r17 = r4
            r4 = r19
            r19 = r17
            goto L_0x0180
        L_0x02a9:
            r4 = move-exception
            r6 = r7
            r7 = r10
            goto L_0x0180
        L_0x02ae:
            r5 = move-exception
            r6 = r7
            r7 = r10
            r17 = r4
            r4 = r5
            r5 = r17
            goto L_0x0180
        L_0x02b8:
            r5 = move-exception
            r17 = r5
            r5 = r4
            r4 = r17
            r18 = r6
            r6 = r7
            r7 = r18
            goto L_0x0180
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.forum.android.service.impl.helper.PostsServiceImplHelper.getPostsWithFriend(java.lang.String, java.util.List):java.util.List");
    }

    private static boolean isUserFollow(List<UserInfoModel> friendList, long userId) {
        for (int i = 0; i < friendList.size(); i++) {
            if (friendList.get(i).getUserId() == userId) {
                return true;
            }
        }
        return false;
    }

    /* JADX INFO: Multiple debug info for r0v12 com.mobcent.forum.android.model.SoundModel: [D('soundModel' com.mobcent.forum.android.model.SoundModel), D('info' java.lang.String)] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0018  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<com.mobcent.forum.android.model.TopicContentModel> getReplyContent(java.lang.String r9, java.lang.String r10, java.lang.String r11, com.mobcent.forum.android.model.ReplyModel r12) {
        /*
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.lang.String r0 = ""
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ Exception -> 0x00bc }
            r3.<init>(r9)     // Catch:{ Exception -> 0x00bc }
            r9 = 0
            int r1 = r3.length()     // Catch:{ Exception -> 0x00bc }
            r5 = r0
        L_0x0012:
            if (r9 < r1) goto L_0x002e
            r10 = r5
            r9 = r4
        L_0x0016:
            if (r12 == 0) goto L_0x002d
            int r11 = r10.length()
            r0 = 1
            if (r11 < r0) goto L_0x002a
            r11 = 0
            int r0 = r10.length()
            r1 = 1
            int r0 = r0 - r1
            java.lang.String r10 = r10.substring(r11, r0)
        L_0x002a:
            r12.setShortContent(r10)
        L_0x002d:
            return r9
        L_0x002e:
            org.json.JSONObject r2 = r3.optJSONObject(r9)     // Catch:{ Exception -> 0x00c1 }
            com.mobcent.forum.android.model.TopicContentModel r6 = new com.mobcent.forum.android.model.TopicContentModel     // Catch:{ Exception -> 0x00c1 }
            r6.<init>()     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r0 = "infor"
            java.lang.String r0 = r2.optString(r0)     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r0 = r0.trim()     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r7 = "type"
            int r7 = r2.optInt(r7)     // Catch:{ Exception -> 0x00c1 }
            r6.setType(r7)     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r7 = "infor"
            java.lang.String r7 = r2.optString(r7)     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r7 = r7.trim()     // Catch:{ Exception -> 0x00c1 }
            r6.setInfor(r7)     // Catch:{ Exception -> 0x00c1 }
            int r7 = r6.getType()     // Catch:{ Exception -> 0x00c1 }
            r8 = 1
            if (r7 != r8) goto L_0x006f
            r6.setBaseUrl(r10)     // Catch:{ Exception -> 0x00c1 }
            if (r12 == 0) goto L_0x00c4
            r0 = 1
            r12.setHasImg(r0)     // Catch:{ Exception -> 0x00c1 }
            r0 = r5
        L_0x0068:
            r4.add(r6)     // Catch:{ Exception -> 0x00bc }
            int r9 = r9 + 1
            r5 = r0
            goto L_0x0012
        L_0x006f:
            int r7 = r6.getType()     // Catch:{ Exception -> 0x00c1 }
            r8 = 5
            if (r7 != r8) goto L_0x00a2
            com.mobcent.forum.android.model.SoundModel r0 = new com.mobcent.forum.android.model.SoundModel     // Catch:{ Exception -> 0x00c1 }
            r0.<init>()     // Catch:{ Exception -> 0x00c1 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r8 = java.lang.String.valueOf(r11)     // Catch:{ Exception -> 0x00c1 }
            r7.<init>(r8)     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r8 = r6.getInfor()     // Catch:{ Exception -> 0x00c1 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x00c1 }
            r0.setSoundPath(r7)     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r7 = "desc"
            int r2 = r2.optInt(r7)     // Catch:{ Exception -> 0x00c1 }
            long r7 = (long) r2     // Catch:{ Exception -> 0x00c1 }
            r0.setSoundTime(r7)     // Catch:{ Exception -> 0x00c1 }
            r6.setSoundModel(r0)     // Catch:{ Exception -> 0x00c1 }
            r0 = r5
            goto L_0x0068
        L_0x00a2:
            if (r12 == 0) goto L_0x00c4
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r7 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x00c1 }
            r2.<init>(r7)     // Catch:{ Exception -> 0x00c1 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r2 = ","
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00c1 }
            goto L_0x0068
        L_0x00bc:
            r9 = move-exception
            r10 = r0
        L_0x00be:
            r9 = 0
            goto L_0x0016
        L_0x00c1:
            r9 = move-exception
            r10 = r5
            goto L_0x00be
        L_0x00c4:
            r0 = r5
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.forum.android.service.impl.helper.PostsServiceImplHelper.getReplyContent(java.lang.String, java.lang.String, java.lang.String, com.mobcent.forum.android.model.ReplyModel):java.util.List");
    }

    public static List<TopicContentModel> getTopicContent(String jsonStr) {
        Exception e;
        new ArrayList();
        try {
            JSONObject jObject = new JSONObject(jsonStr);
            List<TopicContentModel> list = getTopicContent(jObject.optJSONArray(PostsApiConstant.TOPIC_CONTENT), jObject.optString("img_url"), jObject.optString(BaseRestfulApiConstant.SOUND_URL));
            PollTopicModel pollTopicModel = getTopicPollInfo(jObject.optJSONObject(PostsApiConstant.POLL_INFO));
            if (pollTopicModel == null) {
                return list;
            }
            if (list == null) {
                ArrayList arrayList = new ArrayList();
                try {
                    TopicContentModel model = new TopicContentModel();
                    model.setInfor("");
                    model.setType(0);
                    arrayList.add(model);
                    list = arrayList;
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return null;
                }
            }
            list.get(0).setPollTopicModel(pollTopicModel);
            return list;
        } catch (Exception e3) {
            e = e3;
        }
    }

    public static List<TopicContentModel> getTopicContent(JSONArray jsonArray, String baseURL, String soundURL) {
        String forward;
        List<TopicContentModel> list = new ArrayList<>();
        try {
            int j = jsonArray.length();
            for (int i = 0; i < j; i++) {
                JSONObject jobj = jsonArray.optJSONObject(i);
                TopicContentModel topicContent = new TopicContentModel();
                topicContent.setType(jobj.optInt("type"));
                topicContent.setInfor(jobj.optString("infor").trim());
                if (topicContent.getType() == 1) {
                    topicContent.setBaseUrl(baseURL);
                }
                if (topicContent.getType() == 5) {
                    SoundModel soundModel = new SoundModel();
                    soundModel.setSoundPath(String.valueOf(soundURL) + topicContent.getInfor());
                    soundModel.setSoundTime((long) jobj.optInt("desc"));
                    topicContent.setSoundModel(soundModel);
                }
                if (i == 0 && (forward = jobj.optString("forward")) != null && !forward.trim().equals("") && !forward.equals("null")) {
                    topicContent.setRepostTopicModel(getRepostTopic(new JSONObject(forward), baseURL, soundURL));
                }
                list.add(topicContent);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r0v13 int: [D('pollIds' java.lang.String), D('p' int)] */
    /* JADX INFO: Multiple debug info for r11v5 org.json.JSONArray: [D('array' org.json.JSONArray), D('jsonObject' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r11v6 com.mobcent.forum.android.model.PollTopicModel: [D('pollTopicModel' com.mobcent.forum.android.model.PollTopicModel), D('array' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r3v10 double: [D('jsonObject1' org.json.JSONObject), D('total' double)] */
    public static PollTopicModel getTopicPollInfo(JSONObject jsonObject) {
        if (jsonObject == null) {
            return null;
        }
        try {
            PollTopicModel pollTopicModel = new PollTopicModel();
            try {
                pollTopicModel.setPoolType(jsonObject.optInt("type"));
                pollTopicModel.setDeadline(jsonObject.optLong(PostsApiConstant.DEADLINE));
                pollTopicModel.setIs_visible(jsonObject.optBoolean(PostsApiConstant.IS_VISIBLE));
                pollTopicModel.setPollStatus(jsonObject.optInt(PostsApiConstant.POLL_STATUS));
                String pollIds = jsonObject.optString(PostsApiConstant.POLLS_ID);
                if (pollIds.contains(AdApiConstant.RES_SPLIT_COMMA)) {
                    String[] pollId = pollIds.substring(1, pollIds.length() - 1).split(AdApiConstant.RES_SPLIT_COMMA);
                    int[] pollItemid = new int[pollId.length];
                    for (int i = 0; i < pollId.length; i++) {
                        if (!pollId[i].equals("")) {
                            pollItemid[i] = Integer.parseInt(pollId[i]);
                        }
                    }
                    pollTopicModel.setPollId(pollItemid);
                } else if (!pollIds.equals("[]")) {
                    pollTopicModel.setPollId(new int[]{Integer.parseInt(pollIds.substring(1, pollIds.length() - 1))});
                }
                JSONArray array = jsonObject.optJSONArray(PostsApiConstant.POLL_ITEM_LIST);
                double count = 0.0d;
                for (int i2 = 0; i2 < array.length(); i2++) {
                    count += (double) ((JSONObject) array.get(i2)).optInt("total_num");
                }
                List<PollItemModel> poolList = new ArrayList<>();
                for (int i3 = 0; i3 < array.length(); i3++) {
                    PollItemModel pollTopicContentModel = new PollItemModel();
                    JSONObject object = (JSONObject) array.get(i3);
                    pollTopicContentModel.setPollName(object.optString("name"));
                    pollTopicContentModel.setPollItemId(object.optInt(PostsApiConstant.POLL_NAME_ID));
                    pollTopicContentModel.setTotalNum(object.optInt("total_num"));
                    double total = (double) object.optInt("total_num");
                    if (count > 0.0d) {
                        pollTopicContentModel.setRatio(total / count);
                    } else {
                        pollTopicContentModel.setRatio(0.0d);
                    }
                    poolList.add(pollTopicContentModel);
                }
                pollTopicModel.setPooList(poolList);
                return pollTopicModel;
            } catch (Exception e) {
                PollTopicModel pollTopicModel2 = pollTopicModel;
                return null;
            }
        } catch (Exception e2) {
            return null;
        }
    }

    public static AnnoModel getAnnoDetail(String jsonStr) {
        AnnoModel model = new AnnoModel();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String baseURL = jsonObject.optString("img_url");
            String soundURL = jsonObject.optString(BaseRestfulApiConstant.SOUND_URL);
            JSONObject jsonObj = jsonObject.optJSONObject(PostsApiConstant.ANNOUNCE_DETAIL);
            model.setTopicContentList(getTopicContent(jsonObj.optJSONArray(PostsApiConstant.ANNOUNCE_CONTENT), baseURL, soundURL));
            model.setAnnoId(jsonObj.optLong("announce_id"));
            model.setAuthor(jsonObj.optString("author"));
            model.setBoardId(jsonObj.optLong("board_id"));
            model.setForumId(jsonObj.optLong("forum_id"));
            model.setAnnoStartDate(jsonObj.optLong("start_date"));
            model.setTitle(jsonObj.optString("title"));
            return model;
        } catch (Exception e) {
            return null;
        }
    }

    public static String createPublishTopicJson(String content, String iconPath1, String iconPath2, String iconPath3) {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObj1 = new JSONObject();
        try {
            jsonObj1.put("infor", content);
            jsonObj1.put("type", 1);
            jsonArray.put(jsonObj1);
            if (iconPath1 != null) {
                JSONObject jsonObj2 = new JSONObject();
                jsonObj2.put("type", 1);
                jsonObj2.put("infor", iconPath1);
                jsonArray.put(jsonObj2);
            }
            if (iconPath2 != null) {
                JSONObject jsonObj3 = new JSONObject();
                jsonObj3.put("type", 1);
                jsonObj3.put("infor", iconPath2);
                jsonArray.put(jsonObj3);
            }
            if (iconPath3 != null) {
                JSONObject jsonObj4 = new JSONObject();
                jsonObj4.put("type", 1);
                jsonObj4.put("infor", iconPath3);
                jsonArray.put(jsonObj4);
            }
        } catch (Exception e) {
        }
        return jsonArray.toString();
    }

    public static String parseUploadImageJson(String jsonStr) {
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.optInt("rs") == 0) {
                return null;
            }
            return jsonObj.optString("suffixPath");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String parseUploadAudioJson(String jsonStr) {
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.optInt("rs") == 0) {
                return null;
            }
            return jsonObj.optString(PostsApiConstant.SOUND_PATH);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<PostsNoticeModel> getPostsNoticeList(String jsonStr) {
        List<PostsNoticeModel> postsNoticeList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray jsonArray = jsonObject.optJSONArray("list");
            String baseUrl = jsonObject.optString("img_url");
            String iconUrl = jsonObject.optString("icon_url");
            String soundURL = jsonObject.optString(BaseRestfulApiConstant.SOUND_URL);
            int j = jsonArray.length();
            for (int i = 0; i < j; i++) {
                JSONObject jobj = jsonArray.optJSONObject(i);
                PostsNoticeModel postsNoticModel = new PostsNoticeModel();
                postsNoticModel.setIconUrl(iconUrl);
                postsNoticModel.setIcon(jobj.optString("icon"));
                postsNoticModel.setUserId(jobj.optLong("user_id"));
                postsNoticModel.setIsReply(jobj.optInt("is_reply"));
                postsNoticModel.setIsRead(jobj.optInt(PostsApiConstant.IS_READ));
                postsNoticModel.setBoardId(jobj.optLong("board_id"));
                postsNoticModel.setReplyDate(jobj.optLong(PostsApiConstant.REPLY_DATE));
                String replyContent = jobj.optString(PostsApiConstant.REPLY_CONTENT).trim();
                postsNoticModel.setReplyContent(replyContent);
                postsNoticModel.setReplyContentList(getReplyContent(replyContent, baseUrl, soundURL, null));
                postsNoticModel.setQuoteContent(jobj.optString(PostsApiConstant.QUOTE_CONTENT).trim());
                postsNoticModel.setReplyNickName(jobj.optString(PostsApiConstant.REPLY_NICK_NAME));
                postsNoticModel.setReplyRemindId(jobj.optLong(PostsApiConstant.REPLY_REMIND_ID));
                postsNoticModel.setTopicId(jobj.optLong("topic_id"));
                postsNoticModel.setTopicSubjtect(jobj.optString(PostsApiConstant.TOPIC_SUBJECT));
                postsNoticModel.setReplyPostsId(jobj.optLong(PostsApiConstant.REPLY_POSTS_ID));
                postsNoticModel.setModelType(jobj.optLong(PostsApiConstant.MODEL_TYPE));
                postsNoticeList.add(postsNoticModel);
            }
            PostsNoticeModel firstPostsNoticModel = (PostsNoticeModel) postsNoticeList.get(0);
            firstPostsNoticModel.setRs(jsonObject.optInt("rs"));
            firstPostsNoticModel.setHasNext(jsonObject.optBoolean("has_next"));
            postsNoticeList.set(0, firstPostsNoticModel);
            return postsNoticeList;
        } catch (Exception e) {
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r13v9 java.util.List<com.mobcent.forum.android.model.PostsNoticeModel>: [D('firstPostsNoticModel' com.mobcent.forum.android.model.PostsNoticeModel), D('postsNoticeList' java.util.List<com.mobcent.forum.android.model.PostsNoticeModel>)] */
    public static List<PostsNoticeModel> getAtPostsNoticeList(String jsonStr) {
        List<PostsNoticeModel> postsNoticeList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray jsonArray = jsonObject.optJSONArray("list");
            String baseUrl = jsonObject.optString("img_url");
            String iconUrl = jsonObject.optString("icon_url");
            String soundURL = jsonObject.optString(BaseRestfulApiConstant.SOUND_URL);
            int j = jsonArray.length();
            for (int i = 0; i < j; i++) {
                JSONObject jobj = jsonArray.optJSONObject(i);
                PostsNoticeModel postsNoticModel = new PostsNoticeModel();
                postsNoticModel.setIconUrl(iconUrl);
                postsNoticModel.setIcon(jobj.optString("icon"));
                postsNoticModel.setUserId(jobj.optLong("user_id"));
                postsNoticModel.setIsReply(jobj.optInt("is_reply"));
                postsNoticModel.setIsRead(jobj.optInt(PostsApiConstant.IS_READ));
                postsNoticModel.setBoardId(jobj.optLong("board_id"));
                postsNoticModel.setReplyDate(jobj.optLong(PostsApiConstant.CREATE_TIME));
                String replyContent = jobj.optString(PostsApiConstant.REPLY_CONTENT).trim();
                postsNoticModel.setReplyContent(replyContent);
                try {
                    postsNoticModel.setReplyContent(new JSONObject(replyContent).optString("infor").trim());
                } catch (Exception e) {
                    postsNoticModel.setReplyContentList(getReplyContent(replyContent, baseUrl, soundURL, null));
                }
                postsNoticModel.setQuoteContent(jobj.optString(PostsApiConstant.QUOTE_CONTENT).trim());
                postsNoticModel.setReplyNickName(jobj.optString(PostsApiConstant.REPLY_AT_NICK_NAME));
                postsNoticModel.setReplyRemindId(jobj.optLong(PostsApiConstant.RELATIONAL_CONTENT_ID));
                postsNoticModel.setTopicId(jobj.optLong("topic_id"));
                postsNoticModel.setTopicSubjtect(jobj.optString(PostsApiConstant.TOPIC_SUBJECT));
                postsNoticModel.setReplyPostsId(jobj.optLong(PostsApiConstant.REPLY_POSTS_ID));
                postsNoticModel.setModelType(jobj.optLong(PostsApiConstant.MODEL_TYPE));
                postsNoticeList.add(postsNoticModel);
            }
            PostsNoticeModel firstPostsNoticModel = (PostsNoticeModel) postsNoticeList.get(0);
            firstPostsNoticModel.setRs(jsonObject.optInt("rs"));
            firstPostsNoticModel.setHasNext(jsonObject.optBoolean("has_next"));
            postsNoticeList.set(0, firstPostsNoticModel);
            return postsNoticeList;
        } catch (Exception e2) {
            return null;
        }
    }

    public static JSONArray createPublishTopicJson(String content, String splitChar, String tagImg, String audioPath, int audioDuration) {
        JSONArray jsonArray = new JSONArray();
        String[] s = content.split(splitChar);
        try {
            if (!StringUtil.isEmpty(audioPath)) {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("type", 5);
                jsonObj.put("infor", audioPath);
                jsonObj.put("desc", audioDuration);
                jsonArray.put(jsonObj);
            }
            for (int i = 0; i < s.length; i++) {
                JSONObject jsonObj2 = new JSONObject();
                if (s[i].indexOf(tagImg) > -1) {
                    jsonObj2.put("type", 1);
                    jsonObj2.put("infor", s[i].substring(1, s[i].length()));
                } else {
                    jsonObj2.put("type", 0);
                    jsonObj2.put("infor", s[i].toString());
                }
                jsonArray.put(jsonObj2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    private static JSONArray createMentionedFriends(List<UserInfoModel> mentionedFriends) {
        JSONArray array = new JSONArray();
        if (mentionedFriends != null) {
            try {
                if (!mentionedFriends.isEmpty()) {
                    for (int j = 0; j < mentionedFriends.size(); j++) {
                        UserInfoModel user = mentionedFriends.get(j);
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("name", user.getNickname().replace("@", ""));
                        jsonObject.put("userId", user.getUserId());
                        jsonObject.put(PostsApiConstant.FLAT_UD, user.getPlatformId());
                        array.put(jsonObject);
                    }
                }
            } catch (Exception e) {
                return null;
            }
        }
        return array;
    }

    public static String createPublishTopicJson(String content, String splitChar, String tagImg, List<UserInfoModel> mentionedFriends, String audioPath, int audioDuration) {
        JSONObject firstObject;
        JSONArray jsonArray = new JSONArray();
        try {
            jsonArray = createPublishTopicJson(content, splitChar, tagImg, audioPath, audioDuration);
            JSONArray mentionedFriendsJson = createMentionedFriends(mentionedFriends);
            if (jsonArray.length() > 0) {
                firstObject = jsonArray.getJSONObject(0);
            } else {
                firstObject = new JSONObject();
                firstObject.put("type", 0);
                firstObject.put("infor", "");
            }
            if (mentionedFriendsJson != null && mentionedFriends.size() > 0) {
                firstObject.put("friendList", mentionedFriendsJson);
            }
            jsonArray.put(0, firstObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray.toString();
    }

    /* JADX INFO: Multiple debug info for r1v4 java.lang.String: [D('jsonArray' org.json.JSONArray), D('js' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v1 org.json.JSONArray: [D('content' java.lang.String), D('jsonArray' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r8v1 org.json.JSONArray: [D('mentionedFriendsJson' org.json.JSONArray), D('splitChar' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r2v1 org.json.JSONObject: [D('repostJson' org.json.JSONObject), D('boardName' java.lang.String)] */
    public static String createRepostTopicJson(TopicModel topicModel, String boardName, long forwardTime, long boardId, String content, String splitChar, String tagImg, List<UserInfoModel> mentionedFriends, String audioPath, int audioDuration) {
        JSONArray jsonArray;
        JSONObject firstObject;
        JSONArray jsonArray2 = new JSONArray();
        try {
            JSONArray jsonArray3 = createPublishTopicJson(content, splitChar, tagImg, audioPath, audioDuration);
            try {
                JSONArray mentionedFriendsJson = createMentionedFriends(mentionedFriends);
                JSONObject repostJson = createRepostjson(topicModel, boardName, forwardTime, boardId);
                if (jsonArray3.length() > 0) {
                    firstObject = jsonArray3.getJSONObject(0);
                } else {
                    firstObject = new JSONObject();
                    firstObject.put("type", 0);
                    firstObject.put("infor", "");
                }
                if (mentionedFriendsJson != null && mentionedFriends.size() > 0) {
                    firstObject.put("friendList", mentionedFriendsJson);
                }
                if (repostJson != null) {
                    firstObject.put("forward", repostJson);
                }
                jsonArray3.put(0, firstObject);
                jsonArray = jsonArray3;
            } catch (Exception e) {
                jsonArray = jsonArray3;
            }
        } catch (Exception e2) {
            jsonArray = jsonArray2;
        }
        return jsonArray.toString();
    }

    public static String createPublishTopicJson(List<String> pollContent) {
        JSONArray jsonArray = new JSONArray();
        int i = 0;
        while (i < pollContent.size()) {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(PostsApiConstant.ITEM_NAME, pollContent.get(i));
                jsonArray.put(jsonObject);
                i++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonArray.toString();
    }

    public static List<PollItemModel> getUserPoll(String jsonStr) {
        List<PollItemModel> pollList = new ArrayList<>();
        double count = 0.0d;
        try {
            JSONArray array = new JSONObject(jsonStr).getJSONArray(PostsApiConstant.POLL_LIST);
            for (int i = 0; i < array.length(); i++) {
                count += (double) ((JSONObject) array.get(i)).optInt("total_num");
            }
            for (int i2 = 0; i2 < array.length(); i2++) {
                JSONObject jsonObject = (JSONObject) array.get(i2);
                PollItemModel model = new PollItemModel();
                model.setPollItemId(jsonObject.optInt(PostsApiConstant.POLL_ID));
                model.setPollName(jsonObject.optString("name"));
                model.setTotalNum(jsonObject.optInt("total_num"));
                double total = (double) jsonObject.optInt("total_num");
                if (count > 0.0d) {
                    model.setRatio(total / count);
                } else {
                    model.setRatio(0.0d);
                }
                pollList.add(model);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pollList;
    }

    public static List<TopicModel> getSurroudTopic(String jsonStr) {
        List<TopicModel> topicList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String baseUrl = jsonObject.optString("img_url");
            try {
                JSONArray jsonArrayTopic = jsonObject.optJSONArray("pois");
                int j = jsonArrayTopic.length();
                for (int i = 0; i < j; i++) {
                    JSONObject jobj = jsonArrayTopic.optJSONObject(i);
                    TopicModel topicModel = new TopicModel();
                    topicModel.setBaseUrl(baseUrl);
                    topicModel.setTopicId(jobj.optLong("topic_id"));
                    topicModel.setTitle(jobj.optString("title"));
                    topicModel.setLastReplyDate(jobj.optLong("last_reply_date"));
                    topicModel.setHitCount(jobj.optInt(PostsApiConstant.HITS));
                    topicModel.setReplieCount(jobj.optInt("replies"));
                    topicModel.setUserId((long) jobj.optInt("user_id"));
                    topicModel.setUserNickName(jobj.optString("user_nick_name"));
                    topicModel.setType(jobj.optInt("type"));
                    topicModel.setPicPath(jobj.optString("pic_path"));
                    topicModel.setBoardId(jobj.optLong("board_id", 0));
                    topicModel.setTop(jobj.optInt("top"));
                    topicModel.setBoardName(jobj.optString("board_name"));
                    topicModel.setHot(jobj.optInt("hot"));
                    topicModel.setEssence(jobj.optInt(PostsApiConstant.ESSENCE));
                    topicModel.setPoll(jobj.optString(PostsApiConstant.POLL));
                    topicModel.setDistance(jobj.optInt("distance"));
                    topicModel.setLocation(jobj.optString("location"));
                    topicModel.setUploadType(jobj.optInt(PostsApiConstant.UPLOAD_TYPE));
                    topicList.add(topicModel);
                }
                TopicModel firstTopicModel = (TopicModel) topicList.get(0);
                firstTopicModel.setHasNext(jsonObject.optInt("has_next"));
                firstTopicModel.setPage(jsonObject.optInt("page"));
                firstTopicModel.setRs(jsonObject.optInt("rs"));
                topicList.set(0, firstTopicModel);
            } catch (Exception e) {
                topicList = null;
            }
            return topicList;
        } catch (Exception e2) {
            Exception exc = e2;
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r7v1 org.json.JSONArray: [D('boardName' java.lang.String), D('array' org.json.JSONArray)] */
    private static JSONObject createRepostjson(TopicModel topicModel, String boardName, long forwardTime, long boardId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("board_id", boardId);
            jsonObject.put("board_name", boardName);
            List<TopicContentModel> list = topicModel.getTopicContentList();
            JSONArray array = new JSONArray();
            for (int i = 0; i < list.size(); i++) {
                JSONObject object = new JSONObject();
                TopicContentModel model = list.get(i);
                if (model.getType() == 0) {
                    object.put("infor", model.getInfor().trim());
                    object.put("type", 0);
                } else if (model.getType() == 1) {
                    object.put("infor", model.getInfor());
                    object.put("type", 1);
                } else if (model.getType() == 5) {
                    object.put("type", 5);
                    object.put("infor", model.getInfor());
                    if (model.getSoundModel() != null) {
                        object.put("desc", model.getSoundModel().getSoundTime());
                    }
                }
                array.put(object);
            }
            jsonObject.put("content", array);
            jsonObject.put(PostsApiConstant.FORWARD_TIME, forwardTime);
            jsonObject.put("nickname", topicModel.getUserNickName());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static BaseModel getLongPicUrl(String jsonStr) {
        Exception e;
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            int rs = jsonObject.optInt("rs");
            BaseModel baseModel = new BaseModel();
            if (rs == 1) {
                try {
                    baseModel.setPathOrContent(jsonObject.optString("pic_path"));
                    return baseModel;
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return null;
                }
            } else {
                baseModel.setErrorCode(jsonObject.optString(BaseRestfulApiConstant.ERROR_CODE));
                return baseModel;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isContainsPic(String content, String splitChar, String tagImg, String audioPath, int audioDuration) {
        JSONArray array = createPublishTopicJson(content, splitChar, tagImg, audioPath, audioDuration);
        for (int i = 0; i < array.length(); i++) {
            if (array.optJSONObject(i).optInt("type") == 1) {
                return true;
            }
        }
        return false;
    }
}
