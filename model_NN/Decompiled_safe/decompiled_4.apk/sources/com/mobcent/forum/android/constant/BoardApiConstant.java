package com.mobcent.forum.android.constant;

public interface BoardApiConstant {
    public static final String BOADR_CATEGORY_ID = "board_category_id";
    public static final String BOADR_CATEGORY_NAME = "board_category_name";
    public static final String BOADR_CATEGORY_TYPE = "board_category_type";
    public static final String BOADR_ID = "board_id";
    public static final String BOADR_LIST = "board_list";
    public static final String BOADR_NAME = "board_name";
    public static final String BOARD_DESC = "board_desc";
    public static final String BOARD_PERMISSION = "board_permission";
    public static final String LAST_POSTS_DATE = "last_posts_date";
    public static final String LIST = "list";
    public static final String ONLINE_USER_NUM = "online_user_num";
    public static final String PIC_PATH = "pic_path";
    public static final String POSTS_TOTAL_NUM = "posts_total_num";
    public static final String TD_POSTS_NUM = "td_posts_num";
    public static final String TD_VISITORS = "td_visitors";
    public static final String TOPIC_TOTAL_NUM = "topic_total_num";
    public static final int TYPE_DOUBLE = 2;
    public static final int TYPE_SIMPLE = 1;
}
