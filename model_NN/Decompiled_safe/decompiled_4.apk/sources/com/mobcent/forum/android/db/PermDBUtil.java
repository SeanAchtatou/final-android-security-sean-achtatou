package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.forum.android.db.constant.PermDBConstant;

public class PermDBUtil extends BaseDBUtil implements PermDBConstant {
    private static final int PERM_ID = 1;
    private static String SQL_SELECT_PERM = "SELECT * FROM perm";
    private static PermDBUtil permDBUtil;

    public static PermDBUtil getInstance(Context context) {
        if (permDBUtil == null) {
            permDBUtil = new PermDBUtil(context);
        }
        return permDBUtil;
    }

    private PermDBUtil(Context context) {
        super(context);
    }

    public String getPermJsonString() {
        String jsonStr = null;
        Cursor cursor = null;
        try {
            openReadableDB();
            cursor = this.readableDatabase.rawQuery(SQL_SELECT_PERM, null);
            while (cursor.moveToNext()) {
                jsonStr = cursor.getString(1);
            }
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
            throw th;
        }
        return jsonStr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean updatePermJsonString(String jsonStr) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("jsonStr", jsonStr);
            values.put("id", (Integer) 1);
            if (!isRowExisted(this.writableDatabase, "perm", "id", 1)) {
                this.writableDatabase.insertOrThrow("perm", null, values);
            } else {
                this.writableDatabase.update("perm", values, "id=1", null);
            }
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean deltePermList() {
        return removeAllEntries("perm");
    }
}
