package com.mobcent.forum.android.db.constant;

public interface UserJsonDBConstant {
    public static final String COLUMN_CURRENT_USER = "currUser";
    public static final String COLUMN_USER_ID = "userId";
    public static final String COLUMN_USER_INFO = "userInfo";
    public static final String CREATE_TABLE_PERSONAL = "CREATE TABLE IF NOT EXISTS t_json_personal(userId LONG PRIMARY KEY,currUser INTEGER,userInfo TEXT);";
    public static final String CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS t_json_user(userId LONG PRIMARY KEY,currUser INTEGER,userInfo TEXT);";
    public static final int IS_CURR_USER = 1;
    public static final int IS_NOT_CURR_USER = 0;
    public static final String TABLE_JSON_PERSONAL = "t_json_personal";
    public static final String TABLE_JSON_USER = "t_json_user";
}
