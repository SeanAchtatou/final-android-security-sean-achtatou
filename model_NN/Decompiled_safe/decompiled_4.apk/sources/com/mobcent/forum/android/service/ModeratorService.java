package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.ModeratorModel;
import java.util.List;

public interface ModeratorService {
    boolean BoardPermission(long j, long j2);

    String addModerator(long j, String str, long j2);

    String delModerator(long j, String str, long j2);

    ModeratorModel getModeratorBoardByLocal(long j);

    ModeratorModel getModeratorBoardList(long j, boolean z);

    ModeratorModel getModeratorBoardListNet(long j);

    List<ModeratorModel> getModeratorList();

    String setModerator(long j, String str, long j2, long j3);
}
