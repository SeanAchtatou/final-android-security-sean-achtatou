package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.constant.BaseRestfulApiConstant;
import com.mobcent.forum.android.constant.PostsApiConstant;
import com.mobcent.forum.android.util.MCResource;
import com.mobcent.forum.android.util.StringUtil;
import java.util.HashMap;

public class PostsRestfulApiRequester extends BaseRestfulApiRequester implements PostsApiConstant {
    public static String REQUEST_REPLY_URL = "forum/replyTo.do";
    public static String UPLOAD_IMAGE_URL = "mobcentFile/servlet/UploadFileServlet";

    public static String getTopicList(Context context, long boardId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("forum/topicList.do", params, context);
    }

    public static String getHotTopicList(Context context, long boardId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("forum/hotTopic.do", params, context);
    }

    public static String getEssenceTopicList(Context context, long boardId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("forum/essenceTopic.do", params, context);
    }

    public static String getPostTimeTopicList(Context context, long boardId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        params.put(PostsApiConstant.SORT, "publish");
        return doPostRequest("forum/topicList.do", params, context);
    }

    public static String searchTopicList(Context context, long userId, long boardId, String keyword, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("keyword", keyword);
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("baikeType", "1");
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("search/searchKeyword.do", params, context);
    }

    public static String getUserTopicList(Context context, long userId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("forum/userTopicList.do", params, context);
    }

    public static String getUserReplyList(Context context, long userId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest(MCResource.getInstance(context).getString("mc_forum_user_reply_list_url"), params, context);
    }

    public static String getPosts(Context context, long boardId, long topicId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("forum/replyList.do", params, context);
    }

    public static String getUserPosts(Context context, long boardId, long topicId, long userId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("forum/userPosts.do", params, context);
    }

    public static String getPostsByDesc(Context context, long boardId, long topicId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("forum/reversePosts.do", params, context);
    }

    public static String getTopicContent(Context context, long boardId, long topicId, long userId) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        if (userId > 0) {
            params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        }
        return doPostRequest("forum/topic.do", params, context);
    }

    public static String getAnnoDetail(Context context, long boardId, long announceId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        params.put("announceId", new StringBuilder(String.valueOf(announceId)).toString());
        return doPostRequest("forum/showAnnounceDetail.do", params, context);
    }

    public static String publishTopic(Context context, long boardId, String title, String content, boolean pageFrom) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("title", title);
        params.put("content", content);
        if (pageFrom) {
            params.put("pageFrom", "0");
        }
        return doPostRequest("forum/publish.do", params, context);
    }

    /* JADX INFO: Multiple debug info for r0v1 java.util.HashMap: [D('url' java.lang.String), D('params' java.util.HashMap<java.lang.String, java.lang.String>)] */
    public static String publishTopic(Context context, long boardId, String title, String content, boolean pageFrom, double longitude, double latitude, String location, int requireLocation, int selectVisibleId) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("title", title);
        params.put("content", content);
        params.put(PostsApiConstant.VISIBLE, new StringBuilder(String.valueOf(selectVisibleId)).toString());
        if (!(longitude == 0.0d || latitude == 0.0d || StringUtil.isEmpty(location))) {
            params.put("longitude", new StringBuilder(String.valueOf(longitude)).toString());
            params.put("latitude", new StringBuilder(String.valueOf(latitude)).toString());
            params.put("location", location);
            if (requireLocation > 0) {
                params.put(BaseRestfulApiConstant.REQUIRE_LOCATION, new StringBuilder(String.valueOf(requireLocation)).toString());
            }
        }
        if (pageFrom) {
            params.put("pageFrom", "0");
        }
        return doPostRequest("forum/publish.do", params, context);
    }

    public static String publishAnnounce(Context context, long boardId, int isReply, String title, String content, String fromDate, String toDate, int selectVisibleId) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId >= 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put(PostsApiConstant.ISREPLY, new StringBuilder(String.valueOf(isReply)).toString());
        params.put("title", title);
        params.put("content", content);
        params.put(PostsApiConstant.FROM_DATE, fromDate);
        params.put(PostsApiConstant.TO_DATE, toDate);
        params.put(PostsApiConstant.VISIBLE, new StringBuilder(String.valueOf(selectVisibleId)).toString());
        return doPostRequest("announce/addAnnounce.do", params, context);
    }

    public static String replyTopic(Context context, long boardId, long topicId, String rContent, String rTitle, long toReplyId) {
        String url = REQUEST_REPLY_URL;
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        params.put(PostsApiConstant.PARAM_RTITLE, rTitle);
        params.put(PostsApiConstant.PARAM_RCONTENT, rContent);
        if (toReplyId != -1) {
            params.put("toReplyId", new StringBuilder(String.valueOf(toReplyId)).toString());
        }
        return doPostRequest(url, params, context);
    }

    public static String replyTopic(Context context, long boardId, long topicId, String rContent, String rTitle, long toReplyId, boolean isQuote, long pageFrom) {
        String url = REQUEST_REPLY_URL;
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        params.put(PostsApiConstant.PARAM_RTITLE, rTitle);
        params.put(PostsApiConstant.PARAM_RCONTENT, rContent);
        params.put(PostsApiConstant.PARAM_IS_QUOTE, Boolean.toString(isQuote));
        if (pageFrom == 1) {
            params.put("pageFrom", "0");
        } else if (pageFrom == 2) {
            params.put("pageFrom", "1");
        }
        if (toReplyId != -1) {
            params.put("toReplyId", new StringBuilder(String.valueOf(toReplyId)).toString());
        }
        return doPostRequest(url, params, context);
    }

    public static String replyTopic(Context context, long boardId, long topicId, String rContent, String rTitle, long toReplyId, boolean isQuote, long pageFrom, double longitude, double latitude, String location, int requireLocation, int selectVisibleId) {
        String url = REQUEST_REPLY_URL;
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        params.put(PostsApiConstant.PARAM_RTITLE, rTitle);
        params.put(PostsApiConstant.PARAM_RCONTENT, rContent);
        params.put(PostsApiConstant.PARAM_IS_QUOTE, Boolean.toString(isQuote));
        params.put(PostsApiConstant.VISIBLE, new StringBuilder(String.valueOf(selectVisibleId)).toString());
        if (!(longitude == 0.0d || latitude == 0.0d || StringUtil.isEmpty(location))) {
            params.put("longitude", new StringBuilder(String.valueOf(longitude)).toString());
            params.put("latitude", new StringBuilder(String.valueOf(latitude)).toString());
            params.put("location", location);
            if (requireLocation > 0) {
                params.put(BaseRestfulApiConstant.REQUIRE_LOCATION, new StringBuilder(String.valueOf(requireLocation)).toString());
            }
        }
        if (pageFrom == 1) {
            params.put("pageFrom", "0");
        } else if (pageFrom == 2) {
            params.put("pageFrom", "1");
        }
        if (toReplyId != -1) {
            params.put("toReplyId", new StringBuilder(String.valueOf(toReplyId)).toString());
        }
        return doPostRequest(url, params, context);
    }

    public static String uploadImage(Context context, String uploadFile, String action) {
        return uploadFile(UPLOAD_IMAGE_URL, uploadFile, context, action);
    }

    public static String getPostsNoticeList(Context context, long userId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("message/postsNotices.do", params, context);
    }

    public static String updateReplyRemindState(Context context, long userId, String ids) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(PostsApiConstant.PARAM_RELPY_REMIND_IDS, ids);
        return doPostRequest("message/updateRead.do", params, context);
    }

    public static String deleteTopic(Context context, long boardId, long topicId) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        return doPostRequest("forum/deleteTopic.do", params, context);
    }

    public static String deleteReply(Context context, long boardId, long replyPostsId) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("replyPostsId", new StringBuilder(String.valueOf(replyPostsId)).toString());
        return doPostRequest("forum/deleteReply.do", params, context);
    }

    public static String updateTopic(Context context, long boardId, long topicId, String content) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        params.put("content", content);
        return doPostRequest("forum/updateTopic.do", params, context);
    }

    public static String updateReply(Context context, long boardId, long replyPostsId, String content) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("replyPostsId", new StringBuilder(String.valueOf(replyPostsId)).toString());
        params.put("content", content);
        return doPostRequest("forum/updateReply.do", params, context);
    }

    public static String getUserEssenceTopic(Context context, int page, int pageSize, long userId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("forum/myEssenceTopicList.do", params, context);
    }

    public static String getAtPostsNoticeList(Context context, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("forum/getRelationalReplyRemind.do", params, context);
    }

    public static String updateAtReply(Context context, String relationalContentIds, long userId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(PostsApiConstant.PARAM_RELATIONAL_CONTENT_IDS, relationalContentIds);
        return doPostRequest("forum/updateRelationalRead.do", params, context);
    }

    public static String getRelatedPosts(Context context, long userId, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("forum/getRelationalTopicList.do", params, context);
    }

    public static String publishPollTopic(Context context, long boardId, String title, String content, int type, int isVisible, long deadline, String voteContent) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("title", title);
        params.put("content", content);
        params.put("type", new StringBuilder(String.valueOf(type)).toString());
        params.put(PostsApiConstant.IS_VISIBLE, new StringBuilder(String.valueOf(isVisible)).toString());
        params.put(PostsApiConstant.DEADLINE, new StringBuilder(String.valueOf(deadline)).toString());
        params.put(PostsApiConstant.POLLITEM, voteContent);
        return doPostRequest("forum/publishPoll.do", params, context);
    }

    /* JADX INFO: Multiple debug info for r3v1 java.util.HashMap: [D('url' java.lang.String), D('params' java.util.HashMap<java.lang.String, java.lang.String>)] */
    public static String publishPollTopic(Context context, long boardId, String title, String content, int type, int isVisiable, long deadline, String voteContent, double longitude, double latitude, String location, int requireLocation, int selectVisibleId) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("title", title);
        params.put("content", content);
        params.put("type", new StringBuilder(String.valueOf(type)).toString());
        params.put(PostsApiConstant.IS_VISIBLE, new StringBuilder(String.valueOf(isVisiable)).toString());
        params.put(PostsApiConstant.DEADLINE, new StringBuilder(String.valueOf(deadline)).toString());
        params.put(PostsApiConstant.POLLITEM, voteContent);
        params.put(PostsApiConstant.VISIBLE, new StringBuilder(String.valueOf(selectVisibleId)).toString());
        if (!(longitude == 0.0d || latitude == 0.0d || StringUtil.isEmpty(location))) {
            params.put("longitude", new StringBuilder(String.valueOf(longitude)).toString());
            params.put("latitude", new StringBuilder(String.valueOf(latitude)).toString());
            params.put("location", location);
            if (requireLocation > 0) {
                params.put(BaseRestfulApiConstant.REQUIRE_LOCATION, new StringBuilder(String.valueOf(requireLocation)).toString());
            }
        }
        return doPostRequest("forum/publishPoll.do", params, context);
    }

    public static String getUserPoll(Context context, long boardId, long topicId, String itemId) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        params.put(PostsApiConstant.POLL_ITEM_ID, itemId);
        return doPostRequest("forum/userVote.do", params, context);
    }

    public static String getSurroudTopic(Context context, double longitude, double latitude, int radius, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("poi", "topic");
        params.put("longitude", new StringBuilder(String.valueOf(longitude)).toString());
        params.put("latitude", new StringBuilder(String.valueOf(latitude)).toString());
        if (radius > 0) {
            params.put(BaseRestfulApiConstant.RADIUS, new StringBuilder(String.valueOf(radius)).toString());
        }
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("lbs/lookAround.do", params, context);
    }

    public static String shareTopic(Context context, long boardId, long topicId) {
        HashMap<String, String> params = new HashMap<>();
        if (boardId > 0) {
            params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        }
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        return doPostRequest("forum/shareTopic.do", params, context);
    }

    public static String getPicTopicList(Context context, int page, int pageSize) {
        HashMap<String, String> params = new HashMap<>();
        params.put("page", new StringBuilder(String.valueOf(page)).toString());
        params.put("pageSize", new StringBuilder(String.valueOf(pageSize)).toString());
        return doPostRequest("forum/picTopics.do", params, context);
    }

    public static String getEssenceTopic(Context context, long boardId, long topicId, int status) {
        HashMap<String, String> params = new HashMap<>();
        params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        params.put("status", new StringBuilder(String.valueOf(status)).toString());
        return doPostRequest("forum/switchTopicEssence.do", params, context);
    }

    public static String getTopTopic(Context context, long boardId, long topicId, int status) {
        HashMap<String, String> params = new HashMap<>();
        params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        params.put("status", new StringBuilder(String.valueOf(status)).toString());
        return doPostRequest("forum/switchTopicTop.do", params, context);
    }

    public static String getCloseTopic(Context context, long boardId, long topicId, int status) {
        HashMap<String, String> params = new HashMap<>();
        params.put("boardId", new StringBuilder(String.valueOf(boardId)).toString());
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        params.put("status", new StringBuilder(String.valueOf(status)).toString());
        return doPostRequest("forum/switchTopicLock.do", params, context);
    }

    public static String uploadAudio(Context context, String audioFile) {
        return uploadFile("uld/uploadSound", audioFile, context, "");
    }

    public static String getPhysicalDelTopic(Context context, long topicId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("topicId", new StringBuilder(String.valueOf(topicId)).toString());
        return doPostRequest("forum/physicalDelTopic.do", params, context);
    }

    public static String getLongPicUrl(Context context, String title, String content, String baseUrl) {
        String url = String.valueOf(MCResource.getInstance(context).getString("mc_forum_request_img_domain_url")) + "uld/genPhpLongPic";
        HashMap<String, String> params = new HashMap<>();
        params.put(PostsApiConstant.TOPIC_TITLE, title);
        params.put(PostsApiConstant.TOPIC_CONTENT, content);
        params.put("baseUrl", baseUrl);
        return doPostRequest(url, params, context);
    }
}
