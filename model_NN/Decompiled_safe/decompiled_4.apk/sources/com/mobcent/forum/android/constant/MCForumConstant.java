package com.mobcent.forum.android.constant;

public interface MCForumConstant {
    public static final int DEFAULT_PAGE_SIZE = 25;
    public static final String RESOLUTION_100X100 = "100x100";
    public static final String RESOLUTION_114x114 = "114x114";
    public static final String RESOLUTION_200X200 = "200x200";
    public static final String RESOLUTION_240X320 = "240x320";
    public static final String RESOLUTION_320X480 = "320x480";
    public static final String RESOLUTION_480X800 = "480x800";
    public static final String RESOLUTION_50X50 = "50x50";
    public static final String RESOLUTION_60X60 = "60x60";
    public static final String RESOLUTION_640X480 = "640x480";
    public static final String RESOLUTION_ORIGINAL = "original";
}
