package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.UserConstant;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class UserServiceImplHelper extends BaseJsonHelper implements UserConstant {
    public static UserInfoModel parseRegUserJson(String jsonStr) {
        UserInfoModel userInfoModel = new UserInfoModel();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.optInt("rs") == 0) {
                return null;
            }
            userInfoModel.setUas(jsonObj.optString("secret"));
            userInfoModel.setUat(jsonObj.optString("token"));
            userInfoModel.setUserId(jsonObj.optLong("uid"));
            userInfoModel.setForumId(jsonObj.optInt(UserConstant.FORUM_ID));
            userInfoModel.setRoleNum(2);
            userInfoModel.setPermModelStr(jsonObj.optString("perm"));
            return userInfoModel;
        } catch (Exception e) {
            return null;
        }
    }

    public static UserInfoModel parseRegUserByOpenPlatform(String jsonStr) {
        UserInfoModel userInfoModel = new UserInfoModel();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.optInt("rs") == 0) {
                return null;
            }
            userInfoModel.setUas(jsonObj.optString("secret"));
            userInfoModel.setUat(jsonObj.optString("token"));
            userInfoModel.setRoleNum(jsonObj.optInt("role_num"));
            userInfoModel.setUserId(jsonObj.optLong("uid"));
            userInfoModel.setForumId(jsonObj.optInt(UserConstant.FORUM_ID));
            return userInfoModel;
        } catch (Exception e) {
            return null;
        }
    }

    public static UserInfoModel parseGetUserInfoJson(String jsonStr) {
        UserInfoModel userInfoModel = new UserInfoModel();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            if (jsonObject.optInt("rs") != 1) {
                return null;
            }
            String iconUrl = jsonObject.optString("icon_url");
            userInfoModel.setUserId(jsonObject.optLong("uid"));
            userInfoModel.setIconUrl(jsonObject.optString("icon"));
            userInfoModel.setIcon(StringUtil.formatImgUrl(String.valueOf(iconUrl) + jsonObject.optString("icon"), "100x100"));
            userInfoModel.setNickname(jsonObject.optString("name"));
            if (jsonObject.optBoolean("gender")) {
                userInfoModel.setGender(1);
            } else {
                userInfoModel.setGender(0);
            }
            userInfoModel.setIsFollow(jsonObject.optInt(UserConstant.IS_FOLLOW));
            userInfoModel.setStatus(jsonObject.optInt("status"));
            userInfoModel.setCredits(jsonObject.optInt("credits"));
            userInfoModel.setGoldNum(jsonObject.optInt(UserConstant.GOLD_NUM));
            userInfoModel.setLevel(jsonObject.optInt("level"));
            userInfoModel.setTopicNum(jsonObject.optInt(UserConstant.TOPIC_NUM));
            userInfoModel.setReplyPostsNum(jsonObject.optInt(UserConstant.REPLY_POSTS_NUM));
            userInfoModel.setEssenceNum(jsonObject.optInt(UserConstant.ESSENCE_NUM));
            userInfoModel.setUserFavoriteNum(jsonObject.optInt("favor_num"));
            userInfoModel.setRelatePostsNum(jsonObject.optInt(UserConstant.USER_RELATE_POSTS_NUM));
            userInfoModel.setUserFriendsNum(jsonObject.optInt(UserConstant.USER_FRIEND_NUM));
            userInfoModel.setLastLoginTime(jsonObject.optLong(UserConstant.LAST_LOGIN_TIME));
            userInfoModel.setBlackStatus(jsonObject.optInt(UserConstant.IS_BLACK_USER));
            userInfoModel.setLevel(jsonObject.optInt("level"));
            userInfoModel.setFollowNum(jsonObject.optInt(UserConstant.USER_FOLLOW_NUM));
            userInfoModel.setBannedBoardNum(jsonObject.optInt(UserConstant.GAG_BOARD_NUM));
            userInfoModel.setShieldBoardNum(jsonObject.optInt(UserConstant.SHIELD_BOARD_NUM));
            userInfoModel.setIsDelAccounts(jsonObject.optInt(UserConstant.IS_DELE_ACCOUNT));
            userInfoModel.setIsDelIp(jsonObject.optInt(UserConstant.IS_DELE_IP));
            userInfoModel.setRoleNum(jsonObject.optInt("role_num"));
            userInfoModel.setPermModelStr(jsonObject.optString("perm"));
            return userInfoModel;
        } catch (Exception e) {
            return null;
        }
    }

    public static UserInfoModel parseOpenplatformUserInfo(String jsonStr) {
        UserInfoModel userInfoModel = new UserInfoModel();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            userInfoModel.setRs(jsonObj.optInt("rs"));
            userInfoModel.setRegister(jsonObj.optBoolean(UserConstant.REGISTER));
            if (userInfoModel.isRegister()) {
                userInfoModel.setBaseUrl(jsonObj.optString("baseUrl"));
                JSONObject jobj = jsonObj.optJSONObject(UserConstant.USER_BEAN);
                userInfoModel.setIcon(jobj.optString("icon"));
                userInfoModel.setPlatformUserId(jobj.optString("uid"));
                userInfoModel.setGender(jobj.optInt("gender"));
                userInfoModel.setNickname(jobj.optString("nickname"));
                userInfoModel.setEmail(jobj.optString("email"));
                JSONObject jsObj = jsonObj.optJSONObject("userInfo");
                userInfoModel.setUas(jsObj.optString("secret"));
                userInfoModel.setUat(jsObj.optString("token"));
                userInfoModel.setRoleNum(jsObj.optInt("role_num"));
                userInfoModel.setUserId(jsObj.optLong("uid"));
                userInfoModel.setForumId(jsObj.optInt(UserConstant.FORUM_ID));
                userInfoModel.setPermModelStr(jsObj.optString("perm"));
            } else {
                JSONObject jobj2 = jsonObj.optJSONObject("userInfo");
                userInfoModel.setUas(jobj2.optString("secret"));
                userInfoModel.setUat(jobj2.optString("token"));
                userInfoModel.setRoleNum(jobj2.optInt("role_num"));
                userInfoModel.setUserId(jobj2.optLong("uid"));
                userInfoModel.setForumId(jobj2.optInt(UserConstant.FORUM_ID));
                userInfoModel.setEmail(jobj2.optString("email"));
                userInfoModel.setPermModelStr(jobj2.optString("perm"));
            }
            return userInfoModel;
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean parseChangePwdJson(String jsonStr) {
        try {
            if (new JSONObject(jsonStr).optInt("rs") == 1) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static UserInfoModel parseLoginUserJson(String jsonStr) {
        UserInfoModel userInfoModel = new UserInfoModel();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.optInt("rs") == 0) {
                return null;
            }
            userInfoModel.setUas(jsonObj.optString("secret"));
            userInfoModel.setUat(jsonObj.optString("token"));
            userInfoModel.setUserId(jsonObj.optLong("uid"));
            userInfoModel.setForumId(jsonObj.optInt(UserConstant.FORUM_ID));
            userInfoModel.setRoleNum(jsonObj.optInt("role_num"));
            userInfoModel.setBannedBoardNum(jsonObj.optInt(UserConstant.GAG_BOARD_NUM));
            userInfoModel.setShieldBoardNum(jsonObj.optInt(UserConstant.SHIELD_BOARD_NUM));
            userInfoModel.setPermModelStr(jsonObj.optString("perm"));
            return userInfoModel;
        } catch (Exception e) {
            return null;
        }
    }

    public static String parseUploadIconJson(String jsonStr) {
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.optInt("rs") == 0) {
                return null;
            }
            return jsonObj.optString("suffixPath");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean parseUpdateUserJson(String jsonStr) {
        try {
            if (new JSONObject(jsonStr).optInt("rs") == 0) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static UserInfoModel parseGetUserJson(String jsonStr) {
        UserInfoModel userInfoModel = new UserInfoModel();
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.optInt("rs") == 0) {
                return null;
            }
            String iconUrl = jsonObj.optString("icon_url");
            userInfoModel.setLevel(jsonObj.optInt("level"));
            userInfoModel.setEmail(jsonObj.optString("email"));
            userInfoModel.setNickname(jsonObj.optString("name"));
            userInfoModel.setIcon(String.valueOf(iconUrl) + jsonObj.optString("icon"));
            if (jsonObj.optBoolean("gender")) {
                userInfoModel.setGender(1);
            } else {
                userInfoModel.setGender(0);
            }
            userInfoModel.setLevel(jsonObj.optInt("level"));
            return userInfoModel;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean parseLogoutJson(String jsonStr) {
        try {
            if (new JSONObject(jsonStr).optInt("rs") == 0) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* JADX INFO: Multiple debug info for r13v1 int: [D('pageSize' int), D('j' int)] */
    public static List<UserInfoModel> parseUserFriendListJson(String jsonStr, int page, int pageSize) {
        int totalNum;
        int totalNum2;
        List<UserInfoModel> userFriendList = new ArrayList<>();
        int totalNum3 = page * pageSize;
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String iconUrl = jsonObject.optString("icon_url");
            int hashNextPage = jsonObject.optInt("has_next");
            JSONArray jsonArray = jsonObject.optJSONArray("list");
            if (hashNextPage > 0) {
                totalNum2 = (page + 1) * pageSize;
            } else {
                totalNum2 = totalNum3;
            }
            try {
                int j = jsonArray.length();
                for (int i = 0; i < j; i++) {
                    JSONObject jsonObj = jsonArray.optJSONObject(i);
                    UserInfoModel userInfoModel = new UserInfoModel();
                    if (jsonObj.optBoolean("gender")) {
                        userInfoModel.setGender(1);
                    } else {
                        userInfoModel.setGender(0);
                    }
                    userInfoModel.setIcon(String.valueOf(iconUrl) + jsonObj.optString("icon"));
                    userInfoModel.setNickname(jsonObj.optString("name"));
                    userInfoModel.setStatus(jsonObj.optInt("status"));
                    userInfoModel.setLevel(jsonObj.optInt("level"));
                    userInfoModel.setUserId(jsonObj.optLong("uid"));
                    userInfoModel.setBlackStatus(jsonObj.optInt(UserConstant.IS_BLACK_USER));
                    userInfoModel.setIsFollow(jsonObj.optInt(UserConstant.IS_FRIEND));
                    userInfoModel.setLevel(jsonObj.optInt("level"));
                    userFriendList.add(userInfoModel);
                }
                if (userFriendList.size() > 0) {
                    UserInfoModel firstUserFriendModel = (UserInfoModel) userFriendList.get(0);
                    firstUserFriendModel.setRs(jsonObject.optInt("rs"));
                    firstUserFriendModel.setPage(page);
                    firstUserFriendModel.setHasNext(hashNextPage);
                    userFriendList.set(0, firstUserFriendModel);
                }
                int i2 = totalNum2;
                return userFriendList;
            } catch (Exception e) {
                totalNum = totalNum2;
                return null;
            }
        } catch (Exception e2) {
            totalNum = totalNum3;
        }
    }

    public static List<UserInfoModel> parseSurroundUserJson(String jsonStr, int page, int pageSize) {
        List<UserInfoModel> surroundUserList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String iconUrl = jsonObject.optString("icon_url");
            int hashNextPage = jsonObject.optInt("has_next");
            JSONArray jsonArray = jsonObject.optJSONArray("pois");
            int j = jsonArray.length();
            for (int i = 0; i < j; i++) {
                JSONObject jsonObj = jsonArray.optJSONObject(i);
                UserInfoModel userInfoModel = new UserInfoModel();
                userInfoModel.setLocation(jsonObj.optString("location"));
                userInfoModel.setDistance(jsonObj.optInt("distance"));
                userInfoModel.setGender(jsonObj.optInt("gender"));
                userInfoModel.setIcon(String.valueOf(iconUrl) + jsonObj.optString("icon"));
                userInfoModel.setBlackStatus(jsonObj.optInt(UserConstant.IS_BLACK_USER));
                userInfoModel.setIsFollow(jsonObj.optInt(UserConstant.IS_FRIEND));
                userInfoModel.setNickname(jsonObj.optString("nickname"));
                userInfoModel.setUserId(jsonObj.optLong("uid"));
                surroundUserList.add(userInfoModel);
            }
            if (surroundUserList.size() > 0) {
                UserInfoModel firstSurroundUserModel = (UserInfoModel) surroundUserList.get(0);
                firstSurroundUserModel.setRs(jsonObject.optInt("rs"));
                firstSurroundUserModel.setPage(page);
                firstSurroundUserModel.setHasNext(hashNextPage);
                surroundUserList.set(0, firstSurroundUserModel);
            }
            return surroundUserList;
        } catch (Exception e) {
            return null;
        }
    }

    public static List<UserInfoModel> parseGetRegUserInfoJson(String jsonStr) {
        Exception e;
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray jsonArray = jsonObject.optJSONArray("list");
            List<UserInfoModel> userList = new ArrayList<>();
            int i = 0;
            while (i < jsonArray.length()) {
                try {
                    JSONObject jsonObj = jsonArray.optJSONObject(i);
                    UserInfoModel model = new UserInfoModel();
                    model.setUserId(jsonObj.optLong("user_id"));
                    model.setNickname(jsonObj.optString("nick_name"));
                    model.setEmail(jsonObj.optString("email"));
                    model.setRegSource(jsonObj.optInt(UserConstant.REG_SOURCE));
                    userList.add(model);
                    i++;
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return null;
                }
            }
            if (userList.size() <= 0) {
                return userList;
            }
            UserInfoModel userModel = (UserInfoModel) userList.get(0);
            userModel.setRs(jsonObject.optInt("rs"));
            userList.set(0, userModel);
            return userList;
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
    }
}
