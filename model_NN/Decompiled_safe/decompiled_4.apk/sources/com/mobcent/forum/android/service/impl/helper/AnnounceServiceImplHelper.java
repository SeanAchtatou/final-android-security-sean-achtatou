package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.AnnoConstant;
import com.mobcent.forum.android.model.AnnoModel;
import com.mobcent.forum.android.model.TopicContentModel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class AnnounceServiceImplHelper extends BaseJsonHelper implements AnnoConstant {
    public static List<AnnoModel> getAnnounceList(String jsonStr) {
        String imgUrl;
        String iconUrl;
        List<AnnoModel> annoList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            try {
                imgUrl = jsonObject.optString("img_url");
                iconUrl = jsonObject.optString("icon_url");
                JSONObject jSONObject = jsonObject;
                return annoList;
            } catch (Exception e) {
                return null;
            }
            try {
                JSONArray annoArray = jsonObject.optJSONArray("anno_list");
                for (int i = 0; i < annoArray.length(); i++) {
                    JSONObject anno = annoArray.getJSONObject(i);
                    AnnoModel annoModel = new AnnoModel();
                    annoModel.setAnnoId(anno.getLong("announce_id"));
                    annoModel.setAuthor(anno.getString("author"));
                    annoModel.setAuthorId(anno.getLong(AnnoConstant.AUTHOR_ID));
                    annoModel.setBoardId(anno.getLong("board_id"));
                    annoModel.setAnnoCreateDate(anno.getLong("create_date"));
                    annoModel.setAnnoEndDate(anno.getLong(AnnoConstant.END_DATE));
                    annoModel.setForumId(anno.getLong("forum_id"));
                    annoModel.setIcon(String.valueOf(iconUrl) + anno.getString("icon"));
                    annoModel.setAnnoStartDate(anno.getLong("start_date"));
                    annoModel.setSubject(anno.getString(AnnoConstant.SUBJECT));
                    annoModel.setVerify(anno.getInt(AnnoConstant.VERIFY));
                    annoModel.setTopicContentList(getAnnoContent(anno.optJSONArray("content"), imgUrl));
                    annoList.add(annoModel);
                }
            } catch (Exception e2) {
                annoList = null;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            return null;
        }
    }

    private static List<TopicContentModel> getAnnoContent(JSONArray contentArray, String baseURL) {
        List<TopicContentModel> list = new ArrayList<>();
        try {
            int j = contentArray.length();
            for (int i = 0; i < j; i++) {
                JSONObject content = contentArray.getJSONObject(i);
                TopicContentModel topicContent = new TopicContentModel();
                topicContent.setType(content.getInt("type"));
                topicContent.setInfor(content.getString("infor").trim());
                if (topicContent.getType() == 1) {
                    topicContent.setBaseUrl(baseURL);
                }
                list.add(topicContent);
            }
            return list;
        } catch (Exception e) {
            return null;
        }
    }
}
