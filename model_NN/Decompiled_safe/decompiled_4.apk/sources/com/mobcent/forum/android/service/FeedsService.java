package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.FeedsModel;
import com.mobcent.forum.android.model.NewFeedsModel;
import com.mobcent.forum.android.model.UserFeedModel;
import java.util.List;

public interface FeedsService {
    List<FeedsModel> getFeeds(int i, int i2, boolean z);

    List<FeedsModel> getFeedsByNet(int i, int i2);

    List<NewFeedsModel> getNewFeeds(long j, boolean z);

    List<NewFeedsModel> getNewFeedsByNet(long j);

    UserFeedModel getUserFeed(long j, int i, int i2);
}
