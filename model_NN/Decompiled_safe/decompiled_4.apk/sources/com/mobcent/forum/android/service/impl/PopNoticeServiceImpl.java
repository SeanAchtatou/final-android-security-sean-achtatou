package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.PopNoticeRestfulApiRequester;
import com.mobcent.forum.android.db.PopNoticeDBUtil;
import com.mobcent.forum.android.db.constant.PopNoticeDBConstant;
import com.mobcent.forum.android.model.PopNoticeModel;
import com.mobcent.forum.android.service.PopNoticeService;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.service.impl.helper.PopNoticeServiceImplHelper;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;

public class PopNoticeServiceImpl implements PopNoticeService, PopNoticeDBConstant {
    private Context context;

    public PopNoticeServiceImpl(Context context2) {
        this.context = context2;
    }

    public PopNoticeModel getPopNoticeModel() {
        String jsonStr = PopNoticeRestfulApiRequester.getPopNoticeModel(this.context);
        PopNoticeModel popNoticeModel = PopNoticeServiceImplHelper.parsePopNoticeJson(jsonStr);
        if (popNoticeModel == null) {
            PopNoticeModel popNoticeModel2 = new PopNoticeModel();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                popNoticeModel2.setErrorCode(errorCode);
            }
            return popNoticeModel2;
        }
        MCLogUtil.d("PopNotice", "popModel-->" + popNoticeModel.getContent());
        int existedState = PopNoticeDBUtil.getInstance(this.context).existedState(popNoticeModel);
        MCLogUtil.d("popDB", "状态-->" + existedState);
        if (existedState == -1) {
            return popNoticeModel;
        }
        return null;
    }
}
