package com.mobcent.forum.android.db.constant;

public interface ModeratorBoardDBConstant extends BaseDBConstant {
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_JSON_STR = "jsonStr";
    public static final String COLUMN_UPDATE_TIME = "update_time";
    public static final String SQL_CREATE_TABLE_MODERATOR_BOARD = "CREATE TABLE IF NOT EXISTS moderator_board(id LONG PRIMARY KEY,jsonStr TEXT , update_time LONG );";
    public static final String SQL_SELECT_MODERATOR_BOARD = "SELECT * FROM moderator_board";
    public static final String TABLE_MODERATOR_BOARD = "moderator_board";
}
