package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.AnnoConstant;
import com.mobcent.forum.android.constant.FeedsConstant;
import com.mobcent.forum.android.constant.PostsApiConstant;
import com.mobcent.forum.android.constant.UserConstant;
import com.mobcent.forum.android.model.AnnoModel;
import com.mobcent.forum.android.model.FeedsModel;
import com.mobcent.forum.android.model.NewFeedsModel;
import com.mobcent.forum.android.model.ReplyModel;
import com.mobcent.forum.android.model.TopicFeedModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserFeedModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.model.UserTopicFeedModel;
import com.mobcent.forum.android.model.VoteFeedModel;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public class FeedsServiceImplHelper extends BaseJsonHelper implements FeedsConstant {
    public static List<FeedsModel> getFeeds(String jsonStr) {
        Exception e;
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String baseUrl = jsonObject.optString("img_url");
            JSONArray array = jsonObject.optJSONArray("feeds");
            List<FeedsModel> trendsList = new ArrayList<>();
            int i = 0;
            while (i < array.length()) {
                try {
                    JSONObject object = (JSONObject) array.get(i);
                    FeedsModel model = new FeedsModel();
                    long type = object.optLong(FeedsConstant.FTYPE);
                    if (type == 1 || type == 8 || type == 6 || type == 4 || type == 3 || type == 10 || type == 11 || type == 9 || type == 2 || type == 7) {
                        model.setFtype(object.optLong(FeedsConstant.FTYPE));
                        model.setsId(object.optLong(FeedsConstant.SID));
                        model.setsName(object.optString(FeedsConstant.SNAME));
                        model.setoId(object.optLong("oid"));
                        model.setoName(object.optString(FeedsConstant.ONAME));
                        model.setaId(object.optLong("aid"));
                        model.setTime(object.optLong("time"));
                        model.setIconUrl(String.valueOf(baseUrl) + object.optString("icon"));
                        model.setHasNext(jsonObject.optInt("has_next"));
                        trendsList.add(model);
                    }
                    i++;
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return null;
                }
            }
            return trendsList;
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r15v4 org.json.JSONArray: [D('jsonArray' org.json.JSONArray), D('userInfo' com.mobcent.forum.android.model.UserInfoModel)] */
    /* JADX INFO: Multiple debug info for r5v51 com.mobcent.forum.android.model.AnnoModel: [D('annoObject' org.json.JSONObject), D('annoModel' com.mobcent.forum.android.model.AnnoModel)] */
    /* JADX INFO: Multiple debug info for r5v52 int: [D('annoModel' com.mobcent.forum.android.model.AnnoModel), D('i' int)] */
    public static List<NewFeedsModel> getNewFeeds(String jsonStr) {
        try {
            JSONObject jSONObject = new JSONObject(jsonStr);
            String baseUrl = jSONObject.optString("img_url");
            String iconUrl = jSONObject.optString("icon_url");
            int hasNext = jSONObject.optInt("has_next");
            long timestamp = jSONObject.optLong("endTime");
            List<NewFeedsModel> feedList = new ArrayList<>();
            try {
                HashSet hashSet = new HashSet();
                JSONArray annoArray = jSONObject.optJSONArray("anno_list");
                if (annoArray != null) {
                    int as = annoArray.length();
                    for (int i = 0; i < as; i++) {
                        JSONObject annoObject = (JSONObject) annoArray.opt(i);
                        NewFeedsModel newFeedsModel = new NewFeedsModel();
                        newFeedsModel.setType((long) annoObject.optInt(FeedsConstant.FTYPE));
                        newFeedsModel.setTime(annoObject.optLong("time"));
                        newFeedsModel.setAnnoModel(getFeedAnno(annoObject.optJSONObject(FeedsConstant.ANNO), iconUrl, hashSet));
                        feedList.add(newFeedsModel);
                    }
                }
                JSONArray array = jSONObject.optJSONArray("list");
                if (array != null) {
                    int is = array.length();
                    for (int i2 = 0; i2 < is; i2++) {
                        JSONObject object = (JSONObject) array.opt(i2);
                        NewFeedsModel newFeedsModel2 = new NewFeedsModel();
                        int type = object.optInt("type");
                        newFeedsModel2.setType((long) type);
                        if (type == 1) {
                            UserFeedModel userFeed = new UserFeedModel();
                            UserInfoModel userInfo = new UserInfoModel();
                            JSONObject userJson = object.optJSONObject("user");
                            userInfo.setUserId(userJson.optLong("user_id"));
                            userInfo.setNickname(userJson.optString("nickname"));
                            String icon = "";
                            if (!StringUtil.isEmpty(userJson.optString("icon"))) {
                                icon = StringUtil.formatImgUrl(String.valueOf(iconUrl) + userJson.optString("icon"), "100x100");
                                hashSet.add(icon);
                            }
                            userInfo.setIcon(icon);
                            userInfo.setLevel(userJson.optInt("level"));
                            userInfo.setRoleNum(userJson.optInt("role_num"));
                            userFeed.setUserInfo(userInfo);
                            JSONArray jsonArray = object.optJSONArray("feeds");
                            ArrayList arrayList = new ArrayList();
                            int js = jsonArray.length();
                            for (int j = 0; j < js; j++) {
                                UserTopicFeedModel userTopicFeedModel = new UserTopicFeedModel();
                                JSONObject jsonobjet = (JSONObject) jsonArray.opt(j);
                                int ftpye = jsonobjet.optInt(FeedsConstant.FTYPE);
                                userTopicFeedModel.setFtype(ftpye);
                                userTopicFeedModel.setTime(jsonobjet.optLong("time"));
                                userTopicFeedModel.setTopic(getFeedTopic(jsonobjet.optJSONObject("topic"), iconUrl, baseUrl, hashSet, true));
                                if (ftpye == 2) {
                                    ReplyModel replyModel = getFeedReply(jsonobjet.optJSONObject("reply"), iconUrl, baseUrl);
                                    ReplyModel replyQuoteModel = getFeedReply(jsonobjet.optJSONObject(FeedsConstant.QUOTE), iconUrl, baseUrl);
                                    userTopicFeedModel.setReply(replyModel);
                                    userTopicFeedModel.setReplyQuote(replyQuoteModel);
                                } else if (ftpye == 11) {
                                    userTopicFeedModel.setVote(getVote(jsonobjet.optJSONObject("vote"), iconUrl, baseUrl));
                                }
                                arrayList.add(userTopicFeedModel);
                            }
                            userFeed.setUserTopicList(arrayList);
                            newFeedsModel2.setUserFeed(userFeed);
                        } else if (type == 2) {
                            TopicFeedModel topicFeed = new TopicFeedModel();
                            topicFeed.setTopic(getFeedTopic(object.optJSONObject("topic"), iconUrl, baseUrl, hashSet, false));
                            JSONArray replyJsonArray = object.optJSONArray("feeds");
                            ArrayList arrayList2 = new ArrayList();
                            int ks = replyJsonArray.length() > 5 ? 5 : replyJsonArray.length();
                            String[] nicknames = new String[ks];
                            UserInfoModel userInfo2 = new UserInfoModel();
                            for (int k = 0; k < ks; k++) {
                                JSONObject jsonobjet2 = (JSONObject) replyJsonArray.opt(k);
                                if (jsonobjet2.optInt(FeedsConstant.FTYPE) == 2) {
                                    ReplyModel replyModel2 = getFeedReply(jsonobjet2.optJSONObject("reply"), iconUrl, baseUrl);
                                    arrayList2.add(replyModel2);
                                    nicknames[k] = replyModel2.getUserNickName();
                                    if (k == 0) {
                                        userInfo2.setUserId(replyModel2.getReplyUserId());
                                        userInfo2.setNickname(replyModel2.getUserNickName());
                                        userInfo2.setLevel(replyModel2.getLevel());
                                        userInfo2.setRoleNum(replyModel2.getRoleNum());
                                        userInfo2.setIcon(replyModel2.getIcon());
                                        if (!StringUtil.isEmpty(replyModel2.getIcon())) {
                                            hashSet.add(replyModel2.getIcon());
                                        }
                                    }
                                }
                            }
                            topicFeed.setNicknames(nicknames);
                            topicFeed.setUserInfo(userInfo2);
                            topicFeed.setReplyList(arrayList2);
                            newFeedsModel2.setTopicFeed(topicFeed);
                        }
                        feedList.add(newFeedsModel2);
                    }
                }
                if (feedList.size() <= 0) {
                    return feedList;
                }
                ((NewFeedsModel) feedList.get(0)).setEndTime(timestamp);
                ((NewFeedsModel) feedList.get(0)).setHasNext(hasNext);
                ((NewFeedsModel) feedList.get(0)).setImgSet(hashSet);
                return feedList;
            } catch (Exception e) {
                e = e;
                e.printStackTrace();
                return null;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r15v4 java.lang.String: [D('jsonStr' java.lang.String), D('baseUrl' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r15v5 com.mobcent.forum.android.model.UserFeedModel: [D('userFeedModel' com.mobcent.forum.android.model.UserFeedModel), D('baseUrl' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v8 org.json.JSONObject: [D('ftpye' int), D('toUserJson' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r0v10 int: [D('ftpye' int), D('j' int)] */
    public static UserFeedModel getUserFeeds(String jsonStr) {
        UserFeedModel userFeedModel = new UserFeedModel();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String baseUrl = jsonObject.optString("img_url");
            String iconUrl = jsonObject.optString("icon_url");
            int hasNext = jsonObject.optInt("has_next");
            int page = jsonObject.optInt("page");
            JSONObject userJson = jsonObject.optJSONObject("user");
            if (userJson != null) {
                userFeedModel.setUserInfo(getUserInfo(userJson, iconUrl));
            }
            Set<String> imgSet = new HashSet<>();
            JSONArray jsonArray = jsonObject.optJSONArray("list");
            List<UserTopicFeedModel> userTopicList = new ArrayList<>();
            int js = jsonArray.length();
            for (int j = 0; j < js; j++) {
                UserTopicFeedModel userTopicFeedModel = new UserTopicFeedModel();
                JSONObject jsonobjet = (JSONObject) jsonArray.opt(j);
                int ftpye = jsonobjet.optInt(FeedsConstant.FTYPE);
                userTopicFeedModel.setFtype(ftpye);
                userTopicFeedModel.setTime(jsonobjet.optLong("time"));
                if (ftpye == 9) {
                    userTopicList.add(userTopicFeedModel);
                } else {
                    if (ftpye == 4) {
                        userTopicFeedModel.setToUser(getUserInfo(jsonobjet.optJSONObject(FeedsConstant.TO_USER), iconUrl));
                    } else {
                        userTopicFeedModel.setTopic(getFeedTopic(jsonobjet.optJSONObject("topic"), iconUrl, baseUrl, imgSet, true));
                        if (ftpye == 2) {
                            userTopicFeedModel.setReply(getFeedReply(jsonobjet.optJSONObject("reply"), iconUrl, baseUrl));
                        }
                    }
                    userTopicList.add(userTopicFeedModel);
                }
            }
            userFeedModel.setUserTopicList(userTopicList);
            userFeedModel.setPage(page);
            userFeedModel.setHasNext(hasNext);
            userFeedModel.setImgSet(imgSet);
            return userFeedModel;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static TopicModel getFeedTopic(JSONObject jsonObject, String iconUrl, String baseUrl, Set<String> imgSet, boolean showPic) {
        Exception e;
        try {
            TopicModel topicModel = new TopicModel();
            try {
                topicModel.setTopicId(jsonObject.optLong("topic_id"));
                topicModel.setTitle(jsonObject.optString("title"));
                String pic = jsonObject.optString(FeedsConstant.PIC);
                if (showPic && !StringUtil.isEmpty(pic)) {
                    topicModel.setHasImg(true);
                    String picUrl = StringUtil.formatImgUrl(String.valueOf(baseUrl) + pic, "100x100");
                    topicModel.setPic(picUrl);
                    imgSet.add(picUrl);
                }
                topicModel.setThumbnail(jsonObject.optString(PostsApiConstant.THUMBNAIL));
                topicModel.setCreateDate(jsonObject.optLong("create_date"));
                topicModel.setUserId(jsonObject.optLong("user_id"));
                topicModel.setUserNickName(jsonObject.optString("nickname"));
                topicModel.setIcon(StringUtil.formatImgUrl(String.valueOf(iconUrl) + jsonObject.optString("icon"), "100x100"));
                topicModel.setLevel(jsonObject.optInt("level"));
                topicModel.setRoleNum(jsonObject.optInt("role_num"));
                topicModel.setHasVoice(jsonObject.optInt(FeedsConstant.HAS_VOICE));
                return topicModel;
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return null;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
    }

    public static AnnoModel getFeedAnno(JSONObject jsonObject, String iconUrl, Set<String> set) {
        Exception e;
        try {
            AnnoModel annoModel = new AnnoModel();
            try {
                annoModel.setAnnoId(jsonObject.optLong("aid"));
                annoModel.setIsReply(jsonObject.optInt("is_reply"));
                annoModel.setTopicId(jsonObject.optLong("topic_id"));
                annoModel.setSubject(jsonObject.optString(AnnoConstant.SUBJECT));
                annoModel.setThumbnail(jsonObject.optString(PostsApiConstant.THUMBNAIL));
                annoModel.setUserId(jsonObject.optLong("user_id"));
                annoModel.setUserNickName(jsonObject.optString("nickname"));
                annoModel.setIcon(StringUtil.formatImgUrl(String.valueOf(iconUrl) + jsonObject.optString("icon"), "100x100"));
                annoModel.setLevel(jsonObject.optInt("level"));
                annoModel.setRoleNum(jsonObject.optInt("role_num"));
                return annoModel;
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return null;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
    }

    public static ReplyModel getFeedReply(JSONObject jsonObject, String iconUrl, String baseUrl) {
        Exception e;
        try {
            ReplyModel replyModel = new ReplyModel();
            try {
                replyModel.setReplyPostsId(jsonObject.optLong("reply_id"));
                replyModel.setThumbnail(jsonObject.optString(PostsApiConstant.THUMBNAIL));
                replyModel.setPostsDate(jsonObject.optLong("create_date"));
                replyModel.setReplyUserId(jsonObject.optLong("user_id"));
                replyModel.setUserNickName(jsonObject.optString("nickname"));
                replyModel.setLevel(jsonObject.optInt("level"));
                replyModel.setRoleNum(jsonObject.optInt("role_num"));
                String icon = jsonObject.optString("icon");
                if (StringUtil.isEmpty(icon)) {
                    return replyModel;
                }
                replyModel.setIcon(StringUtil.formatImgUrl(String.valueOf(iconUrl) + icon, "100x100"));
                return replyModel;
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return null;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
    }

    public static VoteFeedModel getVote(JSONObject jsonObject, String iconUrl, String baseUrl) {
        try {
            VoteFeedModel voteModel = new VoteFeedModel();
            try {
                voteModel.setUserId(jsonObject.optLong("user_id"));
                voteModel.setUserNickName(jsonObject.optString("nickname"));
                voteModel.setIcon(StringUtil.formatImgUrl(String.valueOf(iconUrl) + jsonObject.optString("icon"), "100x100"));
                JSONArray ja = jsonObject.optJSONArray(FeedsConstant.VOTES);
                if (ja == null || ja.length() <= 0) {
                    return voteModel;
                }
                String[] s = new String[ja.length()];
                int j = ja.length();
                for (int i = 0; i < j; i++) {
                    s[i] = ja.optString(i);
                }
                voteModel.setVotes(s);
                return voteModel;
            } catch (Exception e) {
                return null;
            }
        } catch (Exception e2) {
            return null;
        }
    }

    public static UserInfoModel getUserInfo(JSONObject jsonObject, String iconUrl) {
        UserInfoModel userInfoModel = new UserInfoModel();
        try {
            userInfoModel.setUserId(jsonObject.optLong("user_id"));
            userInfoModel.setIcon(StringUtil.formatImgUrl(String.valueOf(iconUrl) + jsonObject.optString("icon"), "100x100"));
            userInfoModel.setNickname(jsonObject.optString("nickname"));
            if (jsonObject.optBoolean("gender")) {
                userInfoModel.setGender(1);
            } else {
                userInfoModel.setGender(0);
            }
            userInfoModel.setIsFollow(jsonObject.optInt(UserConstant.IS_FOLLOW));
            userInfoModel.setStatus(jsonObject.optInt("status"));
            userInfoModel.setCredits(jsonObject.optInt("credits"));
            userInfoModel.setGoldNum(jsonObject.optInt(UserConstant.GOLD_NUM));
            userInfoModel.setLevel(jsonObject.optInt("level"));
            userInfoModel.setTopicNum(jsonObject.optInt(UserConstant.TOPIC_NUM));
            userInfoModel.setReplyPostsNum(jsonObject.optInt(UserConstant.REPLY_POSTS_NUM));
            userInfoModel.setEssenceNum(jsonObject.optInt(UserConstant.ESSENCE_NUM));
            userInfoModel.setUserFavoriteNum(jsonObject.optInt("favor_num"));
            userInfoModel.setRelatePostsNum(jsonObject.optInt(UserConstant.USER_RELATE_POSTS_NUM));
            userInfoModel.setUserFriendsNum(jsonObject.optInt(UserConstant.USER_FRIEND_NUM));
            userInfoModel.setLastLoginTime(jsonObject.optLong(UserConstant.LAST_LOGIN_TIME));
            userInfoModel.setBlackStatus(jsonObject.optInt(UserConstant.IS_BLACK_USER));
            userInfoModel.setLevel(jsonObject.optInt("level"));
            userInfoModel.setFollowNum(jsonObject.optInt(UserConstant.USER_FOLLOW_NUM));
            userInfoModel.setBannedBoardNum(jsonObject.optInt(UserConstant.GAG_BOARD_NUM));
            userInfoModel.setShieldBoardNum(jsonObject.optInt(UserConstant.SHIELD_BOARD_NUM));
            userInfoModel.setIsDelAccounts(jsonObject.optInt(UserConstant.IS_DELE_ACCOUNT));
            userInfoModel.setIsDelIp(jsonObject.optInt(UserConstant.IS_DELE_IP));
            userInfoModel.setRoleNum(jsonObject.optInt("role_num"));
            return userInfoModel;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
