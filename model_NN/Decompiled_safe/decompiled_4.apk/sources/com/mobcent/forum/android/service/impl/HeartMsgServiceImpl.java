package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.forum.android.api.HeartBeatRestfulApiRequester;
import com.mobcent.forum.android.api.MsgRestfulApiRequester;
import com.mobcent.forum.android.api.PostsRestfulApiRequester;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.db.HeartMsgDBUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.HeartMsgModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.HeartMsgService;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.service.impl.helper.HeartMsgServiceImplHelper;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class HeartMsgServiceImpl implements HeartMsgService {
    /* access modifiers changed from: private */
    public Context context;

    public HeartMsgServiceImpl(Context context2) {
        this.context = context2;
    }

    public List<HeartMsgModel> getHeartBeatListByNet() {
        String jsonStr = HeartBeatRestfulApiRequester.getHeartBeat(this.context);
        long currUserId = SharedPreferencesDB.getInstance(this.context).getUserId();
        List<HeartMsgModel> heartBeatList = HeartMsgServiceImplHelper.parseHeartMsgJson(this.context, jsonStr, currUserId);
        HeartMsgDBUtil.getInstance(this.context).deleteHeartBeatList();
        if (heartBeatList != null && heartBeatList.size() > 0) {
            if (heartBeatList.get(0) != null) {
                SharedPreferencesDB.getInstance(this.context).saveHeartInfo(heartBeatList.get(0), currUserId);
            }
            if (heartBeatList.get(0).getMsgTotalNum() > 0) {
                for (HeartMsgModel heartMsgModel : heartBeatList) {
                    long chatUserId = heartMsgModel.getFormUserId();
                    String json = HeartMsgServiceImplHelper.createHeartMsgJson(heartMsgModel);
                    if (json != null) {
                        HeartMsgDBUtil.getInstance(this.context).saveHeartBeatModel(chatUserId, json);
                    }
                }
            }
        }
        return heartBeatList;
    }

    public List<HeartMsgModel> getHeartBeatListLocally() {
        List<HeartMsgModel> heartMsgList = new ArrayList<>();
        List<String> strs = HeartMsgDBUtil.getInstance(this.context).getHeartBeatList();
        int size = strs.size();
        for (int i = 0; i < size; i++) {
            HeartMsgModel heartModel = HeartMsgServiceImplHelper.parseHeatJson(strs.get(i));
            if (heartModel != null) {
                heartMsgList.add(heartModel);
            }
        }
        return heartMsgList;
    }

    public boolean deleteHeartBeatList(long userId) {
        return HeartMsgDBUtil.getInstance(this.context).deleteHeartBeatList(userId);
    }

    public List<HeartMsgModel> getMessageList(long chatUserId) {
        int dbSaveNum = 0;
        List<HeartMsgModel> heartMsgList = new ArrayList<>();
        List<String> jsonStrs = HeartMsgDBUtil.getInstance(this.context).getHeartBeatList(chatUserId);
        MCLogUtil.i("HeartMsgServiceImpl", "jsonStrs = " + jsonStrs);
        for (String jsonStr : jsonStrs) {
            if (jsonStr != null) {
                if (HeartMsgDBUtil.getInstance(this.context).saveMessageModel(chatUserId, jsonStr)) {
                    dbSaveNum++;
                }
                HeartMsgModel heartMsgModel = HeartMsgServiceImplHelper.parseHeatJson(jsonStr);
                if (heartMsgModel != null) {
                    heartMsgList.add(heartMsgModel);
                }
            }
        }
        if (heartMsgList.size() > 0) {
            ((HeartMsgModel) heartMsgList.get(0)).setDBSaveNum(dbSaveNum);
        }
        deleteHeartBeatList(chatUserId);
        return heartMsgList;
    }

    public List<UserInfoModel> getMsgUserList(int page, int pageSize) {
        String jsonStr = MsgRestfulApiRequester.getMsgUserList(this.context, page, pageSize);
        List<UserInfoModel> msgUserList = HeartMsgServiceImplHelper.getMsgUserList(jsonStr, page, pageSize);
        if (msgUserList == null) {
            msgUserList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                UserInfoModel userInfoModel = new UserInfoModel();
                userInfoModel.setErrorCode(errorCode);
                msgUserList.add(userInfoModel);
            }
        }
        return msgUserList;
    }

    public String sendMessage(HeartMsgModel messageModel) {
        String jsonStr;
        if (messageModel.getSoundModel() != null) {
            jsonStr = MsgRestfulApiRequester.sendMessage(this.context, messageModel.getToUserId(), messageModel.getContent(), messageModel.getType(), messageModel.getSoundModel().getSoundTime());
        } else {
            jsonStr = MsgRestfulApiRequester.sendMessage(this.context, messageModel.getToUserId(), messageModel.getContent(), messageModel.getType(), 0);
        }
        if (!HeartMsgServiceImplHelper.sendMessage(jsonStr)) {
            String error = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(error)) {
                return error;
            }
            return null;
        }
        String json = HeartMsgServiceImplHelper.createHeartMsgJson(messageModel);
        if (json == null || HeartMsgDBUtil.getInstance(this.context).saveMessageModel(messageModel.getToUserId(), json)) {
            return null;
        }
        return "";
    }

    public void updateMessage(final List<HeartMsgModel> heartMsgList) {
        if (heartMsgList != null && !heartMsgList.isEmpty()) {
            final long currUserId = SharedPreferencesDB.getInstance(this.context).getUserId();
            new Thread() {
                public void run() {
                    String messageRelationIds = "";
                    for (HeartMsgModel heartMsgModel : heartMsgList) {
                        messageRelationIds = String.valueOf(messageRelationIds) + heartMsgModel.getMsgId() + AdApiConstant.RES_SPLIT_COMMA;
                    }
                    if (messageRelationIds.length() > 1) {
                        messageRelationIds = messageRelationIds.substring(0, messageRelationIds.length() - 1);
                    }
                    MsgRestfulApiRequester.updateMessage(HeartMsgServiceImpl.this.context, messageRelationIds, currUserId);
                }
            }.start();
        }
    }

    public List<HeartMsgModel> getAllMessages(long chatUserId, int start, int len) {
        List<HeartMsgModel> messageList = new ArrayList<>();
        for (String jsonStr : HeartMsgDBUtil.getInstance(this.context).getMessages(chatUserId, start, len)) {
            HeartMsgModel messageModel = HeartMsgServiceImplHelper.parseHeatJson(jsonStr);
            if (messageModel != null) {
                messageList.add(messageModel);
            }
        }
        return messageList;
    }

    public String setUserBlack(int state, long userId, long toUserId) {
        String jsonStr = MsgRestfulApiRequester.setUserBlackList(this.context, userId, toUserId, state);
        if (HeartMsgServiceImplHelper.parseSetUserBlack(jsonStr)) {
            return null;
        }
        String error = BaseJsonHelper.formJsonRS(jsonStr);
        if (!StringUtil.isEmpty(error)) {
            return error;
        }
        return null;
    }

    public String uploadAudio(String filePath) {
        String jsonStr = PostsRestfulApiRequester.uploadAudio(this.context, filePath);
        String audioPath = HeartMsgServiceImplHelper.parseUploadAudioJson(jsonStr);
        if (audioPath == null) {
            return BaseReturnCodeConstant.ERROR_CODE + BaseJsonHelper.formJsonRS(jsonStr);
        }
        return audioPath;
    }
}
