package com.mobcent.forum.android.model;

import java.util.List;

public class PostsNoticeModel extends BaseModel {
    private static final long serialVersionUID = -6439796391269740906L;
    private long boardId;
    private boolean hasNext;
    private String icon;
    private String infor;
    private boolean isAd;
    private int isRead;
    private int isReply;
    private long modelType;
    private String quoteContent;
    private String replyContent;
    private List<TopicContentModel> replyContentList;
    private long replyDate;
    private String replyNickName;
    private long replyPostsId;
    private long replyRemindId;
    private long topicId;
    private String topicSubjtect;
    private long userId;

    public long getModelType() {
        return this.modelType;
    }

    public void setModelType(long modelType2) {
        this.modelType = modelType2;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public String getInfor() {
        return this.infor;
    }

    public void setInfor(String infor2) {
        this.infor = infor2;
    }

    public int getIsReply() {
        return this.isReply;
    }

    public void setIsReply(int isReply2) {
        this.isReply = isReply2;
    }

    public boolean isHasNext() {
        return this.hasNext;
    }

    public void setHasNext(boolean hasNext2) {
        this.hasNext = hasNext2;
    }

    public long getReplyDate() {
        return this.replyDate;
    }

    public void setReplyDate(long replyDate2) {
        this.replyDate = replyDate2;
    }

    public String getReplyContent() {
        return this.replyContent;
    }

    public void setReplyContent(String replyContent2) {
        this.replyContent = replyContent2;
    }

    public String getQuoteContent() {
        return this.quoteContent;
    }

    public void setQuoteContent(String quoteContent2) {
        this.quoteContent = quoteContent2;
    }

    public String getReplyNickName() {
        return this.replyNickName;
    }

    public void setReplyNickName(String replyNickName2) {
        this.replyNickName = replyNickName2;
    }

    public long getReplyRemindId() {
        return this.replyRemindId;
    }

    public void setReplyRemindId(long replyRemindId2) {
        this.replyRemindId = replyRemindId2;
    }

    public long getTopicId() {
        return this.topicId;
    }

    public void setTopicId(long topicId2) {
        this.topicId = topicId2;
    }

    public String getTopicSubjtect() {
        return this.topicSubjtect;
    }

    public void setTopicSubjtect(String topicSubjtect2) {
        this.topicSubjtect = topicSubjtect2;
    }

    public int getIsRead() {
        return this.isRead;
    }

    public void setIsRead(int isRead2) {
        this.isRead = isRead2;
    }

    public long getBoardId() {
        return this.boardId;
    }

    public void setBoardId(long boardId2) {
        this.boardId = boardId2;
    }

    public long getReplyPostsId() {
        return this.replyPostsId;
    }

    public void setReplyPostsId(long replyPostsId2) {
        this.replyPostsId = replyPostsId2;
    }

    public List<TopicContentModel> getReplyContentList() {
        return this.replyContentList;
    }

    public void setReplyContentList(List<TopicContentModel> replyContentList2) {
        this.replyContentList = replyContentList2;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
    }

    public boolean isAd() {
        return this.isAd;
    }

    public void setAd(boolean isAd2) {
        this.isAd = isAd2;
    }
}
