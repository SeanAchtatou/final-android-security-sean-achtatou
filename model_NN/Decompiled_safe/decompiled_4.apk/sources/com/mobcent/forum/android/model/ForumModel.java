package com.mobcent.forum.android.model;

import java.util.List;

public class ForumModel extends BaseModel {
    private static final long serialVersionUID = -338241177831967151L;
    private List<BoardCategoryModel> boardCategoryList;
    private int onlineUserNum;
    private int tdVisitors;

    public int getOnlineUserNum() {
        return this.onlineUserNum;
    }

    public void setOnlineUserNum(int onlineUserNum2) {
        this.onlineUserNum = onlineUserNum2;
    }

    public int getTdVisitors() {
        return this.tdVisitors;
    }

    public void setTdVisitors(int tdVisitors2) {
        this.tdVisitors = tdVisitors2;
    }

    public List<BoardCategoryModel> getBoardCategoryList() {
        return this.boardCategoryList;
    }

    public void setBoardCategoryList(List<BoardCategoryModel> boardCategoryList2) {
        this.boardCategoryList = boardCategoryList2;
    }
}
