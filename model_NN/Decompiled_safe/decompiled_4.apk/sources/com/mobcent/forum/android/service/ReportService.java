package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.ReportModel;
import java.util.List;

public interface ReportService {
    String cancelReportTopic(long j);

    String cancelReportUser(long j);

    List<ReportModel> getReportTopicList(int i, int i2);

    List<ReportModel> getReportUserList(int i, int i2);

    String report(int i, long j, String str);
}
