package com.mobcent.forum.android.service;

import com.mobcent.forum.android.model.UserInfoModel;
import java.util.List;

public interface UserService {
    UserInfoModel changeUserPwd(String str, String str2);

    boolean currentUserIsAdmin();

    boolean currentUserIsBoardowner(long j);

    boolean currentUserIsSuperAdmin();

    String followUser(long j, long j2);

    List<UserInfoModel> getAllUsers();

    UserInfoModel getCurrUser();

    List<UserInfoModel> getLocalSurroudUser(long j, int i, int i2);

    long getLoginUserId();

    String getNickname();

    List<UserInfoModel> getRecommendUser(long j, int i, int i2);

    List<UserInfoModel> getRecommendUser(long j, int i, int i2, boolean z);

    List<UserInfoModel> getRegUserList();

    String getResetPassword(long j, String str);

    int getRoleNum();

    List<UserInfoModel> getSurroudUser(long j, double d, double d2, int i, int i2, int i3);

    UserInfoModel getUser();

    UserInfoModel getUserBoardInfoList(long j);

    List<UserInfoModel> getUserFanList(long j, int i, int i2);

    List<UserInfoModel> getUserFriendList(long j, int i, int i2);

    List<UserInfoModel> getUserFriendList(long j, int i, int i2, boolean z);

    List<UserInfoModel> getUserFriendListByNet(long j, int i, int i2);

    UserInfoModel getUserInfo(long j);

    boolean isLogin();

    UserInfoModel loginUser(String str, String str2);

    String logout();

    UserInfoModel parseOpenplatformUserInfo(String str);

    UserInfoModel regUser(String str, String str2);

    UserInfoModel regUserByOpenPlatform(String str, String str2, String str3, int i, String str4, long j, String str5);

    String unfollowUser(long j, long j2);

    String updateUser(String str, String str2, int i, String str3, long j);

    String uploadIcon(String str);
}
