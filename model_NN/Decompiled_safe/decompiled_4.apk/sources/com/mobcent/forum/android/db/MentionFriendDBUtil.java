package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.forum.android.db.constant.MentionFriendsDBConstant;

public class MentionFriendDBUtil extends BaseDBUtil implements MentionFriendsDBConstant {
    private static MentionFriendDBUtil friendDBUtil;

    public MentionFriendDBUtil(Context ctx) {
        super(ctx);
    }

    public static MentionFriendDBUtil getInstance(Context context) {
        if (friendDBUtil == null) {
            friendDBUtil = new MentionFriendDBUtil(context);
        }
        return friendDBUtil;
    }

    public String getMentionFriendJsonString(long userId) {
        String jsonStr = null;
        Cursor cursor = null;
        try {
            openReadableDB();
            cursor = this.readableDatabase.query(MentionFriendsDBConstant.TABLE_MENTION_FRIEND, null, "userid=" + userId, null, null, null, null);
            while (cursor.moveToNext()) {
                jsonStr = cursor.getString(2);
            }
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            jsonStr = null;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
            throw th;
        }
        return jsonStr;
    }

    public synchronized boolean updateMentionFriendJsonString(String jsonStr, long openPlatformid, long userid, long lastUpdateTime) {
        boolean z;
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put(MentionFriendsDBConstant.COLUMN_JSON_STR, jsonStr);
            values.put(MentionFriendsDBConstant.COLUMN_OPEN_PLATFORM_ID, Long.valueOf(openPlatformid));
            values.put(MentionFriendsDBConstant.COLUMN_USER_ID, Long.valueOf(userid));
            values.put(MentionFriendsDBConstant.COLUMN_UPDATE_TIME, Long.valueOf(lastUpdateTime));
            if (!isRowExisted(this.writableDatabase, MentionFriendsDBConstant.TABLE_MENTION_FRIEND, MentionFriendsDBConstant.COLUMN_USER_ID, userid)) {
                this.writableDatabase.insertOrThrow(MentionFriendsDBConstant.TABLE_MENTION_FRIEND, null, values);
            } else {
                this.writableDatabase.update(MentionFriendsDBConstant.TABLE_MENTION_FRIEND, values, "userid=" + userid, null);
            }
            closeWriteableDB();
            z = true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            z = false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
        return z;
    }

    public boolean delteMentionFriendList() {
        return removeAllEntries(MentionFriendsDBConstant.TABLE_MENTION_FRIEND);
    }
}
