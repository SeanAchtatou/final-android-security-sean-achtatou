package com.mobcent.forum.android.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    public static Date getTimestampByTimeString(String time) {
        try {
            return new SimpleDateFormat("yyyy/MM/dd HH:mm").parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getGenericCurrentTime() {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm").format(Calendar.getInstance().getTime());
    }

    public static String getFormatTime(long time) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Long.valueOf(time));
    }

    public static String getFormatTimeExceptHourAndSecond(long time) {
        return new SimpleDateFormat("yyyy-MM-dd").format(Long.valueOf(time));
    }

    public static String getFormatShortTime(long time) {
        return new SimpleDateFormat("MM-dd HH:mm").format(Long.valueOf(time));
    }

    public static String getFormatTimeByYear(long time) {
        if (Calendar.getInstance().get(1) == new Integer(new SimpleDateFormat("yyyy").format(Long.valueOf(time))).intValue()) {
            return new SimpleDateFormat("MM-dd HH:mm").format(Long.valueOf(time));
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(Long.valueOf(time));
    }

    public static String getFormatTimeToSecond(long time) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Long.valueOf(time));
    }

    public static long getCurrentTime() {
        return Calendar.getInstance().getTimeInMillis();
    }
}
