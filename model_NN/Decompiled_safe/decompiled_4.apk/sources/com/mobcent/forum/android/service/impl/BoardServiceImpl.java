package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.BoardRestfulApiRequester;
import com.mobcent.forum.android.db.BoardDBUtil;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.model.ForumModel;
import com.mobcent.forum.android.service.BoardService;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.service.impl.helper.BoardServiceImplHelper;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BoardServiceImpl implements BoardService {
    /* access modifiers changed from: private */
    public Context context;

    public BoardServiceImpl(Context context2) {
        this.context = context2;
    }

    public ForumModel getForumInfo() {
        String jsonStr;
        try {
            jsonStr = BoardDBUtil.getInstance(this.context).getBoardJsonString();
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
        }
        if (jsonStr == null || StringUtil.isEmpty(jsonStr)) {
            return getForumInfoByNet();
        }
        return BoardServiceImplHelper.getForumInfo(jsonStr);
    }

    public ForumModel getForumInfoByNet() {
        String jsonStr = BoardRestfulApiRequester.getBoards(this.context);
        ForumModel forumModel = BoardServiceImplHelper.getForumInfo(jsonStr);
        if (forumModel == null) {
            forumModel = new ForumModel();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                forumModel.setErrorCode(errorCode);
            }
        } else {
            long lastUpdateTime = Calendar.getInstance().getTimeInMillis();
            forumModel.setLastUpdateTime(lastUpdateTime);
            final String finalJsonStr = BoardServiceImplHelper.getForumInfoStrWithLastUpdateTime(jsonStr, lastUpdateTime);
            new Thread() {
                public void run() {
                    BoardDBUtil.getInstance(BoardServiceImpl.this.context).updateBoardJsonString(finalJsonStr);
                }
            }.start();
        }
        return forumModel;
    }

    public boolean updateForumInfo() {
        String jsonStr = BoardRestfulApiRequester.getBoards(this.context);
        if (BaseJsonHelper.formJsonRS(jsonStr) != null) {
            return false;
        }
        return BoardDBUtil.getInstance(this.context).updateBoardJsonString(BoardServiceImplHelper.getForumInfoStrWithLastUpdateTime(jsonStr, Calendar.getInstance().getTimeInMillis()));
    }

    public List<BoardModel> getBoadListByLocal() {
        try {
            return BoardServiceImplHelper.getForumBoardInfo(BoardDBUtil.getInstance(this.context).getBoardJsonString());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<BoardModel> getBoadListByNet() {
        String jsonStr = BoardRestfulApiRequester.getBoards(this.context);
        List<BoardModel> boardList = BoardServiceImplHelper.getForumBoardInfo(jsonStr);
        if (boardList == null) {
            boardList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                BoardModel board = new BoardModel();
                board.setErrorCode(errorCode);
                boardList.add(board);
            }
        }
        return boardList;
    }
}
