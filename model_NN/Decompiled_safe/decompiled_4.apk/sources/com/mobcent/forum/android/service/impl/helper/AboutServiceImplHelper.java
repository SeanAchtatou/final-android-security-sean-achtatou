package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.AboutConstant;
import com.mobcent.forum.android.model.AboutModel;
import org.json.JSONObject;

public class AboutServiceImplHelper implements AboutConstant {
    public static AboutModel getAboutInfo(String jsonStr) {
        Exception e;
        try {
            AboutModel aboutModel = new AboutModel();
            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                aboutModel.setRs(jsonObject.optInt("rs"));
                JSONObject object = jsonObject.optJSONObject(AboutConstant.ABOUT_INFO);
                aboutModel.setDesc(object.optString(AboutConstant.DESC));
                aboutModel.setEmail(object.optString(AboutConstant.EMAIL));
                aboutModel.setTel(object.optString(AboutConstant.TEL));
                aboutModel.setWebsite(object.optString(AboutConstant.WEBSITE));
                aboutModel.setWeibo_qq(object.optString(AboutConstant.WEIBO_QQ));
                aboutModel.setWeibo_sina(object.optString(AboutConstant.WEIBO_SINA));
                aboutModel.setQq(object.optString(AboutConstant.QQ));
                return aboutModel;
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return null;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
    }
}
