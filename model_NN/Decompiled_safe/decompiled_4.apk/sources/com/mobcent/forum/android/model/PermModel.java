package com.mobcent.forum.android.model;

import java.util.List;

public class PermModel extends BaseModel {
    private static final long serialVersionUID = -7666846334330920310L;
    private long boardId;
    private List<PermModel> boards;
    private int download;
    private int groupId;
    private String groupType;
    private int post;
    private int read;
    private int reply;
    private int upload;
    private PermModel userGroup;
    private int visit;

    public PermModel getUserGroup() {
        return this.userGroup;
    }

    public void setUserGroup(PermModel userGroup2) {
        this.userGroup = userGroup2;
    }

    public int getGroupId() {
        return this.groupId;
    }

    public void setGroupId(int groupId2) {
        this.groupId = groupId2;
    }

    public String getGroupType() {
        return this.groupType;
    }

    public void setGroupType(String groupType2) {
        this.groupType = groupType2;
    }

    public int getVisit() {
        return this.visit;
    }

    public void setVisit(int visit2) {
        this.visit = visit2;
    }

    public int getPost() {
        return this.post;
    }

    public void setPost(int post2) {
        this.post = post2;
    }

    public int getRead() {
        return this.read;
    }

    public void setRead(int read2) {
        this.read = read2;
    }

    public int getReply() {
        return this.reply;
    }

    public void setReply(int reply2) {
        this.reply = reply2;
    }

    public int getUpload() {
        return this.upload;
    }

    public void setUpload(int upload2) {
        this.upload = upload2;
    }

    public int getDownload() {
        return this.download;
    }

    public void setDownload(int download2) {
        this.download = download2;
    }

    public long getBoardId() {
        return this.boardId;
    }

    public void setBoardId(long boardId2) {
        this.boardId = boardId2;
    }

    public List<PermModel> getBoards() {
        return this.boards;
    }

    public void setBoards(List<PermModel> boards2) {
        this.boards = boards2;
    }
}
