package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.forum.android.db.constant.SurrroundTopicDBConstant;
import com.mobcent.forum.android.util.DateUtil;

public class SurroundTopicDBUtil extends BaseDBUtil implements SurrroundTopicDBConstant {
    private static final int SURROUND_ID = 1;
    private static SurroundTopicDBUtil surroundTopicDBUtil;

    protected SurroundTopicDBUtil(Context ctx) {
        super(ctx);
    }

    public static SurroundTopicDBUtil getInstance(Context context) {
        if (surroundTopicDBUtil == null) {
            surroundTopicDBUtil = new SurroundTopicDBUtil(context);
        }
        return surroundTopicDBUtil;
    }

    public String getSurroundTopicJsonString() {
        String jsonStr = null;
        Cursor cursor = null;
        try {
            openReadableDB();
            cursor = this.readableDatabase.rawQuery(SurrroundTopicDBConstant.SQL_SELECT_BOARD, null);
            while (cursor.moveToNext()) {
                jsonStr = cursor.getString(1);
            }
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            jsonStr = null;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
            throw th;
        }
        return jsonStr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean updateSurroundTopicJsonString(String jsonStr) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("id", (Integer) 1);
            values.put("jsonStr", jsonStr);
            values.put("update_time", Long.valueOf(DateUtil.getCurrentTime()));
            if (!isRowExisted(this.writableDatabase, SurrroundTopicDBConstant.TABLE_SURROUND_TOPIC, "id", 1)) {
                this.writableDatabase.insertOrThrow(SurrroundTopicDBConstant.TABLE_SURROUND_TOPIC, null, values);
            } else {
                this.writableDatabase.update(SurrroundTopicDBConstant.TABLE_SURROUND_TOPIC, values, "id=1", null);
            }
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean deleteSurroundTopicList() {
        return removeAllEntries(SurrroundTopicDBConstant.TABLE_SURROUND_TOPIC);
    }
}
