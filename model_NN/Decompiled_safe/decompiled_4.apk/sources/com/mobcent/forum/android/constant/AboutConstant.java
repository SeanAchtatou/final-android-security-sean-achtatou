package com.mobcent.forum.android.constant;

public interface AboutConstant {
    public static final String ABOUT_INFO = "aboutInfo";
    public static final String DESC = "enterprise_desc";
    public static final String EMAIL = "enterprise_email";
    public static final String QQ = "enterprise_qq";
    public static final String RS = "rs";
    public static final String TEL = "enterprise_tel";
    public static final String WEBSITE = "enterprise_website";
    public static final String WEIBO_QQ = "enterprise_weibo_qq";
    public static final String WEIBO_SINA = "enterprise_weibo_sina";
}
