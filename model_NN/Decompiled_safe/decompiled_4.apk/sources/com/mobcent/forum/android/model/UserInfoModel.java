package com.mobcent.forum.android.model;

import java.util.List;

public class UserInfoModel extends BaseModel {
    private static final long serialVersionUID = -5139521112098754278L;
    private int bannedBoardNum;
    private String bannedShieldedDate;
    private String bannedShieldedStartDate;
    private int blackStatus;
    private int credits;
    private int currUser;
    private String descriptionStr;
    private int distance = -1;
    private String email;
    private int essenceNum;
    private int followNum;
    private int forumId;
    private int gender;
    private int goldNum;
    private int hasNext;
    private String icon;
    private boolean isAd;
    private int isDelAccounts;
    private int isDelIp;
    private int isFollow;
    private long lastLoginTime;
    private int level;
    private String location;
    private String nickname;
    private long pageFrom;
    private String permModelStr;
    private long platformId;
    private String platformUserId;
    private String pwd;
    private String reasonStr;
    private int regSource;
    private boolean register;
    private int relatePostsNum;
    private int replyPostsNum;
    private int roleNum;
    private int score;
    private int shieldBoardNum;
    private String signature;
    private int status;
    private int topicNum;
    private String uas;
    private String uat;
    private List<UserBoardInfoModel> userBoardInfoList;
    private int userFavoriteNum;
    private int userFriendsNum;
    private long userId;

    public String getPermModelStr() {
        return this.permModelStr;
    }

    public void setPermModelStr(String permModelStr2) {
        this.permModelStr = permModelStr2;
    }

    public boolean isAd() {
        return this.isAd;
    }

    public void setAd(boolean isAd2) {
        this.isAd = isAd2;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location2) {
        this.location = location2;
    }

    public int getDistance() {
        return this.distance;
    }

    public void setDistance(int distance2) {
        this.distance = distance2;
    }

    public long getPageFrom() {
        return this.pageFrom;
    }

    public void setPageFrom(long pageFrom2) {
        this.pageFrom = pageFrom2;
    }

    public int getRelatePostsNum() {
        return this.relatePostsNum;
    }

    public void setRelatePostsNum(int relatePostsNum2) {
        this.relatePostsNum = relatePostsNum2;
    }

    public int getBlackStatus() {
        return this.blackStatus;
    }

    public void setBlackStatus(int blackStatus2) {
        this.blackStatus = blackStatus2;
    }

    public int getUserFriendsNum() {
        return this.userFriendsNum;
    }

    public void setUserFriendsNum(int userFriendsNum2) {
        this.userFriendsNum = userFriendsNum2;
    }

    public int getUserFavoriteNum() {
        return this.userFavoriteNum;
    }

    public void setUserFavoriteNum(int userFavoriteNum2) {
        this.userFavoriteNum = userFavoriteNum2;
    }

    public int getIsFollow() {
        return this.isFollow;
    }

    public void setIsFollow(int isFollow2) {
        this.isFollow = isFollow2;
    }

    public String getSignature() {
        return this.signature;
    }

    public void setSignature(String signature2) {
        this.signature = signature2;
    }

    public int getGoldNum() {
        return this.goldNum;
    }

    public void setGoldNum(int goldNum2) {
        this.goldNum = goldNum2;
    }

    public int getTopicNum() {
        return this.topicNum;
    }

    public void setTopicNum(int topicNum2) {
        this.topicNum = topicNum2;
    }

    public int getReplyPostsNum() {
        return this.replyPostsNum;
    }

    public void setReplyPostsNum(int replyPostsNum2) {
        this.replyPostsNum = replyPostsNum2;
    }

    public int getEssenceNum() {
        return this.essenceNum;
    }

    public void setEssenceNum(int essenceNum2) {
        this.essenceNum = essenceNum2;
    }

    public long getLastLoginTime() {
        return this.lastLoginTime;
    }

    public void setLastLoginTime(long lastLoginTime2) {
        this.lastLoginTime = lastLoginTime2;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public int getCurrUser() {
        return this.currUser;
    }

    public void setCurrUser(int currUser2) {
        this.currUser = currUser2;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
    }

    public int getForumId() {
        return this.forumId;
    }

    public void setForumId(int forumId2) {
        this.forumId = forumId2;
    }

    public String getNickname() {
        return this.nickname;
    }

    public void setNickname(String nickname2) {
        this.nickname = nickname2;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public String getUas() {
        return this.uas;
    }

    public void setUas(String uas2) {
        this.uas = uas2;
    }

    public String getUat() {
        return this.uat;
    }

    public void setUat(String uat2) {
        this.uat = uat2;
    }

    public String getPwd() {
        return this.pwd;
    }

    public void setPwd(String pwd2) {
        this.pwd = pwd2;
    }

    public int getScore() {
        return this.score;
    }

    public void setScore(int score2) {
        this.score = score2;
    }

    public int getCredits() {
        return this.credits;
    }

    public void setCredits(int credits2) {
        this.credits = credits2;
    }

    public int getGender() {
        return this.gender;
    }

    public void setGender(int gender2) {
        this.gender = gender2;
    }

    public int getRoleNum() {
        return this.roleNum;
    }

    public void setRoleNum(int roleNum2) {
        this.roleNum = roleNum2;
    }

    public boolean isRegister() {
        return this.register;
    }

    public void setRegister(boolean register2) {
        this.register = register2;
    }

    public String getPlatformUserId() {
        return this.platformUserId;
    }

    public void setPlatformUserId(String platformUserId2) {
        this.platformUserId = platformUserId2;
    }

    public boolean equals(Object obj) {
        UserInfoModel userInfoModel = (UserInfoModel) obj;
        if (userInfoModel == null || this == null) {
            return false;
        }
        if (getNickname() != null && userInfoModel.getNickname() != null && !getNickname().equals(userInfoModel.getNickname())) {
            return true;
        }
        if (userInfoModel.getIcon() != null && getIcon() != null && !getIcon().equals(userInfoModel.getIcon())) {
            return true;
        }
        if (userInfoModel.getIcon() != null && getIcon() == null) {
            return true;
        }
        if (getGender() != userInfoModel.getGender()) {
            return true;
        }
        return false;
    }

    public long getPlatformId() {
        return this.platformId;
    }

    public void setPlatformId(long platformId2) {
        this.platformId = platformId2;
    }

    public int getHasNext() {
        return this.hasNext;
    }

    public void setHasNext(int hasNext2) {
        this.hasNext = hasNext2;
    }

    public int getFollowNum() {
        return this.followNum;
    }

    public void setFollowNum(int followNum2) {
        this.followNum = followNum2;
    }

    public int getRegSource() {
        return this.regSource;
    }

    public void setRegSource(int regSource2) {
        this.regSource = regSource2;
    }

    public String getBannedShieldedDate() {
        return this.bannedShieldedDate;
    }

    public void setBannedShieldedDate(String bannedShieldedDate2) {
        this.bannedShieldedDate = bannedShieldedDate2;
    }

    public String getBannedShieldedStartDate() {
        return this.bannedShieldedStartDate;
    }

    public void setBannedShieldedStartDate(String bannedShieldedStartDate2) {
        this.bannedShieldedStartDate = bannedShieldedStartDate2;
    }

    public String getReasonStr() {
        return this.reasonStr;
    }

    public void setReasonStr(String reasonStr2) {
        this.reasonStr = reasonStr2;
    }

    public String getDescriptionStr() {
        return this.descriptionStr;
    }

    public void setDescriptionStr(String descriptionStr2) {
        this.descriptionStr = descriptionStr2;
    }

    public int getBannedBoardNum() {
        return this.bannedBoardNum;
    }

    public void setBannedBoardNum(int banned) {
        this.bannedBoardNum = banned;
    }

    public int getShieldBoardNum() {
        return this.shieldBoardNum;
    }

    public void setShieldBoardNum(int shield) {
        this.shieldBoardNum = shield;
    }

    public List<UserBoardInfoModel> getUserBoardInfoList() {
        return this.userBoardInfoList;
    }

    public void setUserBoardInfoList(List<UserBoardInfoModel> userBoardInfoList2) {
        this.userBoardInfoList = userBoardInfoList2;
    }

    public int getIsDelAccounts() {
        return this.isDelAccounts;
    }

    public void setIsDelAccounts(int isDelAccounts2) {
        this.isDelAccounts = isDelAccounts2;
    }

    public int getIsDelIp() {
        return this.isDelIp;
    }

    public void setIsDelIp(int isDelIp2) {
        this.isDelIp = isDelIp2;
    }
}
