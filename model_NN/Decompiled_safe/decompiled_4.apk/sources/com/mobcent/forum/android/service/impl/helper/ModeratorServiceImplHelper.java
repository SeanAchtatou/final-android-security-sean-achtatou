package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.ModeratorConstant;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.model.ModeratorModel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class ModeratorServiceImplHelper extends BaseJsonHelper implements ModeratorConstant {
    /* JADX INFO: Multiple debug info for r12v13 java.util.List<com.mobcent.forum.android.model.ModeratorModel>: [D('moderatorList' java.util.List<com.mobcent.forum.android.model.ModeratorModel>), D('model' com.mobcent.forum.android.model.ModeratorModel)] */
    /* JADX INFO: Multiple debug info for r12v15 org.json.JSONArray: [D('object' org.json.JSONObject), D('array' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r12v16 int: [D('array' org.json.JSONArray), D('i' int)] */
    public static List<ModeratorModel> getModeratorList(String jsonStr) {
        try {
            List<ModeratorModel> moderatorList = new ArrayList<>();
            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                String iconUrl = jsonObject.optString("icon_url");
                JSONArray jsonArray = jsonObject.optJSONArray("list");
                for (int i = 0; i < jsonArray.length(); i++) {
                    ModeratorModel model = new ModeratorModel();
                    JSONObject object = jsonArray.optJSONObject(i);
                    model.setIcon(String.valueOf(iconUrl) + object.optString("icon"));
                    model.setModerator(object.optInt("is_moderator"));
                    model.setNickname(object.optString("nickname"));
                    model.setUserId(object.optLong("user_id"));
                    JSONArray array = object.optJSONArray(ModeratorConstant.MBS);
                    List<BoardModel> list = new ArrayList<>();
                    int j = 0;
                    while (true) {
                        int j2 = j;
                        if (j2 >= array.length()) {
                            break;
                        }
                        BoardModel boardModel = new BoardModel();
                        JSONObject jsonObject2 = array.optJSONObject(j2);
                        boardModel.setBoardId(jsonObject2.optLong("board_id"));
                        boardModel.setBoardName(jsonObject2.optString("board_name"));
                        list.add(boardModel);
                        j = j2 + 1;
                    }
                    model.setBoardList(list);
                    moderatorList.add(model);
                }
                if (moderatorList.size() <= 0) {
                    return moderatorList;
                }
                ModeratorModel model2 = (ModeratorModel) moderatorList.get(0);
                model2.setRs(jsonObject.optInt("rs"));
                moderatorList.set(0, model2);
                return moderatorList;
            } catch (Exception e) {
                return null;
            }
        } catch (Exception e2) {
            return null;
        }
    }

    public static ModeratorModel getModeratorModel(String jsonStr) {
        ModeratorModel model = new ModeratorModel();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            model.setModerator(jsonObject.optInt("role_num"));
            if (jsonObject.optInt("rs") == 1) {
                JSONArray jsonArray = jsonObject.optJSONArray("list");
                List<BoardModel> list = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    BoardModel boardModel = new BoardModel();
                    JSONObject jsonObject2 = jsonArray.optJSONObject(i);
                    boardModel.setBoardId(jsonObject2.optLong("board_id"));
                    boardModel.setBoardName(jsonObject2.optString("board_name"));
                    list.add(boardModel);
                }
                model.setBoardList(list);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return model;
    }
}
