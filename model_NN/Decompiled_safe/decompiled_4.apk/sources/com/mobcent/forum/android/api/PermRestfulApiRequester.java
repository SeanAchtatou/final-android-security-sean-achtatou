package com.mobcent.forum.android.api;

import android.content.Context;
import com.mobcent.forum.android.api.util.HttpClientUtil;
import com.mobcent.forum.android.util.MCResource;
import java.util.HashMap;

public class PermRestfulApiRequester extends BaseRestfulApiRequester {
    public static String getPerm(Context context) {
        String url = String.valueOf(MCResource.getInstance(context).getString("mc_forum_base_request_domain_url")) + "user/permission.do";
        HashMap<String, String> params = new HashMap<>();
        params.put(HttpClientUtil.SET_CONNECTION_TIMEOUT_STR, "1000");
        params.put(HttpClientUtil.SET_SOCKET_TIMEOUT_STR, "2000");
        return doPostRequest(url, params, context);
    }
}
