package com.mobcent.forum.android.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.mobcent.forum.android.db.constant.HeartMsgDBConstant;

public class HeartMsgDBOpenHelper extends SQLiteOpenHelper {
    public HeartMsgDBOpenHelper(Context context, String databaseName, int version) {
        super(context, databaseName, (SQLiteDatabase.CursorFactory) null, version);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(HeartMsgDBConstant.CREATE_TABLE_HEART_BEAT);
        db.execSQL(HeartMsgDBConstant.CREATE_TABLE_MESSAGE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.delete(HeartMsgDBConstant.TABLE_HEART_BEAT, null, null);
        db.delete(HeartMsgDBConstant.TABLE_MESSAGE, null, null);
        onCreate(db);
    }
}
