package com.mobcent.forum.android.service;

import android.content.Context;

public interface ForumService {
    String getForumKey(Context context);
}
