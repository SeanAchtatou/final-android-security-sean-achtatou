package com.mobcent.forum.android.db.constant;

public interface RecomUserDBConstant extends BaseDBConstant {
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_JSON_STR = "jsonStr";
    public static final String COLUMN_UPDATE_TIME = "update_time";
    public static final String SQL_CREATE_TABLE_RECOM_USER = "CREATE TABLE IF NOT EXISTS recom_user_table(id LONG PRIMARY KEY,jsonStr TEXT , update_time LONG );";
    public static final String SQL_SELECT_RECOM_USER = "SELECT * FROM recom_user_table";
    public static final String TABLE_RECOM_USER = "recom_user_table";
}
