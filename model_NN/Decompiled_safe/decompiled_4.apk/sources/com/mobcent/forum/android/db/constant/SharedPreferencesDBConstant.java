package com.mobcent.forum.android.db.constant;

public interface SharedPreferencesDBConstant {
    public static final String ACCESS_SECRET = "accessSecret";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String ADV = "adv";
    public static final String CLIENT_MANAGER = "client_manager";
    public static final String FORUM_ID = "forumId";
    public static final String IMG_URL = "img_url";
    public static final String IS_USER_DB_UPGRADE = "isUserDBUpgrade";
    public static final String LAST_REPLY_REMIND_ID = "lastReplyRemindId";
    public static final String LOADING_PAGE = "loadingpage";
    public static final String LOADING_PAGE_IMAGE = "loadingpage_image";
    public static final String MC_CHANNEL_ID = "mc_ad_channel";
    public static final String MC_FORUM_ID = "mc_forum_id";
    public static final String MC_FORUM_KEY = "mc_forum_key";
    public static final String MENTION_FRIEND_NOTIFICATION_FLAG = "isMentionFriendNotifyReply";
    public static final String MESSAGE_VOICE_FLAG = "isMessageVoice";
    public static final String MSG_PUSH = "msg_push";
    public static final String MSG_TOTAL_NUM = "msgTotalNum";
    public static final String PIC_MODE_TAG = "picMode";
    public static final String POWER_BY = "powerby";
    public static final String PREFS_FILE = "mc_forum.prefs";
    public static final String PUSH_LIST_JSON = "PushListJson";
    public static final String RELATIONAL_NOTICE_NUM = "relationalNoticeNum";
    public static final String REPLY_NOTICE_NUM = "replyNoticeNum";
    public static final String REPLY_NOTIFICATION_FLAG = "isNotifyReply";
    public static final String ROLE_NUM = "role_num";
    public static final String SHARE_KEY = "share_key";
    public static final String SQUARE = "square";
    public static final String USER_ID = "userId";
    public static final String USER_NICKNAME = "nickname";
    public static final String VERSION_CODE = "version_code";
    public static final String WATER_MARK = "watermark";
    public static final String WATER_MARK_IMAGE = "watermark_image";
    public static final String WEIXIN_APPKEY = "weixin_appkey";
}
