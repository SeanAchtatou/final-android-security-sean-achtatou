package com.mobcent.forum.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.forum.android.db.constant.PicTopicDBConstant;
import com.mobcent.forum.android.util.DateUtil;

public class PicTopicDBUtil extends BaseDBUtil implements PicTopicDBConstant {
    private static final int PIC_TOPIC_ID = 1;
    private static PicTopicDBUtil picTopicDBUtil;

    public PicTopicDBUtil(Context ctx) {
        super(ctx);
    }

    public static PicTopicDBUtil getInstance(Context context) {
        if (picTopicDBUtil == null) {
            picTopicDBUtil = new PicTopicDBUtil(context);
        }
        return picTopicDBUtil;
    }

    public String[] getPicTopicJsonString() {
        String[] result = new String[2];
        String jsonStr = null;
        Cursor cursor = null;
        long time = 0;
        try {
            openReadableDB();
            cursor = this.readableDatabase.rawQuery(PicTopicDBConstant.SQL_SELECT_PIC_TOPTIC, null);
            while (cursor.moveToNext()) {
                jsonStr = cursor.getString(1);
                time = cursor.getLong(2);
            }
            result[0] = jsonStr;
            result[1] = new StringBuilder(String.valueOf(time)).toString();
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Exception e) {
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            closeReadableDB();
            throw th;
        }
        return result;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public boolean updatePicTopicJsonString(String jsonStr) {
        try {
            openWriteableDB();
            ContentValues values = new ContentValues();
            values.put("id", (Integer) 1);
            values.put("jsonStr", jsonStr);
            values.put("update_time", Long.valueOf(DateUtil.getCurrentTime()));
            if (!isRowExisted(this.writableDatabase, PicTopicDBConstant.TABLE_PIC_TOPIC, "id", 1)) {
                this.writableDatabase.insertOrThrow(PicTopicDBConstant.TABLE_PIC_TOPIC, null, values);
            } else {
                this.writableDatabase.update(PicTopicDBConstant.TABLE_PIC_TOPIC, values, "id=1", null);
            }
            closeWriteableDB();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            closeWriteableDB();
            return false;
        } catch (Throwable th) {
            closeWriteableDB();
            throw th;
        }
    }

    public boolean deltePicTopicList() {
        return removeAllEntries(PicTopicDBConstant.TABLE_PIC_TOPIC);
    }
}
