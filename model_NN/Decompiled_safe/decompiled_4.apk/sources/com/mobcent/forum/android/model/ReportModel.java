package com.mobcent.forum.android.model;

import java.util.List;

public class ReportModel extends BaseModel {
    private static final long serialVersionUID = 8041563023917750951L;
    private String boardName;
    private String content;
    private int count;
    private int gender;
    private int hasNext;
    private List<InformantModel> informantList;
    private String nickName;
    private long postId;
    private int processed;
    private int reportId;
    private long reprtTime;
    private int status;
    private String title;
    private long topicId;
    private int type;
    private long userId;

    public String getBoardName() {
        return this.boardName;
    }

    public void setBoardName(String boardName2) {
        this.boardName = boardName2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int count2) {
        this.count = count2;
    }

    public int getProcessed() {
        return this.processed;
    }

    public void setProcessed(int processed2) {
        this.processed = processed2;
    }

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(String nickName2) {
        this.nickName = nickName2;
    }

    public long getPostId() {
        return this.postId;
    }

    public void setPostId(long postId2) {
        this.postId = postId2;
    }

    public int getReportId() {
        return this.reportId;
    }

    public void setReportId(int reportId2) {
        this.reportId = reportId2;
    }

    public long getReprtTime() {
        return this.reprtTime;
    }

    public void setReprtTime(long reprtTime2) {
        this.reprtTime = reprtTime2;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public List<InformantModel> getInformantList() {
        return this.informantList;
    }

    public void setInformantList(List<InformantModel> informantList2) {
        this.informantList = informantList2;
    }

    public int getHasNext() {
        return this.hasNext;
    }

    public void setHasNext(int hasNext2) {
        this.hasNext = hasNext2;
    }

    public int getGender() {
        return this.gender;
    }

    public void setGender(int gender2) {
        this.gender = gender2;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
    }

    public long getTopicId() {
        return this.topicId;
    }

    public void setTopicId(long topicId2) {
        this.topicId = topicId2;
    }
}
