package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.MoreApplicationRestfulApiRequester;
import com.mobcent.forum.android.model.MoreApplicationModel;
import com.mobcent.forum.android.service.MoreApplicationService;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.service.impl.helper.MoreApplicationServiceImplHelper;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class MoreApplicationServiceImpl implements MoreApplicationService {
    public List<MoreApplicationModel> getMoreApplicationList(Context context, long appSize) {
        String jsonStr = MoreApplicationRestfulApiRequester.getMoreApplications(context, appSize);
        List<MoreApplicationModel> moreApplicationList = MoreApplicationServiceImplHelper.parseMoreApplicationListJson(jsonStr, context);
        if (moreApplicationList == null) {
            moreApplicationList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                MoreApplicationModel moreForumModel = new MoreApplicationModel();
                moreForumModel.setErrorCode(errorCode);
                moreApplicationList.add(moreForumModel);
            }
        }
        return moreApplicationList;
    }
}
