package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.forum.android.constant.UserManageConstant;
import com.mobcent.forum.android.model.UserBoardInfoModel;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class UserManageServiceImplHelper extends BaseJsonHelper implements UserManageConstant {
    public static List<UserInfoModel> getUserInfoList(String jsonStr) {
        List<UserInfoModel> userInfoList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String baseUrl = jsonObject.optString("icon_url");
            try {
                JSONArray jsonArray = jsonObject.optJSONArray(UserManageConstant.SHIELDED_LIST);
                int j = jsonArray.length();
                for (int i = 0; i < j; i++) {
                    JSONObject jsonModel = jsonArray.optJSONObject(i);
                    UserInfoModel userInfoModel = new UserInfoModel();
                    userInfoModel.setIcon(String.valueOf(baseUrl) + jsonModel.optString("icon"));
                    userInfoModel.setNickname(jsonModel.getString("nickname"));
                    userInfoModel.setBannedShieldedDate(jsonModel.optString(UserManageConstant.SHIELDED_DATE));
                    userInfoModel.setReasonStr(jsonModel.getString(UserManageConstant.SHIELDED_REASON));
                    userInfoModel.setBannedShieldedStartDate(jsonModel.getString("start_date"));
                    userInfoModel.setUserId(jsonModel.getLong("user_id"));
                    List<UserBoardInfoModel> userBoardInfoList = new ArrayList<>();
                    UserBoardInfoModel userBoardInfoModel = new UserBoardInfoModel();
                    userBoardInfoModel.setBoardId(jsonModel.getLong("board_id"));
                    userBoardInfoModel.setBoardName(jsonModel.getString("board_name"));
                    userBoardInfoList.add(userBoardInfoModel);
                    userInfoModel.setUserBoardInfoList(userBoardInfoList);
                    userInfoList.add(userInfoModel);
                }
                if (userInfoList.size() > 0) {
                    UserInfoModel firstUserInfoModel = (UserInfoModel) userInfoList.get(0);
                    firstUserInfoModel.setBaseUrl(baseUrl);
                    firstUserInfoModel.setHasNext(jsonObject.optInt("has_next"));
                }
            } catch (Exception e) {
                userInfoList = null;
            }
            return userInfoList;
        } catch (Exception e2) {
            Exception exc = e2;
            return null;
        }
    }
}
