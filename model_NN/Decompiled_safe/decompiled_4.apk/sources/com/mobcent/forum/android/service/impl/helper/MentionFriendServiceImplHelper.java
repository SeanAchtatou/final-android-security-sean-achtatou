package com.mobcent.forum.android.service.impl.helper;

import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.forum.android.constant.BaseRestfulApiConstant;
import com.mobcent.forum.android.constant.MentionFriendConstant;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class MentionFriendServiceImplHelper extends BaseJsonHelper implements MentionFriendConstant {
    /* JADX INFO: Multiple debug info for r3v1 int: [D('j' int), D('jsonObject' org.json.JSONObject)] */
    public static List<UserInfoModel> parseMentionFriendListJson(String jsonStr) {
        Exception e;
        List<UserInfoModel> userList = new ArrayList<>();
        boolean isAddAdmin = false;
        boolean isAddFriend = false;
        try {
            JSONArray jsonArray = new JSONObject(jsonStr).optJSONArray("list");
            int j = jsonArray.length();
            String adminIds = "";
            int i = 0;
            while (i < j) {
                try {
                    JSONObject jsonObj = jsonArray.optJSONObject(i);
                    UserInfoModel userInfoModel = new UserInfoModel();
                    userInfoModel.setNickname(jsonObj.optString("name"));
                    userInfoModel.setRoleNum(jsonObj.optInt("role_num"));
                    userInfoModel.setUserId(jsonObj.optLong("uid"));
                    if (userInfoModel.getRoleNum() >= 8 && !isAddAdmin) {
                        UserInfoModel userInfo = new UserInfoModel();
                        userInfo.setUserId(-1);
                        userInfo.setRoleNum(8);
                        userList.add(userInfo);
                        isAddAdmin = true;
                    }
                    if (userInfoModel.getRoleNum() >= 8) {
                        if (adminIds.contains(new StringBuilder(String.valueOf(userInfoModel.getUserId())).toString())) {
                            userInfoModel.setRoleNum(2);
                        } else {
                            adminIds = String.valueOf(adminIds) + userInfoModel.getUserId() + AdApiConstant.RES_SPLIT_COMMA;
                        }
                    }
                    if (!(userInfoModel.getRoleNum() == 8 || userInfoModel.getRoleNum() == 16 || isAddFriend)) {
                        UserInfoModel userInfo2 = new UserInfoModel();
                        userInfo2.setUserId(-1);
                        userInfo2.setRoleNum(2);
                        userList.add(userInfo2);
                        isAddFriend = true;
                    }
                    userList.add(userInfoModel);
                    i++;
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return null;
                }
            }
            return userList;
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return null;
        }
    }

    public static String getMentionFriendInfoStrWithLastUpdateTime(String jsonStr, long time) {
        try {
            JSONObject jsonObject = new JSONObject(jsonStr);
            jsonObject.put(BaseRestfulApiConstant.LAST_UPDATE_TIME, time);
            return jsonObject.toString();
        } catch (Exception e) {
            return jsonStr;
        }
    }
}
