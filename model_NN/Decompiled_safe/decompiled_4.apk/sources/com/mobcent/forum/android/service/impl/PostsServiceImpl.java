package com.mobcent.forum.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.api.PostsRestfulApiRequester;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.db.HotTopicDBUtil;
import com.mobcent.forum.android.db.MentionFriendDBUtil;
import com.mobcent.forum.android.db.NewTopicDBUtil;
import com.mobcent.forum.android.db.PicTopicDBUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.db.SurroundTopicDBUtil;
import com.mobcent.forum.android.model.AnnoModel;
import com.mobcent.forum.android.model.BaseModel;
import com.mobcent.forum.android.model.PollItemModel;
import com.mobcent.forum.android.model.PostsModel;
import com.mobcent.forum.android.model.PostsNoticeModel;
import com.mobcent.forum.android.model.TopicContentModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.service.impl.helper.MentionFriendServiceImplHelper;
import com.mobcent.forum.android.service.impl.helper.PostsServiceImplHelper;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class PostsServiceImpl implements PostsService {
    private Context context;

    public PostsServiceImpl(Context context2) {
        this.context = context2;
    }

    public List<TopicModel> getTopicList(long boardId, int page, int pageSize) {
        new ArrayList();
        String jsonStr = PostsRestfulApiRequester.getTopicList(this.context, boardId, page, pageSize);
        List<TopicModel> topicList = PostsServiceImplHelper.getTopicList(jsonStr, boardId);
        if (topicList == null || topicList.size() == 0) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topic = new TopicModel();
                topic.setErrorCode(errorCode);
                topicList.add(topic);
            }
        }
        return topicList;
    }

    public List<TopicModel> getHotTopicList(long boardId, int page, int pageSize) {
        new ArrayList();
        String jsonStr = PostsRestfulApiRequester.getHotTopicList(this.context, boardId, page, pageSize);
        List<TopicModel> topicList = PostsServiceImplHelper.getTopicList(jsonStr, boardId);
        if (topicList == null || topicList.size() == 0) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topicModel = new TopicModel();
                topicModel.setErrorCode(errorCode);
                topicList.add(topicModel);
            }
        }
        return topicList;
    }

    public List<TopicModel> getEssenceTopicList(long boardId, int page, int pageSize) {
        new ArrayList();
        String jsonStr = PostsRestfulApiRequester.getEssenceTopicList(this.context, boardId, page, pageSize);
        List<TopicModel> topicList = PostsServiceImplHelper.getTopicList(jsonStr, boardId);
        if (topicList == null || topicList.size() == 0) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topicModel = new TopicModel();
                topicModel.setErrorCode(errorCode);
                topicList.add(topicModel);
            }
        }
        return topicList;
    }

    public List<TopicModel> searchTopicList(long userId, long boardId, String keyword, int page, int pageSize) {
        new ArrayList();
        String jsonStr = PostsRestfulApiRequester.searchTopicList(this.context, userId, boardId, keyword, page, pageSize);
        List<TopicModel> topicList = PostsServiceImplHelper.searchTopicList(jsonStr);
        if (topicList == null || topicList.size() == 0) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topic = new TopicModel();
                topic.setErrorCode(errorCode);
                topicList.add(topic);
            }
        }
        return topicList;
    }

    public List<PostsModel> getPosts(long boardId, long topicId, int page, int pageSize) {
        return parsePosts(PostsRestfulApiRequester.getPosts(this.context, boardId, topicId, page, pageSize), getUserFriendList());
    }

    public List<PostsModel> getUserPosts(long boardId, long topicId, long userId, int page, int pageSize) {
        return parsePosts(PostsRestfulApiRequester.getUserPosts(this.context, boardId, topicId, userId, page, pageSize), getUserFriendList());
    }

    public List<PostsModel> getPostsByDesc(long boardId, long topicId, int page, int pageSize) {
        return parsePosts(PostsRestfulApiRequester.getPostsByDesc(this.context, boardId, topicId, page, pageSize), getUserFriendList());
    }

    private List<UserInfoModel> getUserFriendList() {
        String friendJsonStr;
        long loginUserId = SharedPreferencesDB.getInstance(this.context).getUserId();
        if (loginUserId == 0) {
            return null;
        }
        try {
            friendJsonStr = MentionFriendDBUtil.getInstance(this.context).getMentionFriendJsonString(loginUserId);
        } catch (Exception e) {
            e.printStackTrace();
            friendJsonStr = null;
        }
        if (!StringUtil.isEmpty(friendJsonStr)) {
            return MentionFriendServiceImplHelper.parseMentionFriendListJson(friendJsonStr);
        }
        return null;
    }

    private List<PostsModel> parsePosts(String jsonStr, List<UserInfoModel> friendList) {
        List<PostsModel> postsList;
        if (friendList != null) {
            postsList = PostsServiceImplHelper.getPostsWithFriend(jsonStr, friendList);
        } else {
            postsList = PostsServiceImplHelper.getPosts(jsonStr);
        }
        if (postsList == null) {
            postsList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                PostsModel postsModel = new PostsModel();
                postsModel.setErrorCode(errorCode);
                postsList.add(postsModel);
            }
        }
        return postsList;
    }

    public AnnoModel getAnnoDetail(long boardId, long announceId) {
        String jsonStr = PostsRestfulApiRequester.getAnnoDetail(this.context, boardId, announceId);
        AnnoModel annoModel = PostsServiceImplHelper.getAnnoDetail(jsonStr);
        if (annoModel == null) {
            annoModel = new AnnoModel();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                annoModel.setErrorCode(errorCode);
            }
        }
        return annoModel;
    }

    public String publishTopic(long boardId, String title, String content, boolean pageFrom) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.publishTopic(this.context, boardId, title, content, pageFrom));
    }

    public String publishTopic(long boardId, String title, String content, boolean pageFrom, double longitude, double latitude, String location, int requireLocation, int selectVisibleId) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.publishTopic(this.context, boardId, title, content, pageFrom, longitude, latitude, location, requireLocation, selectVisibleId));
    }

    public String publishAnnounce(long boardId, int isReply, String title, String content, String fromDate, String toDate, int selectVisibleId) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.publishAnnounce(this.context, boardId, isReply, title, content, fromDate, toDate, selectVisibleId));
    }

    public String createPublishTopicJson(String content, String iconPath1, String iconPath2, String iconPath3) {
        return PostsServiceImplHelper.createPublishTopicJson(content, iconPath1, iconPath2, iconPath3);
    }

    public String replyTopic(long boardId, long topicId, String rContent, String rTitle, long toReplyId) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.replyTopic(this.context, boardId, topicId, rContent, rTitle, toReplyId));
    }

    public String replyTopic(long boardId, long topicId, String rContent, String rTitle, long toReplyId, boolean isQuote, long pageFrom) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.replyTopic(this.context, boardId, topicId, rContent, rTitle, toReplyId, isQuote, pageFrom));
    }

    public String replyTopic(long boardId, long topicId, String rContent, String rTitle, long toReplyId, boolean isQuote, long pageFrom, double longitude, double latitude, String location, int requireLocation, int selectVisibleId) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.replyTopic(this.context, boardId, topicId, rContent, rTitle, toReplyId, isQuote, pageFrom, longitude, latitude, location, requireLocation, selectVisibleId));
    }

    public String uploadImage(String uploadFile, String action) {
        String jsonStr = PostsRestfulApiRequester.uploadImage(this.context, uploadFile, action);
        String iconPath = PostsServiceImplHelper.parseUploadImageJson(jsonStr);
        if (iconPath == null) {
            return BaseReturnCodeConstant.ERROR_CODE + BaseJsonHelper.formJsonRS(jsonStr);
        }
        return iconPath;
    }

    public boolean getReplyNotificationFlag() {
        return SharedPreferencesDB.getInstance(this.context).getReplyNotificationFlag(new UserServiceImpl(this.context).getLoginUserId());
    }

    public void setReplyNotificationFlag(boolean isNotifyReply) {
        SharedPreferencesDB.getInstance(this.context).setReplyNotificationFlag(isNotifyReply, new UserServiceImpl(this.context).getLoginUserId());
    }

    public List<PostsNoticeModel> getPostsNoticeList(long userId, int page, int pageSize) {
        String jsonStr = PostsRestfulApiRequester.getPostsNoticeList(this.context, userId, page, pageSize);
        new ArrayList();
        List<PostsNoticeModel> postsNoticeList = PostsServiceImplHelper.getPostsNoticeList(jsonStr);
        if (postsNoticeList == null || postsNoticeList.size() == 0) {
            if (postsNoticeList == null) {
                postsNoticeList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                PostsNoticeModel postsNotice = new PostsNoticeModel();
                postsNotice.setErrorCode(errorCode);
                postsNoticeList.add(postsNotice);
            }
        }
        return postsNoticeList;
    }

    public boolean updateReplyRemindState(long userId, String ids) {
        if (StringUtil.isEmpty(BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.updateReplyRemindState(this.context, userId, ids)))) {
            return true;
        }
        return false;
    }

    public long getLastReplyRemindId() {
        return SharedPreferencesDB.getInstance(this.context).getLastReplyRemindId();
    }

    public void updateLastReplyRemindId(long lastReplyRemindId) {
        SharedPreferencesDB.getInstance(this.context).updateLastReplyRemindId(lastReplyRemindId);
    }

    public List<TopicModel> getUserTopicList(long userId, int page, int pageSize) {
        new ArrayList();
        String jsonStr = PostsRestfulApiRequester.getUserTopicList(this.context, userId, page, pageSize);
        List<TopicModel> topicList = PostsServiceImplHelper.getUserTopicList(jsonStr, 0);
        if (topicList == null || topicList.isEmpty()) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topic = new TopicModel();
                topic.setErrorCode(errorCode);
                topicList.add(topic);
            }
        }
        return topicList;
    }

    public List<TopicModel> getUserReplyList(long userId, int page, int pageSize) {
        new ArrayList();
        String jsonStr = PostsRestfulApiRequester.getUserReplyList(this.context, userId, page, pageSize);
        List<TopicModel> topicList = PostsServiceImplHelper.getUserTopicList(jsonStr, 0);
        if (topicList == null || topicList.isEmpty()) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topic = new TopicModel();
                topic.setErrorCode(errorCode);
                topicList.add(topic);
            }
        }
        return topicList;
    }

    public String createPublishTopicJson(String content, String splitChar, String tagImg) {
        return PostsServiceImplHelper.createPublishTopicJson(content, splitChar, tagImg, "", 0).toString();
    }

    public String createPublishTopicJson(String content, String splitChar, String tagImg, List<UserInfoModel> mentionedFriends, String audioPath, int audioDuration) {
        return PostsServiceImplHelper.createPublishTopicJson(content, splitChar, tagImg, mentionedFriends, audioPath, audioDuration);
    }

    public String deleteTopic(long boardId, long topicId) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.deleteTopic(this.context, boardId, topicId));
    }

    public String deleteReply(long boardId, long replyPostsId) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.deleteReply(this.context, boardId, replyPostsId));
    }

    public String updateTopic(long boardId, long topicId, String content) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.updateTopic(this.context, boardId, topicId, content));
    }

    public String updateReply(long boardId, long replyPostsId, String content) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.updateReply(this.context, boardId, replyPostsId, content));
    }

    public boolean updateAtReplyRemindState(long userId, String ids) {
        if (StringUtil.isEmpty(BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.updateAtReply(this.context, ids, userId)))) {
            return true;
        }
        return false;
    }

    public List<PostsNoticeModel> getAtPostsNoticeList(int page, int pageSize) {
        String jsonStr = PostsRestfulApiRequester.getAtPostsNoticeList(this.context, page, pageSize);
        new ArrayList();
        List<PostsNoticeModel> postsNoticeList = PostsServiceImplHelper.getAtPostsNoticeList(jsonStr);
        if (postsNoticeList == null || postsNoticeList.size() == 0) {
            if (postsNoticeList == null) {
                postsNoticeList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                PostsNoticeModel postsNotice = new PostsNoticeModel();
                postsNotice.setErrorCode(errorCode);
                postsNoticeList.add(postsNotice);
            }
        }
        return postsNoticeList;
    }

    public List<TopicModel> getPostTimeTopicList(long boardId, int page, int pageSize) {
        new ArrayList();
        String jsonStr = PostsRestfulApiRequester.getPostTimeTopicList(this.context, boardId, page, pageSize);
        List<TopicModel> topicList = PostsServiceImplHelper.getTopicList(jsonStr, boardId);
        if (topicList == null || topicList.size() == 0) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topicModel = new TopicModel();
                topicModel.setErrorCode(errorCode);
                topicList.add(topicModel);
            }
        }
        return topicList;
    }

    public void setMentionFriendNotificationFlag(boolean isNotifyReply) {
        SharedPreferencesDB.getInstance(this.context).setMentionFriendNotificationFlag(isNotifyReply, new UserServiceImpl(this.context).getLoginUserId());
    }

    public boolean getMentionFriendNotificationFlag() {
        return SharedPreferencesDB.getInstance(this.context).getMentionFriendNotificationFlag(new UserServiceImpl(this.context).getLoginUserId());
    }

    public void setMessageVoiceFlag(boolean isNotifyReply) {
        SharedPreferencesDB.getInstance(this.context).setMessageVoiceFlag(isNotifyReply, new UserServiceImpl(this.context).getLoginUserId());
    }

    public boolean getMessageVoiceFlag() {
        return SharedPreferencesDB.getInstance(this.context).getMessageVoiceFlag(new UserServiceImpl(this.context).getLoginUserId());
    }

    public List<TopicModel> getRelatedPosts(long userId, int page, int pageSize) {
        new ArrayList();
        String jsonStr = PostsRestfulApiRequester.getRelatedPosts(this.context, userId, page, pageSize);
        List<TopicModel> topicList = PostsServiceImplHelper.getUserTopicList(jsonStr, 0);
        if (topicList == null || topicList.isEmpty()) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topic = new TopicModel();
                topic.setErrorCode(errorCode);
                topicList.add(topic);
            }
        }
        return topicList;
    }

    public String publishPollTopic(long boardId, String title, String content, int type, int isVisiable, String pollContent, long deadline) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.publishPollTopic(this.context, boardId, title, content, type, isVisiable, deadline, pollContent));
    }

    public String publishPollTopic(long boardId, String title, String content, int type, int isVisiable, String pollContent, long deadline, double longitude, double latitude, String location, int requireLocation, int selectVisibleId) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.publishPollTopic(this.context, boardId, title, content, type, isVisiable, deadline, pollContent, longitude, latitude, location, requireLocation, selectVisibleId));
    }

    public String createPublishPollTopicJson(List<String> pollContent) {
        return PostsServiceImplHelper.createPublishTopicJson(pollContent);
    }

    public List<TopicModel> getUserEssenceTopicList(long userId, int page, int pageSize) {
        new ArrayList();
        String jsonStr = PostsRestfulApiRequester.getUserEssenceTopic(this.context, page, pageSize, userId);
        List<TopicModel> topicList = PostsServiceImplHelper.getUserTopicList(jsonStr, 0);
        if (topicList == null || topicList.isEmpty()) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topic = new TopicModel();
                topic.setErrorCode(errorCode);
                topicList.add(topic);
            }
        }
        return topicList;
    }

    public List<PollItemModel> getUserPolls(long boardId, long topicId, String pollItemId) {
        new ArrayList();
        String jsonStr = PostsRestfulApiRequester.getUserPoll(this.context, boardId, topicId, pollItemId);
        List<PollItemModel> pollList = PostsServiceImplHelper.getUserPoll(jsonStr);
        if (pollList == null || pollList.size() == 0) {
            if (pollList == null) {
                pollList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                PollItemModel poll = new PollItemModel();
                poll.setErrorCode(errorCode);
                pollList.add(poll);
            }
        }
        return pollList;
    }

    public List<TopicContentModel> getTopicContent(long boardId, long topicId) {
        String jsonStr = PostsRestfulApiRequester.getTopicContent(this.context, boardId, topicId, 0);
        List<TopicContentModel> topicContentList = PostsServiceImplHelper.getTopicContent(jsonStr);
        if (topicContentList == null) {
            topicContentList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicContentModel model = new TopicContentModel();
                model.setErrorCode(errorCode);
                topicContentList.add(model);
            }
        }
        return topicContentList;
    }

    public List<TopicContentModel> getTopicContent(long boardId, long topicId, long userId) {
        String jsonStr = PostsRestfulApiRequester.getTopicContent(this.context, boardId, topicId, userId);
        List<TopicContentModel> topicContentList = PostsServiceImplHelper.getTopicContent(jsonStr);
        if (topicContentList == null) {
            topicContentList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicContentModel model = new TopicContentModel();
                model.setErrorCode(errorCode);
                topicContentList.add(model);
            }
        }
        return topicContentList;
    }

    public List<TopicModel> getNetSurroudTopic(double longitude, double latitude, int radius, int page, int pageSize) {
        String json = PostsRestfulApiRequester.getSurroudTopic(this.context, longitude, latitude, radius, page, pageSize);
        List<TopicModel> topicList = PostsServiceImplHelper.getSurroudTopic(json);
        if (topicList == null || topicList.size() == 0) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(json);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topic = new TopicModel();
                topic.setErrorCode(errorCode);
                topicList.add(topic);
            }
        } else {
            SurroundTopicDBUtil.getInstance(this.context).updateSurroundTopicJsonString(json);
        }
        return topicList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public List<TopicModel> getLocalSurroudTopic() {
        String json;
        List<TopicModel> topicList = null;
        try {
            json = SurroundTopicDBUtil.getInstance(this.context).getSurroundTopicJsonString();
        } catch (Exception e) {
            json = null;
        }
        if (json != null) {
            MCLogUtil.i("PostsServiceImpl", "json = " + json);
            topicList = PostsServiceImplHelper.getSurroudTopic(json);
            if (topicList != null && topicList.size() > 0) {
                topicList.get(0).setHasNext(0);
            }
        }
        return topicList;
    }

    public String createRepostTopicJson(TopicModel topicModel, String boardName, long forwardTime, long boardId, String content, String splitChar, String tagImg, List<UserInfoModel> mentionedFriends, String audioPath, int audioDuration) {
        return PostsServiceImplHelper.createRepostTopicJson(topicModel, boardName, forwardTime, boardId, content, splitChar, tagImg, mentionedFriends, audioPath, audioDuration);
    }

    public List<TopicModel> getHotTopicListByNet(long boardId, int page, int pageSize) {
        new ArrayList();
        String jsonStr = PostsRestfulApiRequester.getHotTopicList(this.context, boardId, page, pageSize);
        List<TopicModel> topicList = PostsServiceImplHelper.getTopicList(jsonStr, boardId);
        if (topicList == null || topicList.size() == 0) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topicModel = new TopicModel();
                topicModel.setErrorCode(errorCode);
                topicList.add(topicModel);
            }
        } else {
            HotTopicDBUtil.getInstance(this.context).updateHotTopicJsonString(jsonStr);
        }
        return topicList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    public List<TopicModel> getHotTopicList(long boardId, int page, int pageSize, boolean isLocal) {
        String jsonStr;
        String[] result = null;
        if (!isLocal) {
            return getHotTopicListByNet(boardId, page, pageSize);
        }
        try {
            result = HotTopicDBUtil.getInstance(this.context).getHotTopicJsonString();
            jsonStr = result[0];
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
        }
        if (StringUtil.isEmpty(jsonStr)) {
            return getHotTopicListByNet(boardId, page, pageSize);
        }
        List<TopicModel> hotTopicList = PostsServiceImplHelper.getTopicList(jsonStr, boardId);
        if (hotTopicList != null && hotTopicList.size() > 0) {
            hotTopicList.get(0).setTotalNum(0);
        }
        hotTopicList.get(0).setHasNext(-1);
        hotTopicList.get(0).setLastUpdate(result[1]);
        return hotTopicList;
    }

    public List<TopicModel> getPostTimeTopicListByNet(long boardId, int page, int pageSize) {
        new ArrayList();
        String jsonStr = PostsRestfulApiRequester.getPostTimeTopicList(this.context, boardId, page, pageSize);
        List<TopicModel> topicList = PostsServiceImplHelper.getTopicList(jsonStr, boardId);
        if (topicList == null || topicList.size() == 0) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topicModel = new TopicModel();
                topicModel.setErrorCode(errorCode);
                topicList.add(topicModel);
            }
        } else {
            NewTopicDBUtil.getInstance(this.context).updateNewsTopicJsonString(jsonStr);
        }
        return topicList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    public List<TopicModel> getPostTimeTopicList(long boardId, int page, int pageSize, boolean isLocal) {
        String jsonStr;
        String[] result = null;
        if (!isLocal) {
            return getPostTimeTopicListByNet(boardId, page, pageSize);
        }
        try {
            result = NewTopicDBUtil.getInstance(this.context).getNewsTopicJsonString();
            jsonStr = result[0];
            List<TopicModel> newTopicList = PostsServiceImplHelper.getTopicList(jsonStr, boardId);
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
        }
        if (StringUtil.isEmpty(jsonStr)) {
            return getPostTimeTopicListByNet(boardId, page, pageSize);
        }
        List<TopicModel> newTopicList2 = PostsServiceImplHelper.getTopicList(jsonStr, boardId);
        if (newTopicList2 == null || newTopicList2.size() <= 0) {
            return newTopicList2;
        }
        newTopicList2.get(0).setTotalNum(0);
        newTopicList2.get(0).setHasNext(-1);
        newTopicList2.get(0).setLastUpdate(result[1]);
        return newTopicList2;
    }

    public boolean shareTopic(long boardId, long topicId) {
        try {
            if (new JSONObject(PostsRestfulApiRequester.shareTopic(this.context, boardId, topicId)).optInt("rs") == 1) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public List<TopicModel> getPicTopicListByNet(int page, int pageSize) {
        new ArrayList();
        String jsonStr = PostsRestfulApiRequester.getPicTopicList(this.context, page, pageSize);
        List<TopicModel> topicList = PostsServiceImplHelper.getTopicList(jsonStr, 0);
        if (topicList == null || topicList.size() == 0) {
            if (topicList == null) {
                topicList = new ArrayList<>();
            }
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                TopicModel topic = new TopicModel();
                topic.setErrorCode(errorCode);
                topicList.add(topic);
            }
        } else {
            PicTopicDBUtil.getInstance(this.context).updatePicTopicJsonString(jsonStr);
        }
        return topicList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public List<TopicModel> getPicTopicList(int page, int pageSize, boolean isLocal) {
        String jsonStr;
        String[] result = PicTopicDBUtil.getInstance(this.context).getPicTopicJsonString();
        if (!isLocal) {
            return getPicTopicListByNet(page, pageSize);
        }
        try {
            jsonStr = result[0];
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
        }
        if (StringUtil.isEmpty(jsonStr)) {
            return getPicTopicListByNet(page, pageSize);
        }
        List<TopicModel> picTopicList = PostsServiceImplHelper.getTopicList(jsonStr, 0);
        if (picTopicList == null || picTopicList.size() <= 0) {
            return picTopicList;
        }
        picTopicList.get(0).setHasNext(-1);
        picTopicList.get(0).setLastUpdate(result[1]);
        return picTopicList;
    }

    public String setEssenceTopic(long boardId, long topicId, int status) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.getEssenceTopic(this.context, boardId, topicId, status));
    }

    public String setTopTopic(long boardId, long topicId, int status) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.getTopTopic(this.context, boardId, topicId, status));
    }

    public String setCloseTopic(long boardId, long topicId, int status) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.getCloseTopic(this.context, boardId, topicId, status));
    }

    public String uploadAudio(String audioFile) {
        String jsonStr = PostsRestfulApiRequester.uploadAudio(this.context, audioFile);
        String audioPath = PostsServiceImplHelper.parseUploadAudioJson(jsonStr);
        if (audioPath == null) {
            return BaseReturnCodeConstant.ERROR_CODE + BaseJsonHelper.formJsonRS(jsonStr);
        }
        return audioPath;
    }

    public String PhysicalDelTopic(long topicId) {
        return BaseJsonHelper.formJsonRS(PostsRestfulApiRequester.getPhysicalDelTopic(this.context, topicId));
    }

    public BaseModel getLongPic(String title, String content, String baseUrl) {
        return PostsServiceImplHelper.getLongPicUrl(PostsRestfulApiRequester.getLongPicUrl(this.context, title, content, baseUrl));
    }

    public boolean isContainsPic(String content, String splitChar, String tagImg, String audioPath, int audioDuration) {
        return PostsServiceImplHelper.isContainsPic(content, splitChar, tagImg, audioPath, audioDuration);
    }
}
