package com.mobcent.base.forum.android.util;

import android.graphics.Bitmap;
import com.mobcent.forum.android.service.delegate.DownProgressDelegate;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public interface AsyncImageLoader {
    public static final int defaultMaxWidth = 320;
    public static final float defaultScale = 1.0f;
    public static final ExecutorService noExtMediaThreadPool = Executors.newFixedThreadPool(10);
    public static final ExecutorService originalThreadPool = Executors.newFixedThreadPool(2);
    public static final ExecutorService threadPool = Executors.newCachedThreadPool();

    boolean insertImageToMediaStore(String str, String str2, InsExtMediaCallBack insExtMediaCallBack);

    Bitmap loadBitmap(String str, BitmapCallback bitmapCallback);

    Bitmap loadBitmap(String str, String str2, float f, BitmapCallback bitmapCallback, boolean z);

    Bitmap loadBitmap(String str, String str2, int i, BitmapCallback bitmapCallback, boolean z);

    Bitmap loadBitmap(String str, String str2, BitmapCallback bitmapCallback);

    Bitmap loadBitmap(String str, String str2, BitmapCallback bitmapCallback, boolean z);

    Bitmap loadBitmap(String str, String str2, boolean z, boolean z2, float f, int i, BitmapCallback bitmapCallback, boolean z3);

    Bitmap loadBitmap(String str, String str2, boolean z, boolean z2, BitmapCallback bitmapCallback);

    Bitmap loadBitmap(String str, String str2, boolean z, boolean z2, BitmapCallback bitmapCallback, boolean z3);

    Bitmap loadBitmapProBar(String str, String str2, boolean z, boolean z2, float f, int i, BitmapCallback bitmapCallback, DownProgressDelegate downProgressDelegate, boolean z3);

    Bitmap loadBitmapProBar(String str, String str2, boolean z, boolean z2, BitmapCallback bitmapCallback, DownProgressDelegate downProgressDelegate);

    Bitmap loadBitmapProBar(String str, String str2, boolean z, boolean z2, BitmapCallback bitmapCallback, DownProgressDelegate downProgressDelegate, boolean z3);

    void recycleBitmap(List<String> list);

    void recycleBitmaps();
}
