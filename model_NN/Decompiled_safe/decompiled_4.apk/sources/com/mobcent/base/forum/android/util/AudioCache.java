package com.mobcent.base.forum.android.util;

import android.content.Context;
import android.util.Log;
import com.mobcent.forum.android.service.impl.FileTransferServiceImpl;
import com.mobcent.forum.android.util.MCLibIOUtil;
import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AudioCache {
    private static AudioCache audioCache;
    /* access modifiers changed from: private */
    public static String audioPath = null;
    public static ExecutorService originalThreadPool = Executors.newFixedThreadPool(8);
    private Context context;

    public interface AudioDownCallback {
        void onAudioLoaded(String str, String str2);
    }

    private AudioCache(Context context2) {
        this.context = context2;
        audioPath = MCLibIOUtil.getAudioCachePath(context2);
    }

    public static synchronized AudioCache getInstance(Context context2) {
        AudioCache audioCache2;
        synchronized (AudioCache.class) {
            if (audioCache == null) {
                audioCache = new AudioCache(context2);
            }
            audioCache2 = audioCache;
        }
        return audioCache2;
    }

    public static String getPath(String url) {
        return audioPath + getHash(url);
    }

    public static String getHash(String url) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(url.getBytes());
            return new BigInteger(digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            return url;
        }
    }

    /* access modifiers changed from: private */
    public boolean loadSync(String url, String fileName, String tempFileName) {
        String dirPath;
        Exception ex;
        File f = null;
        if (audioPath == null) {
            dirPath = this.context.getCacheDir().getAbsolutePath();
        } else {
            dirPath = audioPath;
        }
        try {
            File f2 = new File(dirPath + fileName);
            try {
                if (f2.exists()) {
                    return true;
                }
                if (f2.exists()) {
                    return false;
                }
                File tempFile = new File(dirPath + tempFileName);
                if (tempFile.exists()) {
                    tempFile.delete();
                }
                new FileTransferServiceImpl(this.context).downloadFile(url, tempFile);
                tempFile.renameTo(f2);
                return true;
            } catch (Exception e) {
                ex = e;
                f = f2;
            }
        } catch (Exception e2) {
            ex = e2;
            f.delete();
            Log.e(getClass().getName(), ex.getMessage(), ex);
            return false;
        }
    }

    public void loadAsync(String url, AudioDownCallback callback) {
        if (url != null && !url.equals("")) {
            final String hash = getHash(url);
            final String tempFileName = MCStringBundleUtil.getPathName(url);
            final String str = url;
            final AudioDownCallback audioDownCallback = callback;
            originalThreadPool.execute(new Thread(new Runnable() {
                public void run() {
                    if (AudioCache.this.loadSync(str, hash, tempFileName)) {
                        audioDownCallback.onAudioLoaded(str, AudioCache.audioPath + hash);
                    }
                }
            }, "AudioCache loader: " + url));
        }
    }

    public String getAudioPath() {
        return audioPath;
    }
}
