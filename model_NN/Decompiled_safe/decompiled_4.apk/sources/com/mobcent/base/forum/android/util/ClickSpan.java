package com.mobcent.base.forum.android.util;

import android.view.View;

public class ClickSpan {
    private View.OnClickListener clickListener;
    private String colorName;
    private int endPostion;
    private int startPostion;

    public int getStartPostion() {
        return this.startPostion;
    }

    public void setStartPostion(int startPostion2) {
        this.startPostion = startPostion2;
    }

    public int getEndPostion() {
        return this.endPostion;
    }

    public void setEndPostion(int endPostion2) {
        this.endPostion = endPostion2;
    }

    public String getColorName() {
        return this.colorName;
    }

    public void setColorName(String colorName2) {
        this.colorName = colorName2;
    }

    public View.OnClickListener getClickListener() {
        return this.clickListener;
    }

    public void setClickListener(View.OnClickListener clickListener2) {
        this.clickListener = clickListener2;
    }
}
