package com.mobcent.base.forum.android.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public class MCFaceUtil {
    private static LinkedHashMap<String, Integer> allHashMap = new LinkedHashMap<>();
    private static LinkedHashMap<String, Integer> allMHashMap = new LinkedHashMap<>();
    private static LinkedHashMap<String, Integer> allQQHashMap = new LinkedHashMap<>();
    private static MCFaceUtil mcFaceUtil;
    private static MCResource resource;

    private MCFaceUtil(Context context) {
        resource = MCResource.getInstance(context);
        allHashMap.put("[泪]", Integer.valueOf(resource.getDrawableId("mc_forum_face198")));
        allHashMap.put("[哈哈]", Integer.valueOf(resource.getDrawableId("mc_forum_face234")));
        allHashMap.put("[抓狂]", Integer.valueOf(resource.getDrawableId("mc_forum_face239")));
        allHashMap.put("[嘻嘻]", Integer.valueOf(resource.getDrawableId("mc_forum_face233")));
        allHashMap.put("[偷笑]", Integer.valueOf(resource.getDrawableId("mc_forum_face247")));
        allHashMap.put("[鼓掌]", Integer.valueOf(resource.getDrawableId("mc_forum_face255")));
        allHashMap.put("[怒]", Integer.valueOf(resource.getDrawableId("mc_forum_face242")));
        allHashMap.put("[心]", Integer.valueOf(resource.getDrawableId("mc_forum_face279")));
        allHashMap.put("[心碎了]", Integer.valueOf(resource.getDrawableId("mc_forum_face280")));
        allHashMap.put("[生病]", Integer.valueOf(resource.getDrawableId("mc_forum_face258")));
        allHashMap.put("[爱你]", Integer.valueOf(resource.getDrawableId("mc_forum_face17")));
        allHashMap.put("[害羞]", Integer.valueOf(resource.getDrawableId("mc_forum_face201")));
        allHashMap.put("[可怜]", Integer.valueOf(resource.getDrawableId("mc_forum_face268")));
        allHashMap.put("[馋嘴]", Integer.valueOf(resource.getDrawableId("mc_forum_face238")));
        allHashMap.put("[晕]", Integer.valueOf(resource.getDrawableId("mc_forum_face7")));
        allHashMap.put("[花心]", Integer.valueOf(resource.getDrawableId("mc_forum_face254")));
        allHashMap.put("[太开心]", Integer.valueOf(resource.getDrawableId("mc_forum_face261")));
        allHashMap.put("[亲亲]", Integer.valueOf(resource.getDrawableId("mc_forum_face259")));
        allHashMap.put("[鄙视]", Integer.valueOf(resource.getDrawableId("mc_forum_face252")));
        allHashMap.put("[呵呵]", Integer.valueOf(resource.getDrawableId("mc_forum_face25")));
        allHashMap.put("[挖鼻屎]", Integer.valueOf(resource.getDrawableId("mc_forum_face253")));
        allHashMap.put("[衰]", Integer.valueOf(resource.getDrawableId("mc_forum_face6")));
        allHashMap.put("[兔子]", Integer.valueOf(resource.getDrawableId("mc_forum_rabbit_thumb")));
        allHashMap.put("[good]", Integer.valueOf(resource.getDrawableId("mc_forum_face100")));
        allHashMap.put("[来]", Integer.valueOf(resource.getDrawableId("mc_forum_face277")));
        allHashMap.put("[威武]", Integer.valueOf(resource.getDrawableId("mc_forum_face219")));
        allHashMap.put("[围观]", Integer.valueOf(resource.getDrawableId("mc_forum_face218")));
        allHashMap.put("[萌]", Integer.valueOf(resource.getDrawableId("mc_forum_kawayi_thumb")));
        allHashMap.put("[送花]", Integer.valueOf(resource.getDrawableId("mc_forum_face120")));
        allHashMap.put("[囧]", Integer.valueOf(resource.getDrawableId("mc_forum_face121")));
        allQQHashMap.put("[酷]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_01")));
        allQQHashMap.put("[糗大了]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_02")));
        allQQHashMap.put("[撇嘴]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_03")));
        allQQHashMap.put("[发呆]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_04")));
        allQQHashMap.put("[汗]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_05")));
        allQQHashMap.put("[睡]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_06")));
        allQQHashMap.put("[吃惊]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_11")));
        allQQHashMap.put("[白眼]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_12")));
        allQQHashMap.put("[疑问]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_13")));
        allQQHashMap.put("[阴险]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_14")));
        allQQHashMap.put("[左哼哼]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_15")));
        allQQHashMap.put("[右哼哼]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_16")));
        allQQHashMap.put("[敲到]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_21")));
        allQQHashMap.put("[委屈]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_22")));
        allQQHashMap.put("[嘘]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_23")));
        allQQHashMap.put("[吐]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_24")));
        allQQHashMap.put("[做鬼脸]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_25")));
        allQQHashMap.put("[ByeBye]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_26")));
        allQQHashMap.put("[快哭了]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_31")));
        allQQHashMap.put("[傲慢]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_32")));
        allQQHashMap.put("[月亮]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_33")));
        allQQHashMap.put("[太阳]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_34")));
        allQQHashMap.put("[耶]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_35")));
        allQQHashMap.put("[握手]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_36")));
        allQQHashMap.put("[OK]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_41")));
        allQQHashMap.put("[咖啡]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_42")));
        allQQHashMap.put("[饭]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_43")));
        allQQHashMap.put("[礼物]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_44")));
        allQQHashMap.put("[猪头]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_45")));
        allQQHashMap.put("[拥抱]", Integer.valueOf(resource.getDrawableId("mc_forum_qq_46")));
        allMHashMap.put("[赞]", Integer.valueOf(resource.getDrawableId("mc_forum_m_01")));
        allMHashMap.put("[Hold]", Integer.valueOf(resource.getDrawableId("mc_forum_m_02")));
        allMHashMap.put("[神马]", Integer.valueOf(resource.getDrawableId("mc_forum_m_03")));
        allMHashMap.put("[坑爹]", Integer.valueOf(resource.getDrawableId("mc_forum_m_04")));
        allMHashMap.put("[有木有?]", Integer.valueOf(resource.getDrawableId("mc_forum_m_05")));
        allMHashMap.put("[谢谢]", Integer.valueOf(resource.getDrawableId("mc_forum_m_06")));
        allMHashMap.put("[魔鬼]", Integer.valueOf(resource.getDrawableId("mc_forum_m_11")));
        allMHashMap.put("[外星人]", Integer.valueOf(resource.getDrawableId("mc_forum_m_12")));
        allMHashMap.put("[蓝心]", Integer.valueOf(resource.getDrawableId("mc_forum_m_13")));
        allMHashMap.put("[紫心]", Integer.valueOf(resource.getDrawableId("mc_forum_m_14")));
        allMHashMap.put("[黄心]", Integer.valueOf(resource.getDrawableId("mc_forum_m_15")));
        allMHashMap.put("[绿心]", Integer.valueOf(resource.getDrawableId("mc_forum_m_16")));
        allMHashMap.put("[音乐]", Integer.valueOf(resource.getDrawableId("mc_forum_m_21")));
        allMHashMap.put("[闪烁]", Integer.valueOf(resource.getDrawableId("mc_forum_m_22")));
        allMHashMap.put("[星星]", Integer.valueOf(resource.getDrawableId("mc_forum_m_23")));
        allMHashMap.put("[水滴]", Integer.valueOf(resource.getDrawableId("mc_forum_m_24")));
        allMHashMap.put("[火焰]", Integer.valueOf(resource.getDrawableId("mc_forum_m_25")));
        allMHashMap.put("[便便]", Integer.valueOf(resource.getDrawableId("mc_forum_m_26")));
        allMHashMap.put("[踩一脚]", Integer.valueOf(resource.getDrawableId("mc_forum_m_31")));
        allMHashMap.put("[下雨]", Integer.valueOf(resource.getDrawableId("mc_forum_m_32")));
        allMHashMap.put("[多云]", Integer.valueOf(resource.getDrawableId("mc_forum_m_33")));
        allMHashMap.put("[闪电]", Integer.valueOf(resource.getDrawableId("mc_forum_m_34")));
        allMHashMap.put("[雪花]", Integer.valueOf(resource.getDrawableId("mc_forum_m_35")));
        allMHashMap.put("[旋风]", Integer.valueOf(resource.getDrawableId("mc_forum_m_36")));
        allMHashMap.put("[包]", Integer.valueOf(resource.getDrawableId("mc_forum_m_41")));
        allMHashMap.put("[房子]", Integer.valueOf(resource.getDrawableId("mc_forum_m_42")));
        allMHashMap.put("[烟花]", Integer.valueOf(resource.getDrawableId("mc_forum_m_43")));
    }

    public static MCFaceUtil getFaceConstant(Context context) {
        if (mcFaceUtil == null) {
            mcFaceUtil = new MCFaceUtil(context);
        }
        return mcFaceUtil;
    }

    public LinkedHashMap<String, Integer> getAllFaceMap() {
        LinkedHashMap<String, Integer> map = new LinkedHashMap<>();
        map.putAll(allHashMap);
        map.putAll(allQQHashMap);
        map.putAll(allMHashMap);
        return map;
    }

    /* JADX INFO: Multiple debug info for r4v1 int: [D('startLeft' int), D('i' int)] */
    /* JADX INFO: Multiple debug info for r0v6 java.util.Set<java.lang.String>: [D('charText' char), D('faceNameSet' java.util.Set<java.lang.String>)] */
    /* JADX INFO: Multiple debug info for r3v5 java.lang.String: [D('isHavaLeftFace' boolean), D('faceStr' java.lang.String)] */
    public static void setStrToFace(TextView tv, String text, Context context) {
        LinkedHashMap<String, Integer> faceMap = getFaceConstant(context).getAllFaceMap();
        SpannableString sp = new SpannableString(text);
        tv.setText(sp);
        boolean isHavaLeftFace = false;
        int endRight = -1;
        int endRight2 = 0;
        int startLeft = -1;
        while (true) {
            int i = endRight2;
            int endRight3 = endRight;
            if (i < text.length()) {
                char charText = text.charAt(i);
                if (charText == '[') {
                    startLeft = i;
                    isHavaLeftFace = true;
                }
                if (isHavaLeftFace && charText == ']') {
                    endRight3 = i;
                    Set<String> faceNameSet = faceMap.keySet();
                    String faceStr = text.substring(startLeft, endRight3 + 1);
                    if (faceNameSet.contains(faceStr)) {
                        Drawable d = context.getResources().getDrawable(faceMap.get(faceStr).intValue());
                        d.setBounds(0, 0, (int) context.getResources().getDimension(resource.getDimenId("mc_forum_face_width")), (int) context.getResources().getDimension(resource.getDimenId("mc_forum_face_height")));
                        sp.setSpan(new ImageSpan(d, 1), startLeft, endRight3 + 1, 17);
                    }
                    isHavaLeftFace = false;
                }
                endRight = endRight3;
                endRight2 = i + 1;
            } else {
                tv.setText(sp);
                return;
            }
        }
    }

    /* JADX INFO: Multiple debug info for r4v1 int: [D('startLeft' int), D('i' int)] */
    /* JADX INFO: Multiple debug info for r0v7 java.util.Set<java.lang.String>: [D('charText' char), D('faceNameSet' java.util.Set<java.lang.String>)] */
    /* JADX INFO: Multiple debug info for r3v8 java.lang.String: [D('isHavaLeftFace' boolean), D('faceStr' java.lang.String)] */
    public static void setStrToFaceWithSpan(TextView tv, String text, Context context, List<ClickSpan> clickSpanList) {
        LinkedHashMap<String, Integer> faceMap = getFaceConstant(context).getAllFaceMap();
        SpannableString sp = new SpannableString(text);
        tv.setText(sp);
        boolean isHavaLeftFace = false;
        int endRight = -1;
        int endRight2 = 0;
        int startLeft = -1;
        while (true) {
            int i = endRight2;
            int endRight3 = endRight;
            if (i >= text.length()) {
                break;
            }
            char charText = text.charAt(i);
            if (charText == '[') {
                startLeft = i;
                isHavaLeftFace = true;
            }
            if (isHavaLeftFace && charText == ']') {
                endRight3 = i;
                Set<String> faceNameSet = faceMap.keySet();
                String faceStr = text.substring(startLeft, endRight3 + 1);
                if (faceNameSet.contains(faceStr)) {
                    Drawable d = context.getResources().getDrawable(faceMap.get(faceStr).intValue());
                    d.setBounds(0, 0, (int) context.getResources().getDimension(resource.getDimenId("mc_forum_face_width")), (int) context.getResources().getDimension(resource.getDimenId("mc_forum_face_height")));
                    sp.setSpan(new ImageSpan(d, 1), startLeft, endRight3 + 1, 17);
                }
                isHavaLeftFace = false;
            }
            endRight = endRight3;
            endRight2 = i + 1;
        }
        int s = clickSpanList.size();
        for (int i2 = 0; i2 < s; i2++) {
            ClickSpan clickSpan = clickSpanList.get(i2);
            if (clickSpan.getClickListener() != null) {
                sp.setSpan(new Clickable(clickSpan.getClickListener()), clickSpan.getStartPostion(), clickSpan.getEndPostion(), 33);
            }
            sp.setSpan(new ForegroundColorSpan(resource.getColor(clickSpan.getColorName())), clickSpan.getStartPostion(), clickSpan.getEndPostion(), 33);
        }
        tv.setText(sp);
    }

    static class Clickable extends ClickableSpan implements View.OnClickListener {
        private final View.OnClickListener mListener;

        public Clickable(View.OnClickListener l) {
            this.mListener = l;
        }

        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }

        public void onClick(View v) {
            this.mListener.onClick(v);
        }
    }

    public List<LinkedHashMap> getFaceList() {
        List<LinkedHashMap> list = new ArrayList<>();
        list.add(allHashMap);
        list.add(allQQHashMap);
        list.add(allMHashMap);
        return list;
    }
}
