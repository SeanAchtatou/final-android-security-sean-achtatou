package com.mobcent.base.forum.android.util;

import com.mobcent.forum.android.util.DateUtil;
import java.util.LinkedList;
import java.util.Queue;

public class MCForumSearchUtil {
    private static final long SEARCH_TIME_INTERVAL_MAX = 120000;
    public static String TAG = "MCForumSearchUtil";
    private static boolean isSearchEnable = true;
    private static Queue<Long> queue = new LinkedList();

    public static boolean isSearchEnable() {
        long currentTime = DateUtil.getCurrentTime();
        if (queue.size() < 10) {
            isSearchEnable = true;
        } else if (queue.size() >= 10) {
            if (currentTime - queue.peek().longValue() < SEARCH_TIME_INTERVAL_MAX) {
                isSearchEnable = false;
            } else {
                isSearchEnable = true;
            }
        }
        return isSearchEnable;
    }

    public static void monitorSearchAction() {
        long currentTime = DateUtil.getCurrentTime();
        if (queue.size() < 10) {
            queue.offer(Long.valueOf(currentTime));
        } else if (queue.size() >= 10) {
            queue.poll();
            queue.offer(Long.valueOf(currentTime));
        }
    }
}
