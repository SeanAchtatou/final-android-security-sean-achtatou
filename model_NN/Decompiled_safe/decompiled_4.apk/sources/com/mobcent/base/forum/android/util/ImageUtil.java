package com.mobcent.base.forum.android.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import com.mobcent.forum.android.util.ImageLoaderUtil;
import com.mobcent.forum.android.util.MCLogUtil;

public class ImageUtil {
    public static final int DEPENDS_ON_HEIGHT = 4;
    public static final int DEPENDS_ON_LONGEST_EDGE = 2;
    public static final int DEPENDS_ON_SHORTEST_EDGE = 1;
    public static final int DEPENDS_ON_WIDTH = 3;
    private static final int defaultMaxWidth = 640;
    private static final float defaultScale = 0.8f;
    private static final int defaultShowWidth = 200;

    public static boolean downloadGifImage(String url, String size, Context context) {
        return ImageLoaderUtil.downloadGif(url, size, context);
    }

    public static Bitmap getBitmapFromMedia(Context context, float scale, String pathName) {
        return getBitmapFromMedia(context, pathName, scale, (int) defaultMaxWidth);
    }

    public static Bitmap getBitmapFromMedia(Context context, String pathName) {
        return getBitmapFromMedia(context, pathName, (float) defaultScale, (int) defaultMaxWidth);
    }

    public static Bitmap compressBitmap(Bitmap bitmap, int policy, Context context) {
        return compressBitmap(bitmap, defaultShowWidth, policy, context);
    }

    public static Bitmap compressBitmap(Bitmap bitmap, int upperLimitSize, int policy, Context context) {
        try {
            Bitmap resizeBitmap = resizeBitmap(bitmap, upperLimitSize, policy);
            if (resizeBitmap == bitmap) {
                return bitmap;
            }
            bitmap.recycle();
            return resizeBitmap;
        } catch (OutOfMemoryError e) {
            System.gc();
            if (null == bitmap) {
                return bitmap;
            }
            bitmap.recycle();
            return null;
        } catch (Exception e2) {
            if (null == bitmap) {
                return bitmap;
            }
            bitmap.recycle();
            return null;
        } catch (Throwable th) {
            if (null != bitmap) {
                bitmap.recycle();
            }
            throw th;
        }
    }

    public static boolean compressBitmap(String compressPath, Bitmap bitmap, int quality, int policy, Context context) {
        return compressBitmap(compressPath, bitmap, (int) defaultMaxWidth, quality, policy, context);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002e, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean compressBitmap(java.lang.String r5, android.graphics.Bitmap r6, int r7, int r8, int r9, android.content.Context r10) {
        /*
            r2 = 0
            android.graphics.Bitmap r2 = resizeBitmap(r6, r7, r9)     // Catch:{ OutOfMemoryError -> 0x0020, Exception -> 0x0030 }
            java.io.File r3 = new java.io.File     // Catch:{ OutOfMemoryError -> 0x0020, Exception -> 0x0030 }
            r3.<init>(r5)     // Catch:{ OutOfMemoryError -> 0x0020, Exception -> 0x0030 }
            r1 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ OutOfMemoryError -> 0x0020, Exception -> 0x0030 }
            r1.<init>(r3)     // Catch:{ OutOfMemoryError -> 0x0020, Exception -> 0x0030 }
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ OutOfMemoryError -> 0x0020, Exception -> 0x0030 }
            boolean r4 = r2.compress(r4, r8, r1)     // Catch:{ OutOfMemoryError -> 0x0020, Exception -> 0x0030 }
            if (r2 == r6) goto L_0x001f
            r2.recycle()
            r2 = 0
            java.lang.System.gc()
        L_0x001f:
            return r4
        L_0x0020:
            r4 = move-exception
            r0 = r4
            java.lang.System.gc()     // Catch:{ all -> 0x003b }
            if (r2 == r6) goto L_0x002e
            r2.recycle()
            r2 = 0
            java.lang.System.gc()
        L_0x002e:
            r4 = 0
            goto L_0x001f
        L_0x0030:
            r4 = move-exception
            if (r2 == r6) goto L_0x002e
            r2.recycle()
            r2 = 0
            java.lang.System.gc()
            goto L_0x002e
        L_0x003b:
            r4 = move-exception
            if (r2 == r6) goto L_0x0045
            r2.recycle()
            r2 = 0
            java.lang.System.gc()
        L_0x0045:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.base.forum.android.util.ImageUtil.compressBitmap(java.lang.String, android.graphics.Bitmap, int, int, int, android.content.Context):boolean");
    }

    public static Bitmap resizeBitmap(Bitmap bitmap, int upperLimitSize, int policy) throws Exception {
        if (policy == 4) {
            return resizeAccordHeightEdgeBitmap(bitmap, upperLimitSize);
        }
        if (policy == 3) {
            return resizeAccordWidthEdgeBitmap(bitmap, upperLimitSize);
        }
        if (policy == 1) {
            return resizeAccordShortestEdgeBitmap(bitmap, upperLimitSize);
        }
        if (policy == 2) {
            return resizeAccordLongestEdgeBitmap(bitmap, upperLimitSize);
        }
        return resizeAccordWidthEdgeBitmap(bitmap, upperLimitSize);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap resizeAccordWidthEdgeBitmap(Bitmap bitmap, int upperLimitSize) throws Exception {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width <= upperLimitSize && height <= upperLimitSize) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        matrix.postScale(((float) upperLimitSize) / ((float) width), ((float) ((int) (((float) upperLimitSize) * (((float) height) / ((float) width))))) / ((float) height));
        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap resizeAccordHeightEdgeBitmap(Bitmap bitmap, int upperLimitSize) throws Exception {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width <= upperLimitSize && height <= upperLimitSize) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        matrix.postScale(((float) ((int) (((float) upperLimitSize) * (((float) width) / ((float) height))))) / ((float) width), ((float) upperLimitSize) / ((float) height));
        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap resizeAccordShortestEdgeBitmap(Bitmap bitmap, int upperLimitSize) throws Exception {
        float scaleWidth;
        float scaleHeight;
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width <= height) {
            scaleWidth = ((float) upperLimitSize) / ((float) width);
            scaleHeight = ((float) ((int) (((float) upperLimitSize) * (((float) height) / ((float) width))))) / ((float) height);
        } else {
            scaleWidth = ((float) ((int) (((float) upperLimitSize) * (((float) width) / ((float) height))))) / ((float) width);
            scaleHeight = ((float) upperLimitSize) / ((float) height);
        }
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap resizeAccordLongestEdgeBitmap(Bitmap bitmap, int upperLimitSize) throws Exception {
        float scaleWidth;
        float scaleHeight;
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width <= upperLimitSize && height <= upperLimitSize) {
            return bitmap;
        }
        if (width >= height) {
            scaleWidth = ((float) upperLimitSize) / ((float) width);
            scaleHeight = ((float) ((int) (((float) upperLimitSize) * (((float) height) / ((float) width))))) / ((float) height);
        } else {
            scaleWidth = ((float) ((int) (((float) upperLimitSize) * (((float) width) / ((float) height))))) / ((float) width);
            scaleHeight = ((float) upperLimitSize) / ((float) height);
        }
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap rotateBitmap(Bitmap bitmap, float degrees) {
        if (bitmap == null) {
            return null;
        }
        if (bitmap.isRecycled()) {
            return null;
        }
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        if (resizedBitmap != bitmap) {
            bitmap.recycle();
            bitmap = resizedBitmap;
        }
        return bitmap;
    }

    /* JADX INFO: Multiple debug info for r1v3 int: [D('outputHeight' int), D('maxW' int)] */
    /* JADX INFO: Multiple debug info for r6v1 int: [D('scale' float), D('targetWidth' int)] */
    /* JADX INFO: Multiple debug info for r6v4 int: [D('targetWidth' int), D('inSampleSize' int)] */
    /* JADX INFO: Multiple debug info for r5v3 android.graphics.Bitmap: [D('bitmap' android.graphics.Bitmap), D('pathName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v7 int: [D('outputHeight' int), D('maxW' int)] */
    public static Bitmap compressBitmap(Context context, String pathName, float scale, int maxWidth, boolean isMatrix) {
        int outputWidth;
        int inSampleSize;
        Bitmap bitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName, options);
        options.inJustDecodeBounds = false;
        int outputHeight = options.outHeight;
        int outputWidth2 = options.outWidth;
        if (outputHeight > outputWidth2) {
            outputWidth = outputHeight;
        } else {
            outputWidth = outputWidth2;
        }
        if (scale == 1.0f) {
            inSampleSize = 1;
        } else {
            inSampleSize = (int) (((float) outputWidth) / ((float) getMinWidthOfBitmap(context, scale, maxWidth)));
            if (inSampleSize <= 0) {
                inSampleSize = 1;
            }
        }
        options.inSampleSize = inSampleSize;
        try {
            Bitmap bitmap2 = BitmapFactory.decodeFile(pathName, options);
            if (!isMatrix || bitmap2 == null) {
                bitmap = bitmap2;
            } else {
                bitmap = showMatrixBitmap(context, bitmap2);
            }
            return bitmap;
        } catch (OutOfMemoryError e) {
            System.gc();
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static Bitmap getBitmapFromMedia(Context context, String pathName, float scale, int maxWidth) {
        Bitmap bitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(pathName, options);
            options.inJustDecodeBounds = false;
            int outputWidth = options.outWidth;
            int i = options.outHeight;
            MCLogUtil.i("ImageUtil", "outputWidth = " + outputWidth);
            if (outputWidth < maxWidth) {
                bitmap = BitmapFactory.decodeFile(pathName);
            } else {
                options.inSampleSize = outputWidth / maxWidth;
                bitmap = BitmapFactory.decodeFile(pathName, options);
            }
            return bitmap;
        } catch (OutOfMemoryError e) {
            System.gc();
            return null;
        } catch (Exception e2) {
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r0v3 int: [D('inSampleSize' int), D('widthSmapleSize' int)] */
    /* JADX INFO: Multiple debug info for r8v1 int: [D('maxWidth' int), D('heightSmapleSize' int)] */
    public static Bitmap getBitmapFromMedia(Context context, String pathName, int maxWidth, int maxHeight) {
        int heightSmapleSize;
        Bitmap bitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(pathName, options);
            options.inJustDecodeBounds = false;
            int outputWidth = options.outWidth;
            int outputHeight = options.outHeight;
            if (outputWidth >= maxWidth || outputHeight >= maxHeight) {
                int widthSmapleSize = outputWidth / maxWidth;
                int heightSmapleSize2 = outputHeight / maxHeight;
                MCLogUtil.i("ImageUtil", "outputWidth = " + outputWidth + " widthSmapleSize = " + widthSmapleSize);
                MCLogUtil.i("ImageUtil", "outputHeight = " + outputHeight + " heightSmapleSize = " + heightSmapleSize2);
                if (widthSmapleSize >= heightSmapleSize2) {
                    heightSmapleSize = widthSmapleSize;
                } else {
                    heightSmapleSize = heightSmapleSize2;
                }
                options.inSampleSize = heightSmapleSize;
                bitmap = BitmapFactory.decodeFile(pathName, options);
            } else {
                bitmap = BitmapFactory.decodeFile(pathName);
            }
            return bitmap;
        } catch (OutOfMemoryError e) {
            System.gc();
            return null;
        } catch (Exception e2) {
            return null;
        }
    }

    private static int getInSampleSizeByScreen(Context context, float scale, int outputWidth, int outputHeight) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float screenWidth = ((float) displayMetrics.widthPixels) * scale;
        float screenHeight = ((float) displayMetrics.heightPixels) * scale;
        int widthSmapleSize = (int) (((float) outputWidth) / screenWidth);
        int heightSmapleSize = (int) (((float) outputHeight) / screenHeight);
        MCLogUtil.i("ImageUtil", "outputWidth = " + outputWidth + " screenWidth = " + screenWidth + " widthSmapleSize = " + widthSmapleSize);
        MCLogUtil.i("ImageUtil", "outputHeight = " + outputHeight + " screenHeight = " + screenHeight + " heightSmapleSize = " + heightSmapleSize);
        if (widthSmapleSize >= heightSmapleSize) {
            return widthSmapleSize;
        }
        return heightSmapleSize;
    }

    /* JADX INFO: Multiple debug info for r1v4 int: [D('outputHeight' int), D('maxW' int)] */
    /* JADX INFO: Multiple debug info for r7v1 int: [D('maxWidth' int), D('targetWidth' int)] */
    /* JADX INFO: Multiple debug info for r7v4 int: [D('targetWidth' int), D('inSampleSize' int)] */
    /* JADX INFO: Multiple debug info for r6v3 android.graphics.Bitmap: [D('bitmap' android.graphics.Bitmap), D('length' int)] */
    /* JADX INFO: Multiple debug info for r1v8 int: [D('outputHeight' int), D('maxW' int)] */
    public static Bitmap compressBitmap(Context context, byte[] imgData, int length, int maxWidth, float scale, boolean isMatrix) {
        int outputWidth;
        int maxWidth2;
        Bitmap bitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(imgData, 0, length, options);
        options.inJustDecodeBounds = false;
        int outputHeight = options.outHeight;
        int outputWidth2 = options.outWidth;
        if (outputHeight > outputWidth2) {
            outputWidth = outputHeight;
        } else {
            outputWidth = outputWidth2;
        }
        if (scale == 1.0f) {
            maxWidth2 = 1;
        } else {
            maxWidth2 = (int) (((float) outputWidth) / ((float) getMinWidthOfBitmap(context, scale, maxWidth)));
            if (maxWidth2 <= 0) {
                maxWidth2 = 1;
            } else if (maxWidth2 >= 10) {
                maxWidth2 = 10;
            }
        }
        options.inSampleSize = maxWidth2;
        try {
            Bitmap bitmap2 = BitmapFactory.decodeByteArray(imgData, 0, length, options);
            if (!isMatrix || bitmap2 == null) {
                bitmap = bitmap2;
            } else {
                bitmap = showMatrixBitmap(context, bitmap2);
            }
            return bitmap;
        } catch (OutOfMemoryError e) {
            System.gc();
            return null;
        } catch (Exception e2) {
            return null;
        } catch (Throwable th) {
            throw th;
        }
    }

    private static int getMinWidthOfBitmap(Context context, float scale, int maxWidth) {
        int minWidth;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int width = (int) (((float) displayMetrics.widthPixels) * scale);
        int height = (int) (((float) displayMetrics.heightPixels) * scale);
        if (height > width) {
            minWidth = width;
        } else {
            minWidth = height;
        }
        if (minWidth > maxWidth) {
            return maxWidth;
        }
        return minWidth;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private static Bitmap showMatrixBitmap(Context context, Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int screenWidth = displayMetrics.widthPixels;
        int screenHeight = displayMetrics.heightPixels;
        float widthPoint = ((float) width) / ((float) screenWidth);
        float heightPoint = ((float) height) / ((float) screenHeight);
        Matrix matrix = new Matrix();
        if (widthPoint > 1.0f || heightPoint > 1.0f) {
            if (widthPoint > 1.0f && heightPoint < 1.0f) {
                float scale = ((float) screenWidth) / ((float) width);
                matrix.postScale(scale, scale);
            } else if (widthPoint < 1.0f && heightPoint > 1.0f) {
                float scale2 = ((float) screenHeight) / ((float) width);
                matrix.postScale(scale2, scale2);
            } else if (widthPoint > 1.0f && heightPoint > 1.0f) {
                if (widthPoint >= heightPoint) {
                    float scale3 = ((float) screenWidth) / ((float) width);
                    matrix.postScale(scale3, scale3);
                } else {
                    float scale4 = ((float) screenHeight) / ((float) height);
                    matrix.postScale(scale4, scale4);
                }
            }
        } else if (widthPoint >= heightPoint) {
            float scale5 = ((float) screenWidth) / ((float) width);
            matrix.postScale(scale5, scale5);
        } else if (widthPoint < heightPoint) {
            float scale6 = ((float) screenHeight) / ((float) height);
            matrix.postScale(scale6, scale6);
        }
        Bitmap bitmapNew = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        if (bitmapNew != bitmap) {
            bitmap.recycle();
            bitmap = bitmapNew;
        }
        return bitmap;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static int getBitmapByteCount(Bitmap bitmap) {
        if (bitmap == null || bitmap.isRecycled()) {
            return 0;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap.Config config = bitmap.getConfig();
        if (config == null) {
            return 0;
        }
        int sizePerPixel = 0;
        if (config.equals(Bitmap.Config.ALPHA_8)) {
            sizePerPixel = 1;
        } else if (config.equals(Bitmap.Config.ARGB_4444)) {
            sizePerPixel = 2;
        } else if (config.equals(Bitmap.Config.ARGB_8888)) {
            sizePerPixel = 4;
        } else if (config.equals(Bitmap.Config.RGB_565)) {
            sizePerPixel = 2;
        }
        return width * height * sizePerPixel;
    }

    public static Bitmap trimBitmap(Bitmap bitmap, int x, int y, int width, int height) {
        if (bitmap == null) {
            return null;
        }
        if (bitmap.isRecycled()) {
            return null;
        }
        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, x, y, width, height);
        if (resizedBitmap != bitmap) {
            bitmap.recycle();
            bitmap = resizedBitmap;
        }
        return bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Drawable zoomDrawable(Drawable drawable, float zoomScale) {
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        Bitmap oldbmp = drawableToBitmap(drawable);
        Matrix matrix = new Matrix();
        matrix.postScale(zoomScale, zoomScale);
        Bitmap newbmp = Bitmap.createBitmap(oldbmp, 0, 0, width, height, matrix, true);
        if (oldbmp != null && !oldbmp.isRecycled()) {
            oldbmp.recycle();
        }
        return new BitmapDrawable(newbmp);
    }
}
