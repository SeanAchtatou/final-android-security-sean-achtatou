package com.mobcent.base.forum.android.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.mobcent.forum.android.service.impl.FileTransferServiceImpl;
import com.mobcent.forum.android.util.MCLibIOUtil;
import java.io.File;
import java.lang.ref.WeakReference;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

public class ImageCache {
    private static final long CACHE_TIMEOUT = 86400000;
    private static ImageCache _instance = null;
    private HashMap<String, WeakReference<Drawable>> _cache = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, List<ImageCallback>> _callbacks = new HashMap<>();
    /* access modifiers changed from: private */
    public final Object _lock = new Object();
    private Context context;
    private String imagePath = null;

    public interface ImageCallback {
        void onImageLoaded(Drawable drawable, String str);
    }

    public static synchronized ImageCache getInstance(Context context2) {
        ImageCache imageCache;
        synchronized (ImageCache.class) {
            if (_instance == null) {
                _instance = new ImageCache(context2);
            }
            imageCache = _instance;
        }
        return imageCache;
    }

    private ImageCache(Context context2) {
        this.context = context2;
        this.imagePath = createFileDir();
    }

    public static String getHash(String url) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(url.getBytes());
            return new BigInteger(digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            return url;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: android.graphics.drawable.Drawable} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.drawable.Drawable drawableFromCache(java.lang.String r6, java.lang.String r7) {
        /*
            r5 = this;
            r1 = 0
            java.lang.Object r4 = r5._lock
            monitor-enter(r4)
            java.util.HashMap<java.lang.String, java.lang.ref.WeakReference<android.graphics.drawable.Drawable>> r3 = r5._cache     // Catch:{ all -> 0x0027 }
            boolean r3 = r3.containsKey(r7)     // Catch:{ all -> 0x0027 }
            if (r3 == 0) goto L_0x0025
            java.util.HashMap<java.lang.String, java.lang.ref.WeakReference<android.graphics.drawable.Drawable>> r3 = r5._cache     // Catch:{ all -> 0x0027 }
            java.lang.Object r2 = r3.get(r7)     // Catch:{ all -> 0x0027 }
            java.lang.ref.WeakReference r2 = (java.lang.ref.WeakReference) r2     // Catch:{ all -> 0x0027 }
            if (r2 == 0) goto L_0x0025
            java.lang.Object r3 = r2.get()     // Catch:{ all -> 0x0027 }
            r0 = r3
            android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0     // Catch:{ all -> 0x0027 }
            r1 = r0
            if (r1 != 0) goto L_0x0025
            java.util.HashMap<java.lang.String, java.lang.ref.WeakReference<android.graphics.drawable.Drawable>> r3 = r5._cache     // Catch:{ all -> 0x0027 }
            r3.remove(r7)     // Catch:{ all -> 0x0027 }
        L_0x0025:
            monitor-exit(r4)     // Catch:{ all -> 0x0027 }
            return r1
        L_0x0027:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0027 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.base.forum.android.util.ImageCache.drawableFromCache(java.lang.String, java.lang.String):android.graphics.drawable.Drawable");
    }

    /* access modifiers changed from: private */
    public Drawable loadSync(String url, String hash) {
        File f;
        Drawable d = null;
        try {
            d = drawableFromCache(url, hash);
            if (this.imagePath == null) {
                f = new File(this.context.getCacheDir(), hash);
            } else {
                f = new File(this.imagePath + hash);
            }
            if (d == null) {
                if (!f.exists()) {
                    new FileTransferServiceImpl(this.context).downloadFile(url, f);
                }
                if (f.exists()) {
                    d = Drawable.createFromPath(f.getAbsolutePath());
                }
                synchronized (this._lock) {
                    this._cache.put(hash, new WeakReference(d));
                }
            }
        } catch (Exception e) {
            Exception ex = e;
            Log.e(getClass().getName(), ex.getMessage(), ex);
        }
        return d;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadAsync(final java.lang.String r7, com.mobcent.base.forum.android.util.ImageCache.ImageCallback r8) {
        /*
            r6 = this;
            if (r7 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.lang.String r2 = ""
            boolean r2 = r7.equals(r2)
            if (r2 != 0) goto L_0x0002
            java.lang.String r1 = getHash(r7)
            java.lang.Object r2 = r6._lock
            monitor-enter(r2)
            java.util.HashMap<java.lang.String, java.util.List<com.mobcent.base.forum.android.util.ImageCache$ImageCallback>> r3 = r6._callbacks     // Catch:{ all -> 0x0023 }
            java.lang.Object r0 = r3.get(r1)     // Catch:{ all -> 0x0023 }
            java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x0023 }
            if (r0 == 0) goto L_0x0026
            if (r8 == 0) goto L_0x0021
            r0.add(r8)     // Catch:{ all -> 0x0023 }
        L_0x0021:
            monitor-exit(r2)     // Catch:{ all -> 0x0023 }
            goto L_0x0002
        L_0x0023:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0023 }
            throw r3
        L_0x0026:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0023 }
            r0.<init>()     // Catch:{ all -> 0x0023 }
            if (r8 == 0) goto L_0x0030
            r0.add(r8)     // Catch:{ all -> 0x0023 }
        L_0x0030:
            java.util.HashMap<java.lang.String, java.util.List<com.mobcent.base.forum.android.util.ImageCache$ImageCallback>> r3 = r6._callbacks     // Catch:{ all -> 0x0023 }
            r3.put(r1, r0)     // Catch:{ all -> 0x0023 }
            monitor-exit(r2)     // Catch:{ all -> 0x0023 }
            java.lang.Thread r2 = new java.lang.Thread
            com.mobcent.base.forum.android.util.ImageCache$1 r3 = new com.mobcent.base.forum.android.util.ImageCache$1
            r3.<init>(r7, r1)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "ImageCache loader: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.String r4 = r4.toString()
            r2.<init>(r3, r4)
            r2.start()
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.base.forum.android.util.ImageCache.loadAsync(java.lang.String, com.mobcent.base.forum.android.util.ImageCache$ImageCallback):void");
    }

    public static String formatUrl(String url, String resolution) {
        if (url == null || url.length() == 0) {
            return "";
        }
        if (resolution == null || resolution.length() == 0) {
            return url;
        }
        return url.replace("xgsize", resolution);
    }

    public static String formatAdUrl(String url, String resolution) {
        if (url == null) {
            return "";
        }
        if (resolution == null) {
            return url;
        }
        if (!url.contains(".")) {
            return "";
        }
        return url.concat("." + resolution + url.substring(url.lastIndexOf(".")));
    }

    private String createFileDir() {
        String imagePath2 = MCLibIOUtil.getImageCachePath(this.context);
        if (MCLibIOUtil.isDirExist(imagePath2) || MCLibIOUtil.makeDirs(imagePath2)) {
            return imagePath2;
        }
        return null;
    }
}
