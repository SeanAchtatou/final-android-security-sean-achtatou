package com.mobcent.base.forum.android.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import com.appbyme.android.api.constant.AutogenConfigApiConstant;
import com.mobcent.forum.android.application.McForumApplication;

public class MCResource {
    private static MCResource mcResource;
    private String pkg;
    private Resources resources;

    private MCResource(Context context) {
        this.pkg = context.getPackageName();
        this.resources = context.getResources();
    }

    public static MCResource getInstance(Context context) {
        Context appContext;
        if (mcResource == null) {
            McForumApplication app = McForumApplication.getInstance();
            if (app == null) {
                appContext = context.getApplicationContext();
            } else {
                appContext = app.getApplicationContext();
            }
            mcResource = new MCResource(appContext);
        }
        return mcResource;
    }

    /* access modifiers changed from: protected */
    public int getResourcesId(String type, String name) {
        try {
            return this.resources.getIdentifier(name, type, this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getStringId(String name) {
        try {
            return this.resources.getIdentifier(name, "string", this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String getString(String name) {
        try {
            return this.resources.getString(getStringId(name));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public int getColorId(String name) {
        try {
            return this.resources.getIdentifier(name, "color", this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getColor(String name) {
        try {
            return this.resources.getColor(getColorId(name));
        } catch (Exception e) {
            return 0;
        }
    }

    public int getDimenId(String name) {
        try {
            return this.resources.getIdentifier(name, "dimen", this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getStyleId(String name) {
        try {
            return this.resources.getIdentifier(name, AutogenConfigApiConstant.STYLE, this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getLayoutId(String name) {
        try {
            return this.resources.getIdentifier(name, "layout", this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getViewId(String name) {
        try {
            return this.resources.getIdentifier(name, "id", this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getAnimId(String name) {
        try {
            return this.resources.getIdentifier(name, "anim", this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getArrayId(String name) {
        try {
            return this.resources.getIdentifier(name, "array", this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getDrawableId(String name) {
        try {
            return this.resources.getIdentifier(name, "drawable", this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Drawable getDrawable(String name) {
        try {
            return this.resources.getDrawable(getDrawableId(name));
        } catch (Exception e) {
            return null;
        }
    }

    public int getRawId(String name) {
        try {
            return this.resources.getIdentifier(name, "raw", this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
