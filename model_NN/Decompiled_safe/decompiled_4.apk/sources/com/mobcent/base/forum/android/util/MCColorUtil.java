package com.mobcent.base.forum.android.util;

import android.content.Context;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

public class MCColorUtil {
    public static void setTextViewPart(Context context, TextView textView, String s, int startPostion, int endPostion, String colorName) {
        textView.setText(s, TextView.BufferType.SPANNABLE);
        if (endPostion > startPostion) {
            ((Spannable) textView.getText()).setSpan(new ForegroundColorSpan(MCResource.getInstance(context).getColor(colorName)), startPostion, endPostion, 33);
        }
    }

    public static void setTextViewPart(TextView textView, String s, int startPostion, int endPostion, int color) {
        textView.setText(s, TextView.BufferType.SPANNABLE);
        if (endPostion > startPostion) {
            ((Spannable) textView.getText()).setSpan(new ForegroundColorSpan(color), startPostion, endPostion, 33);
        }
    }
}
