package com.mobcent.base.forum.android.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class MCForumReverseList {
    public static List reverseList(List result) {
        List tResult = new ArrayList();
        Stack stack = new Stack();
        for (int i = 0; i < result.size(); i++) {
            stack.push(result.get(i));
        }
        while (!stack.empty()) {
            tResult.add(stack.pop());
        }
        return tResult;
    }

    public static String[] convertListToArray(List<String> list) {
        String[] s = new String[list.size()];
        int j = list.size();
        for (int i = 0; i < j; i++) {
            s[i] = list.get(i);
        }
        return s;
    }
}
