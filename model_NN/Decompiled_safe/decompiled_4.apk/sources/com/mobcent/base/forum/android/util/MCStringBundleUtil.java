package com.mobcent.base.forum.android.util;

import android.content.Context;
import com.mobcent.forum.android.util.DateUtil;
import com.tencent.mm.sdk.platformtools.Util;
import java.util.Calendar;

public class MCStringBundleUtil {
    public static String resolveString(int stringSource, String[] args, Context context) {
        String string = context.getResources().getString(stringSource);
        for (int i = 0; i < args.length; i++) {
            string = string.replace("{" + i + "}", args[i]);
        }
        return string;
    }

    public static String resolveString(int stringSource, String arg, Context context) {
        return context.getResources().getString(stringSource).replace("{0}", arg);
    }

    public static String timeToString(Context context, long time) {
        MCResource forumResource = MCResource.getInstance(context);
        if (Calendar.getInstance().getTimeInMillis() - time <= Util.MILLSECONDS_OF_MINUTE) {
            return context.getResources().getString(forumResource.getStringId("mc_forum_just_now"));
        }
        if (time > 0) {
            return DateUtil.getFormatTime(time);
        }
        return "";
    }

    public static String getPathName(String url) {
        try {
            if (url.contains("?") && url.contains("/")) {
                return url.substring(url.lastIndexOf("/") + 1, url.indexOf("?"));
            }
            if (url.contains("/")) {
                return url.substring(url.lastIndexOf("/") + 1);
            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }
}
