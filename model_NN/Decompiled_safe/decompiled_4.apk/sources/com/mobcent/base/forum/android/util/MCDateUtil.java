package com.mobcent.base.forum.android.util;

import android.content.Context;
import com.tencent.mm.sdk.platformtools.Util;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class MCDateUtil {
    private static Map<String, SimpleDateFormat> formatMap = new HashMap();

    public static void initDateFormat() {
        try {
            SimpleDateFormat formater = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
            formater.setTimeZone(TimeZone.getTimeZone("GMT"));
            getDateFormat("EEE MMM dd HH:mm:ss z yyyy").parse(formater.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /* JADX INFO: Multiple debug info for r10v2 java.util.Date: [D('time' java.lang.String), D('createdAt' java.util.Date)] */
    /* JADX INFO: Multiple debug info for r10v7 java.lang.String: [D('createdAt' java.util.Date), D('time' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r10v16 java.lang.String: [D('createdAt' java.util.Date), D('time' java.lang.String)] */
    public static String convertTime(Context context, long lastReplyDate, MCResource mcResource) {
        Date createdAt = new Date(lastReplyDate);
        long l = new Date().getTime() - createdAt.getTime();
        long day = l / 86400000;
        long hour = (l / Util.MILLSECONDS_OF_HOUR) - (24 * day);
        long min = ((l / Util.MILLSECONDS_OF_MINUTE) - ((24 * day) * 60)) - (60 * hour);
        long s = (((l / 1000) - (((24 * day) * 60) * 60)) - ((60 * hour) * 60)) - (60 * min);
        if (day > 0) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(createdAt);
        }
        if (hour > 0) {
            return hour + mcResource.getString("mc_forum_hour_before");
        }
        if (min > 0) {
            return min + mcResource.getString("mc_forum_minute_before");
        }
        if (s < 0) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(createdAt);
        }
        return s + mcResource.getString("mc_forum_mill_before");
    }

    private static SimpleDateFormat getDateFormat(String format) {
        SimpleDateFormat sdf = formatMap.get(format);
        if (sdf != null) {
            return sdf;
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat(format, Locale.ENGLISH);
        sdf2.setTimeZone(TimeZone.getTimeZone("GMT"));
        formatMap.put(format, sdf2);
        return sdf2;
    }

    private static Date parseDate(String str, String format) throws ParseException {
        Date parse;
        if (str == null || "".equals(str)) {
            return null;
        }
        SimpleDateFormat sdf = formatMap.get(format);
        if (sdf == null) {
            sdf = new SimpleDateFormat(format, Locale.ENGLISH);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            formatMap.put(format, sdf);
        }
        try {
            synchronized (sdf) {
                parse = sdf.parse(str);
            }
            return parse;
        } catch (ParseException e) {
            throw e;
        }
    }

    public static String getFormatTimeByYear(long time) {
        if (Calendar.getInstance().get(1) == new Integer(new SimpleDateFormat("yyyy").format(Long.valueOf(time))).intValue()) {
            return new SimpleDateFormat("MM-dd HH:mm").format(Long.valueOf(time));
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(Long.valueOf(time));
    }
}
