package com.mobcent.base.forum.android.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import com.mobcent.forum.android.application.McForumApplication;

public class MCThemeResource {
    private static MCThemeResource mcThemeResource;
    private String pkg;
    private Resources resources;

    private MCThemeResource(Context context) {
        this.pkg = context.getPackageName();
        this.resources = context.getResources();
    }

    public static MCThemeResource getInstance(Context context) {
        Context appContext;
        if (mcThemeResource == null && mcThemeResource == null) {
            McForumApplication app = McForumApplication.getInstance();
            if (app == null) {
                appContext = context.getApplicationContext();
            } else {
                appContext = app.getApplicationContext();
            }
            mcThemeResource = new MCThemeResource(appContext);
        }
        return mcThemeResource;
    }

    public static MCThemeResource getInstance(Context context, String packageName) {
        Context context2;
        try {
            context2 = context.createPackageContext(packageName, 2);
        } catch (Exception e) {
            context2 = context;
        }
        if (mcThemeResource == null || !mcThemeResource.pkg.equals(packageName)) {
            mcThemeResource = new MCThemeResource(context2.getApplicationContext());
        }
        return mcThemeResource;
    }

    /* access modifiers changed from: protected */
    public int getResourcesId(String type, String name) {
        try {
            return this.resources.getIdentifier(name, type, this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getColorId(String name) {
        try {
            return this.resources.getIdentifier(name, "color", this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getColor(String name) {
        try {
            return this.resources.getColor(getColorId(name));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public int getDrawableId(String name) {
        try {
            return this.resources.getIdentifier(name, "drawable", this.pkg);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Drawable getDrawable(String name) {
        try {
            return this.resources.getDrawable(getDrawableId(name));
        } catch (Exception e) {
            return null;
        }
    }
}
