package com.mobcent.base.forum.android.util;

import android.graphics.Bitmap;

public interface BitmapCallback {
    void imageLoaded(Bitmap bitmap, String str);
}
