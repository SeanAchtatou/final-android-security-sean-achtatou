package com.mobcent.base.forum.android.util;

import android.content.Context;
import java.util.LinkedHashMap;
import java.util.Map;

public class MCWeatherUtil {
    private static MCResource mcResource;
    private static MCWeatherUtil mcWeatherUtil = null;
    private Map<Integer, Integer> weatherMap = new LinkedHashMap();

    public MCWeatherUtil(Context context) {
        mcResource = MCResource.getInstance(context);
        this.weatherMap.put(0, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon1")));
        this.weatherMap.put(1, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon2")));
        this.weatherMap.put(2, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon3")));
        this.weatherMap.put(3, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon4")));
        this.weatherMap.put(4, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon5")));
        this.weatherMap.put(5, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon6")));
        this.weatherMap.put(6, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon7")));
        this.weatherMap.put(7, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon8")));
        this.weatherMap.put(8, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon9")));
        this.weatherMap.put(9, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon10")));
        this.weatherMap.put(10, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon11")));
        this.weatherMap.put(11, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon12")));
        this.weatherMap.put(12, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon13")));
        this.weatherMap.put(13, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon32")));
        this.weatherMap.put(14, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon14")));
        this.weatherMap.put(15, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon15")));
        this.weatherMap.put(16, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon16")));
        this.weatherMap.put(17, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon17")));
        this.weatherMap.put(18, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon18")));
        this.weatherMap.put(19, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon19")));
        this.weatherMap.put(20, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon30")));
        this.weatherMap.put(21, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon20")));
        this.weatherMap.put(22, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon21")));
        this.weatherMap.put(23, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon22")));
        this.weatherMap.put(24, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon23")));
        this.weatherMap.put(25, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon24")));
        this.weatherMap.put(26, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon25")));
        this.weatherMap.put(27, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon26")));
        this.weatherMap.put(28, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon27")));
        this.weatherMap.put(29, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon28")));
        this.weatherMap.put(30, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon29")));
        this.weatherMap.put(31, Integer.valueOf(mcResource.getDrawableId("mc_forum_weather_icon31")));
    }

    public static MCWeatherUtil getInstance(Context context) {
        if (mcWeatherUtil == null) {
            mcWeatherUtil = new MCWeatherUtil(context);
        }
        return mcWeatherUtil;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public int getWeatherIcon(int weatherIcon) {
        if (this.weatherMap.containsKey(Integer.valueOf(weatherIcon))) {
            return this.weatherMap.get(Integer.valueOf(weatherIcon)).intValue();
        }
        return 0;
    }
}
