package com.mobcent.base.forum.android.util;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import com.baidu.location.LocationClientOption;
import com.mobcent.forum.android.util.MCLogUtil;
import java.io.IOException;

public class AudioRecordUtil {
    public static MediaRecorder startRecording(String fileName) {
        MediaRecorder mRecorder = new MediaRecorder();
        if (Build.VERSION.SDK_INT >= 8) {
            mRecorder.setAudioSamplingRate(8000);
        }
        mRecorder.setAudioSource(1);
        mRecorder.setOutputFormat(3);
        mRecorder.setOutputFile(fileName);
        mRecorder.setAudioEncoder(1);
        mRecorder.setMaxDuration(60000);
        try {
            mRecorder.prepare();
            try {
                mRecorder.start();
            } catch (Exception e) {
                mRecorder.reset();
                mRecorder.release();
                mRecorder = null;
            }
            return mRecorder;
        } catch (IOException e2) {
            e2.printStackTrace();
            mRecorder.reset();
            mRecorder.release();
            return null;
        }
    }

    public static void stopRecording(MediaRecorder mRecorder) {
        MCLogUtil.e("AudioRecordUtil", "AudioRecordUtil stopRecording 1");
        if (mRecorder != null) {
            try {
                mRecorder.stop();
                mRecorder.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        MCLogUtil.e("AudioRecordUtil", "AudioRecordUtil stopRecording 2");
    }

    public static int getDuration(Context context, String fileName) {
        try {
            MediaPlayer mPlayer = MediaPlayer.create(context, Uri.parse(fileName));
            if (mPlayer != null) {
                mPlayer.stop();
            }
            int duration = mPlayer.getDuration() / LocationClientOption.MIN_SCAN_SPAN;
            Log.e("", fileName + "BaseRecordActivity " + duration);
            return duration;
        } catch (Exception e) {
            return 0;
        }
    }

    public static MediaPlayer startPlaying(String fileName, int startPosition) {
        MediaPlayer mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(fileName);
            mPlayer.prepare();
            mPlayer.seekTo(startPosition);
            mPlayer.start();
            return mPlayer;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int stopPlaying(MediaPlayer mPlayer) {
        int currentPosition = mPlayer.getCurrentPosition();
        mPlayer.release();
        return currentPosition;
    }
}
