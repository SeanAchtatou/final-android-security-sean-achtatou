package com.mobcent.base.forum.android.util;

import android.content.Context;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.util.StringUtil;

public class MCForumErrorUtil implements BaseReturnCodeConstant {
    public static String convertErrorCode(Context context, String errorCode) {
        MCResource forumResource = MCResource.getInstance(context);
        if (errorCode.equals("connection_fail")) {
            return forumResource.getString("mc_forum_connection_fail");
        }
        if (errorCode.equals("upload_images_fail")) {
            return forumResource.getString("mc_forum_uplode_imgae_fail");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_OTHER_WRONG)) {
            return forumResource.getString("mc_forum_other_error");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_JSON_WRONG)) {
            return forumResource.getString("mc_forum_json_error");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_NO_PERMISSION_TAG)) {
            return forumResource.getString("mc_forum_no_permission_tag");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_NO_PERMISSION)) {
            return forumResource.getString("mc_forum_no_permission");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PWD_NO_SAME)) {
            return forumResource.getString("mc_forum_pwd_no_same");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_TOKEN_INVALID)) {
            return forumResource.getString("mc_forum_token_invalid");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_USER_NOT_EXIST)) {
            return forumResource.getString("mc_forum_user_not_exist");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_NEW_USER_NOT_EXIST)) {
            return forumResource.getString("mc_forum_new_user_not_exist");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_USER_PWD_ERROR)) {
            return forumResource.getString("mc_forum_user_pwd_error");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_USER_NICK_REPEAT)) {
            return forumResource.getString("mc_forum_user_nick_repeat");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_EAMIL_EXIST)) {
            return forumResource.getString("mc_forum_email_exist");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_REPEAT_FAVORIT)) {
            return forumResource.getString("mc_forum_repeat_favorit");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_KEY_INVALID)) {
            return forumResource.getString("mc_forum_key_invalid");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_SDK_TYPE_VERSION_TOO_LOW)) {
            return forumResource.getString("mc_forum_sdk_type_version_too_low");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_UNKNOW_ERROR)) {
            return forumResource.getString("mc_forum_unknow_error");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_SUBJECT_NO_EXIST)) {
            return forumResource.getString("mc_forum_subject_no_exist");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_DELETE_TOPIC_FAIL)) {
            return forumResource.getString("mc_forum_delete_topic_fail");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_DELETE_REPLY_FAIL)) {
            return forumResource.getString("mc_forum_delete_reply_fail");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_UPDATE_TOPIC_FAIL)) {
            return forumResource.getString("mc_forum_update_topic_fail");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_UPDATE_REPLY_FAIL)) {
            return forumResource.getString("mc_forum_update_reply_fail");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_SEARCH_KEYWORD_EMPTY)) {
            return forumResource.getString("mc_forum_no_keywords");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_BLACK_USER_SEND_MESSAGE)) {
            return forumResource.getString("mc_forum_error_black_user_send_msg");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_POLL_POSTS)) {
            return forumResource.getString("mc_forum_error_poll_posts");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_POLL_POSTS_DEADTIME)) {
            return forumResource.getString("mc_forum_error_poll_posts_deadtime");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_USER_NOT_LOGIN)) {
            return forumResource.getString("mc_forum_not_login_warn");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_USER_ALREADY_REPORT)) {
            return forumResource.getString("mc_forum_already_report_warn");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_REQUEST_USER_BANNED)) {
            return forumResource.getString("mc_forum_error_user_banned");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_REQUEST_USER_SHIELED)) {
            return forumResource.getString("mc_forum_error_user_shielded");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_REPORT_POSTS_SAFETY)) {
            return forumResource.getString("mc_forum_report_post_safety_warn");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_MAINTAIN)) {
            return forumResource.getString("mc_forum_error_maintain");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PUBLISH_SENSITIVE_WORD)) {
            return forumResource.getString("mc_forum_error_publish_sensitive_word");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_OPERATION_FAILURE)) {
            return forumResource.getString("mc_forum_error_operation_failure");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_REQUEST_PARAMETER)) {
            return forumResource.getString("mc_forum_error_requeset_parameter");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_REQUEST_TOPIC_CLOSE)) {
            return forumResource.getString("mc_forum_error_topic_close");
        }
        if (errorCode.equals(BaseReturnCodeConstant.EEROR_REQUEST_TO_ONESELF)) {
            return forumResource.getString("mc_forum_to_oneself");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_REQUEST_ANNO_MAX)) {
            return forumResource.getString("mc_forum_error_anno_max");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_USER_PREMISSION_LOWER)) {
            return forumResource.getString("mc_forum_error_permission_lower");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_USER_ACCOUNTS_CLOSE)) {
            return forumResource.getString("mc_forum_error_user_accounts_close");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_USER_ACCOUNTS_DELETE)) {
            return forumResource.getString("mc_forum_error_user_accounts_delete");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_GAIN_PICTURE_FAIL)) {
            return forumResource.getString("mc_forum_error_gain_picture_fail");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PERMISSION_VISIT)) {
            return forumResource.getString("mc_forum_error_permission_visit");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PERMISSION_BOARD_VISIT)) {
            return forumResource.getString("mc_forum_error_permission_board_visit");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PERMISSION_READ)) {
            return forumResource.getString("mc_forum_error_permission_read");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PERMISSION_BOARD_READ)) {
            return forumResource.getString("mc_forum_error_permission_board_read");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PERMISSION_POST)) {
            return forumResource.getString("mc_forum_error_permission_post");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PERMISSION_BOARD_POST)) {
            return forumResource.getString("mc_forum_error_permission_board_post");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PERMISSION_REPLY)) {
            return forumResource.getString("mc_forum_error_permission_reply");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PERMISSION_BOARD_REPLY)) {
            return forumResource.getString("mc_forum_error_permission_board_reply");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PERMISSION_UPLOAD)) {
            return forumResource.getString("mc_forum_error_permission_upload");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PERMISSION_BOARD_UPLOAD)) {
            return forumResource.getString("mc_forum_error_permission_board_upload");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PERMISSION_DOWNLOAD)) {
            return forumResource.getString("mc_forum_error_permission_download");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_PERMISSION_BOARD_DOWNLOAD)) {
            return forumResource.getString("mc_forum_error_permission_board_download");
        }
        if (errorCode.equals(BaseReturnCodeConstant.ERROR_IS_BLACK_OF_MANAGER)) {
            String errorMsg = forumResource.getString("mc_forum_error_is_black_of_manager");
            SharedPreferencesDB.getInstance(context).removeUserAccess();
            return errorMsg;
        } else if (errorCode.equals(BaseReturnCodeConstant.ERROR_MODERATOR_CANCEL_SHIELDED)) {
            return forumResource.getString("mc_forum_error_moderator_cancel_shielded");
        } else {
            if (errorCode.equals(BaseReturnCodeConstant.ERROR_MODERATOR_CANCEL_BANNED)) {
                return forumResource.getString("mc_forum_error_moderator_cancel_banned");
            }
            if (!StringUtil.isEmpty(errorCode)) {
                return forumResource.getString("mc_forum_unknow_error");
            }
            return "";
        }
    }
}
