package com.mobcent.base.forum.android.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import com.tencent.mm.sdk.platformtools.Util;

public class MCPhoneUtil {
    public static int getCacheSize(Context context) {
        return (Util.BYTE_OF_MB * ((ActivityManager) context.getSystemService("activity")).getMemoryClass()) / 8;
    }

    public static String getExtensionName(String filename) {
        int dot;
        if (filename == null || filename.length() <= 0 || (dot = filename.lastIndexOf(46)) <= -1 || dot >= filename.length() - 1) {
            return filename;
        }
        return filename.substring(dot + 1);
    }

    public static boolean isStorageState() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        return false;
    }

    @Deprecated
    public static int getStatusBarHeight(Activity activity) {
        Rect rect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        return rect.top;
    }

    public static int getDisplayWidth(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public static int getDisplayHeight(Activity activity) {
        activity.getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
        return activity.getWindowManager().getDefaultDisplay().getHeight();
    }

    public static float getDisplayDensity(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.density;
    }

    public static int getRawSize(Context context, int unit, float size) {
        Resources resources;
        if (context == null) {
            resources = Resources.getSystem();
        } else {
            resources = context.getResources();
        }
        return (int) TypedValue.applyDimension(unit, size, resources.getDisplayMetrics());
    }

    public static int dip2px(Context context, float dpValue) {
        return (int) ((dpValue * context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static int px2dip(Context context, float pxValue) {
        return (int) ((pxValue / context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static String getAppName(Context context) {
        return context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
    }
}
