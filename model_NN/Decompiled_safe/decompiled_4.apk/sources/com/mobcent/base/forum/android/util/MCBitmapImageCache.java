package com.mobcent.base.forum.android.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import com.mobcent.forum.android.service.impl.FileTransferServiceImpl;
import com.mobcent.forum.android.util.MCLibIOUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MCBitmapImageCache {
    private static final long CACHE_TIMEOUT = 86400000;
    private static Map<String, MCBitmapImageCache> _instanceMap = new HashMap();
    public static ExecutorService originalThreadPool = Executors.newFixedThreadPool(8);
    public HashMap<String, Bitmap> _cache = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, List<BitmapImageCallback>> _callbacks = new HashMap<>();
    /* access modifiers changed from: private */
    public final Object _lock = new Object();
    private Context context;
    private String imagePath = null;

    public interface BitmapImageCallback {
        void onImageLoaded(Bitmap bitmap, String str);
    }

    public static synchronized MCBitmapImageCache getInstance(Context context2, String tag) {
        MCBitmapImageCache mCBitmapImageCache;
        synchronized (MCBitmapImageCache.class) {
            if (_instanceMap == null) {
                _instanceMap = new HashMap();
            }
            if (_instanceMap.get(tag) == null) {
                _instanceMap.put(tag, new MCBitmapImageCache(context2));
            }
            mCBitmapImageCache = _instanceMap.get(tag);
        }
        return mCBitmapImageCache;
    }

    public static void clearInstanceMap() {
        if (_instanceMap == null) {
            _instanceMap = new HashMap();
        } else {
            _instanceMap.clear();
        }
    }

    private MCBitmapImageCache(Context context2) {
        this.context = context2;
        this.imagePath = createFileDir();
    }

    public static String getHash(String url) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(url.getBytes());
            return new BigInteger(digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            return url;
        }
    }

    private Bitmap bitmapFromCache(String url, String hash) {
        Bitmap b = null;
        synchronized (this._lock) {
            if (this._cache.containsKey(hash) && (b = this._cache.get(hash)) != null && b.isRecycled()) {
                this._cache.remove(hash);
                b = null;
            }
        }
        return b;
    }

    /* access modifiers changed from: private */
    public Bitmap loadSync(String url, String hash) {
        File f;
        Bitmap b = null;
        try {
            b = bitmapFromCache(url, hash);
            if (this.imagePath == null) {
                f = new File(this.context.getCacheDir(), hash);
            } else {
                f = new File(this.imagePath + hash);
            }
            if (b == null) {
                if (f.exists() && (b = getBitmap(f.getAbsolutePath())) == null) {
                    f.delete();
                }
                if (!f.exists()) {
                    new FileTransferServiceImpl(this.context).downloadFile(url, f);
                }
                if (f.exists() && b == null && (b = getBitmap(f.getAbsolutePath())) == null) {
                    f.delete();
                }
                synchronized (this._lock) {
                    if (b != null) {
                        if (!b.isRecycled()) {
                            this._cache.put(hash, b);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Exception ex = e;
            Log.e(getClass().getName(), ex.getMessage(), ex);
        }
        return b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadAsync(final java.lang.String r7, com.mobcent.base.forum.android.util.MCBitmapImageCache.BitmapImageCallback r8) {
        /*
            r6 = this;
            if (r7 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.lang.String r3 = ""
            boolean r3 = r7.equals(r3)
            if (r3 != 0) goto L_0x0002
            java.lang.String r1 = getHash(r7)
            java.lang.Object r3 = r6._lock
            monitor-enter(r3)
            java.util.HashMap<java.lang.String, java.util.List<com.mobcent.base.forum.android.util.MCBitmapImageCache$BitmapImageCallback>> r4 = r6._callbacks     // Catch:{ all -> 0x0023 }
            java.lang.Object r0 = r4.get(r1)     // Catch:{ all -> 0x0023 }
            java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x0023 }
            if (r0 == 0) goto L_0x0026
            if (r8 == 0) goto L_0x0021
            r0.add(r8)     // Catch:{ all -> 0x0023 }
        L_0x0021:
            monitor-exit(r3)     // Catch:{ all -> 0x0023 }
            goto L_0x0002
        L_0x0023:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0023 }
            throw r4
        L_0x0026:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0023 }
            r0.<init>()     // Catch:{ all -> 0x0023 }
            if (r8 == 0) goto L_0x0030
            r0.add(r8)     // Catch:{ all -> 0x0023 }
        L_0x0030:
            java.util.HashMap<java.lang.String, java.util.List<com.mobcent.base.forum.android.util.MCBitmapImageCache$BitmapImageCallback>> r4 = r6._callbacks     // Catch:{ all -> 0x0023 }
            r4.put(r1, r0)     // Catch:{ all -> 0x0023 }
            monitor-exit(r3)     // Catch:{ all -> 0x0023 }
            java.lang.Thread r2 = new java.lang.Thread
            com.mobcent.base.forum.android.util.MCBitmapImageCache$1 r3 = new com.mobcent.base.forum.android.util.MCBitmapImageCache$1
            r3.<init>(r7, r1)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "BitmapImageCache loader: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.String r4 = r4.toString()
            r2.<init>(r3, r4)
            java.util.concurrent.ExecutorService r3 = com.mobcent.base.forum.android.util.MCBitmapImageCache.originalThreadPool
            r3.execute(r2)
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.base.forum.android.util.MCBitmapImageCache.loadAsync(java.lang.String, com.mobcent.base.forum.android.util.MCBitmapImageCache$BitmapImageCallback):void");
    }

    private String createFileDir() {
        String imagePath2 = MCLibIOUtil.getImageCachePath(this.context);
        if (MCLibIOUtil.isDirExist(imagePath2) || MCLibIOUtil.makeDirs(imagePath2)) {
            return imagePath2;
        }
        return null;
    }

    private Bitmap getBitmap(String fileName) {
        Bitmap bitmap = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inDither = false;
            options.inPurgeable = true;
            FileInputStream fs = null;
            try {
                fs = new FileInputStream(fileName);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            if (fs != null) {
                try {
                    bitmap = BitmapFactory.decodeFileDescriptor(fs.getFD(), null, options);
                    if (fs != null) {
                        try {
                            fs.close();
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                } catch (IOException e3) {
                    e3.printStackTrace();
                    if (fs != null) {
                        try {
                            fs.close();
                        } catch (Exception e22) {
                            e22.printStackTrace();
                        }
                    }
                } catch (Throwable th) {
                    if (fs != null) {
                        try {
                            fs.close();
                        } catch (Exception e23) {
                            e23.printStackTrace();
                        }
                    }
                    throw th;
                }
            }
        } catch (OutOfMemoryError e4) {
        }
        return bitmap;
    }

    public static String formatUrl(String url, String resolution) {
        if (url == null || url.length() == 0) {
            return "";
        }
        if (resolution == null || resolution.length() == 0) {
            return url;
        }
        return url.replace("xgsize", resolution);
    }
}
