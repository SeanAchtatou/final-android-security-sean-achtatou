package com.mobcent.base.android.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.BannedShieldedAsyncTask;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.model.ReplyModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserBoardInfoModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.UserManageService;
import com.mobcent.forum.android.service.impl.UserManageServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.Date;
import java.util.List;

public class UserShieldedActivity extends BaseBannedShieldedDetailsActivity implements MCConstant {
    /* access modifiers changed from: private */
    public BannedShieldedAsyncTask bannedShieldedAsyncTask;
    /* access modifiers changed from: private */
    public String reason;
    /* access modifiers changed from: private */
    public ReplyModel replyModel;
    /* access modifiers changed from: private */
    public Date shieldedDate;
    /* access modifiers changed from: private */
    public String shieldedReasonStr;
    /* access modifiers changed from: private */
    public String shieldedTimeStr;
    /* access modifiers changed from: private */
    public long shieldedUserId;
    /* access modifiers changed from: private */
    public TopicModel topicModel;
    /* access modifiers changed from: private */
    public int type;
    /* access modifiers changed from: private */
    public UserManageService userManageService;

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.userManageService = new UserManageServiceImpl(this);
        Intent intent = getIntent();
        this.type = intent.getIntExtra(MCConstant.SHIELDED_TYPE, 0);
        this.currentUserId = intent.getLongExtra("userId", this.userService.getLoginUserId());
        this.shieldedUserId = intent.getLongExtra("shieldedUserId", 0);
        this.boardId = (int) intent.getLongExtra("boardId", 0);
        this.sureStr = getString(this.resource.getStringId("mc_forum_sure_shielded_user_str"));
        this.topicModel = (TopicModel) intent.getSerializableExtra(MCConstant.TOPICMODEL);
        this.replyModel = (ReplyModel) intent.getSerializableExtra(MCConstant.REPLYMODEL);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        if (this.type == 1) {
            this.titleText.setText(getString(this.resource.getStringId("mc_forum_shielded_str")));
            this.reasonEdit.setHint(getString(this.resource.getStringId("mc_forum_select_shielded_reason_str")));
            this.selectBoardText.setText(getString(this.resource.getStringId("mc_forum_select_shielded_board_str")));
            this.boardTitleText.setText(getString(this.resource.getStringId("mc_forum_shielded_board_str")));
            this.timeBtn.setText(getString(this.resource.getStringId("mc_forum_shielded_time")));
            this.detailDescriptionEdit.setHint(getString(this.resource.getStringId("mc_forum_shielded_tip_detail_content_str")));
        }
        if (this.boardId == -1) {
            this.boardBox.setVisibility(0);
            this.boardTitleText.setVisibility(0);
            this.scrollView.setVisibility(0);
            this.boardDetailText.setVisibility(0);
            return;
        }
        this.boardBox.setVisibility(8);
        this.boardTitleText.setVisibility(8);
        this.scrollView.setVisibility(8);
        this.boardDetailText.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.submitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String unused = UserShieldedActivity.this.shieldedTimeStr = UserShieldedActivity.this.timeText.getText().toString().trim();
                String unused2 = UserShieldedActivity.this.shieldedReasonStr = UserShieldedActivity.this.reasonEdit.getText().toString().trim();
                String unused3 = UserShieldedActivity.this.reason = "[" + UserShieldedActivity.this.shieldedReasonStr + "]" + UserShieldedActivity.this.detailDescriptionEdit.getText().toString().trim();
                Date unused4 = UserShieldedActivity.this.shieldedDate = UserShieldedActivity.this.convertDate(UserShieldedActivity.this.shieldedTimeStr);
                if (UserShieldedActivity.this.selectBoardList.size() <= 0 && UserShieldedActivity.this.boardId == -1) {
                    UserShieldedActivity.this.post();
                } else if (StringUtil.isEmpty(UserShieldedActivity.this.shieldedReasonStr)) {
                    Toast.makeText(UserShieldedActivity.this, UserShieldedActivity.this.getString(UserShieldedActivity.this.resource.getStringId("mc_forum_select_shielded_reason_str")), 1).show();
                } else if (UserShieldedActivity.this.shieldedDate == null) {
                    Toast.makeText(UserShieldedActivity.this, UserShieldedActivity.this.getString(UserShieldedActivity.this.resource.getStringId("mc_forum_select_shielded_time_str")), 1).show();
                } else {
                    UserShieldedActivity.this.post();
                }
            }
        });
    }

    public UserInfoModel getUserInfoModel() {
        return this.userService.getUserBoardInfoList(this.shieldedUserId);
    }

    public List<BoardModel> getBoardInfo(UserInfoModel userInfoModel, List<BoardModel> boardList) {
        if (!(userInfoModel == null || boardList == null || boardList.size() <= 0)) {
            int count = 0;
            List<UserBoardInfoModel> userBoardInfoList = userInfoModel.getUserBoardInfoList();
            if (userBoardInfoList != null && userBoardInfoList.size() > 0) {
                for (int i = 0; i < userBoardInfoList.size(); i++) {
                    if (userBoardInfoList.get(i).getIsShielded() == 1) {
                        for (int j = 0; j < boardList.size(); j++) {
                            if (boardList.get(j).getBoardId() == userBoardInfoList.get(i).getBoardId()) {
                                boardList.get(j).setSelected(true);
                                count++;
                            }
                        }
                    }
                }
                if (count == userBoardInfoList.size()) {
                    boardList.get(0).setSelected(true);
                }
            }
        }
        return boardList;
    }

    /* access modifiers changed from: private */
    public void post() {
        new AlertDialog.Builder(this).setTitle(this.sureStr).setNegativeButton(this.resource.getString("mc_forum_dialog_cancel"), (DialogInterface.OnClickListener) null).setPositiveButton(this.resource.getString("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (UserShieldedActivity.this.boardId == -1) {
                    BannedShieldedAsyncTask unused = UserShieldedActivity.this.bannedShieldedAsyncTask = new BannedShieldedAsyncTask(UserShieldedActivity.this, UserShieldedActivity.this.resource, UserShieldedActivity.this.userManageService, UserShieldedActivity.this.type, UserShieldedActivity.this.currentUserId, UserShieldedActivity.this.shieldedUserId, UserShieldedActivity.this.boardId, UserShieldedActivity.this.selectBoardList, UserShieldedActivity.this.shieldedDate, UserShieldedActivity.this.reason);
                } else {
                    BannedShieldedAsyncTask unused2 = UserShieldedActivity.this.bannedShieldedAsyncTask = new BannedShieldedAsyncTask(UserShieldedActivity.this, UserShieldedActivity.this.resource, UserShieldedActivity.this.userManageService, UserShieldedActivity.this.type, UserShieldedActivity.this.currentUserId, UserShieldedActivity.this.shieldedUserId, UserShieldedActivity.this.boardId, UserShieldedActivity.this.shieldedDate, UserShieldedActivity.this.reason);
                }
                UserShieldedActivity.this.bannedShieldedAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                    public void executeSuccess() {
                        if (UserShieldedActivity.this.topicModel != null) {
                            UserShieldedActivity.this.topicModel.setShieldedUser(true);
                        } else if (UserShieldedActivity.this.replyModel != null) {
                            UserShieldedActivity.this.replyModel.setShieldedUser(true);
                        }
                    }

                    public void executeFail() {
                        if (UserShieldedActivity.this.topicModel != null) {
                            UserShieldedActivity.this.topicModel.setShieldedUser(false);
                        } else if (UserShieldedActivity.this.replyModel != null) {
                            UserShieldedActivity.this.replyModel.setShieldedUser(false);
                        }
                    }
                });
                UserShieldedActivity.this.bannedShieldedAsyncTask.execute(new Object[0]);
                UserShieldedActivity.this.finish();
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.bannedShieldedAsyncTask != null) {
            this.bannedShieldedAsyncTask.cancel(true);
        }
    }
}
