package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.ad.android.ui.widget.AdView;

public class TopicAdapterHolder {
    private AdView adView;
    private LinearLayout locationBox;
    private TextView locationText;
    private ImageView previewImage;
    private AdView topAdview;
    private ImageView topIconImage;
    private RelativeLayout topicItemBox;
    private TextView topicLastUpdateTimeText;
    private TextView topicPublishUserText;
    private TextView topicReplyHitText;
    private TextView topicTitleText;
    private ImageView topicVoiceImg;

    public AdView getAdView() {
        return this.adView;
    }

    public void setAdView(AdView adView2) {
        this.adView = adView2;
    }

    public ImageView getTopicVoiceImg() {
        return this.topicVoiceImg;
    }

    public void setTopicVoiceImg(ImageView topicVoiceImg2) {
        this.topicVoiceImg = topicVoiceImg2;
    }

    public TextView getTopicTitleText() {
        return this.topicTitleText;
    }

    public void setTopicTitleText(TextView topicTitleText2) {
        this.topicTitleText = topicTitleText2;
    }

    public TextView getTopicLastUpdateTimeText() {
        return this.topicLastUpdateTimeText;
    }

    public void setTopicLastUpdateTimeText(TextView topicLastUpdateTimeText2) {
        this.topicLastUpdateTimeText = topicLastUpdateTimeText2;
    }

    public TextView getTopicReplyHitText() {
        return this.topicReplyHitText;
    }

    public void setTopicReplyHitText(TextView topicReplyHitText2) {
        this.topicReplyHitText = topicReplyHitText2;
    }

    public TextView getTopicPublishUserText() {
        return this.topicPublishUserText;
    }

    public void setTopicPublishUserText(TextView topicPublishUserText2) {
        this.topicPublishUserText = topicPublishUserText2;
    }

    public ImageView getPreviewImage() {
        return this.previewImage;
    }

    public void setPreviewImage(ImageView previewImage2) {
        this.previewImage = previewImage2;
    }

    public ImageView getTopIconImage() {
        return this.topIconImage;
    }

    public void setTopIconImage(ImageView topIconImage2) {
        this.topIconImage = topIconImage2;
    }

    public LinearLayout getLocationBox() {
        return this.locationBox;
    }

    public void setLocationBox(LinearLayout locationBox2) {
        this.locationBox = locationBox2;
    }

    public TextView getLocationText() {
        return this.locationText;
    }

    public void setLocationText(TextView locationText2) {
        this.locationText = locationText2;
    }

    public RelativeLayout getTopicItemBox() {
        return this.topicItemBox;
    }

    public void setTopicItemBox(RelativeLayout topicItemBox2) {
        this.topicItemBox = topicItemBox2;
    }

    public AdView getTopAdview() {
        return this.topAdview;
    }

    public void setTopAdview(AdView topAdview2) {
        this.topAdview = topAdview2;
    }
}
