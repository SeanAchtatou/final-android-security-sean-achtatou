package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.FriendAdapter;
import com.mobcent.base.android.ui.activity.fragment.UserFanFragment;
import com.mobcent.base.android.ui.activity.fragment.UserFollowFragment;
import com.mobcent.base.android.ui.activity.fragment.UserFriendsFrament;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.List;

public class FanOrFollowActivity extends BaseFragmentActivity implements MCConstant {
    private Button backBtn;
    private int fragmentId;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private int friendType;
    private FriendAdapter.FriendsClickListener friendsClickListener = new FriendAdapter.FriendsClickListener() {
        public void onUserHomeClick(View arg0, UserInfoModel userInfoModel) {
            MCForumHelper.gotoUserInfo(FanOrFollowActivity.this, FanOrFollowActivity.this.resource, userInfoModel.getUserId());
        }

        public void onMsgChatRoomClick(View arg0, UserInfoModel userInfoModel) {
            Intent intent = new Intent(FanOrFollowActivity.this, MsgChatRoomFragmentActivity.class);
            intent.putExtra(MCConstant.CHAT_USER_ID, userInfoModel.getUserId());
            intent.putExtra(MCConstant.CHAT_USER_NICKNAME, userInfoModel.getNickname());
            intent.putExtra(MCConstant.CHAT_USER_ICON, userInfoModel.getIcon());
            intent.putExtra(MCConstant.BLACK_USER_STATUS, userInfoModel.getBlackStatus());
            FanOrFollowActivity.this.startActivity(intent);
        }
    };
    private UserFriendsFrament friendsFrament;
    private TextView titleText;
    private long userId;

    /* access modifiers changed from: protected */
    public void initData() {
        this.userId = getIntent().getLongExtra("userId", 0);
        this.friendType = getIntent().getIntExtra(MCConstant.FRIEND_TYPE, 0);
        this.fragmentManager = getSupportFragmentManager();
        this.fragmentTransaction = this.fragmentManager.beginTransaction();
        if (this.friendType == 1) {
            this.friendsFrament = new UserFanFragment();
        } else if (this.friendType == 2) {
            this.friendsFrament = new UserFollowFragment();
        }
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_user_fan_and_follow_activity"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.titleText = (TextView) findViewById(this.resource.getViewId("mc_forum_title_text"));
        this.fragmentId = this.resource.getViewId("mc_forum_user_fragment");
        this.friendsFrament.setUserId(this.userId);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        if (this.friendType == 1) {
            this.titleText.setText(this.resource.getStringId("mc_forum_user_fan"));
            if (this.fragmentManager.findFragmentById(this.fragmentId) == null) {
                this.fragmentTransaction.add(this.fragmentId, this.friendsFrament);
            }
        } else if (this.friendType == 2) {
            this.titleText.setText(this.resource.getStringId("mc_forum_user_follow"));
            this.friendsFrament.setAdPosition(new Integer(getResources().getString(this.resource.getStringId("mc_forum_user_friend_position"))).intValue());
            if (this.fragmentManager.findFragmentById(this.fragmentId) == null) {
                this.fragmentTransaction.add(this.fragmentId, this.friendsFrament);
            }
        }
        this.fragmentTransaction.commit();
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FanOrFollowActivity.this.finish();
            }
        });
        this.friendsFrament.setFriendsClickListener(this.friendsClickListener);
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return null;
    }
}
