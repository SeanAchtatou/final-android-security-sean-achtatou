package com.mobcent.base.android.ui.activity.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.ad.android.ui.widget.manager.AdManager;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.BannedManageActivity;
import com.mobcent.base.android.ui.activity.PublishAnnounceActivity;
import com.mobcent.base.android.ui.activity.PublishPollTopicActivity;
import com.mobcent.base.android.ui.activity.PublishTopicActivity;
import com.mobcent.base.android.ui.activity.ReportTopicManageActivity;
import com.mobcent.base.android.ui.activity.ShieldedManageActivity;
import com.mobcent.base.android.ui.activity.adapter.TopicListAdapter;
import com.mobcent.base.android.ui.activity.delegate.GoToPageDelegate;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.android.ui.activity.view.MCFooterPagerBar;
import com.mobcent.base.android.ui.activity.view.MCHeaderSearchView;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.ModeratorService;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.ModeratorServiceImpl;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TopicListFragment extends BaseFragment implements GoToPageDelegate, MCConstant {
    private static final int SHOW_ALL_TOPIC = 1;
    private static final int SHOW_ESSENCE_TOPIC = 2;
    private static final int SHOW_HOT_TOPIC = 3;
    private static final int SHOW_POSTS_TIME = 5;
    /* access modifiers changed from: private */
    public String TAG = "TopicListFragment";
    /* access modifiers changed from: private */
    public int adPosition = 0;
    /* access modifiers changed from: private */
    public TopicListAdapter adapter;
    private TextView allPostsText;
    private TextView applyGlobalAnnounce;
    private Button backBtn;
    private TextView bannedManageText;
    /* access modifiers changed from: private */
    public long boardId = 0;
    /* access modifiers changed from: private */
    public String boardName = "";
    private LinearLayout boardNameLayout;
    /* access modifiers changed from: private */
    public TextView boardNameText;
    private boolean boardPermission;
    /* access modifiers changed from: private */
    public TextView essencePostsText;
    /* access modifiers changed from: private */
    public View headerSearchView;
    /* access modifiers changed from: private */
    public TextView hotPostsText;
    /* access modifiers changed from: private */
    public LayoutInflater inflater;
    /* access modifiers changed from: private */
    public boolean isShowPublishTypeBox = false;
    /* access modifiers changed from: private */
    public boolean isShowTopicTypeBox = false;
    private ImageView line1;
    private ImageView line2;
    /* access modifiers changed from: private */
    public PullToRefreshListView listView;
    private Animation mAnimationDown;
    private Animation mAnimationUp;
    private ModeratorService moderatorService;
    private ObtainEssenceTopicInfoTask obtainEssenceTopicInfoTask;
    private ObtainHotTopicInfoTask obtainHotTopicInfoTask;
    private ObtainPostsTimeTopicInfoTask obtainPostsTimeTopicInfoTask;
    private ObtainTopicInfoTask obtainTopicInfoTask;
    /* access modifiers changed from: private */
    public int pageSize = 20;
    /* access modifiers changed from: private */
    public MCFooterPagerBar pagerBar;
    /* access modifiers changed from: private */
    public PostsService postsService;
    /* access modifiers changed from: private */
    public TextView postsTimeText;
    private TextView publishAnnounce;
    private Button publishBtn;
    private TextView publishPollText;
    private TextView publishText;
    public RelativeLayout publishTypeBox;
    private TextView reportManageText;
    private ImageView selectArrowImg;
    private TextView shieldedManageText;
    /* access modifiers changed from: private */
    public int showState = 1;
    /* access modifiers changed from: private */
    public List<TopicModel> topicList;
    /* access modifiers changed from: private */
    public ScrollView topicTypeBox;
    /* access modifiers changed from: private */
    public LinearLayout transparentLayout;
    private UserService userService;

    /* access modifiers changed from: protected */
    public void initData() {
        this.inflater = LayoutInflater.from(this.activity);
        this.boardId = this.activity.getIntent().getLongExtra("boardId", 0);
        this.boardName = this.activity.getIntent().getStringExtra("boardName");
        this.topicList = new ArrayList();
        initAnimationAction();
        this.postsService = new PostsServiceImpl(this.activity);
        if (this.adPosition == 0) {
            this.adPosition = new Integer(this.activity.getResources().getString(this.mcResource.getStringId("mc_forum_topic_list_position"))).intValue();
        }
        this.moderatorService = new ModeratorServiceImpl(this.activity);
        this.userService = new UserServiceImpl(this.activity);
        this.boardPermission = this.moderatorService.BoardPermission(this.boardId, this.userService.getLoginUserId());
        this.permService = new PermServiceImpl(this.activity);
    }

    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater2, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater2.inflate(this.mcResource.getLayoutId("mc_forum_topic_list_fragment"), (ViewGroup) null);
        this.pagerBar = new MCFooterPagerBar(this.activity, this);
        this.listView = (PullToRefreshListView) this.view.findViewById(this.mcResource.getViewId("mc_forum_lv"));
        this.backBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_back_btn"));
        this.publishBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_publish_btn"));
        this.boardNameLayout = (LinearLayout) this.view.findViewById(this.mcResource.getViewId("mc_forum_board_name_layout"));
        this.boardNameText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_board_name_text"));
        this.selectArrowImg = (ImageView) this.view.findViewById(this.mcResource.getViewId("mc_forum_select_arrow_img"));
        this.transparentLayout = (LinearLayout) this.view.findViewById(this.mcResource.getViewId("mc_forum_transparent_box"));
        this.topicTypeBox = (ScrollView) this.view.findViewById(this.mcResource.getViewId("mc_forum_topic_type_box"));
        this.allPostsText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_all_posts_text"));
        this.essencePostsText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_essence_posts_text"));
        this.hotPostsText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_hot_posts_text"));
        this.postsTimeText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_posts_time_text"));
        this.reportManageText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_report_manage_text"));
        this.bannedManageText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_banned_manage_text"));
        this.shieldedManageText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_shielded_manage_text"));
        this.line1 = (ImageView) this.view.findViewById(this.mcResource.getViewId("mc_forum_line1_img"));
        this.line2 = (ImageView) this.view.findViewById(this.mcResource.getViewId("mc_forum_line2_img"));
        this.publishTypeBox = (RelativeLayout) this.view.findViewById(this.mcResource.getViewId("mc_forum_publish_type_box"));
        this.publishText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_publish_text"));
        this.publishPollText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_publish_vote_text"));
        this.publishAnnounce = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_publish_announce_text"));
        this.applyGlobalAnnounce = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_publish_global_announce_text"));
        MCHeaderSearchView mcHeaderSearchView = new MCHeaderSearchView(this.activity);
        mcHeaderSearchView.setBoardId(this.boardId);
        mcHeaderSearchView.setBoardName(this.boardName);
        this.headerSearchView = mcHeaderSearchView.getHeaderSearchView();
        this.headerSearchView.setVisibility(8);
        this.pagerBar.getView().setVisibility(8);
        this.listView.addHeaderView(this.headerSearchView, null, false);
        this.listView.addFooterView(this.pagerBar.getView());
        this.boardNameText.setText(this.boardName);
        this.adapter = new TopicListAdapter(this.activity, this.topicList, this.boardName, this.mHandler, inflater2, this.asyncTaskLoaderImage, this.adPosition, null, new Integer(this.activity.getResources().getString(this.mcResource.getStringId("mc_forum_topic_list_position_bottom"))).intValue(), 1);
        this.listView.setAdapter((ListAdapter) this.adapter);
        this.listView.removeFooterLayout();
        this.headerSearchView.setVisibility(8);
        this.pagerBar.goToPage(1);
        if (this.boardPermission || this.userService.currentUserIsAdmin()) {
            this.reportManageText.setVisibility(0);
            this.bannedManageText.setVisibility(0);
            this.shieldedManageText.setVisibility(0);
            this.line1.setVisibility(0);
            this.line2.setVisibility(0);
            this.publishAnnounce.setVisibility(0);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_line_image2")).setVisibility(0);
            this.applyGlobalAnnounce.setVisibility(0);
            if (this.userService.currentUserIsAdmin()) {
                this.view.findViewById(this.mcResource.getViewId("mc_forum_line_image2")).setVisibility(8);
                this.applyGlobalAnnounce.setVisibility(8);
            }
        }
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TopicListFragment.this.back();
            }
        });
        this.publishBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!LoginInterceptor.doInterceptorByDialog(TopicListFragment.this.activity, TopicListFragment.this.mcResource, null, null)) {
                    return;
                }
                if (TopicListFragment.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.POST, -1) == 0 || TopicListFragment.this.permService.getPermNum(PermConstant.BOARDS, PermConstant.POST, TopicListFragment.this.boardId) == 0) {
                    TopicListFragment.this.warnMessageByStr(TopicListFragment.this.mcResource.getString("mc_forum_permission_cannot_post_toipc"));
                } else if (TopicListFragment.this.topicTypeBox.getVisibility() == 0) {
                    TopicListFragment.this.showTopicTypeBox(false);
                    boolean unused = TopicListFragment.this.isShowTopicTypeBox = false;
                } else if (!TopicListFragment.this.isShowPublishTypeBox) {
                    TopicListFragment.this.showPublishTypeBox(true);
                } else {
                    TopicListFragment.this.showPublishTypeBox(false);
                }
            }
        });
        this.publishText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TopicListFragment.this.showPublishTypeBox(false);
                TopicListFragment.this.publishTypeBox.setVisibility(8);
                if (LoginInterceptor.doInterceptorByDialog(TopicListFragment.this.activity, TopicListFragment.this.mcResource, PublishTopicActivity.class, new HashMap<>())) {
                    Intent intent = new Intent(TopicListFragment.this.activity, PublishTopicActivity.class);
                    intent.putExtra("boardId", TopicListFragment.this.boardId);
                    intent.putExtra("boardName", TopicListFragment.this.boardName);
                    TopicListFragment.this.startActivity(intent);
                }
                boolean unused = TopicListFragment.this.isShowPublishTypeBox = false;
            }
        });
        this.publishPollText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TopicListFragment.this.showPublishTypeBox(false);
                TopicListFragment.this.publishTypeBox.setVisibility(8);
                if (LoginInterceptor.doInterceptorByDialog(TopicListFragment.this.activity, TopicListFragment.this.mcResource, PublishPollTopicActivity.class, new HashMap<>())) {
                    Intent intent = new Intent(TopicListFragment.this.activity, PublishPollTopicActivity.class);
                    intent.putExtra("boardId", TopicListFragment.this.boardId);
                    intent.putExtra("boardName", TopicListFragment.this.boardName);
                    TopicListFragment.this.startActivity(intent);
                }
                boolean unused = TopicListFragment.this.isShowPublishTypeBox = false;
            }
        });
        this.boardNameLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!TopicListFragment.this.isShowTopicTypeBox) {
                    TopicListFragment.this.showTopicTypeBox(true);
                } else {
                    TopicListFragment.this.showTopicTypeBox(false);
                }
            }
        });
        this.allPostsText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TopicListFragment.this.showTopicTypeBox(false);
                TopicListFragment.this.boardNameText.setText(TopicListFragment.this.boardName);
                TopicListFragment.this.clearCache();
                int unused = TopicListFragment.this.showState = 1;
                TopicListFragment.this.pagerBar.goToPage(1);
            }
        });
        this.essencePostsText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TopicListFragment.this.showTopicTypeBox(false);
                TopicListFragment.this.boardNameText.setText(TopicListFragment.this.essencePostsText.getText());
                TopicListFragment.this.clearCache();
                int unused = TopicListFragment.this.showState = 2;
                TopicListFragment.this.pagerBar.goToPage(1);
            }
        });
        this.hotPostsText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TopicListFragment.this.showTopicTypeBox(false);
                TopicListFragment.this.boardNameText.setText(TopicListFragment.this.hotPostsText.getText());
                TopicListFragment.this.clearCache();
                int unused = TopicListFragment.this.showState = 3;
                TopicListFragment.this.pagerBar.goToPage(1);
            }
        });
        this.transparentLayout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (TopicListFragment.this.transparentLayout.getVisibility() != 0) {
                    return false;
                }
                TopicListFragment.this.showTopicTypeBox(false);
                TopicListFragment.this.showPublishTypeBox(false);
                return false;
            }
        });
        this.postsTimeText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TopicListFragment.this.showTopicTypeBox(false);
                TopicListFragment.this.boardNameText.setText(TopicListFragment.this.postsTimeText.getText());
                TopicListFragment.this.clearCache();
                int unused = TopicListFragment.this.showState = 5;
                TopicListFragment.this.pagerBar.goToPage(1);
                boolean unused2 = TopicListFragment.this.isShowTopicTypeBox = false;
            }
        });
        this.bannedManageText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TopicListFragment.this.showTopicTypeBox(false);
                Intent intent = new Intent(TopicListFragment.this.activity, BannedManageActivity.class);
                intent.putExtra("boardId", TopicListFragment.this.boardId);
                intent.putExtra(MCConstant.BANNED_TYPE, 4);
                TopicListFragment.this.startActivity(intent);
            }
        });
        this.shieldedManageText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TopicListFragment.this.showTopicTypeBox(false);
                Intent intent = new Intent(TopicListFragment.this.activity, ShieldedManageActivity.class);
                intent.putExtra("boardId", TopicListFragment.this.boardId);
                intent.putExtra(MCConstant.SHIELDED_TYPE, 1);
                TopicListFragment.this.startActivity(intent);
            }
        });
        this.publishAnnounce.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TopicListFragment.this.publishTypeBox.setVisibility(8);
                HashMap<String, Serializable> param = new HashMap<>();
                param.put("boardId", Long.valueOf(TopicListFragment.this.boardId));
                param.put("boardName", TopicListFragment.this.boardName);
                if (LoginInterceptor.doInterceptorByDialog(TopicListFragment.this.activity, TopicListFragment.this.mcResource, PublishAnnounceActivity.class, param)) {
                    Intent intent = new Intent(TopicListFragment.this.activity, PublishAnnounceActivity.class);
                    intent.putExtra("boardId", TopicListFragment.this.boardId);
                    intent.putExtra("boardName", TopicListFragment.this.boardName);
                    TopicListFragment.this.startActivity(intent);
                }
            }
        });
        this.listView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                TopicListFragment.this.pagerBar.goToPage(TopicListFragment.this.pagerBar.getCurrentPage());
            }
        });
        this.reportManageText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TopicListFragment.this.showTopicTypeBox(false);
                TopicListFragment.this.activity.startActivity(new Intent(TopicListFragment.this.activity, ReportTopicManageActivity.class));
            }
        });
        this.applyGlobalAnnounce.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
            public void onClick(View v) {
                TopicListFragment.this.publishTypeBox.setVisibility(8);
                if (LoginInterceptor.doInterceptorByDialog(TopicListFragment.this.activity, TopicListFragment.this.mcResource, PublishAnnounceActivity.class, new HashMap<>())) {
                    Intent intent = new Intent(TopicListFragment.this.activity, PublishAnnounceActivity.class);
                    intent.putExtra("boardId", 0L);
                    intent.putExtra("boardName", TopicListFragment.this.mcResource.getString("mc_forum_announce_detail"));
                    TopicListFragment.this.startActivity(intent);
                }
            }
        });
        this.listView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
    }

    /* access modifiers changed from: private */
    public void showTopicTypeBox(boolean isShowTopicTypeBox2) {
        if (this.isShowTopicTypeBox != isShowTopicTypeBox2) {
            this.isShowTopicTypeBox = isShowTopicTypeBox2;
            if (isShowTopicTypeBox2) {
                this.topicTypeBox.setVisibility(0);
                this.transparentLayout.setVisibility(0);
                this.selectArrowImg.startAnimation(this.mAnimationDown);
                return;
            }
            this.topicTypeBox.setVisibility(8);
            this.transparentLayout.setVisibility(8);
            this.selectArrowImg.startAnimation(this.mAnimationUp);
        }
    }

    /* access modifiers changed from: private */
    public void showPublishTypeBox(boolean isShowPublishTypeBox2) {
        if (this.isShowPublishTypeBox != isShowPublishTypeBox2) {
            this.isShowPublishTypeBox = isShowPublishTypeBox2;
            if (isShowPublishTypeBox2) {
                this.publishTypeBox.setVisibility(0);
                this.transparentLayout.setVisibility(0);
                return;
            }
            this.publishTypeBox.setVisibility(8);
            this.transparentLayout.setVisibility(8);
        }
    }

    private void initAnimationAction() {
        this.mAnimationDown = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);
        this.mAnimationDown.setInterpolator(new LinearInterpolator());
        this.mAnimationDown.setDuration(250);
        this.mAnimationDown.setFillAfter(true);
        this.mAnimationUp = new RotateAnimation(-180.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.mAnimationUp.setInterpolator(new LinearInterpolator());
        this.mAnimationUp.setDuration(250);
        this.mAnimationUp.setFillAfter(true);
    }

    /* access modifiers changed from: private */
    public void clearCache() {
        this.asyncTaskLoaderImage.recycleBitmaps(getImageURL(this.topicList));
        this.topicList = new ArrayList();
        this.adapter.setTopicList(this.topicList);
        this.adapter.notifyDataSetInvalidated();
        this.adapter.notifyDataSetChanged();
    }

    private class ObtainTopicInfoTask extends AbstractObtainTopicInfoTask {
        private ObtainTopicInfoTask() {
            super();
        }

        /* access modifiers changed from: protected */
        public List<TopicModel> getTopicList(long boardId, int page) {
            return TopicListFragment.this.postsService.getTopicList(boardId, page, TopicListFragment.this.pageSize);
        }
    }

    private class ObtainEssenceTopicInfoTask extends AbstractObtainTopicInfoTask {
        private ObtainEssenceTopicInfoTask() {
            super();
        }

        /* access modifiers changed from: protected */
        public List<TopicModel> getTopicList(long boardId, int page) {
            return TopicListFragment.this.postsService.getEssenceTopicList(boardId, page, TopicListFragment.this.pageSize);
        }
    }

    private class ObtainHotTopicInfoTask extends AbstractObtainTopicInfoTask {
        private ObtainHotTopicInfoTask() {
            super();
        }

        /* access modifiers changed from: protected */
        public List<TopicModel> getTopicList(long boardId, int page) {
            return TopicListFragment.this.postsService.getHotTopicList(boardId, page, TopicListFragment.this.pageSize);
        }
    }

    private class ObtainPostsTimeTopicInfoTask extends AbstractObtainTopicInfoTask {
        private ObtainPostsTimeTopicInfoTask() {
            super();
        }

        /* access modifiers changed from: protected */
        public List<TopicModel> getTopicList(long boardId, int page) {
            return TopicListFragment.this.postsService.getPostTimeTopicList(boardId, page, TopicListFragment.this.pageSize);
        }
    }

    private abstract class AbstractObtainTopicInfoTask extends AsyncTask<Object, Void, List<TopicModel>> {
        /* access modifiers changed from: protected */
        public abstract List<TopicModel> getTopicList(long j, int i);

        private AbstractObtainTopicInfoTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<TopicModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            TopicListFragment.this.listView.setSelection(0);
            if (TopicListFragment.this.listView.getFooterViewsCount() == 0) {
                TopicListFragment.this.listView.addFooterView(TopicListFragment.this.pagerBar.getView());
            }
            TopicListFragment.this.pagerBar.getView().setVisibility(8);
        }

        /* access modifiers changed from: protected */
        public List<TopicModel> doInBackground(Object... params) {
            return getTopicList(((Long) params[0]).longValue(), ((Integer) params[1]).intValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<TopicModel> topicModelList) {
            TopicListFragment.this.listView.onRefreshComplete();
            TopicListFragment.this.headerSearchView.setVisibility(0);
            if (topicModelList == null) {
                TopicListFragment.this.listView.onBottomRefreshComplete(3, TopicListFragment.this.getString(TopicListFragment.this.mcResource.getStringId("mc_forum_no_topic_receive")));
            } else if (topicModelList.isEmpty()) {
                TopicListFragment.this.listView.onBottomRefreshComplete(3, TopicListFragment.this.getString(TopicListFragment.this.mcResource.getStringId("mc_forum_no_topic_receive")));
            } else if (!StringUtil.isEmpty(topicModelList.get(0).getErrorCode())) {
                Toast.makeText(TopicListFragment.this.activity, MCForumErrorUtil.convertErrorCode(TopicListFragment.this.activity, topicModelList.get(0).getErrorCode()), 0).show();
                if (topicModelList.get(0).getErrorCode().equals(BaseReturnCodeConstant.ERROR_REQUEST_USER_SHIELED)) {
                    TopicListFragment.this.activity.finish();
                }
            } else {
                TopicListFragment.this.asyncTaskLoaderImage.recycleBitmaps(TopicListFragment.this.getImageURL(TopicListFragment.this.topicList));
                TopicListFragment.this.pagerBar.getView().setVisibility(0);
                TopicListFragment.this.listView.setVisibility(0);
                TopicListFragment.this.pagerBar.updatePagerInfo(topicModelList.get(0).getTotalNum(), TopicListFragment.this.pageSize);
                if (TopicListFragment.this.pagerBar.getTotalPage() <= 1 && TopicListFragment.this.listView.getFooterViewsCount() > 0) {
                    TopicListFragment.this.listView.removeFooterView(TopicListFragment.this.pagerBar.getView());
                }
                AdManager.getInstance().recyclAdByTag(TopicListFragment.this.TAG);
                TopicListAdapter unused = TopicListFragment.this.adapter = new TopicListAdapter(TopicListFragment.this.activity, topicModelList, TopicListFragment.this.boardName, TopicListFragment.this.mHandler, TopicListFragment.this.inflater, TopicListFragment.this.asyncTaskLoaderImage, TopicListFragment.this.adPosition, null, new Integer(TopicListFragment.this.activity.getResources().getString(TopicListFragment.this.mcResource.getStringId("mc_forum_topic_list_position_bottom"))).intValue(), 1);
                TopicListFragment.this.listView.setAdapter((ListAdapter) TopicListFragment.this.adapter);
                TopicListFragment.this.listView.setSelection(1);
                List unused2 = TopicListFragment.this.topicList = topicModelList;
            }
        }
    }

    public void goToPage(int page) {
        this.listView.onRefreshWithOutListener();
        switch (this.showState) {
            case 1:
                this.obtainTopicInfoTask = new ObtainTopicInfoTask();
                this.obtainTopicInfoTask.execute(new Object[]{Long.valueOf(this.boardId), Integer.valueOf(page)});
                return;
            case 2:
                this.obtainEssenceTopicInfoTask = new ObtainEssenceTopicInfoTask();
                this.obtainEssenceTopicInfoTask.execute(new Object[]{Long.valueOf(this.boardId), Integer.valueOf(page)});
                return;
            case 3:
                this.obtainHotTopicInfoTask = new ObtainHotTopicInfoTask();
                this.obtainHotTopicInfoTask.execute(new Object[]{Long.valueOf(this.boardId), Integer.valueOf(page)});
                return;
            case 4:
            default:
                this.obtainTopicInfoTask.execute(new Object[]{Long.valueOf(this.boardId), Integer.valueOf(page)});
                return;
            case 5:
                this.obtainPostsTimeTopicInfoTask = new ObtainPostsTimeTopicInfoTask();
                this.obtainPostsTimeTopicInfoTask.execute(new Object[]{Long.valueOf(this.boardId), Integer.valueOf(page)});
                return;
        }
    }

    /* access modifiers changed from: private */
    public List<String> getImageURL(List<TopicModel> topicList2) {
        List<String> imgUrl = new ArrayList<>();
        if (topicList2 != null && !topicList2.isEmpty()) {
            int j = topicList2.size();
            for (int i = 0; i < j; i++) {
                TopicModel topicModel = topicList2.get(i);
                if (!StringUtil.isEmpty(topicModel.getPicPath())) {
                    imgUrl.add(AsyncTaskLoaderImage.formatUrl(topicModel.getBaseUrl() + topicModel.getPicPath(), "100x100"));
                }
            }
        }
        return imgUrl;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.topicTypeBox.getVisibility() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        showTopicTypeBox(false);
        return false;
    }

    public void onDestroy() {
        super.onDestroy();
        if (!(this.obtainTopicInfoTask == null || this.obtainTopicInfoTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.obtainTopicInfoTask.cancel(true);
        }
        if (!(this.obtainEssenceTopicInfoTask == null || this.obtainEssenceTopicInfoTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.obtainEssenceTopicInfoTask.cancel(true);
        }
        if (!(this.obtainHotTopicInfoTask == null || this.obtainHotTopicInfoTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.obtainHotTopicInfoTask.cancel(true);
        }
        if (!(this.obtainPostsTimeTopicInfoTask == null || this.obtainPostsTimeTopicInfoTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.obtainPostsTimeTopicInfoTask.cancel(true);
        }
        AdManager.getInstance().recyclAdByTag(this.TAG);
    }
}
