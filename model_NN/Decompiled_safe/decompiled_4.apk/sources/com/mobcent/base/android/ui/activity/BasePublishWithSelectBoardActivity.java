package com.mobcent.base.android.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.PublishTopicBoardListAdapter;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.service.BoardService;
import com.mobcent.forum.android.service.impl.BoardServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public abstract class BasePublishWithSelectBoardActivity extends BasePublishTopicActivityWithDraft implements MCConstant {
    protected PublishTopicBoardListAdapter boardListAdapter;
    private GetBoardListAsyncTask boardListAsyncTask;
    protected String boardName;
    protected RelativeLayout boardNameLayout;
    protected BoardService boardService;
    protected boolean isShowBoardBox = false;
    protected ProgressDialog loadBoardDialog;
    protected boolean pageFromNotBoard = false;
    protected RelativeLayout transparentLayout;

    /* access modifiers changed from: protected */
    public abstract void updateBoardList(List<BoardModel> list);

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        Intent intent = getIntent();
        if (intent != null) {
            this.initBoardId = intent.getLongExtra("boardId", 0);
            this.boardId = this.initBoardId;
            this.boardName = intent.getStringExtra("boardName");
        }
        if (this.initBoardId <= 0) {
            this.pageFromNotBoard = true;
        }
        this.boardList = new ArrayList();
        this.boardListAdapter = new PublishTopicBoardListAdapter(this, this.boardList);
        this.boardService = new BoardServiceImpl(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        this.titleEdit.setVisibility(0);
        this.boardNameLayout = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_board_name_layout"));
        this.transparentLayout = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_transparent_box"));
        this.loadBoardDialog = new ProgressDialog(this);
        this.loadBoardDialog.setMessage(getResources().getString(this.resource.getStringId("mc_forum_loading_board")));
        this.selectBoardImg.setText(this.boardName);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.boardListview.setAdapter((ListAdapter) this.boardListAdapter);
        this.selectBoardImg.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                BasePublishWithSelectBoardActivity.this.showBoardListBoxEvent();
                return true;
            }
        });
        this.boardListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                BoardModel boardModel = (BoardModel) BasePublishWithSelectBoardActivity.this.boardList.get(position);
                for (int i = 0; i < BasePublishWithSelectBoardActivity.this.boardList.size(); i++) {
                    if (((BoardModel) BasePublishWithSelectBoardActivity.this.boardList.get(i)).getBoardId() != boardModel.getBoardId()) {
                        ((BoardModel) BasePublishWithSelectBoardActivity.this.boardList.get(i)).setSelected(false);
                    } else {
                        boardModel.setSelected(true);
                    }
                }
                BasePublishWithSelectBoardActivity.this.boardListAdapter.notifyDataSetInvalidated();
                BasePublishWithSelectBoardActivity.this.boardListAdapter.notifyDataSetChanged();
                BasePublishWithSelectBoardActivity.this.boardId = boardModel.getBoardId();
                BasePublishWithSelectBoardActivity.this.boardName = boardModel.getBoardName();
                BasePublishWithSelectBoardActivity.this.selectBoardImg.setText(boardModel.getBoardName());
                BasePublishWithSelectBoardActivity.this.transparentLayout.setVisibility(8);
                BasePublishWithSelectBoardActivity.this.boardListview.setVisibility(8);
                BasePublishWithSelectBoardActivity.this.showBoardListBox(false);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void showBoardListBox(boolean isShowBoardBox2) {
        if (this.isShowBoardBox != isShowBoardBox2) {
            this.isShowBoardBox = isShowBoardBox2;
            if (isShowBoardBox2) {
                this.boardListview.setVisibility(0);
                this.transparentLayout.setVisibility(0);
                this.isShowBoardBox = false;
                return;
            }
            this.transparentLayout.setVisibility(8);
            this.boardListview.setVisibility(8);
            this.isShowBoardBox = true;
        }
    }

    /* access modifiers changed from: protected */
    public void showBoardListBoxEvent() {
        if (this.boardList == null || this.boardList.isEmpty()) {
            this.boardList = this.boardService.getBoadListByLocal();
            if (this.boardList == null) {
                if (this.boardListAsyncTask != null) {
                    this.boardListAsyncTask.cancel(true);
                }
                this.boardListAsyncTask = new GetBoardListAsyncTask();
                this.boardListAsyncTask.execute(new Void[0]);
            } else {
                updateBoardList(this.boardList);
            }
        }
        if (!this.isShowBoardBox) {
            showBoardListBox(true);
        } else {
            showBoardListBox(false);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.loadBoardDialog != null && this.loadBoardDialog.isShowing()) {
            this.loadBoardDialog.cancel();
        }
        if (this.boardListview.getVisibility() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.boardListview.setVisibility(8);
        return true;
    }

    class GetBoardListAsyncTask extends AsyncTask<Void, Void, List<BoardModel>> {
        GetBoardListAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<BoardModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            BasePublishWithSelectBoardActivity.this.loadBoardDialog.show();
        }

        /* access modifiers changed from: protected */
        public List<BoardModel> doInBackground(Void... arg0) {
            return BasePublishWithSelectBoardActivity.this.boardService.getBoadListByNet();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<BoardModel> result) {
            super.onPostExecute((Object) result);
            if (BasePublishWithSelectBoardActivity.this.loadBoardDialog != null && BasePublishWithSelectBoardActivity.this.loadBoardDialog.isShowing()) {
                BasePublishWithSelectBoardActivity.this.loadBoardDialog.dismiss();
            }
            if (result != null && result.size() > 0) {
                if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                    Toast.makeText(BasePublishWithSelectBoardActivity.this, MCForumErrorUtil.convertErrorCode(BasePublishWithSelectBoardActivity.this, result.get(0).getErrorCode()), 0).show();
                    return;
                }
                BasePublishWithSelectBoardActivity.this.updateBoardList(result);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.boardListAsyncTask != null) {
            this.boardListAsyncTask.cancel(true);
        }
    }
}
