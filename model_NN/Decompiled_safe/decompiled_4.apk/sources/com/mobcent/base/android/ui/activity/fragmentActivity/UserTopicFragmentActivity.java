package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.TopicListAdapter;
import com.mobcent.base.android.ui.activity.fragment.SurroundTopicFragment;
import com.mobcent.base.android.ui.activity.fragment.UserFavoriteTopicFragment;
import com.mobcent.base.android.ui.activity.fragment.UserGenerateTopicFragment;
import com.mobcent.base.android.ui.activity.fragment.UserGenericTopicFragment;
import com.mobcent.base.android.ui.activity.fragment.UserReplyTopicFragment;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.forum.android.model.TopicModel;
import java.util.List;

public class UserTopicFragmentActivity extends BaseFragmentActivity implements MCConstant {
    private int category;
    private int fragmentId;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    TopicListAdapter.PostsClickListener listener = new TopicListAdapter.PostsClickListener() {
        public void onPostsClick(View v, TopicModel topicModel, String boardName) {
            MCForumHelper.onPostsClick(UserTopicFragmentActivity.this, UserTopicFragmentActivity.this.resource, v, topicModel, boardName);
        }
    };
    private SurroundTopicFragment surroundTopicFragment;
    private UserGenericTopicFragment topicFrament;
    private long userId;
    private String userNickname;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.userId = getIntent().getLongExtra("userId", 0);
        this.userNickname = getIntent().getStringExtra("nickname");
        this.fragmentManager = getSupportFragmentManager();
        this.fragmentTransaction = this.fragmentManager.beginTransaction();
        this.category = getIntent().getIntExtra(MCConstant.USER_TOPIC, 0);
        if (this.category == 1) {
            this.topicFrament = new UserGenerateTopicFragment();
        } else if (this.category == 0) {
            this.topicFrament = new UserReplyTopicFragment();
        } else if (this.category == 2) {
            this.topicFrament = new UserFavoriteTopicFragment();
        } else if (this.category == 3) {
            this.surroundTopicFragment = new SurroundTopicFragment();
        }
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_user_topic_fragment_activity"));
        this.fragmentId = this.resource.getViewId("mc_forum_user_generic_fragment");
        if (this.topicFrament != null && this.userId != 0 && this.userNickname != null) {
            this.topicFrament.setUserId(this.userId);
            this.topicFrament.setNickName(this.userNickname);
        }
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        if (this.category == 1) {
            this.topicFrament.setAdPosition(new Integer(getResources().getString(this.resource.getStringId("mc_forum_user_generate_position"))).intValue());
            if (this.fragmentManager.findFragmentById(this.fragmentId) == null) {
                this.fragmentTransaction.add(this.fragmentId, this.topicFrament);
            }
        } else if (this.category == 0) {
            this.topicFrament.setAdPosition(new Integer(getResources().getString(this.resource.getStringId("mc_forum_user_joined_position"))).intValue());
            if (this.fragmentManager.findFragmentById(this.fragmentId) == null) {
                this.fragmentTransaction.add(this.fragmentId, this.topicFrament);
            }
        } else if (this.category == 2) {
            this.topicFrament.setAdPosition(new Integer(getResources().getString(this.resource.getStringId("mc_forum_user_favorite_position"))).intValue());
            if (this.fragmentManager.findFragmentById(this.fragmentId) == null) {
                this.fragmentTransaction.add(this.fragmentId, this.topicFrament);
            }
        } else if (this.category == 3 && this.fragmentManager.findFragmentById(this.fragmentId) == null) {
            this.fragmentTransaction.add(this.fragmentId, this.surroundTopicFragment);
        }
        this.fragmentTransaction.commit();
        if (this.topicFrament != null) {
            this.topicFrament.setPostsClickListener(this.listener);
        }
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return null;
    }
}
