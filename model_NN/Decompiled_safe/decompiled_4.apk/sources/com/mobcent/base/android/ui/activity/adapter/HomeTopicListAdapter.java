package com.mobcent.base.android.ui.activity.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.location.LocationClientOption;
import com.mobcent.ad.android.model.AdModel;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.PostsActivity;
import com.mobcent.base.android.ui.activity.adapter.holder.TopicAdapterHolder;
import com.mobcent.base.forum.android.util.MCColorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.constant.PostsApiConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.PermService;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.HashMap;
import java.util.List;

public class HomeTopicListAdapter extends BaseAdapter implements PostsApiConstant, MCConstant {
    private final String TEXT_BLANL = "   ";
    /* access modifiers changed from: private */
    public Activity activity;
    private HashMap<Integer, List<AdModel>> adHashMap;
    private AsyncTaskLoaderImage asyncTaskLoaderImage;
    private String boardName;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public PermService permService;
    /* access modifiers changed from: private */
    public MCResource resource;
    private List<TopicModel> topicList;

    public HomeTopicListAdapter(Activity activity2, List<TopicModel> topicList2, String boardName2, Handler mHandler2, LayoutInflater inflater2, int pageSize, AsyncTaskLoaderImage asyncTaskLoaderImage2) {
        this.activity = activity2;
        this.topicList = topicList2;
        this.inflater = inflater2;
        this.boardName = boardName2;
        this.mHandler = mHandler2;
        this.resource = MCResource.getInstance(activity2);
        this.asyncTaskLoaderImage = asyncTaskLoaderImage2;
        this.permService = new PermServiceImpl(activity2);
    }

    public HashMap<Integer, List<AdModel>> getAdHashMap() {
        return this.adHashMap;
    }

    public void setAdHashMap(HashMap<Integer, List<AdModel>> adHashMap2) {
        this.adHashMap = adHashMap2;
    }

    public List<TopicModel> getTopicList() {
        return this.topicList;
    }

    public void setTopicList(List<TopicModel> topicList2) {
        this.topicList = topicList2;
    }

    public int getCount() {
        return getTopicList().size();
    }

    public Object getItem(int position) {
        return getTopicList().get(position);
    }

    public long getItemId(int position) {
        return getTopicList().get(position).getTopicId();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        String signStr;
        String s;
        TopicModel topicModel = getTopicList().get(position);
        View convertView2 = getTopicConvertView(convertView);
        TopicAdapterHolder holder = (TopicAdapterHolder) convertView2.getTag();
        if (topicModel.getUserNickName().length() <= 7) {
            holder.getTopicPublishUserText().setText(topicModel.getUserNickName());
        } else {
            holder.getTopicPublishUserText().setText(topicModel.getUserNickName().substring(0, 6) + "...");
        }
        if (topicModel.getBoardName().length() <= 4) {
            holder.getTopicLastUpdateTimeText().setText("[" + topicModel.getBoardName() + "]");
        } else {
            holder.getTopicLastUpdateTimeText().setText("[" + topicModel.getBoardName().substring(0, 4) + "...]");
        }
        if (topicModel.getHitCount() >= 0) {
            MCColorUtil.setTextViewPart(this.activity, holder.getTopicReplyHitText(), topicModel.getReplieCount() + "/" + topicModel.getHitCount(), 0, ("" + topicModel.getReplieCount()).length(), "mc_forum_text_hight_color");
        } else {
            holder.getTopicReplyHitText().setText("");
        }
        if (topicModel.getTop() == 1) {
            holder.getTopIconImage().setVisibility(0);
        } else {
            holder.getTopIconImage().setVisibility(8);
        }
        if (topicModel.getHot() == 1 && topicModel.getEssence() == 1) {
            signStr = this.activity.getResources().getString(this.resource.getStringId("mc_forum_essence_posts_mark")) + this.activity.getResources().getString(this.resource.getStringId("mc_forum_hot_posts_mark"));
        } else if (topicModel.getHot() == 1) {
            signStr = this.activity.getResources().getString(this.resource.getStringId("mc_forum_hot_posts_mark"));
        } else if (topicModel.getEssence() == 1) {
            signStr = this.activity.getResources().getString(this.resource.getStringId("mc_forum_essence_posts_mark"));
        } else {
            signStr = "";
        }
        if (topicModel.getType() == 1) {
            signStr = signStr + "[" + this.activity.getResources().getString(this.resource.getStringId("mc_forum_poll")) + "]";
        }
        String titleStr = signStr + topicModel.getTitle();
        int signEndPosition = signStr.length();
        holder.getTopicTitleText().setText(titleStr);
        MCColorUtil.setTextViewPart(this.activity, holder.getTopicTitleText(), titleStr, 0, signEndPosition, "mc_forum_text_hight_color");
        if (topicModel.getPicPath() == null || topicModel.getPicPath().trim().equals("")) {
            holder.getPreviewImage().setVisibility(8);
        } else if (topicModel.getStatus() != 0) {
            holder.getPreviewImage().setVisibility(0);
            holder.getPreviewImage().setImageResource(this.resource.getDrawableId("mc_forum_x_img"));
            updateThumbnailImage(topicModel.getBaseUrl() + topicModel.getPicPath(), holder.getPreviewImage());
        }
        if (!StringUtil.isEmpty(topicModel.getLocation()) || topicModel.getDistance() >= 0) {
            holder.getLocationBox().setVisibility(0);
            String location = topicModel.getLocation();
            int distance = topicModel.getDistance() / LocationClientOption.MIN_SCAN_SPAN;
            if (distance == 0) {
                s = location + "   " + MCStringBundleUtil.resolveString(this.resource.getStringId("mc_forum_surround_distance_str1"), topicModel.getDistance() + "", this.activity);
            } else {
                s = location + "   " + MCStringBundleUtil.resolveString(this.resource.getStringId("mc_forum_surround_distance_str2"), distance + "", this.activity);
            }
            holder.getLocationText().setText(s);
            MCColorUtil.setTextViewPart(this.activity, holder.getLocationText(), s, location.length(), s.length(), "mc_forum_text_unapparent_color");
        } else {
            holder.getLocationBox().setVisibility(8);
        }
        if (topicModel.getUploadType() == 2) {
            holder.getTopicVoiceImg().setVisibility(0);
        } else {
            holder.getTopicVoiceImg().setVisibility(8);
        }
        final TopicModel topicModel2 = topicModel;
        convertView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (topicModel2.getUserId() == new UserServiceImpl(HomeTopicListAdapter.this.activity).getLoginUserId()) {
                    HomeTopicListAdapter.this.convertViewClickEvent(topicModel2);
                } else if (HomeTopicListAdapter.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.READ, -1) != 1 || HomeTopicListAdapter.this.permService.getPermNum(PermConstant.BOARDS, PermConstant.READ, topicModel2.getBoardId()) != 1) {
                    Toast.makeText(HomeTopicListAdapter.this.activity, HomeTopicListAdapter.this.resource.getString("mc_forum_permission_cannot_read_topic"), 0).show();
                } else if (topicModel2.getVisible() != 3) {
                    HomeTopicListAdapter.this.convertViewClickEvent(topicModel2);
                } else if (StringUtil.isEmpty(HomeTopicListAdapter.this.permService.getGroupType()) || !PermConstant.SYSTEM.equals(HomeTopicListAdapter.this.permService.getGroupType())) {
                    Toast.makeText(HomeTopicListAdapter.this.activity, HomeTopicListAdapter.this.resource.getString("mc_forum_permission_only_moderator_see"), 0).show();
                } else {
                    HomeTopicListAdapter.this.convertViewClickEvent(topicModel2);
                }
            }
        });
        return convertView2;
    }

    /* access modifiers changed from: private */
    public void convertViewClickEvent(TopicModel topicModel) {
        Intent intent = new Intent(this.activity, PostsActivity.class);
        intent.putExtra("boardId", topicModel.getBoardId());
        intent.putExtra("boardName", this.boardName);
        intent.putExtra("topicId", topicModel.getTopicId());
        intent.putExtra(MCConstant.TOPIC_USER_ID, topicModel.getUserId());
        intent.putExtra("baseUrl", topicModel.getBaseUrl());
        intent.putExtra(MCConstant.THUMBNAIL_IMAGE_URL, topicModel.getPicPath());
        intent.putExtra("type", topicModel.getType());
        intent.putExtra(MCConstant.TOP, topicModel.getTop());
        intent.putExtra(MCConstant.ESSENCE, topicModel.getEssence());
        intent.putExtra(MCConstant.CLOSE, topicModel.getStatus());
        this.activity.startActivity(intent);
    }

    private View getTopicConvertView(View convertView) {
        TopicAdapterHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_topic_item"), (ViewGroup) null);
            holder = new TopicAdapterHolder();
            initTopicAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (TopicAdapterHolder) convertView.getTag();
        }
        if (holder != null) {
            return convertView;
        }
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_topic_item"), (ViewGroup) null);
        TopicAdapterHolder holder2 = new TopicAdapterHolder();
        initTopicAdapterHolder(convertView2, holder2);
        convertView2.setTag(holder2);
        return convertView2;
    }

    private void updateThumbnailImage(String imgUrl, final ImageView imageView) {
        if (SharedPreferencesDB.getInstance(this.activity).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(AsyncTaskLoaderImage.formatUrl(imgUrl, "100x100"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    if (image != null) {
                        HomeTopicListAdapter.this.mHandler.post(new Runnable() {
                            public void run() {
                                if (image != null) {
                                    imageView.setVisibility(0);
                                    imageView.setImageBitmap(image);
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    private void initTopicAdapterHolder(View convertView, TopicAdapterHolder holder) {
        holder.setPreviewImage((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_thumbnail_img")));
        holder.setTopicLastUpdateTimeText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_last_update_time_text")));
        holder.setTopicPublishUserText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_nickname_text")));
        holder.setTopicReplyHitText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_reply_hit_text")));
        holder.setTopicTitleText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_topic_title_text")));
        holder.setTopIconImage((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_top_icon_img")));
        holder.setLocationBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_location_box")));
        holder.setLocationText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_location_text")));
        holder.setTopicVoiceImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_topic_voice_img")));
    }
}
