package com.mobcent.base.android.ui.activity.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.AnnounceActivity;
import com.mobcent.base.android.ui.activity.PostsActivity;
import com.mobcent.base.android.ui.activity.ReplyTopicActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.MsgChatRoomFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.android.ui.activity.helper.MCForumLaunchShareHelper;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.forum.android.util.MCColorUtil;
import com.mobcent.base.forum.android.util.MCForumReverseList;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.constant.FeedsConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.AnnoModel;
import com.mobcent.forum.android.model.ReplyModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.DateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class AbstractFeedsAdapter extends BaseAdapter implements FeedsConstant {
    protected AsyncTaskLoaderImage asyncTaskLoaderImage;
    protected Context context;
    protected LayoutInflater inflater;
    protected ListView listView;
    protected Handler mHandler;
    protected MCResource resource;

    public AbstractFeedsAdapter(Context context2, LayoutInflater inflater2, Handler mHandler2, AsyncTaskLoaderImage asyncTaskLoaderImage2, ListView listView2) {
        this.context = context2;
        this.inflater = inflater2;
        this.resource = MCResource.getInstance(context2);
        this.mHandler = mHandler2;
        this.asyncTaskLoaderImage = asyncTaskLoaderImage2;
        this.listView = listView2;
    }

    /* access modifiers changed from: protected */
    public View getUserTopicView(final TopicModel topicModel, int ftype) {
        View view = this.inflater.inflate(this.resource.getLayoutId("mc_forum_new_feeds_user_topic"), (ViewGroup) null);
        TextView feedType = (TextView) view.findViewById(this.resource.getViewId("mc_forum_feed_type"));
        ((TextView) view.findViewById(this.resource.getViewId("mc_forum_time_text"))).setText(DateUtil.getFormatTimeByYear(topicModel.getCreateDate()));
        ((TextView) view.findViewById(this.resource.getViewId("mc_forum_feed_title_text"))).setText(topicModel.getTitle());
        TextView contentText = (TextView) view.findViewById(this.resource.getViewId("mc_forum_feed_content_text"));
        ImageView thumbnailImg = (ImageView) view.findViewById(this.resource.getViewId("mc_forum_feed_thumbnail_img"));
        ImageView voiceImg = (ImageView) view.findViewById(this.resource.getViewId("mc_forum_voice_img"));
        if (topicModel.getHasVoice() == 1) {
            voiceImg.setVisibility(0);
        } else {
            voiceImg.setVisibility(8);
        }
        if (!topicModel.isHasImg()) {
            thumbnailImg.setVisibility(8);
        } else {
            thumbnailImg.setVisibility(0);
            updateImage(topicModel.getPic(), thumbnailImg);
        }
        String typeStr = "";
        switch (ftype) {
            case 1:
                typeStr = "mc_forum_home_publish";
                break;
            case 3:
                typeStr = "mc_forum_home_mention2";
                break;
            case 5:
                typeStr = "mc_forum_home_share_topic";
                break;
            case 6:
                typeStr = "mc_forum_home_attention_topic";
                break;
            case 7:
                typeStr = "mc_forum_home_top";
                break;
            case 8:
                typeStr = "mc_forum_home_enssence";
                break;
            case 10:
                typeStr = "mc_forum_home_poll_publish";
                break;
            case 11:
                typeStr = "mc_forum_home_poll_reply";
                break;
        }
        feedType.setText(this.resource.getString(typeStr));
        if (ftype == 1 || ftype == 10) {
            contentText.setText(topicModel.getThumbnail());
        } else if (ftype == 6 || ftype == 11 || ftype == 5 || ftype == 7 || ftype == 8 || ftype == 3) {
            String s = topicModel.getUserNickName() + ":" + topicModel.getThumbnail();
            contentText.setText(s);
            MCColorUtil.setTextViewPart(this.context.getApplicationContext(), contentText, s, 0, topicModel.getUserNickName().length(), "mc_forum_text_hight_color");
        }
        ((ImageButton) view.findViewById(this.resource.getViewId("mc_forum_more_btn"))).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AbstractFeedsAdapter.this.getTopicFunctionDialog(topicModel);
            }
        });
        return view;
    }

    /* access modifiers changed from: protected */
    public View getUserReplyView(TopicModel topicModel, ReplyModel replyModel, ReplyModel replyQuoteModel) {
        View view = this.inflater.inflate(this.resource.getLayoutId("mc_forum_new_feeds_user_reply"), (ViewGroup) null);
        ((TextView) view.findViewById(this.resource.getViewId("mc_forum_feed_type"))).setText(this.resource.getString("mc_forum_home_reply"));
        ((TextView) view.findViewById(this.resource.getViewId("mc_forum_time_text"))).setText(DateUtil.getFormatTimeByYear(replyModel.getPostsDate()));
        ((TextView) view.findViewById(this.resource.getViewId("mc_forum_feed_content_text"))).setText(topicModel.getTitle());
        ImageView contentImg = (ImageView) view.findViewById(this.resource.getViewId("mc_forum_feed_quote_content_img"));
        if (topicModel.isHasImg()) {
            contentImg.setVisibility(0);
            updateImage(topicModel.getPic(), contentImg);
        }
        TextView quoteContent = (TextView) view.findViewById(this.resource.getViewId("mc_forum_feed_quote_content_text"));
        if (replyQuoteModel != null) {
            MCColorUtil.setTextViewPart(this.context.getApplicationContext(), quoteContent, replyQuoteModel.getUserNickName() + ":" + replyQuoteModel.getThumbnail(), 0, replyQuoteModel.getUserNickName().length(), "mc_forum_text_hight_color");
            quoteContent.setVisibility(0);
        } else {
            quoteContent.setVisibility(8);
        }
        TextView replyedText = (TextView) view.findViewById(this.resource.getViewId("mc_forum_feed_replyed_text"));
        String replyedStr = replyModel.getUserNickName() + ":" + replyModel.getThumbnail();
        replyedText.setText(replyedStr);
        MCColorUtil.setTextViewPart(this.context.getApplicationContext(), replyedText, replyedStr, 0, replyModel.getUserNickName().length(), "mc_forum_text_hight_color");
        final TopicModel topicModel2 = topicModel;
        final ReplyModel replyModel2 = replyModel;
        ((ImageButton) view.findViewById(this.resource.getViewId("mc_forum_more_btn"))).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AbstractFeedsAdapter.this.getReplyFunctionDialog(topicModel2, replyModel2, true);
            }
        });
        return view;
    }

    /* access modifiers changed from: protected */
    public View getTopicPeplyView(ReplyModel replyModel, View view) {
        MCColorUtil.setTextViewPart(this.context.getApplicationContext(), (TextView) view.findViewById(this.resource.getViewId("mc_forum_feed_reply_text")), replyModel.getUserNickName() + ":" + replyModel.getThumbnail(), 0, replyModel.getUserNickName().length(), "mc_forum_text_hight_color");
        return view;
    }

    public void updateUserIcon(String imgUrl, final ImageView imgView) {
        if (SharedPreferencesDB.getInstance(this.context).getPicModeFlag()) {
            imgView.setTag(imgUrl);
            imgView.setBackgroundResource(this.resource.getDrawableId("mc_forum_head"));
            this.asyncTaskLoaderImage.loadAsync(imgUrl, new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    if (image != null) {
                        AbstractFeedsAdapter.this.mHandler.post(new Runnable() {
                            public void run() {
                                if (image != null) {
                                    imgView.setBackgroundDrawable(new BitmapDrawable(image));
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void updateImage(final String imgUrl, ImageView imgView) {
        if (SharedPreferencesDB.getInstance(this.context).getPicModeFlag()) {
            imgView.setTag(imgUrl);
            this.asyncTaskLoaderImage.loadAsync(imgUrl, new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    if (image != null) {
                        AbstractFeedsAdapter.this.mHandler.post(new Runnable() {
                            public void run() {
                                if (image != null) {
                                    ((ImageView) AbstractFeedsAdapter.this.listView.findViewWithTag(imgUrl)).setImageBitmap(image);
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    class Clickable extends ClickableSpan implements View.OnClickListener {
        protected final View.OnClickListener mListener;

        public Clickable(View.OnClickListener l) {
            this.mListener = l;
        }

        public void onClick(View v) {
            this.mListener.onClick(v);
        }
    }

    /* access modifiers changed from: protected */
    public void showUserRole(TextView textView, int roleNum) {
        String text = "";
        if (roleNum == 4) {
            text = this.context.getResources().getString(this.resource.getStringId("mc_forum_role_moderator"));
        } else if (roleNum == 8) {
            text = this.context.getResources().getString(this.resource.getStringId("mc_forum_role_administrator"));
        } else if (roleNum == 16) {
            text = this.context.getResources().getString(this.resource.getStringId("mc_forum_role_super_administrator"));
        }
        textView.setText(text);
    }

    /* access modifiers changed from: protected */
    public AlertDialog getUserFunctionDialog(UserInfoModel userInfo) {
        List<String> f = new ArrayList<>();
        final String sendMsg = this.resource.getString("mc_forum_send_msg");
        final String visitHome = this.resource.getString("mc_forum_visit_user_home");
        f.add(sendMsg);
        f.add(visitHome);
        final String[] function = MCForumReverseList.convertListToArray(f);
        final UserInfoModel userInfoModel = userInfo;
        AlertDialog alertDialog = new AlertDialog.Builder(this.context).setTitle(this.resource.getString("mc_forum_function")).setItems(function, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (function[which].equals(sendMsg)) {
                    Intent intent = new Intent(AbstractFeedsAdapter.this.context, MsgChatRoomFragmentActivity.class);
                    intent.putExtra(MCConstant.CHAT_USER_ID, userInfoModel.getUserId());
                    intent.putExtra(MCConstant.CHAT_USER_NICKNAME, userInfoModel.getNickname());
                    intent.putExtra(MCConstant.CHAT_USER_ICON, userInfoModel.getIcon());
                    intent.putExtra(MCConstant.BLACK_USER_STATUS, userInfoModel.getBlackStatus());
                    AbstractFeedsAdapter.this.context.startActivity(intent);
                } else if (function[which].equals(visitHome)) {
                    MCForumHelper.gotoUserInfo((Activity) AbstractFeedsAdapter.this.context, AbstractFeedsAdapter.this.resource, userInfoModel.getUserId());
                }
            }
        }).create();
        alertDialog.show();
        return alertDialog;
    }

    /* access modifiers changed from: protected */
    public void getTopicFunctionDialog(TopicModel topicModel) {
        List<String> f = new ArrayList<>();
        final String visitAnno = this.resource.getString("mc_forum_home_visit_anno");
        final String visitTopic = this.resource.getString("mc_forum_check_topic");
        final String reply = this.resource.getString("mc_forum_reply");
        final String share = this.resource.getString("mc_forum_share");
        String visitHome = MCStringBundleUtil.resolveString(this.resource.getStringId("mc_forum_visit_user_home_with_name"), topicModel.getUserNickName(), this.context);
        if (topicModel instanceof AnnoModel) {
            f.add(visitAnno);
        } else {
            f.add(visitTopic);
        }
        f.add(reply);
        if ((topicModel instanceof AnnoModel) && ((AnnoModel) topicModel).getIsReply() == 0) {
            f.remove(reply);
        }
        f.add(share);
        f.add(visitHome);
        final String[] function = MCForumReverseList.convertListToArray(f);
        final TopicModel topicModel2 = topicModel;
        new AlertDialog.Builder(this.context).setTitle(this.resource.getString("mc_forum_function")).setItems(function, new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(DialogInterface dialog, int which) {
                if (function[which].equals(visitTopic)) {
                    AbstractFeedsAdapter.this.visitTopic(topicModel2);
                } else if (function[which].equals(visitAnno)) {
                    if (!(topicModel2 instanceof AnnoModel)) {
                        return;
                    }
                    if (((AnnoModel) topicModel2).getIsReply() == 1) {
                        AbstractFeedsAdapter.this.visitTopic(topicModel2);
                        return;
                    }
                    Intent intent = new Intent(AbstractFeedsAdapter.this.context, AnnounceActivity.class);
                    intent.putExtra("boardId", topicModel2.getBoardId());
                    intent.putExtra("announceId", ((AnnoModel) topicModel2).getAnnoId());
                    intent.putExtra("baseUrl", topicModel2.getBaseUrl());
                    AbstractFeedsAdapter.this.context.startActivity(intent);
                } else if (function[which].equals(reply)) {
                    HashMap<String, Serializable> param = new HashMap<>();
                    param.put("boardId", Long.valueOf(topicModel2.getBoardId()));
                    param.put("topicId", Long.valueOf(topicModel2.getTopicId()));
                    param.put("toReplyId", -1L);
                    param.put(MCConstant.IS_QUOTE_TOPIC, false);
                    param.put(MCConstant.POSTS_USER_LIST, new ArrayList());
                    if (LoginInterceptor.doInterceptor(AbstractFeedsAdapter.this.context, ReplyTopicActivity.class, param)) {
                        Intent intent2 = new Intent(AbstractFeedsAdapter.this.context, ReplyTopicActivity.class);
                        intent2.putExtra("boardId", topicModel2.getBoardId());
                        intent2.putExtra("topicId", topicModel2.getTopicId());
                        intent2.putExtra("toReplyId", -1L);
                        intent2.putExtra(MCConstant.IS_QUOTE_TOPIC, false);
                        intent2.putExtra(MCConstant.POSTS_USER_LIST, new ArrayList());
                        AbstractFeedsAdapter.this.context.startActivity(intent2);
                    }
                } else if (function[which].equals(share)) {
                    AbstractFeedsAdapter.this.shareTopic(topicModel2);
                } else {
                    MCForumHelper.gotoUserInfo((Activity) AbstractFeedsAdapter.this.context, AbstractFeedsAdapter.this.resource, topicModel2.getUserId());
                }
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void getReplyFunctionDialog(TopicModel topicModel, ReplyModel replyModel, boolean shareReply) {
        List<String> f = new ArrayList<>();
        final String visitTopic = this.resource.getString("mc_forum_check_topic");
        final String reply = this.resource.getString("mc_forum_reply");
        final String share = this.resource.getString("mc_forum_share");
        String visitHome = MCStringBundleUtil.resolveString(this.resource.getStringId("mc_forum_visit_user_home_with_name"), replyModel.getUserNickName(), this.context);
        f.add(visitTopic);
        f.add(reply);
        f.add(share);
        f.add(visitHome);
        final String[] function = MCForumReverseList.convertListToArray(f);
        final TopicModel topicModel2 = topicModel;
        final ReplyModel replyModel2 = replyModel;
        final boolean z = shareReply;
        new AlertDialog.Builder(this.context).setTitle(this.resource.getString("mc_forum_function")).setItems(function, new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(DialogInterface dialog, int which) {
                if (function[which].equals(visitTopic)) {
                    Intent intent = new Intent(AbstractFeedsAdapter.this.context, PostsActivity.class);
                    intent.putExtra("boardId", topicModel2.getBoardId());
                    intent.putExtra("boardName", "");
                    intent.putExtra("topicId", topicModel2.getTopicId());
                    intent.putExtra(MCConstant.TOPIC_USER_ID, topicModel2.getUserId());
                    intent.putExtra("baseUrl", topicModel2.getBaseUrl());
                    intent.putExtra(MCConstant.THUMBNAIL_IMAGE_URL, topicModel2.getPicPath());
                    intent.putExtra("type", topicModel2.getType());
                    intent.putExtra(MCConstant.TOP, topicModel2.getTop());
                    intent.putExtra(MCConstant.ESSENCE, topicModel2.getEssence());
                    intent.putExtra(MCConstant.CLOSE, topicModel2.getStatus());
                    AbstractFeedsAdapter.this.context.startActivity(intent);
                } else if (function[which].equals(reply)) {
                    HashMap<String, Serializable> param = new HashMap<>();
                    param.put("boardId", Long.valueOf(topicModel2.getBoardId()));
                    param.put("topicId", Long.valueOf(topicModel2.getTopicId()));
                    param.put("toReplyId", Long.valueOf(replyModel2.getReplyPostsId()));
                    param.put(MCConstant.IS_QUOTE_TOPIC, true);
                    param.put(MCConstant.POSTS_USER_LIST, new ArrayList());
                    if (LoginInterceptor.doInterceptor(AbstractFeedsAdapter.this.context, ReplyTopicActivity.class, param)) {
                        Intent intent2 = new Intent(AbstractFeedsAdapter.this.context, ReplyTopicActivity.class);
                        intent2.putExtra("boardId", topicModel2.getBoardId());
                        intent2.putExtra("topicId", topicModel2.getTopicId());
                        intent2.putExtra("toReplyId", replyModel2.getReplyPostsId());
                        intent2.putExtra(MCConstant.IS_QUOTE_TOPIC, true);
                        intent2.putExtra(MCConstant.POSTS_USER_LIST, new ArrayList());
                        AbstractFeedsAdapter.this.context.startActivity(intent2);
                    }
                } else if (!function[which].equals(share)) {
                    MCForumHelper.gotoUserInfo((Activity) AbstractFeedsAdapter.this.context, AbstractFeedsAdapter.this.resource, replyModel2.getReplyUserId());
                } else if (z) {
                    AbstractFeedsAdapter.this.shareReply(topicModel2, replyModel2);
                } else {
                    AbstractFeedsAdapter.this.shareTopic(topicModel2);
                }
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public String getShareContent(TopicModel topic, ReplyModel reply) {
        String shareContent;
        String shareContent2;
        try {
            if (topic instanceof AnnoModel) {
                shareContent = BaseReturnCodeConstant.ERROR_CODE + ((AnnoModel) topic).getSubject() + BaseReturnCodeConstant.ERROR_CODE + "\n";
            } else {
                shareContent = BaseReturnCodeConstant.ERROR_CODE + topic.getTitle() + BaseReturnCodeConstant.ERROR_CODE + "\n";
            }
            if (reply == null) {
                if (topic.getThumbnail().length() <= 70) {
                    shareContent2 = shareContent + topic.getThumbnail();
                } else {
                    shareContent2 = shareContent + topic.getThumbnail().substring(0, 70);
                }
            } else if (reply.getThumbnail().length() <= 70) {
                shareContent2 = shareContent + reply.getThumbnail();
            } else {
                shareContent2 = shareContent + reply.getThumbnail().substring(0, 70);
            }
            return shareContent2;
        } catch (Exception e) {
            return "";
        }
    }

    /* access modifiers changed from: protected */
    public void shareTopic(TopicModel topicModel) {
        String shareContent = getShareContent(topicModel, null);
        if (topicModel.isHasImg() || shareContent.length() > 70) {
            MCForumLaunchShareHelper.shareContentWithImageUrl(shareContent, topicModel.getTopicId() + "-" + 1, "", "", this.context);
        } else {
            MCForumLaunchShareHelper.shareContent(shareContent, "", "", this.context);
        }
    }

    /* access modifiers changed from: protected */
    public void shareReply(TopicModel topicModel, ReplyModel replyModel) {
        String shareContent = getShareContent(topicModel, replyModel);
        if (replyModel.isHasImg() || shareContent.length() > 70) {
            MCForumLaunchShareHelper.shareContentWithImageUrl(shareContent, replyModel.getReplyPostsId() + "-" + 0, "", "", this.context);
        } else {
            MCForumLaunchShareHelper.shareContent(shareContent, "", "", this.context);
        }
    }

    /* access modifiers changed from: protected */
    public void visitTopic(TopicModel topicModel) {
        Intent intent = new Intent(this.context, PostsActivity.class);
        intent.putExtra("boardId", topicModel.getBoardId());
        intent.putExtra("boardName", "");
        intent.putExtra("topicId", topicModel.getTopicId());
        intent.putExtra(MCConstant.TOPIC_USER_ID, topicModel.getUserId());
        intent.putExtra("baseUrl", topicModel.getBaseUrl());
        intent.putExtra(MCConstant.THUMBNAIL_IMAGE_URL, topicModel.getPicPath());
        intent.putExtra("type", topicModel.getType());
        intent.putExtra(MCConstant.TOP, topicModel.getTop());
        intent.putExtra(MCConstant.ESSENCE, topicModel.getEssence());
        intent.putExtra(MCConstant.CLOSE, topicModel.getStatus());
        this.context.startActivity(intent);
    }
}
