package com.mobcent.base.android.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.base.android.ui.activity.fragmentActivity.HomeTopicFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.ForumInitializeHelper;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;
import com.mobcent.base.forum.android.util.ImageCache;
import com.mobcent.forum.android.constant.MCForumConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.PhoneUtil;

public class SplashScreen extends BaseActivity {
    public Long defaultColseMillis = 2000L;
    public Long defaultDelayMillis = 1200L;
    /* access modifiers changed from: private */
    public Handler handler;
    /* access modifiers changed from: private */
    public ImageView splashBackgroundImg;
    private RelativeLayout splashBottomLayout;
    private MCProgressBar splashLoadingImg;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ForumInitializeHelper.getInstance().init(this, new ForumInitializeHelper.McForumInitializeListener() {
            public void onPostExecute(String result) {
                if (!SharedPreferencesDB.getInstance(SplashScreen.this).getPayStateLoadingPage()) {
                    SplashScreen.this.defaultDelayMillis = 0L;
                }
                SplashScreen.this.handler.postDelayed(new Runnable() {
                    public void run() {
                        SplashScreen.this.startActivity(new Intent(SplashScreen.this, HomeTopicFragmentActivity.class));
                    }
                }, SplashScreen.this.defaultDelayMillis.longValue());
                SplashScreen.this.handler.postDelayed(new Runnable() {
                    public void run() {
                        if (SplashScreen.this.splashBackgroundImg != null) {
                            SplashScreen.this.splashBackgroundImg.setBackgroundDrawable(null);
                            SplashScreen.this.splashBackgroundImg.setImageDrawable(null);
                        }
                        SplashScreen.this.finish();
                    }
                }, SplashScreen.this.defaultColseMillis.longValue());
            }
        });
        AsyncTaskLoaderImage.getInstance(getApplicationContext()).loadAsync(ImageCache.formatUrl(SharedPreferencesDB.getInstance(getApplicationContext()).getPayStateImgUrl() + SharedPreferencesDB.getInstance(getApplicationContext()).getPayStateLoadingPageImage().split(AdApiConstant.RES_SPLIT_COMMA)[0], MCForumConstant.RESOLUTION_640X480), new AsyncTaskLoaderImage.BitmapImageCallback() {
            public void onImageLoaded(Bitmap bitmap, String arg1) {
                if (SplashScreen.this.splashBackgroundImg != null && bitmap != null) {
                    TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), new BitmapDrawable(SplashScreen.this.getResources(), bitmap)});
                    td.startTransition(350);
                    SplashScreen.this.splashBackgroundImg.setImageDrawable(td);
                    SplashScreen.this.splashBackgroundImg.setBackgroundDrawable(null);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.handler = new Handler();
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        getWindow().setFlags(1024, 1024);
        setContentView(this.resource.getLayoutId("mc_forum_splash_activity"));
        this.splashBackgroundImg = (ImageView) findViewById(this.resource.getViewId("mc_forum_splash_background_img"));
        this.splashBottomLayout = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_splash_bottom_layout"));
        this.splashLoadingImg = (MCProgressBar) findViewById(this.resource.getViewId("mc_forum_splash_loading_img"));
        int screenHeight = PhoneUtil.getDisplayHeight((Activity) this);
        int screenWidth = PhoneUtil.getDisplayWidth((Activity) this);
        int imgHeight = (int) (((double) screenWidth) * 1.5d);
        int bottomBoxHeight = screenHeight - imgHeight;
        RelativeLayout.LayoutParams imgLps = (RelativeLayout.LayoutParams) this.splashBackgroundImg.getLayoutParams();
        imgLps.width = screenWidth;
        imgLps.height = imgHeight;
        if (screenHeight <= imgHeight) {
            this.splashBottomLayout.setVisibility(8);
            return;
        }
        this.splashBottomLayout.getLayoutParams().width = screenWidth;
        this.splashBottomLayout.getLayoutParams().height = bottomBoxHeight;
        this.splashBottomLayout.setVisibility(0);
        this.splashLoadingImg.show();
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
