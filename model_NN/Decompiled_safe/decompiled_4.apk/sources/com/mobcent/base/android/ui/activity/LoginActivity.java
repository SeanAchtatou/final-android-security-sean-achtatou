package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.base.android.constant.EmailResourceConstant;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.constant.PlatFormResourceConstant;
import com.mobcent.base.android.ui.activity.adapter.PlatformLoginAdapter;
import com.mobcent.base.android.ui.activity.adapter.RegLoginListAdapter;
import com.mobcent.base.android.ui.activity.delegate.RegLoginFinishDelegate;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserLoginFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCTouchUtil;
import com.mobcent.forum.android.db.UserJsonDBUtil;
import com.mobcent.forum.android.db.helper.UserJsonDBHelper;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.os.service.helper.LocationOSServiceHelper;
import com.mobcent.forum.android.service.MentionFriendService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.MentionFriendServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.MD5Util;
import com.mobcent.forum.android.util.StringUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LoginActivity extends BaseActivity implements MCConstant, RegLoginFinishDelegate {
    private static Set<LoginActivity> loginActSets;
    private String TAG = "LoginActivity";
    private Button aboutBtn;
    private Button backBtn;
    /* access modifiers changed from: private */
    public String changeDefaultPwd = "(*& ^%$";
    /* access modifiers changed from: private */
    public String changedPwd;
    /* access modifiers changed from: private */
    public String defaultPwd = "$%^ &*(";
    /* access modifiers changed from: private */
    public int emailMaxLen = 64;
    private TextView forgetPassText;
    /* access modifiers changed from: private */
    public MentionFriendService friendService;
    /* access modifiers changed from: private */
    public HashMap<String, Serializable> goParam;
    /* access modifiers changed from: private */
    public Class<?> goToActivityClass;
    private GridView gridView;
    /* access modifiers changed from: private */
    public boolean isKeyEnterFromEmail = false;
    /* access modifiers changed from: private */
    public LoginAsyncTask loginAsyncTask;
    private RelativeLayout loginBox;
    /* access modifiers changed from: private */
    public RelativeLayout loginEmailBox;
    /* access modifiers changed from: private */
    public EditText loginEmailEdit;
    private ListView loginEmailList;
    /* access modifiers changed from: private */
    public EditText loginPwdEdit;
    private Button loginSubmitBtn;
    private PlatformLoginAdapter platformLoginAdapter;
    /* access modifiers changed from: private */
    public String pwd;
    /* access modifiers changed from: private */
    public int pwdMinLen = 6;
    private RelativeLayout regLoginBox;
    /* access modifiers changed from: private */
    public RegLoginListAdapter regLoginListAdapter;
    private Button regSubmitBtn;
    private UserInfoModel userInfoModel;
    private List<UserInfoModel> userInfoModels;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (loginActSets == null) {
            loginActSets = new HashSet();
        }
        for (LoginActivity act : loginActSets) {
            act.finish();
        }
        loginActSets.add(this);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            this.goToActivityClass = (Class) intent.getSerializableExtra(MCConstant.TAG);
            this.goParam = (HashMap) intent.getSerializableExtra(MCConstant.GO_PARAM);
        }
        RegActivity.setLoginDelegate(this);
        this.regLoginListAdapter = new RegLoginListAdapter(this, new ArrayList(), this.resource);
        UserService userService = new UserServiceImpl(this);
        this.userInfoModel = userService.getCurrUser();
        this.userInfoModels = userService.getAllUsers();
        this.platformLoginAdapter = new PlatformLoginAdapter(this, PlatFormResourceConstant.getMCResourceConstant().getPlatformInfos());
        this.friendService = new MentionFriendServiceImpl(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_user_login_activity"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.loginSubmitBtn = (Button) findViewById(this.resource.getViewId("mc_forum_user_login_submit_btn"));
        this.regSubmitBtn = (Button) findViewById(this.resource.getViewId("mc_forum_user_reg_submit_btn"));
        this.loginEmailEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_user_login_email_edit"));
        this.gridView = (GridView) findViewById(this.resource.getViewId("mc_forum_platform_grid"));
        this.forgetPassText = (TextView) findViewById(this.resource.getViewId("mc_froum_user_forget_password"));
        MCTouchUtil.createTouchDelegate(this.forgetPassText, 10);
        if (!(this.userInfoModel == null || this.userInfoModel.getEmail() == null || this.userInfoModel.getEmail().equals(""))) {
            this.loginEmailEdit.setText(this.userInfoModel.getEmail());
        }
        if (MCForumHelper.isCopyright(this)) {
            findViewById(this.resource.getViewId("mc_forum_power_by")).setVisibility(8);
            findViewById(this.resource.getViewId("mc_forum_user_mobcent_login_warn_text")).setVisibility(8);
        }
        this.loginPwdEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_user_login_password_edit"));
        if (!(this.userInfoModel == null || this.userInfoModel.getPwd() == null || this.userInfoModel.getPwd().equals(""))) {
            this.loginPwdEdit.setText(this.defaultPwd);
            this.pwd = this.userInfoModel.getPwd();
        }
        this.regLoginBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_user_login_register_login_box"));
        this.loginBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_user_login_box"));
        this.loginEmailBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_user_login_email_box"));
        this.loginEmailList = (ListView) findViewById(this.resource.getViewId("mc_forum_user_login_email_list"));
        this.aboutBtn = (Button) findViewById(this.resource.getViewId("mc_forum_about"));
        this.loginEmailList.setAdapter((ListAdapter) this.regLoginListAdapter);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.gridView.setAdapter((ListAdapter) this.platformLoginAdapter);
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoginActivity.this.back();
            }
        });
        this.loginSubmitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String password;
                String email = LoginActivity.this.loginEmailEdit.getText().toString();
                if (!StringUtil.isEmail(email)) {
                    LoginActivity.this.warnMessageById("mc_forum_user_email_format_error_warn");
                } else if (email.length() > LoginActivity.this.emailMaxLen) {
                    LoginActivity.this.warnMessageById("mc_forum_user_email_length_error_warn");
                } else {
                    String password2 = LoginActivity.this.loginPwdEdit.getText().toString();
                    if (password2.equals(LoginActivity.this.defaultPwd)) {
                        password = LoginActivity.this.pwd;
                    } else if (password2.equals(LoginActivity.this.changeDefaultPwd)) {
                        password = LoginActivity.this.changedPwd;
                    } else if (password2.length() < LoginActivity.this.pwdMinLen || password2.length() > 20) {
                        LoginActivity.this.warnMessageById("mc_forum_user_password_length_error_warn");
                        return;
                    } else if (!StringUtil.isPwdMatchRule(password2)) {
                        LoginActivity.this.warnMessageById("mc_forum_user_password_format_error_warn");
                        return;
                    } else {
                        password = MD5Util.toMD5(password2);
                    }
                    if (LoginActivity.this.loginAsyncTask != null) {
                        LoginActivity.this.loginAsyncTask.cancel(true);
                    }
                    LoginAsyncTask unused = LoginActivity.this.loginAsyncTask = new LoginAsyncTask();
                    LoginActivity.this.loginAsyncTask.execute(email, password);
                }
            }
        });
        this.loginPwdEdit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String loginPwd = LoginActivity.this.loginPwdEdit.getText().toString();
                if (loginPwd.equals(LoginActivity.this.changeDefaultPwd) || loginPwd.equals(LoginActivity.this.defaultPwd)) {
                    LoginActivity.this.loginPwdEdit.setText("");
                    LoginActivity.this.loginEmailBox.setVisibility(8);
                }
            }
        });
        this.loginPwdEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View arg0, boolean arg1) {
                if (arg1) {
                    String loginPwd = LoginActivity.this.loginPwdEdit.getText().toString();
                    if (loginPwd.equals(LoginActivity.this.changeDefaultPwd) || loginPwd.equals(LoginActivity.this.defaultPwd)) {
                        LoginActivity.this.loginPwdEdit.setText("");
                        LoginActivity.this.loginEmailBox.setVisibility(8);
                    }
                }
            }
        });
        this.loginPwdEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66) {
                    if (!LoginActivity.this.isKeyEnterFromEmail) {
                        LoginActivity.this.hideSoftKeyboard();
                    }
                    boolean unused = LoginActivity.this.isKeyEnterFromEmail = false;
                }
                return false;
            }
        });
        this.regSubmitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegActivity.class);
                if (LoginActivity.this.goToActivityClass != null) {
                    intent.putExtra(MCConstant.TAG, LoginActivity.this.goToActivityClass);
                    intent.putExtra(MCConstant.GO_PARAM, LoginActivity.this.goParam);
                }
                LoginActivity.this.startActivity(intent);
            }
        });
        this.loginEmailEdit.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String loginEmail = LoginActivity.this.loginEmailEdit.getText().toString();
                int len = loginEmail.length();
                if (len > 0) {
                    LoginActivity.this.notifyData(loginEmail);
                } else if (len == 0) {
                    LoginActivity.this.getUsersByLimit();
                }
            }

            public void afterTextChanged(Editable s) {
                if (LoginActivity.this.loginEmailEdit.getText().toString().length() > 0) {
                    LoginActivity.this.getPwdByEmail(LoginActivity.this.loginEmailEdit.getText().toString());
                }
            }
        });
        this.loginEmailEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode != 66) {
                    return false;
                }
                LoginActivity.this.loginPwdEdit.requestFocus();
                boolean unused = LoginActivity.this.isKeyEnterFromEmail = true;
                return false;
            }
        });
        this.loginEmailEdit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoginActivity.this.showLoginEmailBox(LoginActivity.this.loginEmailEdit.getText().toString());
            }
        });
        this.loginEmailEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                LoginActivity.this.loginPwdEdit.setText("");
                String loginEmail = LoginActivity.this.loginEmailEdit.getText().toString();
                if (hasFocus) {
                    LoginActivity.this.showLoginEmailBox(loginEmail);
                }
            }
        });
        this.loginEmailList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                LoginActivity.this.loginEmailEdit.setText(LoginActivity.this.regLoginListAdapter.getEmailSections().get(position));
                Editable loginEmail = LoginActivity.this.loginEmailEdit.getText();
                Selection.setSelection(loginEmail, loginEmail.length());
                LoginActivity.this.loginEmailBox.setVisibility(8);
            }
        });
        this.regLoginBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoginActivity.this.loginEmailBox.setVisibility(8);
            }
        });
        this.forgetPassText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoginActivity.this.startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class));
            }
        });
        this.aboutBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoginActivity.this.startActivity(new Intent(LoginActivity.this, AboutActivity.class));
            }
        });
    }

    /* access modifiers changed from: private */
    public void showLoginEmailBox(String loginEmail) {
        if (StringUtil.isEmail(loginEmail) || loginEmail.length() == 0) {
            getUsersByLimit();
        } else {
            notifyData(loginEmail);
        }
    }

    /* access modifiers changed from: private */
    public void notifyData(String str) {
        this.loginEmailBox.setVisibility(0);
        new ArrayList();
        this.regLoginListAdapter.setEmailSections(EmailResourceConstant.getResourceConstant().convertEmail(str));
        this.regLoginListAdapter.notifyDataSetChanged();
        this.regLoginListAdapter.notifyDataSetInvalidated();
    }

    /* access modifiers changed from: private */
    public void getUsersByLimit() {
        if (this.userInfoModels == null) {
            this.loginEmailBox.setVisibility(8);
            return;
        }
        ArrayList<String> emailSelections = new ArrayList<>();
        for (UserInfoModel userInfoModel2 : this.userInfoModels) {
            emailSelections.add(userInfoModel2.getEmail());
        }
        if (emailSelections.size() == 0) {
            this.loginEmailBox.setVisibility(8);
        } else {
            this.loginEmailBox.setVisibility(0);
        }
        this.regLoginListAdapter.setEmailSections(emailSelections);
        this.regLoginListAdapter.notifyDataSetChanged();
        this.regLoginListAdapter.notifyDataSetInvalidated();
    }

    /* access modifiers changed from: private */
    public void getPwdByEmail(String str) {
        if (this.userInfoModels != null) {
            for (UserInfoModel userInfoModel2 : this.userInfoModels) {
                String email = userInfoModel2.getEmail();
                MCLogUtil.d(this.TAG, "email--->" + email);
                MCLogUtil.d(this.TAG, "str-->" + str);
                if (email.equals(str)) {
                    this.loginPwdEdit.setText(this.changeDefaultPwd);
                    this.changedPwd = userInfoModel2.getPwd();
                }
            }
        }
    }

    class LoginAsyncTask extends AsyncTask<String, Void, UserInfoModel> {
        LoginAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            LoginActivity.this.showProgressDialog("mc_forum_warn_login", this);
        }

        /* access modifiers changed from: protected */
        public UserInfoModel doInBackground(String... params) {
            UserService userService = new UserServiceImpl(LoginActivity.this);
            if (params[0] == null && params[1] == null) {
                return null;
            }
            return userService.loginUser(params[0], params[1]);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(final UserInfoModel result) {
            LoginActivity.this.hideProgressDialog();
            super.onPostExecute((Object) result);
            if (result == null) {
                return;
            }
            if (result.getErrorCode() == null || "".equals(result.getErrorCode())) {
                UserJsonDBUtil.getInstance(LoginActivity.this).savePersonalInfo(result.getUserId(), 1, UserJsonDBHelper.buildPersonalInfo(result));
                UserLoginFragmentActivity.finishAll();
                LoginActivity.this.clearNotification();
                new Thread() {
                    public void run() {
                        LoginActivity.this.friendService.getFroumMentionFriend(result.getUserId());
                    }
                }.start();
                LocationOSServiceHelper.startLocationOSService(LoginActivity.this);
                if (LoginActivity.this.goToActivityClass != null) {
                    LoginActivity.this.goToTargetActivity();
                }
                LoginActivity.this.finish();
                return;
            }
            LoginActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(LoginActivity.this, result.getErrorCode()));
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean back = false;
        if (keyCode == 4) {
            back = backEventClickListener();
        }
        if (!back) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean backEventClickListener() {
        if (this.loginEmailBox.getVisibility() != 0 || this.loginBox.getVisibility() != 0) {
            return true;
        }
        this.loginEmailBox.setVisibility(8);
        return false;
    }

    /* access modifiers changed from: protected */
    public void goToTargetActivity() {
        if (this.goToActivityClass != null) {
            Intent intent = new Intent(this, this.goToActivityClass);
            if (this.goParam != null) {
                for (String key : this.goParam.keySet()) {
                    intent.putExtra(key, this.goParam.get(key));
                }
            }
            startActivity(intent);
        }
    }

    public Class<?> getGoToActivityClass() {
        return this.goToActivityClass;
    }

    public void setGoToActivityClass(Class<?> goToActivityClass2) {
        this.goToActivityClass = goToActivityClass2;
    }

    public HashMap<String, Serializable> getGoParam() {
        return this.goParam;
    }

    public void setGoParam(HashMap<String, Serializable> goParam2) {
        this.goParam = goParam2;
    }

    public void activityFinish() {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        loginActSets.remove(this);
        if (this.loginAsyncTask != null) {
            this.loginAsyncTask.cancel(true);
        }
    }
}
