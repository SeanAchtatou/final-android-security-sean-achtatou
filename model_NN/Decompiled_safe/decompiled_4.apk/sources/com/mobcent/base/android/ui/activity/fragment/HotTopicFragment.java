package com.mobcent.base.android.ui.activity.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import java.util.List;

public class HotTopicFragment extends HomeTopicFragment {
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.initViews(inflater, container, savedInstanceState);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.topicListAdapter);
        return this.view;
    }

    public List<TopicModel> getHomeTopicList() {
        return new PostsServiceImpl(getActivity()).getHotTopicList(0, this.currentPage, this.pageSize, this.isLocal);
    }
}
