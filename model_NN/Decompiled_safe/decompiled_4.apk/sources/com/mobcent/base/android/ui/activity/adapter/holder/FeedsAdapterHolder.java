package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FeedsAdapterHolder {
    private LinearLayout contentBox;
    private TextView contentText;
    private ImageView iconImg;
    private TextView nameText;
    private TextView timeText;

    public LinearLayout getContentBox() {
        return this.contentBox;
    }

    public void setContentBox(LinearLayout contentBox2) {
        this.contentBox = contentBox2;
    }

    public TextView getNameText() {
        return this.nameText;
    }

    public void setNameText(TextView nameText2) {
        this.nameText = nameText2;
    }

    public TextView getTimeText() {
        return this.timeText;
    }

    public void setTimeText(TextView timeText2) {
        this.timeText = timeText2;
    }

    public ImageView getIconImg() {
        return this.iconImg;
    }

    public void setIconImg(ImageView iconImg2) {
        this.iconImg = iconImg2;
    }

    public TextView getContentText() {
        return this.contentText;
    }

    public void setContentText(TextView contentText2) {
        this.contentText = contentText2;
    }
}
