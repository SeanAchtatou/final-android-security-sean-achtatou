package com.mobcent.base.android.ui.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.UserMyInfoActivity;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.android.ui.activity.receiver.MCForumReceiver;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCThemeResource;
import com.mobcent.forum.android.application.McForumApplication;
import com.mobcent.forum.android.service.PermService;
import com.mobcent.forum.android.service.impl.ForumServiceImpl;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseActivity extends Activity implements MCConstant {
    protected String appKey;
    protected AsyncTaskLoaderImage asyncTaskLoaderImage;
    private MCForumReceiver forumReceiver;
    private InputMethodManager imm;
    protected Intent intent;
    protected Handler mHandler;
    protected ProgressDialog myDialog;
    protected PermService permService;
    protected List<String> recycleUrls;
    protected MCResource resource;
    protected MCThemeResource themeResource;

    /* access modifiers changed from: protected */
    public abstract void initData();

    /* access modifiers changed from: protected */
    public abstract void initViews();

    /* access modifiers changed from: protected */
    public abstract void initWidgetActions();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.resource = MCResource.getInstance(this);
        this.themeResource = MCThemeResource.getInstance(this, "packageName");
        this.imm = (InputMethodManager) getSystemService("input_method");
        this.appKey = new ForumServiceImpl().getForumKey(this);
        this.mHandler = new Handler();
        if (MCForumHelper.logoutApp() && McForumApplication.getInstance() != null) {
            McForumApplication.getInstance().addAcitvity(this);
        }
        this.recycleUrls = new ArrayList();
        this.asyncTaskLoaderImage = AsyncTaskLoaderImage.getInstance(getApplicationContext(), toString());
        this.forumReceiver = new MCForumReceiver(this);
        requestWindowFeature(1);
        initData();
        initViews();
        initNavView(this, this.resource, this.themeResource);
        initWidgetActions();
        onTheme();
        MCForumHelper.setAnimation(this, true);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (!MCForumHelper.logoutApp()) {
            return false;
        }
        menu.add(0, 2, 0, this.resource.getStringId("mc_forum_user_logout_app"));
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                McForumApplication.getInstance().finishAllActivity();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.forumReceiver.regBroadcastReceiver();
        if (isAction() && !MCForumHelper.getForumConfig().getApplicationVisible()) {
            this.permService = new PermServiceImpl(this);
            this.permService.getPerm();
            MCForumHelper.getForumConfig().setApplicationVisible(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.forumReceiver.unregBroadcastReceiver();
        if (!isAction()) {
            MCForumHelper.getForumConfig().setApplicationVisible(false);
        }
    }

    private boolean isAction() {
        List<ActivityManager.RunningTaskInfo> tasks = ((ActivityManager) getApplicationContext().getSystemService("activity")).getRunningTasks(1);
        if (tasks == null || tasks.isEmpty() || !tasks.get(0).topActivity.getPackageName().equals(getApplicationContext().getPackageName())) {
            return false;
        }
        return true;
    }

    public void onBackPressed() {
        super.onBackPressed();
        MCForumHelper.setAnimation(this, false);
    }

    public void finish() {
        super.finish();
        MCForumHelper.setAnimation(this, false);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (MCForumHelper.logoutApp() && McForumApplication.getInstance() != null) {
            McForumApplication.getInstance().remove(this);
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void warnMessageById(String warnMessId) {
        Toast.makeText(this, this.resource.getStringId(warnMessId), 0).show();
    }

    /* access modifiers changed from: protected */
    public void warnMessageByStr(String str) {
        Toast.makeText(this, str, 0).show();
    }

    /* access modifiers changed from: protected */
    public void showProgressDialog(String str, final AsyncTask obj) {
        this.myDialog = new ProgressDialog(this);
        this.myDialog.setProgressStyle(0);
        this.myDialog.setTitle(getResources().getString(this.resource.getStringId("mc_forum_dialog_tip")));
        this.myDialog.setMessage(getResources().getString(this.resource.getStringId(str)));
        this.myDialog.setIndeterminate(false);
        this.myDialog.setCancelable(true);
        this.myDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == 4) {
                    BaseActivity.this.hideProgressDialog();
                    if (!(obj == null || obj.getStatus() == AsyncTask.Status.FINISHED)) {
                        obj.cancel(true);
                    }
                    if (obj instanceof UserMyInfoActivity.LoadAsyncTask) {
                        BaseActivity.this.finish();
                    }
                }
                return true;
            }
        });
        this.myDialog.show();
    }

    /* access modifiers changed from: protected */
    public void hideProgressDialog() {
        if (this.myDialog != null) {
            this.myDialog.cancel();
        }
    }

    /* access modifiers changed from: protected */
    public void back() {
        if (this.myDialog == null || !this.myDialog.isShowing()) {
            finish();
        } else {
            hideProgressDialog();
        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            this.imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 2);
        }
    }

    public void showSoftKeyboard() {
        if (getCurrentFocus() != null) {
            this.imm.showSoftInput(getCurrentFocus(), 1);
        }
    }

    public void showSoftKeyboard(View view) {
        view.requestFocus();
        this.imm.showSoftInput(view, 1);
    }

    public void clearNotification() {
        NotificationManager manager = (NotificationManager) getSystemService("notification");
        manager.cancel(1);
        manager.cancel(3);
        manager.cancel(2);
    }

    /* access modifiers changed from: protected */
    public void onTheme() {
        try {
            findViewById(this.resource.getViewId("mc_forum_top_bar_box")).setBackgroundDrawable(this.themeResource.getDrawable("mc_forum_top_bar_bg"));
            findViewById(this.resource.getViewId("mc_forum_back_btn")).setBackgroundDrawable(this.themeResource.getDrawable("mc_forum_top_bar_button1"));
            ((TextView) findViewById(this.resource.getViewId("mc_forum_title_text"))).setTextColor(this.themeResource.getColor("mc_forum_tool_bar_normal_color"));
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void initNavView(Activity activity, MCResource resource2, MCThemeResource themeResource2) {
        MCForumHelper.initNav(activity, resource2, themeResource2);
    }
}
