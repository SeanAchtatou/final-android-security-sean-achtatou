package com.mobcent.base.android.ui.activity.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.UserMyInfoActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.FanOrFollowActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.ImageViewerFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.constant.MCForumConstant;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.RichImageModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.MentionFriendService;
import com.mobcent.forum.android.service.PermService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.MentionFriendServiceImpl;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;

public class MCUserInfoView implements MCConstant {
    protected AsyncTaskLoaderImage asyncTaskLoaderImage;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public long currUserId = this.userService.getLoginUserId();
    /* access modifiers changed from: private */
    public Button followBtn;
    protected Handler mHandler;
    /* access modifiers changed from: private */
    public MentionFriendService mentionFriendService;
    /* access modifiers changed from: private */
    public PermService permService;
    public TextView publishNumText;
    protected MCResource resource;
    /* access modifiers changed from: private */
    public ArrayList<RichImageModel> richImageModelList;
    /* access modifiers changed from: private */
    public UserInfoModel userInfoModel;
    private View userInfoView;
    /* access modifiers changed from: private */
    public UserService userService;

    public TextView getPublishNumText() {
        return this.publishNumText;
    }

    public void setPublishNumText(TextView publishNumText2) {
        this.publishNumText = publishNumText2;
    }

    public MCUserInfoView(Context context2, LayoutInflater layoutInflater, long currUserId2, AsyncTaskLoaderImage asyncTaskLoaderImage2, Handler mHandler2) {
        this.context = context2;
        this.resource = MCResource.getInstance(context2.getApplicationContext());
        this.userInfoView = layoutInflater.inflate(this.resource.getLayoutId("mc_forum_user_info_view"), (ViewGroup) null);
        this.asyncTaskLoaderImage = asyncTaskLoaderImage2;
        this.mHandler = mHandler2;
        this.userService = new UserServiceImpl(context2.getApplicationContext());
        this.mentionFriendService = new MentionFriendServiceImpl(context2.getApplicationContext());
        this.publishNumText = (TextView) this.userInfoView.findViewById(this.resource.getViewId("mc_forum_user_topic_num_text"));
        this.richImageModelList = new ArrayList<>();
        this.permService = new PermServiceImpl(context2);
    }

    public void updateUserInfo(UserInfoModel model) {
        this.userInfoModel = model;
        RichImageModel model1 = new RichImageModel();
        model1.setImageUrl(this.userInfoModel.getIcon().replace("100x100", MCForumConstant.RESOLUTION_ORIGINAL));
        this.richImageModelList.add(model1);
        if (this.userInfoModel != null) {
            ImageView iconImg = (ImageView) this.userInfoView.findViewById(this.resource.getViewId("mc_forum_user_icon_img"));
            TextView nickText = (TextView) this.userInfoView.findViewById(this.resource.getViewId("mc_forum_user_name_text"));
            TextView statusText = (TextView) this.userInfoView.findViewById(this.resource.getViewId("mc_forum_user_status"));
            TextView genderText = (TextView) this.userInfoView.findViewById(this.resource.getViewId("mc_forum_user_gender_text"));
            this.followBtn = (Button) this.userInfoView.findViewById(this.resource.getViewId("mc_forum_add_follow_btn"));
            Button settingBtn = (Button) this.userInfoView.findViewById(this.resource.getViewId("mc_forum_user_info_setting_btn"));
            TextView replyNumText = (TextView) this.userInfoView.findViewById(this.resource.getViewId("mc_forum_user_reply_num_text"));
            TextView followNumText = (TextView) this.userInfoView.findViewById(this.resource.getViewId("mc_forum_follow_text"));
            TextView fanNumText = (TextView) this.userInfoView.findViewById(this.resource.getViewId("mc_forum_user_fan_num_text"));
            LinearLayout levelBox = (LinearLayout) this.userInfoView.findViewById(this.resource.getViewId("mc_froum_user_level_box"));
            levelBox.removeAllViews();
            new MCLevelView(this.context, this.userInfoModel.getLevel(), levelBox, 1);
            updateUserIcon(this.userInfoModel.getIcon(), iconImg);
            TextView roleText = (TextView) this.userInfoView.findViewById(this.resource.getViewId("mc_forum_user_role"));
            if (this.userInfoModel.getRoleNum() == 8) {
                roleText.setVisibility(0);
                roleText.setText(this.resource.getString("mc_forum_role_administrator"));
            } else if (this.userInfoModel.getRoleNum() == 4) {
                roleText.setVisibility(0);
                roleText.setText(this.resource.getString("mc_forum_role_moderator"));
            } else if (this.userInfoModel.getRoleNum() == 16) {
                roleText.setVisibility(0);
                roleText.setText(this.resource.getString("mc_forum_role_super_administrator"));
            } else {
                roleText.setVisibility(8);
            }
            if (this.userInfoModel.getGender() == 1) {
                genderText.setText(this.resource.getStringId("mc_forum_user_gender_man"));
            } else {
                genderText.setText(this.resource.getStringId("mc_forum_user_gender_woman"));
            }
            if (this.userInfoModel.getStatus() == 1) {
                statusText.setText(this.resource.getStringId("mc_forum_user_status_online"));
            } else {
                statusText.setText(this.resource.getStringId("mc_forum_user_status_offline"));
            }
            nickText.setText(this.userInfoModel.getNickname());
            if (this.userInfoModel.getTopicNum() < 0) {
                this.userInfoModel.setTopicNum(0);
            }
            this.publishNumText.setText(this.userInfoModel.getTopicNum() + "\n" + this.resource.getString("mc_forum_user_topic_num_label"));
            if (this.userInfoModel.getUserFriendsNum() < 0) {
                this.userInfoModel.setUserFriendsNum(0);
            }
            followNumText.setText(this.userInfoModel.getUserFriendsNum() + "\n" + this.resource.getString("mc_forum_user_follow"));
            if (this.userInfoModel.getFollowNum() < 0) {
                this.userInfoModel.setFollowNum(0);
            }
            fanNumText.setText(this.userInfoModel.getFollowNum() + "\n" + this.resource.getString("mc_forum_user_fan"));
            if (this.userInfoModel.getReplyPostsNum() < 0) {
                this.userInfoModel.setReplyPostsNum(0);
            }
            replyNumText.setText(this.userInfoModel.getReplyPostsNum() + "\n" + this.resource.getString("mc_forum_user_reply_num_label"));
            this.currUserId = this.userService.getLoginUserId();
            if (this.userInfoModel.getUserId() == this.currUserId) {
                this.followBtn.setVisibility(8);
                settingBtn.setVisibility(0);
            } else {
                this.followBtn.setVisibility(0);
                settingBtn.setVisibility(8);
                if (this.userInfoModel.getIsFollow() == 1) {
                    this.followBtn.setBackgroundResource(this.resource.getDrawableId("mc_forum_personal_icon2"));
                } else {
                    this.followBtn.setBackgroundResource(this.resource.getDrawableId("mc_forum_personal_icon1"));
                }
            }
            iconImg.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (MCUserInfoView.this.userInfoModel != null && !StringUtil.isEmpty(MCUserInfoView.this.userInfoModel.getIcon())) {
                        Intent intent = new Intent(MCUserInfoView.this.context, ImageViewerFragmentActivity.class);
                        intent.putExtra(MCConstant.RICH_IMAGE_LIST, MCUserInfoView.this.richImageModelList);
                        intent.putExtra(MCConstant.IMAGE_URL, MCUserInfoView.this.userInfoModel.getIcon().replace("100x100", MCForumConstant.RESOLUTION_ORIGINAL));
                        MCUserInfoView.this.context.startActivity(intent);
                    }
                }
            });
            settingBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (MCUserInfoView.this.userInfoModel != null) {
                        MCUserInfoView.this.context.startActivity(new Intent(MCUserInfoView.this.context, UserMyInfoActivity.class));
                    }
                }
            });
            this.followBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (MCUserInfoView.this.userInfoModel == null) {
                        return;
                    }
                    if (MCUserInfoView.this.userInfoModel.getIsFollow() == 1) {
                        new UnfollowUserAsyncTask().execute(Long.valueOf(MCUserInfoView.this.currUserId), Long.valueOf(MCUserInfoView.this.userInfoModel.getUserId()));
                        return;
                    }
                    new FollowUserAsyncTask().execute(Long.valueOf(MCUserInfoView.this.currUserId), Long.valueOf(MCUserInfoView.this.userInfoModel.getUserId()));
                }
            });
            replyNumText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (MCUserInfoView.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
                        MCForumHelper.onTopicClick((Activity) MCUserInfoView.this.context, MCUserInfoView.this.resource, v, 0, MCUserInfoView.this.userInfoModel.getUserId(), MCUserInfoView.this.userInfoModel);
                        return;
                    }
                    MCUserInfoView.this.warnMessageByStr(MCUserInfoView.this.resource.getString("mc_forum_permission_cannot_visit_topic"));
                }
            });
            this.publishNumText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (MCUserInfoView.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
                        MCForumHelper.onTopicClick((Activity) MCUserInfoView.this.context, MCUserInfoView.this.resource, v, 1, MCUserInfoView.this.userInfoModel.getUserId(), MCUserInfoView.this.userInfoModel);
                        return;
                    }
                    MCUserInfoView.this.warnMessageByStr(MCUserInfoView.this.resource.getString("mc_forum_permission_cannot_visit_topic"));
                }
            });
            followNumText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(MCUserInfoView.this.context, FanOrFollowActivity.class);
                    intent.putExtra("userId", MCUserInfoView.this.userInfoModel.getUserId());
                    intent.putExtra(MCConstant.FRIEND_TYPE, 2);
                    MCUserInfoView.this.context.startActivity(intent);
                }
            });
            fanNumText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(MCUserInfoView.this.context, FanOrFollowActivity.class);
                    intent.putExtra("userId", MCUserInfoView.this.userInfoModel.getUserId());
                    intent.putExtra(MCConstant.FRIEND_TYPE, 1);
                    MCUserInfoView.this.context.startActivity(intent);
                }
            });
        }
    }

    public void publishView(boolean isVisiable) {
        if (isVisiable) {
            this.publishNumText.setVisibility(0);
        } else {
            this.publishNumText.setVisibility(8);
        }
    }

    public View getUserInfoView() {
        return this.userInfoView;
    }

    public void setUserInfoView(View userInfoView2) {
        this.userInfoView = userInfoView2;
    }

    private void updateUserIcon(String imgUrl, final ImageView imgView) {
        if (SharedPreferencesDB.getInstance(this.context).getPicModeFlag()) {
            imgView.setTag(imgUrl);
            imgView.setBackgroundDrawable(this.resource.getDrawable("mc_forum_head"));
            this.asyncTaskLoaderImage.loadAsync(imgUrl, new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    if (image != null) {
                        MCUserInfoView.this.mHandler.post(new Runnable() {
                            public void run() {
                                if (image != null) {
                                    imgView.setBackgroundDrawable(new BitmapDrawable(image));
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    private class UnfollowUserAsyncTask extends AsyncTask<Long, Void, String> {
        private UnfollowUserAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Long... params) {
            return MCUserInfoView.this.userService.unfollowUser(params[0].longValue(), params[1].longValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (result != null) {
                MCUserInfoView.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(MCUserInfoView.this.context, result));
                return;
            }
            if (MCUserInfoView.this.userInfoModel != null) {
                MCUserInfoView.this.userInfoModel.setIsFollow(0);
            }
            MCUserInfoView.this.warnMessageById("mc_forum_unfollow_succ");
            MCUserInfoView.this.followBtn.setBackgroundResource(MCUserInfoView.this.resource.getDrawableId("mc_forum_personal_icon1"));
            new Thread(new Runnable() {
                public void run() {
                    MCUserInfoView.this.mentionFriendService.deletedLocalForumMentionFriend(MCUserInfoView.this.currUserId, MCUserInfoView.this.userInfoModel);
                }
            }).start();
        }
    }

    private class FollowUserAsyncTask extends AsyncTask<Long, Void, String> {
        private FollowUserAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Long... params) {
            return MCUserInfoView.this.userService.followUser(params[0].longValue(), params[1].longValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (result != null) {
                MCUserInfoView.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(MCUserInfoView.this.context, result));
                return;
            }
            if (MCUserInfoView.this.userInfoModel != null) {
                MCUserInfoView.this.userInfoModel.setIsFollow(1);
            }
            MCUserInfoView.this.warnMessageById("mc_forum_follow_succ");
            MCUserInfoView.this.followBtn.setBackgroundResource(MCUserInfoView.this.resource.getDrawableId("mc_forum_personal_icon2"));
            try {
                new Thread(new Runnable() {
                    public void run() {
                        MCUserInfoView.this.mentionFriendService.addLocalForumMentionFriend(MCUserInfoView.this.currUserId, MCUserInfoView.this.userInfoModel);
                    }
                }).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void warnMessageByStr(String str) {
        Toast.makeText(this.context, str, 0).show();
    }

    /* access modifiers changed from: protected */
    public void warnMessageById(String warnMessId) {
        Toast.makeText(this.context, this.resource.getStringId(warnMessId), 0).show();
    }
}
