package com.mobcent.base.android.constant;

import com.mobcent.forum.android.model.LoginModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class PlatFormResourceConstant {
    public static final Long PLATFORM_QQ_WEIBO = 2L;
    public static final Long PLATFORM_QZONE = 20L;
    public static final Long PLATFORM_RENREN = 4L;
    public static final Long PLATFORM_SINA = 1L;
    private static List<LoginModel> loginModelList = new ArrayList();
    private static HashMap<Long, String> platformInfos = new LinkedHashMap();
    private static PlatFormResourceConstant platformResourceConstant;

    private PlatFormResourceConstant() {
        LoginModel model1 = new LoginModel();
        model1.setId(PLATFORM_QZONE.longValue());
        model1.setIcon("mc_forum_shareto_icon6");
        model1.setName("mc_forum_login_qq");
        loginModelList.add(model1);
        LoginModel model2 = new LoginModel();
        model2.setId(PLATFORM_SINA.longValue());
        model2.setIcon("mc_forum_shareto_icon2");
        model2.setName("mc_forum_login_sina");
        loginModelList.add(model2);
        LoginModel model3 = new LoginModel();
        model3.setId(PLATFORM_QQ_WEIBO.longValue());
        model3.setIcon("mc_forum_shareto_icon1");
        model3.setName("mc_forum_login_qq_weibo");
        loginModelList.add(model3);
        LoginModel model4 = new LoginModel();
        model4.setId(PLATFORM_RENREN.longValue());
        model4.setIcon("mc_forum_shareto_icon4");
        model4.setName("mc_forum_login_renren");
        loginModelList.add(model4);
    }

    public static PlatFormResourceConstant getMCResourceConstant() {
        if (platformResourceConstant == null) {
            platformResourceConstant = new PlatFormResourceConstant();
        }
        return platformResourceConstant;
    }

    public HashMap<Long, String> getPlatformInfos() {
        return platformInfos;
    }

    public List<LoginModel> getLoginModelList() {
        return loginModelList;
    }
}
