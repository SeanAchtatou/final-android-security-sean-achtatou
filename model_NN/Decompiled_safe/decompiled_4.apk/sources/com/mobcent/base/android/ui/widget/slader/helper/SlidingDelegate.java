package com.mobcent.base.android.ui.widget.slader.helper;

import android.view.View;
import android.view.ViewGroup;
import com.mobcent.base.android.ui.widget.slader.SlidingMenu;

public interface SlidingDelegate {
    SlidingMenu getSlidingMenu();

    void setBehindContentView(int i);

    void setBehindContentView(View view);

    void setBehindContentView(View view, ViewGroup.LayoutParams layoutParams);

    void setSlidingActionBarEnabled(boolean z);

    void showContent();

    void showMenu();

    void showSecondaryMenu();

    void toggle();
}
