package com.mobcent.base.android.ui.activity.view;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import com.mobcent.base.android.ui.activity.PublishAnnounceActivity;
import com.mobcent.base.android.ui.activity.PublishPollTopicActivity;
import com.mobcent.base.android.ui.activity.PublishTopicActivity;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.service.impl.UserServiceImpl;

public class MCPublishMenuDialog extends Dialog {
    public static final int HOME_PUBLIC_ANNO = 3;
    public static final int HOME_PUBLIC_POLL = 2;
    public static final int HOME_PUBLIC_TOPIC = 1;
    private LinearLayout annBox;
    /* access modifiers changed from: private */
    public Context context;
    private MCResource mcResource;
    private LinearLayout pollBox;
    private View publishBtn;
    private LinearLayout topicBox;

    public MCPublishMenuDialog(Context context2) {
        super(context2);
        this.context = context2;
    }

    public MCPublishMenuDialog(Context context2, int theme, View mainBtn) {
        super(context2, theme);
        this.context = context2;
        this.publishBtn = mainBtn;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mcResource = MCResource.getInstance(this.context);
        setContentView(this.mcResource.getLayoutId("mc_forum_publish_dialog"));
        this.topicBox = (LinearLayout) findViewById(this.mcResource.getViewId("mc_forum_home_public_topic_box"));
        this.pollBox = (LinearLayout) findViewById(this.mcResource.getViewId("mc_forum_home_public_poll_box"));
        this.annBox = (LinearLayout) findViewById(this.mcResource.getViewId("mc_forum_home_public_anno_box"));
        if (new UserServiceImpl(this.context).currentUserIsAdmin()) {
            this.annBox.setVisibility(0);
        } else {
            this.annBox.setVisibility(8);
        }
        setCanceledOnTouchOutside(true);
        this.topicBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCPublishMenuDialog.this.dismiss();
                if (LoginInterceptor.doInterceptor(MCPublishMenuDialog.this.context, PublishTopicActivity.class, null)) {
                    MCPublishMenuDialog.this.context.startActivity(new Intent(MCPublishMenuDialog.this.context, PublishTopicActivity.class));
                }
            }
        });
        this.pollBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCPublishMenuDialog.this.dismiss();
                if (LoginInterceptor.doInterceptor(MCPublishMenuDialog.this.context, PublishPollTopicActivity.class, null)) {
                    MCPublishMenuDialog.this.context.startActivity(new Intent(MCPublishMenuDialog.this.context, PublishPollTopicActivity.class));
                }
            }
        });
        this.annBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCPublishMenuDialog.this.dismiss();
                if (LoginInterceptor.doInterceptor(MCPublishMenuDialog.this.context, PublishAnnounceActivity.class, null)) {
                    MCPublishMenuDialog.this.context.startActivity(new Intent(MCPublishMenuDialog.this.context, PublishAnnounceActivity.class));
                }
            }
        });
    }

    public void show() {
        super.show();
        if (this.publishBtn != null) {
            this.publishBtn.setSelected(true);
        }
    }

    public void dismiss() {
        super.dismiss();
        if (this.publishBtn != null) {
            this.publishBtn.setSelected(false);
        }
    }
}
