package com.mobcent.base.android.ui.activity.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.PlatformLoginListActivity;
import com.mobcent.base.android.ui.activity.PublishTopicActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.BoardListFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.HomeTopicFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.TopicListFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserHomeFragmentActivity;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCThemeResource;
import com.mobcent.forum.android.model.PostsNoticeModel;
import com.mobcent.forum.android.model.PushMessageModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.os.service.helper.HeartBeatOSServiceHelper;
import com.mobcent.forum.android.os.service.helper.LocationOSServiceHelper;
import com.mobcent.forum.android.os.service.helper.MentionFriendServiceHelper;
import com.mobcent.forum.android.os.service.helper.PopNoticeServiceHelper;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.update.android.os.service.helper.UpdateOSServiceHelper;
import java.util.HashMap;

public class MCForumHelper {
    private static ForumConfig forumConfig;
    public static String notificationIconStr = "msg";

    public static void prepareToLaunchForum(Context context) {
        HeartBeatOSServiceHelper.startHeartBeatService(context, MCResource.getInstance(context).getDrawableId(notificationIconStr));
        MentionFriendServiceHelper.startMentionFriendService(context);
        LocationOSServiceHelper.startLocationOSService(context);
        UpdateOSServiceHelper.startUpdateNoticeService(context, null);
        PopNoticeServiceHelper.startPopNoticeService(context);
    }

    public static void LogoutForum(Context context) {
        MentionFriendServiceHelper.stopMentionFriendService(context);
        LocationOSServiceHelper.stopLocationOSService(context);
        UpdateOSServiceHelper.stopUpdateNoticeService(context);
        PopNoticeServiceHelper.stopPopNoticeService(context);
    }

    public static void launchForum(Context context) {
        context.startActivity(new Intent(context, HomeTopicFragmentActivity.class));
    }

    public static void launchBoardList(Context context) {
        context.startActivity(new Intent(context, BoardListFragmentActivity.class));
    }

    public static void launchBoard(Context context, long boardId, String boardName) {
        Intent intent = new Intent(context, TopicListFragmentActivity.class);
        intent.putExtra("boardId", boardId);
        intent.putExtra("boardName", boardName);
        context.startActivity(intent);
    }

    public static void launchUserHome(Context context) {
        UserService userService = new UserServiceImpl(context);
        boolean isLogin = userService.isLogin();
        long loginUserId = userService.getLoginUserId();
        if (isLogin) {
            Intent intent = new Intent(context, UserHomeFragmentActivity.class);
            intent.putExtra("userId", loginUserId);
            context.startActivity(intent);
            return;
        }
        context.startActivity(new Intent(context, PlatformLoginListActivity.class));
    }

    public static void launchPublishTopic(Context context, MCResource forumResource) {
        if (LoginInterceptor.doInterceptorByDialog(context, forumResource, PublishTopicActivity.class, new HashMap<>())) {
            context.startActivity(new Intent(context, PublishTopicActivity.class));
        }
    }

    public static void initNav(Activity activity, MCResource resource) {
        getForumConfig().initNav(activity, resource);
    }

    public static void initNav(Activity activity, MCResource resource, MCThemeResource themeResource) {
        getForumConfig().initNav(activity, resource, themeResource);
    }

    public static void gotoUserInfo(Activity activity, MCResource resource, long userId) {
        getForumConfig().gotoUserInfo(activity, resource, userId);
    }

    public static void onPostsClick(Activity activity, MCResource resource, View v, TopicModel topicModel, String boardName) {
        getForumConfig().onPostsClick(activity, resource, v, topicModel, boardName);
    }

    public static void onPostsClick(Activity activity, MCResource resource, View v, PostsNoticeModel model) {
        getForumConfig().onPostsClick(activity, resource, v, model);
    }

    public static void onReplyClick(Activity Activity, MCResource resource, View v, PostsNoticeModel model, long pageFrom) {
        getForumConfig().onReplyClick(Activity, resource, v, model, pageFrom);
    }

    public static void onTopicClick(Activity Activity, MCResource resource, View v, int TopicTab, long userId, UserInfoModel infoModel) {
        getForumConfig().onTopicClick(Activity, resource, v, TopicTab, userId, infoModel);
    }

    public static void onMessageClick(Activity Activity) {
        getForumConfig().onMessageClick(Activity);
    }

    public static Intent onBeatClickListener(Context context, int messageTab) {
        return getForumConfig().onBeatClickListener(context, messageTab);
    }

    public static void onLogoutClick(Activity activity) {
        getForumConfig().onLogoutClick(activity);
    }

    public static boolean onNoPicModel(Context activity) {
        return getForumConfig().setNoPicModel(activity);
    }

    public static boolean onUserHomeSurroundTopic(Activity activity) {
        return getForumConfig().onUserHomeSurroundTopic(activity);
    }

    public static boolean onUserHomeFindFriend(Activity activity) {
        return getForumConfig().onUserHomeFindFriend(activity);
    }

    public static boolean setUserHomeRefreshBtn() {
        return getForumConfig().setUserHomeRefreshBtn();
    }

    public static boolean setUserHomePublishView(Activity activity) {
        return getForumConfig().setUserHomePublishText(activity);
    }

    public static boolean onUserHomeBackPressed(Activity activity) {
        return getForumConfig().onUserHomeBackPressed(activity);
    }

    public static boolean setAtReplyMessageFragment(Activity activity) {
        return getForumConfig().setAtReplyMessageFragment(activity);
    }

    public static boolean isCopyright(Activity activity) {
        return getForumConfig().isCopyright(activity);
    }

    public static boolean logoutApp() {
        return getForumConfig().logoutApp();
    }

    public static boolean setFroumManage(Activity activity) {
        return getForumConfig().setFroumManage(activity);
    }

    public static void changeFavoritText(Activity activity, TextView textView) {
        getForumConfig().changeFavoritText(activity, textView);
    }

    public static void setAnimation(Activity activity, boolean isStart) {
        getForumConfig().setAnimation(activity, isStart);
    }

    public static void setBack(Activity activity) {
        getForumConfig().setBack(activity);
    }

    public static Intent setPushMsgIntent(Context activity, PushMessageModel pushMessageModel) {
        return getForumConfig().setPushMsgIntent(activity, pushMessageModel);
    }

    public static Intent setLoginIntent(Context activity) {
        return getForumConfig().setLoginIntent(activity);
    }

    public static ForumConfig getForumConfig() {
        if (forumConfig == null) {
            forumConfig = new DefaultForumConfigImpl();
        }
        return forumConfig;
    }

    public static void setForumConfig(ForumConfig tForumConfig) {
        if (forumConfig == null) {
            forumConfig = tForumConfig;
        }
    }

    public static void resetForumConfig(ForumConfig tForumConfig) {
        forumConfig = tForumConfig;
    }

    public static ForumConfig getCurrForumConfig() {
        return forumConfig;
    }

    public static void setCurrForumConfig(ForumConfig tForumConfig) {
        forumConfig = tForumConfig;
    }
}
