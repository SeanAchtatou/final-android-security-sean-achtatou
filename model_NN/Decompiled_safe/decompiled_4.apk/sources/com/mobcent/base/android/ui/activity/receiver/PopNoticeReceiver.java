package com.mobcent.base.android.ui.activity.receiver;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.PopNoticeModel;
import com.mobcent.forum.android.os.service.PopNoticeOSService;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;

public class PopNoticeReceiver extends BroadcastReceiver implements MCConstant {
    private AlertDialog.Builder alertBuilder = null;
    private int currentVersionCode;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public Intent intent;
    private TextView newVersionText;
    private TextView popNoticeContentText;
    /* access modifiers changed from: private */
    public PopNoticeModel popNoticeModel;
    private TextView popNoticeUrlText;
    private MCResource resource;
    private View view;

    public PopNoticeReceiver(Context context) {
        initView(context);
    }

    private void initView(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.view = this.inflater.inflate(MCResource.getInstance(context).getLayoutId("mc_forum_pop_notice_dialog"), (ViewGroup) null);
        this.resource = MCResource.getInstance(context);
        this.popNoticeContentText = (TextView) this.view.findViewById(this.resource.getViewId("mc_forum_pop_notice_content"));
        this.popNoticeUrlText = (TextView) this.view.findViewById(this.resource.getViewId("mc_forum_pop_notice_url"));
    }

    public void onReceive(Context context, Intent intent2) {
        MCLogUtil.d("PopNoticeReceiver", "" + intent2.getAction());
        this.popNoticeModel = (PopNoticeModel) intent2.getSerializableExtra(PopNoticeOSService.RETURN_POP_NOTICE_MODEL);
        if (this.popNoticeModel != null) {
            if (StringUtil.isEmpty(this.popNoticeModel.getUrl())) {
                this.popNoticeUrlText.setVisibility(8);
            }
            this.popNoticeContentText.setText(this.popNoticeModel.getContent());
            this.popNoticeUrlText.setText(this.popNoticeModel.getUrl());
            if ((context.getPackageName() + PopNoticeOSService.POP_NOTICE_SERVER_MSG).equals(intent2.getAction())) {
                try {
                    this.currentVersionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
                    MCLogUtil.d("popNotice", "currentVersionCode-->" + this.currentVersionCode);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                if (this.popNoticeModel.getType() == 2) {
                    if (this.popNoticeModel.getVersion() > this.currentVersionCode) {
                        showDialog(context);
                    }
                } else if (this.popNoticeModel.getType() != 1) {
                } else {
                    if (this.popNoticeModel.getVersion() == 0) {
                        showDialog(context);
                    } else if (this.popNoticeModel.getVersion() == this.currentVersionCode) {
                        showDialog(context);
                    }
                }
            }
        }
    }

    public void showDialog(final Context context) {
        if (this.alertBuilder == null) {
            this.alertBuilder = new AlertDialog.Builder(context);
            this.alertBuilder.setTitle(MCResource.getInstance(context).getString("mc_forum_pop_notice")).setView(this.view).create();
        }
        if (StringUtil.isEmpty(this.popNoticeModel.getUrl())) {
            this.alertBuilder.setPositiveButton(this.resource.getStringId("mc_forum_dialog_confirm"), (DialogInterface.OnClickListener) null);
        } else {
            this.alertBuilder.setPositiveButton(this.resource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    String url = PopNoticeReceiver.this.popNoticeModel.getUrl();
                    if (!url.contains("http")) {
                        url = "http://" + url;
                    }
                    Intent unused = PopNoticeReceiver.this.intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
                    context.startActivity(PopNoticeReceiver.this.intent);
                }
            }).setNegativeButton(this.resource.getStringId("mc_forum_dialog_cancel"), (DialogInterface.OnClickListener) null);
        }
        this.alertBuilder.show();
    }
}
