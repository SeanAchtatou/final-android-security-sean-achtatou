package com.mobcent.base.android.ui.activity.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import com.mobcent.base.forum.android.util.AudioCache;
import com.mobcent.forum.android.model.SoundModel;
import com.mobcent.forum.android.util.MCLogUtil;

public class MediaService extends Service implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    public static final String CURRENT_MODEL_POSTION = "current_postion";
    public static final String INTENT_TAG = "com_mobcent_media_service";
    public static final String NEED_NET_DOWN = "need_net_down";
    public static final String SERVICE_MODEL = "service_model";
    public static final String SERVICE_STATUS = "service_status";
    public static final String SERVICE_TAG = "service_tag";
    public static final String SERVICE_TYPE = "service_type";
    private final Object _lock = new Object();
    private AudioBroadCastReceiver audioBroadCastReceiver;
    /* access modifiers changed from: private */
    public SoundModel currSoundModel;
    /* access modifiers changed from: private */
    public String currTAG;
    /* access modifiers changed from: private */
    public boolean isPlaying = false;
    private SoundModel lastSoundModel;
    private String lastTAG;
    /* access modifiers changed from: private */
    public MediaPlayer mPlayer = null;
    private MonitorThread monitorThread;
    private boolean needNetDown = false;

    public boolean onError(MediaPlayer mp, int what, int extra) {
        errorMediaPlayer();
        return true;
    }

    public void onCompletion(MediaPlayer mp) {
        synchronized (this._lock) {
            this.currSoundModel.setCurrentPosition(0);
            this.currSoundModel.setPalyStatus(4);
            this.isPlaying = false;
        }
        stopMediaPalyer();
        MCLogUtil.i("MediaService", "onCompletion");
    }

    public void onPrepared(MediaPlayer mp) {
        MCLogUtil.i("MediaService", "onPrepared");
        if (this.mPlayer != null) {
            synchronized (this._lock) {
                this.currSoundModel.setPalyStatus(2);
            }
            sendBoardCast(this.currSoundModel, this.currTAG);
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        initData();
    }

    private void initData() {
        ((TelephonyManager) getSystemService("phone")).listen(new MobliePhoneStateListener(), 32);
        this.audioBroadCastReceiver = new AudioBroadCastReceiver();
        ((AudioManager) getSystemService("audio")).setMode(2);
        this.monitorThread = new MonitorThread();
        this.monitorThread.start();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void initMediaPlayer() {
        /*
            r4 = this;
            android.media.MediaPlayer r1 = r4.mPlayer     // Catch:{ Exception -> 0x0067 }
            if (r1 != 0) goto L_0x0020
            android.media.MediaPlayer r1 = new android.media.MediaPlayer     // Catch:{ Exception -> 0x0067 }
            r1.<init>()     // Catch:{ Exception -> 0x0067 }
            r4.mPlayer = r1     // Catch:{ Exception -> 0x0067 }
            android.media.MediaPlayer r1 = r4.mPlayer     // Catch:{ Exception -> 0x0067 }
            r1.setOnErrorListener(r4)     // Catch:{ Exception -> 0x0067 }
            android.media.MediaPlayer r1 = r4.mPlayer     // Catch:{ Exception -> 0x0067 }
            r1.setOnCompletionListener(r4)     // Catch:{ Exception -> 0x0067 }
            android.media.MediaPlayer r1 = r4.mPlayer     // Catch:{ Exception -> 0x0067 }
            r1.setOnPreparedListener(r4)     // Catch:{ Exception -> 0x0067 }
            android.media.MediaPlayer r1 = r4.mPlayer     // Catch:{ Exception -> 0x0067 }
            r2 = 2
            r1.setAudioStreamType(r2)     // Catch:{ Exception -> 0x0067 }
        L_0x0020:
            android.media.MediaPlayer r1 = r4.mPlayer     // Catch:{ Exception -> 0x0067 }
            r1.reset()     // Catch:{ Exception -> 0x0067 }
            boolean r1 = r4.needNetDown     // Catch:{ Exception -> 0x0067 }
            if (r1 == 0) goto L_0x005b
            android.media.MediaPlayer r1 = r4.mPlayer     // Catch:{ Exception -> 0x0067 }
            com.mobcent.forum.android.model.SoundModel r2 = r4.currSoundModel     // Catch:{ Exception -> 0x0067 }
            java.lang.String r2 = r2.getSoundPath()     // Catch:{ Exception -> 0x0067 }
            java.lang.String r2 = com.mobcent.base.forum.android.util.AudioCache.getPath(r2)     // Catch:{ Exception -> 0x0067 }
            r1.setDataSource(r2)     // Catch:{ Exception -> 0x0067 }
        L_0x0038:
            java.lang.Object r1 = r4._lock     // Catch:{ Exception -> 0x0067 }
            monitor-enter(r1)     // Catch:{ Exception -> 0x0067 }
            com.mobcent.forum.android.model.SoundModel r2 = r4.currSoundModel     // Catch:{ all -> 0x0070 }
            r3 = 0
            r2.setPlayProgress(r3)     // Catch:{ all -> 0x0070 }
            r2 = 1
            r4.isPlaying = r2     // Catch:{ all -> 0x0070 }
            monitor-exit(r1)     // Catch:{ all -> 0x0070 }
            android.media.MediaPlayer r1 = r4.mPlayer     // Catch:{ Exception -> 0x0067 }
            r1.prepare()     // Catch:{ Exception -> 0x0067 }
            android.media.MediaPlayer r1 = r4.mPlayer     // Catch:{ Exception -> 0x0067 }
            com.mobcent.forum.android.model.SoundModel r2 = r4.currSoundModel     // Catch:{ Exception -> 0x0067 }
            int r2 = r2.getCurrentPosition()     // Catch:{ Exception -> 0x0067 }
            r1.seekTo(r2)     // Catch:{ Exception -> 0x0067 }
            android.media.MediaPlayer r1 = r4.mPlayer     // Catch:{ Exception -> 0x0067 }
            r1.start()     // Catch:{ Exception -> 0x0067 }
        L_0x005a:
            return
        L_0x005b:
            android.media.MediaPlayer r1 = r4.mPlayer     // Catch:{ Exception -> 0x0067 }
            com.mobcent.forum.android.model.SoundModel r2 = r4.currSoundModel     // Catch:{ Exception -> 0x0067 }
            java.lang.String r2 = r2.getSoundPath()     // Catch:{ Exception -> 0x0067 }
            r1.setDataSource(r2)     // Catch:{ Exception -> 0x0067 }
            goto L_0x0038
        L_0x0067:
            r1 = move-exception
            r0 = r1
            r4.errorMediaPlayer()
            r0.printStackTrace()
            goto L_0x005a
        L_0x0070:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0070 }
            throw r2     // Catch:{ Exception -> 0x0067 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.base.android.ui.activity.service.MediaService.initMediaPlayer():void");
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        if (intent != null) {
            if (intent.getIntExtra(SERVICE_STATUS, 7) != 6) {
                this.lastSoundModel = this.currSoundModel;
                this.lastTAG = this.currTAG;
                this.currSoundModel = (SoundModel) intent.getSerializableExtra(SERVICE_MODEL);
                this.currTAG = intent.getStringExtra(SERVICE_TAG);
                this.needNetDown = intent.getBooleanExtra(NEED_NET_DOWN, true);
                if (this.currSoundModel.getPalyStatus() == 0) {
                    this.currSoundModel.setPalyStatus(1);
                }
                MCLogUtil.i("MediaService", "onStart status=" + this.currSoundModel.getPalyStatus() + ", position=" + this.currSoundModel.getCurrentPosition());
                if (this.currSoundModel.getPalyStatus() == 2) {
                    pauseMediaPlayer(this.currSoundModel, this.currTAG);
                } else {
                    startMediaPlayer();
                }
            } else if (this.currSoundModel != null) {
                pauseMediaPlayer(this.currSoundModel, this.currTAG);
            }
        }
    }

    private void startMediaPlayer() {
        if (!(this.mPlayer == null || this.lastSoundModel == null)) {
            pauseMediaPlayer(this.lastSoundModel, this.lastTAG);
        }
        if (this.currSoundModel.getPalyStatus() == 1) {
            sendBoardCast(this.currSoundModel, this.currTAG);
        }
        if (this.needNetDown) {
            this.currSoundModel.setPalyStatus(8);
            sendBoardCast(this.currSoundModel, this.currTAG);
            AudioCache.getInstance(this).loadAsync(this.currSoundModel.getSoundPath(), new AudioCache.AudioDownCallback() {
                public void onAudioLoaded(String url, String fileName) {
                    MediaService.this.initMediaPlayer();
                }
            });
            return;
        }
        initMediaPlayer();
    }

    /* access modifiers changed from: private */
    public void osReplayMediaPlayer() {
        if (this.mPlayer != null) {
            try {
                synchronized (this._lock) {
                    this.isPlaying = true;
                }
                this.mPlayer.start();
            } catch (Exception e) {
                e.printStackTrace();
                errorMediaPlayer();
            }
        }
    }

    /* access modifiers changed from: private */
    public void osPauseMediaPlayer() {
        if (this.mPlayer != null) {
            try {
                synchronized (this._lock) {
                    this.isPlaying = false;
                }
                this.mPlayer.pause();
            } catch (Exception e) {
                e.printStackTrace();
                errorMediaPlayer();
            }
        }
    }

    private void stopMediaPalyer() {
        if (this.mPlayer != null) {
            try {
                synchronized (this._lock) {
                    this.isPlaying = false;
                }
                sendBoardCast(this.currSoundModel, this.currTAG);
                this.mPlayer.release();
                this.mPlayer = null;
            } catch (Exception e) {
            }
        }
    }

    private void pauseMediaPlayer(SoundModel soundModel, String tag) {
        MCLogUtil.i("MediaService", "pauseMediaPlayer");
        if (this.mPlayer != null) {
            try {
                synchronized (this._lock) {
                    soundModel.setPalyStatus(3);
                    soundModel.setCurrentPosition(this.mPlayer.getCurrentPosition());
                    this.isPlaying = false;
                }
                this.mPlayer.stop();
                this.mPlayer.release();
                this.mPlayer = null;
                sendBoardCast(soundModel, tag);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void errorMediaPlayer() {
        synchronized (this._lock) {
            this.isPlaying = false;
            this.currSoundModel.setCurrentPosition(0);
            this.currSoundModel.setPalyStatus(5);
        }
        stopMediaPalyer();
        MCLogUtil.i("MediaService", "onError");
    }

    /* access modifiers changed from: private */
    public void sendBoardCast(SoundModel soundModel, String tag) {
        Intent intent = new Intent(INTENT_TAG + getPackageName());
        intent.putExtra(SERVICE_MODEL, soundModel);
        intent.putExtra(SERVICE_TAG, tag);
        sendBroadcast(intent);
    }

    private class MobliePhoneStateListener extends PhoneStateListener {
        private MobliePhoneStateListener() {
        }

        public void onCallStateChanged(int state, String incomingNumber) {
            switch (state) {
                case 0:
                    MediaService.this.osReplayMediaPlayer();
                    return;
                case 1:
                    MediaService.this.osPauseMediaPlayer();
                    return;
                case 2:
                    MediaService.this.osPauseMediaPlayer();
                    return;
                default:
                    return;
            }
        }
    }

    public class AudioBroadCastReceiver extends BroadcastReceiver {
        public AudioBroadCastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
        }
    }

    private class MonitorThread extends Thread {
        private MonitorThread() {
        }

        public void run() {
            while (true) {
                if (MediaService.this.isPlaying && MediaService.this.mPlayer != null && MediaService.this.currSoundModel != null && MediaService.this.currSoundModel.getPalyStatus() == 2) {
                    MediaService.this.currSoundModel.setPlayProgress(MediaService.this.currSoundModel.getPlayProgress() + 1);
                    MCLogUtil.i("MediaService", "getPlayProgress = " + MediaService.this.currSoundModel.getPlayProgress());
                    MediaService.this.sendBoardCast(MediaService.this.currSoundModel, MediaService.this.currTAG);
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
