package com.mobcent.base.android.ui.activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.PostsActivity;
import com.mobcent.base.android.ui.activity.adapter.holder.FeedsAdapterHolder;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.constant.FeedsConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.FeedsModel;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.DateUtil;
import java.util.List;

public class FeedsAdapter extends BaseAdapter implements FeedsConstant {
    private final String TEXT_BLANL = " ";
    private AsyncTaskLoaderImage asyncTaskLoaderImage;
    /* access modifiers changed from: private */
    public Context context;
    private List<FeedsModel> feedsList;
    private LayoutInflater inflater;
    private long loginUserId;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public MCResource resource;

    public FeedsAdapter(Context context2, LayoutInflater inflater2, List<FeedsModel> feedsList2, Handler mHandler2, AsyncTaskLoaderImage asyncTaskLoaderImage2) {
        this.context = context2;
        this.inflater = inflater2;
        this.resource = MCResource.getInstance(context2);
        this.feedsList = feedsList2;
        this.mHandler = mHandler2;
        this.asyncTaskLoaderImage = asyncTaskLoaderImage2;
    }

    public List<FeedsModel> getFeedsList() {
        return this.feedsList;
    }

    public void setFeedsList(List<FeedsModel> feedsList2) {
        this.feedsList = feedsList2;
    }

    public int getCount() {
        return this.feedsList.size();
    }

    public Object getItem(int position) {
        return this.feedsList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        FeedsModel model = this.feedsList.get(position);
        View convertView2 = getFeedConvertView(convertView);
        FeedsAdapterHolder holder = (FeedsAdapterHolder) convertView2.getTag();
        updataContent(holder, model);
        holder.getContentText().setMovementMethod(LinkMovementMethod.getInstance());
        holder.getIconImg().setBackgroundResource(this.resource.getDrawableId("mc_forum_head"));
        updateUserIconImage(model.getIconUrl(), convertView2, holder.getIconImg());
        initFeedActions(holder, model);
        return convertView2;
    }

    private void updateUserIconImage(String imgUrl, View convertView, final ImageView userIconImg) {
        if (SharedPreferencesDB.getInstance(this.context).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(AsyncTaskLoaderImage.formatUrl(imgUrl, "100x100"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    if (image != null) {
                        FeedsAdapter.this.mHandler.post(new Runnable() {
                            public void run() {
                                if (image != null) {
                                    userIconImg.setBackgroundDrawable(new BitmapDrawable(image));
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    private void updataContent(FeedsAdapterHolder holder, FeedsModel model) {
        String time = DateUtil.getFormatShortTime(model.getTime());
        this.loginUserId = new UserServiceImpl(this.context).getLoginUserId();
        holder.getTimeText().setText(time);
        String content = "";
        model.setObjectLength(model.getsName().length());
        if (model.getFtype() == 1) {
            holder.getNameText().setText(model.getsName());
            content = this.context.getResources().getString(this.resource.getStringId("mc_forum_home_publish")) + " " + model.getoName();
            model.setTypeLenth(this.context.getResources().getString(this.resource.getStringId("mc_forum_home_publish")).length());
        } else if (model.getFtype() == 2) {
            holder.getNameText().setText(model.getsName());
            if (this.loginUserId == 0 || this.loginUserId != model.getaId()) {
                content = this.context.getResources().getString(this.resource.getStringId("mc_forum_home_reply")) + " " + model.getoName();
                model.setTypeLenth(this.context.getResources().getString(this.resource.getStringId("mc_forum_home_reply")).length());
            } else {
                content = this.context.getResources().getString(this.resource.getStringId("mc_forum_home_my_reply")) + " " + model.getoName();
                model.setTypeLenth(this.context.getResources().getString(this.resource.getStringId("mc_forum_home_my_reply")).length());
            }
        } else if (model.getFtype() == 3) {
            holder.getNameText().setText(model.getsName());
            if (this.loginUserId == 0 || this.loginUserId != model.getaId()) {
                content = this.context.getResources().getString(this.resource.getStringId("mc_forum_home_mention1")) + " " + model.getoName() + " " + this.context.getResources().getString(this.resource.getStringId("mc_forum_home_mention2"));
            } else {
                content = this.context.getResources().getString(this.resource.getStringId("mc_forum_home_mention1")) + " " + model.getoName() + " " + this.context.getResources().getString(this.resource.getStringId("mc_forum_home_mention3"));
            }
            model.setTypeLenth(this.context.getResources().getString(this.resource.getStringId("mc_forum_home_mention1")).length());
        } else if (model.getFtype() == 4) {
            holder.getNameText().setText(model.getsName());
            content = this.context.getResources().getString(this.resource.getStringId("mc_forum_home_follow")) + " " + model.getoName();
            model.setTypeLenth(this.context.getResources().getString(this.resource.getStringId("mc_forum_home_follow")).length());
        } else if (model.getFtype() == 6) {
            holder.getNameText().setText(model.getsName());
            content = this.context.getResources().getString(this.resource.getStringId("mc_forum_home_favorite")) + " " + model.getoName();
            model.setTypeLenth(this.context.getResources().getString(this.resource.getStringId("mc_forum_home_favorite")).length());
        } else if (model.getFtype() == 7) {
            holder.getNameText().setText(model.getsName());
            if (this.loginUserId == 0 || this.loginUserId != model.getaId()) {
                content = this.context.getString(this.resource.getStringId("mc_forum_home_post")) + " " + model.getoName() + " " + this.context.getString(this.resource.getStringId("mc_forum_home_top"));
                model.setTypeLenth(this.context.getResources().getString(this.resource.getStringId("mc_forum_home_post")).length());
            } else {
                content = this.context.getString(this.resource.getStringId("mc_forum_home_my_post")) + " " + model.getoName() + " " + this.context.getString(this.resource.getStringId("mc_forum_home_top"));
                model.setTypeLenth(this.context.getResources().getString(this.resource.getStringId("mc_forum_home_my_post")).length());
            }
        } else if (model.getFtype() == 8) {
            holder.getNameText().setText(model.getsName());
            if (this.loginUserId == 0 || this.loginUserId != model.getaId()) {
                content = this.context.getString(this.resource.getStringId("mc_forum_home_post")) + " " + model.getoName() + " " + this.context.getString(this.resource.getStringId("mc_forum_home_enssence"));
                model.setTypeLenth(this.context.getResources().getString(this.resource.getStringId("mc_forum_home_post")).length());
            } else {
                content = this.context.getResources().getString(this.resource.getStringId("mc_forum_home_my_post")) + " " + model.getoName() + " " + this.context.getString(this.resource.getStringId("mc_forum_home_enssence"));
                model.setTypeLenth(this.context.getResources().getString(this.resource.getStringId("mc_forum_home_my_post")).length());
            }
        } else if (model.getFtype() == 9) {
            holder.getNameText().setText(model.getsName());
            content = this.context.getResources().getString(this.resource.getStringId("mc_forum_home_register"));
        } else if (model.getFtype() == 10) {
            holder.getNameText().setText(model.getsName());
            content = this.context.getResources().getString(this.resource.getStringId("mc_forum_home_poll_publish")) + " " + model.getoName();
        } else if (model.getFtype() == 11) {
            holder.getNameText().setText(model.getsName());
            if (this.loginUserId == 0 || this.loginUserId != model.getaId()) {
                content = this.context.getResources().getString(this.resource.getStringId("mc_forum_home_poll_reply")) + " " + model.getoName();
            } else {
                content = this.context.getResources().getString(this.resource.getStringId("mc_forum_home_my_poll_reply")) + " " + model.getoName();
            }
        }
        holder.getContentText().setText(content.replaceAll("\n", ""));
    }

    private View getFeedConvertView(View convertView) {
        FeedsAdapterHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_home_feeds_item"), (ViewGroup) null);
            holder = new FeedsAdapterHolder();
            initFeedsAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (FeedsAdapterHolder) convertView.getTag();
        }
        if (holder != null) {
            return convertView;
        }
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_home_feeds_item"), (ViewGroup) null);
        FeedsAdapterHolder holder2 = new FeedsAdapterHolder();
        initFeedsAdapterHolder(convertView2, holder2);
        convertView2.setTag(holder2);
        return convertView2;
    }

    private void initFeedsAdapterHolder(View convertView, FeedsAdapterHolder holder) {
        holder.setIconImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_feeds_icon")));
        holder.setContentText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_trends_content")));
        holder.setNameText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_trends_name")));
        holder.setTimeText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_trends_time")));
        holder.setContentBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_froum_content_box")));
    }

    private void initFeedActions(FeedsAdapterHolder holder, final FeedsModel model) {
        holder.getContentBox().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FeedsAdapter.this.contentActions(model);
            }
        });
        holder.getContentText().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FeedsAdapter.this.contentActions(model);
            }
        });
        holder.getIconImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCForumHelper.gotoUserInfo((Activity) FeedsAdapter.this.context, FeedsAdapter.this.resource, model.getsId());
            }
        });
    }

    /* access modifiers changed from: private */
    public void contentActions(FeedsModel model) {
        if (model.getFtype() == 4) {
            MCForumHelper.gotoUserInfo((Activity) this.context, this.resource, model.getoId());
        } else if (model.getFtype() == 9) {
            MCForumHelper.gotoUserInfo((Activity) this.context, this.resource, model.getsId());
        } else {
            Intent intent = new Intent(this.context, PostsActivity.class);
            intent.putExtra("topicId", model.getoId());
            this.context.startActivity(intent);
        }
    }

    class Clickable extends ClickableSpan implements View.OnClickListener {
        private final View.OnClickListener mListener;

        public Clickable(View.OnClickListener l) {
            this.mListener = l;
        }

        public void onClick(View v) {
            this.mListener.onClick(v);
        }
    }
}
