package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.view.MCMsgChatAudioView;

public class MsgChatUserAdapterHolder {
    private MCMsgChatAudioView audioView;
    private TextView chatTime;
    private TextView content;
    private ImageView imgView;
    private Button userIcon;

    public Button getUserIcon() {
        return this.userIcon;
    }

    public void setUserIcon(Button userIcon2) {
        this.userIcon = userIcon2;
    }

    public TextView getContent() {
        return this.content;
    }

    public void setContent(TextView content2) {
        this.content = content2;
    }

    public TextView getChatTime() {
        return this.chatTime;
    }

    public void setChatTime(TextView chatTime2) {
        this.chatTime = chatTime2;
    }

    public MCMsgChatAudioView getAudioView() {
        return this.audioView;
    }

    public void setAudioView(MCMsgChatAudioView audioView2) {
        this.audioView = audioView2;
    }

    public ImageView getImgView() {
        return this.imgView;
    }

    public void setImgView(ImageView imgView2) {
        this.imgView = imgView2;
    }
}
