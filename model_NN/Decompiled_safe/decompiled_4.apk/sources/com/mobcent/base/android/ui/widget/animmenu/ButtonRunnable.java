package com.mobcent.base.android.ui.widget.animmenu;

import android.os.Handler;

public class ButtonRunnable implements Runnable {
    private volatile int intData;
    private Handler mHandler;

    public ButtonRunnable(int intData2, Handler handler) {
        this.intData = intData2;
        this.mHandler = handler;
    }

    public void run() {
        this.mHandler.sendEmptyMessage(this.intData);
    }
}
