package com.mobcent.base.android.ui.activity.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.List;

public class RecommendUserFragment extends UserFriendsFrament {
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.initViews(inflater, container, savedInstanceState);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public List<UserInfoModel> getUserFriendsList() {
        return this.userService.getRecommendUser(this.userId, this.currentPage, this.pageSize, this.isLocal);
    }
}
