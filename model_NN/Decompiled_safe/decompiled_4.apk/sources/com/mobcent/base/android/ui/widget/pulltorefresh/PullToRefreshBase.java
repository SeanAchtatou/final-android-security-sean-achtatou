package com.mobcent.base.android.ui.widget.pulltorefresh;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import com.mobcent.base.forum.android.util.MCResource;

public abstract class PullToRefreshBase<T extends View> extends LinearLayout {
    public static final int BOTTOM_CACHE_FINISH = 5;
    public static final int BOTTOM_LOAD_FAIL = 4;
    public static final int BOTTOM_LOAD_FINISH = 3;
    public static final int BOTTOM_LOAD_MORE = 0;
    public static final int BOTTOM_NO_DATA = 2;
    public static final int BOTTOM_REFRESH = 1;
    static final float FRICTION = 2.0f;
    static final int MANUAL_REFRESHING = 3;
    public static final int MODE_BOTH = 3;
    public static final int MODE_PULL_DOWN_TO_REFRESH = 1;
    public static final int MODE_PULL_UP_TO_REFRESH = 2;
    static final int PULL_TO_REFRESH = 0;
    static final int REFRESHING = 2;
    static final int RELEASE_TO_REFRESH = 1;
    /* access modifiers changed from: private */
    public int bottomState;
    private String buttonFailLabel;
    private String buttonIsEndLabel;
    private String buttonNodataLabel;
    private String cacheEndLabel;
    private Context context;
    private int currentMode;
    private PullToRefreshBase<T>.SmoothScrollRunnable currentSmoothScrollRunnable;
    private boolean disableScrollingWhileRefreshing = true;
    /* access modifiers changed from: private */
    public PullToRefreshLayout footerLayout;
    /* access modifiers changed from: private */
    public final Handler handler = new Handler();
    private int headerHeight;
    private PullToRefreshLayout headerLayout;
    private float initialMotionY;
    private boolean isBeingDragged = false;
    private boolean isHand;
    private boolean isPullToRefreshEnabled = true;
    private float lastMotionX;
    private float lastMotionY;
    /* access modifiers changed from: private */
    public ImageView mTopImageView;
    private MCResource mcResource;
    private int mode = 1;
    private OnBottomRefreshListener onBottomRefreshListener;
    private OnRefreshListener onRefreshListener;
    private String pullLabel;
    private String pullLabelbottom;
    T refreshableView;
    private String refreshingLabel;
    private String refreshingLabelbottom;
    private String releaseLabel;
    private int state = 0;
    private int touchSlop;

    public interface OnBottomRefreshListener {
        void onRefresh();
    }

    public interface OnLastItemVisibleListener {
        void onLastItemVisible();
    }

    public interface OnRefreshListener {
        void onRefresh();
    }

    /* access modifiers changed from: protected */
    public abstract T createRefreshableView(Context context2, AttributeSet attributeSet);

    /* access modifiers changed from: protected */
    public abstract void gotoTop();

    /* access modifiers changed from: protected */
    public abstract boolean isReadyForPullDown();

    /* access modifiers changed from: protected */
    public abstract boolean onScroll(MotionEvent motionEvent);

    final class SmoothScrollRunnable implements Runnable {
        static final int ANIMATION_DURATION_MS = 500;
        static final int ANIMATION_FPS = 16;
        private boolean continueRunning = true;
        private int currentY = -1;
        private final Handler handler;
        private final Interpolator interpolator;
        private final int scrollFromY;
        private final int scrollToY;
        private long startTime = -1;

        public SmoothScrollRunnable(Handler handler2, int fromY, int toY) {
            this.handler = handler2;
            this.scrollFromY = fromY;
            this.scrollToY = toY;
            this.interpolator = new AccelerateDecelerateInterpolator();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(float, float):float}
          ClspMth{java.lang.Math.max(long, long):long} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(float, float):float}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(long, long):long} */
        public void run() {
            if (this.startTime == -1) {
                this.startTime = System.currentTimeMillis();
            } else {
                this.currentY = this.scrollFromY - Math.round(((float) (this.scrollFromY - this.scrollToY)) * this.interpolator.getInterpolation(((float) Math.max(Math.min(((System.currentTimeMillis() - this.startTime) * 1000) / 500, 1000L), 0L)) / 1000.0f));
                PullToRefreshBase.this.setHeaderScroll(this.currentY);
            }
            if (this.continueRunning && this.scrollToY != this.currentY) {
                this.handler.postDelayed(this, 16);
            }
        }

        public void stop() {
            this.continueRunning = false;
            this.handler.removeCallbacks(this);
        }
    }

    public PullToRefreshBase(Context context2) {
        super(context2);
        init(context2, null);
    }

    public PullToRefreshBase(Context context2, int mode2) {
        super(context2);
        this.mode = mode2;
        init(context2, null);
    }

    public PullToRefreshBase(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        init(context2, attrs);
    }

    public final T getRefreshableView() {
        return this.refreshableView;
    }

    public final boolean isPullToRefreshEnabled() {
        return this.isPullToRefreshEnabled;
    }

    public final boolean isDisableScrollingWhileRefreshing() {
        return this.disableScrollingWhileRefreshing;
    }

    public final boolean isRefreshing() {
        return this.state == 2 || this.state == 3;
    }

    public final void setDisableScrollingWhileRefreshing(boolean disableScrollingWhileRefreshing2) {
        this.disableScrollingWhileRefreshing = disableScrollingWhileRefreshing2;
    }

    public OnBottomRefreshListener getOnBottomRefreshListener() {
        return this.onBottomRefreshListener;
    }

    public void setOnBottomRefreshListener(OnBottomRefreshListener onBottomRefreshListener2) {
        this.onBottomRefreshListener = onBottomRefreshListener2;
    }

    public final void setOnRefreshListener(OnRefreshListener listener) {
        this.onRefreshListener = listener;
    }

    public final void setPullToRefreshEnabled(boolean enable) {
        this.isPullToRefreshEnabled = enable;
    }

    public final void setRefreshing() {
        setRefreshing(true);
    }

    public final void setRefreshing(boolean doScroll) {
        if (!isRefreshing()) {
            setRefreshingInternal(doScroll);
            this.state = 3;
        }
    }

    public final boolean hasPullFromTop() {
        return this.currentMode != 2;
    }

    public boolean isHand() {
        return this.isHand;
    }

    public void setHand(boolean isHand2) {
        this.isHand = isHand2;
    }

    private void init(Context context2, AttributeSet attrs) {
        this.context = context2;
        this.mcResource = MCResource.getInstance(context2);
        this.mode = 3;
        setOrientation(1);
        this.touchSlop = ViewConfiguration.getTouchSlop();
        this.refreshableView = createRefreshableView(context2, attrs);
        addRefreshableView(context2, this.refreshableView);
        this.pullLabel = this.mcResource.getString("mc_forum_drop_dowm");
        this.refreshingLabel = this.mcResource.getString("mc_forum_doing_update");
        this.releaseLabel = this.mcResource.getString("mc_forum_release_update");
        this.pullLabelbottom = this.mcResource.getString("mc_forum_more");
        this.refreshingLabelbottom = this.mcResource.getString("mc_forum_doing_update");
        this.buttonNodataLabel = this.mcResource.getString("mc_forum_no_content");
        this.buttonIsEndLabel = this.mcResource.getString("mc_forum_loaded");
        this.buttonFailLabel = this.mcResource.getString("mc_forum_load_fail");
        this.cacheEndLabel = this.mcResource.getString("mc_forum_cache_load_more");
        if (this.mode == 1 || this.mode == 3) {
            this.headerLayout = new PullToRefreshLayout(context2, 1, this.releaseLabel, this.pullLabel, this.refreshingLabel);
            addView(this.headerLayout, 0, new LinearLayout.LayoutParams(-1, -2));
            measureView(this.headerLayout);
            this.headerHeight = this.headerLayout.getMeasuredHeight();
        }
        if (this.mode == 2 || this.mode == 3) {
            this.footerLayout = new PullToRefreshLayout(context2, 2, "", this.pullLabelbottom, this.refreshingLabelbottom);
            measureView(this.footerLayout);
            this.headerHeight = this.footerLayout.getMeasuredHeight();
            this.footerLayout.setOnClickListener(new clickToRefresh());
        }
        switch (this.mode) {
            case 2:
                setPadding(0, 0, 0, 0);
                break;
            case 3:
                setPadding(0, -this.headerHeight, 0, 0);
                break;
            default:
                setPadding(0, -this.headerHeight, 0, 0);
                break;
        }
        if (this.mode != 3) {
            this.currentMode = this.mode;
        }
        this.bottomState = 0;
    }

    public void onTheme() {
        this.headerLayout.onTheme();
        this.footerLayout.onTheme();
    }

    public void measureView(View child) {
        int childHeightSpec;
        ViewGroup.LayoutParams p = child.getLayoutParams();
        if (p == null) {
            p = new ViewGroup.LayoutParams(-1, -2);
        }
        int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0, p.width);
        int lpHeight = p.height;
        if (lpHeight > 0) {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(lpHeight, 1073741824);
        } else {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        child.measure(childWidthSpec, childHeightSpec);
    }

    public void addContentView(View content) {
        addContentView(content, null);
    }

    public void addContentView(View content, View header) {
        ScrollView mScrollView = (ScrollView) getRefreshableView();
        LinearLayout linearLayout = new LinearLayout(this.context);
        linearLayout.setOrientation(1);
        if (header != null) {
            linearLayout.addView(header);
        }
        linearLayout.addView(content);
        if (this.footerLayout != null) {
            linearLayout.addView(this.footerLayout, new LinearLayout.LayoutParams(-1, -2));
        }
        mScrollView.addView(linearLayout, new LinearLayout.LayoutParams(-1, -1));
        this.footerLayout.resetBottom();
    }

    public final boolean onTouchEvent(MotionEvent event) {
        if (!this.isPullToRefreshEnabled) {
            return false;
        }
        if (isRefreshing() && this.disableScrollingWhileRefreshing) {
            return true;
        }
        if (event.getAction() == 0 && event.getEdgeFlags() != 0) {
            return false;
        }
        switch (event.getAction()) {
            case 0:
                if (isReadyForPull()) {
                    float y = event.getY();
                    this.initialMotionY = y;
                    this.lastMotionY = y;
                    return true;
                }
                break;
            case 1:
            case 3:
                if (this.isBeingDragged) {
                    this.isBeingDragged = false;
                    if (this.state != 1 || this.onRefreshListener == null) {
                        smoothScrollTo(0);
                    } else {
                        setRefreshingInternal(true);
                        setHand(true);
                        this.onRefreshListener.onRefresh();
                        setHand(false);
                    }
                    return true;
                }
                break;
            case 2:
                if (this.isBeingDragged) {
                    this.lastMotionY = event.getY();
                    pullEvent();
                    return true;
                }
                break;
        }
        return false;
    }

    public final boolean onInterceptTouchEvent(MotionEvent event) {
        onScroll(event);
        if (!this.isPullToRefreshEnabled) {
            return false;
        }
        if (isRefreshing() && this.disableScrollingWhileRefreshing) {
            return false;
        }
        int action = event.getAction();
        if (action == 3 || action == 1) {
            this.isBeingDragged = false;
            return false;
        } else if (action != 0 && this.isBeingDragged) {
            return true;
        } else {
            switch (action) {
                case 0:
                    if (isReadyForPull()) {
                        float y = event.getY();
                        this.initialMotionY = y;
                        this.lastMotionY = y;
                        this.lastMotionX = event.getX();
                        this.isBeingDragged = false;
                        break;
                    }
                    break;
                case 2:
                    if (isReadyForPull()) {
                        float y2 = event.getY();
                        float dy = y2 - this.lastMotionY;
                        float yDiff = Math.abs(dy);
                        float xDiff = Math.abs(event.getX() - this.lastMotionX);
                        if (yDiff > ((float) this.touchSlop) && yDiff > xDiff && ((this.mode == 1 || this.mode == 3) && dy >= 1.0E-4f && isReadyForPullDown())) {
                            this.lastMotionY = y2;
                            this.isBeingDragged = true;
                            if (this.mode == 3) {
                                this.currentMode = 1;
                                break;
                            }
                        }
                    }
                    break;
            }
            return this.isBeingDragged;
        }
    }

    /* access modifiers changed from: protected */
    public void addRefreshableView(Context context2, T refreshableView2) {
        addView(refreshableView2, new LinearLayout.LayoutParams(-1, 0, 1.0f));
    }

    /* access modifiers changed from: protected */
    public void resetHeader() {
        this.state = 0;
        this.isBeingDragged = false;
        if (this.headerLayout != null) {
            this.headerLayout.reset(null);
        }
        smoothScrollTo(0);
    }

    /* access modifiers changed from: protected */
    public void setRefreshingInternal(boolean doScroll) {
        this.state = 2;
        if (this.headerLayout != null) {
            this.headerLayout.refreshing();
        }
        if (doScroll) {
            smoothScrollTo(-this.headerHeight);
        }
    }

    /* access modifiers changed from: protected */
    public final void setHeaderScroll(int y) {
        scrollTo(0, y);
    }

    /* access modifiers changed from: protected */
    public final void smoothScrollTo(int y) {
        if (this.currentSmoothScrollRunnable != null) {
            this.currentSmoothScrollRunnable.stop();
        }
        if (getScrollY() != y) {
            this.currentSmoothScrollRunnable = new SmoothScrollRunnable(this.handler, getScrollY(), y);
            this.handler.post(this.currentSmoothScrollRunnable);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private boolean pullEvent() {
        int newHeight;
        int oldHeight = getScrollY();
        switch (this.currentMode) {
            case 2:
                newHeight = Math.round(Math.max(this.initialMotionY - this.lastMotionY, 0.0f) / FRICTION);
                break;
            default:
                newHeight = Math.round(Math.min(this.initialMotionY - this.lastMotionY, 0.0f) / FRICTION);
                break;
        }
        setHeaderScroll(newHeight);
        if (newHeight != 0) {
            if (this.state == 0 && this.headerHeight < Math.abs(newHeight)) {
                this.state = 1;
                switch (this.currentMode) {
                    case 1:
                        this.headerLayout.releaseToRefresh();
                        break;
                }
                return true;
            } else if (this.state == 1 && this.headerHeight >= Math.abs(newHeight)) {
                this.state = 0;
                switch (this.currentMode) {
                    case 1:
                        this.headerLayout.pullToRefresh(true);
                        break;
                }
                return true;
            }
        }
        return oldHeight != newHeight;
    }

    private boolean isReadyForPull() {
        switch (this.mode) {
            case 1:
                return isReadyForPullDown();
            case 2:
            default:
                return false;
            case 3:
                return isReadyForPullDown();
        }
    }

    class clickToRefresh implements View.OnClickListener {
        clickToRefresh() {
        }

        public void onClick(View v) {
            if (PullToRefreshBase.this.bottomState != 3 && PullToRefreshBase.this.bottomState != 5) {
                int unused = PullToRefreshBase.this.bottomState = 1;
                PullToRefreshBase.this.footerLayout.bottomRefreshing();
                PullToRefreshBase.this.onBottomRefresh();
            } else if (PullToRefreshBase.this.bottomState == 5) {
                if (PullToRefreshBase.this.mTopImageView != null) {
                    PullToRefreshBase.this.mTopImageView.performClick();
                }
                PullToRefreshBase.this.handler.postDelayed(new Runnable() {
                    public void run() {
                        PullToRefreshBase.this.onHandRefresh();
                    }
                }, 150);
            }
        }
    }

    public void onHandRefresh() {
        this.footerLayout.hideBottom();
        setRefreshingInternal(true);
        if (this.onRefreshListener != null) {
            setHand(true);
            this.onRefreshListener.onRefresh();
        }
    }

    public void onRefresh() {
        onRefresh(true);
    }

    public void onRefresh(boolean isShowHeader) {
        this.footerLayout.hideBottom();
        setRefreshingInternal(isShowHeader);
        if (this.onRefreshListener != null) {
            setHand(false);
            this.onRefreshListener.onRefresh();
        }
    }

    public final void onRefreshComplete() {
        this.footerLayout.showBottom();
        if (this.state != 0) {
            resetHeader();
        }
    }

    public void onRefreshWithOutListener() {
        this.footerLayout.hideBottom();
        setRefreshingInternal(true);
        if (this.onRefreshListener != null) {
            setHand(false);
        }
    }

    public void onBottomRefreshExt() {
        this.bottomState = 1;
        this.footerLayout.bottomRefreshing();
        onBottomRefresh();
    }

    /* access modifiers changed from: private */
    public void onBottomRefresh() {
        if (this.onBottomRefreshListener != null) {
            this.onBottomRefreshListener.onRefresh();
        }
    }

    public void onBottomRefreshComplete(int status) {
        onBottomRefreshComplete(status, null);
    }

    public void onBottomRefreshComplete(int status, String text) {
        this.footerLayout.resetBottom();
        switch (status) {
            case 0:
                this.bottomState = 0;
                return;
            case 1:
            default:
                this.bottomState = 0;
                return;
            case 2:
                this.bottomState = 2;
                if (text == null || "".equals(text)) {
                    this.footerLayout.changeBottomRefreshLabel(this.buttonNodataLabel);
                    return;
                } else {
                    this.footerLayout.changeBottomRefreshLabel(text);
                    return;
                }
            case 3:
                this.bottomState = 3;
                if (text == null || "".equals(text)) {
                    this.footerLayout.changeBottomRefreshLabel(this.buttonIsEndLabel);
                    return;
                } else {
                    this.footerLayout.changeBottomRefreshLabel(text);
                    return;
                }
            case 4:
                this.bottomState = 4;
                if (text == null || "".equals(text)) {
                    this.footerLayout.changeBottomRefreshLabel(this.buttonFailLabel);
                    return;
                } else {
                    this.footerLayout.changeBottomRefreshLabel(text);
                    return;
                }
            case 5:
                this.bottomState = 5;
                if (text == null || "".equals(text)) {
                    this.footerLayout.changeBottomRefreshLabel(this.cacheEndLabel);
                    return;
                } else {
                    this.footerLayout.changeBottomRefreshLabel(text);
                    return;
                }
        }
    }

    public void setBackToTopView(ImageView mTopImageView2) {
        this.mTopImageView = mTopImageView2;
        mTopImageView2.setOnClickListener(new View.OnClickListener() {
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void onClick(android.view.View r3) {
                /*
                    r2 = this;
                    r1 = 0
                    com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshBase r0 = com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshBase.this
                    T r0 = r0.refreshableView
                    r0.scrollTo(r1, r1)
                    com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshBase r0 = com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshBase.this
                    r0.gotoTop()
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshBase.AnonymousClass1.onClick(android.view.View):void");
            }
        });
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void scrolToTop() {
        /*
            r2 = this;
            r1 = 0
            T r0 = r2.refreshableView
            r0.scrollTo(r1, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshBase.scrolToTop():void");
    }

    public void setLongClickable(boolean longClickable) {
        getRefreshableView().setLongClickable(longClickable);
    }
}
