package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.base.android.constant.EmailResourceConstant;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.constant.PlatFormResourceConstant;
import com.mobcent.base.android.ui.activity.AboutActivity;
import com.mobcent.base.android.ui.activity.ForgetPasswordActivity;
import com.mobcent.base.android.ui.activity.RegActivity;
import com.mobcent.base.android.ui.activity.adapter.PlatformLoginAdapter;
import com.mobcent.base.android.ui.activity.adapter.RegLoginListAdapter;
import com.mobcent.base.android.ui.activity.delegate.RegLoginFinishDelegate;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCTouchUtil;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.MentionFriendService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.MentionFriendServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.MD5Util;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseLoginActivity extends BaseFragmentActivity implements MCConstant, RegLoginFinishDelegate {
    private String TAG = "RegLoginActivity";
    private Button aboutBtn;
    /* access modifiers changed from: private */
    public String changeDefaultPwd = "(*& ^%$";
    /* access modifiers changed from: private */
    public String changedPwd;
    /* access modifiers changed from: private */
    public String defaultPwd = "$%^ &*(";
    /* access modifiers changed from: private */
    public int emailMaxLen = 64;
    private ArrayList<String> emailSelections;
    private TextView forgetPassText;
    /* access modifiers changed from: private */
    public MentionFriendService friendService;
    /* access modifiers changed from: private */
    public GridView gridView;
    /* access modifiers changed from: private */
    public boolean isKeyEnterFromEmail = false;
    /* access modifiers changed from: private */
    public LoginAsyncTask loginAsyncTask;
    private RelativeLayout loginBox;
    /* access modifiers changed from: private */
    public RelativeLayout loginEmailBox;
    /* access modifiers changed from: private */
    public EditText loginEmailEdit;
    /* access modifiers changed from: private */
    public ListView loginEmailList;
    /* access modifiers changed from: private */
    public EditText loginPwdEdit;
    private Button loginSubmitBtn;
    /* access modifiers changed from: private */
    public PlatformLoginAdapter platformLoginAdapter;
    /* access modifiers changed from: private */
    public String pwd;
    /* access modifiers changed from: private */
    public int pwdMinLen = 6;
    private RelativeLayout regLoginBox;
    /* access modifiers changed from: private */
    public RegLoginListAdapter regLoginListAdapter;
    private Button regSubmitBtn;
    private UserInfoModel userInfoModel;
    private List<UserInfoModel> userInfoModels;

    /* access modifiers changed from: protected */
    public abstract void loginSuccEvent();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        RegActivity.setLoginDelegate(this);
        this.regLoginListAdapter = new RegLoginListAdapter(this, new ArrayList(), this.resource);
        UserService userService = new UserServiceImpl(this);
        this.userInfoModel = userService.getCurrUser();
        this.userInfoModels = userService.getAllUsers();
        this.friendService = new MentionFriendServiceImpl(this);
        this.emailSelections = new ArrayList<>();
        if (this.userInfoModels != null && !this.userInfoModels.isEmpty()) {
            for (UserInfoModel userInfoModel2 : this.userInfoModels) {
                this.emailSelections.add(userInfoModel2.getEmail());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        this.loginSubmitBtn = (Button) findViewById(this.resource.getViewId("mc_forum_user_login_submit_btn"));
        this.regSubmitBtn = (Button) findViewById(this.resource.getViewId("mc_forum_user_reg_submit_btn"));
        this.loginEmailEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_user_login_email_edit"));
        this.forgetPassText = (TextView) findViewById(this.resource.getViewId("mc_froum_user_forget_password"));
        MCTouchUtil.createTouchDelegate(this.forgetPassText, 10);
        this.aboutBtn = (Button) findViewById(this.resource.getViewId("mc_forum_about"));
        this.gridView = (GridView) findViewById(this.resource.getViewId("mc_forum_platform_grid"));
        if (!(this.userInfoModel == null || this.userInfoModel.getEmail() == null || this.userInfoModel.getEmail().equals(""))) {
            this.loginEmailEdit.setText(this.userInfoModel.getEmail());
        }
        this.loginPwdEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_user_login_password_edit"));
        if (!(this.userInfoModel == null || this.userInfoModel.getPwd() == null || this.userInfoModel.getPwd().equals(""))) {
            this.loginPwdEdit.setText(this.defaultPwd);
            this.pwd = this.userInfoModel.getPwd();
        }
        this.regLoginBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_user_login_register_login_box"));
        this.loginBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_user_login_box"));
        this.loginEmailBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_user_login_email_box"));
        this.loginEmailList = (ListView) findViewById(this.resource.getViewId("mc_forum_user_login_email_list"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        new Thread() {
            public void run() {
                BaseLoginActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        BaseLoginActivity.this.loginEmailList.setAdapter((ListAdapter) BaseLoginActivity.this.regLoginListAdapter);
                        PlatformLoginAdapter unused = BaseLoginActivity.this.platformLoginAdapter = new PlatformLoginAdapter(BaseLoginActivity.this, PlatFormResourceConstant.getMCResourceConstant().getPlatformInfos());
                        BaseLoginActivity.this.gridView.setAdapter((ListAdapter) BaseLoginActivity.this.platformLoginAdapter);
                    }
                });
            }
        }.start();
        this.loginSubmitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String password;
                String email = BaseLoginActivity.this.loginEmailEdit.getText().toString();
                if (!StringUtil.isEmail(email)) {
                    BaseLoginActivity.this.warnMessageById("mc_forum_user_email_format_error_warn");
                } else if (email.length() > BaseLoginActivity.this.emailMaxLen) {
                    BaseLoginActivity.this.warnMessageById("mc_forum_user_email_length_error_warn");
                } else {
                    String password2 = BaseLoginActivity.this.loginPwdEdit.getText().toString();
                    if (password2.equals(BaseLoginActivity.this.defaultPwd)) {
                        password = BaseLoginActivity.this.pwd;
                    } else if (password2.equals(BaseLoginActivity.this.changeDefaultPwd)) {
                        password = BaseLoginActivity.this.changedPwd;
                    } else if (password2.length() < BaseLoginActivity.this.pwdMinLen || password2.length() > 20) {
                        BaseLoginActivity.this.warnMessageById("mc_forum_user_password_length_error_warn");
                        return;
                    } else if (!StringUtil.isPwdMatchRule(password2)) {
                        BaseLoginActivity.this.warnMessageById("mc_forum_user_password_format_error_warn");
                        return;
                    } else {
                        password = MD5Util.toMD5(password2);
                    }
                    if (BaseLoginActivity.this.loginAsyncTask != null) {
                        BaseLoginActivity.this.loginAsyncTask.cancel(true);
                    }
                    LoginAsyncTask unused = BaseLoginActivity.this.loginAsyncTask = new LoginAsyncTask();
                    BaseLoginActivity.this.loginAsyncTask.execute(email, password);
                }
            }
        });
        this.loginPwdEdit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String loginPwd = BaseLoginActivity.this.loginPwdEdit.getText().toString();
                if (loginPwd.equals(BaseLoginActivity.this.changeDefaultPwd) || loginPwd.equals(BaseLoginActivity.this.defaultPwd)) {
                    BaseLoginActivity.this.loginPwdEdit.setText("");
                    BaseLoginActivity.this.loginEmailBox.setVisibility(8);
                }
            }
        });
        this.loginPwdEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View arg0, boolean arg1) {
                if (arg1) {
                    String loginPwd = BaseLoginActivity.this.loginPwdEdit.getText().toString();
                    if (loginPwd.equals(BaseLoginActivity.this.changeDefaultPwd) || loginPwd.equals(BaseLoginActivity.this.defaultPwd)) {
                        BaseLoginActivity.this.loginPwdEdit.setText("");
                        BaseLoginActivity.this.loginEmailBox.setVisibility(8);
                    }
                }
            }
        });
        this.loginPwdEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66) {
                    if (!BaseLoginActivity.this.isKeyEnterFromEmail) {
                        BaseLoginActivity.this.hideSoftKeyboard();
                    }
                    boolean unused = BaseLoginActivity.this.isKeyEnterFromEmail = false;
                }
                return false;
            }
        });
        this.regSubmitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaseLoginActivity.this.startActivity(new Intent(BaseLoginActivity.this, RegActivity.class));
            }
        });
        this.loginEmailEdit.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String loginEmail = BaseLoginActivity.this.loginEmailEdit.getText().toString();
                int len = loginEmail.length();
                if (len > 0) {
                    BaseLoginActivity.this.notifyData(loginEmail);
                } else if (len == 0) {
                    BaseLoginActivity.this.getUsersByLimit();
                }
            }

            public void afterTextChanged(Editable s) {
            }
        });
        this.loginEmailEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode != 66) {
                    return false;
                }
                BaseLoginActivity.this.loginPwdEdit.requestFocus();
                boolean unused = BaseLoginActivity.this.isKeyEnterFromEmail = true;
                return false;
            }
        });
        this.loginEmailEdit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaseLoginActivity.this.showLoginEmailBox(BaseLoginActivity.this.loginEmailEdit.getText().toString());
            }
        });
        this.loginEmailEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                BaseLoginActivity.this.loginPwdEdit.setText("");
                String loginEmail = BaseLoginActivity.this.loginEmailEdit.getText().toString();
                if (hasFocus) {
                    BaseLoginActivity.this.showLoginEmailBox(loginEmail);
                }
            }
        });
        this.loginEmailList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                BaseLoginActivity.this.loginEmailEdit.setText(BaseLoginActivity.this.regLoginListAdapter.getEmailSections().get(position));
                Editable loginEmail = BaseLoginActivity.this.loginEmailEdit.getText();
                Selection.setSelection(loginEmail, loginEmail.length());
                if (StringUtil.isEmail(BaseLoginActivity.this.loginEmailEdit.getText().toString())) {
                    BaseLoginActivity.this.getPwdByEmail(BaseLoginActivity.this.loginEmailEdit.getText().toString());
                } else {
                    BaseLoginActivity.this.loginPwdEdit.setText("");
                    String unused = BaseLoginActivity.this.changedPwd = "";
                }
                BaseLoginActivity.this.loginEmailBox.setVisibility(8);
            }
        });
        this.regLoginBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaseLoginActivity.this.loginEmailBox.setVisibility(8);
            }
        });
        this.forgetPassText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaseLoginActivity.this.startActivity(new Intent(BaseLoginActivity.this, ForgetPasswordActivity.class));
            }
        });
        this.aboutBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaseLoginActivity.this.startActivity(new Intent(BaseLoginActivity.this, AboutActivity.class));
            }
        });
    }

    /* access modifiers changed from: private */
    public void showLoginEmailBox(String loginEmail) {
        if (StringUtil.isEmail(loginEmail) || loginEmail.length() == 0) {
            getUsersByLimit();
        } else {
            notifyData(loginEmail);
        }
    }

    /* access modifiers changed from: private */
    public void notifyData(String str) {
        this.loginEmailBox.setVisibility(0);
        new ArrayList();
        this.regLoginListAdapter.setEmailSections(EmailResourceConstant.getResourceConstant().convertEmail(str));
        this.regLoginListAdapter.notifyDataSetChanged();
        this.regLoginListAdapter.notifyDataSetInvalidated();
    }

    /* access modifiers changed from: private */
    public void getUsersByLimit() {
        if (this.userInfoModels == null) {
            this.loginEmailBox.setVisibility(8);
            return;
        }
        if (this.emailSelections.size() == 0) {
            this.loginEmailBox.setVisibility(8);
        } else {
            this.loginEmailBox.setVisibility(0);
        }
        this.regLoginListAdapter.setEmailSections(this.emailSelections);
        this.regLoginListAdapter.notifyDataSetChanged();
        this.regLoginListAdapter.notifyDataSetInvalidated();
    }

    /* access modifiers changed from: private */
    public void getPwdByEmail(String str) {
        if (this.userInfoModels != null && !this.userInfoModels.isEmpty()) {
            for (UserInfoModel userInfoModel2 : this.userInfoModels) {
                String email = userInfoModel2.getEmail();
                MCLogUtil.d(this.TAG, "email--->" + email);
                MCLogUtil.d(this.TAG, "str-->" + str);
                if (email.equals(str)) {
                    this.loginPwdEdit.setText(this.changeDefaultPwd);
                    this.changedPwd = userInfoModel2.getPwd();
                    return;
                }
                this.loginPwdEdit.setText("");
                this.changedPwd = "";
            }
        }
    }

    class LoginAsyncTask extends AsyncTask<String, Void, UserInfoModel> {
        LoginAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            BaseLoginActivity.this.showProgressDialog("mc_forum_warn_login", this);
        }

        /* access modifiers changed from: protected */
        public UserInfoModel doInBackground(String... params) {
            UserService userService = new UserServiceImpl(BaseLoginActivity.this);
            if (params[0] == null && params[1] == null) {
                return null;
            }
            return userService.loginUser(params[0], params[1]);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(final UserInfoModel result) {
            BaseLoginActivity.this.hideProgressDialog();
            super.onPostExecute((Object) result);
            if (result == null) {
                return;
            }
            if (result.getErrorCode() == null || "".equals(result.getErrorCode())) {
                BaseLoginActivity.this.clearNotification();
                new Thread() {
                    public void run() {
                        BaseLoginActivity.this.friendService.getFroumMentionFriend(result.getUserId());
                    }
                }.start();
                BaseLoginActivity.this.loginSuccEvent();
                return;
            }
            BaseLoginActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(BaseLoginActivity.this, result.getErrorCode()));
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == 4 && event.getAction() == 0 && event.getRepeatCount() == 0 && !backEventClickListener()) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    private boolean backEventClickListener() {
        if (this.loginEmailBox.getVisibility() != 0 || this.loginBox.getVisibility() != 0) {
            return true;
        }
        this.loginEmailBox.setVisibility(8);
        return false;
    }

    public void activityFinish() {
        loginSuccEvent();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.loginAsyncTask != null) {
            this.loginAsyncTask.cancel(true);
        }
    }
}
