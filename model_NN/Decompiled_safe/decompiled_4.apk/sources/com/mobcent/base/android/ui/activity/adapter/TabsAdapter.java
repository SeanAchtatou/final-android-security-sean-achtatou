package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabWidget;
import java.util.ArrayList;

public class TabsAdapter extends FragmentPagerAdapter implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {
    private final Context mContext;
    private final TabHost mTabHost;
    private final ArrayList<TabInfo> mTabs = new ArrayList<>();
    private final ViewPager mViewPager;

    static final class TabInfo {
        /* access modifiers changed from: private */
        public final Bundle args;
        /* access modifiers changed from: private */
        public final Class<?> clss;
        private final String tag;

        TabInfo(String _tag, Class<?> _class, Bundle _args) {
            this.tag = _tag;
            this.clss = _class;
            this.args = _args;
        }
    }

    static class DummyTabFactory implements TabHost.TabContentFactory {
        private final Context mContext;

        public DummyTabFactory(Context context) {
            this.mContext = context;
        }

        public View createTabContent(String tag) {
            View v = new View(this.mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }
    }

    public TabsAdapter(FragmentActivity activity, TabHost tabHost, ViewPager pager) {
        super(activity.getSupportFragmentManager());
        this.mContext = activity;
        this.mTabHost = tabHost;
        this.mViewPager = pager;
        this.mTabHost.setOnTabChangedListener(this);
        this.mViewPager.setAdapter(this);
        this.mViewPager.setOnPageChangeListener(this);
    }

    public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
        tabSpec.setContent(new DummyTabFactory(this.mContext));
        this.mTabs.add(new TabInfo(tabSpec.getTag(), clss, args));
        this.mTabHost.addTab(tabSpec);
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.mTabs.size();
    }

    public Fragment getItem(int position) {
        TabInfo info = this.mTabs.get(position);
        return Fragment.instantiate(this.mContext, info.clss.getName(), info.args);
    }

    public void onTabChanged(String tabId) {
        this.mViewPager.setCurrentItem(this.mTabHost.getCurrentTab());
    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    public void onPageSelected(int position) {
        TabWidget widget = this.mTabHost.getTabWidget();
        int oldFocusability = widget.getDescendantFocusability();
        widget.setDescendantFocusability(393216);
        this.mTabHost.setCurrentTab(position);
        widget.setDescendantFocusability(oldFocusability);
    }

    public void onPageScrollStateChanged(int state) {
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
    }
}
