package com.mobcent.base.android.ui.activity.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.adapter.holder.AnnounceAdapterHolder;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.AnnounceAsyncTask;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCFaceUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.constant.AnnoConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.AnnoModel;
import com.mobcent.forum.android.model.TopicContentModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AnnounceListAdapter extends BaseAdapter implements AnnoConstant {
    /* access modifiers changed from: private */
    public Activity activity;
    private List<AnnoModel> annoList;
    /* access modifiers changed from: private */
    public AnnounceAsyncTask announceAsyncTask;
    private AsyncTaskLoaderImage asyncTaskLoaderImage;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public MCResource resource;

    public AnnounceListAdapter(Activity activity2, MCResource resource2, List<AnnoModel> annoList2, LayoutInflater inflater2, AsyncTaskLoaderImage asyncTaskLoaderImage2, Handler mHandler2) {
        this.activity = activity2;
        this.annoList = annoList2;
        this.inflater = inflater2;
        this.asyncTaskLoaderImage = asyncTaskLoaderImage2;
        this.mHandler = mHandler2;
        this.resource = resource2;
    }

    public List<AnnoModel> getAnnoList() {
        return this.annoList;
    }

    public void setAnnoList(List<AnnoModel> annoList2) {
        this.annoList = annoList2;
    }

    public int getCount() {
        return getAnnoList().size();
    }

    public Object getItem(int position) {
        return getAnnoList().get(position);
    }

    public long getItemId(int position) {
        return getAnnoList().get(position).getAnnoId();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final AnnoModel annoModel = this.annoList.get(position);
        View convertView2 = getAnnoConvertView(convertView);
        AnnounceAdapterHolder holder = (AnnounceAdapterHolder) convertView2.getTag();
        holder.getUserIconImage().setBackgroundResource(this.resource.getDrawableId("mc_forum_head"));
        updateUserIconImage(annoModel.getIcon(), convertView2, holder.getUserIconImage());
        if (annoModel.getAuthor().length() <= 16) {
            holder.getUserNameText().setText(annoModel.getAuthor());
        } else {
            holder.getUserNameText().setText(annoModel.getAuthor().substring(0, 15) + "...");
        }
        if (!StringUtil.isEmpty(annoModel.getSubject())) {
            holder.getAnnoTitleText().setText(annoModel.getSubject());
        }
        if (annoModel.getAnnoStartDate() != 0) {
            holder.getStartTimeText().setText(new SimpleDateFormat("yyyy-MM-dd").format(new Date(annoModel.getAnnoStartDate())));
        }
        if (annoModel.getAnnoEndDate() != 0) {
            holder.getEndTimeText().setText(new SimpleDateFormat("yyyy-MM-dd").format(new Date(annoModel.getAnnoEndDate())));
        }
        updateAnnoContentView(annoModel.getTopicContentList(), holder.getAnnoContentLayout());
        if (annoModel.getVerify() == 0) {
            holder.getPassBtn().setVisibility(8);
            holder.getNoPassBtn().setVisibility(8);
            holder.getAlreadyText().setVisibility(8);
            holder.getNotPassText().setVisibility(0);
        } else if (annoModel.getVerify() == 1) {
            holder.getPassBtn().setVisibility(8);
            holder.getNoPassBtn().setVisibility(8);
            holder.getAlreadyText().setVisibility(0);
            holder.getNotPassText().setVisibility(8);
        } else if (annoModel.getVerify() == 2) {
            holder.getPassBtn().setVisibility(0);
            holder.getNoPassBtn().setVisibility(0);
            holder.getAlreadyText().setVisibility(8);
            holder.getNotPassText().setVisibility(8);
        }
        holder.getPassBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AnnounceAsyncTask unused = AnnounceListAdapter.this.announceAsyncTask = new AnnounceAsyncTask(AnnounceListAdapter.this.activity, 1, (int) annoModel.getAnnoId());
                AnnounceListAdapter.this.announceAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                    public void executeSuccess() {
                        Toast.makeText(AnnounceListAdapter.this.activity, AnnounceListAdapter.this.resource.getStringId("mc_forum_announce_pass_str"), 1).show();
                        annoModel.setVerify(1);
                        AnnounceListAdapter.this.notifyDataSetChanged();
                    }

                    public void executeFail() {
                    }
                });
                AnnounceListAdapter.this.announceAsyncTask.execute(new Object[0]);
            }
        });
        holder.getNoPassBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AnnounceAsyncTask announceAsyncTask = new AnnounceAsyncTask(AnnounceListAdapter.this.activity, 0, (int) annoModel.getAnnoId());
                announceAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                    public void executeSuccess() {
                        Toast.makeText(AnnounceListAdapter.this.activity, AnnounceListAdapter.this.resource.getStringId("mc_forum_announce_no_pass_str"), 1).show();
                        annoModel.setVerify(0);
                        AnnounceListAdapter.this.notifyDataSetChanged();
                    }

                    public void executeFail() {
                    }
                });
                announceAsyncTask.execute(new Object[0]);
            }
        });
        holder.getUserIconImage().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCForumHelper.gotoUserInfo(AnnounceListAdapter.this.activity, AnnounceListAdapter.this.resource, annoModel.getAuthorId());
            }
        });
        return convertView2;
    }

    private View getAnnoConvertView(View convertView) {
        AnnounceAdapterHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_apply_announce_item"), (ViewGroup) null);
            holder = new AnnounceAdapterHolder();
            initAnnounceAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (AnnounceAdapterHolder) convertView.getTag();
        }
        if (holder != null) {
            return convertView;
        }
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_apply_announce_item"), (ViewGroup) null);
        AnnounceAdapterHolder holder2 = new AnnounceAdapterHolder();
        initAnnounceAdapterHolder(convertView2, holder2);
        convertView2.setTag(holder2);
        return convertView2;
    }

    private void initAnnounceAdapterHolder(View convertView, AnnounceAdapterHolder holder) {
        holder.setUserIconImage((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_user_icon_img")));
        holder.setUserNameText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_user_name_text")));
        holder.setAnnoTitleText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_apply_publish_announce_text")));
        holder.setStartTimeText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_start_time_text")));
        holder.setEndTimeText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_end_time_text")));
        holder.setAnnoContentLayout((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_announce_content_layout")));
        holder.setPassBtn((Button) convertView.findViewById(this.resource.getViewId("mc_forum_announce_pass_btn")));
        holder.setNoPassBtn((Button) convertView.findViewById(this.resource.getViewId("mc_forum_announce_no_pass_btn")));
        holder.setAlreadyText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_announce_already_pass_text")));
        holder.setNotPassText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_announce_not_pass_text")));
    }

    private LinearLayout updateAnnoContentView(List<TopicContentModel> topicContentList, LinearLayout annoContentLayout) {
        if (topicContentList != null && topicContentList.size() > 0) {
            annoContentLayout.setVisibility(0);
            annoContentLayout.removeAllViews();
            int j = topicContentList.size();
            for (int i = 0; i < j; i++) {
                TopicContentModel annoContent = topicContentList.get(i);
                View view = null;
                if (annoContent.getType() == 0) {
                    view = this.inflater.inflate(this.resource.getLayoutId("mc_forum_reposts_topic_text_item"), (ViewGroup) null);
                    TextView annoInfoText = (TextView) view.findViewById(this.resource.getViewId("mc_forum_topic_info_text"));
                    annoInfoText.setText(annoContent.getInfor());
                    MCFaceUtil.setStrToFace(annoInfoText, annoContent.getInfor(), this.activity);
                } else if (annoContent.getType() == 1) {
                    view = this.inflater.inflate(this.resource.getLayoutId("mc_forum_posts_topic_img_item"), (ViewGroup) null);
                    ImageView annoInfoImg = (ImageView) view.findViewById(this.resource.getViewId("mc_forum_topic_info_img"));
                    String imageUrl = annoContent.getBaseUrl() + annoContent.getInfor();
                    if (!StringUtil.isEmpty(annoContent.getInfor())) {
                        updateAnnoContentImage(imageUrl, annoInfoImg);
                    } else {
                        annoInfoImg.setImageDrawable(null);
                    }
                }
                if (view != null) {
                    annoContentLayout.addView(view);
                }
            }
        }
        return annoContentLayout;
    }

    private void updateAnnoContentImage(String imgUrl, final ImageView imageView) {
        if (SharedPreferencesDB.getInstance(this.activity).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(AsyncTaskLoaderImage.formatUrl(imgUrl, "320x480"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    AnnounceListAdapter.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (image != null) {
                                imageView.setImageBitmap(image);
                            }
                        }
                    });
                }
            });
        }
    }

    private void updateUserIconImage(String imgUrl, final View convertView, ImageView userIconImg) {
        if (SharedPreferencesDB.getInstance(this.activity).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(AsyncTaskLoaderImage.formatUrl(imgUrl, "100x100"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    if (image != null) {
                        AnnounceListAdapter.this.mHandler.post(new Runnable() {
                            public void run() {
                                ImageView imageViewByTag = (ImageView) convertView.findViewById(AnnounceListAdapter.this.resource.getViewId("mc_forum_user_icon_img"));
                                if (image != null && !image.isRecycled()) {
                                    imageViewByTag.setBackgroundDrawable(new BitmapDrawable(image));
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    public void destroy() {
        if (this.announceAsyncTask != null) {
            this.announceAsyncTask.cancel(true);
        }
    }
}
