package com.mobcent.base.android.ui.activity.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.HomeTopicListAdapter;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public abstract class HomeTopicFragment extends BaseListViewFragment implements MCConstant {
    protected int currentPage = 1;
    protected boolean isLocal = false;
    protected MoreToticeListTask moreTask;
    protected PullToRefreshListView pullToRefreshListView;
    protected RefreshTopicListTask refreshTask;
    protected List<String> refreshimgUrls;
    protected List<TopicModel> topicList;
    protected HomeTopicListAdapter topicListAdapter;

    public abstract List<TopicModel> getHomeTopicList();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.pullToRefreshListView.onRefreshWithOutListener();
        this.currentPage = 1;
        this.isLocal = true;
        if (!(this.refreshTask == null || this.refreshTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.refreshTask.cancel(true);
        }
        this.permService = new PermServiceImpl(this.activity);
        if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
            this.refreshTask = new RefreshTopicListTask();
            this.refreshTask.execute(Long.valueOf((long) this.currentPage), Long.valueOf((long) this.pageSize));
        } else {
            this.pullToRefreshListView.onRefreshComplete();
            warnMessageByStr(this.mcResource.getString("mc_forum_permission_cannot_visit_forum"));
        }
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.topicList = new ArrayList();
        this.refreshimgUrls = new ArrayList();
        this.permService = new PermServiceImpl(this.activity);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(this.mcResource.getLayoutId("mc_forum_topic_fragment"), container, false);
        this.pullToRefreshListView = (PullToRefreshListView) this.view.findViewById(this.mcResource.getViewId("mc_forum_lv"));
        this.topicListAdapter = new HomeTopicListAdapter(this.activity, this.topicList, null, this.mHandler, inflater, this.pageSize, this.asyncTaskLoaderImage);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.topicListAdapter);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.pullToRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                if (HomeTopicFragment.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
                    HomeTopicFragment.this.onRefreshs();
                    return;
                }
                HomeTopicFragment.this.pullToRefreshListView.onRefreshComplete();
                HomeTopicFragment.this.warnMessageByStr(HomeTopicFragment.this.mcResource.getString("mc_forum_permission_cannot_visit_forum"));
            }
        });
        this.pullToRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                HomeTopicFragment.this.onLoadMore();
            }
        });
        this.pullToRefreshListView.setScrollListener(this.listOnScrollListener);
    }

    public void onDestroy() {
        super.onDestroy();
        if (!(this.refreshTask == null || this.refreshTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.refreshTask.cancel(true);
        }
        if (this.moreTask != null && this.moreTask.getStatus() != AsyncTask.Status.FINISHED) {
            this.moreTask.cancel(true);
        }
    }

    public void onRefreshs() {
        this.isLocal = false;
        this.currentPage = 1;
        if (!(this.refreshTask == null || this.refreshTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.refreshTask.cancel(true);
        }
        if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
            this.refreshTask = new RefreshTopicListTask();
            this.refreshTask.execute(Long.valueOf((long) this.currentPage), Long.valueOf((long) this.pageSize));
            return;
        }
        this.pullToRefreshListView.onRefreshComplete();
        warnMessageByStr(this.mcResource.getString("mc_forum_permission_cannot_visit_forum"));
    }

    public void onLoadMore() {
        this.currentPage++;
        this.isLocal = false;
        if (!(this.moreTask == null || this.moreTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.moreTask.cancel(true);
        }
        this.moreTask = new MoreToticeListTask();
        this.moreTask.execute(Long.valueOf((long) this.currentPage), Long.valueOf((long) this.pageSize));
    }

    class RefreshTopicListTask extends AsyncTask<Long, Void, List<TopicModel>> {
        RefreshTopicListTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<TopicModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            if (HomeTopicFragment.this.refreshimgUrls != null && !HomeTopicFragment.this.refreshimgUrls.isEmpty() && HomeTopicFragment.this.refreshimgUrls.size() > 0) {
                HomeTopicFragment.this.asyncTaskLoaderImage.recycleBitmaps(HomeTopicFragment.this.refreshimgUrls);
            }
        }

        /* access modifiers changed from: protected */
        public List<TopicModel> doInBackground(Long... params) {
            return HomeTopicFragment.this.getHomeTopicList();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<TopicModel> data) {
            super.onPostExecute((Object) data);
            HomeTopicFragment.this.pullToRefreshListView.onRefreshComplete();
            if (data == null) {
                HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(3, HomeTopicFragment.this.getString(HomeTopicFragment.this.mcResource.getStringId("mc_forum_warn_no_such_data")));
            } else if (data.isEmpty()) {
                HomeTopicFragment.this.topicListAdapter.setTopicList(data);
                HomeTopicFragment.this.topicListAdapter.notifyDataSetInvalidated();
                HomeTopicFragment.this.topicListAdapter.notifyDataSetChanged();
                HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(3, HomeTopicFragment.this.getString(HomeTopicFragment.this.mcResource.getStringId("mc_forum_warn_no_such_data")));
            } else if (!StringUtil.isEmpty(data.get(0).getErrorCode())) {
                Toast.makeText(HomeTopicFragment.this.activity, MCForumErrorUtil.convertErrorCode(HomeTopicFragment.this.activity, data.get(0).getErrorCode()), 0).show();
                HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                HomeTopicFragment.this.refreshimgUrls = HomeTopicFragment.this.getRefreshImgUrl(data);
                HomeTopicFragment.this.topicListAdapter.setTopicList(data);
                HomeTopicFragment.this.topicListAdapter.notifyDataSetInvalidated();
                HomeTopicFragment.this.topicListAdapter.notifyDataSetChanged();
                if (data.get(0).getTotalNum() > 0) {
                    if (data.get(0).getTotalNum() > data.size()) {
                        HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(0);
                    } else {
                        HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(3);
                    }
                } else if (data.get(0).getHasNext() == -1) {
                    HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(3, MCStringBundleUtil.resolveString(HomeTopicFragment.this.mcResource.getStringId("mc_forum_last_update"), MCStringBundleUtil.timeToString(HomeTopicFragment.this.activity, new Long(data.get(0).getLastUpdate()).longValue()), HomeTopicFragment.this.activity));
                    HomeTopicFragment.this.pullToRefreshListView.onRefresh();
                } else if (data.get(0).getHasNext() == 1) {
                    HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(3);
                }
                HomeTopicFragment.this.topicList = data;
            }
        }
    }

    class MoreToticeListTask extends AsyncTask<Long, Void, List<TopicModel>> {
        MoreToticeListTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<TopicModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public List<TopicModel> doInBackground(Long... params) {
            return HomeTopicFragment.this.getHomeTopicList();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<TopicModel> data) {
            super.onPostExecute((Object) data);
            if (data == null) {
                HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(3, HomeTopicFragment.this.getString(HomeTopicFragment.this.mcResource.getStringId("mc_forum_warn_no_such_data")));
            } else if (data.isEmpty()) {
                HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(3, HomeTopicFragment.this.getString(HomeTopicFragment.this.mcResource.getStringId("mc_forum_warn_no_such_data")));
            } else if (!StringUtil.isEmpty(data.get(0).getErrorCode())) {
                Toast.makeText(HomeTopicFragment.this.activity, MCForumErrorUtil.convertErrorCode(HomeTopicFragment.this.activity, data.get(0).getErrorCode()), 0).show();
                HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                List<TopicModel> list = new ArrayList<>();
                list.addAll(HomeTopicFragment.this.topicList);
                list.addAll(data);
                HomeTopicFragment.this.topicListAdapter.setTopicList(list);
                HomeTopicFragment.this.topicListAdapter.notifyDataSetInvalidated();
                HomeTopicFragment.this.topicListAdapter.notifyDataSetChanged();
                if (data.get(0).getTotalNum() > 0) {
                    if (data.get(0).getTotalNum() > data.size()) {
                        HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(0);
                    } else {
                        HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(3);
                    }
                } else if (data.get(0).getHasNext() == 1) {
                    HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    HomeTopicFragment.this.pullToRefreshListView.onBottomRefreshComplete(3);
                }
                HomeTopicFragment.this.topicList = list;
                HomeTopicFragment.this.currentPage++;
            }
        }
    }

    /* access modifiers changed from: protected */
    public List<String> getImageURL(int start, int end) {
        List<String> imgUrls = new ArrayList<>();
        for (int i = start; i < end; i++) {
            TopicModel topicModel = this.topicList.get(i);
            if (!StringUtil.isEmpty(topicModel.getPicPath())) {
                imgUrls.add(AsyncTaskLoaderImage.formatUrl(topicModel.getBaseUrl() + topicModel.getPicPath(), "100x100"));
            }
        }
        return imgUrls;
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return getImageURL(0, this.topicList.size());
    }

    /* access modifiers changed from: private */
    public List<String> getRefreshImgUrl(List<TopicModel> result) {
        List<String> list = new ArrayList<>();
        if (this.topicList != null && !this.topicList.isEmpty()) {
            for (int i = 0; i < this.topicList.size(); i++) {
                TopicModel model = this.topicList.get(i);
                if (!isExit(model, result)) {
                    list.add(AsyncTaskLoaderImage.formatUrl(model.getBaseUrl() + model.getPicPath(), "100x100"));
                }
            }
        }
        return list;
    }

    private boolean isExit(TopicModel model, List<TopicModel> result) {
        int j = result.size();
        for (int i = 0; i < j; i++) {
            TopicModel model2 = result.get(i);
            if (!StringUtil.isEmpty(model.getPicPath()) && !StringUtil.isEmpty(model2.getPicPath()) && model.getPicPath().equals(model2.getPicPath())) {
                return true;
            }
        }
        return false;
    }
}
