package com.mobcent.base.android.ui.activity.view;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.util.PhoneUtil;
import java.util.ArrayList;
import java.util.List;

public class MCLevelView {
    private Context context;
    private LinearLayout levelBox;
    private MCResource resource;

    private class MCLevel {
        int crown;
        int moon;
        int star;
        int sun;

        private MCLevel() {
        }
    }

    public MCLevelView(Context context2, int level, LinearLayout levelBox2, int type) {
        this.resource = MCResource.getInstance(context2);
        this.levelBox = levelBox2;
        this.context = context2;
        if (type == 1) {
            updateView(countLevel(level));
        } else {
            updateWrapView(countLevel(level));
        }
    }

    private MCLevel countLevel(int level) {
        MCLevel l = new MCLevel();
        if (level / 64 > 0) {
            l.crown = 1;
        } else {
            l.sun = level / 16;
            int sun_mod = level % 16;
            l.moon = sun_mod / 4;
            l.star = sun_mod % 4;
        }
        return l;
    }

    private void updateView(MCLevel l) {
        List<View> list = new ArrayList<>();
        for (int i = 0; i < l.crown; i++) {
            list.add(getLeveItemView("mc_forum_icons_level_4"));
            this.levelBox.addView(getLeveItemView("mc_forum_icons_level_4"));
        }
        for (int i2 = 0; i2 < l.sun; i2++) {
            this.levelBox.addView(getLeveItemView("mc_forum_icons_level_2"));
            list.add(getLeveItemView("mc_forum_icons_level_2"));
        }
        for (int i3 = 0; i3 < l.moon; i3++) {
            this.levelBox.addView(getLeveItemView("mc_forum_icons_level_3"));
            list.add(getLeveItemView("mc_forum_icons_level_3"));
        }
        for (int i4 = 0; i4 < l.star; i4++) {
            this.levelBox.addView(getLeveItemView("mc_forum_icons_level_1"));
            list.add(getLeveItemView("mc_forum_icons_level_1"));
        }
        if (l.moon + l.star + l.sun > 6) {
            this.levelBox.removeAllViews();
            for (int i5 = 0; i5 < 5; i5++) {
                this.levelBox.addView((View) list.get(i5));
            }
            this.levelBox.addView(getLeveItemView("mc_forum_icons_level_5"));
        }
    }

    private void updateWrapView(MCLevel l) {
        int time;
        List<View> list = new ArrayList<>();
        for (int i = 0; i < l.sun; i++) {
            list.add(getLeveItemView("mc_forum_icons_level_2"));
        }
        for (int i2 = 0; i2 < l.moon; i2++) {
            list.add(getLeveItemView("mc_forum_icons_level_3"));
        }
        for (int i3 = 0; i3 < l.star; i3++) {
            list.add(getLeveItemView("mc_forum_icons_level_1"));
        }
        for (int i4 = 0; i4 < l.crown; i4++) {
            list.add(getLeveItemView("mc_forum_icons_level_4"));
        }
        if (list.size() % 3 == 0) {
            time = list.size() / 3;
        } else {
            time = (list.size() / 3) + 1;
        }
        for (int i5 = 0; i5 < time; i5++) {
            LinearLayout layout = new LinearLayout(this.context);
            int j = i5 * 3;
            while (j < (i5 * 3) + 3 && j < list.size()) {
                layout.addView((View) list.get(j));
                j++;
            }
            this.levelBox.addView(layout);
        }
    }

    private View getLeveItemView(String drawableId) {
        ImageView levelImg = new ImageView(this.context);
        int dip = PhoneUtil.getRawSize(this.context, 1, 20.0f);
        levelImg.setLayoutParams(new LinearLayout.LayoutParams(dip, dip));
        levelImg.setImageResource(this.resource.getDrawableId(drawableId));
        return levelImg;
    }
}
