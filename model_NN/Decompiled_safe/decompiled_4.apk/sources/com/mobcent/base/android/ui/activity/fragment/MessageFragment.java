package com.mobcent.base.android.ui.activity.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.ad.android.ui.widget.manager.AdManager;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.PostsNoticeListAdapter;
import com.mobcent.base.android.ui.activity.service.MediaService;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.model.PostsNoticeModel;
import com.mobcent.forum.android.model.SoundModel;
import com.mobcent.forum.android.model.TopicContentModel;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public abstract class MessageFragment extends BaseListViewFragment implements MCConstant {
    /* access modifiers changed from: private */
    public String TAG = "MessageFragment";
    protected int adBottomPostion;
    protected int adPosition;
    /* access modifiers changed from: private */
    public PostsNoticeListAdapter adapter;
    /* access modifiers changed from: private */
    public PullToRefreshListView mPullRefreshListView;
    private GetMoreNoticeListTask moreTask;
    /* access modifiers changed from: private */
    public List<PostsNoticeModel> noticeList;
    protected int page = 1;
    protected int pageSize = 20;
    PostsNoticeListAdapter.PostsNoticeClickListener postsNoticeClickListener;
    protected PostsService postsService;
    private RefreshNoticeListTask refreshTask;
    private List<String> refreshimgUrls;
    protected String replyIds;
    protected long userId = 0;

    /* access modifiers changed from: protected */
    public abstract List<PostsNoticeModel> getPostsNoticeList();

    /* access modifiers changed from: protected */
    public abstract boolean updateNoitce();

    /* access modifiers changed from: protected */
    public void initData() {
        this.postsService = new PostsServiceImpl(this.activity);
        this.userId = new UserServiceImpl(this.activity).getLoginUserId();
        this.noticeList = new ArrayList();
        this.refreshimgUrls = new ArrayList();
        this.permService = new PermServiceImpl(this.activity);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(this.mcResource.getLayoutId("mc_forum_message_fragment"), (ViewGroup) null);
        this.mPullRefreshListView = (PullToRefreshListView) this.view.findViewById(this.mcResource.getViewId("mc_forum_lv"));
        this.adapter = new PostsNoticeListAdapter(this.activity, this.noticeList, this.mHandler, inflater, this.adPosition, this, this.adBottomPostion, this.page);
        this.mPullRefreshListView.setAdapter((ListAdapter) this.adapter);
        this.mPullRefreshListView.onRefresh(true);
        if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
            onRefreshs();
        } else {
            this.mPullRefreshListView.onRefreshComplete();
            warnMessageByStr(this.mcResource.getString("mc_forum_permission_cannot_visit_forum"));
        }
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.adapter.setPostsNoticeClickListener(this.postsNoticeClickListener);
        this.mPullRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                if (MessageFragment.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
                    MessageFragment.this.onRefreshs();
                    return;
                }
                MessageFragment.this.mPullRefreshListView.onRefreshComplete();
                MessageFragment.this.warnMessageByStr(MessageFragment.this.mcResource.getString("mc_forum_permission_cannot_visit_forum"));
            }
        });
        this.mPullRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                MessageFragment.this.onLoadMore();
            }
        });
        this.mPullRefreshListView.setScrollListener(this.listOnScrollListener);
    }

    public void onDestroy() {
        super.onDestroy();
        if (!(this.refreshTask == null || this.refreshTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.refreshTask.cancel(true);
        }
        if (!(this.moreTask == null || this.moreTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.moreTask.cancel(true);
        }
        AdManager.getInstance().recyclAdByTag(this.TAG);
    }

    public void onRefreshs() {
        this.page = 1;
        if (!(this.refreshTask == null || this.refreshTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.refreshTask.cancel(true);
        }
        this.refreshTask = new RefreshNoticeListTask();
        this.refreshTask.execute(Long.valueOf((long) this.page), Long.valueOf((long) this.pageSize));
    }

    public void onLoadMore() {
        if (!(this.moreTask == null || this.moreTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.moreTask.cancel(true);
        }
        this.moreTask = new GetMoreNoticeListTask();
        this.moreTask.execute(Long.valueOf((long) this.page), Long.valueOf((long) this.pageSize));
    }

    private class RefreshNoticeListTask extends AsyncTask<Long, Void, List<PostsNoticeModel>> {
        private RefreshNoticeListTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<PostsNoticeModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            Intent intent = new Intent(MessageFragment.this.getActivity(), MediaService.class);
            intent.putExtra(MediaService.SERVICE_TAG, MessageFragment.this.toString());
            intent.putExtra(MediaService.SERVICE_STATUS, 6);
            MessageFragment.this.getActivity().startService(intent);
        }

        /* access modifiers changed from: protected */
        public List<PostsNoticeModel> doInBackground(Long... params) {
            return MessageFragment.this.getPostsNoticeList();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<PostsNoticeModel> result) {
            MessageFragment.this.mPullRefreshListView.onRefreshComplete();
            if (result == null) {
                MessageFragment.this.mPullRefreshListView.onBottomRefreshComplete(3, MessageFragment.this.getString(MessageFragment.this.mcResource.getStringId("mc_forum_warn_no_such_data")));
            } else if (result.isEmpty()) {
                MessageFragment.this.adapter.setNoticeList(result);
                MessageFragment.this.adapter.notifyDataSetInvalidated();
                MessageFragment.this.adapter.notifyDataSetChanged();
                MessageFragment.this.mPullRefreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                Toast.makeText(MessageFragment.this.activity, MCForumErrorUtil.convertErrorCode(MessageFragment.this.activity, result.get(0).getErrorCode()), 0).show();
                MessageFragment.this.mPullRefreshListView.onBottomRefreshComplete(0);
            } else {
                AdManager.getInstance().recyclAdByTag(MessageFragment.this.TAG);
                MessageFragment.this.adapter.setNoticeList(result);
                MessageFragment.this.adapter.notifyDataSetInvalidated();
                MessageFragment.this.adapter.notifyDataSetChanged();
                MessageFragment.this.mPullRefreshListView.onRefreshComplete();
                if (result.get(0).isHasNext()) {
                    MessageFragment.this.page++;
                    MessageFragment.this.mPullRefreshListView.onBottomRefreshComplete(0);
                } else {
                    MessageFragment.this.mPullRefreshListView.onBottomRefreshComplete(3);
                }
                new UpdateReplyRemindStateTask().execute(result);
                List unused = MessageFragment.this.noticeList = result;
            }
        }
    }

    private class GetMoreNoticeListTask extends AsyncTask<Long, Void, List<PostsNoticeModel>> {
        private GetMoreNoticeListTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<PostsNoticeModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public List<PostsNoticeModel> doInBackground(Long... params) {
            return MessageFragment.this.getPostsNoticeList();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<PostsNoticeModel> result) {
            MessageFragment.this.mPullRefreshListView.onRefreshComplete();
            if (result != null && result.size() > 0) {
                if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                    MessageFragment.this.mPullRefreshListView.onBottomRefreshComplete(3);
                    Toast.makeText(MessageFragment.this.activity, MCForumErrorUtil.convertErrorCode(MessageFragment.this.activity, result.get(0).getErrorCode()), 0).show();
                    MessageFragment.this.mPullRefreshListView.onBottomRefreshComplete(0);
                    return;
                }
                List<PostsNoticeModel> tempList = new ArrayList<>();
                tempList.addAll(MessageFragment.this.noticeList);
                tempList.addAll(result);
                AdManager.getInstance().recyclAdByTag(MessageFragment.this.TAG);
                MessageFragment.this.adapter.setNoticeList(tempList);
                MessageFragment.this.adapter.notifyDataSetInvalidated();
                MessageFragment.this.adapter.notifyDataSetChanged();
                List unused = MessageFragment.this.noticeList = tempList;
                new UpdateReplyRemindStateTask().execute(result);
                if (result.get(0).isHasNext()) {
                    MessageFragment.this.mPullRefreshListView.onBottomRefreshComplete(0);
                    MessageFragment.this.page++;
                    return;
                }
                MessageFragment.this.mPullRefreshListView.onBottomRefreshComplete(3);
            }
        }
    }

    private class UpdateReplyRemindStateTask extends AsyncTask<List<PostsNoticeModel>, Void, Void> {
        private UpdateReplyRemindStateTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object[] x0) {
            return doInBackground((List<PostsNoticeModel>[]) ((List[]) x0));
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(List<PostsNoticeModel>... params) {
            List<PostsNoticeModel> list = params[0];
            if (list == null) {
                return null;
            }
            MessageFragment.this.replyIds = "";
            for (PostsNoticeModel model : list) {
                if (model.getIsRead() == 0) {
                    MessageFragment.this.replyIds += model.getReplyRemindId() + AdApiConstant.RES_SPLIT_COMMA;
                }
            }
            if (MessageFragment.this.replyIds.length() > 1) {
                MessageFragment.this.replyIds = MessageFragment.this.replyIds.substring(0, MessageFragment.this.replyIds.length() - 1);
            }
            if (!StringUtil.isEmpty(MessageFragment.this.replyIds)) {
                MessageFragment.this.updateNoitce();
            }
            return null;
        }
    }

    public PostsNoticeListAdapter.PostsNoticeClickListener getPostsNoticeClickListener() {
        return this.postsNoticeClickListener;
    }

    public void setPostsNoticeClickListener(PostsNoticeListAdapter.PostsNoticeClickListener postsNoticeClickListener2) {
        this.postsNoticeClickListener = postsNoticeClickListener2;
    }

    public int getAdPosition() {
        return this.adPosition;
    }

    public void setAdPosition(int adPosition2) {
        this.adPosition = adPosition2;
    }

    public int getAdBottomPostion() {
        return this.adBottomPostion;
    }

    public void setAdBottomPostion(int adBottomPostion2) {
        this.adBottomPostion = adBottomPostion2;
    }

    private List<String> getRefreshImgUrl(List<PostsNoticeModel> postsNoticeList) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < postsNoticeList.size(); i++) {
            PostsNoticeModel model = postsNoticeList.get(i);
            isExit(model, this.noticeList);
            isExitInfor(model, this.noticeList);
        }
        return list;
    }

    private boolean isExit(PostsNoticeModel model, List<PostsNoticeModel> result) {
        int j = result.size();
        for (int i = 0; i < j; i++) {
            PostsNoticeModel model2 = result.get(i);
            if (!StringUtil.isEmpty(model.getIcon()) && !StringUtil.isEmpty(model2.getIcon()) && model.getIcon().equals(model2.getIcon())) {
                return true;
            }
        }
        return false;
    }

    private boolean isExitInfor(PostsNoticeModel model, List<PostsNoticeModel> result) {
        int j = result.size();
        for (int i = 0; i < j; i++) {
            PostsNoticeModel model2 = result.get(i);
            if (model.getTopicId() != 0 && model2.getTopicId() != 0 && model.getTopicId() == model2.getTopicId()) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public List<String> getImageURL(int start, int end) {
        List<String> imgUrls = new ArrayList<>();
        for (int i = start; i < end; i++) {
            PostsNoticeModel noticeModel = this.noticeList.get(i);
            List<TopicContentModel> topicContentList = noticeModel.getReplyContentList();
            if (topicContentList != null && !topicContentList.isEmpty()) {
                int n = topicContentList.size();
                for (int k = 0; k < n; k++) {
                    TopicContentModel topicContent = topicContentList.get(k);
                    if (topicContent.getType() == 1 && topicContent.getBaseUrl() != null && !topicContent.getBaseUrl().trim().equals("") && !topicContent.getBaseUrl().trim().equals("null")) {
                        imgUrls.add(AsyncTaskLoaderImage.formatUrl(topicContent.getBaseUrl() + topicContent.getInfor(), "320x480"));
                    }
                }
            }
            if (!StringUtil.isEmpty(noticeModel.getIcon())) {
                imgUrls.add(AsyncTaskLoaderImage.formatUrl(noticeModel.getIconUrl() + noticeModel.getIcon(), "100x100"));
            }
        }
        return imgUrls;
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return getImageURL(0, this.noticeList.size());
    }

    public void updateReceivePlayImg(SoundModel soundModel) {
        this.adapter.updateReceivePlayImg(soundModel);
    }

    public PostsNoticeListAdapter getAdapter() {
        return this.adapter;
    }

    public void setAdapter(PostsNoticeListAdapter adapter2) {
        this.adapter = adapter2;
    }
}
