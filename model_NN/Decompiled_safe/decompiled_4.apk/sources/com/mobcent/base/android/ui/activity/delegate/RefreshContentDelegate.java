package com.mobcent.base.android.ui.activity.delegate;

public interface RefreshContentDelegate {
    void refreshContent();
}
