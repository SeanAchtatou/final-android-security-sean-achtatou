package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AnnounceAdapterHolder {
    private TextView alreadyText;
    private LinearLayout annoContentLayout;
    private TextView annoTitleText;
    private TextView endTimeText;
    private Button noPassBtn;
    private TextView notPassText;
    private Button passBtn;
    private TextView startTimeText;
    private ImageView userIconImage;
    private TextView userNameText;

    public ImageView getUserIconImage() {
        return this.userIconImage;
    }

    public void setUserIconImage(ImageView userIconImage2) {
        this.userIconImage = userIconImage2;
    }

    public TextView getUserNameText() {
        return this.userNameText;
    }

    public void setUserNameText(TextView userNameText2) {
        this.userNameText = userNameText2;
    }

    public TextView getAnnoTitleText() {
        return this.annoTitleText;
    }

    public void setAnnoTitleText(TextView annoTitleText2) {
        this.annoTitleText = annoTitleText2;
    }

    public TextView getStartTimeText() {
        return this.startTimeText;
    }

    public void setStartTimeText(TextView startTimeText2) {
        this.startTimeText = startTimeText2;
    }

    public TextView getEndTimeText() {
        return this.endTimeText;
    }

    public void setEndTimeText(TextView endTimeText2) {
        this.endTimeText = endTimeText2;
    }

    public LinearLayout getAnnoContentLayout() {
        return this.annoContentLayout;
    }

    public void setAnnoContentLayout(LinearLayout annoContentLayout2) {
        this.annoContentLayout = annoContentLayout2;
    }

    public Button getPassBtn() {
        return this.passBtn;
    }

    public void setPassBtn(Button passBtn2) {
        this.passBtn = passBtn2;
    }

    public Button getNoPassBtn() {
        return this.noPassBtn;
    }

    public void setNoPassBtn(Button noPassBtn2) {
        this.noPassBtn = noPassBtn2;
    }

    public TextView getAlreadyText() {
        return this.alreadyText;
    }

    public void setAlreadyText(TextView alreadyText2) {
        this.alreadyText = alreadyText2;
    }

    public TextView getNotPassText() {
        return this.notPassText;
    }

    public void setNotPassText(TextView notPassText2) {
        this.notPassText = notPassText2;
    }
}
