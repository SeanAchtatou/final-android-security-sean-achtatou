package com.mobcent.base.android.ui.widget.gifview;

public interface GifAction {
    void parseFail(boolean z, boolean z2);

    void parseOk(boolean z, int i);
}
