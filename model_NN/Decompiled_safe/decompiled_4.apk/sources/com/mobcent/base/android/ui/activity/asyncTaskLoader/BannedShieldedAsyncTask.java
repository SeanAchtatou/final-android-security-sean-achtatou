package com.mobcent.base.android.ui.activity.asyncTaskLoader;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.service.UserManageService;
import com.mobcent.forum.android.util.StringUtil;
import java.util.Date;
import java.util.List;

public class BannedShieldedAsyncTask extends AsyncTask<Object, Void, String> {
    private Date bannedShieldedDate;
    private long bannedShieldedUserId;
    private int boardId;
    private Context context;
    private long currentUserId;
    private String reason;
    private MCResource resource;
    private List<BoardModel> selectBoardList;
    private TaskExecuteDelegate taskExecuteDelegate;
    private int type;
    private UserManageService userManageService;

    public BannedShieldedAsyncTask(Context context2, MCResource resource2, UserManageService userManageService2, int type2, long currentUserId2, long bannedShieldedUserId2, int boardId2, Date bannedShieldedDate2, String reason2) {
        this.context = context2;
        this.resource = resource2;
        this.userManageService = userManageService2;
        this.type = type2;
        this.currentUserId = currentUserId2;
        this.bannedShieldedUserId = bannedShieldedUserId2;
        this.boardId = boardId2;
        this.bannedShieldedDate = bannedShieldedDate2;
        this.reason = reason2;
    }

    public BannedShieldedAsyncTask(Context context2, MCResource resource2, UserManageService userManageService2, int type2, long currentUserId2, long bannedShieldedUserId2, int boardId2, List<BoardModel> selectBoardList2, Date bannedShieldedDate2, String reason2) {
        this.context = context2;
        this.resource = resource2;
        this.userManageService = userManageService2;
        this.type = type2;
        this.currentUserId = currentUserId2;
        this.bannedShieldedUserId = bannedShieldedUserId2;
        this.boardId = boardId2;
        this.selectBoardList = selectBoardList2;
        this.bannedShieldedDate = bannedShieldedDate2;
        this.reason = reason2;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Object... params) {
        if (this.boardId == -1) {
            return this.userManageService.bannedShielded(this.type, this.currentUserId, this.bannedShieldedUserId, this.selectBoardList, this.bannedShieldedDate, this.reason);
        }
        return this.userManageService.bannedShielded(this.type, this.currentUserId, this.bannedShieldedUserId, this.boardId, this.bannedShieldedDate, this.reason);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String result) {
        if (!StringUtil.isEmpty(result)) {
            Toast.makeText(this.context, MCForumErrorUtil.convertErrorCode(this.context, result), 1).show();
            if (this.taskExecuteDelegate != null) {
                this.taskExecuteDelegate.executeFail();
                return;
            }
            return;
        }
        if (this.type == 4) {
            if (this.selectBoardList.size() > 0) {
                Toast.makeText(this.context, this.resource.getStringId("mc_forum_banned_succ"), 1).show();
            } else {
                Toast.makeText(this.context, this.resource.getStringId("mc_forum_cancel_banned_succ"), 1).show();
            }
        } else if (this.type == 1) {
            if (this.selectBoardList.size() > 0) {
                Toast.makeText(this.context, this.resource.getStringId("mc_forum_shielded_succ"), 1).show();
            } else {
                Toast.makeText(this.context, this.resource.getStringId("mc_forum_cancel_shielded_succ"), 1).show();
            }
        }
        if (this.taskExecuteDelegate != null) {
            this.taskExecuteDelegate.executeSuccess();
        }
    }

    public TaskExecuteDelegate getTaskExecuteDelegate() {
        return this.taskExecuteDelegate;
    }

    public void setTaskExecuteDelegate(TaskExecuteDelegate taskExecuteDelegate2) {
        this.taskExecuteDelegate = taskExecuteDelegate2;
    }
}
