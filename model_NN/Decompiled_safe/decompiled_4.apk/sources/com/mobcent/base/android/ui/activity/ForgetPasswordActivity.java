package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.adapter.ForgetPasswordUserAdapter;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ForgetPasswordActivity extends BaseActivity {
    private static Set<ForgetPasswordActivity> forgetPasswordActivitySets = null;
    /* access modifiers changed from: private */
    public ForgetPasswordUserAdapter adapter;
    private Button backBtn;
    private ListView listView;
    /* access modifiers changed from: private */
    public RelativeLayout loadingBox;
    private TextView nickName;
    /* access modifiers changed from: private */
    public MCProgressBar progressBar;
    private TextView regUserSizeText;
    private RegUserTask regUserTask;
    /* access modifiers changed from: private */
    public List<UserInfoModel> userList;

    /* access modifiers changed from: protected */
    public void initData() {
        this.adapter = new ForgetPasswordUserAdapter(this);
        if (forgetPasswordActivitySets == null) {
            forgetPasswordActivitySets = new HashSet();
        }
        forgetPasswordActivitySets.add(this);
        if (this.regUserTask != null) {
            this.regUserTask.cancel(true);
        }
        this.regUserTask = new RegUserTask();
        this.regUserTask.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_user_forget_password_activity"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.regUserSizeText = (TextView) findViewById(this.resource.getViewId("mc_forum_user_size_text"));
        this.listView = (ListView) findViewById(this.resource.getViewId("mc_forum_lv"));
        this.nickName = (TextView) findViewById(this.resource.getViewId("mc_forum_user_name_text"));
        this.loadingBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_loading_container"));
        this.progressBar = (MCProgressBar) findViewById(this.resource.getViewId("mc_forum_progress_bar"));
    }

    public void updateView(List<UserInfoModel> userinfoList) {
        int userCount;
        if (userinfoList.get(0).getUserId() > 0) {
            userCount = userinfoList.size();
            this.listView.setAdapter((ListAdapter) this.adapter);
            this.nickName.setVisibility(0);
        } else {
            this.nickName.setVisibility(8);
            userCount = 0;
        }
        this.regUserSizeText.setText(MCStringBundleUtil.resolveString(MCResource.getInstance(this).getStringId("mc_forum_phone_user"), userCount + "", this));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ForgetPasswordActivity.this.back();
            }
        });
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                UserInfoModel model = (UserInfoModel) ForgetPasswordActivity.this.userList.get(position);
                if (model.getRegSource() == 0) {
                    Intent intent = new Intent(ForgetPasswordActivity.this, ResetPassowrdActivity.class);
                    intent.putExtra("userId", model.getUserId());
                    intent.putExtra("email", model.getEmail());
                    ForgetPasswordActivity.this.startActivity(intent);
                }
            }
        });
    }

    class RegUserTask extends AsyncTask<Void, Void, List<UserInfoModel>> {
        RegUserTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<UserInfoModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            ForgetPasswordActivity.this.mHandler.post(new Runnable() {
                public void run() {
                    ForgetPasswordActivity.this.loadingBox.setVisibility(0);
                    ForgetPasswordActivity.this.progressBar.show();
                }
            });
        }

        /* access modifiers changed from: protected */
        public List<UserInfoModel> doInBackground(Void... params) {
            return new UserServiceImpl(ForgetPasswordActivity.this).getRegUserList();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<UserInfoModel> result) {
            super.onPostExecute((Object) result);
            ForgetPasswordActivity.this.mHandler.post(new Runnable() {
                public void run() {
                    if (ForgetPasswordActivity.this.loadingBox.getVisibility() == 0) {
                        ForgetPasswordActivity.this.loadingBox.setVisibility(8);
                        ForgetPasswordActivity.this.progressBar.hide();
                    }
                }
            });
            if (result != null && result.size() > 0) {
                ForgetPasswordActivity.this.updateView(result);
                List unused = ForgetPasswordActivity.this.userList = result;
                ForgetPasswordActivity.this.adapter.setUseList(result);
                ForgetPasswordActivity.this.adapter.notifyDataSetChanged();
                ForgetPasswordActivity.this.adapter.notifyDataSetInvalidated();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        forgetPasswordActivitySets.remove(this);
        if (this.regUserTask != null) {
            this.regUserTask.cancel(true);
        }
    }

    public static Set<ForgetPasswordActivity> getForgetPasswordActivitySets() {
        return forgetPasswordActivitySets;
    }

    public static void setForgetPasswordActivitySets(Set<ForgetPasswordActivity> forgetPasswordActivitySets2) {
        forgetPasswordActivitySets = forgetPasswordActivitySets2;
    }
}
