package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.PlatformLoginActivity;
import com.mobcent.base.android.ui.activity.adapter.holder.PlatformLoginListAdapterHolder;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.LoginModel;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class PlatformLoginListAdapter extends BaseAdapter implements MCConstant {
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public HashMap<String, Serializable> goParam;
    /* access modifiers changed from: private */
    public Class<?> goToActivityClass;
    private LayoutInflater inflater;
    private List<LoginModel> loginModelList;
    private MCResource resource;

    public PlatformLoginListAdapter(Context context2, List<LoginModel> loginModelList2, Class<?> goToActivityClass2, HashMap<String, Serializable> goParam2) {
        this.context = context2;
        this.inflater = LayoutInflater.from(context2);
        this.resource = MCResource.getInstance(context2);
        this.loginModelList = loginModelList2;
        this.goToActivityClass = goToActivityClass2;
        this.goParam = goParam2;
    }

    public int getCount() {
        return this.loginModelList.size();
    }

    public Object getItem(int position) {
        return this.loginModelList.get(position);
    }

    public long getItemId(int position) {
        return this.loginModelList.get(position).getId();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        PlatformLoginListAdapterHolder holder;
        final LoginModel loginModel = this.loginModelList.get(position);
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_user_login_list_item"), (ViewGroup) null);
            holder = new PlatformLoginListAdapterHolder();
            holder.setIconImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_login_item_img")));
            holder.setNameText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_login_item_text")));
            convertView.setTag(holder);
        } else {
            holder = (PlatformLoginListAdapterHolder) convertView.getTag();
        }
        holder.getIconImg().setBackgroundResource(this.resource.getDrawableId(loginModel.getIcon()));
        holder.getNameText().setText(this.resource.getString(loginModel.getName()));
        convertView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(PlatformLoginListAdapter.this.context, PlatformLoginActivity.class);
                intent.putExtra("platformId", loginModel.getId());
                if (PlatformLoginListAdapter.this.goToActivityClass != null) {
                    intent.putExtra(MCConstant.TAG, PlatformLoginListAdapter.this.goToActivityClass);
                    intent.putExtra(MCConstant.GO_PARAM, PlatformLoginListAdapter.this.goParam);
                }
                PlatformLoginListAdapter.this.context.startActivity(intent);
            }
        });
        return convertView;
    }
}
