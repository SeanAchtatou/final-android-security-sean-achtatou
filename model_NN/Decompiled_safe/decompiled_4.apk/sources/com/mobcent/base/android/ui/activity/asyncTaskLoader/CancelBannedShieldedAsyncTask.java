package com.mobcent.base.android.ui.activity.asyncTaskLoader;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.service.UserManageService;
import com.mobcent.forum.android.service.impl.UserManageServiceImpl;
import com.mobcent.forum.android.util.StringUtil;

public class CancelBannedShieldedAsyncTask extends AsyncTask<Object, Void, String> {
    private long bannedShieldedUserId;
    private int boardId;
    private Context context;
    private MCResource resource;
    private TaskExecuteDelegate taskExecuteDelegate;
    private int type;
    private UserManageService userManageService;

    public CancelBannedShieldedAsyncTask(Context context2, MCResource resource2, int type2, long bannedShieldedUserId2, int boardId2) {
        this.userManageService = new UserManageServiceImpl(context2);
        this.context = context2;
        this.resource = resource2;
        this.type = type2;
        this.bannedShieldedUserId = bannedShieldedUserId2;
        this.boardId = boardId2;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Object... params) {
        return this.userManageService.cancelBannedShielded(this.type, this.bannedShieldedUserId, this.boardId);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String result) {
        if (!StringUtil.isEmpty(result)) {
            Toast.makeText(this.context, MCForumErrorUtil.convertErrorCode(this.context, result), 1).show();
            if (this.taskExecuteDelegate != null) {
                this.taskExecuteDelegate.executeFail();
                return;
            }
            return;
        }
        if (this.type == 4) {
            Toast.makeText(this.context, this.resource.getStringId("mc_forum_cancel_banned_succ"), 1).show();
        } else if (this.type == 1) {
            Toast.makeText(this.context, this.resource.getStringId("mc_forum_cancel_shielded_succ"), 1).show();
        }
        if (this.taskExecuteDelegate != null) {
            this.taskExecuteDelegate.executeSuccess();
        }
    }

    public TaskExecuteDelegate getTaskExecuteDelegate() {
        return this.taskExecuteDelegate;
    }

    public void setTaskExecuteDelegate(TaskExecuteDelegate taskExecuteDelegate2) {
        this.taskExecuteDelegate = taskExecuteDelegate2;
    }
}
