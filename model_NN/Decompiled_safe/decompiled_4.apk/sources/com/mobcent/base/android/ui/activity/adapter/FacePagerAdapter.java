package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.mobcent.base.forum.android.util.MCResource;
import java.util.LinkedHashMap;
import java.util.List;

public class FacePagerAdapter extends PagerAdapter {
    /* access modifiers changed from: private */
    public Context context;
    private List<LinkedHashMap> faceList;
    private LayoutInflater inflater = LayoutInflater.from(this.context);
    /* access modifiers changed from: private */
    public OnFaceImageClickListener onFaceImageClickListener;
    /* access modifiers changed from: private */
    public MCResource resource = MCResource.getInstance(this.context);

    public interface OnFaceImageClickListener {
        void onFaceImageClick(String str, Drawable drawable);
    }

    public FacePagerAdapter(Context context2, List<LinkedHashMap> faceList2) {
        this.context = context2;
        this.faceList = faceList2;
    }

    public int getCount() {
        return this.faceList.size();
    }

    public Object instantiateItem(ViewGroup container, int position) {
        View convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_publish_pic_face_pager_item"), (ViewGroup) null);
        container.addView(convertView);
        GridView gridView = (GridView) convertView.findViewById(this.resource.getViewId("mc_forum_face_panle"));
        final FaceImageAdapter faceAdapter = new FaceImageAdapter(this.context, this.faceList.get(position));
        gridView.setAdapter((ListAdapter) faceAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String tag = faceAdapter.getItemKey(position);
                Drawable drawable = FacePagerAdapter.this.context.getResources().getDrawable(faceAdapter.getItem(position).intValue());
                drawable.setBounds(0, 0, (int) FacePagerAdapter.this.context.getResources().getDimension(FacePagerAdapter.this.resource.getDimenId("mc_forum_face_width")), (int) FacePagerAdapter.this.context.getResources().getDimension(FacePagerAdapter.this.resource.getDimenId("mc_forum_face_height")));
                FacePagerAdapter.this.onFaceImageClickListener.onFaceImageClick(tag, drawable);
            }
        });
        return convertView;
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public OnFaceImageClickListener getOnFaceImageClickListener() {
        return this.onFaceImageClickListener;
    }

    public void setOnFaceImageClickListener(OnFaceImageClickListener onFaceImageClickListener2) {
        this.onFaceImageClickListener = onFaceImageClickListener2;
    }
}
