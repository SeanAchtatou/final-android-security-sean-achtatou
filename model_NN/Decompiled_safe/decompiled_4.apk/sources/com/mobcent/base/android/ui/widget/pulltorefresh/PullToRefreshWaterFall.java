package com.mobcent.base.android.ui.widget.pulltorefresh;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshBase;
import com.mobcent.base.forum.android.util.MCPhoneUtil;
import com.mobcent.forum.android.model.FlowTag;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class PullToRefreshWaterFall extends PullToRefreshBase<ScrollView> {
    public static final int DRAW_ADD = 1;
    public static final int DRAW_REFRESH = 0;
    private final int FLING_DOWN = 1;
    private final int FLING_UP = 2;
    private int columnCount = 3;
    private List<LinearLayout> columnLayoutList;
    private int columnSpace = 5;
    private int columnWidth;
    private Context context;
    private int currentFlingState = 1;
    private int currentPosition;
    private final PullToRefreshBase.OnRefreshListener defaultOnRefreshListener = new PullToRefreshBase.OnRefreshListener() {
        public void onRefresh() {
            PullToRefreshWaterFall.this.onRefreshComplete();
        }
    };
    private int expandHeight = 480;
    /* access modifiers changed from: private */
    public List<FlowTag> flowTagList;
    /* access modifiers changed from: private */
    public boolean isFlingFinished;
    /* access modifiers changed from: private */
    public boolean isLoadOrRecycle;
    private boolean isLoaded = false;
    private Handler mHandler;
    private Timer mTimer;
    private int marginTop;
    /* access modifiers changed from: private */
    public int oldY = 0;
    /* access modifiers changed from: private */
    public OnLoadItemListener onLoadItemListener;
    /* access modifiers changed from: private */
    public int preYForScrollViewStateCompare;
    private int rowSpace = 5;

    public interface OnLoadItemListener {
        View getImgBox(FlowTag flowTag);

        void loadImage(String str, boolean z);

        void loadLayout(LinearLayout linearLayout, FlowTag flowTag);

        void onItemClick(int i, FlowTag flowTag);

        void recycleImage(String str);
    }

    public void setOnLoadItemListener(OnLoadItemListener onLoadItemListener2) {
        this.onLoadItemListener = onLoadItemListener2;
    }

    public int getColumnCount() {
        return this.columnCount;
    }

    public void setColumnCount(int columnCount2) {
        this.columnCount = columnCount2;
    }

    public int getColumnSpace() {
        return this.columnSpace;
    }

    public void setColumnSpace(int columnSpace2) {
        this.columnSpace = columnSpace2;
    }

    public int getRowSpace() {
        return this.rowSpace;
    }

    public void setRowSpace(int rowSpace2) {
        this.rowSpace = rowSpace2;
    }

    public void setColumnLayoutList(List<LinearLayout> columnLayoutList2) {
        this.columnLayoutList = columnLayoutList2;
    }

    public void setExpandHeight(int expandHeight2) {
        this.expandHeight = expandHeight2;
    }

    public PullToRefreshWaterFall(Context context2) {
        super(context2);
        setOnRefreshListener(this.defaultOnRefreshListener);
    }

    public PullToRefreshWaterFall(Context context2, int mode) {
        super(context2, mode);
        setOnRefreshListener(this.defaultOnRefreshListener);
    }

    public PullToRefreshWaterFall(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        setOnRefreshListener(this.defaultOnRefreshListener);
    }

    private void initData() {
        this.flowTagList = new ArrayList();
        this.mHandler = new Handler();
        this.expandHeight = MCPhoneUtil.getRawSize(this.context, 1, (float) this.expandHeight);
    }

    public void initView(Context context2, View header) {
        this.context = context2;
        initData();
        int columnSpacePx = MCPhoneUtil.getRawSize(context2, 1, (float) this.columnSpace);
        this.columnWidth = (getDisplay(context2).getWidth() - ((this.columnCount + 1) * columnSpacePx)) / this.columnCount;
        this.marginTop = MCPhoneUtil.getRawSize(context2, 1, (float) this.rowSpace);
        LinearLayout rootLayout = new LinearLayout(context2);
        LinearLayout.LayoutParams rootLp = new LinearLayout.LayoutParams(-1, -2);
        rootLayout.setOrientation(0);
        rootLayout.setLayoutParams(rootLp);
        this.columnLayoutList = new ArrayList();
        for (int i = 0; i < this.columnCount; i++) {
            LinearLayout columnLayout = new LinearLayout(context2);
            LinearLayout.LayoutParams columnLp = new LinearLayout.LayoutParams(this.columnWidth, -2);
            columnLp.setMargins(columnSpacePx, 0, 0, 0);
            columnLayout.setOrientation(1);
            columnLayout.setLayoutParams(columnLp);
            rootLayout.addView(columnLayout);
            columnLayout.setTag(Integer.valueOf(this.marginTop));
            this.columnLayoutList.add(columnLayout);
        }
        if (header != null) {
            this.expandHeight += header.getMeasuredHeight();
        }
        addContentView(rootLayout, header);
        this.mTimer = new Timer();
        this.mTimer.schedule(new TimerTask() {
            public void run() {
                int nowY = ((ScrollView) PullToRefreshWaterFall.this.refreshableView).getScrollY();
                if (PullToRefreshWaterFall.this.oldY == nowY) {
                    boolean unused = PullToRefreshWaterFall.this.isFlingFinished = true;
                } else {
                    boolean unused2 = PullToRefreshWaterFall.this.isFlingFinished = false;
                    int unused3 = PullToRefreshWaterFall.this.oldY = nowY;
                }
                if (PullToRefreshWaterFall.this.isFlingFinished) {
                    if (PullToRefreshWaterFall.this.isLoadOrRecycle) {
                        int unused4 = PullToRefreshWaterFall.this.preYForScrollViewStateCompare = nowY;
                        int size = PullToRefreshWaterFall.this.flowTagList.size();
                        for (int i = 0; i < size; i++) {
                            PullToRefreshWaterFall.this.checkVisible((FlowTag) PullToRefreshWaterFall.this.flowTagList.get(i));
                        }
                    }
                    boolean unused5 = PullToRefreshWaterFall.this.isLoadOrRecycle = false;
                    return;
                }
                boolean unused6 = PullToRefreshWaterFall.this.isLoadOrRecycle = true;
            }
        }, 0, 200);
    }

    public void initView(Context context2) {
        initView(context2, null);
    }

    public void onDrawWaterFall(List modelList, int type) {
        List<FlowTag> flowTagListTemp = new ArrayList<>();
        for (int i = 0; i < modelList.size(); i++) {
            flowTagListTemp.add((FlowTag) modelList.get(i));
        }
        switch (type) {
            case 0:
                this.preYForScrollViewStateCompare = 0;
                for (int i2 = 0; i2 < this.columnLayoutList.size(); i2++) {
                    this.columnLayoutList.get(i2).setTag(0);
                    this.columnLayoutList.get(i2).removeAllViews();
                }
                for (int j = 0; j < this.flowTagList.size(); j++) {
                    this.onLoadItemListener.recycleImage(this.flowTagList.get(j).getThumbnailUrl());
                }
                this.flowTagList.clear();
                loadLayout(flowTagListTemp, type);
                this.flowTagList.addAll(flowTagListTemp);
                return;
            case 1:
                loadLayout(flowTagListTemp, type);
                this.flowTagList.addAll(flowTagListTemp);
                return;
            default:
                return;
        }
    }

    private void loadLayout(List<FlowTag> flowTagListTemp, int type) {
        int imageHeight;
        for (int i = 0; i < flowTagListTemp.size(); i++) {
            this.currentPosition = i;
            final int position = this.flowTagList.size() + i;
            final FlowTag flowTag = flowTagListTemp.get(i);
            float ratio = flowTag.getRatio();
            if (ratio == 0.0f) {
                imageHeight = this.columnWidth * 1;
            } else if (ratio > 2.0f) {
                imageHeight = this.columnWidth * 2;
            } else {
                imageHeight = (int) (((float) this.columnWidth) * ratio);
            }
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-1, imageHeight);
            lp.topMargin = this.marginTop;
            View linearLayout = this.onLoadItemListener.getImgBox(flowTag);
            if (linearLayout == null) {
                linearLayout = new LinearLayout(this.context);
                linearLayout.setBackgroundColor(-1);
                this.onLoadItemListener.loadLayout((LinearLayout) linearLayout, flowTag);
            }
            linearLayout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    PullToRefreshWaterFall.this.onLoadItemListener.onItemClick(position, flowTag);
                }
            });
            getShortColumnLayout(flowTag, imageHeight).addView(linearLayout, lp);
        }
    }

    private LinearLayout getShortColumnLayout(FlowTag flowTag, int imageHeight) {
        int shortPosition = 0;
        int shortHeight = 0;
        for (int i = 0; i < this.columnLayoutList.size(); i++) {
            int height = ((Integer) this.columnLayoutList.get(i).getTag()).intValue();
            if (i == 0) {
                shortHeight = height;
            } else if (shortHeight > height) {
                shortHeight = height;
                shortPosition = i;
            }
        }
        flowTag.setMarginTop(((Integer) this.columnLayoutList.get(shortPosition).getTag()).intValue() + imageHeight + this.marginTop);
        this.columnLayoutList.get(shortPosition).setTag(Integer.valueOf(((Integer) this.columnLayoutList.get(shortPosition).getTag()).intValue() + imageHeight + this.marginTop));
        checkVisible(flowTag);
        return this.columnLayoutList.get(shortPosition);
    }

    /* access modifiers changed from: private */
    public void checkVisible(final FlowTag flowTag) {
        if (flowTag.getMarginTop() <= this.preYForScrollViewStateCompare - this.expandHeight || flowTag.getMarginTop() >= this.preYForScrollViewStateCompare + getDisplay(this.context).getHeight() + this.expandHeight) {
            this.mHandler.post(new Runnable() {
                public void run() {
                    PullToRefreshWaterFall.this.onLoadItemListener.recycleImage(flowTag.getThumbnailUrl());
                }
            });
        } else {
            this.onLoadItemListener.loadImage(flowTag.getThumbnailUrl(), this.isLoaded);
        }
    }

    /* access modifiers changed from: protected */
    public void gotoTop() {
        this.preYForScrollViewStateCompare = 0;
    }

    public void onDestroyView() {
        for (int i = 0; i < this.flowTagList.size(); i++) {
            if (this.onLoadItemListener != null) {
                this.onLoadItemListener.recycleImage(this.flowTagList.get(i).getThumbnailUrl());
            }
        }
        if (this.mTimer != null) {
            this.mTimer.cancel();
            this.mTimer = null;
        }
    }

    public void setFallWallBlank() {
        this.isLoaded = false;
        for (int i = 0; i < this.flowTagList.size(); i++) {
            if (this.onLoadItemListener != null) {
                this.onLoadItemListener.recycleImage(this.flowTagList.get(i).getThumbnailUrl());
            }
        }
    }

    public void setFallWallImage() {
        this.isLoaded = true;
        int size = this.flowTagList.size();
        for (int i = 0; i < size; i++) {
            checkVisible(this.flowTagList.get(i));
        }
    }

    /* access modifiers changed from: protected */
    public boolean onScroll(MotionEvent event) {
        return false;
    }

    private Display getDisplay(Context context2) {
        DisplayMetrics dm = new DisplayMetrics();
        Display display = ((WindowManager) context2.getSystemService("window")).getDefaultDisplay();
        display.getMetrics(dm);
        return display;
    }

    /* access modifiers changed from: protected */
    public ScrollView createRefreshableView(Context context2, AttributeSet attrs) {
        return new ScrollView(context2, attrs);
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForPullDown() {
        return ((ScrollView) this.refreshableView).getScrollY() == 0;
    }
}
