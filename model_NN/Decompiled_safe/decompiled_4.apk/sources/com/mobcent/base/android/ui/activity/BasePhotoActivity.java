package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.util.MCLibIOUtil;
import java.io.File;

public abstract class BasePhotoActivity extends BaseActivity implements MCConstant {
    private static final String LOCAL_POSITION_DIR = (MCLibIOUtil.FS + "mobcent" + MCLibIOUtil.FS + "forum" + MCLibIOUtil.FS + PermConstant.UPLOAD + MCLibIOUtil.FS);
    protected static final int UPLOAD_ICON = 1;
    protected static final int UPLOAD_IMAGE = 2;
    protected static final int UPLOAD_IMAGE_GIF = 3;
    private static final int UPLOAD_NONE = 0;
    protected File cameraFile;
    protected String cameraPath;
    protected File compressFile;
    protected String compressPath;
    protected String fileDirPath;
    protected int iconWidth = 150;
    protected String path = null;
    protected File selectedFile;
    protected String selectedPath;
    protected int uploadType = 0;
    protected File zoomFile;
    protected String zoomPath;

    /* access modifiers changed from: protected */
    public abstract void doSomethingAfterSelectedPhoto();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        String baseLocation = MCLibIOUtil.getBaseLocalLocation(this);
        this.fileDirPath = baseLocation + LOCAL_POSITION_DIR;
        this.compressPath = baseLocation + LOCAL_POSITION_DIR + MCConstant.MOB_COMPRESS_FILE_NAME;
        this.cameraPath = baseLocation + LOCAL_POSITION_DIR + MCConstant.MOB_CAMERA_FILE_NAME;
        this.selectedPath = baseLocation + LOCAL_POSITION_DIR + MCConstant.MOB_SELECTED_FILE_NAME;
        this.zoomPath = baseLocation + LOCAL_POSITION_DIR + MCConstant.MOB_ZOOM_FILE_NAME;
        if (!MCLibIOUtil.isDirExist(this.fileDirPath)) {
            MCLibIOUtil.makeDirs(this.fileDirPath);
        }
        this.cameraFile = new File(this.cameraPath);
        if (!this.cameraFile.getParentFile().exists()) {
            this.cameraFile.mkdirs();
        }
        this.selectedFile = new File(this.selectedPath);
        if (!this.selectedFile.getParentFile().exists()) {
            this.selectedFile.mkdirs();
        }
        this.compressFile = new File(this.compressPath);
        if (!this.compressFile.getParentFile().exists()) {
            this.compressFile.mkdirs();
        }
        this.zoomFile = new File(this.zoomPath);
        if (!this.zoomFile.getParentFile().exists()) {
            this.zoomFile.mkdirs();
        }
    }

    /* access modifiers changed from: protected */
    public void initViews() {
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
    }

    /* access modifiers changed from: protected */
    public void cameraPhotoListener() {
        clearTempFile();
        File file = new File(this.cameraPath);
        if (!file.getParentFile().exists()) {
            file.mkdirs();
        }
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", Uri.fromFile(file));
        startActivityForResult(intent, 1);
    }

    /* access modifiers changed from: protected */
    public void localPhotoListener() {
        clearTempFile();
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction("android.intent.action.GET_CONTENT");
        startActivityForResult(intent, 2);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0008 A[Catch:{ Exception -> 0x001f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityResult(int r6, int r7, android.content.Intent r8) {
        /*
            r5 = this;
            r4 = 1
            switch(r6) {
                case 1: goto L_0x000f;
                case 2: goto L_0x0034;
                case 3: goto L_0x0060;
                default: goto L_0x0004;
            }
        L_0x0004:
            int r2 = r5.uploadType     // Catch:{ Exception -> 0x001f }
            if (r2 == r4) goto L_0x000b
            r5.getImgSucc()     // Catch:{ Exception -> 0x001f }
        L_0x000b:
            super.onActivityResult(r6, r7, r8)
        L_0x000e:
            return
        L_0x000f:
            java.lang.String r2 = r5.cameraPath     // Catch:{ Exception -> 0x001f }
            r5.path = r2     // Catch:{ Exception -> 0x001f }
            java.io.File r2 = r5.cameraFile     // Catch:{ Exception -> 0x001f }
            boolean r2 = r2.exists()     // Catch:{ Exception -> 0x001f }
            if (r2 != 0) goto L_0x0028
            r5.resetData()     // Catch:{ Exception -> 0x001f }
            goto L_0x000e
        L_0x001f:
            r2 = move-exception
            r0 = r2
            r0.printStackTrace()
            r5.resetData()
            goto L_0x000e
        L_0x0028:
            int r2 = r5.uploadType     // Catch:{ Exception -> 0x001f }
            if (r2 != r4) goto L_0x0004
            java.lang.String r2 = r5.cameraPath     // Catch:{ Exception -> 0x001f }
            java.lang.String r3 = r5.zoomPath     // Catch:{ Exception -> 0x001f }
            r5.zoomPic(r2, r3)     // Catch:{ Exception -> 0x001f }
            goto L_0x0004
        L_0x0034:
            android.net.Uri r2 = r8.getData()     // Catch:{ Exception -> 0x001f }
            if (r2 != 0) goto L_0x003e
            r5.resetData()     // Catch:{ Exception -> 0x001f }
            goto L_0x000e
        L_0x003e:
            java.lang.String r2 = r5.selectedPath     // Catch:{ Exception -> 0x001f }
            r5.path = r2     // Catch:{ Exception -> 0x001f }
            java.lang.String r1 = r5.getSelectedPath(r7, r8)     // Catch:{ Exception -> 0x001f }
            r5.path = r1     // Catch:{ Exception -> 0x001f }
            int r2 = r5.uploadType     // Catch:{ Exception -> 0x001f }
            if (r2 != r4) goto L_0x0052
            java.lang.String r2 = r5.zoomPath     // Catch:{ Exception -> 0x001f }
            r5.zoomPic(r1, r2)     // Catch:{ Exception -> 0x001f }
            goto L_0x0004
        L_0x0052:
            boolean r2 = r5.checkPathName(r1)     // Catch:{ Exception -> 0x001f }
            if (r2 == 0) goto L_0x005c
            r2 = 3
            r5.uploadType = r2     // Catch:{ Exception -> 0x001f }
            goto L_0x0004
        L_0x005c:
            r2 = 2
            r5.uploadType = r2     // Catch:{ Exception -> 0x001f }
            goto L_0x0004
        L_0x0060:
            java.io.File r2 = r5.zoomFile     // Catch:{ Exception -> 0x001f }
            boolean r2 = r2.exists()     // Catch:{ Exception -> 0x001f }
            if (r2 != 0) goto L_0x006c
            r5.resetData()     // Catch:{ Exception -> 0x001f }
            goto L_0x000e
        L_0x006c:
            java.lang.String r2 = r5.zoomPath     // Catch:{ Exception -> 0x001f }
            r5.path = r2     // Catch:{ Exception -> 0x001f }
            r5.getImgSucc()     // Catch:{ Exception -> 0x001f }
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.base.android.ui.activity.BasePhotoActivity.onActivityResult(int, int, android.content.Intent):void");
    }

    private void getImgSucc() {
        if (this.path == this.cameraPath) {
            if (!this.cameraFile.exists()) {
                resetData();
            }
        } else if (this.path == this.selectedPath && !this.selectedFile.exists()) {
            resetData();
        }
        if (this.path != null) {
            doSomethingAfterSelectedPhoto();
        }
    }

    private void resetData() {
        this.path = null;
        clearTempFile();
        warnMessageById("mc_forum_user_photo_select_error");
    }

    private String getSelectedPath(int resultCode, Intent data) {
        if (resultCode != -1) {
            return null;
        }
        Cursor cursor = managedQuery(data.getData(), new String[]{"_data"}, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private boolean checkPathName(String imgPath) {
        if (imgPath == null || !imgPath.endsWith(".gif")) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void zoomPic(String inputPath, String outPath) {
        Intent intent = new Intent(this, CropImageActivity.class);
        intent.putExtra(MCConstant.INPUT_PATH, inputPath);
        intent.putExtra(MCConstant.OUT_PATH, outPath);
        intent.putExtra(MCConstant.ZOOM_MAX_WIDTH, 200);
        startActivityForResult(intent, 3);
    }

    /* access modifiers changed from: protected */
    public void clearTempFile() {
        if (this.cameraPath != null && this.compressPath != null && this.selectedPath != null) {
            if (this.cameraFile != null && this.cameraFile.exists()) {
                this.cameraFile.delete();
            }
            if (this.compressFile != null && this.compressFile.exists()) {
                this.compressFile.delete();
            }
            if (this.selectedFile != null && this.selectedFile.exists()) {
                this.selectedFile.delete();
            }
            if (this.zoomFile != null && this.zoomFile.exists()) {
                this.zoomFile.delete();
            }
        }
    }
}
