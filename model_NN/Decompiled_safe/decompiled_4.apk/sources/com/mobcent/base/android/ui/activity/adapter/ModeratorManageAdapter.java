package com.mobcent.base.android.ui.activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.ModeratorActivity;
import com.mobcent.base.android.ui.activity.adapter.holder.ModeratorManageAdapterHolder;
import com.mobcent.base.android.ui.activity.fragmentActivity.MsgChatRoomFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCTouchUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.ModeratorModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import java.util.ArrayList;
import java.util.List;

public class ModeratorManageAdapter extends BaseAdapter {
    private AsyncTaskLoaderImage asyncTaskLoaderImage;
    /* access modifiers changed from: private */
    public Context context;
    private ModeratorManageAdapterHolder holder;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private List<ModeratorModel> moderatorList = new ArrayList();
    private ModeratorModel moderatorModel;
    /* access modifiers changed from: private */
    public MCResource resource;

    public ModeratorManageAdapter(Context context2, AsyncTaskLoaderImage asyncTaskLoaderImage2, Handler mHandler2) {
        this.context = context2;
        this.inflater = LayoutInflater.from(context2);
        this.mHandler = mHandler2;
        this.asyncTaskLoaderImage = asyncTaskLoaderImage2;
        this.resource = MCResource.getInstance(context2);
    }

    public int getCount() {
        return this.moderatorList.size();
    }

    public Object getItem(int position) {
        return this.moderatorList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View convertView2 = getConvertView(convertView);
        this.moderatorModel = this.moderatorList.get(position);
        this.holder = (ModeratorManageAdapterHolder) convertView2.getTag();
        updateModeratorAdapter(this.moderatorModel, this.holder, convertView2);
        initActionsModeratorAdapter(this.moderatorModel, this.holder);
        return convertView2;
    }

    private void updateModeratorAdapter(ModeratorModel moderatorModel2, ModeratorManageAdapterHolder holder2, View convertView) {
        updateUserIconImage(moderatorModel2.getIcon(), convertView, holder2.getIconImg());
        holder2.getUserNameText().setText(moderatorModel2.getNickname());
        String boards = "";
        for (int i = 0; i < moderatorModel2.getBoardList().size(); i++) {
            boards = boards + moderatorModel2.getBoardList().get(i).getBoardName() + AdApiConstant.RES_SPLIT_COMMA;
        }
        holder2.getBoardText().setText(boards.substring(0, boards.length() - 1));
        MCTouchUtil.createTouchDelegate(holder2.getModeratorBtn(), 10);
    }

    private View getConvertView(View convertView) {
        ModeratorManageAdapterHolder holder2;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_moderator_manager_item"), (ViewGroup) null);
            holder2 = new ModeratorManageAdapterHolder();
            initModeratorAdapterHolder(convertView, holder2);
            convertView.setTag(holder2);
        } else {
            holder2 = (ModeratorManageAdapterHolder) convertView.getTag();
        }
        if (holder2 != null) {
            return convertView;
        }
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_moderator_manager_item"), (ViewGroup) null);
        ModeratorManageAdapterHolder holder3 = new ModeratorManageAdapterHolder();
        initModeratorAdapterHolder(convertView2, holder3);
        convertView2.setTag(holder3);
        return convertView2;
    }

    private void initModeratorAdapterHolder(View convertView, ModeratorManageAdapterHolder holder2) {
        holder2.setIconImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_user_icon_img")));
        holder2.setBoardText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_text")));
        holder2.setUserNameText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_user_name_text")));
        holder2.setFollowBtn((Button) convertView.findViewById(this.resource.getViewId("mc_forum_follow_user_btn")));
        holder2.setModeratorBtn((Button) convertView.findViewById(this.resource.getViewId("mc_forum_moderator_btn")));
    }

    private void initActionsModeratorAdapter(final ModeratorModel moderatorModel2, ModeratorManageAdapterHolder holder2) {
        holder2.getFollowBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ModeratorManageAdapter.this.context, MsgChatRoomFragmentActivity.class);
                intent.putExtra(MCConstant.CHAT_USER_ID, moderatorModel2.getUserId());
                intent.putExtra(MCConstant.CHAT_USER_NICKNAME, moderatorModel2.getNickname());
                ModeratorManageAdapter.this.context.startActivity(intent);
            }
        });
        holder2.getModeratorBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ModeratorManageAdapter.this.context, ModeratorActivity.class);
                intent.putExtra("userId", moderatorModel2.getUserId());
                ModeratorManageAdapter.this.context.startActivity(intent);
            }
        });
        holder2.getIconImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCForumHelper.gotoUserInfo((Activity) ModeratorManageAdapter.this.context, ModeratorManageAdapter.this.resource, moderatorModel2.getUserId());
            }
        });
    }

    private void updateUserIconImage(String imgUrl, final View convertView, ImageView userIconImg) {
        userIconImg.setBackgroundResource(this.resource.getDrawableId("mc_forum_head"));
        if (SharedPreferencesDB.getInstance(this.context).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(AsyncTaskLoaderImage.formatUrl(imgUrl, "100x100"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    if (image != null) {
                        ModeratorManageAdapter.this.mHandler.post(new Runnable() {
                            public void run() {
                                ImageView imageViewByTag = (ImageView) convertView.findViewById(ModeratorManageAdapter.this.resource.getViewId("mc_forum_user_icon_img"));
                                if (image != null && !image.isRecycled()) {
                                    imageViewByTag.setBackgroundDrawable(new BitmapDrawable(image));
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    public List<ModeratorModel> getModeratorList() {
        return this.moderatorList;
    }

    public void setModeratorList(List<ModeratorModel> moderatorList2) {
        this.moderatorList = moderatorList2;
    }
}
