package com.mobcent.base.android.ui.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.ad.android.ui.widget.manager.AdManager;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.TopicListAdapter;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCForumSearchUtil;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class SearchTopicListActivity extends BaseActivity implements MCConstant {
    /* access modifiers changed from: private */
    public static String keywordStr;
    private final String TAG = "SearchTopicListActivity";
    private int adPostion = 0;
    /* access modifiers changed from: private */
    public TopicListAdapter adapter;
    private Button backBtn;
    /* access modifiers changed from: private */
    public long boardIdSearch;
    private String boardNameStr;
    /* access modifiers changed from: private */
    public int currentPage = 1;
    private LayoutInflater inflater;
    private TextView keywordBtn;
    private GetMoreTopicInfoAsyncTask moreTopicInfoAsyncTask;
    private int pageSize = 20;
    /* access modifiers changed from: private */
    public PostsService postsService;
    /* access modifiers changed from: private */
    public PullToRefreshListView pullToRefreshListView;
    private RefreshTopicInfoAsyncTask refreshTopicInfoAsyncTask;
    /* access modifiers changed from: private */
    public List<String> refreshimgUrls;
    private Button searchBtn;
    /* access modifiers changed from: private */
    public EditText searchEdit;
    /* access modifiers changed from: private */
    public List<TopicModel> topicList;
    /* access modifiers changed from: private */
    public long userId = 0;

    static /* synthetic */ int access$408(SearchTopicListActivity x0) {
        int i = x0.currentPage;
        x0.currentPage = i + 1;
        return i;
    }

    static /* synthetic */ int access$410(SearchTopicListActivity x0) {
        int i = x0.currentPage;
        x0.currentPage = i - 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.pullToRefreshListView.onRefreshWithOutListener();
        onRefreshs();
        if (this.adPostion == 0) {
            this.adPostion = new Integer(getResources().getString(this.resource.getStringId("mc_forum_search_topic_position"))).intValue();
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.inflater = LayoutInflater.from(this);
        this.userId = new UserServiceImpl(this).getLoginUserId();
        this.boardIdSearch = getIntent().getLongExtra("boardId", 0);
        this.boardNameStr = getIntent().getStringExtra("boardName");
        keywordStr = getIntent().getStringExtra("keyword");
        this.postsService = new PostsServiceImpl(this);
        this.topicList = new ArrayList();
        this.refreshimgUrls = new ArrayList();
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_search_topic_list_activity"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.keywordBtn = (TextView) findViewById(this.resource.getViewId("mc_forum_keyword_btn"));
        this.searchBtn = (Button) findViewById(this.resource.getViewId("mc_forum_search_btn"));
        this.searchEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_search_edit"));
        this.pullToRefreshListView = (PullToRefreshListView) findViewById(this.resource.getViewId("mc_forum_lv"));
        this.adapter = new TopicListAdapter(this, this.topicList, null, this.mHandler, this.inflater, this.asyncTaskLoaderImage, this.adPostion, null, new Integer(getResources().getString(this.resource.getStringId("mc_forum_search_topic_position_bottom"))).intValue(), this.currentPage);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
        if ("".equals(this.boardNameStr) || this.boardNameStr == null) {
            this.keywordBtn.setText(this.resource.getStringId("mc_forum_search_title"));
        } else {
            this.keywordBtn.setText(this.boardNameStr);
        }
        if (this.boardIdSearch > 0) {
            this.searchEdit.setHint(this.resource.getStringId("mc_forum_search_division_board"));
        } else {
            this.searchEdit.setHint(this.resource.getStringId("mc_forum_search_keywords"));
        }
        this.searchEdit.setText(keywordStr);
        this.searchEdit.setTextColor(this.resource.getColorId("mc_forum_input_hint_text_color"));
        Editable editable = this.searchEdit.getText();
        Selection.setSelection(editable, editable.length());
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SearchTopicListActivity.this.back();
            }
        });
        this.keywordBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SearchTopicListActivity.this.onRefreshs();
            }
        });
        this.searchBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ((InputMethodManager) SearchTopicListActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(SearchTopicListActivity.this.searchEdit.getWindowToken(), 0);
                SearchTopicListActivity.this.searchClick();
            }
        });
        this.searchEdit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCLogUtil.d("SearchTopicListActivity", SearchTopicListActivity.keywordStr + "---->");
                SearchTopicListActivity.this.searchEdit.setTextColor(SearchTopicListActivity.this.resource.getColorId("mc_forum_input_normal_text_color"));
                Editable editable = SearchTopicListActivity.this.searchEdit.getText();
                Selection.setSelection(editable, editable.length());
            }
        });
        this.searchEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66) {
                    SearchTopicListActivity.this.searchEdit.setFocusable(false);
                    SearchTopicListActivity.this.searchEdit.setFocusableInTouchMode(true);
                    SearchTopicListActivity.this.hideSoftKeyboard();
                    SearchTopicListActivity.this.searchClick();
                }
                SearchTopicListActivity.this.searchEdit.setFocusable(true);
                return false;
            }
        });
        this.pullToRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                SearchTopicListActivity.this.onRefreshs();
            }
        });
        this.pullToRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                SearchTopicListActivity.this.onLoadMore();
            }
        });
    }

    public void searchClick() {
        String searchEditStr = this.searchEdit.getText().toString();
        if ("".equals(searchEditStr)) {
            Toast.makeText(this, this.resource.getStringId("mc_forum_no_keywords"), 0).show();
        } else if (searchEditStr.length() > 10) {
            Toast.makeText(this, this.resource.getStringId("mc_forum_keywords_length_warn"), 0).show();
        } else if (MCForumSearchUtil.isSearchEnable()) {
            this.topicList = new ArrayList();
            this.adapter.setTopicList(this.topicList);
            this.adapter.notifyDataSetInvalidated();
            this.adapter.notifyDataSetChanged();
            keywordStr = searchEditStr;
            this.pullToRefreshListView.onRefreshWithOutListener();
            onRefreshs();
        } else {
            Toast.makeText(this, this.resource.getStringId("mc_forum_search_over_times"), 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.keywordBtn.setEnabled(true);
    }

    public void onRefreshs() {
        if (this.refreshTopicInfoAsyncTask != null) {
            this.refreshTopicInfoAsyncTask.cancel(true);
        }
        this.refreshTopicInfoAsyncTask = new RefreshTopicInfoAsyncTask();
        this.refreshTopicInfoAsyncTask.execute(Integer.valueOf(this.pageSize));
    }

    public void onLoadMore() {
        if (this.moreTopicInfoAsyncTask != null) {
            this.moreTopicInfoAsyncTask.cancel(true);
        }
        this.moreTopicInfoAsyncTask = new GetMoreTopicInfoAsyncTask();
        this.moreTopicInfoAsyncTask.execute(Integer.valueOf(this.pageSize));
    }

    private class RefreshTopicInfoAsyncTask extends AsyncTask<Object, Void, List<TopicModel>> {
        private RefreshTopicInfoAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<TopicModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            int unused = SearchTopicListActivity.this.currentPage = 1;
            SearchTopicListActivity.this.pullToRefreshListView.setSelection(0);
            if (SearchTopicListActivity.this.refreshimgUrls != null && !SearchTopicListActivity.this.refreshimgUrls.isEmpty() && SearchTopicListActivity.this.refreshimgUrls.size() > 0) {
                SearchTopicListActivity.this.asyncTaskLoaderImage.recycleBitmaps(SearchTopicListActivity.this.refreshimgUrls);
            }
        }

        /* access modifiers changed from: protected */
        public List<TopicModel> doInBackground(Object... params) {
            return SearchTopicListActivity.this.postsService.searchTopicList(SearchTopicListActivity.this.userId, SearchTopicListActivity.this.boardIdSearch, SearchTopicListActivity.keywordStr, SearchTopicListActivity.this.currentPage, ((Integer) params[0]).intValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<TopicModel> topicModelList) {
            SearchTopicListActivity.this.pullToRefreshListView.onRefreshComplete();
            if (topicModelList == null) {
                SearchTopicListActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else if (topicModelList.isEmpty()) {
                MCForumSearchUtil.monitorSearchAction();
                SearchTopicListActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(topicModelList.get(0).getErrorCode())) {
                Toast.makeText(SearchTopicListActivity.this, MCForumErrorUtil.convertErrorCode(SearchTopicListActivity.this, topicModelList.get(0).getErrorCode()), 0).show();
                SearchTopicListActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                List unused = SearchTopicListActivity.this.refreshimgUrls = SearchTopicListActivity.this.getRefreshImgUrl(topicModelList);
                MCForumSearchUtil.monitorSearchAction();
                SearchTopicListActivity.this.adapter.setTopicList(topicModelList);
                AdManager.getInstance().recyclAdByTag("SearchTopicListActivity");
                SearchTopicListActivity.this.adapter.notifyDataSetInvalidated();
                SearchTopicListActivity.this.adapter.notifyDataSetChanged();
                if (topicModelList.get(0).getTotalNum() > topicModelList.size()) {
                    SearchTopicListActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    SearchTopicListActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
                }
                List unused2 = SearchTopicListActivity.this.topicList = topicModelList;
            }
        }
    }

    private class GetMoreTopicInfoAsyncTask extends AsyncTask<Object, Void, List<TopicModel>> {
        private GetMoreTopicInfoAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<TopicModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            SearchTopicListActivity.access$408(SearchTopicListActivity.this);
        }

        /* access modifiers changed from: protected */
        public List<TopicModel> doInBackground(Object... params) {
            return SearchTopicListActivity.this.postsService.searchTopicList(SearchTopicListActivity.this.userId, SearchTopicListActivity.this.boardIdSearch, SearchTopicListActivity.keywordStr, SearchTopicListActivity.this.currentPage, ((Integer) params[0]).intValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<TopicModel> topicModelList) {
            if (topicModelList == null) {
                SearchTopicListActivity.access$410(SearchTopicListActivity.this);
            } else if (topicModelList.isEmpty()) {
                SearchTopicListActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(topicModelList.get(0).getErrorCode())) {
                SearchTopicListActivity.access$410(SearchTopicListActivity.this);
                Toast.makeText(SearchTopicListActivity.this, MCForumErrorUtil.convertErrorCode(SearchTopicListActivity.this, topicModelList.get(0).getErrorCode()), 0).show();
                SearchTopicListActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                List<TopicModel> tempList = new ArrayList<>();
                tempList.addAll(SearchTopicListActivity.this.topicList);
                tempList.addAll(topicModelList);
                SearchTopicListActivity.this.adapter.setTopicList(tempList);
                AdManager.getInstance().recyclAdByTag("SearchTopicListActivity");
                SearchTopicListActivity.this.adapter.notifyDataSetInvalidated();
                SearchTopicListActivity.this.adapter.notifyDataSetChanged();
                if (topicModelList.get(0).getTotalNum() > tempList.size()) {
                    SearchTopicListActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    SearchTopicListActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
                }
                List unused = SearchTopicListActivity.this.topicList = tempList;
            }
        }
    }

    /* access modifiers changed from: private */
    public List<String> getRefreshImgUrl(List<TopicModel> topicModelList) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < topicModelList.size(); i++) {
            isExit(topicModelList.get(i), topicModelList);
        }
        return list;
    }

    private boolean isExit(TopicModel model, List<TopicModel> result) {
        int j = result.size();
        for (int i = 0; i < j; i++) {
            TopicModel model2 = result.get(i);
            if (!StringUtil.isEmpty(model.getPicPath()) && !StringUtil.isEmpty(model2.getPicPath()) && model.getPicPath().equals(model2.getPicPath())) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public List<String> getImageURL(int start, int end) {
        List<String> imgUrls = new ArrayList<>();
        for (int i = start; i < end; i++) {
            TopicModel model = this.topicList.get(i);
            if (!StringUtil.isEmpty(model.getPicPath())) {
                imgUrls.add(AsyncTaskLoaderImage.formatUrl(model.getBaseUrl() + model.getPicPath(), "100x100"));
            }
        }
        return imgUrls;
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return getImageURL(0, this.topicList.size());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.refreshTopicInfoAsyncTask != null) {
            this.refreshTopicInfoAsyncTask.cancel(true);
        }
        if (this.moreTopicInfoAsyncTask != null) {
            this.moreTopicInfoAsyncTask.cancel(true);
        }
        AdManager.getInstance().recyclAdByTag("SearchTopicListActivity");
    }
}
