package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.ad.android.ui.widget.AdView;
import com.mobcent.base.android.ui.activity.view.MCInfoListView;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;

public class PostsTopicAdapterHolder {
    private AdView adView;
    private LinearLayout levelBox;
    private RelativeLayout loadingContainer;
    private LinearLayout locationBox;
    private TextView locationText;
    private ImageView pollImg;
    private LinearLayout pollLayout;
    private LinearLayout pollRsLayout;
    private LinearLayout pollSelectLayout;
    private Button pollSubmitBtn;
    private TextView pollTitleText;
    private MCProgressBar progressBar;
    private TextView repostBoardName;
    private LinearLayout repostBox;
    private LinearLayout repostContentLayout;
    private TextView repostTime;
    private TextView repostUserName;
    private ImageView roleImg;
    private LinearLayout topicBox;
    private LinearLayout topicContentLayout;
    private TextView topicContentText;
    private MCInfoListView topicContentView;
    private ImageButton topicDeleteBtn;
    private ImageButton topicFavoriteBtn;
    private ImageButton topicMoreBtn;
    private ImageView topicMoreImg;
    private ImageButton topicReplyBtn;
    private ImageButton topicShareBtn;
    private ImageView topicThumbnailImg;
    private TextView topicTimeText;
    private TextView topicTitleText;
    private ImageView topicUserImg;
    private TextView topicUserText;
    private TextView userRoleText;

    public AdView getAdView() {
        return this.adView;
    }

    public void setAdView(AdView adView2) {
        this.adView = adView2;
    }

    public LinearLayout getLevelBox() {
        return this.levelBox;
    }

    public void setLevelBox(LinearLayout levelBox2) {
        this.levelBox = levelBox2;
    }

    public ImageButton getTopicMoreBtn() {
        return this.topicMoreBtn;
    }

    public void setTopicMoreBtn(ImageButton topicMoreBtn2) {
        this.topicMoreBtn = topicMoreBtn2;
    }

    public ImageView getPollImg() {
        return this.pollImg;
    }

    public void setPollImg(ImageView pollImg2) {
        this.pollImg = pollImg2;
    }

    public TextView getUserRoleText() {
        return this.userRoleText;
    }

    public void setUserRoleText(TextView userRoleText2) {
        this.userRoleText = userRoleText2;
    }

    public ImageView getTopicUserImg() {
        return this.topicUserImg;
    }

    public void setTopicUserImg(ImageView topicUserImg2) {
        this.topicUserImg = topicUserImg2;
    }

    public ImageView getTopicThumbnailImg() {
        return this.topicThumbnailImg;
    }

    public void setTopicThumbnailImg(ImageView topicThumbnailImg2) {
        this.topicThumbnailImg = topicThumbnailImg2;
    }

    public ImageView getTopicMoreImg() {
        return this.topicMoreImg;
    }

    public void setTopicMoreImg(ImageView topicMoreImg2) {
        this.topicMoreImg = topicMoreImg2;
    }

    public TextView getTopicTitleText() {
        return this.topicTitleText;
    }

    public void setTopicTitleText(TextView topicTitleText2) {
        this.topicTitleText = topicTitleText2;
    }

    public TextView getTopicUserText() {
        return this.topicUserText;
    }

    public void setTopicUserText(TextView topicUserText2) {
        this.topicUserText = topicUserText2;
    }

    public ImageView getRoleImg() {
        return this.roleImg;
    }

    public void setRoleImg(ImageView roleImg2) {
        this.roleImg = roleImg2;
    }

    public TextView getTopicContentText() {
        return this.topicContentText;
    }

    public void setTopicContentText(TextView topicContentText2) {
        this.topicContentText = topicContentText2;
    }

    public TextView getTopicTimeText() {
        return this.topicTimeText;
    }

    public void setTopicTimeText(TextView topicTimeText2) {
        this.topicTimeText = topicTimeText2;
    }

    public ImageButton getTopicDeleteBtn() {
        return this.topicDeleteBtn;
    }

    public void setTopicDeleteBtn(ImageButton topicDeleteBtn2) {
        this.topicDeleteBtn = topicDeleteBtn2;
    }

    public ImageButton getTopicFavoriteBtn() {
        return this.topicFavoriteBtn;
    }

    public void setTopicFavoriteBtn(ImageButton topicFavoriteBtn2) {
        this.topicFavoriteBtn = topicFavoriteBtn2;
    }

    public ImageButton getTopicShareBtn() {
        return this.topicShareBtn;
    }

    public void setTopicShareBtn(ImageButton topicShareBtn2) {
        this.topicShareBtn = topicShareBtn2;
    }

    public ImageButton getTopicReplyBtn() {
        return this.topicReplyBtn;
    }

    public void setTopicReplyBtn(ImageButton topicReplyBtn2) {
        this.topicReplyBtn = topicReplyBtn2;
    }

    public RelativeLayout getLoadingContainer() {
        return this.loadingContainer;
    }

    public void setLoadingContainer(RelativeLayout loadingContainer2) {
        this.loadingContainer = loadingContainer2;
    }

    public MCProgressBar getProgressBar() {
        return this.progressBar;
    }

    public void setProgressBar(MCProgressBar progressBar2) {
        this.progressBar = progressBar2;
    }

    public MCInfoListView getTopicContentView() {
        return this.topicContentView;
    }

    public void setTopicContentView(MCInfoListView topicContentView2) {
        this.topicContentView = topicContentView2;
    }

    public LinearLayout getTopicContentLayout() {
        return this.topicContentLayout;
    }

    public void setTopicContentLayout(LinearLayout topicContentLayout2) {
        this.topicContentLayout = topicContentLayout2;
    }

    public LinearLayout getPollLayout() {
        return this.pollLayout;
    }

    public void setPollLayout(LinearLayout pollLayout2) {
        this.pollLayout = pollLayout2;
    }

    public LinearLayout getPollSelectLayout() {
        return this.pollSelectLayout;
    }

    public void setPollSelectLayout(LinearLayout pollSelectLayout2) {
        this.pollSelectLayout = pollSelectLayout2;
    }

    public LinearLayout getPollRsLayout() {
        return this.pollRsLayout;
    }

    public void setPollRsLayout(LinearLayout pollRsLayout2) {
        this.pollRsLayout = pollRsLayout2;
    }

    public Button getPollSubmitBtn() {
        return this.pollSubmitBtn;
    }

    public void setPollSubmitBtn(Button pollSubmitBtn2) {
        this.pollSubmitBtn = pollSubmitBtn2;
    }

    public TextView getPollTitleText() {
        return this.pollTitleText;
    }

    public void setPollTitleText(TextView pollTitleText2) {
        this.pollTitleText = pollTitleText2;
    }

    public TextView getLocationText() {
        return this.locationText;
    }

    public void setLocationText(TextView locationText2) {
        this.locationText = locationText2;
    }

    public LinearLayout getLocationBox() {
        return this.locationBox;
    }

    public void setLocationBox(LinearLayout locationBox2) {
        this.locationBox = locationBox2;
    }

    public TextView getRepostUserName() {
        return this.repostUserName;
    }

    public void setRepostUserName(TextView repostUserName2) {
        this.repostUserName = repostUserName2;
    }

    public TextView getRepostTime() {
        return this.repostTime;
    }

    public void setRepostTime(TextView repostTime2) {
        this.repostTime = repostTime2;
    }

    public TextView getRepostBoardName() {
        return this.repostBoardName;
    }

    public void setRepostBoardName(TextView repostBoardName2) {
        this.repostBoardName = repostBoardName2;
    }

    public LinearLayout getRepostBox() {
        return this.repostBox;
    }

    public void setRepostBox(LinearLayout repostBox2) {
        this.repostBox = repostBox2;
    }

    public LinearLayout getRepostContentLayout() {
        return this.repostContentLayout;
    }

    public void setRepostContentLayout(LinearLayout repostContentLayout2) {
        this.repostContentLayout = repostContentLayout2;
    }

    public LinearLayout getTopicBox() {
        return this.topicBox;
    }

    public void setTopicBox(LinearLayout topicBox2) {
        this.topicBox = topicBox2;
    }
}
