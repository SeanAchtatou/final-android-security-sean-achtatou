package com.mobcent.base.android.ui.activity.view;

import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.BaseActivity;
import com.mobcent.base.android.ui.activity.SearchTopicListActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.BaseFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.TopicListFragmentActivity;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.forum.android.util.MCForumSearchUtil;
import com.mobcent.base.forum.android.util.MCResource;
import java.io.Serializable;
import java.util.HashMap;

public class MCHeaderSearchView {
    private long boardId = 0;
    private String boardName = "";
    /* access modifiers changed from: private */
    public Activity context;
    private MCResource forumResource;
    private View headerSearchView;
    private String keywordStr;
    private Button searchKeywordsBtn;
    /* access modifiers changed from: private */
    public EditText searchKeywordsEdit;

    public MCHeaderSearchView(Activity context2) {
        this.context = context2;
        initHeadSearhView();
    }

    private void initHeadSearhView() {
        this.forumResource = MCResource.getInstance(this.context);
        if (this.context instanceof TopicListFragmentActivity) {
            this.headerSearchView = LayoutInflater.from(this.context).inflate(this.forumResource.getLayoutId("mc_forum_header_topic_search_view"), (ViewGroup) null);
        } else {
            this.headerSearchView = LayoutInflater.from(this.context).inflate(this.forumResource.getLayoutId("mc_forum_header_board_search_view"), (ViewGroup) null);
        }
        this.searchKeywordsEdit = (EditText) this.headerSearchView.findViewById(this.forumResource.getViewId("mc_forum_search_edit"));
        this.searchKeywordsBtn = (Button) this.headerSearchView.findViewById(this.forumResource.getViewId("mc_forum_search_btn"));
        this.searchKeywordsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCHeaderSearchView.this.searchKeywordsEdit.requestFocus();
                ((InputMethodManager) MCHeaderSearchView.this.context.getSystemService("input_method")).toggleSoftInput(0, 1);
                MCHeaderSearchView.this.searchClick();
            }
        });
        this.searchKeywordsEdit.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                MCHeaderSearchView.this.searchKeywordsEdit.requestFocus();
                ((InputMethodManager) MCHeaderSearchView.this.context.getSystemService("input_method")).toggleSoftInput(0, 1);
                return false;
            }
        });
        this.searchKeywordsEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66) {
                    MCHeaderSearchView.this.searchKeywordsEdit.setFocusable(false);
                    MCHeaderSearchView.this.searchKeywordsEdit.setFocusableInTouchMode(true);
                    if (MCHeaderSearchView.this.context instanceof BaseActivity) {
                        ((BaseActivity) MCHeaderSearchView.this.context).hideSoftKeyboard();
                    } else {
                        ((BaseFragmentActivity) MCHeaderSearchView.this.context).hideSoftKeyboard();
                    }
                    MCHeaderSearchView.this.searchClick();
                }
                MCHeaderSearchView.this.searchKeywordsEdit.setFocusable(true);
                return false;
            }
        });
    }

    /* access modifiers changed from: private */
    public void searchClick() {
        this.keywordStr = this.searchKeywordsEdit.getText().toString();
        if ("".equals(this.keywordStr)) {
            Toast.makeText(this.context, this.forumResource.getStringId("mc_forum_no_keywords"), 0).show();
        } else if (this.keywordStr.length() > 10) {
            Toast.makeText(this.context, this.forumResource.getStringId("mc_forum_keywords_length_warn"), 0).show();
        } else if (MCForumSearchUtil.isSearchEnable()) {
            HashMap<String, Serializable> param = new HashMap<>();
            param.put("keyword", this.keywordStr);
            if (LoginInterceptor.doInterceptorByDialog(this.context, this.forumResource, SearchTopicListActivity.class, param)) {
                Intent intent = new Intent(this.context, SearchTopicListActivity.class);
                intent.putExtra("boardId", this.boardId);
                intent.putExtra("boardName", this.boardName);
                intent.putExtra("keyword", this.keywordStr);
                this.context.startActivity(intent);
            }
        } else {
            Toast.makeText(this.context, this.forumResource.getStringId("mc_forum_search_over_times"), 0).show();
        }
    }

    public View getHeaderSearchView() {
        return this.headerSearchView;
    }

    public void setHeaderSearchView(View headerSearchView2) {
        this.headerSearchView = headerSearchView2;
    }

    public long getBoardId() {
        return this.boardId;
    }

    public void setBoardId(long boardId2) {
        this.boardId = boardId2;
    }

    public String getBoardName() {
        return this.boardName;
    }

    public void setBoardName(String boardName2) {
        this.boardName = boardName2;
    }
}
