package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.mobcent.base.android.constant.EmailResourceConstant;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.RegLoginListAdapter;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.MD5Util;
import com.mobcent.forum.android.util.StringUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class AuthorizationSuccActivity extends BasePhotoPreviewActivity implements MCConstant {
    private Button cancelSubmitBtn;
    /* access modifiers changed from: private */
    public int emailMaxLen = 64;
    private LinearLayout femaleBox;
    /* access modifiers changed from: private */
    public Button femaleRadio;
    private HashMap<String, Serializable> goParam;
    /* access modifiers changed from: private */
    public Class<?> goToActivityClass;
    /* access modifiers changed from: private */
    public ImageView headerImg;
    private Intent intent;
    private Button localPhotoBtn;
    private LinearLayout maleBox;
    /* access modifiers changed from: private */
    public Button maleRadio;
    /* access modifiers changed from: private */
    public String nickName;
    /* access modifiers changed from: private */
    public EditText nickNameEdit;
    /* access modifiers changed from: private */
    public int nickNameMaxLen = 20;
    /* access modifiers changed from: private */
    public int nickNameMinLen = 3;
    /* access modifiers changed from: private */
    public long platformId;
    /* access modifiers changed from: private */
    public String platformUserId;
    /* access modifiers changed from: private */
    public int pwdMaxLen = 20;
    /* access modifiers changed from: private */
    public int pwdMinLen = 6;
    /* access modifiers changed from: private */
    public RelativeLayout regEmailBox;
    private ListView regEmailList;
    /* access modifiers changed from: private */
    public RegLoginListAdapter regLoginListAdapter;
    private Button registerNowBtn;
    private Button saveInfoSubmitBtn;
    /* access modifiers changed from: private */
    public SaveRegisterAsyncTask saveRegisterAsyncTask;
    private Button takePhotoBtn;
    /* access modifiers changed from: private */
    public UpdateAsyncTask updateAsyncTask;
    private String userBaseUrl;
    private String userEmail;
    /* access modifiers changed from: private */
    public EditText userEmailEdit;
    /* access modifiers changed from: private */
    public int userGender;
    /* access modifiers changed from: private */
    public String userIcon;
    /* access modifiers changed from: private */
    public UserInfoModel userInfoModel;
    /* access modifiers changed from: private */
    public EditText userPwdEdit;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.uploadType = 1;
        this.intent = getIntent();
        if (this.intent != null) {
            this.platformId = this.intent.getLongExtra("platformId", 0);
            this.goToActivityClass = (Class) this.intent.getSerializableExtra(MCConstant.TAG);
            this.goParam = (HashMap) this.intent.getSerializableExtra(MCConstant.GO_PARAM);
            this.userInfoModel = (UserInfoModel) this.intent.getSerializableExtra(MCConstant.USER_INFO_MODEL);
        }
        this.userEmail = this.userInfoModel.getEmail();
        this.userBaseUrl = this.userInfoModel.getBaseUrl();
        this.userIcon = this.userInfoModel.getIcon();
        this.userGender = this.userInfoModel.getGender();
        this.nickName = this.userInfoModel.getNickname();
        this.platformUserId = this.userInfoModel.getPlatformUserId();
        this.platformId = this.intent.getLongExtra("platformId", 0);
        this.regLoginListAdapter = new RegLoginListAdapter(this, new ArrayList(), this.resource);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_authorization_succ_activity"));
        super.initViews();
        this.registerNowBtn = (Button) findViewById(this.resource.getViewId("mc_forum_register_now_btn"));
        this.saveInfoSubmitBtn = (Button) findViewById(this.resource.getViewId("mc_forum_user_save_info_submit_btn"));
        this.cancelSubmitBtn = (Button) findViewById(this.resource.getViewId("mc_forum_user_see_anywhere_submit_btn"));
        this.takePhotoBtn = (Button) findViewById(this.resource.getViewId("mc_forum_user_take_photo_btn"));
        this.localPhotoBtn = (Button) findViewById(this.resource.getViewId("mc_forum_user_local_photo_btn"));
        this.userEmailEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_user_input_user_email_edit"));
        this.userPwdEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_user_input_user_password_edit"));
        this.nickNameEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_user_input_username_btn"));
        this.maleRadio = (Button) findViewById(this.resource.getViewId("mc_forum_gender_male_btn"));
        this.femaleRadio = (Button) findViewById(this.resource.getViewId("mc_forum_gender_female_btn"));
        this.maleBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_gender_male_box"));
        this.femaleBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_gender_female_box"));
        this.headerImg = (ImageView) findViewById(this.resource.getViewId("mc_forum_head_img"));
        this.regEmailBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_user_register_email_box"));
        this.regEmailList = (ListView) findViewById(this.resource.getViewId("mc_forum_user_register_email_list"));
        this.regEmailList.setAdapter((ListAdapter) this.regLoginListAdapter);
        if (!StringUtil.isEmpty(this.userEmail)) {
            this.userEmailEdit.setText(this.userEmail);
            Editable emailEdit = this.userEmailEdit.getText();
            Selection.setSelection(emailEdit, emailEdit.length());
        }
        if (!StringUtil.isEmpty(this.nickName)) {
            this.nickNameEdit.setText(this.nickName);
            Editable nickEdit = this.nickNameEdit.getText();
            Selection.setSelection(nickEdit, nickEdit.length());
        }
        if (this.userGender == 1) {
            this.maleRadio.setBackgroundResource(this.resource.getDrawableId("mc_forum_select2_2"));
            this.femaleRadio.setBackgroundResource(this.resource.getDrawableId("mc_forum_select2_1"));
        } else if (this.userGender == 0) {
            this.maleRadio.setBackgroundResource(this.resource.getDrawableId("mc_forum_select2_1"));
            this.femaleRadio.setBackgroundResource(this.resource.getDrawableId("mc_forum_select2_2"));
        }
        showUserIcon();
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.userEmailEdit.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Editable editable = AuthorizationSuccActivity.this.userEmailEdit.getText();
                if (editable.length() > 0) {
                    AuthorizationSuccActivity.this.regEmailBox.setVisibility(0);
                    AuthorizationSuccActivity.this.notifyData(editable.toString());
                    return;
                }
                AuthorizationSuccActivity.this.regEmailBox.setVisibility(8);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        this.regEmailList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                AuthorizationSuccActivity.this.userEmailEdit.setText(AuthorizationSuccActivity.this.regLoginListAdapter.getEmailSections().get(position));
                Editable regEmail = AuthorizationSuccActivity.this.userEmailEdit.getText();
                Selection.setSelection(regEmail, regEmail.length());
                AuthorizationSuccActivity.this.regEmailBox.setVisibility(8);
            }
        });
        this.saveInfoSubmitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!StringUtil.isEmpty(AuthorizationSuccActivity.this.nickName)) {
                    String unused = AuthorizationSuccActivity.this.nickName = AuthorizationSuccActivity.this.nickNameEdit.getText().toString().trim();
                    MCLogUtil.d(MCConstant.TAG, "nickName2-->" + AuthorizationSuccActivity.this.nickName);
                }
                if (!StringUtil.checkNickNameMinLen(AuthorizationSuccActivity.this.nickName, AuthorizationSuccActivity.this.nickNameMinLen) || !StringUtil.checkNickNameMaxLen(AuthorizationSuccActivity.this.nickName, AuthorizationSuccActivity.this.nickNameMaxLen)) {
                    AuthorizationSuccActivity.this.warnMessageById("mc_forum_user_nickname_length_error");
                    return;
                }
                if (AuthorizationSuccActivity.this.iconPath != null) {
                    String unused2 = AuthorizationSuccActivity.this.userIcon = AuthorizationSuccActivity.this.iconPath;
                }
                AuthorizationSuccActivity.this.userInfoModel.setIcon(AuthorizationSuccActivity.this.userIcon);
                AuthorizationSuccActivity.this.userInfoModel.setNickname(AuthorizationSuccActivity.this.nickName);
                AuthorizationSuccActivity.this.userInfoModel.setGender(AuthorizationSuccActivity.this.userGender);
                AuthorizationSuccActivity.this.userInfoModel.setSignature("");
                MCLogUtil.d(MCConstant.TAG, "nickName3-->" + AuthorizationSuccActivity.this.userInfoModel.getNickname());
                if (AuthorizationSuccActivity.this.updateAsyncTask != null) {
                    AuthorizationSuccActivity.this.updateAsyncTask.cancel(true);
                }
                UpdateAsyncTask unused3 = AuthorizationSuccActivity.this.updateAsyncTask = new UpdateAsyncTask();
                AuthorizationSuccActivity.this.updateAsyncTask.execute(AuthorizationSuccActivity.this.userInfoModel);
            }
        });
        this.registerNowBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AuthorizationSuccActivity.this.regEmailBox.setVisibility(8);
                String email = AuthorizationSuccActivity.this.userEmailEdit.getText().toString();
                if (!StringUtil.isEmail(email)) {
                    AuthorizationSuccActivity.this.warnMessageById("mc_forum_user_email_format_error_warn");
                } else if (email.length() > AuthorizationSuccActivity.this.emailMaxLen) {
                    AuthorizationSuccActivity.this.warnMessageById("mc_forum_user_email_length_error_warn");
                } else {
                    String password = AuthorizationSuccActivity.this.userPwdEdit.getText().toString();
                    if (password.length() < AuthorizationSuccActivity.this.pwdMinLen || password.length() > AuthorizationSuccActivity.this.pwdMaxLen) {
                        AuthorizationSuccActivity.this.warnMessageById("mc_forum_user_password_length_error_warn");
                    } else if (!StringUtil.isPwdMatchRule(password)) {
                        AuthorizationSuccActivity.this.warnMessageById("mc_forum_user_password_format_error_warn");
                    } else {
                        String password2 = MD5Util.toMD5(password);
                        String unused = AuthorizationSuccActivity.this.nickName = AuthorizationSuccActivity.this.nickNameEdit.getText().toString();
                        if (!StringUtil.checkNickNameMinLen(AuthorizationSuccActivity.this.nickName, AuthorizationSuccActivity.this.nickNameMinLen) || !StringUtil.checkNickNameMaxLen(AuthorizationSuccActivity.this.nickName, AuthorizationSuccActivity.this.nickNameMaxLen)) {
                            AuthorizationSuccActivity.this.warnMessageById("mc_forum_user_nickname_length_error");
                            return;
                        }
                        if (AuthorizationSuccActivity.this.iconPath != null) {
                            String unused2 = AuthorizationSuccActivity.this.userIcon = AuthorizationSuccActivity.this.iconPath;
                        }
                        AuthorizationSuccActivity.this.userInfoModel.setIcon(AuthorizationSuccActivity.this.userIcon);
                        AuthorizationSuccActivity.this.userInfoModel.setEmail(email);
                        AuthorizationSuccActivity.this.userInfoModel.setPwd(password2);
                        AuthorizationSuccActivity.this.userInfoModel.setNickname(AuthorizationSuccActivity.this.nickName);
                        AuthorizationSuccActivity.this.userInfoModel.setGender(AuthorizationSuccActivity.this.userGender);
                        AuthorizationSuccActivity.this.userInfoModel.setSignature("");
                        if (AuthorizationSuccActivity.this.saveRegisterAsyncTask != null) {
                            AuthorizationSuccActivity.this.saveRegisterAsyncTask.cancel(true);
                        }
                        SaveRegisterAsyncTask unused3 = AuthorizationSuccActivity.this.saveRegisterAsyncTask = new SaveRegisterAsyncTask();
                        AuthorizationSuccActivity.this.saveRegisterAsyncTask.execute(new UserInfoModel[0]);
                    }
                }
            }
        });
        this.cancelSubmitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AuthorizationSuccActivity.this.goToActivityClass != null) {
                    AuthorizationSuccActivity.this.goToTargetActivity();
                }
                AuthorizationSuccActivity.this.back();
            }
        });
        this.takePhotoBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AuthorizationSuccActivity.this.cameraPhotoListener();
            }
        });
        this.localPhotoBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AuthorizationSuccActivity.this.localPhotoListener();
            }
        });
        this.maleBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int unused = AuthorizationSuccActivity.this.userGender = 1;
                AuthorizationSuccActivity.this.maleRadio.setBackgroundResource(AuthorizationSuccActivity.this.resource.getDrawableId("mc_forum_select2_2"));
                AuthorizationSuccActivity.this.femaleRadio.setBackgroundResource(AuthorizationSuccActivity.this.resource.getDrawableId("mc_forum_select2_1"));
            }
        });
        this.femaleBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int unused = AuthorizationSuccActivity.this.userGender = 0;
                AuthorizationSuccActivity.this.maleRadio.setBackgroundResource(AuthorizationSuccActivity.this.resource.getDrawableId("mc_forum_select2_1"));
                AuthorizationSuccActivity.this.femaleRadio.setBackgroundResource(AuthorizationSuccActivity.this.resource.getDrawableId("mc_forum_select2_2"));
            }
        });
        this.nickNameEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66) {
                    ((InputMethodManager) AuthorizationSuccActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(AuthorizationSuccActivity.this.nickNameEdit.getWindowToken(), 0);
                }
                return false;
            }
        });
    }

    private void showUserIcon() {
        this.recycleUrls.add(AsyncTaskLoaderImage.formatUrl(this.userBaseUrl + this.userIcon, "100x100"));
        if (SharedPreferencesDB.getInstance(this).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(AsyncTaskLoaderImage.formatUrl(this.userBaseUrl + this.userIcon, "100x100"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    AuthorizationSuccActivity.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (!AuthorizationSuccActivity.this.isFinishing()) {
                                if (image != null && !image.isRecycled()) {
                                    AuthorizationSuccActivity.this.headerImg.setBackgroundDrawable(new BitmapDrawable(image));
                                }
                            } else if (image != null && !image.isRecycled()) {
                                image.recycle();
                            }
                        }
                    });
                }
            });
        }
    }

    class SaveRegisterAsyncTask extends AsyncTask<UserInfoModel, Void, UserInfoModel> {
        SaveRegisterAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            AuthorizationSuccActivity.this.showProgressDialog("mc_forum_warn_update", this);
        }

        /* access modifiers changed from: protected */
        public UserInfoModel doInBackground(UserInfoModel... params) {
            return new UserServiceImpl(AuthorizationSuccActivity.this).regUserByOpenPlatform(AuthorizationSuccActivity.this.userInfoModel.getEmail(), AuthorizationSuccActivity.this.userInfoModel.getIcon(), AuthorizationSuccActivity.this.userInfoModel.getPwd(), AuthorizationSuccActivity.this.userInfoModel.getGender(), AuthorizationSuccActivity.this.userInfoModel.getNickname(), AuthorizationSuccActivity.this.platformId, AuthorizationSuccActivity.this.platformUserId);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(UserInfoModel result) {
            AuthorizationSuccActivity.this.hideProgressDialog();
            if (result == null) {
                AuthorizationSuccActivity.this.warnMessageById("mc_forum_user_update_succ");
                if (AuthorizationSuccActivity.this.goToActivityClass != null) {
                    AuthorizationSuccActivity.this.goToTargetActivity();
                }
                AuthorizationSuccActivity.this.back();
            } else if (result.getErrorCode() == null || "".equals(result.getErrorCode())) {
                AuthorizationSuccActivity.this.warnMessageById("mc_forum_user_register_succ");
                AuthorizationSuccActivity.this.clearNotification();
                AuthorizationSuccActivity.this.back();
            } else {
                AuthorizationSuccActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(AuthorizationSuccActivity.this, result.getErrorCode()));
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        backEventClickListener();
        back();
        return true;
    }

    /* access modifiers changed from: private */
    public void notifyData(String str) {
        new ArrayList();
        this.regLoginListAdapter.setEmailSections(EmailResourceConstant.getResourceConstant().convertEmail(str));
        this.regLoginListAdapter.notifyDataSetChanged();
        this.regLoginListAdapter.notifyDataSetInvalidated();
    }

    class UpdateAsyncTask extends AsyncTask<UserInfoModel, Void, String> {
        UpdateAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            AuthorizationSuccActivity.this.showProgressDialog("mc_forum_warn_update", this);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(UserInfoModel... params) {
            UserService userService = new UserServiceImpl(AuthorizationSuccActivity.this);
            if (params[0] == null) {
                return null;
            }
            MCLogUtil.d("UpdateAsyncTask", "nickName update-->" + AuthorizationSuccActivity.this.userInfoModel.getNickname());
            return userService.updateUser(AuthorizationSuccActivity.this.userInfoModel.getNickname(), AuthorizationSuccActivity.this.userInfoModel.getIcon(), AuthorizationSuccActivity.this.userInfoModel.getGender(), AuthorizationSuccActivity.this.userInfoModel.getSignature(), -1);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            AuthorizationSuccActivity.this.hideProgressDialog();
            if (result == null) {
                AuthorizationSuccActivity.this.warnMessageById("mc_forum_user_update_succ");
                if (AuthorizationSuccActivity.this.goToActivityClass != null) {
                    AuthorizationSuccActivity.this.goToTargetActivity();
                }
                AuthorizationSuccActivity.this.clearNotification();
                AuthorizationSuccActivity.this.back();
            } else if (!result.equals("")) {
                AuthorizationSuccActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(AuthorizationSuccActivity.this, result));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void exitNoSaveEvent() {
    }

    /* access modifiers changed from: protected */
    public void exitSaveEvent() {
    }

    /* access modifiers changed from: protected */
    public boolean exitCheckChanged() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void updateUIAfterUpload() {
        if (this.bitmap != null && !this.bitmap.isRecycled()) {
            this.headerImg.setBackgroundDrawable(new BitmapDrawable(this.bitmap));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.updateAsyncTask != null) {
            this.updateAsyncTask.cancel(true);
        }
        if (this.saveRegisterAsyncTask != null) {
            this.saveRegisterAsyncTask.cancel(true);
        }
    }

    /* access modifiers changed from: protected */
    public void goToTargetActivity() {
        if (this.goToActivityClass != null) {
            Intent intent2 = new Intent(this, this.goToActivityClass);
            if (this.goParam != null) {
                for (String key : this.goParam.keySet()) {
                    intent2.putExtra(key, this.goParam.get(key));
                }
            }
            startActivity(intent2);
        }
    }
}
