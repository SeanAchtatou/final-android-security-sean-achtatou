package com.mobcent.base.android.ui.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.base.android.ui.activity.adapter.ModeratorBoardAdapter;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.model.UserBoardInfoModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.ModeratorService;
import com.mobcent.forum.android.service.impl.ModeratorServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import java.util.List;

public class ModeratorActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public ModeratorBoardAdapter adapter;
    private Button backBtn;
    /* access modifiers changed from: private */
    public List<UserBoardInfoModel> boardList;
    private GridView gridView;
    /* access modifiers changed from: private */
    public ProgressDialog loadBoardDialog;
    /* access modifiers changed from: private */
    public ModeratorService moderatorService;
    /* access modifiers changed from: private */
    public SetModeratorTask moderatorTask;
    /* access modifiers changed from: private */
    public long replyPostsId;
    private Button saveBtn;
    /* access modifiers changed from: private */
    public long topicId;
    private UserBoardListTask userBoardListTask;
    /* access modifiers changed from: private */
    public long userId;

    /* access modifiers changed from: protected */
    public void initData() {
        this.adapter = new ModeratorBoardAdapter(this);
        this.userId = getIntent().getLongExtra("userId", 0);
        this.topicId = getIntent().getLongExtra("topicId", 0);
        this.replyPostsId = getIntent().getLongExtra("replyPostsId", 0);
        this.moderatorService = new ModeratorServiceImpl(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_moderator_activity"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.saveBtn = (Button) findViewById(this.resource.getViewId("mc_forum_save_btn"));
        this.gridView = (GridView) findViewById(this.resource.getViewId("mc_forum_board_view"));
        this.loadBoardDialog = new ProgressDialog(this);
        this.gridView.setAdapter((ListAdapter) this.adapter);
        if (this.userBoardListTask != null) {
            this.userBoardListTask.cancel(true);
        }
        this.userBoardListTask = new UserBoardListTask();
        this.userBoardListTask.execute(Long.valueOf(this.userId));
        this.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                UserBoardInfoModel boardModel = (UserBoardInfoModel) ModeratorActivity.this.boardList.get(position);
                ImageView selectImg = (ImageView) view.findViewById(ModeratorActivity.this.resource.getViewId("mc_forum_select_btn"));
                if (boardModel.getIsModerator() == 0) {
                    boardModel.setIsModerator(1);
                    selectImg.setBackgroundResource(ModeratorActivity.this.resource.getDrawableId("mc_forum_select1_2"));
                    return;
                }
                selectImg.setBackgroundResource(ModeratorActivity.this.resource.getDrawableId("mc_forum_select1_1"));
                boardModel.setIsModerator(0);
            }
        });
    }

    /* access modifiers changed from: private */
    public String getBoardIds() {
        String boardIds = "";
        for (int i = 0; i < this.boardList.size(); i++) {
            UserBoardInfoModel boardModel = this.boardList.get(i);
            if (boardModel.getIsModerator() == 1) {
                boardIds = boardIds + boardModel.getBoardId() + AdApiConstant.RES_SPLIT_COMMA;
            }
        }
        if (boardIds.length() > 0) {
            return boardIds.substring(0, boardIds.length() - 1);
        }
        return boardIds;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ModeratorActivity.this.back();
            }
        });
        this.saveBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ModeratorActivity.this.moderatorTask != null) {
                    ModeratorActivity.this.moderatorTask.cancel(true);
                }
                SetModeratorTask unused = ModeratorActivity.this.moderatorTask = new SetModeratorTask();
                ModeratorActivity.this.moderatorTask.execute(new Long[0]);
            }
        });
    }

    class UserBoardListTask extends AsyncTask<Long, Void, UserInfoModel> {
        UserBoardListTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            ModeratorActivity.this.loadBoardDialog.setMessage(ModeratorActivity.this.getResources().getString(ModeratorActivity.this.resource.getStringId("mc_forum_loading_board")));
            ModeratorActivity.this.loadBoardDialog.show();
        }

        /* access modifiers changed from: protected */
        public UserInfoModel doInBackground(Long... params) {
            return new UserServiceImpl(ModeratorActivity.this).getUserBoardInfoList(params[0].longValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(UserInfoModel result) {
            super.onPostExecute((Object) result);
            if (ModeratorActivity.this.loadBoardDialog != null && ModeratorActivity.this.loadBoardDialog.isShowing()) {
                ModeratorActivity.this.loadBoardDialog.dismiss();
            }
            if (result == null) {
                return;
            }
            if (result.getErrorCode() == null || "".equals(result.getErrorCode())) {
                List unused = ModeratorActivity.this.boardList = result.getUserBoardInfoList();
                ModeratorActivity.this.adapter.setBoardList(result.getUserBoardInfoList());
                ModeratorActivity.this.adapter.notifyDataSetInvalidated();
                ModeratorActivity.this.adapter.notifyDataSetChanged();
                return;
            }
            ModeratorActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(ModeratorActivity.this, result.getErrorCode()));
        }
    }

    class SetModeratorTask extends AsyncTask<Long, Void, String> {
        SetModeratorTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            ModeratorActivity.this.loadBoardDialog.setMessage(ModeratorActivity.this.getResources().getString(ModeratorActivity.this.resource.getStringId("mc_forum_loading_moderator")));
            ModeratorActivity.this.loadBoardDialog.show();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Long... params) {
            return ModeratorActivity.this.moderatorService.setModerator(ModeratorActivity.this.userId, ModeratorActivity.this.getBoardIds(), ModeratorActivity.this.topicId, ModeratorActivity.this.replyPostsId);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            super.onPostExecute((Object) result);
            if (ModeratorActivity.this.loadBoardDialog != null && ModeratorActivity.this.loadBoardDialog.isShowing()) {
                ModeratorActivity.this.loadBoardDialog.dismiss();
            }
            if (result != null) {
                Toast.makeText(ModeratorActivity.this, MCForumErrorUtil.convertErrorCode(ModeratorActivity.this, result), 0).show();
                return;
            }
            Toast.makeText(ModeratorActivity.this, ModeratorActivity.this.resource.getStringId("mc_forum_moderator_succ"), 0).show();
            ModeratorActivity.this.finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.userBoardListTask != null) {
            this.userBoardListTask.cancel(true);
        }
        if (this.moderatorTask != null) {
            this.moderatorTask.cancel(true);
        }
    }
}
