package com.mobcent.base.android.ui.widget.pointimageview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.ImageView;

public class PointImageView extends ImageView {
    static final float SCALE_RATE = 1.25f;
    float _dy;
    private float baseValue;
    private GestureDetector gestureScanner;
    private Bitmap image;
    private int imageHeight;
    private int imageWidth;
    private Matrix mBaseMatrix;
    private final Matrix mDisplayMatrix;
    public Handler mHandler;
    private final float[] mMatrixValues;
    private float mMaxZoom;
    private float mMinZoom;
    private Matrix mSuppMatrix;
    private int mThisHeight;
    private int mThisWidth;
    private float originalScale;
    /* access modifiers changed from: private */
    public PointImageViewSingleTapListener pointImageViewSingleTapListener;
    private float scaleRate;
    /* access modifiers changed from: private */
    public int screenHeight;
    /* access modifiers changed from: private */
    public int screenWidth;
    /* access modifiers changed from: private */
    public ImageViewPager viewPager;

    public interface PointImageViewSingleTapListener {
        void onSingleTap(MotionEvent motionEvent);
    }

    public PointImageView(Context context) {
        this(context, null);
    }

    public PointImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mBaseMatrix = new Matrix();
        this.mSuppMatrix = new Matrix();
        this.mDisplayMatrix = new Matrix();
        this.mMatrixValues = new float[9];
        this.image = null;
        this.mThisWidth = -1;
        this.mThisHeight = -1;
        this.mMaxZoom = 2.0f;
        this.mHandler = new Handler();
        this._dy = 0.0f;
        init(context);
    }

    public void setViewPager(ImageViewPager viewPager2) {
        this.viewPager = viewPager2;
    }

    public void setPointImageViewSingleTapListener(PointImageViewSingleTapListener pointImageViewSingleTapListener2) {
        this.pointImageViewSingleTapListener = pointImageViewSingleTapListener2;
    }

    private void arithScaleRate() {
        this.scaleRate = Math.min(((float) this.screenWidth) / ((float) this.imageWidth), ((float) this.screenHeight) / ((float) this.imageHeight));
    }

    public float getScaleRate() {
        return this.scaleRate;
    }

    public int getImageWidth() {
        return this.imageWidth;
    }

    public int getImageHeight() {
        return this.imageHeight;
    }

    public void onDraw(Canvas canvas) {
        center(true, true);
        super.onDraw(canvas);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        event.startTracking();
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !event.isTracking() || event.isCanceled() || getScale() <= 1.0f) {
            return super.onKeyUp(keyCode, event);
        }
        zoomTo(1.0f);
        return true;
    }

    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        this.image = bitmap;
        this.imageHeight = this.image.getHeight();
        this.imageWidth = this.image.getWidth();
        arithScaleRate();
        zoomTo(this.scaleRate, ((float) this.screenWidth) / 2.0f, ((float) this.screenHeight) / 2.0f);
        layoutToCenter();
    }

    public void center(boolean horizontal, boolean vertical) {
        if (this.image != null) {
            Matrix m = getImageViewMatrix();
            RectF rect = new RectF(0.0f, 0.0f, (float) this.image.getWidth(), (float) this.image.getHeight());
            m.mapRect(rect);
            float height = rect.height();
            float width = rect.width();
            float deltaX = 0.0f;
            float deltaY = 0.0f;
            if (vertical) {
                int viewHeight = getHeight();
                if (height < ((float) viewHeight)) {
                    deltaY = ((((float) viewHeight) - height) / 2.0f) - rect.top;
                } else if (rect.top > 0.0f) {
                    deltaY = -rect.top;
                } else if (rect.bottom < ((float) viewHeight)) {
                    deltaY = ((float) getHeight()) - rect.bottom;
                }
            }
            if (horizontal) {
                int viewWidth = getWidth();
                if (width < ((float) viewWidth)) {
                    deltaX = ((((float) viewWidth) - width) / 2.0f) - rect.left;
                } else if (rect.left > 0.0f) {
                    deltaX = -rect.left;
                } else if (rect.right < ((float) viewWidth)) {
                    deltaX = ((float) viewWidth) - rect.right;
                }
            }
            postTranslate(deltaX, deltaY);
            setImageMatrix(getImageViewMatrix());
        }
    }

    private void init(Context context) {
        this.gestureScanner = new GestureDetector(new MySimpleGesture());
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        this.screenWidth = dm.widthPixels;
        this.screenHeight = dm.heightPixels;
        setScaleType(ImageView.ScaleType.MATRIX);
    }

    public void layoutToCenter() {
        float width = ((float) this.imageWidth) * getScale();
        float height = ((float) this.imageHeight) * getScale();
        float fill_width = ((float) this.screenWidth) - width;
        float fill_height = ((float) this.screenHeight) - height;
        float tran_width = 0.0f;
        float tran_height = 0.0f;
        if (fill_width > 0.0f) {
            tran_width = fill_width / 2.0f;
        }
        if (fill_height > 0.0f) {
            tran_height = fill_height / 2.0f;
        }
        postTranslate(tran_width, tran_height);
        setImageMatrix(getImageViewMatrix());
    }

    public float getValue(Matrix matrix, int whichValue) {
        matrix.getValues(this.mMatrixValues);
        this.mMinZoom = (((float) this.screenWidth) / 2.0f) / ((float) this.imageWidth);
        return this.mMatrixValues[whichValue];
    }

    public float getScale(Matrix matrix) {
        return getValue(matrix, 0);
    }

    public float getScale() {
        return getScale(this.mSuppMatrix);
    }

    public Matrix getImageViewMatrix() {
        this.mDisplayMatrix.set(this.mBaseMatrix);
        this.mDisplayMatrix.postConcat(this.mSuppMatrix);
        return this.mDisplayMatrix;
    }

    public float maxZoom() {
        if (this.image == null) {
            return 1.0f;
        }
        return Math.max(((float) this.image.getWidth()) / ((float) this.mThisWidth), ((float) this.image.getHeight()) / ((float) this.mThisHeight)) * 4.0f;
    }

    public void zoomTo(float scale, float centerX, float centerY) {
        if (scale > this.mMaxZoom) {
            scale = this.mMaxZoom;
        } else if (scale < this.mMinZoom) {
            scale = this.mMinZoom;
        }
        float deltaScale = scale / getScale();
        this.mSuppMatrix.postScale(deltaScale, deltaScale, centerX, centerY);
        setImageMatrix(getImageViewMatrix());
        center(true, true);
    }

    public void zoomTo(float scale, float centerX, float centerY, float durationMs) {
        final float incrementPerMs = (scale - getScale()) / durationMs;
        final float oldScale = getScale();
        final long startTime = System.currentTimeMillis();
        final float f = durationMs;
        final float f2 = centerX;
        final float f3 = centerY;
        this.mHandler.post(new Runnable() {
            public void run() {
                float currentMs = Math.min(f, (float) (System.currentTimeMillis() - startTime));
                PointImageView.this.zoomTo(oldScale + (incrementPerMs * currentMs), f2, f3);
                if (currentMs < f) {
                    PointImageView.this.mHandler.post(this);
                }
            }
        });
    }

    public void zoomTo(float scale) {
        zoomTo(scale, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f);
    }

    public void zoomToPoint(float scale, float pointX, float pointY) {
        float cx = ((float) getWidth()) / 2.0f;
        float cy = ((float) getHeight()) / 2.0f;
        panBy(cx - pointX, cy - pointY);
        zoomTo(scale, cx, cy);
    }

    public void zoomIn() {
        zoomIn(SCALE_RATE);
    }

    public void zoomOut() {
        zoomOut(SCALE_RATE);
    }

    public void zoomIn(float rate) {
        if (getScale() < this.mMaxZoom && getScale() > this.mMinZoom && this.image != null) {
            this.mSuppMatrix.postScale(rate, rate, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f);
            setImageMatrix(getImageViewMatrix());
        }
    }

    public void zoomOut(float rate) {
        if (this.image != null) {
            float cx = ((float) getWidth()) / 2.0f;
            float cy = ((float) getHeight()) / 2.0f;
            Matrix tmp = new Matrix(this.mSuppMatrix);
            tmp.postScale(1.0f / rate, 1.0f / rate, cx, cy);
            if (getScale(tmp) < 1.0f) {
                this.mSuppMatrix.setScale(1.0f, 1.0f, cx, cy);
            } else {
                this.mSuppMatrix.postScale(1.0f / rate, 1.0f / rate, cx, cy);
            }
            setImageMatrix(getImageViewMatrix());
            center(true, true);
        }
    }

    public void postTranslate(float dx, float dy) {
        this.mSuppMatrix.postTranslate(dx, dy);
        setImageMatrix(getImageViewMatrix());
    }

    public void postTranslateDur(float dy, float durationMs) {
        this._dy = 0.0f;
        final float incrementPerMs = dy / durationMs;
        final long startTime = System.currentTimeMillis();
        final float f = durationMs;
        this.mHandler.post(new Runnable() {
            public void run() {
                float currentMs = Math.min(f, (float) (System.currentTimeMillis() - startTime));
                PointImageView.this.postTranslate(0.0f, (incrementPerMs * currentMs) - PointImageView.this._dy);
                PointImageView.this._dy = incrementPerMs * currentMs;
                if (currentMs < f) {
                    PointImageView.this.mHandler.post(this);
                }
            }
        });
    }

    public void panBy(float dx, float dy) {
        postTranslate(dx, dy);
        setImageMatrix(getImageViewMatrix());
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.gestureScanner.onTouchEvent(event);
        switch (event.getAction()) {
            case 0:
                this.baseValue = 0.0f;
                this.originalScale = getScale();
                break;
            case 1:
                if (getScale() < getScaleRate()) {
                    zoomTo(getScaleRate(), (float) (this.screenWidth / 2), (float) (this.screenHeight / 2), 200.0f);
                    break;
                }
                break;
            case 2:
                if (event.getPointerCount() == 2) {
                    changeViewPagerState(false);
                    float x = event.getX(0) - event.getX(1);
                    float y = event.getY(0) - event.getY(1);
                    float value = (float) Math.sqrt((double) ((x * x) + (y * y)));
                    if (this.baseValue != 0.0f) {
                        zoomTo(this.originalScale * (value / this.baseValue), event.getX(1) + x, event.getY(1) + y);
                        break;
                    } else {
                        this.baseValue = value;
                        break;
                    }
                }
                break;
        }
        return true;
    }

    private class MySimpleGesture extends GestureDetector.SimpleOnGestureListener {
        private MySimpleGesture() {
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (PointImageView.this.pointImageViewSingleTapListener == null) {
                return true;
            }
            PointImageView.this.pointImageViewSingleTapListener.onSingleTap(e);
            return true;
        }

        public boolean onDoubleTap(MotionEvent e) {
            if (PointImageView.this.getScale() > PointImageView.this.getScaleRate()) {
                PointImageView.this.changeViewPagerState(false);
                PointImageView.this.zoomTo(PointImageView.this.getScaleRate(), (float) (PointImageView.this.screenWidth / 2), (float) (PointImageView.this.screenHeight / 2), 200.0f);
            } else {
                PointImageView.this.changeViewPagerState(true);
                PointImageView.this.zoomTo(1.0f, (float) (PointImageView.this.screenWidth / 2), (float) (PointImageView.this.screenHeight / 2), 200.0f);
            }
            return true;
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            int count = 1;
            if (PointImageView.this.viewPager != null) {
                count = PointImageView.this.viewPager.getAdapter().getCount();
            }
            float[] v = new float[9];
            PointImageView.this.getImageMatrix().getValues(v);
            float width = PointImageView.this.getScale() * ((float) PointImageView.this.getImageWidth());
            float height = PointImageView.this.getScale() * ((float) PointImageView.this.getImageHeight());
            if (((int) width) > PointImageView.this.screenWidth || ((int) height) > PointImageView.this.screenHeight) {
                float left = v[2];
                float right = left + width;
                Rect r = new Rect();
                PointImageView.this.getGlobalVisibleRect(r);
                if (distanceX > 0.0f) {
                    if (r.left > 0 && count > 1) {
                        PointImageView.this.changeViewPagerState(true);
                        return false;
                    } else if (right > ((float) PointImageView.this.screenWidth) || count <= 1) {
                        PointImageView.this.changeViewPagerState(false);
                        PointImageView.this.postTranslate(-distanceX, -distanceY);
                        return false;
                    } else {
                        PointImageView.this.changeViewPagerState(true);
                        return false;
                    }
                } else if (distanceX >= 0.0f) {
                    return false;
                } else {
                    if (r.right < PointImageView.this.screenWidth && count > 1) {
                        PointImageView.this.changeViewPagerState(true);
                        return false;
                    } else if (left != 0.0f || count <= 1) {
                        PointImageView.this.changeViewPagerState(false);
                        PointImageView.this.postTranslate(-distanceX, -distanceY);
                        return false;
                    } else {
                        PointImageView.this.changeViewPagerState(true);
                        return false;
                    }
                }
            } else {
                PointImageView.this.changeViewPagerState(true);
                return false;
            }
        }
    }

    public void changeViewPagerState(boolean isFlag) {
        if (this.viewPager != null) {
            this.viewPager.setScroll(isFlag);
        }
    }
}
