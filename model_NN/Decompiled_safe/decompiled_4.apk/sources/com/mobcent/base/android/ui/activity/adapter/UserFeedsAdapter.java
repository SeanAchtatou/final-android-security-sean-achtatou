package com.mobcent.base.android.ui.activity.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.adapter.holder.FeedUserAdapterHolder;
import com.mobcent.base.android.ui.activity.view.MCLevelView;
import com.mobcent.forum.android.constant.FeedsConstant;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.model.UserTopicFeedModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.DateUtil;
import java.util.List;

public class UserFeedsAdapter extends AbstractFeedsAdapter implements FeedsConstant {
    private List<UserTopicFeedModel> feedsList;
    /* access modifiers changed from: private */
    public AlertDialog userFunctionDialog;
    /* access modifiers changed from: private */
    public UserInfoModel userInfoModel;

    public UserFeedsAdapter(Context context, LayoutInflater inflater, Handler mHandler, AsyncTaskLoaderImage asyncTaskLoaderImage, UserInfoModel userInfoModel2, ListView listView, List<UserTopicFeedModel> feedsList2) {
        super(context, inflater, mHandler, asyncTaskLoaderImage, listView);
        this.userInfoModel = userInfoModel2;
        this.feedsList = feedsList2;
    }

    public int getCount() {
        return this.feedsList.size();
    }

    public Object getItem(int position) {
        return this.feedsList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public List<UserTopicFeedModel> getFeedsList() {
        return this.feedsList;
    }

    public void setFeedsList(List<UserTopicFeedModel> feedsList2) {
        this.feedsList = feedsList2;
    }

    public UserInfoModel getUserInfoModel() {
        return this.userInfoModel;
    }

    public void setUserInfoModel(UserInfoModel userInfoModel2) {
        this.userInfoModel = userInfoModel2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        return getUserItemView(position, convertView, parent);
    }

    private View getUserItemView(int position, View convertView, ViewGroup parent) {
        View convertView2 = getUserConvertView(convertView);
        FeedUserAdapterHolder holder = (FeedUserAdapterHolder) convertView2.getTag();
        holder.getUserNameText().setText(this.userInfoModel.getNickname());
        showUserRole(holder.getRoleText(), this.userInfoModel.getRoleNum());
        updateUserIcon(this.userInfoModel.getIcon(), holder.getIconImg());
        holder.getLevelBox().removeAllViews();
        new MCLevelView(this.context, this.userInfoModel.getLevel(), holder.getLevelBox(), 0);
        holder.getUserTopicBox().setVisibility(8);
        holder.getUserReplyBox().setVisibility(8);
        holder.getFollowUserBox().setVisibility(8);
        holder.getRegisterUserBox().setVisibility(8);
        UserTopicFeedModel userTopicFeed = this.feedsList.get(position);
        if (userTopicFeed.getFtype() == 1 || userTopicFeed.getFtype() == 6 || userTopicFeed.getFtype() == 10 || userTopicFeed.getFtype() == 11 || userTopicFeed.getFtype() == 7 || userTopicFeed.getFtype() == 8 || userTopicFeed.getFtype() == 5) {
            holder.getUserTopicBox().setVisibility(0);
            holder.getUserTopicBox().removeAllViews();
            holder.getUserTopicBox().addView(getUserTopicView(userTopicFeed.getTopic(), userTopicFeed.getFtype()));
        } else if (userTopicFeed.getFtype() == 2) {
            holder.getUserReplyBox().setVisibility(0);
            holder.getUserReplyBox().removeAllViews();
            holder.getUserReplyBox().addView(getUserReplyView(userTopicFeed.getTopic(), userTopicFeed.getReply(), userTopicFeed.getReplyQuote()));
        } else if (userTopicFeed.getFtype() == 4) {
            holder.getFollowUserBox().setVisibility(0);
            holder.getFollowUserNameText().setText(userTopicFeed.getToUser().getNickname());
            holder.getFollowTimeText().setText(DateUtil.getFormatTimeByYear(userTopicFeed.getTime()));
        } else if (userTopicFeed.getFtype() == 9) {
            holder.getRegisterUserBox().setVisibility(0);
            holder.getRegisterTimeText().setText(DateUtil.getFormatTimeByYear(userTopicFeed.getTime()));
        }
        holder.getIconImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (UserFeedsAdapter.this.userFunctionDialog == null) {
                    AlertDialog unused = UserFeedsAdapter.this.userFunctionDialog = UserFeedsAdapter.this.getUserFunctionDialog(UserFeedsAdapter.this.userInfoModel);
                } else {
                    UserFeedsAdapter.this.userFunctionDialog.show();
                }
            }
        });
        return convertView2;
    }

    private View getUserConvertView(View convertView) {
        FeedUserAdapterHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_user_feeds_item"), (ViewGroup) null);
            holder = new FeedUserAdapterHolder();
            initFeedUserAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            try {
                holder = (FeedUserAdapterHolder) convertView.getTag();
            } catch (Exception e) {
                holder = null;
            }
        }
        if (holder != null) {
            return convertView;
        }
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_user_feeds_item"), (ViewGroup) null);
        FeedUserAdapterHolder holder2 = new FeedUserAdapterHolder();
        initFeedUserAdapterHolder(convertView2, holder2);
        convertView2.setTag(holder2);
        return convertView2;
    }

    private void initFeedUserAdapterHolder(View convertView, FeedUserAdapterHolder holder) {
        holder.setIconImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_feeds_icon")));
        holder.setUserNameText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_user_name")));
        holder.setRoleText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_role_text")));
        holder.setLevelBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_level_box")));
        holder.setFeedsBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_feeds_box")));
        holder.setUserTopicBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_user_topic_box")));
        holder.setUserReplyBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_user_reply_box")));
        holder.setFollowUserBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_follow_user_box")));
        holder.setRegisterUserBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_register_user_box")));
        holder.setFollowTimeText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_follow_time_text")));
        holder.setFollowUserNameText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_follow_user_name_text")));
        holder.setRegisterText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_register_text")));
        holder.setRegisterTimeText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_register_time_text")));
    }
}
