package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.mobcent.base.forum.android.util.MCResource;
import java.util.LinkedHashMap;

public class FaceImageAdapter extends BaseAdapter {
    private Context context;
    private LinkedHashMap<String, Integer> hashMap;
    private LayoutInflater inflater = LayoutInflater.from(this.context);
    private String[] keyArray;
    private MCResource resource = MCResource.getInstance(this.context);

    public FaceImageAdapter(Context context2, LinkedHashMap<String, Integer> hashMap2) {
        this.context = context2;
        this.hashMap = hashMap2;
        this.keyArray = (String[]) hashMap2.keySet().toArray(new String[0]);
    }

    public int getCount() {
        return this.hashMap.size();
    }

    public Integer getItem(int position) {
        return this.hashMap.get(this.keyArray[position]);
    }

    public String getItemKey(int position) {
        return this.keyArray[position];
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_publish_pic_face_item"), (ViewGroup) null);
        ((ImageView) convertView2.findViewById(this.resource.getViewId("mc_forum_face_img"))).setImageResource(getItem(position).intValue());
        return convertView2;
    }
}
