package com.mobcent.base.android.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.mobcent.ad.android.ui.widget.AdView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshBase;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshWaterFall;
import com.mobcent.base.forum.android.util.MCBitmapImageCache;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.constant.MCForumConstant;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.FlowTag;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PicTopicListActivity extends BaseActivity {
    private static Set<PicTopicListActivity> activitys = null;
    /* access modifiers changed from: private */
    public AdView adView;
    private Button backBtn;
    /* access modifiers changed from: private */
    public Map<String, Bitmap> bitmapMap;
    /* access modifiers changed from: private */
    public Map<String, ImageView> imageMap;
    /* access modifiers changed from: private */
    public boolean isLocal = false;
    /* access modifiers changed from: private */
    public MoreTask moreTask;
    /* access modifiers changed from: private */
    public int page = 1;
    /* access modifiers changed from: private */
    public int pageSize = 30;
    /* access modifiers changed from: private */
    public PullToRefreshWaterFall pullToRefreshWaterFall;
    /* access modifiers changed from: private */
    public RefreshTask refreshTask;
    /* access modifiers changed from: private */
    public boolean showPic = false;
    /* access modifiers changed from: private */
    public List<TopicModel> topicList;

    static /* synthetic */ int access$908(PicTopicListActivity x0) {
        int i = x0.page;
        x0.page = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (activitys == null) {
            activitys = new HashSet();
        }
        for (PicTopicListActivity act : activitys) {
            act.finish();
        }
        activitys.add(this);
        checkPicModel();
        this.isLocal = true;
        this.pullToRefreshWaterFall.onRefreshWithOutListener();
        if (this.refreshTask != null) {
            this.refreshTask.cancel(true);
        }
        if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
            this.refreshTask = new RefreshTask();
            this.refreshTask.execute(new Void[0]);
            return;
        }
        this.pullToRefreshWaterFall.onRefreshComplete();
        warnMessageByStr(this.resource.getString("mc_forum_permission_cannot_visit_forum"));
    }

    private void checkPicModel() {
        AlertDialog.Builder exitAlertDialog = new AlertDialog.Builder(this).setTitle(this.resource.getStringId("mc_forum_dialog_tip")).setMessage(this.resource.getStringId("mc_forum_warn_pic_list_title"));
        exitAlertDialog.setPositiveButton(this.resource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                boolean unused = PicTopicListActivity.this.showPic = true;
            }
        });
        exitAlertDialog.setNegativeButton(this.resource.getStringId("mc_forum_dialog_cancel"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                boolean unused = PicTopicListActivity.this.showPic = false;
            }
        }).create();
        if (!SharedPreferencesDB.getInstance(this).getPicModeFlag()) {
            this.showPic = false;
            exitAlertDialog.show();
            return;
        }
        this.showPic = true;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.imageMap = new HashMap();
        this.bitmapMap = new HashMap();
        this.topicList = new ArrayList();
        this.permService = new PermServiceImpl(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_pic_topic_list_activity"));
        this.pullToRefreshWaterFall = (PullToRefreshWaterFall) findViewById(this.resource.getViewId("mc_forum_list"));
        this.pullToRefreshWaterFall.setColumnCount(3);
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.adView = new AdView(this, null);
        this.adView.setPadding(5, 0, 5, 0);
        this.pullToRefreshWaterFall.initView(getApplicationContext(), this.adView);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PicTopicListActivity.this.back();
            }
        });
        this.pullToRefreshWaterFall.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            public void onRefresh() {
                boolean unused = PicTopicListActivity.this.isLocal = false;
                if (PicTopicListActivity.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
                    RefreshTask unused2 = PicTopicListActivity.this.refreshTask = new RefreshTask();
                    PicTopicListActivity.this.refreshTask.execute(new Void[0]);
                    return;
                }
                PicTopicListActivity.this.pullToRefreshWaterFall.onRefreshComplete();
                PicTopicListActivity.this.warnMessageByStr(PicTopicListActivity.this.resource.getString("mc_forum_permission_cannot_visit_forum"));
            }
        });
        this.pullToRefreshWaterFall.setOnBottomRefreshListener(new PullToRefreshBase.OnBottomRefreshListener() {
            public void onRefresh() {
                boolean unused = PicTopicListActivity.this.isLocal = false;
                if (PicTopicListActivity.this.moreTask != null) {
                    PicTopicListActivity.this.moreTask.cancel(true);
                }
                MoreTask unused2 = PicTopicListActivity.this.moreTask = new MoreTask();
                PicTopicListActivity.this.moreTask.execute(new Void[0]);
            }
        });
        this.pullToRefreshWaterFall.setOnLoadItemListener(new PullToRefreshWaterFall.OnLoadItemListener() {
            public void recycleImage(String imageUrl) {
                if (PicTopicListActivity.this.imageMap.get(imageUrl) != null) {
                    ((ImageView) PicTopicListActivity.this.imageMap.get(imageUrl)).setImageBitmap(null);
                }
                if (PicTopicListActivity.this.bitmapMap.get(imageUrl) != null && !((Bitmap) PicTopicListActivity.this.bitmapMap.get(imageUrl)).isRecycled()) {
                    ((Bitmap) PicTopicListActivity.this.bitmapMap.get(imageUrl)).recycle();
                    PicTopicListActivity.this.bitmapMap.remove(imageUrl);
                }
            }

            public void onItemClick(int currentPosition, FlowTag flowTag) {
                TopicModel topicModel = (TopicModel) PicTopicListActivity.this.topicList.get(currentPosition);
                if (topicModel.getUserId() == new UserServiceImpl(PicTopicListActivity.this).getLoginUserId()) {
                    PicTopicListActivity.this.clickEvent(topicModel);
                } else if (PicTopicListActivity.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.READ, -1) != 1 || PicTopicListActivity.this.permService.getPermNum(PermConstant.BOARDS, PermConstant.READ, topicModel.getBoardId()) != 1) {
                    PicTopicListActivity.this.warnMessageByStr(PicTopicListActivity.this.resource.getString("mc_forum_permission_cannot_read_topic"));
                } else if (topicModel.getVisible() != 3) {
                    PicTopicListActivity.this.clickEvent(topicModel);
                } else if (StringUtil.isEmpty(PicTopicListActivity.this.permService.getGroupType()) || !PermConstant.SYSTEM.equals(PicTopicListActivity.this.permService.getGroupType())) {
                    Toast.makeText(PicTopicListActivity.this, PicTopicListActivity.this.resource.getString("mc_forum_permission_only_moderator_see"), 0).show();
                } else {
                    PicTopicListActivity.this.clickEvent(topicModel);
                }
            }

            public void loadLayout(LinearLayout linearLayout, FlowTag flowTag) {
                ImageView imageView = new ImageView(PicTopicListActivity.this.getApplicationContext());
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-1, -1);
                imageView.setBackgroundResource(PicTopicListActivity.this.themeResource.getDrawableId("mc_forum_list9_li_bg"));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                linearLayout.addView(imageView, lp);
                PicTopicListActivity.this.imageMap.put(flowTag.getThumbnailUrl(), imageView);
            }

            public void loadImage(final String imageUrl, boolean isLoad) {
                if (PicTopicListActivity.this.showPic) {
                    MCBitmapImageCache.getInstance(PicTopicListActivity.this.getApplicationContext(), "fallwall").loadAsync(MCBitmapImageCache.formatUrl(imageUrl, MCForumConstant.RESOLUTION_240X320), new MCBitmapImageCache.BitmapImageCallback() {
                        public void onImageLoaded(final Bitmap image, String url) {
                            PicTopicListActivity.this.mHandler.post(new Runnable() {
                                public void run() {
                                    if (image != null && !image.isRecycled()) {
                                        if (PicTopicListActivity.this.imageMap.get(imageUrl) != null) {
                                            ((ImageView) PicTopicListActivity.this.imageMap.get(imageUrl)).setImageBitmap(image);
                                        }
                                        PicTopicListActivity.this.bitmapMap.put(imageUrl, image);
                                    } else if (PicTopicListActivity.this.imageMap.get(imageUrl) != null) {
                                        ((ImageView) PicTopicListActivity.this.imageMap.get(imageUrl)).setImageDrawable(PicTopicListActivity.this.themeResource.getDrawable("mc_forum_list9_x_img"));
                                    }
                                }
                            });
                        }
                    });
                }
            }

            public View getImgBox(FlowTag flowTag) {
                return null;
            }
        });
    }

    /* access modifiers changed from: private */
    public void clickEvent(TopicModel topicModel) {
        Intent intent = new Intent(this, PostsActivity.class);
        intent.putExtra("boardId", topicModel.getBoardId());
        intent.putExtra("boardName", "");
        intent.putExtra("topicId", topicModel.getTopicId());
        intent.putExtra(MCConstant.TOPIC_USER_ID, topicModel.getUserId());
        intent.putExtra("baseUrl", topicModel.getBaseUrl());
        intent.putExtra(MCConstant.THUMBNAIL_IMAGE_URL, topicModel.getPicPath());
        intent.putExtra("type", topicModel.getType());
        intent.putExtra(MCConstant.TOP, topicModel.getTop());
        intent.putExtra(MCConstant.ESSENCE, topicModel.getEssence());
        intent.putExtra(MCConstant.CLOSE, topicModel.getStatus());
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onTheme() {
        super.onTheme();
    }

    class RefreshTask extends AsyncTask<Void, Void, List<TopicModel>> {
        RefreshTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<TopicModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            PicTopicListActivity.this.imageMap.clear();
            int unused = PicTopicListActivity.this.page = 1;
            PicTopicListActivity.this.adView.free();
        }

        /* access modifiers changed from: protected */
        public List<TopicModel> doInBackground(Void... params) {
            return new PostsServiceImpl(PicTopicListActivity.this).getPicTopicList(PicTopicListActivity.this.page, PicTopicListActivity.this.pageSize, PicTopicListActivity.this.isLocal);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<TopicModel> result) {
            PicTopicListActivity.this.pullToRefreshWaterFall.onRefreshComplete();
            super.onPostExecute((Object) result);
            if (result == null || result.isEmpty()) {
                PicTopicListActivity.this.pullToRefreshWaterFall.onBottomRefreshComplete(3, PicTopicListActivity.this.getString(PicTopicListActivity.this.resource.getStringId("mc_forum_no_topic_receive")));
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                Toast.makeText(PicTopicListActivity.this, MCForumErrorUtil.convertErrorCode(PicTopicListActivity.this, result.get(0).getErrorCode()), 0).show();
                PicTopicListActivity.this.pullToRefreshWaterFall.onBottomRefreshComplete(3);
            } else {
                PicTopicListActivity.this.adView.showAd(new Integer(PicTopicListActivity.this.resource.getString("mc_forum_pic_topic_list_position")).intValue());
                PicTopicListActivity.this.pullToRefreshWaterFall.onDrawWaterFall(result, 0);
                if (result.get(0).getHasNext() == 1) {
                    PicTopicListActivity.this.pullToRefreshWaterFall.onBottomRefreshComplete(0);
                } else if (result.get(0).getHasNext() == -1) {
                    PicTopicListActivity.this.pullToRefreshWaterFall.onBottomRefreshComplete(3, MCStringBundleUtil.resolveString(PicTopicListActivity.this.resource.getStringId("mc_forum_last_update"), MCStringBundleUtil.timeToString(PicTopicListActivity.this, new Long(result.get(0).getLastUpdate()).longValue()), PicTopicListActivity.this));
                    PicTopicListActivity.this.pullToRefreshWaterFall.onRefresh();
                } else {
                    PicTopicListActivity.this.pullToRefreshWaterFall.onBottomRefreshComplete(3);
                }
                List unused = PicTopicListActivity.this.topicList = result;
            }
        }
    }

    class MoreTask extends AsyncTask<Void, Void, List<TopicModel>> {
        MoreTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<TopicModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            PicTopicListActivity.access$908(PicTopicListActivity.this);
        }

        /* access modifiers changed from: protected */
        public List<TopicModel> doInBackground(Void... params) {
            return new PostsServiceImpl(PicTopicListActivity.this).getPicTopicList(PicTopicListActivity.this.page, PicTopicListActivity.this.pageSize, PicTopicListActivity.this.isLocal);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<TopicModel> result) {
            super.onPostExecute((Object) result);
            if (result == null || result.isEmpty()) {
                PicTopicListActivity.this.pullToRefreshWaterFall.onBottomRefreshComplete(3, PicTopicListActivity.this.getString(PicTopicListActivity.this.resource.getStringId("mc_forum_no_topic_receive")));
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                Toast.makeText(PicTopicListActivity.this, MCForumErrorUtil.convertErrorCode(PicTopicListActivity.this, result.get(0).getErrorCode()), 0).show();
                PicTopicListActivity.this.pullToRefreshWaterFall.onBottomRefreshComplete(3);
            } else {
                PicTopicListActivity.this.pullToRefreshWaterFall.onDrawWaterFall(result, 1);
                if (result.get(0).getHasNext() == 1) {
                    PicTopicListActivity.this.pullToRefreshWaterFall.onBottomRefreshComplete(0);
                } else {
                    PicTopicListActivity.this.pullToRefreshWaterFall.onBottomRefreshComplete(3);
                }
                PicTopicListActivity.this.topicList.addAll(result);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.pullToRefreshWaterFall.onDestroyView();
        activitys.remove(this);
        if (this.refreshTask != null) {
            this.refreshTask.cancel(true);
        }
        if (this.moreTask != null) {
            this.moreTask.cancel(true);
        }
        this.adView.free();
    }
}
