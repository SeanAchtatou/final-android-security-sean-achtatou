package com.mobcent.base.android.ui.activity.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.FeedsAdapter;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.BaseAsyncTaskLoader;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.model.FeedsModel;
import com.mobcent.forum.android.service.impl.FeedsServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class HomeFeedsFragment extends BaseListViewFragment implements LoaderManager.LoaderCallbacks<List<FeedsModel>> {
    private FeedsAdapter adapter;
    private int currentPage = 1;
    private List<FeedsModel> feedsList;
    private boolean hasFooter = false;
    private int pageSize = 20;
    private PullToRefreshListView pullToRefreshListView;
    private List<String> refreshimgUrls;

    public /* bridge */ /* synthetic */ void onLoadFinished(Loader x0, Object x1) {
        onLoadFinished((Loader<List<FeedsModel>>) x0, (List<FeedsModel>) ((List) x1));
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.feedsList = new ArrayList();
        this.refreshimgUrls = new ArrayList();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(this.mcResource.getLayoutId("mc_forum_topic_fragment"), container, false);
        this.pullToRefreshListView = (PullToRefreshListView) this.view.findViewById(this.mcResource.getViewId("mc_forum_lv"));
        this.adapter = new FeedsAdapter(this.activity, inflater, this.feedsList, this.mHandler, this.asyncTaskLoaderImage);
        this.pullToRefreshListView.setOnScrollListener(null);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
        return this.view;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return this.view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.pullToRefreshListView.onRefreshWithOutListener();
        Bundle bundle = new Bundle();
        this.currentPage = 1;
        bundle.putInt("page", this.currentPage);
        bundle.putInt("pageSize", this.pageSize);
        bundle.putBoolean(MCConstant.LOCAL, true);
        getLoaderManager().initLoader(1, bundle, this);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.pullToRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                HomeFeedsFragment.this.onRefreshs();
            }
        });
        this.pullToRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                HomeFeedsFragment.this.onLoadMore();
            }
        });
    }

    public void onRefreshs() {
        if (this.refreshimgUrls != null && !this.refreshimgUrls.isEmpty() && this.refreshimgUrls.size() > 0) {
            this.asyncTaskLoaderImage.recycleBitmaps(this.refreshimgUrls);
        }
        Bundle bundle = new Bundle();
        this.currentPage = 1;
        bundle.putInt("page", this.currentPage);
        bundle.putInt("pageSize", this.pageSize);
        bundle.putBoolean(MCConstant.LOCAL, false);
        getLoaderManager().restartLoader(1, bundle, this);
    }

    public void onLoadMore() {
        this.currentPage++;
        Bundle bundle = new Bundle();
        bundle.putInt("page", this.currentPage);
        bundle.putInt("pageSize", this.pageSize);
        bundle.putBoolean(MCConstant.LOCAL, false);
        if (getLoaderManager().getLoader(2) == null) {
            getLoaderManager().initLoader(2, bundle, this);
        } else {
            getLoaderManager().restartLoader(2, bundle, this);
        }
    }

    public Loader<List<FeedsModel>> onCreateLoader(int arg0, Bundle bundle) {
        return new RefreshTaskLoader(getActivity(), bundle.getInt("page"), bundle.getInt("pageSize"), bundle.getBoolean(MCConstant.LOCAL));
    }

    public void onLoadFinished(Loader<List<FeedsModel>> loader, List<FeedsModel> data) {
        if (loader.getId() == 1) {
            onRefreshDone(data);
        } else {
            onMoreDone(data);
        }
    }

    private void onRefreshDone(List<FeedsModel> data) {
        this.pullToRefreshListView.onRefreshComplete(true);
        if (!this.hasFooter) {
            this.hasFooter = true;
            this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
        }
        if (data == null) {
            Toast.makeText(this.activity, getString(this.mcResource.getStringId("mc_forum_warn_no_such_data")), 1).show();
        } else if (data.isEmpty()) {
            this.pullToRefreshListView.onBottomRefreshComplete(3);
            this.adapter.setFeedsList(data);
            this.adapter.notifyDataSetInvalidated();
            this.adapter.notifyDataSetChanged();
            Toast.makeText(this.activity, getString(this.mcResource.getStringId("mc_forum_warn_no_such_data")), 1).show();
        } else if (!StringUtil.isEmpty(data.get(0).getErrorCode())) {
            Toast.makeText(this.activity, MCForumErrorUtil.convertErrorCode(this.activity, data.get(0).getErrorCode()), 0).show();
            this.pullToRefreshListView.onBottomRefreshComplete(0);
        } else {
            this.refreshimgUrls = getRefreshImgUrl(data);
            this.adapter.setFeedsList(data);
            this.adapter.notifyDataSetInvalidated();
            this.adapter.notifyDataSetChanged();
            if (data.get(0).getHasNext() == 1) {
                this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                this.pullToRefreshListView.onBottomRefreshComplete(3);
            }
            this.feedsList = data;
        }
    }

    private void onMoreDone(List<FeedsModel> data) {
        if (data == null) {
            this.currentPage--;
        } else if (data.isEmpty()) {
            this.pullToRefreshListView.onBottomRefreshComplete(3);
        } else if (!StringUtil.isEmpty(data.get(0).getErrorCode())) {
            this.currentPage--;
            Toast.makeText(this.activity, MCForumErrorUtil.convertErrorCode(this.activity, data.get(0).getErrorCode()), 0).show();
            this.pullToRefreshListView.onBottomRefreshComplete(0);
        } else {
            List<FeedsModel> list = new ArrayList<>();
            list.addAll(this.feedsList);
            list.addAll(data);
            this.adapter.setFeedsList(list);
            this.adapter.notifyDataSetInvalidated();
            this.adapter.notifyDataSetChanged();
            if (data.get(0).getHasNext() == 1) {
                this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                this.pullToRefreshListView.onBottomRefreshComplete(3);
            }
            this.feedsList = list;
        }
    }

    public void onLoaderReset(Loader<List<FeedsModel>> loader) {
        loader.reset();
    }

    public static class RefreshTaskLoader extends BaseAsyncTaskLoader<List<FeedsModel>> {
        private boolean isLocal = false;
        private int page = 0;
        private int pageSize = 0;

        public RefreshTaskLoader(Context context) {
            super(context);
        }

        public RefreshTaskLoader(Context context, int page2, int pageSize2, boolean isLocal2) {
            super(context);
            this.page = page2;
            this.pageSize = pageSize2;
            this.isLocal = isLocal2;
        }

        public List<FeedsModel> loadInBackground() {
            return new FeedsServiceImpl(getContext()).getFeeds(this.page, this.pageSize, this.isLocal);
        }
    }

    /* access modifiers changed from: protected */
    public List<String> getImageURL(int start, int end) {
        List<String> imgUrls = new ArrayList<>();
        for (int i = start; i < end; i++) {
            imgUrls.add(AsyncTaskLoaderImage.formatUrl(this.feedsList.get(i).getIconUrl(), "100x100"));
        }
        return imgUrls;
    }

    private List<String> getRefreshImgUrl(List<FeedsModel> result) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < this.feedsList.size(); i++) {
            FeedsModel model = this.feedsList.get(i);
            if (!isExit(model, result)) {
                list.add(AsyncTaskLoaderImage.formatUrl(model.getIconUrl(), "100x100"));
            }
        }
        return list;
    }

    private boolean isExit(FeedsModel model, List<FeedsModel> result) {
        int j = result.size();
        for (int i = 0; i < j; i++) {
            FeedsModel model2 = result.get(i);
            if (!StringUtil.isEmpty(model.getIconUrl()) && !StringUtil.isEmpty(model2.getIconUrl()) && model.getIconUrl().equals(model2.getIconUrl())) {
                return true;
            }
        }
        return false;
    }
}
