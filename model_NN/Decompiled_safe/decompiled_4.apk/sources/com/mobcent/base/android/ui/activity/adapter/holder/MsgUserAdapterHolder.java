package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.ImageView;
import android.widget.TextView;

public class MsgUserAdapterHolder {
    private TextView msgCountText;
    private ImageView newMsgImg;
    private ImageView userIconImg;
    private TextView userNicknameText;
    private ImageView userRoleImg;

    public ImageView getUserIconImg() {
        return this.userIconImg;
    }

    public void setUserIconImg(ImageView userIconImg2) {
        this.userIconImg = userIconImg2;
    }

    public ImageView getUserRoleImg() {
        return this.userRoleImg;
    }

    public void setUserRoleImg(ImageView userRoleImg2) {
        this.userRoleImg = userRoleImg2;
    }

    public ImageView getNewMsgImg() {
        return this.newMsgImg;
    }

    public void setNewMsgImg(ImageView newMsgImg2) {
        this.newMsgImg = newMsgImg2;
    }

    public TextView getUserNicknameText() {
        return this.userNicknameText;
    }

    public void setUserNicknameText(TextView userNicknameText2) {
        this.userNicknameText = userNicknameText2;
    }

    public TextView getMsgCountText() {
        return this.msgCountText;
    }

    public void setMsgCountText(TextView msgCountText2) {
        this.msgCountText = msgCountText2;
    }
}
