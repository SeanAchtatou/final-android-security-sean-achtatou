package com.mobcent.base.android.ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;
import com.mobcent.base.android.ui.widget.pointimageview.PointImageView;
import com.mobcent.base.forum.android.util.MCBitmapImageCache;
import com.mobcent.forum.android.db.SharedPreferencesDB;

public class PointImageViewActivity extends BaseActivity {
    private String iconUrl;
    /* access modifiers changed from: private */
    public Bitmap imageTemp;
    /* access modifiers changed from: private */
    public ImageView pointLoadingFail;
    /* access modifiers changed from: private */
    public MCProgressBar pointLoadingProgress;
    /* access modifiers changed from: private */
    public PointImageView pointView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.imageTemp != null && !this.imageTemp.isRecycled()) {
            this.imageTemp.recycle();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.iconUrl = (String) getIntent().getSerializableExtra(MCConstant.IMAGE_URL);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_point_imageview"));
        this.pointView = (PointImageView) findViewById(this.resource.getViewId("mc_forum_point_imageview"));
        this.pointLoadingProgress = (MCProgressBar) findViewById(this.resource.getViewId("mc_forum_point_progress"));
        this.pointLoadingProgress.show();
        this.pointLoadingFail = (ImageView) findViewById(this.resource.getViewId("mc_forum_point_loading_fail"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        if (SharedPreferencesDB.getInstance(this).getPicModeFlag()) {
            MCBitmapImageCache.getInstance(getApplicationContext(), "point").loadAsync(MCBitmapImageCache.formatUrl(this.iconUrl, "100x100"), new MCBitmapImageCache.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    PointImageViewActivity.this.mHandler.post(new Runnable() {
                        public void run() {
                            PointImageViewActivity.this.pointLoadingProgress.stop();
                            PointImageViewActivity.this.pointLoadingProgress.setVisibility(8);
                            if (image == null) {
                                PointImageViewActivity.this.pointLoadingFail.setVisibility(0);
                            } else if (!image.isRecycled()) {
                                Bitmap unused = PointImageViewActivity.this.imageTemp = image;
                                PointImageViewActivity.this.pointView.setImageBitmap(image);
                            } else {
                                PointImageViewActivity.this.pointLoadingFail.setVisibility(0);
                            }
                        }
                    });
                }
            });
        }
    }
}
