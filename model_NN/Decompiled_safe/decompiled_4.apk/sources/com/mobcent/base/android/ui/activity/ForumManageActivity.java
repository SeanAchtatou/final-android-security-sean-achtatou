package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.mobcent.base.android.constant.MCConstant;

public class ForumManageActivity extends BaseActivity {
    private TextView announceApplyManage;
    private Button backBtn;
    private TextView bannedUserManage;
    private TextView moderatorManager;
    private TextView reportTopicManage;
    private TextView reportUserManage;
    private TextView shieldedUserManage;

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_manage_activity"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.moderatorManager = (TextView) findViewById(this.resource.getViewId("mc_forum_board_manage_text"));
        this.reportTopicManage = (TextView) findViewById(this.resource.getViewId("mc_forum_report_topic_manage_text"));
        this.bannedUserManage = (TextView) findViewById(this.resource.getViewId("mc_forum_banned_user_manage_text"));
        this.shieldedUserManage = (TextView) findViewById(this.resource.getViewId("mc_forum_shielded_user_manage_text"));
        this.reportUserManage = (TextView) findViewById(this.resource.getViewId("mc_forum_report_user_manage_text"));
        this.announceApplyManage = (TextView) findViewById(this.resource.getViewId("mc_forum_announce_text"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ForumManageActivity.this.back();
            }
        });
        this.moderatorManager.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ForumManageActivity.this.startActivity(new Intent(ForumManageActivity.this, ModeratorManageActivity.class));
            }
        });
        this.reportUserManage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ForumManageActivity.this.startActivity(new Intent(ForumManageActivity.this, ReportUserManageActivity.class));
            }
        });
        this.reportTopicManage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ForumManageActivity.this.startActivity(new Intent(ForumManageActivity.this, ReportTopicManageActivity.class));
            }
        });
        this.bannedUserManage.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
            public void onClick(View v) {
                Intent intent = new Intent(ForumManageActivity.this, BannedManageActivity.class);
                intent.putExtra("boardId", 0L);
                intent.putExtra(MCConstant.BANNED_TYPE, 4);
                ForumManageActivity.this.startActivity(intent);
            }
        });
        this.shieldedUserManage.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
            public void onClick(View v) {
                Intent intent = new Intent(ForumManageActivity.this, ShieldedManageActivity.class);
                intent.putExtra("boardId", 0L);
                intent.putExtra(MCConstant.SHIELDED_TYPE, 1);
                ForumManageActivity.this.startActivity(intent);
            }
        });
        this.announceApplyManage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ForumManageActivity.this.startActivity(new Intent(ForumManageActivity.this, AnnounceApplyManageActivity.class));
            }
        });
    }
}
