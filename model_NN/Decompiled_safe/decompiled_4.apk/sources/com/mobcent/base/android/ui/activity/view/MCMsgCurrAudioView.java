package com.mobcent.base.android.ui.activity.view;

import android.content.Context;
import android.util.AttributeSet;

public class MCMsgCurrAudioView extends MCBaseMsgAudioView {
    public MCMsgCurrAudioView(Context context) {
        super(context);
    }

    public MCMsgCurrAudioView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void initData() {
        this.layoutName = "mc_forum_msg_curr_audio_view";
        this.voiceImgName = "mc_forum_voice_chat2";
    }
}
