package com.mobcent.base.android.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import com.mobcent.ad.android.model.AdIntentModel;
import com.mobcent.ad.android.ui.activity.WebViewActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.BaseFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.plaza.android.api.constant.PlazaApiConstant;
import com.mobcent.plaza.android.service.PlazaService;
import com.mobcent.plaza.android.service.impl.PlazaServiceImpl;
import com.mobcent.plaza.android.service.model.PlazaAppModel;
import com.mobcent.plaza.android.ui.activity.IntentPlazaModel;
import com.mobcent.plaza.android.ui.activity.PlazaSearchActivity;
import com.mobcent.plaza.android.ui.activity.PlazaWebViewActivity;
import com.mobcent.plaza.android.ui.activity.adapter.PlazaViewPagerAdapter;
import com.mobcent.plaza.android.ui.activity.constant.PlazaConstant;
import com.mobcent.plaza.android.ui.activity.delegate.PlazaConfig;
import com.mobcent.plaza.android.ui.activity.fragment.PlazaFragment;
import com.mobcent.update.android.util.MCLogUtil;
import java.util.ArrayList;
import java.util.List;

public class PlazaMyActivity extends BaseFragmentActivity implements PlazaFragment.PlazaFragmentListener, DialogInterface.OnDismissListener, PlazaApiConstant {
    public static final String APP_KEY = "appKey";
    public static final String INTENT_PLAZA_MODEL = "plazaIntentModel";
    public static final String SEARCH_TYPES = "searchTypes";
    public static final String USER_ID = "userId";
    private String TAG = "PlazaActivity";
    /* access modifiers changed from: private */
    public RelativeLayout aboutBox;
    /* access modifiers changed from: private */
    public Button aboutBtn;
    private RelativeLayout baseSearchBox;
    private View beforeView;
    private RelativeLayout channelBox;
    private AsyncTask<Void, Void, List<PlazaAppModel>> currentTask;
    /* access modifiers changed from: private */
    public IntentPlazaModel intentPlazaModel;
    /* access modifiers changed from: private */
    public RelativeLayout loadingBox;
    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (v == PlazaMyActivity.this.personalBox) {
                if (event.getAction() == 1) {
                    PlazaMyActivity.this.personalBtn.setPressed(false);
                } else {
                    PlazaMyActivity.this.personalBtn.setPressed(true);
                }
            } else if (v == PlazaMyActivity.this.setBox) {
                if (event.getAction() == 1) {
                    PlazaMyActivity.this.setBtn.setPressed(false);
                } else {
                    PlazaMyActivity.this.setBtn.setPressed(true);
                }
            } else if (v == PlazaMyActivity.this.aboutBox) {
                if (event.getAction() == 1) {
                    PlazaMyActivity.this.aboutBtn.setPressed(false);
                } else {
                    PlazaMyActivity.this.aboutBtn.setPressed(true);
                }
            }
            return false;
        }
    };
    /* access modifiers changed from: private */
    public RelativeLayout personalBox;
    /* access modifiers changed from: private */
    public Button personalBtn;
    /* access modifiers changed from: private */
    public List<PlazaAppModel> plazaAppModels = new ArrayList();
    /* access modifiers changed from: private */
    public PlazaService plazaService;
    /* access modifiers changed from: private */
    public RelativeLayout setBox;
    /* access modifiers changed from: private */
    public Button setBtn;
    private ViewPager viewPager;
    /* access modifiers changed from: private */
    public PlazaViewPagerAdapter viewPagerAdapter;

    /* access modifiers changed from: protected */
    public void initData() {
        this.plazaService = new PlazaServiceImpl(this);
        this.intentPlazaModel = (IntentPlazaModel) getIntent().getSerializableExtra("plazaIntentModel");
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        requestWindowFeature(1);
        setContentView(this.resource.getLayoutId("mc_plaza_activity"));
        this.viewPager = (ViewPager) findViewById(this.resource.getViewId("view_pager"));
        this.viewPagerAdapter = new PlazaViewPagerAdapter(getSupportFragmentManager(), this.plazaAppModels);
        this.viewPager.setAdapter(this.viewPagerAdapter);
        this.viewPager.setPageMargin(20);
        ((EditText) findViewById(this.resource.getViewId("key_word_edit"))).setFocusable(false);
        this.channelBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_plaza_channel_layout"));
        this.beforeView = findViewById(this.resource.getViewId("mc_plaza_before_view"));
        this.loadingBox = (RelativeLayout) findViewById(this.resource.getViewId("loading_layout"));
        this.baseSearchBox = (RelativeLayout) findViewById(this.resource.getViewId("base_search_layout"));
        this.personalBox = (RelativeLayout) findViewById(this.resource.getViewId("personal_layout"));
        this.setBox = (RelativeLayout) findViewById(this.resource.getViewId("set_layout"));
        this.aboutBox = (RelativeLayout) findViewById(this.resource.getViewId("about_layout"));
        this.personalBtn = (Button) findViewById(this.resource.getViewId("personal_btn"));
        this.setBtn = (Button) findViewById(this.resource.getViewId("set_btn"));
        this.aboutBtn = (Button) findViewById(this.resource.getViewId("about_btn"));
        if (this.intentPlazaModel != null) {
            if (this.intentPlazaModel.getSearchTypes() == null || this.intentPlazaModel.getSearchTypes().length == 0) {
                this.baseSearchBox.setVisibility(8);
            } else if (this.intentPlazaModel.getSearchTypes().length == 1) {
                this.channelBox.setVisibility(8);
            }
            getDataTask();
        } else {
            this.baseSearchBox.setVisibility(8);
        }
        MCForumHelper.getForumConfig().initNav(this, MCResource.getInstance(this));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.beforeView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PlazaConfig.getInstance().getPlazaDelegate() != null) {
                    long userId = PlazaConfig.getInstance().getPlazaDelegate().getUserId(PlazaMyActivity.this);
                    if (userId > 0) {
                        PlazaMyActivity.this.intentPlazaModel.setUserId(userId);
                        Intent intent = new Intent(PlazaMyActivity.this, PlazaSearchActivity.class);
                        intent.putExtra("plazaIntentModel", PlazaMyActivity.this.intentPlazaModel);
                        PlazaMyActivity.this.startActivity(intent);
                    }
                }
            }
        });
        this.loadingBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlazaMyActivity.this.loadingBox.setVisibility(8);
                PlazaMyActivity.this.setProgressBarVisibility(false);
            }
        });
        this.personalBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PlazaConfig.getInstance().getPlazaDelegate() != null) {
                    PlazaConfig.getInstance().getPlazaDelegate().onPersonalClick(PlazaMyActivity.this);
                }
            }
        });
        this.setBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PlazaConfig.getInstance().getPlazaDelegate() != null) {
                    PlazaConfig.getInstance().getPlazaDelegate().onSetClick(PlazaMyActivity.this);
                }
            }
        });
        this.aboutBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PlazaConfig.getInstance().getPlazaDelegate() != null) {
                    PlazaConfig.getInstance().getPlazaDelegate().onAboutClick(PlazaMyActivity.this);
                }
            }
        });
        this.personalBox.setOnTouchListener(this.onTouchListener);
        this.setBox.setOnTouchListener(this.onTouchListener);
        this.aboutBox.setOnTouchListener(this.onTouchListener);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    private void getDataTask() {
        this.currentTask = new GetDataTask().execute(new Void[0]);
    }

    class GetDataTask extends AsyncTask<Void, Void, List<PlazaAppModel>> {
        GetDataTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<PlazaAppModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            PlazaMyActivity.this.loadingBox.setVisibility(0);
            PlazaMyActivity.this.setProgressBarVisibility(true);
        }

        /* access modifiers changed from: protected */
        public List<PlazaAppModel> doInBackground(Void... params) {
            List<PlazaAppModel> listTemp = PlazaMyActivity.this.plazaService.getPlazaAppModelListByLocal();
            if (listTemp == null) {
                return PlazaMyActivity.this.plazaService.getPlazaAppModelListByNet(PlazaMyActivity.this.intentPlazaModel.getForumKey(), PlazaMyActivity.this.intentPlazaModel.getUserId());
            }
            new GetNetDataUpdateLocal().execute(new Void[0]);
            return listTemp;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<PlazaAppModel> result) {
            PlazaMyActivity.this.loadingBox.setVisibility(8);
            PlazaMyActivity.this.setProgressBarVisibility(false);
            if (result != null) {
                if (!result.isEmpty()) {
                    PlazaMyActivity.this.plazaAppModels.clear();
                    if (PlazaMyActivity.this.intentPlazaModel.getPlazaAppModels() != null) {
                        PlazaMyActivity.this.plazaAppModels.addAll(PlazaMyActivity.this.intentPlazaModel.getPlazaAppModels());
                    }
                    PlazaMyActivity.this.plazaAppModels.addAll(result);
                    PlazaMyActivity.this.viewPagerAdapter.setPlazaAppModels(PlazaMyActivity.this.plazaAppModels);
                    PlazaMyActivity.this.viewPagerAdapter.notifyDataSetChanged();
                }
                result.clear();
            }
        }
    }

    class GetNetDataUpdateLocal extends AsyncTask<Void, Void, List<PlazaAppModel>> {
        GetNetDataUpdateLocal() {
        }

        /* access modifiers changed from: protected */
        public List<PlazaAppModel> doInBackground(Void... params) {
            return PlazaMyActivity.this.plazaService.getPlazaAppModelListByNet(PlazaMyActivity.this.intentPlazaModel.getForumKey(), PlazaMyActivity.this.intentPlazaModel.getUserId());
        }
    }

    public void onDismiss(DialogInterface dialog) {
        cancelTask();
    }

    private void cancelTask() {
        if (this.currentTask != null && !this.currentTask.isCancelled()) {
            this.currentTask.cancel(true);
        }
    }

    public void onBackPressed() {
        if (this.loadingBox.getVisibility() == 0) {
            this.loadingBox.setVisibility(8);
            setProgressBarVisibility(false);
        } else if (PlazaConfig.getInstance().getPlazaDelegate() == null) {
            super.onBackPressed();
        } else if (!PlazaConfig.getInstance().getPlazaDelegate().onPlazaBackPressed(this)) {
            super.onBackPressed();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void plazaAppImgClick(PlazaAppModel paAppModel) {
        Intent intent;
        String url = this.plazaService.getPlazaLinkUrl(this.intentPlazaModel.getForumKey(), paAppModel.getModelId(), this.intentPlazaModel.getUserId());
        if (paAppModel.getModelAction() == 0) {
            if (PlazaConfig.getInstance().getPlazaDelegate() != null) {
                PlazaConfig.getInstance().getPlazaDelegate().onAppItemClick(this, paAppModel);
            }
        } else if (paAppModel.getModelAction() == 3) {
            Intent intent2 = new Intent(this, WebViewActivity.class);
            AdIntentModel adIntentModel = new AdIntentModel();
            adIntentModel.setAid(paAppModel.getModelId());
            adIntentModel.setPo(PlazaApiConstant.PLAZA_POSITION);
            adIntentModel.setUrl(url);
            intent2.putExtra(WebViewActivity.AD_INTENT_MODEL, adIntentModel);
            startActivity(intent2);
        } else {
            MCLogUtil.e(this.TAG, "request url -- " + url);
            if (paAppModel.getModelAction() == 4) {
                intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse(url));
            } else {
                intent = new Intent(this, PlazaWebViewActivity.class);
                intent.putExtra(PlazaConstant.WEB_VIEW_URL, url);
                if (paAppModel.getModelAction() == 1) {
                    intent.putExtra(PlazaConstant.WEB_VIEW_TOP, true);
                } else if (paAppModel.getModelAction() == 2) {
                    intent.putExtra(PlazaConstant.WEB_VIEW_TOP, false);
                }
            }
            startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return null;
    }
}
