package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.ImageView;
import android.widget.TextView;

public class PlatformLoginListAdapterHolder {
    private ImageView iconImg;
    private TextView nameText;

    public ImageView getIconImg() {
        return this.iconImg;
    }

    public void setIconImg(ImageView iconImg2) {
        this.iconImg = iconImg2;
    }

    public TextView getNameText() {
        return this.nameText;
    }

    public void setNameText(TextView nameText2) {
        this.nameText = nameText2;
    }
}
