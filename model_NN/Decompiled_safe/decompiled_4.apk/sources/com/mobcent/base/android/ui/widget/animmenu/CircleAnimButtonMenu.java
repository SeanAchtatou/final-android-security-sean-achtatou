package com.mobcent.base.android.ui.widget.animmenu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import java.util.List;

public class CircleAnimButtonMenu extends BaseAnimButtonMenu {
    private View.OnClickListener menuItemClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            AnimButtonItem pathAnimItem = (AnimButtonItem) v;
            Animation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(500);
            AnimationSet animationSet = new AnimationSet(CircleAnimButtonMenu.this.getContext(), null);
            animationSet.addAnimation(alphaAnimation);
            animationSet.addAnimation(CircleAnimButtonMenu.this.getScaleAnim(((Integer) pathAnimItem.getTag()).intValue(), 1.0f, 2.0f));
            pathAnimItem.startAnimation(animationSet);
            int curentIndex = ((Integer) pathAnimItem.getTag()).intValue();
            int size = CircleAnimButtonMenu.this.animButtonItemList.size();
            for (int i = 0; i < size; i++) {
                if (curentIndex != i) {
                    AnimButtonItem tempAnimItem = (AnimButtonItem) CircleAnimButtonMenu.this.animButtonItemList.get(i);
                    AnimationSet animationSet1 = new AnimationSet(CircleAnimButtonMenu.this.getContext(), null);
                    animationSet1.addAnimation(alphaAnimation);
                    animationSet1.addAnimation(CircleAnimButtonMenu.this.getScaleAnim(((Integer) tempAnimItem.getTag()).intValue(), 1.0f, 0.0f));
                    tempAnimItem.startAnimation(animationSet1);
                }
            }
            if (CircleAnimButtonMenu.this.animButtonMenuListener != null) {
                CircleAnimButtonMenu.this.animButtonMenuListener.didSelectedItem(pathAnimItem, ((Integer) pathAnimItem.getTag()).intValue());
            }
            CircleAnimButtonMenu.this.isExpand = false;
        }
    };
    private View.OnClickListener menuMainClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            CircleAnimButtonMenu.this.isExpand = !CircleAnimButtonMenu.this.isExpand;
            if (CircleAnimButtonMenu.this.isExpand) {
                CircleAnimButtonMenu.this.animMainButton.startAnimation(CircleAnimButtonMenu.this.animRotate(0.0f, -45.0f, 0.5f, 0.5f, 200));
                CircleAnimButtonMenu.this.expendAnim();
                return;
            }
            CircleAnimButtonMenu.this.animMainButton.startAnimation(CircleAnimButtonMenu.this.animRotate(-45.0f, 0.0f, 0.5f, 0.5f, 200));
            CircleAnimButtonMenu.this.closedAnim();
        }
    };

    public CircleAnimButtonMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
    }

    public void addAllItems(List<AnimButtonItem> pathAnimItemList, AnimButtonItem animMainButton) {
        initAllParams(pathAnimItemList.get(0));
        this.animButtonItemList = pathAnimItemList;
        int size = this.animButtonItemList.size();
        double angle = 0.017453292519943295d * ((double) (90 / (size - 1)));
        for (int i = 0; i < size; i++) {
            AnimButtonItem eachAnimItem = (AnimButtonItem) this.animButtonItemList.get(i);
            eachAnimItem.setStartPoint(this.startPoint);
            double sin = Math.sin(((double) i) * angle);
            double cos = Math.cos(((double) i) * angle);
            eachAnimItem.setEndPoint(new ButtonPoint(this.startPoint.x + ((int) (((double) this.END_LENGTH) * sin)), this.startPoint.y - ((int) (((double) this.END_LENGTH) * cos))));
            eachAnimItem.setNearPoint(new ButtonPoint(this.startPoint.x + ((int) (((double) this.NEAR_LENGTH) * sin)), this.startPoint.y - ((int) (((double) this.NEAR_LENGTH) * cos))));
            eachAnimItem.setFarPoint(new ButtonPoint(this.startPoint.x + ((int) (((double) this.FAR_LENGTH) * sin)), this.startPoint.y - ((int) (((double) this.FAR_LENGTH) * cos))));
            eachAnimItem.setOnClickListener(this.menuItemClickListener);
            addView(eachAnimItem);
            eachAnimItem.setLayoutParams(this.menuItemLp);
            eachAnimItem.setTag(Integer.valueOf(i));
        }
        this.animMainButton = animMainButton;
        animMainButton.setStartPoint(this.startPoint);
        animMainButton.setOnClickListener(this.menuMainClickListener);
        animMainButton.setLayoutParams(this.menuMainLp);
        addView(animMainButton);
        this.isExpand = false;
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void initAllParams(AnimButtonItem pathAnimItem) {
        super.initAllParams(pathAnimItem);
    }
}
