package com.mobcent.base.android.ui.activity.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;
import com.mobcent.base.android.ui.activity.fragment.ImageViewerFragment;
import com.mobcent.forum.android.constant.MCForumConstant;
import com.mobcent.forum.android.model.RichImageModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ImageViewerFragmentAdapter extends FragmentStatePagerAdapter {
    private Map<Integer, ImageViewerFragment> fragmentMap = new HashMap();
    private int imageSourceType;
    private List<RichImageModel> imageUrlList;

    public void setImageSourceType(int imageSourceType2) {
        this.imageSourceType = imageSourceType2;
    }

    public void setImageUrlList(List<RichImageModel> imageUrlList2) {
        this.imageUrlList = imageUrlList2;
    }

    public ImageViewerFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    public Fragment getItem(int position) {
        if (this.fragmentMap.get(Integer.valueOf(position)) == null) {
            ImageViewerFragment imageViewerFragment = ImageViewerFragment.newInstance();
            RichImageModel imgModel = this.imageUrlList.get(position);
            imgModel.setImageUrl(AsyncTaskLoaderImage.formatUrl(this.imageUrlList.get(position).getImageUrl(), MCForumConstant.RESOLUTION_ORIGINAL));
            imageViewerFragment.setImageModel(imgModel);
            imageViewerFragment.setImageSourceType(this.imageSourceType);
            this.fragmentMap.put(Integer.valueOf(position), imageViewerFragment);
        }
        return this.fragmentMap.get(Integer.valueOf(position));
    }

    public void destroyItem(ViewGroup container, int position, Object object) {
        this.fragmentMap.remove(Integer.valueOf(position));
        super.destroyItem(container, position, object);
    }

    public int getCount() {
        return this.imageUrlList.size();
    }
}
