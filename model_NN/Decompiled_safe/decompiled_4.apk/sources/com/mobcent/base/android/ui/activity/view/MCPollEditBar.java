package com.mobcent.base.android.ui.activity.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.base.forum.android.util.MCResource;

public class MCPollEditBar {
    private EditText contentEdit;
    /* access modifiers changed from: private */
    public Context context;
    private ImageView deleImg;
    /* access modifiers changed from: private */
    public MCResource forumResource;
    private LayoutInflater layoutInflater;
    private int num;
    private TextView numText;
    /* access modifiers changed from: private */
    public PollEditBarDelegate pollEditBarDelegate;
    /* access modifiers changed from: private */
    public View view;

    public interface PollEditBarDelegate {
        void deletePollBar(View view, MCPollEditBar mCPollEditBar);

        void onEditClick(View view);
    }

    public MCPollEditBar(Activity activity, int num2, PollEditBarDelegate delegate) {
        this.layoutInflater = activity.getLayoutInflater();
        this.forumResource = MCResource.getInstance(activity);
        this.pollEditBarDelegate = delegate;
        this.num = num2;
        this.context = activity;
        initVoteBar();
        initVoteBarActions();
        updateVoteBar();
    }

    public MCPollEditBar(Activity activity, int num2, String s, PollEditBarDelegate delete) {
        this(activity, num2, delete);
        this.contentEdit.setText(s);
    }

    private void updateVoteBar() {
        this.numText.setText(this.num + "");
    }

    private void initVoteBar() {
        this.view = this.layoutInflater.inflate(this.forumResource.getLayoutId("mc_forum_widget_poll_edit_item"), (ViewGroup) null);
        this.numText = (TextView) this.view.findViewById(this.forumResource.getViewId("mc_forum_publish_vote_num_text"));
        this.contentEdit = (EditText) this.view.findViewById(this.forumResource.getViewId("mc_forum_publish_vote_conent_edit"));
        this.deleImg = (ImageView) this.view.findViewById(this.forumResource.getViewId("mc_forum_publish_vote_delete_img"));
    }

    private void initVoteBarActions() {
        this.deleImg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MCPollEditBar.this.getEditContent().length() > 0) {
                    AlertDialog.Builder exitAlertDialog = new AlertDialog.Builder(MCPollEditBar.this.context).setTitle(MCPollEditBar.this.forumResource.getStringId("mc_forum_dialog_tip")).setMessage(MCPollEditBar.this.forumResource.getStringId("mc_forum_warn_poll_delete"));
                    exitAlertDialog.setNegativeButton(MCPollEditBar.this.forumResource.getStringId("mc_forum_dialog_cancel"), (DialogInterface.OnClickListener) null);
                    exitAlertDialog.setPositiveButton(MCPollEditBar.this.forumResource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            MCPollEditBar.this.pollEditBarDelegate.deletePollBar(MCPollEditBar.this.view, MCPollEditBar.this);
                        }
                    });
                    exitAlertDialog.show();
                    return;
                }
                MCPollEditBar.this.pollEditBarDelegate.deletePollBar(MCPollEditBar.this.view, MCPollEditBar.this);
            }
        });
        this.contentEdit.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                MCPollEditBar.this.pollEditBarDelegate.onEditClick(v);
                return false;
            }
        });
    }

    public View getView() {
        return this.view;
    }

    public String getEditContent() {
        return this.contentEdit.getText().toString();
    }

    public TextView getNum() {
        return this.numText;
    }

    public void hideDeleteBtn() {
        this.deleImg.setVisibility(4);
    }

    public void showDeleteBtn() {
        this.deleImg.setVisibility(0);
    }
}
