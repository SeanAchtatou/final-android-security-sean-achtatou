package com.mobcent.base.android.ui.widget.gifview;

import android.content.Context;
import android.os.AsyncTask;
import com.mobcent.forum.android.service.impl.FileTransferServiceImpl;
import com.mobcent.forum.android.util.MCLibIOUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class GifCache extends AsyncTask<Void, Void, InputStream> {
    public static String LOCAL_POSITION_DIR = (MCLibIOUtil.FS + "mobcent" + MCLibIOUtil.FS + "autogen" + MCLibIOUtil.FS + "imageCache" + MCLibIOUtil.FS);
    public static HashMap<String, InputStream> _isCache;
    private static Context context;
    private static String imagePath = null;
    private GifCallback gifCallback;
    private String url;

    public interface GifCallback {
        void onGifLoaded(InputStream inputStream, String str);
    }

    public GifCache(Context context2, String url2, GifCallback gifCallback2) {
        _isCache = new HashMap<>();
        this.gifCallback = gifCallback2;
        context = context2;
        this.url = url2;
        imagePath = createFileDir();
    }

    public static String getHash(String url2) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(url2.getBytes());
            return new BigInteger(digest.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            return url2;
        }
    }

    private InputStream inputStreamFromCache(String hash) {
        if (_isCache.containsKey(hash)) {
            return _isCache.get(hash);
        }
        return null;
    }

    private String createFileDir() {
        String imagePath2 = MCLibIOUtil.getBaseLocalLocation(context) + LOCAL_POSITION_DIR;
        if (MCLibIOUtil.isDirExist(imagePath2) || MCLibIOUtil.makeDirs(imagePath2)) {
            return imagePath2;
        }
        return null;
    }

    private InputStream getInputStream(String fileName) {
        try {
            return new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public InputStream doInBackground(Void... params) {
        File f;
        String hash = getHash(this.url);
        InputStream is = inputStreamFromCache(hash);
        if (imagePath == null) {
            f = new File(context.getCacheDir(), hash);
        } else {
            f = new File(imagePath + hash);
        }
        if (is == null) {
            if (!f.exists()) {
                new FileTransferServiceImpl(context).downloadFile(this.url, f);
            }
            if (f.exists() && (is = getInputStream(f.getAbsolutePath())) == null) {
                f.delete();
            }
            if (is != null) {
                _isCache.put(hash, is);
            }
        }
        return is;
    }

    public static void removeInputStream(InputStream is) {
        File f;
        if (_isCache != null && _isCache.containsValue(is)) {
            for (String hash : _isCache.keySet()) {
                if (is.equals(_isCache.get(hash))) {
                    if (imagePath == null) {
                        f = new File(context.getCacheDir(), hash);
                    } else {
                        f = new File(imagePath + hash);
                    }
                    if (f != null) {
                        _isCache.put(hash, null);
                        f.delete();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(InputStream is) {
        this.gifCallback.onGifLoaded(is, this.url);
    }

    public static void setImageCacheDir(String imageCacheDir) {
        LOCAL_POSITION_DIR = imageCacheDir;
    }
}
