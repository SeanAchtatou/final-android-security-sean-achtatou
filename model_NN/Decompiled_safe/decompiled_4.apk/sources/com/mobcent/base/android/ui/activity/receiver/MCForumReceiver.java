package com.mobcent.base.android.ui.activity.receiver;

import android.content.Context;
import android.content.IntentFilter;
import com.mobcent.forum.android.os.service.PopNoticeOSService;
import com.mobcent.update.android.constant.MCUpdateConstant;
import com.mobcent.update.android.ui.activity.receiver.UpdateNoticeReceiver;

public class MCForumReceiver {
    private Context context;
    private PopNoticeReceiver popNoticeReceiver = null;
    private UpdateNoticeReceiver updateRecevier;

    public MCForumReceiver(Context context2) {
        this.context = context2;
    }

    public void regBroadcastReceiver() {
        if (this.popNoticeReceiver == null) {
            this.popNoticeReceiver = new PopNoticeReceiver(this.context);
        }
        IntentFilter filterNotice = new IntentFilter();
        filterNotice.addAction(this.context.getPackageName() + PopNoticeOSService.POP_NOTICE_SERVER_MSG);
        this.context.registerReceiver(this.popNoticeReceiver, filterNotice);
        this.updateRecevier = new UpdateNoticeReceiver();
        IntentFilter updatefilter = new IntentFilter();
        updatefilter.addAction(this.context.getPackageName() + MCUpdateConstant.UPDATE_NOTICE_SERVER_MSG);
        this.context.registerReceiver(this.updateRecevier, updatefilter);
    }

    public void unregBroadcastReceiver() {
        if (this.popNoticeReceiver != null) {
            this.context.unregisterReceiver(this.popNoticeReceiver);
        }
        if (this.updateRecevier != null) {
            this.context.unregisterReceiver(this.updateRecevier);
        }
    }
}
