package com.mobcent.base.android.ui.widget.animmenu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.RelativeLayout;
import java.util.List;

public class PullDownAnimButtonMenu extends BaseAnimButtonMenu {
    private View.OnClickListener menuItemClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            AnimButtonItem pathAnimItem = (AnimButtonItem) v;
            Animation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(500);
            AnimationSet animationSet = new AnimationSet(PullDownAnimButtonMenu.this.getContext(), null);
            animationSet.addAnimation(alphaAnimation);
            animationSet.addAnimation(PullDownAnimButtonMenu.this.getScaleAnim(((Integer) pathAnimItem.getTag()).intValue(), 1.0f, 2.0f));
            pathAnimItem.startAnimation(animationSet);
            int curentIndex = ((Integer) pathAnimItem.getTag()).intValue();
            int size = PullDownAnimButtonMenu.this.animButtonItemList.size();
            for (int i = 0; i < size; i++) {
                if (curentIndex != i) {
                    AnimButtonItem tempAnimItem = (AnimButtonItem) PullDownAnimButtonMenu.this.animButtonItemList.get(i);
                    AnimationSet animationSet1 = new AnimationSet(PullDownAnimButtonMenu.this.getContext(), null);
                    animationSet1.addAnimation(alphaAnimation);
                    animationSet1.addAnimation(PullDownAnimButtonMenu.this.getScaleAnim(((Integer) tempAnimItem.getTag()).intValue(), 1.0f, 0.0f));
                    tempAnimItem.startAnimation(animationSet1);
                }
            }
            if (PullDownAnimButtonMenu.this.animButtonMenuListener != null) {
                PullDownAnimButtonMenu.this.animButtonMenuListener.didSelectedItem(pathAnimItem, ((Integer) pathAnimItem.getTag()).intValue());
            }
            PullDownAnimButtonMenu.this.isExpand = false;
        }
    };
    private View.OnClickListener menuMainClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            PullDownAnimButtonMenu.this.isExpand = !PullDownAnimButtonMenu.this.isExpand;
            if (PullDownAnimButtonMenu.this.isExpand) {
                PullDownAnimButtonMenu.this.animMainButton.startAnimation(PullDownAnimButtonMenu.this.animRotate(0.0f, 180.0f, 0.5f, 0.5f, 400));
                PullDownAnimButtonMenu.this.expendAnim();
                return;
            }
            PullDownAnimButtonMenu.this.animMainButton.startAnimation(PullDownAnimButtonMenu.this.animRotate(180.0f, 360.0f, 0.5f, 0.5f, 400));
            PullDownAnimButtonMenu.this.closedAnim();
        }
    };

    public PullDownAnimButtonMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
    }

    public void addAllItems(List<AnimButtonItem> pathAnimItemList, AnimButtonItem animMainButton) {
        initAllParams(pathAnimItemList.get(0));
        this.animButtonItemList = pathAnimItemList;
        int size = this.animButtonItemList.size();
        for (int i = 0; i < size; i++) {
            AnimButtonItem eachAnimItem = (AnimButtonItem) this.animButtonItemList.get(i);
            eachAnimItem.setStartPoint(this.startPoint);
            eachAnimItem.setEndPoint(new ButtonPoint(this.startPoint.x, (this.startPoint.y + this.END_LENGTH) - ((this.END_LENGTH / size) * i)));
            eachAnimItem.setNearPoint(new ButtonPoint(this.startPoint.x, (this.startPoint.y + this.NEAR_LENGTH) - ((this.END_LENGTH / size) * i)));
            eachAnimItem.setFarPoint(new ButtonPoint(this.startPoint.x, (this.startPoint.y + this.FAR_LENGTH) - ((this.END_LENGTH / size) * i)));
            eachAnimItem.setOnClickListener(this.menuItemClickListener);
            addView(eachAnimItem);
            eachAnimItem.setLayoutParams(this.menuItemLp);
            eachAnimItem.setTag(Integer.valueOf(i));
        }
        this.animMainButton = animMainButton;
        animMainButton.setStartPoint(this.startPoint);
        animMainButton.setOnClickListener(this.menuMainClickListener);
        animMainButton.setLayoutParams(this.menuMainLp);
        addView(animMainButton);
        this.isExpand = false;
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void initAllParams(AnimButtonItem pathAnimItem) {
        this.MENU_BAR_HEIGHT = getLayoutParams().height;
        this.MENU_ITEM_HEIGHT = pathAnimItem.getViewHeight();
        this.MENU_MAIN_HEIGHT = this.MENU_ITEM_HEIGHT;
        this.FAR_LENGTH = (this.MENU_BAR_HEIGHT - 10) - this.MENU_MAIN_HEIGHT;
        this.END_LENGTH = (this.FAR_LENGTH / 20) * 19;
        this.NEAR_LENGTH = (this.FAR_LENGTH / 20) * 18;
        this.startPoint = new ButtonPoint(10, 10);
        this.menuItemLp = new RelativeLayout.LayoutParams(this.MENU_ITEM_HEIGHT, this.MENU_ITEM_HEIGHT);
        this.menuItemLp.topMargin = this.startPoint.y;
        this.menuItemLp.leftMargin = this.startPoint.x;
        this.menuMainLp = this.menuItemLp;
    }
}
