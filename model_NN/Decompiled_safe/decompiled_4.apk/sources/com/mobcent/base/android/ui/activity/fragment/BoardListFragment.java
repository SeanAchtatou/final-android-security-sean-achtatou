package com.mobcent.base.android.ui.activity.fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.ad.android.ui.widget.manager.AdManager;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.BoardListAdapter;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.BoardCategoryModel;
import com.mobcent.forum.android.model.ForumModel;
import com.mobcent.forum.android.service.BoardService;
import com.mobcent.forum.android.service.impl.BoardServiceImpl;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class BoardListFragment extends BasePullDownListFragment implements MCConstant {
    /* access modifiers changed from: private */
    public String TAG = "BoardListFragment";
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public BoardListAdapter boardListAdapter;
    /* access modifiers changed from: private */
    public ExpandableListView boardListView;
    /* access modifiers changed from: private */
    public BoardService boardService;
    private List<BoardCategoryModel> categoryList;
    /* access modifiers changed from: private */
    public ForumModel forumModel;
    /* access modifiers changed from: private */
    public boolean isNeedNetUpdate = true;
    private boolean isStartHeartBeatService = false;
    /* access modifiers changed from: private */
    public TextView lastUpdateText;
    /* access modifiers changed from: private */
    public ObtainForumInfoTask obtainForumInfoTask;
    public RelativeLayout publishTypeBox;
    /* access modifiers changed from: private */
    public TextView refreshText;
    private LinearLayout refreshView;
    private View statusView;
    public RelativeLayout transparentLayout;
    /* access modifiers changed from: private */
    public UpdateForumInfoTask updateForumInfoTask;

    /* access modifiers changed from: protected */
    public void initData() {
        this.activity = getActivity();
        this.categoryList = new ArrayList();
        this.boardService = new BoardServiceImpl(this.activity);
        this.forumModel = new ForumModel();
        this.forumModel.setBoardCategoryList(this.categoryList);
        try {
            this.isStartHeartBeatService = this.activity.getPackageManager().getActivityInfo(this.activity.getComponentName(), AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER).metaData.getBoolean(MCConstant.IS_STAR_HEAR_BEAT_SERVICE);
            if (this.isStartHeartBeatService) {
                MCForumHelper.prepareToLaunchForum(this.activity);
            }
        } catch (Exception e) {
        }
        this.permService = new PermServiceImpl(this.activity);
    }

    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String title;
        this.view = inflater.inflate(this.mcResource.getLayoutId("mc_forum_board_list_fragment"), (ViewGroup) null);
        initListView(this.view);
        this.boardListView = (ExpandableListView) this.mListView;
        try {
            title = getString(this.mcResource.getStringId("app_name"));
        } catch (Exception e) {
            title = null;
        }
        if (StringUtil.isEmpty(title)) {
            String title2 = getString(this.mcResource.getStringId("mc_forum_default_title_name"));
        }
        initStatusView();
        initRefreshView();
        this.boardListView.addFooterView(this.statusView);
        if (MCForumHelper.isCopyright(this.activity)) {
            this.view.findViewById(this.mcResource.getViewId("mc_forum_power_by")).setVisibility(8);
        }
        if (this.obtainForumInfoTask != null) {
            this.obtainForumInfoTask.cancel(true);
        }
        if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
            this.obtainForumInfoTask = new ObtainForumInfoTask();
            this.obtainForumInfoTask.execute(true);
        } else {
            endUpdate();
            warnMessageByStr(this.mcResource.getString("mc_forum_permission_cannot_visit_forum"));
        }
        this.boardListAdapter = new BoardListAdapter(this.activity, new ArrayList(), inflater);
        this.boardListView.setAdapter(this.boardListAdapter);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void onTheme() {
        super.onTheme();
    }

    private void initStatusView() {
        this.statusView = ((LayoutInflater) this.activity.getSystemService("layout_inflater")).inflate(this.mcResource.getLayoutId("mc_forum_board_foot_item"), (ViewGroup) null);
    }

    private void initRefreshView() {
        this.refreshView = (LinearLayout) this.statusView.findViewById(this.mcResource.getViewId("mc_forum_refresh_box"));
        this.refreshText = (TextView) this.statusView.findViewById(this.mcResource.getViewId("mc_forum_refresh_text"));
        this.lastUpdateText = (TextView) this.statusView.findViewById(this.mcResource.getViewId("mc_forum_last_update_text"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
    }

    private class ObtainForumInfoTask extends AsyncTask<Boolean, Void, ForumModel> {
        private ObtainForumInfoTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            BoardListFragment.this.onHeadLoading();
            BoardListFragment.this.boardListView.setSelection(0);
        }

        /* access modifiers changed from: protected */
        public ForumModel doInBackground(Boolean... params) {
            if (params[0].booleanValue()) {
                ForumModel forumModel = BoardListFragment.this.boardService.getForumInfo();
                boolean unused = BoardListFragment.this.isNeedNetUpdate = true;
                if (forumModel != null) {
                    return forumModel;
                }
                ForumModel forumModel2 = BoardListFragment.this.boardService.getForumInfoByNet();
                boolean unused2 = BoardListFragment.this.isNeedNetUpdate = false;
                return forumModel2;
            }
            ForumModel forumModel3 = BoardListFragment.this.boardService.getForumInfoByNet();
            boolean unused3 = BoardListFragment.this.isNeedNetUpdate = false;
            return forumModel3;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(ForumModel result) {
            BoardListFragment.this.endUpdate();
            if (result == null) {
                Toast.makeText(BoardListFragment.this.activity, BoardListFragment.this.getString(BoardListFragment.this.mcResource.getStringId("mc_forum_no_board")), 0).show();
            } else if (!StringUtil.isEmpty(result.getErrorCode())) {
                Toast.makeText(BoardListFragment.this.activity, MCForumErrorUtil.convertErrorCode(BoardListFragment.this.activity, result.getErrorCode()), 0).show();
            } else {
                ForumModel unused = BoardListFragment.this.forumModel = result;
                BoardListFragment.this.updateStatusText();
                List<BoardCategoryModel> list = result.getBoardCategoryList();
                if (list != null && list.size() > 0) {
                    AdManager.getInstance().recyclAdByTag(BoardListFragment.this.TAG);
                    BoardListFragment.this.boardListAdapter.setBoardCategoryList(list);
                    BoardListFragment.this.boardListAdapter.notifyDataSetInvalidated();
                    BoardListFragment.this.boardListAdapter.notifyDataSetChanged();
                    BoardListFragment.this.expandAllView();
                }
            }
            if (BoardListFragment.this.isNeedNetUpdate) {
                if (BoardListFragment.this.updateForumInfoTask != null) {
                    BoardListFragment.this.updateForumInfoTask.cancel(true);
                }
                UpdateForumInfoTask unused2 = BoardListFragment.this.updateForumInfoTask = new UpdateForumInfoTask();
                BoardListFragment.this.updateForumInfoTask.execute(new Void[0]);
            }
            BoardListFragment.this.updateRefreshView();
        }
    }

    /* access modifiers changed from: private */
    public void updateStatusText() {
        this.statusView.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void updateRefreshView() {
        this.refreshView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                BoardListFragment.this.refreshText.setText(BoardListFragment.this.mcResource.getStringId("mc_forum_loading"));
                BoardListFragment.this.lastUpdateText.setVisibility(4);
                if (BoardListFragment.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
                    ObtainForumInfoTask unused = BoardListFragment.this.obtainForumInfoTask = new ObtainForumInfoTask();
                    BoardListFragment.this.obtainForumInfoTask.execute(true);
                    return;
                }
                BoardListFragment.this.endUpdate();
                BoardListFragment.this.warnMessageByStr(BoardListFragment.this.mcResource.getString("mc_forum_permission_cannot_visit_forum"));
            }
        });
        this.refreshText.setText(this.mcResource.getStringId("mc_forum_refresh"));
        this.lastUpdateText.setText(MCStringBundleUtil.resolveString(this.mcResource.getStringId("mc_forum_last_update"), MCStringBundleUtil.timeToString(this.activity, SharedPreferencesDB.getInstance(this.activity).getListLastUpdateDate(getClass().getName())), this.activity));
        this.lastUpdateText.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void expandAllView() {
        int j = this.boardListAdapter.getGroupCount();
        for (int i = 0; i < j; i++) {
            this.boardListView.expandGroup(i);
        }
    }

    private class UpdateForumInfoTask extends AsyncTask<Void, Void, Void> {
        private UpdateForumInfoTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            BoardListFragment.this.boardService.updateForumInfo();
            return null;
        }
    }

    public void onRefresh() {
        if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
            this.obtainForumInfoTask = new ObtainForumInfoTask();
            this.obtainForumInfoTask.execute(false);
            return;
        }
        endUpdate();
        warnMessageByStr(this.mcResource.getString("mc_forum_permission_cannot_visit_forum"));
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.obtainForumInfoTask != null) {
            this.obtainForumInfoTask.cancel(true);
        }
        if (this.updateForumInfoTask != null) {
            this.updateForumInfoTask.cancel(true);
        }
        AdManager.getInstance().recyclAdByTag(this.TAG);
    }
}
