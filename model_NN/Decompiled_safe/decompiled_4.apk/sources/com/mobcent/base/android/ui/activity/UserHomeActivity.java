package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserHomeFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserLoginFragmentActivity;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.UserServiceImpl;

public class UserHomeActivity extends BaseActivity {
    private UserService userService;

    /* access modifiers changed from: protected */
    public void initData() {
        this.userService = new UserServiceImpl(this);
        long userId = this.intent.getLongExtra("userId", 0);
        if (this.userService.isLogin()) {
            Intent intent = new Intent(this, UserHomeFragmentActivity.class);
            intent.putExtra("userId", userId);
            startActivity(intent);
            return;
        }
        startActivity(new Intent(this, UserLoginFragmentActivity.class));
    }

    /* access modifiers changed from: protected */
    public void initViews() {
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
    }
}
