package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.ad.android.ui.widget.AdView;

public class UserFriendsAdapterHolder {
    private TextView UserStatusText;
    private AdView adView;
    private Button blackUserBtn;
    private Button followUser;
    private LinearLayout levelBox;
    private AdView topAdView;
    private TextView userGenderText;
    private ImageView userIconImg;
    private RelativeLayout userItemBox;
    private RelativeLayout userLocationBox;
    private TextView userLocationText;
    private TextView userNameText;
    private ImageView userRoleImg;

    public LinearLayout getLevelBox() {
        return this.levelBox;
    }

    public void setLevelBox(LinearLayout levelBox2) {
        this.levelBox = levelBox2;
    }

    public TextView getUserLocationText() {
        return this.userLocationText;
    }

    public void setUserLocationText(TextView userLocationText2) {
        this.userLocationText = userLocationText2;
    }

    public RelativeLayout getUserLocationBox() {
        return this.userLocationBox;
    }

    public void setUserLocationBox(RelativeLayout userLocationBox2) {
        this.userLocationBox = userLocationBox2;
    }

    public Button getBlackUserBtn() {
        return this.blackUserBtn;
    }

    public void setBlackUserBtn(Button blackUserBtn2) {
        this.blackUserBtn = blackUserBtn2;
    }

    public Button getFollowUser() {
        return this.followUser;
    }

    public void setFollowUser(Button followUser2) {
        this.followUser = followUser2;
    }

    public TextView getUserNameText() {
        return this.userNameText;
    }

    public void setUserNameText(TextView userNameText2) {
        this.userNameText = userNameText2;
    }

    public TextView getUserGenderText() {
        return this.userGenderText;
    }

    public void setUserGenderText(TextView userGenderText2) {
        this.userGenderText = userGenderText2;
    }

    public TextView getUserStatusText() {
        return this.UserStatusText;
    }

    public void setUserStatusText(TextView userStatusText) {
        this.UserStatusText = userStatusText;
    }

    public ImageView getUserIconImg() {
        return this.userIconImg;
    }

    public void setUserIconImg(ImageView userIconImg2) {
        this.userIconImg = userIconImg2;
    }

    public ImageView getUserRoleImg() {
        return this.userRoleImg;
    }

    public void setUserRoleImg(ImageView userRoleImg2) {
        this.userRoleImg = userRoleImg2;
    }

    public RelativeLayout getUserItemBox() {
        return this.userItemBox;
    }

    public void setUserItemBox(RelativeLayout userItemBox2) {
        this.userItemBox = userItemBox2;
    }

    public AdView getAdView() {
        return this.adView;
    }

    public void setAdView(AdView adView2) {
        this.adView = adView2;
    }

    public AdView getTopAdView() {
        return this.topAdView;
    }

    public void setTopAdView(AdView topAdView2) {
        this.topAdView = topAdView2;
    }
}
