package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.ImageView;

public class TopicContentImgHolder {
    private ImageView topicInfoImg;

    public ImageView getTopicInfoImg() {
        return this.topicInfoImg;
    }

    public void setTopicInfoImg(ImageView topicInfoImg2) {
        this.topicInfoImg = topicInfoImg2;
    }
}
