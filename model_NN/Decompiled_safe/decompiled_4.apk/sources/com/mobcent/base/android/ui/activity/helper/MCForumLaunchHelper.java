package com.mobcent.base.android.ui.activity.helper;

import android.content.Context;
import android.content.Intent;
import com.mobcent.base.android.ui.activity.PublishTopicActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.BoardListFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.HomeTopicFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.TopicListFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserHomeFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserLoginFragmentActivity;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import java.util.HashMap;

public class MCForumLaunchHelper {
    public static void launchForum(Context context) {
        MCForumHelper.prepareToLaunchForum(context);
        context.startActivity(new Intent(context, HomeTopicFragmentActivity.class));
    }

    public static void launchBoardList(Context context) {
        MCForumHelper.prepareToLaunchForum(context);
        context.startActivity(new Intent(context, BoardListFragmentActivity.class));
    }

    public static void launchBoard(Context context, long boardId, String boardName) {
        MCForumHelper.prepareToLaunchForum(context);
        Intent intent = new Intent(context, TopicListFragmentActivity.class);
        intent.putExtra("boardId", boardId);
        intent.putExtra("boardName", boardName);
        context.startActivity(intent);
    }

    public static void launchUserHome(Context context) {
        MCForumHelper.prepareToLaunchForum(context);
        UserService userService = new UserServiceImpl(context);
        boolean isLogin = userService.isLogin();
        long loginUserId = userService.getLoginUserId();
        if (isLogin) {
            Intent intent = new Intent(context, UserHomeFragmentActivity.class);
            intent.putExtra("userId", loginUserId);
            context.startActivity(intent);
            return;
        }
        context.startActivity(new Intent(context, UserLoginFragmentActivity.class));
    }

    public static void launchPublishTopic(Context context, MCResource forumResource) {
        MCForumHelper.prepareToLaunchForum(context);
        if (LoginInterceptor.doInterceptorByDialog(context, forumResource, PublishTopicActivity.class, new HashMap<>())) {
            context.startActivity(new Intent(context, PublishTopicActivity.class));
        }
    }
}
