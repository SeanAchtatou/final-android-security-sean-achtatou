package com.mobcent.base.android.ui.activity.service;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import java.io.Serializable;
import java.util.HashMap;

public class LoginInterceptor {
    public static boolean doInterceptor(Context context, Class<?> goToActicityClass) {
        boolean isLogin = new UserServiceImpl(context).isLogin();
        if (!isLogin) {
            Intent intent = MCForumHelper.setLoginIntent(context);
            intent.putExtra(MCConstant.TAG, goToActicityClass);
            context.startActivity(intent);
        }
        return isLogin;
    }

    public static boolean doInterceptor(Context context, Class<?> goToActicityClass, HashMap<String, Serializable> param) {
        boolean isLogin = new UserServiceImpl(context).isLogin();
        if (!isLogin) {
            Intent intent = MCForumHelper.setLoginIntent(context);
            intent.putExtra(MCConstant.TAG, goToActicityClass);
            intent.putExtra(MCConstant.GO_PARAM, param);
            context.startActivity(intent);
        }
        return isLogin;
    }

    public static boolean doInterceptorByDialog(final Context context, MCResource resource, final Class<?> goToActicityClass, final HashMap<String, Serializable> param) {
        boolean isLogin = new UserServiceImpl(context).isLogin();
        if (!isLogin) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(resource.getStringId("mc_forum_dialog_tip"));
            builder.setMessage(resource.getStringId("mc_forum_login_tip_info"));
            builder.setPositiveButton(resource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = MCForumHelper.setLoginIntent(context);
                    intent.putExtra(MCConstant.TAG, goToActicityClass);
                    intent.putExtra(MCConstant.GO_PARAM, param);
                    context.startActivity(intent);
                }
            });
            builder.setNegativeButton(resource.getStringId("mc_forum_dialog_cancel"), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.create().show();
        }
        return isLogin;
    }
}
