package com.mobcent.base.android.ui.activity.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.PollItemModel;

public class MCPollResultView {
    private final int MAX_PROGRESS = 100;
    private Context context;
    private MCResource forumResource;
    private PollItemModel pollModel;
    private View view;
    private TextView voteNumLableText;
    private ProgressBar voteRsProgress;
    private TextView voteRsText;

    public MCPollResultView(Context context2, PollItemModel pollModel2) {
        this.pollModel = pollModel2;
        this.context = context2;
        initViews(this.context);
        updateView(this.pollModel);
    }

    private void initViews(Context context2) {
        this.forumResource = MCResource.getInstance(context2);
        this.view = LayoutInflater.from(context2).inflate(this.forumResource.getLayoutId("mc_forum_widget_poll_result_view"), (ViewGroup) null);
        this.voteNumLableText = (TextView) this.view.findViewById(this.forumResource.getViewId("mc_forum_vote_num_lable_text"));
        this.voteRsProgress = (ProgressBar) this.view.findViewById(this.forumResource.getViewId("mc_forum_vote_rs_progress"));
        this.voteRsProgress.setMax(100);
        this.voteRsText = (TextView) this.view.findViewById(this.forumResource.getViewId("mc_forum_vote_rs_text"));
    }

    private void updateView(PollItemModel pollModel2) {
        if (pollModel2 != null) {
            this.voteNumLableText.setText(pollModel2.getPollItemId() + "");
            this.voteRsProgress.setProgress((int) (pollModel2.getRatio() * 100.0d));
            this.voteRsText.setText(pollModel2.getPercent());
        }
    }

    public View getView() {
        return this.view;
    }

    public void setView(View view2) {
        this.view = view2;
    }
}
