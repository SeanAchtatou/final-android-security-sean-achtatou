package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import com.mobcent.base.android.ui.activity.fragment.UserLoginFragment;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserLoginFragmentActivity extends BaseFragmentActivity {
    private static Set<UserLoginFragmentActivity> loginActSets;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private UserLoginFragment userLoginFragment;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (loginActSets == null) {
            loginActSets = new HashSet();
        }
        finishAll();
        loginActSets.add(this);
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_user_login_fragment_activity"));
        this.fragmentManager = getSupportFragmentManager();
        if (this.fragmentManager.findFragmentById(this.resource.getViewId("mc_forum_user_login_box")) == null) {
            this.fragmentTransaction = this.fragmentManager.beginTransaction();
            this.userLoginFragment = new UserLoginFragment();
            this.fragmentTransaction.add(this.resource.getViewId("mc_forum_user_login_box"), this.userLoginFragment);
            this.fragmentTransaction.commit();
        }
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        loginActSets.remove(this);
    }

    public static void finishAll() {
        if (loginActSets != null) {
            for (UserLoginFragmentActivity act : loginActSets) {
                if (act != null) {
                    act.finish();
                }
            }
        }
    }
}
