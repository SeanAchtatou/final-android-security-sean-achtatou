package com.mobcent.base.android.ui.activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.PostsListAdapter;
import com.mobcent.base.android.ui.activity.adapter.holder.PostsTopicAdapterHolder;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.LongPicAsynTask;
import com.mobcent.base.android.ui.activity.delegate.GoToPageDelegate;
import com.mobcent.base.android.ui.activity.delegate.LongTaskDelegate;
import com.mobcent.base.android.ui.activity.delegate.PostsFavorDelegate;
import com.mobcent.base.android.ui.activity.delegate.RefreshContentDelegate;
import com.mobcent.base.android.ui.activity.delegate.ReplyRetrunDelegate;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.android.ui.activity.helper.MCForumLaunchShareHelper;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.android.ui.activity.service.MediaService;
import com.mobcent.base.android.ui.activity.view.MCFooterPagerBar;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.constant.MCForumConstant;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.PostsModel;
import com.mobcent.forum.android.model.ReplyModel;
import com.mobcent.forum.android.model.SoundModel;
import com.mobcent.forum.android.model.TopicContentModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.FavoriteService;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.FavoriteServiceImpl;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import java.util.ArrayList;
import java.util.List;

public class PostsActivity extends BasePullDownListActivity implements GoToPageDelegate, ReplyRetrunDelegate, MCConstant, PostsFavorDelegate {
    private static final int SHOW_POSTS_BY_ASC = 1;
    private static final int SHOW_POSTS_BY_DESC = 2;
    private static final int SHOW_POSTS_BY_USER = 3;
    public static final String TAG = "PostsActivity";
    private Button backBtn;
    /* access modifiers changed from: private */
    public long boardId;
    /* access modifiers changed from: private */
    public String boardName = "";
    private LinearLayout boardNameLayout;
    /* access modifiers changed from: private */
    public TextView boardNameText;
    private int close;
    /* access modifiers changed from: private */
    public long currPage = 0;
    /* access modifiers changed from: private */
    public LinearLayout dropdownSelectBox;
    private int essence;
    /* access modifiers changed from: private */
    public FavoriteAsyncTask favoriteAsyncTask;
    /* access modifiers changed from: private */
    public FavoriteService favoriteService;
    private List<String> iconUrls;
    /* access modifiers changed from: private */
    public List<String> imgUrls;
    private LayoutInflater inflater;
    private boolean isPush = false;
    private boolean isReplyRefresh = false;
    private boolean isShowPopMenu = false;
    private LongPicAsynTask longPicAsynTask;
    private Animation mAnimationDown;
    private Animation mAnimationUp;
    private MediaPlayerBroadCastReceiver mediaPlayerBroadCastReceiver = null;
    private ObtainPostsListByDescTask obtainPostsListByDescTask;
    private ObtainPostsListByUserTask obtainPostsListByUserTask;
    private ObtainPostsListTask obtainPostsListTask;
    private final int pageSize = 20;
    /* access modifiers changed from: private */
    public MCFooterPagerBar pagerBar;
    /* access modifiers changed from: private */
    public TextView postsByAscText;
    /* access modifiers changed from: private */
    public TextView postsByDescText;
    /* access modifiers changed from: private */
    public TextView postsByUserText;
    /* access modifiers changed from: private */
    public List<PostsModel> postsList;
    /* access modifiers changed from: private */
    public PostsListAdapter postsListAdapter;
    /* access modifiers changed from: private */
    public ListView postsListView;
    /* access modifiers changed from: private */
    public PostsService postsService;
    /* access modifiers changed from: private */
    public List<UserInfoModel> postsUserList;
    private RefreshContentDelegate refreshContentDelegate = new RefreshContentDelegate() {
        public void refreshContent() {
            PostsActivity.this.pagerBar.goToPage(PostsActivity.this.pagerBar.getCurrentPage());
        }
    };
    /* access modifiers changed from: private */
    public Button replyBtn;
    private ImageView selectArrowImg;
    /* access modifiers changed from: private */
    public int showPostsState = 1;
    private String thumbnailBaseUrl;
    private String thumbnailUrl;
    private int top;
    /* access modifiers changed from: private */
    public ImageView topicFavoriteBtn;
    /* access modifiers changed from: private */
    public long topicId;
    /* access modifiers changed from: private */
    public TopicModel topicModel;
    private ImageView topicShareBtn;
    private int topicType = 0;
    /* access modifiers changed from: private */
    public long topicUserId;
    /* access modifiers changed from: private */
    public RelativeLayout transparentLayout;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.postsService = new PostsServiceImpl(this);
        this.pagerBar.goToPage(1);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.inflater = LayoutInflater.from(this);
        this.boardId = getIntent().getLongExtra("boardId", 0);
        this.boardName = getIntent().getStringExtra("boardName");
        if (StringUtil.isEmpty(this.boardName)) {
            this.boardName = getString(this.resource.getStringId("mc_forum_posts_detail"));
        }
        this.topicId = getIntent().getLongExtra("topicId", 0);
        this.topicUserId = getIntent().getLongExtra(MCConstant.TOPIC_USER_ID, 0);
        this.thumbnailBaseUrl = getIntent().getStringExtra("baseUrl");
        this.thumbnailUrl = getIntent().getStringExtra(MCConstant.THUMBNAIL_IMAGE_URL);
        this.topicType = getIntent().getIntExtra("type", 0);
        ReplyTopicActivity.setReplyRetrunDelegate(this);
        this.postsUserList = new ArrayList();
        this.iconUrls = new ArrayList();
        this.imgUrls = new ArrayList();
        this.top = getIntent().getIntExtra(MCConstant.TOP, 0);
        this.essence = getIntent().getIntExtra(MCConstant.ESSENCE, 0);
        this.close = getIntent().getIntExtra(MCConstant.CLOSE, 0);
        this.isPush = getIntent().getBooleanExtra("push", false);
        if (this.isPush) {
            SharedPreferencesDB.getInstance(this).setPushMsgListJson("");
        }
        this.permService = new PermServiceImpl(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_posts_list_activity"));
        initListView();
        this.pagerBar = new MCFooterPagerBar(this, this);
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.replyBtn = (Button) findViewById(this.resource.getViewId("mc_forum_reply_btn"));
        this.boardNameLayout = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_board_name_layout"));
        this.boardNameText = (TextView) findViewById(this.resource.getViewId("mc_forum_board_name_text"));
        this.boardNameText.setText(this.boardName);
        this.selectArrowImg = (ImageView) findViewById(this.resource.getViewId("mc_forum_select_arrow_img"));
        this.dropdownSelectBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_dropdown_select_box"));
        this.postsByUserText = (TextView) findViewById(this.resource.getViewId("mc_forum_posts_by_user_text"));
        this.postsByDescText = (TextView) findViewById(this.resource.getViewId("mc_forum_posts_by_desc_text"));
        this.postsByAscText = (TextView) findViewById(this.resource.getViewId("mc_forum_posts_by_asc_text"));
        this.transparentLayout = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_transparent_box"));
        this.topicFavoriteBtn = (ImageView) findViewById(this.resource.getViewId("mc_forum_topic_favorite_btn"));
        this.topicShareBtn = (ImageView) findViewById(this.resource.getViewId("mc_forum_topic_share_btn"));
        this.postsListView = this.mListView;
        this.postsListView.addFooterView(this.pagerBar.getView());
        this.pagerBar.getView().setVisibility(8);
        this.postsList = new ArrayList();
        this.postsListAdapter = new PostsListAdapter(this, this.postsList, this.boardId, this.boardName, this.thumbnailUrl, this.mHandler, this.inflater, this.topicType, this.asyncTaskLoaderImage, this);
        this.postsListView.setAdapter((ListAdapter) this.postsListAdapter);
        this.postsListAdapter.setEssence(this.essence);
        this.postsListAdapter.setTop(this.top);
        this.postsListAdapter.setClose(this.close);
        initAnimationAction();
    }

    private void initAnimationAction() {
        this.mAnimationDown = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);
        this.mAnimationDown.setInterpolator(new LinearInterpolator());
        this.mAnimationDown.setDuration(250);
        this.mAnimationDown.setFillAfter(true);
        this.mAnimationUp = new RotateAnimation(-180.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.mAnimationUp.setInterpolator(new LinearInterpolator());
        this.mAnimationUp.setDuration(250);
        this.mAnimationUp.setFillAfter(true);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.boardNameLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PostsActivity.this.dropdownSelectBox.getVisibility() == 8) {
                    PostsActivity.this.changePopMenuVisible(true);
                } else {
                    PostsActivity.this.changePopMenuVisible(false);
                }
            }
        });
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCForumHelper.setBack(PostsActivity.this);
            }
        });
        this.replyBtn.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                PostsActivity.this.replyBtn.setEnabled(false);
                if (!LoginInterceptor.doInterceptor(PostsActivity.this, null)) {
                    return;
                }
                if (PostsActivity.this.permService.getPermNum(PermConstant.USER_GROUP, "reply", -1) == 1 && PostsActivity.this.permService.getPermNum(PermConstant.BOARDS, "reply", PostsActivity.this.boardId) == 1) {
                    Intent intent = new Intent(PostsActivity.this, ReplyTopicActivity.class);
                    intent.putExtra("boardId", PostsActivity.this.boardId);
                    intent.putExtra("boardName", PostsActivity.this.boardName);
                    intent.putExtra("topicId", PostsActivity.this.topicId);
                    intent.putExtra("toReplyId", -1L);
                    intent.putExtra(MCConstant.IS_QUOTE_TOPIC, false);
                    intent.putExtra(MCConstant.POSTS_USER_LIST, (ArrayList) PostsActivity.this.postsUserList);
                    PostsActivity.this.startActivity(intent);
                    return;
                }
                PostsActivity.this.warnMessageByStr(PostsActivity.this.resource.getString("mc_forum_permission_cannot_reply_topic"));
            }
        });
        this.postsByUserText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PostsActivity.this.changePopMenuVisible(false);
                PostsActivity.this.boardNameText.setText(PostsActivity.this.postsByUserText.getText());
                int unused = PostsActivity.this.showPostsState = 3;
                PostsActivity.this.pagerBar.goToPage(1);
            }
        });
        this.postsByDescText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PostsActivity.this.changePopMenuVisible(false);
                PostsActivity.this.boardNameText.setText(PostsActivity.this.postsByDescText.getText());
                int unused = PostsActivity.this.showPostsState = 2;
                PostsActivity.this.pagerBar.goToPage(1);
            }
        });
        this.postsByAscText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PostsActivity.this.changePopMenuVisible(false);
                PostsActivity.this.boardNameText.setText(PostsActivity.this.postsByAscText.getText());
                int unused = PostsActivity.this.showPostsState = 1;
                PostsActivity.this.pagerBar.goToPage(1);
            }
        });
        this.transparentLayout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (PostsActivity.this.transparentLayout.getVisibility() != 0) {
                    return false;
                }
                PostsActivity.this.changePopMenuVisible(false);
                return false;
            }
        });
        this.topicFavoriteBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PostsActivity.this.postsList != null && PostsActivity.this.postsList.size() > 0 && LoginInterceptor.doInterceptor(PostsActivity.this, null, null)) {
                    PostsActivity.this.topicFavoriteBtn.setEnabled(false);
                    FavoriteAsyncTask unused = PostsActivity.this.favoriteAsyncTask = new FavoriteAsyncTask();
                    PostsActivity.this.favoriteAsyncTask.execute(Integer.valueOf(PostsActivity.this.topicModel.getIsFavor()), Long.valueOf(PostsActivity.this.topicModel.getTopicId()), Long.valueOf(PostsActivity.this.boardId), PostsActivity.this.topicFavoriteBtn);
                }
            }
        });
        this.topicShareBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String shareContent = PostsActivity.this.getShareContent(PostsActivity.this.topicModel, null);
                if (PostsActivity.this.postsList != null) {
                    PostsActivity.this.shareEvent(shareContent, PostsActivity.this.topicModel, !StringUtil.isEmpty(SharedPreferencesDB.getInstance(PostsActivity.this).getWeiXinAppKey()), WXAPIFactory.createWXAPI(PostsActivity.this, SharedPreferencesDB.getInstance(PostsActivity.this).getWeiXinAppKey()));
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void shareEvent(String shareContent, TopicModel topicModel2, boolean ExistWeiXinAppKey, IWXAPI api) {
        this.longPicAsynTask = new LongPicAsynTask(this);
        final TopicModel topicModel3 = topicModel2;
        final boolean z = ExistWeiXinAppKey;
        final String str = shareContent;
        final IWXAPI iwxapi = api;
        this.longPicAsynTask.setLongTaskDelegate(new LongTaskDelegate() {
            public void executeSuccess(String url) {
                PostsActivity.this.myDialog.cancel();
                if (topicModel3 != null) {
                    topicModel3.setPic(url);
                }
                if (z) {
                    MCForumLaunchShareHelper.shareWay(true, str, "", url, "", "", PostsActivity.this, iwxapi);
                    return;
                }
                MCForumLaunchShareHelper.shareContentWithImageUrl(str, url, "", "", PostsActivity.this);
            }

            public void executeFail() {
                PostsActivity.this.myDialog.cancel();
                if (z) {
                    MCForumLaunchShareHelper.shareWay(false, str, "", "", "", "", PostsActivity.this, iwxapi);
                } else {
                    MCForumLaunchShareHelper.shareContent(str, "", "", PostsActivity.this);
                }
            }
        });
        if (topicModel2 == null) {
            return;
        }
        if (topicModel2.isHasImg()) {
            if (StringUtil.isEmpty(topicModel2.getPic())) {
                getLongPicUrl(topicModel2.getTitle(), topicModel2.getContentAll(), AsyncTaskLoaderImage.formatUrl(topicModel2.getBaseUrl(), MCForumConstant.RESOLUTION_ORIGINAL));
            } else if (ExistWeiXinAppKey) {
                MCForumLaunchShareHelper.shareWay(true, shareContent, "", topicModel2.getPic(), "", "", this, api);
            } else {
                MCForumLaunchShareHelper.shareContentWithImageUrl(shareContent, topicModel2.getTopicId() + "-" + 1, "", "", this);
            }
        } else if (ExistWeiXinAppKey) {
            MCForumLaunchShareHelper.shareWay(false, shareContent, "", "", "", "", this, api);
        } else {
            MCForumLaunchShareHelper.shareContent(shareContent, "", "", this);
        }
    }

    private void getLongPicUrl(String title, String content, String baseUrl) {
        this.myDialog = new ProgressDialog(this);
        this.myDialog.setProgressStyle(0);
        this.myDialog.setTitle(getResources().getString(this.resource.getStringId("mc_forum_dialog_tip")));
        this.myDialog.setMessage(getResources().getString(this.resource.getStringId("mc_forum_warn_load")));
        this.myDialog.setIndeterminate(false);
        this.myDialog.setCancelable(true);
        this.myDialog.show();
        this.longPicAsynTask.execute(title, content, baseUrl);
    }

    /* access modifiers changed from: private */
    public String getShareContent(TopicModel topic, ReplyModel reply) {
        String shareContent;
        try {
            String shareContent2 = BaseReturnCodeConstant.ERROR_CODE + topic.getTitle() + BaseReturnCodeConstant.ERROR_CODE + "\n";
            if (reply == null) {
                if (topic.getContent().length() <= 70) {
                    shareContent = shareContent2 + topic.getContent();
                } else {
                    shareContent = shareContent2 + topic.getContent().substring(0, 70);
                }
            } else if (reply.getShortContent().length() <= 70) {
                shareContent = shareContent2 + reply.getShortContent();
            } else {
                shareContent = shareContent2 + reply.getShortContent().substring(0, 70);
            }
            return shareContent;
        } catch (Exception e) {
            return "";
        }
    }

    private abstract class AbstractObtainPostsListTask extends AsyncTask<Long, Void, List<PostsModel>> {
        /* access modifiers changed from: protected */
        public abstract List<PostsModel> getPosts(long j, long j2, int i);

        private AbstractObtainPostsListTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<PostsModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            PostsActivity.this.postsListView.setSelection(0);
            PostsActivity.this.onHeadLoading();
            if (PostsActivity.this.postsListView.getFooterViewsCount() == 0) {
                PostsActivity.this.postsListView.addFooterView(PostsActivity.this.pagerBar.getView());
            }
            PostsActivity.this.pagerBar.getView().setVisibility(8);
            if (PostsActivity.this.imgUrls != null && !PostsActivity.this.imgUrls.isEmpty() && PostsActivity.this.imgUrls.size() > 0) {
                PostsActivity.this.asyncTaskLoaderImage.recycleBitmaps(PostsActivity.this.imgUrls);
            }
            Intent intent = new Intent(PostsActivity.this, MediaService.class);
            intent.putExtra(MediaService.SERVICE_TAG, PostsActivity.this.toString());
            intent.putExtra(MediaService.SERVICE_STATUS, 6);
            PostsActivity.this.startService(intent);
        }

        /* access modifiers changed from: protected */
        public List<PostsModel> doInBackground(Long... params) {
            long boardId = params[0].longValue();
            long topicId = params[1].longValue();
            long page = params[2].longValue();
            long unused = PostsActivity.this.currPage = page;
            return getPosts(boardId, topicId, (int) page);
        }

        /* Debug info: failed to restart local var, previous not found, register: 7 */
        /* access modifiers changed from: protected */
        public void onPostExecute(List<PostsModel> result) {
            super.onPostExecute((Object) result);
            PostsActivity.this.endUpdate();
            if (result == null || result.size() <= 0) {
                Toast.makeText(PostsActivity.this, PostsActivity.this.getString(PostsActivity.this.resource.getStringId("mc_forum_no_data")), 0).show();
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                Toast.makeText(PostsActivity.this, MCForumErrorUtil.convertErrorCode(PostsActivity.this, result.get(0).getErrorCode()), 0).show();
            } else {
                List unused = PostsActivity.this.imgUrls = PostsActivity.this.getImageURL(PostsActivity.this.postsList);
                PostsActivity.this.getIconURL(result);
                PostsActivity.this.pagerBar.getView().setVisibility(0);
                List unused2 = PostsActivity.this.postsList = result;
                List unused3 = PostsActivity.this.postsUserList = PostsActivity.this.getPostsUserList(PostsActivity.this.postsList);
                PostsActivity.this.pagerBar.updatePagerInfo(((PostsModel) PostsActivity.this.postsList.get(0)).getTotalNum(), 20);
                if (PostsActivity.this.pagerBar.getTotalPage() <= 1 && PostsActivity.this.postsListView.getFooterViewsCount() > 0) {
                    PostsActivity.this.postsListView.removeFooterView(PostsActivity.this.pagerBar.getView());
                }
                if (PostsActivity.this.pagerBar.getCurrentPage() == 1) {
                    try {
                        TopicModel unused4 = PostsActivity.this.topicModel = ((PostsModel) PostsActivity.this.postsList.get(0)).getTopic();
                        if (PostsActivity.this.topicModel.getIsFavor() == 1) {
                            PostsActivity.this.topicFavoriteBtn.setImageDrawable(PostsActivity.this.getResources().getDrawable(PostsActivity.this.resource.getDrawableId("mc_forum_tools_bar_button11")));
                        } else {
                            PostsActivity.this.topicFavoriteBtn.setImageDrawable(PostsActivity.this.getResources().getDrawable(PostsActivity.this.resource.getDrawableId("mc_forum_tools_bar_button12")));
                        }
                        if (PostsActivity.this.topicModel.getStatus() == 0) {
                            PostsActivity.this.replyBtn.setVisibility(8);
                        } else {
                            PostsActivity.this.replyBtn.setVisibility(0);
                        }
                    } catch (Exception e) {
                        PostsActivity.this.replyBtn.setVisibility(8);
                    }
                }
                PostsActivity.this.postsListAdapter.setPostsList(PostsActivity.this.postsList);
                PostsActivity.this.postsListAdapter.notifyDataSetInvalidated();
                PostsActivity.this.postsListAdapter.notifyDataSetChanged();
                if (PostsActivity.this.postsListAdapter.getCount() > 0) {
                    PostsActivity.this.postsListView.setSelection(0);
                }
                if (PostsActivity.this.topicModel.getStatus() == 0) {
                    PostsActivity.this.findViewById(PostsActivity.this.resource.getViewId("mc_forum_posts_list_navigation")).setVisibility(8);
                }
            }
        }
    }

    private class ObtainPostsListTask extends AbstractObtainPostsListTask {
        private ObtainPostsListTask() {
            super();
        }

        /* access modifiers changed from: protected */
        public List<PostsModel> getPosts(long boardId, long topicId, int page) {
            return PostsActivity.this.postsService.getPosts(boardId, topicId, page, 20);
        }
    }

    private class ObtainPostsListByUserTask extends AbstractObtainPostsListTask {
        private ObtainPostsListByUserTask() {
            super();
        }

        /* access modifiers changed from: protected */
        public List<PostsModel> getPosts(long boardId, long topicId, int page) {
            return PostsActivity.this.postsService.getUserPosts(boardId, topicId, PostsActivity.this.topicUserId, page, 20);
        }
    }

    private class ObtainPostsListByDescTask extends AbstractObtainPostsListTask {
        private ObtainPostsListByDescTask() {
            super();
        }

        /* access modifiers changed from: protected */
        public List<PostsModel> getPosts(long boardId, long topicId, int page) {
            return PostsActivity.this.postsService.getPostsByDesc(boardId, topicId, page, 20);
        }
    }

    public void goToPage(int page) {
        if (this.postsList == null) {
            findViewById(this.resource.getViewId("mc_forum_posts_list_navigation")).setVisibility(8);
        }
        switch (this.showPostsState) {
            case 1:
                if (this.obtainPostsListTask != null) {
                    this.obtainPostsListTask.cancel(true);
                }
                this.obtainPostsListTask = new ObtainPostsListTask();
                this.obtainPostsListTask.execute(new Long[]{Long.valueOf(this.boardId), Long.valueOf(this.topicId), Long.valueOf((long) page)});
                return;
            case 2:
                if (this.obtainPostsListByDescTask != null) {
                    this.obtainPostsListByDescTask.cancel(true);
                }
                this.obtainPostsListByDescTask = new ObtainPostsListByDescTask();
                this.obtainPostsListByDescTask.execute(new Long[]{Long.valueOf(this.boardId), Long.valueOf(this.topicId), Long.valueOf((long) page)});
                return;
            case 3:
                if (this.obtainPostsListByUserTask != null) {
                    this.obtainPostsListByUserTask.cancel(true);
                }
                this.obtainPostsListByUserTask = new ObtainPostsListByUserTask();
                this.obtainPostsListByUserTask.execute(new Long[]{Long.valueOf(this.boardId), Long.valueOf(this.topicId), Long.valueOf((long) page)});
                return;
            default:
                if (this.obtainPostsListTask != null) {
                    this.obtainPostsListTask.cancel(true);
                }
                this.obtainPostsListTask = new ObtainPostsListTask();
                this.obtainPostsListTask.execute(new Long[]{Long.valueOf(this.boardId), Long.valueOf(this.topicId), Long.valueOf((long) page)});
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.replyBtn.setEnabled(true);
        int currentPage = this.pagerBar.getCurrentPage();
        int totalPage = this.pagerBar.getTotalPage();
        if (currentPage >= totalPage && this.isReplyRefresh) {
            if (currentPage > 1 && this.postsList.size() >= 20) {
                this.pagerBar.updatePagerInfo(this.postsList.get(0).getTotalNum() + 1, totalPage + 1);
                currentPage++;
            } else if (currentPage == 1 && this.postsList.size() - 1 >= 20) {
                this.pagerBar.updatePagerInfo(this.postsList.get(0).getTotalNum() + 1, totalPage + 1);
                currentPage++;
            }
            this.isReplyRefresh = false;
            this.pagerBar.goToPage(currentPage);
        }
    }

    public MCFooterPagerBar getPagerBar() {
        return this.pagerBar;
    }

    public RefreshContentDelegate getRefreshContentDelegate() {
        return this.refreshContentDelegate;
    }

    public void setRefreshContentDelegate(RefreshContentDelegate refreshContentDelegate2) {
        this.refreshContentDelegate = refreshContentDelegate2;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.dropdownSelectBox.getVisibility() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        changePopMenuVisible(false);
        return false;
    }

    public void changePopMenuVisible(boolean isShowPopMenu2) {
        if (this.isShowPopMenu != isShowPopMenu2) {
            this.isShowPopMenu = isShowPopMenu2;
            if (this.isShowPopMenu) {
                this.dropdownSelectBox.setVisibility(0);
                this.transparentLayout.setVisibility(0);
                this.selectArrowImg.startAnimation(this.mAnimationDown);
                return;
            }
            this.dropdownSelectBox.setVisibility(8);
            this.transparentLayout.setVisibility(8);
            this.selectArrowImg.startAnimation(this.mAnimationUp);
        }
    }

    public void onRefresh() {
        this.pagerBar.goToPage(this.pagerBar.getCurrentPage());
    }

    public void replyReturn() {
        this.isReplyRefresh = true;
    }

    /* access modifiers changed from: private */
    public List<UserInfoModel> getPostsUserList(List<PostsModel> postsList2) {
        List<UserInfoModel> userlist = new ArrayList<>();
        for (PostsModel postsModel : postsList2) {
            UserInfoModel user = new UserInfoModel();
            TopicModel topic = postsModel.getTopic();
            if (topic != null) {
                user.setNickname(topic.getUserNickName());
                user.setUserId(topic.getUserId());
                user.setPlatformId(0);
            } else {
                ReplyModel reply = postsModel.getReply();
                user.setNickname(reply.getUserNickName());
                user.setUserId(reply.getReplyUserId());
                user.setPlatformId(0);
            }
            if (!isContainUser(userlist, user)) {
                userlist.add(user);
            }
        }
        return userlist;
    }

    private boolean isContainUser(List<UserInfoModel> userlist, UserInfoModel user) {
        for (UserInfoModel userInfo : userlist) {
            if (userInfo.getUserId() == user.getUserId()) {
                return true;
            }
        }
        return false;
    }

    public List<UserInfoModel> getPostsUserList() {
        return this.postsUserList;
    }

    public void setPostsUserList(List<UserInfoModel> postsUserList2) {
        this.postsUserList = postsUserList2;
    }

    /* access modifiers changed from: protected */
    public List<String> getImageURL(List<PostsModel> postsList2) {
        List<String> imgUrl = new ArrayList<>();
        int j = postsList2.size();
        for (int i = 0; i < j; i++) {
            PostsModel posts = postsList2.get(i);
            if (posts.getPostType() == 0) {
                imgUrl.addAll(getTopicContentListImg(posts.getReply().getReplyContentList()));
            }
        }
        return imgUrl;
    }

    /* access modifiers changed from: private */
    public void getIconURL(List<PostsModel> postsList2) {
        int j = postsList2.size();
        for (int i = 0; i < j; i++) {
            PostsModel posts = postsList2.get(i);
            if (posts.getPostType() == 1) {
                TopicModel topicModel2 = posts.getTopic();
                if (!StringUtil.isEmpty(topicModel2.getIcon())) {
                    String icon = topicModel2.getIconUrl() + topicModel2.getIcon();
                    if (!this.iconUrls.contains(icon)) {
                        this.iconUrls.add(AsyncTaskLoaderImage.formatUrl(icon, "100x100"));
                    }
                }
            } else {
                ReplyModel replyModel = posts.getReply();
                if (!StringUtil.isEmpty(replyModel.getIcon())) {
                    String icon2 = replyModel.getIconUrl() + replyModel.getIcon();
                    if (!this.iconUrls.contains(icon2)) {
                        this.iconUrls.add(AsyncTaskLoaderImage.formatUrl(icon2, "100x100"));
                    }
                }
            }
        }
    }

    private List<String> getTopicContentListImg(List<TopicContentModel> topicContentList) {
        List<String> imgUrl = new ArrayList<>();
        if (topicContentList != null && !topicContentList.isEmpty()) {
            int n = topicContentList.size();
            for (int k = 0; k < n; k++) {
                TopicContentModel topicContent = topicContentList.get(k);
                if (topicContent.getType() == 1 && topicContent.getBaseUrl() != null && !topicContent.getBaseUrl().trim().equals("") && !topicContent.getBaseUrl().trim().equals("null")) {
                    imgUrl.add(AsyncTaskLoaderImage.formatUrl(topicContent.getBaseUrl() + topicContent.getInfor(), "320x480"));
                }
            }
        }
        return imgUrl;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Intent intent = new Intent(this, MediaService.class);
        intent.putExtra(MediaService.SERVICE_TAG, toString());
        intent.putExtra(MediaService.SERVICE_STATUS, 6);
        startService(intent);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.mediaPlayerBroadCastReceiver == null) {
            this.mediaPlayerBroadCastReceiver = new MediaPlayerBroadCastReceiver();
        }
        registerReceiver(this.mediaPlayerBroadCastReceiver, new IntentFilter(MediaService.INTENT_TAG + getPackageName()));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.mediaPlayerBroadCastReceiver != null) {
            unregisterReceiver(this.mediaPlayerBroadCastReceiver);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (!(this.obtainPostsListByDescTask == null || this.obtainPostsListByDescTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.obtainPostsListByDescTask.cancel(true);
        }
        if (!(this.obtainPostsListByUserTask == null || this.obtainPostsListByUserTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.obtainPostsListByUserTask.cancel(true);
        }
        if (!(this.obtainPostsListTask == null || this.obtainPostsListTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.obtainPostsListTask.cancel(true);
        }
        if (this.favoriteAsyncTask != null) {
            this.favoriteAsyncTask.cancel(true);
        }
        this.postsListAdapter.destroy();
    }

    public class MediaPlayerBroadCastReceiver extends BroadcastReceiver {
        private SoundModel currSoundModel = null;
        private String tag;

        public MediaPlayerBroadCastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            MCLogUtil.i(PostsActivity.TAG, "onReceive");
            this.tag = intent.getStringExtra(MediaService.SERVICE_TAG);
            if (this.tag != null && this.tag.equals(PostsActivity.this.toString())) {
                this.currSoundModel = (SoundModel) intent.getSerializableExtra(MediaService.SERVICE_MODEL);
                PostsActivity.this.postsListAdapter.updateReceivePlayImg(this.currSoundModel);
            }
        }
    }

    private class FavoriteAsyncTask extends AsyncTask<Object, Void, String> {
        private ImageView favorBtn;
        private int isFavor;

        private FavoriteAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (PostsActivity.this.favoriteService == null) {
                FavoriteService unused = PostsActivity.this.favoriteService = new FavoriteServiceImpl(PostsActivity.this);
            }
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Object... params) {
            this.isFavor = ((Integer) params[0]).intValue();
            long topicId = ((Long) params[1]).longValue();
            long boardId = ((Long) params[2]).longValue();
            this.favorBtn = (ImageView) params[3];
            if (this.isFavor == 1) {
                return PostsActivity.this.favoriteService.delFavoriteTopic(topicId, boardId);
            }
            return PostsActivity.this.favoriteService.addFavoriteTopic(topicId, boardId);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (result != null) {
                PostsActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(PostsActivity.this, result));
            } else if (PostsActivity.this.topicModel != null) {
                if (this.isFavor == 1) {
                    PostsActivity.this.topicModel.setIsFavor(0);
                    Toast.makeText(PostsActivity.this, PostsActivity.this.resource.getStringId("mc_forum_cancel_favorit_succ"), 0).show();
                    this.favorBtn.setImageDrawable(PostsActivity.this.getResources().getDrawable(PostsActivity.this.resource.getDrawableId("mc_forum_tools_bar_button12")));
                    PostsActivity.this.btnSetChange();
                } else {
                    Toast.makeText(PostsActivity.this, PostsActivity.this.resource.getStringId("mc_forum_add_favorit_succ"), 0).show();
                    PostsActivity.this.topicModel.setIsFavor(1);
                    this.favorBtn.setImageDrawable(PostsActivity.this.getResources().getDrawable(PostsActivity.this.resource.getDrawableId("mc_forum_tools_bar_button11")));
                    PostsActivity.this.btnSetChange();
                }
            }
            this.favorBtn.setEnabled(true);
        }
    }

    public void onFavorBtnClick() {
        this.topicFavoriteBtn.performClick();
    }

    /* access modifiers changed from: private */
    public void btnSetChange() {
        int firstPosition;
        if (this.postsList.get(0).getPostType() == 1 && (firstPosition = this.postsListView.getFirstVisiblePosition()) == 0) {
            ImageButton topicFavoriteBtn2 = ((PostsTopicAdapterHolder) this.postsListView.getChildAt(firstPosition).getTag()).getTopicFavoriteBtn();
            if (this.topicModel.getIsFavor() == 0) {
                topicFavoriteBtn2.setBackgroundResource(this.resource.getDrawableId("mc_forum_icon28"));
            } else {
                topicFavoriteBtn2.setBackgroundResource(this.resource.getDrawableId("mc_forum_icon27"));
            }
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        MCForumHelper.setBack(this);
    }
}
