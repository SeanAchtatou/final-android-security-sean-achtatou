package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.ad.android.model.AdModel;
import com.mobcent.ad.android.ui.widget.AdView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.holder.BoardAdapterHolder;
import com.mobcent.base.android.ui.activity.adapter.holder.BoardCategoryAdapterHolder;
import com.mobcent.base.android.ui.activity.adapter.holder.BoardDoubleAdapterHolder;
import com.mobcent.base.android.ui.activity.fragmentActivity.TopicListFragmentActivity;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.model.BoardCategoryModel;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.service.PermService;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.util.DateUtil;
import java.util.List;

public class BoardListAdapter extends BaseExpandableListAdapter implements MCConstant {
    private int BOARD_LIST_POSITION;
    private String TAG = "BoardListFragment";
    private List<AdModel> adList;
    private List<BoardCategoryModel> boardCategoryList;
    /* access modifiers changed from: private */
    public Context context;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public PermService permService;
    /* access modifiers changed from: private */
    public MCResource resource;

    public BoardListAdapter(Context context2, List<BoardCategoryModel> boardCategoryList2, LayoutInflater inflater2) {
        this.context = context2;
        this.boardCategoryList = boardCategoryList2;
        this.inflater = inflater2;
        this.resource = MCResource.getInstance(context2);
        this.permService = new PermServiceImpl(context2);
    }

    public List<AdModel> getAdList() {
        return this.adList;
    }

    public void setAdList(List<AdModel> adList2) {
        this.adList = adList2;
    }

    public List<BoardCategoryModel> getBoardCategoryList() {
        return this.boardCategoryList;
    }

    public void setBoardCategoryList(List<BoardCategoryModel> boardCategoryList2) {
        this.boardCategoryList = boardCategoryList2;
    }

    public int getGroupCount() {
        return this.boardCategoryList.size();
    }

    public int getChildrenCount(int groupPosition) {
        return this.boardCategoryList.get(groupPosition).getBoardList().size();
    }

    public BoardCategoryModel getGroup(int groupPosition) {
        return this.boardCategoryList.get(groupPosition);
    }

    public BoardModel getChild(int groupPosition, int childPosition) {
        return this.boardCategoryList.get(groupPosition).getBoardList().get(childPosition);
    }

    public long getGroupId(int groupPosition) {
        return this.boardCategoryList.get(groupPosition).getBoardCategoryId();
    }

    public long getChildId(int groupPosition, int childPosition) {
        return this.boardCategoryList.get(groupPosition).getBoardList().get(childPosition).getBoardId();
    }

    public boolean hasStableIds() {
        return false;
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        BoardCategoryAdapterHolder holder;
        if (convertView == null) {
            convertView = (RelativeLayout) this.inflater.inflate(this.resource.getLayoutId("mc_forum_board_category_item"), (ViewGroup) null);
            holder = new BoardCategoryAdapterHolder();
            initBoardCategoryAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (BoardCategoryAdapterHolder) convertView.getTag();
        }
        holder.getBoardCategoryNameText().setText(getGroup(groupPosition).getBoardCategoryName());
        this.BOARD_LIST_POSITION = new Integer(this.context.getResources().getString(this.resource.getStringId("mc_forum_board_list_postion"))).intValue();
        holder.getAdViewBox().setVisibility(0);
        holder.getAdViewBox().showAd(this.TAG, this.BOARD_LIST_POSITION + groupPosition, groupPosition);
        return convertView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobcent.base.android.ui.activity.adapter.BoardListAdapter.getChild(int, int):com.mobcent.forum.android.model.BoardModel
     arg types: [int, int]
     candidates:
      com.mobcent.base.android.ui.activity.adapter.BoardListAdapter.getChild(int, int):java.lang.Object
      com.mobcent.base.android.ui.activity.adapter.BoardListAdapter.getChild(int, int):com.mobcent.forum.android.model.BoardModel */
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final BoardModel boardModel = getChild(groupPosition, childPosition);
        int type = getGroup(groupPosition).getBoardCategoryType();
        if (type == 1) {
            View convertView2 = getSingleBoardView(convertView);
            updateboard((BoardAdapterHolder) convertView2.getTag(), boardModel);
            convertView2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (BoardListAdapter.this.permService.getPermNum(PermConstant.BOARDS, PermConstant.VISIT, boardModel.getBoardId()) == 1) {
                        Intent intent = new Intent(BoardListAdapter.this.context, TopicListFragmentActivity.class);
                        intent.putExtra("boardId", boardModel.getBoardId());
                        intent.putExtra("boardName", boardModel.getBoardName());
                        BoardListAdapter.this.context.startActivity(intent);
                        return;
                    }
                    Toast.makeText(BoardListAdapter.this.context, BoardListAdapter.this.resource.getString("mc_forum_permission_cannot_visit_topic"), 1).show();
                }
            });
            return convertView2;
        } else if (type != 2) {
            return convertView;
        } else {
            View convertView3 = getDoubleBoardView(convertView);
            BoardDoubleAdapterHolder doubleHolder = (BoardDoubleAdapterHolder) convertView3.getTag();
            updateboardDouble(doubleHolder, boardModel);
            updateboardDoubleActions(doubleHolder, boardModel);
            return convertView3;
        }
    }

    private View getSingleBoardView(View convertView) {
        BoardAdapterHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_board_item"), (ViewGroup) null);
            holder = new BoardAdapterHolder();
            initBoardAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            try {
                holder = (BoardAdapterHolder) convertView.getTag();
            } catch (ClassCastException e) {
                convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_board_item"), (ViewGroup) null);
                holder = new BoardAdapterHolder();
                initBoardAdapterHolder(convertView, holder);
                convertView.setTag(holder);
            }
        }
        if (holder != null) {
            return convertView;
        }
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_board_item"), (ViewGroup) null);
        BoardAdapterHolder holder2 = new BoardAdapterHolder();
        initBoardAdapterHolder(convertView2, holder2);
        convertView2.setTag(holder2);
        return convertView2;
    }

    private View getDoubleBoardView(View convertView) {
        BoardDoubleAdapterHolder doubleHolder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_board_double_item"), (ViewGroup) null);
            doubleHolder = new BoardDoubleAdapterHolder();
            initBoardDoubleAdapterHolder(convertView, doubleHolder);
            convertView.setTag(doubleHolder);
        } else {
            try {
                doubleHolder = (BoardDoubleAdapterHolder) convertView.getTag();
            } catch (ClassCastException e) {
                convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_board_double_item"), (ViewGroup) null);
                doubleHolder = new BoardDoubleAdapterHolder();
                initBoardDoubleAdapterHolder(convertView, doubleHolder);
                convertView.setTag(doubleHolder);
            }
        }
        if (doubleHolder != null) {
            return convertView;
        }
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_board_double_item"), (ViewGroup) null);
        BoardDoubleAdapterHolder doubleHolder2 = new BoardDoubleAdapterHolder();
        initBoardDoubleAdapterHolder(convertView2, doubleHolder2);
        convertView2.setTag(doubleHolder2);
        return convertView2;
    }

    private void updateboardDoubleActions(BoardDoubleAdapterHolder holderDouble, final BoardModel boardModel) {
        holderDouble.getBoardLeftBox().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (boardModel.getBoards().get(0) == null) {
                    return;
                }
                if (BoardListAdapter.this.permService.getPermNum(PermConstant.BOARDS, PermConstant.VISIT, boardModel.getBoards().get(0).getBoardId()) == 1) {
                    Intent intent = new Intent(BoardListAdapter.this.context, TopicListFragmentActivity.class);
                    intent.putExtra("boardId", boardModel.getBoards().get(0).getBoardId());
                    intent.putExtra("boardName", boardModel.getBoards().get(0).getBoardName());
                    BoardListAdapter.this.context.startActivity(intent);
                    return;
                }
                Toast.makeText(BoardListAdapter.this.context, BoardListAdapter.this.resource.getString("mc_forum_permission_cannot_visit_topic"), 1).show();
            }
        });
        holderDouble.getBoardRightBox().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (boardModel.getBoards().get(1) == null) {
                    return;
                }
                if (BoardListAdapter.this.permService.getPermNum(PermConstant.BOARDS, PermConstant.VISIT, boardModel.getBoards().get(0).getBoardId()) == 1) {
                    Intent intent = new Intent(BoardListAdapter.this.context, TopicListFragmentActivity.class);
                    intent.putExtra("boardId", boardModel.getBoards().get(1).getBoardId());
                    intent.putExtra("boardName", boardModel.getBoards().get(1).getBoardName());
                    BoardListAdapter.this.context.startActivity(intent);
                    return;
                }
                Toast.makeText(BoardListAdapter.this.context, BoardListAdapter.this.resource.getString("mc_forum_permission_cannot_visit_topic"), 1).show();
            }
        });
    }

    private void updateboardDouble(BoardDoubleAdapterHolder holderDouble, BoardModel boardModel) {
        List<BoardModel> models = boardModel.getBoards();
        if (models != null && !models.isEmpty() && models.size() == 2) {
            updateDoubleItemView(models.get(0), holderDouble.getBoardLeftName(), holderDouble.getBoardLeftTodayTotal(), holderDouble.getBoardLeftTime());
            updateDoubleItemView(models.get(1), holderDouble.getBoardRightName(), holderDouble.getBoardRightTodayTotal(), holderDouble.getBoardRightTime());
        }
    }

    private void initBoardDoubleAdapterHolder(View convertView, BoardDoubleAdapterHolder holder) {
        holder.setBoardLeftName((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_left_name_text")));
        holder.setBoardLeftTime((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_left_time_text")));
        holder.setBoardLeftTodayTotal((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_left_today_total_text")));
        holder.setBoardRightName((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_right_name_text")));
        holder.setBoardRightTime((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_right_time_text")));
        holder.setBoardRightTodayTotal((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_right_today_total_text")));
        holder.setBoardLeftBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_board_left_item_box")));
        holder.setBoardRightBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_board_right_item_box")));
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private void initBoardCategoryAdapterHolder(View convertView, BoardCategoryAdapterHolder holder) {
        holder.setBoardCategoryNameText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_category_name_text")));
        holder.setAdViewBox((AdView) convertView.findViewById(this.resource.getViewId("mc_ad_box")));
    }

    private void initBoardAdapterHolder(View convertView, BoardAdapterHolder holder) {
        holder.setBoardNameText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_name_text")));
        holder.setBoardTimeText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_time_text")));
        holder.setBoardTopicTotalText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_topic_total_text")));
        holder.setBoardPostsText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_posts_text")));
        holder.setTodayTotalText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_today_total_text")));
    }

    private void updateboard(BoardAdapterHolder holder, BoardModel boardModel) {
        holder.getBoardTopicTotalText().setText(boardModel.getTopicTotalNum() + "");
        int totalPostsNum = boardModel.getPostsTotalNum();
        holder.getBoardPostsText().setText(" / " + totalPostsNum);
        holder.getBoardNameText().setText(boardModel.getBoardName());
        if (boardModel.getTodayPostsNum() > 0) {
            holder.getTodayTotalText().setText("(" + boardModel.getTodayPostsNum() + ")");
        } else {
            holder.getTodayTotalText().setText("");
        }
        if (boardModel.getLastPostsDate() <= 0 || totalPostsNum <= 0) {
            holder.getBoardTimeText().setText(this.resource.getStringId("mc_forum_board_no_update"));
        } else {
            holder.getBoardTimeText().setText(this.resource.getString("mc_forum_board_last_update") + DateUtil.getFormatTime(boardModel.getLastPostsDate()));
        }
    }

    private void updateDoubleItemView(BoardModel board, TextView boardNameText, TextView boardTotalText, TextView boardTimeText) {
        if (board != null) {
            boardNameText.setText(board.getBoardName());
            if (board.getTodayPostsNum() > 0) {
                boardTotalText.setText("(" + board.getTodayPostsNum() + ")");
            } else {
                boardTotalText.setText("");
            }
            boardTimeText.setText(DateUtil.getFormatTime(board.getLastPostsDate()));
            return;
        }
        boardNameText.setText("");
        boardTotalText.setText("");
        boardTimeText.setText("");
    }
}
