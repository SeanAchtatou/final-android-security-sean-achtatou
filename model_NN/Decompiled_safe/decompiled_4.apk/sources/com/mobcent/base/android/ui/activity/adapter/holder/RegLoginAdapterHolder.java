package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.TextView;

public class RegLoginAdapterHolder {
    private TextView emailSelectionText;

    public TextView getEmailSelectionText() {
        return this.emailSelectionText;
    }

    public void setEmailSelectionText(TextView emailSelectionText2) {
        this.emailSelectionText = emailSelectionText2;
    }
}
