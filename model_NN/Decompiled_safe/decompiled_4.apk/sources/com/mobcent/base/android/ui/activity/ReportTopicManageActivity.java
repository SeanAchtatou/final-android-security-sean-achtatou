package com.mobcent.base.android.ui.activity;

import com.mobcent.forum.android.model.ReportModel;
import java.util.List;

public class ReportTopicManageActivity extends BaseReportManageActivity {
    /* access modifiers changed from: protected */
    public List<ReportModel> getReportManageList(int page, int pageSize) {
        return this.reportService.getReportTopicList(page, pageSize);
    }
}
