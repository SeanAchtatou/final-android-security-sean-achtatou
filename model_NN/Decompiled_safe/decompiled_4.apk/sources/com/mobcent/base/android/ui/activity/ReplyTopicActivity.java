package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.BasePublishTopicActivityWithAudio;
import com.mobcent.base.android.ui.activity.delegate.ReplyRetrunDelegate;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.model.TopicDraftModel;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;

public class ReplyTopicActivity extends BasePublishTopicActivityWithDraft implements MCConstant {
    /* access modifiers changed from: private */
    public static ReplyRetrunDelegate replyRetrunDelegate;
    private long boardId;
    private String boardName;
    protected long draftId = 3;
    /* access modifiers changed from: private */
    public boolean isClosed = false;
    private boolean isQuoteTopic = true;
    private long pageFrom;
    private ReplyAsyncTask replyAsyncTask;
    private long toReplyId = -1;
    private long topicId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDraft(this.draftId);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        Intent intent = getIntent();
        if (intent != null) {
            this.boardId = intent.getLongExtra("boardId", 0);
            this.boardName = intent.getStringExtra("boardName");
            this.topicId = intent.getLongExtra("topicId", 0);
            this.toReplyId = intent.getLongExtra("toReplyId", -1);
            this.pageFrom = intent.getLongExtra("pageFrom", 0);
            this.isQuoteTopic = intent.getBooleanExtra(MCConstant.IS_QUOTE_TOPIC, true);
            this.postsUserList = (ArrayList) intent.getSerializableExtra(MCConstant.POSTS_USER_LIST);
            if (this.postsUserList != null) {
                this.userInfoModels.addAll(this.postsUserList);
            }
        }
        this.permService = new PermServiceImpl(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_publish_topic_activity"));
        super.initViews();
        this.setVisible.setVisibility(8);
        this.titleLabelText.setText(this.resource.getStringId("mc_forum_warn_photo_reply_topic"));
        this.titleEdit.setVisibility(8);
        findViewById(this.resource.getViewId("mc_forum_publish_board_edit")).setVisibility(8);
        findViewById(this.resource.getViewId("mc_froum_line_image")).setVisibility(8);
        this.adView.showAd(this.REPLY_TOPIC_POSITION);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
    }

    /* access modifiers changed from: protected */
    public boolean publicTopic() {
        super.publicTopic();
        String content = this.contentEdit.getText().toString();
        if (StringUtil.isEmpty(content) && !this.hasAudio) {
            warnMessageById("mc_forum_publish_min_length_error");
            return false;
        } else if (StringUtil.isEmpty(content) || content.length() <= 7000) {
            this.canPublishTopic = true;
            if (this.permService.getPermNum(PermConstant.USER_GROUP, "reply", -1) != 1 || this.permService.getPermNum(PermConstant.BOARDS, "reply", this.boardId) != 1) {
                warnMessageByStr(this.resource.getString("mc_forum_permission_cannot_reply_topic"));
                return false;
            } else if (!this.locationComplete) {
                return false;
            } else {
                if (this.hasAudio) {
                    if (this.uploadAudioFileAsyncTask != null) {
                        this.uploadAudioFileAsyncTask.cancel(true);
                    }
                    this.uploadAudioFileAsyncTask = new BasePublishTopicActivityWithAudio.UploadAudioFileAsyncTask();
                    this.uploadAudioFileAsyncTask.execute(new Void[0]);
                } else {
                    uploadAudioSucc();
                }
                return true;
            }
        } else {
            warnMessageById("mc_forum_publish_max_length_error");
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void uploadAudioSucc() {
        super.uploadAudioSucc();
        String content = this.contentEdit.getText().toString();
        PostsService postsService = new PostsServiceImpl(this);
        String contentStr = postsService.createPublishTopicJson(content.trim(), "ß", "á", this.mentionedFriends, this.audioPath, this.audioDuration);
        if (!postsService.isContainsPic(content.trim(), "ß", "á", this.audioPath, this.audioDuration)) {
            if (this.replyAsyncTask != null) {
                this.replyAsyncTask.cancel(true);
            }
            this.replyAsyncTask = new ReplyAsyncTask();
            this.replyAsyncTask.execute(this.boardId + "", this.topicId + "", contentStr, "", this.toReplyId + "", Boolean.toString(this.isQuoteTopic), this.pageFrom + "", this.selectVisibleId + "");
        } else if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.UPLOAD, -1) == 1 && this.permService.getPermNum(PermConstant.BOARDS, PermConstant.UPLOAD, this.boardId) == 1) {
            if (this.replyAsyncTask != null) {
                this.replyAsyncTask.cancel(true);
            }
            this.replyAsyncTask = new ReplyAsyncTask();
            this.replyAsyncTask.execute(this.boardId + "", this.topicId + "", contentStr, "", this.toReplyId + "", Boolean.toString(this.isQuoteTopic), this.pageFrom + "", this.selectVisibleId + "");
        } else {
            warnMessageByStr(this.resource.getString("mc_forum_permission_cannot_upload_pic"));
        }
    }

    /* access modifiers changed from: protected */
    public boolean exitCheckChanged() {
        if (this.isClosed) {
            return false;
        }
        return !this.contentEdit.getText().toString().trim().equals("") || this.hasAudio;
    }

    class ReplyAsyncTask extends AsyncTask<String, Void, String> {
        ReplyAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            ((InputMethodManager) ReplyTopicActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(ReplyTopicActivity.this.contentEdit.getWindowToken(), 0);
            try {
                ReplyTopicActivity.this.showProgressDialog("mc_forum_warn_reply", this);
            } catch (Exception e) {
            }
            ReplyTopicActivity.this.publishIng = true;
            ReplyTopicActivity.this.publishBtn.setEnabled(false);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            return new PostsServiceImpl(ReplyTopicActivity.this).replyTopic(Long.parseLong(params[0]), Long.parseLong(params[1]), params[2], params[3], Long.parseLong(params[4]), new Boolean(params[5]).booleanValue(), Long.parseLong(params[6]), ReplyTopicActivity.this.longitude, ReplyTopicActivity.this.latitude, ReplyTopicActivity.this.locationStr, ReplyTopicActivity.this.requireLocation, Integer.parseInt(params[7]));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            ReplyTopicActivity.this.publishBtn.setEnabled(true);
            ReplyTopicActivity.this.publishIng = false;
            try {
                ReplyTopicActivity.this.hideProgressDialog();
            } catch (Exception e) {
            }
            if (result == null) {
                if (ReplyTopicActivity.getReplyRetrunDelegate() != null) {
                    ReplyTopicActivity.replyRetrunDelegate.replyReturn();
                }
                ReplyTopicActivity.this.warnMessageById("mc_forum_reply_succ");
                ReplyTopicActivity.this.deleteDraft(ReplyTopicActivity.this.draftId);
                ReplyTopicActivity.this.setResult(-1);
                ReplyTopicActivity.this.finish();
            } else if (!result.equals("")) {
                ReplyTopicActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(ReplyTopicActivity.this, result) + "\n" + ReplyTopicActivity.this.getString(ReplyTopicActivity.this.resource.getStringId("mc_forum_warn_publish_fail")));
                ReplyTopicActivity.this.saveAsDraft(ReplyTopicActivity.this.draftId, new TopicDraftModel());
                boolean unused = ReplyTopicActivity.this.isClosed = true;
            }
        }
    }

    public static ReplyRetrunDelegate getReplyRetrunDelegate() {
        return replyRetrunDelegate;
    }

    public static void setReplyRetrunDelegate(ReplyRetrunDelegate replyRetrunDelegate2) {
        replyRetrunDelegate = replyRetrunDelegate2;
    }

    /* access modifiers changed from: protected */
    public void exitSaveEvent() {
        saveAsDraft(this.draftId, new TopicDraftModel());
        finish();
    }

    /* access modifiers changed from: protected */
    public TopicDraftModel saveOtherDataToDraft(TopicDraftModel draft) {
        return draft;
    }

    /* access modifiers changed from: protected */
    public void restoreOtherViewFromDraft(TopicDraftModel draft) {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.replyAsyncTask != null) {
            this.replyAsyncTask.cancel(true);
        }
    }
}
