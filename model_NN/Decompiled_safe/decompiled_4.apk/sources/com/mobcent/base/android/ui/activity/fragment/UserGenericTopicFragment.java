package com.mobcent.base.android.ui.activity.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.ad.android.ui.widget.manager.AdManager;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.TopicListAdapter;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.FavoriteService;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.FavoriteServiceImpl;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public abstract class UserGenericTopicFragment extends BaseListViewFragment implements MCConstant {
    /* access modifiers changed from: private */
    public String TAG = "UserGenericTopicFragment";
    protected int adPosition;
    protected Button backBtn;
    protected int bottomPosition;
    protected int currentPage = 1;
    protected FavoriteService favoriteService;
    protected LayoutInflater inflater;
    /* access modifiers changed from: private */
    public PullToRefreshListView mPullToRefreshListView;
    private MoreTask moreTask;
    protected String nickName;
    protected int pageSize = 50;
    protected TopicListAdapter.PostsClickListener postsClickListener;
    protected PostsService postsService;
    private RefreshTask refreshTask;
    /* access modifiers changed from: private */
    public List<String> refreshimgUrls;
    protected TextView titleText;
    public RelativeLayout tobBarBox;
    protected TopicListAdapter topicListAdapter;
    protected List<TopicModel> topicModelList = new ArrayList();
    protected long userId;

    /* access modifiers changed from: protected */
    public abstract List<TopicModel> getGenericTopicModelList(int i, int i2);

    /* access modifiers changed from: protected */
    public abstract void initSpecialViews();

    public void setUserId(long userId2) {
        this.userId = userId2;
    }

    public void setNickName(String nickName2) {
        this.nickName = nickName2;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.inflater = LayoutInflater.from(this.activity);
        this.postsService = new PostsServiceImpl(this.activity);
        this.favoriteService = new FavoriteServiceImpl(this.activity);
        this.topicListAdapter = new TopicListAdapter(this.activity, this.topicModelList, "", this.mHandler, this.inflater, this.asyncTaskLoaderImage, this.adPosition, this.postsClickListener, this.bottomPosition, this.currentPage);
        this.refreshimgUrls = new ArrayList();
    }

    public View onCreateView(LayoutInflater inflater2, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater2, container, savedInstanceState);
        this.mPullToRefreshListView.onRefresh(true);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater2, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater2.inflate(this.mcResource.getLayoutId("mc_forum_user_generic_topic_fragment"), (ViewGroup) null);
        this.mPullToRefreshListView = (PullToRefreshListView) this.view.findViewById(this.mcResource.getViewId("mc_forum_lv"));
        this.backBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_back_btn"));
        this.titleText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_topic_list_title_btn"));
        this.tobBarBox = (RelativeLayout) this.view.findViewById(this.mcResource.getViewId("mc_forum_top_bar_box"));
        initSpecialViews();
        this.mPullToRefreshListView.setAdapter((ListAdapter) this.topicListAdapter);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.mPullToRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                UserGenericTopicFragment.this.onRefreshs();
            }
        });
        this.mPullToRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                UserGenericTopicFragment.this.onLoadMore();
            }
        });
        this.mPullToRefreshListView.setScrollListener(this.listOnScrollListener);
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserGenericTopicFragment.this.activity.finish();
            }
        });
        this.titleText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserGenericTopicFragment.this.onRefreshs();
            }
        });
    }

    public int getAdPosition() {
        return this.adPosition;
    }

    public void setAdPosition(int adPosition2) {
        this.adPosition = adPosition2;
    }

    public void onRefreshs() {
        if (!(this.refreshTask == null || this.refreshTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.refreshTask.cancel(true);
        }
        this.refreshTask = new RefreshTask();
        this.refreshTask.execute(Integer.valueOf(this.pageSize));
    }

    public void onLoadMore() {
        if (!(this.moreTask == null || this.moreTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.moreTask.cancel(true);
        }
        this.moreTask = new MoreTask();
        this.moreTask.execute(Integer.valueOf(this.pageSize));
    }

    public void onDestroy() {
        super.onDestroy();
        if (!(this.refreshTask == null || this.refreshTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.refreshTask.cancel(true);
        }
        if (!(this.moreTask == null || this.moreTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.moreTask.cancel(true);
        }
        AdManager.getInstance().recyclAdByTag(this.TAG);
    }

    private class RefreshTask extends AsyncTask<Integer, Void, List<TopicModel>> {
        private RefreshTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<TopicModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            UserGenericTopicFragment.this.currentPage = 1;
            if (UserGenericTopicFragment.this.refreshimgUrls != null && !UserGenericTopicFragment.this.refreshimgUrls.isEmpty() && UserGenericTopicFragment.this.refreshimgUrls.size() > 0) {
                UserGenericTopicFragment.this.asyncTaskLoaderImage.recycleBitmaps(UserGenericTopicFragment.this.refreshimgUrls);
            }
        }

        /* access modifiers changed from: protected */
        public List<TopicModel> doInBackground(Integer... params) {
            return UserGenericTopicFragment.this.getGenericTopicModelList(UserGenericTopicFragment.this.currentPage, params[0].intValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<TopicModel> result) {
            super.onPostExecute((Object) result);
            UserGenericTopicFragment.this.mPullToRefreshListView.onRefreshComplete();
            if (result == null) {
                UserGenericTopicFragment.this.mPullToRefreshListView.onBottomRefreshComplete(3, UserGenericTopicFragment.this.getString(UserGenericTopicFragment.this.mcResource.getStringId("mc_forum_warn_no_such_data")));
            } else if (result.isEmpty()) {
                UserGenericTopicFragment.this.topicListAdapter.setTopicList(result);
                UserGenericTopicFragment.this.topicListAdapter.notifyDataSetInvalidated();
                UserGenericTopicFragment.this.topicListAdapter.notifyDataSetChanged();
                UserGenericTopicFragment.this.mPullToRefreshListView.onBottomRefreshComplete(3, UserGenericTopicFragment.this.getString(UserGenericTopicFragment.this.mcResource.getStringId("mc_forum_warn_no_such_data")));
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                Toast.makeText(UserGenericTopicFragment.this.activity, MCForumErrorUtil.convertErrorCode(UserGenericTopicFragment.this.activity, result.get(0).getErrorCode()), 0).show();
                UserGenericTopicFragment.this.mPullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                List unused = UserGenericTopicFragment.this.refreshimgUrls = UserGenericTopicFragment.this.getRefreshImgUrl(result);
                AdManager.getInstance().recyclAdByTag(UserGenericTopicFragment.this.TAG);
                UserGenericTopicFragment.this.mPullToRefreshListView.onRefreshComplete();
                UserGenericTopicFragment.this.topicListAdapter.setTopicList(result);
                UserGenericTopicFragment.this.topicListAdapter.notifyDataSetInvalidated();
                UserGenericTopicFragment.this.topicListAdapter.notifyDataSetChanged();
                if (result.get(0).getHasNext() == 1) {
                    UserGenericTopicFragment.this.mPullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    UserGenericTopicFragment.this.mPullToRefreshListView.onBottomRefreshComplete(3);
                }
                UserGenericTopicFragment.this.topicModelList = result;
            }
        }
    }

    private class MoreTask extends AsyncTask<Integer, Void, List<TopicModel>> {
        private MoreTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<TopicModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            UserGenericTopicFragment.this.currentPage++;
        }

        /* access modifiers changed from: protected */
        public List<TopicModel> doInBackground(Integer... params) {
            return UserGenericTopicFragment.this.getGenericTopicModelList(UserGenericTopicFragment.this.currentPage, params[0].intValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<TopicModel> result) {
            super.onPostExecute((Object) result);
            if (result == null) {
                UserGenericTopicFragment.this.currentPage--;
            } else if (result.isEmpty()) {
                UserGenericTopicFragment.this.mPullToRefreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                UserGenericTopicFragment.this.currentPage--;
                Toast.makeText(UserGenericTopicFragment.this.activity, MCForumErrorUtil.convertErrorCode(UserGenericTopicFragment.this.activity, result.get(0).getErrorCode()), 0).show();
                UserGenericTopicFragment.this.mPullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                List<TopicModel> list = new ArrayList<>();
                list.addAll(UserGenericTopicFragment.this.topicModelList);
                list.addAll(result);
                AdManager.getInstance().recyclAdByTag(UserGenericTopicFragment.this.TAG);
                UserGenericTopicFragment.this.topicListAdapter.setTopicList(result);
                UserGenericTopicFragment.this.topicListAdapter.notifyDataSetInvalidated();
                UserGenericTopicFragment.this.topicListAdapter.notifyDataSetChanged();
                if (result.get(0).getHasNext() == 1) {
                    UserGenericTopicFragment.this.mPullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    UserGenericTopicFragment.this.mPullToRefreshListView.onBottomRefreshComplete(3);
                }
                UserGenericTopicFragment.this.topicModelList = list;
            }
        }
    }

    /* access modifiers changed from: private */
    public List<String> getRefreshImgUrl(List<TopicModel> topicModelList2) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < topicModelList2.size(); i++) {
            isExit(topicModelList2.get(i), topicModelList2);
        }
        return list;
    }

    private boolean isExit(TopicModel model, List<TopicModel> result) {
        int j = result.size();
        for (int i = 0; i < j; i++) {
            TopicModel model2 = result.get(i);
            if (!StringUtil.isEmpty(model.getPicPath()) && !StringUtil.isEmpty(model2.getPicPath()) && model.getPicPath().equals(model2.getPicPath())) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public List<String> getImageURL(int start, int end) {
        List<String> imgUrls = new ArrayList<>();
        for (int i = start; i < end; i++) {
            TopicModel model = this.topicModelList.get(i);
            if (!StringUtil.isEmpty(model.getPicPath())) {
                imgUrls.add(AsyncTaskLoaderImage.formatUrl(model.getBaseUrl() + model.getPicPath(), "100x100"));
            }
        }
        return imgUrls;
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return getImageURL(0, this.topicModelList.size());
    }

    public TopicListAdapter.PostsClickListener getPostsClickListener() {
        return this.postsClickListener;
    }

    public void setPostsClickListener(TopicListAdapter.PostsClickListener postsClickListener2) {
        this.postsClickListener = postsClickListener2;
    }

    public int getadPostion(String position) {
        return new Integer(this.activity.getResources().getString(this.mcResource.getStringId(position))).intValue();
    }
}
