package com.mobcent.base.android.ui.activity.delegate;

public interface GoToPageDelegate {
    void goToPage(int i);
}
