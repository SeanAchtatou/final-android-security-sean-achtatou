package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class BannedShieldedUserAdapterHolder {
    private TextView boardText;
    private TextView boardTitleText;
    private Button cancelBannedBtn;
    private Button cancelShieldedBtn;
    private Button chatBtn;
    private TextView reasonText;
    private TextView reasonTitleText;
    private TextView timeText;
    private TextView timeTitleText;
    private ImageView userIconImage;
    private TextView userNameText;

    public ImageView getUserIconImage() {
        return this.userIconImage;
    }

    public void setUserIconImage(ImageView userIconImage2) {
        this.userIconImage = userIconImage2;
    }

    public TextView getUserNameText() {
        return this.userNameText;
    }

    public void setUserNameText(TextView userNameText2) {
        this.userNameText = userNameText2;
    }

    public Button getChatBtn() {
        return this.chatBtn;
    }

    public void setChatBtn(Button chatBtn2) {
        this.chatBtn = chatBtn2;
    }

    public Button getCancelBannedBtn() {
        return this.cancelBannedBtn;
    }

    public void setCancelBannedBtn(Button cancelBannedBtn2) {
        this.cancelBannedBtn = cancelBannedBtn2;
    }

    public Button getCancelShieldedBtn() {
        return this.cancelShieldedBtn;
    }

    public void setCancelShieldedBtn(Button cancelShieldedBtn2) {
        this.cancelShieldedBtn = cancelShieldedBtn2;
    }

    public TextView getReasonTitleText() {
        return this.reasonTitleText;
    }

    public void setReasonTitleText(TextView reasonTitleText2) {
        this.reasonTitleText = reasonTitleText2;
    }

    public TextView getReasonText() {
        return this.reasonText;
    }

    public void setReasonText(TextView reasonText2) {
        this.reasonText = reasonText2;
    }

    public TextView getTimeTitleText() {
        return this.timeTitleText;
    }

    public void setTimeTitleText(TextView timeTitleText2) {
        this.timeTitleText = timeTitleText2;
    }

    public TextView getTimeText() {
        return this.timeText;
    }

    public void setTimeText(TextView timeText2) {
        this.timeText = timeText2;
    }

    public TextView getBoardTitleText() {
        return this.boardTitleText;
    }

    public void setBoardTitleText(TextView boardTitleText2) {
        this.boardTitleText = boardTitleText2;
    }

    public TextView getBoardText() {
        return this.boardText;
    }

    public void setBoardText(TextView boardText2) {
        this.boardText = boardText2;
    }
}
