package com.mobcent.base.android.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.widget.cropimage.CropImage;
import com.mobcent.base.android.ui.widget.cropimage.CropImageView;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.util.MCLogUtil;

public class CropImageActivity extends Activity {
    public static final int DEFAULT_MAX_WIDTH = 800;
    private Button cancelBtn;
    /* access modifiers changed from: private */
    public Bitmap mBitmap;
    /* access modifiers changed from: private */
    public CropImage mCrop;
    private CropImageView mImageView;
    public MCResource resource;
    private Button saveBtn;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.resource = MCResource.getInstance(getApplicationContext());
        setContentView(this.resource.getLayoutId("mc_forum_crop_activity"));
        init();
    }

    private void init() {
        Intent intent = getIntent();
        String inputPath = intent.getStringExtra(MCConstant.INPUT_PATH);
        int maxWidth = intent.getIntExtra(MCConstant.ZOOM_MAX_WIDTH, DEFAULT_MAX_WIDTH);
        final String outPath = intent.getStringExtra(MCConstant.OUT_PATH);
        this.mBitmap = getBitmapFromMedia(this, inputPath, 0.8f, maxWidth);
        this.mImageView = (CropImageView) findViewById(this.resource.getViewId("mc_forum_image"));
        this.saveBtn = (Button) findViewById(this.resource.getViewId("mc_forum_save_btn"));
        this.cancelBtn = (Button) findViewById(this.resource.getViewId("mc_forum_cancel_btn"));
        this.mImageView.setImageBitmap(this.mBitmap);
        this.mImageView.setImageBitmapResetBase(this.mBitmap, true);
        this.mCrop = new CropImage(this, this.mImageView);
        this.mCrop.crop(this.mBitmap);
        this.saveBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CropImageActivity.this.mCrop.saveToLocal(CropImageActivity.this.mCrop.cropAndSave(CropImageActivity.this.mBitmap), outPath);
                Intent intent = new Intent();
                intent.putExtra(MCConstant.OUT_PATH, outPath);
                CropImageActivity.this.setResult(-1, intent);
                CropImageActivity.this.finish();
            }
        });
        this.cancelBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CropImageActivity.this.finish();
            }
        });
    }

    public static Bitmap getBitmapFromMedia(Context context, String pathName, float scale, int maxWidth) {
        Bitmap bitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(pathName, options);
            options.inJustDecodeBounds = false;
            int outputWidth = options.outWidth;
            MCLogUtil.i("ImageUtil", "outputWidth2 = " + outputWidth);
            if (outputWidth < maxWidth) {
                bitmap = BitmapFactory.decodeFile(pathName);
            } else {
                float b = ((float) outputWidth) / ((float) maxWidth);
                MCLogUtil.i("ImageUtil", "b = " + b);
                options.inSampleSize = Math.round(b);
                MCLogUtil.i("ImageUtil", "options.inSampleSize = " + options.inSampleSize);
                bitmap = BitmapFactory.decodeFile(pathName, options);
            }
            return bitmap;
        } catch (OutOfMemoryError e) {
            System.gc();
            return null;
        } catch (Exception e2) {
            return null;
        }
    }
}
