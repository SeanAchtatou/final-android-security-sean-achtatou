package com.mobcent.base.android.ui.activity.asyncTaskLoader;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.service.AnnounceService;
import com.mobcent.forum.android.service.impl.AnnounceServiceImpl;
import com.mobcent.forum.android.util.StringUtil;

public class AnnounceAsyncTask extends AsyncTask<Object, Void, String> {
    private int announceId;
    private AnnounceService announceService;
    private Context context;
    private TaskExecuteDelegate taskExecuteDelegate;
    private int verify;

    public AnnounceAsyncTask(Context context2, int verify2, int announceId2) {
        this.announceService = new AnnounceServiceImpl(context2);
        this.context = context2;
        this.verify = verify2;
        this.announceId = announceId2;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Object... params) {
        return this.announceService.verifyAnno(this.verify, this.announceId);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String result) {
        if (!StringUtil.isEmpty(result)) {
            Toast.makeText(this.context, MCForumErrorUtil.convertErrorCode(this.context, result), 1).show();
            if (this.taskExecuteDelegate != null) {
                this.taskExecuteDelegate.executeFail();
            }
        } else if (this.taskExecuteDelegate != null) {
            this.taskExecuteDelegate.executeSuccess();
        }
    }

    public TaskExecuteDelegate getTaskExecuteDelegate() {
        return this.taskExecuteDelegate;
    }

    public void setTaskExecuteDelegate(TaskExecuteDelegate taskExecuteDelegate2) {
        this.taskExecuteDelegate = taskExecuteDelegate2;
    }
}
