package com.mobcent.base.android.ui.activity.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;
import com.mobcent.base.android.ui.widget.gifview.GifCache;
import com.mobcent.base.android.ui.widget.gifview.GifView;
import com.mobcent.base.android.ui.widget.pointimageview.ImageViewPager;
import com.mobcent.base.android.ui.widget.pointimageview.PointImageView;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.RichImageModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ImageViewerFragment extends BaseFragment {
    private List<AsyncTask> asyncTaskList;
    /* access modifiers changed from: private */
    public GifView gifImageview;
    /* access modifiers changed from: private */
    public List<GifView> gifViewList;
    private String imageCacheKey = "imageviewer";
    private RichImageModel imageModel = null;
    private int imageSourceType;
    private String imageUrl = null;
    /* access modifiers changed from: private */
    public List<String> imageUrls;
    private ImageView imageView;
    /* access modifiers changed from: private */
    public ImageViewerFragmentSelectedListener imageViewerFragmentSelectedListener;
    private RelativeLayout imageviewerFragmentPagerItem;
    private PointImageView pointImageView;
    /* access modifiers changed from: private */
    public ImageView pointLoadingFail;
    /* access modifiers changed from: private */
    public MCProgressBar pointLoadingProgress;

    public interface ImageViewerFragmentSelectedListener {
        ViewPager getViewPager();

        void onSelected();

        void onSingleTap(MotionEvent motionEvent);
    }

    public void setImageSourceType(int imageSourceType2) {
        this.imageSourceType = imageSourceType2;
    }

    public ImageView getImageView() {
        return this.imageView;
    }

    public void setImageView(ImageView imageView2) {
        this.imageView = imageView2;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public RichImageModel getImageModel() {
        return this.imageModel;
    }

    public void setImageModel(RichImageModel imageModel2) {
        this.imageModel = imageModel2;
        this.imageUrl = imageModel2.getImageUrl();
        this.imageCacheKey += this.imageUrl;
    }

    public static ImageViewerFragment newInstance() {
        return new ImageViewerFragment();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.imageViewerFragmentSelectedListener = (ImageViewerFragmentSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "must implement OnArticleSelectedListener");
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.asyncTaskList = new ArrayList();
        this.gifViewList = new ArrayList();
        this.imageUrls = new ArrayList();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.imageviewerFragmentPagerItem = (RelativeLayout) inflater.inflate(this.mcResource.getLayoutId("mc_forum_imageviewer_fragment_pager_item"), container, false);
        this.pointImageView = (PointImageView) this.imageviewerFragmentPagerItem.findViewById(this.mcResource.getViewId("mc_forum_point_imageview"));
        this.gifImageview = (GifView) this.imageviewerFragmentPagerItem.findViewById(this.mcResource.getViewId("mc_forum_gif_imageview"));
        this.pointLoadingProgress = (MCProgressBar) this.imageviewerFragmentPagerItem.findViewById(this.mcResource.getViewId("mc_forum_download_progress_bar"));
        this.pointLoadingFail = (ImageView) this.imageviewerFragmentPagerItem.findViewById(this.mcResource.getViewId("mc_forum_point_loading_fail"));
        this.pointImageView.setPointImageViewSingleTapListener(new PointImageView.PointImageViewSingleTapListener() {
            public void onSingleTap(MotionEvent e) {
                ImageViewerFragment.this.imageViewerFragmentSelectedListener.onSingleTap(e);
            }
        });
        return this.imageviewerFragmentPagerItem;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.pointLoadingProgress.show();
        if (this.imageUrl != null) {
            if (MCConstant.PIC_GIF.equals(StringUtil.getExtensionName(this.imageUrl).toLowerCase())) {
                this.pointImageView.setVisibility(8);
                this.gifImageview.setVisibility(0);
                GifCache inputStreamCache = new GifCache(getActivity(), this.imageUrl, new GifCache.GifCallback() {
                    public void onGifLoaded(InputStream is, String url) {
                        if (is != null) {
                            ImageViewerFragment.this.pointLoadingProgress.stop();
                            ImageViewerFragment.this.pointLoadingProgress.setVisibility(8);
                            ImageViewerFragment.this.gifImageview.setGifViewListener(new GifView.GifViewListener() {
                                public void onSingleTap(MotionEvent e) {
                                    ImageViewerFragment.this.imageViewerFragmentSelectedListener.onSingleTap(e);
                                }

                                public void done(final boolean isFail) {
                                    ImageViewerFragment.this.mHandler.post(new Runnable() {
                                        public void run() {
                                            if (isFail) {
                                                ImageViewerFragment.this.pointLoadingFail.setVisibility(0);
                                            } else {
                                                ImageViewerFragment.this.pointLoadingFail.setVisibility(8);
                                            }
                                        }
                                    });
                                }
                            });
                            ImageViewerFragment.this.gifImageview.setVisibility(0);
                            ImageViewerFragment.this.setImageView(ImageViewerFragment.this.gifImageview);
                            ImageViewerFragment.this.gifImageview.setGifImageType(GifView.GifImageType.SYNC_DECODER);
                            ImageViewerFragment.this.gifImageview.setGifImage(is);
                            ImageViewerFragment.this.gifViewList.add(ImageViewerFragment.this.gifImageview);
                        }
                    }
                });
                this.asyncTaskList.add(inputStreamCache);
                inputStreamCache.execute(new Void[0]);
            } else {
                this.pointImageView.setVisibility(0);
                this.gifImageview.setVisibility(8);
                setImageView(this.pointImageView);
                loadImage(this.pointImageView, this.imageCacheKey, this.imageUrl);
            }
            this.imageViewerFragmentSelectedListener.onSelected();
        }
    }

    public void loadImage(final PointImageView imageView2, final String key, String imageUrl2) {
        Log.i("imageSourceType", this.imageSourceType + "");
        if (SharedPreferencesDB.getInstance(getActivity()).getPicModeFlag() || this.imageSourceType == 1) {
            AsyncTaskLoaderImage.getInstance(getActivity(), key).loadAsync(imageUrl2, new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, final String url) {
                    ImageViewerFragment.this.mHandler.post(new Runnable() {
                        public void run() {
                            ImageViewerFragment.this.pointLoadingProgress.stop();
                            ImageViewerFragment.this.pointLoadingProgress.setVisibility(8);
                            if (image == null) {
                                ImageViewerFragment.this.pointLoadingFail.setVisibility(0);
                            } else if (ImageViewerFragment.this.isVisible()) {
                                if (!image.isRecycled()) {
                                    imageView2.setImageBitmap(image);
                                    imageView2.setViewPager((ImageViewPager) ImageViewerFragment.this.imageViewerFragmentSelectedListener.getViewPager());
                                    ImageViewerFragment.this.imageUrls.add(url);
                                    return;
                                }
                                ImageViewerFragment.this.pointLoadingFail.setVisibility(0);
                            } else if (!image.isRecycled()) {
                                AsyncTaskLoaderImage.getInstance(ImageViewerFragment.this.getActivity(), key).recycleBitmap(url);
                            }
                        }
                    });
                }
            });
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (this.asyncTaskList != null && !this.asyncTaskList.isEmpty()) {
            for (AsyncTask asyncTask : this.asyncTaskList) {
                asyncTask.cancel(true);
            }
        }
        AsyncTaskLoaderImage.getInstance(getActivity(), this.imageCacheKey).recycleBitmaps(this.imageUrls);
        if (this.gifViewList != null && !this.gifViewList.isEmpty()) {
            for (int i = 0; i < this.gifViewList.size(); i++) {
                this.gifViewList.get(i).free();
            }
        }
        this.gifViewList.clear();
    }
}
