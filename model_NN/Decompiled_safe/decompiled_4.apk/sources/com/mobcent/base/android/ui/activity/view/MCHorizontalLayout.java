package com.mobcent.base.android.ui.activity.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Scroller;
import com.mobcent.base.forum.android.util.MCPhoneUtil;

public class MCHorizontalLayout extends LinearLayout {
    private View leftLayout;
    private Scroller mScroller;
    private View rightLayout;
    private int screenWidth = 0;

    public MCHorizontalLayout(Context context) {
        super(context);
        init();
    }

    public MCHorizontalLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        this.mScroller = new Scroller(getContext());
        this.screenWidth = MCPhoneUtil.getDisplayWidth((Activity) getContext());
    }

    public void computeScroll() {
        if (this.mScroller.computeScrollOffset()) {
            scrollTo(this.mScroller.getCurrX(), this.mScroller.getCurrY());
            postInvalidate();
        }
    }

    public void scrollerToLeft() {
        if (this.mScroller.isFinished()) {
            int x = getScrollX();
            if (x == 0) {
                x = this.screenWidth;
            }
            this.mScroller.startScroll(x, 0, -this.screenWidth, 0, 300);
            invalidate();
        }
    }

    public void scrollerToRight() {
        if (this.mScroller.isFinished()) {
            this.mScroller.startScroll(getScrollX(), 0, this.screenWidth, 0, 300);
            invalidate();
        }
    }

    public View getLeftLayout() {
        return this.leftLayout;
    }

    public void setLeftLayout(View leftLayout2) {
        this.leftLayout = leftLayout2;
    }

    public View getRightLayout() {
        return this.rightLayout;
    }

    public void setRightLayout(View rightLayout2) {
        this.rightLayout = rightLayout2;
    }
}
