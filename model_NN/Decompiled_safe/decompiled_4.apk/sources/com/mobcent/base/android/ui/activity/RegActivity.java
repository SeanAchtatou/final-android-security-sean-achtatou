package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.mobcent.base.android.constant.EmailResourceConstant;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.RegLoginListAdapter;
import com.mobcent.base.android.ui.activity.delegate.RegLoginFinishDelegate;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserLoginFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.db.UserJsonDBUtil;
import com.mobcent.forum.android.db.helper.UserJsonDBHelper;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.MD5Util;
import com.mobcent.forum.android.util.StringUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class RegActivity extends BaseActivity implements MCConstant {
    /* access modifiers changed from: private */
    public static RegLoginFinishDelegate loginDelegate;
    private Button aboutBtn;
    private Button backBtn;
    /* access modifiers changed from: private */
    public int emailMaxLen = 64;
    /* access modifiers changed from: private */
    public HashMap<String, Serializable> goParam;
    /* access modifiers changed from: private */
    public Class<?> goToActivityClass;
    /* access modifiers changed from: private */
    public boolean isRegShowPwd = false;
    /* access modifiers changed from: private */
    public int pwdMaxLen = 20;
    /* access modifiers changed from: private */
    public int pwdMinLen = 6;
    /* access modifiers changed from: private */
    public RegAsyncTask regAsyncTask;
    private RelativeLayout regBox;
    /* access modifiers changed from: private */
    public RelativeLayout regEmailBox;
    /* access modifiers changed from: private */
    public EditText regEmailEdit;
    private ListView regEmailList;
    private RelativeLayout regLoginBox;
    /* access modifiers changed from: private */
    public RegLoginListAdapter regLoginListAdapter;
    /* access modifiers changed from: private */
    public EditText regPwdEdit;
    /* access modifiers changed from: private */
    public ImageButton regShowPwdBtn;
    private Button regSubmitBtn;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            this.goToActivityClass = (Class) intent.getSerializableExtra(MCConstant.TAG);
            this.goParam = (HashMap) intent.getSerializableExtra(MCConstant.GO_PARAM);
        }
        this.regLoginListAdapter = new RegLoginListAdapter(this, new ArrayList(), this.resource);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_user_register_activity"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.regSubmitBtn = (Button) findViewById(this.resource.getViewId("mc_forum_user_register_submit_btn"));
        this.regEmailEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_user_register_email_edit"));
        this.regPwdEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_user_register_password_edit"));
        this.regBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_user_register_box"));
        this.regEmailBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_user_register_email_box"));
        this.regShowPwdBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_user_show_password_btn"));
        this.regEmailList = (ListView) findViewById(this.resource.getViewId("mc_forum_user_register_email_list"));
        this.regLoginBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_user_login_register_login_box"));
        this.regEmailList.setAdapter((ListAdapter) this.regLoginListAdapter);
        this.aboutBtn = (Button) findViewById(this.resource.getViewId("mc_forum_about"));
        if (MCForumHelper.isCopyright(this)) {
            findViewById(this.resource.getViewId("mc_forum_power_by")).setVisibility(8);
            findViewById(this.resource.getViewId("mc_forum_register_text")).setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.regShowPwdBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!RegActivity.this.isRegShowPwd) {
                    RegActivity.this.regShowPwdBtn.setBackgroundResource(RegActivity.this.resource.getDrawableId("mc_forum_input_show_pwd_d"));
                    boolean unused = RegActivity.this.isRegShowPwd = true;
                    RegActivity.this.regPwdEdit.setInputType(144);
                    Editable etable = RegActivity.this.regPwdEdit.getText();
                    Selection.setSelection(etable, etable.length());
                    return;
                }
                RegActivity.this.regShowPwdBtn.setBackgroundResource(RegActivity.this.resource.getDrawableId("mc_forum_input_show_pwd_n"));
                boolean unused2 = RegActivity.this.isRegShowPwd = false;
                RegActivity.this.regPwdEdit.setInputType(129);
                Editable etable2 = RegActivity.this.regPwdEdit.getText();
                Selection.setSelection(etable2, etable2.length());
            }
        });
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RegActivity.this.back();
            }
        });
        this.regPwdEdit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RegActivity.this.regEmailBox.setVisibility(8);
            }
        });
        this.regPwdEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode != 66) {
                    return false;
                }
                RegActivity.this.hideSoftKeyboard();
                return false;
            }
        });
        this.regSubmitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String email = RegActivity.this.regEmailEdit.getText().toString();
                if (!StringUtil.isEmail(email)) {
                    RegActivity.this.warnMessageById("mc_forum_user_email_format_error_warn");
                } else if (email.length() > RegActivity.this.emailMaxLen) {
                    RegActivity.this.warnMessageById("mc_forum_user_email_length_error_warn");
                } else {
                    String password = RegActivity.this.regPwdEdit.getText().toString();
                    if (password.length() < RegActivity.this.pwdMinLen || password.length() > RegActivity.this.pwdMaxLen) {
                        RegActivity.this.warnMessageById("mc_forum_user_password_length_error_warn");
                    } else if (!StringUtil.isPwdMatchRule(password)) {
                        RegActivity.this.warnMessageById("mc_forum_user_password_format_error_warn");
                    } else {
                        String password2 = MD5Util.toMD5(password);
                        if (RegActivity.this.regAsyncTask != null) {
                            RegActivity.this.regAsyncTask.cancel(true);
                        }
                        RegAsyncTask unused = RegActivity.this.regAsyncTask = new RegAsyncTask();
                        RegActivity.this.regAsyncTask.execute(email, password2);
                    }
                }
            }
        });
        this.regEmailEdit.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Editable editable = RegActivity.this.regEmailEdit.getText();
                if (editable.length() > 0) {
                    RegActivity.this.regEmailBox.setVisibility(0);
                    RegActivity.this.notifyData(editable.toString());
                    return;
                }
                RegActivity.this.regEmailBox.setVisibility(8);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        this.regEmailList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                RegActivity.this.regEmailEdit.setText(RegActivity.this.regLoginListAdapter.getEmailSections().get(position));
                Editable regEmail = RegActivity.this.regEmailEdit.getText();
                Selection.setSelection(regEmail, regEmail.length());
                RegActivity.this.regEmailBox.setVisibility(8);
            }
        });
        this.regLoginBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RegActivity.this.regEmailBox.setVisibility(8);
            }
        });
        this.aboutBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RegActivity.this.startActivity(new Intent(RegActivity.this, AboutActivity.class));
            }
        });
    }

    /* access modifiers changed from: private */
    public void notifyData(String str) {
        new ArrayList();
        this.regLoginListAdapter.setEmailSections(EmailResourceConstant.getResourceConstant().convertEmail(str));
        this.regLoginListAdapter.notifyDataSetChanged();
        this.regLoginListAdapter.notifyDataSetInvalidated();
    }

    class RegAsyncTask extends AsyncTask<String, Void, UserInfoModel> {
        RegAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            RegActivity.this.showProgressDialog("mc_forum_warn_register", this);
        }

        /* access modifiers changed from: protected */
        public UserInfoModel doInBackground(String... params) {
            UserService userService = new UserServiceImpl(RegActivity.this);
            if (params[0] == null && params[1] == null) {
                return null;
            }
            return userService.regUser(params[0], params[1]);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(UserInfoModel result) {
            RegActivity.this.hideProgressDialog();
            if (result == null) {
                return;
            }
            if (result.getErrorCode() == null || "".equals(result.getErrorCode())) {
                UserJsonDBUtil.getInstance(RegActivity.this).savePersonalInfo(result.getUserId(), 1, UserJsonDBHelper.buildPersonalInfo(result));
                RegActivity.this.warnMessageById("mc_forum_user_register_succ");
                RegActivity.loginDelegate.activityFinish();
                UserLoginFragmentActivity.finishAll();
                Intent intent = new Intent(RegActivity.this, RegSuccActivity.class);
                RegActivity.this.clearNotification();
                if (RegActivity.this.goToActivityClass != null) {
                    intent.putExtra(MCConstant.TAG, RegActivity.this.goToActivityClass);
                    intent.putExtra(MCConstant.GO_PARAM, RegActivity.this.goParam);
                }
                RegActivity.this.startActivity(intent);
                RegActivity.this.finish();
                return;
            }
            RegActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(RegActivity.this, result.getErrorCode()));
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean back = false;
        if (keyCode == 4) {
            back = backEventClickListener();
        }
        if (!back) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean backEventClickListener() {
        if (this.regEmailBox.getVisibility() != 0 || this.regBox.getVisibility() != 0) {
            return true;
        }
        this.regEmailBox.setVisibility(8);
        return false;
    }

    /* access modifiers changed from: protected */
    public void goToTargetActivity() {
        if (this.goToActivityClass != null) {
            Intent intent = new Intent(this, this.goToActivityClass);
            if (this.goParam != null) {
                for (String key : this.goParam.keySet()) {
                    intent.putExtra(key, this.goParam.get(key));
                }
            }
            startActivity(intent);
        }
    }

    public Class<?> getGoToActivityClass() {
        return this.goToActivityClass;
    }

    public void setGoToActivityClass(Class<?> goToActivityClass2) {
        this.goToActivityClass = goToActivityClass2;
    }

    public HashMap<String, Serializable> getGoParam() {
        return this.goParam;
    }

    public void setGoParam(HashMap<String, Serializable> goParam2) {
        this.goParam = goParam2;
    }

    public static RegLoginFinishDelegate getLoginDelegate() {
        return loginDelegate;
    }

    public static void setLoginDelegate(RegLoginFinishDelegate loginDelegate2) {
        loginDelegate = loginDelegate2;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.regAsyncTask != null) {
            this.regAsyncTask.cancel(true);
        }
    }
}
