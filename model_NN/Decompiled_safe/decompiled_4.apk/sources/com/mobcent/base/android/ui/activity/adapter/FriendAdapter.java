package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.location.LocationClientOption;
import com.mobcent.ad.android.model.AdModel;
import com.mobcent.ad.android.ui.widget.AdView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.holder.UserFriendsAdapterHolder;
import com.mobcent.base.android.ui.activity.fragment.UserFollowFragment;
import com.mobcent.base.android.ui.activity.view.MCLevelView;
import com.mobcent.base.forum.android.util.MCColorUtil;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.HeartMsgService;
import com.mobcent.forum.android.service.MentionFriendService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.HeartMsgServiceImpl;
import com.mobcent.forum.android.service.impl.MentionFriendServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FriendAdapter extends BaseAdapter implements MCConstant {
    private final String TEXT_BLANL = "   ";
    private HashMap<Integer, List<AdModel>> adHashMap;
    private int adPosition;
    /* access modifiers changed from: private */
    public List<AsyncTask> asyncTaskList;
    private AsyncTaskLoaderImage asyncTaskLoaderImage;
    protected FriendsClickListener clickListener;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public long currentUserId;
    private int flagUserstatus;
    /* access modifiers changed from: private */
    public FollowUserAsyncTask followUserAsyncTask;
    private Fragment fragment;
    /* access modifiers changed from: private */
    public HeartMsgService heartMsgService;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public MCResource mcResource;
    /* access modifiers changed from: private */
    public MentionFriendService mentionFriendService;
    private int page;
    /* access modifiers changed from: private */
    public UnfollowUserAsyncTask unfollowUserAsyncTask;
    private List<UserInfoModel> userInfoList;
    /* access modifiers changed from: private */
    public UserService userService;

    public interface FriendsClickListener {
        void onMsgChatRoomClick(View view, UserInfoModel userInfoModel);

        void onUserHomeClick(View view, UserInfoModel userInfoModel);
    }

    public FriendsClickListener getClickListener() {
        return this.clickListener;
    }

    public void setClickListener(FriendsClickListener clickListener2) {
        this.clickListener = clickListener2;
    }

    public FriendAdapter(Context context2, List<UserInfoModel> userInfoList2, Handler mHandler2, AsyncTaskLoaderImage asyncTaskLoaderImage2, LayoutInflater inflater2, int flagUserstatus2, int adPosition2, Fragment fragment2, int page2) {
        this.context = context2;
        this.userInfoList = userInfoList2;
        this.inflater = inflater2;
        this.mHandler = mHandler2;
        this.mcResource = MCResource.getInstance(context2);
        this.heartMsgService = new HeartMsgServiceImpl(context2);
        this.userService = new UserServiceImpl(context2);
        this.mentionFriendService = new MentionFriendServiceImpl(context2);
        this.currentUserId = this.userService.getLoginUserId();
        this.flagUserstatus = flagUserstatus2;
        this.asyncTaskList = new ArrayList();
        this.asyncTaskLoaderImage = asyncTaskLoaderImage2;
        this.adPosition = adPosition2;
        this.fragment = fragment2;
    }

    public List<UserInfoModel> getUserInfoList() {
        return this.userInfoList;
    }

    public void setUserInfoList(List<UserInfoModel> userInfoList2) {
        this.userInfoList = userInfoList2;
    }

    public HashMap<Integer, List<AdModel>> getAdHashMap() {
        return this.adHashMap;
    }

    public void setAdHashMap(HashMap<Integer, List<AdModel>> adHashMap2) {
        this.adHashMap = adHashMap2;
    }

    public int getCount() {
        return getUserInfoList().size();
    }

    public Object getItem(int position) {
        return getUserInfoList().get(position);
    }

    public long getItemId(int position) {
        return getUserInfoList().get(position).getUserId();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        String s;
        final UserInfoModel userInfoModel = getUserInfoList().get(position);
        View convertView2 = getUserFriendsConvertView(convertView);
        final UserFriendsAdapterHolder holder = (UserFriendsAdapterHolder) convertView2.getTag();
        if (this.fragment instanceof UserFollowFragment) {
            holder.getAdView().free();
            holder.getTopAdView().free();
            if (position == 0) {
                holder.getTopAdView().setVisibility(0);
                holder.getTopAdView().showAd(MCConstant.TAG, this.adPosition, position);
            } else if (position == (((position - 1) * 20) + position) - 1) {
                holder.getAdView().showAd(MCConstant.TAG, new Integer(this.context.getResources().getString(this.mcResource.getStringId("mc_forum_user_friend_position_bottom"))).intValue(), position);
            }
        }
        if (this.flagUserstatus == 1) {
            holder.getUserLocationBox().setVisibility(8);
            holder.getLevelBox().setVisibility(0);
            holder.getLevelBox().removeAllViews();
            new MCLevelView(this.context, userInfoModel.getLevel(), holder.getLevelBox(), 1);
        } else if (this.flagUserstatus == 0) {
            holder.getFollowUser().setVisibility(8);
            holder.getUserLocationBox().setVisibility(8);
            holder.getLevelBox().setVisibility(0);
            holder.getLevelBox().removeAllViews();
            new MCLevelView(this.context, userInfoModel.getLevel(), holder.getLevelBox(), 1);
        } else if (this.flagUserstatus == 2) {
            if (!StringUtil.isEmpty(userInfoModel.getLocation()) || userInfoModel.getDistance() >= 0) {
                holder.getLevelBox().setVisibility(8);
                holder.getUserGenderText().setPadding(0, 5, 0, 0);
                holder.getUserStatusText().setPadding(0, 5, 0, 0);
                holder.getUserLocationBox().setVisibility(0);
                String location = userInfoModel.getLocation();
                int distance = userInfoModel.getDistance() / LocationClientOption.MIN_SCAN_SPAN;
                if (distance == 0) {
                    s = location + "   " + MCStringBundleUtil.resolveString(this.mcResource.getStringId("mc_forum_surround_distance_str1"), userInfoModel.getDistance() + "", this.context);
                } else {
                    s = location + "   " + MCStringBundleUtil.resolveString(this.mcResource.getStringId("mc_forum_surround_distance_str2"), distance + "", this.context);
                }
                holder.getUserLocationText().setText(s);
                MCColorUtil.setTextViewPart(this.context, holder.getUserLocationText(), s, location.length(), s.length(), "mc_forum_text_unapparent_color");
            } else {
                holder.getUserLocationBox().setVisibility(8);
            }
        }
        holder.getUserNameText().setText(userInfoModel.getNickname());
        if (userInfoModel.getGender() == 1) {
            holder.getUserGenderText().setText(this.context.getResources().getString(this.mcResource.getStringId("mc_forum_user_gender_man")));
        } else {
            holder.getUserGenderText().setText(this.context.getResources().getString(this.mcResource.getStringId("mc_forum_user_gender_woman")));
        }
        if (userInfoModel.getStatus() == 1) {
            holder.getUserStatusText().setText(this.context.getResources().getString(this.mcResource.getStringId("mc_forum_user_status_online")));
            holder.getUserStatusText().setTextColor(this.mcResource.getColor("mc_forum_text_hight_color"));
        } else {
            holder.getUserStatusText().setText(this.context.getResources().getString(this.mcResource.getStringId("mc_forum_user_status_offline")));
            holder.getUserStatusText().setText("");
            holder.getUserStatusText().setTextColor(this.mcResource.getColor("mc_forum_text_unapparent_color"));
        }
        if (userInfoModel.getBlackStatus() == 1) {
            holder.getBlackUserBtn().setBackgroundResource(this.mcResource.getDrawableId("mc_forum_icon17"));
        } else {
            holder.getBlackUserBtn().setBackgroundResource(this.mcResource.getDrawableId("mc_forum_icon19"));
        }
        if (userInfoModel.getUserId() == this.userService.getLoginUserId()) {
            holder.getFollowUser().setVisibility(8);
            holder.getBlackUserBtn().setVisibility(8);
        } else if (userInfoModel.getIsFollow() == 1) {
            holder.getFollowUser().setBackgroundResource(this.mcResource.getDrawableId("mc_forum_icon16"));
        } else {
            holder.getFollowUser().setBackgroundResource(this.mcResource.getDrawableId("mc_forum_icon15"));
        }
        holder.getFollowUser().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (userInfoModel.getIsFollow() == 1) {
                    UnfollowUserAsyncTask unused = FriendAdapter.this.unfollowUserAsyncTask = new UnfollowUserAsyncTask(holder);
                    FriendAdapter.this.asyncTaskList.add(FriendAdapter.this.unfollowUserAsyncTask);
                    FriendAdapter.this.unfollowUserAsyncTask.execute(new Object[]{Long.valueOf(FriendAdapter.this.currentUserId), userInfoModel});
                    return;
                }
                FollowUserAsyncTask unused2 = FriendAdapter.this.followUserAsyncTask = new FollowUserAsyncTask(holder);
                FriendAdapter.this.asyncTaskList.add(FriendAdapter.this.followUserAsyncTask);
                FriendAdapter.this.followUserAsyncTask.execute(new Object[]{Long.valueOf(FriendAdapter.this.currentUserId), userInfoModel});
            }
        });
        holder.getBlackUserBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (userInfoModel.getBlackStatus() == 1) {
                    BlackAsyncTask blackAsyncTask = new BlackAsyncTask(holder);
                    FriendAdapter.this.asyncTaskList.add(blackAsyncTask);
                    blackAsyncTask.execute(0, Long.valueOf(new UserServiceImpl(FriendAdapter.this.context).getLoginUserId()), userInfoModel);
                } else if (FriendAdapter.this.getClickListener() != null) {
                    FriendAdapter.this.clickListener.onMsgChatRoomClick(arg0, userInfoModel);
                }
            }
        });
        holder.getUserIconImg().setBackgroundResource(this.mcResource.getDrawableId("mc_forum_head"));
        updateUserIconImage(userInfoModel.getIcon(), convertView2, holder.getUserIconImg());
        holder.getUserIconImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (FriendAdapter.this.getClickListener() != null) {
                    FriendAdapter.this.clickListener.onUserHomeClick(v, userInfoModel);
                }
            }
        });
        return convertView2;
    }

    private abstract class AbstractAsyncTask extends AsyncTask<Object, Void, String> {
        protected UserFriendsAdapterHolder holder;
        protected UserInfoModel userInfo;

        /* access modifiers changed from: protected */
        public abstract void changeState(UserInfoModel userInfoModel);

        /* access modifiers changed from: protected */
        public abstract String getFollowCode(long j, long j2);

        /* access modifiers changed from: protected */
        public abstract void onPreExecute();

        public AbstractAsyncTask(UserFriendsAdapterHolder holder2) {
            this.holder = holder2;
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Object... params) {
            long currentUserId = ((Long) params[0]).longValue();
            this.userInfo = (UserInfoModel) params[1];
            return getFollowCode(currentUserId, this.userInfo.getUserId());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            this.holder.getFollowUser().setEnabled(true);
            if (result != null) {
                Toast.makeText(FriendAdapter.this.context, MCForumErrorUtil.convertErrorCode(FriendAdapter.this.context, result), 0).show();
            } else {
                changeState(this.userInfo);
            }
        }
    }

    private class FollowUserAsyncTask extends AbstractAsyncTask {
        public FollowUserAsyncTask(UserFriendsAdapterHolder holder) {
            super(holder);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.holder.getFollowUser().setEnabled(false);
        }

        /* access modifiers changed from: protected */
        public String getFollowCode(long currentUserId, long focusId) {
            return FriendAdapter.this.userService.followUser(currentUserId, focusId);
        }

        /* access modifiers changed from: protected */
        public void changeState(final UserInfoModel userInfoModel) {
            if (userInfoModel != null) {
                userInfoModel.setIsFollow(1);
            }
            try {
                new Thread(new Runnable() {
                    public void run() {
                        FriendAdapter.this.mentionFriendService.addLocalForumMentionFriend(FriendAdapter.this.currentUserId, userInfoModel);
                    }
                }).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.holder.getFollowUser().setBackgroundResource(FriendAdapter.this.mcResource.getDrawableId("mc_forum_icon16"));
            Toast.makeText(FriendAdapter.this.context, FriendAdapter.this.mcResource.getStringId("mc_forum_follow_succ"), 1).show();
        }
    }

    private class UnfollowUserAsyncTask extends AbstractAsyncTask {
        public UnfollowUserAsyncTask(UserFriendsAdapterHolder holder) {
            super(holder);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.holder.getFollowUser().setEnabled(false);
        }

        /* access modifiers changed from: protected */
        public String getFollowCode(long currentUserId, long focusId) {
            return FriendAdapter.this.userService.unfollowUser(currentUserId, focusId);
        }

        /* access modifiers changed from: protected */
        public void changeState(final UserInfoModel userInfoModel) {
            if (userInfoModel != null) {
                userInfoModel.setIsFollow(0);
            }
            new Thread(new Runnable() {
                public void run() {
                    FriendAdapter.this.mentionFriendService.deletedLocalForumMentionFriend(FriendAdapter.this.currentUserId, userInfoModel);
                }
            }).start();
            FriendAdapter.this.mentionFriendService.getFroumMentionFriend(FriendAdapter.this.currentUserId);
            this.holder.getFollowUser().setBackgroundResource(FriendAdapter.this.mcResource.getDrawableId("mc_forum_icon15"));
            Toast.makeText(FriendAdapter.this.context, FriendAdapter.this.mcResource.getStringId("mc_forum_unfollow_succ"), 1).show();
        }
    }

    private class BlackAsyncTask extends AsyncTask<Object, Void, String> {
        private int blackStatus;
        private long currentUserId;
        private UserFriendsAdapterHolder holder;
        private long isBlackUserId;
        private UserInfoModel userInfoModel;

        public BlackAsyncTask(UserFriendsAdapterHolder holder2) {
            this.holder = holder2;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.holder.getBlackUserBtn().setEnabled(false);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Object... params) {
            this.blackStatus = ((Integer) params[0]).intValue();
            this.currentUserId = ((Long) params[1]).longValue();
            this.userInfoModel = (UserInfoModel) params[2];
            this.isBlackUserId = this.userInfoModel.getUserId();
            return FriendAdapter.this.heartMsgService.setUserBlack(this.blackStatus, this.currentUserId, this.isBlackUserId);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            this.holder.getBlackUserBtn().setEnabled(true);
            if (!StringUtil.isEmpty(result)) {
                Toast.makeText(FriendAdapter.this.context, MCForumErrorUtil.convertErrorCode(FriendAdapter.this.context, result), 0).show();
            } else if (result == null && this.userInfoModel.getBlackStatus() == 1) {
                Toast.makeText(FriendAdapter.this.context, FriendAdapter.this.context.getResources().getString(FriendAdapter.this.mcResource.getStringId("mc_forum_un_black_user_succ")), 0).show();
                this.userInfoModel.setBlackStatus(0);
                this.holder.getBlackUserBtn().setBackgroundResource(FriendAdapter.this.mcResource.getDrawableId("mc_forum_icon19"));
            }
        }
    }

    private View getUserFriendsConvertView(View convertView) {
        UserFriendsAdapterHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.mcResource.getLayoutId("mc_forum_user_friends_item"), (ViewGroup) null);
            holder = new UserFriendsAdapterHolder();
            initUserFriendsAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (UserFriendsAdapterHolder) convertView.getTag();
        }
        if (holder == null) {
            initUserFriendsAdapterHolder(convertView, holder);
        }
        return convertView;
    }

    private void initUserFriendsAdapterHolder(View convertView, UserFriendsAdapterHolder holder) {
        holder.setUserIconImg((ImageView) convertView.findViewById(this.mcResource.getViewId("mc_forum_user_icon_img")));
        holder.setUserNameText((TextView) convertView.findViewById(this.mcResource.getViewId("mc_forum_user_name_text")));
        holder.setUserGenderText((TextView) convertView.findViewById(this.mcResource.getViewId("mc_forum_user_gender_text")));
        holder.setUserStatusText((TextView) convertView.findViewById(this.mcResource.getViewId("mc_forum_user_status_text")));
        holder.setBlackUserBtn((Button) convertView.findViewById(this.mcResource.getViewId("mc_forum_black_user_btn")));
        holder.setFollowUser((Button) convertView.findViewById(this.mcResource.getViewId("mc_forum_follow_user_btn")));
        holder.setUserLocationBox((RelativeLayout) convertView.findViewById(this.mcResource.getViewId("mc_forum_user_location_box")));
        holder.setUserLocationText((TextView) convertView.findViewById(this.mcResource.getViewId("mc_forum_user_location_text")));
        holder.setLevelBox((LinearLayout) convertView.findViewById(this.mcResource.getViewId("mc_forum_level_box")));
        holder.setAdView((AdView) convertView.findViewById(this.mcResource.getViewId("mc_ad_box")));
        holder.setTopAdView((AdView) convertView.findViewById(this.mcResource.getViewId("mc_top_ad_box")));
        holder.setUserItemBox((RelativeLayout) convertView.findViewById(this.mcResource.getViewId("mc_forum_user_info_title_box")));
    }

    private void updateUserIconImage(String imgUrl, final View convertView, ImageView userIconImg) {
        if (SharedPreferencesDB.getInstance(this.context).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(AsyncTaskLoaderImage.formatUrl(imgUrl, "100x100"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    if (image != null) {
                        FriendAdapter.this.mHandler.post(new Runnable() {
                            public void run() {
                                ImageView imageViewByTag = (ImageView) convertView.findViewById(FriendAdapter.this.mcResource.getViewId("mc_forum_user_icon_img"));
                                if (image != null && !image.isRecycled()) {
                                    imageViewByTag.setBackgroundDrawable(new BitmapDrawable(image));
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    public List<AsyncTask> getAsyncTaskList() {
        return this.asyncTaskList;
    }

    public void destory() {
        if (this.followUserAsyncTask != null) {
            this.followUserAsyncTask.cancel(true);
        }
        if (this.unfollowUserAsyncTask != null) {
            this.unfollowUserAsyncTask.cancel(true);
        }
    }
}
