package com.mobcent.base.android.ui.activity.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;
import com.mobcent.base.android.ui.widget.gifview.GifCache;
import com.mobcent.base.android.ui.widget.gifview.GifView;
import com.mobcent.base.android.ui.widget.scaleview.ScaleView;
import com.mobcent.base.android.ui.widget.scaleview.ScaleViewPager;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.RichImageModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.PhoneUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ImagePreviewFragment extends BaseFragment {
    private List<AsyncTask> asyncTaskList;
    /* access modifiers changed from: private */
    public GifView gifImageview;
    /* access modifiers changed from: private */
    public List<GifView> gifViewList;
    private String imageCacheKey = "imageviewer";
    private RichImageModel imageModel = null;
    private int imageSourceType;
    private String imageUrl = null;
    /* access modifiers changed from: private */
    public List<String> imageUrls;
    private ImageView imageView;
    /* access modifiers changed from: private */
    public ImageViewerFragmentSelectedListener imageViewerFragmentSelectedListener;
    private RelativeLayout imageviewerFragmentPagerItem;
    /* access modifiers changed from: private */
    public ImageView pointLoadingFail;
    /* access modifiers changed from: private */
    public MCProgressBar pointLoadingProgress;
    private ScaleView scaleView;

    public interface ImageViewerFragmentSelectedListener {
        ViewPager getViewPager();

        void onSelected();

        void onSingleTap();
    }

    public void setImageSourceType(int imageSourceType2) {
        this.imageSourceType = imageSourceType2;
    }

    public ImageView getImageView() {
        return this.imageView;
    }

    public void setImageView(ImageView imageView2) {
        this.imageView = imageView2;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public RichImageModel getImageModel() {
        return this.imageModel;
    }

    public void setImageModel(RichImageModel imageModel2) {
        this.imageModel = imageModel2;
        this.imageUrl = imageModel2.getImageUrl();
        this.imageCacheKey += this.imageUrl;
    }

    public static ImagePreviewFragment newInstance() {
        return new ImagePreviewFragment();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.imageViewerFragmentSelectedListener = (ImageViewerFragmentSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "must implement OnArticleSelectedListener");
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.asyncTaskList = new ArrayList();
        this.gifViewList = new ArrayList();
        this.imageUrls = new ArrayList();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.imageviewerFragmentPagerItem = (RelativeLayout) inflater.inflate(this.mcResource.getLayoutId("mc_forum_img_preview_fragment_pager_item"), container, false);
        this.scaleView = (ScaleView) this.imageviewerFragmentPagerItem.findViewById(this.mcResource.getViewId("mc_forum_point_imageview"));
        this.gifImageview = (GifView) this.imageviewerFragmentPagerItem.findViewById(this.mcResource.getViewId("mc_forum_gif_imageview"));
        this.pointLoadingProgress = (MCProgressBar) this.imageviewerFragmentPagerItem.findViewById(this.mcResource.getViewId("mc_forum_download_progress_bar"));
        this.pointLoadingFail = (ImageView) this.imageviewerFragmentPagerItem.findViewById(this.mcResource.getViewId("mc_forum_point_loading_fail"));
        this.scaleView.setScaleViewWidth((float) PhoneUtil.getDisplayWidth((Activity) getActivity()));
        this.scaleView.setScaleViewHeight((float) PhoneUtil.getDisplayHeight((Activity) getActivity()));
        this.scaleView.setViewPager((ScaleViewPager) this.imageViewerFragmentSelectedListener.getViewPager());
        return this.imageviewerFragmentPagerItem;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.scaleView.setOnScaleViewListener(new ScaleView.ScaleViewListener() {
            public void onSingleTapConfirmed() {
                ImagePreviewFragment.this.imageViewerFragmentSelectedListener.onSingleTap();
            }
        });
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.pointLoadingProgress.show();
        if (this.imageUrl != null) {
            if (MCConstant.PIC_GIF.equals(StringUtil.getExtensionName(this.imageUrl).toLowerCase())) {
                this.scaleView.setVisibility(8);
                this.gifImageview.setVisibility(0);
                GifCache inputStreamCache = new GifCache(getActivity(), this.imageUrl, new GifCache.GifCallback() {
                    public void onGifLoaded(InputStream is, String url) {
                        if (is != null) {
                            ImagePreviewFragment.this.pointLoadingProgress.stop();
                            ImagePreviewFragment.this.pointLoadingProgress.setVisibility(8);
                            ImagePreviewFragment.this.gifImageview.setGifViewListener(new GifView.GifViewListener() {
                                public void onSingleTap(MotionEvent e) {
                                    ImagePreviewFragment.this.imageViewerFragmentSelectedListener.onSingleTap();
                                }

                                public void done(final boolean isFail) {
                                    ImagePreviewFragment.this.mHandler.post(new Runnable() {
                                        public void run() {
                                            if (isFail) {
                                                ImagePreviewFragment.this.pointLoadingFail.setVisibility(0);
                                            } else {
                                                ImagePreviewFragment.this.pointLoadingFail.setVisibility(8);
                                            }
                                        }
                                    });
                                }
                            });
                            ImagePreviewFragment.this.gifImageview.setVisibility(0);
                            ImagePreviewFragment.this.setImageView(ImagePreviewFragment.this.gifImageview);
                            ImagePreviewFragment.this.gifImageview.setGifImageType(GifView.GifImageType.SYNC_DECODER);
                            ImagePreviewFragment.this.gifImageview.setGifImage(is);
                            ImagePreviewFragment.this.gifViewList.add(ImagePreviewFragment.this.gifImageview);
                        }
                    }
                });
                this.asyncTaskList.add(inputStreamCache);
                inputStreamCache.execute(new Void[0]);
            } else {
                this.scaleView.setVisibility(0);
                this.gifImageview.setVisibility(8);
                setImageView(this.scaleView);
                loadImage(this.scaleView, this.imageCacheKey, this.imageUrl);
            }
            this.imageViewerFragmentSelectedListener.onSelected();
        }
    }

    public void loadImage(final ScaleView imageView2, String key, String imageUrl2) {
        Log.i("imageSourceType", this.imageSourceType + "");
        if (SharedPreferencesDB.getInstance(getActivity()).getPicModeFlag() || this.imageSourceType == 1) {
            AsyncTaskLoaderImage.getInstance(getActivity(), key).loadAsync(imageUrl2, new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, final String url) {
                    ImagePreviewFragment.this.mHandler.post(new Runnable() {
                        public void run() {
                            ImagePreviewFragment.this.pointLoadingProgress.stop();
                            ImagePreviewFragment.this.pointLoadingProgress.setVisibility(8);
                            if (image == null) {
                                ImagePreviewFragment.this.pointLoadingFail.setVisibility(0);
                            } else if (!ImagePreviewFragment.this.isVisible()) {
                            } else {
                                if (!image.isRecycled()) {
                                    imageView2.setImageBitmap(image);
                                    ImagePreviewFragment.this.imageUrls.add(url);
                                    return;
                                }
                                ImagePreviewFragment.this.pointLoadingFail.setVisibility(0);
                            }
                        }
                    });
                }
            });
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (this.asyncTaskList != null && !this.asyncTaskList.isEmpty()) {
            for (AsyncTask asyncTask : this.asyncTaskList) {
                asyncTask.cancel(true);
            }
        }
        if (this.gifViewList != null && !this.gifViewList.isEmpty()) {
            for (int i = 0; i < this.gifViewList.size(); i++) {
                this.gifViewList.get(i).free();
            }
        }
        this.gifViewList.clear();
        this.scaleView.resetView();
        this.scaleView.setImageBitmap(null);
    }
}
