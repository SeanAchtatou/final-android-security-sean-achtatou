package com.mobcent.base.android.ui.activity;

import android.widget.AbsListView;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseListViewActivity extends BaseActivity {
    protected PullToRefreshListView.OnScrollListener listOnScrollListener = new PullToRefreshListView.OnScrollListener() {
        private int firstVisibleItem;
        private int totalItemCount;
        private int visibleItemCount;

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0) {
                BaseListViewActivity.this.asyncTaskLoaderImage.recycleBitmaps(getRecycleImgUrls(BaseListViewActivity.this.pageSize));
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
            this.firstVisibleItem = firstVisibleItem2;
            this.visibleItemCount = visibleItemCount2;
            this.totalItemCount = totalItemCount2 - 2;
        }

        private List<String> getRecycleImgUrls(int pageSize) {
            int size = pageSize;
            List<String> imgUrls = new ArrayList<>();
            int firstIndex = 0;
            int endIndex = this.firstVisibleItem + this.visibleItemCount + size;
            if (this.firstVisibleItem - size > 0) {
                firstIndex = this.firstVisibleItem - size;
            }
            if (endIndex >= this.totalItemCount) {
                endIndex = this.totalItemCount;
            }
            imgUrls.addAll(BaseListViewActivity.this.getImageURL(0, firstIndex));
            imgUrls.addAll(BaseListViewActivity.this.getImageURL(endIndex + 1, this.totalItemCount));
            return imgUrls;
        }

        public void onScrollDirection(boolean isUp, int distance) {
        }
    };
    protected int pageSize = 20;

    /* access modifiers changed from: protected */
    public abstract List<String> getImageURL(int i, int i2);
}
