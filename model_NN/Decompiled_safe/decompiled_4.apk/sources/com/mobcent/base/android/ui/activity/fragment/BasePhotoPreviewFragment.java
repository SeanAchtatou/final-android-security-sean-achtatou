package com.mobcent.base.android.ui.activity.fragment;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.constant.BaseRestfulApiConstant;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.util.ImageUtil;

public abstract class BasePhotoPreviewFragment extends BasePhotoFragment {
    private static final int leftDegrees = -90;
    private static final int rightDegrees = 90;
    protected Bitmap bitmap;
    protected ImageButton closeBtn;
    private ImageButton leftRotateBtn;
    protected PostsService postsService = null;
    /* access modifiers changed from: private */
    public LinearLayout previewBox;
    private ImageView previewGifImg;
    /* access modifiers changed from: private */
    public ImageView previewJPGImg;
    protected String returnPath = null;
    private ImageButton rightRotateBtn;
    private ImageButton sureBtn;
    /* access modifiers changed from: private */
    public UploadAsyncTask uploadAsyncTask;

    /* access modifiers changed from: protected */
    public abstract void updateUIAfterUpload();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.postsService = new PostsServiceImpl(this.activity);
    }

    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.previewBox = (LinearLayout) this.view.findViewById(this.mcResource.getViewId("mc_forum_user_preview_box"));
        this.previewJPGImg = (ImageView) this.view.findViewById(this.mcResource.getViewId("mc_forum_user_preview_jpg_img"));
        this.previewGifImg = (ImageView) this.view.findViewById(this.mcResource.getViewId("mc_forum_user_preview_gif_img"));
        this.leftRotateBtn = (ImageButton) this.view.findViewById(this.mcResource.getViewId("mc_forum_rotate_left_btn"));
        this.rightRotateBtn = (ImageButton) this.view.findViewById(this.mcResource.getViewId("mc_forum_rotate_right_btn"));
        this.closeBtn = (ImageButton) this.view.findViewById(this.mcResource.getViewId("mc_forum_close_btn"));
        this.sureBtn = (ImageButton) this.view.findViewById(this.mcResource.getViewId("mc_forum_sure_btn"));
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.leftRotateBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePhotoPreviewFragment.this.bitmap = ImageUtil.rotateBitmap(BasePhotoPreviewFragment.this.bitmap, -90.0f);
                if (BasePhotoPreviewFragment.this.bitmap != null && !BasePhotoPreviewFragment.this.bitmap.isRecycled()) {
                    BasePhotoPreviewFragment.this.previewJPGImg.setImageBitmap(BasePhotoPreviewFragment.this.bitmap);
                }
            }
        });
        this.rightRotateBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePhotoPreviewFragment.this.bitmap = ImageUtil.rotateBitmap(BasePhotoPreviewFragment.this.bitmap, 90.0f);
                if (BasePhotoPreviewFragment.this.bitmap != null && !BasePhotoPreviewFragment.this.bitmap.isRecycled()) {
                    BasePhotoPreviewFragment.this.previewJPGImg.setImageBitmap(BasePhotoPreviewFragment.this.bitmap);
                }
            }
        });
        this.closeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePhotoPreviewFragment.this.previewBox.setVisibility(8);
                BasePhotoPreviewFragment.this.clearTempFile();
            }
        });
        this.sureBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UploadAsyncTask unused = BasePhotoPreviewFragment.this.uploadAsyncTask = new UploadAsyncTask();
                BasePhotoPreviewFragment.this.uploadAsyncTask.execute(new String[0]);
            }
        });
    }

    private class UploadAsyncTask extends AsyncTask<String, Void, Object> {
        private UploadAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            BasePhotoPreviewFragment.this.showProgressDialog("mc_forum_warn_upload_img", this);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            if (BasePhotoPreviewFragment.this.uploadType != 3) {
                BasePhotoPreviewFragment.this.getUploadingBitmap(BasePhotoPreviewFragment.this.bitmap);
                BasePhotoPreviewFragment.this.path = BasePhotoPreviewFragment.this.compressPath;
            }
            if (BasePhotoPreviewFragment.this.uploadType == 2 || BasePhotoPreviewFragment.this.uploadType == 3) {
                return BasePhotoPreviewFragment.this.postsService.uploadImage(BasePhotoPreviewFragment.this.path, BaseRestfulApiConstant.FORUM_LOCAL_ACTION);
            }
            return BasePhotoPreviewFragment.this.postsService.uploadImage(BasePhotoPreviewFragment.this.path, BaseRestfulApiConstant.FORUM_LOCAL_ACTION);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object result) {
            BasePhotoPreviewFragment.this.hideProgressDialog();
            if (result == null) {
                BasePhotoPreviewFragment.this.uploadFail();
            } else if (result instanceof String) {
                String path = (String) result;
                if (path.startsWith(BaseReturnCodeConstant.ERROR_CODE)) {
                    String errorCode = path.substring(path.indexOf(BaseReturnCodeConstant.ERROR_CODE) + 1, path.length());
                    if (errorCode != null && !errorCode.equals("")) {
                        BasePhotoPreviewFragment.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(BasePhotoPreviewFragment.this.activity, errorCode));
                        BasePhotoPreviewFragment.this.clearTempFile();
                        BasePhotoPreviewFragment.this.clearMemory();
                    }
                } else {
                    if (BasePhotoPreviewFragment.this.uploadType == 2 || BasePhotoPreviewFragment.this.uploadType == 3) {
                        BasePhotoPreviewFragment.this.warnMessageById("mc_forum_user_photo_upload_image_succ");
                    }
                    BasePhotoPreviewFragment.this.previewBox.setVisibility(8);
                    BasePhotoPreviewFragment.this.bitmap = BasePhotoPreviewFragment.this.getUploadedBitmap(BasePhotoPreviewFragment.this.bitmap);
                    BasePhotoPreviewFragment.this.returnPath = path;
                    BasePhotoPreviewFragment.this.updateUIAfterUpload();
                }
            }
            BasePhotoPreviewFragment.this.previewBox.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void doSomethingAfterSelectedPhoto() {
        this.bitmap = getPreviewBitmap(this.path);
        if (this.bitmap == null) {
            uploadFail();
        } else if (!this.bitmap.isRecycled()) {
            this.previewBox.setVisibility(0);
            this.previewJPGImg.setVisibility(0);
            this.previewJPGImg.setImageBitmap(this.bitmap);
            if (this.uploadType == 3) {
                this.leftRotateBtn.setVisibility(8);
                this.rightRotateBtn.setVisibility(8);
                return;
            }
            this.leftRotateBtn.setVisibility(0);
            this.rightRotateBtn.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap getPreviewBitmap(String path) {
        return ImageUtil.getBitmapFromMedia(this.activity, path);
    }

    /* access modifiers changed from: protected */
    public void getUploadingBitmap(Bitmap bitmap2) {
        ImageUtil.compressBitmap(this.compressPath, bitmap2, 100, 3, this.activity);
    }

    /* access modifiers changed from: private */
    public void clearMemory() {
        if (this.bitmap != null && !this.bitmap.isRecycled()) {
            this.bitmap.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap getUploadedBitmap(Bitmap bitmap2) {
        return ImageUtil.compressBitmap(bitmap2, 3, this.activity);
    }

    /* access modifiers changed from: private */
    public void uploadFail() {
        warnMessageById("mc_forum_user_photo_select_error");
        clearTempFile();
        clearMemory();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.uploadAsyncTask != null) {
            this.uploadAsyncTask.cancel(true);
        }
    }
}
