package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.ImagePreviewFragmentAdapter;
import com.mobcent.base.android.ui.activity.fragment.ImagePreviewFragment;
import com.mobcent.base.android.ui.activity.helper.MCForumLaunchShareHelper;
import com.mobcent.base.android.ui.activity.helper.MCImageViewerHelper;
import com.mobcent.base.android.ui.widget.scaleview.ScaleView;
import com.mobcent.base.android.ui.widget.scaleview.ScaleViewPager;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.RichImageModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.ImageLoaderUtil;
import com.mobcent.forum.android.util.MCLibIOUtil;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.platformtools.Util;
import java.util.ArrayList;
import java.util.List;

public class ImagePreviewFragmentActivity extends BaseFragmentActivity implements ImagePreviewFragment.ImageViewerFragmentSelectedListener {
    /* access modifiers changed from: private */
    public RichImageModel currentImageModel = null;
    /* access modifiers changed from: private */
    public String currentImageUrl = null;
    protected int currentPosition = 0;
    private int imageIndex;
    private int imageSourceType;
    /* access modifiers changed from: private */
    public List<RichImageModel> imageUrlList;
    private ImageButton imageViewerBack;
    private ImageButton imageViewerDownload;
    /* access modifiers changed from: private */
    public ImagePreviewFragmentAdapter imageViewerFragmentAdapter;
    private RelativeLayout imageViewerTopbar;
    private ImageButton imgShare;
    private ScaleViewPager mPager;
    private MCImageViewerHelper.ImageViewerSizeListener viewSizeListener = new MCImageViewerHelper.ImageViewerSizeListener() {
        public void onViewerSizeListener(List<RichImageModel> richImageModels) {
            ImagePreviewFragmentActivity.this.imageUrlList.clear();
            ImagePreviewFragmentActivity.this.imageUrlList.addAll(richImageModels);
            ImagePreviewFragmentActivity.this.imageViewerFragmentAdapter.setImageUrlList(ImagePreviewFragmentActivity.this.imageUrlList);
            ImagePreviewFragmentActivity.this.imageViewerFragmentAdapter.notifyDataSetChanged();
        }
    };

    /* access modifiers changed from: protected */
    public void initData() {
        overridePendingTransition(this.resource.getAnimId("mc_forum_fade_in"), this.resource.getAnimId("mc_forum_fade_out"));
        this.imageUrlList = (ArrayList) getIntent().getSerializableExtra(MCConstant.RICH_IMAGE_LIST);
        this.imageSourceType = getIntent().getIntExtra(MCConstant.IMAGE_SOURCE_TYPE, 0);
        this.imageIndex = getPosition(getIntent().getStringExtra(MCConstant.IMAGE_URL));
        this.currentPosition = this.imageIndex;
        MCImageViewerHelper.getInstance().setOnViewSizeListener(this.viewSizeListener);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_img_preview_fragment_pager"));
        this.imageViewerFragmentAdapter = new ImagePreviewFragmentAdapter(getSupportFragmentManager());
        this.imageViewerFragmentAdapter.setImageUrlList(this.imageUrlList);
        this.imageViewerFragmentAdapter.setImageSourceType(this.imageSourceType);
        this.mPager = (ScaleViewPager) findViewById(this.resource.getViewId("mc_forum_imageviewer_pager"));
        this.mPager.setAdapter(this.imageViewerFragmentAdapter);
        this.mPager.setCurrentItem(this.imageIndex);
        this.mPager.setOnPageChangeListener(new ViewPageChangeListener());
        this.mPager.setPageMargin((int) getResources().getDimension(this.resource.getDimenId("mc_forum_image_detail_pager_margin")));
        this.imageViewerTopbar = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_imageviewer_bar"));
        this.imageViewerBack = (ImageButton) findViewById(this.resource.getViewId("mc_forum_img_back"));
        this.imageViewerDownload = (ImageButton) findViewById(this.resource.getViewId("mc_forum_img_download"));
        this.imgShare = (ImageButton) findViewById(this.resource.getViewId("mc_forum_img_share"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.imageViewerBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImagePreviewFragmentActivity.this.finish();
            }
        });
        this.imageViewerDownload.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ImagePreviewFragmentActivity.this.currentImageUrl != null && !"".equals(ImagePreviewFragmentActivity.this.currentImageUrl)) {
                    new AsyncTaskDownloadImage().execute(new Void[0]);
                }
            }
        });
        this.imgShare.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                IWXAPI api = WXAPIFactory.createWXAPI(ImagePreviewFragmentActivity.this, SharedPreferencesDB.getInstance(ImagePreviewFragmentActivity.this).getWeiXinAppKey());
                if (StringUtil.isEmpty(SharedPreferencesDB.getInstance(ImagePreviewFragmentActivity.this).getWeiXinAppKey())) {
                    if (!StringUtil.isEmpty(ImagePreviewFragmentActivity.this.currentImageUrl)) {
                        MCForumLaunchShareHelper.shareContentWithImageFileAndUrl(ImagePreviewFragmentActivity.this.currentImageModel.getImageDesc(), MCLibIOUtil.getImageCachePath(ImagePreviewFragmentActivity.this.getApplicationContext()) + AsyncTaskLoaderImage.getHash(ImagePreviewFragmentActivity.this.currentImageUrl), ImagePreviewFragmentActivity.this.currentImageUrl, "", "", ImagePreviewFragmentActivity.this);
                    }
                } else if (!StringUtil.isEmpty(ImagePreviewFragmentActivity.this.currentImageUrl)) {
                    MCForumLaunchShareHelper.shareWay(true, ImagePreviewFragmentActivity.this.currentImageModel.getImageDesc(), MCLibIOUtil.getImageCachePath(ImagePreviewFragmentActivity.this.getApplicationContext()) + AsyncTaskLoaderImage.getHash(ImagePreviewFragmentActivity.this.currentImageUrl), ImagePreviewFragmentActivity.this.currentImageUrl, "", "", ImagePreviewFragmentActivity.this, api);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        MCImageViewerHelper.getInstance().setListener(null);
    }

    private class AsyncTaskDownloadImage extends AsyncTask<Void, Void, Boolean> {
        private AsyncTaskDownloadImage() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Void... params) {
            boolean isSaveSucc;
            String fileName = ImageLoaderUtil.getPathName(ImagePreviewFragmentActivity.this.currentImageUrl);
            MCLogUtil.i("ImageViewerFragmentActivity", "fileName = " + fileName);
            String imagePath = MCLibIOUtil.getImageCachePath(ImagePreviewFragmentActivity.this.getApplicationContext()) + AsyncTaskLoaderImage.getHash(ImagePreviewFragmentActivity.this.currentImageUrl);
            if (MCConstant.PIC_GIF.equals(StringUtil.getExtensionName(fileName).toLowerCase())) {
                isSaveSucc = MCLibIOUtil.addToSysGallery(ImagePreviewFragmentActivity.this, imagePath, fileName + ".gif");
            } else {
                isSaveSucc = MCLibIOUtil.addToSysGallery(ImagePreviewFragmentActivity.this, imagePath, fileName + Util.PHOTO_DEFAULT_EXT);
            }
            return Boolean.valueOf(isSaveSucc);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean isSaveSucc) {
            if (isSaveSucc.booleanValue()) {
                Toast.makeText(ImagePreviewFragmentActivity.this.getApplicationContext(), ImagePreviewFragmentActivity.this.resource.getStringId("mc_forum_add_to_sys_gallery_succ"), 0).show();
            } else {
                Toast.makeText(ImagePreviewFragmentActivity.this.getApplicationContext(), ImagePreviewFragmentActivity.this.resource.getStringId("mc_forum_add_to_sys_gallery_fail"), 0).show();
            }
        }
    }

    public ViewPager getViewPager() {
        return this.mPager;
    }

    public void onSingleTap() {
        if (this.imageViewerTopbar.isShown()) {
            this.imageViewerTopbar.setVisibility(8);
        } else {
            this.imageViewerTopbar.setVisibility(0);
        }
    }

    private class ViewPageChangeListener implements ViewPager.OnPageChangeListener {
        private ViewPageChangeListener() {
        }

        public void onPageScrollStateChanged(int status) {
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        public void onPageSelected(int position) {
            ImagePreviewFragmentActivity.this.currentPosition = position;
            ImagePreviewFragment currentImageViewerFragment = ImagePreviewFragmentActivity.this.imageViewerFragmentAdapter.getItem(ImagePreviewFragmentActivity.this.currentPosition);
            if (currentImageViewerFragment != null) {
                String unused = ImagePreviewFragmentActivity.this.currentImageUrl = currentImageViewerFragment.getImageUrl();
                RichImageModel unused2 = ImagePreviewFragmentActivity.this.currentImageModel = currentImageViewerFragment.getImageModel();
            }
            if (MCImageViewerHelper.getInstance().getListener() != null) {
                MCImageViewerHelper.getInstance().getListener().onViewerPageSelect(ImagePreviewFragmentActivity.this.currentPosition);
            }
            ImagePreviewFragmentActivity.this.resetScaleView(ImagePreviewFragmentActivity.this.currentPosition - 1);
            ImagePreviewFragmentActivity.this.resetScaleView(ImagePreviewFragmentActivity.this.currentPosition + 1);
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(this.resource.getAnimId("mc_forum_fade_in"), this.resource.getAnimId("mc_forum_fade_out"));
    }

    public void onSelected() {
        if (this.currentPosition == this.imageIndex) {
            ImagePreviewFragment imageViewerFragment = this.imageViewerFragmentAdapter.getItem(this.currentPosition);
            this.currentImageUrl = imageViewerFragment.getImageUrl();
            this.currentImageModel = imageViewerFragment.getImageModel();
        }
    }

    /* access modifiers changed from: private */
    public void resetScaleView(int position) {
        ImagePreviewFragment leftFragment = this.imageViewerFragmentAdapter.getItem(position);
        if (leftFragment != null && (leftFragment.getImageView() instanceof ScaleView)) {
            ((ScaleView) leftFragment.getImageView()).resetView();
        }
    }

    private int getPosition(String imageUrl) {
        for (int i = 0; i < this.imageUrlList.size(); i++) {
            if (this.imageUrlList.get(i).getImageUrl().equals(imageUrl)) {
                return i;
            }
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return null;
    }
}
