package com.mobcent.base.android.ui.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.adapter.ModeratorManageAdapter;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.model.ModeratorModel;
import com.mobcent.forum.android.service.ModeratorService;
import com.mobcent.forum.android.service.impl.ModeratorServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class ModeratorManageActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public ModeratorManageAdapter adapter;
    private Button backBtn;
    /* access modifiers changed from: private */
    public ModeratorManagerAsyncTask managerAsyncTask;
    /* access modifiers changed from: private */
    public List<ModeratorModel> moderatorList;
    /* access modifiers changed from: private */
    public ModeratorService moderatorService;
    /* access modifiers changed from: private */
    public PullToRefreshListView refreshListView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.refreshListView.onRefreshWithOutListener();
        if (this.managerAsyncTask != null) {
            this.managerAsyncTask.cancel(true);
        }
        this.managerAsyncTask = new ModeratorManagerAsyncTask();
        this.managerAsyncTask.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.moderatorService = new ModeratorServiceImpl(this);
        this.adapter = new ModeratorManageAdapter(this, this.asyncTaskLoaderImage, this.mHandler);
        this.moderatorList = new ArrayList();
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_moderator_manager_activity"));
        this.refreshListView = (PullToRefreshListView) findViewById(this.resource.getViewId("mc_forum_lv"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.refreshListView.setAdapter((ListAdapter) this.adapter);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ModeratorManageActivity.this.back();
            }
        });
        this.refreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                if (ModeratorManageActivity.this.managerAsyncTask != null) {
                    ModeratorManageActivity.this.managerAsyncTask.cancel(true);
                }
                ModeratorManagerAsyncTask unused = ModeratorManageActivity.this.managerAsyncTask = new ModeratorManagerAsyncTask();
                ModeratorManageActivity.this.managerAsyncTask.execute(new Void[0]);
            }
        });
    }

    class ModeratorManagerAsyncTask extends AsyncTask<Void, Void, List<ModeratorModel>> {
        ModeratorManagerAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<ModeratorModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public List<ModeratorModel> doInBackground(Void... params) {
            return ModeratorManageActivity.this.moderatorService.getModeratorList();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<ModeratorModel> result) {
            super.onPostExecute((Object) result);
            ModeratorManageActivity.this.refreshListView.onRefreshComplete();
            ModeratorManageActivity.this.refreshListView.setSelection(0);
            if (result == null || result.size() <= 0) {
                ModeratorManageActivity.this.refreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                Toast.makeText(ModeratorManageActivity.this, MCForumErrorUtil.convertErrorCode(ModeratorManageActivity.this, result.get(0).getErrorCode()), 0).show();
                ModeratorManageActivity.this.refreshListView.onBottomRefreshComplete(3);
            } else {
                List unused = ModeratorManageActivity.this.getRefreshImgUrl(result);
                ModeratorManageActivity.this.adapter.setModeratorList(result);
                ModeratorManageActivity.this.adapter.notifyDataSetInvalidated();
                ModeratorManageActivity.this.adapter.notifyDataSetChanged();
                ModeratorManageActivity.this.refreshListView.onBottomRefreshComplete(3);
                List unused2 = ModeratorManageActivity.this.moderatorList = result;
            }
        }
    }

    /* access modifiers changed from: private */
    public List<String> getRefreshImgUrl(List<ModeratorModel> result) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < this.moderatorList.size(); i++) {
            ModeratorModel model = this.moderatorList.get(i);
            if (!isExit(model, result)) {
                list.add(AsyncTaskLoaderImage.formatUrl(model.getIcon(), "100x100"));
            }
        }
        return list;
    }

    private boolean isExit(ModeratorModel model, List<ModeratorModel> result) {
        int j = result.size();
        for (int i = 0; i < j; i++) {
            ModeratorModel model2 = result.get(i);
            if (!StringUtil.isEmpty(model.getIcon()) && !StringUtil.isEmpty(model2.getIcon()) && model.getIcon().equals(model2.getIcon())) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public List<String> getImageURL(int start, int end) {
        List<String> imgUrls = new ArrayList<>();
        for (int i = start; i < end; i++) {
            imgUrls.add(AsyncTaskLoaderImage.formatUrl(this.moderatorList.get(i).getIcon(), "100x100"));
        }
        return imgUrls;
    }

    /* access modifiers changed from: protected */
    public List<String> getRecycledImgUrls() {
        return getImageURL(0, this.moderatorList.size());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.managerAsyncTask != null) {
            this.managerAsyncTask.cancel(true);
        }
    }
}
