package com.mobcent.base.android.ui.activity.widget.wheel;

public interface OnWheelChangedListener {
    void onChanged(WheelView wheelView, int i, int i2);
}
