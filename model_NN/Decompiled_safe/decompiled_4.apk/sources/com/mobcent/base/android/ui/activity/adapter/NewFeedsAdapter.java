package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.base.android.ui.activity.UserFeedActivity;
import com.mobcent.base.android.ui.activity.adapter.holder.FeedTopicAdapterHolder;
import com.mobcent.base.android.ui.activity.adapter.holder.FeedUserAdapterHolder;
import com.mobcent.base.android.ui.activity.view.MCLevelView;
import com.mobcent.base.forum.android.util.MCTouchUtil;
import com.mobcent.forum.android.constant.FeedsConstant;
import com.mobcent.forum.android.model.AnnoModel;
import com.mobcent.forum.android.model.NewFeedsModel;
import com.mobcent.forum.android.model.ReplyModel;
import com.mobcent.forum.android.model.TopicFeedModel;
import com.mobcent.forum.android.model.UserFeedModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.model.UserTopicFeedModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.DateUtil;
import java.util.List;

public class NewFeedsAdapter extends AbstractFeedsAdapter implements FeedsConstant {
    private List<NewFeedsModel> feedsList;

    public NewFeedsAdapter(Context context, LayoutInflater inflater, Handler mHandler, AsyncTaskLoaderImage asyncTaskLoaderImage, ListView listView, List<NewFeedsModel> feedsList2) {
        super(context, inflater, mHandler, asyncTaskLoaderImage, listView);
        this.feedsList = feedsList2;
    }

    public List<NewFeedsModel> getFeedsList() {
        return this.feedsList;
    }

    public void setFeedsList(List<NewFeedsModel> feedsList2) {
        this.feedsList = feedsList2;
    }

    public int getCount() {
        return this.feedsList.size();
    }

    public Object getItem(int position) {
        return this.feedsList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        NewFeedsModel model = this.feedsList.get(position);
        if (model.getType() == 1) {
            return getUserItemView(position, convertView, parent, model.getUserFeed());
        }
        if (model.getType() == 2) {
            return getTopicItemView(position, convertView, parent, model.getTopicFeed());
        }
        if (model.getType() == 12) {
            return getAnnoView(position, convertView, parent, model);
        }
        return convertView;
    }

    private View getUserItemView(int position, View convertView, ViewGroup parent, final UserFeedModel userFeedModel) {
        View convertView2 = getUserConvertView(convertView);
        FeedUserAdapterHolder holder = (FeedUserAdapterHolder) convertView2.getTag();
        holder.getUserNameText().setText(userFeedModel.getUserInfo().getNickname());
        showUserRole(holder.getRoleText(), userFeedModel.getUserInfo().getRoleNum());
        updateUserIcon(userFeedModel.getUserInfo().getIcon(), holder.getIconImg());
        holder.getLevelBox().removeAllViews();
        new MCLevelView(this.context, userFeedModel.getUserInfo().getLevel(), holder.getLevelBox(), 0);
        holder.getFeedsBox().removeAllViews();
        int j = userFeedModel.getUserTopicList().size();
        for (int i = 0; i < j; i++) {
            UserTopicFeedModel userTopicFeed = userFeedModel.getUserTopicList().get(i);
            if (userTopicFeed.getFtype() == 1 || userTopicFeed.getFtype() == 6 || userTopicFeed.getFtype() == 10 || userTopicFeed.getFtype() == 11) {
                holder.getFeedsBox().addView(getUserTopicView(userTopicFeed.getTopic(), userTopicFeed.getFtype()));
            } else if (userTopicFeed.getFtype() == 2) {
                holder.getFeedsBox().addView(getUserReplyView(userTopicFeed.getTopic(), userTopicFeed.getReply(), userTopicFeed.getReplyQuote()));
            }
        }
        holder.getIconImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewFeedsAdapter.this.getUserFunctionDialog(userFeedModel.getUserInfo());
            }
        });
        MCTouchUtil.createTouchDelegate(holder.getMoreText(), 10);
        holder.getMoreText().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(NewFeedsAdapter.this.context, UserFeedActivity.class);
                intent.putExtra("userId", userFeedModel.getUserInfo().getUserId());
                intent.putExtra("nickname", userFeedModel.getUserInfo().getNickname());
                NewFeedsAdapter.this.context.startActivity(intent);
            }
        });
        return convertView2;
    }

    private View getTopicItemView(int position, View convertView, ViewGroup parent, final TopicFeedModel topicFeedModel) {
        View convertView2 = getTopicConvertView(convertView);
        FeedTopicAdapterHolder holder = (FeedTopicAdapterHolder) convertView2.getTag();
        String nickName = "";
        for (int i = 0; i < topicFeedModel.getNicknames().length; i++) {
            if (!nickName.contains(topicFeedModel.getNicknames()[i])) {
                nickName = nickName + topicFeedModel.getNicknames()[i] + AdApiConstant.RES_SPLIT_COMMA;
            }
        }
        if (nickName.length() > 1) {
            nickName = nickName.substring(0, nickName.length() - 1);
        }
        if (topicFeedModel.getTopic().getHasVoice() == 1) {
            holder.getHasVoice().setVisibility(0);
        } else {
            holder.getHasVoice().setVisibility(8);
        }
        holder.getUserNameText().setText(nickName);
        holder.getLevelBox().removeAllViews();
        holder.getFeedsBox().removeAllViews();
        new MCLevelView(this.context, topicFeedModel.getUserInfo().getLevel(), holder.getLevelBox(), 0);
        showUserRole(holder.getRoleText(), topicFeedModel.getUserInfo().getRoleNum());
        updateUserIcon(topicFeedModel.getUserInfo().getIcon(), holder.getIconImg());
        holder.getFeedType().setText(this.resource.getString("mc_forum_home_reply_attention_topic"));
        holder.getTimeText().setText(DateUtil.getFormatTimeByYear(topicFeedModel.getTopic().getCreateDate()));
        holder.getFeedTitle().setText(topicFeedModel.getTopic().getTitle());
        holder.getQuoteContentText().setText(topicFeedModel.getTopic().getThumbnail());
        if (topicFeedModel.getReplyList() != null && !topicFeedModel.getReplyList().isEmpty()) {
            int j = topicFeedModel.getReplyList().size();
            for (int i2 = 0; i2 < j; i2++) {
                holder.getFeedsBox().addView(getTopicPeplyView(topicFeedModel.getReplyList().get(i2), this.inflater.inflate(this.resource.getLayoutId("mc_forum_new_feeds_topic_reply"), (ViewGroup) null)));
            }
        }
        holder.getIconImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewFeedsAdapter.this.getUserFunctionDialog(topicFeedModel.getUserInfo());
            }
        });
        MCTouchUtil.createTouchDelegate(holder.getMoreText(), 10);
        holder.getMoreBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReplyModel firstReplyModel = null;
                if (topicFeedModel.getReplyList() != null && !topicFeedModel.getReplyList().isEmpty()) {
                    firstReplyModel = topicFeedModel.getReplyList().get(0);
                }
                if (firstReplyModel != null) {
                    NewFeedsAdapter.this.getReplyFunctionDialog(topicFeedModel.getTopic(), firstReplyModel, false);
                } else {
                    NewFeedsAdapter.this.getTopicFunctionDialog(topicFeedModel.getTopic());
                }
            }
        });
        holder.getMoreText().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(NewFeedsAdapter.this.context, UserFeedActivity.class);
                intent.putExtra("userId", topicFeedModel.getUserInfo().getUserId());
                intent.putExtra("nickname", topicFeedModel.getUserInfo().getNickname());
                NewFeedsAdapter.this.context.startActivity(intent);
            }
        });
        return convertView2;
    }

    private View getAnnoView(int position, View convertView, ViewGroup parent, NewFeedsModel newFeedsModel) {
        View convertView2 = getUserConvertView(convertView);
        FeedUserAdapterHolder holder = (FeedUserAdapterHolder) convertView2.getTag();
        AnnoModel annoModel = newFeedsModel.getAnnoModel();
        holder.getUserNameText().setText(annoModel.getUserNickName());
        showUserRole(holder.getRoleText(), annoModel.getRoleNum());
        updateUserIcon(annoModel.getIcon(), holder.getIconImg());
        holder.getLevelBox().removeAllViews();
        new MCLevelView(this.context, annoModel.getLevel(), holder.getLevelBox(), 0);
        holder.getFeedsBox().removeAllViews();
        View view = this.inflater.inflate(this.resource.getLayoutId("mc_forum_new_feeds_user_topic"), (ViewGroup) null);
        holder.getFeedsBox().addView(view);
        TextView feedType = (TextView) view.findViewById(this.resource.getViewId("mc_forum_feed_type"));
        ((TextView) view.findViewById(this.resource.getViewId("mc_forum_time_text"))).setText(DateUtil.getFormatTimeByYear(newFeedsModel.getTime()));
        ((TextView) view.findViewById(this.resource.getViewId("mc_forum_feed_title_text"))).setText("[" + annoModel.getSubject() + "]");
        TextView contentText = (TextView) view.findViewById(this.resource.getViewId("mc_forum_feed_content_text"));
        ImageView thumbnailImg = (ImageView) view.findViewById(this.resource.getViewId("mc_forum_feed_thumbnail_img"));
        if (!annoModel.isHasImg()) {
            thumbnailImg.setVisibility(8);
        } else {
            thumbnailImg.setVisibility(0);
            updateImage(annoModel.getPic(), thumbnailImg);
        }
        feedType.setText(this.resource.getString("mc_forum_home_publish_anno"));
        contentText.setText(annoModel.getThumbnail());
        final AnnoModel annoModel2 = annoModel;
        ((ImageButton) view.findViewById(this.resource.getViewId("mc_forum_more_btn"))).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewFeedsAdapter.this.getTopicFunctionDialog(annoModel2);
            }
        });
        final AnnoModel annoModel3 = annoModel;
        holder.getIconImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserInfoModel userInfoModel = new UserInfoModel();
                userInfoModel.setUserId(annoModel3.getUserId());
                userInfoModel.setNickname(annoModel3.getUserNickName());
                NewFeedsAdapter.this.getUserFunctionDialog(userInfoModel);
            }
        });
        MCTouchUtil.createTouchDelegate(holder.getMoreText(), 10);
        final AnnoModel annoModel4 = annoModel;
        holder.getMoreText().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(NewFeedsAdapter.this.context, UserFeedActivity.class);
                intent.putExtra("userId", annoModel4.getUserId());
                intent.putExtra("nickname", annoModel4.getUserNickName());
                NewFeedsAdapter.this.context.startActivity(intent);
            }
        });
        return convertView2;
    }

    private View getUserConvertView(View convertView) {
        FeedUserAdapterHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_new_feeds_user_item"), (ViewGroup) null);
            holder = new FeedUserAdapterHolder();
            initFeedUserAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            try {
                holder = (FeedUserAdapterHolder) convertView.getTag();
            } catch (Exception e) {
                holder = null;
            }
        }
        if (holder != null) {
            return convertView;
        }
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_new_feeds_user_item"), (ViewGroup) null);
        FeedUserAdapterHolder holder2 = new FeedUserAdapterHolder();
        initFeedUserAdapterHolder(convertView2, holder2);
        convertView2.setTag(holder2);
        return convertView2;
    }

    private void initFeedUserAdapterHolder(View convertView, FeedUserAdapterHolder holder) {
        holder.setIconImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_feeds_icon")));
        holder.setUserNameText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_user_name")));
        holder.setRoleText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_role_text")));
        holder.setLevelBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_level_box")));
        holder.setFeedsBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_feeds_box")));
        holder.setMoreText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_more_text")));
    }

    private View getTopicConvertView(View convertView) {
        FeedTopicAdapterHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_new_feeds_topic_item"), (ViewGroup) null);
            holder = new FeedTopicAdapterHolder();
            initFeedTopicAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            try {
                holder = (FeedTopicAdapterHolder) convertView.getTag();
            } catch (Exception e) {
                holder = null;
            }
        }
        if (holder != null) {
            return convertView;
        }
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_new_feeds_topic_item"), (ViewGroup) null);
        FeedTopicAdapterHolder holder2 = new FeedTopicAdapterHolder();
        initFeedTopicAdapterHolder(convertView2, holder2);
        convertView2.setTag(holder2);
        return convertView2;
    }

    private void initFeedTopicAdapterHolder(View convertView, FeedTopicAdapterHolder holder) {
        holder.setIconImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_feeds_icon")));
        holder.setUserNameText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_user_name")));
        holder.setRoleText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_role_text")));
        holder.setLevelBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_level_box")));
        holder.setFeedsBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_feeds_box")));
        holder.setFeedType((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_feed_type")));
        holder.setTimeText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_time_text")));
        holder.setFeedTitle((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_feed_title")));
        holder.setContentText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_feed_content_text")));
        holder.setQuoteContentText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_feed_quote2_content")));
        holder.setMoreBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_more_btn")));
        holder.setMoreText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_more_text")));
        holder.setHasVoice((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_voice_img")));
    }
}
