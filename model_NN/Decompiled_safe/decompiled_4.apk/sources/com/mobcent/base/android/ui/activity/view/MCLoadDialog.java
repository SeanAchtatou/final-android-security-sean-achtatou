package com.mobcent.base.android.ui.activity.view;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import com.mobcent.base.forum.android.util.MCResource;

public class MCLoadDialog extends Dialog {
    private AsyncTask asyncTask;
    private Context context;
    private TextView loadingText;
    private MCResource mcResource;

    public MCLoadDialog(Context context2) {
        super(context2);
        this.context = context2;
    }

    public MCLoadDialog(Context context2, int theme) {
        super(context2, theme);
        this.context = context2;
    }

    public MCLoadDialog(Context context2, int theme, boolean isCancel) {
        super(context2, theme);
        this.context = context2;
    }

    public void setAsyncTask(AsyncTask asyncTask2) {
        this.asyncTask = asyncTask2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mcResource = MCResource.getInstance(this.context);
        setContentView(this.mcResource.getLayoutId("mc_forum_load_dialog"));
        this.loadingText = (TextView) findViewById(this.mcResource.getViewId("mc_forum_loading_text"));
    }

    public void show(String text) {
        show();
        this.loadingText.setText(text + "");
    }

    public void show() {
        super.show();
    }

    public void dismiss() {
        super.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.asyncTask != null) {
            this.asyncTask.cancel(true);
        }
        super.onStop();
    }
}
