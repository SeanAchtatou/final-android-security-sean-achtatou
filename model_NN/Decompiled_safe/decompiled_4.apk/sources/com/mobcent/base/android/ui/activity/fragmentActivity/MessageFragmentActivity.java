package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.PostsNoticeListAdapter;
import com.mobcent.base.android.ui.activity.adapter.TabsAdapter;
import com.mobcent.base.android.ui.activity.fragment.AtReplyMessageFragment;
import com.mobcent.base.android.ui.activity.fragment.MsgUserListFragment;
import com.mobcent.base.android.ui.activity.fragment.ReplyMessageFragment;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.android.ui.activity.service.MediaService;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.model.PostsNoticeModel;
import com.mobcent.forum.android.model.SoundModel;
import com.mobcent.forum.android.service.PermService;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import java.util.List;

public class MessageFragmentActivity extends BaseFragmentActivity {
    public static final int AT_NOTICE_ID = 1;
    public static final int MSG_NOTICE_ID = 2;
    public static final int REPLY_NOTICE_ID = 0;
    String atReply;
    /* access modifiers changed from: private */
    public AtReplyMessageFragment atReplyFragment = null;
    protected Button backBtn;
    private int defaultTab = 0;
    LayoutInflater inflater;
    TabHost mTabHost;
    MessageTabsAdapter mTabsAdapter;
    ViewPager mViewPager;
    private MediaPlayerBroadCastReceiver mediaPlayerBroadCastReceiver = null;
    String msg;
    /* access modifiers changed from: private */
    public MsgUserListFragment msgFragment = null;
    /* access modifiers changed from: private */
    public PermService permService;
    PostsNoticeListAdapter.PostsNoticeClickListener postsNoticeClickListener = new PostsNoticeListAdapter.PostsNoticeClickListener() {
        public void onPostsNoticeClick(View v, PostsNoticeModel model) {
            if (MessageFragmentActivity.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.READ, -1) == 1 && MessageFragmentActivity.this.permService.getPermNum(PermConstant.BOARDS, PermConstant.READ, model.getBoardId()) == 1) {
                MCForumHelper.onPostsClick(MessageFragmentActivity.this, MessageFragmentActivity.this.resource, v, model);
            } else {
                MessageFragmentActivity.this.warnMessageByStr(MessageFragmentActivity.this.resource.getString("mc_forum_permission_cannot_read_topic"));
            }
        }

        public void onReplyNoticeClick(View v, PostsNoticeModel model, long pageFrom) {
            if (MessageFragmentActivity.this.permService.getPermNum(PermConstant.USER_GROUP, "reply", -1) == 1 && MessageFragmentActivity.this.permService.getPermNum(PermConstant.BOARDS, "reply", model.getBoardId()) == 1) {
                MCForumHelper.onReplyClick(MessageFragmentActivity.this, MessageFragmentActivity.this.resource, v, model, pageFrom);
            } else {
                MessageFragmentActivity.this.warnMessageByStr(MessageFragmentActivity.this.resource.getString("mc_forum_permission_cannot_reply_topic"));
            }
        }

        public void onUserHomeClick(View arg0, PostsNoticeModel model) {
            MCForumHelper.gotoUserInfo(MessageFragmentActivity.this, MessageFragmentActivity.this.resource, model.getUserId());
        }
    };
    String reply;
    /* access modifiers changed from: private */
    public ReplyMessageFragment replyFragment = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            this.mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.inflater = LayoutInflater.from(this);
        this.defaultTab = getIntent().getIntExtra(MCConstant.MESSAGE_TAP, 0);
        this.permService = new PermServiceImpl(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_message_fragment_activity"));
        this.mTabHost = (TabHost) findViewById(16908306);
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.mTabHost.setup();
        this.mViewPager = (ViewPager) findViewById(this.resource.getViewId("mc_forum_pager"));
        this.mTabsAdapter = new MessageTabsAdapter(this, this.mTabHost, this.mViewPager);
        this.reply = getResources().getString(this.resource.getStringId("mc_forum_warn_photo_reply_topic"));
        this.atReply = getResources().getString(this.resource.getStringId("mc_forum_mention_friends"));
        this.msg = getResources().getString(this.resource.getStringId("mc_forum_user_private_msg"));
        this.mTabsAdapter.addTab(this.mTabHost.newTabSpec(this.reply).setIndicator(createTabView(this.reply)), ReplyMessageFragment.class, null);
        if (MCForumHelper.setAtReplyMessageFragment(this)) {
            this.mTabsAdapter.addTab(this.mTabHost.newTabSpec(this.atReply).setIndicator(createTabView(this.atReply)), AtReplyMessageFragment.class, null);
        }
        this.mTabsAdapter.addTab(this.mTabHost.newTabSpec(this.msg).setIndicator(createTabView(this.msg)), MsgUserListFragment.class, null);
        if (this.defaultTab == 1) {
            this.mTabHost.setCurrentTabByTag(this.atReply);
        } else if (this.defaultTab == 2) {
            this.mTabHost.setCurrentTabByTag(this.msg);
        } else {
            this.mTabHost.setCurrentTabByTag(this.reply);
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.defaultTab = intent.getIntExtra(MCConstant.MESSAGE_TAP, 0);
        if (this.defaultTab == 1) {
            this.mTabHost.setCurrentTabByTag(this.atReply);
            if (this.atReplyFragment != null) {
                this.atReplyFragment.onRefreshs();
            }
        } else if (this.defaultTab == 2) {
            this.mTabHost.setCurrentTabByTag(this.msg);
            if (this.msgFragment != null) {
                this.msgFragment.onRefresh();
            }
        } else {
            this.mTabHost.setCurrentTabByTag(this.reply);
            if (this.replyFragment != null) {
                this.replyFragment.onRefreshs();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCForumHelper.setBack(MessageFragmentActivity.this);
            }
        });
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return null;
    }

    private View createTabView(String string) {
        View v = this.inflater.inflate(this.resource.getLayoutId("mc_forum_tabs_item"), (ViewGroup) null);
        ((TextView) v.findViewById(this.resource.getViewId("mc_forum_tab_text"))).setText(string);
        return v;
    }

    public class MessageTabsAdapter extends TabsAdapter {
        public MessageTabsAdapter(FragmentActivity activity, TabHost tabHost, ViewPager pager) {
            super(activity, tabHost, pager);
        }

        public Fragment getItem(int position) {
            Fragment fragment = super.getItem(position);
            if (fragment.getClass() == ReplyMessageFragment.class) {
                if (MessageFragmentActivity.this.replyFragment == null) {
                    ReplyMessageFragment unused = MessageFragmentActivity.this.replyFragment = (ReplyMessageFragment) fragment;
                    MessageFragmentActivity.this.replyFragment.setPostsNoticeClickListener(MessageFragmentActivity.this.postsNoticeClickListener);
                    MessageFragmentActivity.this.replyFragment.setAdPosition(new Integer(MessageFragmentActivity.this.getResources().getString(MessageFragmentActivity.this.resource.getStringId("mc_forum_posts_notice_position"))).intValue());
                    MessageFragmentActivity.this.replyFragment.setAdPosition(new Integer(MessageFragmentActivity.this.getResources().getString(MessageFragmentActivity.this.resource.getStringId("mc_forum_posts_notice_position_bottom"))).intValue());
                }
            } else if (fragment.getClass() == AtReplyMessageFragment.class) {
                if (MessageFragmentActivity.this.atReplyFragment == null) {
                    AtReplyMessageFragment unused2 = MessageFragmentActivity.this.atReplyFragment = (AtReplyMessageFragment) fragment;
                    MessageFragmentActivity.this.atReplyFragment.setPostsNoticeClickListener(MessageFragmentActivity.this.postsNoticeClickListener);
                    MessageFragmentActivity.this.atReplyFragment.setAdPosition(new Integer(MessageFragmentActivity.this.getResources().getString(MessageFragmentActivity.this.resource.getStringId("mc_forum_posts_at_notice_position"))).intValue());
                    MessageFragmentActivity.this.atReplyFragment.setAdPosition(new Integer(MessageFragmentActivity.this.getResources().getString(MessageFragmentActivity.this.resource.getStringId("mc_forum_posts_at_notice_position_bottom"))).intValue());
                }
            } else if (fragment.getClass() == MsgUserListFragment.class && MessageFragmentActivity.this.msgFragment == null) {
                MsgUserListFragment unused3 = MessageFragmentActivity.this.msgFragment = (MsgUserListFragment) fragment;
            }
            return fragment;
        }

        public void onTabChanged(String tabId) {
            MessageFragmentActivity.this.mViewPager.setCurrentItem(MessageFragmentActivity.this.mTabHost.getCurrentTab());
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        public void onPageSelected(int position) {
            TabWidget widget = MessageFragmentActivity.this.mTabHost.getTabWidget();
            int oldFocusability = widget.getDescendantFocusability();
            widget.setDescendantFocusability(393216);
            MessageFragmentActivity.this.mTabHost.setCurrentTab(position);
            widget.setDescendantFocusability(oldFocusability);
            if (position == 0) {
                if (MessageFragmentActivity.this.atReplyFragment != null) {
                    Intent intent = new Intent(MessageFragmentActivity.this, MediaService.class);
                    intent.putExtra(MediaService.SERVICE_TAG, MessageFragmentActivity.this.atReplyFragment.toString());
                    intent.putExtra(MediaService.SERVICE_STATUS, 6);
                    MessageFragmentActivity.this.startService(intent);
                }
            } else if (position != 1) {
                if (MessageFragmentActivity.this.atReplyFragment != null) {
                    Intent intent2 = new Intent(MessageFragmentActivity.this, MediaService.class);
                    intent2.putExtra(MediaService.SERVICE_TAG, MessageFragmentActivity.this.atReplyFragment.toString());
                    intent2.putExtra(MediaService.SERVICE_STATUS, 6);
                    MessageFragmentActivity.this.startService(intent2);
                }
                if (MessageFragmentActivity.this.replyFragment != null) {
                    Intent intent22 = new Intent(MessageFragmentActivity.this, MediaService.class);
                    intent22.putExtra(MediaService.SERVICE_TAG, MessageFragmentActivity.this.replyFragment.toString());
                    intent22.putExtra(MediaService.SERVICE_STATUS, 6);
                    MessageFragmentActivity.this.startService(intent22);
                }
            } else if (MessageFragmentActivity.this.replyFragment != null) {
                Intent intent3 = new Intent(MessageFragmentActivity.this, MediaService.class);
                intent3.putExtra(MediaService.SERVICE_TAG, MessageFragmentActivity.this.replyFragment.toString());
                intent3.putExtra(MediaService.SERVICE_STATUS, 6);
                MessageFragmentActivity.this.startService(intent3);
            }
        }

        public void onPageScrollStateChanged(int state) {
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Intent intent = new Intent(this, MediaService.class);
        intent.putExtra(MediaService.SERVICE_TAG, toString());
        intent.putExtra(MediaService.SERVICE_STATUS, 6);
        startService(intent);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.mediaPlayerBroadCastReceiver == null) {
            this.mediaPlayerBroadCastReceiver = new MediaPlayerBroadCastReceiver();
        }
        registerReceiver(this.mediaPlayerBroadCastReceiver, new IntentFilter(MediaService.INTENT_TAG + getPackageName()));
    }

    public void onBackPressed() {
        super.onBackPressed();
        MCForumHelper.setBack(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.mediaPlayerBroadCastReceiver != null) {
            unregisterReceiver(this.mediaPlayerBroadCastReceiver);
        }
    }

    public class MediaPlayerBroadCastReceiver extends BroadcastReceiver {
        private SoundModel currSoundModel = null;
        private String tag;

        public MediaPlayerBroadCastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            this.tag = intent.getStringExtra(MediaService.SERVICE_TAG);
            if (this.tag == null) {
                return;
            }
            if (this.tag.equals(MessageFragmentActivity.this.replyFragment.toString()) || this.tag.equals(MessageFragmentActivity.this.atReplyFragment.toString())) {
                this.currSoundModel = (SoundModel) intent.getSerializableExtra(MediaService.SERVICE_MODEL);
                if (this.tag.equals(MessageFragmentActivity.this.replyFragment.toString())) {
                    MessageFragmentActivity.this.replyFragment.updateReceivePlayImg(this.currSoundModel);
                } else if (this.tag.equals(MessageFragmentActivity.this.atReplyFragment.toString())) {
                    MessageFragmentActivity.this.atReplyFragment.updateReceivePlayImg(this.currSoundModel);
                }
            }
        }
    }
}
