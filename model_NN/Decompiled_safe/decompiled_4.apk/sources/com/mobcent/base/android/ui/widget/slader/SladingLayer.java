package com.mobcent.base.android.ui.widget.slader;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import android.widget.Scroller;
import com.baidu.location.LocationClientOption;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.MCResource;

public class SladingLayer extends FrameLayout {
    public static final int DRAGGING_LEFT = 0;
    public static final int DRAGGING_MID = 2;
    public static final int DRAGGING_RIGHT = 1;
    public static final int MAX_SCROLLING_DURATION = 600;
    public String TAG;
    private int mDraggingState;
    private boolean mDrawingCacheEnabled;
    private boolean mEnabled;
    private Handler mHandler;
    private boolean mIsDragging;
    private boolean mIsOpen;
    private float mLastX;
    private float mLastY;
    private int mMaximumVelocity;
    private int mMinimumVelocity;
    private int mRightDis;
    private int mRightMargin;
    private int mScrollLeftBound;
    private int mScrollRightBound;
    private Scroller mScroller;
    private boolean mScrolling;
    private Drawable mShadowDrawable;
    private int mShadowWidth;
    private int mTouchSlop;
    private SladdingLayerListener sladdingLayerListener;
    private VelocityTracker velocityTracker;

    public interface SladdingLayerListener {
        void close();

        void leftClose();

        void onScrollChanged(int i, int i2, int i3, int i4);

        void open();

        void rightClose();
    }

    public boolean ismIsOpen() {
        return this.mIsOpen;
    }

    public void setmIsOpen(boolean mIsOpen2) {
        this.mIsOpen = mIsOpen2;
    }

    public SladdingLayerListener getSladdingLayerListener() {
        return this.sladdingLayerListener;
    }

    public void setSladdingLayerListener(SladdingLayerListener sladdingLayerListener2) {
        this.sladdingLayerListener = sladdingLayerListener2;
    }

    public Drawable getmShadowDrawable() {
        return this.mShadowDrawable;
    }

    public void setmShadowDrawable(Drawable mShadowDrawable2) {
        this.mShadowDrawable = mShadowDrawable2;
    }

    public int getmDraggingState() {
        return this.mDraggingState;
    }

    public void setmDraggingState(int mDraggingState2) {
        this.mDraggingState = mDraggingState2;
        initBounds();
    }

    public int getmRightDis() {
        return this.mRightDis;
    }

    public void setmRightDis(int mRightDis2) {
        this.mRightDis = mRightDis2;
    }

    public SladingLayer(Context context) {
        this(context, null);
    }

    public SladingLayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.TAG = "SladingLayer";
        this.mIsOpen = true;
        this.mEnabled = true;
        this.mShadowWidth = 20;
        this.mRightDis = 60;
        this.mRightMargin = 100;
        this.mDraggingState = 0;
        this.mShadowDrawable = null;
        this.mHandler = new Handler();
        init(context);
    }

    private void init(Context context) {
        this.mScroller = new Scroller(context);
        ViewConfiguration configuration = ViewConfiguration.get(context);
        this.mTouchSlop = configuration.getScaledTouchSlop();
        this.mMinimumVelocity = configuration.getScaledMinimumFlingVelocity();
        this.mMaximumVelocity = configuration.getScaledMaximumFlingVelocity();
        try {
            this.mRightDis = (int) getResources().getDimension(MCResource.getInstance(context).getDimenId("home_slader_right_dis"));
        } catch (Exception e) {
            this.mRightDis = 60;
        }
        this.mRightMargin = (int) dip2px((float) this.mRightMargin);
        this.mShadowWidth = (int) dip2px((float) this.mShadowWidth);
        initBounds();
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!this.mEnabled) {
            return false;
        }
        int action = ev.getAction() & 255;
        if (action == 3 || action == 1) {
            return false;
        }
        if (action != 0 && this.mIsDragging) {
            return true;
        }
        switch (action) {
            case 0:
                this.mLastX = ev.getX();
                this.mLastY = ev.getY();
                if (this.mIsOpen) {
                    this.mIsDragging = false;
                    return super.onInterceptTouchEvent(ev);
                } else if (getScrollX() == this.mScrollLeftBound) {
                    if (this.mLastX > ((float) (-this.mScrollLeftBound)) && this.mLastX < ((float) getScreenWidth())) {
                        return true;
                    }
                } else if (getScrollX() == this.mScrollRightBound && this.mLastX > 0.0f && this.mLastX < ((float) (getScreenWidth() - this.mScrollRightBound))) {
                    return true;
                }
            case 2:
                float xDiff = Math.abs(ev.getX() - this.mLastX);
                float yDiff = Math.abs(ev.getY() - this.mLastY);
                if (xDiff > ((float) this.mTouchSlop) && xDiff > yDiff) {
                    this.mIsDragging = true;
                    this.mLastX = ev.getX();
                    setDrawingCacheEnabled(true);
                    break;
                } else {
                    this.mIsDragging = false;
                    break;
                }
                break;
        }
        return this.mIsDragging;
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (!isAllowDragging(ev) && !this.mIsDragging) {
            return false;
        }
        if (this.velocityTracker == null) {
            this.velocityTracker = VelocityTracker.obtain();
        }
        this.velocityTracker.addMovement(ev);
        switch (ev.getAction() & 255) {
            case 0:
                this.mLastX = ev.getX();
                this.mLastY = ev.getY();
                break;
            case 1:
                onDraggingCancelEvent(ev);
                break;
            case 2:
                if (!this.mIsDragging) {
                    float xDiff = Math.abs(ev.getX() - this.mLastX);
                    float yDiff = Math.abs(ev.getY() - this.mLastY);
                    if (xDiff > ((float) this.mTouchSlop) && xDiff > yDiff) {
                        this.mIsDragging = true;
                        this.mLastX = ev.getX();
                        setDrawingCacheEnabled(true);
                        break;
                    }
                } else {
                    onDraggingEvent(ev);
                    break;
                }
            case 3:
                onDraggingCancelEvent(ev);
                break;
            case 6:
                onDraggingCancelEvent(ev);
                break;
        }
        return true;
    }

    public void computeScroll() {
        if (this.mScroller.isFinished() || !this.mScroller.computeScrollOffset()) {
            completeScroll();
            return;
        }
        int oldX = getScrollX();
        int oldY = getScrollY();
        int x = this.mScroller.getCurrX();
        int y = this.mScroller.getCurrY();
        if (!(oldX == x && oldY == y)) {
            scrollToByHandler(x, y);
        }
        invalidate(getLeft() + oldX, getTop(), getRight(), getBottom());
    }

    public void setDrawingCacheEnabled(boolean enabled) {
        if (this.mDrawingCacheEnabled != enabled) {
            super.setDrawingCacheEnabled(enabled);
            this.mDrawingCacheEnabled = enabled;
            setChildrenDrawingCacheEnabled(enabled);
        }
    }

    private void smoothScroll(int x) {
        if (getChildCount() == 0) {
            setDrawingCacheEnabled(false);
            return;
        }
        int dx = x - getScrollX();
        if (dx == 0) {
            completeScroll();
            return;
        }
        setDrawingCacheEnabled(true);
        this.mScrolling = true;
        this.mScroller.startScroll(getScrollX(), getScrollY(), dx, 0, MAX_SCROLLING_DURATION);
        invalidate();
    }

    private void completeScroll() {
        if (this.mScrolling) {
            MCLogUtil.e(this.TAG, "completeScroll   mScrolling");
            setDrawingCacheEnabled(false);
            this.mScroller.abortAnimation();
            int oldX = getScrollX();
            int oldY = getScrollY();
            int x = this.mScroller.getCurrX();
            int y = this.mScroller.getCurrY();
            if (!(oldX == x && oldY == y)) {
                scrollToByHandler(x, y);
            }
        }
        this.mScrolling = false;
    }

    private void onDraggingEvent(MotionEvent ev) {
        float disX = this.mLastX - ev.getX();
        this.mLastX = ev.getX();
        float scrollX = ((float) getScrollX()) + disX;
        switch (this.mDraggingState) {
            case 0:
                if (scrollX >= ((float) this.mScrollLeftBound)) {
                    if (scrollX > ((float) this.mScrollRightBound)) {
                        scrollX = (float) this.mScrollRightBound;
                        break;
                    }
                } else {
                    scrollX = (float) this.mScrollLeftBound;
                    break;
                }
                break;
            case 1:
                if (scrollX >= ((float) this.mScrollLeftBound)) {
                    if (scrollX > ((float) this.mScrollRightBound)) {
                        scrollX = (float) this.mScrollRightBound;
                        break;
                    }
                } else {
                    scrollX = (float) this.mScrollLeftBound;
                    break;
                }
                break;
            case 2:
                if (scrollX >= ((float) this.mScrollLeftBound)) {
                    if (scrollX > ((float) this.mScrollRightBound)) {
                        scrollX = (float) this.mScrollRightBound;
                        break;
                    }
                } else {
                    scrollX = (float) this.mScrollLeftBound;
                    break;
                }
                break;
        }
        scrollToByHandler((int) scrollX, getScrollY());
    }

    private void onDraggingCancelEvent(MotionEvent ev) {
        if (this.mIsDragging) {
            switchLayer(getNextOptionState());
            endDragDo();
        }
        if (this.velocityTracker != null) {
            this.velocityTracker.computeCurrentVelocity(LocationClientOption.MIN_SCAN_SPAN, (float) this.mMaximumVelocity);
            this.velocityTracker.recycle();
            this.velocityTracker = null;
        }
    }

    private boolean getNextOptionState() {
        switch (this.mDraggingState) {
            case 0:
                if (getScrollX() > this.mScrollLeftBound / 2) {
                    return true;
                }
                return false;
            case 1:
                if (getScrollX() < this.mScrollRightBound / 2) {
                    return true;
                }
                return false;
            case 2:
                if ((getScrollX() <= this.mScrollLeftBound / 2 || getScrollX() > 0) && (getScrollX() <= 0 || getScrollX() >= this.mScrollRightBound / 2)) {
                    return false;
                }
                return true;
            default:
                return false;
        }
    }

    public void switchLayer(boolean open) {
        this.mIsOpen = open;
        if (this.sladdingLayerListener != null) {
            if (open) {
                this.sladdingLayerListener.open();
            } else {
                this.sladdingLayerListener.close();
            }
        }
        int scrollX = 0;
        switch (this.mDraggingState) {
            case 0:
                if (!this.mIsOpen) {
                    scrollX = this.mScrollLeftBound;
                    break;
                } else {
                    scrollX = this.mScrollRightBound;
                    break;
                }
            case 1:
                if (!this.mIsOpen) {
                    scrollX = this.mScrollRightBound;
                    break;
                } else {
                    scrollX = this.mScrollLeftBound;
                    break;
                }
            case 2:
                if (!this.mIsOpen) {
                    if (this.mScrollLeftBound > getScrollX() || getScrollX() >= this.mScrollLeftBound / 2) {
                        if (getScrollX() > this.mScrollRightBound / 2 && getScrollX() <= this.mScrollRightBound) {
                            scrollX = this.mScrollRightBound;
                            break;
                        } else {
                            this.mIsOpen = true;
                            scrollX = 0;
                            break;
                        }
                    } else {
                        scrollX = this.mScrollLeftBound;
                        break;
                    }
                } else {
                    scrollX = 0;
                    break;
                }
                break;
        }
        smoothScroll(scrollX);
    }

    public void switchLayerMid(boolean open, boolean isLeft) {
        this.mIsOpen = open;
        int scrollX = 0;
        if (!this.mIsOpen) {
            switch (this.mDraggingState) {
                case 0:
                    scrollX = this.mScrollLeftBound;
                    if (this.sladdingLayerListener != null) {
                        this.sladdingLayerListener.close();
                        break;
                    }
                    break;
                case 1:
                    scrollX = this.mScrollRightBound;
                    if (this.sladdingLayerListener != null) {
                        this.sladdingLayerListener.close();
                        break;
                    }
                    break;
                case 2:
                    if (!isLeft) {
                        scrollX = this.mScrollRightBound;
                        if (this.sladdingLayerListener != null) {
                            this.sladdingLayerListener.rightClose();
                            break;
                        }
                    } else {
                        scrollX = this.mScrollLeftBound;
                        if (this.sladdingLayerListener != null) {
                            this.sladdingLayerListener.leftClose();
                            break;
                        }
                    }
                    break;
            }
        } else if (this.sladdingLayerListener != null) {
            this.sladdingLayerListener.open();
        }
        smoothScroll(scrollX);
    }

    public void initBounds() {
        switch (this.mDraggingState) {
            case 0:
                this.mScrollLeftBound = this.mRightDis - getScreenWidth();
                this.mScrollRightBound = 0;
                return;
            case 1:
                this.mScrollLeftBound = 0;
                this.mScrollRightBound = this.mRightMargin;
                return;
            case 2:
                this.mScrollLeftBound = this.mRightDis - getScreenWidth();
                this.mScrollRightBound = this.mRightMargin;
                return;
            default:
                return;
        }
    }

    private boolean isAllowDragging(MotionEvent ev) {
        boolean isAllowDragging = false;
        if (this.mIsOpen) {
            return true;
        }
        switch (this.mDraggingState) {
            case 0:
                if (ev.getX() > ((float) Math.abs(this.mScrollLeftBound)) && ev.getX() < ((float) getScreenWidth())) {
                    isAllowDragging = true;
                    break;
                }
            case 1:
                if (ev.getX() > 0.0f && ev.getX() < ((float) (getScreenWidth() - this.mScrollRightBound))) {
                    isAllowDragging = true;
                    break;
                }
            case 2:
                if (getScrollX() != this.mScrollLeftBound) {
                    if (ev.getX() > 0.0f && ev.getX() < ((float) (getScreenWidth() - this.mScrollRightBound))) {
                        isAllowDragging = true;
                        break;
                    }
                } else if (ev.getX() > ((float) Math.abs(this.mScrollLeftBound)) && ev.getX() < ((float) getScreenWidth())) {
                    isAllowDragging = true;
                    break;
                }
        }
        return isAllowDragging;
    }

    private void endDragDo() {
        this.mIsDragging = false;
        setDrawingCacheEnabled(false);
    }

    private float dip2px(float dipValue) {
        return TypedValue.applyDimension(1, dipValue, Resources.getSystem().getDisplayMetrics());
    }

    private int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.mShadowWidth > 0 && this.mShadowDrawable != null) {
            switch (this.mDraggingState) {
                case 0:
                    this.mShadowDrawable.setBounds(-this.mShadowWidth, 0, 0, getHeight());
                    break;
                case 1:
                    this.mShadowDrawable.setBounds(getWidth(), 0, getWidth() + this.mShadowWidth, getHeight());
                    break;
                case 2:
                    this.mShadowDrawable.setBounds(-this.mShadowWidth, 0, 0, getHeight());
                    break;
            }
            this.mShadowDrawable.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (this.sladdingLayerListener != null) {
            this.sladdingLayerListener.onScrollChanged(l, t, oldl, oldt);
        }
    }

    private void scrollToByHandler(int x, int y) {
        if (getScrollX() != x) {
            scrollTo(x, y);
        }
    }
}
