package com.mobcent.base.android.ui.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.WheelListAdapter;
import com.mobcent.base.android.ui.activity.widget.wheel.OnWheelChangedListener;
import com.mobcent.base.android.ui.activity.widget.wheel.OnWheelScrollListener;
import com.mobcent.base.android.ui.activity.widget.wheel.WheelView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.constant.MentionFriendConstant;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.model.TopicDraftModel;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PublishAnnounceActivity extends BasePublishWithSelectBoardActivity implements MCConstant, MentionFriendConstant {
    private Button backBtn;
    private OnWheelChangedListener changedListener = new OnWheelChangedListener() {
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            if (!PublishAnnounceActivity.this.wheelScrolled) {
                PublishAnnounceActivity.this.updateStatus();
            }
        }
    };
    protected String content;
    /* access modifiers changed from: private */
    public int currentRoleNum = 2;
    protected long draftId = 4;
    private Button endTimeBtn;
    private TextView endTimeText;
    private int isReply = 0;
    private boolean isSelect = false;
    /* access modifiers changed from: private */
    public boolean isShowWheel = false;
    private PublishAsyncTask publishAsyncTask;
    private Button removeBtn;
    OnWheelScrollListener scrolledListener = new OnWheelScrollListener() {
        public void onScrollingStarted(WheelView wheel) {
            boolean unused = PublishAnnounceActivity.this.wheelScrolled = true;
        }

        public void onScrollingFinished(WheelView wheel) {
            boolean unused = PublishAnnounceActivity.this.wheelScrolled = false;
            PublishAnnounceActivity.this.updateStatus();
        }
    };
    protected String title;
    protected RelativeLayout transparentLayout;
    private UserService userService;
    /* access modifiers changed from: private */
    public LinearLayout wheelBox;
    /* access modifiers changed from: private */
    public boolean wheelScrolled = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.draftId == 4) {
            getDraft(this.draftId);
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.boardList = new ArrayList();
        this.userService = new UserServiceImpl(this);
        this.boardId = getIntent().getLongExtra("boardId", -1);
        this.currentRoleNum = this.userService.getRoleNum();
        this.permService = new PermServiceImpl(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_publish_announce_activity"));
        super.initViews();
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.wheelBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_wheel_box"));
        this.transparentLayout = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_transparent_box"));
        this.endTimeBtn = (Button) findViewById(this.resource.getViewId("mc_forum_end_time_btn"));
        this.removeBtn = (Button) findViewById(this.resource.getViewId("mc_forum_remove_btn"));
        this.endTimeText = (TextView) findViewById(this.resource.getViewId("mc_forum_anno_time_end_text"));
        if (this.boardId >= 0) {
            this.selectBoardImg.setEnabled(false);
        }
        initWheel(this.resource.getViewId("mc_forum_wheel1"));
        initWheel(this.resource.getViewId("mc_forum_wheel2"));
        initWheel(this.resource.getViewId("mc_forum_wheel3"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.boardListview.setAdapter((ListAdapter) this.boardListAdapter);
        this.selectBoardImg.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (PublishAnnounceActivity.this.currentRoleNum >= 8) {
                    PublishAnnounceActivity.this.showBoardListBoxEvent();
                } else if (PublishAnnounceActivity.this.currentRoleNum == 4 && (PublishAnnounceActivity.this.boardList == null || PublishAnnounceActivity.this.boardList.isEmpty())) {
                    PublishAnnounceActivity.this.boardList = new ArrayList();
                    PublishAnnounceActivity.this.updateBoardList(PublishAnnounceActivity.this.boardList);
                    if (!PublishAnnounceActivity.this.isShowBoardBox) {
                        PublishAnnounceActivity.this.showBoardListBox(true);
                    } else {
                        PublishAnnounceActivity.this.showBoardListBox(false);
                    }
                }
                return true;
            }
        });
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PublishAnnounceActivity.this.back();
            }
        });
        this.endTimeBtn.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.mobcent.base.android.ui.activity.PublishAnnounceActivity.access$302(com.mobcent.base.android.ui.activity.PublishAnnounceActivity, boolean):boolean
             arg types: [com.mobcent.base.android.ui.activity.PublishAnnounceActivity, int]
             candidates:
              com.mobcent.base.android.ui.activity.BasePublishWithFriendActivity.access$302(com.mobcent.base.android.ui.activity.BasePublishWithFriendActivity, android.app.ProgressDialog):android.app.ProgressDialog
              com.mobcent.base.android.ui.activity.BasePhotoPreviewActivity.access$302(com.mobcent.base.android.ui.activity.BasePhotoPreviewActivity, com.mobcent.base.android.ui.activity.BasePhotoPreviewActivity$UploadAsyncTask):com.mobcent.base.android.ui.activity.BasePhotoPreviewActivity$UploadAsyncTask
              com.mobcent.base.android.ui.activity.PublishAnnounceActivity.access$302(com.mobcent.base.android.ui.activity.PublishAnnounceActivity, boolean):boolean */
            public void onClick(View v) {
                PublishAnnounceActivity.this.hideSoftKeyboard();
                if (PublishAnnounceActivity.this.wheelBox.getVisibility() == 0) {
                    PublishAnnounceActivity.this.iSShowWheel(true);
                    boolean unused = PublishAnnounceActivity.this.isShowWheel = false;
                } else if (PublishAnnounceActivity.this.wheelBox.getVisibility() == 8) {
                    PublishAnnounceActivity.this.iSShowWheel(false);
                    boolean unused2 = PublishAnnounceActivity.this.isShowWheel = true;
                }
            }
        });
        this.removeBtn.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.mobcent.base.android.ui.activity.PublishAnnounceActivity.access$302(com.mobcent.base.android.ui.activity.PublishAnnounceActivity, boolean):boolean
             arg types: [com.mobcent.base.android.ui.activity.PublishAnnounceActivity, int]
             candidates:
              com.mobcent.base.android.ui.activity.BasePublishWithFriendActivity.access$302(com.mobcent.base.android.ui.activity.BasePublishWithFriendActivity, android.app.ProgressDialog):android.app.ProgressDialog
              com.mobcent.base.android.ui.activity.BasePhotoPreviewActivity.access$302(com.mobcent.base.android.ui.activity.BasePhotoPreviewActivity, com.mobcent.base.android.ui.activity.BasePhotoPreviewActivity$UploadAsyncTask):com.mobcent.base.android.ui.activity.BasePhotoPreviewActivity$UploadAsyncTask
              com.mobcent.base.android.ui.activity.PublishAnnounceActivity.access$302(com.mobcent.base.android.ui.activity.PublishAnnounceActivity, boolean):boolean */
            public void onClick(View v) {
                PublishAnnounceActivity.this.iSShowWheel(true);
                boolean unused = PublishAnnounceActivity.this.isShowWheel = false;
            }
        });
    }

    /* access modifiers changed from: private */
    public void iSShowWheel(boolean isShowWheel2) {
        if (!isShowWheel2) {
            this.wheelBox.setVisibility(0);
            this.removeBtn.setVisibility(0);
            this.transparentLayout.setVisibility(8);
            return;
        }
        this.wheelBox.setVisibility(8);
        this.removeBtn.setVisibility(8);
        this.transparentLayout.setVisibility(0);
    }

    public void showSoftKeyboard() {
        super.showSoftKeyboard();
        iSShowWheel(true);
        this.isShowWheel = false;
    }

    public boolean checkTitle() {
        this.title = this.titleEdit.getText().toString();
        int len = this.title.length();
        if (this.title == null || this.title.trim().equals("")) {
            warnMessageById("mc_forum_anno_title_error");
            return false;
        } else if (len <= 25) {
            return true;
        } else {
            warnMessageById("mc_forum_publish_max_title_length_error");
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean publicTopic() {
        String toDate;
        super.publicTopic();
        this.content = this.contentEdit.getText().toString();
        String fromDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        try {
            toDate = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyy年MM月dd日").parse(this.endTimeText.getText().toString().trim()));
        } catch (ParseException e) {
            toDate = "";
        }
        if (this.boardId < 0) {
            Toast.makeText(this, getResources().getString(this.resource.getStringId("mc_forum_publish_select_board")), 0).show();
            return false;
        } else if (!checkTitle()) {
            return false;
        } else {
            if (StringUtil.isEmpty(this.content)) {
                warnMessageById("mc_forum_publish_min_length_error");
                return false;
            } else if (this.content.length() > 7000) {
                warnMessageById("mc_forum_publish_max_length_error");
                return false;
            } else if (StringUtil.isEmpty(toDate)) {
                warnMessageById("mc_forum_anno_end_time");
                return false;
            } else if (!dateIsRight(fromDate, toDate)) {
                warnMessageById("mc_forum_anno_end_time_error");
                return false;
            } else {
                this.canPublishTopic = true;
                if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.POST, -1) == 0 || this.permService.getPermNum(PermConstant.BOARDS, PermConstant.POST, this.boardId) == 0) {
                    warnMessageByStr(this.resource.getString("mc_forum_permission_cannot_post_toipc"));
                    return false;
                }
                if (this.locationComplete) {
                    PostsService postsService = new PostsServiceImpl(this);
                    String contentStr = postsService.createPublishTopicJson(this.content.trim(), "ß", "á", this.mentionedFriends, null, 0);
                    if (!postsService.isContainsPic(this.content.trim(), "ß", "á", this.audioPath, this.audioDuration)) {
                        if (this.publishAsyncTask != null) {
                            this.publishAsyncTask.cancel(true);
                        }
                        this.publishAsyncTask = new PublishAsyncTask();
                        this.publishAsyncTask.execute(Long.toString(this.boardId), Integer.toString(this.isReply), this.title, contentStr, fromDate, toDate, this.selectVisibleId + "");
                    } else if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.UPLOAD, -1) == 1 && this.permService.getPermNum(PermConstant.BOARDS, PermConstant.UPLOAD, this.boardId) == 1) {
                        if (this.publishAsyncTask != null) {
                            this.publishAsyncTask.cancel(true);
                        }
                        this.publishAsyncTask = new PublishAsyncTask();
                        this.publishAsyncTask.execute(Long.toString(this.boardId), Integer.toString(this.isReply), this.title, contentStr, fromDate, toDate, this.selectVisibleId + "");
                    } else {
                        warnMessageByStr(this.resource.getString("mc_forum_permission_cannot_upload_pic"));
                        return false;
                    }
                }
                return true;
            }
        }
    }

    private void initWheel(int id) {
        WheelView wheel = getWheel(id);
        if (id == this.resource.getViewId("mc_forum_wheel1")) {
            List<String> list = new ArrayList<>();
            int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(new Date()));
            for (int i = year; i <= year + 20; i++) {
                list.add(i + getResources().getString(this.resource.getStringId("mc_forum_anno_time_year")));
            }
            wheel.setAdapter(new WheelListAdapter(list));
        } else if (id == this.resource.getViewId("mc_forum_wheel2")) {
            List<String> list2 = new ArrayList<>();
            for (int i2 = 1; i2 <= 12; i2++) {
                list2.add(i2 + getResources().getString(this.resource.getStringId("mc_forum_anno_time_month")));
            }
            wheel.setAdapter(new WheelListAdapter(list2));
            wheel.setCurrentItem(Integer.parseInt(new SimpleDateFormat("MM").format(new Date())) - 1);
        } else if (id == this.resource.getViewId("mc_forum_wheel3")) {
            List<String> list3 = new ArrayList<>();
            for (int i3 = 1; i3 <= 31; i3++) {
                list3.add(i3 + getResources().getString(this.resource.getStringId("mc_forum_anno_time_day")));
            }
            wheel.setAdapter(new WheelListAdapter(list3));
            wheel.setCurrentItem(Integer.parseInt(new SimpleDateFormat("dd").format(new Date())) - 1);
        }
        wheel.addChangingListener(this.changedListener);
        wheel.addScrollingListener(this.scrolledListener);
        wheel.setCyclic(true);
        wheel.setInterpolator(new AnticipateOvershootInterpolator());
    }

    private WheelView getWheel(int id) {
        return (WheelView) findViewById(id);
    }

    /* access modifiers changed from: private */
    public void updateStatus() {
        this.endTimeText.setText(getAllCode());
    }

    private String getAllCode() {
        return (getWheel(this.resource.getViewId("mc_forum_wheel1")).getAdapter().getItem(getWheel(this.resource.getViewId("mc_forum_wheel1")).getCurrentItem()) + "") + (getWheel(this.resource.getViewId("mc_forum_wheel2")).getAdapter().getItem(getWheel(this.resource.getViewId("mc_forum_wheel2")).getCurrentItem()) + "") + (getWheel(this.resource.getViewId("mc_forum_wheel3")).getAdapter().getItem(getWheel(this.resource.getViewId("mc_forum_wheel3")).getCurrentItem()) + "");
    }

    /* access modifiers changed from: protected */
    public void exitSaveEvent() {
        saveAsDraft(this.draftId, new TopicDraftModel());
        finish();
    }

    /* access modifiers changed from: protected */
    public boolean exitCheckChanged() {
        if (!this.titleEdit.getText().toString().trim().equals("") || !this.contentEdit.getText().toString().trim().equals("")) {
            return true;
        }
        return false;
    }

    class PublishAsyncTask extends AsyncTask<String, Void, String> {
        PublishAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            ((InputMethodManager) PublishAnnounceActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(PublishAnnounceActivity.this.titleEdit.getWindowToken(), 0);
            ((InputMethodManager) PublishAnnounceActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(PublishAnnounceActivity.this.contentEdit.getWindowToken(), 0);
            try {
                PublishAnnounceActivity.this.showProgressDialog("mc_forum_warn_publish", this);
            } catch (Exception e) {
            }
            PublishAnnounceActivity.this.publishIng = true;
            PublishAnnounceActivity.this.publishBtn.setEnabled(false);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            PostsService postsService = new PostsServiceImpl(PublishAnnounceActivity.this);
            int boardId = Integer.parseInt(params[0]);
            return postsService.publishAnnounce((long) boardId, Integer.parseInt(params[1]), params[2], params[3], params[4], params[5], Integer.parseInt(params[6]));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            PublishAnnounceActivity.this.publishBtn.setEnabled(true);
            PublishAnnounceActivity.this.publishIng = false;
            PublishAnnounceActivity.this.hideProgressDialog();
            if (result == null) {
                if (PublishAnnounceActivity.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.POST, -1) == 2 || PublishAnnounceActivity.this.permService.getPermNum(PermConstant.BOARDS, PermConstant.POST, PublishAnnounceActivity.this.boardId) == 2) {
                    PublishAnnounceActivity.this.warnMessageById("mc_forum_permission_post_after_verify");
                } else {
                    PublishAnnounceActivity.this.warnMessageById("mc_forum_publish_succ");
                }
                PublishAnnounceActivity.this.deleteDraft(PublishAnnounceActivity.this.draftId);
                PublishAnnounceActivity.this.exitNoSaveEvent();
            } else if (!result.equals("")) {
                PublishAnnounceActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(PublishAnnounceActivity.this, result) + "\n" + PublishAnnounceActivity.this.getString(PublishAnnounceActivity.this.resource.getStringId("mc_forum_warn_publish_fail")));
                PublishAnnounceActivity.this.saveAsDraft(PublishAnnounceActivity.this.draftId, new TopicDraftModel());
            }
            if (PublishAnnounceActivity.this.isExit) {
                PublishAnnounceActivity.this.exitNoSaveEvent();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void updateBoardList(List<BoardModel> boardList) {
        if (this.currentRoleNum >= 8) {
            BoardModel boardModel = new BoardModel();
            boardModel.setBoardId(0);
            boardModel.setBoardName(getString(this.resource.getStringId("mc_forum_announce_detail")));
            boardList.add(0, boardModel);
        } else if (this.currentRoleNum == 4) {
            this.selectBoardImg.setText(this.boardName);
            this.selectBoardImg.setEnabled(false);
        }
        this.boardListAdapter.setBoardList(boardList);
        this.boardListAdapter.notifyDataSetInvalidated();
        this.boardListAdapter.notifyDataSetChanged();
        this.boardList = boardList;
    }

    /* access modifiers changed from: protected */
    public TopicDraftModel saveOtherDataToDraft(TopicDraftModel draft) {
        draft.setBoardId(this.boardId);
        draft.setBoardName(this.selectBoardImg.getText().toString());
        return draft;
    }

    /* access modifiers changed from: protected */
    public void restoreOtherViewFromDraft(TopicDraftModel draft) {
        this.boardName = draft.getBoardName();
        this.boardId = draft.getBoardId();
        if (!StringUtil.isEmpty(this.boardName)) {
            this.selectBoardImg.setText(this.boardName);
        }
    }

    private boolean dateIsRight(String fromDate, String toDate) {
        int fromYear = Integer.parseInt(fromDate.substring(0, 4));
        int toYear = Integer.parseInt(toDate.substring(0, 4));
        if (toYear > fromYear) {
            return true;
        }
        if (toYear != fromYear) {
            return false;
        }
        int fromMonth = Integer.parseInt(fromDate.substring(5, 7));
        int toMonth = Integer.parseInt(toDate.substring(5, 7));
        if (toMonth > fromMonth) {
            return true;
        }
        if (toMonth != fromMonth || Integer.parseInt(fromDate.substring(8, 10)) >= Integer.parseInt(toDate.substring(8, 10))) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.publishAsyncTask != null) {
            this.publishAsyncTask.cancel(true);
        }
    }
}
