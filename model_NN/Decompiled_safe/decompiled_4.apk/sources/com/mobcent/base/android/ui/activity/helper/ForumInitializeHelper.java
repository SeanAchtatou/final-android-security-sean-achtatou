package com.mobcent.base.android.ui.activity.helper;

import android.content.Context;
import android.os.AsyncTask;
import com.mobcent.ad.android.api.BaseAdApiRequester;
import com.mobcent.ad.android.ui.widget.manager.AdManager;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.PayStateModel;
import com.mobcent.forum.android.service.impl.PayStateServiceImpl;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public class ForumInitializeHelper {
    private static final ForumInitializeHelper mcFroumInitializeHelper = new ForumInitializeHelper();
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public McForumInitializeListener forumInitializeListener;
    /* access modifiers changed from: private */
    public PayStateModel payStateModel;

    public interface McForumInitializeListener {
        void onPostExecute(String str);
    }

    private ForumInitializeHelper() {
    }

    public static ForumInitializeHelper getInstance() {
        return mcFroumInitializeHelper;
    }

    public void init(Context context2, McForumInitializeListener forumInitializeListener2) {
        this.context = context2;
        this.forumInitializeListener = forumInitializeListener2;
        new LoadDataAsyncTask().execute(new Void[0]);
    }

    private class LoadDataAsyncTask extends AsyncTask<Void, Void, String> {
        private LoadDataAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            BaseAdApiRequester.BASE_URL = "http://adapi.mobcent.com/";
            SharedPreferencesDB sharedDB = SharedPreferencesDB.getInstance(ForumInitializeHelper.this.context);
            AdManager.getInstance().init(ForumInitializeHelper.this.context, sharedDB.getForumKey(), sharedDB.getChannelId() + "", sharedDB.getUserId());
            MCForumHelper.prepareToLaunchForum(ForumInitializeHelper.this.context);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... params) {
            PayStateModel unused = ForumInitializeHelper.this.payStateModel = new PayStateServiceImpl(ForumInitializeHelper.this.context).controll(SharedPreferencesDB.getInstance(ForumInitializeHelper.this.context).getForumKey());
            new PermServiceImpl(ForumInitializeHelper.this.context).getPerm();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (ForumInitializeHelper.this.payStateModel != null) {
                SharedPreferencesDB.getInstance(ForumInitializeHelper.this.context).setPayState(ForumInitializeHelper.this.payStateModel);
            }
            if (!StringUtil.isEmpty(SharedPreferencesDB.getInstance(ForumInitializeHelper.this.context).getWeiXinAppKey())) {
                WXAPIFactory.createWXAPI(ForumInitializeHelper.this.context, SharedPreferencesDB.getInstance(ForumInitializeHelper.this.context).getWeiXinAppKey()).registerApp(SharedPreferencesDB.getInstance(ForumInitializeHelper.this.context).getWeiXinAppKey());
            }
            if (ForumInitializeHelper.this.forumInitializeListener != null) {
                ForumInitializeHelper.this.forumInitializeListener.onPostExecute(result);
            }
        }
    }
}
