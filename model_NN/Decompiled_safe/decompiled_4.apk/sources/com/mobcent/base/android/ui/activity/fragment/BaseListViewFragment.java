package com.mobcent.base.android.ui.activity.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseListViewFragment extends BaseFragment {
    protected PullToRefreshListView.OnScrollListener listOnScrollListener = new PullToRefreshListView.OnScrollListener() {
        private int firstVisibleItem;
        private int totalItemCount;
        private int visibleItemCount;

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0) {
                BaseListViewFragment.this.asyncTaskLoaderImage.recycleBitmaps(getRecycleImgUrls(BaseListViewFragment.this.pageSize));
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
            this.firstVisibleItem = firstVisibleItem2;
            this.visibleItemCount = visibleItemCount2;
            this.totalItemCount = totalItemCount2 - 2;
        }

        private List<String> getRecycleImgUrls(int pageSize) {
            int size = pageSize;
            List<String> imgUrls = new ArrayList<>();
            int firstIndex = 0;
            int endIndex = this.firstVisibleItem + this.visibleItemCount + size;
            if (this.firstVisibleItem - size > 0) {
                firstIndex = this.firstVisibleItem - size;
            }
            if (endIndex >= this.totalItemCount) {
                endIndex = this.totalItemCount;
            }
            imgUrls.addAll(BaseListViewFragment.this.getImageURL(0, firstIndex));
            imgUrls.addAll(BaseListViewFragment.this.getImageURL(endIndex + 1, this.totalItemCount));
            return imgUrls;
        }

        public void onScrollDirection(boolean isUp, int distance) {
        }
    };
    protected int pageSize = 20;

    /* access modifiers changed from: protected */
    public abstract List<String> getImageURL(int i, int i2);

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
