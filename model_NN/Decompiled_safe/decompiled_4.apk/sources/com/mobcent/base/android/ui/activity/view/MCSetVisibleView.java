package com.mobcent.base.android.ui.activity.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;
import com.mobcent.base.forum.android.util.MCResource;

public class MCSetVisibleView extends RadioButton {
    private Context context;
    private MCResource resource;

    public MCSetVisibleView(Context context2, AttributeSet attrs, int defStyle) {
        super(context2, attrs, defStyle);
        initData(context2);
    }

    public MCSetVisibleView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        initData(context2);
    }

    public MCSetVisibleView(Context context2) {
        super(context2);
        initData(context2);
    }

    private void initData(Context context2) {
        this.context = context2;
        this.resource = MCResource.getInstance(context2);
    }

    public RadioButton create(String content, int id) {
        RadioButton button = new RadioButton(this.context);
        button.setButtonDrawable(this.resource.getDrawableId("mc_forum_select2_1"));
        button.setTextColor(this.resource.getColorId("mc_forum_gray_color"));
        button.setId(id);
        button.setText(content);
        return button;
    }
}
