package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.adapter.holder.MentionFriendAdapterHolder;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.BoardModel;
import java.util.List;

public class PublishTopicBoardListAdapter extends BaseAdapter {
    private List<BoardModel> boardList;
    private MCResource forumResource;
    private MentionFriendAdapterHolder holder;
    private LayoutInflater inflater;

    public List<BoardModel> getBoardList() {
        return this.boardList;
    }

    public void setBoardList(List<BoardModel> boardList2) {
        this.boardList = boardList2;
    }

    public PublishTopicBoardListAdapter(Context context, List<BoardModel> boardList2) {
        this.boardList = boardList2;
        this.inflater = LayoutInflater.from(context);
        this.forumResource = MCResource.getInstance(context);
    }

    public int getCount() {
        if (this.boardList != null) {
            return this.boardList.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        return this.boardList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (this.boardList == null) {
            return null;
        }
        BoardModel boardModel = this.boardList.get(position);
        View convertView2 = getBoardConvertView(convertView);
        ImageView selectImg = (ImageView) convertView2.findViewById(this.forumResource.getViewId("mc_forum_select_board_image"));
        this.holder.getNameText().setText(boardModel.getBoardName());
        if (!boardModel.isSelected()) {
            selectImg.setVisibility(8);
        } else {
            selectImg.setVisibility(0);
        }
        return convertView2;
    }

    private View getBoardConvertView(View convertView) {
        if (convertView == null) {
            convertView = this.inflater.inflate(this.forumResource.getLayoutId("mc_forum_publish_board_item"), (ViewGroup) null);
            this.holder = new MentionFriendAdapterHolder();
            this.holder.setNameText((TextView) convertView.findViewById(this.forumResource.getViewId("mc_forum_board_name_text")));
            convertView.setTag(this.holder);
        } else {
            this.holder = (MentionFriendAdapterHolder) convertView.getTag();
        }
        if (this.holder != null) {
            return convertView;
        }
        View convertView2 = this.inflater.inflate(this.forumResource.getLayoutId("mc_forum_publish_board_item"), (ViewGroup) null);
        this.holder = new MentionFriendAdapterHolder();
        this.holder.setNameText((TextView) convertView2.findViewById(this.forumResource.getViewId("mc_forum_board_name_text")));
        convertView2.setTag(this.holder);
        return convertView2;
    }
}
