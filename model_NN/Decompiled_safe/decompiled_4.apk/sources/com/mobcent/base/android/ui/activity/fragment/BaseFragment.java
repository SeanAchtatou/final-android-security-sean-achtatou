package com.mobcent.base.android.ui.activity.fragment;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCThemeResource;
import com.mobcent.forum.android.service.PermService;
import com.mobcent.forum.android.service.impl.ForumServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;

public abstract class BaseFragment extends Fragment {
    protected int AD_NUM;
    protected Activity activity;
    protected String appKey;
    protected AsyncTaskLoaderImage asyncTaskLoaderImage;
    private InputMethodManager imm;
    protected Handler mHandler;
    protected MCResource mcResource;
    protected ProgressDialog myDialog;
    protected PermService permService;
    protected MCThemeResource themeResource;
    protected View view;

    /* access modifiers changed from: protected */
    public abstract void initData();

    /* access modifiers changed from: protected */
    public abstract View initViews(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle);

    /* access modifiers changed from: protected */
    public abstract void initWidgetActions();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.activity = getActivity();
        this.appKey = new ForumServiceImpl().getForumKey(this.activity);
        this.mcResource = MCResource.getInstance(getActivity().getApplicationContext());
        this.themeResource = MCThemeResource.getInstance(getActivity().getApplicationContext(), "packageName");
        this.mHandler = new Handler();
        this.asyncTaskLoaderImage = AsyncTaskLoaderImage.getInstance(getActivity().getApplicationContext(), toString());
        initData();
        this.view = initViews(inflater, container, savedInstanceState);
        this.imm = (InputMethodManager) getActivity().getSystemService("input_method");
        initWidgetActions();
        this.AD_NUM = new Integer(this.activity.getResources().getString(this.mcResource.getStringId("mc_forum_ad_num"))).intValue();
        onTheme();
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void showProgressDialog(String str, final AsyncTask obj) {
        this.myDialog = new ProgressDialog(getActivity());
        this.myDialog.setProgressStyle(0);
        this.myDialog.setTitle(getResources().getString(this.mcResource.getStringId("mc_forum_dialog_tip")));
        this.myDialog.setMessage(getResources().getString(this.mcResource.getStringId(str)));
        this.myDialog.setIndeterminate(false);
        this.myDialog.setCancelable(true);
        this.myDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == 4) {
                    BaseFragment.this.hideProgressDialog();
                    if (!(obj == null || obj.getStatus() == AsyncTask.Status.FINISHED)) {
                        obj.cancel(true);
                    }
                }
                return true;
            }
        });
        this.myDialog.show();
    }

    /* access modifiers changed from: protected */
    public void hideProgressDialog() {
        if (this.myDialog != null) {
            this.myDialog.cancel();
        }
    }

    /* access modifiers changed from: protected */
    public void back() {
        if (this.myDialog == null || !this.myDialog.isShowing()) {
            this.activity.finish();
        } else {
            hideProgressDialog();
        }
    }

    /* access modifiers changed from: protected */
    public void onTheme() {
        try {
            this.view.findViewById(this.mcResource.getViewId("mc_forum_top_bar_box")).setBackgroundDrawable(this.themeResource.getDrawable("mc_forum_top_bar_bg"));
            this.view.findViewById(this.mcResource.getViewId("mc_forum_back_btn")).setBackgroundDrawable(this.themeResource.getDrawable("mc_forum_top_bar_button1"));
            ((TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_title_text"))).setTextColor(this.themeResource.getColor("mc_forum_tool_bar_normal_color"));
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void warnMessageById(String warnMessId) {
        Toast.makeText(this.activity, this.mcResource.getStringId(warnMessId), 0).show();
    }

    /* access modifiers changed from: protected */
    public void warnMessageByStr(String str) {
        Toast.makeText(this.activity, str, 0).show();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            this.imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 2);
        }
    }

    public void showSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            this.imm.showSoftInput(getActivity().getCurrentFocus(), 1);
        }
    }

    public void showSoftKeyboard(View view2) {
        view2.requestFocus();
        this.imm.showSoftInput(view2, 1);
    }

    public void clearNotification() {
        NotificationManager manager = (NotificationManager) getActivity().getSystemService("notification");
        manager.cancel(1);
        manager.cancel(3);
        manager.cancel(2);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        back();
        return true;
    }

    public void onStop() {
        super.onStop();
        unregBroadcastReceiver();
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void unregBroadcastReceiver() {
    }
}
