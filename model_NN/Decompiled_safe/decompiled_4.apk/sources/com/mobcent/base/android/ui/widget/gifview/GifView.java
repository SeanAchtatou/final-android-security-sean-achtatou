package com.mobcent.base.android.ui.widget.gifview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.forum.android.util.BitmapUtil;
import com.mobcent.forum.android.model.BitmapModel;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class GifView extends ImageView implements GifAction {
    private GifImageType animationType;
    private Context context;
    /* access modifiers changed from: private */
    public Bitmap currentImage;
    private boolean doOverScreen;
    private DrawThread drawThread;
    private GestureDetector gestureScanner;
    /* access modifiers changed from: private */
    public GifDecoder gifDecoder;
    /* access modifiers changed from: private */
    public GifViewListener gifViewListener;
    private String imageUrl;
    private InputStream is;
    private boolean isLikeScreen;
    /* access modifiers changed from: private */
    public boolean isRun;
    private int maxLen;
    private boolean noWidth;
    /* access modifiers changed from: private */
    public boolean pause;
    private int proportion;
    private Rect rect;
    /* access modifiers changed from: private */
    public Handler redrawHandler;
    private int showHeight;
    private int showWidth;

    public interface GifViewListener {
        void done(boolean z);

        void onSingleTap(MotionEvent motionEvent);
    }

    public enum GifImageType {
        WAIT_FINISH(0),
        SYNC_DECODER(1),
        COVER(2);
        
        final int nativeInt;

        private GifImageType(int i) {
            this.nativeInt = i;
        }
    }

    public GifView(Context context2) {
        super(context2);
        this.gifDecoder = null;
        this.currentImage = null;
        this.isRun = true;
        this.pause = false;
        this.showWidth = -1;
        this.showHeight = -1;
        this.rect = null;
        this.drawThread = null;
        this.animationType = GifImageType.SYNC_DECODER;
        this.noWidth = false;
        this.isLikeScreen = false;
        this.doOverScreen = false;
        this.proportion = 1;
        this.maxLen = -1;
        this.redrawHandler = new Handler() {
            public void handleMessage(Message msg) {
                GifView.this.invalidate();
            }
        };
        this.context = context2;
        initData();
    }

    public GifView(Context context2, AttributeSet attrs) {
        this(context2, attrs, 0);
        this.context = context2;
        initData();
    }

    public GifView(Context context2, AttributeSet attrs, int defStyle) {
        super(context2, attrs, defStyle);
        this.gifDecoder = null;
        this.currentImage = null;
        this.isRun = true;
        this.pause = false;
        this.showWidth = -1;
        this.showHeight = -1;
        this.rect = null;
        this.drawThread = null;
        this.animationType = GifImageType.SYNC_DECODER;
        this.noWidth = false;
        this.isLikeScreen = false;
        this.doOverScreen = false;
        this.proportion = 1;
        this.maxLen = -1;
        this.redrawHandler = new Handler() {
            public void handleMessage(Message msg) {
                GifView.this.invalidate();
            }
        };
        this.context = context2;
        initData();
    }

    public void setGifViewListener(GifViewListener gifViewListener2) {
        this.gifViewListener = gifViewListener2;
    }

    private void initData() {
        this.gestureScanner = new GestureDetector(new MySimpleGesture());
    }

    private void setGifDecoderImage(byte[] gif) {
        if (this.gifDecoder != null) {
            this.gifDecoder.free();
            this.gifDecoder = null;
        }
        this.isRun = true;
        this.pause = false;
        this.gifDecoder = new GifDecoder(gif, this);
        this.gifDecoder.start();
    }

    private void setGifDecoderImage(InputStream is2) {
        this.is = is2;
        if (this.gifDecoder != null) {
            this.gifDecoder.free();
            this.gifDecoder = null;
        }
        this.isRun = true;
        this.pause = false;
        this.gifDecoder = new GifDecoder(is2, this);
        this.gifDecoder.start();
    }

    public void setGifImage(byte[] gif) {
        setGifDecoderImage(gif);
    }

    public void setGifImage(InputStream is2) {
        setGifDecoderImage(is2);
    }

    public void setGifImage(String imageUrl2) {
        this.imageUrl = imageUrl2;
        try {
            setGifDecoderImage((InputStream) new URL(imageUrl2).getContent());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public void setGifImage(int resId) {
        setGifDecoderImage(getResources().openRawResource(resId));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.gifDecoder != null) {
            if (this.currentImage == null) {
                this.currentImage = this.gifDecoder.getImage();
            }
            if (this.currentImage != null) {
                int saveCount = canvas.getSaveCount();
                canvas.save();
                canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
                if (this.showWidth == -1) {
                    canvas.drawBitmap(this.currentImage, 0.0f, 0.0f, (Paint) null);
                } else {
                    if (this.isLikeScreen) {
                        setLikeScreenDimension(this.currentImage);
                    }
                    canvas.drawBitmap(this.currentImage, (Rect) null, this.rect, (Paint) null);
                }
                canvas.restoreToCount(saveCount);
            }
        }
    }

    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int w;
        int h;
        int pleft = getPaddingLeft();
        int pright = getPaddingRight();
        int ptop = getPaddingTop();
        int pbottom = getPaddingBottom();
        if (this.gifDecoder == null) {
            w = 1;
            h = 1;
        } else {
            w = this.gifDecoder.width;
            h = this.gifDecoder.height;
        }
        int w2 = Math.max(w + pleft + pright, getSuggestedMinimumWidth());
        int h2 = Math.max(h + ptop + pbottom, getSuggestedMinimumHeight());
        int widthSize = resolveSize(w2, widthMeasureSpec);
        if (widthSize <= 1) {
            this.noWidth = true;
        } else {
            this.noWidth = false;
        }
        setMeasuredDimension(widthSize, resolveSize(h2, heightMeasureSpec));
    }

    public void showCover() {
        if (this.gifDecoder != null) {
            this.pause = true;
            this.currentImage = this.gifDecoder.getImage();
            invalidate();
        }
    }

    public void showAnimation() {
        if (this.pause) {
            this.pause = false;
        }
    }

    public void setGifImageType(GifImageType type) {
        if (this.gifDecoder == null) {
            this.animationType = type;
        }
    }

    public void setShowDimension(int width, int height) {
        if (width > 0 && height > 0) {
            this.showWidth = width;
            this.showHeight = height;
            this.rect = new Rect();
            this.rect.left = 0;
            this.rect.top = 0;
            this.rect.right = width;
            this.rect.bottom = height;
        }
    }

    public void setIsLikeScreen(boolean isLikeScreen2) {
        this.isLikeScreen = isLikeScreen2;
        this.showWidth = 0;
    }

    public void setIsLikeScreen(boolean isLikeScreen2, boolean isOverScreen) {
        this.isLikeScreen = isLikeScreen2;
        this.showWidth = 0;
        this.doOverScreen = isOverScreen;
    }

    public void setIsLikeScreen(boolean isLikeScreen2, boolean isOverScreen, int proportion2) {
        this.isLikeScreen = isLikeScreen2;
        this.showWidth = 0;
        this.doOverScreen = isOverScreen;
        this.proportion = proportion2;
    }

    public void setIsLikeScreen(boolean isLikeScreen2, boolean isOverScreen, int proportion2, int maxLen2) {
        this.isLikeScreen = isLikeScreen2;
        this.showWidth = 0;
        this.doOverScreen = isOverScreen;
        this.proportion = proportion2;
        this.maxLen = maxLen2;
    }

    private void setLikeScreenDimension(Bitmap currentBitmap) {
        BitmapModel bitmapModel = BitmapUtil.getBitmapWidHeightByScreen(this.context, currentBitmap, this.doOverScreen);
        this.rect = new Rect();
        this.rect.left = 0;
        this.rect.top = 0;
        if (this.maxLen > 0 && bitmapModel.getHeight() / this.proportion > this.maxLen) {
            bitmapModel.setWidth((this.maxLen * bitmapModel.getWidth()) / bitmapModel.getHeight());
            bitmapModel.setHeight(this.maxLen);
        }
        this.rect.right = bitmapModel.getWidth() / this.proportion;
        this.rect.bottom = bitmapModel.getHeight() / this.proportion;
    }

    public void parseOk(boolean parseStatus, int frameIndex) {
        if (this.gifDecoder == null) {
            return;
        }
        if (this.gifDecoder.getFrameCount() == 0) {
            parseFail(true, true);
            return;
        }
        if (this.noWidth) {
            this.noWidth = false;
            parseFail(false, true);
        } else {
            parseFail(false, false);
        }
        if (!parseStatus) {
            return;
        }
        if (this.gifDecoder != null) {
            switch (this.animationType) {
                case WAIT_FINISH:
                    if (frameIndex != -1) {
                        return;
                    }
                    if (this.gifDecoder.getFrameCount() > 1) {
                        new DrawThread().start();
                        return;
                    } else {
                        reDraw();
                        return;
                    }
                case COVER:
                    if (frameIndex == 1) {
                        this.currentImage = this.gifDecoder.getImage();
                        reDraw();
                        return;
                    } else if (frameIndex != -1) {
                        return;
                    } else {
                        if (this.gifDecoder.getFrameCount() <= 1) {
                            reDraw();
                            return;
                        } else if (this.drawThread == null) {
                            this.drawThread = new DrawThread();
                            this.drawThread.start();
                            return;
                        } else {
                            return;
                        }
                    }
                case SYNC_DECODER:
                    if (frameIndex == 1) {
                        this.currentImage = this.gifDecoder.getImage();
                        reDraw();
                        return;
                    } else if (frameIndex == -1) {
                        reDraw();
                        return;
                    } else if (this.drawThread == null) {
                        this.drawThread = new DrawThread();
                        this.drawThread.start();
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        } else {
            Log.e(MCConstant.PIC_GIF, "parse error");
        }
    }

    private void reDraw() {
        if (this.redrawHandler != null) {
            this.redrawHandler.sendMessage(this.redrawHandler.obtainMessage());
        }
    }

    private class DrawThread extends Thread {
        private DrawThread() {
        }

        public void run() {
            if (GifView.this.gifDecoder != null) {
                while (GifView.this.isRun) {
                    if (!GifView.this.pause) {
                        GifFrame frame = GifView.this.gifDecoder.next();
                        Bitmap unused = GifView.this.currentImage = frame.image;
                        long sp = (long) frame.delay;
                        if (GifView.this.redrawHandler != null) {
                            GifView.this.redrawHandler.sendMessage(GifView.this.redrawHandler.obtainMessage());
                            SystemClock.sleep(sp);
                        } else {
                            return;
                        }
                    } else {
                        SystemClock.sleep(10);
                    }
                }
            }
        }
    }

    public void free() {
        this.isRun = false;
        this.pause = true;
        try {
            if (this.gifDecoder != null) {
                this.gifDecoder.interrupt();
                this.gifDecoder.free();
                this.gifDecoder = null;
            }
            if (this.drawThread != null) {
                this.drawThread.interrupt();
                this.drawThread = null;
            }
            if (this.currentImage != null && !this.currentImage.isRecycled()) {
                this.currentImage.recycle();
                this.currentImage = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class MySimpleGesture extends GestureDetector.SimpleOnGestureListener {
        private MySimpleGesture() {
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (GifView.this.gifViewListener == null) {
                return true;
            }
            GifView.this.gifViewListener.onSingleTap(e);
            return true;
        }
    }

    public void parseFail(boolean isDel, boolean isFail) {
        if (isDel) {
            GifCache.removeInputStream(this.is);
        }
        if (this.gifViewListener != null) {
            this.gifViewListener.done(isFail);
        }
    }

    public GifDecoder getGifDecoder() {
        return this.gifDecoder;
    }

    public void setGifDecoder(GifDecoder gifDecoder2) {
        this.gifDecoder = gifDecoder2;
    }
}
