package com.mobcent.base.android.ui.activity.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.PollItemModel;

public class MCPollSelectBar {
    private final int MAX_PROGRESS = 100;
    private TextView contentText;
    private Context context;
    private MCResource forumResource;
    /* access modifiers changed from: private */
    public boolean isSelect = false;
    private LayoutInflater layoutInflater;
    private TextView numText;
    private PollItemModel pollItem;
    private LinearLayout pollResultBox;
    private Button seclectBtn;
    /* access modifiers changed from: private */
    public SelectPollDelegate selectPoll;
    private View view;
    private ProgressBar voteRsProgress;
    private TextView voteRsText;

    public interface SelectPollDelegate {
        void onItemSelect(View view, MCPollSelectBar mCPollSelectBar, boolean z);
    }

    public MCPollSelectBar(LayoutInflater layoutInflater2, MCResource forumResource2, PollItemModel pollItem2, SelectPollDelegate selectPoll2, Context context2) {
        this.layoutInflater = layoutInflater2;
        this.selectPoll = selectPoll2;
        this.pollItem = pollItem2;
        this.forumResource = forumResource2;
        this.context = context2;
        initSelectVoteBar();
        initSelectVoteBarActions();
    }

    private void initSelectVoteBar() {
        this.view = this.layoutInflater.inflate(this.forumResource.getLayoutId("mc_forum_widget_poll_select_item"), (ViewGroup) null);
        this.seclectBtn = (Button) this.view.findViewById(this.forumResource.getViewId("mc_froum_select_btn"));
        this.contentText = (TextView) this.view.findViewById(this.forumResource.getViewId("mc_forum_poll_item_text"));
        this.voteRsProgress = (ProgressBar) this.view.findViewById(this.forumResource.getViewId("mc_forum_vote_rs_progress"));
        this.voteRsProgress.setMax(100);
        this.voteRsText = (TextView) this.view.findViewById(this.forumResource.getViewId("mc_forum_vote_rs_text"));
        this.pollResultBox = (LinearLayout) this.view.findViewById(this.forumResource.getViewId("mc_froum_poll_result_box"));
        this.numText = (TextView) this.view.findViewById(this.forumResource.getViewId("mc_forum_vote_num_lable_text"));
        this.numText.setText(this.pollItem.getNum() + "");
        this.contentText.setText(this.pollItem.getPollName());
    }

    public void updateResultView(PollItemModel pollModel) {
        if (pollModel != null) {
            this.voteRsProgress.setProgress((int) (pollModel.getRatio() * 100.0d));
            this.voteRsText.setText(pollModel.getPercent());
            this.pollResultBox.setVisibility(0);
            this.voteRsText.setText(pollModel.getTotalNum() + "(" + pollModel.getPercent() + ")");
        }
    }

    private void initSelectVoteBarActions() {
        this.view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCPollSelectBar.this.getSelect();
                MCPollSelectBar.this.selectPoll.onItemSelect(v, MCPollSelectBar.this, MCPollSelectBar.this.isSelect);
            }
        });
        this.seclectBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCPollSelectBar.this.getSelect();
                MCPollSelectBar.this.selectPoll.onItemSelect(v, MCPollSelectBar.this, MCPollSelectBar.this.isSelect);
            }
        });
    }

    /* access modifiers changed from: private */
    public void getSelect() {
        if (this.isSelect) {
            this.seclectBtn.setBackgroundResource(this.forumResource.getDrawableId("mc_forum_select1_1"));
            this.isSelect = false;
            return;
        }
        this.seclectBtn.setBackgroundResource(this.forumResource.getDrawableId("mc_forum_select1_2"));
        this.isSelect = true;
    }

    public View getView() {
        return this.view;
    }

    public boolean isSelect() {
        return this.isSelect;
    }

    public void setSelect(boolean isSelect2) {
        if (isSelect2) {
            this.seclectBtn.setBackgroundResource(this.forumResource.getDrawableId("mc_forum_select1_1"));
        } else {
            this.seclectBtn.setBackgroundResource(this.forumResource.getDrawableId("mc_forum_select1_2"));
        }
        this.isSelect = isSelect2;
    }

    public PollItemModel getPollItem() {
        return this.pollItem;
    }

    public void setPollItem(PollItemModel pollItem2) {
        this.pollItem = pollItem2;
    }
}
