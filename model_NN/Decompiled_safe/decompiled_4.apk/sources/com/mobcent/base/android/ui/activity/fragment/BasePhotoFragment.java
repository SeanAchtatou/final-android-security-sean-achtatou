package com.mobcent.base.android.ui.activity.fragment;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.util.MCLibIOUtil;
import java.io.File;

public abstract class BasePhotoFragment extends BaseFragment implements MCConstant {
    private static final String LOCAL_POSITION_DIR = (MCLibIOUtil.FS + "mobcent" + MCLibIOUtil.FS + "forum" + MCLibIOUtil.FS + PermConstant.UPLOAD + MCLibIOUtil.FS);
    protected static final int UPLOAD_IMAGE = 2;
    protected static final int UPLOAD_IMAGE_GIF = 3;
    private static final int UPLOAD_NONE = 0;
    protected File cameraFile;
    protected String cameraPath;
    protected File compressFile;
    protected String compressPath;
    protected String fileDirPath;
    protected String path = null;
    protected File selectedFile;
    protected String selectedPath;
    protected int uploadType = 0;

    /* access modifiers changed from: protected */
    public abstract void doSomethingAfterSelectedPhoto();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        String baseLocation = MCLibIOUtil.getBaseLocalLocation(this.activity);
        this.fileDirPath = baseLocation + LOCAL_POSITION_DIR;
        this.compressPath = baseLocation + LOCAL_POSITION_DIR + MCConstant.MOB_COMPRESS_FILE_NAME;
        this.cameraPath = baseLocation + LOCAL_POSITION_DIR + MCConstant.MOB_CAMERA_FILE_NAME;
        this.selectedPath = baseLocation + LOCAL_POSITION_DIR + MCConstant.MOB_SELECTED_FILE_NAME;
        if (!MCLibIOUtil.isDirExist(this.fileDirPath)) {
            MCLibIOUtil.makeDirs(this.fileDirPath);
        }
        this.cameraFile = new File(this.cameraPath);
        if (!this.cameraFile.getParentFile().exists()) {
            this.cameraFile.mkdirs();
        }
        this.selectedFile = new File(this.selectedPath);
        if (!this.selectedFile.getParentFile().exists()) {
            this.selectedFile.mkdirs();
        }
        this.compressFile = new File(this.compressPath);
        if (!this.compressFile.getParentFile().exists()) {
            this.compressFile.mkdirs();
        }
    }

    /* access modifiers changed from: protected */
    public void cameraPhotoListener() {
        clearTempFile();
        File file = new File(this.cameraPath);
        if (!file.getParentFile().exists()) {
            file.mkdirs();
        }
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", Uri.fromFile(file));
        startActivityForResult(intent, 1);
    }

    /* access modifiers changed from: protected */
    public void localPhotoListener() {
        clearTempFile();
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction("android.intent.action.GET_CONTENT");
        startActivityForResult(intent, 2);
    }

    /* access modifiers changed from: protected */
    public void clearTempFile() {
        if (this.cameraPath != null && this.compressPath != null && this.selectedPath != null) {
            if (this.cameraFile != null && this.cameraFile.exists()) {
                this.cameraFile.delete();
            }
            if (this.compressFile != null && this.compressFile.exists()) {
                this.compressFile.delete();
            }
            if (this.selectedFile != null && this.selectedFile.exists()) {
                this.selectedFile.delete();
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                this.path = this.cameraPath;
                if (!this.cameraFile.exists()) {
                    resetData();
                    return;
                }
                try {
                    getImgSucc();
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    resetData();
                    return;
                }
            case 2:
                if (data.getData() == null) {
                    resetData();
                    return;
                }
                this.path = this.selectedPath;
                String selePath = getSelectedPath(resultCode, data);
                this.path = selePath;
                if (checkPathName(selePath)) {
                    this.uploadType = 3;
                } else {
                    this.uploadType = 2;
                }
                getImgSucc();
                return;
            default:
                getImgSucc();
                return;
        }
    }

    private void resetData() {
        this.path = null;
        clearTempFile();
        warnMessageById("mc_forum_user_photo_select_error");
    }

    private String getSelectedPath(int resultCode, Intent data) {
        Activity activity = this.activity;
        if (resultCode != -1) {
            return null;
        }
        Cursor cursor = this.activity.managedQuery(data.getData(), new String[]{"_data"}, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private boolean checkPathName(String imgPath) {
        if (imgPath == null || !imgPath.endsWith(".gif")) {
            return false;
        }
        return true;
    }

    private void getImgSucc() {
        if (this.path == this.cameraPath) {
            if (!this.cameraFile.exists()) {
                resetData();
            }
        } else if (this.path == this.selectedPath && !this.selectedFile.exists()) {
            resetData();
        }
        if (this.path != null) {
            doSomethingAfterSelectedPhoto();
        }
    }
}
