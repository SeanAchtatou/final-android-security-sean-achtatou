package com.mobcent.base.android.ui.widget.scaleview;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

public class ScaleViewPager extends ViewPager {
    public String TAG;
    private boolean isScroll;
    private boolean isScrollLeftEnable;
    private boolean isScrollRightEnable;
    public float touchSlop;

    public boolean isScroll() {
        return this.isScroll;
    }

    public void setScroll(boolean isScroll2) {
        this.isScroll = isScroll2;
    }

    public boolean isScrollLeftEnable() {
        return this.isScrollLeftEnable;
    }

    public void setScrollLeftEnable(boolean isScrollLeftEnable2) {
        this.isScrollLeftEnable = isScrollLeftEnable2;
    }

    public boolean isScrollRightEnable() {
        return this.isScrollRightEnable;
    }

    public void setScrollRightEnable(boolean isScrollRightEnable2) {
        this.isScrollRightEnable = isScrollRightEnable2;
    }

    public ScaleViewPager(Context context) {
        this(context, null);
    }

    public ScaleViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.TAG = "MyViewPager";
        this.isScrollLeftEnable = true;
        this.isScrollRightEnable = true;
        init(context);
    }

    private void init(Context context) {
        this.touchSlop = (float) ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public boolean onTouchEvent(MotionEvent ev) {
        try {
            if (isScroll()) {
                return super.onTouchEvent(ev);
            }
            return true;
        } catch (Exception e) {
            return true;
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        try {
            if (isScroll()) {
                return super.onInterceptTouchEvent(event);
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
