package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import com.mobcent.base.android.constant.MCConstant;

public class WebViewActivity extends BaseActivity implements MCConstant {
    private String webUrl;
    private ImageButton webviewBackBtn;
    /* access modifiers changed from: private */
    public WebView webviewBrowser;
    private ImageButton webviewCloseBtn;
    private ImageButton webviewForwardBtn;
    /* access modifiers changed from: private */
    public ProgressBar webviewProgressbar;
    private ImageButton webviewRefreshBtn;

    /* access modifiers changed from: protected */
    public void initData() {
        this.webUrl = getIntent().getStringExtra("webViewUrl");
        if (this.webUrl == null) {
            warnMessageById("mc_forum_webview_url_error");
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_webview_activity"));
        this.webviewProgressbar = (ProgressBar) findViewById(this.resource.getViewId("mc_forum_webview_progressbar"));
        this.webviewRefreshBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_webview_refresh_btn"));
        this.webviewCloseBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_webview_close_btn"));
        this.webviewForwardBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_webview_forward_btn"));
        this.webviewBackBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_webview_back_btn"));
        this.webviewBrowser = (WebView) findViewById(this.resource.getViewId("mc_forum_webview_browser"));
        this.webviewBrowser.getSettings().setJavaScriptEnabled(true);
        this.webviewBrowser.getSettings().setPluginsEnabled(true);
        this.webviewBrowser.getSettings().setSupportZoom(true);
        this.webviewBrowser.getSettings().setBuiltInZoomControls(true);
        this.webviewBrowser.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        this.webviewBrowser.setWebViewClient(new MyWebViewClient());
        this.webviewBrowser.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                WebViewActivity.this.webviewProgressbar.setProgress(progress);
            }
        });
        this.webviewBrowser.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                WebViewActivity.this.downApk(url);
            }
        });
        this.webviewBrowser.loadUrl(this.webUrl);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.webviewForwardBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (WebViewActivity.this.webviewBrowser != null && WebViewActivity.this.webviewBrowser.canGoForward()) {
                    WebViewActivity.this.webviewBrowser.goForward();
                }
            }
        });
        this.webviewBackBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (WebViewActivity.this.webviewBrowser != null && WebViewActivity.this.webviewBrowser.canGoBack()) {
                    WebViewActivity.this.webviewBrowser.goBack();
                }
            }
        });
        this.webviewCloseBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebViewActivity.this.webviewBrowser.setWebViewClient(new WebViewClient() {
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    }

                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        return false;
                    }
                });
                WebViewActivity.this.finish();
            }
        });
        this.webviewRefreshBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebViewActivity.this.webviewBrowser.reload();
            }
        });
    }

    /* access modifiers changed from: private */
    public void downApk(String downurl) {
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.setData(Uri.parse(downurl));
            intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
            startActivity(intent);
        } catch (Exception e) {
            try {
                Intent intent2 = new Intent();
                intent2.setAction("android.intent.action.VIEW");
                intent2.setData(Uri.parse(downurl));
                startActivity(intent2);
            } catch (Exception e2) {
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.webviewBrowser == null || !this.webviewBrowser.canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        this.webviewBrowser.goBack();
        return true;
    }

    final class MyWebViewClient extends WebViewClient {
        MyWebViewClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }

    final class InJavaScriptLocalObj {
        InJavaScriptLocalObj() {
        }

        public void showSource(String html) {
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        this.webviewBrowser.stopLoading();
        this.webviewBrowser.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.webviewBrowser.setVisibility(8);
        this.webviewBrowser.loadUrl("about:blank");
        this.webviewBrowser.stopLoading();
        this.webviewBrowser.clearView();
        this.webviewBrowser.freeMemory();
        this.webviewBrowser.destroy();
    }
}
