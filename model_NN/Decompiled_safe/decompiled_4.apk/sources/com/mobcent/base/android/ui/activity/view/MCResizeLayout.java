package com.mobcent.base.android.ui.activity.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class MCResizeLayout extends RelativeLayout {
    private OnResizeListener mListener;

    public interface OnResizeListener {
        void OnResize(int i, int i2, int i3, int i4);
    }

    public void setOnResizeListener(OnResizeListener l) {
        this.mListener = l;
    }

    public MCResizeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (this.mListener != null) {
            this.mListener.OnResize(w, h, oldw, oldh);
        }
    }
}
