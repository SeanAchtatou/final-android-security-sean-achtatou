package com.mobcent.base.android.ui.activity.helper;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.AboutActivity;
import com.mobcent.base.android.ui.activity.PicTopicListActivity;
import com.mobcent.base.android.ui.activity.PlazaMyActivity;
import com.mobcent.base.android.ui.activity.PostsActivity;
import com.mobcent.base.android.ui.activity.ReplyTopicActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.BoardListFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.HomeTopicFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.MessageFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.SettingFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserHomeFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserLoginFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserTopicFragmentActivity;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.android.ui.activity.view.MCPublishMenuDialog;
import com.mobcent.base.forum.android.util.MCPhoneUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCThemeResource;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.PostsNoticeModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.ForumService;
import com.mobcent.forum.android.service.PermService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.ForumServiceImpl;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.PhoneUtil;
import com.mobcent.plaza.android.service.model.PlazaAppModel;
import com.mobcent.plaza.android.service.model.SearchModel;
import com.mobcent.plaza.android.ui.activity.IntentPlazaModel;
import com.mobcent.plaza.android.ui.activity.delegate.PlazaConfig;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class DefaultForumConfigImpl extends ForumConfig implements MCConstant {
    public void initNav(final Activity activity, final MCResource resource) {
        try {
            final PermService permService = new PermServiceImpl(activity);
            ((ImageView) activity.findViewById(resource.getViewId("mc_forum_nav_home_img"))).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
                        activity.startActivity(new Intent(activity, HomeTopicFragmentActivity.class));
                        return;
                    }
                    DefaultForumConfigImpl.this.warnMessageByStr(activity, resource.getString("mc_forum_permission_cannot_visit_forum"));
                }
            });
            ((ImageView) activity.findViewById(resource.getViewId("mc_forum_nav_recommend_img"))).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) != 1) {
                        DefaultForumConfigImpl.this.warnMessageByStr(activity, resource.getString("mc_forum_permission_cannot_visit_forum"));
                    } else if (!(activity instanceof BoardListFragmentActivity)) {
                        activity.startActivity(new Intent(activity, BoardListFragmentActivity.class));
                    }
                }
            });
            ((ImageView) activity.findViewById(resource.getViewId("mc_forum_nav_pic_img"))).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) != 1) {
                        DefaultForumConfigImpl.this.warnMessageByStr(activity, resource.getString("mc_forum_permission_cannot_visit_forum"));
                    } else if (!(activity instanceof PicTopicListActivity)) {
                        activity.startActivity(new Intent(activity, PicTopicListActivity.class));
                    }
                }
            });
            ((ImageView) activity.findViewById(resource.getViewId("mc_forum_nav_msg_img"))).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) != 1) {
                        DefaultForumConfigImpl.this.warnMessageByStr(activity, resource.getString("mc_forum_permission_cannot_visit_forum"));
                    } else if (new UserServiceImpl(activity).isLogin()) {
                        if (!(activity instanceof MessageFragmentActivity)) {
                            activity.startActivity(new Intent(activity, MessageFragmentActivity.class));
                        }
                    } else if (!(activity instanceof UserLoginFragmentActivity)) {
                        activity.startActivity(new Intent(activity, UserLoginFragmentActivity.class));
                    }
                }
            });
            ((ImageView) activity.findViewById(resource.getViewId("mc_forum_nav_plaza_img"))).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
                        PlazaConfig.getInstance().setPlazaDelegate(new PlazaSearchConfigImpl());
                        Intent plazaIntent = new Intent(activity, PlazaMyActivity.class);
                        ForumService forumService = new ForumServiceImpl();
                        UserService userService = new UserServiceImpl(activity);
                        SharedPreferencesDB shareDB = SharedPreferencesDB.getInstance(activity);
                        IntentPlazaModel intentModel = new IntentPlazaModel();
                        intentModel.setForumId(shareDB.getForumid());
                        intentModel.setForumKey(forumService.getForumKey(activity));
                        intentModel.setUserId(userService.getLoginUserId());
                        intentModel.setSearchTypes(new int[]{1});
                        plazaIntent.putExtra("plazaIntentModel", intentModel);
                        activity.startActivity(plazaIntent);
                        return;
                    }
                    DefaultForumConfigImpl.this.warnMessageByStr(activity, resource.getString("mc_forum_permission_cannot_visit_forum"));
                }
            });
        } catch (Exception e) {
        }
    }

    public void initNav(Activity activity, MCResource resource, MCThemeResource themeResource) {
        try {
            ImageView navHomeImg = (ImageView) activity.findViewById(resource.getViewId("mc_forum_nav_home_img"));
            ImageView navBoardImg = (ImageView) activity.findViewById(resource.getViewId("mc_forum_nav_recommend_img"));
            ImageView navPicImg = (ImageView) activity.findViewById(resource.getViewId("mc_forum_nav_pic_img"));
            ImageView navMsgImg = (ImageView) activity.findViewById(resource.getViewId("mc_forum_nav_msg_img"));
            ImageView navPublishImg = (ImageView) activity.findViewById(resource.getViewId("mc_forum_nav_publish_img"));
            ImageView navPlazaImg = (ImageView) activity.findViewById(resource.getViewId("mc_forum_nav_plaza_img"));
            navHomeImg.setImageDrawable(themeResource.getDrawable("mc_forum_main_bar2_button1"));
            if (activity instanceof HomeTopicFragmentActivity) {
                navHomeImg.setImageDrawable(themeResource.getDrawable("mc_forum_main_bar2_button1_d"));
            }
            navBoardImg.setImageDrawable(themeResource.getDrawable("mc_forum_main_bar2_button14"));
            if (activity instanceof BoardListFragmentActivity) {
                navBoardImg.setImageDrawable(themeResource.getDrawable("mc_forum_main_bar2_button14_d"));
            }
            navPicImg.setImageDrawable(themeResource.getDrawable("mc_forum_main_bar2_button8"));
            if (activity instanceof PicTopicListActivity) {
                navPicImg.setImageDrawable(themeResource.getDrawable("mc_forum_main_bar2_button8_d"));
            }
            navMsgImg.setImageDrawable(themeResource.getDrawable("mc_forum_main_bar2_button4"));
            if (activity instanceof MessageFragmentActivity) {
                navMsgImg.setImageDrawable(themeResource.getDrawable("mc_forum_main_bar2_button4_d"));
            }
            final PermServiceImpl permServiceImpl = new PermServiceImpl(activity);
            final PermServiceImpl permServiceImpl2 = permServiceImpl;
            final Activity activity2 = activity;
            final MCResource mCResource = resource;
            navHomeImg.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (permServiceImpl2.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
                        activity2.startActivity(new Intent(activity2, HomeTopicFragmentActivity.class));
                        return;
                    }
                    DefaultForumConfigImpl.this.warnMessageByStr(activity2, mCResource.getString("mc_forum_permission_cannot_visit_forum"));
                }
            });
            final PermServiceImpl permServiceImpl3 = permServiceImpl;
            final Activity activity3 = activity;
            final MCResource mCResource2 = resource;
            navBoardImg.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (permServiceImpl3.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) != 1) {
                        DefaultForumConfigImpl.this.warnMessageByStr(activity3, mCResource2.getString("mc_forum_permission_cannot_visit_forum"));
                    } else if (!(activity3 instanceof BoardListFragmentActivity)) {
                        activity3.startActivity(new Intent(activity3, BoardListFragmentActivity.class));
                    }
                }
            });
            final PermServiceImpl permServiceImpl4 = permServiceImpl;
            final Activity activity4 = activity;
            final MCResource mCResource3 = resource;
            navPicImg.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (permServiceImpl4.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) != 1) {
                        DefaultForumConfigImpl.this.warnMessageByStr(activity4, mCResource3.getString("mc_forum_permission_cannot_visit_forum"));
                    } else if (!(activity4 instanceof PicTopicListActivity)) {
                        activity4.startActivity(new Intent(activity4, PicTopicListActivity.class));
                    }
                }
            });
            final PermServiceImpl permServiceImpl5 = permServiceImpl;
            final Activity activity5 = activity;
            final MCResource mCResource4 = resource;
            navMsgImg.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (permServiceImpl5.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) != 1) {
                        DefaultForumConfigImpl.this.warnMessageByStr(activity5, mCResource4.getString("mc_forum_permission_cannot_visit_forum"));
                    } else if (new UserServiceImpl(activity5).isLogin()) {
                        if (!(activity5 instanceof MessageFragmentActivity)) {
                            activity5.startActivity(new Intent(activity5, MessageFragmentActivity.class));
                        }
                    } else if (!(activity5 instanceof UserLoginFragmentActivity)) {
                        activity5.startActivity(new Intent(activity5, UserLoginFragmentActivity.class));
                    }
                }
            });
            final MCPublishMenuDialog mCPublishMenuDialog = new MCPublishMenuDialog(activity, resource.getStyleId("mc_forum_home_publish_dialog"), navPublishImg);
            Window win = mCPublishMenuDialog.getWindow();
            int displayHeight = PhoneUtil.getDisplayHeight(activity.getApplicationContext());
            int diffHeight = MCPhoneUtil.getRawSize(activity.getApplicationContext(), 1, (float) 120);
            WindowManager.LayoutParams params = new WindowManager.LayoutParams();
            params.y = ((displayHeight - MCPhoneUtil.getRawSize(activity.getApplicationContext(), 1, 20.0f)) / 2) - diffHeight;
            params.dimAmount = 0.3f;
            win.setAttributes(params);
            win.addFlags(2);
            final Activity activity6 = activity;
            final MCResource mCResource5 = resource;
            navPublishImg.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (!LoginInterceptor.doInterceptorByDialog(activity6, mCResource5, null, null)) {
                        return;
                    }
                    if (permServiceImpl.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) != 1) {
                        DefaultForumConfigImpl.this.warnMessageByStr(activity6, mCResource5.getString("mc_forum_permission_cannot_visit_forum"));
                    } else if (permServiceImpl.getPermNum(PermConstant.USER_GROUP, PermConstant.POST, -1) == 0) {
                        DefaultForumConfigImpl.this.warnMessageByStr(activity6, mCResource5.getString("mc_forum_permission_cannot_post_toipc"));
                    } else if (mCPublishMenuDialog.isShowing()) {
                        mCPublishMenuDialog.dismiss();
                    } else {
                        mCPublishMenuDialog.show();
                    }
                }
            });
            final PermServiceImpl permServiceImpl6 = permServiceImpl;
            final Activity activity7 = activity;
            final MCResource mCResource6 = resource;
            navPlazaImg.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (permServiceImpl6.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
                        PlazaConfig.getInstance().setPlazaDelegate(new PlazaSearchConfigImpl());
                        Intent plazaIntent = new Intent(activity7, PlazaMyActivity.class);
                        ForumService forumService = new ForumServiceImpl();
                        UserService userService = new UserServiceImpl(activity7);
                        SharedPreferencesDB shareDB = SharedPreferencesDB.getInstance(activity7);
                        IntentPlazaModel intentModel = new IntentPlazaModel();
                        intentModel.setForumId(shareDB.getForumid());
                        intentModel.setForumKey(forumService.getForumKey(activity7));
                        intentModel.setUserId(userService.getLoginUserId());
                        intentModel.setSearchTypes(new int[]{1});
                        plazaIntent.putExtra("plazaIntentModel", intentModel);
                        activity7.startActivity(plazaIntent);
                        return;
                    }
                    DefaultForumConfigImpl.this.warnMessageByStr(activity7, mCResource6.getString("mc_forum_permission_cannot_visit_forum"));
                }
            });
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public void warnMessageByStr(Activity activity, String str) {
        Toast.makeText(activity, str, 0).show();
    }

    public void gotoUserInfo(Activity activity, MCResource resource, long userId) {
        HashMap<String, Serializable> param = new HashMap<>();
        param.put("userId", Long.valueOf(userId));
        if (LoginInterceptor.doInterceptorByDialog(activity, resource, UserHomeFragmentActivity.class, param)) {
            Intent intent = new Intent(activity, UserHomeFragmentActivity.class);
            intent.putExtra("userId", userId);
            activity.startActivity(intent);
        }
    }

    public void onPostsClick(Activity activity, MCResource resource, View v, TopicModel topicModel, String boardName) {
        Intent intent = new Intent(activity, PostsActivity.class);
        intent.putExtra("boardId", topicModel.getBoardId());
        intent.putExtra("boardName", boardName);
        intent.putExtra("topicId", topicModel.getTopicId());
        intent.putExtra(MCConstant.TOPIC_USER_ID, topicModel.getUserId());
        intent.putExtra("baseUrl", topicModel.getBaseUrl());
        intent.putExtra(MCConstant.THUMBNAIL_IMAGE_URL, topicModel.getPicPath());
        intent.putExtra("type", topicModel.getType());
        intent.putExtra(MCConstant.TOP, topicModel.getTop());
        intent.putExtra(MCConstant.ESSENCE, topicModel.getEssence());
        intent.putExtra(MCConstant.CLOSE, topicModel.getStatus());
        activity.startActivity(intent);
    }

    public void onPostsClick(Activity activity, MCResource resource, View v, PostsNoticeModel model) {
        Intent intent = new Intent(activity, PostsActivity.class);
        intent.putExtra("topicId", model.getTopicId());
        intent.putExtra("boardId", model.getBoardId());
        activity.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onReplyClick(Activity activity, MCResource resource, View v, PostsNoticeModel model, long pageFrom) {
        Intent intent = new Intent(activity, ReplyTopicActivity.class);
        intent.putExtra("boardId", model.getBoardId());
        intent.putExtra("topicId", model.getTopicId());
        intent.putExtra("toReplyId", model.getReplyPostsId());
        intent.putExtra(MCConstant.IS_QUOTE_TOPIC, true);
        intent.putExtra("pageFrom", pageFrom);
        intent.putExtra(MCConstant.POSTS_USER_LIST, new ArrayList());
        activity.startActivity(intent);
    }

    public Intent onBeatClickListener(Context context, int messageTab) {
        Intent intent = new Intent(context, MessageFragmentActivity.class);
        intent.putExtra(MCConstant.MESSAGE_TAP, messageTab);
        return intent;
    }

    public void onMessageClick(Activity activity) {
        activity.startActivity(new Intent(activity, MessageFragmentActivity.class));
    }

    public void onLogoutClick(Activity activity) {
        MCForumHelper.LogoutForum(activity);
        MCForumHelper.launchForum(activity);
        activity.finish();
    }

    public boolean logoutApp() {
        return true;
    }

    public void clearNotification(Activity activity) {
        NotificationManager manager = (NotificationManager) activity.getSystemService("notification");
        manager.cancel(1);
        manager.cancel(3);
        manager.cancel(2);
    }

    public void onTopicClick(Activity activity, MCResource arg1, View arg2, int TopicTab, long userId, UserInfoModel infoModel) {
        Intent intent = new Intent(activity, UserTopicFragmentActivity.class);
        intent.putExtra(MCConstant.USER_TOPIC, TopicTab);
        intent.putExtra("userId", userId);
        intent.putExtra("nickname", infoModel.getNickname());
        activity.startActivity(intent);
    }

    private class PlazaSearchConfigImpl implements PlazaConfig.PlazaDelegate {
        private PlazaSearchConfigImpl() {
        }

        public long getUserId(Context context) {
            if (!LoginInterceptor.doInterceptor(context, null)) {
                return 0;
            }
            return new UserServiceImpl(context).getLoginUserId();
        }

        public void onAppItemClick(Context arg0, PlazaAppModel arg1) {
        }

        public boolean onPlazaBackPressed(Context arg0) {
            return false;
        }

        public void onSearchItemClick(Context context, SearchModel searchModel) {
            System.out.println("getBaikeType:" + searchModel.getBaikeType());
            if (searchModel.getBaikeType() == 1) {
                Intent intent = new Intent(context, PostsActivity.class);
                intent.putExtra("boardId", searchModel.getBoardId());
                intent.putExtra("topicId", searchModel.getTopicId());
                intent.putExtra(MCConstant.TOPIC_USER_ID, searchModel.getUserId());
                context.startActivity(intent);
            }
        }

        public void onPersonalClick(Context context) {
            UserService userService = new UserServiceImpl(context);
            boolean isLogin = userService.isLogin();
            long loginUserId = userService.getLoginUserId();
            if (isLogin) {
                if (!(context instanceof UserHomeFragmentActivity) || ((UserHomeFragmentActivity) context).getUserId() != loginUserId) {
                    Intent intent = new Intent(context, UserHomeFragmentActivity.class);
                    intent.putExtra("userId", loginUserId);
                    context.startActivity(intent);
                }
            } else if (!(context instanceof UserLoginFragmentActivity)) {
                context.startActivity(new Intent(context, UserLoginFragmentActivity.class));
            }
        }

        public void onSetClick(Context context) {
            context.startActivity(new Intent(context, SettingFragmentActivity.class));
        }

        public void onAboutClick(Context context) {
            context.startActivity(new Intent(context, AboutActivity.class));
        }
    }
}
