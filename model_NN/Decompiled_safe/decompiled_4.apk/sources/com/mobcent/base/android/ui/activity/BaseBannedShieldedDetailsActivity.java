package com.mobcent.base.android.ui.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.PublishTopicBoardListAdapter;
import com.mobcent.base.android.ui.activity.adapter.WheelListAdapter;
import com.mobcent.base.android.ui.activity.widget.wheel.OnWheelChangedListener;
import com.mobcent.base.android.ui.activity.widget.wheel.OnWheelScrollListener;
import com.mobcent.base.android.ui.activity.widget.wheel.WheelView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.BoardService;
import com.mobcent.forum.android.service.ModeratorService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.BoardServiceImpl;
import com.mobcent.forum.android.service.impl.ModeratorServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public abstract class BaseBannedShieldedDetailsActivity extends BaseActivity implements MCConstant {
    private ArrayAdapter<String> adapter;
    protected Button backBtn;
    protected RelativeLayout boardBox;
    protected TextView boardDetailText;
    protected int boardId;
    protected ImageButton boardImgBtn;
    protected List<BoardModel> boardList;
    /* access modifiers changed from: private */
    public PublishTopicBoardListAdapter boardListAdapter;
    private GetBoardListAsyncTask boardListAsyncTask;
    protected ListView boardListView;
    /* access modifiers changed from: private */
    public BoardService boardService;
    protected TextView boardTitleText;
    private OnWheelChangedListener changedListener = new OnWheelChangedListener() {
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            if (!BaseBannedShieldedDetailsActivity.this.wheelScrolled) {
                BaseBannedShieldedDetailsActivity.this.updateStatus();
            }
        }
    };
    private String[] content;
    private int currentRoleNum = 2;
    protected long currentUserId;
    protected EditText detailDescriptionEdit;
    private String[] endTimeStr;
    /* access modifiers changed from: private */
    public boolean isShowBoardBox = false;
    /* access modifiers changed from: private */
    public boolean isShowTopicTypeBox = false;
    /* access modifiers changed from: private */
    public boolean isShowWheel = false;
    /* access modifiers changed from: private */
    public ProgressDialog loadBoardDialog;
    private ModeratorService moderatorService;
    protected EditText reasonEdit;
    protected ImageButton reasonImgBtn;
    protected ListView reasonListview;
    protected Button removeBtn;
    protected ScrollView scrollView;
    OnWheelScrollListener scrolledListener = new OnWheelScrollListener() {
        public void onScrollingStarted(WheelView wheel) {
            boolean unused = BaseBannedShieldedDetailsActivity.this.wheelScrolled = true;
        }

        public void onScrollingFinished(WheelView wheel) {
            boolean unused = BaseBannedShieldedDetailsActivity.this.wheelScrolled = false;
            BaseBannedShieldedDetailsActivity.this.updateStatus();
        }
    };
    protected List<BoardModel> selectBoardList;
    protected TextView selectBoardText;
    protected Button submitBtn;
    protected String sureStr = "";
    protected Button timeBtn;
    protected TextView timeText;
    protected TextView titleText;
    protected LinearLayout transparentBox;
    private GetUserInfoModeAsyncTask userInfoModeAsyncTask;
    protected UserService userService;
    protected LinearLayout wheelBox;
    /* access modifiers changed from: private */
    public boolean wheelScrolled = false;

    public abstract List<BoardModel> getBoardInfo(UserInfoModel userInfoModel, List<BoardModel> list);

    public abstract UserInfoModel getUserInfoModel();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.boardId < 0) {
            this.userInfoModeAsyncTask = new GetUserInfoModeAsyncTask();
            this.userInfoModeAsyncTask.execute(new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.content = new String[]{getString(this.resource.getStringId("mc_forum_report_adult_content")), getString(this.resource.getStringId("mc_forum_polity_sensi_content")), getString(this.resource.getStringId("mc_forum_vulgar_speech")), getString(this.resource.getStringId("mc_forum_other_reason"))};
        this.endTimeStr = getResources().getStringArray(this.resource.getArrayId("mc_forum_end_time_array"));
        this.adapter = new ArrayAdapter<>(this, this.resource.getLayoutId("mc_forum_content_item"), this.content);
        this.userService = new UserServiceImpl(this);
        this.moderatorService = new ModeratorServiceImpl(this);
        this.boardService = new BoardServiceImpl(this);
        this.boardList = new ArrayList();
        this.selectBoardList = new ArrayList();
        this.boardListAdapter = new PublishTopicBoardListAdapter(this, this.boardList);
        this.currentRoleNum = this.userService.getRoleNum();
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_banned_shielded_activity"));
        this.titleText = (TextView) findViewById(this.resource.getViewId("mc_forum_title_text"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.transparentBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_transparent_box"));
        this.reasonListview = (ListView) findViewById(this.resource.getViewId("mc_forum_reason_detail_list"));
        this.boardListView = (ListView) findViewById(this.resource.getViewId("mc_forum_board_detail_list"));
        this.reasonEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_reason_edit"));
        this.selectBoardText = (TextView) findViewById(this.resource.getViewId("mc_forum_board_text"));
        this.reasonImgBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_reason_imgBtn"));
        this.boardImgBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_board_imgBtn"));
        this.timeBtn = (Button) findViewById(this.resource.getViewId("mc_forum_end_time_btn"));
        this.wheelBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_wheel_box"));
        this.removeBtn = (Button) findViewById(this.resource.getViewId("mc_forum_remove_btn"));
        this.detailDescriptionEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_detail_description_edit"));
        this.submitBtn = (Button) findViewById(this.resource.getViewId("mc_forum_submit_btn"));
        this.timeText = (TextView) findViewById(this.resource.getViewId("mc_forum_time_text"));
        this.boardTitleText = (TextView) findViewById(this.resource.getViewId("mc_forum_board_title_text"));
        this.boardDetailText = (TextView) findViewById(this.resource.getViewId("mc_forum_board_detail_text"));
        this.boardBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_board_box"));
        this.scrollView = (ScrollView) findViewById(this.resource.getViewId("mc_forum_scroll_view"));
        this.loadBoardDialog = new ProgressDialog(this);
        this.loadBoardDialog.setMessage(getResources().getString(this.resource.getStringId("mc_forum_loading_board")));
        if (this.currentRoleNum >= 8) {
            showBoardListBoxEvent();
            try {
                if (this.boardList != null) {
                    this.boardList.add(0, createTotalAnnounce());
                }
            } catch (Exception e) {
                this.boardList = new ArrayList();
                this.boardList.add(0, createTotalAnnounce());
            }
            this.boardListAdapter.setBoardList(this.boardList);
        } else if (this.currentRoleNum == 4) {
            this.boardList = this.moderatorService.getModeratorBoardList(new UserServiceImpl(this).getLoginUserId(), false).getBoardList();
            this.boardList.add(0, createTotalAnnounce());
            this.boardListAdapter.setBoardList(this.boardList);
        }
        initWheel(this.resource.getViewId("mc_forum_wheel1"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.reasonListview.setAdapter((ListAdapter) this.adapter);
        this.boardListView.setAdapter((ListAdapter) this.boardListAdapter);
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaseBannedShieldedDetailsActivity.this.finish();
            }
        });
        this.reasonImgBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!BaseBannedShieldedDetailsActivity.this.isShowTopicTypeBox) {
                    BaseBannedShieldedDetailsActivity.this.showDropDownBox(true);
                    boolean unused = BaseBannedShieldedDetailsActivity.this.isShowTopicTypeBox = true;
                    return;
                }
                BaseBannedShieldedDetailsActivity.this.showDropDownBox(false);
                boolean unused2 = BaseBannedShieldedDetailsActivity.this.isShowTopicTypeBox = false;
            }
        });
        this.boardImgBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!BaseBannedShieldedDetailsActivity.this.isShowBoardBox) {
                    BaseBannedShieldedDetailsActivity.this.showBoardListBox(true);
                    boolean unused = BaseBannedShieldedDetailsActivity.this.isShowBoardBox = true;
                    return;
                }
                BaseBannedShieldedDetailsActivity.this.showBoardListBox(false);
                boolean unused2 = BaseBannedShieldedDetailsActivity.this.isShowBoardBox = false;
            }
        });
        this.reasonListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX WARN: Type inference failed for: r0v1, types: [android.widget.Adapter] */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void onItemClick(android.widget.AdapterView<?> r4, android.view.View r5, int r6, long r7) {
                /*
                    r3 = this;
                    r2 = 0
                    com.mobcent.base.android.ui.activity.BaseBannedShieldedDetailsActivity r0 = com.mobcent.base.android.ui.activity.BaseBannedShieldedDetailsActivity.this
                    android.widget.EditText r1 = r0.reasonEdit
                    android.widget.Adapter r0 = r4.getAdapter()
                    java.lang.Object r0 = r0.getItem(r6)
                    java.lang.String r0 = (java.lang.String) r0
                    r1.setText(r0)
                    com.mobcent.base.android.ui.activity.BaseBannedShieldedDetailsActivity r0 = com.mobcent.base.android.ui.activity.BaseBannedShieldedDetailsActivity.this
                    r0.showDropDownBox(r2)
                    com.mobcent.base.android.ui.activity.BaseBannedShieldedDetailsActivity r0 = com.mobcent.base.android.ui.activity.BaseBannedShieldedDetailsActivity.this
                    boolean unused = r0.isShowTopicTypeBox = r2
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.mobcent.base.android.ui.activity.BaseBannedShieldedDetailsActivity.AnonymousClass4.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
            }
        });
        this.boardListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                BoardModel boardModel = BaseBannedShieldedDetailsActivity.this.boardList.get(position);
                if (boardModel.getBoardId() != 0) {
                    for (int i = 0; i < BaseBannedShieldedDetailsActivity.this.selectBoardList.size(); i++) {
                        if (BaseBannedShieldedDetailsActivity.this.selectBoardList.get(i).getBoardId() == 0 && BaseBannedShieldedDetailsActivity.this.selectBoardList.get(i).isSelected()) {
                            BaseBannedShieldedDetailsActivity.this.selectBoardList.get(i).setSelected(false);
                            BaseBannedShieldedDetailsActivity.this.selectBoardList.remove(i);
                        }
                    }
                    if (boardModel.isSelected()) {
                        boardModel.setSelected(false);
                        BaseBannedShieldedDetailsActivity.this.selectBoardList.remove(boardModel);
                        BaseBannedShieldedDetailsActivity.this.boardDetailText.setText(BaseBannedShieldedDetailsActivity.this.getBoardDetailText(BaseBannedShieldedDetailsActivity.this.selectBoardList));
                    } else {
                        boardModel.setSelected(true);
                        BaseBannedShieldedDetailsActivity.this.selectBoardList.add(boardModel);
                        BaseBannedShieldedDetailsActivity.this.boardDetailText.setText(BaseBannedShieldedDetailsActivity.this.getBoardDetailText(BaseBannedShieldedDetailsActivity.this.selectBoardList));
                    }
                } else if (boardModel.isSelected()) {
                    for (int i2 = 0; i2 < BaseBannedShieldedDetailsActivity.this.selectBoardList.size(); i2++) {
                        BaseBannedShieldedDetailsActivity.this.selectBoardList.get(i2).setSelected(false);
                    }
                    BaseBannedShieldedDetailsActivity.this.selectBoardList.clear();
                    BaseBannedShieldedDetailsActivity.this.boardDetailText.setText(BaseBannedShieldedDetailsActivity.this.getBoardDetailText(BaseBannedShieldedDetailsActivity.this.selectBoardList));
                } else {
                    BaseBannedShieldedDetailsActivity.this.selectBoardList.clear();
                    BaseBannedShieldedDetailsActivity.this.selectBoardList.addAll(BaseBannedShieldedDetailsActivity.this.boardList);
                    for (int i3 = 0; i3 < BaseBannedShieldedDetailsActivity.this.selectBoardList.size(); i3++) {
                        BaseBannedShieldedDetailsActivity.this.selectBoardList.get(i3).setSelected(true);
                    }
                    BaseBannedShieldedDetailsActivity.this.boardDetailText.setText(BaseBannedShieldedDetailsActivity.this.getBoardDetailText(BaseBannedShieldedDetailsActivity.this.selectBoardList));
                }
                BaseBannedShieldedDetailsActivity.this.boardListAdapter.notifyDataSetChanged();
            }
        });
        this.transparentBox.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (BaseBannedShieldedDetailsActivity.this.transparentBox.getVisibility() != 0) {
                    return false;
                }
                BaseBannedShieldedDetailsActivity.this.showDropDownBox(false);
                BaseBannedShieldedDetailsActivity.this.showBoardListBox(false);
                return false;
            }
        });
        this.timeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!BaseBannedShieldedDetailsActivity.this.isShowWheel) {
                    BaseBannedShieldedDetailsActivity.this.wheelBox.setVisibility(0);
                    BaseBannedShieldedDetailsActivity.this.removeBtn.setVisibility(0);
                    boolean unused = BaseBannedShieldedDetailsActivity.this.isShowWheel = true;
                    BaseBannedShieldedDetailsActivity.this.transparentBox.setVisibility(8);
                    return;
                }
                BaseBannedShieldedDetailsActivity.this.wheelBox.setVisibility(8);
                BaseBannedShieldedDetailsActivity.this.removeBtn.setVisibility(8);
                boolean unused2 = BaseBannedShieldedDetailsActivity.this.isShowWheel = false;
                BaseBannedShieldedDetailsActivity.this.transparentBox.setVisibility(0);
            }
        });
        this.removeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaseBannedShieldedDetailsActivity.this.wheelBox.setVisibility(8);
                BaseBannedShieldedDetailsActivity.this.removeBtn.setVisibility(8);
                boolean unused = BaseBannedShieldedDetailsActivity.this.isShowWheel = false;
                BaseBannedShieldedDetailsActivity.this.transparentBox.setVisibility(0);
            }
        });
    }

    /* access modifiers changed from: private */
    public void showDropDownBox(boolean isShowTopicTypeBox2) {
        if (this.isShowTopicTypeBox != isShowTopicTypeBox2) {
            this.isShowTopicTypeBox = isShowTopicTypeBox2;
            if (isShowTopicTypeBox2) {
                this.reasonListview.setVisibility(0);
                this.transparentBox.setVisibility(0);
                this.reasonImgBtn.setBackgroundResource(this.resource.getDrawableId("mc_forum_input_select_d"));
                return;
            }
            this.reasonListview.setVisibility(8);
            this.transparentBox.setVisibility(8);
            this.reasonImgBtn.setBackgroundResource(this.resource.getDrawableId("mc_forum_input_select_n"));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.userInfoModeAsyncTask != null) {
            this.userInfoModeAsyncTask.cancel(true);
        }
        if (this.boardListAsyncTask != null) {
            this.boardListAsyncTask.cancel(true);
        }
    }

    /* access modifiers changed from: private */
    public void showBoardListBox(boolean isShowBoardBox2) {
        if (this.isShowBoardBox != isShowBoardBox2) {
            this.isShowBoardBox = isShowBoardBox2;
            if (isShowBoardBox2) {
                this.boardListView.setVisibility(0);
                this.transparentBox.setVisibility(0);
                this.boardImgBtn.setBackgroundResource(this.resource.getDrawableId("mc_forum_input_select_d"));
                return;
            }
            this.boardListView.setVisibility(8);
            this.transparentBox.setVisibility(8);
            this.boardImgBtn.setBackgroundResource(this.resource.getDrawableId("mc_forum_input_select_n"));
        }
    }

    private void initWheel(int id) {
        WheelView wheel = getWheel(id);
        if (id == this.resource.getViewId("mc_forum_wheel1")) {
            List<String> list = new ArrayList<>();
            for (String add : this.endTimeStr) {
                list.add(add);
            }
            wheel.setAdapter(new WheelListAdapter(list));
        }
        wheel.setCurrentItem(0);
        wheel.addChangingListener(this.changedListener);
        wheel.addScrollingListener(this.scrolledListener);
        wheel.setCyclic(true);
        wheel.setInterpolator(new AnticipateOvershootInterpolator());
    }

    private WheelView getWheel(int id) {
        return (WheelView) findViewById(id);
    }

    /* access modifiers changed from: private */
    public void updateStatus() {
        this.timeText.setText(getEndTimeStr());
    }

    private String getEndTimeStr() {
        return getWheel(this.resource.getViewId("mc_forum_wheel1")).getAdapter().getItem(getWheel(this.resource.getViewId("mc_forum_wheel1")).getCurrentItem());
    }

    /* access modifiers changed from: protected */
    public Date convertDate(String bannedTimeStr) {
        if (bannedTimeStr == null || "".equals(bannedTimeStr)) {
            return null;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        if (bannedTimeStr.equals(this.endTimeStr[0])) {
            cal.add(5, 1);
        }
        if (bannedTimeStr.equals(this.endTimeStr[1])) {
            cal.add(5, 3);
        }
        if (bannedTimeStr.equals(this.endTimeStr[2])) {
            cal.add(5, 7);
        }
        if (bannedTimeStr.equals(this.endTimeStr[3])) {
            cal.add(2, 1);
        }
        if (bannedTimeStr.equals(this.endTimeStr[4])) {
            cal.add(2, 3);
        }
        return cal.getTime();
    }

    private void showBoardListBoxEvent() {
        if (this.boardList == null || this.boardList.isEmpty()) {
            this.boardList = this.boardService.getBoadListByLocal();
            if (this.boardList == null) {
                if (this.boardListAsyncTask != null) {
                    this.boardListAsyncTask.cancel(true);
                }
                this.boardListAsyncTask = new GetBoardListAsyncTask();
                this.boardListAsyncTask.execute(new Void[0]);
                return;
            }
            updateBoardList(this.boardList);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.loadBoardDialog != null && this.loadBoardDialog.isShowing()) {
            this.loadBoardDialog.cancel();
        }
        if (this.boardListView.getVisibility() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.boardListView.setVisibility(8);
        return true;
    }

    class GetBoardListAsyncTask extends AsyncTask<Void, Void, List<BoardModel>> {
        GetBoardListAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<BoardModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            BaseBannedShieldedDetailsActivity.this.loadBoardDialog.show();
        }

        /* access modifiers changed from: protected */
        public List<BoardModel> doInBackground(Void... arg0) {
            return BaseBannedShieldedDetailsActivity.this.boardService.getBoadListByNet();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<BoardModel> result) {
            super.onPostExecute((Object) result);
            if (BaseBannedShieldedDetailsActivity.this.loadBoardDialog != null && BaseBannedShieldedDetailsActivity.this.loadBoardDialog.isShowing()) {
                BaseBannedShieldedDetailsActivity.this.loadBoardDialog.dismiss();
            }
            if (result != null && result.size() > 0) {
                if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                    Toast.makeText(BaseBannedShieldedDetailsActivity.this, MCForumErrorUtil.convertErrorCode(BaseBannedShieldedDetailsActivity.this, result.get(0).getErrorCode()), 0).show();
                    return;
                }
                BaseBannedShieldedDetailsActivity.this.updateBoardList(result);
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateBoardList(List<BoardModel> boardList2) {
        this.boardListAdapter.setBoardList(boardList2);
        this.boardListAdapter.notifyDataSetInvalidated();
        this.boardListAdapter.notifyDataSetChanged();
        this.boardList = boardList2;
    }

    private BoardModel createTotalAnnounce() {
        BoardModel boardModel = new BoardModel();
        boardModel.setBoardId(0);
        boardModel.setBoardName(getString(this.resource.getStringId("mc_forum_total_board")));
        return boardModel;
    }

    /* access modifiers changed from: private */
    public String getBoardDetailText(List<BoardModel> selectBoardList2) {
        String detail = "";
        StringBuffer boardName = new StringBuffer();
        if (selectBoardList2.size() > 0) {
            for (int i = 0; i < selectBoardList2.size(); i++) {
                if (selectBoardList2.get(i).getBoardId() == 0) {
                    return selectBoardList2.get(i).getBoardName();
                }
                boardName.append(selectBoardList2.get(i).getBoardName() + AdApiConstant.RES_SPLIT_COMMA);
            }
            detail = boardName.toString().substring(0, boardName.toString().length() - 1);
        }
        return detail;
    }

    private class GetUserInfoModeAsyncTask extends AsyncTask<Object, Void, UserInfoModel> {
        private UserInfoModel userInfoModel;

        private GetUserInfoModeAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            BaseBannedShieldedDetailsActivity.this.loadBoardDialog.isShowing();
        }

        /* access modifiers changed from: protected */
        public UserInfoModel doInBackground(Object... params) {
            return BaseBannedShieldedDetailsActivity.this.getUserInfoModel();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(UserInfoModel result) {
            super.onPostExecute((Object) result);
            BaseBannedShieldedDetailsActivity.this.loadBoardDialog.cancel();
            if (result == null) {
                BaseBannedShieldedDetailsActivity.this.boardList = new ArrayList();
            } else if (!StringUtil.isEmpty(result.getErrorCode())) {
                BaseBannedShieldedDetailsActivity.this.boardList = new ArrayList();
                BaseBannedShieldedDetailsActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(BaseBannedShieldedDetailsActivity.this, result.getErrorCode()));
                this.userInfoModel = null;
            } else {
                this.userInfoModel = result;
                if (this.userInfoModel != null) {
                    BaseBannedShieldedDetailsActivity.this.boardList = BaseBannedShieldedDetailsActivity.this.getBoardInfo(this.userInfoModel, BaseBannedShieldedDetailsActivity.this.boardList);
                    if (BaseBannedShieldedDetailsActivity.this.boardList == null) {
                        BaseBannedShieldedDetailsActivity.this.boardList = new ArrayList();
                    }
                    BaseBannedShieldedDetailsActivity.this.boardListAdapter.setBoardList(BaseBannedShieldedDetailsActivity.this.boardList);
                    BaseBannedShieldedDetailsActivity.this.selectBoardList.clear();
                    for (int i = 0; i < BaseBannedShieldedDetailsActivity.this.boardList.size(); i++) {
                        if (BaseBannedShieldedDetailsActivity.this.boardList.get(i).isSelected()) {
                            BaseBannedShieldedDetailsActivity.this.selectBoardList.add(BaseBannedShieldedDetailsActivity.this.boardList.get(i));
                            BaseBannedShieldedDetailsActivity.this.boardDetailText.setText(BaseBannedShieldedDetailsActivity.this.getBoardDetailText(BaseBannedShieldedDetailsActivity.this.selectBoardList));
                        }
                    }
                }
            }
        }
    }
}
