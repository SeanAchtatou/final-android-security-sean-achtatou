package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.LinearLayout;
import android.widget.TextView;

public class ReportTopicManageAdapterHolder {
    private LinearLayout ReportTopicBox;
    private TextView boardName;
    private TextView topicTitle;

    public TextView getBoardName() {
        return this.boardName;
    }

    public void setBoardName(TextView boardName2) {
        this.boardName = boardName2;
    }

    public TextView getTopicTitle() {
        return this.topicTitle;
    }

    public void setTopicTitle(TextView topicTitle2) {
        this.topicTitle = topicTitle2;
    }

    public LinearLayout getReportTopicBox() {
        return this.ReportTopicBox;
    }

    public void setReportTopicBox(LinearLayout reportTopicBox) {
        this.ReportTopicBox = reportTopicBox;
    }
}
