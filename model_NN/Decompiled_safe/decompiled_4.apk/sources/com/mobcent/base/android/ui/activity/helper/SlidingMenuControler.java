package com.mobcent.base.android.ui.activity.helper;

import android.support.v4.view.ViewPager;
import com.mobcent.base.android.ui.widget.slader.SlidingMenu;

public class SlidingMenuControler {
    private static SlidingMenuControler controler;
    private SlidingMenuControlerListener listener;
    private ViewPager viewPager;

    public interface SlidingMenuControlerListener {
        SlidingMenu getSlidingMenu();

        void onPostsView(ViewPager viewPager);
    }

    public static SlidingMenuControler getInstance() {
        if (controler == null) {
            controler = new SlidingMenuControler();
        }
        return controler;
    }

    public SlidingMenuControlerListener getListener() {
        return this.listener;
    }

    public void setListener(SlidingMenuControlerListener listener2) {
        this.listener = listener2;
    }

    public ViewPager getViewPager() {
        return this.viewPager;
    }

    public void setViewPager(ViewPager viewPager2) {
        this.viewPager = viewPager2;
    }
}
