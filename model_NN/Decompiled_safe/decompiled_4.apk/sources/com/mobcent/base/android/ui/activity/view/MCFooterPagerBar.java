package com.mobcent.base.android.ui.activity.view;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.delegate.GoToPageDelegate;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;

public class MCFooterPagerBar {
    /* access modifiers changed from: private */
    public String TAG = "PagerBar";
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public int currentPage = 1;
    /* access modifiers changed from: private */
    public Button goToPageBtn;
    private GoToPageDelegate goToPageDelegate;
    private LayoutInflater inflater;
    private Button nextPageBtn;
    /* access modifiers changed from: private */
    public EditText pageEdit;
    /* access modifiers changed from: private */
    public String pageEditTempStr;
    private View pagerContainer;
    private Button prePageBtn;
    /* access modifiers changed from: private */
    public MCResource resource;
    /* access modifiers changed from: private */
    public int totalPage;
    private TextView totalPageText;

    static /* synthetic */ int access$008(MCFooterPagerBar x0) {
        int i = x0.currentPage;
        x0.currentPage = i + 1;
        return i;
    }

    static /* synthetic */ int access$010(MCFooterPagerBar x0) {
        int i = x0.currentPage;
        x0.currentPage = i - 1;
        return i;
    }

    public MCFooterPagerBar(Activity activity2, GoToPageDelegate goToPageDelegate2) {
        this.activity = activity2;
        this.goToPageDelegate = goToPageDelegate2;
        this.resource = MCResource.getInstance(activity2);
        this.inflater = LayoutInflater.from(this.activity);
        initPagerBarWidgets();
    }

    public void goToPage(int page) {
        MCLogUtil.i(this.TAG, "Go to page " + page);
        this.currentPage = page;
        if (this.currentPage == 0) {
            this.currentPage = 1;
        }
        if (this.currentPage <= 1) {
            this.prePageBtn.setVisibility(8);
        } else {
            this.prePageBtn.setVisibility(0);
        }
        if (this.currentPage >= this.totalPage) {
            this.nextPageBtn.setVisibility(8);
        } else {
            this.nextPageBtn.setVisibility(0);
        }
        this.pageEditTempStr = this.currentPage + "";
        this.pageEdit.setText(this.currentPage + "");
        this.pageEdit.clearFocus();
        this.goToPageDelegate.goToPage(this.currentPage);
    }

    public void updatePagerInfo(int totalNum, int pageSize) {
        if (totalNum % pageSize == 0) {
            this.totalPage = totalNum / pageSize;
        } else {
            this.totalPage = (totalNum / pageSize) + 1;
        }
        if (this.totalPage == 0) {
            this.totalPage = 1;
        }
        if (this.pagerContainer.getVisibility() != 0) {
            initWidgets(this.totalPage);
        }
        this.pageEdit.setText(this.currentPage + "");
        this.totalPageText.setText(" / " + this.totalPage);
        this.goToPageBtn.setBackgroundResource(this.resource.getDrawableId("mc_forum_icon30"));
        if (this.currentPage <= 1) {
            this.prePageBtn.setVisibility(8);
        } else {
            this.prePageBtn.setVisibility(0);
        }
        if (this.currentPage >= this.totalPage) {
            this.nextPageBtn.setVisibility(8);
        } else {
            this.nextPageBtn.setVisibility(0);
        }
    }

    public int getCurrentPage() {
        return this.currentPage;
    }

    public int getTotalPage() {
        return this.totalPage;
    }

    private void initWidgets(int totalPage2) {
        this.totalPage = totalPage2;
        this.pagerContainer.setVisibility(0);
        initPagerBarActions();
    }

    private void initPagerBarWidgets() {
        this.pagerContainer = this.inflater.inflate(this.resource.getLayoutId("mc_forum_widget_footer_page_bar"), (ViewGroup) null);
        this.prePageBtn = (Button) this.pagerContainer.findViewById(this.resource.getViewId("mc_forum_pre_page_btn"));
        this.nextPageBtn = (Button) this.pagerContainer.findViewById(this.resource.getViewId("mc_forum_next_page_btn"));
        this.goToPageBtn = (Button) this.pagerContainer.findViewById(this.resource.getViewId("mc_forum_go_to_btn"));
        this.pageEdit = (EditText) this.pagerContainer.findViewById(this.resource.getViewId("mc_forum_page_edit"));
        this.totalPageText = (TextView) this.pagerContainer.findViewById(this.resource.getViewId("mc_forum_total_page_text"));
        this.pageEdit.setInputType(2);
        initPagerBarActions();
    }

    private void initPagerBarActions() {
        this.prePageBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MCFooterPagerBar.this.currentPage > 1) {
                    MCFooterPagerBar.access$010(MCFooterPagerBar.this);
                }
                MCFooterPagerBar.this.goToPage(MCFooterPagerBar.this.currentPage);
            }
        });
        this.nextPageBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MCFooterPagerBar.this.currentPage < MCFooterPagerBar.this.totalPage) {
                    MCFooterPagerBar.access$008(MCFooterPagerBar.this);
                }
                MCFooterPagerBar.this.goToPage(MCFooterPagerBar.this.currentPage);
            }
        });
        this.goToPageBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String toPage = MCFooterPagerBar.this.pageEdit.getText().toString();
                if (!toPage.trim().equals("")) {
                    try {
                        int unused = MCFooterPagerBar.this.currentPage = Integer.parseInt(toPage);
                    } catch (Exception e) {
                        MCLogUtil.e(MCFooterPagerBar.this.TAG, "Parse page error. The error page is " + toPage);
                    }
                    ((InputMethodManager) MCFooterPagerBar.this.activity.getSystemService("input_method")).hideSoftInputFromWindow(MCFooterPagerBar.this.pageEdit.getWindowToken(), 0);
                    MCFooterPagerBar.this.goToPage(MCFooterPagerBar.this.currentPage);
                }
            }
        });
        this.pageEdit.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                try {
                    String newPageStr = MCFooterPagerBar.this.pageEdit.getText().toString();
                    if (!newPageStr.trim().equals("")) {
                        if (!StringUtil.isNumeric(newPageStr)) {
                            MCFooterPagerBar.this.pageEdit.setText(MCFooterPagerBar.this.pageEditTempStr);
                        } else if (Integer.parseInt(newPageStr) > MCFooterPagerBar.this.totalPage) {
                            String unused = MCFooterPagerBar.this.pageEditTempStr = MCFooterPagerBar.this.totalPage + "";
                            MCFooterPagerBar.this.pageEdit.setText(MCFooterPagerBar.this.pageEditTempStr);
                            MCFooterPagerBar.this.pageEdit.setSelection(MCFooterPagerBar.this.pageEdit.getText().length());
                        } else {
                            String unused2 = MCFooterPagerBar.this.pageEditTempStr = newPageStr;
                            if (MCFooterPagerBar.this.pageEditTempStr.equals("" + MCFooterPagerBar.this.currentPage)) {
                                MCFooterPagerBar.this.goToPageBtn.setBackgroundResource(MCFooterPagerBar.this.resource.getDrawableId("mc_forum_icon30"));
                            } else {
                                MCFooterPagerBar.this.goToPageBtn.setBackgroundResource(MCFooterPagerBar.this.resource.getDrawableId("mc_forum_icon26"));
                            }
                        }
                    }
                } catch (Exception e) {
                    MCFooterPagerBar.this.pageEdit.setText(MCFooterPagerBar.this.pageEditTempStr);
                }
            }
        });
        this.pageEdit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCFooterPagerBar.this.pageEdit.setSelection(0, MCFooterPagerBar.this.pageEdit.getText().length());
            }
        });
    }

    public View getView() {
        return this.pagerContainer;
    }
}
