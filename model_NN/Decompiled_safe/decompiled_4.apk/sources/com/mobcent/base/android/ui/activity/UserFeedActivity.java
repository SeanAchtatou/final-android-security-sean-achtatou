package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.adapter.UserFeedsAdapter;
import com.mobcent.base.android.ui.activity.view.MCUserInfoView;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.forum.android.model.UserFeedModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.model.UserTopicFeedModel;
import com.mobcent.forum.android.service.FeedsService;
import com.mobcent.forum.android.service.impl.FeedsServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserFeedActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public UserFeedsAdapter adapter;
    private Set<String> allImgUrls;
    private Button backBtn;
    private long currUserId;
    protected int currentPage = 1;
    protected PullToRefreshListView.OnScrollListener feedListOnScrollListener = new PullToRefreshListView.OnScrollListener() {
        private int firstVisibleItem;
        private int totalItemCount;
        private int visibleItemCount;

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0) {
                UserFeedActivity.this.asyncTaskLoaderImage.recycleBitmaps(getRecycleImgUrls(UserFeedActivity.this.pageSize));
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
            this.firstVisibleItem = firstVisibleItem2;
            this.visibleItemCount = visibleItemCount2;
            this.totalItemCount = totalItemCount2 - 2;
        }

        private Set<String> getRecycleImgUrls(int pageSize) {
            int size = pageSize;
            int firstIndex = 0;
            int endIndex = this.firstVisibleItem + this.visibleItemCount + size;
            if (this.firstVisibleItem - size > 0) {
                firstIndex = this.firstVisibleItem - size;
            }
            if (endIndex >= this.totalItemCount) {
                endIndex = this.totalItemCount;
            }
            return UserFeedActivity.this.getRecycleImageURL(firstIndex, endIndex);
        }

        public void onScrollDirection(boolean isUp, int distance) {
        }
    };
    /* access modifiers changed from: private */
    public FeedsService feedsService;
    private LayoutInflater layoutInflater;
    /* access modifiers changed from: private */
    public MoreFeedsTask moreFeedsTask;
    /* access modifiers changed from: private */
    public int pageSize = 25;
    /* access modifiers changed from: private */
    public PullToRefreshListView pullToRefreshListView;
    /* access modifiers changed from: private */
    public RefreshFeedsTask refreshFeedsTask;
    private TextView titleText;
    /* access modifiers changed from: private */
    public long userId;
    /* access modifiers changed from: private */
    public UserInfoModel userInfoModel;
    /* access modifiers changed from: private */
    public MCUserInfoView userInfoView;
    private String userName;
    /* access modifiers changed from: private */
    public List<UserTopicFeedModel> userTopicFeeds;

    /* access modifiers changed from: protected */
    public void initData() {
        this.allImgUrls = new HashSet();
        this.userTopicFeeds = new ArrayList();
        this.currUserId = new UserServiceImpl(getApplicationContext()).getLoginUserId();
        this.feedsService = new FeedsServiceImpl(getApplicationContext());
        Intent intent = getIntent();
        this.userId = intent.getLongExtra("userId", this.currUserId);
        this.userName = intent.getStringExtra("nickname");
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_user_feeds_activity"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.titleText = (TextView) findViewById(this.resource.getViewId("mc_forum_title_text"));
        this.titleText.setText(this.userName);
        this.layoutInflater = LayoutInflater.from(this);
        this.pullToRefreshListView = (PullToRefreshListView) findViewById(this.resource.getViewId("mc_forum_lv"));
        this.pullToRefreshListView.setScrollListener(this.feedListOnScrollListener);
        this.userInfoView = new MCUserInfoView(this, this.layoutInflater, this.currUserId, this.asyncTaskLoaderImage, this.mHandler);
        this.pullToRefreshListView.addHeaderView(this.userInfoView.getUserInfoView(), null, false);
        this.adapter = new UserFeedsAdapter(this, this.layoutInflater, this.mHandler, this.asyncTaskLoaderImage, this.userInfoModel, this.pullToRefreshListView, this.userTopicFeeds);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserFeedActivity.this.back();
            }
        });
        this.pullToRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                if (UserFeedActivity.this.refreshFeedsTask != null) {
                    UserFeedActivity.this.refreshFeedsTask.cancel(true);
                }
                RefreshFeedsTask unused = UserFeedActivity.this.refreshFeedsTask = new RefreshFeedsTask();
                UserFeedActivity.this.refreshFeedsTask.execute(new Void[0]);
            }
        });
        this.pullToRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                if (UserFeedActivity.this.moreFeedsTask != null) {
                    UserFeedActivity.this.moreFeedsTask.cancel(true);
                }
                MoreFeedsTask unused = UserFeedActivity.this.moreFeedsTask = new MoreFeedsTask();
                UserFeedActivity.this.moreFeedsTask.execute(new Void[0]);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.pullToRefreshListView.onRefreshWithOutListener();
        new RefreshFeedsTask().execute(new Void[0]);
    }

    private class RefreshFeedsTask extends AsyncTask<Void, Void, UserFeedModel> {
        private RefreshFeedsTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            UserFeedActivity.this.currentPage = 1;
        }

        /* access modifiers changed from: protected */
        public UserFeedModel doInBackground(Void... params) {
            return UserFeedActivity.this.feedsService.getUserFeed(UserFeedActivity.this.userId, UserFeedActivity.this.currentPage, UserFeedActivity.this.pageSize);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(UserFeedModel result) {
            super.onPostExecute((Object) result);
            UserFeedActivity.this.pullToRefreshListView.onRefreshComplete();
            UserFeedActivity.this.pullToRefreshListView.setSelection(0);
            if (result == null) {
                UserFeedActivity.this.warnMessageById("mc_forum_warn_no_such_data");
            } else if (!StringUtil.isEmpty(result.getErrorCode())) {
                UserFeedActivity.this.warnMessageByStr(result.getErrorCode());
                UserFeedActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                if (UserFeedActivity.this.userInfoModel == null) {
                    UserInfoModel unused = UserFeedActivity.this.userInfoModel = result.getUserInfo();
                    UserFeedActivity.this.adapter.setUserInfoModel(UserFeedActivity.this.userInfoModel);
                    UserFeedActivity.this.userInfoView.updateUserInfo(UserFeedActivity.this.userInfoModel);
                }
                UserFeedActivity.this.adapter.setFeedsList(result.getUserTopicList());
                UserFeedActivity.this.adapter.notifyDataSetInvalidated();
                UserFeedActivity.this.adapter.notifyDataSetChanged();
                UserFeedActivity.this.pullToRefreshListView.onRefreshComplete();
                if (result.getHasNext() == 1) {
                    UserFeedActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    UserFeedActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
                }
                List unused2 = UserFeedActivity.this.userTopicFeeds = result.getUserTopicList();
            }
        }
    }

    private class MoreFeedsTask extends AsyncTask<Void, Void, UserFeedModel> {
        private MoreFeedsTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            UserFeedActivity.this.currentPage++;
        }

        /* access modifiers changed from: protected */
        public UserFeedModel doInBackground(Void... params) {
            return UserFeedActivity.this.feedsService.getUserFeed(UserFeedActivity.this.userId, UserFeedActivity.this.currentPage, UserFeedActivity.this.pageSize);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(UserFeedModel result) {
            if (result == null) {
                UserFeedActivity.this.currentPage--;
            } else if (!StringUtil.isEmpty(result.getErrorCode())) {
                UserFeedActivity.this.currentPage--;
                UserFeedActivity.this.warnMessageByStr(result.getErrorCode());
                UserFeedActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                if (UserFeedActivity.this.userInfoModel == null) {
                    UserInfoModel unused = UserFeedActivity.this.userInfoModel = result.getUserInfo();
                    UserFeedActivity.this.adapter.setUserInfoModel(UserFeedActivity.this.userInfoModel);
                    UserFeedActivity.this.userInfoView.updateUserInfo(UserFeedActivity.this.userInfoModel);
                }
                List<UserTopicFeedModel> tempList = new ArrayList<>();
                tempList.addAll(UserFeedActivity.this.userTopicFeeds);
                tempList.addAll(result.getUserTopicList());
                UserFeedActivity.this.adapter.setFeedsList(tempList);
                UserFeedActivity.this.adapter.notifyDataSetInvalidated();
                UserFeedActivity.this.adapter.notifyDataSetChanged();
                if (result.getHasNext() == 1) {
                    UserFeedActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    UserFeedActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
                }
                List unused2 = UserFeedActivity.this.userTopicFeeds = tempList;
            }
        }
    }

    /* access modifiers changed from: protected */
    public Set<String> getRecycleImageURL(int start, int end) {
        Set<String> imgUrls = new HashSet<>();
        imgUrls.addAll(this.allImgUrls);
        int j = this.userTopicFeeds.size();
        for (int k = 0; k < j; k++) {
            UserTopicFeedModel userTopicFeed = this.userTopicFeeds.get(k);
            if (userTopicFeed.getTopic() != null && userTopicFeed.getTopic().isHasImg()) {
                imgUrls.remove(userTopicFeed.getTopic().getPic());
            }
        }
        return imgUrls;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.refreshFeedsTask != null) {
            this.refreshFeedsTask.cancel(true);
        }
        if (this.moreFeedsTask != null) {
            this.moreFeedsTask.cancel(true);
        }
    }
}
