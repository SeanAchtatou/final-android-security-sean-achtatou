package com.mobcent.base.android.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.constant.BaseRestfulApiConstant;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.ImageUtil;

public abstract class BasePhotoPreviewActivity extends BasePhotoActivity {
    private static final int leftDegrees = -90;
    private static final int rightDegrees = 90;
    protected Bitmap bitmap;
    protected ImageButton closeBtn;
    protected AlertDialog.Builder exitAlertDialog;
    protected String iconPath = null;
    protected final int iconWidth = 150;
    protected final int imageMaxHeight = 960;
    protected final int imageMaxWidth = 640;
    protected final int imageWidth = 200;
    private ImageButton leftRotateBtn;
    protected PostsService postsService = null;
    /* access modifiers changed from: private */
    public LinearLayout previewBox;
    private ImageView previewGifImg;
    /* access modifiers changed from: private */
    public ImageView previewIconImg;
    /* access modifiers changed from: private */
    public ImageView previewJPGImg;
    private ImageButton rightRotateBtn;
    private ImageButton sureBtn;
    /* access modifiers changed from: private */
    public UploadAsyncTask uploadAsyncTask;
    protected boolean uploadSucc = true;
    /* access modifiers changed from: private */
    public UserService userService = null;

    /* access modifiers changed from: protected */
    public abstract boolean exitCheckChanged();

    /* access modifiers changed from: protected */
    public abstract void exitNoSaveEvent();

    /* access modifiers changed from: protected */
    public abstract void exitSaveEvent();

    /* access modifiers changed from: protected */
    public abstract void updateUIAfterUpload();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.userService = new UserServiceImpl(this);
        this.postsService = new PostsServiceImpl(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        this.previewBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_user_preview_box"));
        this.previewJPGImg = (ImageView) findViewById(this.resource.getViewId("mc_forum_user_preview_jpg_img"));
        this.previewIconImg = (ImageView) findViewById(this.resource.getViewId("mc_forum_user_preview_icon_img"));
        this.previewGifImg = (ImageView) findViewById(this.resource.getViewId("mc_forum_user_preview_gif_img"));
        this.leftRotateBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_rotate_left_btn"));
        this.rightRotateBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_rotate_right_btn"));
        this.closeBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_close_btn"));
        this.sureBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_sure_btn"));
        initExitAlertDialog();
    }

    /* access modifiers changed from: protected */
    public void initExitAlertDialog() {
        this.exitAlertDialog = new AlertDialog.Builder(this).setTitle(this.resource.getStringId("mc_forum_dialog_tip")).setMessage(this.resource.getStringId("mc_forum_warn_photo_title"));
        this.exitAlertDialog.setPositiveButton(this.resource.getStringId("mc_forum_warn_photo_ok"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                BasePhotoPreviewActivity.this.exitSaveEvent();
            }
        });
        this.exitAlertDialog.setNegativeButton(this.resource.getStringId("mc_forum_warn_photo_cancel"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                BasePhotoPreviewActivity.this.exitNoSaveEvent();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.previewBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
            }
        });
        this.leftRotateBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePhotoPreviewActivity.this.bitmap = ImageUtil.rotateBitmap(BasePhotoPreviewActivity.this.bitmap, -90.0f);
                if (BasePhotoPreviewActivity.this.bitmap != null && !BasePhotoPreviewActivity.this.bitmap.isRecycled()) {
                    if (BasePhotoPreviewActivity.this.uploadType == 1) {
                        BasePhotoPreviewActivity.this.previewIconImg.setImageBitmap(BasePhotoPreviewActivity.this.bitmap);
                    } else {
                        BasePhotoPreviewActivity.this.previewJPGImg.setImageBitmap(BasePhotoPreviewActivity.this.bitmap);
                    }
                }
            }
        });
        this.rightRotateBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePhotoPreviewActivity.this.bitmap = ImageUtil.rotateBitmap(BasePhotoPreviewActivity.this.bitmap, 90.0f);
                if (BasePhotoPreviewActivity.this.bitmap != null && !BasePhotoPreviewActivity.this.bitmap.isRecycled()) {
                    if (BasePhotoPreviewActivity.this.uploadType == 1) {
                        BasePhotoPreviewActivity.this.previewIconImg.setImageBitmap(BasePhotoPreviewActivity.this.bitmap);
                    } else {
                        BasePhotoPreviewActivity.this.previewJPGImg.setImageBitmap(BasePhotoPreviewActivity.this.bitmap);
                    }
                }
            }
        });
        this.closeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePhotoPreviewActivity.this.previewBox.setVisibility(8);
                BasePhotoPreviewActivity.this.clearTempFile();
            }
        });
        this.sureBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BasePhotoPreviewActivity.this.uploadAsyncTask != null) {
                    BasePhotoPreviewActivity.this.uploadAsyncTask.cancel(true);
                }
                UploadAsyncTask unused = BasePhotoPreviewActivity.this.uploadAsyncTask = new UploadAsyncTask();
                BasePhotoPreviewActivity.this.uploadAsyncTask.execute(new String[0]);
            }
        });
    }

    class UploadAsyncTask extends AsyncTask<String, Void, Object> {
        UploadAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            BasePhotoPreviewActivity.this.showProgressDialog("mc_forum_warn_upload_img", this);
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(String... params) {
            if (BasePhotoPreviewActivity.this.uploadType != 3) {
                BasePhotoPreviewActivity.this.getUploadingBitmap(BasePhotoPreviewActivity.this.bitmap);
                BasePhotoPreviewActivity.this.path = BasePhotoPreviewActivity.this.compressPath;
            }
            if (BasePhotoPreviewActivity.this.uploadType == 1) {
                return BasePhotoPreviewActivity.this.userService.uploadIcon(BasePhotoPreviewActivity.this.path);
            }
            if (BasePhotoPreviewActivity.this.uploadType == 2 || BasePhotoPreviewActivity.this.uploadType == 3) {
                return BasePhotoPreviewActivity.this.postsService.uploadImage(BasePhotoPreviewActivity.this.path, BaseRestfulApiConstant.FORUM_LOCAL_ACTION);
            }
            return BasePhotoPreviewActivity.this.postsService.uploadImage(BasePhotoPreviewActivity.this.path, BaseRestfulApiConstant.FORUM_LOCAL_ACTION);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object result) {
            BasePhotoPreviewActivity.this.hideProgressDialog();
            if (result == null) {
                BasePhotoPreviewActivity.this.uploadIconFail();
            } else if (result instanceof String) {
                String path = (String) result;
                if (path.startsWith(BaseReturnCodeConstant.ERROR_CODE)) {
                    String errorCode = path.substring(path.indexOf(BaseReturnCodeConstant.ERROR_CODE) + 1, path.length());
                    if (errorCode != null && !errorCode.equals("")) {
                        BasePhotoPreviewActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(BasePhotoPreviewActivity.this, errorCode));
                        BasePhotoPreviewActivity.this.clearTempFile();
                        BasePhotoPreviewActivity.this.clearMemory();
                    }
                } else {
                    if (BasePhotoPreviewActivity.this.uploadType == 1) {
                        BasePhotoPreviewActivity.this.warnMessageById("mc_forum_user_photo_upload_succ");
                    } else if (BasePhotoPreviewActivity.this.uploadType == 2 || BasePhotoPreviewActivity.this.uploadType == 3) {
                        BasePhotoPreviewActivity.this.warnMessageById("mc_forum_user_photo_upload_image_succ");
                    }
                    BasePhotoPreviewActivity.this.previewBox.setVisibility(8);
                    if (BasePhotoPreviewActivity.this.uploadType != 1) {
                        BasePhotoPreviewActivity.this.bitmap = BasePhotoPreviewActivity.this.getUploadedBitmap(BasePhotoPreviewActivity.this.bitmap);
                    }
                    BasePhotoPreviewActivity.this.iconPath = path;
                    BasePhotoPreviewActivity.this.updateUIAfterUpload();
                }
            }
            BasePhotoPreviewActivity.this.previewBox.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void uploadIconFail() {
        warnMessageById("mc_forum_user_photo_select_error");
        clearTempFile();
        clearMemory();
    }

    /* access modifiers changed from: protected */
    public void doSomethingAfterSelectedPhoto() {
        this.bitmap = getPreviewBitmap(this.path);
        if (this.bitmap == null) {
            uploadIconFail();
        } else if (!this.bitmap.isRecycled()) {
            this.previewBox.setVisibility(0);
            if (this.uploadType == 1) {
                this.previewIconImg.setVisibility(0);
                this.previewIconImg.setImageBitmap(this.bitmap);
            } else {
                this.previewJPGImg.setVisibility(0);
                this.previewJPGImg.setImageBitmap(this.bitmap);
            }
            if (this.uploadType == 3) {
                this.leftRotateBtn.setVisibility(8);
                this.rightRotateBtn.setVisibility(8);
                return;
            }
            this.leftRotateBtn.setVisibility(0);
            this.rightRotateBtn.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public boolean backEventClickListener() {
        if (this.previewBox.getVisibility() != 0) {
            return true;
        }
        this.previewBox.setVisibility(8);
        clearTempFile();
        clearMemory();
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (!backEventClickListener()) {
            return true;
        }
        if (exitCheckChanged()) {
            this.exitAlertDialog.show();
            return true;
        }
        exitNoSaveEvent();
        return true;
    }

    /* access modifiers changed from: protected */
    public Bitmap getPreviewBitmap(String path) {
        return ImageUtil.getBitmapFromMedia(this, path);
    }

    /* access modifiers changed from: protected */
    public void getUploadingBitmap(Bitmap bitmap2) {
        ImageUtil.compressBitmap(this.compressPath, bitmap2, 100, 3, this);
    }

    /* access modifiers changed from: protected */
    public Bitmap getUploadedBitmap(Bitmap bitmap2) {
        return ImageUtil.compressBitmap(bitmap2, 3, this);
    }

    /* access modifiers changed from: private */
    public void clearMemory() {
        if (this.bitmap != null && !this.bitmap.isRecycled()) {
            this.bitmap.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        clearMemory();
        clearTempFile();
        if (this.uploadAsyncTask != null) {
            this.uploadAsyncTask.cancel(true);
        }
    }
}
