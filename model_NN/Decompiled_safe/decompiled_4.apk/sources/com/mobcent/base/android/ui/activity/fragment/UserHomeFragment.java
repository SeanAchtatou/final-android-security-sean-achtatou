package com.mobcent.base.android.ui.activity.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.ForumManageActivity;
import com.mobcent.base.android.ui.activity.ModeratorActivity;
import com.mobcent.base.android.ui.activity.UserBannedActivity;
import com.mobcent.base.android.ui.activity.UserShieldedActivity;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.BannedShieldedAsyncTask;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.CancelBannedShieldedAsyncTask;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;
import com.mobcent.base.android.ui.activity.view.MCUserInfoView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.db.UserJsonDBUtil;
import com.mobcent.forum.android.db.helper.UserJsonDBHelper;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.HeartMsgService;
import com.mobcent.forum.android.service.UserManageService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.HeartMsgServiceImpl;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.UserManageServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.StringUtil;

public class UserHomeFragment extends BaseFragment implements MCConstant {
    /* access modifiers changed from: private */
    public TextView accountClose;
    /* access modifiers changed from: private */
    public TextView accountDelete;
    /* access modifiers changed from: private */
    public TextView addBlackText;
    public Button backBtn;
    /* access modifiers changed from: private */
    public BannedShieldedAsyncTask bannedShieldedAsyncTask;
    private TextView bannedText;
    /* access modifiers changed from: private */
    public BlackAsyncTask blackAsyncTask;
    private TextView boardManageText;
    /* access modifiers changed from: private */
    public CancelBannedShieldedAsyncTask cancelBannedShieldedAsyncTask;
    /* access modifiers changed from: private */
    public long currUserId;
    private TextView favoritesText;
    private TextView findFriendText;
    private TextView findSurroundText;
    private TextView forumManageText;
    /* access modifiers changed from: private */
    public HeartMsgService heartMsgService;
    /* access modifiers changed from: private */
    public boolean isBlack = false;
    /* access modifiers changed from: private */
    public boolean isClose = false;
    /* access modifiers changed from: private */
    public boolean isDeltete = false;
    /* access modifiers changed from: private */
    public RelativeLayout loadingBox;
    private TextView messageText;
    private ViewStub myStub;
    private LinearLayout myUserHomeBox;
    private ViewStub otherStub;
    private LinearLayout otherUserHomeBox;
    private TextView privateText;
    /* access modifiers changed from: private */
    public MCProgressBar progressBar;
    public Button refreshBtn;
    private TextView reportText;
    private TextView shieldText;
    /* access modifiers changed from: private */
    public long userId;
    private UserInfoAsynTask userInfoAsynTask;
    private LinearLayout userInfoBox;
    /* access modifiers changed from: private */
    public UserInfoModel userInfoModel;
    private MCUserInfoView userInfoView;
    /* access modifiers changed from: private */
    public UserManageService userManageService;
    /* access modifiers changed from: private */
    public UserService userService;
    /* access modifiers changed from: private */
    public UserTopicClickListener userTopicClickListener;

    public interface UserTopicClickListener {
        void onIconClick(View view, UserInfoModel userInfoModel);

        void onMessageClick(View view);

        void onPrivateClick(View view, UserInfoModel userInfoModel);

        void onReportClick(View view, UserInfoModel userInfoModel);

        void onSettingClick(View view);

        void onUserFanClick(View view, UserInfoModel userInfoModel);

        void onUserFavoritesClick(View view, UserInfoModel userInfoModel, long j);

        void onUserFindFriendsClick(View view);

        void onUserFindSurroundClick(View view, UserInfoModel userInfoModel);

        void onUserFollowClick(View view, UserInfoModel userInfoModel);

        void onUserReplyClick(View view, UserInfoModel userInfoModel, long j);

        void onUserTopicClick(View view, UserInfoModel userInfoModel, long j);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.userId > 0) {
            refresh();
        }
        return this.view;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
        this.currUserId = this.userService.getLoginUserId();
        if (userId2 <= 0) {
            this.userId = this.currUserId;
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.userService = new UserServiceImpl(this.activity);
        this.userId = this.activity.getIntent().getLongExtra("userId", 0);
        this.currUserId = this.userService.getLoginUserId();
        if (this.userId <= 0) {
            this.userId = this.currUserId;
        }
        this.heartMsgService = new HeartMsgServiceImpl(this.activity);
        this.userManageService = new UserManageServiceImpl(this.activity);
        this.permService = new PermServiceImpl(this.activity);
    }

    public void refresh() {
        setUserId(this.userId);
        if (this.userId == this.currUserId) {
            initMyUserHomeBox();
        } else {
            initOtherUserHomeBox();
        }
        this.userInfoAsynTask = new UserInfoAsynTask();
        this.userInfoAsynTask.execute(Long.valueOf(this.userId));
    }

    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(this.mcResource.getLayoutId("mc_forum_user_home_fragment"), (ViewGroup) null);
        this.backBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_back_btn"));
        this.refreshBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_refresh_btn"));
        this.userInfoBox = (LinearLayout) this.view.findViewById(this.mcResource.getViewId("mc_forum_user_info_title_box"));
        this.userInfoView = new MCUserInfoView(this.activity, inflater, this.currUserId, this.asyncTaskLoaderImage, this.mHandler);
        this.userInfoBox.addView(this.userInfoView.getUserInfoView());
        if (MCForumHelper.setUserHomePublishView(this.activity)) {
            this.userInfoView.publishView(false);
        }
        this.loadingBox = (RelativeLayout) this.view.findViewById(this.mcResource.getViewId("mc_forum_loading_container"));
        this.progressBar = (MCProgressBar) this.view.findViewById(this.mcResource.getViewId("mc_forum_progress_bar"));
        if (MCForumHelper.setUserHomeRefreshBtn() && this.userId == this.userService.getLoginUserId()) {
            setRefreshBtn();
        }
        return this.view;
    }

    private void initMyUserHomeBox() {
        this.refreshBtn.setVisibility(0);
        if (this.myStub == null) {
            this.myStub = (ViewStub) this.view.findViewById(this.mcResource.getViewId("mc_forum_my_stub"));
            this.myStub.setVisibility(0);
            this.myUserHomeBox = (LinearLayout) this.view.findViewById(this.mcResource.getViewId("mc_forum_my_user_home_box"));
            this.myUserHomeBox.setVisibility(0);
            this.messageText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_my_messages_text"));
            this.favoritesText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_user_favorites_text"));
            this.findFriendText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_find_friends_text"));
            this.findSurroundText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_find_surround_text"));
            this.forumManageText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_user_manage_text"));
            if (MCForumHelper.onUserHomeSurroundTopic(this.activity)) {
                this.findSurroundText.setVisibility(8);
                this.view.findViewById(this.mcResource.getViewId("mc_forum_find_surround_line")).setVisibility(8);
            }
            if (MCForumHelper.onUserHomeFindFriend(this.activity)) {
                this.findFriendText.setVisibility(8);
                this.view.findViewById(this.mcResource.getViewId("mc_forum_find_friend_line")).setVisibility(8);
            }
            MCForumHelper.changeFavoritText(this.activity, this.favoritesText);
            this.messageText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (UserHomeFragment.this.getUserTopicClickListener() != null && UserHomeFragment.this.userInfoModel != null) {
                        UserHomeFragment.this.userTopicClickListener.onMessageClick(v);
                    }
                }
            });
            this.favoritesText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (UserHomeFragment.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) != 1) {
                        UserHomeFragment.this.warnMessageByStr(UserHomeFragment.this.mcResource.getString("mc_forum_permission_cannot_visit_topic"));
                    } else if (UserHomeFragment.this.getUserTopicClickListener() != null && UserHomeFragment.this.userInfoModel != null) {
                        UserHomeFragment.this.userTopicClickListener.onUserFavoritesClick(v, UserHomeFragment.this.userInfoModel, UserHomeFragment.this.userId);
                    }
                }
            });
            this.findFriendText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (UserHomeFragment.this.getUserTopicClickListener() != null && UserHomeFragment.this.userInfoModel != null) {
                        UserHomeFragment.this.userTopicClickListener.onUserFindFriendsClick(v);
                    }
                }
            });
            this.findSurroundText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (UserHomeFragment.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) != 1) {
                        UserHomeFragment.this.warnMessageByStr(UserHomeFragment.this.mcResource.getString("mc_forum_permission_cannot_visit_topic"));
                    } else if (UserHomeFragment.this.getUserTopicClickListener() != null && UserHomeFragment.this.userInfoModel != null) {
                        UserHomeFragment.this.userTopicClickListener.onUserFindSurroundClick(v, UserHomeFragment.this.userInfoModel);
                    }
                }
            });
            this.forumManageText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    UserHomeFragment.this.startActivity(new Intent(UserHomeFragment.this.activity, ForumManageActivity.class));
                }
            });
        }
    }

    private void initOtherUserHomeBox() {
        if (this.otherStub == null) {
            this.otherStub = (ViewStub) this.view.findViewById(this.mcResource.getViewId("mc_forum_other_stub"));
            this.otherStub.setVisibility(0);
            this.otherUserHomeBox = (LinearLayout) this.view.findViewById(this.mcResource.getViewId("mc_forum_other_user_home_box"));
            this.otherUserHomeBox.setVisibility(0);
            this.privateText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_private_text"));
            this.addBlackText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_add_cancel_black_text"));
            this.reportText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_report_user_text"));
            this.boardManageText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_board_manage_text"));
            this.bannedText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_banned_user_text"));
            this.shieldText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_shieled_text"));
            this.accountDelete = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_accounts_delete_text"));
            this.accountClose = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_accounts_close_text"));
            this.privateText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (UserHomeFragment.this.userInfoModel != null && UserHomeFragment.this.getUserTopicClickListener() != null) {
                        UserHomeFragment.this.userTopicClickListener.onPrivateClick(v, UserHomeFragment.this.userInfoModel);
                    }
                }
            });
            this.addBlackText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (UserHomeFragment.this.userInfoModel != null) {
                        BlackAsyncTask unused = UserHomeFragment.this.blackAsyncTask = new BlackAsyncTask();
                        if (UserHomeFragment.this.isBlack) {
                            UserHomeFragment.this.addBlackText.setText(UserHomeFragment.this.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_add_black")));
                            UserHomeFragment.this.blackAsyncTask.execute(0);
                            boolean unused2 = UserHomeFragment.this.isBlack = false;
                            return;
                        }
                        UserHomeFragment.this.addBlackText.setText(UserHomeFragment.this.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_cancel_black")));
                        UserHomeFragment.this.blackAsyncTask.execute(1);
                        boolean unused3 = UserHomeFragment.this.isBlack = true;
                    }
                }
            });
            this.reportText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (UserHomeFragment.this.userInfoModel != null && UserHomeFragment.this.getUserTopicClickListener() != null) {
                        UserHomeFragment.this.userTopicClickListener.onReportClick(v, UserHomeFragment.this.userInfoModel);
                    }
                }
            });
            this.boardManageText.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(UserHomeFragment.this.activity, ModeratorActivity.class);
                    intent.putExtra("userId", UserHomeFragment.this.userId);
                    UserHomeFragment.this.activity.startActivity(intent);
                }
            });
            this.bannedText.setOnClickListener(new View.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
                public void onClick(View v) {
                    Intent intent = new Intent(UserHomeFragment.this.activity, UserBannedActivity.class);
                    intent.putExtra(MCConstant.BANNED_TYPE, 4);
                    intent.putExtra("userId", UserHomeFragment.this.currUserId);
                    intent.putExtra(MCConstant.BANNED_USER_ID, UserHomeFragment.this.userId);
                    intent.putExtra("boardId", -1L);
                    UserHomeFragment.this.activity.startActivity(intent);
                }
            });
            this.shieldText.setOnClickListener(new View.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                  ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
                public void onClick(View v) {
                    Intent intent = new Intent(UserHomeFragment.this.activity, UserShieldedActivity.class);
                    intent.putExtra(MCConstant.SHIELDED_TYPE, 1);
                    intent.putExtra("userId", UserHomeFragment.this.currUserId);
                    intent.putExtra("shieldedUserId", UserHomeFragment.this.userId);
                    intent.putExtra("boardId", -1L);
                    UserHomeFragment.this.activity.startActivity(intent);
                }
            });
            this.accountDelete.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (UserHomeFragment.this.isDeltete) {
                        CancelBannedShieldedAsyncTask unused = UserHomeFragment.this.cancelBannedShieldedAsyncTask = new CancelBannedShieldedAsyncTask(UserHomeFragment.this.activity, UserHomeFragment.this.mcResource, 2, UserHomeFragment.this.userId, 0);
                        UserHomeFragment.this.cancelBannedShieldedAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                            public void executeSuccess() {
                                UserHomeFragment.this.accountDelete.setText(UserHomeFragment.this.activity.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_user_accounts_delete")));
                                boolean unused = UserHomeFragment.this.isDeltete = false;
                                Toast.makeText(UserHomeFragment.this.activity, UserHomeFragment.this.activity.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_user_accounts_delete_fail")), 1).show();
                            }

                            public void executeFail() {
                            }
                        });
                        UserHomeFragment.this.cancelBannedShieldedAsyncTask.execute(new Object[0]);
                        return;
                    }
                    new AlertDialog.Builder(UserHomeFragment.this.activity).setMessage(UserHomeFragment.this.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_accounts_delete"))).setNegativeButton(UserHomeFragment.this.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_dialog_cancel")), (DialogInterface.OnClickListener) null).setPositiveButton(UserHomeFragment.this.mcResource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            BannedShieldedAsyncTask unused = UserHomeFragment.this.bannedShieldedAsyncTask = new BannedShieldedAsyncTask(UserHomeFragment.this.activity, UserHomeFragment.this.mcResource, UserHomeFragment.this.userManageService, 2, UserHomeFragment.this.currUserId, UserHomeFragment.this.userId, 0, null, "");
                            UserHomeFragment.this.bannedShieldedAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                                public void executeSuccess() {
                                    UserHomeFragment.this.accountDelete.setText(UserHomeFragment.this.activity.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_user_accounts_delete_fail")));
                                    boolean unused = UserHomeFragment.this.isDeltete = true;
                                    Toast.makeText(UserHomeFragment.this.activity, UserHomeFragment.this.activity.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_user_accounts_delete_succ")), 1).show();
                                }

                                public void executeFail() {
                                }
                            });
                            UserHomeFragment.this.bannedShieldedAsyncTask.execute(new Object[0]);
                        }
                    }).show();
                }
            });
            this.accountClose.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (UserHomeFragment.this.isClose) {
                        CancelBannedShieldedAsyncTask unused = UserHomeFragment.this.cancelBannedShieldedAsyncTask = new CancelBannedShieldedAsyncTask(UserHomeFragment.this.activity, UserHomeFragment.this.mcResource, 3, UserHomeFragment.this.userId, 0);
                        UserHomeFragment.this.cancelBannedShieldedAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                            public void executeSuccess() {
                                UserHomeFragment.this.accountClose.setText(UserHomeFragment.this.activity.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_user_accounts_close")));
                                Toast.makeText(UserHomeFragment.this.activity, UserHomeFragment.this.activity.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_user_accounts_close_fail")), 1).show();
                                boolean unused = UserHomeFragment.this.isClose = false;
                            }

                            public void executeFail() {
                            }
                        });
                        UserHomeFragment.this.cancelBannedShieldedAsyncTask.execute(new Object[0]);
                        return;
                    }
                    new AlertDialog.Builder(UserHomeFragment.this.activity).setMessage(UserHomeFragment.this.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_accounts_close"))).setNegativeButton(UserHomeFragment.this.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_dialog_cancel")), (DialogInterface.OnClickListener) null).setPositiveButton(UserHomeFragment.this.mcResource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            BannedShieldedAsyncTask unused = UserHomeFragment.this.bannedShieldedAsyncTask = new BannedShieldedAsyncTask(UserHomeFragment.this.activity, UserHomeFragment.this.mcResource, UserHomeFragment.this.userManageService, 3, UserHomeFragment.this.currUserId, UserHomeFragment.this.userId, 0, null, "");
                            UserHomeFragment.this.bannedShieldedAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                                public void executeSuccess() {
                                    UserHomeFragment.this.accountClose.setText(UserHomeFragment.this.activity.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_user_accounts_close_fail")));
                                    Toast.makeText(UserHomeFragment.this.activity, UserHomeFragment.this.activity.getResources().getString(UserHomeFragment.this.mcResource.getStringId("mc_forum_user_accounts_close_succ")), 1).show();
                                    boolean unused = UserHomeFragment.this.isClose = true;
                                }

                                public void executeFail() {
                                }
                            });
                            UserHomeFragment.this.bannedShieldedAsyncTask.execute(new Object[0]);
                        }
                    }).show();
                }
            });
        }
    }

    public void setRefreshBtn() {
        this.backBtn.setVisibility(8);
        if (this.backBtn.getVisibility() == 8) {
            this.refreshBtn.setVisibility(0);
        } else if (this.backBtn.getVisibility() == 0) {
            this.refreshBtn.setVisibility(8);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (!(this.userInfoAsynTask == null || this.userInfoAsynTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.userInfoAsynTask.cancel(true);
        }
        if (!(this.blackAsyncTask == null || this.blackAsyncTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.blackAsyncTask.cancel(true);
        }
        if (this.bannedShieldedAsyncTask != null) {
            this.bannedShieldedAsyncTask.cancel(true);
        }
        if (this.cancelBannedShieldedAsyncTask != null) {
            this.cancelBannedShieldedAsyncTask.cancel(true);
        }
    }

    /* access modifiers changed from: private */
    public void updateView(UserInfoModel userInfoModel2) {
        this.userInfoView.updateUserInfo(userInfoModel2);
        if (userInfoModel2.getUserId() == this.currUserId) {
            if (this.myUserHomeBox != null) {
                this.myUserHomeBox.setVisibility(0);
            }
            if (this.otherUserHomeBox != null) {
                this.otherUserHomeBox.setVisibility(8);
            }
            updateCurrUserView(userInfoModel2);
            return;
        }
        if (this.myUserHomeBox != null) {
            this.myUserHomeBox.setVisibility(8);
        }
        if (this.otherUserHomeBox != null) {
            this.otherUserHomeBox.setVisibility(0);
        }
        updateOtherUserView(userInfoModel2);
    }

    /* access modifiers changed from: private */
    public void updateOtherUserView(UserInfoModel userInfoModel2) {
        this.refreshBtn.setVisibility(8);
        if (userInfoModel2.getBlackStatus() == 0) {
            this.isBlack = false;
            this.addBlackText.setText(this.activity.getResources().getString(this.mcResource.getStringId("mc_forum_add_black")));
        } else {
            this.isBlack = true;
            this.addBlackText.setText(this.activity.getResources().getString(this.mcResource.getStringId("mc_forum_cancel_black")));
        }
        if (userInfoModel2.getIsDelAccounts() == 0) {
            this.isDeltete = false;
            this.accountDelete.setText(this.activity.getResources().getString(this.mcResource.getStringId("mc_forum_user_accounts_delete")));
        } else {
            this.isDeltete = true;
            this.accountDelete.setText(this.activity.getResources().getString(this.mcResource.getStringId("mc_forum_user_accounts_delete_fail")));
        }
        if (userInfoModel2.getIsDelIp() == 0) {
            this.isClose = false;
            this.accountClose.setText(this.activity.getResources().getString(this.mcResource.getStringId("mc_forum_user_accounts_close")));
        } else {
            this.isClose = true;
            this.accountClose.setText(this.activity.getResources().getString(this.mcResource.getStringId("mc_forum_user_accounts_close_fail")));
        }
        if ((this.userService.currentUserIsAdmin() || this.userService.currentUserIsSuperAdmin()) && userInfoModel2.getRoleNum() <= 8) {
            this.accountDelete.setVisibility(0);
            this.boardManageText.setVisibility(0);
            this.accountClose.setVisibility(0);
            this.bannedText.setVisibility(0);
            this.shieldText.setVisibility(0);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_moderator_line")).setVisibility(0);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_accounts_delete_line")).setVisibility(0);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_banned_user_line")).setVisibility(0);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_shieled_line")).setVisibility(0);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_black_line")).setVisibility(0);
            this.reportText.setVisibility(8);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_report_user_line")).setVisibility(8);
        } else if ((this.userService.currentUserIsAdmin() || this.userService.currentUserIsSuperAdmin()) && userInfoModel2.getRoleNum() >= 8) {
            this.reportText.setVisibility(8);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_report_user_line")).setVisibility(8);
            this.accountDelete.setVisibility(8);
            this.boardManageText.setVisibility(8);
            this.accountClose.setVisibility(8);
            this.bannedText.setVisibility(8);
            this.shieldText.setVisibility(8);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_moderator_line")).setVisibility(8);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_accounts_delete_line")).setVisibility(8);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_banned_user_line")).setVisibility(8);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_shieled_line")).setVisibility(8);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_black_line")).setVisibility(8);
        } else {
            this.reportText.setVisibility(0);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_report_user_line")).setVisibility(0);
        }
    }

    private void updateCurrUserView(UserInfoModel userInfoModel2) {
        if ((this.userService.getRoleNum() == 8 || this.userService.getRoleNum() == 16) && userInfoModel2.getRoleNum() >= 8 && MCForumHelper.setFroumManage(this.activity)) {
            this.forumManageText.setVisibility(0);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_user_manage_line")).setVisibility(0);
            return;
        }
        this.forumManageText.setVisibility(8);
        this.view.findViewById(this.mcResource.getViewId("mc_forum_user_manage_line")).setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.refreshBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserHomeFragment.this.refresh();
            }
        });
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserHomeFragment.this.activity.finish();
            }
        });
    }

    private class UserInfoAsynTask extends AsyncTask<Long, Void, UserInfoModel> {
        private UserInfoAsynTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            UserHomeFragment.this.mHandler.post(new Runnable() {
                public void run() {
                    UserHomeFragment.this.loadingBox.setVisibility(0);
                    UserHomeFragment.this.progressBar.show();
                }
            });
        }

        /* access modifiers changed from: protected */
        public UserInfoModel doInBackground(Long... params) {
            return UserHomeFragment.this.userService.getUserInfo(params[0].longValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(UserInfoModel result) {
            UserHomeFragment.this.mHandler.post(new Runnable() {
                public void run() {
                    if (UserHomeFragment.this.loadingBox.getVisibility() == 0) {
                        UserHomeFragment.this.loadingBox.setVisibility(8);
                        UserHomeFragment.this.progressBar.hide();
                    }
                }
            });
            if (result == null) {
                return;
            }
            if (result.getErrorCode() == null || "".equals(result.getErrorCode())) {
                UserInfoModel unused = UserHomeFragment.this.userInfoModel = result;
                UserHomeFragment.this.userInfoModel.setUserId(UserHomeFragment.this.userId);
                UserJsonDBUtil.getInstance(UserHomeFragment.this.activity).savePersonalInfo(UserHomeFragment.this.userId, 1, UserJsonDBHelper.buildPersonalInfo(UserHomeFragment.this.userInfoModel));
                UserHomeFragment.this.updateView(UserHomeFragment.this.userInfoModel);
                return;
            }
            UserHomeFragment.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(UserHomeFragment.this.activity, result.getErrorCode()));
        }
    }

    private class BlackAsyncTask extends AsyncTask<Integer, Void, String> {
        private int blackStatus;

        private BlackAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Integer... params) {
            this.blackStatus = params[0].intValue();
            return UserHomeFragment.this.heartMsgService.setUserBlack(this.blackStatus, UserHomeFragment.this.currUserId, UserHomeFragment.this.userId);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (!StringUtil.isEmpty(result)) {
                UserHomeFragment.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(UserHomeFragment.this.activity, result));
            } else if (result == null) {
                if (UserHomeFragment.this.userInfoModel.getBlackStatus() == 0) {
                    UserHomeFragment.this.warnMessageById("mc_forum_black_user_succ");
                    UserHomeFragment.this.userInfoModel.setBlackStatus(1);
                } else if (UserHomeFragment.this.userInfoModel.getBlackStatus() == 1) {
                    UserHomeFragment.this.warnMessageById("mc_forum_un_black_user_succ");
                    UserHomeFragment.this.userInfoModel.setBlackStatus(0);
                }
                if (this.blackStatus == 1) {
                    boolean unused = UserHomeFragment.this.isBlack = true;
                }
                UserHomeFragment.this.updateOtherUserView(UserHomeFragment.this.userInfoModel);
            }
        }
    }

    public UserTopicClickListener getUserTopicClickListener() {
        return this.userTopicClickListener;
    }

    public void setUserTopicClickListener(UserTopicClickListener userTopicClickListener2) {
        this.userTopicClickListener = userTopicClickListener2;
    }
}
