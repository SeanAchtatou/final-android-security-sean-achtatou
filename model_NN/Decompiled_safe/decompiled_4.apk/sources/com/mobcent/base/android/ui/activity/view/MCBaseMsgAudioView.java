package com.mobcent.base.android.ui.activity.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.SoundModel;

public abstract class MCBaseMsgAudioView extends LinearLayout {
    private LayoutInflater layoutInflater;
    protected String layoutName;
    private MCProgressBar loadingProgressBar;
    private MCResource resource;
    private SoundModel soundModel;
    private TextView timeText;
    private View view;
    private ImageView voiceImg;
    protected String voiceImgName;

    public abstract void initData();

    public MCBaseMsgAudioView(Context context) {
        super(context);
        initData();
        initView(context);
    }

    public MCBaseMsgAudioView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initData();
        initView(context);
    }

    private void initView(Context context) {
        this.layoutInflater = ((Activity) context).getLayoutInflater();
        this.resource = MCResource.getInstance(context);
        this.view = this.layoutInflater.inflate(this.resource.getLayoutId(this.layoutName), (ViewGroup) null);
        this.voiceImg = (ImageView) this.view.findViewById(this.resource.getViewId("mc_forum_msg_audio_img"));
        this.timeText = (TextView) this.view.findViewById(this.resource.getViewId("mc_forum_msg_time_text"));
        this.loadingProgressBar = (MCProgressBar) this.view.findViewById(this.resource.getViewId("mc_forum_audio_loading"));
        addView(this.view);
    }

    public void updateView(SoundModel soundModel2) {
        this.soundModel = soundModel2;
        this.timeText.setText(this.soundModel.getSoundTime() + "\"");
        if (soundModel2.getPalyStatus() == 8) {
            this.voiceImg.setImageResource(this.resource.getDrawableId(this.voiceImgName + "_img0"));
            this.loadingProgressBar.show();
        } else if (soundModel2.getPalyStatus() == 1) {
            this.voiceImg.setImageResource(this.resource.getDrawableId(this.voiceImgName + "_img0"));
            this.loadingProgressBar.hide();
        } else if (soundModel2.getPalyStatus() == 3) {
            this.voiceImg.setImageResource(this.resource.getDrawableId(this.voiceImgName + "_img0"));
            this.loadingProgressBar.hide();
        } else if (soundModel2.getPalyStatus() == 2) {
            this.voiceImg.setImageResource(this.resource.getDrawableId(this.voiceImgName + "_img1"));
            this.loadingProgressBar.hide();
            switch (soundModel2.getPlayProgress() % 3) {
                case 0:
                    this.voiceImg.setImageResource(this.resource.getDrawableId(this.voiceImgName + "_img1"));
                    return;
                case 1:
                    this.voiceImg.setImageResource(this.resource.getDrawableId(this.voiceImgName + "_img2"));
                    return;
                case 2:
                    this.voiceImg.setImageResource(this.resource.getDrawableId(this.voiceImgName + "_img3"));
                    return;
                default:
                    this.voiceImg.setImageResource(this.resource.getDrawableId(this.voiceImgName + "_img0"));
                    return;
            }
        } else if (soundModel2.getPalyStatus() == 4 || soundModel2.getPalyStatus() == 5) {
            this.voiceImg.setImageResource(this.resource.getDrawableId(this.voiceImgName + "_img0"));
            this.loadingProgressBar.hide();
        } else {
            this.voiceImg.setImageResource(this.resource.getDrawableId(this.voiceImgName + "_img0"));
            this.loadingProgressBar.hide();
        }
    }
}
