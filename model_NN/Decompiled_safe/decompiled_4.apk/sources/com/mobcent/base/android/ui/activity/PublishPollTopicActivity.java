package com.mobcent.base.android.ui.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.BasePublishTopicActivityWithAudio;
import com.mobcent.base.android.ui.activity.view.MCPollEditBar;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.model.TopicDraftModel;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

public class PublishPollTopicActivity extends PublishTopicActivity implements MCConstant {
    private final int INIT_POLL_NUM = 2;
    /* access modifiers changed from: private */
    public LinearLayout addBox;
    private Button addBtn;
    private LinearLayout dropdownSelectBox;
    protected boolean isShowPublicSelBox = false;
    /* access modifiers changed from: private */
    public LinearLayout pollBox;
    private String pollCount;
    private EditText pollCountEdt;
    private LinearLayout pollLayout;
    /* access modifiers changed from: private */
    public List<MCPollEditBar> pollList = new ArrayList();
    private List<String> pollListString;
    /* access modifiers changed from: private */
    public int pollType = 1;
    /* access modifiers changed from: private */
    public int pollVisible = 0;
    private PublishPollAsyncTask publishPollAsyncTask;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        this.draftId = 2;
        super.onCreate(savedInstanceState);
        initViewActions();
        getDraft(this.draftId);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.audioTempFileName = this.audioTempFileName.replace("{0}", "2");
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        this.titleLabelText.setText(getResources().getString(this.resource.getStringId("mc_forum_publish_vote")));
        this.contentTransparentLayout.setVisibility(8);
        showPublicSelectBox(false);
        this.dropdownSelectBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_dropdown_select_box"));
        this.pollBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_poll_item"));
        this.pollLayout = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_poll_layout"));
        this.pollLayout.setVisibility(0);
        this.addBtn = (Button) findViewById(this.resource.getViewId("mc_froum_add_btn"));
        this.addBox = (LinearLayout) findViewById(this.resource.getViewId("mc_froum_add_box"));
        this.pollCountEdt = (EditText) findViewById(this.resource.getViewId("mc_forum_poll_count_edt"));
        for (int i = 0; i < 2; i++) {
            MCPollEditBar voteItem = createPollBar(0, "");
            voteItem.hideDeleteBtn();
            this.pollBox.addView(voteItem.getView());
            this.pollList.add(voteItem);
        }
        updatePollItemNum();
    }

    private void initViewActions() {
        this.transparentLayout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (PublishPollTopicActivity.this.transparentLayout.getVisibility() != 0) {
                    return false;
                }
                if (PublishPollTopicActivity.this.pageFromNotBoard) {
                    if (PublishPollTopicActivity.this.isShowBoardBox) {
                        PublishPollTopicActivity.this.showBoardListBox(false);
                    }
                } else if (PublishPollTopicActivity.this.isShowPublicSelBox) {
                    PublishPollTopicActivity.this.showPublicSelectBox(false);
                }
                return false;
            }
        });
        this.addBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PublishPollTopicActivity.this.addPollItemEvent();
            }
        });
        this.addBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PublishPollTopicActivity.this.addPollItemEvent();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void showPublicSelectBox(boolean isShowPublicSelBox2) {
        if (this.isShowPublicSelBox != isShowPublicSelBox2) {
            this.isShowPublicSelBox = isShowPublicSelBox2;
            if (isShowPublicSelBox2) {
                this.transparentLayout.setVisibility(0);
                this.dropdownSelectBox.setVisibility(0);
                return;
            }
            this.transparentLayout.setVisibility(8);
            this.dropdownSelectBox.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void addPollItemEvent() {
        if (this.pollList.size() <= 10) {
            MCPollEditBar voteItem = createPollBar(0, "");
            this.pollBox.addView(voteItem.getView());
            this.pollList.add(voteItem);
            if (this.pollList.size() >= 10) {
                this.addBox.setVisibility(8);
            }
            updateInitPollItemView();
            updatePollItemNum();
        }
    }

    private MCPollEditBar createPollBar(int pollNum, String s) {
        return new MCPollEditBar(this, pollNum, s, new MCPollEditBar.PollEditBarDelegate() {
            public void deletePollBar(View v, MCPollEditBar bar) {
                if (PublishPollTopicActivity.this.pollList.size() > 2) {
                    PublishPollTopicActivity.this.pollBox.removeView(v);
                    PublishPollTopicActivity.this.pollList.remove(bar);
                    PublishPollTopicActivity.this.updatePollItemNum();
                    PublishPollTopicActivity.this.updateInitPollItemView();
                    PublishPollTopicActivity.this.addBox.setVisibility(0);
                }
            }

            public void onEditClick(View v) {
                PublishPollTopicActivity.this.changeKeyboardState(1);
            }
        });
    }

    private List<String> getPollcontent() {
        List<String> pollContet = new ArrayList<>();
        int i = 0;
        while (i < this.pollList.size()) {
            MCPollEditBar pollBar = this.pollList.get(i);
            if (pollBar.getEditContent() == null || pollBar.getEditContent().equals("")) {
                Toast.makeText(this, MCStringBundleUtil.resolveString(MCResource.getInstance(this).getStringId("mc_forum_poll_item_not_null"), (i + 1) + "", this), 0).show();
                return null;
            } else if (pollBar.getEditContent().length() > 20) {
                Toast.makeText(this, getResources().getString(this.resource.getStringId("mc_forum_poll_item_len")), 0).show();
                return null;
            } else {
                pollContet.add(pollBar.getEditContent());
                i++;
            }
        }
        return pollContet;
    }

    /* access modifiers changed from: private */
    public void updatePollItemNum() {
        for (int i = 1; i <= this.pollList.size(); i++) {
            this.pollList.get(i - 1).getNum().setText(i + "");
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    /* access modifiers changed from: private */
    public void updateInitPollItemView() {
        if (!this.pollList.isEmpty() && this.pollList.size() == 2) {
            this.pollList.get(0).hideDeleteBtn();
            this.pollList.get(1).hideDeleteBtn();
        } else if (!this.pollList.isEmpty() && this.pollList.size() > 2) {
            this.pollList.get(0).showDeleteBtn();
            this.pollList.get(1).showDeleteBtn();
        }
    }

    /* access modifiers changed from: protected */
    public boolean publicTopic() {
        getMentionedFriend();
        this.content = this.contentEdit.getText().toString();
        this.pollCount = this.pollCountEdt.getText().toString();
        if (this.boardId <= 0) {
            Toast.makeText(this, getResources().getString(this.resource.getStringId("mc_forum_publish_select_board")), 0).show();
            return false;
        } else if (!checkTitle()) {
            return false;
        } else {
            if (StringUtil.isEmpty(this.pollCount) || !StringUtil.isNumeric(this.pollCount)) {
                warnMessageById("mc_forum_select_poll_count");
                return false;
            }
            if (!StringUtil.isEmpty(this.pollCount)) {
                this.pollType = new Integer(this.pollCount).intValue();
            }
            if (this.pollType > this.pollList.size()) {
                warnMessageById("mc_forum_error_select_poll_count");
                return false;
            }
            this.pollListString = getPollcontent();
            if (StringUtil.isEmpty(this.content) && !this.hasAudio && (this.pollListString == null || this.pollListString.isEmpty())) {
                warnMessageById("mc_forum_publish_min_length_error");
                return false;
            } else if (StringUtil.isEmpty(this.content) || this.content.length() <= 7000) {
                if (this.pollListString != null) {
                    this.canPublishTopic = true;
                    if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.POST, -1) == 0 || this.permService.getPermNum(PermConstant.BOARDS, PermConstant.POST, this.boardId) == 0) {
                        warnMessageByStr(this.resource.getString("mc_forum_permission_cannot_post_toipc"));
                        return false;
                    } else if (this.locationComplete) {
                        if (this.hasAudio) {
                            if (this.uploadAudioFileAsyncTask != null) {
                                this.uploadAudioFileAsyncTask.cancel(true);
                            }
                            this.uploadAudioFileAsyncTask = new BasePublishTopicActivityWithAudio.UploadAudioFileAsyncTask();
                            this.uploadAudioFileAsyncTask.execute(new Void[0]);
                        } else {
                            uploadAudioSucc();
                        }
                    }
                }
                return true;
            } else {
                warnMessageById("mc_forum_publish_max_length_error");
                return false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void uploadAudioSucc() {
        String contentStr = null;
        PostsService postsService = new PostsServiceImpl(this);
        if (this.content != null || this.hasAudio) {
            contentStr = postsService.createPublishTopicJson(this.content.trim(), "ß", "á", this.mentionedFriends, this.audioPath, this.audioDuration);
        }
        if (!postsService.isContainsPic(this.content.trim(), "ß", "á", this.audioPath, this.audioDuration)) {
            String pollContent = postsService.createPublishPollTopicJson(this.pollListString);
            if (this.publishPollAsyncTask != null) {
                this.publishPollAsyncTask.cancel(true);
            }
            this.publishPollAsyncTask = new PublishPollAsyncTask();
            this.publishPollAsyncTask.execute(this.boardId + "", this.title, contentStr, pollContent, this.selectVisibleId + "");
        } else if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.UPLOAD, -1) == 1 && this.permService.getPermNum(PermConstant.BOARDS, PermConstant.UPLOAD, this.boardId) == 1) {
            String pollContent2 = postsService.createPublishPollTopicJson(this.pollListString);
            if (this.publishPollAsyncTask != null) {
                this.publishPollAsyncTask.cancel(true);
            }
            this.publishPollAsyncTask = new PublishPollAsyncTask();
            this.publishPollAsyncTask.execute(this.boardId + "", this.title, contentStr, pollContent2, this.selectVisibleId + "");
        } else {
            warnMessageByStr(this.resource.getString("mc_forum_permission_cannot_upload_pic"));
        }
    }

    class PublishPollAsyncTask extends AsyncTask<String, Void, String> {
        PublishPollAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            ((InputMethodManager) PublishPollTopicActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(PublishPollTopicActivity.this.titleEdit.getWindowToken(), 0);
            ((InputMethodManager) PublishPollTopicActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(PublishPollTopicActivity.this.contentEdit.getWindowToken(), 0);
            try {
                PublishPollTopicActivity.this.showProgressDialog("mc_forum_warn_publish", this);
            } catch (Exception e) {
            }
            PublishPollTopicActivity.this.publishIng = true;
            PublishPollTopicActivity.this.publishBtn.setEnabled(false);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            PostsService postsService = new PostsServiceImpl(PublishPollTopicActivity.this);
            if (params[0] == null || params[1] == null || params[2] == null) {
                return null;
            }
            return postsService.publishPollTopic(Long.parseLong(params[0]), params[1], params[2], PublishPollTopicActivity.this.pollType, PublishPollTopicActivity.this.pollVisible, params[3], MCConstant.POLL_DEADLINE, PublishPollTopicActivity.this.longitude, PublishPollTopicActivity.this.latitude, PublishPollTopicActivity.this.locationStr, PublishPollTopicActivity.this.requireLocation, Integer.parseInt(params[4]));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            PublishPollTopicActivity.this.publishBtn.setEnabled(true);
            PublishPollTopicActivity.this.publishIng = false;
            PublishPollTopicActivity.this.hideProgressDialog();
            if (result == null) {
                if (PublishPollTopicActivity.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.POST, -1) == 2 || PublishPollTopicActivity.this.permService.getPermNum(PermConstant.BOARDS, PermConstant.POST, PublishPollTopicActivity.this.boardId) == 2) {
                    PublishPollTopicActivity.this.warnMessageById("mc_forum_permission_post_after_verify");
                } else {
                    PublishPollTopicActivity.this.warnMessageById("mc_forum_publish_succ");
                }
                PublishPollTopicActivity.this.deleteDraft(PublishPollTopicActivity.this.draftId);
                PublishPollTopicActivity.this.exitNoSaveEvent();
            } else if (!result.equals("")) {
                PublishPollTopicActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(PublishPollTopicActivity.this, result));
                PublishPollTopicActivity.this.saveAsDraft(PublishPollTopicActivity.this.draftId, new TopicDraftModel());
            }
            if (PublishPollTopicActivity.this.isExit) {
                PublishPollTopicActivity.this.exitNoSaveEvent();
            }
        }
    }

    /* access modifiers changed from: protected */
    public TopicDraftModel saveOtherDataToDraft(TopicDraftModel topicDraftModel) {
        TopicDraftModel topicDraftModel2 = super.saveOtherDataToDraft(topicDraftModel);
        List<String> pollContet = new ArrayList<>();
        for (int i = 0; i < this.pollList.size(); i++) {
            String s = this.pollList.get(i).getEditContent();
            if (!StringUtil.isEmpty(s)) {
                pollContet.add(s);
            } else {
                pollContet.add("ß");
            }
        }
        topicDraftModel2.setVoteString(pollContet.toString());
        return topicDraftModel2;
    }

    /* access modifiers changed from: protected */
    public void restoreOtherViewFromDraft(TopicDraftModel draft) {
        super.restoreOtherViewFromDraft(draft);
        this.pollBox.removeAllViews();
        this.pollList.clear();
        try {
            JSONArray ja = new JSONArray(draft.getVoteString());
            int j = ja.length();
            for (int i = 0; i < j; i++) {
                String s = ja.getString(i);
                if (s.equals("ß")) {
                    s = "";
                }
                MCPollEditBar voteItem = createPollBar(0, s);
                this.pollBox.addView(voteItem.getView());
                this.pollList.add(voteItem);
            }
            updatePollItemNum();
            updateInitPollItemView();
            if (this.pollList.size() >= 10) {
                this.addBox.setVisibility(8);
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.pollBox.removeAllViews();
            this.pollList.clear();
            for (int i2 = 0; i2 < 2; i2++) {
                MCPollEditBar voteItem2 = createPollBar(0, "");
                voteItem2.hideDeleteBtn();
                this.pollBox.addView(voteItem2.getView());
                this.pollList.add(voteItem2);
            }
            updatePollItemNum();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.publishPollAsyncTask != null) {
            this.publishPollAsyncTask.cancel(true);
        }
    }
}
