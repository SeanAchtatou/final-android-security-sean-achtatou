package com.mobcent.base.android.ui.activity.adapter;

public interface WheelAdapter {
    String getItem(int i);

    int getItemsCount();

    int getMaximumLength();
}
