package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.mobcent.android.db.McShareSharedPreferencesDB;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserLoginFragmentActivity;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.PhoneUtil;
import java.io.Serializable;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;

public class PlatformLoginActivity extends BaseActivity implements MCConstant {
    /* access modifiers changed from: private */
    public String TAG = "PlatformLoginActivity";
    private ImageButton backBtn;
    /* access modifiers changed from: private */
    public HashMap<String, Serializable> goParam;
    /* access modifiers changed from: private */
    public Class<?> goToActivityClass;
    /* access modifiers changed from: private */
    public String jsonStr;
    private RelativeLayout mcForumLoadingbox;
    /* access modifiers changed from: private */
    public long platformId;
    private WebView platformWebView;
    /* access modifiers changed from: private */
    public MCProgressBar progressBar;
    private ImageButton refreshBtn;
    /* access modifiers changed from: private */
    public UserInfoModel userInfoModel;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            this.platformId = intent.getLongExtra("platformId", 0);
            this.goToActivityClass = (Class) intent.getSerializableExtra(MCConstant.TAG);
            this.goParam = (HashMap) intent.getSerializableExtra(MCConstant.GO_PARAM);
        }
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_platform_webview"));
        this.backBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.refreshBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_web_refresh_btn"));
        this.platformWebView = (WebView) findViewById(this.resource.getViewId("mc_forum_webview"));
        this.mcForumLoadingbox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_loading_box"));
        this.progressBar = (MCProgressBar) findViewById(this.resource.getViewId("mc_forum_web_progressbar"));
        this.platformWebView.getSettings().setJavaScriptEnabled(true);
        this.platformWebView.requestFocus(130);
        initData(this.platformId);
    }

    public void initData(long platformId2) {
        if (this.platformWebView == null) {
            initViews();
        }
        this.platformId = platformId2;
        this.platformWebView.clearView();
        this.platformWebView.clearHistory();
        String forumKey = SharedPreferencesDB.getInstance(this).getForumKey();
        String imsi = PhoneUtil.getIMSI(this);
        String imei = PhoneUtil.getIMEI(this);
        fillWebViewPage(getResources().getString(this.resource.getStringId("mc_forum_domain_platforum_url")) + "wb/login.do" + "?platformId=" + platformId2 + "&forumKey=" + forumKey + "&imsi=" + imsi + "&imei=" + imei + "&version=" + MCConstant.VERSION + "&shareUrl=" + McShareSharedPreferencesDB.getInstance(this).getShareUrl());
    }

    /* access modifiers changed from: protected */
    public void fillWebViewPage(String webUrl) {
        this.platformWebView.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                MCLogUtil.i(PlatformLoginActivity.this.TAG, "url = " + url);
                if (url != null && url.indexOf("success.jsp") > -1) {
                    try {
                        String unused = PlatformLoginActivity.this.jsonStr = new URL(url).getQuery();
                        String unused2 = PlatformLoginActivity.this.jsonStr = URLDecoder.decode(PlatformLoginActivity.this.jsonStr, "UTF-8");
                        MCLogUtil.i(PlatformLoginActivity.this.TAG, "jsonStr = " + PlatformLoginActivity.this.jsonStr);
                    } catch (Exception e) {
                        Toast.makeText(PlatformLoginActivity.this, PlatformLoginActivity.this.getResources().getString(PlatformLoginActivity.this.resource.getStringId("mc_forum_platform_bind_fail_warn")), 0).show();
                        PlatformLoginActivity.this.back();
                    }
                    UserInfoModel unused3 = PlatformLoginActivity.this.userInfoModel = new UserServiceImpl(PlatformLoginActivity.this).parseOpenplatformUserInfo(PlatformLoginActivity.this.jsonStr);
                    if (PlatformLoginActivity.this.userInfoModel == null) {
                        Toast.makeText(PlatformLoginActivity.this, MCForumErrorUtil.convertErrorCode(PlatformLoginActivity.this, PlatformLoginActivity.this.userInfoModel.getErrorCode()), 0).show();
                        PlatformLoginActivity.this.back();
                    } else if (PlatformLoginActivity.this.userInfoModel.isRegister()) {
                        Intent intent = new Intent(PlatformLoginActivity.this, AuthorizationSuccActivity.class);
                        if (PlatformLoginActivity.this.goToActivityClass != null) {
                            intent.putExtra(MCConstant.TAG, PlatformLoginActivity.this.goToActivityClass);
                            intent.putExtra(MCConstant.GO_PARAM, PlatformLoginActivity.this.goParam);
                        }
                        intent.putExtra(MCConstant.USER_INFO_MODEL, PlatformLoginActivity.this.userInfoModel);
                        intent.putExtra("platformId", PlatformLoginActivity.this.platformId);
                        PlatformLoginActivity.this.startActivity(intent);
                        UserLoginFragmentActivity.finishAll();
                        PlatformLoginActivity.this.back();
                    } else {
                        if (PlatformLoginActivity.this.goToActivityClass != null) {
                            PlatformLoginActivity.this.goToTargetActivity();
                        }
                        UserLoginFragmentActivity.finishAll();
                        PlatformLoginActivity.this.back();
                    }
                } else if (url == null || url.indexOf("fail.jsp") <= -1) {
                    PlatformLoginActivity.this.showLoading();
                } else {
                    Toast.makeText(PlatformLoginActivity.this, PlatformLoginActivity.this.getResources().getString(PlatformLoginActivity.this.resource.getStringId("mc_forum_platform_bind_fail_warn")), 0).show();
                    PlatformLoginActivity.this.back();
                }
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                PlatformLoginActivity.this.hideLoading();
                PlatformLoginActivity.this.progressBar.setVisibility(4);
            }
        });
        this.platformWebView.loadUrl(webUrl);
    }

    /* access modifiers changed from: protected */
    public void showLoading() {
        this.mcForumLoadingbox.setVisibility(0);
        this.progressBar.show();
    }

    /* access modifiers changed from: protected */
    public void hideLoading() {
        this.mcForumLoadingbox.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                PlatformLoginActivity.this.back();
            }
        });
        this.refreshBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                PlatformLoginActivity.this.initData(PlatformLoginActivity.this.platformId);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void goToTargetActivity() {
        if (this.goToActivityClass != null) {
            Intent intent = new Intent(this, this.goToActivityClass);
            if (this.goParam != null) {
                for (String key : this.goParam.keySet()) {
                    intent.putExtra(key, this.goParam.get(key));
                }
            }
            startActivity(intent);
        }
    }
}
