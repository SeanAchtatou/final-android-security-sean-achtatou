package com.mobcent.base.android.ui.activity.fragment;

import com.mobcent.base.android.ui.activity.adapter.TopicListAdapter;
import com.mobcent.forum.android.model.TopicModel;
import java.util.List;

public class UserFavoriteTopicFragment extends UserGenericTopicFragment {
    /* access modifiers changed from: protected */
    public void initSpecialViews() {
        this.titleText.setText(this.mcResource.getStringId("mc_forum_my_favorite"));
        this.topicListAdapter = new TopicListAdapter(this.activity, this.topicModelList, "", this.mHandler, this.inflater, this.asyncTaskLoaderImage, getadPostion("mc_forum_user_favorite_position"), this.postsClickListener, getadPostion("mc_forum_user_favorite_position_bottom"), this.currentPage);
    }

    /* access modifiers changed from: protected */
    public List<TopicModel> getGenericTopicModelList(int page, int pageSize) {
        return this.favoriteService.getUserFavoriteList(page, pageSize);
    }
}
