package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.PublishAnnounceActivity;
import com.mobcent.base.android.ui.activity.PublishPollTopicActivity;
import com.mobcent.base.android.ui.activity.PublishTopicActivity;
import com.mobcent.base.android.ui.activity.adapter.HomeViewPagerAdapter;
import com.mobcent.base.android.ui.activity.delegate.OnMCPageChangeListener;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.android.ui.widget.leadview.LeadView;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HomeTopicFragmentActivity extends BaseFragmentActivity {
    private static Set<HomeTopicFragmentActivity> activitys = null;
    protected Button backBtn;
    private TextView boardTab;
    private View boardView;
    private TextView feedsTab;
    private View feedsView;
    private TextView hotTopicTab;
    private View hotTopicView;
    private LayoutInflater inflater;
    private boolean isPush = false;
    /* access modifiers changed from: private */
    public boolean isShowPublishTypeBox = false;
    protected ViewPager mViewPager;
    private TextView newTopicTab;
    private View newTopicView;
    protected TextView publish;
    protected TextView publishAnnounce;
    protected Button publishBtn;
    protected TextView publishPoll;
    public RelativeLayout publishTypeBox;
    private List<TextView> tabvViews;
    protected TextView titleText;
    public RelativeLayout transparentLayout;
    private UserService userService;
    private List<View> views;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (activitys == null) {
            activitys = new HashSet();
        }
        for (HomeTopicFragmentActivity act : activitys) {
            act.finish();
        }
        activitys.add(this);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.inflater = LayoutInflater.from(this);
        this.userService = new UserServiceImpl(this);
        this.isPush = getIntent().getBooleanExtra("push", false);
        if (this.isPush) {
            SharedPreferencesDB.getInstance(this).setPushMsgListJson("");
        }
        this.permService = new PermServiceImpl(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_home_topic_fragment_activity"));
        RelativeLayout rlLayout = (RelativeLayout) findViewById(this.resource.getViewId("home_box"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.publishBtn = (Button) findViewById(this.resource.getViewId("mc_forum_publish_btn"));
        this.titleText = (TextView) findViewById(this.resource.getViewId("mc_forum_user_list_title_btn"));
        this.transparentLayout = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_transparent_box"));
        this.publishTypeBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_publish_type_box"));
        this.publish = (TextView) findViewById(this.resource.getViewId("mc_forum_publish_text"));
        this.publishPoll = (TextView) findViewById(this.resource.getViewId("mc_forum_publish_vote_text"));
        this.publishAnnounce = (TextView) findViewById(this.resource.getViewId("mc_forum_publish_announce_text"));
        if (rlLayout != null) {
            LeadView leadView = new LeadView(getApplicationContext());
            rlLayout.addView(leadView);
            leadView.initViews();
        }
        InitViewPager();
        if (this.userService.getRoleNum() >= 8) {
            this.publishAnnounce.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        String title;
        try {
            title = getString(this.resource.getStringId("app_name"));
        } catch (Exception e) {
            title = null;
        }
        if (StringUtil.isEmpty(title)) {
            title = getString(this.resource.getStringId("mc_forum_home_title"));
        }
        this.titleText.setText(title);
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeTopicFragmentActivity.this.finish();
            }
        });
        this.publishBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!LoginInterceptor.doInterceptorByDialog(HomeTopicFragmentActivity.this, HomeTopicFragmentActivity.this.resource, null, null)) {
                    return;
                }
                if (HomeTopicFragmentActivity.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) != 1) {
                    HomeTopicFragmentActivity.this.warnMessageByStr(HomeTopicFragmentActivity.this.resource.getString("mc_forum_permission_cannot_visit_forum"));
                } else if (HomeTopicFragmentActivity.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.POST, -1) == 0) {
                    HomeTopicFragmentActivity.this.warnMessageByStr(HomeTopicFragmentActivity.this.resource.getString("mc_forum_permission_cannot_post_toipc"));
                } else if (!HomeTopicFragmentActivity.this.isShowPublishTypeBox) {
                    HomeTopicFragmentActivity.this.showPublishTypeBox(true);
                } else {
                    HomeTopicFragmentActivity.this.showPublishTypeBox(false);
                }
            }
        });
        this.publish.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeTopicFragmentActivity.this.showPublishTypeBox(false);
                if (LoginInterceptor.doInterceptorByDialog(HomeTopicFragmentActivity.this, HomeTopicFragmentActivity.this.resource, PublishTopicActivity.class, new HashMap<>())) {
                    HomeTopicFragmentActivity.this.startActivity(new Intent(HomeTopicFragmentActivity.this, PublishTopicActivity.class));
                }
                boolean unused = HomeTopicFragmentActivity.this.isShowPublishTypeBox = false;
            }
        });
        this.publishPoll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeTopicFragmentActivity.this.showPublishTypeBox(false);
                if (LoginInterceptor.doInterceptorByDialog(HomeTopicFragmentActivity.this, HomeTopicFragmentActivity.this.resource, PublishPollTopicActivity.class, new HashMap<>())) {
                    HomeTopicFragmentActivity.this.startActivity(new Intent(HomeTopicFragmentActivity.this, PublishPollTopicActivity.class));
                }
                boolean unused = HomeTopicFragmentActivity.this.isShowPublishTypeBox = false;
            }
        });
        this.transparentLayout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (HomeTopicFragmentActivity.this.transparentLayout.getVisibility() != 0) {
                    return false;
                }
                HomeTopicFragmentActivity.this.showPublishTypeBox(false);
                return false;
            }
        });
        this.publishAnnounce.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeTopicFragmentActivity.this.showPublishTypeBox(false);
                if (LoginInterceptor.doInterceptorByDialog(HomeTopicFragmentActivity.this, HomeTopicFragmentActivity.this.resource, PublishAnnounceActivity.class, new HashMap<>())) {
                    HomeTopicFragmentActivity.this.startActivity(new Intent(HomeTopicFragmentActivity.this, PublishAnnounceActivity.class));
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void showPublishTypeBox(boolean isShowPublishTypeBox2) {
        if (this.isShowPublishTypeBox != isShowPublishTypeBox2) {
            this.isShowPublishTypeBox = isShowPublishTypeBox2;
            if (isShowPublishTypeBox2) {
                this.publishTypeBox.setVisibility(0);
                this.transparentLayout.setVisibility(0);
                return;
            }
            this.publishTypeBox.setVisibility(8);
            this.transparentLayout.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return null;
    }

    private void InitViewPager() {
        this.views = new ArrayList();
        this.inflater = getLayoutInflater();
        this.mViewPager = (ViewPager) findViewById(this.resource.getViewId("vPager"));
        this.boardView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_board_list_fragment_tab"), (ViewGroup) null);
        this.feedsView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_feeds_fragment_tab"), (ViewGroup) null);
        this.hotTopicView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_hot_topic_fragment_tab"), (ViewGroup) null);
        this.newTopicView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_new_topic_fragment_tab"), (ViewGroup) null);
        this.boardTab = (TextView) findViewById(this.resource.getViewId("mc_forum_board_tab_text"));
        this.feedsTab = (TextView) findViewById(this.resource.getViewId("mc_forum_feed_tab_text"));
        this.newTopicTab = (TextView) findViewById(this.resource.getViewId("mc_forum_new_topic_tab_text"));
        this.hotTopicTab = (TextView) findViewById(this.resource.getViewId("mc_forum_hot_topic_tab_text"));
        this.boardTab.setOnClickListener(new MyOnClickListener(0));
        this.feedsTab.setOnClickListener(new MyOnClickListener(1));
        this.newTopicTab.setOnClickListener(new MyOnClickListener(2));
        this.hotTopicTab.setOnClickListener(new MyOnClickListener(3));
        this.views.add(this.boardView);
        this.views.add(this.feedsView);
        this.views.add(this.newTopicView);
        this.views.add(this.hotTopicView);
        this.tabvViews = new ArrayList();
        this.tabvViews.add(this.boardTab);
        this.tabvViews.add(this.feedsTab);
        this.tabvViews.add(this.newTopicTab);
        this.tabvViews.add(this.hotTopicTab);
        selectTab(this.boardTab);
        this.mViewPager.setAdapter(new HomeViewPagerAdapter(this.views));
        this.mViewPager.setCurrentItem(0);
        this.mViewPager.setOnPageChangeListener(new MyOnPageChangeListener(this.tabvViews));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || this.publishTypeBox.getVisibility() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.publishTypeBox.setVisibility(8);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        activitys.remove(this);
    }

    private class MyOnClickListener implements View.OnClickListener {
        private int index = 0;

        public MyOnClickListener(int i) {
            this.index = i;
        }

        public void onClick(View v) {
            HomeTopicFragmentActivity.this.mViewPager.setCurrentItem(this.index);
        }
    }

    public class MyOnPageChangeListener extends OnMCPageChangeListener {
        private List<TextView> textViews;

        public MyOnPageChangeListener(List<TextView> textViews2) {
            this.textViews = textViews2;
        }

        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageSelected(int arg0) {
            super.onPageSelected(arg0);
            HomeTopicFragmentActivity.this.selectTab(this.textViews.get(HomeTopicFragmentActivity.this.mViewPager.getCurrentItem()));
        }
    }

    public void selectTab(TextView textView) {
        this.boardTab.setSelected(false);
        this.feedsTab.setSelected(false);
        this.hotTopicTab.setSelected(false);
        this.newTopicTab.setSelected(false);
        textView.setSelected(true);
    }
}
