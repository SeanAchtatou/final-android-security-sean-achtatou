package com.mobcent.base.android.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.service.MediaService;
import com.mobcent.base.forum.android.util.AudioRecordUtil;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.model.SoundModel;
import com.mobcent.forum.android.util.MCLibIOUtil;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.io.File;

public abstract class BasePublishTopicActivityWithAudio extends BasePublishTopicActivity {
    public static final int MAX_RECORD_TIME = 59;
    public static final int MAX_UV_VALUE = 20;
    protected int audioDuration;
    protected String audioPath;
    protected String audioTempFileName = "audio_record_{0}_temp.amr";
    protected long endTime;
    protected boolean hasAudio = false;
    protected boolean isOverTime = false;
    protected boolean isRecording = false;
    protected Handler mHandler = new Handler();
    protected MediaRecorder mRecorder = null;
    protected MediaPlayer mediaPlayer = null;
    private MediaPlayerBroadCastReceiver mediaPlayerBroadCastReceiver = null;
    private ImageView playImg;
    private RelativeLayout playLayout;
    private TextView playTimeText;
    private Button reRecordBtn;
    protected String recordAbsolutePath;
    /* access modifiers changed from: private */
    public ImageView recordImg;
    private RelativeLayout recordLayout;
    /* access modifiers changed from: private */
    public TextView recordTimeText;
    /* access modifiers changed from: private */
    public Button recordingBtn;
    /* access modifiers changed from: private */
    public SoundModel soundModel;
    protected long startTime;
    protected UploadAudioFileAsyncTask uploadAudioFileAsyncTask;

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        if (this instanceof PublishPollTopicActivity) {
            this.audioTempFileName = this.audioTempFileName.replace("{0}", "2");
        } else if (this instanceof ReplyTopicActivity) {
            this.audioTempFileName = this.audioTempFileName.replace("{0}", "3");
        } else {
            this.audioTempFileName = this.audioTempFileName.replace("{0}", "1");
        }
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        this.recordLayout = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_record_layout"));
        this.playLayout = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_play_layout"));
        this.recordingBtn = (Button) findViewById(this.resource.getViewId("mc_forum_recording_btn"));
        this.reRecordBtn = (Button) findViewById(this.resource.getViewId("mc_forum_re_record_btn"));
        this.recordImg = (ImageView) findViewById(this.resource.getViewId("mc_forum_record_img"));
        this.playImg = (ImageView) findViewById(this.resource.getViewId("mc_forum_play_img"));
        this.recordTimeText = (TextView) findViewById(this.resource.getViewId("mc_forum_record_time_text"));
        this.playTimeText = (TextView) findViewById(this.resource.getViewId("mc_forum_play_time_text"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.takeVoice.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BasePublishTopicActivityWithAudio.this.keyboardState != 3) {
                    BasePublishTopicActivityWithAudio.this.takeVoice.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_ico31_h"));
                    BasePublishTopicActivityWithAudio.this.changeKeyboardState(3);
                    return;
                }
                BasePublishTopicActivityWithAudio.this.changeKeyboardState(0);
                BasePublishTopicActivityWithAudio.this.takeVoice.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_ico31_n"));
            }
        });
        this.recordingBtn.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0) {
                    BasePublishTopicActivityWithAudio.this.recordingBtn.setBackgroundResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_button_h"));
                    BasePublishTopicActivityWithAudio.this.recordingBtn.setText(BasePublishTopicActivityWithAudio.this.resource.getStringId("mc_forum_release_end"));
                    BasePublishTopicActivityWithAudio.this.recording();
                } else if (event.getAction() == 1 || event.getAction() == 3) {
                    BasePublishTopicActivityWithAudio.this.recordingBtn.setText(BasePublishTopicActivityWithAudio.this.resource.getStringId("mc_forum_press_record"));
                    BasePublishTopicActivityWithAudio.this.recordingBtn.setBackgroundResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_button_n"));
                    if (!BasePublishTopicActivityWithAudio.this.isOverTime) {
                        BasePublishTopicActivityWithAudio.this.recorded();
                    }
                }
                return true;
            }
        });
        this.reRecordBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePublishTopicActivityWithAudio.this.reRecord();
            }
        });
        this.playImg.setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                Intent intent = new Intent(BasePublishTopicActivityWithAudio.this, MediaService.class);
                intent.putExtra(MediaService.SERVICE_MODEL, BasePublishTopicActivityWithAudio.this.soundModel);
                intent.putExtra(MediaService.SERVICE_TAG, BasePublishTopicActivityWithAudio.this.toString());
                intent.putExtra(MediaService.NEED_NET_DOWN, false);
                BasePublishTopicActivityWithAudio.this.startService(intent);
            }
        });
    }

    /* access modifiers changed from: private */
    public void recording() {
        this.recordAbsolutePath = getRecordAbsolutePath();
        File f = new File(this.recordAbsolutePath);
        if (f.exists()) {
            f.delete();
        }
        this.mRecorder = AudioRecordUtil.startRecording(this.recordAbsolutePath);
        this.isRecording = true;
        this.isOverTime = false;
        this.startTime = System.currentTimeMillis();
        new TimeThread().start();
        new MonitorThread().start();
    }

    /* access modifiers changed from: private */
    public void recorded() {
        this.hasAudio = true;
        this.endTime = System.currentTimeMillis();
        int time = (int) ((this.endTime - this.startTime) / 1000);
        AudioRecordUtil.stopRecording(this.mRecorder);
        this.mRecorder = null;
        this.isRecording = false;
        if (time < 1) {
            warnMessageById("mc_forum_warn_recorder_min_len");
            return;
        }
        this.audioDuration = time;
        updatePlay();
    }

    /* access modifiers changed from: protected */
    public void updatePlay() {
        this.audioLayout.setVisibility(0);
        this.recordLayout.setVisibility(8);
        this.recordingBtn.setVisibility(8);
        this.playLayout.setVisibility(0);
        this.reRecordBtn.setVisibility(0);
        this.playTimeText.setText(this.audioDuration + "\"");
        this.soundModel = new SoundModel();
        this.soundModel.setSoundPath(this.recordAbsolutePath);
        this.soundModel.setSoundTime((long) this.audioDuration);
        this.soundModel.setPalyStatus(1);
    }

    /* access modifiers changed from: private */
    public void reRecord() {
        this.hasAudio = false;
        this.recordLayout.setVisibility(0);
        this.recordingBtn.setVisibility(0);
        this.playLayout.setVisibility(8);
        this.reRecordBtn.setVisibility(8);
        this.endTime = 0;
        this.startTime = 0;
        this.recordTimeText.setText("");
    }

    /* access modifiers changed from: protected */
    public String getRecordAbsolutePath() {
        return MCLibIOUtil.getAudioCachePath(getApplicationContext()) + this.audioTempFileName;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.uploadAudioFileAsyncTask != null) {
            this.uploadAudioFileAsyncTask.cancel(true);
        }
    }

    private class TimeThread extends Thread {
        private TimeThread() {
        }

        public void run() {
            while (BasePublishTopicActivityWithAudio.this.mRecorder != null && BasePublishTopicActivityWithAudio.this.isRecording) {
                try {
                    BasePublishTopicActivityWithAudio.this.endTime = System.currentTimeMillis();
                    final int time = (int) ((BasePublishTopicActivityWithAudio.this.endTime - BasePublishTopicActivityWithAudio.this.startTime) / 1000);
                    BasePublishTopicActivityWithAudio.this.mHandler.post(new Runnable() {
                        public void run() {
                            BasePublishTopicActivityWithAudio.this.recordTimeText.setText(time + "\"");
                            if (time >= 59) {
                                BasePublishTopicActivityWithAudio.this.isOverTime = true;
                                BasePublishTopicActivityWithAudio.this.warnMessageById("mc_forum_warn_recorder_limit");
                                BasePublishTopicActivityWithAudio.this.recorded();
                            }
                        }
                    });
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class MonitorThread extends Thread {
        private MonitorThread() {
        }

        public void run() {
            while (BasePublishTopicActivityWithAudio.this.mRecorder != null && BasePublishTopicActivityWithAudio.this.isRecording) {
                try {
                    final int uv = (BasePublishTopicActivityWithAudio.this.mRecorder.getMaxAmplitude() * 20) / 32768;
                    BasePublishTopicActivityWithAudio.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (uv == 0) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_1"));
                            } else if (uv == 1) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_2"));
                            } else if (uv == 2) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_3"));
                            } else if (uv == 3) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_4"));
                            } else if (uv == 4) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_5"));
                            } else if (uv == 5) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_6"));
                            } else if (uv == 6) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_7"));
                            } else if (uv == 7) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_8"));
                            } else if (uv == 8) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_9"));
                            } else if (uv == 9) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_10"));
                            } else if (uv == 10) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_11"));
                            } else if (uv == 11) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_12"));
                            } else if (uv == 12) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_13"));
                            } else if (uv == 13) {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_14"));
                            } else {
                                BasePublishTopicActivityWithAudio.this.recordImg.setImageResource(BasePublishTopicActivityWithAudio.this.resource.getDrawableId("mc_forum_recording_img1_15"));
                            }
                        }
                    });
                    Thread.sleep(200);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                BasePublishTopicActivityWithAudio.this.mHandler.post(new Runnable() {
                    public void run() {
                        BasePublishTopicActivityWithAudio.this.recordImg.setBackgroundDrawable(null);
                    }
                });
            }
        }
    }

    protected class UploadAudioFileAsyncTask extends AsyncTask<Void, Void, String> {
        protected UploadAudioFileAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            BasePublishTopicActivityWithAudio.this.showProgressDialog("mc_forum_warn_update_recored", this);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... arg0) {
            return BasePublishTopicActivityWithAudio.this.postsService.uploadAudio(BasePublishTopicActivityWithAudio.this.recordAbsolutePath);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            BasePublishTopicActivityWithAudio.this.hideProgressDialog();
            if (!StringUtil.isEmpty(result)) {
                String path = result;
                if (path.startsWith(BaseReturnCodeConstant.ERROR_CODE)) {
                    String errorCode = path.substring(path.indexOf(BaseReturnCodeConstant.ERROR_CODE) + 1, path.length());
                    if (errorCode != null && !errorCode.equals("BasePublishTopicActivityWithAudio")) {
                        BasePublishTopicActivityWithAudio.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(BasePublishTopicActivityWithAudio.this, errorCode));
                        BasePublishTopicActivityWithAudio.this.uploadAudioFail();
                        return;
                    }
                    return;
                }
                BasePublishTopicActivityWithAudio.this.audioPath = path;
                BasePublishTopicActivityWithAudio.this.warnMessageById("mc_forum_warn_update_succ");
                BasePublishTopicActivityWithAudio.this.changeKeyboardState(0);
                BasePublishTopicActivityWithAudio.this.clearAudioTempFile();
                BasePublishTopicActivityWithAudio.this.uploadAudioSucc();
                return;
            }
            BasePublishTopicActivityWithAudio.this.warnMessageById("mc_forum_warn_update_fail");
            BasePublishTopicActivityWithAudio.this.uploadAudioFail();
        }
    }

    /* access modifiers changed from: protected */
    public void uploadAudioSucc() {
    }

    /* access modifiers changed from: private */
    public void uploadAudioFail() {
        changeKeyboardState(0);
    }

    /* access modifiers changed from: private */
    public void clearAudioTempFile() {
        File f = new File(this.recordAbsolutePath);
        if (f.exists()) {
            f.delete();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Intent intent = new Intent(this, MediaService.class);
        intent.putExtra(MediaService.SERVICE_TAG, toString());
        intent.putExtra(MediaService.SERVICE_STATUS, 6);
        startService(intent);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.mediaPlayerBroadCastReceiver == null) {
            this.mediaPlayerBroadCastReceiver = new MediaPlayerBroadCastReceiver();
        }
        registerReceiver(this.mediaPlayerBroadCastReceiver, new IntentFilter(MediaService.INTENT_TAG + getPackageName()));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.mediaPlayerBroadCastReceiver != null) {
            unregisterReceiver(this.mediaPlayerBroadCastReceiver);
        }
    }

    public class MediaPlayerBroadCastReceiver extends BroadcastReceiver {
        private SoundModel currSoundModel = null;
        private String tag;

        public MediaPlayerBroadCastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            MCLogUtil.i("BasePublishTopicActivityWithAudio", "onReceive ");
            this.tag = intent.getStringExtra(MediaService.SERVICE_TAG);
            if (this.tag != null && this.tag.equals(BasePublishTopicActivityWithAudio.this.toString())) {
                this.currSoundModel = (SoundModel) intent.getSerializableExtra(MediaService.SERVICE_MODEL);
                BasePublishTopicActivityWithAudio.this.updatePlayImg(this.currSoundModel);
            }
        }
    }

    /* access modifiers changed from: private */
    public void updatePlayImg(SoundModel currSoundModel) {
        this.soundModel = null;
        this.soundModel = currSoundModel;
        if (this.soundModel.getPalyStatus() == 1) {
            this.playImg.setImageResource(this.resource.getDrawableId("mc_forum_recording_img3"));
        } else if (this.soundModel.getPalyStatus() == 3) {
            this.playImg.setImageResource(this.resource.getDrawableId("mc_forum_recording_img3"));
        } else if (this.soundModel.getPalyStatus() == 2) {
            int progress = this.soundModel.getPlayProgress() % 5;
            MCLogUtil.i("BasePublishTopicActivityWithAudio", "progress = " + progress);
            switch (progress) {
                case 0:
                    this.playImg.setImageResource(this.resource.getDrawableId("mc_forum_recording_img4_1"));
                    return;
                case 1:
                    this.playImg.setImageResource(this.resource.getDrawableId("mc_forum_recording_img4_2"));
                    return;
                case 2:
                    this.playImg.setImageResource(this.resource.getDrawableId("mc_forum_recording_img4_3"));
                    return;
                case 3:
                    this.playImg.setImageResource(this.resource.getDrawableId("mc_forum_recording_img4_4"));
                    return;
                case 4:
                    this.playImg.setImageResource(this.resource.getDrawableId("mc_forum_recording_img4_5"));
                    return;
                default:
                    this.playImg.setImageResource(this.resource.getDrawableId("mc_forum_recording_img4_1"));
                    return;
            }
        } else if (this.soundModel.getPalyStatus() == 4 || this.soundModel.getPalyStatus() == 5) {
            this.playImg.setImageResource(this.resource.getDrawableId("mc_forum_recording_img3"));
        } else {
            this.playImg.setImageResource(this.resource.getDrawableId("mc_forum_recording_img3"));
        }
    }
}
