package com.mobcent.base.android.ui.activity.fragment;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.mobcent.ad.android.ui.widget.manager.AdManager;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.FriendAdapter;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public abstract class UserFriendsFrament extends BaseListViewFragment implements MCConstant {
    public static final String TAG = "FriendFrament";
    public static final UserFollowFragment UserFriendsFrament = null;
    protected Activity activity;
    protected int adPosition;
    protected FriendAdapter adapter;
    protected int currentPage = 1;
    private FriendAdapter.FriendsClickListener friendsClickListener;
    protected boolean isLocal;
    private GetMoreUserFriendListTask moreTask;
    protected int pageSize = 20;
    protected PullToRefreshListView pullToRefreshListView;
    private RefreshUserFriendListTask refreshTask;
    /* access modifiers changed from: private */
    public List<String> refreshimgUrls;
    protected long userId;
    protected List<UserInfoModel> userInfoList;
    protected UserService userService;

    /* access modifiers changed from: protected */
    public abstract List<UserInfoModel> getUserFriendsList();

    public FriendAdapter.FriendsClickListener getFriendsClickListener() {
        return this.friendsClickListener;
    }

    public void setFriendsClickListener(FriendAdapter.FriendsClickListener friendsClickListener2) {
        this.friendsClickListener = friendsClickListener2;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.activity = getActivity();
        this.userInfoList = new ArrayList();
        this.refreshimgUrls = new ArrayList();
        this.activity = getActivity();
        this.userService = new UserServiceImpl(this.activity);
        if (getUserId() == 0) {
            this.userId = this.userService.getLoginUserId();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.pullToRefreshListView.onRefresh(true);
        return this.view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(this.mcResource.getLayoutId("mc_forum_user_friends_fragment"), container, false);
        this.pullToRefreshListView = (PullToRefreshListView) this.view.findViewById(this.mcResource.getViewId("mc_forum_lv"));
        this.adapter = new FriendAdapter(this.activity, this.userInfoList, this.mHandler, this.asyncTaskLoaderImage, inflater, 1, this.adPosition, this, this.currentPage);
        this.adapter.setClickListener(this.friendsClickListener);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.pullToRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                UserFriendsFrament.this.onRefreshs();
            }
        });
        this.pullToRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                UserFriendsFrament.this.onLoadMore();
            }
        });
        this.pullToRefreshListView.setScrollListener(this.listOnScrollListener);
    }

    public void onRefreshs() {
        if (!(this.refreshTask == null || this.refreshTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.refreshTask.cancel(true);
        }
        this.refreshTask = new RefreshUserFriendListTask();
        this.refreshTask.execute(Integer.valueOf(this.pageSize));
        this.isLocal = false;
    }

    public void onLoadMore() {
        if (!(this.moreTask == null || this.moreTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.moreTask.cancel(true);
        }
        this.moreTask = new GetMoreUserFriendListTask();
        this.moreTask.execute(Integer.valueOf(this.pageSize));
        this.isLocal = false;
    }

    public void onDestroy() {
        super.onDestroy();
        if (!(this.refreshTask == null || this.refreshTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.refreshTask.cancel(true);
        }
        if (!(this.moreTask == null || this.moreTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.moreTask.cancel(true);
        }
        this.adapter.destory();
        AdManager.getInstance().recyclAdByTag(TAG);
    }

    /* access modifiers changed from: private */
    public List<String> getRefreshImgUrl(List<UserInfoModel> result) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < this.userInfoList.size(); i++) {
            UserInfoModel model = this.userInfoList.get(i);
            if (!isExit(model, result)) {
                list.add(AsyncTaskLoaderImage.formatUrl(model.getIcon(), "100x100"));
            }
        }
        return list;
    }

    private boolean isExit(UserInfoModel model, List<UserInfoModel> result) {
        int j = result.size();
        for (int i = 0; i < j; i++) {
            UserInfoModel model2 = result.get(i);
            if (!StringUtil.isEmpty(model.getIcon()) && !StringUtil.isEmpty(model2.getIcon()) && model.getIcon().equals(model2.getIcon())) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public List<String> getImageURL(int start, int end) {
        List<String> imgUrls = new ArrayList<>();
        for (int i = start; i < end; i++) {
            imgUrls.add(AsyncTaskLoaderImage.formatUrl(this.userInfoList.get(i).getIcon(), "100x100"));
        }
        return imgUrls;
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return getImageURL(0, this.userInfoList.size());
    }

    private class RefreshUserFriendListTask extends AsyncTask<Integer, Void, List<UserInfoModel>> {
        private RefreshUserFriendListTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<UserInfoModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            UserFriendsFrament.this.currentPage = 1;
            if (UserFriendsFrament.this.refreshimgUrls != null && !UserFriendsFrament.this.refreshimgUrls.isEmpty() && UserFriendsFrament.this.refreshimgUrls.size() > 0) {
                UserFriendsFrament.this.asyncTaskLoaderImage.recycleBitmaps(UserFriendsFrament.this.refreshimgUrls);
            }
        }

        /* access modifiers changed from: protected */
        public List<UserInfoModel> doInBackground(Integer... params) {
            return UserFriendsFrament.this.getUserFriendsList();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<UserInfoModel> result) {
            super.onPostExecute((Object) result);
            UserFriendsFrament.this.pullToRefreshListView.onRefreshComplete();
            UserFriendsFrament.this.pullToRefreshListView.setSelection(0);
            if (result == null) {
                UserFriendsFrament.this.pullToRefreshListView.onBottomRefreshComplete(3, UserFriendsFrament.this.getString(UserFriendsFrament.this.mcResource.getStringId("mc_forum_no_follow_user")));
            } else if (result.isEmpty()) {
                UserFriendsFrament.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                Toast.makeText(UserFriendsFrament.this.activity, MCForumErrorUtil.convertErrorCode(UserFriendsFrament.this.activity, result.get(0).getErrorCode()), 0).show();
                UserFriendsFrament.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else {
                List unused = UserFriendsFrament.this.refreshimgUrls = UserFriendsFrament.this.getRefreshImgUrl(result);
                AdManager.getInstance().recyclAdByTag(UserFriendsFrament.TAG);
                UserFriendsFrament.this.adapter.setUserInfoList(result);
                UserFriendsFrament.this.adapter.notifyDataSetInvalidated();
                UserFriendsFrament.this.adapter.notifyDataSetChanged();
                UserFriendsFrament.this.pullToRefreshListView.onRefreshComplete();
                if (result.get(0).getHasNext() == 1) {
                    UserFriendsFrament.this.pullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    UserFriendsFrament.this.pullToRefreshListView.onBottomRefreshComplete(3);
                }
                UserFriendsFrament.this.userInfoList = result;
            }
        }
    }

    private class GetMoreUserFriendListTask extends AsyncTask<Integer, Void, List<UserInfoModel>> {
        private GetMoreUserFriendListTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<UserInfoModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            UserFriendsFrament.this.currentPage++;
        }

        /* access modifiers changed from: protected */
        public List<UserInfoModel> doInBackground(Integer... params) {
            return UserFriendsFrament.this.getUserFriendsList();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<UserInfoModel> result) {
            if (result == null) {
                UserFriendsFrament.this.currentPage--;
            } else if (result.isEmpty()) {
                UserFriendsFrament.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                UserFriendsFrament.this.currentPage--;
                Toast.makeText(UserFriendsFrament.this.activity, MCForumErrorUtil.convertErrorCode(UserFriendsFrament.this.activity, result.get(0).getErrorCode()), 0).show();
                UserFriendsFrament.this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                List<UserInfoModel> tempList = new ArrayList<>();
                tempList.addAll(UserFriendsFrament.this.userInfoList);
                tempList.addAll(result);
                AdManager.getInstance().recyclAdByTag(UserFriendsFrament.TAG);
                UserFriendsFrament.this.adapter.setUserInfoList(tempList);
                UserFriendsFrament.this.adapter.notifyDataSetInvalidated();
                UserFriendsFrament.this.adapter.notifyDataSetChanged();
                if (result.get(0).getHasNext() == 1) {
                    UserFriendsFrament.this.pullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    UserFriendsFrament.this.pullToRefreshListView.onBottomRefreshComplete(3);
                }
                UserFriendsFrament.this.userInfoList = tempList;
            }
        }
    }

    public int getAdPosition() {
        return this.adPosition;
    }

    public void setAdPosition(int adPosition2) {
        this.adPosition = adPosition2;
    }
}
