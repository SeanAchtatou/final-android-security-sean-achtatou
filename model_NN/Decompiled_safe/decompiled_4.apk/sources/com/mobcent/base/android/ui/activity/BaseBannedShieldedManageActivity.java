package com.mobcent.base.android.ui.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.BannedShieldedUserListAdapter;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCForumSearchUtil;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseBannedShieldedManageActivity extends BaseListViewActivity implements MCConstant {
    protected BannedShieldedUserListAdapter adapter;
    private Button backBtn;
    protected int boardId;
    protected int currentPage = 1;
    protected LayoutInflater inflater;
    private GetMoreBannedUserInfoAsyncTask moreBannedUserInfoAsyncTask;
    protected int pageSize = 20;
    protected PullToRefreshListView pullToRefreshListView;
    private RefreshBannedUserInfoAsyncTask refreshBannedUserInfoAsyncTask;
    /* access modifiers changed from: private */
    public List<String> refreshimgUrls;
    protected Button searchBtn;
    protected EditText searchEdit;
    protected TextView titleText;
    protected int type;
    protected List<UserInfoModel> userInfoList;

    public abstract List<UserInfoModel> getBannedShieldedUser(int i, int i2, int i3, int i4);

    public abstract List<UserInfoModel> searchBannedShieldedUser(int i, int i2, String str, int i3, int i4);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.pullToRefreshListView.onRefreshWithOutListener();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.refreshimgUrls = new ArrayList();
        this.inflater = LayoutInflater.from(this);
        this.userInfoList = new ArrayList();
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_banned_shielded_manage_activity"));
        this.titleText = (TextView) findViewById(this.resource.getViewId("mc_forum_title_text"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.searchBtn = (Button) findViewById(this.resource.getViewId("mc_forum_search_btn"));
        this.searchEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_search_edit"));
        this.pullToRefreshListView = (PullToRefreshListView) findViewById(this.resource.getViewId("mc_forum_lv"));
        this.adapter = new BannedShieldedUserListAdapter(this, this.type, this.boardId, this.userInfoList, this.inflater, this.asyncTaskLoaderImage, this.mHandler);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaseBannedShieldedManageActivity.this.back();
            }
        });
        this.searchBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ((InputMethodManager) BaseBannedShieldedManageActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(BaseBannedShieldedManageActivity.this.searchEdit.getWindowToken(), 0);
                BaseBannedShieldedManageActivity.this.searchClick();
            }
        });
        this.searchEdit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaseBannedShieldedManageActivity.this.searchEdit.setTextColor(BaseBannedShieldedManageActivity.this.resource.getColorId("mc_forum_input_normal_text_color"));
                Editable editable = BaseBannedShieldedManageActivity.this.searchEdit.getText();
                Selection.setSelection(editable, editable.length());
            }
        });
        this.searchEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66) {
                    BaseBannedShieldedManageActivity.this.searchEdit.setFocusable(false);
                    BaseBannedShieldedManageActivity.this.searchEdit.setFocusableInTouchMode(true);
                    BaseBannedShieldedManageActivity.this.hideSoftKeyboard();
                    BaseBannedShieldedManageActivity.this.searchClick();
                }
                BaseBannedShieldedManageActivity.this.searchEdit.setFocusable(true);
                return false;
            }
        });
        this.pullToRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                BaseBannedShieldedManageActivity.this.onRefreshs();
            }
        });
        this.pullToRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                BaseBannedShieldedManageActivity.this.onLoadMore();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void back() {
        if (this.myDialog == null || !this.myDialog.isShowing()) {
            finish();
        } else {
            hideProgressDialog();
        }
    }

    public void onRefreshs() {
        if (this.refreshBannedUserInfoAsyncTask != null) {
            this.refreshBannedUserInfoAsyncTask.cancel(true);
        }
        this.refreshBannedUserInfoAsyncTask = new RefreshBannedUserInfoAsyncTask();
        this.refreshBannedUserInfoAsyncTask.execute(Integer.valueOf(this.pageSize));
    }

    public void onLoadMore() {
        if (this.moreBannedUserInfoAsyncTask != null) {
            this.moreBannedUserInfoAsyncTask.cancel(true);
        }
        this.moreBannedUserInfoAsyncTask = new GetMoreBannedUserInfoAsyncTask();
        this.moreBannedUserInfoAsyncTask.execute(Integer.valueOf(this.pageSize));
    }

    public void searchClick() {
        String searchEditStr = this.searchEdit.getText().toString();
        if ("".equals(searchEditStr)) {
            Toast.makeText(this, this.resource.getStringId("mc_forum_no_keywords"), 0).show();
        } else if (searchEditStr.length() > 10) {
            Toast.makeText(this, this.resource.getStringId("mc_forum_keywords_length_warn"), 0).show();
        } else if (MCForumSearchUtil.isSearchEnable()) {
            this.userInfoList = new ArrayList();
            this.adapter.setUserInfoList(this.userInfoList);
            this.adapter.notifyDataSetInvalidated();
            this.adapter.notifyDataSetChanged();
            this.pullToRefreshListView.onRefreshWithOutListener();
            onRefreshs();
        } else {
            Toast.makeText(this, this.resource.getStringId("mc_forum_search_over_times"), 0).show();
        }
    }

    private class RefreshBannedUserInfoAsyncTask extends AsyncTask<Object, Void, List<UserInfoModel>> {
        private RefreshBannedUserInfoAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<UserInfoModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            BaseBannedShieldedManageActivity.this.currentPage = 1;
            BaseBannedShieldedManageActivity.this.pullToRefreshListView.setSelection(0);
            if (BaseBannedShieldedManageActivity.this.refreshimgUrls != null && !BaseBannedShieldedManageActivity.this.refreshimgUrls.isEmpty() && BaseBannedShieldedManageActivity.this.refreshimgUrls.size() > 0) {
                BaseBannedShieldedManageActivity.this.asyncTaskLoaderImage.recycleBitmaps(BaseBannedShieldedManageActivity.this.refreshimgUrls);
            }
        }

        /* access modifiers changed from: protected */
        public List<UserInfoModel> doInBackground(Object... params) {
            int pageSize = ((Integer) params[0]).intValue();
            BaseBannedShieldedManageActivity.this.userInfoList = BaseBannedShieldedManageActivity.this.getBannedShieldedUser(BaseBannedShieldedManageActivity.this.type, BaseBannedShieldedManageActivity.this.boardId, BaseBannedShieldedManageActivity.this.currentPage, pageSize);
            return BaseBannedShieldedManageActivity.this.userInfoList;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<UserInfoModel> userInfoModelList) {
            BaseBannedShieldedManageActivity.this.pullToRefreshListView.onRefreshComplete();
            if (userInfoModelList == null) {
                BaseBannedShieldedManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else if (userInfoModelList.isEmpty()) {
                BaseBannedShieldedManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(userInfoModelList.get(0).getErrorCode())) {
                Toast.makeText(BaseBannedShieldedManageActivity.this, MCForumErrorUtil.convertErrorCode(BaseBannedShieldedManageActivity.this, userInfoModelList.get(0).getErrorCode()), 0).show();
                BaseBannedShieldedManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                List unused = BaseBannedShieldedManageActivity.this.refreshimgUrls = BaseBannedShieldedManageActivity.this.getRefreshImgUrl(BaseBannedShieldedManageActivity.this.userInfoList);
                BaseBannedShieldedManageActivity.this.updateDataOnPostExecute(userInfoModelList);
                BaseBannedShieldedManageActivity.this.adapter.setUserInfoList(userInfoModelList);
                BaseBannedShieldedManageActivity.this.adapter.notifyDataSetInvalidated();
                BaseBannedShieldedManageActivity.this.adapter.notifyDataSetChanged();
                if (userInfoModelList.get(0).getHasNext() == 1) {
                    BaseBannedShieldedManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    BaseBannedShieldedManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
                }
                BaseBannedShieldedManageActivity.this.userInfoList = userInfoModelList;
            }
        }
    }

    private class GetMoreBannedUserInfoAsyncTask extends AsyncTask<Object, Void, List<UserInfoModel>> {
        private GetMoreBannedUserInfoAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<UserInfoModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            BaseBannedShieldedManageActivity.this.currentPage++;
        }

        /* access modifiers changed from: protected */
        public List<UserInfoModel> doInBackground(Object... params) {
            int pageSize = ((Integer) params[0]).intValue();
            BaseBannedShieldedManageActivity.this.userInfoList = BaseBannedShieldedManageActivity.this.getBannedShieldedUser(BaseBannedShieldedManageActivity.this.type, BaseBannedShieldedManageActivity.this.boardId, BaseBannedShieldedManageActivity.this.currentPage, pageSize);
            return BaseBannedShieldedManageActivity.this.userInfoList;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<UserInfoModel> userInfoModelList) {
            if (userInfoModelList == null) {
                BaseBannedShieldedManageActivity.this.currentPage--;
            } else if (userInfoModelList.isEmpty()) {
                BaseBannedShieldedManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(userInfoModelList.get(0).getErrorCode())) {
                BaseBannedShieldedManageActivity.this.currentPage--;
                Toast.makeText(BaseBannedShieldedManageActivity.this, MCForumErrorUtil.convertErrorCode(BaseBannedShieldedManageActivity.this, userInfoModelList.get(0).getErrorCode()), 0).show();
                BaseBannedShieldedManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                BaseBannedShieldedManageActivity.this.updateDataOnPostExecute(userInfoModelList);
                List<UserInfoModel> tempList = new ArrayList<>();
                tempList.addAll(BaseBannedShieldedManageActivity.this.userInfoList);
                tempList.addAll(userInfoModelList);
                BaseBannedShieldedManageActivity.this.adapter.setUserInfoList(tempList);
                BaseBannedShieldedManageActivity.this.adapter.notifyDataSetInvalidated();
                BaseBannedShieldedManageActivity.this.adapter.notifyDataSetChanged();
                if (userInfoModelList.get(0).getHasNext() == 1) {
                    BaseBannedShieldedManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    BaseBannedShieldedManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
                }
                BaseBannedShieldedManageActivity.this.userInfoList = tempList;
            }
        }
    }

    public void updateDataOnPostExecute(List<UserInfoModel> result) {
        List<UserInfoModel> userInfoList2 = result;
        int size = userInfoList2.size();
        for (int i = 0; i < size; i++) {
            userInfoList2.get(i).setPage(this.currentPage);
        }
    }

    /* access modifiers changed from: private */
    public List<String> getRefreshImgUrl(List<UserInfoModel> result) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < this.userInfoList.size(); i++) {
            UserInfoModel model = this.userInfoList.get(i);
            if (!isExit(model, result)) {
                list.add(AsyncTaskLoaderImage.formatUrl(model.getIcon(), "100x100"));
            }
        }
        return list;
    }

    private boolean isExit(UserInfoModel model, List<UserInfoModel> result) {
        int j = result.size();
        for (int i = 0; i < j; i++) {
            UserInfoModel model2 = result.get(i);
            if (!StringUtil.isEmpty(model.getIcon()) && !StringUtil.isEmpty(model2.getIcon()) && model.getIcon().equals(model2.getIcon())) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public List<String> getImageURL(int start, int end) {
        List<String> imgUrls = new ArrayList<>();
        for (int i = start; i < end; i++) {
            imgUrls.add(AsyncTaskLoaderImage.formatUrl(this.userInfoList.get(i).getIcon(), "100x100"));
        }
        return imgUrls;
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return getImageURL(0, this.userInfoList.size());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.refreshBannedUserInfoAsyncTask != null) {
            this.refreshBannedUserInfoAsyncTask.cancel(true);
        }
        if (this.moreBannedUserInfoAsyncTask != null) {
            this.moreBannedUserInfoAsyncTask.cancel(true);
        }
    }
}
