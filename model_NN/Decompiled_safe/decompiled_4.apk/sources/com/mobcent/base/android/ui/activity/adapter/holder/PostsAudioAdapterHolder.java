package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;

public class PostsAudioAdapterHolder {
    private MCProgressBar downProgressBar;
    private ImageView playStautsImg;
    private ImageView playingImg;
    private TextView timeText;

    public ImageView getPlayStautsImg() {
        return this.playStautsImg;
    }

    public void setPlayStautsImg(ImageView playStautsImg2) {
        this.playStautsImg = playStautsImg2;
    }

    public ImageView getPlayingImg() {
        return this.playingImg;
    }

    public void setPlayingImg(ImageView playingImg2) {
        this.playingImg = playingImg2;
    }

    public TextView getTimeText() {
        return this.timeText;
    }

    public void setTimeText(TextView timeText2) {
        this.timeText = timeText2;
    }

    public MCProgressBar getDownProgressBar() {
        return this.downProgressBar;
    }

    public void setDownProgressBar(MCProgressBar downProgressBar2) {
        this.downProgressBar = downProgressBar2;
    }
}
