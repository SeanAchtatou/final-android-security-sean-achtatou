package com.mobcent.base.android.ui.activity.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.InformantModel;
import com.mobcent.forum.android.util.DateUtil;

public class MCReportTopicManageBar {
    private InformantModel informantModel;
    private LayoutInflater layoutInflater;
    private ImageView lineImageView;
    private TextView nickNameText;
    private TextView reasonText;
    private TextView reportTime;
    private MCResource resource;
    private View view;

    public MCReportTopicManageBar(LayoutInflater layoutInflater2, MCResource resource2, InformantModel informantModel2) {
        this.layoutInflater = layoutInflater2;
        this.resource = resource2;
        this.informantModel = informantModel2;
        initViews();
    }

    private void initViews() {
        this.view = this.layoutInflater.inflate(this.resource.getLayoutId("mc_forum_report_topic_manage_view"), (ViewGroup) null);
        this.nickNameText = (TextView) this.view.findViewById(this.resource.getViewId("mc_forum_nick_name_text"));
        this.reasonText = (TextView) this.view.findViewById(this.resource.getViewId("mc_forum_reason_text"));
        this.reportTime = (TextView) this.view.findViewById(this.resource.getViewId("mc_forum_report_topic_time_text"));
        this.lineImageView = (ImageView) this.view.findViewById(this.resource.getViewId("mc_forum_line"));
        this.nickNameText.setText(this.informantModel.getNickName());
        this.reasonText.setText(this.informantModel.getReason());
        this.reportTime.setText(DateUtil.getFormatTime(this.informantModel.getReportTime()));
    }

    public void hideLine() {
        this.lineImageView.setVisibility(8);
    }

    public View getView() {
        return this.view;
    }

    public void setView(View view2) {
        this.view = view2;
    }
}
