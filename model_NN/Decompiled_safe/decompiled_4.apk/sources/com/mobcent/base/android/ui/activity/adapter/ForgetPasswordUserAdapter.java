package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.ArrayList;
import java.util.List;

public class ForgetPasswordUserAdapter extends BaseAdapter {
    private int REG_QQ = 2;
    private int REG_RENREN = 4;
    private int REG_SINA = 1;
    private Context context;
    private LayoutInflater inflater;
    private MCResource resource;
    private List<UserInfoModel> userList;

    public ForgetPasswordUserAdapter(Context context2) {
        this.inflater = LayoutInflater.from(context2);
        this.resource = MCResource.getInstance(context2);
        this.userList = new ArrayList();
        this.context = context2;
    }

    public int getCount() {
        return this.userList.size();
    }

    public Object getItem(int position) {
        return this.userList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_forget_password_item"), (ViewGroup) null);
        UserInfoModel infoModel = this.userList.get(position);
        TextView userNameText = (TextView) convertView2.findViewById(this.resource.getViewId("mc_forum_reg_user_name_text"));
        TextView emailText = (TextView) convertView2.findViewById(this.resource.getViewId("mc_forum_reg_user_email_text"));
        ImageView goImg = (ImageView) convertView2.findViewById(this.resource.getViewId("mc_forum_go_img"));
        if (infoModel.getRegSource() == 0) {
            userNameText.setText(infoModel.getNickname());
            emailText.setText(infoModel.getEmail());
            goImg.setVisibility(0);
        } else if (infoModel.getRegSource() == this.REG_SINA) {
            userNameText.setText(infoModel.getNickname());
            emailText.setText(this.context.getResources().getString(this.resource.getStringId("mc_forum_forget_password_sina")));
            goImg.setVisibility(8);
        } else if (infoModel.getRegSource() == this.REG_QQ) {
            userNameText.setText(infoModel.getNickname());
            emailText.setText(this.context.getResources().getString(this.resource.getStringId("mc_forum_forget_password_qq")));
            goImg.setVisibility(8);
        } else if (infoModel.getRegSource() == this.REG_RENREN) {
            userNameText.setText(infoModel.getNickname());
            emailText.setText(this.context.getResources().getString(this.resource.getStringId("mc_forum_forget_password_renren")));
            goImg.setVisibility(8);
        }
        return convertView2;
    }

    public List<UserInfoModel> getUseList() {
        return this.userList;
    }

    public void setUseList(List<UserInfoModel> useList) {
        this.userList = useList;
    }
}
