package com.mobcent.base.android.ui.activity.helper;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.PostsActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.HomeTopicFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.UserLoginFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.ForumInitializeHelper;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCThemeResource;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.PostsNoticeModel;
import com.mobcent.forum.android.model.PushMessageModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserInfoModel;

public abstract class ForumConfig {
    private boolean isApplicationVisible = true;

    public abstract void gotoUserInfo(Activity activity, MCResource mCResource, long j);

    public abstract void initNav(Activity activity, MCResource mCResource);

    public abstract void initNav(Activity activity, MCResource mCResource, MCThemeResource mCThemeResource);

    public abstract Intent onBeatClickListener(Context context, int i);

    public abstract void onLogoutClick(Activity activity);

    public abstract void onMessageClick(Activity activity);

    public abstract void onPostsClick(Activity activity, MCResource mCResource, View view, PostsNoticeModel postsNoticeModel);

    public abstract void onPostsClick(Activity activity, MCResource mCResource, View view, TopicModel topicModel, String str);

    public abstract void onReplyClick(Activity activity, MCResource mCResource, View view, PostsNoticeModel postsNoticeModel, long j);

    public abstract void onTopicClick(Activity activity, MCResource mCResource, View view, int i, long j, UserInfoModel userInfoModel);

    public void setApplicationVisible(boolean isApplicationVisible2) {
        this.isApplicationVisible = isApplicationVisible2;
    }

    public boolean getApplicationVisible() {
        return this.isApplicationVisible;
    }

    public boolean setNoPicModel(Context activity) {
        return false;
    }

    public boolean onUserHomeBackPressed(Activity activity) {
        return false;
    }

    public boolean onUserHomeSurroundTopic(Activity activity) {
        return false;
    }

    public boolean setUserHomeRefreshBtn() {
        return false;
    }

    public boolean setUserHomePublishText(Activity activity) {
        return false;
    }

    public boolean onUserHomeFindFriend(Activity activity) {
        return false;
    }

    public boolean setAtReplyMessageFragment(Activity activity) {
        return true;
    }

    public boolean logoutApp() {
        return false;
    }

    public boolean setFroumManage(Activity activity) {
        return true;
    }

    public boolean isCopyright(Activity activity) {
        return SharedPreferencesDB.getInstance(activity).getPayStatePowerBy();
    }

    public void setAnimation(Activity activity, boolean isStart) {
    }

    public void changeFavoritText(Activity activity, TextView textView) {
    }

    public void setBack(Activity activity) {
        activity.finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public Intent setPushMsgIntent(Context context, PushMessageModel messageModel) {
        ForumInitializeHelper.getInstance().init(context, new ForumInitializeHelper.McForumInitializeListener() {
            public void onPostExecute(String result) {
            }
        });
        if (messageModel.getPushType() == 1) {
            Intent intent = new Intent(context, HomeTopicFragmentActivity.class);
            intent.setFlags(276824064);
            intent.putExtra("push", true);
            context.startActivity(intent);
            return null;
        } else if (messageModel.getPushType() != 2 || messageModel.getPushDetailType() != 1) {
            return null;
        } else {
            Intent intent2 = new Intent(context, PostsActivity.class);
            intent2.setFlags(276824064);
            intent2.putExtra("topicId", messageModel.getTopicId());
            intent2.putExtra("push", true);
            context.startActivity(intent2);
            return null;
        }
    }

    public Intent setLoginIntent(Context activity) {
        return new Intent(activity, UserLoginFragmentActivity.class);
    }
}
