package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.holder.TopicContentImgHolder;
import com.mobcent.base.android.ui.activity.adapter.holder.TopicContentTextHolder;
import com.mobcent.base.android.ui.activity.fragmentActivity.ImageViewerFragmentActivity;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.RichImageModel;
import com.mobcent.forum.android.model.TopicContentModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import java.util.ArrayList;
import java.util.List;

public class PostsDetailAdapter extends BaseAdapter {
    private AsyncTaskLoaderImage asyncTaskLoaderImage;
    /* access modifiers changed from: private */
    public Context context;
    private MCResource forumResource = MCResource.getInstance(this.context);
    private LayoutInflater inflater = LayoutInflater.from(this.context);
    /* access modifiers changed from: private */
    public Handler mHandler;
    private ArrayList<RichImageModel> richImageModelList;
    private List<TopicContentModel> topicContentList;

    public PostsDetailAdapter(Context context2, List<TopicContentModel> topicContentList2, Handler mHandler2, ArrayList<RichImageModel> arrayList, AsyncTaskLoaderImage asyncTaskLoaderImage2) {
        this.context = context2;
        this.topicContentList = topicContentList2;
        this.mHandler = mHandler2;
        this.richImageModelList = getAllImageUrl(topicContentList2);
        this.asyncTaskLoaderImage = asyncTaskLoaderImage2;
    }

    public int getCount() {
        return this.topicContentList.size();
    }

    public TopicContentModel getItem(int position) {
        return this.topicContentList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TopicContentModel topicContent = getItem(position);
        if (topicContent.getType() == 0) {
            return getTopicContentTextView(topicContent, convertView, parent);
        }
        if (topicContent.getType() == 1) {
            return getTopicContentImgView(topicContent, convertView, parent);
        }
        return convertView;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.mobcent.base.android.ui.activity.adapter.holder.TopicContentTextHolder} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.view.View getTopicContentTextView(com.mobcent.forum.android.model.TopicContentModel r8, android.view.View r9, android.view.ViewGroup r10) {
        /*
            r7 = this;
            r6 = 0
            r2 = 0
            if (r9 != 0) goto L_0x0036
            android.view.LayoutInflater r3 = r7.inflater
            com.mobcent.base.forum.android.util.MCResource r4 = r7.forumResource
            java.lang.String r5 = "mc_forum_posts_topic_text_item"
            int r4 = r4.getLayoutId(r5)
            android.view.View r9 = r3.inflate(r4, r6)
            com.mobcent.base.android.ui.activity.adapter.holder.TopicContentTextHolder r2 = new com.mobcent.base.android.ui.activity.adapter.holder.TopicContentTextHolder
            r2.<init>()
            r9.setTag(r2)
            r7.initTopicContentTextHolder(r9, r2)
        L_0x001d:
            android.widget.TextView r3 = r2.getTopicInfoText()
            java.lang.String r4 = r8.getInfor()
            r3.setText(r4)
            android.widget.TextView r3 = r2.getTopicInfoText()
            java.lang.String r4 = r8.getInfor()
            android.content.Context r5 = r7.context
            com.mobcent.base.forum.android.util.MCFaceUtil.setStrToFace(r3, r4, r5)
            return r9
        L_0x0036:
            java.lang.Object r3 = r9.getTag()     // Catch:{ ClassCastException -> 0x0042 }
            r0 = r3
            com.mobcent.base.android.ui.activity.adapter.holder.TopicContentTextHolder r0 = (com.mobcent.base.android.ui.activity.adapter.holder.TopicContentTextHolder) r0     // Catch:{ ClassCastException -> 0x0042 }
            r2 = r0
            r7.initTopicContentTextHolder(r9, r2)     // Catch:{ ClassCastException -> 0x0042 }
            goto L_0x001d
        L_0x0042:
            r3 = move-exception
            r1 = r3
            android.view.LayoutInflater r3 = r7.inflater
            com.mobcent.base.forum.android.util.MCResource r4 = r7.forumResource
            java.lang.String r5 = "mc_forum_posts_topic_text_item"
            int r4 = r4.getLayoutId(r5)
            android.view.View r9 = r3.inflate(r4, r6)
            com.mobcent.base.android.ui.activity.adapter.holder.TopicContentTextHolder r2 = new com.mobcent.base.android.ui.activity.adapter.holder.TopicContentTextHolder
            r2.<init>()
            r9.setTag(r2)
            r7.initTopicContentTextHolder(r9, r2)
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.base.android.ui.activity.adapter.PostsDetailAdapter.getTopicContentTextView(com.mobcent.forum.android.model.TopicContentModel, android.view.View, android.view.ViewGroup):android.view.View");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.mobcent.base.android.ui.activity.adapter.holder.TopicContentImgHolder} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.view.View getTopicContentImgView(com.mobcent.forum.android.model.TopicContentModel r9, android.view.View r10, android.view.ViewGroup r11) {
        /*
            r8 = this;
            r7 = 0
            r3 = 0
            if (r10 != 0) goto L_0x0051
            android.view.LayoutInflater r4 = r8.inflater
            com.mobcent.base.forum.android.util.MCResource r5 = r8.forumResource
            java.lang.String r6 = "mc_forum_posts_topic_img_item"
            int r5 = r5.getLayoutId(r6)
            android.view.View r10 = r4.inflate(r5, r7)
            com.mobcent.base.android.ui.activity.adapter.holder.TopicContentImgHolder r3 = new com.mobcent.base.android.ui.activity.adapter.holder.TopicContentImgHolder
            r3.<init>()
            r10.setTag(r3)
            r8.initTopicContentImgHolder(r10, r3)
        L_0x001d:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = r9.getBaseUrl()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = r9.getInfor()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r2 = r4.toString()
            android.widget.ImageView r4 = r3.getTopicInfoImg()
            java.util.ArrayList<com.mobcent.forum.android.model.RichImageModel> r5 = r8.richImageModelList
            r8.setAllImageOnClickAction(r4, r2, r5)
            java.lang.String r4 = r9.getInfor()
            boolean r4 = com.mobcent.forum.android.util.StringUtil.isEmpty(r4)
            if (r4 != 0) goto L_0x0079
            android.widget.ImageView r4 = r3.getTopicInfoImg()
            r8.updateTopicContentImage(r2, r4)
        L_0x0050:
            return r10
        L_0x0051:
            java.lang.Object r4 = r10.getTag()     // Catch:{ ClassCastException -> 0x005d }
            r0 = r4
            com.mobcent.base.android.ui.activity.adapter.holder.TopicContentImgHolder r0 = (com.mobcent.base.android.ui.activity.adapter.holder.TopicContentImgHolder) r0     // Catch:{ ClassCastException -> 0x005d }
            r3 = r0
            r8.initTopicContentImgHolder(r10, r3)     // Catch:{ ClassCastException -> 0x005d }
            goto L_0x001d
        L_0x005d:
            r4 = move-exception
            r1 = r4
            android.view.LayoutInflater r4 = r8.inflater
            com.mobcent.base.forum.android.util.MCResource r5 = r8.forumResource
            java.lang.String r6 = "mc_forum_posts_topic_img_item"
            int r5 = r5.getLayoutId(r6)
            android.view.View r10 = r4.inflate(r5, r7)
            com.mobcent.base.android.ui.activity.adapter.holder.TopicContentImgHolder r3 = new com.mobcent.base.android.ui.activity.adapter.holder.TopicContentImgHolder
            r3.<init>()
            r10.setTag(r3)
            r8.initTopicContentImgHolder(r10, r3)
            goto L_0x001d
        L_0x0079:
            android.widget.ImageView r4 = r3.getTopicInfoImg()
            com.mobcent.base.forum.android.util.MCResource r5 = r8.forumResource
            java.lang.String r6 = "mc_forum_x_img3"
            int r5 = r5.getDrawableId(r6)
            r4.setImageResource(r5)
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.base.android.ui.activity.adapter.PostsDetailAdapter.getTopicContentImgView(com.mobcent.forum.android.model.TopicContentModel, android.view.View, android.view.ViewGroup):android.view.View");
    }

    private void initTopicContentTextHolder(View view, TopicContentTextHolder textHolder) {
        textHolder.setTopicInfoText((TextView) view.findViewById(this.forumResource.getViewId("mc_forum_topic_info_text")));
    }

    private void initTopicContentImgHolder(View view, TopicContentImgHolder imgHolder) {
        imgHolder.setTopicInfoImg((ImageView) view.findViewById(this.forumResource.getViewId("mc_forum_topic_info_img")));
    }

    private void updateTopicContentImage(String imgUrl, final ImageView imageView) {
        if (SharedPreferencesDB.getInstance(this.context).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(AsyncTaskLoaderImage.formatUrl(imgUrl, "320x480"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    PostsDetailAdapter.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (image != null) {
                                imageView.setImageBitmap(image);
                            }
                        }
                    });
                }
            });
        }
    }

    public ArrayList<RichImageModel> getAllImageUrl(List<TopicContentModel> topicContentList2) {
        if (topicContentList2 == null || topicContentList2.size() == 0) {
            return null;
        }
        ArrayList<RichImageModel> richImageModelList2 = new ArrayList<>();
        if (topicContentList2 != null && topicContentList2.size() > 0) {
            for (TopicContentModel topicContent : topicContentList2) {
                if (topicContent.getType() == 1) {
                    RichImageModel model = new RichImageModel();
                    model.setImageUrl(topicContent.getBaseUrl() + topicContent.getInfor());
                    richImageModelList2.add(model);
                }
            }
        }
        return richImageModelList2;
    }

    private void setAllImageOnClickAction(ImageView imageView, final String imageUrl, final ArrayList<RichImageModel> richImageModelList2) {
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(PostsDetailAdapter.this.context, ImageViewerFragmentActivity.class);
                intent.putExtra(MCConstant.RICH_IMAGE_LIST, richImageModelList2);
                intent.putExtra(MCConstant.IMAGE_URL, imageUrl);
                PostsDetailAdapter.this.context.startActivity(intent);
            }
        });
    }
}
