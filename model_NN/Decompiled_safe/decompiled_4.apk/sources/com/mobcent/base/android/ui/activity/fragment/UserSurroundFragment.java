package com.mobcent.base.android.ui.activity.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.baidu.location.BDLocation;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.FriendAdapter;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.BaseAsyncTaskLoader;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.model.LocationModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.LocationUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class UserSurroundFragment extends BaseListViewFragment implements LoaderManager.LoaderCallbacks<List<UserInfoModel>> {
    private int FLAG_SURROUND_USER_STATUS = 2;
    private FriendAdapter adapter;
    private int currentPage = 1;
    private FriendAdapter.FriendsClickListener friendsClickListener;
    private boolean isHasFooter = false;
    /* access modifiers changed from: private */
    public LocationModel locationModel;
    /* access modifiers changed from: private */
    public LocationUtil locationUtil;
    private int pageSize = 20;
    private PullToRefreshListView pullToRefreshListView;
    /* access modifiers changed from: private */
    public List<String> refreshimgUrls;
    private List<UserInfoModel> surroundUserInfoList;
    private long userId;
    private UserService userService;

    public /* bridge */ /* synthetic */ void onLoadFinished(Loader x0, Object x1) {
        onLoadFinished((Loader<List<UserInfoModel>>) x0, (List<UserInfoModel>) ((List) x1));
    }

    public FriendAdapter.FriendsClickListener getFriendsClickListener() {
        return this.friendsClickListener;
    }

    public void setFriendsClickListener(FriendAdapter.FriendsClickListener friendsClickListener2) {
        this.friendsClickListener = friendsClickListener2;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.surroundUserInfoList = new ArrayList();
        initLocationUtil(this.activity);
        this.refreshimgUrls = new ArrayList();
        this.userService = new UserServiceImpl(this.activity);
        this.userId = this.userService.getLoginUserId();
    }

    private void initLocationUtil(Context context) {
        this.locationUtil = new LocationUtil(getActivity(), true, new LocationUtil.LocationDelegate() {
            public void locationResults(BDLocation location) {
                if (location.getLocType() == 161 || location.getLocType() == 61) {
                    LocationModel unused = UserSurroundFragment.this.locationModel = UserSurroundFragment.this.locationUtil.getLocation();
                    UserSurroundFragment.this.refresh(UserSurroundFragment.this.locationModel);
                } else if (location.getLocType() == 62) {
                    UserSurroundFragment.this.warnMessageById("mc_forum_location_fail_warn");
                    UserSurroundFragment.this.updateLocationFail();
                } else if (location.getLocType() == 167) {
                    UserSurroundFragment.this.warnMessageById("mc_forum_location_fail_warn");
                    UserSurroundFragment.this.updateLocationFail();
                } else if (location.getLocType() == 63) {
                    UserSurroundFragment.this.warnMessageById("mc_forum_location_fail_warn");
                    UserSurroundFragment.this.updateLocationFail();
                }
            }

            public void onReceivePoi(boolean hasPoi, List<String> list) {
            }
        });
    }

    /* access modifiers changed from: private */
    public void refresh(LocationModel locationModel2) {
        if (locationModel2 != null) {
            this.currentPage = 1;
            Bundle bundle = new Bundle();
            bundle.putInt("page", this.currentPage);
            bundle.putInt("pageSize", this.pageSize);
            bundle.putSerializable(MCConstant.LOCATION_MODEL, locationModel2);
            if (getLoaderManager().getLoader(1) == null) {
                getLoaderManager().initLoader(1, bundle, this);
            } else {
                getLoaderManager().restartLoader(1, bundle, this);
            }
        }
    }

    public void onLoadMore() {
        this.currentPage++;
        Bundle bundle = new Bundle();
        bundle.putInt("page", this.currentPage);
        bundle.putInt("pageSize", this.pageSize);
        bundle.putSerializable(MCConstant.LOCATION_MODEL, this.locationModel);
        if (getLoaderManager().getLoader(2) == null) {
            getLoaderManager().initLoader(2, bundle, this);
        } else {
            getLoaderManager().restartLoader(2, bundle, this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(this.mcResource.getLayoutId("mc_forum_user_friends_fragment"), container, false);
        this.pullToRefreshListView = (PullToRefreshListView) this.view.findViewById(this.mcResource.getViewId("mc_forum_lv"));
        this.adapter = new FriendAdapter(this.activity, this.surroundUserInfoList, this.mHandler, this.asyncTaskLoaderImage, inflater, this.FLAG_SURROUND_USER_STATUS, 0, this, this.currentPage);
        this.adapter.setClickListener(this.friendsClickListener);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.pullToRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                if (UserSurroundFragment.this.refreshimgUrls != null && !UserSurroundFragment.this.refreshimgUrls.isEmpty() && UserSurroundFragment.this.refreshimgUrls.size() > 0) {
                    UserSurroundFragment.this.asyncTaskLoaderImage.recycleBitmaps(UserSurroundFragment.this.refreshimgUrls);
                }
                if (UserSurroundFragment.this.locationModel != null) {
                    UserSurroundFragment.this.refresh(UserSurroundFragment.this.locationModel);
                } else {
                    UserSurroundFragment.this.locationUtil.startLocation();
                }
            }
        });
        this.pullToRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                UserSurroundFragment.this.onLoadMore();
            }
        });
        this.pullToRefreshListView.setScrollListener(this.listOnScrollListener);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        List<UserInfoModel> surroundUserInfoList2 = this.userService.getLocalSurroudUser(this.userId, this.currentPage, this.pageSize);
        if (surroundUserInfoList2 == null) {
            this.locationUtil.startLocation();
        } else {
            onRefreshDone(surroundUserInfoList2);
        }
        return this.view;
    }

    public static class RefreshUserSurroundListTask extends BaseAsyncTaskLoader<List<UserInfoModel>> {
        private LocationModel locationModel;
        private int page = 0;
        private int pageSize = 0;
        private int radius = 0;
        private long userId;

        public RefreshUserSurroundListTask(Context context) {
            super(context);
        }

        public RefreshUserSurroundListTask(Context context, long userId2, LocationModel locationModel2, int page2, int pageSize2) {
            super(context);
            this.userId = userId2;
            this.locationModel = locationModel2;
            this.page = page2;
            this.pageSize = pageSize2;
        }

        public List<UserInfoModel> loadInBackground() {
            UserService userService = new UserServiceImpl(getContext());
            if (this.locationModel != null) {
                return userService.getSurroudUser(this.userId, this.locationModel.getLongitude(), this.locationModel.getLatitude(), this.radius, this.page, this.pageSize);
            }
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void updateLocationFail() {
        if (!this.isHasFooter) {
            this.isHasFooter = true;
            this.adapter = new FriendAdapter(this.activity, this.surroundUserInfoList, this.mHandler, this.asyncTaskLoaderImage, null, this.FLAG_SURROUND_USER_STATUS, 0, this, this.currentPage);
            this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.locationUtil != null) {
            this.locationUtil.stopLocation();
        }
    }

    /* access modifiers changed from: protected */
    public List<String> getImageURL(int start, int end) {
        List<String> imgUrls = new ArrayList<>();
        for (int i = start; i < end; i++) {
            imgUrls.add(AsyncTaskLoaderImage.formatUrl(this.surroundUserInfoList.get(i).getIcon(), "100x100"));
        }
        return imgUrls;
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return getImageURL(0, this.surroundUserInfoList.size());
    }

    private List<String> getRefreshImgUrl(List<UserInfoModel> result) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < this.surroundUserInfoList.size(); i++) {
            UserInfoModel model = this.surroundUserInfoList.get(i);
            if (!isExit(model, result)) {
                list.add(AsyncTaskLoaderImage.formatUrl(model.getIcon(), "100x100"));
            }
        }
        return list;
    }

    private boolean isExit(UserInfoModel model, List<UserInfoModel> result) {
        int j = result.size();
        for (int i = 0; i < j; i++) {
            UserInfoModel model2 = result.get(i);
            if (!StringUtil.isEmpty(model.getIcon()) && !StringUtil.isEmpty(model2.getIcon()) && model.getIcon().equals(model2.getIcon())) {
                return true;
            }
        }
        return false;
    }

    public Loader<List<UserInfoModel>> onCreateLoader(int arg0, Bundle bundle) {
        int page = bundle.getInt("page");
        int pageSize2 = bundle.getInt("pageSize");
        return new RefreshUserSurroundListTask(getActivity(), this.userId, (LocationModel) bundle.getSerializable(MCConstant.LOCATION_MODEL), page, pageSize2);
    }

    public void onLoadFinished(Loader<List<UserInfoModel>> loader, List<UserInfoModel> data) {
        if (loader.getId() == 1) {
            onRefreshDone(data);
        } else {
            onMoreDone(data);
        }
    }

    private void onMoreDone(List<UserInfoModel> result) {
        if (result == null) {
            this.currentPage--;
        } else if (result.isEmpty()) {
            this.pullToRefreshListView.onBottomRefreshComplete(3);
        } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
            this.currentPage--;
            warnMessageByStr(MCForumErrorUtil.convertErrorCode(this.activity, result.get(0).getErrorCode()));
            this.pullToRefreshListView.onBottomRefreshComplete(0);
        } else {
            List<UserInfoModel> tempList = new ArrayList<>();
            tempList.addAll(this.surroundUserInfoList);
            tempList.addAll(result);
            this.adapter.setUserInfoList(tempList);
            this.adapter.notifyDataSetInvalidated();
            this.adapter.notifyDataSetChanged();
            if (result.get(0).getHasNext() == 1) {
                this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                this.pullToRefreshListView.onBottomRefreshComplete(3);
            }
            this.surroundUserInfoList = tempList;
        }
    }

    public void onLoaderReset(Loader<List<UserInfoModel>> loader) {
        loader.reset();
    }

    private void onRefreshDone(List<UserInfoModel> result) {
        this.pullToRefreshListView.onRefreshComplete();
        if (result == null) {
            this.pullToRefreshListView.onBottomRefreshComplete(3, getString(this.mcResource.getStringId("mc_forum_no_follow_user")));
        } else if (result.isEmpty()) {
            this.pullToRefreshListView.onBottomRefreshComplete(3, getString(this.mcResource.getStringId("mc_forum_no_follow_user")));
        } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
            Toast.makeText(this.activity, MCForumErrorUtil.convertErrorCode(this.activity, result.get(0).getErrorCode()), 0).show();
            this.pullToRefreshListView.onBottomRefreshComplete(3);
        } else {
            this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
            this.refreshimgUrls = getRefreshImgUrl(result);
            this.adapter.setUserInfoList(result);
            this.adapter.notifyDataSetInvalidated();
            this.adapter.notifyDataSetChanged();
            this.pullToRefreshListView.onRefreshComplete();
            if (result.get(0).getHasNext() == 1) {
                this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                this.pullToRefreshListView.onBottomRefreshComplete(3);
            }
            this.surroundUserInfoList = result;
        }
    }
}
