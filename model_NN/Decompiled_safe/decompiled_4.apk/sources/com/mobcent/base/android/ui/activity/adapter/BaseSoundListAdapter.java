package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.adapter.holder.PostsAudioAdapterHolder;
import com.mobcent.base.android.ui.activity.service.MediaService;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.SoundModel;
import com.mobcent.forum.android.util.MCLogUtil;

public abstract class BaseSoundListAdapter extends BaseAdapter {
    protected Context context;
    protected PostsAudioAdapterHolder currAudioAdapterHolder;
    protected SoundModel currSoundModel;
    protected LayoutInflater inflater;
    protected PostsAudioAdapterHolder lastAudioAdapterHolder;
    protected SoundModel lastSoundModel;
    protected MCResource resource = MCResource.getInstance(this.context);
    protected String tag;

    public BaseSoundListAdapter(Context context2, String tag2, LayoutInflater inflater2) {
        this.context = context2;
        this.tag = tag2;
        this.inflater = inflater2;
    }

    /* access modifiers changed from: protected */
    public View getSoundView(final SoundModel soundModel) {
        View view = this.inflater.inflate(this.resource.getLayoutId("mc_forum_posts_topic_audio_item"), (ViewGroup) null);
        final PostsAudioAdapterHolder audioAdapterHolder = new PostsAudioAdapterHolder();
        audioAdapterHolder.setTimeText((TextView) view.findViewById(this.resource.getViewId("mc_forum_topic_time_text")));
        audioAdapterHolder.setPlayStautsImg((ImageView) view.findViewById(this.resource.getViewId("mc_forum_topic_play_stauts_img")));
        audioAdapterHolder.setPlayingImg((ImageView) view.findViewById(this.resource.getViewId("mc_forum_topic_playing_img")));
        audioAdapterHolder.getTimeText().setText(soundModel.getSoundTime() + "\"");
        audioAdapterHolder.setDownProgressBar((MCProgressBar) view.findViewById(this.resource.getViewId("mc_forum_down_progress_bar")));
        updateDetailPlayImg(soundModel, audioAdapterHolder);
        audioAdapterHolder.getPlayStautsImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaseSoundListAdapter.this.lastSoundModel = BaseSoundListAdapter.this.currSoundModel;
                BaseSoundListAdapter.this.lastAudioAdapterHolder = BaseSoundListAdapter.this.currAudioAdapterHolder;
                BaseSoundListAdapter.this.currSoundModel = soundModel;
                BaseSoundListAdapter.this.currAudioAdapterHolder = audioAdapterHolder;
                Intent intent = new Intent(BaseSoundListAdapter.this.context, MediaService.class);
                intent.putExtra(MediaService.SERVICE_MODEL, soundModel);
                intent.putExtra(MediaService.SERVICE_TAG, BaseSoundListAdapter.this.tag);
                BaseSoundListAdapter.this.context.startService(intent);
            }
        });
        return view;
    }

    public void updateReceivePlayImg(SoundModel soundModel) {
        if (soundModel != null) {
            if (this.lastSoundModel != null && soundModel.getSoundPath().equals(this.lastSoundModel.getSoundPath())) {
                MCLogUtil.i("BaseSoundListAdapter", "update last");
                this.lastSoundModel.setPalyStatus(soundModel.getPalyStatus());
                this.lastSoundModel.setPlayProgress(soundModel.getPlayProgress());
                this.lastSoundModel.setCurrentPosition(soundModel.getCurrentPosition());
                updatePlayImg(this.lastSoundModel, this.lastAudioAdapterHolder);
            } else if (this.currSoundModel != null && soundModel.getSoundPath().equals(this.currSoundModel.getSoundPath())) {
                MCLogUtil.i("BaseSoundListAdapter", "update curr");
                this.currSoundModel.setPalyStatus(soundModel.getPalyStatus());
                this.currSoundModel.setPlayProgress(soundModel.getPlayProgress());
                this.currSoundModel.setCurrentPosition(soundModel.getCurrentPosition());
                updatePlayImg(this.currSoundModel, this.currAudioAdapterHolder);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void updateDetailPlayImg(SoundModel soundModel, PostsAudioAdapterHolder audioAdapterHolder) {
        if (this.currSoundModel != null && soundModel.getSoundPath().equals(this.currSoundModel.getSoundPath())) {
            soundModel.setPalyStatus(this.currSoundModel.getPalyStatus());
            soundModel.setPlayProgress(this.currSoundModel.getPlayProgress());
            soundModel.setCurrentPosition(this.currSoundModel.getCurrentPosition());
            this.currSoundModel = soundModel;
            this.currAudioAdapterHolder = audioAdapterHolder;
            updatePlayImg(this.currSoundModel, audioAdapterHolder);
        }
    }

    /* access modifiers changed from: protected */
    public void updatePlayImg(SoundModel soundModel, PostsAudioAdapterHolder audioAdapterHolder) {
        MCLogUtil.i("BaseSoundListAdapter", "soundModel.getPalyStatus() = " + soundModel.getPalyStatus());
        if (soundModel.getPalyStatus() == 8) {
            audioAdapterHolder.getPlayStautsImg().setImageResource(this.resource.getDrawableId("mc_forum_voice_play_n"));
            audioAdapterHolder.getPlayingImg().setVisibility(8);
            audioAdapterHolder.getDownProgressBar().show();
        } else if (soundModel.getPalyStatus() == 1) {
            audioAdapterHolder.getPlayStautsImg().setImageResource(this.resource.getDrawableId("mc_forum_voice_play_n"));
            audioAdapterHolder.getPlayingImg().setVisibility(8);
            audioAdapterHolder.getDownProgressBar().hide();
        } else if (soundModel.getPalyStatus() == 3) {
            audioAdapterHolder.getPlayStautsImg().setImageResource(this.resource.getDrawableId("mc_forum_voice_play_n"));
            audioAdapterHolder.getPlayingImg().setVisibility(8);
            audioAdapterHolder.getDownProgressBar().hide();
        } else if (soundModel.getPalyStatus() == 2) {
            audioAdapterHolder.getPlayStautsImg().setImageResource(this.resource.getDrawableId("mc_forum_voice_play_h"));
            audioAdapterHolder.getPlayingImg().setVisibility(0);
            audioAdapterHolder.getDownProgressBar().hide();
            switch (soundModel.getPlayProgress() % 3) {
                case 0:
                    audioAdapterHolder.getPlayingImg().setImageResource(this.resource.getDrawableId("mc_forum_voice_chat_img1"));
                    return;
                case 1:
                    audioAdapterHolder.getPlayingImg().setImageResource(this.resource.getDrawableId("mc_forum_voice_chat_img2"));
                    return;
                case 2:
                    audioAdapterHolder.getPlayingImg().setImageResource(this.resource.getDrawableId("mc_forum_voice_chat_img3"));
                    return;
                default:
                    audioAdapterHolder.getPlayingImg().setImageResource(this.resource.getDrawableId("mc_forum_voice_chat_img1"));
                    return;
            }
        } else if (soundModel.getPalyStatus() == 4 || soundModel.getPalyStatus() == 5) {
            audioAdapterHolder.getPlayStautsImg().setImageResource(this.resource.getDrawableId("mc_forum_voice_play_n"));
            audioAdapterHolder.getPlayingImg().setVisibility(8);
            audioAdapterHolder.getDownProgressBar().hide();
        } else {
            audioAdapterHolder.getPlayStautsImg().setImageResource(this.resource.getDrawableId("mc_forum_voice_play_n"));
            audioAdapterHolder.getPlayingImg().setVisibility(8);
            audioAdapterHolder.getDownProgressBar().hide();
        }
    }

    public void resetPlayStatus() {
        if (this.currSoundModel != null) {
            this.currSoundModel.setPalyStatus(1);
            updatePlayImg(this.currSoundModel, this.currAudioAdapterHolder);
        }
        if (this.lastSoundModel != null) {
            this.lastSoundModel.setPalyStatus(1);
            updatePlayImg(this.lastSoundModel, this.lastAudioAdapterHolder);
        }
        this.currSoundModel = null;
        this.currAudioAdapterHolder = null;
        this.lastSoundModel = null;
        this.lastAudioAdapterHolder = null;
    }
}
