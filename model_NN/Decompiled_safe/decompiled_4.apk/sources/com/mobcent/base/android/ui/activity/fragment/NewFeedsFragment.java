package com.mobcent.base.android.ui.activity.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.NewFeedsAdapter;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.BaseAsyncTaskLoader;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.constant.FeedsConstant;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.model.NewFeedsModel;
import com.mobcent.forum.android.model.UserFeedModel;
import com.mobcent.forum.android.model.UserTopicFeedModel;
import com.mobcent.forum.android.service.impl.FeedsServiceImpl;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NewFeedsFragment extends BaseListViewFragment implements LoaderManager.LoaderCallbacks<List<NewFeedsModel>>, FeedsConstant {
    private NewFeedsAdapter adapter;
    private Set<String> allImgUrls;
    private int currentPage = 1;
    private long endTime = 0;
    protected PullToRefreshListView.OnScrollListener feedListOnScrollListener = new PullToRefreshListView.OnScrollListener() {
        private int firstVisibleItem;
        private int totalItemCount;
        private int visibleItemCount;

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0) {
                NewFeedsFragment.this.asyncTaskLoaderImage.recycleBitmaps(getRecycleImgUrls(NewFeedsFragment.this.pageSize));
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
            this.firstVisibleItem = firstVisibleItem2;
            this.visibleItemCount = visibleItemCount2;
            this.totalItemCount = totalItemCount2 - 2;
        }

        private Set<String> getRecycleImgUrls(int pageSize) {
            int size = pageSize;
            int firstIndex = 0;
            int endIndex = this.firstVisibleItem + this.visibleItemCount + size;
            if (this.firstVisibleItem - size > 0) {
                firstIndex = this.firstVisibleItem - size;
            }
            if (endIndex >= this.totalItemCount) {
                endIndex = this.totalItemCount;
            }
            return NewFeedsFragment.this.getRecycleImageURL(firstIndex, endIndex);
        }

        public void onScrollDirection(boolean isUp, int distance) {
        }
    };
    private List<NewFeedsModel> feedsList;
    private boolean hasFooter = false;
    /* access modifiers changed from: private */
    public int pageSize = 10;
    /* access modifiers changed from: private */
    public PullToRefreshListView pullToRefreshListView;

    public /* bridge */ /* synthetic */ void onLoadFinished(Loader x0, Object x1) {
        onLoadFinished((Loader<List<NewFeedsModel>>) x0, (List<NewFeedsModel>) ((List) x1));
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.feedsList = new ArrayList();
        this.allImgUrls = new HashSet();
        this.permService = new PermServiceImpl(this.activity);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(this.mcResource.getLayoutId("mc_forum_topic_fragment"), container, false);
        this.pullToRefreshListView = (PullToRefreshListView) this.view.findViewById(this.mcResource.getViewId("mc_forum_lv"));
        this.adapter = new NewFeedsAdapter(this.activity, inflater, this.mHandler, this.asyncTaskLoaderImage, this.pullToRefreshListView, this.feedsList);
        this.pullToRefreshListView.setScrollListener(this.feedListOnScrollListener);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
        return this.view;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return this.view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.pullToRefreshListView.onRefreshWithOutListener();
        Bundle bundle = new Bundle();
        this.currentPage = 1;
        bundle.putLong("endTime", this.endTime);
        bundle.putBoolean(MCConstant.LOCAL, true);
        getLoaderManager().initLoader(1, bundle, this);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.pullToRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                if (NewFeedsFragment.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
                    NewFeedsFragment.this.onRefreshs();
                    return;
                }
                NewFeedsFragment.this.pullToRefreshListView.onRefreshComplete();
                NewFeedsFragment.this.warnMessageByStr(NewFeedsFragment.this.mcResource.getString("mc_forum_permission_cannot_visit_forum"));
            }
        });
        this.pullToRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                NewFeedsFragment.this.onLoadMore();
            }
        });
    }

    public void onRefreshs() {
        Bundle bundle = new Bundle();
        this.currentPage = 1;
        this.endTime = 0;
        bundle.putLong("endTime", this.endTime);
        bundle.putBoolean(MCConstant.LOCAL, false);
        getLoaderManager().restartLoader(1, bundle, this);
    }

    public void onLoadMore() {
        this.currentPage++;
        Bundle bundle = new Bundle();
        bundle.putLong("endTime", this.endTime);
        bundle.putBoolean(MCConstant.LOCAL, false);
        if (getLoaderManager().getLoader(2) == null) {
            getLoaderManager().initLoader(2, bundle, this);
        } else {
            getLoaderManager().restartLoader(2, bundle, this);
        }
    }

    public Loader<List<NewFeedsModel>> onCreateLoader(int arg0, Bundle bundle) {
        return new RefreshTaskLoader(getActivity(), bundle.getLong("endTime", 0), bundle.getBoolean(MCConstant.LOCAL));
    }

    public void onLoadFinished(Loader<List<NewFeedsModel>> loader, List<NewFeedsModel> data) {
        if (loader.getId() == 1) {
            onRefreshDone(data);
        } else {
            onMoreDone(data);
        }
    }

    private void onRefreshDone(List<NewFeedsModel> data) {
        this.pullToRefreshListView.onRefreshComplete(true);
        if (!this.hasFooter) {
            this.hasFooter = true;
            this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
        }
        if (data == null) {
            this.pullToRefreshListView.onBottomRefreshComplete(3, getString(this.mcResource.getStringId("mc_forum_warn_no_such_data")));
        } else if (data.isEmpty()) {
            this.adapter.setFeedsList(data);
            this.adapter.notifyDataSetInvalidated();
            this.adapter.notifyDataSetChanged();
            this.pullToRefreshListView.onBottomRefreshComplete(3, getString(this.mcResource.getStringId("mc_forum_warn_no_such_data")));
        } else if (!StringUtil.isEmpty(data.get(0).getErrorCode())) {
            Toast.makeText(this.activity, MCForumErrorUtil.convertErrorCode(this.activity, data.get(0).getErrorCode()), 0).show();
            this.pullToRefreshListView.onBottomRefreshComplete(0);
        } else {
            this.adapter.setFeedsList(data);
            this.adapter.notifyDataSetInvalidated();
            this.adapter.notifyDataSetChanged();
            if (data.get(0).getHasNext() == 1) {
                this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else if (data.get(0).getHasNext() == -1) {
                this.pullToRefreshListView.onBottomRefreshComplete(3, MCStringBundleUtil.resolveString(this.mcResource.getStringId("mc_forum_last_update"), MCStringBundleUtil.timeToString(this.activity, new Long(data.get(0).getLastUpdate()).longValue()), this.activity));
                this.pullToRefreshListView.onRefresh();
            } else {
                this.pullToRefreshListView.onBottomRefreshComplete(3);
            }
            this.feedsList = data;
            this.endTime = data.get(0).getEndTime();
        }
    }

    private void onMoreDone(List<NewFeedsModel> data) {
        if (data == null) {
            this.currentPage--;
        } else if (data.isEmpty()) {
            this.pullToRefreshListView.onBottomRefreshComplete(3);
        } else if (!StringUtil.isEmpty(data.get(0).getErrorCode())) {
            this.currentPage--;
            Toast.makeText(this.activity, MCForumErrorUtil.convertErrorCode(this.activity, data.get(0).getErrorCode()), 0).show();
            this.pullToRefreshListView.onBottomRefreshComplete(0);
        } else {
            List<NewFeedsModel> list = new ArrayList<>();
            list.addAll(this.feedsList);
            list.addAll(data);
            this.allImgUrls.addAll(data.get(0).getImgSet());
            this.adapter.setFeedsList(list);
            this.adapter.notifyDataSetInvalidated();
            this.adapter.notifyDataSetChanged();
            if (data.get(0).getHasNext() == 1) {
                this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                this.pullToRefreshListView.onBottomRefreshComplete(3);
            }
            this.feedsList = list;
            this.endTime = data.get(0).getEndTime();
        }
    }

    public void onLoaderReset(Loader<List<NewFeedsModel>> loader) {
        loader.reset();
    }

    public static class RefreshTaskLoader extends BaseAsyncTaskLoader<List<NewFeedsModel>> {
        private long endTime = 0;
        private boolean isLocal = false;

        public RefreshTaskLoader(Context context) {
            super(context);
        }

        public RefreshTaskLoader(Context context, long endTime2, boolean isLocal2) {
            super(context);
            this.endTime = endTime2;
            this.isLocal = isLocal2;
        }

        public List<NewFeedsModel> loadInBackground() {
            return new FeedsServiceImpl(getContext()).getNewFeeds(this.endTime, this.isLocal);
        }
    }

    /* access modifiers changed from: protected */
    public List<String> getImageURL(int start, int end) {
        return null;
    }

    /* access modifiers changed from: protected */
    public Set<String> getRecycleImageURL(int start, int end) {
        Set<String> imgUrls = new HashSet<>();
        imgUrls.addAll(this.allImgUrls);
        if (this.feedsList != null && !this.feedsList.isEmpty()) {
            for (int i = start; i < end; i++) {
                NewFeedsModel feedModel = this.feedsList.get(i);
                if (feedModel.getType() == 1) {
                    UserFeedModel userFeedModel = feedModel.getUserFeed();
                    if (!StringUtil.isEmpty(userFeedModel.getUserInfo().getIcon())) {
                        imgUrls.remove(userFeedModel.getUserInfo().getIcon());
                    }
                    int j = userFeedModel.getUserTopicList().size();
                    for (int k = 0; k < j; k++) {
                        UserTopicFeedModel userTopicFeed = userFeedModel.getUserTopicList().get(k);
                        if ((userTopicFeed.getFtype() == 1 || userTopicFeed.getFtype() == 6 || userTopicFeed.getFtype() == 10 || userTopicFeed.getFtype() == 11) && userTopicFeed.getTopic().isHasImg()) {
                            imgUrls.remove(userTopicFeed.getTopic().getPic());
                        }
                    }
                } else if (feedModel.getType() == 2 && !StringUtil.isEmpty(feedModel.getTopicFeed().getUserInfo().getIcon())) {
                    imgUrls.remove(feedModel.getTopicFeed().getUserInfo().getIcon());
                }
            }
        }
        return imgUrls;
    }
}
