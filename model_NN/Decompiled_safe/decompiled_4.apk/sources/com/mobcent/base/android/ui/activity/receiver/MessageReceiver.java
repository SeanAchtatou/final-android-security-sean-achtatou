package com.mobcent.base.android.ui.activity.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.ListView;
import com.mobcent.base.android.ui.activity.adapter.MsgChatRoomListAdapter;
import com.mobcent.base.android.ui.activity.fragment.MsgChatRoomFragment;
import com.mobcent.base.android.ui.activity.fragment.MsgUserListFragment;
import com.mobcent.forum.android.model.HeartMsgModel;
import com.mobcent.forum.android.service.HeartMsgService;
import com.mobcent.forum.android.service.impl.HeartMsgServiceImpl;
import java.util.List;

public class MessageReceiver extends BroadcastReceiver {
    private MsgChatRoomListAdapter adapter;
    private ListView chatRoomListView;
    private long chatUserId;
    private HeartMsgService heartMsgService;
    private HeartBeatReceiver heartbeatReceiver;
    private MsgUserListFragment leftFragment;
    private List<HeartMsgModel> unReadMessageList;

    public MsgUserListFragment getLeftFragment() {
        return this.leftFragment;
    }

    public void setLeftFragment(MsgUserListFragment leftFragment2) {
        this.leftFragment = leftFragment2;
    }

    public MsgChatRoomListAdapter getAdapter() {
        return this.adapter;
    }

    public void setAdapter(MsgChatRoomListAdapter adapter2) {
        this.adapter = adapter2;
    }

    public long getChatUserId() {
        return this.chatUserId;
    }

    public void setChatUserId(long chatUserId2) {
        this.chatUserId = chatUserId2;
    }

    public MessageReceiver(Context context, ListView chatRoomListView2, HeartBeatReceiver heartbeatReceiver2) {
        this.chatRoomListView = chatRoomListView2;
        this.heartMsgService = new HeartMsgServiceImpl(context);
        this.heartbeatReceiver = heartbeatReceiver2;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras() != null && intent.getIntExtra("msg_total_num", 0) > 0) {
            this.unReadMessageList = this.heartMsgService.getMessageList(this.chatUserId);
            if (this.unReadMessageList != null && !this.unReadMessageList.isEmpty()) {
                MsgChatRoomFragment.queryDBStartNum += this.unReadMessageList.size();
                List<HeartMsgModel> historyMessageList = this.adapter.getMessageList();
                historyMessageList.addAll(this.unReadMessageList);
                this.adapter.setMessageList(historyMessageList);
                this.adapter.notifyDataSetInvalidated();
                this.adapter.notifyDataSetChanged();
                this.chatRoomListView.setSelection(historyMessageList.size() - 1);
                this.heartMsgService.updateMessage(this.unReadMessageList);
                if (this.leftFragment != null) {
                    this.leftFragment.invalidateUserList();
                }
                if (this.heartbeatReceiver != null) {
                    this.heartbeatReceiver.setMsgNotification(context, 0);
                }
            }
        }
    }
}
