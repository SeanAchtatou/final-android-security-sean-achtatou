package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.mobcent.base.forum.android.util.MCResource;
import java.util.List;

public class PoiAdapter extends BaseAdapter {
    private MCResource forumResource;
    private LayoutInflater inflater;
    private List<String> poi;

    public PoiAdapter(Context context, List<String> poi2) {
        this.poi = poi2;
        this.inflater = LayoutInflater.from(context);
        this.forumResource = MCResource.getInstance(context);
    }

    public int getCount() {
        return this.poi.size();
    }

    public String getItem(int position) {
        return this.poi.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public List<String> getPoi() {
        return this.poi;
    }

    public void setPoi(List<String> poi2) {
        this.poi = poi2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View convertView2 = this.inflater.inflate(this.forumResource.getLayoutId("mc_forum_mention_friend_item"), (ViewGroup) null);
        ((TextView) convertView2.findViewById(this.forumResource.getViewId("mc_forum_mention_friend_name_text"))).setText(getItem(position));
        return convertView2;
    }
}
