package com.mobcent.base.android.ui.activity.asyncTaskLoader;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.impl.MentionFriendServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;

public class FollowUserAsyncTask extends AsyncTask<Void, Void, String> {
    private Context context;
    private MCResource resource;
    private TaskExecuteDelegate taskExecuteDelegate;
    private long userId;
    protected UserInfoModel userInfo = new UserInfoModel();

    public FollowUserAsyncTask(Context context2, MCResource resource2, long userId2, long attentionUserId, String attentionUserName) {
        this.context = context2;
        this.resource = resource2;
        this.userId = userId2;
        this.userInfo.setUserId(attentionUserId);
        this.userInfo.setNickname(attentionUserName);
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Void... params) {
        String rs = new UserServiceImpl(this.context.getApplicationContext()).followUser(this.userId, this.userInfo.getUserId());
        if (rs == null) {
            new MentionFriendServiceImpl(this.context.getApplicationContext()).addLocalForumMentionFriend(this.userId, this.userInfo);
        }
        return rs;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String result) {
        super.onPostExecute((Object) result);
        if (result == null) {
            Toast.makeText(this.context, this.resource.getStringId("mc_forum_follow_succ"), 1).show();
            if (this.taskExecuteDelegate != null) {
                this.taskExecuteDelegate.executeSuccess();
                return;
            }
            return;
        }
        Toast.makeText(this.context, MCForumErrorUtil.convertErrorCode(this.context, result), 0).show();
        if (this.taskExecuteDelegate != null) {
            this.taskExecuteDelegate.executeFail();
        }
    }

    public TaskExecuteDelegate getTaskExecuteDelegate() {
        return this.taskExecuteDelegate;
    }

    public void setTaskExecuteDelegate(TaskExecuteDelegate taskExecuteDelegate2) {
        this.taskExecuteDelegate = taskExecuteDelegate2;
    }
}
