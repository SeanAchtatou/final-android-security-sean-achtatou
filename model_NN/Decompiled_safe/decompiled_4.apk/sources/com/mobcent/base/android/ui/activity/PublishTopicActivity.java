package com.mobcent.base.android.ui.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.BasePublishTopicActivityWithAudio;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.model.TopicDraftModel;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.List;

public class PublishTopicActivity extends BasePublishWithSelectBoardActivity implements MCConstant {
    protected String content;
    protected long draftId = 1;
    private PublishAsyncTask publishAsyncTask;
    protected String title;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.draftId == 1) {
            getDraft(this.draftId);
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.audioTempFileName = this.audioTempFileName.replace("{0}", "1");
        this.permService = new PermServiceImpl(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_publish_topic_activity"));
        super.initViews();
        this.titleLabelText.setText(this.resource.getStringId("mc_forum_publish"));
        this.titleEdit.setVisibility(0);
        this.adView.showAd(this.PUBLIC_TOPIC_POSITION);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.titleEdit.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (PublishTopicActivity.this.showMentionFriendsView) {
                    return false;
                }
                PublishTopicActivity.this.titleEdit.requestFocus();
                PublishTopicActivity.this.changeKeyboardState(1);
                return false;
            }
        });
    }

    /* access modifiers changed from: protected */
    public boolean publicTopic() {
        super.publicTopic();
        this.content = this.contentEdit.getText().toString();
        if (this.boardId <= 0) {
            Toast.makeText(this, getResources().getString(this.resource.getStringId("mc_forum_publish_select_board")), 0).show();
            return false;
        } else if (!checkTitle()) {
            return false;
        } else {
            if (StringUtil.isEmpty(this.content) && !this.hasAudio) {
                warnMessageById("mc_forum_publish_min_length_error");
                return false;
            } else if (StringUtil.isEmpty(this.content) || this.content.length() <= 7000) {
                this.canPublishTopic = true;
                if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.POST, -1) == 0 || this.permService.getPermNum(PermConstant.BOARDS, PermConstant.POST, this.boardId) == 0) {
                    warnMessageByStr(this.resource.getString("mc_forum_permission_cannot_post_toipc"));
                    return false;
                }
                if (this.locationComplete) {
                    if (this.hasAudio) {
                        if (this.uploadAudioFileAsyncTask != null) {
                            this.uploadAudioFileAsyncTask.cancel(true);
                        }
                        this.uploadAudioFileAsyncTask = new BasePublishTopicActivityWithAudio.UploadAudioFileAsyncTask();
                        this.uploadAudioFileAsyncTask.execute(new Void[0]);
                    } else {
                        uploadAudioSucc();
                    }
                }
                return true;
            } else {
                warnMessageById("mc_forum_publish_max_length_error");
                return false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void uploadAudioSucc() {
        super.uploadAudioSucc();
        PostsService postsService = new PostsServiceImpl(this);
        String contentStr = postsService.createPublishTopicJson(this.content.trim(), "ß", "á", this.mentionedFriends, this.audioPath, this.audioDuration);
        if (!postsService.isContainsPic(this.content.trim(), "ß", "á", this.audioPath, this.audioDuration)) {
            if (this.publishAsyncTask != null) {
                this.publishAsyncTask.cancel(true);
            }
            this.publishAsyncTask = new PublishAsyncTask();
            this.publishAsyncTask.execute(this.boardId + "", this.title, contentStr, this.selectVisibleId + "");
        } else if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.UPLOAD, -1) == 1 && this.permService.getPermNum(PermConstant.BOARDS, PermConstant.UPLOAD, this.boardId) == 1) {
            if (this.publishAsyncTask != null) {
                this.publishAsyncTask.cancel(true);
            }
            this.publishAsyncTask = new PublishAsyncTask();
            this.publishAsyncTask.execute(this.boardId + "", this.title, contentStr, this.selectVisibleId + "");
        } else {
            warnMessageByStr(this.resource.getString("mc_forum_permission_cannot_upload_pic"));
        }
    }

    public boolean checkTitle() {
        this.title = this.titleEdit.getText().toString();
        int len = this.title.length();
        if (this.title == null || this.title.trim().equals("")) {
            warnMessageById("mc_forum_publish_min_title_length_error");
            return false;
        } else if (len <= 25) {
            return true;
        } else {
            warnMessageById("mc_forum_publish_max_title_length_error");
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean exitCheckChanged() {
        if (!this.titleEdit.getText().toString().trim().equals("") || !this.contentEdit.getText().toString().trim().equals("") || this.hasAudio) {
            return true;
        }
        return false;
    }

    class PublishAsyncTask extends AsyncTask<String, Void, String> {
        PublishAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            ((InputMethodManager) PublishTopicActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(PublishTopicActivity.this.titleEdit.getWindowToken(), 0);
            ((InputMethodManager) PublishTopicActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(PublishTopicActivity.this.contentEdit.getWindowToken(), 0);
            try {
                PublishTopicActivity.this.showProgressDialog("mc_forum_warn_publish", this);
            } catch (Exception e) {
            }
            PublishTopicActivity.this.publishIng = true;
            PublishTopicActivity.this.publishBtn.setEnabled(false);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            PostsService postsService = new PostsServiceImpl(PublishTopicActivity.this);
            if (params[0] == null || params[1] == null || params[2] == null) {
                return null;
            }
            return postsService.publishTopic(Long.parseLong(params[0]), params[1], params[2], PublishTopicActivity.this.pageFromNotBoard, PublishTopicActivity.this.longitude, PublishTopicActivity.this.latitude, PublishTopicActivity.this.locationStr, PublishTopicActivity.this.requireLocation, Integer.parseInt(params[3]));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            PublishTopicActivity.this.publishBtn.setEnabled(true);
            PublishTopicActivity.this.publishIng = false;
            PublishTopicActivity.this.hideProgressDialog();
            if (result == null) {
                if (PublishTopicActivity.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.POST, -1) == 2 || PublishTopicActivity.this.permService.getPermNum(PermConstant.BOARDS, PermConstant.POST, PublishTopicActivity.this.boardId) == 2) {
                    PublishTopicActivity.this.warnMessageById("mc_forum_permission_post_after_verify");
                } else {
                    PublishTopicActivity.this.warnMessageById("mc_forum_publish_succ");
                }
                PublishTopicActivity.this.deleteDraft(PublishTopicActivity.this.draftId);
                PublishTopicActivity.this.exitNoSaveEvent();
            } else if (!result.equals("")) {
                PublishTopicActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(PublishTopicActivity.this, result) + "\n" + PublishTopicActivity.this.getString(PublishTopicActivity.this.resource.getStringId("mc_forum_warn_publish_fail")));
                PublishTopicActivity.this.saveAsDraft(PublishTopicActivity.this.draftId, new TopicDraftModel());
            }
            if (PublishTopicActivity.this.isExit) {
                PublishTopicActivity.this.exitNoSaveEvent();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void exitSaveEvent() {
        saveAsDraft(this.draftId, new TopicDraftModel());
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.publishAsyncTask != null) {
            this.publishAsyncTask.cancel(true);
        }
    }

    /* access modifiers changed from: protected */
    public void updateBoardList(List<BoardModel> boardList) {
        this.boardListAdapter.setBoardList(boardList);
        this.boardListAdapter.notifyDataSetInvalidated();
        this.boardListAdapter.notifyDataSetChanged();
        this.boardList = boardList;
    }

    /* access modifiers changed from: protected */
    public TopicDraftModel saveOtherDataToDraft(TopicDraftModel draft) {
        draft.setBoardId(this.boardId);
        draft.setBoardName(this.selectBoardImg.getText().toString());
        return draft;
    }

    /* access modifiers changed from: protected */
    public void restoreOtherViewFromDraft(TopicDraftModel draft) {
        this.boardName = draft.getBoardName();
        this.boardId = draft.getBoardId();
        if (!StringUtil.isEmpty(this.boardName) && this.boardId > 0) {
            this.selectBoardImg.setText(this.boardName);
        }
    }
}
