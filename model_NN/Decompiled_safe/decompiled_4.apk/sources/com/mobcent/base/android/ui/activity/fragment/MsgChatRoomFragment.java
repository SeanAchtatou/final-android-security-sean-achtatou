package com.mobcent.base.android.ui.activity.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.FacePagerAdapter;
import com.mobcent.base.android.ui.activity.adapter.MsgChatRoomListAdapter;
import com.mobcent.base.android.ui.activity.delegate.ChatRetrunDelegate;
import com.mobcent.base.android.ui.activity.receiver.HeartBeatReceiver;
import com.mobcent.base.android.ui.activity.receiver.MessageReceiver;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.android.ui.activity.service.MediaService;
import com.mobcent.base.forum.android.util.AudioCache;
import com.mobcent.base.forum.android.util.AudioRecordUtil;
import com.mobcent.base.forum.android.util.ImageCache;
import com.mobcent.base.forum.android.util.MCFaceUtil;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCForumReverseList;
import com.mobcent.base.forum.android.util.MCTouchUtil;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.model.HeartMsgModel;
import com.mobcent.forum.android.model.SoundModel;
import com.mobcent.forum.android.os.service.HeartMsgOSService;
import com.mobcent.forum.android.service.HeartMsgService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.HeartMsgServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.DateUtil;
import com.mobcent.forum.android.util.MCLibIOUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MsgChatRoomFragment extends BasePhotoPreviewFragment implements MCConstant {
    public static final String AUDIO_TEMP_FILE_NAME = "audio_record_msg_temp.amr";
    public static final int MAX_RECORD_TIME = 59;
    public static final int MAX_UV_VALUE = 20;
    /* access modifiers changed from: private */
    public static ChatRetrunDelegate chatRetrunDelegate;
    public static int queryDBStartNum = 0;
    public static int sendReceiveMessageTotalNum = 0;
    private final int FACE_STATE_SHOW = 2;
    private final int KEYBOARD_STATE_HIDE = 0;
    private final int KEYBOARD_STATE_SHOW = 1;
    protected String audioPath;
    private Button backBtn;
    /* access modifiers changed from: private */
    public BlackAsyncTask blackAsyncTask;
    /* access modifiers changed from: private */
    public Button blackBtn;
    private int blackStatus = 0;
    protected ImageView cameraPicImg;
    /* access modifiers changed from: private */
    public ListView chatRoomListView;
    private String chatUserIcon = "";
    /* access modifiers changed from: private */
    public long chatUserId = 0;
    private String chatUserNickname = null;
    /* access modifiers changed from: private */
    public long currUserId;
    /* access modifiers changed from: private */
    public long endTime;
    protected ImageView faceImg;
    protected LinearLayout faceLayout;
    protected List<LinkedHashMap> faceList;
    protected ViewPager facePager;
    protected FacePagerAdapter facePagerAdapter;
    private boolean flag = true;
    /* access modifiers changed from: private */
    public boolean hasNext;
    /* access modifiers changed from: private */
    public List<HeartMsgModel> heartMsgList;
    /* access modifiers changed from: private */
    public HeartMsgService heartMsgService;
    /* access modifiers changed from: private */
    public HeartBeatReceiver heartbeatReceiver;
    /* access modifiers changed from: private */
    public boolean isLoading;
    /* access modifiers changed from: private */
    public boolean isOverTime = false;
    /* access modifiers changed from: private */
    public boolean isRecord = false;
    /* access modifiers changed from: private */
    public boolean isRecording = false;
    /* access modifiers changed from: private */
    public int keyboardState = 1;
    protected ImageView localPicImg;
    /* access modifiers changed from: private */
    public MediaRecorder mRecorder = null;
    private MessageReceiver messageReceiver = null;
    /* access modifiers changed from: private */
    public MoreAsyncTask moreAsyncTask;
    /* access modifiers changed from: private */
    public Button msgBtn;
    public MsgChatRoomListAdapter msgChatRoomListAdapter;
    /* access modifiers changed from: private */
    public EditText msgEdit;
    private final int msgEditMaxLen = 200;
    private TextView msgTitle;
    /* access modifiers changed from: private */
    public String recordAbsolutePath;
    /* access modifiers changed from: private */
    public Button recordBtn;
    protected ImageView recordImg;
    private RelativeLayout recordLayout;
    /* access modifiers changed from: private */
    public TextView recordTimeText;
    private RefreshAsyncTask refreshAsyncTask;
    protected ImageView select1;
    protected ImageView select2;
    protected ImageView select3;
    protected ImageView[] selects;
    /* access modifiers changed from: private */
    public SendAsyncTask sendAsyncTask;
    /* access modifiers changed from: private */
    public long startTime;
    /* access modifiers changed from: private */
    public int time;
    private LinearLayout transparentBox;
    private UploadAudioFileAsyncTask uploadAudioFileAsyncTask;
    private UserService userService;
    protected ImageView videoImg;

    public MsgChatRoomFragment() {
    }

    public MsgChatRoomFragment(HeartBeatReceiver heartbeatReceiver2) {
        this.heartbeatReceiver = heartbeatReceiver2;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.heartMsgService = new HeartMsgServiceImpl(this.activity);
        this.userService = new UserServiceImpl(this.activity);
        this.currUserId = this.userService.getLoginUserId();
        this.faceList = MCFaceUtil.getFaceConstant(this.activity).getFaceList();
        updateData();
    }

    private void updateData() {
        this.isLoading = false;
        this.hasNext = true;
        queryDBStartNum = 0;
        sendReceiveMessageTotalNum = 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(this.mcResource.getLayoutId("mc_forum_msg_chat_room_list_fragment"), container, false);
        super.initViews(inflater, container, savedInstanceState);
        this.backBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_back_btn"));
        this.msgEdit = (EditText) this.view.findViewById(this.mcResource.getViewId("mc_forum_msg_chat_room_edit"));
        this.msgBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_msg_chat_room_btn"));
        this.blackBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_black_btn"));
        this.msgTitle = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_msg_chat_room_title_text"));
        updateBlackBtn();
        updateNicknameView();
        this.chatRoomListView = (ListView) this.view.findViewById(this.mcResource.getViewId("mc_forum_chat_room_listview"));
        this.msgChatRoomListAdapter = new MsgChatRoomListAdapter(this.activity, this.currUserId, this.chatUserIcon, new ArrayList(), this.mHandler, this.chatRoomListView, this.asyncTaskLoaderImage);
        this.heartMsgList = new ArrayList();
        this.msgChatRoomListAdapter.setMessageList(this.heartMsgList);
        this.chatRoomListView.setAdapter((ListAdapter) this.msgChatRoomListAdapter);
        this.chatRoomListView.setOnScrollListener(new ChatRoomOnScrollListener());
        this.faceLayout = (LinearLayout) this.view.findViewById(this.mcResource.getViewId("mc_forum_face_layout"));
        this.facePager = (ViewPager) this.view.findViewById(this.mcResource.getViewId("mc_forum_face_pager"));
        this.facePagerAdapter = new FacePagerAdapter(this.activity, this.faceList);
        this.facePager.setAdapter(this.facePagerAdapter);
        this.select1 = (ImageView) this.view.findViewById(this.mcResource.getViewId("mc_forum_select1"));
        this.select2 = (ImageView) this.view.findViewById(this.mcResource.getViewId("mc_forum_select2"));
        this.select3 = (ImageView) this.view.findViewById(this.mcResource.getViewId("mc_forum_select3"));
        this.selects = new ImageView[]{this.select1, this.select2, this.select3};
        this.faceImg = (ImageView) this.view.findViewById(this.mcResource.getViewId("mc_forum_face_btn"));
        this.videoImg = (ImageView) this.view.findViewById(this.mcResource.getViewId("mc_forum_video_btn"));
        this.recordBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_press_record_btn"));
        this.recordImg = (ImageView) this.view.findViewById(this.mcResource.getViewId("mc_forum_record_img"));
        this.recordLayout = (RelativeLayout) this.view.findViewById(this.mcResource.getViewId("mc_forum_record_layout"));
        this.recordTimeText = (TextView) this.view.findViewById(this.mcResource.getViewId("mc_forum_record_time_text"));
        this.transparentBox = (LinearLayout) this.view.findViewById(this.mcResource.getViewId("mc_forum_transparent_box"));
        this.cameraPicImg = (ImageView) this.view.findViewById(this.mcResource.getViewId("mc_forum_camera_pic_img"));
        this.localPicImg = (ImageView) this.view.findViewById(this.mcResource.getViewId("mc_forum_local_pic_img"));
        MCTouchUtil.createTouchDelegate(this.faceImg, 10);
        MCTouchUtil.createTouchDelegate(this.videoImg, 10);
        MCTouchUtil.createTouchDelegate(this.cameraPicImg, 10);
        MCTouchUtil.createTouchDelegate(this.localPicImg, 10);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MsgChatRoomFragment.this.getActivity().finish();
            }
        });
        this.msgBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (LoginInterceptor.doInterceptor(MsgChatRoomFragment.this.activity, null, null)) {
                    if (MsgChatRoomFragment.this.chatUserId == 0) {
                        MsgChatRoomFragment.this.warnMessageById("mc_forum_msg_send_user_error");
                        return;
                    }
                    String message = MsgChatRoomFragment.this.msgEdit.getText().toString();
                    int len = message.length();
                    if (len <= 0) {
                        return;
                    }
                    if (len > 200) {
                        MsgChatRoomFragment.this.warnMessageById("mc_forum_msg_chat_room_len_error");
                        return;
                    }
                    HeartMsgModel heartMsgModel = MsgChatRoomFragment.this.createSendMessageModel(message, 1, 0);
                    MsgChatRoomFragment.this.heartMsgList.add(heartMsgModel);
                    MsgChatRoomFragment.this.msgChatRoomListAdapter.setMessageList(MsgChatRoomFragment.this.heartMsgList);
                    MsgChatRoomFragment.this.msgChatRoomListAdapter.notifyDataSetInvalidated();
                    MsgChatRoomFragment.this.msgChatRoomListAdapter.notifyDataSetChanged();
                    MsgChatRoomFragment.this.chatRoomListView.setSelection(MsgChatRoomFragment.this.heartMsgList.size() - 1);
                    if (!(MsgChatRoomFragment.this.sendAsyncTask == null || MsgChatRoomFragment.this.sendAsyncTask.getStatus() == AsyncTask.Status.FINISHED)) {
                        MsgChatRoomFragment.this.sendAsyncTask.cancel(true);
                    }
                    SendAsyncTask unused = MsgChatRoomFragment.this.sendAsyncTask = new SendAsyncTask();
                    MsgChatRoomFragment.this.sendAsyncTask.execute(heartMsgModel);
                }
            }
        });
        this.msgEdit.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                MsgChatRoomFragment.this.msgEdit.requestFocus();
                MsgChatRoomFragment.this.changeKeyboardState(1);
                return false;
            }
        });
        this.blackBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!(MsgChatRoomFragment.this.blackAsyncTask == null || MsgChatRoomFragment.this.blackAsyncTask.getStatus() == AsyncTask.Status.FINISHED)) {
                    MsgChatRoomFragment.this.blackAsyncTask.cancel(true);
                }
                BlackAsyncTask unused = MsgChatRoomFragment.this.blackAsyncTask = new BlackAsyncTask();
                if (MsgChatRoomFragment.this.getBlackStatus() == 1) {
                    MsgChatRoomFragment.this.blackAsyncTask.execute(0);
                    return;
                }
                MsgChatRoomFragment.this.blackAsyncTask.execute(1);
            }
        });
        this.faceImg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MsgChatRoomFragment.this.msgEdit.setVisibility(0);
                MsgChatRoomFragment.this.recordBtn.setVisibility(8);
                MsgChatRoomFragment.this.msgBtn.setVisibility(0);
                if (MsgChatRoomFragment.this.keyboardState != 2) {
                    MsgChatRoomFragment.this.changeKeyboardState(2);
                } else {
                    MsgChatRoomFragment.this.changeKeyboardState(1);
                }
            }
        });
        this.facePager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                for (int i = 0; i < MsgChatRoomFragment.this.selects.length; i++) {
                    if (i == position) {
                        MsgChatRoomFragment.this.selects[i].setImageDrawable(MsgChatRoomFragment.this.mcResource.getDrawable("mc_forum_select2_2"));
                    } else {
                        MsgChatRoomFragment.this.selects[i].setImageDrawable(MsgChatRoomFragment.this.mcResource.getDrawable("mc_forum_select2_1"));
                    }
                }
            }
        });
        this.facePagerAdapter.setOnFaceImageClickListener(new FacePagerAdapter.OnFaceImageClickListener() {
            public void onFaceImageClick(String tag, Drawable drawable) {
                SpannableStringBuilder spannable = new SpannableStringBuilder(tag);
                spannable.setSpan(new ImageSpan(drawable, 1), 0, tag.length(), 33);
                MsgChatRoomFragment.this.msgEdit.getText().insert(MsgChatRoomFragment.this.msgEdit.getSelectionEnd(), spannable);
            }
        });
        this.videoImg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MsgChatRoomFragment.this.faceLayout.setVisibility(8);
                MsgChatRoomFragment.this.changeKeyboardState(0);
                if (!MsgChatRoomFragment.this.isRecord) {
                    MsgChatRoomFragment.this.faceImg.setVisibility(0);
                    MsgChatRoomFragment.this.videoImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_icon31"));
                    MsgChatRoomFragment.this.msgBtn.setVisibility(8);
                    MsgChatRoomFragment.this.msgEdit.setVisibility(8);
                    MsgChatRoomFragment.this.recordBtn.setVisibility(0);
                    boolean unused = MsgChatRoomFragment.this.isRecord = true;
                    return;
                }
                MsgChatRoomFragment.this.faceImg.setVisibility(0);
                MsgChatRoomFragment.this.videoImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_icon31"));
                MsgChatRoomFragment.this.msgBtn.setVisibility(0);
                MsgChatRoomFragment.this.msgEdit.setVisibility(0);
                MsgChatRoomFragment.this.recordBtn.setVisibility(8);
                boolean unused2 = MsgChatRoomFragment.this.isRecord = false;
            }
        });
        this.recordBtn.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0) {
                    MsgChatRoomFragment.this.recordBtn.setBackgroundResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_button1_h"));
                    MsgChatRoomFragment.this.recordBtn.setText(MsgChatRoomFragment.this.mcResource.getStringId("mc_forum_release_end"));
                    MsgChatRoomFragment.this.showRecordLayout();
                    MsgChatRoomFragment.this.recording();
                } else if (event.getAction() == 1 || event.getAction() == 3) {
                    MsgChatRoomFragment.this.recordBtn.setText(MsgChatRoomFragment.this.mcResource.getStringId("mc_forum_press_record"));
                    MsgChatRoomFragment.this.recordBtn.setBackgroundResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_button1_n"));
                    MsgChatRoomFragment.this.hideRecordLayout();
                    if (!MsgChatRoomFragment.this.isOverTime) {
                        MsgChatRoomFragment.this.recorded();
                    }
                }
                return true;
            }
        });
        this.cameraPicImg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MsgChatRoomFragment.this.msgEdit.setVisibility(0);
                MsgChatRoomFragment.this.recordBtn.setVisibility(8);
                MsgChatRoomFragment.this.msgBtn.setVisibility(0);
                MsgChatRoomFragment.this.cameraPhotoListener();
            }
        });
        this.localPicImg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MsgChatRoomFragment.this.msgEdit.setVisibility(0);
                MsgChatRoomFragment.this.recordBtn.setVisibility(8);
                MsgChatRoomFragment.this.msgBtn.setVisibility(0);
                MsgChatRoomFragment.this.localPhotoListener();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showRecordLayout() {
        this.recordLayout.setVisibility(0);
        this.transparentBox.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void hideRecordLayout() {
        this.recordLayout.setVisibility(8);
        this.transparentBox.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void recording() {
        this.recordAbsolutePath = getRecordAbsolutePath();
        this.mRecorder = AudioRecordUtil.startRecording(this.recordAbsolutePath);
        this.isRecording = true;
        this.isOverTime = false;
        this.startTime = System.currentTimeMillis();
        new TimeThread().start();
        new MonitorThread().start();
    }

    private String getRecordAbsolutePath() {
        String recordAbsolutePath2 = MCLibIOUtil.getAudioCachePath(this.activity.getApplicationContext()) + AUDIO_TEMP_FILE_NAME;
        File f = new File(recordAbsolutePath2);
        if (f.exists()) {
            f.delete();
        }
        return recordAbsolutePath2;
    }

    /* access modifiers changed from: private */
    public void recorded() {
        this.endTime = System.currentTimeMillis();
        this.time = (int) ((this.endTime - this.startTime) / 1000);
        AudioRecordUtil.stopRecording(this.mRecorder);
        this.mRecorder = null;
        this.isRecording = false;
        if (this.time < 1) {
            warnMessageById("mc_forum_warn_recorder_min_len");
            return;
        }
        if (this.uploadAudioFileAsyncTask != null) {
            this.uploadAudioFileAsyncTask.cancel(true);
        }
        this.uploadAudioFileAsyncTask = new UploadAudioFileAsyncTask();
        this.uploadAudioFileAsyncTask.execute(new Void[0]);
    }

    private class TimeThread extends Thread {
        private TimeThread() {
        }

        public void run() {
            while (MsgChatRoomFragment.this.mRecorder != null && MsgChatRoomFragment.this.isRecording) {
                try {
                    long unused = MsgChatRoomFragment.this.endTime = System.currentTimeMillis();
                    final int time = (int) ((MsgChatRoomFragment.this.endTime - MsgChatRoomFragment.this.startTime) / 1000);
                    MsgChatRoomFragment.this.mHandler.post(new Runnable() {
                        public void run() {
                            MsgChatRoomFragment.this.recordTimeText.setText(time + "\"");
                            if (time >= 59) {
                                boolean unused = MsgChatRoomFragment.this.isOverTime = true;
                                MsgChatRoomFragment.this.warnMessageById("mc_forum_warn_recorder_limit");
                                MsgChatRoomFragment.this.recorded();
                            }
                        }
                    });
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class MonitorThread extends Thread {
        private MonitorThread() {
        }

        public void run() {
            while (MsgChatRoomFragment.this.mRecorder != null && MsgChatRoomFragment.this.isRecording) {
                try {
                    final int uv = (MsgChatRoomFragment.this.mRecorder.getMaxAmplitude() * 20) / 32768;
                    MsgChatRoomFragment.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (uv == 0) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_1"));
                            } else if (uv == 1) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_2"));
                            } else if (uv == 2) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_3"));
                            } else if (uv == 3) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_4"));
                            } else if (uv == 4) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_5"));
                            } else if (uv == 5) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_6"));
                            } else if (uv == 6) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_7"));
                            } else if (uv == 7) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_8"));
                            } else if (uv == 8) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_9"));
                            } else if (uv == 9) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_10"));
                            } else if (uv == 10) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_11"));
                            } else if (uv == 11) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_12"));
                            } else if (uv == 12) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_13"));
                            } else if (uv == 13) {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_14"));
                            } else {
                                MsgChatRoomFragment.this.recordImg.setImageResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_recording_img1_15"));
                            }
                        }
                    });
                    Thread.sleep(200);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                MsgChatRoomFragment.this.mHandler.post(new Runnable() {
                    public void run() {
                        MsgChatRoomFragment.this.recordImg.setBackgroundDrawable(null);
                    }
                });
            }
        }
    }

    private class UploadAudioFileAsyncTask extends AsyncTask<Void, Void, String> {
        private UploadAudioFileAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            MsgChatRoomFragment.this.showProgressDialog("mc_forum_warn_update_recored", this);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... arg0) {
            return MsgChatRoomFragment.this.heartMsgService.uploadAudio(MsgChatRoomFragment.this.recordAbsolutePath);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            MsgChatRoomFragment.this.hideProgressDialog();
            if (!StringUtil.isEmpty(result)) {
                String path = result;
                if (path.startsWith(BaseReturnCodeConstant.ERROR_CODE)) {
                    String errorCode = path.substring(path.indexOf(BaseReturnCodeConstant.ERROR_CODE) + 1, path.length());
                    if (errorCode != null && !errorCode.equals("BasePublishTopicActivityWithAudio")) {
                        MsgChatRoomFragment.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(MsgChatRoomFragment.this.activity, errorCode));
                        uploadFail();
                        return;
                    }
                    return;
                }
                MsgChatRoomFragment.this.audioPath = path;
                MsgChatRoomFragment.this.warnMessageById("mc_forum_warn_update_succ");
                uploadSucc();
                return;
            }
            MsgChatRoomFragment.this.warnMessageById("mc_forum_warn_update_fail");
            uploadFail();
        }

        private void uploadSucc() {
            MsgChatRoomFragment.this.changeKeyboardState(0);
            MCLibIOUtil.moveFile(MsgChatRoomFragment.this.recordAbsolutePath, MCLibIOUtil.getAudioCachePath(MsgChatRoomFragment.this.activity.getApplicationContext()) + AudioCache.getHash(MsgChatRoomFragment.this.audioPath));
            HeartMsgModel heartMsgModel = MsgChatRoomFragment.this.createSendMessageModel(MsgChatRoomFragment.this.audioPath, 3, MsgChatRoomFragment.this.time);
            MsgChatRoomFragment.this.heartMsgList.add(heartMsgModel);
            MsgChatRoomFragment.this.msgChatRoomListAdapter.setMessageList(MsgChatRoomFragment.this.heartMsgList);
            MsgChatRoomFragment.this.msgChatRoomListAdapter.notifyDataSetInvalidated();
            MsgChatRoomFragment.this.msgChatRoomListAdapter.notifyDataSetChanged();
            MsgChatRoomFragment.this.chatRoomListView.setSelection(MsgChatRoomFragment.this.heartMsgList.size() - 1);
            if (!(MsgChatRoomFragment.this.sendAsyncTask == null || MsgChatRoomFragment.this.sendAsyncTask.getStatus() == AsyncTask.Status.FINISHED)) {
                MsgChatRoomFragment.this.sendAsyncTask.cancel(true);
            }
            SendAsyncTask unused = MsgChatRoomFragment.this.sendAsyncTask = new SendAsyncTask();
            MsgChatRoomFragment.this.sendAsyncTask.execute(heartMsgModel);
        }

        private void uploadFail() {
            MsgChatRoomFragment.this.clearAudioTempFile();
            MsgChatRoomFragment.this.changeKeyboardState(0);
        }
    }

    /* access modifiers changed from: private */
    public void clearAudioTempFile() {
        File f = new File(MCLibIOUtil.getAudioCachePath(this.activity.getApplicationContext()) + AUDIO_TEMP_FILE_NAME);
        if (f.exists()) {
            f.delete();
        }
    }

    public void onStart() {
        super.onStart();
        registerHeartBeatReceiver(this.activity, this.chatRoomListView);
    }

    public void onResume() {
        super.onResume();
        if (this.flag) {
            updateData();
            updateMessageReceiverData();
            updateNicknameView();
            updateFirstPageView();
            updateBlackBtn();
        }
        this.flag = false;
    }

    /* access modifiers changed from: protected */
    public void changeKeyboardState(int kbState) {
        this.keyboardState = kbState;
        switch (this.keyboardState) {
            case 1:
                this.faceLayout.setVisibility(8);
                this.faceImg.setImageResource(this.mcResource.getDrawableId("mc_forum_icon1"));
                showSoftKeyboard();
                return;
            case 2:
                hideSoftKeyboard();
                this.faceImg.setImageResource(this.mcResource.getDrawableId("mc_forum_icon21"));
                this.faceLayout.setVisibility(0);
                return;
            default:
                hideSoftKeyboard();
                this.faceLayout.setVisibility(8);
                this.faceImg.setImageResource(this.mcResource.getDrawableId("mc_forum_icon1"));
                return;
        }
    }

    private void updateMessageReceiverData() {
        this.messageReceiver.setChatUserId(this.chatUserId);
        this.messageReceiver.setAdapter(this.msgChatRoomListAdapter);
    }

    public void registerHeartBeatReceiver(Context context, ListView chatRoomListView2) {
        if (this.messageReceiver == null) {
            this.messageReceiver = new MessageReceiver(this.activity, chatRoomListView2, this.heartbeatReceiver);
            updateMessageReceiverData();
            if (chatRetrunDelegate != null) {
                this.messageReceiver.setLeftFragment(chatRetrunDelegate.getMsgUserListFragment());
            }
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction(this.activity.getPackageName() + HeartMsgOSService.SERVER_NOTIFICATION_MSG);
        this.activity.registerReceiver(this.messageReceiver, filter);
    }

    public void onStop() {
        super.onStop();
        if (this.messageReceiver != null) {
            this.activity.unregisterReceiver(this.messageReceiver);
        }
    }

    private void updateBlackBtn() {
        if (getBlackStatus() == 1) {
            this.blackBtn.setBackgroundResource(this.mcResource.getDrawableId("mc_forum_top_bar_button23"));
        } else if (getBlackStatus() == 0) {
            this.blackBtn.setBackgroundResource(this.mcResource.getDrawableId("mc_forum_top_bar_button24"));
        }
    }

    private void updateFirstPageView() {
        if (this.chatUserId != 0) {
            if (!(this.refreshAsyncTask == null || this.refreshAsyncTask.getStatus() == AsyncTask.Status.FINISHED)) {
                this.refreshAsyncTask.cancel(true);
            }
            this.refreshAsyncTask = new RefreshAsyncTask();
            this.refreshAsyncTask.execute(new Void[0]);
        }
    }

    private void updateNicknameView() {
        if (getChatUserNickname() != null) {
            this.msgTitle.setText(getChatUserNickname());
            if (getChatUserNickname().length() > 10) {
                this.msgTitle.setFocusable(true);
                this.msgTitle.setGravity(0);
                return;
            }
            this.msgTitle.setFocusable(false);
            this.msgTitle.setGravity(17);
        }
    }

    /* access modifiers changed from: private */
    public HeartMsgModel createSendMessageModel(String content, int type, int time2) {
        HeartMsgModel heartMsgModel = new HeartMsgModel();
        heartMsgModel.setContent(content);
        heartMsgModel.setCreateDate(DateUtil.getCurrentTime());
        heartMsgModel.setFormUserId(this.currUserId);
        heartMsgModel.setToUserId(this.chatUserId);
        heartMsgModel.setSendStatus(-1);
        heartMsgModel.setType(type);
        if (type == 3) {
            SoundModel soundModel = new SoundModel();
            soundModel.setSoundTime((long) time2);
            soundModel.setSoundPath(content);
            heartMsgModel.setSoundModel(soundModel);
        }
        return heartMsgModel;
    }

    /* access modifiers changed from: private */
    public void updateUIAfterSendMess(HeartMsgModel sendMessModel, int sendStatus) {
        int size = this.heartMsgList.size();
        for (int i = 0; i < size; i++) {
            if (this.heartMsgList.get(i).equals(sendMessModel)) {
                this.heartMsgList.get(i).setSendStatus(sendStatus);
                this.msgChatRoomListAdapter.setMessageList(this.heartMsgList);
                this.msgChatRoomListAdapter.notifyDataSetInvalidated();
                this.msgChatRoomListAdapter.notifyDataSetChanged();
                this.chatRoomListView.setSelection(this.heartMsgList.size() - 1);
            }
        }
    }

    private class RefreshAsyncTask extends AsyncTask<Void, Void, List<HeartMsgModel>> {
        private List<HeartMsgModel> messageNetList;

        private RefreshAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<HeartMsgModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            MsgChatRoomFragment.sendReceiveMessageTotalNum = 0;
            MsgChatRoomFragment.queryDBStartNum = 0;
            boolean unused = MsgChatRoomFragment.this.isLoading = true;
            Intent intent = new Intent(MsgChatRoomFragment.this.getActivity(), MediaService.class);
            intent.putExtra(MediaService.SERVICE_TAG, MsgChatRoomFragment.this.getActivity().toString());
            intent.putExtra(MediaService.SERVICE_STATUS, 6);
            MsgChatRoomFragment.this.getActivity().startService(intent);
        }

        /* access modifiers changed from: protected */
        public List<HeartMsgModel> doInBackground(Void... params) {
            this.messageNetList = MsgChatRoomFragment.this.heartMsgService.getMessageList(MsgChatRoomFragment.this.chatUserId);
            List<HeartMsgModel> messageTotalList = new ArrayList<>();
            int size = this.messageNetList.size();
            if (size > 0) {
                MsgChatRoomFragment.sendReceiveMessageTotalNum = this.messageNetList.get(0).getDBSaveNum() + MsgChatRoomFragment.sendReceiveMessageTotalNum;
            }
            if (size < 50) {
                List<HeartMsgModel> messageDBList = MsgChatRoomFragment.this.heartMsgService.getAllMessages(MsgChatRoomFragment.this.chatUserId, MsgChatRoomFragment.sendReceiveMessageTotalNum, 50 - size);
                if (messageDBList.size() > 0) {
                    List<HeartMsgModel> messageDBList2 = MCForumReverseList.reverseList(messageDBList);
                    messageTotalList.addAll(messageDBList2);
                    MsgChatRoomFragment.queryDBStartNum += messageDBList2.size();
                } else {
                    boolean unused = MsgChatRoomFragment.this.hasNext = false;
                }
            }
            if (size > 0) {
                messageTotalList.addAll(this.messageNetList);
            }
            MsgChatRoomFragment.queryDBStartNum += MsgChatRoomFragment.sendReceiveMessageTotalNum;
            return messageTotalList;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<HeartMsgModel> result) {
            super.onPostExecute((Object) result);
            if (result != null && !result.isEmpty() && StringUtil.isEmpty(result.get(0).getErrorCode())) {
                MsgChatRoomFragment.this.heartMsgList.addAll(result);
                MsgChatRoomFragment.this.msgChatRoomListAdapter.setMessageList(MsgChatRoomFragment.this.heartMsgList);
                MsgChatRoomFragment.this.msgChatRoomListAdapter.notifyDataSetInvalidated();
                MsgChatRoomFragment.this.msgChatRoomListAdapter.notifyDataSetChanged();
                MsgChatRoomFragment.this.chatRoomListView.setSelection(result.size() - 1);
                boolean unused = MsgChatRoomFragment.this.isLoading = false;
                MsgChatRoomFragment.this.heartMsgService.updateMessage(this.messageNetList);
                if (MsgChatRoomFragment.chatRetrunDelegate != null) {
                    MsgChatRoomFragment.chatRetrunDelegate.invalidateUserList();
                }
                if (MsgChatRoomFragment.this.heartbeatReceiver != null) {
                    MsgChatRoomFragment.this.heartbeatReceiver.setMsgNotification(MsgChatRoomFragment.this.activity, 0);
                }
            }
        }
    }

    private class MoreAsyncTask extends AsyncTask<Void, Void, List<HeartMsgModel>> {
        private MoreAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<HeartMsgModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            boolean unused = MsgChatRoomFragment.this.isLoading = true;
        }

        /* access modifiers changed from: protected */
        public List<HeartMsgModel> doInBackground(Void... params) {
            List<HeartMsgModel> messageDBList = MsgChatRoomFragment.this.heartMsgService.getAllMessages(MsgChatRoomFragment.this.chatUserId, MsgChatRoomFragment.queryDBStartNum, 50);
            MsgChatRoomFragment.queryDBStartNum += messageDBList.size();
            return messageDBList;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<HeartMsgModel> result) {
            super.onPostExecute((Object) result);
            if (result == null) {
                return;
            }
            if (result.isEmpty()) {
                boolean unused = MsgChatRoomFragment.this.hasNext = false;
                return;
            }
            List<HeartMsgModel> tResult = MCForumReverseList.reverseList(result);
            tResult.addAll(MsgChatRoomFragment.this.heartMsgList);
            List unused2 = MsgChatRoomFragment.this.heartMsgList = tResult;
            MsgChatRoomFragment.this.msgChatRoomListAdapter.setMessageList(MsgChatRoomFragment.this.heartMsgList);
            MsgChatRoomFragment.this.msgChatRoomListAdapter.notifyDataSetInvalidated();
            MsgChatRoomFragment.this.msgChatRoomListAdapter.notifyDataSetChanged();
            MsgChatRoomFragment.this.chatRoomListView.setSelection(result.size() - 1);
            boolean unused3 = MsgChatRoomFragment.this.isLoading = false;
        }
    }

    private class SendAsyncTask extends AsyncTask<HeartMsgModel, Void, String> {
        HeartMsgModel sendMessModel;

        private SendAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            MsgChatRoomFragment.this.msgEdit.setText("");
        }

        /* access modifiers changed from: protected */
        public String doInBackground(HeartMsgModel... params) {
            this.sendMessModel = params[0];
            return MsgChatRoomFragment.this.heartMsgService.sendMessage(this.sendMessModel);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            super.onPostExecute((Object) result);
            if (!StringUtil.isEmpty(result)) {
                MsgChatRoomFragment.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(MsgChatRoomFragment.this.activity, result));
                MsgChatRoomFragment.this.updateUIAfterSendMess(this.sendMessModel, -2);
            }
            if (result == null) {
                MsgChatRoomFragment.this.updateUIAfterSendMess(this.sendMessModel, 0);
            }
            if (result != null && result.equals("")) {
                MsgChatRoomFragment.this.updateUIAfterSendMess(this.sendMessModel, 0);
            }
        }
    }

    private class BlackAsyncTask extends AsyncTask<Integer, Void, String> {
        private int blackStatus;

        private BlackAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Integer... params) {
            this.blackStatus = params[0].intValue();
            return MsgChatRoomFragment.this.heartMsgService.setUserBlack(this.blackStatus, MsgChatRoomFragment.this.currUserId, MsgChatRoomFragment.this.chatUserId);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (!StringUtil.isEmpty(result)) {
                MsgChatRoomFragment.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(MsgChatRoomFragment.this.activity, result));
            } else if (result != null) {
            } else {
                if (MsgChatRoomFragment.this.getBlackStatus() == 0) {
                    MsgChatRoomFragment.this.warnMessageById("mc_forum_black_user_succ");
                    MsgChatRoomFragment.this.blackBtn.setBackgroundResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_top_bar_button23"));
                    MsgChatRoomFragment.this.setBlackStatus(1);
                } else if (MsgChatRoomFragment.this.getBlackStatus() == 1) {
                    MsgChatRoomFragment.this.warnMessageById("mc_forum_un_black_user_succ");
                    MsgChatRoomFragment.this.blackBtn.setBackgroundResource(MsgChatRoomFragment.this.mcResource.getDrawableId("mc_forum_top_bar_button24"));
                    MsgChatRoomFragment.this.setBlackStatus(0);
                }
            }
        }
    }

    private class ChatRoomOnScrollListener implements AbsListView.OnScrollListener {
        private int firstVisibleItem;
        private int totalItemCount;
        private int visibleItemCount;

        private ChatRoomOnScrollListener() {
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0 && this.firstVisibleItem == 0 && !MsgChatRoomFragment.this.isLoading && MsgChatRoomFragment.this.hasNext && MsgChatRoomFragment.this.chatUserId != 0) {
                if (!(MsgChatRoomFragment.this.moreAsyncTask == null || MsgChatRoomFragment.this.moreAsyncTask.getStatus() == AsyncTask.Status.FINISHED)) {
                    MsgChatRoomFragment.this.moreAsyncTask.cancel(true);
                }
                MoreAsyncTask unused = MsgChatRoomFragment.this.moreAsyncTask = new MoreAsyncTask();
                MsgChatRoomFragment.this.moreAsyncTask.execute(new Void[0]);
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
            this.firstVisibleItem = firstVisibleItem2;
            this.visibleItemCount = visibleItemCount2;
            this.totalItemCount = totalItemCount2;
        }
    }

    public String getChatUserNickname() {
        return this.chatUserNickname;
    }

    public void setChatUserNickname(String chatUserNickname2) {
        this.chatUserNickname = chatUserNickname2;
    }

    public long getChatUserId() {
        return this.chatUserId;
    }

    public void setChatUserId(long chatUserId2) {
        this.chatUserId = chatUserId2;
    }

    public int getBlackStatus() {
        return this.blackStatus;
    }

    public void setBlackStatus(int blackStatus2) {
        this.blackStatus = blackStatus2;
    }

    public String getChatUserIcon() {
        return this.chatUserIcon;
    }

    public void setChatUserIcon(String chatUserIcon2) {
        this.chatUserIcon = chatUserIcon2;
        if (this.msgChatRoomListAdapter != null) {
            this.msgChatRoomListAdapter.setChatUserIcon(chatUserIcon2);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (!(this.refreshAsyncTask == null || this.refreshAsyncTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.refreshAsyncTask.cancel(true);
        }
        if (!(this.moreAsyncTask == null || this.moreAsyncTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.moreAsyncTask.cancel(true);
        }
        if (!(this.sendAsyncTask == null || this.sendAsyncTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.sendAsyncTask.cancel(true);
        }
        if (this.blackAsyncTask != null && this.blackAsyncTask.getStatus() != AsyncTask.Status.FINISHED) {
            this.blackAsyncTask.cancel(true);
        }
    }

    public static ChatRetrunDelegate getChatRetrunDelegate() {
        return chatRetrunDelegate;
    }

    public static void setChatRetrunDelegate(ChatRetrunDelegate chatRetrunDelegate2) {
        chatRetrunDelegate = chatRetrunDelegate2;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.faceLayout.getVisibility() == 0) {
                this.faceLayout.setVisibility(8);
                this.faceImg.setImageResource(this.mcResource.getDrawableId("mc_forum_icon1"));
                return true;
            } else if (this.recordBtn.getVisibility() == 0) {
                this.faceImg.setVisibility(0);
                this.videoImg.setImageResource(this.mcResource.getDrawableId("mc_forum_icon31"));
                this.msgBtn.setVisibility(0);
                this.msgEdit.setVisibility(0);
                this.recordBtn.setVisibility(8);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void updateUIAfterUpload() {
        MCLibIOUtil.moveFile(this.path, MCLibIOUtil.getImageCachePath(this.activity) + ImageCache.getHash(this.returnPath));
        HeartMsgModel heartMsgModel = createSendMessageModel(this.returnPath, 2, 0);
        this.heartMsgList.add(heartMsgModel);
        this.msgChatRoomListAdapter.setMessageList(this.heartMsgList);
        this.msgChatRoomListAdapter.notifyDataSetInvalidated();
        this.msgChatRoomListAdapter.notifyDataSetChanged();
        this.chatRoomListView.setSelection(this.heartMsgList.size() - 1);
        if (!(this.sendAsyncTask == null || this.sendAsyncTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.sendAsyncTask.cancel(true);
        }
        this.sendAsyncTask = new SendAsyncTask();
        this.sendAsyncTask.execute(heartMsgModel);
    }
}
