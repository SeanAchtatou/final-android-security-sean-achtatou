package com.mobcent.base.android.ui.activity.helper;

import android.content.Context;
import android.graphics.Bitmap;
import com.mobcent.android.db.McShareSharedPreferencesDB;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.util.MCLibIOUtil;
import com.mobcent.forum.android.util.StringUtil;
import com.mobcent.share.android.activity.helper.MCShareLaunchShareHelper;
import com.mobcent.share.android.constant.MCShareConstant;
import com.tencent.mm.sdk.openapi.IWXAPI;
import java.io.File;

public class MCForumLaunchShareHelper {
    public static final String LOCAL_POSITION_DIR = (File.separator + "mobcent" + File.separator + MCShareConstant.EXHIBITION_SHARE + File.separator);

    public static void shareAppContent(String content, String shareUrl, String smsUrl, Context context) {
        MCShareLaunchShareHelper.shareAppContent(getShareAppKey(context), content, shareUrl, smsUrl, context);
    }

    public static void shareContent(String content, String shareUrl, String smsUrl, Context context) {
        MCShareLaunchShareHelper.shareContent(getShareAppKey(context), content, shareUrl, smsUrl, context);
    }

    public static void shareContentWithImageUrl(String content, String picUrl, String shareUrl, String smsUrl, Context context) {
        MCShareLaunchShareHelper.shareContentWithImageUrl(getShareAppKey(context), content, picUrl, shareUrl, smsUrl, context);
    }

    public static void shareContentWithBitmap(String content, Bitmap shareBitmap, String shareUrl, String smsUrl, Context context) {
        MCShareLaunchShareHelper.shareContentWithBitmap(getShareAppKey(context), content, shareBitmap, shareUrl, smsUrl, context);
    }

    public static void shareContentWithBitmapAndUrl(String content, Bitmap shareBitmap, String picUrl, String shareUrl, String smsUrl, Context context) {
        MCShareLaunchShareHelper.shareContentWithBitmapAndUrl(getShareAppKey(context), content, shareBitmap, picUrl, shareUrl, smsUrl, context);
    }

    public static void shareContentWithImageFile(String content, String imageFilePath, String shareUrl, String smsUrl, Context context) {
        MCShareLaunchShareHelper.shareContentWithImageFile(getShareAppKey(context), content, imageFilePath, getShareDownURL(shareUrl, context), smsUrl, context);
    }

    public static void shareContentWithImageFileAndUrl(String content, String imageFilePath, String picUrl, String shareUrl, String smsUrl, Context context) {
        MCShareLaunchShareHelper.shareContentWithImageFileAndUrl(getShareAppKey(context), content, imageFilePath, picUrl, getShareDownURL(shareUrl, context), smsUrl, context);
    }

    public static void shareWay(boolean isHasImg, String content, String imageFilePath, String picUrl, String shareUrl, String smsUrl, Context context, IWXAPI api) {
        boolean z = isHasImg;
        MCShareLaunchShareHelper.shareWay(z, getShareAppKey(context), content, imageFilePath, picUrl, getShareDownURL(shareUrl, context), smsUrl, context, api);
    }

    private static String getImageSharePath(Context context) {
        return MCLibIOUtil.getBaseLocalLocation(context) + LOCAL_POSITION_DIR + "mcshareimage.jpg";
    }

    public static String getDefaultShareUrl(Context context) {
        int shareUrlResId = MCResource.getInstance(context).getStringId("mc_forum_share_url");
        if (shareUrlResId <= 0) {
            return "";
        }
        return context.getResources().getString(shareUrlResId);
    }

    private static String getShareDownURL(String shareURL, Context context) {
        String shareUrl = shareURL;
        if (!StringUtil.isEmpty(shareURL)) {
            return shareUrl;
        }
        try {
            return McShareSharedPreferencesDB.getInstance(context).getShareUrl();
        } catch (Exception e) {
            return "";
        }
    }

    private static String getShareAppKey(Context context) {
        try {
            return SharedPreferencesDB.getInstance(context).getForumKey();
        } catch (Exception e) {
            return MCResource.getInstance(context).getString("mc_forum_share_key");
        }
    }
}
