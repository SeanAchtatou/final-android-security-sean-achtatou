package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TextView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.FriendAdapter;
import com.mobcent.base.android.ui.activity.adapter.TabsAdapter;
import com.mobcent.base.android.ui.activity.fragment.RecommendUserFragment;
import com.mobcent.base.android.ui.activity.fragment.UserFanFragment;
import com.mobcent.base.android.ui.activity.fragment.UserFollowFragment;
import com.mobcent.base.android.ui.activity.fragment.UserFriendsFrament;
import com.mobcent.base.android.ui.activity.fragment.UserSurroundFragment;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.List;

public class UserFriendsFragmentActivity extends BaseFragmentActivity implements MCConstant {
    Button backBtn;
    /* access modifiers changed from: private */
    public FriendAdapter.FriendsClickListener friendsClickListener = new FriendAdapter.FriendsClickListener() {
        public void onUserHomeClick(View arg0, UserInfoModel userInfoModel) {
            MCForumHelper.gotoUserInfo(UserFriendsFragmentActivity.this, UserFriendsFragmentActivity.this.resource, userInfoModel.getUserId());
        }

        public void onMsgChatRoomClick(View arg0, UserInfoModel userInfoModel) {
            Intent intent = new Intent(UserFriendsFragmentActivity.this, MsgChatRoomFragmentActivity.class);
            intent.putExtra(MCConstant.CHAT_USER_ID, userInfoModel.getUserId());
            intent.putExtra(MCConstant.CHAT_USER_NICKNAME, userInfoModel.getNickname());
            intent.putExtra(MCConstant.BLACK_USER_STATUS, userInfoModel.getBlackStatus());
            intent.putExtra(MCConstant.CHAT_USER_ICON, userInfoModel.getIcon());
            UserFriendsFragmentActivity.this.startActivity(intent);
        }
    };
    LayoutInflater inflater;
    private boolean isMyFriend = false;
    TabHost mTabHost;
    UserFriendsTabsAdapter mTabsAdapter;
    ViewPager mViewPager;
    private TextView titleText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            this.mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
        }
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.inflater = LayoutInflater.from(this);
        this.isMyFriend = ((Boolean) getIntent().getSerializableExtra(MCConstant.IS_MY_FRIENDS)).booleanValue();
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_user_friends_fragment_activity"));
        this.titleText = (TextView) findViewById(this.resource.getViewId("mc_forum_user_list_title_btn"));
        this.mTabHost = (TabHost) findViewById(16908306);
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.mTabHost.setup();
        this.mViewPager = (ViewPager) findViewById(this.resource.getViewId("mc_forum_pager"));
        this.mTabsAdapter = new UserFriendsTabsAdapter(this, this.mTabHost, this.mViewPager);
        String recommend = getResources().getString(this.resource.getStringId("mc_forum_recommend_users"));
        String follow = getResources().getString(this.resource.getStringId("mc_forum_user_follow"));
        String fan = getResources().getString(this.resource.getStringId("mc_forum_user_fan"));
        String surround = getResources().getString(this.resource.getStringId("mc_forum_surround_user"));
        this.mTabsAdapter.addTab(this.mTabHost.newTabSpec(recommend).setIndicator(createTabView(recommend)), RecommendUserFragment.class, null);
        if (this.isMyFriend) {
            this.mTabsAdapter.addTab(this.mTabHost.newTabSpec(follow).setIndicator(createTabView(follow)), UserFollowFragment.class, null);
            this.mTabsAdapter.addTab(this.mTabHost.newTabSpec(fan).setIndicator(createTabView(fan)), UserFanFragment.class, null);
            return;
        }
        this.titleText.setText(getResources().getString(this.resource.getStringId("mc_forum_find_friends")));
        this.mTabsAdapter.addTab(this.mTabHost.newTabSpec(surround).setIndicator(createTabView(surround)), UserSurroundFragment.class, null);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserFriendsFragmentActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tab", this.mTabHost.getCurrentTabTag());
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return null;
    }

    private View createTabView(String string) {
        View v = this.inflater.inflate(this.resource.getLayoutId("mc_forum_tabs_item"), (ViewGroup) null);
        ((TextView) v.findViewById(this.resource.getViewId("mc_forum_tab_text"))).setText(string);
        return v;
    }

    public class UserFriendsTabsAdapter extends TabsAdapter {
        public UserFriendsTabsAdapter(FragmentActivity activity, TabHost tabHost, ViewPager pager) {
            super(activity, tabHost, pager);
        }

        public Fragment getItem(int position) {
            Fragment fragment = super.getItem(position);
            if (fragment.getClass() == UserSurroundFragment.class) {
                ((UserSurroundFragment) fragment).setFriendsClickListener(UserFriendsFragmentActivity.this.friendsClickListener);
            } else {
                ((UserFriendsFrament) fragment).setFriendsClickListener(UserFriendsFragmentActivity.this.friendsClickListener);
            }
            return fragment;
        }
    }
}
