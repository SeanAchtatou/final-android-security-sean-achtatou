package com.mobcent.base.android.ui.activity.delegate;

public interface TaskExecuteDelegate {
    void executeFail();

    void executeSuccess();
}
