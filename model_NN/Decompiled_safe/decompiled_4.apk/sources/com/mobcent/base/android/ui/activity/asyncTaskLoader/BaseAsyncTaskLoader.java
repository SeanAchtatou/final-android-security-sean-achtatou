package com.mobcent.base.android.ui.activity.asyncTaskLoader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

public abstract class BaseAsyncTaskLoader<D> extends AsyncTaskLoader<D> {
    private D data;

    public BaseAsyncTaskLoader(Context context) {
        super(context);
    }

    public void deliverResult(D data2) {
        if (isReset() && data2 != null) {
            onReleaseResources(data2);
        }
        this.data = data2;
        if (isStarted()) {
            super.deliverResult(data2);
        }
    }

    /* access modifiers changed from: protected */
    public void onStartLoading() {
        if (this.data != null) {
            deliverResult(this.data);
        }
        if (takeContentChanged() || this.data == null) {
            forceLoad();
        }
    }

    /* access modifiers changed from: protected */
    public void onStopLoading() {
        cancelLoad();
    }

    public void onCanceled(D data2) {
        super.onCanceled(data2);
        onReleaseResources(data2);
    }

    /* access modifiers changed from: protected */
    public void onReset() {
        super.onReset();
        onStopLoading();
        if (this.data != null) {
            onReleaseResources(this.data);
            this.data = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onReleaseResources(D d) {
    }
}
