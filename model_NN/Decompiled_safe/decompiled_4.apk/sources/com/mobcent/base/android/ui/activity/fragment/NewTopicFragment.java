package com.mobcent.base.android.ui.activity.fragment;

import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import java.util.List;

public class NewTopicFragment extends HomeTopicFragment {
    public List<TopicModel> getHomeTopicList() {
        return new PostsServiceImpl(getActivity()).getPostTimeTopicList(0, this.currentPage, this.pageSize, this.isLocal);
    }
}
