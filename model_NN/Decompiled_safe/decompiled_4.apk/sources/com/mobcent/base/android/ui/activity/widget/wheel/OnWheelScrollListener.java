package com.mobcent.base.android.ui.activity.widget.wheel;

public interface OnWheelScrollListener {
    void onScrollingFinished(WheelView wheelView);

    void onScrollingStarted(WheelView wheelView);
}
