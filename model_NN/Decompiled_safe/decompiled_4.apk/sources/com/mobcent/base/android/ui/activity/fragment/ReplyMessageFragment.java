package com.mobcent.base.android.ui.activity.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mobcent.forum.android.model.PostsNoticeModel;
import java.util.List;

public class ReplyMessageFragment extends MessageFragment {
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.initViews(inflater, container, savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public List<PostsNoticeModel> getPostsNoticeList() {
        return this.postsService.getPostsNoticeList(this.userId, this.page, this.pageSize);
    }

    /* access modifiers changed from: protected */
    public boolean updateNoitce() {
        return this.postsService.updateReplyRemindState(this.userId, this.replyIds);
    }
}
