package com.mobcent.base.android.ui.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.adapter.PostsDetailAdapter;
import com.mobcent.base.android.ui.activity.helper.MCForumLaunchShareHelper;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.model.AnnoModel;
import com.mobcent.forum.android.model.RichImageModel;
import com.mobcent.forum.android.model.TopicContentModel;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.util.DateUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class AnnounceActivity extends BaseActivity {
    public static final String TAG = "AnnounceActivity";
    /* access modifiers changed from: private */
    public ImageView annoAuthorIconImg;
    /* access modifiers changed from: private */
    public TextView annoAuthorNameText;
    /* access modifiers changed from: private */
    public ImageView annoAuthorRoleImg;
    /* access modifiers changed from: private */
    public PostsDetailAdapter annoContentAdapter;
    /* access modifiers changed from: private */
    public List<TopicContentModel> annoContentList;
    /* access modifiers changed from: private */
    public AnnoModel annoModel;
    /* access modifiers changed from: private */
    public TextView annoStartDateText;
    /* access modifiers changed from: private */
    public TextView annoTitleText;
    /* access modifiers changed from: private */
    public ListView announceContentListView;
    private ObtainAnnounceDetailTask announceDetailTask;
    private long announceId;
    private Button backBtn;
    private long boardId;
    private String boardName = "";
    private TextView boardNameBtn;
    /* access modifiers changed from: private */
    public PostsService postsService;
    /* access modifiers changed from: private */
    public ArrayList<RichImageModel> richImageModelList;
    private Button shareAnnounceBtn;
    /* access modifiers changed from: private */
    public RelativeLayout showAnnounceDetailLayout;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onRefresh();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.postsService = new PostsServiceImpl(this);
        this.boardName = getIntent().getStringExtra("boardName");
        this.announceId = getIntent().getLongExtra("announceId", 0);
        this.boardId = getIntent().getLongExtra("boardId", 0);
        if (this.boardId == 0) {
            this.boardName = getString(this.resource.getStringId("mc_forum_announce_detail"));
        } else {
            this.boardName = getString(this.resource.getStringId("mc_forum_board_announce_detail"));
        }
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_announce_list_activity"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.shareAnnounceBtn = (Button) findViewById(this.resource.getViewId("mc_forum_share_announce_btn"));
        this.boardNameBtn = (TextView) findViewById(this.resource.getViewId("mc_forum_board_name_btn"));
        this.annoAuthorIconImg = (ImageView) findViewById(this.resource.getViewId("mc_forum_announce_author_img"));
        this.annoAuthorRoleImg = (ImageView) findViewById(this.resource.getViewId("mc_forum_role_img"));
        this.announceContentListView = (ListView) findViewById(this.resource.getViewId("mc_forum_anno_content_list"));
        this.annoAuthorNameText = (TextView) findViewById(this.resource.getViewId("mc_forum_announce_author_text"));
        this.annoTitleText = (TextView) findViewById(this.resource.getViewId("mc_forum_announce_title_text"));
        this.annoStartDateText = (TextView) findViewById(this.resource.getViewId("mc_forum_announce_start_date_text"));
        this.showAnnounceDetailLayout = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_show_announce_detail_layout"));
        this.boardNameBtn.setText(this.boardName);
        this.boardNameBtn.setEnabled(false);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AnnounceActivity.this.back();
            }
        });
        this.shareAnnounceBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String shareContent;
                if (AnnounceActivity.this.annoModel.getTopicContentList().get(0).getInfor().length() <= 70) {
                    shareContent = AnnounceActivity.this.annoModel.getTopicContentList().get(0).getInfor();
                } else {
                    shareContent = AnnounceActivity.this.annoModel.getTopicContentList().get(0).getInfor().substring(0, 70);
                }
                MCForumLaunchShareHelper.shareContent(BaseReturnCodeConstant.ERROR_CODE + AnnounceActivity.this.annoModel.getTitle() + BaseReturnCodeConstant.ERROR_CODE + "\n" + shareContent, "", "", AnnounceActivity.this);
            }
        });
    }

    private class ObtainAnnounceDetailTask extends AsyncTask<Long, Void, AnnoModel> {
        private ObtainAnnounceDetailTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public AnnoModel doInBackground(Long... params) {
            return AnnounceActivity.this.postsService.getAnnoDetail(params[0].longValue(), params[1].longValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(AnnoModel result) {
            if (result == null) {
                Toast.makeText(AnnounceActivity.this, AnnounceActivity.this.getString(AnnounceActivity.this.resource.getStringId("mc_forum_no_data")), 0).show();
            } else if (!StringUtil.isEmpty(result.getErrorCode())) {
                Toast.makeText(AnnounceActivity.this, MCForumErrorUtil.convertErrorCode(AnnounceActivity.this, result.getErrorCode()), 0).show();
            } else {
                AnnoModel unused = AnnounceActivity.this.annoModel = result;
                AnnounceActivity.this.annoAuthorNameText.setText(AnnounceActivity.this.annoModel.getAuthor());
                AnnounceActivity.this.annoTitleText.setText(AnnounceActivity.this.annoModel.getTitle());
                AnnounceActivity.this.annoAuthorIconImg.setBackgroundResource(AnnounceActivity.this.resource.getDrawableId("mc_forum_head"));
                AnnounceActivity.this.annoAuthorRoleImg.setVisibility(8);
                if (AnnounceActivity.this.annoModel.getAnnoStartDate() > 0) {
                    AnnounceActivity.this.annoStartDateText.setText(DateUtil.getFormatTime(AnnounceActivity.this.annoModel.getAnnoStartDate()));
                }
                List unused2 = AnnounceActivity.this.annoContentList = AnnounceActivity.this.annoModel.getTopicContentList();
                PostsDetailAdapter unused3 = AnnounceActivity.this.annoContentAdapter = new PostsDetailAdapter(AnnounceActivity.this, AnnounceActivity.this.annoContentList, AnnounceActivity.this.mHandler, AnnounceActivity.this.richImageModelList, AnnounceActivity.this.asyncTaskLoaderImage);
                AnnounceActivity.this.announceContentListView.setAdapter((ListAdapter) AnnounceActivity.this.annoContentAdapter);
                AnnounceActivity.this.showAnnounceDetailLayout.setVisibility(0);
            }
        }
    }

    public void onRefresh() {
        if (this.announceDetailTask != null) {
            this.announceDetailTask.cancel(true);
        }
        this.announceDetailTask = new ObtainAnnounceDetailTask();
        this.announceDetailTask.execute(Long.valueOf(this.boardId), Long.valueOf(this.announceId));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.announceDetailTask != null) {
            this.announceDetailTask.cancel(true);
        }
    }
}
