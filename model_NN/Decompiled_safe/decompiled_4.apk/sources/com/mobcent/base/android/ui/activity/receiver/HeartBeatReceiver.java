package com.mobcent.base.android.ui.activity.receiver;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCPhoneUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.HeartMsgModel;
import com.mobcent.forum.android.model.PushMessageModel;
import com.mobcent.forum.android.os.service.HeartMsgOSService;
import com.mobcent.forum.android.service.HeartMsgService;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.HeartMsgServiceImpl;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.service.impl.helper.HeartMsgServiceImplHelper;
import com.mobcent.forum.android.util.AppUtil;
import com.mobcent.forum.android.util.MCLogUtil;
import java.util.List;

public class HeartBeatReceiver extends BroadcastReceiver implements MCConstant {
    public static final int MSG_TOTAL_NUM_ID = 1;
    public static final String PUSH = "push";
    public static final String PUSH_MSG_MODEL = "pushMessageModel";
    public static final int PUSH_NUM_ID = 4;
    public static final int RELATIONAL_NOTICE_NUM_ID = 3;
    public static final int REPLY_NOTICE_NUM_ID = 2;
    private final int NONE_MESSAGE_NONE_USER = 1;
    private final int SOME_MESSAGE_ONE_USER = 2;
    private final int SOME_MESSAGE_SOME_USER = 3;
    private final String TAG = "HeartBeatReceiver";
    private Notification atNotification;
    private HeartMsgService heartMsgService;
    private int icon;
    private Notification msgNotification;
    private NotificationManager myNotificationManager;
    private PostsService postsService;
    private Notification pushMsgNotification;
    private Notification replyNotification;
    private boolean showRelationalNotification = true;
    private boolean showReplyNotification = true;

    public void onReceive(Context context, Intent intent) {
        if (this.msgNotification == null) {
            this.myNotificationManager = (NotificationManager) context.getSystemService("notification");
            this.msgNotification = new Notification(0, "", System.currentTimeMillis());
            this.replyNotification = new Notification(0, "", System.currentTimeMillis());
            this.atNotification = new Notification(0, "", System.currentTimeMillis());
            this.pushMsgNotification = new Notification(0, "", System.currentTimeMillis());
            this.postsService = new PostsServiceImpl(context);
            this.heartMsgService = new HeartMsgServiceImpl(context);
        }
        PushMessageModel pushMessageModel = (PushMessageModel) intent.getSerializableExtra(PUSH_MSG_MODEL);
        if (pushMessageModel != null) {
            MCForumHelper.setPushMsgIntent(context, pushMessageModel);
        } else if (intent.getExtras() != null) {
            boolean netCheck = intent.getBooleanExtra(HeartMsgOSService.NET_CHECK, false);
            MCLogUtil.e("test", "netCheck==" + netCheck);
            if (netCheck) {
                setNetNotification(context, intent.getBooleanExtra(HeartMsgOSService.MOBILE_NET, false));
                return;
            }
            int replyNoticeNum = intent.getIntExtra("reply_notice_num", 0);
            int msgTotalNum = intent.getIntExtra("msg_total_num", 0);
            int relationalNoticeNum = intent.getIntExtra("relational_notice_num", 0);
            int pushMsgNum = intent.getIntExtra(HeartMsgOSService.PUSH_NOTICE_NUM, 0);
            this.icon = intent.getIntExtra(HeartMsgOSService.APP_ICON_SEND_INTENT, 0);
            if (this.icon <= 0) {
                this.icon = MCResource.getInstance(context).getDrawableId("mc_forum_msg");
            }
            if (this.postsService.getReplyNotificationFlag()) {
                setReplyNotification(context, replyNoticeNum);
            }
            if (this.postsService.getMentionFriendNotificationFlag()) {
                setRelationalNotification(context, relationalNoticeNum);
            }
            if (msgTotalNum > 0) {
                setMsgNotification(context, msgTotalNum);
            }
            if (pushMsgNum > 0) {
                setPushMsgNotification(context, msgTotalNum);
            }
        }
    }

    private void setPushMsgNotification(Context context, int inviateTotalNum) {
        List<PushMessageModel> pushMsgList = HeartMsgServiceImplHelper.getPushMsgList(SharedPreferencesDB.getInstance(context).getPushMsgListJson());
        MCLogUtil.e("HeartBeatReceiver", "----------" + (pushMsgList != null ? Integer.valueOf(pushMsgList.size()) : null));
        if (pushMsgList != null && pushMsgList.size() > 0) {
            for (int i = 0; i < pushMsgList.size(); i++) {
                this.myNotificationManager.cancel(4);
                String title = pushMsgList.get(i).getTitle();
                if (context == null) {
                    MCLogUtil.e("HeartBeatReceiver", "context == null");
                }
                Intent intent = new Intent();
                intent.setAction(AppUtil.getPackageName(context) + HeartMsgOSService.SERVER_NOTIFICATION_MSG);
                Bundle bundle = new Bundle();
                bundle.putSerializable(PUSH_MSG_MODEL, pushMsgList.get(i));
                intent.putExtras(bundle);
                PendingIntent contentIntent = PendingIntent.getBroadcast(context, 4, intent, 134217728);
                this.pushMsgNotification.flags = 16;
                this.pushMsgNotification.icon = this.icon;
                this.pushMsgNotification.tickerText = title;
                this.pushMsgNotification.when = System.currentTimeMillis();
                this.pushMsgNotification.defaults = 1;
                this.pushMsgNotification.setLatestEventInfo(context, title, pushMsgList.get(i).getPushDesc(), contentIntent);
                this.myNotificationManager.notify(4, this.pushMsgNotification);
            }
        }
    }

    private void setNetNotification(final Context context, boolean isMoblieNet) {
        if (isMoblieNet && !MCForumHelper.onNoPicModel(context)) {
            MCResource resource = MCResource.getInstance(context);
            AlertDialog.Builder exitAlertDialog = new AlertDialog.Builder(context.getApplicationContext()).setTitle(resource.getStringId("mc_forum_dialog_tip")).setMessage(resource.getStringId("mc_forum_warn_net_title"));
            exitAlertDialog.setPositiveButton(resource.getStringId("mc_forum_warn_net_ok"), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferencesDB.getInstance(context).setPicModeFlag(false);
                }
            });
            exitAlertDialog.setNegativeButton(resource.getStringId("mc_forum_warn_net_cancel"), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).create();
            exitAlertDialog.setCancelable(false);
            exitAlertDialog.show();
        }
    }

    private void setReplyNotification(Context context, int replyNoticeNum) {
        if (this.showReplyNotification && replyNoticeNum > 0) {
            String replyTitle = MCPhoneUtil.getAppName(context);
            PendingIntent contentIntent = PendingIntent.getActivity(context, 2, MCForumHelper.onBeatClickListener(context, 0), 134217728);
            String title = MCStringBundleUtil.resolveString(MCResource.getInstance(context).getStringId("mc_forum_heart_beat_reply_content"), replyNoticeNum + "", context);
            this.replyNotification.flags = 16;
            this.replyNotification.icon = this.icon;
            this.replyNotification.tickerText = title;
            this.replyNotification.when = System.currentTimeMillis();
            this.replyNotification.setLatestEventInfo(context, replyTitle, title, contentIntent);
            if (this.postsService.getMessageVoiceFlag()) {
                this.replyNotification.defaults = 1;
            }
            this.myNotificationManager.notify(2, this.replyNotification);
        }
    }

    private void setRelationalNotification(Context context, int relationalNoticeNum) {
        if (this.showRelationalNotification && relationalNoticeNum > 0) {
            String atTitle = MCPhoneUtil.getAppName(context);
            PendingIntent contentIntent = PendingIntent.getActivity(context, 3, MCForumHelper.onBeatClickListener(context, 1), 134217728);
            String title = MCStringBundleUtil.resolveString(MCResource.getInstance(context).getStringId("mc_forum_heart_beat_at_content"), relationalNoticeNum + "", context);
            this.atNotification.flags = 16;
            this.atNotification.icon = this.icon;
            this.atNotification.tickerText = title;
            this.atNotification.when = System.currentTimeMillis();
            if (this.postsService.getMessageVoiceFlag()) {
                this.atNotification.defaults = 1;
            }
            this.atNotification.setLatestEventInfo(context, atTitle, title, contentIntent);
            this.myNotificationManager.notify(3, this.atNotification);
        }
    }

    private int getMsgContentIntent(List<HeartMsgModel> heartBeatList) {
        if (heartBeatList.size() == 0) {
            return 1;
        }
        HeartMsgModel tHeartMsgModel = heartBeatList.get(0);
        for (HeartMsgModel heartMsgModel : heartBeatList) {
            if (heartMsgModel.getFormUserId() != tHeartMsgModel.getFormUserId()) {
                return 3;
            }
        }
        return 2;
    }

    public void setMsgNotification(Context context, int msgTotalNum) {
        List<HeartMsgModel> heartMsgList = this.heartMsgService.getHeartBeatListLocally();
        MCLogUtil.i("HeartBeatReceiver", "update heart beat:" + msgTotalNum);
        if (msgTotalNum == 0) {
            if (heartMsgList.size() <= 0) {
                this.myNotificationManager.cancel(1);
                return;
            } else {
                msgTotalNum = heartMsgList.size();
                MCLogUtil.i("HeartBeatReceiver", "update heart beat:" + msgTotalNum);
            }
        }
        String title = MCStringBundleUtil.resolveString(MCResource.getInstance(context).getStringId("mc_forum_heart_beat_msg_content"), msgTotalNum + "", context);
        String msgTitle = MCPhoneUtil.getAppName(context);
        int msgContentIntent = getMsgContentIntent(heartMsgList);
        Intent intent = null;
        if (msgContentIntent == 2) {
            MCLogUtil.i("HeartBeatReceiver", "SOME_MESSAGE_ONE_USER" + heartMsgList.get(0).getFormUserId() + ":");
            intent = MCForumHelper.onBeatClickListener(context, 2);
        } else if (msgContentIntent == 3) {
            MCLogUtil.i("HeartBeatReceiver", "SOME_MSG_SOME_USER");
            intent = MCForumHelper.onBeatClickListener(context, 2);
        }
        PendingIntent contentIntent = null;
        if (intent != null) {
            contentIntent = PendingIntent.getActivity(context, 1, intent, 134217728);
        }
        if (contentIntent != null) {
            this.msgNotification.flags = 16;
            this.msgNotification.icon = this.icon;
            this.msgNotification.tickerText = title;
            this.msgNotification.when = System.currentTimeMillis();
            if (this.postsService.getMessageVoiceFlag()) {
                this.msgNotification.defaults = 1;
            }
            this.msgNotification.setLatestEventInfo(context, msgTitle, title, contentIntent);
            this.myNotificationManager.notify(1, this.msgNotification);
        }
    }

    public boolean isShowReplyNotification() {
        return this.showReplyNotification;
    }

    public void setShowReplyNotification(boolean showReplyNotification2) {
        this.showReplyNotification = showReplyNotification2;
    }

    public boolean isShowRelationalNotification() {
        return this.showRelationalNotification;
    }

    public void setShowRelationalNotification(boolean showRelationalNotification2) {
        this.showRelationalNotification = showRelationalNotification2;
    }

    public void clearReplyNotification() {
        this.myNotificationManager.cancel(2);
    }

    public void clearMsgNotification() {
        this.myNotificationManager.cancel(1);
    }

    public void clearRelationNotification() {
        this.myNotificationManager.cancel(3);
    }
}
