package com.mobcent.base.android.ui.activity.view;

import android.content.Context;
import android.util.AttributeSet;

public class MCMsgChatAudioView extends MCBaseMsgAudioView {
    public MCMsgChatAudioView(Context context) {
        super(context);
    }

    public MCMsgChatAudioView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void initData() {
        this.layoutName = "mc_forum_msg_chat_audio_view";
        this.voiceImgName = "mc_forum_voice_chat";
    }
}
