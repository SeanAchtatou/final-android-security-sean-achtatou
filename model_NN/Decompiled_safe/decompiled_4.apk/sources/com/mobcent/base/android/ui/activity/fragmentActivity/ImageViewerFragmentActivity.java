package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.ImageViewerFragmentAdapter;
import com.mobcent.base.android.ui.activity.fragment.ImageViewerFragment;
import com.mobcent.base.android.ui.activity.helper.MCForumLaunchShareHelper;
import com.mobcent.base.android.ui.activity.helper.MCImageViewerHelper;
import com.mobcent.base.android.ui.widget.pointimageview.ImageViewPager;
import com.mobcent.base.android.ui.widget.pointimageview.PointImageView;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.RichImageModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.ImageLoaderUtil;
import com.mobcent.forum.android.util.MCLibIOUtil;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.PhoneUtil;
import com.mobcent.forum.android.util.StringUtil;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.platformtools.Util;
import java.util.ArrayList;
import java.util.List;

public class ImageViewerFragmentActivity extends BaseFragmentActivity implements ImageViewerFragment.ImageViewerFragmentSelectedListener {
    /* access modifiers changed from: private */
    public RichImageModel currentImageModel = null;
    /* access modifiers changed from: private */
    public String currentImageUrl = null;
    protected int currentPosition = 0;
    private int imageIndex;
    private int imageSourceType;
    /* access modifiers changed from: private */
    public List<RichImageModel> imageUrlList;
    private ImageButton imageViewerBack;
    private ImageButton imageViewerDownload;
    /* access modifiers changed from: private */
    public ImageViewerFragmentAdapter imageViewerFragmentAdapter;
    private RelativeLayout imageViewerTopbar;
    private ImageButton imgShare;
    /* access modifiers changed from: private */
    public boolean isScrolling = false;
    /* access modifiers changed from: private */
    public int lastValue = -1;
    /* access modifiers changed from: private */
    public boolean left = false;
    private ImageViewPager mPager;
    /* access modifiers changed from: private */
    public boolean right = false;
    /* access modifiers changed from: private */
    public int screenHeight;
    /* access modifiers changed from: private */
    public int screenWidth;
    private MCImageViewerHelper.ImageViewerSizeListener viewSizeListener = new MCImageViewerHelper.ImageViewerSizeListener() {
        public void onViewerSizeListener(List<RichImageModel> richImageModels) {
            ImageViewerFragmentActivity.this.imageUrlList.clear();
            ImageViewerFragmentActivity.this.imageUrlList.addAll(richImageModels);
            ImageViewerFragmentActivity.this.imageViewerFragmentAdapter.setImageUrlList(ImageViewerFragmentActivity.this.imageUrlList);
            ImageViewerFragmentActivity.this.imageViewerFragmentAdapter.notifyDataSetChanged();
        }
    };

    /* access modifiers changed from: protected */
    public void initData() {
        overridePendingTransition(this.resource.getAnimId("mc_forum_fade_in"), this.resource.getAnimId("mc_forum_fade_out"));
        this.screenWidth = PhoneUtil.getDisplayWidth((Activity) this);
        this.screenHeight = PhoneUtil.getDisplayHeight((Activity) this);
        this.imageUrlList = (ArrayList) getIntent().getSerializableExtra(MCConstant.RICH_IMAGE_LIST);
        this.imageSourceType = getIntent().getIntExtra(MCConstant.IMAGE_SOURCE_TYPE, 0);
        this.imageIndex = getPosition(getIntent().getStringExtra(MCConstant.IMAGE_URL));
        this.currentPosition = this.imageIndex;
        MCImageViewerHelper.getInstance().setOnViewSizeListener(this.viewSizeListener);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_imageviewer_fragment_pager"));
        this.imageViewerFragmentAdapter = new ImageViewerFragmentAdapter(getSupportFragmentManager());
        this.imageViewerFragmentAdapter.setImageUrlList(this.imageUrlList);
        this.imageViewerFragmentAdapter.setImageSourceType(this.imageSourceType);
        this.mPager = (ImageViewPager) findViewById(this.resource.getViewId("mc_forum_imageviewer_pager"));
        this.mPager.setAdapter(this.imageViewerFragmentAdapter);
        this.mPager.setCurrentItem(this.imageIndex);
        this.mPager.setOnPageChangeListener(new ViewPageChangeListener());
        this.mPager.setPageMargin((int) getResources().getDimension(this.resource.getDimenId("mc_forum_image_detail_pager_margin")));
        this.imageViewerTopbar = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_imageviewer_bar"));
        this.imageViewerBack = (ImageButton) findViewById(this.resource.getViewId("mc_forum_img_back"));
        this.imageViewerDownload = (ImageButton) findViewById(this.resource.getViewId("mc_forum_img_download"));
        this.imgShare = (ImageButton) findViewById(this.resource.getViewId("mc_forum_img_share"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.imageViewerBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImageViewerFragmentActivity.this.finish();
            }
        });
        this.imageViewerDownload.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ImageViewerFragmentActivity.this.currentImageUrl != null && !"".equals(ImageViewerFragmentActivity.this.currentImageUrl)) {
                    new AsyncTaskDownloadImage().execute(new Void[0]);
                }
            }
        });
        this.imgShare.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                IWXAPI api = WXAPIFactory.createWXAPI(ImageViewerFragmentActivity.this, SharedPreferencesDB.getInstance(ImageViewerFragmentActivity.this).getWeiXinAppKey());
                if (StringUtil.isEmpty(SharedPreferencesDB.getInstance(ImageViewerFragmentActivity.this).getWeiXinAppKey())) {
                    if (!StringUtil.isEmpty(ImageViewerFragmentActivity.this.currentImageUrl)) {
                        MCForumLaunchShareHelper.shareContentWithImageFileAndUrl(ImageViewerFragmentActivity.this.currentImageModel.getImageDesc(), MCLibIOUtil.getImageCachePath(ImageViewerFragmentActivity.this.getApplicationContext()) + AsyncTaskLoaderImage.getHash(ImageViewerFragmentActivity.this.currentImageUrl), ImageViewerFragmentActivity.this.currentImageUrl, "", "", ImageViewerFragmentActivity.this);
                    }
                } else if (!StringUtil.isEmpty(ImageViewerFragmentActivity.this.currentImageUrl)) {
                    MCForumLaunchShareHelper.shareWay(true, ImageViewerFragmentActivity.this.currentImageModel.getImageDesc(), MCLibIOUtil.getImageCachePath(ImageViewerFragmentActivity.this.getApplicationContext()) + AsyncTaskLoaderImage.getHash(ImageViewerFragmentActivity.this.currentImageUrl), ImageViewerFragmentActivity.this.currentImageUrl, "", "", ImageViewerFragmentActivity.this, api);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        MCImageViewerHelper.getInstance().setListener(null);
    }

    private class AsyncTaskDownloadImage extends AsyncTask<Void, Void, Boolean> {
        private AsyncTaskDownloadImage() {
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Void... params) {
            boolean isSaveSucc;
            String fileName = ImageLoaderUtil.getPathName(ImageViewerFragmentActivity.this.currentImageUrl);
            MCLogUtil.i("ImageViewerFragmentActivity", "fileName = " + fileName);
            String imagePath = MCLibIOUtil.getImageCachePath(ImageViewerFragmentActivity.this.getApplicationContext()) + AsyncTaskLoaderImage.getHash(ImageViewerFragmentActivity.this.currentImageUrl);
            if (MCConstant.PIC_GIF.equals(StringUtil.getExtensionName(fileName).toLowerCase())) {
                isSaveSucc = MCLibIOUtil.addToSysGallery(ImageViewerFragmentActivity.this, imagePath, fileName + ".gif");
            } else {
                isSaveSucc = MCLibIOUtil.addToSysGallery(ImageViewerFragmentActivity.this, imagePath, fileName + Util.PHOTO_DEFAULT_EXT);
            }
            return Boolean.valueOf(isSaveSucc);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean isSaveSucc) {
            if (isSaveSucc.booleanValue()) {
                Toast.makeText(ImageViewerFragmentActivity.this.getApplicationContext(), ImageViewerFragmentActivity.this.resource.getStringId("mc_forum_add_to_sys_gallery_succ"), 0).show();
            } else {
                Toast.makeText(ImageViewerFragmentActivity.this.getApplicationContext(), ImageViewerFragmentActivity.this.resource.getStringId("mc_forum_add_to_sys_gallery_fail"), 0).show();
            }
        }
    }

    public ViewPager getViewPager() {
        return this.mPager;
    }

    public void onSingleTap(MotionEvent e) {
        if (this.imageViewerTopbar.isShown()) {
            this.imageViewerTopbar.setVisibility(8);
        } else {
            this.imageViewerTopbar.setVisibility(0);
        }
    }

    private class ViewPageChangeListener implements ViewPager.OnPageChangeListener {
        private ViewPageChangeListener() {
        }

        public void onPageScrollStateChanged(int status) {
            if (status == 1) {
                boolean unused = ImageViewerFragmentActivity.this.isScrolling = true;
            } else {
                boolean unused2 = ImageViewerFragmentActivity.this.isScrolling = false;
            }
        }

        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            if (ImageViewerFragmentActivity.this.isScrolling) {
                if (ImageViewerFragmentActivity.this.lastValue > positionOffsetPixels) {
                    boolean unused = ImageViewerFragmentActivity.this.right = true;
                    boolean unused2 = ImageViewerFragmentActivity.this.left = false;
                } else if (ImageViewerFragmentActivity.this.lastValue < positionOffsetPixels) {
                    boolean unused3 = ImageViewerFragmentActivity.this.right = false;
                    boolean unused4 = ImageViewerFragmentActivity.this.left = true;
                } else if (ImageViewerFragmentActivity.this.lastValue == positionOffsetPixels) {
                    boolean unused5 = ImageViewerFragmentActivity.this.right = ImageViewerFragmentActivity.this.left = false;
                }
            }
            int unused6 = ImageViewerFragmentActivity.this.lastValue = positionOffsetPixels;
        }

        public void onPageSelected(int position) {
            ImageViewerFragmentActivity.this.currentPosition = position;
            ImageViewerFragment imageViewerFragment = null;
            if (ImageViewerFragmentActivity.this.left && position - 1 >= 0) {
                imageViewerFragment = (ImageViewerFragment) ImageViewerFragmentActivity.this.imageViewerFragmentAdapter.getItem(position - 1);
            } else if (ImageViewerFragmentActivity.this.right && position + 1 < ImageViewerFragmentActivity.this.imageViewerFragmentAdapter.getCount()) {
                imageViewerFragment = (ImageViewerFragment) ImageViewerFragmentActivity.this.imageViewerFragmentAdapter.getItem(position + 1);
            }
            if (!(imageViewerFragment == null || imageViewerFragment.getImageView() == null || !(imageViewerFragment.getImageView() instanceof PointImageView) || imageViewerFragment == null || imageViewerFragment.getImageView() == null)) {
                ((PointImageView) imageViewerFragment.getImageView()).zoomTo(((PointImageView) imageViewerFragment.getImageView()).getScaleRate(), (float) (ImageViewerFragmentActivity.this.screenWidth / 2), (float) (ImageViewerFragmentActivity.this.screenHeight / 2), 200.0f);
            }
            ImageViewerFragment currentImageViewerFragment = (ImageViewerFragment) ImageViewerFragmentActivity.this.imageViewerFragmentAdapter.getItem(ImageViewerFragmentActivity.this.currentPosition);
            if (currentImageViewerFragment != null) {
                String unused = ImageViewerFragmentActivity.this.currentImageUrl = currentImageViewerFragment.getImageUrl();
                RichImageModel unused2 = ImageViewerFragmentActivity.this.currentImageModel = currentImageViewerFragment.getImageModel();
            }
            if (MCImageViewerHelper.getInstance().getListener() != null) {
                MCImageViewerHelper.getInstance().getListener().onViewerPageSelect(ImageViewerFragmentActivity.this.currentPosition);
            }
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(this.resource.getAnimId("mc_forum_fade_in"), this.resource.getAnimId("mc_forum_fade_out"));
    }

    public void onSelected() {
        if (this.currentPosition == this.imageIndex) {
            ImageViewerFragment imageViewerFragment = (ImageViewerFragment) this.imageViewerFragmentAdapter.getItem(this.currentPosition);
            this.currentImageUrl = imageViewerFragment.getImageUrl();
            this.currentImageModel = imageViewerFragment.getImageModel();
        }
    }

    private int getPosition(String imageUrl) {
        for (int i = 0; i < this.imageUrlList.size(); i++) {
            if (this.imageUrlList.get(i).getImageUrl().equals(imageUrl)) {
                return i;
            }
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return null;
    }
}
