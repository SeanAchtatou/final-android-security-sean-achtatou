package com.mobcent.base.android.ui.activity.delegate;

public interface LongTaskDelegate {
    void executeFail();

    void executeSuccess(String str);
}
