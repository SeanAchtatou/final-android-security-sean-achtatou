package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.content.Intent;
import android.view.View;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.PointImageViewActivity;
import com.mobcent.base.android.ui.activity.ReportActivity;
import com.mobcent.base.android.ui.activity.fragment.UserHomeFragment;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserHomeFragmentActivity extends BaseFragmentActivity implements MCConstant {
    protected static Set<UserHomeFragmentActivity> userHomeSets = null;
    private UserHomeFragment.UserTopicClickListener clickListener = new UserHomeFragment.UserTopicClickListener() {
        public void onUserTopicClick(View v, UserInfoModel userInfoModel, long userId) {
            MCForumHelper.onTopicClick(UserHomeFragmentActivity.this, UserHomeFragmentActivity.this.resource, v, 1, userId, userInfoModel);
        }

        public void onUserReplyClick(View v, UserInfoModel userInfoModel, long userId) {
            MCForumHelper.onTopicClick(UserHomeFragmentActivity.this, UserHomeFragmentActivity.this.resource, v, 0, userId, userInfoModel);
        }

        public void onUserFollowClick(View arg0, UserInfoModel userInfoModel) {
            Intent intent = new Intent(UserHomeFragmentActivity.this, FanOrFollowActivity.class);
            intent.putExtra("userId", userInfoModel.getUserId());
            intent.putExtra(MCConstant.FRIEND_TYPE, 2);
            UserHomeFragmentActivity.this.startActivity(intent);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onUserFindFriendsClick(View arg0) {
            Intent intent = new Intent(UserHomeFragmentActivity.this, UserFriendsFragmentActivity.class);
            intent.putExtra(MCConstant.IS_MY_FRIENDS, false);
            UserHomeFragmentActivity.this.startActivity(intent);
        }

        public void onUserFindSurroundClick(View view, UserInfoModel userInfoModel) {
            Intent intent = new Intent(UserHomeFragmentActivity.this, UserTopicFragmentActivity.class);
            intent.putExtra(MCConstant.USER_TOPIC, 3);
            UserHomeFragmentActivity.this.startActivity(intent);
        }

        public void onUserFavoritesClick(View v, UserInfoModel userInfoModel, long userId) {
            MCForumHelper.onTopicClick(UserHomeFragmentActivity.this, UserHomeFragmentActivity.this.resource, v, 2, userId, userInfoModel);
        }

        public void onUserFanClick(View arg0, UserInfoModel userInfoModel) {
            Intent intent = new Intent(UserHomeFragmentActivity.this, FanOrFollowActivity.class);
            intent.putExtra("userId", userInfoModel.getUserId());
            intent.putExtra(MCConstant.FRIEND_TYPE, 1);
            UserHomeFragmentActivity.this.startActivity(intent);
        }

        public void onSettingClick(View arg0) {
            UserHomeFragmentActivity.this.startActivity(new Intent(UserHomeFragmentActivity.this, SettingFragmentActivity.class));
        }

        public void onReportClick(View arg0, UserInfoModel userInfoModel) {
            Intent intent = new Intent(UserHomeFragmentActivity.this, ReportActivity.class);
            intent.putExtra(MCConstant.REPORT_TYPE, 3);
            intent.putExtra("oid", userInfoModel.getUserId());
            UserHomeFragmentActivity.this.startActivity(intent);
        }

        public void onPrivateClick(View arg0, UserInfoModel userInfoModel) {
            Intent intent = new Intent(UserHomeFragmentActivity.this, MsgChatRoomFragmentActivity.class);
            intent.putExtra(MCConstant.CHAT_USER_ID, userInfoModel.getUserId());
            intent.putExtra(MCConstant.CHAT_USER_NICKNAME, userInfoModel.getNickname());
            intent.putExtra(MCConstant.CHAT_USER_ICON, userInfoModel.getIcon());
            intent.putExtra(MCConstant.BLACK_USER_STATUS, userInfoModel.getBlackStatus());
            UserHomeFragmentActivity.this.startActivity(intent);
        }

        public void onMessageClick(View arg0) {
            MCForumHelper.onMessageClick(UserHomeFragmentActivity.this);
        }

        public void onIconClick(View arg0, UserInfoModel userInfoModel) {
            Intent intent = new Intent(UserHomeFragmentActivity.this, PointImageViewActivity.class);
            intent.putExtra(MCConstant.IMAGE_URL, userInfoModel.getIcon());
            UserHomeFragmentActivity.this.startActivity(intent);
        }
    };
    protected UserHomeFragment userHomeFragment;

    /* access modifiers changed from: protected */
    public void initData() {
        this.fragmentManager = getSupportFragmentManager();
        if (userHomeSets == null) {
            userHomeSets = new HashSet();
        }
        userHomeSets.add(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_user_home_fragment_activity"));
        this.userHomeFragment = (UserHomeFragment) this.fragmentManager.findFragmentById(this.resource.getViewId("mc_forum_user_home_fragment"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.userHomeFragment.setUserTopicClickListener(this.clickListener);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.userHomeFragment.setUserId(intent.getLongExtra("userId", 0));
        this.userHomeFragment.refresh();
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return null;
    }

    public void onBackPressed() {
        super.onBackPressed();
        MCForumHelper.onUserHomeBackPressed(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        userHomeSets.remove(this);
    }

    public static Set<UserHomeFragmentActivity> getUserHomeSets() {
        return userHomeSets;
    }

    public long getUserId() {
        return this.userHomeFragment.getUserId();
    }
}
