package com.mobcent.base.android.ui.activity.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.constant.PlatFormResourceConstant;
import com.mobcent.base.android.ui.activity.AboutActivity;
import com.mobcent.base.android.ui.activity.LoginActivity;
import com.mobcent.base.android.ui.activity.adapter.PlatformLoginListAdapter;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import java.io.Serializable;
import java.util.HashMap;

public class UserLoginFragment extends BaseFragment {
    private Button aboutBtn;
    /* access modifiers changed from: private */
    public Activity activity;
    private PlatformLoginListAdapter adapter;
    /* access modifiers changed from: private */
    public HashMap<String, Serializable> goParam;
    /* access modifiers changed from: private */
    public Class<?> goToActivityClass;
    private ListView loginList;
    private Button loginRegBtn;

    /* access modifiers changed from: protected */
    public void initData() {
        this.activity = getActivity();
        Intent intent = this.activity.getIntent();
        if (intent != null) {
            this.goToActivityClass = (Class) intent.getSerializableExtra(MCConstant.TAG);
            this.goParam = (HashMap) intent.getSerializableExtra(MCConstant.GO_PARAM);
        }
        this.adapter = new PlatformLoginListAdapter(this.activity, PlatFormResourceConstant.getMCResourceConstant().getLoginModelList(), this.goToActivityClass, this.goParam);
    }

    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(this.mcResource.getLayoutId("mc_forum_user_login_fragment"), (ViewGroup) null);
        this.aboutBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_about"));
        this.loginList = (ListView) this.view.findViewById(this.mcResource.getViewId("mc_forum_login_list"));
        this.loginRegBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_login_regist_btn"));
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.loginList.setAdapter((ListAdapter) this.adapter);
        if (MCForumHelper.isCopyright(this.activity)) {
            this.view.findViewById(this.mcResource.getViewId("mc_forum_user_mobcent_login_warn_text")).setVisibility(8);
        }
        this.aboutBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserLoginFragment.this.startActivity(new Intent(UserLoginFragment.this.activity, AboutActivity.class));
            }
        });
        this.loginRegBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(UserLoginFragment.this.activity, LoginActivity.class);
                if (UserLoginFragment.this.goToActivityClass != null) {
                    intent.putExtra(MCConstant.TAG, UserLoginFragment.this.goToActivityClass);
                    intent.putExtra(MCConstant.GO_PARAM, UserLoginFragment.this.goParam);
                }
                UserLoginFragment.this.startActivity(intent);
            }
        });
    }
}
