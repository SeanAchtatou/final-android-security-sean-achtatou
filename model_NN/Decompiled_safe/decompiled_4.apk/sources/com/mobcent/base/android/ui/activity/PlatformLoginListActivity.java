package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.constant.PlatFormResourceConstant;
import com.mobcent.base.android.ui.activity.adapter.PlatformLoginListAdapter;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class PlatformLoginListActivity extends BaseActivity {
    private static Set<PlatformLoginListActivity> loginActSets;
    private Button aboutBtn;
    private PlatformLoginListAdapter adapter;
    /* access modifiers changed from: private */
    public HashMap<String, Serializable> goParam;
    /* access modifiers changed from: private */
    public Class<?> goToActivityClass;
    private ListView loginList;
    private Button loginRegBtn;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (loginActSets == null) {
            loginActSets = new HashSet();
        }
        finishAll();
        loginActSets.add(this);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            this.goToActivityClass = (Class) intent.getSerializableExtra(MCConstant.TAG);
            this.goParam = (HashMap) intent.getSerializableExtra(MCConstant.GO_PARAM);
        }
        this.adapter = new PlatformLoginListAdapter(this, PlatFormResourceConstant.getMCResourceConstant().getLoginModelList(), this.goToActivityClass, this.goParam);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_user_login_platform_activity"));
        this.aboutBtn = (Button) findViewById(this.resource.getViewId("mc_forum_about"));
        this.loginList = (ListView) findViewById(this.resource.getViewId("mc_forum_login_list"));
        this.loginRegBtn = (Button) findViewById(this.resource.getViewId("mc_forum_login_regist_btn"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.loginList.setAdapter((ListAdapter) this.adapter);
        this.aboutBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlatformLoginListActivity.this.startActivity(new Intent(PlatformLoginListActivity.this, AboutActivity.class));
            }
        });
        this.loginRegBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(PlatformLoginListActivity.this, LoginActivity.class);
                if (PlatformLoginListActivity.this.goToActivityClass != null) {
                    intent.putExtra(MCConstant.TAG, PlatformLoginListActivity.this.goToActivityClass);
                    intent.putExtra(MCConstant.GO_PARAM, PlatformLoginListActivity.this.goParam);
                }
                PlatformLoginListActivity.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        loginActSets.remove(this);
    }

    public static void finishAll() {
        for (PlatformLoginListActivity act : loginActSets) {
            act.finish();
        }
    }
}
