package com.mobcent.base.android.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.BannedShieldedAsyncTask;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.model.ReplyModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserBoardInfoModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.UserManageService;
import com.mobcent.forum.android.service.impl.UserManageServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.Date;
import java.util.List;

public class UserBannedActivity extends BaseBannedShieldedDetailsActivity implements MCConstant {
    /* access modifiers changed from: private */
    public Date bannedDate;
    /* access modifiers changed from: private */
    public String bannedReasonStr;
    /* access modifiers changed from: private */
    public BannedShieldedAsyncTask bannedShieldedAsyncTask;
    /* access modifiers changed from: private */
    public String bannedTimeStr;
    /* access modifiers changed from: private */
    public long bannedUserId;
    /* access modifiers changed from: private */
    public String reason;
    /* access modifiers changed from: private */
    public ReplyModel replyModel;
    /* access modifiers changed from: private */
    public TopicModel topicModel;
    /* access modifiers changed from: private */
    public int type;
    /* access modifiers changed from: private */
    public UserManageService userManageService;

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.userManageService = new UserManageServiceImpl(this);
        Intent intent = getIntent();
        this.type = intent.getIntExtra(MCConstant.BANNED_TYPE, 0);
        this.currentUserId = intent.getLongExtra("userId", this.userService.getLoginUserId());
        this.bannedUserId = intent.getLongExtra(MCConstant.BANNED_USER_ID, 0);
        this.boardId = (int) intent.getLongExtra("boardId", 0);
        this.sureStr = getString(this.resource.getStringId("mc_forum_sure_banned_user_str"));
        this.topicModel = (TopicModel) intent.getSerializableExtra(MCConstant.TOPICMODEL);
        this.replyModel = (ReplyModel) intent.getSerializableExtra(MCConstant.REPLYMODEL);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        if (this.type == 4) {
            this.titleText.setText(getString(this.resource.getStringId("mc_forum_banned_str")));
            this.reasonEdit.setHint(getString(this.resource.getStringId("mc_forum_select_banned_reason_str")));
            this.selectBoardText.setText(getString(this.resource.getStringId("mc_forum_select_banned_board_str")));
            this.boardTitleText.setText(getString(this.resource.getStringId("mc_forum_banned_board_str")));
            this.timeBtn.setText(getString(this.resource.getStringId("mc_forum_banned_time")));
            this.detailDescriptionEdit.setHint(getString(this.resource.getStringId("mc_forum_banned_tip_detail_content_str")));
        }
        if (this.boardId == -1) {
            this.boardBox.setVisibility(0);
            this.boardTitleText.setVisibility(0);
            this.scrollView.setVisibility(0);
            this.boardDetailText.setVisibility(0);
            return;
        }
        this.boardBox.setVisibility(8);
        this.boardTitleText.setVisibility(8);
        this.scrollView.setVisibility(8);
        this.boardDetailText.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.submitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String unused = UserBannedActivity.this.bannedTimeStr = UserBannedActivity.this.timeText.getText().toString().trim();
                String unused2 = UserBannedActivity.this.bannedReasonStr = UserBannedActivity.this.reasonEdit.getText().toString().trim();
                String unused3 = UserBannedActivity.this.reason = "[" + UserBannedActivity.this.bannedReasonStr + "]" + UserBannedActivity.this.detailDescriptionEdit.getText().toString().trim();
                Date unused4 = UserBannedActivity.this.bannedDate = UserBannedActivity.this.convertDate(UserBannedActivity.this.bannedTimeStr);
                if (UserBannedActivity.this.selectBoardList.size() <= 0 && UserBannedActivity.this.boardId == -1) {
                    UserBannedActivity.this.post();
                } else if (StringUtil.isEmpty(UserBannedActivity.this.bannedReasonStr)) {
                    Toast.makeText(UserBannedActivity.this, UserBannedActivity.this.getString(UserBannedActivity.this.resource.getStringId("mc_forum_select_banned_reason_str")), 1).show();
                } else if (UserBannedActivity.this.bannedDate == null) {
                    Toast.makeText(UserBannedActivity.this, UserBannedActivity.this.getString(UserBannedActivity.this.resource.getStringId("mc_forum_select_banned_time_str")), 1).show();
                } else {
                    UserBannedActivity.this.post();
                }
            }
        });
    }

    public UserInfoModel getUserInfoModel() {
        return this.userService.getUserBoardInfoList(this.bannedUserId);
    }

    public List<BoardModel> getBoardInfo(UserInfoModel userInfoModel, List<BoardModel> boardList) {
        if (!(userInfoModel == null || boardList == null || boardList.size() <= 0)) {
            int count = 0;
            List<UserBoardInfoModel> userBoardInfoList = userInfoModel.getUserBoardInfoList();
            if (userBoardInfoList != null && userBoardInfoList.size() > 0) {
                for (int i = 0; i < userBoardInfoList.size(); i++) {
                    if (userBoardInfoList.get(i).getIsBanned() == 1) {
                        for (int j = 0; j < boardList.size(); j++) {
                            if (boardList.get(j).getBoardId() == userBoardInfoList.get(i).getBoardId()) {
                                boardList.get(j).setSelected(true);
                                count++;
                            }
                        }
                    }
                }
                if (count == userBoardInfoList.size()) {
                    boardList.get(0).setSelected(true);
                }
            }
        }
        return boardList;
    }

    /* access modifiers changed from: private */
    public void post() {
        new AlertDialog.Builder(this).setTitle(this.sureStr).setNegativeButton(this.resource.getString("mc_forum_dialog_cancel"), (DialogInterface.OnClickListener) null).setPositiveButton(this.resource.getString("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (UserBannedActivity.this.boardId == -1) {
                    BannedShieldedAsyncTask unused = UserBannedActivity.this.bannedShieldedAsyncTask = new BannedShieldedAsyncTask(UserBannedActivity.this, UserBannedActivity.this.resource, UserBannedActivity.this.userManageService, UserBannedActivity.this.type, UserBannedActivity.this.currentUserId, UserBannedActivity.this.bannedUserId, UserBannedActivity.this.boardId, UserBannedActivity.this.selectBoardList, UserBannedActivity.this.bannedDate, UserBannedActivity.this.reason);
                } else {
                    BannedShieldedAsyncTask unused2 = UserBannedActivity.this.bannedShieldedAsyncTask = new BannedShieldedAsyncTask(UserBannedActivity.this, UserBannedActivity.this.resource, UserBannedActivity.this.userManageService, UserBannedActivity.this.type, UserBannedActivity.this.currentUserId, UserBannedActivity.this.bannedUserId, UserBannedActivity.this.boardId, UserBannedActivity.this.bannedDate, UserBannedActivity.this.reason);
                }
                UserBannedActivity.this.bannedShieldedAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                    public void executeSuccess() {
                        if (UserBannedActivity.this.topicModel != null) {
                            UserBannedActivity.this.topicModel.setBannedUser(true);
                        } else if (UserBannedActivity.this.replyModel != null) {
                            UserBannedActivity.this.replyModel.setBannedUser(true);
                        }
                    }

                    public void executeFail() {
                        if (UserBannedActivity.this.topicModel != null) {
                            UserBannedActivity.this.topicModel.setBannedUser(false);
                        } else if (UserBannedActivity.this.replyModel != null) {
                            UserBannedActivity.this.replyModel.setBannedUser(false);
                        }
                    }
                });
                UserBannedActivity.this.bannedShieldedAsyncTask.execute(new Object[0]);
                UserBannedActivity.this.finish();
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.bannedShieldedAsyncTask != null) {
            this.bannedShieldedAsyncTask.cancel(true);
        }
    }
}
