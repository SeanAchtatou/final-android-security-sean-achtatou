package com.mobcent.base.android.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.BasePublishTopicActivityWithAudio;
import com.mobcent.base.android.ui.activity.PublishTopicActivity;
import com.mobcent.base.android.ui.activity.adapter.holder.PostsAudioAdapterHolder;
import com.mobcent.base.forum.android.util.MCFaceUtil;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.PollItemModel;
import com.mobcent.forum.android.model.SoundModel;
import com.mobcent.forum.android.model.TopicContentModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.DateUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class RepostTopicActivity extends PublishTopicActivity {
    private String boardName;
    private TextView boardNameText;
    private LayoutInflater inflater;
    private TextView nameText;
    private LinearLayout postsRepostBox;
    private LinearLayout postsRepostContentLayout;
    private TextView postsTimeText;
    private TopicModel topicModel;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        this.draftId = 0;
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        Intent intent = getIntent();
        this.boardName = intent.getStringExtra("boardName");
        if (this.boardName != null && this.boardName.toString().trim().equals(this.resource.getString("mc_forum_posts_detail").toString().trim())) {
            this.boardName = null;
        }
        this.boardId = Long.valueOf(intent.getLongExtra(MCConstant.REPOST_BOARD_ID, 0)).longValue();
        this.topicModel = (TopicModel) intent.getSerializableExtra(MCConstant.TOPICMODEL);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        this.inflater = getLayoutInflater();
        this.titleLabelText.setText(this.resource.getStringId("mc_forum_posts_repost"));
        this.postsRepostBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_posts_repost_box"));
        this.postsRepostContentLayout = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_posts_repost_content_layout"));
        this.postsRepostBox.setVisibility(0);
        this.contentTransparentLayout.setVisibility(8);
        this.postsTimeText = (TextView) findViewById(this.resource.getViewId("mc_forum_posts_repost_time"));
        this.boardNameText = (TextView) findViewById(this.resource.getViewId("mc_forum_posts_repost_board_name"));
        this.nameText = (TextView) findViewById(this.resource.getViewId("mc_forum_posts_repost_name"));
        this.nameText.setText(getResources().getString(this.resource.getStringId("mc_forum_posts_repost")) + "//@" + this.topicModel.getUserNickName() + ":");
        String pollContent = "";
        if (this.topicModel.getTopicContentList().get(0).getPollTopicModel() != null) {
            List<PollItemModel> pollList = this.topicModel.getTopicContentList().get(0).getPollTopicModel().getPooList();
            for (int i = 0; i < pollList.size(); i++) {
                pollContent = pollContent + (i + 1) + "." + pollList.get(i).getPollName() + "\n";
            }
            TopicContentModel pollContentModel = new TopicContentModel();
            pollContentModel.setType(0);
            pollContentModel.setInfor(pollContent);
            this.topicModel.getTopicContentList().add(pollContentModel);
        }
        updatePostsDetailView(this.topicModel.getTopicContentList(), this.postsRepostContentLayout);
        if (StringUtil.isEmpty(this.boardName) || this.boardId <= 0 || this.boardName.trim().equals(getResources().getString(this.resource.getStringId("mc_forum_posts_detail")))) {
            this.boardNameText.setVisibility(8);
        } else {
            this.boardNameText.setText(this.boardName);
        }
        this.postsTimeText.setText(DateUtil.getFormatTimeByYear(this.topicModel.getCreateDate()));
        this.titleEdit.setText(getResources().getString(this.resource.getStringId("mc_forum_posts_repost_mark")) + this.topicModel.getTitle());
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.publishBtn.setEnabled(true);
        this.publishBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!RepostTopicActivity.this.checkTitle()) {
                    return;
                }
                if (RepostTopicActivity.this.boardId <= 0) {
                    Toast.makeText(RepostTopicActivity.this, RepostTopicActivity.this.getResources().getString(RepostTopicActivity.this.resource.getStringId("mc_forum_publish_select_board")), 1).show();
                } else if (RepostTopicActivity.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.POST, -1) == 0 || RepostTopicActivity.this.permService.getPermNum(PermConstant.BOARDS, PermConstant.POST, RepostTopicActivity.this.boardId) == 0) {
                    RepostTopicActivity.this.warnMessageByStr(RepostTopicActivity.this.resource.getString("mc_forum_permission_cannot_post_toipc"));
                } else if (RepostTopicActivity.this.hasAudio) {
                    if (RepostTopicActivity.this.uploadAudioFileAsyncTask != null) {
                        RepostTopicActivity.this.uploadAudioFileAsyncTask.cancel(true);
                    }
                    RepostTopicActivity.this.uploadAudioFileAsyncTask = new BasePublishTopicActivityWithAudio.UploadAudioFileAsyncTask();
                    RepostTopicActivity.this.uploadAudioFileAsyncTask.execute(new Void[0]);
                } else {
                    RepostTopicActivity.this.uploadAudioSucc();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void uploadAudioSucc() {
        PostsService postsService = new PostsServiceImpl(this);
        UserInfoModel userModel = new UserInfoModel();
        userModel.setUserId(this.topicModel.getUserId());
        userModel.setNickname(this.topicModel.getUserNickName());
        this.mentionedFriends.add(userModel);
        String content = postsService.createRepostTopicJson(this.topicModel, this.boardName, System.currentTimeMillis(), this.boardId, getContentEditText().getText().toString(), "ß", "á", this.mentionedFriends, this.audioPath, this.audioDuration);
        if (!postsService.isContainsPic(getContentEditText().getText().toString(), "ß", "á", this.audioPath, this.audioDuration)) {
            new PublishTopicActivity.PublishAsyncTask().execute(this.boardId + "", this.title, content, this.selectVisibleId + "");
        } else if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.UPLOAD, -1) == 1 && this.permService.getPermNum(PermConstant.BOARDS, PermConstant.UPLOAD, this.boardId) == 1) {
            new PublishTopicActivity.PublishAsyncTask().execute(this.boardId + "", this.title, content, this.selectVisibleId + "");
        } else {
            warnMessageByStr(this.resource.getString("mc_forum_permission_cannot_upload_pic"));
        }
    }

    /* access modifiers changed from: protected */
    public void initExitAlertDialog() {
        this.exitAlertDialog = new AlertDialog.Builder(this).setTitle(this.resource.getStringId("mc_forum_dialog_tip")).setMessage(this.resource.getStringId("mc_forum_warn_photo_title"));
        this.exitAlertDialog.setPositiveButton(this.resource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                RepostTopicActivity.this.exitSaveEvent();
            }
        });
        this.exitAlertDialog.setNegativeButton(this.resource.getStringId("mc_forum_dialog_cancel"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
    }

    private LinearLayout updatePostsDetailView(List<TopicContentModel> topicContentList, LinearLayout topicContentLayout) {
        if (topicContentList != null && topicContentList.size() > 0) {
            topicContentLayout.setVisibility(0);
            topicContentLayout.removeAllViews();
            int j = topicContentList.size();
            for (int i = 0; i < j; i++) {
                TopicContentModel topicContent = topicContentList.get(i);
                View view = null;
                if (topicContent.getType() == 0) {
                    view = this.inflater.inflate(this.resource.getLayoutId("mc_forum_reposts_topic_text_item"), (ViewGroup) null);
                    TextView topicInfoText = (TextView) view.findViewById(this.resource.getViewId("mc_forum_topic_info_text"));
                    topicInfoText.setText(topicContent.getInfor());
                    MCFaceUtil.setStrToFace(topicInfoText, topicContent.getInfor(), this);
                } else if (topicContent.getType() == 1) {
                    view = this.inflater.inflate(this.resource.getLayoutId("mc_forum_posts_topic_img_item"), (ViewGroup) null);
                    ImageView topicInfoImg = (ImageView) view.findViewById(this.resource.getViewId("mc_forum_topic_info_img"));
                    String imageUrl = topicContent.getBaseUrl() + topicContent.getInfor();
                    if (!StringUtil.isEmpty(topicContent.getInfor())) {
                        updateTopicContentImage(imageUrl, topicInfoImg);
                    } else {
                        topicInfoImg.setImageDrawable(null);
                    }
                } else if (topicContent.getType() == 5) {
                    view = getSoundView(topicContent.getSoundModel());
                }
                if (view != null) {
                    topicContentLayout.addView(view);
                }
            }
            topicContentLayout.setVisibility(0);
        }
        return topicContentLayout;
    }

    private void updateTopicContentImage(String imgUrl, final ImageView imageView) {
        if (SharedPreferencesDB.getInstance(this).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(AsyncTaskLoaderImage.formatUrl(imgUrl, "320x480"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    RepostTopicActivity.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (image != null) {
                                imageView.setImageBitmap(image);
                            }
                        }
                    });
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public View getSoundView(SoundModel soundModel) {
        View view = this.inflater.inflate(this.resource.getLayoutId("mc_forum_posts_topic_audio_item"), (ViewGroup) null);
        PostsAudioAdapterHolder audioAdapterHolder = new PostsAudioAdapterHolder();
        audioAdapterHolder.setTimeText((TextView) view.findViewById(this.resource.getViewId("mc_forum_topic_time_text")));
        audioAdapterHolder.getTimeText().setText(soundModel.getSoundTime() + "\"");
        return view;
    }

    private List<String> getTopicContentListImg(List<TopicContentModel> topicContentList) {
        List<String> imgUrl = new ArrayList<>();
        if (topicContentList != null && !topicContentList.isEmpty()) {
            int n = topicContentList.size();
            for (int k = 0; k < n; k++) {
                TopicContentModel topicContent = topicContentList.get(k);
                if (topicContent.getType() == 1 && topicContent.getBaseUrl() != null && !topicContent.getBaseUrl().trim().equals("") && !topicContent.getBaseUrl().trim().equals("null")) {
                    imgUrl.add(AsyncTaskLoaderImage.formatUrl(topicContent.getBaseUrl() + topicContent.getInfor(), "320x480"));
                }
            }
        }
        return imgUrl;
    }

    /* access modifiers changed from: protected */
    public void exitSaveEvent() {
        finish();
    }
}
