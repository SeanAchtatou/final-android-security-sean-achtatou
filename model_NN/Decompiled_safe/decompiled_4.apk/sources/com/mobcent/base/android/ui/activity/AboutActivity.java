package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.ClipboardManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;
import com.mobcent.forum.android.model.AboutModel;
import com.mobcent.forum.android.service.impl.AboutServiceImpl;

public class AboutActivity extends BaseActivity {
    private AboutInfoAsyncTask aboutInfoAsyncTask;
    /* access modifiers changed from: private */
    public AboutModel aboutModel;
    private Button backBtn;
    private TextView descText;
    private TextView emailText;
    private ImageView line1;
    private ImageView line2;
    private ImageView line3;
    private ImageView line4;
    private ImageView line5;
    private ImageView line6;
    /* access modifiers changed from: private */
    public RelativeLayout loadingBox;
    /* access modifiers changed from: private */
    public MCProgressBar progressBar;
    private TextView qqText;
    private TextView telText;
    private TextView websiteText;
    private TextView weiboQQ;
    private TextView weiboSina;

    /* access modifiers changed from: protected */
    public void initData() {
        if (this.aboutInfoAsyncTask != null) {
            this.aboutInfoAsyncTask.cancel(true);
        }
        this.aboutInfoAsyncTask = new AboutInfoAsyncTask();
        this.aboutInfoAsyncTask.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_about_activity"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.descText = (TextView) findViewById(this.resource.getViewId("mc_forum_desc_text"));
        this.emailText = (TextView) findViewById(this.resource.getViewId("mc_forum_about_email_text"));
        this.qqText = (TextView) findViewById(this.resource.getViewId("mc_forum_aboout_qq_text"));
        this.telText = (TextView) findViewById(this.resource.getViewId("mc_forum_about_tel_text"));
        this.websiteText = (TextView) findViewById(this.resource.getViewId("mc_forum_about_web_text"));
        this.weiboSina = (TextView) findViewById(this.resource.getViewId("mc_forum_about_weibo_sina_text"));
        this.weiboQQ = (TextView) findViewById(this.resource.getViewId("mc_forum_about_webo_qq_text"));
        this.loadingBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_loading_container"));
        this.progressBar = (MCProgressBar) findViewById(this.resource.getViewId("mc_forum_progress_bar"));
        this.line1 = (ImageView) findViewById(this.resource.getViewId("mc_forum_line1"));
        this.line2 = (ImageView) findViewById(this.resource.getViewId("mc_forum_line2"));
        this.line3 = (ImageView) findViewById(this.resource.getViewId("mc_forum_line3"));
        this.line4 = (ImageView) findViewById(this.resource.getViewId("mc_forum_line4"));
        this.line5 = (ImageView) findViewById(this.resource.getViewId("mc_forum_line5"));
        this.line6 = (ImageView) findViewById(this.resource.getViewId("mc_forum_line6"));
    }

    public void updateView(AboutModel model) {
        if (model != null) {
            if (!model.getDesc().equals("")) {
                this.descText.setText(model.getDesc());
            } else {
                this.descText.setVisibility(8);
                this.line1.setVisibility(8);
            }
            if (!model.getEmail().equals("")) {
                this.emailText.setText(getResources().getString(this.resource.getStringId("mc_forum_about_email")) + " " + model.getEmail());
            } else {
                this.emailText.setVisibility(8);
                this.line2.setVisibility(8);
            }
            if (!model.getQq().equals("")) {
                this.qqText.setText(getResources().getString(this.resource.getStringId("mc_forum_about_qq")) + " " + model.getQq() + "");
            } else {
                this.qqText.setVisibility(8);
                this.line4.setVisibility(8);
            }
            if (!model.getTel().equals("")) {
                this.telText.setText(getResources().getString(this.resource.getStringId("mc_forum_about_tel")) + " " + model.getTel());
            } else {
                this.telText.setVisibility(8);
                this.line5.setVisibility(8);
            }
            if (!model.getWebsite().equals("")) {
                this.websiteText.setText(getResources().getString(this.resource.getStringId("mc_forum_about_web")) + " " + model.getWebsite());
            } else {
                this.websiteText.setVisibility(8);
                this.line3.setVisibility(8);
            }
            if (!model.getWeibo_sina().equals("")) {
                this.weiboSina.setText(getResources().getString(this.resource.getStringId("mc_forum_about_weibo_sina")));
            } else {
                this.weiboSina.setVisibility(8);
                this.line6.setVisibility(8);
            }
            if (!model.getWeibo_qq().equals("")) {
                this.weiboQQ.setText(getResources().getString(this.resource.getStringId("mc_forum_about_weibo_qq")));
            } else {
                this.weiboQQ.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AboutActivity.this.back();
            }
        });
        this.telText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AboutActivity.this.aboutModel != null && AboutActivity.this.aboutModel.getTel() != null && AboutActivity.this.aboutModel.getTel().trim() != "") {
                    ((ClipboardManager) AboutActivity.this.getSystemService("clipboard")).setText(AboutActivity.this.aboutModel.getTel());
                    AboutActivity.this.warnMessageById("mc_forum_tel_copy_to_clipboard");
                }
            }
        });
        this.emailText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AboutActivity.this.aboutModel != null) {
                    Intent email = new Intent("android.intent.action.SEND");
                    email.setType("text/plain");
                    email.putExtra("android.intent.extra.EMAIL", new String[]{AboutActivity.this.aboutModel.getEmail()});
                    AboutActivity.this.startActivity(email);
                }
            }
        });
        this.websiteText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AboutActivity.this.aboutModel != null) {
                    String url = AboutActivity.this.aboutModel.getWebsite();
                    if (!url.contains("http://")) {
                        url = "http://" + AboutActivity.this.aboutModel.getWebsite();
                    }
                    AboutActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                }
            }
        });
        this.weiboQQ.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AboutActivity.this.aboutModel != null) {
                    String url = AboutActivity.this.aboutModel.getWeibo_qq();
                    if (!url.contains("http://")) {
                        url = "http://" + AboutActivity.this.aboutModel.getWeibo_qq();
                    }
                    AboutActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                }
            }
        });
        this.weiboSina.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AboutActivity.this.aboutModel != null) {
                    String url = AboutActivity.this.aboutModel.getWeibo_sina();
                    if (!url.contains("http://")) {
                        url = "http://" + AboutActivity.this.aboutModel.getWeibo_sina();
                    }
                    AboutActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                }
            }
        });
    }

    class AboutInfoAsyncTask extends AsyncTask<Void, Void, AboutModel> {
        AboutInfoAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            AboutActivity.this.mHandler.post(new Runnable() {
                public void run() {
                    AboutActivity.this.loadingBox.setVisibility(0);
                    AboutActivity.this.progressBar.show();
                }
            });
        }

        /* access modifiers changed from: protected */
        public AboutModel doInBackground(Void... params) {
            return new AboutServiceImpl(AboutActivity.this).getAoutInfo();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(AboutModel result) {
            super.onPostExecute((Object) result);
            AboutActivity.this.mHandler.post(new Runnable() {
                public void run() {
                    if (AboutActivity.this.loadingBox.getVisibility() == 0) {
                        AboutActivity.this.loadingBox.setVisibility(8);
                        AboutActivity.this.progressBar.hide();
                    }
                }
            });
            if (result != null) {
                AboutActivity.this.updateView(result);
                AboutModel unused = AboutActivity.this.aboutModel = result;
                return;
            }
            AboutActivity.this.warnMessageById("mc_forum_no_about");
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.aboutInfoAsyncTask != null) {
            this.aboutInfoAsyncTask.cancel(true);
        }
    }
}
