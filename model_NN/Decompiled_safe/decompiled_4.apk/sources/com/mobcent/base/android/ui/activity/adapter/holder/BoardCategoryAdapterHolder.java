package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.TextView;
import com.mobcent.ad.android.ui.widget.AdView;

public class BoardCategoryAdapterHolder {
    private AdView adViewBox;
    private TextView boardCategoryNameText;

    public AdView getAdViewBox() {
        return this.adViewBox;
    }

    public void setAdViewBox(AdView adViewBox2) {
        this.adViewBox = adViewBox2;
    }

    public TextView getBoardCategoryNameText() {
        return this.boardCategoryNameText;
    }

    public void setBoardCategoryNameText(TextView boardCategoryNameText2) {
        this.boardCategoryNameText = boardCategoryNameText2;
    }
}
