package com.mobcent.base.android.ui.activity.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import com.baidu.location.BDLocation;
import com.mobcent.base.android.ui.activity.adapter.HomeTopicListAdapter;
import com.mobcent.base.android.ui.activity.fragment.HomeTopicFragment;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.forum.android.model.LocationModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.util.LocationUtil;
import java.util.ArrayList;
import java.util.List;

public class SurroundTopicFragment extends HomeTopicFragment {
    private Button backBtn;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public LocationModel locationModel = null;
    /* access modifiers changed from: private */
    public LocationUtil locationUtil = null;
    private int radius = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater2, ViewGroup container, Bundle savedInstanceState) {
        super.initViews(inflater2, container, savedInstanceState);
        this.view = inflater2.inflate(this.mcResource.getLayoutId("mc_forum_surround_topic_fragment"), container, false);
        this.backBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_back_btn"));
        this.pullToRefreshListView = (PullToRefreshListView) this.view.findViewById(this.mcResource.getViewId("mc_forum_lv"));
        this.topicListAdapter = new HomeTopicListAdapter(this.activity, this.topicList, null, this.mHandler, inflater2, this.pageSize, this.asyncTaskLoaderImage);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.topicListAdapter);
        this.inflater = inflater2;
        return this.view;
    }

    public List<TopicModel> getHomeTopicList() {
        PostsService postsService = new PostsServiceImpl(getActivity());
        if (this.locationModel != null) {
            return postsService.getNetSurroudTopic(this.locationModel.getLongitude(), this.locationModel.getLatitude(), this.radius, this.currentPage, this.pageSize);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.topicList = new ArrayList();
        this.refreshimgUrls = new ArrayList();
        this.isLocal = false;
        initLocationUtil(this.activity);
        if (new PostsServiceImpl(getActivity()).getLocalSurroudTopic() == null) {
            this.locationUtil.startLocation();
        } else {
            onRefreshs();
        }
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SurroundTopicFragment.this.activity.finish();
            }
        });
    }

    public void onRefreshs() {
        this.isLocal = true;
        this.currentPage = 1;
        if (!(this.refreshTask == null || this.refreshTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.refreshTask.cancel(true);
        }
        if (this.locationModel != null) {
            refreshTopic(this.locationModel);
        } else {
            this.locationUtil.startLocation();
        }
    }

    /* access modifiers changed from: private */
    public void refreshTopic(LocationModel locationModel2) {
        if (locationModel2 != null) {
            this.refreshTask = new HomeTopicFragment.RefreshTopicListTask();
            this.refreshTask.execute(Long.valueOf((long) this.currentPage), Long.valueOf((long) this.pageSize));
        }
    }

    private void initLocationUtil(Context context) {
        this.locationUtil = new LocationUtil(context, true, new LocationUtil.LocationDelegate() {
            public void locationResults(BDLocation location) {
                if (location.getLocType() == 161 || location.getLocType() == 61) {
                    LocationModel unused = SurroundTopicFragment.this.locationModel = SurroundTopicFragment.this.locationUtil.getLocation();
                    SurroundTopicFragment.this.refreshTopic(SurroundTopicFragment.this.locationModel);
                } else if (location.getLocType() == 62) {
                    SurroundTopicFragment.this.warnMessageById("mc_forum_location_fail_warn");
                    SurroundTopicFragment.this.updateLocationFail();
                } else if (location.getLocType() == 167) {
                    SurroundTopicFragment.this.warnMessageById("mc_forum_service_location_fail_warn");
                    SurroundTopicFragment.this.updateLocationFail();
                } else if (location.getLocType() == 63) {
                    SurroundTopicFragment.this.warnMessageById("mc_forum_connection_fail");
                    SurroundTopicFragment.this.updateLocationFail();
                }
            }

            public void onReceivePoi(boolean hasPoi, List<String> list) {
            }
        });
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.locationUtil != null) {
            this.locationUtil.stopLocation();
        }
    }

    /* access modifiers changed from: private */
    public void updateLocationFail() {
        this.topicListAdapter = new HomeTopicListAdapter(this.activity, new ArrayList(), null, this.mHandler, this.inflater, this.pageSize, this.asyncTaskLoaderImage);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.topicListAdapter);
        this.pullToRefreshListView.onRefreshComplete();
        this.pullToRefreshListView.onBottomRefreshComplete(3);
        warnMessageById("mc_forum_un_obtain_location_info_warn");
    }
}
