package com.mobcent.base.android.ui.widget.pulltorefresh;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshBase;

public class PullToRefreshScrollView extends PullToRefreshBase<ScrollView> {
    private final PullToRefreshBase.OnRefreshListener defaultOnRefreshListener = new PullToRefreshBase.OnRefreshListener() {
        public void onRefresh() {
            PullToRefreshScrollView.this.onRefreshComplete();
        }
    };

    public PullToRefreshScrollView(Context context) {
        super(context);
        setOnRefreshListener(this.defaultOnRefreshListener);
    }

    public PullToRefreshScrollView(Context context, int mode) {
        super(context, mode);
        setOnRefreshListener(this.defaultOnRefreshListener);
    }

    public PullToRefreshScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnRefreshListener(this.defaultOnRefreshListener);
    }

    /* access modifiers changed from: protected */
    public ScrollView createRefreshableView(Context context, AttributeSet attrs) {
        return new ScrollView(context, attrs);
    }

    /* access modifiers changed from: protected */
    public boolean isReadyForPullDown() {
        return ((ScrollView) this.refreshableView).getScrollY() == 0;
    }

    /* access modifiers changed from: protected */
    public boolean onScroll(MotionEvent event) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void gotoTop() {
    }
}
