package com.mobcent.base.android.ui.activity.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.adapter.MsgUserListAdapter;
import com.mobcent.base.android.ui.activity.delegate.ChatRetrunDelegate;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.model.HeartMsgModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.HeartMsgService;
import com.mobcent.forum.android.service.impl.HeartMsgServiceImpl;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class MsgUserListFragment extends BaseFragment {
    private ChatRetrunDelegate chatRetrunDelegate = new ChatRetrunDelegate() {
        public void invalidateUserList() {
            MsgUserListFragment.this.invalidateUserList();
        }

        public MsgUserListFragment getMsgUserListFragment() {
            return MsgUserListFragment.this;
        }
    };
    /* access modifiers changed from: private */
    public int currentPage = 1;
    protected ViewPager mPager;
    /* access modifiers changed from: private */
    public PullToRefreshListView mPullRefreshListView;
    private MoreTask moreTask;
    /* access modifiers changed from: private */
    public HeartMsgService msgService;
    /* access modifiers changed from: private */
    public List<UserInfoModel> msgUserInfoList = new ArrayList();
    /* access modifiers changed from: private */
    public MsgUserListAdapter msgUserListAdapter;
    private RefreshTask refreshTask;
    /* access modifiers changed from: private */
    public List<String> refreshimgUrls;

    static /* synthetic */ int access$308(MsgUserListFragment x0) {
        int i = x0.currentPage;
        x0.currentPage = i + 1;
        return i;
    }

    static /* synthetic */ int access$310(MsgUserListFragment x0) {
        int i = x0.currentPage;
        x0.currentPage = i - 1;
        return i;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
            this.mPullRefreshListView.onRefresh(true);
        } else {
            this.mPullRefreshListView.onRefreshComplete();
            warnMessageByStr(this.mcResource.getString("mc_forum_permission_cannot_visit_forum"));
        }
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.msgService = new HeartMsgServiceImpl(this.activity);
        this.msgUserListAdapter = new MsgUserListAdapter(this.activity, this.msgUserInfoList, this.mHandler, this.asyncTaskLoaderImage);
        this.refreshimgUrls = new ArrayList();
        MsgChatRoomFragment.setChatRetrunDelegate(this.chatRetrunDelegate);
        this.permService = new PermServiceImpl(this.activity);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(this.mcResource.getLayoutId("mc_forum_msg_user_list_fragment"), container, false);
        this.mPullRefreshListView = (PullToRefreshListView) this.view.findViewById(this.mcResource.getViewId("mc_forum_lv"));
        this.mPullRefreshListView.setAdapter((ListAdapter) this.msgUserListAdapter);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.mPullRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                if (MsgUserListFragment.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.VISIT, -1) == 1) {
                    MsgUserListFragment.this.onRefresh();
                    return;
                }
                MsgUserListFragment.this.mPullRefreshListView.onRefreshComplete();
                MsgUserListFragment.this.warnMessageByStr(MsgUserListFragment.this.mcResource.getString("mc_forum_permission_cannot_visit_forum"));
            }
        });
        this.mPullRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                MsgUserListFragment.this.onLoadMore();
            }
        });
    }

    public void onRefresh() {
        if (this.refreshimgUrls != null && !this.refreshimgUrls.isEmpty() && this.refreshimgUrls.size() > 0) {
            this.asyncTaskLoaderImage.recycleBitmaps(this.refreshimgUrls);
        }
        if (!(this.refreshTask == null || this.refreshTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.refreshTask.cancel(true);
        }
        this.refreshTask = new RefreshTask();
        this.refreshTask.execute(50);
    }

    public void onLoadMore() {
        if (!(this.moreTask == null || this.moreTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.moreTask.cancel(true);
        }
        this.moreTask = new MoreTask();
        this.moreTask.execute(50);
    }

    public MsgUserListFragment() {
    }

    public MsgUserListFragment(ViewPager mPager2) {
        this.mPager = mPager2;
    }

    private class RefreshTask extends AsyncTask<Integer, Void, List<UserInfoModel>> {
        private RefreshTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<UserInfoModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            int unused = MsgUserListFragment.this.currentPage = 1;
            MsgUserListFragment.this.mPullRefreshListView.setSelection(0);
            MsgUserListFragment.this.asyncTaskLoaderImage.recycleBitmaps(MsgUserListFragment.this.refreshimgUrls);
        }

        /* access modifiers changed from: protected */
        public List<UserInfoModel> doInBackground(Integer... params) {
            return MsgUserListFragment.this.msgService.getMsgUserList(MsgUserListFragment.this.currentPage, params[0].intValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<UserInfoModel> result) {
            super.onPostExecute((Object) result);
            MsgUserListFragment.this.mPullRefreshListView.onRefreshComplete();
            if (result == null) {
                Toast.makeText(MsgUserListFragment.this.activity, MsgUserListFragment.this.getString(MsgUserListFragment.this.mcResource.getStringId("mc_forum_no_msg_user")), 1).show();
            } else if (result.isEmpty()) {
                MsgUserListFragment.this.mPullRefreshListView.onBottomRefreshComplete(3);
                Toast.makeText(MsgUserListFragment.this.activity, MsgUserListFragment.this.getString(MsgUserListFragment.this.mcResource.getStringId("mc_forum_no_msg_user")), 1).show();
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                Toast.makeText(MsgUserListFragment.this.activity, MCForumErrorUtil.convertErrorCode(MsgUserListFragment.this.activity, result.get(0).getErrorCode()), 0).show();
                MsgUserListFragment.this.mPullRefreshListView.onBottomRefreshComplete(0);
            } else {
                List unused = MsgUserListFragment.this.refreshimgUrls = MsgUserListFragment.this.getRefreshImgUrl(result);
                MsgUserListFragment.this.msgUserListAdapter.setHeartBeatModelList(MsgUserListFragment.this.msgService.getHeartBeatListLocally());
                MsgUserListFragment.this.msgUserListAdapter.setUserInfoList(result);
                MsgUserListFragment.this.msgUserListAdapter.notifyDataSetInvalidated();
                MsgUserListFragment.this.msgUserListAdapter.notifyDataSetChanged();
                if (result.get(0).getTotalNum() > result.size()) {
                    MsgUserListFragment.this.mPullRefreshListView.onBottomRefreshComplete(0);
                } else {
                    MsgUserListFragment.this.mPullRefreshListView.onBottomRefreshComplete(3);
                }
                List unused2 = MsgUserListFragment.this.msgUserInfoList = result;
            }
        }
    }

    private class MoreTask extends AsyncTask<Integer, Void, List<UserInfoModel>> {
        private MoreTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<UserInfoModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            MsgUserListFragment.access$308(MsgUserListFragment.this);
        }

        /* access modifiers changed from: protected */
        public List<UserInfoModel> doInBackground(Integer... params) {
            return MsgUserListFragment.this.msgService.getMsgUserList(MsgUserListFragment.this.currentPage, params[0].intValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<UserInfoModel> result) {
            super.onPostExecute((Object) result);
            if (result == null) {
                MsgUserListFragment.access$310(MsgUserListFragment.this);
            } else if (result.isEmpty()) {
                MsgUserListFragment.this.mPullRefreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                MsgUserListFragment.access$310(MsgUserListFragment.this);
                Toast.makeText(MsgUserListFragment.this.activity, MCForumErrorUtil.convertErrorCode(MsgUserListFragment.this.activity, result.get(0).getErrorCode()), 0).show();
                MsgUserListFragment.this.mPullRefreshListView.onBottomRefreshComplete(0);
            } else {
                List<UserInfoModel> list = new ArrayList<>();
                list.addAll(MsgUserListFragment.this.msgUserInfoList);
                list.addAll(result);
                MsgUserListFragment.this.msgUserListAdapter.setHeartBeatModelList(MsgUserListFragment.this.msgService.getHeartBeatListLocally());
                MsgUserListFragment.this.msgUserListAdapter.setUserInfoList(list);
                MsgUserListFragment.this.msgUserListAdapter.notifyDataSetInvalidated();
                MsgUserListFragment.this.msgUserListAdapter.notifyDataSetChanged();
                if (result.get(0).getTotalNum() > list.size()) {
                    MsgUserListFragment.this.mPullRefreshListView.onBottomRefreshComplete(0);
                } else {
                    MsgUserListFragment.this.mPullRefreshListView.onBottomRefreshComplete(3);
                }
                List unused = MsgUserListFragment.this.msgUserInfoList = list;
            }
        }
    }

    public void onResume() {
        super.onResume();
    }

    public void invalidateUserList() {
        List<HeartMsgModel> heartBeatModelList;
        if (this.msgService != null && (heartBeatModelList = this.msgService.getHeartBeatListLocally()) != null && this.msgUserListAdapter != null) {
            this.msgUserListAdapter.setHeartBeatModelList(heartBeatModelList);
            this.msgUserListAdapter.notifyDataSetInvalidated();
            this.msgUserListAdapter.notifyDataSetChanged();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (!(this.refreshTask == null || this.refreshTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.refreshTask.cancel(true);
        }
        if (this.moreTask != null && this.moreTask.getStatus() != AsyncTask.Status.FINISHED) {
            this.moreTask.cancel(true);
        }
    }

    /* access modifiers changed from: private */
    public List<String> getRefreshImgUrl(List<UserInfoModel> result) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < this.msgUserInfoList.size(); i++) {
            UserInfoModel model = this.msgUserInfoList.get(i);
            if (!isExit(model, result)) {
                list.add(AsyncTaskLoaderImage.formatUrl(model.getIcon(), "100x100"));
            }
        }
        return list;
    }

    private boolean isExit(UserInfoModel model, List<UserInfoModel> result) {
        int j = result.size();
        for (int i = 0; i < j; i++) {
            UserInfoModel model2 = result.get(i);
            if (!StringUtil.isEmpty(model.getIcon()) && !StringUtil.isEmpty(model2.getIcon()) && model.getIcon().equals(model2.getIcon())) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public List<String> getImageURL(int start, int end) {
        List<String> imgUrls = new ArrayList<>();
        for (int i = start; i < end; i++) {
            imgUrls.add(AsyncTaskLoaderImage.formatUrl(this.msgUserInfoList.get(i).getIcon(), "100x100"));
        }
        return imgUrls;
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return getImageURL(0, this.msgUserInfoList.size());
    }
}
