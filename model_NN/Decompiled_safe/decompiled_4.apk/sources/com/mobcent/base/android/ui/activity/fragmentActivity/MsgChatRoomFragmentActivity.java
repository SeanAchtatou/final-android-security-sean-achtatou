package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.KeyEvent;
import com.mobcent.android.utils.MCLogUtil;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.fragment.MsgChatRoomFragment;
import com.mobcent.base.android.ui.activity.service.MediaService;
import com.mobcent.forum.android.model.SoundModel;
import java.util.List;

public class MsgChatRoomFragmentActivity extends BaseFragmentActivity {
    private int blackStatus = 0;
    private String chatUserIcon = "";
    private long chatUserId = 0;
    private String chatUserNickname = null;
    private MediaPlayerBroadCastReceiver mediaPlayerBroadCastReceiver = null;
    /* access modifiers changed from: private */
    public MsgChatRoomFragment msgChatRoomFragment;

    /* access modifiers changed from: protected */
    public void initData() {
        this.fragmentManager = getSupportFragmentManager();
        Intent intent = getIntent();
        this.chatUserId = intent.getLongExtra(MCConstant.CHAT_USER_ID, 0);
        this.chatUserIcon = intent.getStringExtra(MCConstant.CHAT_USER_ICON);
        this.chatUserNickname = intent.getStringExtra(MCConstant.CHAT_USER_NICKNAME);
        this.blackStatus = intent.getIntExtra(MCConstant.BLACK_USER_STATUS, 0);
        MCLogUtil.e("test", "blackStatus=" + this.blackStatus);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_msg_chat_room_activity"));
        this.msgChatRoomFragment = (MsgChatRoomFragment) this.fragmentManager.findFragmentById(this.resource.getViewId("mc_forum_fragment"));
        this.msgChatRoomFragment.setChatUserId(this.chatUserId);
        this.msgChatRoomFragment.setChatUserNickname(this.chatUserNickname);
        this.msgChatRoomFragment.setChatUserIcon(this.chatUserIcon);
        this.msgChatRoomFragment.setBlackStatus(this.blackStatus);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.msgChatRoomFragment.onKeyDown(keyCode, event)) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Intent intent = new Intent(this, MediaService.class);
        intent.putExtra(MediaService.SERVICE_TAG, toString());
        intent.putExtra(MediaService.SERVICE_STATUS, 6);
        startService(intent);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.mediaPlayerBroadCastReceiver == null) {
            this.mediaPlayerBroadCastReceiver = new MediaPlayerBroadCastReceiver();
        }
        registerReceiver(this.mediaPlayerBroadCastReceiver, new IntentFilter(MediaService.INTENT_TAG + getPackageName()));
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.mediaPlayerBroadCastReceiver != null) {
            unregisterReceiver(this.mediaPlayerBroadCastReceiver);
        }
    }

    public class MediaPlayerBroadCastReceiver extends BroadcastReceiver {
        private SoundModel currSoundModel = null;
        private String tag;

        public MediaPlayerBroadCastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            this.tag = intent.getStringExtra(MediaService.SERVICE_TAG);
            if (this.tag != null && this.tag.equals(MsgChatRoomFragmentActivity.this.toString())) {
                this.currSoundModel = (SoundModel) intent.getSerializableExtra(MediaService.SERVICE_MODEL);
                MsgChatRoomFragmentActivity.this.msgChatRoomFragment.msgChatRoomListAdapter.updateReceivePlayImg(this.currSoundModel);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.msgChatRoomFragment.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
