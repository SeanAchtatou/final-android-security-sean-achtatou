package com.mobcent.base.android.ui.activity.asyncTaskLoader;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.service.ReportService;
import com.mobcent.forum.android.service.impl.ReportServiceImpl;
import com.mobcent.forum.android.util.MCResource;

public class CancelReportTask extends AsyncTask<Long, Void, String> {
    private Context context;
    private MCResource resource;
    private TaskExecuteDelegate taskExecuteDelegate;
    private int type;

    public CancelReportTask(Context context2, int type2) {
        this.context = context2;
        this.resource = MCResource.getInstance(context2);
        this.type = type2;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Long... params) {
        ReportService reportService = new ReportServiceImpl(this.context);
        if (this.type == 1) {
            return reportService.cancelReportTopic(params[0].longValue());
        }
        if (this.type == 3) {
            return reportService.cancelReportUser(params[0].longValue());
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String result) {
        super.onPostExecute((Object) result);
        if (result != null) {
            Toast.makeText(this.context, MCForumErrorUtil.convertErrorCode(this.context, result), 1).show();
            if (this.taskExecuteDelegate != null) {
                this.taskExecuteDelegate.executeFail();
                return;
            }
            return;
        }
        Toast.makeText(this.context, this.context.getResources().getString(this.resource.getStringId("mc_forum_cancel_report_user")), 1).show();
        if (this.taskExecuteDelegate != null) {
            this.taskExecuteDelegate.executeSuccess();
        }
    }

    public TaskExecuteDelegate getTaskExecuteDelegate() {
        return this.taskExecuteDelegate;
    }

    public void setTaskExecuteDelegate(TaskExecuteDelegate taskExecuteDelegate2) {
        this.taskExecuteDelegate = taskExecuteDelegate2;
    }
}
