package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.UserMyInfoActivity;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.android.ui.activity.receiver.HeartBeatReceiver;
import com.mobcent.base.android.ui.activity.receiver.MCForumReceiver;
import com.mobcent.base.android.ui.activity.receiver.PopNoticeReceiver;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCThemeResource;
import com.mobcent.forum.android.application.McForumApplication;
import com.mobcent.forum.android.service.PermService;
import com.mobcent.forum.android.service.impl.ForumServiceImpl;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import java.util.List;

public abstract class BaseFragmentActivity extends FragmentActivity implements MCConstant {
    protected String appKey;
    protected AsyncTaskLoaderImage asyncTaskLoaderImage;
    private MCForumReceiver forumReceiver;
    protected FragmentManager fragmentManager;
    protected HeartBeatReceiver heartBeatReceiver;
    private InputMethodManager imm;
    protected Handler mHandler;
    protected ProgressDialog myDialog;
    protected ImageView navHomeImg;
    protected ImageView navMsgImg;
    protected ImageView navPicImg;
    protected ImageView navRecommendImg;
    protected ImageView navUserImg;
    protected PermService permService;
    protected PopNoticeReceiver popNoticeReceiver = null;
    protected MCResource resource;
    protected MCThemeResource themeResource;

    /* access modifiers changed from: protected */
    public abstract List<String> getAllImageURL();

    /* access modifiers changed from: protected */
    public abstract void initData();

    /* access modifiers changed from: protected */
    public abstract void initViews();

    /* access modifiers changed from: protected */
    public abstract void initWidgetActions();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.appKey = new ForumServiceImpl().getForumKey(this);
        this.resource = MCResource.getInstance(this);
        this.themeResource = MCThemeResource.getInstance(this, "packageName");
        this.mHandler = new Handler();
        if (MCForumHelper.logoutApp() && McForumApplication.getInstance() != null) {
            McForumApplication.getInstance().addAcitvity(this);
        }
        this.imm = (InputMethodManager) getSystemService("input_method");
        requestWindowFeature(1);
        this.asyncTaskLoaderImage = AsyncTaskLoaderImage.getInstance(getApplicationContext(), toString());
        this.forumReceiver = new MCForumReceiver(this);
        initData();
        initViews();
        initNavView(this, this.resource, this.themeResource);
        initWidgetActions();
        getAllImageURL();
        onTheme();
        MCForumHelper.setAnimation(this, true);
    }

    public void clearNotification() {
        NotificationManager manager = (NotificationManager) getSystemService("notification");
        manager.cancel(1);
        manager.cancel(3);
        manager.cancel(2);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (!MCForumHelper.logoutApp()) {
            return false;
        }
        menu.add(0, 2, 0, this.resource.getStringId("mc_forum_user_logout_app"));
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                McForumApplication.getInstance().finishAllActivity();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onTheme() {
    }

    /* access modifiers changed from: protected */
    public void initNavView(Activity activity, MCResource resource2, MCThemeResource themeResource2) {
        MCForumHelper.initNav(activity, resource2, themeResource2);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.forumReceiver.regBroadcastReceiver();
        if (isAction() && !MCForumHelper.getForumConfig().getApplicationVisible()) {
            this.permService = new PermServiceImpl(this);
            this.permService.getPerm();
            MCForumHelper.getForumConfig().setApplicationVisible(true);
        }
    }

    private boolean isAction() {
        List<ActivityManager.RunningTaskInfo> tasks = ((ActivityManager) getApplicationContext().getSystemService("activity")).getRunningTasks(1);
        if (tasks == null || tasks.isEmpty() || !tasks.get(0).topActivity.getPackageName().equals(getApplicationContext().getPackageName())) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.forumReceiver.unregBroadcastReceiver();
        if (!isAction()) {
            MCForumHelper.getForumConfig().setApplicationVisible(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public void onBackPressed() {
        super.onBackPressed();
        MCForumHelper.setAnimation(this, false);
    }

    public void finish() {
        super.finish();
        MCForumHelper.setAnimation(this, false);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.asyncTaskLoaderImage.recycleBitmaps(getAllImageURL());
        if (MCForumHelper.logoutApp() && McForumApplication.getInstance() != null) {
            McForumApplication.getInstance().remove(this);
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void warnMessageById(String warnMessId) {
        Toast.makeText(this, this.resource.getStringId(warnMessId), 0).show();
    }

    /* access modifiers changed from: protected */
    public void warnMessageByStr(String str) {
        Toast.makeText(this, str, 0).show();
    }

    /* access modifiers changed from: protected */
    public void showProgressDialog(String str, final AsyncTask obj) {
        this.myDialog = new ProgressDialog(this);
        this.myDialog.setProgressStyle(0);
        this.myDialog.setTitle(getResources().getString(this.resource.getStringId("mc_forum_dialog_tip")));
        this.myDialog.setMessage(getResources().getString(this.resource.getStringId(str)));
        this.myDialog.setIndeterminate(false);
        this.myDialog.setCancelable(true);
        this.myDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == 4) {
                    BaseFragmentActivity.this.hideProgressDialog();
                    if (!(obj == null || obj.getStatus() == AsyncTask.Status.FINISHED)) {
                        obj.cancel(true);
                    }
                    if (obj instanceof UserMyInfoActivity.LoadAsyncTask) {
                        BaseFragmentActivity.this.finish();
                    }
                }
                return true;
            }
        });
        this.myDialog.show();
    }

    /* access modifiers changed from: protected */
    public void hideProgressDialog() {
        if (this.myDialog != null) {
            this.myDialog.cancel();
        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            this.imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 2);
        }
    }

    public void showSoftKeyboard() {
        if (getCurrentFocus() != null) {
            this.imm.showSoftInput(getCurrentFocus(), 1);
        }
    }

    public void showSoftKeyboard(View view) {
        view.requestFocus();
        this.imm.showSoftInput(view, 1);
    }
}
