package com.mobcent.base.android.ui.activity.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCThemeResource;

public class MCRefreshListView extends ListView {
    public static final int FOOTER_INIT = 10;
    public static final int FOOTER_LOADED = 13;
    public static final int FOOTER_LOADING = 12;
    public static final int FOOTER_MORE = 11;
    public static final int REFRESH_LIST_SHARE_MODE = 0;
    public static final String REFRESH_LIST_SHARE_NAME = "RefreshListSharedName";
    private Context context;
    private MCResource forumResource;
    private RelativeLayout mFooterLoadingView;
    private RelativeLayout mFooterRefreshView;
    private TextView mFooterTextView;
    private LayoutInflater mInflater;
    /* access modifiers changed from: private */
    public onFooterListener mOnFooterListener;
    private MCProgressBar mProgressBar;
    /* access modifiers changed from: private */
    public int mfooterState = 10;
    private MCThemeResource themeResource;

    public interface onFooterListener {
        void onLoadMore();
    }

    public MCRefreshListView(Context context2) {
        super(context2);
        this.context = context2;
        initFooterView();
    }

    public MCRefreshListView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        this.context = context2;
        initFooterView();
    }

    public MCRefreshListView(Context context2, AttributeSet attrs, int defStyle) {
        super(context2, attrs, defStyle);
        this.context = context2;
        initFooterView();
    }

    private void initFooterView() {
        this.forumResource = MCResource.getInstance(getContext());
        this.themeResource = MCThemeResource.getInstance(getContext(), "packageName");
        this.mInflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
        this.mFooterRefreshView = (RelativeLayout) this.mInflater.inflate(this.forumResource.getLayoutId("mc_forum_widget_footer_loading"), (ViewGroup) null);
        this.mFooterTextView = (TextView) this.mFooterRefreshView.findViewById(this.forumResource.getViewId("mc_forum_txt_loading"));
        this.mFooterLoadingView = (RelativeLayout) this.mFooterRefreshView.findViewById(this.forumResource.getViewId("mc_forum_loading_container"));
        this.mProgressBar = (MCProgressBar) this.mFooterRefreshView.findViewById(this.forumResource.getViewId("mc_forum_progress_bar"));
        this.mFooterRefreshView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MCRefreshListView.this.mfooterState == 11) {
                    MCRefreshListView.this.mOnFooterListener.onLoadMore();
                }
            }
        });
        updateFooterState(10);
    }

    public void onTheme() {
        this.mFooterTextView.setTextColor(this.themeResource.getColor("mc_forum_pull_down_refresh_text_color"));
        this.mProgressBar.setBackgroundDrawable(this.themeResource.getDrawable("mc_forum_loading1"));
    }

    public void addFooterView() {
        addFooterView(this.mFooterRefreshView);
    }

    public void setFooterVisible() {
        this.mFooterRefreshView.setVisibility(0);
    }

    public void onFooterReady() {
        updateFooterState(11);
    }

    public void onFooterLoadIng() {
        updateFooterState(12);
    }

    public void onFooterLoaded() {
        updateFooterState(13);
    }

    public void onFooterMore() {
        updateFooterState(11);
    }

    public void goneFooter() {
        this.mFooterRefreshView.setVisibility(8);
    }

    public void updateFooterState(int footerState) {
        this.mfooterState = footerState;
        switch (this.mfooterState) {
            case 10:
                this.mFooterRefreshView.setVisibility(8);
                return;
            case 11:
                this.mFooterRefreshView.setVisibility(0);
                this.mFooterTextView.setVisibility(0);
                this.mFooterTextView.setText(this.forumResource.getString("mc_forum_more"));
                this.mFooterLoadingView.setVisibility(8);
                this.mProgressBar.stop();
                return;
            case 12:
                this.mFooterRefreshView.setVisibility(0);
                this.mFooterTextView.setVisibility(0);
                this.mFooterTextView.setText(this.forumResource.getString("mc_forum_doing_update"));
                this.mFooterLoadingView.setVisibility(0);
                this.mProgressBar.show();
                return;
            case FOOTER_LOADED /*13*/:
                this.mFooterRefreshView.setVisibility(0);
                this.mFooterTextView.setVisibility(0);
                this.mFooterTextView.setText(this.forumResource.getString("mc_forum_loaded"));
                this.mFooterLoadingView.setVisibility(8);
                this.mProgressBar.stop();
                return;
            default:
                return;
        }
    }

    public void setFooterContent(String s) {
        this.mFooterRefreshView.setVisibility(0);
        this.mFooterTextView.setVisibility(0);
        this.mFooterTextView.setText(s);
        this.mFooterLoadingView.setVisibility(8);
        this.mProgressBar.stop();
    }

    public void setFooterContent(int resId) {
        this.mFooterRefreshView.setVisibility(0);
        this.mFooterTextView.setVisibility(0);
        this.mFooterTextView.setText(resId);
        this.mFooterLoadingView.setVisibility(8);
        this.mProgressBar.stop();
    }

    public void setOnFooterListener(onFooterListener mOnFooterListener2) {
        this.mOnFooterListener = mOnFooterListener2;
    }

    public void setAdapter(ListAdapter adapter) {
        super.setAdapter(adapter);
    }

    public void addFooterTopPadding(int topPadding) {
        this.mFooterRefreshView.setPadding(this.mFooterRefreshView.getPaddingLeft(), topPadding, this.mFooterRefreshView.getPaddingRight(), this.mFooterRefreshView.getPaddingBottom());
        this.mFooterRefreshView.invalidate();
    }
}
