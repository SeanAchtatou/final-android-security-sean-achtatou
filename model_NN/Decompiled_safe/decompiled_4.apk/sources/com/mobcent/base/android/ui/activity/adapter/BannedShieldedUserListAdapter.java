package com.mobcent.base.android.ui.activity.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.UserBannedActivity;
import com.mobcent.base.android.ui.activity.UserShieldedActivity;
import com.mobcent.base.android.ui.activity.adapter.holder.BannedShieldedUserAdapterHolder;
import com.mobcent.base.android.ui.activity.fragmentActivity.MsgChatRoomFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCTouchUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.UserManageService;
import com.mobcent.forum.android.service.impl.UserManageServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.List;

public class BannedShieldedUserListAdapter extends BaseAdapter implements MCConstant {
    /* access modifiers changed from: private */
    public Activity activity;
    private AsyncTaskLoaderImage asyncTaskLoaderImage;
    /* access modifiers changed from: private */
    public int boardId;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public MCResource resource;
    /* access modifiers changed from: private */
    public int type;
    /* access modifiers changed from: private */
    public List<UserInfoModel> userInfoList;
    /* access modifiers changed from: private */
    public UserManageService userManageService;

    public BannedShieldedUserListAdapter(Activity activity2, int type2, int boardId2, List<UserInfoModel> userInfoList2, LayoutInflater inflater2, AsyncTaskLoaderImage asyncTaskLoaderImage2, Handler mHandler2) {
        this.activity = activity2;
        this.type = type2;
        this.boardId = boardId2;
        this.userInfoList = userInfoList2;
        this.inflater = inflater2;
        this.resource = MCResource.getInstance(activity2);
        this.mHandler = mHandler2;
        this.asyncTaskLoaderImage = asyncTaskLoaderImage2;
        this.userManageService = new UserManageServiceImpl(activity2);
    }

    public List<UserInfoModel> getUserInfoList() {
        return this.userInfoList;
    }

    public void setUserInfoList(List<UserInfoModel> userInfoList2) {
        this.userInfoList = userInfoList2;
    }

    public int getCount() {
        return getUserInfoList().size();
    }

    public Object getItem(int position) {
        return getUserInfoList().get(position);
    }

    public long getItemId(int position) {
        return getUserInfoList().get(position).getUserId();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final UserInfoModel userInfoModel = this.userInfoList.get(position);
        final int currentPos = position;
        View convertView2 = getBannedShieldedConvertView(convertView);
        BannedShieldedUserAdapterHolder holder = (BannedShieldedUserAdapterHolder) convertView2.getTag();
        if (this.type == 4) {
            holder.getReasonTitleText().setText(this.activity.getResources().getString(this.resource.getStringId("mc_forum_banned_reason_str")));
            holder.getTimeTitleText().setText(this.activity.getResources().getString(this.resource.getStringId("mc_forum_banned_time_str")));
            holder.getBoardTitleText().setText(this.activity.getResources().getString(this.resource.getStringId("mc_forum_banned_board")));
            if (this.boardId == 0) {
                holder.getCancelBannedBtn().setText(this.activity.getResources().getString(this.resource.getStringId("mc_forum_banned_setting")));
            }
            holder.getCancelBannedBtn().setVisibility(0);
            holder.getCancelShieldedBtn().setVisibility(8);
        } else if (this.type == 1) {
            holder.getReasonTitleText().setText(this.activity.getResources().getString(this.resource.getStringId("mc_forum_shielded_reason_str")));
            holder.getTimeTitleText().setText(this.activity.getResources().getString(this.resource.getStringId("mc_forum_shielded_time_str")));
            holder.getBoardTitleText().setText(this.activity.getResources().getString(this.resource.getStringId("mc_forum_shielded_board")));
            if (this.boardId == 0) {
                holder.getCancelShieldedBtn().setText(this.activity.getResources().getString(this.resource.getStringId("mc_forum_shielded_setting")));
            }
            holder.getCancelShieldedBtn().setVisibility(0);
            holder.getCancelBannedBtn().setVisibility(8);
        }
        MCTouchUtil.createTouchDelegate(holder.getCancelBannedBtn(), 10);
        holder.getUserIconImage().setBackgroundResource(this.resource.getDrawableId("mc_forum_head"));
        updateUserIconImage(userInfoModel.getIcon(), convertView2, holder.getUserIconImage());
        if (userInfoModel.getNickname().length() <= 12) {
            holder.getUserNameText().setText(userInfoModel.getNickname());
        } else {
            holder.getUserNameText().setText(userInfoModel.getNickname().substring(0, 11) + "...");
        }
        if (!StringUtil.isEmpty(userInfoModel.getUserBoardInfoList().get(0).getBoardName())) {
            holder.getBoardText().setText(userInfoModel.getUserBoardInfoList().get(0).getBoardName());
        }
        if (!StringUtil.isEmpty(userInfoModel.getReasonStr())) {
            holder.getReasonText().setText(userInfoModel.getReasonStr());
        } else {
            holder.getReasonText().setText(this.activity.getResources().getString(this.resource.getStringId("mc_forum_unknown_reason")));
        }
        holder.getTimeText().setText(userInfoModel.getBannedShieldedStartDate() + this.activity.getResources().getString(this.resource.getStringId("mc_forum_to_str")) + userInfoModel.getBannedShieldedDate());
        holder.getUserIconImage().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCForumHelper.gotoUserInfo(BannedShieldedUserListAdapter.this.activity, BannedShieldedUserListAdapter.this.resource, userInfoModel.getUserId());
            }
        });
        holder.getChatBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(BannedShieldedUserListAdapter.this.activity, MsgChatRoomFragmentActivity.class);
                intent.putExtra(MCConstant.CHAT_USER_ID, userInfoModel.getUserId());
                intent.putExtra(MCConstant.CHAT_USER_NICKNAME, userInfoModel.getNickname());
                intent.putExtra(MCConstant.CHAT_USER_ICON, userInfoModel.getIcon());
                intent.putExtra(MCConstant.BLACK_USER_STATUS, userInfoModel.getBlackStatus());
                BannedShieldedUserListAdapter.this.activity.startActivity(intent);
            }
        });
        holder.getCancelBannedBtn().setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
            public void onClick(View v) {
                if (((long) BannedShieldedUserListAdapter.this.boardId) != 0) {
                    String result = BannedShieldedUserListAdapter.this.userManageService.cancelBannedShielded(BannedShieldedUserListAdapter.this.type, userInfoModel.getUserId(), BannedShieldedUserListAdapter.this.boardId);
                    if (!StringUtil.isEmpty(result)) {
                        Toast.makeText(BannedShieldedUserListAdapter.this.activity, MCForumErrorUtil.convertErrorCode(BannedShieldedUserListAdapter.this.activity, result), 1).show();
                    }
                    if (result == null) {
                        Toast.makeText(BannedShieldedUserListAdapter.this.activity, BannedShieldedUserListAdapter.this.resource.getStringId("mc_forum_cancel_banned_succ"), 1).show();
                        BannedShieldedUserListAdapter.this.userInfoList.remove(currentPos);
                        BannedShieldedUserListAdapter.this.notifyDataSetChanged();
                    }
                } else if (BannedShieldedUserListAdapter.this.type == 4) {
                    Intent intent = new Intent(BannedShieldedUserListAdapter.this.activity, UserBannedActivity.class);
                    intent.putExtra(MCConstant.BANNED_TYPE, 4);
                    intent.putExtra(MCConstant.BANNED_USER_ID, userInfoModel.getUserId());
                    intent.putExtra("boardId", -1L);
                    BannedShieldedUserListAdapter.this.activity.startActivity(intent);
                } else if (BannedShieldedUserListAdapter.this.type == 1) {
                    Intent intent2 = new Intent(BannedShieldedUserListAdapter.this.activity, UserShieldedActivity.class);
                    intent2.putExtra(MCConstant.SHIELDED_TYPE, 1);
                    intent2.putExtra("shieldedUserId", userInfoModel.getUserId());
                    intent2.putExtra("boardId", -1L);
                    BannedShieldedUserListAdapter.this.activity.startActivity(intent2);
                }
            }
        });
        holder.getCancelShieldedBtn().setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
            public void onClick(View v) {
                if (((long) BannedShieldedUserListAdapter.this.boardId) != 0) {
                    String result = BannedShieldedUserListAdapter.this.userManageService.cancelBannedShielded(BannedShieldedUserListAdapter.this.type, userInfoModel.getUserId(), BannedShieldedUserListAdapter.this.boardId);
                    if (!StringUtil.isEmpty(result)) {
                        Toast.makeText(BannedShieldedUserListAdapter.this.activity, MCForumErrorUtil.convertErrorCode(BannedShieldedUserListAdapter.this.activity, result), 1).show();
                    }
                    if (result == null) {
                        Toast.makeText(BannedShieldedUserListAdapter.this.activity, BannedShieldedUserListAdapter.this.resource.getStringId("mc_forum_cancel_shielded_succ"), 1).show();
                        BannedShieldedUserListAdapter.this.userInfoList.remove(currentPos);
                        BannedShieldedUserListAdapter.this.notifyDataSetChanged();
                    }
                } else if (BannedShieldedUserListAdapter.this.type == 4) {
                    Intent intent = new Intent(BannedShieldedUserListAdapter.this.activity, UserBannedActivity.class);
                    intent.putExtra(MCConstant.BANNED_TYPE, 4);
                    intent.putExtra(MCConstant.BANNED_USER_ID, userInfoModel.getUserId());
                    intent.putExtra("boardId", -1L);
                    BannedShieldedUserListAdapter.this.activity.startActivity(intent);
                } else if (BannedShieldedUserListAdapter.this.type == 1) {
                    Intent intent2 = new Intent(BannedShieldedUserListAdapter.this.activity, UserShieldedActivity.class);
                    intent2.putExtra(MCConstant.SHIELDED_TYPE, 1);
                    intent2.putExtra("shieldedUserId", userInfoModel.getUserId());
                    intent2.putExtra("boardId", -1L);
                    BannedShieldedUserListAdapter.this.activity.startActivity(intent2);
                }
            }
        });
        return convertView2;
    }

    private View getBannedShieldedConvertView(View convertView) {
        BannedShieldedUserAdapterHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_banned_shielded_manage_item"), (ViewGroup) null);
            holder = new BannedShieldedUserAdapterHolder();
            initBannedShieldedUserAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (BannedShieldedUserAdapterHolder) convertView.getTag();
        }
        if (holder != null) {
            return convertView;
        }
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_user_banned_shielded_item"), (ViewGroup) null);
        BannedShieldedUserAdapterHolder holder2 = new BannedShieldedUserAdapterHolder();
        initBannedShieldedUserAdapterHolder(convertView2, holder2);
        convertView2.setTag(holder2);
        return convertView2;
    }

    private void initBannedShieldedUserAdapterHolder(View convertView, BannedShieldedUserAdapterHolder holder) {
        holder.setUserIconImage((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_user_icon_img")));
        holder.setUserNameText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_user_name_text")));
        holder.setChatBtn((Button) convertView.findViewById(this.resource.getViewId("mc_forum_chat_user_btn")));
        holder.setCancelBannedBtn((Button) convertView.findViewById(this.resource.getViewId("mc_forum_banned_cancel_btn")));
        holder.setCancelShieldedBtn((Button) convertView.findViewById(this.resource.getViewId("mc_forum_shielded_cancel_btn")));
        holder.setReasonTitleText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_reason_title_text")));
        holder.setReasonText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_reason_text")));
        holder.setTimeTitleText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_time_title_text")));
        holder.setTimeText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_time_text")));
        holder.setBoardTitleText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_title_text")));
        holder.setBoardText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_text")));
    }

    private void updateUserIconImage(String imgUrl, final View convertView, ImageView userIconImg) {
        if (SharedPreferencesDB.getInstance(this.activity).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(AsyncTaskLoaderImage.formatUrl(imgUrl, "100x100"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    if (image != null) {
                        BannedShieldedUserListAdapter.this.mHandler.post(new Runnable() {
                            public void run() {
                                ImageView imageViewByTag = (ImageView) convertView.findViewById(BannedShieldedUserListAdapter.this.resource.getViewId("mc_forum_user_icon_img"));
                                if (image != null && !image.isRecycled()) {
                                    imageViewByTag.setBackgroundDrawable(new BitmapDrawable(image));
                                }
                            }
                        });
                    }
                }
            });
        }
    }
}
