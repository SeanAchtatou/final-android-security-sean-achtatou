package com.mobcent.base.android.ui.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.adapter.ReportManageAdapter;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.model.ReportModel;
import com.mobcent.forum.android.service.ReportService;
import com.mobcent.forum.android.service.impl.ReportServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.util.List;

public abstract class BaseReportManageActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public ReportManageAdapter adapter;
    private Button backBtn;
    /* access modifiers changed from: private */
    public MoreTask moreTask;
    /* access modifiers changed from: private */
    public int page = 1;
    /* access modifiers changed from: private */
    public int pageSize = 25;
    /* access modifiers changed from: private */
    public PullToRefreshListView refreshListView;
    /* access modifiers changed from: private */
    public RefreshTask refreshTask;
    protected ReportService reportService;
    protected TextView titleText;

    /* access modifiers changed from: protected */
    public abstract List<ReportModel> getReportManageList(int i, int i2);

    static /* synthetic */ int access$408(BaseReportManageActivity x0) {
        int i = x0.page;
        x0.page = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.refreshListView.onRefreshWithOutListener();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.reportService = new ReportServiceImpl(this);
        this.adapter = new ReportManageAdapter(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_report_topic_manage_activity"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.titleText = (TextView) findViewById(this.resource.getViewId("mc_forum_report_topic_manage"));
        this.refreshListView = (PullToRefreshListView) findViewById(this.resource.getViewId("mc_forum_lv"));
        this.refreshListView.setAdapter((ListAdapter) this.adapter);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BaseReportManageActivity.this.back();
            }
        });
        this.refreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                if (BaseReportManageActivity.this.refreshTask != null) {
                    BaseReportManageActivity.this.refreshTask.cancel(true);
                }
                RefreshTask unused = BaseReportManageActivity.this.refreshTask = new RefreshTask();
                BaseReportManageActivity.this.refreshTask.execute(new Void[0]);
            }
        });
        this.refreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                if (BaseReportManageActivity.this.moreTask != null) {
                    BaseReportManageActivity.this.moreTask.cancel(true);
                }
                MoreTask unused = BaseReportManageActivity.this.moreTask = new MoreTask();
                BaseReportManageActivity.this.moreTask.execute(new Void[0]);
            }
        });
        this.refreshListView.onRefresh();
    }

    private class RefreshTask extends AsyncTask<Void, Void, List<ReportModel>> {
        private RefreshTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<ReportModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public List<ReportModel> doInBackground(Void... params) {
            return BaseReportManageActivity.this.getReportManageList(BaseReportManageActivity.this.page, BaseReportManageActivity.this.pageSize);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<ReportModel> result) {
            super.onPostExecute((Object) result);
            BaseReportManageActivity.this.refreshListView.onRefreshComplete();
            BaseReportManageActivity.this.refreshListView.setSelection(0);
            if (result == null || result.size() <= 0) {
                BaseReportManageActivity.this.refreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                Toast.makeText(BaseReportManageActivity.this, MCForumErrorUtil.convertErrorCode(BaseReportManageActivity.this, result.get(0).getErrorCode()), 0).show();
                BaseReportManageActivity.this.refreshListView.onBottomRefreshComplete(3);
            } else {
                BaseReportManageActivity.this.adapter.setReportList(result);
                BaseReportManageActivity.this.adapter.notifyDataSetInvalidated();
                BaseReportManageActivity.this.adapter.notifyDataSetChanged();
                if (result.get(0).getHasNext() == 1) {
                    BaseReportManageActivity.this.refreshListView.onBottomRefreshComplete(0);
                } else {
                    BaseReportManageActivity.this.refreshListView.onBottomRefreshComplete(3);
                }
            }
        }
    }

    private class MoreTask extends AsyncTask<Void, Void, List<ReportModel>> {
        private MoreTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<ReportModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            BaseReportManageActivity.access$408(BaseReportManageActivity.this);
        }

        /* access modifiers changed from: protected */
        public List<ReportModel> doInBackground(Void... params) {
            return BaseReportManageActivity.this.getReportManageList(BaseReportManageActivity.this.page, BaseReportManageActivity.this.pageSize);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<ReportModel> result) {
            super.onPostExecute((Object) result);
            if (result == null || result.size() <= 0) {
                BaseReportManageActivity.this.refreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                Toast.makeText(BaseReportManageActivity.this, MCForumErrorUtil.convertErrorCode(BaseReportManageActivity.this, result.get(0).getErrorCode()), 0).show();
                BaseReportManageActivity.this.refreshListView.onBottomRefreshComplete(3);
            } else {
                BaseReportManageActivity.this.adapter.setReportList(result);
                BaseReportManageActivity.this.adapter.notifyDataSetInvalidated();
                BaseReportManageActivity.this.adapter.notifyDataSetChanged();
                if (result.get(0).getHasNext() == 1) {
                    BaseReportManageActivity.this.refreshListView.onBottomRefreshComplete(0);
                } else {
                    BaseReportManageActivity.this.refreshListView.onBottomRefreshComplete(3);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.refreshTask != null) {
            this.refreshTask.cancel(true);
        }
        if (this.moreTask != null) {
            this.moreTask.cancel(true);
        }
        this.adapter.destory();
    }
}
