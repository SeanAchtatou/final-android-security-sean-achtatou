package com.mobcent.base.android.ui.activity.asyncTaskLoader;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.service.ModeratorService;
import com.mobcent.forum.android.service.impl.ModeratorServiceImpl;

public class ModeratorAsyncTask extends AsyncTask<Object, Void, String> {
    private String boardId;
    private Context context;
    private long moderator;
    private long moderatorUserId;
    private MCResource resource;
    private TaskExecuteDelegate taskExecuteDelegate;
    private long topicId;

    public ModeratorAsyncTask(Context context2, MCResource resource2, long isModerator, long userId, String boardId2, long topicId2) {
        this.context = context2;
        this.resource = resource2;
        this.moderator = isModerator;
        this.moderatorUserId = userId;
        this.boardId = boardId2;
        this.topicId = topicId2;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Object... params) {
        ModeratorService moderatorService = new ModeratorServiceImpl(this.context.getApplicationContext());
        if (this.moderator == 2) {
            return moderatorService.addModerator(this.moderatorUserId, this.boardId + "", this.topicId);
        }
        if (this.moderator == 4) {
            return moderatorService.delModerator(this.moderatorUserId, this.boardId + "", this.topicId);
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String result) {
        if (result != null) {
            Toast.makeText(this.context, MCForumErrorUtil.convertErrorCode(this.context, result), 0).show();
            if (this.taskExecuteDelegate != null) {
                this.taskExecuteDelegate.executeFail();
                return;
            }
            return;
        }
        if (this.moderator == 2) {
            Toast.makeText(this.context, this.resource.getStringId("mc_forum_moderator_succ"), 0).show();
        } else {
            Toast.makeText(this.context, this.resource.getStringId("mc_forum_cancel_moderator"), 0).show();
        }
        if (this.taskExecuteDelegate != null) {
            this.taskExecuteDelegate.executeSuccess();
        }
    }

    public TaskExecuteDelegate getTaskExecuteDelegate() {
        return this.taskExecuteDelegate;
    }

    public void setTaskExecuteDelegate(TaskExecuteDelegate taskExecuteDelegate2) {
        this.taskExecuteDelegate = taskExecuteDelegate2;
    }
}
