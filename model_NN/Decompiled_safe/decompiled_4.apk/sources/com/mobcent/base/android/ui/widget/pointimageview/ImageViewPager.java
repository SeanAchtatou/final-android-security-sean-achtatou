package com.mobcent.base.android.ui.widget.pointimageview;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class ImageViewPager extends ViewPager {
    private boolean isScroll = true;

    public boolean isScroll() {
        return this.isScroll;
    }

    public void setScroll(boolean isScroll2) {
        this.isScroll = isScroll2;
    }

    public ImageViewPager(Context context) {
        super(context);
    }

    public ImageViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean onTouchEvent(MotionEvent ev) {
        try {
            if (isScroll()) {
                return super.onTouchEvent(ev);
            }
            return true;
        } catch (Exception e) {
            return true;
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        try {
            if (isScroll()) {
                return super.onInterceptTouchEvent(event);
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
