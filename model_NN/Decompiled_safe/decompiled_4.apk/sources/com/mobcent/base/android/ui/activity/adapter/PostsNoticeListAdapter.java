package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.ad.android.model.AdModel;
import com.mobcent.ad.android.ui.widget.AdView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.holder.PostsNoticeAdapterHolder;
import com.mobcent.base.android.ui.activity.fragment.AtReplyMessageFragment;
import com.mobcent.base.android.ui.activity.fragment.ReplyMessageFragment;
import com.mobcent.base.forum.android.util.ImageCache;
import com.mobcent.base.forum.android.util.MCFaceUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.PostsNoticeModel;
import com.mobcent.forum.android.model.TopicContentModel;
import com.mobcent.forum.android.util.DateUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.HashMap;
import java.util.List;

public class PostsNoticeListAdapter extends BaseSoundListAdapter implements MCConstant {
    private String TAG = "MessageFragment";
    private int adBottomPosition;
    private HashMap<Integer, List<AdModel>> adHashMap;
    private int adPosition;
    private Fragment fragment;
    /* access modifiers changed from: private */
    public boolean isNeedRefresh = false;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private List<PostsNoticeModel> noticeList;
    private int page;
    /* access modifiers changed from: private */
    public long pageFrom;
    /* access modifiers changed from: private */
    public PostsNoticeClickListener postsNoticeClickListener;

    public interface PostsNoticeClickListener {
        void onPostsNoticeClick(View view, PostsNoticeModel postsNoticeModel);

        void onReplyNoticeClick(View view, PostsNoticeModel postsNoticeModel, long j);

        void onUserHomeClick(View view, PostsNoticeModel postsNoticeModel);
    }

    public PostsNoticeClickListener getPostsNoticeClickListener() {
        return this.postsNoticeClickListener;
    }

    public void setPostsNoticeClickListener(PostsNoticeClickListener postsNoticeClickListener2) {
        this.postsNoticeClickListener = postsNoticeClickListener2;
    }

    public PostsNoticeListAdapter(Context context, List<PostsNoticeModel> noticeList2, Handler mHandler2, LayoutInflater inflater, int adPosition2, Fragment fragment2, int adBottomPosition2, int page2) {
        super(context, fragment2.toString(), inflater);
        this.noticeList = noticeList2;
        this.mHandler = mHandler2;
        this.adHashMap = new HashMap<>();
        this.fragment = fragment2;
        this.adPosition = adPosition2;
        this.page = page2;
        this.adBottomPosition = adBottomPosition2;
    }

    public HashMap<Integer, List<AdModel>> getAdHashMap() {
        return this.adHashMap;
    }

    public void setAdHashMap(HashMap<Integer, List<AdModel>> adHashMap2) {
        this.adHashMap = adHashMap2;
    }

    public int getCount() {
        return this.noticeList.size();
    }

    public PostsNoticeModel getItem(int position) {
        return this.noticeList.get(position);
    }

    public long getItemId(int position) {
        return this.noticeList.get(position).getReplyRemindId();
    }

    public List<PostsNoticeModel> getNoticeList() {
        return this.noticeList;
    }

    public void setNoticeList(List<PostsNoticeModel> noticeList2) {
        this.noticeList = noticeList2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        PostsNoticeAdapterHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_posts_notice_item"), (ViewGroup) null);
            holder = new PostsNoticeAdapterHolder();
            initPostsNoticeAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (PostsNoticeAdapterHolder) convertView.getTag();
        }
        if (holder == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_posts_notice_item"), (ViewGroup) null);
            holder = new PostsNoticeAdapterHolder();
            initPostsNoticeAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        }
        PostsNoticeModel model = getItem(position);
        holder.getAdTopView().free();
        holder.getAdView().free();
        if (position == 0) {
            holder.getAdTopView().setVisibility(0);
            holder.getAdTopView().showAd(this.TAG, this.adPosition, position);
        } else if (position == (((this.page - 1) * 20) + position) - 1) {
            holder.getAdView().setVisibility(0);
            holder.getAdView().showAd(this.TAG, this.adBottomPosition, position);
        }
        updateView(model, convertView, holder);
        if (this.fragment instanceof ReplyMessageFragment) {
            this.pageFrom = 1;
        } else if (this.fragment instanceof AtReplyMessageFragment) {
            this.pageFrom = 2;
        }
        return convertView;
    }

    private void initPostsNoticeAdapterHolder(View convertView, PostsNoticeAdapterHolder holder) {
        holder.setIconImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_user_icon_img")));
        holder.setNoticeSubjectText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_notice_subject_text")));
        holder.setNoticeUserText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_notice_user_text")));
        holder.setNoticeTimeText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_notice_time_text")));
        holder.setNoticeContentText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_notice_content_text")));
        holder.setQuoteContentText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_notice_quote_content_text")));
        holder.setNoticeCheckBtn((Button) convertView.findViewById(this.resource.getViewId("mc_forum_notice_check_btn")));
        holder.setNoticeReplyBtn1((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_notice_reply_btn1")));
        holder.setNoticeSelectImgBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_notice_select_img_btn")));
        holder.setNoticeSelectFaceBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_notice_select_face_btn")));
        holder.setReplyEdit((EditText) convertView.findViewById(this.resource.getViewId("mc_forum_reply_edit")));
        holder.setNoticeReplyBtn2((Button) convertView.findViewById(this.resource.getViewId("mc_forum_notice_reply_btn2")));
        holder.setNoticeDetailLayout((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_notice_detail_layout")));
        holder.setNoticeMsgNew((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_notice_msg_new")));
        holder.setNoticeMsgUnreply((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_notice_msg_unreply")));
        holder.setNoticeMsgReplyed((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_notice_msg_replyed")));
        holder.setReplytext((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_posts_notice_some_reply_text")));
        holder.setNoticeContentLayout((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_notice_content_layout")));
        holder.setAdView((AdView) convertView.findViewById(this.resource.getViewId("mc_ad_box")));
        holder.setAdTopView((AdView) convertView.findViewById(this.resource.getViewId("mc_ad_top_box")));
        holder.setNoticeItemBox((RelativeLayout) convertView.findViewById(this.resource.getViewId("mc_forum_posts_notices_item_box")));
    }

    private void updateView(final PostsNoticeModel model, View convertView, PostsNoticeAdapterHolder holder) {
        holder.getNoticeSubjectText().setText(model.getTopicSubjtect());
        holder.getNoticeUserText().setText(model.getReplyNickName());
        holder.getNoticeTimeText().setText(DateUtil.getFormatTime(model.getReplyDate()));
        if (this.fragment instanceof AtReplyMessageFragment) {
            holder.getReplytext().setText(this.resource.getStringId("mc_forum_posts_at_notice_some_reply"));
        }
        List<TopicContentModel> replyContentList = model.getReplyContentList();
        if (replyContentList == null || replyContentList.size() <= 0) {
            holder.getNoticeContentText().setVisibility(0);
            holder.getNoticeContentLayout().setVisibility(8);
            holder.getNoticeContentText().setText(model.getReplyContent());
        } else {
            holder.getNoticeContentText().setVisibility(8);
            holder.getNoticeContentLayout().setVisibility(0);
            updatePostsDetailView(model.getReplyContentList(), holder.getNoticeContentLayout());
        }
        if (StringUtil.isEmpty(model.getQuoteContent())) {
            holder.getQuoteContentText().setVisibility(8);
        } else {
            holder.getQuoteContentText().setVisibility(0);
            holder.getQuoteContentText().setText(model.getQuoteContent());
            MCFaceUtil.setStrToFace(holder.getQuoteContentText(), model.getQuoteContent(), this.context);
        }
        holder.getIconImg().setVisibility(0);
        holder.getIconImg().setBackgroundResource(this.resource.getDrawableId("mc_forum_head"));
        if (!StringUtil.isEmpty(model.getIcon())) {
            updatePostNoticeImageView(holder.getIconImg(), model.getIconUrl() + model.getIcon());
        } else {
            holder.getIconImg().setBackgroundResource(this.resource.getDrawableId("mc_forum_head"));
        }
        if (model.getIsRead() == 0) {
            holder.getNoticeMsgNew().setVisibility(0);
            holder.getNoticeMsgUnreply().setVisibility(8);
            holder.getNoticeMsgReplyed().setVisibility(8);
        } else if (model.getIsReply() == 0) {
            holder.getNoticeMsgUnreply().setVisibility(0);
            holder.getNoticeMsgNew().setVisibility(8);
            holder.getNoticeMsgReplyed().setVisibility(8);
        } else {
            holder.getNoticeMsgReplyed().setVisibility(0);
            holder.getNoticeMsgNew().setVisibility(8);
            holder.getNoticeMsgUnreply().setVisibility(8);
        }
        holder.getNoticeCheckBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PostsNoticeListAdapter.this.postsNoticeClickListener.onPostsNoticeClick(v, model);
            }
        });
        holder.getNoticeReplyBtn1().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean unused = PostsNoticeListAdapter.this.isNeedRefresh = true;
                if (PostsNoticeListAdapter.this.getPostsNoticeClickListener() != null) {
                    PostsNoticeListAdapter.this.postsNoticeClickListener.onReplyNoticeClick(v, model, PostsNoticeListAdapter.this.pageFrom);
                }
            }
        });
        holder.getIconImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PostsNoticeListAdapter.this.getPostsNoticeClickListener() != null) {
                    PostsNoticeListAdapter.this.postsNoticeClickListener.onUserHomeClick(v, model);
                }
            }
        });
        holder.getNoticeSelectImgBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        holder.getNoticeSelectFaceBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        holder.getNoticeReplyBtn2().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
    }

    private LinearLayout updatePostsDetailView(List<TopicContentModel> topicContentList, LinearLayout topicContentLayout) {
        if (topicContentList != null && topicContentList.size() > 0) {
            topicContentLayout.setVisibility(0);
            topicContentLayout.removeAllViews();
            int j = topicContentList.size();
            for (int i = 0; i < j; i++) {
                TopicContentModel topicContent = topicContentList.get(i);
                View view = null;
                if (topicContent.getType() == 0) {
                    view = this.inflater.inflate(this.resource.getLayoutId("mc_forum_posts_topic_text_item"), (ViewGroup) null);
                    TextView topicInfoText = (TextView) view.findViewById(this.resource.getViewId("mc_forum_topic_info_text"));
                    topicInfoText.setText(topicContent.getInfor());
                    MCFaceUtil.setStrToFace(topicInfoText, topicContent.getInfor(), this.context);
                } else if (topicContent.getType() == 1) {
                    view = this.inflater.inflate(this.resource.getLayoutId("mc_forum_posts_topic_img_item"), (ViewGroup) null);
                    ImageView topicInfoImg = (ImageView) view.findViewById(this.resource.getViewId("mc_forum_topic_info_img"));
                    topicInfoImg.setImageResource(this.resource.getDrawableId("mc_forum_x_img"));
                    String imageUrl = topicContent.getBaseUrl() + topicContent.getInfor();
                    if (!StringUtil.isEmpty(topicContent.getInfor())) {
                        updateTopicContentImage(imageUrl, topicInfoImg);
                    } else {
                        topicInfoImg.setImageResource(this.resource.getDrawableId("mc_forum_x_img"));
                    }
                } else if (topicContent.getType() == 5) {
                    view = getSoundView(topicContent.getSoundModel());
                }
                if (view != null) {
                    topicContentLayout.addView(view);
                }
            }
            topicContentLayout.setVisibility(0);
        }
        return topicContentLayout;
    }

    private void updateTopicContentImage(String imgUrl, final ImageView imageView) {
        if (SharedPreferencesDB.getInstance(this.context).getPicModeFlag()) {
            ImageCache.getInstance(this.context).loadAsync(ImageCache.formatUrl(imgUrl, "320x480"), new ImageCache.ImageCallback() {
                public void onImageLoaded(final Drawable image, String url) {
                    PostsNoticeListAdapter.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (image != null) {
                                imageView.setImageDrawable(image);
                            }
                        }
                    });
                }
            });
        }
    }

    public boolean isNeedRefresh() {
        return this.isNeedRefresh;
    }

    public void setNeedRefresh(boolean isNeedRefresh2) {
        this.isNeedRefresh = isNeedRefresh2;
    }

    private void updatePostNoticeImageView(final ImageView topicUserImg, String imgUrl) {
        if (SharedPreferencesDB.getInstance(this.context).getPicModeFlag()) {
            ImageCache.getInstance(this.context).loadAsync(ImageCache.formatUrl(imgUrl, "100x100"), new ImageCache.ImageCallback() {
                public void onImageLoaded(final Drawable image, String url) {
                    PostsNoticeListAdapter.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (image != null) {
                                topicUserImg.setBackgroundDrawable(image);
                            } else {
                                topicUserImg.setBackgroundResource(PostsNoticeListAdapter.this.resource.getDrawableId("mc_forum_head"));
                            }
                        }
                    });
                }
            });
        }
    }
}
