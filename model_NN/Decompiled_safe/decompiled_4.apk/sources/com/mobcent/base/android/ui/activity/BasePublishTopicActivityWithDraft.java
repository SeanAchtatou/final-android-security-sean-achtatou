package com.mobcent.base.android.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.db.TopicDraftDBUtil;
import com.mobcent.forum.android.model.TopicDraftModel;
import com.mobcent.forum.android.util.ImageUtil;
import com.mobcent.forum.android.util.MCLibIOUtil;
import java.io.File;
import java.lang.ref.WeakReference;

public abstract class BasePublishTopicActivityWithDraft extends BasePublishWithFriendActivity implements MCConstant {
    protected long draftId = 0;

    /* access modifiers changed from: protected */
    public abstract void restoreOtherViewFromDraft(TopicDraftModel topicDraftModel);

    /* access modifiers changed from: protected */
    public abstract TopicDraftModel saveOtherDataToDraft(TopicDraftModel topicDraftModel);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void exitSaveEvent() {
        saveAsDraft(this.draftId, new TopicDraftModel());
        finish();
    }

    /* access modifiers changed from: protected */
    public void saveAsDraft(long id, TopicDraftModel draft) {
        Bitmap bitmap;
        String content = this.contentEdit.getText().toString();
        String title = this.titleEdit.getText().toString();
        draft.setId(id);
        draft.setTitle(title);
        draft.setContent(content);
        String basePath = MCLibIOUtil.getImageCachePath(this) + MCConstant.PUBLISH_POSITION_DIR;
        for (String imgPath : this.imageCache.keySet()) {
            WeakReference<Bitmap> weakReference = (WeakReference) this.imageCache.get(imgPath);
            if (!(weakReference == null || (bitmap = (Bitmap) weakReference.get()) == null || bitmap.isRecycled())) {
                MCLibIOUtil.saveImageFile(this, bitmap, Bitmap.CompressFormat.JPEG, imgPath, basePath);
            }
        }
        SharedPreferencesDB.getInstance(this).saveTopicDraftAudioTime(this.audioTempFileName, this.audioDuration);
        TopicDraftDBUtil.getInstance(this).updateDraft(saveOtherDataToDraft(draft));
    }

    /* access modifiers changed from: protected */
    public void getDraft(final long draftId2) {
        final TopicDraftModel draft = TopicDraftDBUtil.getInstance(this).getTopicDraft(draftId2);
        if (draft != null) {
            AlertDialog.Builder draftAlert = new AlertDialog.Builder(this).setTitle(this.resource.getStringId("mc_forum_dialog_tip")).setMessage(this.resource.getStringId("mc_forum_warn_load_draft"));
            draftAlert.setPositiveButton(this.resource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    BasePublishTopicActivityWithDraft.this.restoreView(draft);
                }
            });
            draftAlert.setNegativeButton(this.resource.getStringId("mc_forum_dialog_cancel"), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    BasePublishTopicActivityWithDraft.this.deleteDraft(draftId2);
                }
            });
            draftAlert.show();
        }
    }

    /* access modifiers changed from: protected */
    public void deleteDraft(long id) {
        TopicDraftDBUtil.getInstance(this).deleteDraft(id);
    }

    /* access modifiers changed from: protected */
    public void restoreView(TopicDraftModel draft) {
        this.titleEdit.setText(draft.getTitle());
        String[] s = draft.getContent().split("ß");
        for (int i = 0; i < s.length; i++) {
            if (s[i].indexOf(225) > -1) {
                String imgPath = s[i].substring(1, s[i].length());
                Bitmap tBitmap = ImageUtil.getBitmapFromMedia(this, MCLibIOUtil.getImageCachePath(this) + MCConstant.PUBLISH_POSITION_DIR + imgPath);
                if (tBitmap == null) {
                    tBitmap = BitmapFactory.decodeResource(getResources(), this.resource.getDrawableId("mc_forum_x_img3"));
                }
                updateUIAfterUpload(imgPath, tBitmap);
            } else {
                getContentEditText().getText().insert(getContentEditText().getSelectionEnd(), s[i]);
            }
        }
        this.recordAbsolutePath = getRecordAbsolutePath();
        if (new File(this.recordAbsolutePath).exists()) {
            this.audioDuration = SharedPreferencesDB.getInstance(this).getTopicDraftAudioTime(this.audioTempFileName);
            this.hasAudio = true;
            updatePlay();
        }
        restoreOtherViewFromDraft(draft);
    }
}
