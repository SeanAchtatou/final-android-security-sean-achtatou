package com.mobcent.base.android.ui.activity.adapter;

import java.util.List;

public class WheelListAdapter implements WheelAdapter {
    private List<String> list;

    public WheelListAdapter(List<String> list2) {
        this.list = list2;
    }

    public int getItemsCount() {
        return this.list.size();
    }

    public String getItem(int index) {
        return this.list.get(index);
    }

    public int getMaximumLength() {
        return this.list.size();
    }
}
