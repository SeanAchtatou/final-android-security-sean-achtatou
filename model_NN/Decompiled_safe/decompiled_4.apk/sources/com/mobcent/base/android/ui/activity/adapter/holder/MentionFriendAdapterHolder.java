package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.TextView;

public class MentionFriendAdapterHolder {
    private TextView nameText;

    public TextView getNameText() {
        return this.nameText;
    }

    public void setNameText(TextView nameText2) {
        this.nameText = nameText2;
    }
}
