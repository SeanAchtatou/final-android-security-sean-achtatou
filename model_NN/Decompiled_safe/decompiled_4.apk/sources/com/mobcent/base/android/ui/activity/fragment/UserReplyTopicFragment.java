package com.mobcent.base.android.ui.activity.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.TopicListAdapter;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import java.util.List;

public class UserReplyTopicFragment extends UserGenericTopicFragment implements MCConstant {
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initSpecialViews() {
        this.titleText.setText(this.mcResource.getStringId("mc_forum_user_reply_posts"));
        if (!(this.userId == new UserServiceImpl(this.activity).getLoginUserId() || this.nickName == null)) {
            this.titleText.setText(this.nickName);
        }
        this.topicListAdapter = new TopicListAdapter(this.activity, this.topicModelList, "", this.mHandler, this.inflater, this.asyncTaskLoaderImage, getadPostion("mc_forum_user_joined_position"), this.postsClickListener, getadPostion("mc_forum_user_joined_position_bottom"), this.currentPage);
    }

    /* access modifiers changed from: protected */
    public List<TopicModel> getGenericTopicModelList(int page, int pageSize) {
        return this.postsService.getUserReplyList(this.userId, page, pageSize);
    }
}
