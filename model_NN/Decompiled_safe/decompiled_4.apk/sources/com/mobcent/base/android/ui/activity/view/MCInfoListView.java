package com.mobcent.base.android.ui.activity.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.util.MCLogUtil;

public class MCInfoListView extends AdapterView<Adapter> {
    private int layoutHeight;
    private int layoutWidth;
    private Adapter mAdapter;

    public MCInfoListView(Context context) {
        super(context);
        init(context);
    }

    public MCInfoListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MCInfoListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        this.layoutWidth = getContext().getResources().getDimensionPixelSize(MCResource.getInstance(getContext()).getDimenId("mc_forum_info_layout_width"));
    }

    public Adapter getAdapter() {
        return this.mAdapter;
    }

    public void setAdapter(Adapter adapter) {
        this.mAdapter = adapter;
        if (this.mAdapter != null && this.mAdapter.getCount() != 0) {
            int size = this.mAdapter.getCount();
            for (int i = 0; i < size; i++) {
                makeAndAddView(i, null);
            }
        }
    }

    public View getSelectedView() {
        return null;
    }

    public void setSelection(int position) {
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        int childTop = 0;
        int count = getChildCount();
        MCLogUtil.i("MCInfoListView", "count = " + count);
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() != 8) {
                int childHeight = child.getMeasuredHeight();
                MCLogUtil.i("MCInfoListView", "childHeight = " + childHeight);
                int childWidth = this.layoutWidth;
                child.layout(0, childTop, childWidth, childTop + childHeight);
                View ccv = ((ViewGroup) child).getChildAt(0);
                if (ccv instanceof TextView) {
                    ccv.getLayoutParams().width = childWidth;
                }
                childTop += childHeight;
            }
        }
        this.layoutHeight = childTop;
        getLayoutParams().height = this.layoutHeight;
        requestLayout();
        MCLogUtil.i("MCInfoListView", "layoutHeight = " + this.layoutHeight);
        MCLogUtil.i("MCInfoListView", "getLayoutParams().height = " + getLayoutParams().height);
    }

    public int getViewsCount() {
        return this.mAdapter.getCount();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int count = getChildCount();
        MCLogUtil.i("MCInfoListView", "onMeasure count = " + count);
        for (int i = 0; i < count; i++) {
            int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
            int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
            MCLogUtil.i("MCInfoListView", "width = " + width);
            MCLogUtil.i("MCInfoListView", "height = " + height);
            getChildAt(i).measure(width, height);
        }
    }

    private View setupChild(View child, int position, boolean recycle) {
        ViewGroup.LayoutParams p = child.getLayoutParams();
        if (p == null) {
            p = new LinearLayout.LayoutParams(-2, -2);
        }
        addViewInLayout(child, position, p, true);
        return child;
    }

    private View makeAndAddView(int position, View convertView) {
        return setupChild(this.mAdapter.getView(position, convertView, this), position, convertView != null);
    }
}
