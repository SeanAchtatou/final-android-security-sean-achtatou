package com.mobcent.base.android.ui.activity.helper;

import com.mobcent.forum.android.model.RichImageModel;
import java.util.List;

public class MCImageViewerHelper {
    private static MCImageViewerHelper helper;
    private ImageViewerListener listener;
    private ImageViewerSizeListener onViewSizeListener;

    public interface ImageViewerListener {
        void onViewerPageSelect(int i);
    }

    public interface ImageViewerSizeListener {
        void onViewerSizeListener(List<RichImageModel> list);
    }

    public static MCImageViewerHelper getInstance() {
        if (helper == null) {
            helper = new MCImageViewerHelper();
        }
        return helper;
    }

    public ImageViewerListener getListener() {
        return this.listener;
    }

    public void setListener(ImageViewerListener listener2) {
        this.listener = listener2;
    }

    public ImageViewerSizeListener getOnViewSizeListener() {
        return this.onViewSizeListener;
    }

    public void setOnViewSizeListener(ImageViewerSizeListener onViewSizeListener2) {
        this.onViewSizeListener = onViewSizeListener2;
    }
}
