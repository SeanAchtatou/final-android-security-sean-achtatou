package com.mobcent.base.android.ui.activity.asyncTaskLoader;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;

public class DeleteTopicTask extends AsyncTask<Object, Void, String> {
    private Context context;
    private TaskExecuteDelegate taskExecuteDelegate;
    private int type;

    public DeleteTopicTask(Context context2, TopicModel topicModel) {
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    /* access modifiers changed from: protected */
    public String doInBackground(Object... params) {
        this.type = ((Integer) params[0]).intValue();
        long boardId = ((Long) params[1]).longValue();
        PostsService postsService = new PostsServiceImpl(this.context);
        if (this.type == 1) {
            return postsService.deleteTopic(boardId, ((Long) params[2]).longValue());
        }
        return postsService.deleteReply(boardId, ((Long) params[2]).longValue());
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String result) {
        if (result != null) {
            this.taskExecuteDelegate.executeFail();
            Toast.makeText(this.context, MCForumErrorUtil.convertErrorCode(this.context, result), 1).show();
            return;
        }
        Toast.makeText(this.context, MCResource.getInstance(this.context).getString("mc_forum_delete_topic_succ"), 1).show();
        this.taskExecuteDelegate.executeSuccess();
    }

    public TaskExecuteDelegate getTaskExecuteDelegate() {
        return this.taskExecuteDelegate;
    }

    public void setTaskExecuteDelegate(TaskExecuteDelegate taskExecuteDelegate2) {
        this.taskExecuteDelegate = taskExecuteDelegate2;
    }
}
