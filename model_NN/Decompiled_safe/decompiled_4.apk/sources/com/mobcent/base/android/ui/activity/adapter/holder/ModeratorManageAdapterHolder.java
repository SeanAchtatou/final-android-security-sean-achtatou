package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ModeratorManageAdapterHolder {
    private TextView boardText;
    private Button followBtn;
    private ImageView iconImg;
    private Button moderatorBtn;
    private TextView userNameText;

    public ImageView getIconImg() {
        return this.iconImg;
    }

    public void setIconImg(ImageView iconImg2) {
        this.iconImg = iconImg2;
    }

    public TextView getUserNameText() {
        return this.userNameText;
    }

    public void setUserNameText(TextView userNameText2) {
        this.userNameText = userNameText2;
    }

    public TextView getBoardText() {
        return this.boardText;
    }

    public void setBoardText(TextView boardText2) {
        this.boardText = boardText2;
    }

    public Button getFollowBtn() {
        return this.followBtn;
    }

    public void setFollowBtn(Button followBtn2) {
        this.followBtn = followBtn2;
    }

    public Button getModeratorBtn() {
        return this.moderatorBtn;
    }

    public void setModeratorBtn(Button moderatorBtn2) {
        this.moderatorBtn = moderatorBtn2;
    }
}
