package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.db.UserJsonDBUtil;
import com.mobcent.forum.android.db.helper.UserJsonDBHelper;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import java.io.Serializable;
import java.util.HashMap;

public class RegSuccActivity extends BasePhotoPreviewActivity {
    private Button cancelSubmitBtn;
    private LinearLayout femaleBox;
    /* access modifiers changed from: private */
    public Button femaleRadio;
    int gender = 0;
    private HashMap<String, Serializable> goParam;
    private Class<?> goToActivityClass;
    private ImageView headerImg;
    private Button localPhotoBtn;
    private LinearLayout maleBox;
    /* access modifiers changed from: private */
    public Button maleRadio;
    private String nickName;
    /* access modifiers changed from: private */
    public EditText nickNameEdit;
    /* access modifiers changed from: private */
    public int nickNameMaxLen = 20;
    /* access modifiers changed from: private */
    public int nickNameMinLen = 3;
    private Button saveInfoSubmitBtn;
    private Button takePhotoBtn;
    /* access modifiers changed from: private */
    public UpdateAsyncTask updateAsyncTask;
    private long userId;
    private UserService userService;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.uploadType = 1;
        Intent intent = getIntent();
        if (intent != null) {
            this.goToActivityClass = (Class) getIntent().getSerializableExtra(MCConstant.TAG);
            this.goParam = (HashMap) intent.getSerializableExtra(MCConstant.GO_PARAM);
        }
        this.userService = new UserServiceImpl(this);
        this.nickName = this.userService.getNickname();
        this.userId = this.userService.getLoginUserId();
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_user_register_succ_activity"));
        super.initViews();
        this.saveInfoSubmitBtn = (Button) findViewById(this.resource.getViewId("mc_forum_user_save_info_submit_btn"));
        this.cancelSubmitBtn = (Button) findViewById(this.resource.getViewId("mc_forum_user_see_anywhere_submit_btn"));
        this.takePhotoBtn = (Button) findViewById(this.resource.getViewId("mc_forum_user_take_photo_btn"));
        this.localPhotoBtn = (Button) findViewById(this.resource.getViewId("mc_forum_user_local_photo_btn"));
        this.nickNameEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_user_input_username_btn"));
        this.maleRadio = (Button) findViewById(this.resource.getViewId("mc_forum_gender_male_btn"));
        this.femaleRadio = (Button) findViewById(this.resource.getViewId("mc_forum_gender_female_btn"));
        this.headerImg = (ImageView) findViewById(this.resource.getViewId("mc_forum_head_img"));
        this.maleBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_gender_male_box"));
        this.femaleBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_gender_female_box"));
        if (StringUtil.isEmail(this.nickName)) {
            this.nickName = this.nickName.substring(0, this.nickName.indexOf("@"));
            this.nickName += this.userId;
            this.nickNameEdit.setText(this.nickName);
        }
        Editable nickEdit = this.nickNameEdit.getText();
        Selection.setSelection(nickEdit, nickEdit.length());
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.saveInfoSubmitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String nickName = RegSuccActivity.this.nickNameEdit.getText().toString();
                if (!StringUtil.checkNickNameMinLen(nickName, RegSuccActivity.this.nickNameMinLen) || !StringUtil.checkNickNameMaxLen(nickName, RegSuccActivity.this.nickNameMaxLen)) {
                    RegSuccActivity.this.warnMessageById("mc_forum_user_nickname_length_error");
                    return;
                }
                UserInfoModel userInfoModel = new UserInfoModel();
                userInfoModel.setNickname(nickName);
                userInfoModel.setGender(RegSuccActivity.this.gender);
                String icon = "";
                if (RegSuccActivity.this.iconPath != null) {
                    icon = RegSuccActivity.this.iconPath;
                }
                userInfoModel.setIcon(icon);
                userInfoModel.setSignature("");
                if (RegSuccActivity.this.updateAsyncTask != null) {
                    RegSuccActivity.this.updateAsyncTask.cancel(true);
                }
                UpdateAsyncTask unused = RegSuccActivity.this.updateAsyncTask = new UpdateAsyncTask();
                RegSuccActivity.this.updateAsyncTask.execute(userInfoModel);
            }
        });
        this.maleBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RegSuccActivity.this.gender = 1;
                RegSuccActivity.this.maleRadio.setBackgroundResource(RegSuccActivity.this.resource.getDrawableId("mc_forum_select2_2"));
                RegSuccActivity.this.femaleRadio.setBackgroundResource(RegSuccActivity.this.resource.getDrawableId("mc_forum_select2_1"));
            }
        });
        this.femaleBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RegSuccActivity.this.gender = 0;
                RegSuccActivity.this.maleRadio.setBackgroundResource(RegSuccActivity.this.resource.getDrawableId("mc_forum_select2_1"));
                RegSuccActivity.this.femaleRadio.setBackgroundResource(RegSuccActivity.this.resource.getDrawableId("mc_forum_select2_2"));
            }
        });
        this.cancelSubmitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RegSuccActivity.this.goToTargetActivity();
                RegSuccActivity.this.finish();
            }
        });
        this.takePhotoBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RegSuccActivity.this.cameraPhotoListener();
            }
        });
        this.localPhotoBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RegSuccActivity.this.localPhotoListener();
            }
        });
        this.nickNameEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66) {
                    ((InputMethodManager) RegSuccActivity.this.getSystemService("input_method")).hideSoftInputFromWindow(RegSuccActivity.this.nickNameEdit.getWindowToken(), 0);
                }
                return false;
            }
        });
    }

    class UpdateAsyncTask extends AsyncTask<UserInfoModel, Void, String> {
        private UserInfoModel model;

        UpdateAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            RegSuccActivity.this.showProgressDialog("mc_forum_warn_update", this);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(UserInfoModel... params) {
            UserService userService = new UserServiceImpl(RegSuccActivity.this);
            if (params[0] == null) {
                return null;
            }
            this.model = params[0];
            UserInfoModel userInfoModel = params[0];
            return userService.updateUser(userInfoModel.getNickname(), userInfoModel.getIcon(), userInfoModel.getGender(), userInfoModel.getSignature(), -1);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            RegSuccActivity.this.hideProgressDialog();
            if (result == null) {
                UserJsonDBUtil.getInstance(RegSuccActivity.this).savePersonalInfo(this.model.getUserId(), 1, UserJsonDBHelper.buildPersonalInfo(this.model));
                RegSuccActivity.this.warnMessageById("mc_forum_user_update_succ");
                RegSuccActivity.this.goToTargetActivity();
                RegSuccActivity.this.finish();
            } else if (!result.equals("")) {
                RegSuccActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(RegSuccActivity.this, result));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void exitNoSaveEvent() {
    }

    /* access modifiers changed from: protected */
    public void exitSaveEvent() {
    }

    /* access modifiers changed from: protected */
    public boolean exitCheckChanged() {
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (backEventClickListener()) {
            goToTargetActivity();
        }
        finish();
        return true;
    }

    /* access modifiers changed from: private */
    public void goToTargetActivity() {
        if (this.goToActivityClass != null) {
            Intent intent = new Intent(this, this.goToActivityClass);
            if (this.goParam != null) {
                for (String key : this.goParam.keySet()) {
                    intent.putExtra(key, this.goParam.get(key));
                }
            }
            startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void updateUIAfterUpload() {
        if (this.bitmap != null && this.uploadSucc && !this.bitmap.isRecycled()) {
            this.headerImg.setBackgroundDrawable(new BitmapDrawable(this.bitmap));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.updateAsyncTask != null) {
            this.updateAsyncTask.cancel(true);
        }
    }
}
