package com.mobcent.base.android.ui.activity.asyncTaskLoader;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.delegate.LongTaskDelegate;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.BaseModel;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.util.StringUtil;

public class LongPicAsynTask extends AsyncTask<String, Void, BaseModel> {
    private Context context;
    private LongTaskDelegate longTaskDelegate;

    public LongPicAsynTask(Context context2) {
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }

    /* access modifiers changed from: protected */
    public BaseModel doInBackground(String... params) {
        return new PostsServiceImpl(this.context).getLongPic(params[0], params[1], params[2]);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(BaseModel result) {
        super.onPostExecute((Object) result);
        if (result != null) {
            if (!StringUtil.isEmpty(result.getErrorCode())) {
                Toast.makeText(this.context, MCForumErrorUtil.convertErrorCode(this.context, result.getErrorCode()), 0).show();
                if (this.longTaskDelegate != null) {
                    this.longTaskDelegate.executeFail();
                }
            } else if (this.longTaskDelegate != null) {
                this.longTaskDelegate.executeSuccess(result.getPathOrContent());
            }
        } else if (this.longTaskDelegate != null) {
            this.longTaskDelegate.executeFail();
        } else {
            Toast.makeText(this.context, MCResource.getInstance(this.context).getString("mc_forum_error_gain_picture_fail"), 0).show();
        }
    }

    public LongTaskDelegate getLongTaskDelegate() {
        return this.longTaskDelegate;
    }

    public void setLongTaskDelegate(LongTaskDelegate longTaskDelegate2) {
        this.longTaskDelegate = longTaskDelegate2;
    }
}
