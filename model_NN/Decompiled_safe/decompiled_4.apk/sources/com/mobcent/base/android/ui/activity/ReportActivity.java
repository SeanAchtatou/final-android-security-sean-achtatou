package com.mobcent.base.android.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.service.ReportService;
import com.mobcent.forum.android.service.impl.ReportServiceImpl;
import com.mobcent.forum.android.util.StringUtil;

public class ReportActivity extends BaseActivity implements MCConstant {
    private TextView adultContentText;
    private Button backBtn;
    /* access modifiers changed from: private */
    public boolean isShowTopicTypeBox = false;
    /* access modifiers changed from: private */
    public long oid;
    private TextView otherReasonText;
    private TextView politySensContentText;
    /* access modifiers changed from: private */
    public String reason;
    /* access modifiers changed from: private */
    public ReportAsyncTask reportAsyncTask;
    private ScrollView reportDetailBox;
    /* access modifiers changed from: private */
    public EditText reportDetailDescriptionEdit;
    /* access modifiers changed from: private */
    public EditText reportReasonEdit;
    private ImageButton reportReasonImgBtn;
    /* access modifiers changed from: private */
    public String reportReasonStr;
    /* access modifiers changed from: private */
    public ReportService reportService;
    private Button reportSubmitBtn;
    private TextView reportTitleText;
    /* access modifiers changed from: private */
    public String sureReportStr = "";
    /* access modifiers changed from: private */
    public RelativeLayout transparentBox;
    /* access modifiers changed from: private */
    public int type;
    private TextView vulgarSpeechText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.reportService = new ReportServiceImpl(this);
        Intent intent = getIntent();
        this.type = intent.getIntExtra(MCConstant.REPORT_TYPE, 0);
        this.oid = intent.getLongExtra("oid", 0);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_report_activity"));
        this.reportTitleText = (TextView) findViewById(this.resource.getViewId("mc_forum_report_text"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.reportDetailBox = (ScrollView) findViewById(this.resource.getViewId("mc_forum_report_detail_box"));
        this.adultContentText = (TextView) findViewById(this.resource.getViewId("mc_forum_report_adult_content"));
        this.politySensContentText = (TextView) findViewById(this.resource.getViewId("mc_forum_polity_sensi_content"));
        this.vulgarSpeechText = (TextView) findViewById(this.resource.getViewId("mc_forum_vulgar_speech"));
        this.otherReasonText = (TextView) findViewById(this.resource.getViewId("mc_forum_other_reason"));
        this.reportReasonEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_report_reason_edit"));
        this.reportDetailDescriptionEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_report_detail_description_edit"));
        this.reportReasonImgBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_report_reason_imgBtn"));
        this.transparentBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_transparent_box"));
        this.reportSubmitBtn = (Button) findViewById(this.resource.getViewId("mc_forum_report_submit_btn"));
        if (this.type != 3) {
            this.reportTitleText.setText(getString(this.resource.getStringId("mc_forum_report_post_str")));
            this.sureReportStr = getString(this.resource.getStringId("mc_forum_sure_report_posts_str"));
            return;
        }
        this.reportTitleText.setText(getString(this.resource.getStringId("mc_forum_report_user_str")));
        this.sureReportStr = getString(this.resource.getStringId("mc_forum_sure_report_user_str"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReportActivity.this.finish();
            }
        });
        this.reportReasonImgBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (!ReportActivity.this.isShowTopicTypeBox) {
                    ReportActivity.this.showdropDownBox(true);
                    boolean unused = ReportActivity.this.isShowTopicTypeBox = true;
                    return;
                }
                ReportActivity.this.showdropDownBox(false);
                boolean unused2 = ReportActivity.this.isShowTopicTypeBox = false;
            }
        });
        this.adultContentText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReportActivity.this.reportReasonEdit.setText(ReportActivity.this.getString(ReportActivity.this.resource.getStringId("mc_forum_report_adult_content")));
                ReportActivity.this.showdropDownBox(false);
                boolean unused = ReportActivity.this.isShowTopicTypeBox = false;
            }
        });
        this.politySensContentText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReportActivity.this.reportReasonEdit.setText(ReportActivity.this.getString(ReportActivity.this.resource.getStringId("mc_forum_polity_sensi_content")));
                ReportActivity.this.showdropDownBox(false);
                boolean unused = ReportActivity.this.isShowTopicTypeBox = false;
            }
        });
        this.vulgarSpeechText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReportActivity.this.reportReasonEdit.setText(ReportActivity.this.getString(ReportActivity.this.resource.getStringId("mc_forum_vulgar_speech")));
                ReportActivity.this.showdropDownBox(false);
                boolean unused = ReportActivity.this.isShowTopicTypeBox = false;
            }
        });
        this.otherReasonText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReportActivity.this.reportReasonEdit.setText(ReportActivity.this.getString(ReportActivity.this.resource.getStringId("mc_forum_other_reason")));
                ReportActivity.this.showdropDownBox(false);
                boolean unused = ReportActivity.this.isShowTopicTypeBox = false;
            }
        });
        this.transparentBox.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (ReportActivity.this.transparentBox.getVisibility() != 0) {
                    return false;
                }
                ReportActivity.this.showdropDownBox(false);
                return false;
            }
        });
        this.reportSubmitBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String unused = ReportActivity.this.reportReasonStr = ReportActivity.this.reportReasonEdit.getText().toString().trim();
                String unused2 = ReportActivity.this.reason = "[" + ReportActivity.this.reportReasonStr + "]" + ReportActivity.this.reportDetailDescriptionEdit.getText().toString().trim();
                if (StringUtil.isEmpty(ReportActivity.this.reportReasonStr)) {
                    Toast.makeText(ReportActivity.this, ReportActivity.this.getString(ReportActivity.this.resource.getStringId("mc_forum_select_report_reason_str")), 1).show();
                } else {
                    new AlertDialog.Builder(ReportActivity.this).setTitle(ReportActivity.this.sureReportStr).setNegativeButton(ReportActivity.this.getString(ReportActivity.this.resource.getStringId("mc_forum_dialog_cancel")), (DialogInterface.OnClickListener) null).setPositiveButton(ReportActivity.this.getString(ReportActivity.this.resource.getStringId("mc_forum_dialog_confirm")), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            if (ReportActivity.this.reportAsyncTask != null) {
                                ReportActivity.this.reportAsyncTask.cancel(true);
                            }
                            ReportAsyncTask unused = ReportActivity.this.reportAsyncTask = new ReportAsyncTask();
                            ReportActivity.this.reportAsyncTask.execute(Integer.valueOf(ReportActivity.this.type), Long.valueOf(ReportActivity.this.oid), ReportActivity.this.reason);
                        }
                    }).show();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void showdropDownBox(boolean isShowTopicTypeBox2) {
        if (this.isShowTopicTypeBox != isShowTopicTypeBox2) {
            this.isShowTopicTypeBox = isShowTopicTypeBox2;
            if (isShowTopicTypeBox2) {
                this.reportDetailBox.setVisibility(0);
                this.transparentBox.setVisibility(0);
                this.reportReasonImgBtn.setBackgroundResource(this.resource.getDrawableId("mc_forum_input_select_d"));
                return;
            }
            this.reportDetailBox.setVisibility(8);
            this.transparentBox.setVisibility(8);
            this.reportReasonImgBtn.setBackgroundResource(this.resource.getDrawableId("mc_forum_input_select_n"));
        }
    }

    private class ReportAsyncTask extends AsyncTask<Object, Void, String> {
        private ReportAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Object... params) {
            int unused = ReportActivity.this.type = ((Integer) params[0]).intValue();
            long unused2 = ReportActivity.this.oid = ((Long) params[1]).longValue();
            String unused3 = ReportActivity.this.reason = (String) params[2];
            return ReportActivity.this.reportService.report(ReportActivity.this.type, ReportActivity.this.oid, ReportActivity.this.reason);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (!StringUtil.isEmpty(result)) {
                ReportActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(ReportActivity.this, result));
            }
            if (result == null) {
                ReportActivity.this.warnMessageById("mc_forum_report_succ");
            }
            ReportActivity.this.finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.reportAsyncTask != null) {
            this.reportAsyncTask.cancel(true);
        }
    }
}
