package com.mobcent.base.android.ui.activity.asyncTaskLoader;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;

public class PhysicalDelTopicAsyncTask extends AsyncTask<Object, Void, String> {
    private Context context;
    private MCResource resource;
    private long topicId;

    public PhysicalDelTopicAsyncTask(Context context2, MCResource resource2, long topicId2) {
        this.context = context2;
        this.topicId = topicId2;
        this.resource = resource2;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Object... params) {
        return new PostsServiceImpl(this.context).PhysicalDelTopic(this.topicId);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String result) {
        if (result != null) {
            Toast.makeText(this.context, MCForumErrorUtil.convertErrorCode(this.context, result), 0).show();
            return;
        }
        ((Activity) this.context).finish();
        Toast.makeText(this.context, this.resource.getString("mc_forum_delete_topic_succ"), 0).show();
    }
}
