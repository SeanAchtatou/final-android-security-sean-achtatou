package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.TextView;

public class BoardAdapterHolder {
    private TextView boardNameText;
    private TextView boardPostsText;
    private TextView boardTimeText;
    private TextView boardTopicTotalText;
    private TextView todayTotalText;

    public TextView getBoardNameText() {
        return this.boardNameText;
    }

    public void setBoardNameText(TextView boardNameText2) {
        this.boardNameText = boardNameText2;
    }

    public TextView getBoardTimeText() {
        return this.boardTimeText;
    }

    public void setBoardTimeText(TextView boardTimeText2) {
        this.boardTimeText = boardTimeText2;
    }

    public TextView getBoardTopicTotalText() {
        return this.boardTopicTotalText;
    }

    public void setBoardTopicTotalText(TextView boardTopicTotalText2) {
        this.boardTopicTotalText = boardTopicTotalText2;
    }

    public TextView getBoardPostsText() {
        return this.boardPostsText;
    }

    public void setBoardPostsText(TextView boardPostsText2) {
        this.boardPostsText = boardPostsText2;
    }

    public TextView getTodayTotalText() {
        return this.todayTotalText;
    }

    public void setTodayTotalText(TextView todayTotalText2) {
        this.todayTotalText = todayTotalText2;
    }
}
