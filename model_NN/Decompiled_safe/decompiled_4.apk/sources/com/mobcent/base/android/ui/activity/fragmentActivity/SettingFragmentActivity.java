package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.fragment.SetttingFragment;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.forum.android.db.DBManagerUtil;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.MCLibIOUtil;
import java.io.File;
import java.util.List;

public class SettingFragmentActivity extends BaseFragmentActivity {
    /* access modifiers changed from: private */
    public DBManagerUtil dbManagerUtil;
    /* access modifiers changed from: private */
    public ProgressDialog myDialog;
    private SetttingFragment setttingFragment;

    /* access modifiers changed from: protected */
    public void initData() {
        this.fragmentManager = getSupportFragmentManager();
        this.dbManagerUtil = new DBManagerUtil();
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_settting_activity"));
        this.setttingFragment = (SetttingFragment) this.fragmentManager.findFragmentById(this.resource.getViewId("mc_forum_fragment"));
        this.setttingFragment.setmHandler(this.mHandler);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.setttingFragment.setClearCacheClickListener(new SetttingFragment.ClearCacheClickListener() {
            public void onClearCacheClick(View v) {
                new AlertDialog.Builder(SettingFragmentActivity.this).setMessage(SettingFragmentActivity.this.getResources().getString(SettingFragmentActivity.this.resource.getStringId("mc_forum_confirm_clear_cache"))).setNegativeButton(SettingFragmentActivity.this.getResources().getString(SettingFragmentActivity.this.resource.getStringId("mc_forum_dialog_cancel")), (DialogInterface.OnClickListener) null).setPositiveButton(SettingFragmentActivity.this.getResources().getString(SettingFragmentActivity.this.resource.getStringId("mc_forum_dialog_confirm")), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        new ClearCacheAsyncTask().execute(new Void[0]);
                    }
                }).show();
            }

            public void onUserLogoutClick(View view) {
                SettingFragmentActivity.this.clearNotification();
                new UserServiceImpl(SettingFragmentActivity.this).logout();
                MCForumHelper.onLogoutClick(SettingFragmentActivity.this);
            }
        });
    }

    private class ClearCacheAsyncTask extends AsyncTask<Void, String, String> {
        private ClearCacheAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            ProgressDialog unused = SettingFragmentActivity.this.myDialog = ProgressDialog.show(SettingFragmentActivity.this, null, SettingFragmentActivity.this.getResources().getString(SettingFragmentActivity.this.resource.getStringId("mc_forum_clear_cache_proess")), true);
            SettingFragmentActivity.this.myDialog.show();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... params) {
            boolean unused = SettingFragmentActivity.deleteDir(MCLibIOUtil.getImageCachePathByApp(SettingFragmentActivity.this));
            boolean unused2 = SettingFragmentActivity.deleteDir(MCLibIOUtil.getDownPath(SettingFragmentActivity.this));
            boolean unused3 = SettingFragmentActivity.deleteDir(MCLibIOUtil.getCachePath(SettingFragmentActivity.this));
            MCLibIOUtil.getImageCachePath(SettingFragmentActivity.this);
            MCLibIOUtil.getDownPath(SettingFragmentActivity.this);
            MCLibIOUtil.getCachePath(SettingFragmentActivity.this);
            MCLibIOUtil.getAudioCachePath(SettingFragmentActivity.this);
            return SettingFragmentActivity.this.dbManagerUtil.clearCache(SettingFragmentActivity.this) + "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (result.equals("true")) {
                Toast.makeText(SettingFragmentActivity.this, SettingFragmentActivity.this.getResources().getString(SettingFragmentActivity.this.resource.getStringId("mc_forum_clear_cache_success")), 1).show();
                SettingFragmentActivity.this.myDialog.dismiss();
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean deleteDir(String filePath) {
        File dir = new File(filePath);
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String file : children) {
                if (!deleteDir(new File(dir, file))) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    private static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String file : children) {
                if (!deleteDir(new File(dir, file))) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return null;
    }
}
