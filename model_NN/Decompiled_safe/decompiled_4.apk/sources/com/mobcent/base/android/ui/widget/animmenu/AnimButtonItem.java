package com.mobcent.base.android.ui.widget.animmenu;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

public class AnimButtonItem extends ImageButton {
    private ButtonPoint endPoint;
    private ButtonPoint farPoint;
    private ButtonPoint nearPoint;
    private ButtonPoint startPoint;
    private int viewHeight;

    public AnimButtonItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setBackgroundResource(int resid) {
        super.setBackgroundResource(resid);
        this.viewHeight = getBackground().getIntrinsicHeight();
    }

    public ButtonPoint getStartPoint() {
        return this.startPoint;
    }

    public void setStartPoint(ButtonPoint startPoint2) {
        this.startPoint = startPoint2;
    }

    public ButtonPoint getEndPoint() {
        return this.endPoint;
    }

    public void setEndPoint(ButtonPoint endPoint2) {
        this.endPoint = endPoint2;
    }

    public ButtonPoint getNearPoint() {
        return this.nearPoint;
    }

    public void setNearPoint(ButtonPoint nearPoint2) {
        this.nearPoint = nearPoint2;
    }

    public ButtonPoint getFarPoint() {
        return this.farPoint;
    }

    public void setFarPoint(ButtonPoint farPoint2) {
        this.farPoint = farPoint2;
    }

    public int getViewHeight() {
        return this.viewHeight;
    }
}
