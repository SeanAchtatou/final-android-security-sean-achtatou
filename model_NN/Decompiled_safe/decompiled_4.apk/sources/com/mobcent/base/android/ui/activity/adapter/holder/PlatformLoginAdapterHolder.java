package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.ImageView;

public class PlatformLoginAdapterHolder {
    private ImageView platformImg;

    public ImageView getPlatformImg() {
        return this.platformImg;
    }

    public void setPlatformImg(ImageView platformImg2) {
        this.platformImg = platformImg2;
    }
}
