package com.mobcent.base.android.ui.activity.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.PostsActivity;
import com.mobcent.base.android.ui.activity.ReportTopicManageActivity;
import com.mobcent.base.android.ui.activity.ReportUserManageActivity;
import com.mobcent.base.android.ui.activity.UserShieldedActivity;
import com.mobcent.base.android.ui.activity.adapter.holder.ReportTopicManageAdapterHolder;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.CancelReportTask;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.android.ui.activity.view.MCReportTopicManageBar;
import com.mobcent.base.forum.android.util.MCColorUtil;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCForumReverseList;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.ReportModel;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import java.util.ArrayList;
import java.util.List;

public class ReportManageAdapter extends BaseAdapter {
    /* access modifiers changed from: private */
    public CancelReportTask cancelReportTask;
    /* access modifiers changed from: private */
    public Context context;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public List<ReportModel> reportList = new ArrayList();
    /* access modifiers changed from: private */
    public MCResource resource;
    private DialogInterface.OnClickListener topicFunctionItemListener;

    public ReportManageAdapter(Context context2) {
        this.resource = MCResource.getInstance(context2);
        this.inflater = LayoutInflater.from(context2);
        this.context = context2;
    }

    public int getCount() {
        return this.reportList.size();
    }

    public Object getItem(int position) {
        return this.reportList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View convertView2 = getConvertView(convertView);
        final ReportModel reportModel = this.reportList.get(position);
        updateReportTopicManageAdapter((ReportTopicManageAdapterHolder) convertView2.getTag(), convertView2, reportModel);
        convertView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ReportManageAdapter.this.context instanceof ReportTopicManageActivity) {
                    ReportManageAdapter.this.initTopicFunctionDialog(reportModel);
                } else if (ReportManageAdapter.this.context instanceof ReportUserManageActivity) {
                    ReportManageAdapter.this.initUserFunctionDialog(reportModel);
                }
            }
        });
        return convertView2;
    }

    /* access modifiers changed from: private */
    public void initTopicFunctionDialog(final ReportModel reportModel) {
        final List<String> topicFunctionList = new ArrayList<>();
        topicFunctionList.add(this.resource.getString("mc_forum_check_topic"));
        if (reportModel.getStatus() == 1) {
            topicFunctionList.add(this.resource.getString("mc_forum_delete_topic"));
        }
        topicFunctionList.add(this.resource.getString("mc_forum_cancel_report_user"));
        this.topicFunctionItemListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (((String) topicFunctionList.get(which)).equals(ReportManageAdapter.this.resource.getString("mc_forum_check_topic"))) {
                    Intent intent = new Intent(ReportManageAdapter.this.context, PostsActivity.class);
                    intent.putExtra("topicId", reportModel.getTopicId());
                    ReportManageAdapter.this.context.startActivity(intent);
                } else if (((String) topicFunctionList.get(which)).equals(ReportManageAdapter.this.resource.getString("mc_forum_delete_topic"))) {
                    new AlertDialog.Builder(ReportManageAdapter.this.context).setTitle(ReportManageAdapter.this.resource.getStringId("mc_forum_dialog_tip")).setMessage(ReportManageAdapter.this.resource.getStringId("mc_forum_warn_delete_topic")).setPositiveButton(ReportManageAdapter.this.resource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            new DeleteTopicTask().execute(Integer.valueOf(reportModel.getType()), 0L, Long.valueOf(reportModel.getPostId()));
                        }
                    }).setNegativeButton(ReportManageAdapter.this.resource.getStringId("mc_forum_dialog_cancel"), (DialogInterface.OnClickListener) null).show();
                } else if (((String) topicFunctionList.get(which)).equals(ReportManageAdapter.this.resource.getString("mc_forum_cancel_report_user"))) {
                    CancelReportTask unused = ReportManageAdapter.this.cancelReportTask = new CancelReportTask(ReportManageAdapter.this.context, 1);
                    ReportManageAdapter.this.cancelReportTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                        public void executeSuccess() {
                            ReportManageAdapter.this.reportList.remove(reportModel);
                            ReportManageAdapter.this.notifyDataSetChanged();
                        }

                        public void executeFail() {
                        }
                    });
                    ReportManageAdapter.this.cancelReportTask.execute(Long.valueOf((long) reportModel.getReportId()));
                }
            }
        };
        new AlertDialog.Builder(this.context).setTitle(this.resource.getString("mc_forum_topic_function")).setItems(MCForumReverseList.convertListToArray(topicFunctionList), this.topicFunctionItemListener).show();
    }

    /* access modifiers changed from: private */
    public void initUserFunctionDialog(final ReportModel reportModel) {
        final List<String> topicFunctionList = new ArrayList<>();
        topicFunctionList.add(this.resource.getString("mc_forum_visit_user_home"));
        topicFunctionList.add(this.resource.getString("mc_forum_cancel_report_user"));
        this.topicFunctionItemListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (((String) topicFunctionList.get(which)).equals(ReportManageAdapter.this.resource.getString("mc_forum_visit_user_home"))) {
                    MCForumHelper.gotoUserInfo((Activity) ReportManageAdapter.this.context, ReportManageAdapter.this.resource, reportModel.getUserId());
                } else if (((String) topicFunctionList.get(which)).equals(ReportManageAdapter.this.resource.getString("mc_forum_cancel_report_user"))) {
                    CancelReportTask unused = ReportManageAdapter.this.cancelReportTask = new CancelReportTask(ReportManageAdapter.this.context, 3);
                    ReportManageAdapter.this.cancelReportTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                        public void executeSuccess() {
                            ReportManageAdapter.this.reportList.remove(reportModel);
                            ReportManageAdapter.this.notifyDataSetChanged();
                        }

                        public void executeFail() {
                        }
                    });
                    ReportManageAdapter.this.cancelReportTask.execute(Long.valueOf((long) reportModel.getReportId()));
                } else if (((String) topicFunctionList.get(which)).equals(ReportManageAdapter.this.resource.getString("mc_forum_shielded"))) {
                    Intent intent = new Intent(ReportManageAdapter.this.context, UserShieldedActivity.class);
                    intent.putExtra(MCConstant.SHIELDED_TYPE, 1);
                    intent.putExtra("userId", new UserServiceImpl(ReportManageAdapter.this.context).getLoginUserId());
                    intent.putExtra("shieldedUserId", reportModel.getUserId());
                    ReportManageAdapter.this.context.startActivity(intent);
                }
            }
        };
        new AlertDialog.Builder(this.context).setTitle(this.resource.getString("mc_forum_user_function")).setItems(MCForumReverseList.convertListToArray(topicFunctionList), this.topicFunctionItemListener).show();
    }

    private void updateReportTopicManageAdapter(ReportTopicManageAdapterHolder holder, View convertView, ReportModel reportModel) {
        if (this.context instanceof ReportTopicManageActivity) {
            holder.getBoardName().setText("[" + reportModel.getBoardName() + "]");
            holder.getTopicTitle().setText(reportModel.getContent());
        } else if (this.context instanceof ReportUserManageActivity) {
            MCColorUtil.setTextViewPart(this.context, holder.getBoardName(), reportModel.getNickName(), 0, reportModel.getNickName().length(), "mc_forum_text_hight_color");
            holder.getTopicTitle().setText(this.context.getResources().getString(this.resource.getStringId("mc_forum_user_report")));
        }
        holder.getReportTopicBox().removeAllViews();
        for (int i = 0; i < reportModel.getInformantList().size(); i++) {
            MCReportTopicManageBar manageBar = new MCReportTopicManageBar(this.inflater, this.resource, reportModel.getInformantList().get(i));
            if (i == reportModel.getInformantList().size() - 1) {
                manageBar.hideLine();
            }
            holder.getReportTopicBox().addView(manageBar.getView());
        }
    }

    private View getConvertView(View convertView) {
        ReportTopicManageAdapterHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_report_topic_manage_item"), (ViewGroup) null);
            holder = new ReportTopicManageAdapterHolder();
            initModeratorAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (ReportTopicManageAdapterHolder) convertView.getTag();
        }
        if (holder != null) {
            return convertView;
        }
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_report_topic_manage_item"), (ViewGroup) null);
        ReportTopicManageAdapterHolder holder2 = new ReportTopicManageAdapterHolder();
        initModeratorAdapterHolder(convertView2, holder2);
        convertView2.setTag(holder2);
        return convertView2;
    }

    private void initModeratorAdapterHolder(View convertView, ReportTopicManageAdapterHolder holder) {
        holder.setBoardName((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_board_name_text")));
        holder.setTopicTitle((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_topic_title_text")));
        holder.setReportTopicBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_report_manage_box")));
    }

    public List<ReportModel> getReportList() {
        return this.reportList;
    }

    public void setReportList(List<ReportModel> reportList2) {
        this.reportList = reportList2;
    }

    private class DeleteTopicTask extends AsyncTask<Object, Void, String> {
        private DeleteTopicTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* Debug info: failed to restart local var, previous not found, register: 12 */
        /* access modifiers changed from: protected */
        public String doInBackground(Object... params) {
            int type = ((Integer) params[0]).intValue();
            long boardId = ((Long) params[1]).longValue();
            PostsService postsService = new PostsServiceImpl(ReportManageAdapter.this.context);
            if (type == 1) {
                return postsService.deleteTopic(boardId, ((Long) params[2]).longValue());
            }
            if (type == 2) {
                return postsService.deleteReply(boardId, ((Long) params[2]).longValue());
            }
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (result != null) {
                Toast.makeText(ReportManageAdapter.this.context, MCForumErrorUtil.convertErrorCode(ReportManageAdapter.this.context, result), 1).show();
            } else {
                Toast.makeText(ReportManageAdapter.this.context, ReportManageAdapter.this.context.getResources().getString(ReportManageAdapter.this.resource.getStringId("mc_forum_delete_topic_succ")), 1).show();
            }
        }
    }

    public void destory() {
        if (this.cancelReportTask != null) {
            this.cancelReportTask.cancel(true);
        }
    }
}
