package com.mobcent.base.android.ui.activity.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.mobcent.base.android.ui.activity.adapter.FriendAdapter;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.List;

public class UserFollowFragment extends UserFriendsFrament {
    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.initViews(inflater, container, savedInstanceState);
        this.adapter = new FriendAdapter(this.activity, this.userInfoList, this.mHandler, this.asyncTaskLoaderImage, inflater, 0, getAdPosition(), this, this.currentPage);
        this.adapter.setClickListener(getFriendsClickListener());
        this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public List<UserInfoModel> getUserFriendsList() {
        return this.userService.getUserFriendList(this.userId, this.currentPage, this.pageSize, this.isLocal);
    }
}
