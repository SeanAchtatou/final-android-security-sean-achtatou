package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.adapter.holder.RegLoginAdapterHolder;
import com.mobcent.base.forum.android.util.MCResource;
import java.util.ArrayList;

public class RegLoginListAdapter extends BaseAdapter {
    private ArrayList<String> emailSections;
    private LayoutInflater inflater;
    private MCResource resource;

    public RegLoginListAdapter(Context ctx, ArrayList<String> emailSections2, MCResource resource2) {
        this.emailSections = emailSections2;
        this.inflater = LayoutInflater.from(ctx);
        this.resource = resource2;
    }

    public int getCount() {
        return this.emailSections.size();
    }

    public Object getItem(int position) {
        return this.emailSections.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        RegLoginAdapterHolder holder;
        String emailSection = this.emailSections.get(position);
        if (convertView == null || position == 0) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_user_register_login_item"), (ViewGroup) null);
            holder = new RegLoginAdapterHolder();
            initLoginAndRegisterListAdapter(convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (RegLoginAdapterHolder) convertView.getTag();
        }
        initEmailSectioins(holder, emailSection);
        return convertView;
    }

    private void initLoginAndRegisterListAdapter(View convertView, RegLoginAdapterHolder holder) {
        holder.setEmailSelectionText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_user_email_selection_text")));
    }

    private void initEmailSectioins(RegLoginAdapterHolder holder, String emailSection) {
        holder.getEmailSelectionText().setText(emailSection);
    }

    public ArrayList<String> getEmailSections() {
        return this.emailSections;
    }

    public void setEmailSections(ArrayList<String> emailSections2) {
        this.emailSections = emailSections2;
    }
}
