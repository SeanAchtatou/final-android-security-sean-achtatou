package com.mobcent.base.android.ui.widget.animmenu;

public class ButtonPoint {
    public int x;
    public int y;

    public ButtonPoint(int x2, int y2) {
        this.x = x2;
        this.y = y2;
    }

    public ButtonPoint() {
    }

    public String toString() {
        return "ButtonPoint [x=" + this.x + ", y=" + this.y + "]";
    }
}
