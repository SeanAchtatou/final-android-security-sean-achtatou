package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.LinearLayout;
import android.widget.TextView;

public class BoardDoubleAdapterHolder {
    private LinearLayout boardLeftBox;
    private TextView boardLeftName;
    private TextView boardLeftTime;
    private TextView boardLeftTodayTotal;
    private LinearLayout boardRightBox;
    private TextView boardRightName;
    private TextView boardRightTime;
    private TextView boardRightTodayTotal;

    public LinearLayout getBoardLeftBox() {
        return this.boardLeftBox;
    }

    public void setBoardLeftBox(LinearLayout boardLeftBox2) {
        this.boardLeftBox = boardLeftBox2;
    }

    public LinearLayout getBoardRightBox() {
        return this.boardRightBox;
    }

    public void setBoardRightBox(LinearLayout boardRightBox2) {
        this.boardRightBox = boardRightBox2;
    }

    public TextView getBoardLeftName() {
        return this.boardLeftName;
    }

    public void setBoardLeftName(TextView boardLeftName2) {
        this.boardLeftName = boardLeftName2;
    }

    public TextView getBoardLeftTime() {
        return this.boardLeftTime;
    }

    public void setBoardLeftTime(TextView boardLeftTime2) {
        this.boardLeftTime = boardLeftTime2;
    }

    public TextView getBoardLeftTodayTotal() {
        return this.boardLeftTodayTotal;
    }

    public void setBoardLeftTodayTotal(TextView boardLeftTodayTotal2) {
        this.boardLeftTodayTotal = boardLeftTodayTotal2;
    }

    public TextView getBoardRightName() {
        return this.boardRightName;
    }

    public void setBoardRightName(TextView boardRightName2) {
        this.boardRightName = boardRightName2;
    }

    public TextView getBoardRightTime() {
        return this.boardRightTime;
    }

    public void setBoardRightTime(TextView boardRightTime2) {
        this.boardRightTime = boardRightTime2;
    }

    public TextView getBoardRightTodayTotal() {
        return this.boardRightTodayTotal;
    }

    public void setBoardRightTodayTotal(TextView boardRightTodayTotal2) {
        this.boardRightTodayTotal = boardRightTodayTotal2;
    }
}
