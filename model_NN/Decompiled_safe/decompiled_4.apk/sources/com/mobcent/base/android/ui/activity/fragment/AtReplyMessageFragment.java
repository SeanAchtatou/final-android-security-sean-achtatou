package com.mobcent.base.android.ui.activity.fragment;

import com.mobcent.forum.android.model.PostsNoticeModel;
import java.util.List;

public class AtReplyMessageFragment extends MessageFragment {
    /* access modifiers changed from: protected */
    public List<PostsNoticeModel> getPostsNoticeList() {
        return this.postsService.getAtPostsNoticeList(this.page, this.pageSize);
    }

    /* access modifiers changed from: protected */
    public boolean updateNoitce() {
        return this.postsService.updateAtReplyRemindState(this.userId, this.replyIds);
    }
}
