package com.mobcent.base.android.ui.activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.holder.MsgChatUserAdapterHolder;
import com.mobcent.base.android.ui.activity.adapter.holder.MsgCurrentUserAdapterHolder;
import com.mobcent.base.android.ui.activity.fragmentActivity.ImageViewerFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.android.ui.activity.service.MediaService;
import com.mobcent.base.android.ui.activity.view.MCBaseMsgAudioView;
import com.mobcent.base.android.ui.activity.view.MCMsgChatAudioView;
import com.mobcent.base.android.ui.activity.view.MCMsgCurrAudioView;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;
import com.mobcent.base.forum.android.util.ImageCache;
import com.mobcent.base.forum.android.util.MCFaceUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.HeartMsgModel;
import com.mobcent.forum.android.model.RichImageModel;
import com.mobcent.forum.android.model.SoundModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.DateUtil;
import com.mobcent.forum.android.util.MCLogUtil;
import java.util.ArrayList;
import java.util.List;

public class MsgChatRoomListAdapter extends BaseAdapter implements MCConstant {
    private AsyncTaskLoaderImage asyncTaskLoaderImage;
    private String chatUserIcon;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public UserInfoModel currUserModel = new UserInfoModel();
    /* access modifiers changed from: private */
    public MCResource forumResource;
    private LayoutInflater inflater;
    private ListView listView;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private List<HeartMsgModel> messageList;
    /* access modifiers changed from: private */
    public ArrayList<RichImageModel> richImageModelList;
    /* access modifiers changed from: private */
    public long userId;

    public MsgChatRoomListAdapter(Context context2, long userId2, String chatUserIcon2, List<HeartMsgModel> messageList2, Handler handler, ListView listView2, AsyncTaskLoaderImage asyncTaskLoaderImage2) {
        this.context = context2;
        this.userId = userId2;
        new UserInfoAsynTask().execute(Long.valueOf(userId2));
        this.messageList = messageList2;
        this.chatUserIcon = chatUserIcon2;
        this.mHandler = handler;
        this.forumResource = MCResource.getInstance(context2);
        this.inflater = LayoutInflater.from(context2);
        this.listView = listView2;
        this.asyncTaskLoaderImage = asyncTaskLoaderImage2;
    }

    public List<HeartMsgModel> getMessageList() {
        return this.messageList;
    }

    public void setMessageList(List<HeartMsgModel> messageList2) {
        this.messageList = messageList2;
    }

    public int getCount() {
        return getMessageList().size();
    }

    public HeartMsgModel getItem(int position) {
        return getMessageList().get(position);
    }

    public long getItemId(int position) {
        return getMessageList().get(position).getMsgId();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View convertView2;
        this.richImageModelList = getAllImageUrl(this.messageList);
        HeartMsgModel messageModel = this.messageList.get(position);
        if (this.userId == messageModel.getFormUserId()) {
            convertView2 = getCurrUserView(messageModel, convertView, parent);
        } else {
            convertView2 = getChatUserView(messageModel, convertView, parent);
        }
        convertView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundModel sm = MsgChatRoomListAdapter.this.getItem(position).getSoundModel();
                if (sm != null) {
                    Intent intent = new Intent(MsgChatRoomListAdapter.this.context, MediaService.class);
                    intent.putExtra(MediaService.SERVICE_MODEL, sm);
                    intent.putExtra(MediaService.SERVICE_TAG, MsgChatRoomListAdapter.this.context.toString());
                    MsgChatRoomListAdapter.this.context.startService(intent);
                }
            }
        });
        return convertView2;
    }

    private View getCurrUserView(final HeartMsgModel messageModel, View convertView, ViewGroup parent) {
        MsgCurrentUserAdapterHolder currentUserHolder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.forumResource.getLayoutId("mc_forum_msg_current_user_item"), (ViewGroup) null);
            currentUserHolder = new MsgCurrentUserAdapterHolder();
            inidMsgCurrentUserAdapterHolder(convertView, currentUserHolder);
            convertView.setTag(currentUserHolder);
        } else {
            try {
                currentUserHolder = (MsgCurrentUserAdapterHolder) convertView.getTag();
            } catch (ClassCastException e) {
                convertView = this.inflater.inflate(this.forumResource.getLayoutId("mc_forum_msg_current_user_item"), (ViewGroup) null);
                currentUserHolder = new MsgCurrentUserAdapterHolder();
                inidMsgCurrentUserAdapterHolder(convertView, currentUserHolder);
                convertView.setTag(currentUserHolder);
            }
        }
        convertView.setOnClickListener(null);
        if (this.currUserModel != null) {
            setMsgChatRoomUserIcon(this.currUserModel.getIcon(), currentUserHolder.getUserIcon());
        }
        SoundModel soundModel = messageModel.getSoundModel();
        if (messageModel.getType() == 1) {
            currentUserHolder.getContent().setVisibility(0);
            currentUserHolder.getAudioView().setVisibility(8);
            currentUserHolder.getImgView().setVisibility(8);
            MCFaceUtil.setStrToFace(currentUserHolder.getContent(), messageModel.getContent(), this.context);
        } else if (messageModel.getType() == 3) {
            if (soundModel != null) {
                currentUserHolder.getContent().setVisibility(8);
                currentUserHolder.getImgView().setVisibility(8);
                currentUserHolder.getAudioView().setVisibility(0);
                currentUserHolder.getAudioView().setTag(soundModel.getSoundPath());
                currentUserHolder.getAudioView().updateView(soundModel);
            }
        } else if (messageModel.getType() == 2) {
            currentUserHolder.getContent().setVisibility(8);
            currentUserHolder.getAudioView().setVisibility(8);
            currentUserHolder.getImgView().setVisibility(0);
            updateImageView(AsyncTaskLoaderImage.formatUrl(messageModel.getContent(), "320x480"), currentUserHolder.getImgView());
            currentUserHolder.getImgView().setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(MsgChatRoomListAdapter.this.context, ImageViewerFragmentActivity.class);
                    intent.putExtra(MCConstant.RICH_IMAGE_LIST, MsgChatRoomListAdapter.this.richImageModelList);
                    intent.putExtra(MCConstant.IMAGE_URL, messageModel.getContent());
                    MsgChatRoomListAdapter.this.context.startActivity(intent);
                }
            });
        }
        currentUserHolder.getChatTime().setText(DateUtil.getFormatTime(messageModel.getCreateDate()));
        if (messageModel.getSendStatus() == 0) {
            currentUserHolder.getLoading().hide();
            currentUserHolder.getSendStatus().setVisibility(4);
        } else if (messageModel.getSendStatus() == -2) {
            currentUserHolder.getLoading().hide();
            currentUserHolder.getSendStatus().setVisibility(0);
        } else if (messageModel.getSendStatus() == -1) {
            currentUserHolder.getLoading().show();
            currentUserHolder.getSendStatus().setVisibility(4);
        }
        initMsgCurrentUserIconListener(currentUserHolder, messageModel);
        return convertView;
    }

    private View getChatUserView(final HeartMsgModel messageModel, View convertView, ViewGroup parent) {
        MsgChatUserAdapterHolder chatUserHolder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.forumResource.getLayoutId("mc_forum_msg_chat_user_item"), (ViewGroup) null);
            chatUserHolder = new MsgChatUserAdapterHolder();
            initMsgChatUserAdapterHolder(convertView, chatUserHolder);
            convertView.setTag(chatUserHolder);
        } else {
            try {
                chatUserHolder = (MsgChatUserAdapterHolder) convertView.getTag();
            } catch (ClassCastException e) {
                convertView = this.inflater.inflate(this.forumResource.getLayoutId("mc_forum_msg_chat_user_item"), (ViewGroup) null);
                chatUserHolder = new MsgChatUserAdapterHolder();
                initMsgChatUserAdapterHolder(convertView, chatUserHolder);
                convertView.setTag(chatUserHolder);
            }
        }
        setMsgChatRoomUserIcon(this.chatUserIcon, chatUserHolder.getUserIcon());
        SoundModel soundModel = messageModel.getSoundModel();
        if (messageModel.getType() == 1) {
            chatUserHolder.getAudioView().setVisibility(8);
            chatUserHolder.getImgView().setVisibility(8);
            chatUserHolder.getContent().setVisibility(0);
            MCFaceUtil.setStrToFace(chatUserHolder.getContent(), messageModel.getContent(), this.context);
        } else if (messageModel.getType() == 3) {
            if (soundModel != null) {
                chatUserHolder.getAudioView().setVisibility(0);
                chatUserHolder.getContent().setVisibility(8);
                chatUserHolder.getImgView().setVisibility(8);
                chatUserHolder.getAudioView().setTag(soundModel.getSoundPath());
                chatUserHolder.getAudioView().updateView(soundModel);
            }
        } else if (messageModel.getType() == 2) {
            chatUserHolder.getContent().setVisibility(8);
            chatUserHolder.getAudioView().setVisibility(8);
            chatUserHolder.getImgView().setVisibility(0);
            updateImageView(AsyncTaskLoaderImage.formatUrl(messageModel.getContent(), "320x480"), chatUserHolder.getImgView());
            chatUserHolder.getImgView().setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(MsgChatRoomListAdapter.this.context, ImageViewerFragmentActivity.class);
                    intent.putExtra(MCConstant.RICH_IMAGE_LIST, MsgChatRoomListAdapter.this.richImageModelList);
                    intent.putExtra(MCConstant.IMAGE_URL, messageModel.getContent());
                    MsgChatRoomListAdapter.this.context.startActivity(intent);
                }
            });
        }
        chatUserHolder.getChatTime().setText(DateUtil.getFormatTime(messageModel.getCreateDate()));
        initMsgChatUserIconListener(chatUserHolder, messageModel);
        return convertView;
    }

    private void updateImageView(String imgUrl, final ImageView imageView) {
        if (SharedPreferencesDB.getInstance(this.context).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(imgUrl, new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    MsgChatRoomListAdapter.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (image != null) {
                                imageView.setImageBitmap(image);
                            }
                        }
                    });
                }
            });
        }
    }

    private void inidMsgCurrentUserAdapterHolder(View convertView, MsgCurrentUserAdapterHolder holder) {
        holder.setUserIcon((Button) convertView.findViewById(this.forumResource.getViewId("mc_forum_current_user_img")));
        holder.setContent((TextView) convertView.findViewById(this.forumResource.getViewId("mc_forum_current_user_content_text")));
        holder.setChatTime((TextView) convertView.findViewById(this.forumResource.getViewId("mc_forum_current_user_time_text")));
        holder.setSendStatus((TextView) convertView.findViewById(this.forumResource.getViewId("mc_forum_current_user_send_status")));
        holder.setAudioView((MCMsgCurrAudioView) convertView.findViewById(this.forumResource.getViewId("mc_forum_msg_curr_audio_view")));
        holder.setLoading((MCProgressBar) convertView.findViewById(this.forumResource.getViewId("mc_forum_current_loading")));
        holder.setImgView((ImageView) convertView.findViewById(this.forumResource.getViewId("mc_forum_msg_curr_img_view")));
    }

    private void initMsgChatUserAdapterHolder(View convertView, MsgChatUserAdapterHolder holder) {
        holder.setUserIcon((Button) convertView.findViewById(this.forumResource.getViewId("mc_forum_chat_user_icon_img")));
        holder.setContent((TextView) convertView.findViewById(this.forumResource.getViewId("mc_forum_chat_user_content_text")));
        holder.setChatTime((TextView) convertView.findViewById(this.forumResource.getViewId("mc_forum_chat_user_time_text")));
        holder.setAudioView((MCMsgChatAudioView) convertView.findViewById(this.forumResource.getViewId("mc_forum_msg_chat_audio_view")));
        holder.setImgView((ImageView) convertView.findViewById(this.forumResource.getViewId("mc_forum_msg_chat_img_view")));
    }

    private void setMsgChatRoomUserIcon(String iconUrl, final Button userIcon) {
        if (SharedPreferencesDB.getInstance(this.context).getPicModeFlag()) {
            ImageCache.getInstance(this.context).loadAsync(ImageCache.formatUrl(iconUrl, "100x100"), new ImageCache.ImageCallback() {
                public void onImageLoaded(final Drawable image, String url) {
                    if (image != null) {
                        MsgChatRoomListAdapter.this.mHandler.post(new Runnable() {
                            public void run() {
                                if (image != null) {
                                    userIcon.setBackgroundDrawable(image);
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    private ArrayList<RichImageModel> getAllImageUrl(List<HeartMsgModel> messageList2) {
        ArrayList<RichImageModel> richImageModelList2 = new ArrayList<>();
        for (int i = 0; i < messageList2.size(); i++) {
            HeartMsgModel heartMsgModel = messageList2.get(i);
            if (heartMsgModel.getType() == 2) {
                RichImageModel model = new RichImageModel();
                model.setImageUrl(heartMsgModel.getContent());
                richImageModelList2.add(model);
            }
        }
        return richImageModelList2;
    }

    private class UserInfoAsynTask extends AsyncTask<Long, Void, UserInfoModel> {
        private UserInfoAsynTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public UserInfoModel doInBackground(Long... params) {
            return new UserServiceImpl(MsgChatRoomListAdapter.this.context).getUserInfo(params[0].longValue());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(UserInfoModel result) {
            if (result == null) {
                return;
            }
            if (result.getErrorCode() == null || "".equals(result.getErrorCode())) {
                UserInfoModel unused = MsgChatRoomListAdapter.this.currUserModel = result;
                MsgChatRoomListAdapter.this.notifyDataSetInvalidated();
                MsgChatRoomListAdapter.this.notifyDataSetChanged();
                return;
            }
            UserInfoModel unused2 = MsgChatRoomListAdapter.this.currUserModel = null;
        }
    }

    private void initMsgChatUserIconListener(MsgChatUserAdapterHolder holder, final HeartMsgModel messageModel) {
        holder.getUserIcon().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCForumHelper.gotoUserInfo((Activity) MsgChatRoomListAdapter.this.context, MsgChatRoomListAdapter.this.forumResource, messageModel.getFormUserId());
            }
        });
    }

    private void initMsgCurrentUserIconListener(MsgCurrentUserAdapterHolder holder, HeartMsgModel messageModel) {
        holder.getUserIcon().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCForumHelper.gotoUserInfo((Activity) MsgChatRoomListAdapter.this.context, MsgChatRoomListAdapter.this.forumResource, MsgChatRoomListAdapter.this.userId);
            }
        });
    }

    public String getChatUserIcon() {
        return this.chatUserIcon;
    }

    public void setChatUserIcon(String chatUserIcon2) {
        this.chatUserIcon = chatUserIcon2;
    }

    public void updateReceivePlayImg(SoundModel soundModel) {
        SoundModel sm;
        if (soundModel != null) {
            int i = 0;
            int s = this.messageList.size();
            while (true) {
                if (i >= s) {
                    break;
                }
                HeartMsgModel msgModel = this.messageList.get(i);
                if (msgModel.getType() == 3 && (sm = msgModel.getSoundModel()) != null && sm.getSoundPath().equals(soundModel.getSoundPath())) {
                    sm.setPalyStatus(soundModel.getPalyStatus());
                    sm.setPlayProgress(soundModel.getPlayProgress());
                    sm.setCurrentPosition(soundModel.getCurrentPosition());
                    this.messageList.set(i, msgModel);
                    break;
                }
                i++;
            }
            MCLogUtil.i("MsgChatRoomListAdapter", "soundModel.getPalyStatus() " + soundModel.getPalyStatus());
            MCBaseMsgAudioView view = (MCBaseMsgAudioView) this.listView.findViewWithTag(soundModel.getSoundPath());
            if (view != null) {
                MCLogUtil.i("MsgChatRoomListAdapter", "view.updateView(soundModel)");
                view.updateView(soundModel);
            }
        }
    }
}
