package com.mobcent.base.android.ui.activity.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.UserMyPwdActivity;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.android.ui.activity.helper.MCForumLaunchShareHelper;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.service.PostsService;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import com.mobcent.update.android.os.service.UpdateNoticeOSService;
import com.mobcent.update.android.os.service.helper.UpdateOSServiceHelper;
import java.util.HashMap;

public class SetttingFragment extends BaseFragment {
    /* access modifiers changed from: private */
    public Button MentionFriendNotificationFlagBtn;
    private Button RecomAppBtn;
    /* access modifiers changed from: private */
    public Activity activity;
    private Button backBtn;
    private Button clearCacheBth;
    /* access modifiers changed from: private */
    public ClearCacheClickListener clearCacheClickListener;
    private Button logoutBtn;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public Button messageVoiceFlagBtn;
    private Button modificationPasswordBtn;
    /* access modifiers changed from: private */
    public Button picModeBtn;
    /* access modifiers changed from: private */
    public PostsService postsService;
    /* access modifiers changed from: private */
    public Button replyNotificationFlagBtn;
    private Button updateBtn;

    public interface ClearCacheClickListener {
        void onClearCacheClick(View view);

        void onUserLogoutClick(View view);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.activity = getActivity();
        this.postsService = new PostsServiceImpl(this.activity);
    }

    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(this.mcResource.getLayoutId("mc_forum_settting_fragment"), (ViewGroup) null);
        this.modificationPasswordBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_modification_password_btn"));
        this.replyNotificationFlagBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_user_reply_notification_flag_btn"));
        this.picModeBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_pic_mode_btn"));
        this.clearCacheBth = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_clear_cache_btn"));
        this.backBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_back_btn"));
        this.RecomAppBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_recom_app_btn"));
        this.MentionFriendNotificationFlagBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_mention_friend_notification_flag_btn"));
        this.updateBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_update_btn"));
        this.logoutBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_logout_btn"));
        this.messageVoiceFlagBtn = (Button) this.view.findViewById(this.mcResource.getViewId("mc_forum_message_voice_flag_btn"));
        if (!MCForumHelper.setAtReplyMessageFragment(this.activity)) {
            this.MentionFriendNotificationFlagBtn.setVisibility(8);
        }
        if (MCForumHelper.onNoPicModel(this.activity)) {
            this.view.findViewById(this.mcResource.getViewId("mc_forum_pic_mode_Box")).setVisibility(8);
        }
        if (this.postsService.getReplyNotificationFlag()) {
            this.replyNotificationFlagBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, this.mcResource.getDrawableId("mc_forum_select3_2"), 0);
        } else {
            this.replyNotificationFlagBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, this.mcResource.getDrawableId("mc_forum_select3_1"), 0);
        }
        if (this.postsService.getMentionFriendNotificationFlag()) {
            this.MentionFriendNotificationFlagBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, this.mcResource.getDrawableId("mc_forum_select3_2"), 0);
        } else {
            this.MentionFriendNotificationFlagBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, this.mcResource.getDrawableId("mc_forum_select3_1"), 0);
        }
        if (this.postsService.getMessageVoiceFlag()) {
            this.messageVoiceFlagBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, this.mcResource.getDrawableId("mc_forum_select3_2"), 0);
        } else {
            this.messageVoiceFlagBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, this.mcResource.getDrawableId("mc_forum_select3_1"), 0);
        }
        if (SharedPreferencesDB.getInstance(getActivity()).getPicModeFlag()) {
            this.picModeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, this.mcResource.getDrawableId("mc_forum_select3_1"), 0);
        } else {
            this.picModeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, this.mcResource.getDrawableId("mc_forum_select3_2"), 0);
        }
        if (!new UserServiceImpl(this.activity).isLogin()) {
            this.logoutBtn.setText(this.mcResource.getString("mc_forum_user_login"));
            this.view.findViewById(this.mcResource.getViewId("mc_forum_password_line")).setVisibility(8);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_mention_friend_line")).setVisibility(8);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_reply_line")).setVisibility(8);
            this.modificationPasswordBtn.setVisibility(8);
            this.replyNotificationFlagBtn.setVisibility(8);
            this.MentionFriendNotificationFlagBtn.setVisibility(8);
        } else {
            this.view.findViewById(this.mcResource.getViewId("mc_forum_password_line")).setVisibility(0);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_mention_friend_line")).setVisibility(0);
            this.view.findViewById(this.mcResource.getViewId("mc_forum_reply_line")).setVisibility(0);
            this.replyNotificationFlagBtn.setVisibility(0);
            this.MentionFriendNotificationFlagBtn.setVisibility(0);
            this.modificationPasswordBtn.setVisibility(0);
        }
        return this.view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.modificationPasswordBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (LoginInterceptor.doInterceptorByDialog(SetttingFragment.this.activity, SetttingFragment.this.mcResource, UserMyPwdActivity.class, new HashMap<>())) {
                    SetttingFragment.this.startActivity(new Intent(SetttingFragment.this.activity, UserMyPwdActivity.class));
                }
            }
        });
        this.replyNotificationFlagBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SetttingFragment.this.postsService.getReplyNotificationFlag()) {
                    SetttingFragment.this.replyNotificationFlagBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, SetttingFragment.this.mcResource.getDrawableId("mc_forum_select3_1"), 0);
                    SetttingFragment.this.postsService.setReplyNotificationFlag(false);
                    return;
                }
                SetttingFragment.this.replyNotificationFlagBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, SetttingFragment.this.mcResource.getDrawableId("mc_forum_select3_2"), 0);
                SetttingFragment.this.postsService.setReplyNotificationFlag(true);
            }
        });
        this.clearCacheBth.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SetttingFragment.this.getClearCacheClickListener() != null) {
                    SetttingFragment.this.clearCacheClickListener.onClearCacheClick(v);
                }
            }
        });
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SetttingFragment.this.getActivity().finish();
            }
        });
        this.RecomAppBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bitmap shareAppBitmap;
                String shareURL = "";
                try {
                    shareURL = SetttingFragment.this.getString(SetttingFragment.this.mcResource.getStringId("mc_share_download_url"));
                } catch (Exception e) {
                }
                String shareContent = "";
                try {
                    shareContent = SetttingFragment.this.getString(SetttingFragment.this.mcResource.getStringId("mc_forum_default_share_content"));
                } catch (Exception e2) {
                }
                String sharePicUrl = "";
                try {
                    sharePicUrl = SetttingFragment.this.getString(SetttingFragment.this.mcResource.getStringId("mc_forum_default_share_pic_url"));
                } catch (Exception e3) {
                }
                MCForumLaunchShareHelper.getDefaultShareUrl(SetttingFragment.this.activity);
                try {
                    shareAppBitmap = BitmapFactory.decodeResource(SetttingFragment.this.getResources(), SetttingFragment.this.mcResource.getDrawableId("mc_forum_share_img"));
                } catch (Exception e4) {
                    shareAppBitmap = null;
                }
                if (StringUtil.isEmpty(sharePicUrl) || shareAppBitmap == null) {
                    MCForumLaunchShareHelper.shareAppContent(shareContent, shareURL, "", SetttingFragment.this.activity);
                } else {
                    MCForumLaunchShareHelper.shareContentWithBitmapAndUrl(shareContent, shareAppBitmap, sharePicUrl, shareURL, "", SetttingFragment.this.activity);
                }
            }
        });
        this.MentionFriendNotificationFlagBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SetttingFragment.this.postsService.getMentionFriendNotificationFlag()) {
                    SetttingFragment.this.MentionFriendNotificationFlagBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, SetttingFragment.this.mcResource.getDrawableId("mc_forum_select3_1"), 0);
                    SetttingFragment.this.postsService.setMentionFriendNotificationFlag(false);
                    return;
                }
                SetttingFragment.this.MentionFriendNotificationFlagBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, SetttingFragment.this.mcResource.getDrawableId("mc_forum_select3_2"), 0);
                SetttingFragment.this.postsService.setMentionFriendNotificationFlag(true);
            }
        });
        this.messageVoiceFlagBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SetttingFragment.this.postsService.getMessageVoiceFlag()) {
                    SetttingFragment.this.messageVoiceFlagBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, SetttingFragment.this.mcResource.getDrawableId("mc_forum_select3_1"), 0);
                    SetttingFragment.this.postsService.setMessageVoiceFlag(false);
                    return;
                }
                SetttingFragment.this.messageVoiceFlagBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, SetttingFragment.this.mcResource.getDrawableId("mc_forum_select3_2"), 0);
                SetttingFragment.this.postsService.setMessageVoiceFlag(true);
            }
        });
        this.picModeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((ConnectivityManager) SetttingFragment.this.activity.getSystemService("connectivity")).getNetworkInfo(1).getState() != NetworkInfo.State.CONNECTED) {
                    if (SharedPreferencesDB.getInstance(SetttingFragment.this.getActivity()).getPicModeFlag()) {
                        SetttingFragment.this.picModeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, SetttingFragment.this.mcResource.getDrawableId("mc_forum_select3_2"), 0);
                        SharedPreferencesDB.getInstance(SetttingFragment.this.getActivity()).setPicModeFlag(false);
                        return;
                    }
                    SetttingFragment.this.picModeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, SetttingFragment.this.mcResource.getDrawableId("mc_forum_select3_1"), 0);
                    SharedPreferencesDB.getInstance(SetttingFragment.this.getActivity()).setPicModeFlag(true);
                }
            }
        });
        this.updateBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final ProgressDialog dialog2 = new ProgressDialog(SetttingFragment.this.activity);
                dialog2.setMessage(SetttingFragment.this.getResources().getString(SetttingFragment.this.mcResource.getStringId("mc_forum_update_loding")));
                dialog2.show();
                UpdateOSServiceHelper.startUpdateNoticeService(SetttingFragment.this.activity, new UpdateNoticeOSService.UpdateCallBack() {
                    public void updateResult(final boolean isUpdate) {
                        SetttingFragment.this.mHandler.post(new Runnable() {
                            public void run() {
                                if (isUpdate) {
                                    dialog2.cancel();
                                    return;
                                }
                                dialog2.cancel();
                                Toast.makeText(SetttingFragment.this.activity, SetttingFragment.this.getResources().getString(SetttingFragment.this.mcResource.getStringId("mc_forum_update_new")), 1).show();
                            }
                        });
                    }
                });
            }
        });
        this.logoutBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                if (new UserServiceImpl(SetttingFragment.this.activity).isLogin()) {
                    new AlertDialog.Builder(SetttingFragment.this.activity).setMessage(SetttingFragment.this.getResources().getString(SetttingFragment.this.mcResource.getStringId("mc_forum_logout_dialog"))).setNegativeButton(SetttingFragment.this.getResources().getString(SetttingFragment.this.mcResource.getStringId("mc_forum_dialog_cancel")), (DialogInterface.OnClickListener) null).setPositiveButton(SetttingFragment.this.mcResource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (SetttingFragment.this.getClearCacheClickListener() != null) {
                                SetttingFragment.this.clearCacheClickListener.onUserLogoutClick(v);
                                new PermServiceImpl(SetttingFragment.this.activity).getPerm();
                            }
                        }
                    }).show();
                    return;
                }
                SetttingFragment.this.activity.startActivity(MCForumHelper.setLoginIntent(SetttingFragment.this.activity));
                SetttingFragment.this.activity.finish();
            }
        });
    }

    public ClearCacheClickListener getClearCacheClickListener() {
        return this.clearCacheClickListener;
    }

    public void setClearCacheClickListener(ClearCacheClickListener clearCacheClickListener2) {
        this.clearCacheClickListener = clearCacheClickListener2;
    }

    public Handler getmHandler() {
        return this.mHandler;
    }

    public void setmHandler(Handler mHandler2) {
        this.mHandler = mHandler2;
    }
}
