package com.mobcent.base.android.ui.activity.delegate;

import com.mobcent.base.android.ui.activity.fragment.MsgUserListFragment;

public interface ChatRetrunDelegate {
    MsgUserListFragment getMsgUserListFragment();

    void invalidateUserList();
}
