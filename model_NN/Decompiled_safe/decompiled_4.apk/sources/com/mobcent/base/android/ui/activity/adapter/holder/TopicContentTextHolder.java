package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.TextView;

public class TopicContentTextHolder {
    private TextView topicInfoText;

    public TextView getTopicInfoText() {
        return this.topicInfoText;
    }

    public void setTopicInfoText(TextView topicInfoText2) {
        this.topicInfoText = topicInfoText2;
    }
}
