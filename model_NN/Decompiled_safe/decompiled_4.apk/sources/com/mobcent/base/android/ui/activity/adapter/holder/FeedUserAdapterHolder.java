package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FeedUserAdapterHolder {
    private LinearLayout feedsBox;
    private TextView followTimeText;
    private LinearLayout followUserBox;
    private TextView followUserNameText;
    private ImageView iconImg;
    private LinearLayout levelBox;
    private TextView moreText;
    private TextView registerText;
    private TextView registerTimeText;
    private LinearLayout registerUserBox;
    private TextView roleText;
    private TextView userNameText;
    private LinearLayout userReplyBox;
    private LinearLayout userTopicBox;

    public ImageView getIconImg() {
        return this.iconImg;
    }

    public void setIconImg(ImageView iconImg2) {
        this.iconImg = iconImg2;
    }

    public TextView getUserNameText() {
        return this.userNameText;
    }

    public void setUserNameText(TextView userNameText2) {
        this.userNameText = userNameText2;
    }

    public LinearLayout getFeedsBox() {
        return this.feedsBox;
    }

    public void setFeedsBox(LinearLayout feedsBox2) {
        this.feedsBox = feedsBox2;
    }

    public TextView getRoleText() {
        return this.roleText;
    }

    public void setRoleText(TextView roleText2) {
        this.roleText = roleText2;
    }

    public LinearLayout getLevelBox() {
        return this.levelBox;
    }

    public void setLevelBox(LinearLayout levelBox2) {
        this.levelBox = levelBox2;
    }

    public LinearLayout getUserTopicBox() {
        return this.userTopicBox;
    }

    public void setUserTopicBox(LinearLayout userTopicBox2) {
        this.userTopicBox = userTopicBox2;
    }

    public LinearLayout getUserReplyBox() {
        return this.userReplyBox;
    }

    public void setUserReplyBox(LinearLayout userReplyBox2) {
        this.userReplyBox = userReplyBox2;
    }

    public LinearLayout getFollowUserBox() {
        return this.followUserBox;
    }

    public void setFollowUserBox(LinearLayout followUserBox2) {
        this.followUserBox = followUserBox2;
    }

    public LinearLayout getRegisterUserBox() {
        return this.registerUserBox;
    }

    public void setRegisterUserBox(LinearLayout registerUserBox2) {
        this.registerUserBox = registerUserBox2;
    }

    public TextView getMoreText() {
        return this.moreText;
    }

    public void setMoreText(TextView moreText2) {
        this.moreText = moreText2;
    }

    public TextView getFollowTimeText() {
        return this.followTimeText;
    }

    public void setFollowTimeText(TextView followTimeText2) {
        this.followTimeText = followTimeText2;
    }

    public TextView getFollowUserNameText() {
        return this.followUserNameText;
    }

    public void setFollowUserNameText(TextView followUserNameText2) {
        this.followUserNameText = followUserNameText2;
    }

    public TextView getRegisterText() {
        return this.registerText;
    }

    public void setRegisterText(TextView registerText2) {
        this.registerText = registerText2;
    }

    public TextView getRegisterTimeText() {
        return this.registerTimeText;
    }

    public void setRegisterTimeText(TextView registerTimeText2) {
        this.registerTimeText = registerTimeText2;
    }
}
