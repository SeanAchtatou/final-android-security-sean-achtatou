package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.UserManageService;
import com.mobcent.forum.android.service.impl.UserManageServiceImpl;
import java.util.List;

public class BannedManageActivity extends BaseBannedShieldedManageActivity implements MCConstant {
    private UserManageService userManageService;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.pullToRefreshListView.onRefreshWithOutListener();
        onRefreshs();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.userManageService = new UserManageServiceImpl(this);
        Intent intent = getIntent();
        this.boardId = (int) intent.getLongExtra("boardId", 0);
        this.type = intent.getIntExtra(MCConstant.BANNED_TYPE, 0);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        this.titleText.setText(getString(this.resource.getStringId("mc_forum_banned_manage")));
    }

    public List<UserInfoModel> getBannedShieldedUser(int type, int boardId, int page, int pageSize) {
        return this.userManageService.getBannedShieldedUser(type, boardId, page, pageSize);
    }

    public List<UserInfoModel> searchBannedShieldedUser(int type, int boardId, String searchKeyWord, int page, int pageSize) {
        return this.userManageService.searchBannedShieldedUser(type, boardId, "i", page, pageSize);
    }
}
