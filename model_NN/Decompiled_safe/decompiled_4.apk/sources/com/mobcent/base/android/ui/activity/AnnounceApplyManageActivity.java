package com.mobcent.base.android.ui.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.adapter.AnnounceListAdapter;
import com.mobcent.base.android.ui.widget.pulltorefresh.PullToRefreshListView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.model.AnnoModel;
import com.mobcent.forum.android.service.AnnounceService;
import com.mobcent.forum.android.service.impl.AnnounceServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public class AnnounceApplyManageActivity extends BaseListViewActivity {
    /* access modifiers changed from: private */
    public AnnounceListAdapter adapter;
    /* access modifiers changed from: private */
    public List<AnnoModel> annoList;
    /* access modifiers changed from: private */
    public AnnounceService announceService;
    private Button backBtn;
    /* access modifiers changed from: private */
    public int currentPage = 1;
    private LayoutInflater inflater;
    private GetMoreAnnoListAsyncTask moreAnnoListAsyncTask;
    /* access modifiers changed from: private */
    public PullToRefreshListView pullToRefreshListView;
    private RefreshAnnoListAsyncTask refreshAnnoListAsyncTask;
    /* access modifiers changed from: private */
    public List<String> refreshimgUrls;

    static /* synthetic */ int access$208(AnnounceApplyManageActivity x0) {
        int i = x0.currentPage;
        x0.currentPage = i + 1;
        return i;
    }

    static /* synthetic */ int access$210(AnnounceApplyManageActivity x0) {
        int i = x0.currentPage;
        x0.currentPage = i - 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.pullToRefreshListView.onRefreshWithOutListener();
        onRefreshs();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.refreshimgUrls = new ArrayList();
        this.annoList = new ArrayList();
        this.inflater = LayoutInflater.from(this);
        this.announceService = new AnnounceServiceImpl(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_apply_announce_activity"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.pullToRefreshListView = (PullToRefreshListView) findViewById(this.resource.getViewId("mc_forum_lv"));
        this.adapter = new AnnounceListAdapter(this, this.resource, this.annoList, this.inflater, this.asyncTaskLoaderImage, this.mHandler);
        this.pullToRefreshListView.setAdapter((ListAdapter) this.adapter);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AnnounceApplyManageActivity.this.back();
            }
        });
        this.pullToRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            public void onRefresh() {
                AnnounceApplyManageActivity.this.onRefreshs();
            }
        });
        this.pullToRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                AnnounceApplyManageActivity.this.onLoadMore();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void back() {
        if (this.myDialog == null || !this.myDialog.isShowing()) {
            finish();
        } else {
            hideProgressDialog();
        }
    }

    public void onRefreshs() {
        if (this.refreshAnnoListAsyncTask != null) {
            this.refreshAnnoListAsyncTask.cancel(true);
        }
        this.refreshAnnoListAsyncTask = new RefreshAnnoListAsyncTask();
        this.refreshAnnoListAsyncTask.execute(Integer.valueOf(this.pageSize));
    }

    public void onLoadMore() {
        if (this.moreAnnoListAsyncTask != null) {
            this.moreAnnoListAsyncTask.cancel(true);
        }
        this.moreAnnoListAsyncTask = new GetMoreAnnoListAsyncTask();
        this.moreAnnoListAsyncTask.execute(Integer.valueOf(this.pageSize));
    }

    private class RefreshAnnoListAsyncTask extends AsyncTask<Object, Void, List<AnnoModel>> {
        private RefreshAnnoListAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<AnnoModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            int unused = AnnounceApplyManageActivity.this.currentPage = 1;
            AnnounceApplyManageActivity.this.pullToRefreshListView.setSelection(0);
            if (AnnounceApplyManageActivity.this.refreshimgUrls != null && !AnnounceApplyManageActivity.this.refreshimgUrls.isEmpty() && AnnounceApplyManageActivity.this.refreshimgUrls.size() > 0) {
                AnnounceApplyManageActivity.this.asyncTaskLoaderImage.recycleBitmaps(AnnounceApplyManageActivity.this.refreshimgUrls);
            }
        }

        /* access modifiers changed from: protected */
        public List<AnnoModel> doInBackground(Object... params) {
            List unused = AnnounceApplyManageActivity.this.annoList = AnnounceApplyManageActivity.this.announceService.getAnnoList(AnnounceApplyManageActivity.this.currentPage, ((Integer) params[0]).intValue());
            return AnnounceApplyManageActivity.this.annoList;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<AnnoModel> annoModelList) {
            AnnounceApplyManageActivity.this.pullToRefreshListView.onRefreshComplete();
            if (annoModelList == null) {
                AnnounceApplyManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else if (annoModelList.isEmpty()) {
                AnnounceApplyManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(annoModelList.get(0).getErrorCode())) {
                Toast.makeText(AnnounceApplyManageActivity.this, MCForumErrorUtil.convertErrorCode(AnnounceApplyManageActivity.this, annoModelList.get(0).getErrorCode()), 0).show();
                AnnounceApplyManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                List unused = AnnounceApplyManageActivity.this.refreshimgUrls = AnnounceApplyManageActivity.this.getRefreshImgUrl(annoModelList);
                AnnounceApplyManageActivity.this.updateDataOnPostExecute(annoModelList);
                AnnounceApplyManageActivity.this.adapter.setAnnoList(annoModelList);
                AnnounceApplyManageActivity.this.adapter.notifyDataSetInvalidated();
                AnnounceApplyManageActivity.this.adapter.notifyDataSetChanged();
                if (annoModelList.get(0).getHasNext() == 1) {
                    AnnounceApplyManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    AnnounceApplyManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
                }
                List unused2 = AnnounceApplyManageActivity.this.annoList = annoModelList;
            }
        }
    }

    private class GetMoreAnnoListAsyncTask extends AsyncTask<Object, Void, List<AnnoModel>> {
        private GetMoreAnnoListAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<AnnoModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            AnnounceApplyManageActivity.access$208(AnnounceApplyManageActivity.this);
        }

        /* access modifiers changed from: protected */
        public List<AnnoModel> doInBackground(Object... params) {
            List unused = AnnounceApplyManageActivity.this.annoList = AnnounceApplyManageActivity.this.announceService.getAnnoList(AnnounceApplyManageActivity.this.currentPage, ((Integer) params[0]).intValue());
            return AnnounceApplyManageActivity.this.annoList;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<AnnoModel> annoModelList) {
            if (annoModelList == null) {
                AnnounceApplyManageActivity.access$210(AnnounceApplyManageActivity.this);
            } else if (annoModelList.isEmpty()) {
                AnnounceApplyManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
            } else if (!StringUtil.isEmpty(annoModelList.get(0).getErrorCode())) {
                AnnounceApplyManageActivity.access$210(AnnounceApplyManageActivity.this);
                Toast.makeText(AnnounceApplyManageActivity.this, MCForumErrorUtil.convertErrorCode(AnnounceApplyManageActivity.this, annoModelList.get(0).getErrorCode()), 0).show();
                AnnounceApplyManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
            } else {
                AnnounceApplyManageActivity.this.updateDataOnPostExecute(annoModelList);
                List<AnnoModel> tempList = new ArrayList<>();
                tempList.addAll(annoModelList);
                tempList.addAll(annoModelList);
                AnnounceApplyManageActivity.this.adapter.setAnnoList(tempList);
                AnnounceApplyManageActivity.this.adapter.notifyDataSetInvalidated();
                AnnounceApplyManageActivity.this.adapter.notifyDataSetChanged();
                if (annoModelList.get(0).getHasNext() == 1) {
                    AnnounceApplyManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(0);
                } else {
                    AnnounceApplyManageActivity.this.pullToRefreshListView.onBottomRefreshComplete(3);
                }
                List unused = AnnounceApplyManageActivity.this.annoList = tempList;
            }
        }
    }

    public void updateDataOnPostExecute(List<AnnoModel> result) {
        List<AnnoModel> annoList2 = result;
        int size = annoList2.size();
        for (int i = 0; i < size; i++) {
            annoList2.get(i).setPage(this.currentPage);
        }
    }

    /* access modifiers changed from: private */
    public List<String> getRefreshImgUrl(List<AnnoModel> result) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < this.annoList.size(); i++) {
            AnnoModel model = this.annoList.get(i);
            if (!isExit(model, result)) {
                list.add(AsyncTaskLoaderImage.formatUrl(model.getIcon(), "100x100"));
            }
        }
        return list;
    }

    private boolean isExit(AnnoModel model, List<AnnoModel> result) {
        int j = result.size();
        for (int i = 0; i < j; i++) {
            AnnoModel model2 = result.get(i);
            if (!StringUtil.isEmpty(model.getIcon()) && !StringUtil.isEmpty(model2.getIcon()) && model.getIcon().equals(model2.getIcon())) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return getImageURL(0, this.annoList.size());
    }

    /* access modifiers changed from: protected */
    public List<String> getImageURL(int start, int end) {
        List<String> imgUrls = new ArrayList<>();
        for (int i = start; i < end; i++) {
            imgUrls.add(AsyncTaskLoaderImage.formatUrl(this.annoList.get(i).getIcon(), "100x100"));
        }
        return imgUrls;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.refreshAnnoListAsyncTask != null) {
            this.refreshAnnoListAsyncTask.cancel(true);
        }
        if (this.moreAnnoListAsyncTask != null) {
            this.moreAnnoListAsyncTask.cancel(true);
        }
        this.adapter.destroy();
    }
}
