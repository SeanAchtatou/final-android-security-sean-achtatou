package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.adapter.holder.MentionFriendAdapterHolder;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.UserInfoModel;
import java.util.List;

public class MetionFriendAdapter extends BaseAdapter {
    private MCResource forumResource;
    private MentionFriendAdapterHolder holder;
    private LayoutInflater inflater;
    private List<UserInfoModel> userInfoList;

    public List<UserInfoModel> getUserInfoList() {
        return this.userInfoList;
    }

    public void setUserInfoList(List<UserInfoModel> userInfoList2) {
        this.userInfoList = userInfoList2;
    }

    public MetionFriendAdapter(Context context, List<UserInfoModel> userInfoList2) {
        this.userInfoList = userInfoList2;
        this.inflater = LayoutInflater.from(context);
        this.forumResource = MCResource.getInstance(context);
    }

    public int getCount() {
        return this.userInfoList.size();
    }

    public Object getItem(int position) {
        return this.userInfoList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        UserInfoModel infoModel = this.userInfoList.get(position);
        if (infoModel.getUserId() == -1) {
            if (infoModel.getRoleNum() == 8 || infoModel.getRoleNum() == 16) {
                View convertView2 = this.inflater.inflate(this.forumResource.getLayoutId("mc_forum_mention_friends_divider_item"), (ViewGroup) null);
                ((TextView) convertView2.findViewById(this.forumResource.getViewId("mc_forum_posts_mention_friends_title_text"))).setText(this.forumResource.getStringId("mc_forum_posts_mention_friend_admin"));
                return convertView2;
            } else if (infoModel.getRoleNum() == 2) {
                View convertView3 = this.inflater.inflate(this.forumResource.getLayoutId("mc_forum_mention_friends_divider_item"), (ViewGroup) null);
                ((TextView) convertView3.findViewById(this.forumResource.getViewId("mc_forum_posts_mention_friends_title_text"))).setText(this.forumResource.getStringId("mc_forum_my_friends"));
                return convertView3;
            } else if (infoModel.getRoleNum() == -1) {
                return this.inflater.inflate(this.forumResource.getLayoutId("mc_forum_mention_friends_divider_item"), (ViewGroup) null);
            }
        }
        if (convertView == null) {
            convertView = this.inflater.inflate(this.forumResource.getLayoutId("mc_forum_mention_friend_item"), (ViewGroup) null);
            this.holder = new MentionFriendAdapterHolder();
            this.holder.setNameText((TextView) convertView.findViewById(this.forumResource.getViewId("mc_forum_mention_friend_name_text")));
            convertView.setTag(this.holder);
        } else {
            this.holder = (MentionFriendAdapterHolder) convertView.getTag();
        }
        if (this.holder == null) {
            convertView = this.inflater.inflate(this.forumResource.getLayoutId("mc_forum_mention_friend_item"), (ViewGroup) null);
            this.holder = new MentionFriendAdapterHolder();
            this.holder.setNameText((TextView) convertView.findViewById(this.forumResource.getViewId("mc_forum_mention_friend_name_text")));
            convertView.setTag(this.holder);
        }
        this.holder.getNameText().setText(infoModel.getNickname());
        return convertView;
    }
}
