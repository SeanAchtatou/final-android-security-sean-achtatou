package com.mobcent.base.android.ui.activity.view;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.base.forum.android.util.MCThemeResource;
import java.util.Calendar;

public class MCPullDownView extends FrameLayout implements GestureDetector.OnGestureListener {
    public static int MAX_LENGHT = 0;
    public static int MAX_PADDING = 120;
    public static final long PROGRESS_BAR_DELAY = 100;
    public static final int STATE_CLOSE = 1;
    public static final int STATE_OPEN = 2;
    public static final int STATE_OPEN_MAX = 4;
    public static final int STATE_OPEN_MAX_RELEASE = 5;
    public static final int STATE_OPEN_RELEASE = 3;
    public static final int STATE_UPDATE = 6;
    public static final int STATE_UPDATE_SCROLL = 7;
    public final String TAG = "MCPullDownView";
    private Animation mAnimationDown;
    private Animation mAnimationUp;
    private ImageView mArrow;
    private long mDate;
    private GestureDetector mDetector;
    private boolean mIsAutoScroller;
    private int mPading;
    /* access modifiers changed from: private */
    public MCProgressBar mProgressBar;
    private int mState;
    private TextView mTitle;
    private UpdateHandle mUpdateHandle;
    private View mView;
    private int marginTop = 0;
    private boolean menable = true;
    private MCResource resource;
    private MCThemeResource themeResource;

    public interface UpdateHandle {
        void onRefresh();
    }

    public MCPullDownView(Context paramContext) {
        super(paramContext);
        init();
        addUpdateBar();
    }

    public MCPullDownView(Context paramContext, AttributeSet paramAttributeSet) {
        super(paramContext, paramAttributeSet);
        init();
        addUpdateBar();
    }

    private void init() {
        this.mDetector = new GestureDetector(this);
        this.mState = 1;
        this.resource = MCResource.getInstance(getContext());
        this.themeResource = MCThemeResource.getInstance(getContext(), "packageName");
        MAX_LENGHT = getResources().getDimensionPixelSize(this.resource.getDimenId("mc_forum_pull_down_height"));
        setDrawingCacheEnabled(false);
        setBackgroundDrawable(null);
        setClipChildren(false);
        this.mDetector.setIsLongpressEnabled(true);
        this.mDate = Calendar.getInstance().getTimeInMillis();
    }

    private void addUpdateBar() {
        this.mAnimationUp = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);
        this.mAnimationUp.setInterpolator(new LinearInterpolator());
        this.mAnimationUp.setDuration(250);
        this.mAnimationUp.setFillAfter(true);
        this.mAnimationDown = new RotateAnimation(-180.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.mAnimationDown.setInterpolator(new LinearInterpolator());
        this.mAnimationDown.setDuration(250);
        this.mAnimationDown.setFillAfter(true);
        this.mView = LayoutInflater.from(getContext()).inflate(this.resource.getLayoutId("mc_forum_widget_pull_down_view"), (ViewGroup) null);
        this.mView.setVisibility(4);
        addView(this.mView);
        this.mArrow = (ImageView) this.mView.findViewById(this.resource.getViewId("mc_forum_arrow_img"));
        this.mProgressBar = (MCProgressBar) this.mView.findViewById(this.resource.getViewId("mc_forum_progress_bar"));
        onTheme();
    }

    public void onTheme() {
        this.mTitle = (TextView) this.mView.findViewById(this.resource.getViewId("mc_forum_tv_title"));
        this.mTitle.setTextColor(this.themeResource.getColor("mc_forum_pull_down_refresh_text_color"));
        this.mArrow.setImageDrawable(this.themeResource.getDrawable("mc_forum_refresh"));
        this.mProgressBar.setBackgroundDrawable(this.themeResource.getDrawable("mc_forum_loading1"));
    }

    /* access modifiers changed from: protected */
    public boolean move(float paramFloat, boolean paramBoolean) {
        boolean b = false;
        this.mPading = (int) (((float) this.mPading) + paramFloat);
        if (this.mPading > 0) {
            return false;
        }
        if (!paramBoolean) {
            if (this.mState == 5) {
                this.mState = 6;
                if (this.mUpdateHandle != null) {
                    this.mUpdateHandle.onRefresh();
                }
            }
            if (this.mState == 6 && this.mPading == 0) {
                this.mState = 1;
            }
            if (this.mState == 3 && this.mPading == 0) {
                this.mState = 1;
            }
            if (!(this.mState == 7 && this.mPading == 0)) {
                this.mState = 7;
            }
        }
        switch (this.mState) {
            case 1:
                this.mProgressBar.hide();
                if (this.mPading <= 0) {
                    this.mState = 2;
                    this.mArrow.setVisibility(0);
                    this.mArrow.setAnimation(this.mAnimationDown);
                    updateTitle(getResources().getString(this.resource.getStringId("mc_forum_drop_dowm")));
                    updateView();
                }
                b = true;
                break;
            case 2:
                if (Math.abs(this.mPading) >= MAX_LENGHT) {
                    this.mState = 4;
                    this.mArrow.setVisibility(0);
                    this.mArrow.clearAnimation();
                    this.mArrow.startAnimation(this.mAnimationUp);
                    updateTitle(getResources().getString(this.resource.getStringId("mc_forum_release_update")));
                }
                updateView();
                b = true;
                break;
            case 4:
                if (Math.abs(this.mPading) <= MAX_LENGHT) {
                    this.mState = 2;
                    this.mArrow.setVisibility(0);
                    this.mArrow.clearAnimation();
                    this.mArrow.startAnimation(this.mAnimationDown);
                    updateTitle(getResources().getString(this.resource.getStringId("mc_forum_drop_dowm")));
                    updateView();
                }
                b = true;
                break;
            case 6:
            case 7:
                this.mArrow.clearAnimation();
                this.mArrow.setVisibility(4);
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        MCPullDownView.this.mProgressBar.show();
                    }
                }, 100);
                updateView();
                break;
        }
        if (this.mPading == 0) {
            this.mState = 1;
            updateView();
            b = true;
        }
        return b;
    }

    private boolean release() {
        boolean b = false;
        if (this.mPading >= 0) {
            b = false;
        }
        switch (this.mState) {
            case 2:
            case 3:
            case 4:
            case 5:
                if (Math.abs(this.mPading) < MAX_LENGHT) {
                    this.mState = 3;
                    scrollToClose();
                } else {
                    this.mState = 5;
                    scrollToUpdate();
                }
                return true;
            default:
                return b;
        }
    }

    private void scrollToClose() {
        this.mState = 1;
        this.mPading = 0;
        updateView();
    }

    private void scrollToUpdate() {
        this.mState = 7;
        this.mPading = -MAX_LENGHT;
        updateView();
        if (this.mUpdateHandle != null) {
            this.mUpdateHandle.onRefresh();
        }
    }

    private void updateTitle(String s) {
        StringBuilder sb = new StringBuilder();
        sb.append(s);
        String time = MCStringBundleUtil.timeToString(getContext(), this.mDate);
        if (time != null && time.trim().length() > 0) {
            sb.append("\n").append(getResources().getString(this.resource.getStringId("mc_forum_update_time")) + time);
        }
        this.mTitle.setText(sb);
    }

    private void updateView() {
        View pullView = getChildAt(0);
        View listView = getChildAt(1);
        if (this.marginTop == 0) {
            this.marginTop = ((ViewGroup.MarginLayoutParams) getLayoutParams()).topMargin;
        }
        if (this.mState == 1) {
            if (pullView.getVisibility() != 4) {
                pullView.setVisibility(4);
            }
            listView.offsetTopAndBottom(-listView.getTop());
        } else if (this.mState == 2 || this.mState == 4) {
            listView.offsetTopAndBottom((-this.mPading) - listView.getTop());
            if (pullView.getVisibility() != 0) {
                pullView.setVisibility(0);
            }
            pullView.offsetTopAndBottom(((-MAX_LENGHT) - this.mPading) - pullView.getTop());
        } else if (this.mState == 3 || this.mState == 5) {
            listView.offsetTopAndBottom((-this.mPading) - listView.getTop());
            if (pullView.getVisibility() != 0) {
                pullView.setVisibility(0);
            }
            pullView.offsetTopAndBottom(((-MAX_LENGHT) - this.mPading) - pullView.getTop());
            updateTitle(getResources().getString(this.resource.getStringId("mc_forum_release_update")));
        } else if (this.mState == 6 || this.mState == 7) {
            listView.offsetTopAndBottom((-this.mPading) - listView.getTop());
            if (pullView.getVisibility() != 0) {
                pullView.setVisibility(0);
            }
            if (this.mArrow.getVisibility() != 4) {
                this.mArrow.clearAnimation();
                this.mArrow.setVisibility(4);
            }
            if (this.mProgressBar.getVisibility() != 0) {
                this.mProgressBar.show();
            }
            pullView.offsetTopAndBottom(((-MAX_LENGHT) - this.mPading) - pullView.getTop());
            updateTitle(getResources().getString(this.resource.getStringId("mc_forum_doing_update")));
        }
        invalidate();
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        boolean b = false;
        if (!this.menable) {
            return super.dispatchTouchEvent(ev);
        }
        if (this.mIsAutoScroller) {
            return super.dispatchTouchEvent(ev);
        }
        if (this.mState == 7 || this.mState == 6) {
            update();
            return true;
        }
        boolean b2 = this.mDetector.onTouchEvent(ev);
        if (b2 || this.mPading != 0) {
            if (ev.getAction() == 1) {
                return release();
            }
            if (!(this.mState == 6 || this.mState == 7 || ((!b2 && this.mState != 2 && this.mState != 4 && this.mState != 5 && this.mState != 3) || getChildAt(1).getTop() == 0))) {
                ev.setAction(3);
                return true;
            }
        }
        if (0 == 0) {
            this.mPading = 0;
            updateView();
            b = super.dispatchTouchEvent(ev);
        }
        return b;
    }

    public void endUpdate(long date) {
        this.mDate = date;
        if (this.mPading != 0) {
            scrollToClose();
        }
        this.mState = 1;
    }

    public void end() {
        if (this.mPading != 0) {
            scrollToClose();
        }
        this.mState = 1;
    }

    public boolean onDown(MotionEvent e) {
        return false;
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        AdapterView<?> localAdapterView;
        float f = (float) (((double) distanceY) * 0.5d);
        try {
            localAdapterView = (AdapterView) getChildAt(1);
        } catch (ClassCastException e) {
            localAdapterView = (AdapterView) ((ViewGroup) getChildAt(1)).getChildAt(0);
        }
        boolean bool = false;
        if (localAdapterView.getCount() == 0) {
            bool = false;
        }
        if (localAdapterView.getFirstVisiblePosition() == 0) {
            if (localAdapterView == null || localAdapterView.getCount() <= 0 || localAdapterView.getChildAt(0) == null || localAdapterView.getChildAt(0).getTop() == 0) {
                bool = move(f, true);
            } else if (this.mPading >= 0) {
                return false;
            }
        }
        return bool;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
        getChildAt(0).layout(0, (-MAX_LENGHT) - this.mPading, getMeasuredWidth(), -this.mPading);
        getChildAt(1).layout(0, -this.mPading, getMeasuredWidth(), getMeasuredHeight() - this.mPading);
    }

    public void setUpdateDate(long date) {
        this.mDate = date;
    }

    public void setUpdateHandle(UpdateHandle paramUpdateHandle) {
        this.mUpdateHandle = paramUpdateHandle;
    }

    public void update() {
        this.mPading = -MAX_LENGHT;
        this.mState = 7;
        updateView();
    }

    public void updateWithoutOffset() {
        this.mState = 7;
        invalidate();
    }

    public int getState() {
        return this.mState;
    }

    public boolean isEnable() {
        return this.menable;
    }

    public void setEnable(boolean menable2) {
        this.menable = menable2;
    }
}
