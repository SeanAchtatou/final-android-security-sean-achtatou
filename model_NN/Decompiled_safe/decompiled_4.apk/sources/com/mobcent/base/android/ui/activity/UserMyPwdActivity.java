package com.mobcent.base.android.ui.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.MD5Util;
import com.mobcent.forum.android.util.StringUtil;

public class UserMyPwdActivity extends BaseActivity {
    private final String TAG = "PasswordSettingActivity";
    private Button backBtn;
    /* access modifiers changed from: private */
    public ChangePwdAsynTask changePwdAsynTask;
    /* access modifiers changed from: private */
    public boolean isShowNewPwd = true;
    /* access modifiers changed from: private */
    public boolean isShowOldPwd = true;
    /* access modifiers changed from: private */
    public EditText newPasswordEdit;
    /* access modifiers changed from: private */
    public EditText oldPasswordEdit;
    /* access modifiers changed from: private */
    public int pwdMaxLen = 20;
    /* access modifiers changed from: private */
    public int pwdMinLen = 6;
    private Button savePasswordBtn;
    /* access modifiers changed from: private */
    public ImageButton showNewPwdImgBtn;
    /* access modifiers changed from: private */
    public ImageButton showOldPwdImgBtn;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_password_setting_activity"));
        this.newPasswordEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_new_password_edit"));
        this.oldPasswordEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_old_password_edit"));
        this.savePasswordBtn = (Button) findViewById(this.resource.getViewId("mc_forum_save_btn"));
        this.showOldPwdImgBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_old_password_imgBtn"));
        this.showNewPwdImgBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_new_password_imgBtn"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                UserMyPwdActivity.this.back();
            }
        });
        this.showOldPwdImgBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!UserMyPwdActivity.this.isShowOldPwd) {
                    UserMyPwdActivity.this.showOldPwdImgBtn.setBackgroundResource(UserMyPwdActivity.this.resource.getDrawableId("mc_forum_input_show_pwd_n"));
                    UserMyPwdActivity.this.oldPasswordEdit.setInputType(129);
                    Editable etable = UserMyPwdActivity.this.oldPasswordEdit.getText();
                    Selection.setSelection(etable, etable.length());
                    boolean unused = UserMyPwdActivity.this.isShowOldPwd = true;
                    return;
                }
                UserMyPwdActivity.this.showOldPwdImgBtn.setBackgroundResource(UserMyPwdActivity.this.resource.getDrawableId("mc_forum_input_show_pwd_d"));
                UserMyPwdActivity.this.oldPasswordEdit.setInputType(144);
                Editable etable2 = UserMyPwdActivity.this.oldPasswordEdit.getText();
                Selection.setSelection(etable2, etable2.length());
                boolean unused2 = UserMyPwdActivity.this.isShowOldPwd = false;
            }
        });
        this.showNewPwdImgBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!UserMyPwdActivity.this.isShowNewPwd) {
                    UserMyPwdActivity.this.showNewPwdImgBtn.setBackgroundResource(UserMyPwdActivity.this.resource.getDrawableId("mc_forum_input_show_pwd_n"));
                    UserMyPwdActivity.this.newPasswordEdit.setInputType(129);
                    Editable etable = UserMyPwdActivity.this.newPasswordEdit.getText();
                    Selection.setSelection(etable, etable.length());
                    boolean unused = UserMyPwdActivity.this.isShowNewPwd = true;
                    return;
                }
                UserMyPwdActivity.this.showNewPwdImgBtn.setBackgroundResource(UserMyPwdActivity.this.resource.getDrawableId("mc_forum_input_show_pwd_d"));
                UserMyPwdActivity.this.newPasswordEdit.setInputType(144);
                Editable etable2 = UserMyPwdActivity.this.newPasswordEdit.getText();
                Selection.setSelection(etable2, etable2.length());
                boolean unused2 = UserMyPwdActivity.this.isShowNewPwd = false;
            }
        });
        this.savePasswordBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String oldPassStr = UserMyPwdActivity.this.oldPasswordEdit.getText().toString();
                String newPassStr = UserMyPwdActivity.this.newPasswordEdit.getText().toString();
                MCLogUtil.d("PasswordSettingActivity", "The old password is:" + oldPassStr + ",The new password is:" + newPassStr);
                if (newPassStr.length() < UserMyPwdActivity.this.pwdMinLen) {
                    UserMyPwdActivity.this.warnMessageById("mc_forum_password_over_short_warn");
                } else if (newPassStr.length() > UserMyPwdActivity.this.pwdMaxLen) {
                    UserMyPwdActivity.this.warnMessageById("mc_forum_password_over_length_warn");
                } else if (!StringUtil.isPwdMatchRule(newPassStr)) {
                    UserMyPwdActivity.this.warnMessageById("mc_forum_new_password_invalid_tip");
                } else if (!StringUtil.isPwdMatchRule(oldPassStr)) {
                    UserMyPwdActivity.this.warnMessageById("mc_forum_old_password_invalid_tip");
                } else {
                    String oldPassStr2 = MD5Util.toMD5(oldPassStr);
                    String newPassStr2 = MD5Util.toMD5(newPassStr);
                    if (UserMyPwdActivity.this.changePwdAsynTask != null) {
                        UserMyPwdActivity.this.changePwdAsynTask.cancel(true);
                    }
                    ChangePwdAsynTask unused = UserMyPwdActivity.this.changePwdAsynTask = new ChangePwdAsynTask();
                    UserMyPwdActivity.this.changePwdAsynTask.execute(oldPassStr2, newPassStr2);
                }
            }
        });
    }

    private class ChangePwdAsynTask extends AsyncTask<String, Void, UserInfoModel> {
        private ChangePwdAsynTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            UserMyPwdActivity.this.showProgressDialog("mc_forum_warn_change_pwd", this);
        }

        /* access modifiers changed from: protected */
        public UserInfoModel doInBackground(String... params) {
            UserService userService = new UserServiceImpl(UserMyPwdActivity.this);
            if (params[0] == null && params[1] == null) {
                return null;
            }
            return userService.changeUserPwd(params[0].toUpperCase(), params[1].toUpperCase());
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(UserInfoModel result) {
            UserMyPwdActivity.this.hideProgressDialog();
            if (result == null) {
                return;
            }
            if (result.getErrorCode() == null || "".equals(result.getErrorCode())) {
                UserMyPwdActivity.this.warnMessageById("mc_forum_change_password_succ");
                UserMyPwdActivity.this.finish();
                return;
            }
            UserMyPwdActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(UserMyPwdActivity.this, result.getErrorCode()));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.changePwdAsynTask != null) {
            this.changePwdAsynTask.cancel(true);
        }
    }
}
