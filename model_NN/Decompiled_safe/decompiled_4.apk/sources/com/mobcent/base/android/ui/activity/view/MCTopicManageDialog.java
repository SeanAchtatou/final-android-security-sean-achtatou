package com.mobcent.base.android.ui.activity.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.ReportActivity;
import com.mobcent.base.android.ui.activity.RepostTopicActivity;
import com.mobcent.base.android.ui.activity.adapter.PostsListAdapter;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.CloseTopicAsyncTask;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.DeleteTopicTask;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.EssenceTopicAsyncTask;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.PhysicalDelTopicAsyncTask;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.TopTopicAsyncTask;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.base.android.ui.activity.delegate.TopicManageDelegate;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.forum.android.util.MCForumReverseList;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.model.ReplyModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.ModeratorService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.ModeratorServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MCTopicManageDialog extends Dialog implements MCConstant {
    /* access modifiers changed from: private */
    public long boardId;
    /* access modifiers changed from: private */
    public String boardName;
    /* access modifiers changed from: private */
    public CloseTopicAsyncTask closeTopicAsyncTask;
    /* access modifiers changed from: private */
    public Context context;
    private long currentUserId;
    /* access modifiers changed from: private */
    public PhysicalDelTopicAsyncTask delTopicAsyncTask;
    /* access modifiers changed from: private */
    public EssenceTopicAsyncTask essenceTopicAsyncTask;
    String[] function = null;
    private boolean isForwardTopic;
    private ModeratorService moderatorService;
    private ReplyModel replyModel;
    /* access modifiers changed from: private */
    public MCResource resource;
    /* access modifiers changed from: private */
    public TopTopicAsyncTask topTopicAsyncTask;
    /* access modifiers changed from: private */
    public AlertDialog.Builder topicFunctionDialog;
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener topicFunctionItemListener;
    /* access modifiers changed from: private */
    public List<String> topicFunctionList = null;
    /* access modifiers changed from: private */
    public TopicManageDelegate topicManageDelegate;
    private TopicModel topicModel;
    private int type;
    private UserService userService;

    public MCTopicManageDialog(Context context2) {
        super(context2);
        this.context = context2;
    }

    public MCTopicManageDialog(Context context2, long boardId2, String boardName2, TopicModel topicModel2, ReplyModel replyModel2, int type2, TopicManageDelegate topicManageDelegate2, boolean isForwardTopic2) {
        super(context2);
        this.context = context2;
        this.userService = new UserServiceImpl(context2);
        this.moderatorService = new ModeratorServiceImpl(context2);
        this.currentUserId = this.userService.getLoginUserId();
        this.resource = MCResource.getInstance(context2);
        this.boardId = boardId2;
        this.boardName = boardName2;
        this.topicModel = topicModel2;
        this.replyModel = replyModel2;
        this.type = type2;
        this.topicManageDelegate = topicManageDelegate2;
        this.isForwardTopic = isForwardTopic2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.type == 1) {
            initTopicFunctionDialog(this.topicModel);
        } else {
            initRplyFunctionDialog(this.replyModel);
        }
    }

    private void initTopicFunctionDialog(final TopicModel topicModel2) {
        if (this.topicFunctionDialog == null || this.topicFunctionList == null) {
            this.topicFunctionList = new ArrayList();
            if (this.isForwardTopic) {
                this.topicFunctionList.add(this.resource.getString("mc_forum_topic_function_forward"));
            }
            if (isShowAdmin(topicModel2.getRoleNum())) {
                if (topicModel2.getTop() == 1) {
                    this.topicFunctionList.add(this.resource.getString("mc_forum_cancel_top"));
                } else {
                    this.topicFunctionList.add(this.resource.getString("mc_forum_top"));
                }
                if (topicModel2.getEssence() == 1) {
                    this.topicFunctionList.add(this.resource.getString("mc_forum_cancel_essence"));
                } else {
                    this.topicFunctionList.add(this.resource.getString("mc_forum_add_essence"));
                }
                if (topicModel2.getStatus() == PostsListAdapter.POSTS_STATUS_CLOSE) {
                    this.topicFunctionList.add(this.resource.getString("mc_forum_cancel_close"));
                } else {
                    this.topicFunctionList.add(this.resource.getString("mc_forum_close"));
                }
                this.topicFunctionList.add(this.resource.getString("mc_forum_topic_function_delete"));
                this.topicFunctionList.add(this.resource.getString("mc_forum_topic_function_delete_topic"));
            } else if (topicModel2.getUserId() == this.userService.getLoginUserId()) {
                this.topicFunctionList.add(this.resource.getString("mc_forum_topic_function_delete"));
            } else {
                this.topicFunctionList.add(this.resource.getString("mc_forum_topic_function_report_topic"));
                this.topicFunctionList.add(this.resource.getString("mc_forum_topic_function_report_user"));
            }
            this.topicFunctionItemListener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (((String) MCTopicManageDialog.this.topicFunctionList.get(which)).equals(MCTopicManageDialog.this.resource.getString("mc_forum_topic_function_forward"))) {
                        HashMap<String, Serializable> param = new HashMap<>();
                        param.put(MCConstant.TOPICMODEL, topicModel2);
                        param.put("boardName", MCTopicManageDialog.this.boardName);
                        if (LoginInterceptor.doInterceptorByDialog(MCTopicManageDialog.this.context, MCTopicManageDialog.this.resource, RepostTopicActivity.class, param)) {
                            Intent intent = new Intent(MCTopicManageDialog.this.context, RepostTopicActivity.class);
                            intent.putExtra(MCConstant.TOPICMODEL, topicModel2);
                            intent.putExtra("boardName", MCTopicManageDialog.this.boardName);
                            intent.putExtra(MCConstant.REPOST_BOARD_ID, MCTopicManageDialog.this.boardId);
                            MCTopicManageDialog.this.context.startActivity(intent);
                        }
                    } else if (((String) MCTopicManageDialog.this.topicFunctionList.get(which)).equals(MCTopicManageDialog.this.resource.getString("mc_forum_topic_function_delete"))) {
                        MCTopicManageDialog.this.initExitAlertDialog(1, MCTopicManageDialog.this.boardId, topicModel2.getTopicId(), topicModel2).show();
                    } else if (((String) MCTopicManageDialog.this.topicFunctionList.get(which)).equals(MCTopicManageDialog.this.resource.getString("mc_forum_topic_function_report_topic"))) {
                        HashMap<String, Serializable> params = new HashMap<>();
                        params.put(MCConstant.REPORT_TYPE, 1);
                        params.put("oid", Long.valueOf(topicModel2.getTopicId()));
                        if (LoginInterceptor.doInterceptorByDialog(MCTopicManageDialog.this.context, MCTopicManageDialog.this.resource, ReportActivity.class, params)) {
                            Intent intent2 = new Intent(MCTopicManageDialog.this.context, ReportActivity.class);
                            intent2.putExtra(MCConstant.REPORT_TYPE, 1);
                            intent2.putExtra("oid", topicModel2.getTopicId());
                            MCTopicManageDialog.this.context.startActivity(intent2);
                        }
                    } else if (((String) MCTopicManageDialog.this.topicFunctionList.get(which)).equals(MCTopicManageDialog.this.resource.getString("mc_forum_topic_function_report_user"))) {
                        HashMap<String, Serializable> params2 = new HashMap<>();
                        params2.put(MCConstant.REPORT_TYPE, 3);
                        params2.put("oid", Long.valueOf(topicModel2.getUserId()));
                        if (LoginInterceptor.doInterceptorByDialog(MCTopicManageDialog.this.context, MCTopicManageDialog.this.resource, ReportActivity.class, params2)) {
                            Intent intent3 = new Intent(MCTopicManageDialog.this.context, ReportActivity.class);
                            intent3.putExtra(MCConstant.REPORT_TYPE, 3);
                            intent3.putExtra("oid", topicModel2.getUserId());
                            MCTopicManageDialog.this.context.startActivity(intent3);
                        }
                    } else if (((String) MCTopicManageDialog.this.topicFunctionList.get(which)).equals(MCTopicManageDialog.this.resource.getString("mc_forum_top"))) {
                        MCTopicManageDialog.this.setTopicTop(topicModel2);
                    } else if (((String) MCTopicManageDialog.this.topicFunctionList.get(which)).equals(MCTopicManageDialog.this.resource.getString("mc_forum_cancel_top"))) {
                        MCTopicManageDialog.this.cancelTopicTop(topicModel2);
                    } else if (((String) MCTopicManageDialog.this.topicFunctionList.get(which)).equals(MCTopicManageDialog.this.resource.getString("mc_forum_add_essence"))) {
                        MCTopicManageDialog.this.addEssenceTopicTop(topicModel2);
                    } else if (((String) MCTopicManageDialog.this.topicFunctionList.get(which)).equals(MCTopicManageDialog.this.resource.getString("mc_forum_cancel_essence"))) {
                        MCTopicManageDialog.this.cancelEssenceTopicTop(topicModel2);
                    } else if (((String) MCTopicManageDialog.this.topicFunctionList.get(which)).equals(MCTopicManageDialog.this.resource.getString("mc_forum_close"))) {
                        MCTopicManageDialog.this.closeTopic(topicModel2);
                    } else if (((String) MCTopicManageDialog.this.topicFunctionList.get(which)).equals(MCTopicManageDialog.this.resource.getString("mc_forum_cancel_close"))) {
                        MCTopicManageDialog.this.cancelcloseTopic(topicModel2);
                    } else if (((String) MCTopicManageDialog.this.topicFunctionList.get(which)).equals(MCTopicManageDialog.this.resource.getString("mc_forum_topic_function_delete_topic"))) {
                        MCTopicManageDialog.this.physicalDelTopic(topicModel2);
                    }
                }
            };
            this.topicFunctionDialog = new AlertDialog.Builder(this.context).setTitle(this.resource.getString("mc_forum_topic_function")).setItems(MCForumReverseList.convertListToArray(this.topicFunctionList), this.topicFunctionItemListener);
        }
    }

    private void initRplyFunctionDialog(final ReplyModel replyModel2) {
        if (isShowAdmin(replyModel2.getRoleNum())) {
            this.function = new String[1];
            this.function[0] = this.resource.getString("mc_forum_topic_function_delete");
        } else if (replyModel2.getReplyUserId() == this.currentUserId) {
            this.function = new String[1];
            this.function[0] = this.resource.getString("mc_forum_topic_function_delete");
        } else {
            this.function = new String[2];
            this.function[0] = this.resource.getString("mc_forum_topic_function_report_topic");
            this.function[1] = this.resource.getString("mc_forum_topic_function_report_user");
        }
        new AlertDialog.Builder(this.context).setTitle(this.resource.getString("mc_forum_topic_function")).setItems(this.function, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (MCTopicManageDialog.this.function[which].equals(MCTopicManageDialog.this.resource.getString("mc_forum_topic_function_delete"))) {
                    MCTopicManageDialog.this.initExitAlertDialog(0, MCTopicManageDialog.this.boardId, replyModel2.getReplyPostsId(), null).show();
                } else if (MCTopicManageDialog.this.function[which].equals(MCTopicManageDialog.this.resource.getString("mc_forum_topic_function_report_topic"))) {
                    HashMap<String, Serializable> params = new HashMap<>();
                    params.put(MCConstant.REPORT_TYPE, 2);
                    params.put("oid", Long.valueOf(replyModel2.getReplyPostsId()));
                    if (LoginInterceptor.doInterceptorByDialog(MCTopicManageDialog.this.context, MCTopicManageDialog.this.resource, ReportActivity.class, params)) {
                        Intent intent = new Intent(MCTopicManageDialog.this.context, ReportActivity.class);
                        intent.putExtra(MCConstant.REPORT_TYPE, 2);
                        intent.putExtra("oid", replyModel2.getReplyPostsId());
                        MCTopicManageDialog.this.context.startActivity(intent);
                    }
                } else if (MCTopicManageDialog.this.function[which].equals(MCTopicManageDialog.this.resource.getString("mc_forum_topic_function_report_user"))) {
                    HashMap<String, Serializable> params2 = new HashMap<>();
                    params2.put(MCConstant.REPORT_TYPE, 3);
                    params2.put("oid", Long.valueOf(replyModel2.getReplyUserId()));
                    if (LoginInterceptor.doInterceptorByDialog(MCTopicManageDialog.this.context, MCTopicManageDialog.this.resource, ReportActivity.class, params2)) {
                        Intent intent2 = new Intent(MCTopicManageDialog.this.context, ReportActivity.class);
                        intent2.putExtra(MCConstant.REPORT_TYPE, 3);
                        intent2.putExtra("oid", replyModel2.getReplyUserId());
                        MCTopicManageDialog.this.context.startActivity(intent2);
                    }
                }
            }
        }).show();
    }

    public void show() {
        super.show();
        dismiss();
        if (this.topicFunctionDialog != null) {
            this.topicFunctionDialog.show();
        }
    }

    private boolean isShowAdmin(int roleNum) {
        if (this.userService.currentUserIsSuperAdmin()) {
            return true;
        }
        if (this.userService.currentUserIsAdmin() && roleNum != 16) {
            return true;
        }
        if (!this.moderatorService.BoardPermission(this.boardId, this.currentUserId)) {
            return false;
        }
        if (roleNum == 8 || roleNum == 16) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public AlertDialog.Builder initExitAlertDialog(int type2, long boardId2, long postsId, TopicModel topicModel2) {
        AlertDialog.Builder exitAlertDialog = new AlertDialog.Builder(this.context).setTitle(this.resource.getStringId("mc_forum_dialog_tip")).setMessage(this.resource.getStringId("mc_forum_warn_delete_topic"));
        final TopicModel topicModel3 = topicModel2;
        final int i = type2;
        final long j = boardId2;
        final long j2 = postsId;
        exitAlertDialog.setPositiveButton(this.resource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                DeleteTopicTask deleteTopicTask = new DeleteTopicTask(MCTopicManageDialog.this.context, topicModel3);
                deleteTopicTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                    public void executeSuccess() {
                        if (MCTopicManageDialog.this.topicManageDelegate != null) {
                            MCTopicManageDialog.this.topicManageDelegate.topicManageDelegate(1);
                        }
                    }

                    public void executeFail() {
                    }
                });
                deleteTopicTask.execute(Integer.valueOf(i), Long.valueOf(j), Long.valueOf(j2));
            }
        });
        exitAlertDialog.setNegativeButton(this.resource.getStringId("mc_forum_dialog_cancel"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        return exitAlertDialog;
    }

    /* access modifiers changed from: protected */
    public void setTopicTop(TopicModel topicModel2) {
        this.topTopicAsyncTask = new TopTopicAsyncTask(this.context, this.resource, topicModel2.getTopicId(), this.boardId, 1);
        this.topTopicAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
            public void executeSuccess() {
                MCTopicManageDialog.this.topicManageDelegate.topicManageDelegate(2);
                MCTopicManageDialog.this.topicFunctionList.set(MCTopicManageDialog.this.topicFunctionList.indexOf(MCTopicManageDialog.this.context.getResources().getString(MCTopicManageDialog.this.resource.getStringId("mc_forum_top"))), MCTopicManageDialog.this.context.getResources().getString(MCTopicManageDialog.this.resource.getStringId("mc_forum_cancel_top")));
                MCTopicManageDialog.this.topicFunctionDialog.setItems(MCForumReverseList.convertListToArray(MCTopicManageDialog.this.topicFunctionList), MCTopicManageDialog.this.topicFunctionItemListener);
            }

            public void executeFail() {
            }
        });
        this.topTopicAsyncTask.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void cancelTopicTop(final TopicModel topicModel2) {
        new AlertDialog.Builder(this.context).setMessage(this.context.getResources().getString(this.resource.getStringId("mc_forum_top_cancel"))).setNegativeButton(this.context.getResources().getString(this.resource.getStringId("mc_forum_dialog_cancel")), (DialogInterface.OnClickListener) null).setPositiveButton(this.resource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                TopTopicAsyncTask unused = MCTopicManageDialog.this.topTopicAsyncTask = new TopTopicAsyncTask(MCTopicManageDialog.this.context, MCTopicManageDialog.this.resource, topicModel2.getTopicId(), MCTopicManageDialog.this.boardId, 0);
                MCTopicManageDialog.this.topTopicAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                    public void executeSuccess() {
                        MCTopicManageDialog.this.topicManageDelegate.topicManageDelegate(3);
                        MCTopicManageDialog.this.topicFunctionList.set(MCTopicManageDialog.this.topicFunctionList.indexOf(MCTopicManageDialog.this.context.getResources().getString(MCTopicManageDialog.this.resource.getStringId("mc_forum_cancel_top"))), MCTopicManageDialog.this.context.getResources().getString(MCTopicManageDialog.this.resource.getStringId("mc_forum_top")));
                        MCTopicManageDialog.this.topicFunctionDialog.setItems(MCForumReverseList.convertListToArray(MCTopicManageDialog.this.topicFunctionList), MCTopicManageDialog.this.topicFunctionItemListener);
                    }

                    public void executeFail() {
                    }
                });
                MCTopicManageDialog.this.topTopicAsyncTask.execute(new Void[0]);
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void addEssenceTopicTop(TopicModel topicModel2) {
        this.essenceTopicAsyncTask = new EssenceTopicAsyncTask(this.context, this.resource, topicModel2.getTopicId(), this.boardId, 1);
        this.essenceTopicAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
            public void executeSuccess() {
                MCTopicManageDialog.this.topicFunctionList.set(MCTopicManageDialog.this.topicFunctionList.indexOf(MCTopicManageDialog.this.context.getResources().getString(MCTopicManageDialog.this.resource.getStringId("mc_forum_add_essence"))), MCTopicManageDialog.this.context.getResources().getString(MCTopicManageDialog.this.resource.getStringId("mc_forum_cancel_essence")));
                MCTopicManageDialog.this.topicFunctionDialog.setItems(MCForumReverseList.convertListToArray(MCTopicManageDialog.this.topicFunctionList), MCTopicManageDialog.this.topicFunctionItemListener);
                MCTopicManageDialog.this.topicManageDelegate.topicManageDelegate(4);
            }

            public void executeFail() {
            }
        });
        this.essenceTopicAsyncTask.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void cancelEssenceTopicTop(final TopicModel topicModel2) {
        new AlertDialog.Builder(this.context).setMessage(this.context.getResources().getString(this.resource.getStringId("mc_forum_essence_cancel"))).setNegativeButton(this.context.getResources().getString(this.resource.getStringId("mc_forum_dialog_cancel")), (DialogInterface.OnClickListener) null).setPositiveButton(this.resource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                EssenceTopicAsyncTask unused = MCTopicManageDialog.this.essenceTopicAsyncTask = new EssenceTopicAsyncTask(MCTopicManageDialog.this.context, MCTopicManageDialog.this.resource, topicModel2.getTopicId(), MCTopicManageDialog.this.boardId, 0);
                MCTopicManageDialog.this.essenceTopicAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                    public void executeSuccess() {
                        MCTopicManageDialog.this.topicFunctionList.set(MCTopicManageDialog.this.topicFunctionList.indexOf(MCTopicManageDialog.this.context.getResources().getString(MCTopicManageDialog.this.resource.getStringId("mc_forum_cancel_essence"))), MCTopicManageDialog.this.context.getResources().getString(MCTopicManageDialog.this.resource.getStringId("mc_forum_add_essence")));
                        MCTopicManageDialog.this.topicFunctionDialog.setItems(MCForumReverseList.convertListToArray(MCTopicManageDialog.this.topicFunctionList), MCTopicManageDialog.this.topicFunctionItemListener);
                        MCTopicManageDialog.this.topicManageDelegate.topicManageDelegate(5);
                    }

                    public void executeFail() {
                    }
                });
                MCTopicManageDialog.this.essenceTopicAsyncTask.execute(new Void[0]);
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void closeTopic(final TopicModel topicModel2) {
        new AlertDialog.Builder(this.context).setMessage(this.context.getResources().getString(this.resource.getStringId("mc_forum_close_cancel"))).setNegativeButton(this.context.getResources().getString(this.resource.getStringId("mc_forum_dialog_cancel")), (DialogInterface.OnClickListener) null).setPositiveButton(this.resource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CloseTopicAsyncTask unused = MCTopicManageDialog.this.closeTopicAsyncTask = new CloseTopicAsyncTask(MCTopicManageDialog.this.context, MCTopicManageDialog.this.resource, topicModel2.getTopicId(), MCTopicManageDialog.this.boardId, 1);
                MCTopicManageDialog.this.closeTopicAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                    public void executeSuccess() {
                        MCTopicManageDialog.this.topicFunctionList.set(MCTopicManageDialog.this.topicFunctionList.indexOf(MCTopicManageDialog.this.context.getResources().getString(MCTopicManageDialog.this.resource.getStringId("mc_forum_close"))), MCTopicManageDialog.this.context.getResources().getString(MCTopicManageDialog.this.resource.getStringId("mc_forum_cancel_close")));
                        MCTopicManageDialog.this.topicFunctionDialog.setItems(MCForumReverseList.convertListToArray(MCTopicManageDialog.this.topicFunctionList), MCTopicManageDialog.this.topicFunctionItemListener);
                        MCTopicManageDialog.this.topicManageDelegate.topicManageDelegate(6);
                    }

                    public void executeFail() {
                    }
                });
                MCTopicManageDialog.this.closeTopicAsyncTask.execute(new Void[0]);
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void cancelcloseTopic(TopicModel topicModel2) {
        this.closeTopicAsyncTask = new CloseTopicAsyncTask(this.context, this.resource, topicModel2.getTopicId(), this.boardId, 0);
        this.closeTopicAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
            public void executeSuccess() {
                MCTopicManageDialog.this.topicManageDelegate.topicManageDelegate(7);
                MCTopicManageDialog.this.topicFunctionList.set(MCTopicManageDialog.this.topicFunctionList.indexOf(MCTopicManageDialog.this.context.getResources().getString(MCTopicManageDialog.this.resource.getStringId("mc_forum_cancel_close"))), MCTopicManageDialog.this.context.getResources().getString(MCTopicManageDialog.this.resource.getStringId("mc_forum_close")));
                MCTopicManageDialog.this.topicFunctionDialog.setItems(MCForumReverseList.convertListToArray(MCTopicManageDialog.this.topicFunctionList), MCTopicManageDialog.this.topicFunctionItemListener);
            }

            public void executeFail() {
            }
        });
        this.closeTopicAsyncTask.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void physicalDelTopic(final TopicModel topicModel2) {
        new AlertDialog.Builder(this.context).setTitle(this.resource.getStringId("mc_forum_dialog_tip")).setMessage(this.resource.getStringId("mc_forum_warn_physical_delete_topic")).setPositiveButton(this.resource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (MCTopicManageDialog.this.delTopicAsyncTask != null) {
                    MCTopicManageDialog.this.delTopicAsyncTask.cancel(true);
                }
                PhysicalDelTopicAsyncTask unused = MCTopicManageDialog.this.delTopicAsyncTask = new PhysicalDelTopicAsyncTask(MCTopicManageDialog.this.context, MCTopicManageDialog.this.resource, topicModel2.getTopicId());
                MCTopicManageDialog.this.delTopicAsyncTask.execute(new Object[0]);
            }
        }).setNegativeButton(this.resource.getStringId("mc_forum_dialog_cancel"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }
}
