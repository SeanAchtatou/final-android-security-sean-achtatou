package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.ad.android.ui.widget.AdView;
import com.mobcent.base.android.ui.activity.view.MCInfoListView;

public class PostsReplyAdapterHolder {
    private AdView adView;
    private LinearLayout levelBox;
    private LinearLayout locationBox;
    private TextView locationText;
    private RelativeLayout replyBox;
    private LinearLayout replyContentLayout;
    private TextView replyContentText;
    private MCInfoListView replyContentView;
    private ImageButton replyDeleteBtn;
    private TextView replyLabText;
    private ImageButton replyMoreBtn;
    private TextView replyQuoteContentText;
    private ImageButton replyReplyBtn;
    private ImageButton replyShareBtn;
    private TextView replyTimeText;
    private ImageView replyUserImg;
    private TextView replyUserText;
    private ImageView roleImg;
    private AdView topAdViewBox;
    private TextView userRoleText;

    public AdView getTopAdViewBox() {
        return this.topAdViewBox;
    }

    public void setTopAdViewBox(AdView topAdViewBox2) {
        this.topAdViewBox = topAdViewBox2;
    }

    public RelativeLayout getReplyBox() {
        return this.replyBox;
    }

    public void setReplyBox(RelativeLayout replyBox2) {
        this.replyBox = replyBox2;
    }

    public LinearLayout getLevelBox() {
        return this.levelBox;
    }

    public AdView getAdView() {
        return this.adView;
    }

    public void setAdView(AdView adView2) {
        this.adView = adView2;
    }

    public void setLevelBox(LinearLayout levelBox2) {
        this.levelBox = levelBox2;
    }

    public ImageButton getReplyMoreBtn() {
        return this.replyMoreBtn;
    }

    public void setReplyMoreBtn(ImageButton replyMoreBtn2) {
        this.replyMoreBtn = replyMoreBtn2;
    }

    public TextView getUserRoleText() {
        return this.userRoleText;
    }

    public void setUserRoleText(TextView userRoleText2) {
        this.userRoleText = userRoleText2;
    }

    public ImageView getReplyUserImg() {
        return this.replyUserImg;
    }

    public void setReplyUserImg(ImageView replyUserImg2) {
        this.replyUserImg = replyUserImg2;
    }

    public ImageView getRoleImg() {
        return this.roleImg;
    }

    public void setRoleImg(ImageView roleImg2) {
        this.roleImg = roleImg2;
    }

    public TextView getReplyUserText() {
        return this.replyUserText;
    }

    public void setReplyUserText(TextView replyUserText2) {
        this.replyUserText = replyUserText2;
    }

    public TextView getReplyTimeText() {
        return this.replyTimeText;
    }

    public void setReplyTimeText(TextView replyTimeText2) {
        this.replyTimeText = replyTimeText2;
    }

    public ImageButton getReplyShareBtn() {
        return this.replyShareBtn;
    }

    public void setReplyShareBtn(ImageButton replyShareBtn2) {
        this.replyShareBtn = replyShareBtn2;
    }

    public ImageButton getReplyReplyBtn() {
        return this.replyReplyBtn;
    }

    public void setReplyReplyBtn(ImageButton replyReplyBtn2) {
        this.replyReplyBtn = replyReplyBtn2;
    }

    public TextView getReplyLabText() {
        return this.replyLabText;
    }

    public void setReplyLabText(TextView replyLabText2) {
        this.replyLabText = replyLabText2;
    }

    public TextView getReplyQuoteContentText() {
        return this.replyQuoteContentText;
    }

    public void setReplyQuoteContentText(TextView replyQuoteContentText2) {
        this.replyQuoteContentText = replyQuoteContentText2;
    }

    public MCInfoListView getReplyContentView() {
        return this.replyContentView;
    }

    public void setReplyContentView(MCInfoListView replyContentView2) {
        this.replyContentView = replyContentView2;
    }

    public TextView getReplyContentText() {
        return this.replyContentText;
    }

    public void setReplyContentText(TextView replyContentText2) {
        this.replyContentText = replyContentText2;
    }

    public ImageButton getReplyDeleteBtn() {
        return this.replyDeleteBtn;
    }

    public void setReplyDeleteBtn(ImageButton replyDeleteBtn2) {
        this.replyDeleteBtn = replyDeleteBtn2;
    }

    public LinearLayout getReplyContentLayout() {
        return this.replyContentLayout;
    }

    public void setReplyContentLayout(LinearLayout replyContentLayout2) {
        this.replyContentLayout = replyContentLayout2;
    }

    public LinearLayout getLocationBox() {
        return this.locationBox;
    }

    public void setLocationBox(LinearLayout locationBox2) {
        this.locationBox = locationBox2;
    }

    public TextView getLocationText() {
        return this.locationText;
    }

    public void setLocationText(TextView locationText2) {
        this.locationText = locationText2;
    }
}
