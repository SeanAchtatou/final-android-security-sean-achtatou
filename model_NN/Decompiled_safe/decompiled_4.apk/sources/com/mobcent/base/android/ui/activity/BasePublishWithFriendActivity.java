package com.mobcent.base.android.ui.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.MetionFriendAdapter;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.base.forum.android.util.MCTouchUtil;
import com.mobcent.forum.android.db.MentionFriendDBUtil;
import com.mobcent.forum.android.model.BoardModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.MentionFriendService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.MentionFriendServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.service.impl.helper.MentionFriendServiceImplHelper;
import com.mobcent.forum.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

public abstract class BasePublishWithFriendActivity extends BasePublishTopicActivityWithAudio implements MCConstant {
    public static final int MAX_AT_COUNT = 20;
    protected boolean StartTask = false;
    protected List<BoardModel> boardList;
    /* access modifiers changed from: private */
    public long currentUserId = 0;
    private TextView friendText;
    /* access modifiers changed from: private */
    public ProgressDialog loadFriendDialog;
    /* access modifiers changed from: private */
    public RelativeLayout mentionFriendLayout;
    /* access modifiers changed from: private */
    public MentionFriendService mentionFriendservice;
    protected List<UserInfoModel> mentionedFriends;
    private ListView metionFriedtList;
    /* access modifiers changed from: private */
    public MetionFriendAdapter metionFriendAdapter;
    private ObtainMetionFriendsTask metionFriendsTask;
    /* access modifiers changed from: private */
    public TextView noMEntionFriendText;
    protected ArrayList<UserInfoModel> postsUserList;
    protected boolean showMentionFriendsView = false;
    private char spaceChar = ' ';
    protected List<UserInfoModel> userInfoModels;
    private UserService userService;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.mentionFriendservice = new MentionFriendServiceImpl(this);
        this.userService = new UserServiceImpl(getApplicationContext());
        this.currentUserId = this.userService.getLoginUserId();
        this.mentionedFriends = new ArrayList();
        this.userInfoModels = new ArrayList();
        this.metionFriendAdapter = new MetionFriendAdapter(this, this.userInfoModels);
        this.loadFriendDialog = new ProgressDialog(this);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        this.takeUser = (ImageView) findViewById(this.resource.getViewId("mc_forum_take_user"));
        MCTouchUtil.createTouchDelegate(this.takeUser, 10);
        this.friendText = (TextView) findViewById(this.resource.getViewId("mc_forum_friend_text"));
        this.mentionFriendLayout = (RelativeLayout) findViewById(this.resource.getViewId("mc_froum_mention_friend_box"));
        this.metionFriedtList = (ListView) findViewById(this.resource.getViewId("mc_forum_mention_friend_list"));
        this.noMEntionFriendText = (TextView) findViewById(this.resource.getViewId("mc_forum_no_mention_friend_Text"));
        this.selectBoardImg = (EditText) findViewById(this.resource.getViewId("mc_forum_publish_board_edit"));
        this.metionFriedtList.setAdapter((ListAdapter) this.metionFriendAdapter);
        this.openAnimation = new TranslateAnimation(0.0f, 0.0f, 550.0f, 30.0f);
        this.openAnimation.setDuration(300);
        this.mentionFriendLayout.setAnimation(this.openAnimation);
        this.closeAnimation = new TranslateAnimation(0.0f, 0.0f, 30.0f, 500.0f);
        this.closeAnimation.setDuration(300);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.contentEdit.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (BasePublishWithFriendActivity.this.showMentionFriendsView) {
                    return false;
                }
                BasePublishWithFriendActivity.this.contentEdit.requestFocus();
                BasePublishWithFriendActivity.this.changeKeyboardState(1);
                return false;
            }
        });
        this.takeUser.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePublishWithFriendActivity.this.openAnimation.start();
                BasePublishWithFriendActivity.this.mentionFriendLayout.setAnimation(BasePublishWithFriendActivity.this.openAnimation);
                BasePublishWithFriendActivity.this.contentEdit.getText().insert(BasePublishWithFriendActivity.this.contentEdit.getSelectionEnd(), "@");
                BasePublishWithFriendActivity.this.changeMentionFriendsView(true);
                if (!BasePublishWithFriendActivity.this.StartTask) {
                    BasePublishWithFriendActivity.this.getMentionFriends();
                    BasePublishWithFriendActivity.this.StartTask = true;
                }
                BasePublishWithFriendActivity.this.getMentionedFriend();
            }
        });
        this.metionFriedtList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                UserInfoModel mentionFriend = BasePublishWithFriendActivity.this.userInfoModels.get(position);
                if (mentionFriend.getUserId() > -1) {
                    BasePublishWithFriendActivity.this.getMentionedFriend();
                    if (BasePublishWithFriendActivity.this.mentionedFriends.size() < 20) {
                        BasePublishWithFriendActivity.this.contentEdit.getText().insert(BasePublishWithFriendActivity.this.contentEdit.getSelectionEnd(), mentionFriend.getNickname() + " ");
                        BasePublishWithFriendActivity.this.changeMentionFriendsView(false);
                        BasePublishWithFriendActivity.this.mentionedFriends.add(mentionFriend);
                    } else {
                        Toast.makeText(BasePublishWithFriendActivity.this, MCStringBundleUtil.resolveString(BasePublishWithFriendActivity.this.resource.getStringId("mc_forum_mention_friend_count"), "20", BasePublishWithFriendActivity.this), 0).show();
                        BasePublishWithFriendActivity.this.changeMentionFriendsView(false);
                    }
                    BasePublishWithFriendActivity.this.mentionFriendLayout.setAnimation(BasePublishWithFriendActivity.this.closeAnimation);
                }
            }
        });
    }

    public void getMentionFriends() {
        changeLableBackgroundState(0);
        if (this.metionFriendsTask != null) {
            this.metionFriendsTask.cancel(true);
        }
        this.metionFriendsTask = new ObtainMetionFriendsTask();
        this.metionFriendsTask.execute(true);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.loadFriendDialog.isShowing()) {
            this.loadFriendDialog.dismiss();
            return true;
        } else if (this.mentionFriendLayout.getVisibility() != 0) {
            return super.onKeyDown(keyCode, event);
        } else {
            this.mentionFriendLayout.setAnimation(this.closeAnimation);
            changeMentionFriendsView(false);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void changeMentionFriendsView(boolean show) {
        this.showMentionFriendsView = show;
        this.contentEdit.getLineHeight();
        if (this.showMentionFriendsView) {
            hideSoftKeyboard();
            this.mentionFriendLayout.setVisibility(0);
            this.publishBarBox.setVisibility(8);
            if (this.titleEdit != null) {
                this.titleEdit.setVisibility(8);
                this.selectBoardImg.setVisibility(8);
            }
            this.contentEdit.setSingleLine(true);
        } else {
            this.closeAnimation.start();
            this.mentionFriendLayout.setAnimation(this.closeAnimation);
            this.mentionFriendLayout.setVisibility(8);
            this.publishBarBox.setVisibility(0);
            if ((this instanceof PublishTopicActivity) && this.titleEdit != null) {
                this.titleEdit.setVisibility(0);
                this.selectBoardImg.setVisibility(0);
            }
            this.contentEdit.setSingleLine(false);
        }
        this.contentEdit.setSelection(this.contentEdit.length());
    }

    /* access modifiers changed from: protected */
    public void changeLableBackgroundState(int select) {
        this.selectBackground = select;
        switch (this.selectBackground) {
            case 0:
                this.friendText.setBackgroundResource(this.resource.getDrawableId("mc_forum_tab2"));
                this.friendText.setTextColor(this.resource.getColor("mc_forum_text_hight_color"));
                return;
            case 1:
                this.friendText.setBackgroundResource(this.resource.getDrawableId("mc_forum_tab1"));
                this.friendText.setTextColor(this.resource.getColor("mc_forum_text_apparent_color"));
                return;
            default:
                return;
        }
    }

    private class ObtainMetionFriendsTask extends AsyncTask<Boolean, Void, List<UserInfoModel>> {
        private ObtainMetionFriendsTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<UserInfoModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public List<UserInfoModel> doInBackground(Boolean... params) {
            String jsonStr;
            if (params[0].booleanValue()) {
                try {
                    jsonStr = MentionFriendDBUtil.getInstance(BasePublishWithFriendActivity.this).getMentionFriendJsonString(BasePublishWithFriendActivity.this.currentUserId);
                } catch (Exception e) {
                    e.printStackTrace();
                    jsonStr = null;
                }
                if (!StringUtil.isEmpty(jsonStr)) {
                    return MentionFriendServiceImplHelper.parseMentionFriendListJson(jsonStr);
                }
                BasePublishWithFriendActivity.this.mHandler.post(new Runnable() {
                    public void run() {
                        ProgressDialog unused = BasePublishWithFriendActivity.this.loadFriendDialog = ProgressDialog.show(BasePublishWithFriendActivity.this, BasePublishWithFriendActivity.this.getResources().getString(BasePublishWithFriendActivity.this.resource.getStringId("mc_forum_please_wait")), BasePublishWithFriendActivity.this.getResources().getString(BasePublishWithFriendActivity.this.resource.getStringId("mc_forum_loading")), true);
                    }
                });
                return BasePublishWithFriendActivity.this.mentionFriendservice.getForumMentionFriendByNet(BasePublishWithFriendActivity.this.currentUserId);
            }
            BasePublishWithFriendActivity.this.mHandler.post(new Runnable() {
                public void run() {
                    ProgressDialog unused = BasePublishWithFriendActivity.this.loadFriendDialog = ProgressDialog.show(BasePublishWithFriendActivity.this, BasePublishWithFriendActivity.this.getResources().getString(BasePublishWithFriendActivity.this.resource.getStringId("mc_forum_please_wait")), BasePublishWithFriendActivity.this.getResources().getString(BasePublishWithFriendActivity.this.resource.getStringId("mc_forum_loading")), true);
                }
            });
            return BasePublishWithFriendActivity.this.mentionFriendservice.getForumMentionFriendByNet(BasePublishWithFriendActivity.this.currentUserId);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<UserInfoModel> result) {
            List<UserInfoModel> tempList = new ArrayList<>();
            tempList.addAll(result);
            if (BasePublishWithFriendActivity.this.loadFriendDialog.isShowing()) {
                BasePublishWithFriendActivity.this.loadFriendDialog.dismiss();
            }
            if (result == null || result.size() <= 0) {
                if (BasePublishWithFriendActivity.this.userInfoModels == null || BasePublishWithFriendActivity.this.userInfoModels.size() <= 0) {
                    BasePublishWithFriendActivity.this.noMEntionFriendText.setVisibility(0);
                } else {
                    UserInfoModel user = new UserInfoModel();
                    user.setUserId(-1);
                    user.setRoleNum(-1);
                    tempList.add(user);
                    tempList.addAll(BasePublishWithFriendActivity.this.userInfoModels);
                    BasePublishWithFriendActivity.this.noMEntionFriendText.setVisibility(8);
                }
            } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                Toast.makeText(BasePublishWithFriendActivity.this, MCForumErrorUtil.convertErrorCode(BasePublishWithFriendActivity.this, result.get(0).getErrorCode()), 0).show();
            } else {
                if (BasePublishWithFriendActivity.this.userInfoModels != null && BasePublishWithFriendActivity.this.userInfoModels.size() > 0) {
                    UserInfoModel user2 = new UserInfoModel();
                    user2.setUserId(-1);
                    user2.setRoleNum(-1);
                    tempList.add(user2);
                    tempList.addAll(BasePublishWithFriendActivity.this.userInfoModels);
                }
                BasePublishWithFriendActivity.this.noMEntionFriendText.setVisibility(8);
            }
            BasePublishWithFriendActivity.this.metionFriendAdapter.setUserInfoList(tempList);
            BasePublishWithFriendActivity.this.metionFriendAdapter.notifyDataSetInvalidated();
            BasePublishWithFriendActivity.this.metionFriendAdapter.notifyDataSetChanged();
            BasePublishWithFriendActivity.this.userInfoModels = tempList;
        }
    }

    /* access modifiers changed from: protected */
    public boolean publicTopic() {
        getMentionedFriend();
        return true;
    }

    /* access modifiers changed from: protected */
    public List<UserInfoModel> getMentionedFriend() {
        String mentionfriendcount = "";
        String content = this.contentEdit.getText().toString();
        for (int i = 0; i < this.mentionedFriends.size(); i++) {
            mentionfriendcount = mentionfriendcount + this.mentionedFriends.get(i).getNickname() + AdApiConstant.RES_SPLIT_COMMA;
        }
        List<UserInfoModel> tempList = new ArrayList<>();
        if (content.contains("@")) {
            String[] s = content.split("@");
            for (int j = 0; j < s.length; j++) {
                int endIndex = s[j].indexOf(this.spaceChar);
                if (endIndex > -1) {
                    String nickname = s[j].substring(0, endIndex);
                    UserInfoModel user = new UserInfoModel();
                    if (!mentionfriendcount.contains(nickname + AdApiConstant.RES_SPLIT_COMMA)) {
                        user.setUserId(0);
                        user.setNickname(nickname);
                    } else {
                        user = getUserFromFriendsList(this.mentionedFriends, nickname);
                    }
                    if (user != null) {
                        tempList.add(user);
                    }
                }
            }
        }
        this.mentionedFriends = tempList;
        return this.mentionedFriends;
    }

    private UserInfoModel getUserFromFriendsList(List<UserInfoModel> friends, String name) {
        int s = friends.size();
        for (int i = 0; i < s; i++) {
            if (name.equals(friends.get(i).getNickname())) {
                return friends.get(i);
            }
        }
        return null;
    }

    private List<UserInfoModel> mergeFriendsList(List<UserInfoModel> postsUserList2, List<UserInfoModel> friends) {
        List<UserInfoModel> tempList = new ArrayList<>();
        for (UserInfoModel user : postsUserList2) {
            int i = containUserIndex(friends, user);
            if (i > -1) {
                friends.remove(i);
            }
        }
        tempList.addAll(postsUserList2);
        tempList.addAll(friends);
        return tempList;
    }

    private int containUserIndex(List<UserInfoModel> userlist, UserInfoModel user) {
        int j = userlist.size();
        for (int i = 0; i < j; i++) {
            if (userlist.get(i).getUserId() == user.getUserId()) {
                return i;
            }
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.metionFriedtList = null;
        super.onDestroy();
        if (this.metionFriendsTask != null) {
            this.metionFriendsTask.cancel(true);
        }
    }
}
