package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FeedTopicAdapterHolder {
    private TextView contentText;
    private TextView feedTitle;
    private TextView feedType;
    private LinearLayout feedsBox;
    private ImageView hasVoice;
    private ImageView iconImg;
    private LinearLayout levelBox;
    private ImageButton moreBtn;
    private TextView moreText;
    private TextView quoteContentText;
    private TextView roleText;
    private TextView timeText;
    private TextView userNameText;

    public ImageView getHasVoice() {
        return this.hasVoice;
    }

    public void setHasVoice(ImageView hasVoice2) {
        this.hasVoice = hasVoice2;
    }

    public ImageView getIconImg() {
        return this.iconImg;
    }

    public void setIconImg(ImageView iconImg2) {
        this.iconImg = iconImg2;
    }

    public TextView getUserNameText() {
        return this.userNameText;
    }

    public void setUserNameText(TextView userNameText2) {
        this.userNameText = userNameText2;
    }

    public TextView getRoleText() {
        return this.roleText;
    }

    public void setRoleText(TextView roleText2) {
        this.roleText = roleText2;
    }

    public TextView getFeedType() {
        return this.feedType;
    }

    public void setFeedType(TextView feedType2) {
        this.feedType = feedType2;
    }

    public TextView getFeedTitle() {
        return this.feedTitle;
    }

    public void setFeedTitle(TextView feedTitle2) {
        this.feedTitle = feedTitle2;
    }

    public TextView getContentText() {
        return this.contentText;
    }

    public void setContentText(TextView contentText2) {
        this.contentText = contentText2;
    }

    public LinearLayout getLevelBox() {
        return this.levelBox;
    }

    public void setLevelBox(LinearLayout levelBox2) {
        this.levelBox = levelBox2;
    }

    public LinearLayout getFeedsBox() {
        return this.feedsBox;
    }

    public void setFeedsBox(LinearLayout feedsBox2) {
        this.feedsBox = feedsBox2;
    }

    public TextView getTimeText() {
        return this.timeText;
    }

    public void setTimeText(TextView timeText2) {
        this.timeText = timeText2;
    }

    public ImageButton getMoreBtn() {
        return this.moreBtn;
    }

    public void setMoreBtn(ImageButton moreBtn2) {
        this.moreBtn = moreBtn2;
    }

    public TextView getMoreText() {
        return this.moreText;
    }

    public void setMoreText(TextView moreText2) {
        this.moreText = moreText2;
    }

    public TextView getQuoteContentText() {
        return this.quoteContentText;
    }

    public void setQuoteContentText(TextView quoteContentText2) {
        this.quoteContentText = quoteContentText2;
    }
}
