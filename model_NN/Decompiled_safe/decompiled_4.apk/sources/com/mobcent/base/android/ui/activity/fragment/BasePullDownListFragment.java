package com.mobcent.base.android.ui.activity.fragment;

import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import com.mobcent.base.android.ui.activity.view.MCPullDownView;
import com.mobcent.forum.android.db.SharedPreferencesDB;

public abstract class BasePullDownListFragment extends BaseFragment implements MCPullDownView.UpdateHandle {
    protected Long mLastUpdateDate;
    protected ListView mListView;
    protected MCPullDownView mPullDownView;

    /* access modifiers changed from: protected */
    public void initListView(View view) {
        if (this.mPullDownView == null || this.mListView == null) {
            this.mPullDownView = (MCPullDownView) view.findViewById(this.mcResource.getViewId("mc_forum_pd_list"));
            this.mPullDownView.setUpdateHandle(this);
            this.mListView = (ListView) view.findViewById(this.mcResource.getViewId("mc_forum_lv"));
            this.mLastUpdateDate = Long.valueOf(SharedPreferencesDB.getInstance(this.activity).getListLastUpdateDate(getClass().getName()));
            if (this.mLastUpdateDate.longValue() != 0) {
                this.mPullDownView.setUpdateDate(this.mLastUpdateDate.longValue());
            }
            this.mListView.setOnTouchListener(new View.OnTouchListener() {
                int deltaY = 0;
                int mMotionY = 0;

                public boolean onTouch(View v, MotionEvent event) {
                    int y = (int) event.getY();
                    switch (event.getAction()) {
                        case 0:
                            this.mMotionY = y;
                            return false;
                        case 1:
                        default:
                            return false;
                        case 2:
                            this.deltaY = y - this.mMotionY;
                            if (this.deltaY >= 0 || BasePullDownListFragment.this.mPullDownView.getState() == 1) {
                                return false;
                            }
                            BasePullDownListFragment.this.mPullDownView.end();
                            return false;
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void endUpdate() {
        this.mLastUpdateDate = Long.valueOf(System.currentTimeMillis());
        SharedPreferencesDB.getInstance(this.activity).updateListLastUpdateDate(getClass().getName(), this.mLastUpdateDate.longValue());
        if (this.mPullDownView != null) {
            this.mPullDownView.endUpdate(this.mLastUpdateDate.longValue());
            this.mPullDownView.setEnable(true);
        }
    }

    /* access modifiers changed from: protected */
    public void resetPullDownView() {
        if (this.mPullDownView != null && this.mPullDownView.getState() != 1) {
            this.mPullDownView.end();
        }
    }

    public void onHeadLoading() {
        if (this.mPullDownView != null && this.mListView != null) {
            this.mPullDownView.update();
            this.mPullDownView.setEnable(false);
        }
    }
}
