package com.mobcent.base.android.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.Selection;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.mobcent.base.forum.android.util.MCColorUtil;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.MD5Util;
import com.mobcent.forum.android.util.StringUtil;

public class ResetPassowrdActivity extends BaseActivity {
    private Button backBtn;
    private String email;
    /* access modifiers changed from: private */
    public boolean isRegShowPwd = false;
    /* access modifiers changed from: private */
    public String password;
    /* access modifiers changed from: private */
    public EditText passwordEdit;
    /* access modifiers changed from: private */
    public ImageButton regShowPwdBtn;
    /* access modifiers changed from: private */
    public ResetPassowrdTask resetPassowrdTask;
    private TextView restText;
    private Button saveBtn;
    /* access modifiers changed from: private */
    public long userId;

    /* access modifiers changed from: protected */
    public void initData() {
        this.userId = getIntent().getLongExtra("userId", 0);
        this.email = getIntent().getStringExtra("email");
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_user_reset_password_activity"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.saveBtn = (Button) findViewById(this.resource.getViewId("mc_forum_save_btn"));
        this.passwordEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_user_register_password_edit"));
        this.regShowPwdBtn = (ImageButton) findViewById(this.resource.getViewId("mc_forum_user_show_password_btn"));
        this.restText = (TextView) findViewById(this.resource.getViewId("mc_forum_reset_text"));
        MCColorUtil.setTextViewPart(this, this.restText, MCStringBundleUtil.resolveString(this.resource.getStringId("mc_forum_reset_desc"), this.email, this), 3, this.email.length() + 3, "mc_forum_text_hight_color");
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ResetPassowrdActivity.this.back();
            }
        });
        this.regShowPwdBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!ResetPassowrdActivity.this.isRegShowPwd) {
                    ResetPassowrdActivity.this.regShowPwdBtn.setBackgroundResource(ResetPassowrdActivity.this.resource.getDrawableId("mc_forum_input_show_pwd_d"));
                    boolean unused = ResetPassowrdActivity.this.isRegShowPwd = true;
                    ResetPassowrdActivity.this.passwordEdit.setInputType(144);
                    Editable etable = ResetPassowrdActivity.this.passwordEdit.getText();
                    Selection.setSelection(etable, etable.length());
                    return;
                }
                ResetPassowrdActivity.this.regShowPwdBtn.setBackgroundResource(ResetPassowrdActivity.this.resource.getDrawableId("mc_forum_input_show_pwd_n"));
                boolean unused2 = ResetPassowrdActivity.this.isRegShowPwd = false;
                ResetPassowrdActivity.this.passwordEdit.setInputType(129);
                Editable etable2 = ResetPassowrdActivity.this.passwordEdit.getText();
                Selection.setSelection(etable2, etable2.length());
            }
        });
        this.passwordEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode != 66) {
                    return false;
                }
                ResetPassowrdActivity.this.hideSoftKeyboard();
                return false;
            }
        });
        this.saveBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String unused = ResetPassowrdActivity.this.password = ResetPassowrdActivity.this.passwordEdit.getText().toString();
                if (ResetPassowrdActivity.this.password.length() < 6 || ResetPassowrdActivity.this.password.length() > 20) {
                    ResetPassowrdActivity.this.warnMessageById("mc_forum_user_password_length_error_warn");
                } else if (!StringUtil.isPwdMatchRule(ResetPassowrdActivity.this.password)) {
                    ResetPassowrdActivity.this.warnMessageById("mc_forum_user_password_format_error_warn");
                } else {
                    if (ResetPassowrdActivity.this.resetPassowrdTask != null) {
                        ResetPassowrdActivity.this.resetPassowrdTask.cancel(true);
                    }
                    ResetPassowrdTask unused2 = ResetPassowrdActivity.this.resetPassowrdTask = new ResetPassowrdTask();
                    ResetPassowrdActivity.this.resetPassowrdTask.execute(new Void[0]);
                }
            }
        });
    }

    class ResetPassowrdTask extends AsyncTask<Void, Void, String> {
        ResetPassowrdTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            ResetPassowrdActivity.this.showProgressDialog("mc_forum_warn_change_pwd", this);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... params) {
            return new UserServiceImpl(ResetPassowrdActivity.this).getResetPassword(ResetPassowrdActivity.this.userId, MD5Util.toMD5(ResetPassowrdActivity.this.password));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            super.onPostExecute((Object) result);
            ResetPassowrdActivity.this.hideProgressDialog();
            if (result == null) {
                ResetPassowrdActivity.this.warnMessageById("mc_forum_reset_password_success");
                ResetPassowrdActivity.this.startActivity(new Intent(ResetPassowrdActivity.this, LoginActivity.class));
                ResetPassowrdActivity.this.finish();
                for (ForgetPasswordActivity act : ForgetPasswordActivity.getForgetPasswordActivitySets()) {
                    act.finish();
                }
            } else if (!result.equals("")) {
                ResetPassowrdActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(ResetPassowrdActivity.this, result));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.resetPassowrdTask != null) {
            this.resetPassowrdTask.cancel(true);
        }
    }
}
