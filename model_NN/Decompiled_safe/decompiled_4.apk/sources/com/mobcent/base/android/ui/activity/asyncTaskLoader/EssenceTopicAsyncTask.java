package com.mobcent.base.android.ui.activity.asyncTaskLoader;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;

public class EssenceTopicAsyncTask extends AsyncTask<Void, Void, String> {
    private long boardId;
    private Context context;
    private MCResource resource;
    private int status;
    private TaskExecuteDelegate taskExecuteDelegate;
    private long topicId;

    public EssenceTopicAsyncTask(Context context2, MCResource resource2, long topicId2, long boardId2, int status2) {
        this.context = context2;
        this.resource = resource2;
        this.topicId = topicId2;
        this.boardId = boardId2;
        this.status = status2;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Void... params) {
        return new PostsServiceImpl(this.context.getApplicationContext()).setEssenceTopic(this.boardId, this.topicId, this.status);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String result) {
        super.onPostExecute((Object) result);
        if (result != null) {
            Toast.makeText(this.context, MCForumErrorUtil.convertErrorCode(this.context, result), 0).show();
            if (this.taskExecuteDelegate != null) {
                this.taskExecuteDelegate.executeFail();
                return;
            }
            return;
        }
        if (this.status == 1) {
            Toast.makeText(this.context, this.resource.getStringId("mc_forum_set_essence_suess"), 0).show();
        } else {
            Toast.makeText(this.context, this.resource.getStringId("mc_forum_cancel_essence_success"), 0).show();
        }
        if (this.taskExecuteDelegate != null) {
            this.taskExecuteDelegate.executeSuccess();
        }
    }

    public TaskExecuteDelegate getTaskExecuteDelegate() {
        return this.taskExecuteDelegate;
    }

    public void setTaskExecuteDelegate(TaskExecuteDelegate taskExecuteDelegate2) {
        this.taskExecuteDelegate = taskExecuteDelegate2;
    }
}
