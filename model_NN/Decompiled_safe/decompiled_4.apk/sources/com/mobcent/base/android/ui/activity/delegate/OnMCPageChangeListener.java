package com.mobcent.base.android.ui.activity.delegate;

import android.support.v4.view.ViewPager;
import com.mobcent.base.android.ui.activity.helper.SlidingMenuControler;
import com.mobcent.base.android.ui.widget.slader.SlidingMenu;

public abstract class OnMCPageChangeListener implements ViewPager.OnPageChangeListener {
    public void onPageScrollStateChanged(int state) {
    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    public void onPageSelected(int position) {
        SlidingMenu menu;
        if (SlidingMenuControler.getInstance().getListener() != null && (menu = SlidingMenuControler.getInstance().getListener().getSlidingMenu()) != null) {
            if (position == 0) {
                menu.setTouchModeAbove(1);
            } else {
                menu.setTouchModeAbove(0);
            }
        }
    }
}
