package com.mobcent.base.android.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.db.UserJsonDBUtil;
import com.mobcent.forum.android.db.helper.UserJsonDBHelper;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;

public class UserMyInfoActivity extends BasePhotoPreviewActivity {
    private Button backBtn;
    /* access modifiers changed from: private */
    public UserInfoModel currUserInfoModel;
    private TextView emailText;
    private LinearLayout femaleBox;
    /* access modifiers changed from: private */
    public Button femaleRadio;
    /* access modifiers changed from: private */
    public ImageView headerImg;
    /* access modifiers changed from: private */
    public boolean isExit = false;
    /* access modifiers changed from: private */
    public LoadAsyncTask loadAsyncTask;
    private Button localPhotoBtn;
    private LinearLayout maleBox;
    /* access modifiers changed from: private */
    public Button maleRadio;
    /* access modifiers changed from: private */
    public int nickNameMaxLen = 20;
    /* access modifiers changed from: private */
    public int nickNameMinLen = 3;
    /* access modifiers changed from: private */
    public EditText nicknameEdit;
    /* access modifiers changed from: private */
    public AlertDialog.Builder reloadDialog;
    private Button saveBtn;
    private Button takePhotoBtn;
    /* access modifiers changed from: private */
    public UpdateAsyncTask updateAsyncTask;
    /* access modifiers changed from: private */
    public UserInfoModel userInfoModel;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.loadAsyncTask = new LoadAsyncTask();
        this.loadAsyncTask.execute(new String[0]);
        this.uploadType = 1;
        this.userInfoModel = new UserInfoModel();
        this.currUserInfoModel = new UserInfoModel();
        this.currUserInfoModel.setGender(-1);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_user_info_setting_activity"));
        super.initViews();
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.saveBtn = (Button) findViewById(this.resource.getViewId("mc_forum_save_btn"));
        this.takePhotoBtn = (Button) findViewById(this.resource.getViewId("mc_forum_take_photo_btn"));
        this.localPhotoBtn = (Button) findViewById(this.resource.getViewId("mc_forum_gallery_pic_btn"));
        this.emailText = (TextView) findViewById(this.resource.getViewId("mc_forum_email_text"));
        this.nicknameEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_nickname_edit"));
        this.headerImg = (ImageView) findViewById(this.resource.getViewId("mc_forum_user_icon_img"));
        this.maleRadio = (Button) findViewById(this.resource.getViewId("mc_forum_gender_male_btn"));
        this.femaleRadio = (Button) findViewById(this.resource.getViewId("mc_forum_gender_female_btn"));
        this.maleBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_gender_male_box"));
        this.femaleBox = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_gender_female_box"));
        initReloadDialog();
    }

    /* access modifiers changed from: protected */
    public void initReloadDialog() {
        this.reloadDialog = new AlertDialog.Builder(this).setTitle(this.resource.getStringId("mc_forum_dialog_tip")).setMessage(this.resource.getStringId("mc_forum_warn_error"));
        this.reloadDialog.setPositiveButton(this.resource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                UserMyInfoActivity.this.loadAsyncTask.execute(new String[0]);
            }
        });
        this.reloadDialog.setNegativeButton(this.resource.getStringId("mc_forum_dialog_cancel"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                UserMyInfoActivity.this.finish();
            }
        });
        this.reloadDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode != 4) {
                    return true;
                }
                UserMyInfoActivity.this.finish();
                return true;
            }
        });
        this.myDialog.show();
        this.exitAlertDialog = new AlertDialog.Builder(this).setTitle(this.resource.getStringId("mc_forum_dialog_tip")).setMessage(this.resource.getStringId("mc_forum_user_back_warn"));
        this.exitAlertDialog.setPositiveButton(this.resource.getStringId("mc_forum_dialog_confirm"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                UserMyInfoActivity.this.finish();
            }
        });
        this.exitAlertDialog.setNegativeButton(this.resource.getStringId("mc_forum_dialog_cancel"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (UserMyInfoActivity.this.exitCheckChanged()) {
                    UserMyInfoActivity.this.exitAlertDialog.create();
                    UserMyInfoActivity.this.exitAlertDialog.show();
                    return;
                }
                UserMyInfoActivity.this.exitNoSaveEvent();
            }
        });
        this.saveBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String nickName = UserMyInfoActivity.this.nicknameEdit.getText().toString();
                if (!StringUtil.checkNickNameMinLen(nickName, UserMyInfoActivity.this.nickNameMinLen) || !StringUtil.checkNickNameMaxLen(nickName, UserMyInfoActivity.this.nickNameMaxLen)) {
                    UserMyInfoActivity.this.warnMessageById("mc_forum_user_nickname_length_error");
                    return;
                }
                UserMyInfoActivity.this.currUserInfoModel.setNickname(nickName);
                UserMyInfoActivity.this.currUserInfoModel.setIcon(UserMyInfoActivity.this.iconPath);
                UserMyInfoActivity.this.currUserInfoModel.setSignature("");
                if (UserMyInfoActivity.this.updateAsyncTask != null) {
                    UserMyInfoActivity.this.updateAsyncTask.cancel(true);
                }
                UpdateAsyncTask unused = UserMyInfoActivity.this.updateAsyncTask = new UpdateAsyncTask();
                UserMyInfoActivity.this.updateAsyncTask.execute(UserMyInfoActivity.this.currUserInfoModel);
            }
        });
        this.takePhotoBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserMyInfoActivity.this.cameraPhotoListener();
            }
        });
        this.localPhotoBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserMyInfoActivity.this.localPhotoListener();
            }
        });
        this.maleBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserMyInfoActivity.this.currUserInfoModel.setGender(1);
                UserMyInfoActivity.this.maleRadio.setBackgroundResource(UserMyInfoActivity.this.resource.getDrawableId("mc_forum_select2_2"));
                UserMyInfoActivity.this.femaleRadio.setBackgroundResource(UserMyInfoActivity.this.resource.getDrawableId("mc_forum_select2_1"));
            }
        });
        this.femaleBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                UserMyInfoActivity.this.currUserInfoModel.setGender(0);
                UserMyInfoActivity.this.maleRadio.setBackgroundResource(UserMyInfoActivity.this.resource.getDrawableId("mc_forum_select2_1"));
                UserMyInfoActivity.this.femaleRadio.setBackgroundResource(UserMyInfoActivity.this.resource.getDrawableId("mc_forum_select2_2"));
            }
        });
        this.nicknameEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66) {
                    UserMyInfoActivity.this.nicknameEdit.setFocusable(false);
                    UserMyInfoActivity.this.nicknameEdit.setFocusableInTouchMode(true);
                    UserMyInfoActivity.this.hideSoftKeyboard();
                }
                UserMyInfoActivity.this.nicknameEdit.setFocusable(true);
                return false;
            }
        });
    }

    public class LoadAsyncTask extends AsyncTask<String, Void, UserInfoModel> {
        public LoadAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            UserMyInfoActivity.this.showProgressDialog("mc_forum_warn_load", this);
        }

        /* access modifiers changed from: protected */
        public UserInfoModel doInBackground(String... params) {
            UserInfoModel unused = UserMyInfoActivity.this.userInfoModel = new UserServiceImpl(UserMyInfoActivity.this).getUser();
            return UserMyInfoActivity.this.userInfoModel;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(UserInfoModel result) {
            UserMyInfoActivity.this.hideProgressDialog();
            if (result == null) {
                return;
            }
            if (result.getErrorCode() == null || "".equals(result.getErrorCode())) {
                UserMyInfoActivity.this.updateView(result);
                return;
            }
            UserMyInfoActivity.this.reloadDialog.create();
            UserMyInfoActivity.this.reloadDialog.show();
        }
    }

    private class UpdateAsyncTask extends AsyncTask<UserInfoModel, Void, String> {
        private UserInfoModel model;

        private UpdateAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            UserMyInfoActivity.this.showProgressDialog("mc_forum_warn_update", this);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(UserInfoModel... params) {
            UserService userService = new UserServiceImpl(UserMyInfoActivity.this);
            if (params[0] == null) {
                return null;
            }
            this.model = params[0];
            UserInfoModel userInfoModel = params[0];
            return userService.updateUser(userInfoModel.getNickname(), userInfoModel.getIcon(), userInfoModel.getGender(), userInfoModel.getSignature(), -1);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            UserMyInfoActivity.this.hideProgressDialog();
            if (result == null) {
                UserJsonDBUtil.getInstance(UserMyInfoActivity.this).savePersonalInfo(this.model.getUserId(), 1, UserJsonDBHelper.buildPersonalInfo(this.model));
                UserMyInfoActivity.this.warnMessageById("mc_forum_user_update_succ");
                UserMyInfoActivity.this.getUserInfoModel();
                UserMyInfoActivity.this.exitNoSaveEvent();
            } else if (!result.equals("")) {
                UserMyInfoActivity.this.warnMessageByStr(MCForumErrorUtil.convertErrorCode(UserMyInfoActivity.this, result));
            }
            if (UserMyInfoActivity.this.isExit) {
                UserMyInfoActivity.this.exitNoSaveEvent();
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateView(UserInfoModel userInfoModel2) {
        this.emailText.setText(userInfoModel2.getEmail());
        this.nicknameEdit.setText(userInfoModel2.getNickname());
        Editable nickEdit = this.nicknameEdit.getText();
        Selection.setSelection(nickEdit, nickEdit.length());
        int gender = userInfoModel2.getGender();
        if (gender == 1) {
            this.maleRadio.setBackgroundResource(this.resource.getDrawableId("mc_forum_select2_2"));
            this.femaleRadio.setBackgroundResource(this.resource.getDrawableId("mc_forum_select2_1"));
        } else if (gender == 0) {
            this.maleRadio.setBackgroundResource(this.resource.getDrawableId("mc_forum_select2_1"));
            this.femaleRadio.setBackgroundResource(this.resource.getDrawableId("mc_forum_select2_2"));
        }
        this.currUserInfoModel.setGender(gender);
        this.recycleUrls.add(AsyncTaskLoaderImage.formatUrl(userInfoModel2.getIcon(), "100x100"));
        if (SharedPreferencesDB.getInstance(this).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(AsyncTaskLoaderImage.formatUrl(userInfoModel2.getIcon(), "100x100"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    UserMyInfoActivity.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (!UserMyInfoActivity.this.isFinishing()) {
                                if (image != null && !image.isRecycled()) {
                                    UserMyInfoActivity.this.headerImg.setBackgroundDrawable(new BitmapDrawable(image));
                                }
                            } else if (image != null && !image.isRecycled()) {
                                image.recycle();
                            }
                        }
                    });
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void exitNoSaveEvent() {
        finish();
    }

    /* access modifiers changed from: protected */
    public void exitSaveEvent() {
        this.isExit = true;
        this.updateAsyncTask = new UpdateAsyncTask();
        this.updateAsyncTask.execute(this.currUserInfoModel);
    }

    /* access modifiers changed from: protected */
    public boolean exitCheckChanged() {
        getCurrUserInfoModel();
        return this.userInfoModel.equals(this.currUserInfoModel);
    }

    private void getCurrUserInfoModel() {
        this.currUserInfoModel.setNickname(this.nicknameEdit.getText().toString());
        this.currUserInfoModel.setIcon(this.iconPath);
        this.currUserInfoModel.setSignature("");
    }

    /* access modifiers changed from: private */
    public void getUserInfoModel() {
        this.userInfoModel.setNickname(this.currUserInfoModel.getNickname());
        this.userInfoModel.setIcon(this.currUserInfoModel.getIcon());
        this.userInfoModel.setSignature(this.currUserInfoModel.getSignature());
        this.userInfoModel.setGender(this.currUserInfoModel.getGender());
    }

    /* access modifiers changed from: protected */
    public void updateUIAfterUpload() {
        if (this.bitmap != null && this.uploadSucc && !this.bitmap.isRecycled()) {
            this.headerImg.setBackgroundDrawable(new BitmapDrawable(this.bitmap));
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.loadAsyncTask != null) {
            this.loadAsyncTask.cancel(true);
        }
        if (this.updateAsyncTask != null) {
            this.updateAsyncTask.cancel(true);
        }
    }
}
