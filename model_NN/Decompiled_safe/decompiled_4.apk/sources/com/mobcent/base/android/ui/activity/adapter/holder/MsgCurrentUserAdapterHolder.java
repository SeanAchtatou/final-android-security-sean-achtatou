package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.base.android.ui.activity.view.MCMsgCurrAudioView;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;

public class MsgCurrentUserAdapterHolder {
    private MCMsgCurrAudioView audioView;
    private TextView chatTime;
    private TextView content;
    private ImageView imgView;
    private MCProgressBar loading;
    private TextView sendStatus;
    private Button userIcon;

    public TextView getSendStatus() {
        return this.sendStatus;
    }

    public void setSendStatus(TextView sendStatus2) {
        this.sendStatus = sendStatus2;
    }

    public Button getUserIcon() {
        return this.userIcon;
    }

    public void setUserIcon(Button userIcon2) {
        this.userIcon = userIcon2;
    }

    public TextView getContent() {
        return this.content;
    }

    public void setContent(TextView content2) {
        this.content = content2;
    }

    public TextView getChatTime() {
        return this.chatTime;
    }

    public void setChatTime(TextView chatTime2) {
        this.chatTime = chatTime2;
    }

    public MCMsgCurrAudioView getAudioView() {
        return this.audioView;
    }

    public void setAudioView(MCMsgCurrAudioView audioView2) {
        this.audioView = audioView2;
    }

    public MCProgressBar getLoading() {
        return this.loading;
    }

    public void setLoading(MCProgressBar loading2) {
        this.loading = loading2;
    }

    public ImageView getImgView() {
        return this.imgView;
    }

    public void setImgView(ImageView imgView2) {
        this.imgView = imgView2;
    }
}
