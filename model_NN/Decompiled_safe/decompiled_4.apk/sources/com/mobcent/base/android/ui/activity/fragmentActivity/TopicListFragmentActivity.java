package com.mobcent.base.android.ui.activity.fragmentActivity;

import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import com.mobcent.base.android.ui.activity.fragment.TopicListFragment;
import java.util.List;

public class TopicListFragmentActivity extends BaseFragmentActivity {
    private TopicListFragment topicListFragment;

    /* access modifiers changed from: protected */
    public void initData() {
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        setContentView(this.resource.getLayoutId("mc_forum_topic_list_fragment_activity"));
        this.fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = this.fragmentManager.beginTransaction();
        this.topicListFragment = new TopicListFragment();
        fragmentTransaction.add(this.resource.getViewId("mc_forum_fragment"), this.topicListFragment);
        fragmentTransaction.commit();
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
    }

    /* access modifiers changed from: protected */
    public List<String> getAllImageURL() {
        return null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || this.topicListFragment == null || this.topicListFragment.publishTypeBox.getVisibility() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        this.topicListFragment.publishTypeBox.setVisibility(8);
        return true;
    }
}
