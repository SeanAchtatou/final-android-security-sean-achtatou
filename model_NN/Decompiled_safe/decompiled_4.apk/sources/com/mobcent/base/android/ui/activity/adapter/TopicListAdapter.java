package com.mobcent.base.android.ui.activity.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.ad.android.model.AdModel;
import com.mobcent.ad.android.ui.widget.AdView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.AnnounceActivity;
import com.mobcent.base.android.ui.activity.PostsActivity;
import com.mobcent.base.android.ui.activity.SearchTopicListActivity;
import com.mobcent.base.android.ui.activity.adapter.holder.TopicAdapterHolder;
import com.mobcent.base.android.ui.activity.fragmentActivity.TopicListFragmentActivity;
import com.mobcent.base.forum.android.util.MCColorUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.constant.PostsApiConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.AnnoModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.service.PermService;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.DateUtil;
import com.mobcent.forum.android.util.StringUtil;
import java.util.HashMap;
import java.util.List;

public class TopicListAdapter extends BaseAdapter implements PostsApiConstant, MCConstant {
    private String TAG = "TopicListFragment";
    /* access modifiers changed from: private */
    public Activity activity;
    private HashMap<Integer, List<AdModel>> adHashMap;
    private int adPosition;
    private AsyncTaskLoaderImage asyncTaskLoaderImage;
    private String boardName;
    private int bottomPosition;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private int page;
    /* access modifiers changed from: private */
    public PermService permService;
    private PostsClickListener postsClickListener;
    /* access modifiers changed from: private */
    public MCResource resource;
    private List<TopicModel> topicList;

    public interface PostsClickListener {
        void onPostsClick(View view, TopicModel topicModel, String str);
    }

    public TopicListAdapter(Activity activity2, List<TopicModel> topicList2, String boardName2, Handler mHandler2, LayoutInflater inflater2, AsyncTaskLoaderImage asyncTaskLoaderImage2, int adPosition2, PostsClickListener postsClickListener2, int bottomPosition2, int page2) {
        this.activity = activity2;
        this.topicList = topicList2;
        this.inflater = inflater2;
        this.boardName = boardName2;
        this.mHandler = mHandler2;
        this.resource = MCResource.getInstance(activity2);
        this.asyncTaskLoaderImage = asyncTaskLoaderImage2;
        this.adPosition = adPosition2;
        this.postsClickListener = postsClickListener2;
        this.bottomPosition = bottomPosition2;
        this.page = page2;
        this.permService = new PermServiceImpl(activity2);
    }

    public HashMap<Integer, List<AdModel>> getAdHashMap() {
        return this.adHashMap;
    }

    public void setAdHashMap(HashMap<Integer, List<AdModel>> adHashMap2) {
        this.adHashMap = adHashMap2;
    }

    public List<TopicModel> getTopicList() {
        return this.topicList;
    }

    public void setTopicList(List<TopicModel> topicList2) {
        this.topicList = topicList2;
    }

    public int getCount() {
        return getTopicList().size();
    }

    public Object getItem(int position) {
        return getTopicList().get(position);
    }

    public long getItemId(int position) {
        return getTopicList().get(position).getTopicId();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        String signStr;
        TopicModel topicModel = getTopicList().get(position);
        View convertView2 = getTopicConvertView(convertView);
        TopicAdapterHolder holder = (TopicAdapterHolder) convertView2.getTag();
        holder.getAdView().setVisibility(0);
        holder.getAdView().free();
        holder.getTopAdview().free();
        if (position == 0) {
            holder.getTopAdview().setVisibility(0);
            holder.getTopAdview().showAd(this.TAG, this.adPosition, position);
        } else if (position == 9) {
            if (this.activity instanceof TopicListFragmentActivity) {
                holder.getAdView().showAd(this.TAG, getadPostion("mc_forum_topic_list_position_milld"), position);
            } else if (this.activity instanceof SearchTopicListActivity) {
                holder.getAdView().showAd(this.TAG, getadPostion("mc_forum_search_topic_position_milld"), position);
            }
        } else if (position == (((position - 1) * 20) + position) - 1 && (this.activity instanceof TopicListFragmentActivity)) {
            holder.getAdView().showAd(this.TAG, this.bottomPosition, position);
        }
        if (topicModel instanceof AnnoModel) {
            AnnoModel annoModel = (AnnoModel) topicModel;
            signStr = this.activity.getResources().getString(this.resource.getStringId("mc_forum_announce_mark"));
            holder.getTopicPublishUserText().setText(annoModel.getAuthor());
            if (annoModel.getAnnoStartDate() >= 0) {
                holder.getTopicLastUpdateTimeText().setText(DateUtil.getFormatTimeExceptHourAndSecond(annoModel.getAnnoStartDate()));
            } else {
                holder.getTopicLastUpdateTimeText().setText("");
            }
            holder.getTopicReplyHitText().setText("");
        } else {
            if (topicModel.getUserNickName().length() <= 7) {
                holder.getTopicPublishUserText().setText(topicModel.getUserNickName());
            } else {
                holder.getTopicPublishUserText().setText(topicModel.getUserNickName().substring(0, 6) + "...");
            }
            if (topicModel.getLastReplyDate() >= 0) {
                holder.getTopicLastUpdateTimeText().setText(DateUtil.getFormatTime(topicModel.getLastReplyDate()));
            } else {
                holder.getTopicLastUpdateTimeText().setText("");
            }
            if (topicModel.getHitCount() > 0) {
                MCColorUtil.setTextViewPart(this.activity, holder.getTopicReplyHitText(), topicModel.getReplieCount() + "/" + topicModel.getHitCount(), 0, ("" + topicModel.getReplieCount()).length(), "mc_forum_text_hight_color");
            } else {
                holder.getTopicReplyHitText().setText("");
            }
            if (!(this.activity instanceof TopicListFragmentActivity)) {
                holder.getTopIconImage().setVisibility(8);
            } else if (topicModel.getTop() == 1) {
                holder.getTopIconImage().setVisibility(0);
            } else {
                holder.getTopIconImage().setVisibility(8);
            }
            if (topicModel.getHot() == 1 && topicModel.getEssence() == 1) {
                signStr = this.activity.getResources().getString(this.resource.getStringId("mc_forum_essence_posts_mark")) + this.activity.getResources().getString(this.resource.getStringId("mc_forum_hot_posts_mark"));
            } else if (topicModel.getHot() == 1) {
                signStr = this.activity.getResources().getString(this.resource.getStringId("mc_forum_hot_posts_mark"));
            } else if (topicModel.getEssence() == 1) {
                signStr = this.activity.getResources().getString(this.resource.getStringId("mc_forum_essence_posts_mark"));
            } else {
                signStr = "";
            }
            if (topicModel.getType() == 1) {
                signStr = signStr + "[" + this.activity.getResources().getString(this.resource.getStringId("mc_forum_poll")) + "]";
            }
            if (topicModel.getStatus() == 2) {
                signStr = signStr + "[" + this.activity.getResources().getString(this.resource.getStringId("mc_forum_topic_status_close")) + "]";
            }
        }
        String titleStr = signStr + topicModel.getTitle();
        int signEndPosition = signStr.length();
        holder.getTopicTitleText().setText(titleStr);
        MCColorUtil.setTextViewPart(this.activity, holder.getTopicTitleText(), titleStr, 0, signEndPosition, "mc_forum_text_hight_color");
        holder.getPreviewImage().setImageResource(this.resource.getDrawableId("mc_forum_x_img"));
        if (topicModel.getPicPath() == null || topicModel.getPicPath().trim().equals("")) {
            holder.getPreviewImage().setVisibility(8);
        } else if (topicModel.getStatus() != 0) {
            holder.getPreviewImage().setVisibility(0);
            updateThumbnailImage(topicModel.getBaseUrl() + topicModel.getPicPath(), holder.getPreviewImage());
        }
        if (topicModel.getUploadType() == 2) {
            holder.getTopicVoiceImg().setVisibility(0);
        } else {
            holder.getTopicVoiceImg().setVisibility(8);
        }
        final TopicModel topicModel2 = topicModel;
        convertView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (topicModel2 instanceof AnnoModel) {
                    TopicListAdapter.this.convertViewClickEvent(v, topicModel2);
                } else if (topicModel2.getUserId() == new UserServiceImpl(TopicListAdapter.this.activity).getLoginUserId()) {
                    TopicListAdapter.this.convertViewClickEvent(v, topicModel2);
                } else if (TopicListAdapter.this.permService.getPermNum(PermConstant.USER_GROUP, PermConstant.READ, -1) != 1 || TopicListAdapter.this.permService.getPermNum(PermConstant.BOARDS, PermConstant.READ, topicModel2.getBoardId()) != 1) {
                    Toast.makeText(TopicListAdapter.this.activity, TopicListAdapter.this.resource.getString("mc_forum_permission_cannot_read_topic"), 0).show();
                } else if (topicModel2.getVisible() != 3) {
                    TopicListAdapter.this.convertViewClickEvent(v, topicModel2);
                } else if (StringUtil.isEmpty(TopicListAdapter.this.permService.getGroupType()) || !PermConstant.SYSTEM.equals(TopicListAdapter.this.permService.getGroupType())) {
                    Toast.makeText(TopicListAdapter.this.activity, TopicListAdapter.this.resource.getString("mc_forum_permission_only_moderator_see"), 0).show();
                } else {
                    TopicListAdapter.this.convertViewClickEvent(v, topicModel2);
                }
            }
        });
        return convertView2;
    }

    /* access modifiers changed from: private */
    public void convertViewClickEvent(View v, TopicModel topicModel) {
        if (topicModel instanceof AnnoModel) {
            Intent intent = new Intent(this.activity, AnnounceActivity.class);
            intent.putExtra("boardId", topicModel.getBoardId());
            intent.putExtra("boardName", this.boardName);
            intent.putExtra("announceId", ((AnnoModel) topicModel).getAnnoId());
            intent.putExtra("baseUrl", topicModel.getBaseUrl());
            this.activity.startActivity(intent);
        } else if (this.postsClickListener != null) {
            this.postsClickListener.onPostsClick(v, topicModel, this.boardName);
        } else {
            Intent intent2 = new Intent(this.activity, PostsActivity.class);
            intent2.putExtra("boardId", topicModel.getBoardId());
            intent2.putExtra("boardName", this.boardName);
            intent2.putExtra("topicId", topicModel.getTopicId());
            intent2.putExtra(MCConstant.TOPIC_USER_ID, topicModel.getUserId());
            intent2.putExtra("baseUrl", topicModel.getBaseUrl());
            intent2.putExtra(MCConstant.THUMBNAIL_IMAGE_URL, topicModel.getPicPath());
            intent2.putExtra("type", topicModel.getType());
            intent2.putExtra(MCConstant.TOP, topicModel.getTop());
            intent2.putExtra(MCConstant.ESSENCE, topicModel.getEssence());
            intent2.putExtra(MCConstant.CLOSE, topicModel.getStatus());
            this.activity.startActivity(intent2);
        }
    }

    private int getadPostion(String position) {
        return new Integer(this.activity.getResources().getString(this.resource.getStringId(position))).intValue();
    }

    private View getTopicConvertView(View convertView) {
        TopicAdapterHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_topic_item"), (ViewGroup) null);
            holder = new TopicAdapterHolder();
            initTopicAdapterHolder(convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (TopicAdapterHolder) convertView.getTag();
        }
        if (holder != null) {
            return convertView;
        }
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_topic_item"), (ViewGroup) null);
        TopicAdapterHolder holder2 = new TopicAdapterHolder();
        initTopicAdapterHolder(convertView2, holder2);
        convertView2.setTag(holder2);
        return convertView2;
    }

    private void initTopicAdapterHolder(View convertView, TopicAdapterHolder holder) {
        holder.setPreviewImage((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_thumbnail_img")));
        holder.setTopicLastUpdateTimeText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_last_update_time_text")));
        holder.setTopicPublishUserText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_nickname_text")));
        holder.setTopicReplyHitText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_reply_hit_text")));
        holder.setTopicTitleText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_topic_title_text")));
        holder.setTopIconImage((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_top_icon_img")));
        holder.setAdView((AdView) convertView.findViewById(this.resource.getViewId("mc_ad_box")));
        holder.setTopAdview((AdView) convertView.findViewById(this.resource.getViewId("mc_top_ad_box")));
        holder.setTopicVoiceImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_topic_voice_img")));
        holder.setTopicItemBox((RelativeLayout) convertView.findViewById(this.resource.getViewId("mc_forum_topic_item_box")));
    }

    private void updateThumbnailImage(String imgUrl, final ImageView imageView) {
        if (SharedPreferencesDB.getInstance(this.activity).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(AsyncTaskLoaderImage.formatUrl(imgUrl, "100x100"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    if (image != null) {
                        TopicListAdapter.this.mHandler.post(new Runnable() {
                            public void run() {
                                if (image != null) {
                                    imageView.setVisibility(0);
                                    imageView.setImageBitmap(image);
                                }
                            }
                        });
                    }
                }
            });
        }
    }
}
