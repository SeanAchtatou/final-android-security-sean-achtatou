package com.mobcent.base.android.ui.activity.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.holder.MsgUserAdapterHolder;
import com.mobcent.base.android.ui.activity.fragmentActivity.MsgChatRoomFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.HeartMsgModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.HeartMsgService;
import com.mobcent.forum.android.service.impl.HeartMsgServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import java.util.List;

public class MsgUserListAdapter extends BaseAdapter implements MCConstant {
    private AsyncTaskLoaderImage asyncTaskLoaderImage;
    /* access modifiers changed from: private */
    public Context context;
    private List<HeartMsgModel> heartBeatModelList = this.heartBeatService.getHeartBeatListLocally();
    private HeartMsgService heartBeatService;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public MCResource resource;
    private List<UserInfoModel> userInfoList;

    public MsgUserListAdapter(Context context2, List<UserInfoModel> userInfoList2, Handler mHandler2, AsyncTaskLoaderImage asyncTaskLoaderImage2) {
        this.context = context2;
        this.userInfoList = userInfoList2;
        this.inflater = LayoutInflater.from(context2);
        this.mHandler = mHandler2;
        this.resource = MCResource.getInstance(context2);
        this.heartBeatService = new HeartMsgServiceImpl(context2);
        this.asyncTaskLoaderImage = asyncTaskLoaderImage2;
    }

    public List<UserInfoModel> getUserInfoList() {
        return this.userInfoList;
    }

    public void setUserInfoList(List<UserInfoModel> userInfoList2) {
        this.userInfoList = userInfoList2;
    }

    public void setHeartBeatModelList(List<HeartMsgModel> heartBeatModelList2) {
        this.heartBeatModelList = heartBeatModelList2;
    }

    public int getCount() {
        return getUserInfoList().size();
    }

    public Object getItem(int position) {
        return getUserInfoList().get(position);
    }

    public long getItemId(int position) {
        return getUserInfoList().get(position).getUserId();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final UserInfoModel userInfoModel = getUserInfoList().get(position);
        View convertView2 = getMsgUserInfoConvertView(convertView);
        MsgUserAdapterHolder holder = (MsgUserAdapterHolder) convertView2.getTag();
        holder.getUserNicknameText().setText(userInfoModel.getNickname());
        int userMsgNum = getNewMsgCount(userInfoModel.getUserId());
        if (userMsgNum > 0) {
            holder.getMsgCountText().setText("(" + userMsgNum + ")");
            holder.getNewMsgImg().setVisibility(0);
        } else {
            holder.getMsgCountText().setText("");
            holder.getNewMsgImg().setVisibility(8);
        }
        holder.getUserIconImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCForumHelper.gotoUserInfo((Activity) MsgUserListAdapter.this.context, MsgUserListAdapter.this.resource, userInfoModel.getUserId());
            }
        });
        convertView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MsgUserListAdapter.this.context, MsgChatRoomFragmentActivity.class);
                intent.putExtra(MCConstant.CHAT_USER_ID, userInfoModel.getUserId());
                intent.putExtra(MCConstant.CHAT_USER_NICKNAME, userInfoModel.getNickname());
                intent.putExtra(MCConstant.CHAT_USER_ICON, userInfoModel.getIcon());
                intent.putExtra(MCConstant.BLACK_USER_STATUS, userInfoModel.getBlackStatus());
                MsgUserListAdapter.this.context.startActivity(intent);
            }
        });
        updateUserIconImage(userInfoModel.getIcon(), convertView2, holder.getUserIconImg());
        return convertView2;
    }

    private int getNewMsgCount(long userId) {
        int count = 0;
        for (HeartMsgModel heartBeatModel : this.heartBeatModelList) {
            if (heartBeatModel.getFormUserId() == userId) {
                count++;
            }
        }
        return count;
    }

    private View getMsgUserInfoConvertView(View convertView) {
        MsgUserAdapterHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_msg_user_item"), (ViewGroup) null);
            holder = new MsgUserAdapterHolder();
            convertView.setTag(holder);
            initMsgUserAdapterHolder(convertView, holder);
        } else {
            holder = (MsgUserAdapterHolder) convertView.getTag();
        }
        if (holder == null) {
            initMsgUserAdapterHolder(convertView, holder);
        }
        return convertView;
    }

    private void initMsgUserAdapterHolder(View convertView, MsgUserAdapterHolder holder) {
        holder.setUserIconImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_user_icon_img")));
        holder.setUserRoleImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_user_role_img")));
        holder.setMsgCountText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_new_msg_count_text")));
        holder.setUserNicknameText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_user_name_text")));
        holder.setNewMsgImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_new_msg_img")));
    }

    private void updateUserIconImage(String imgUrl, View convertView, final ImageView userIconImg) {
        userIconImg.setImageResource(this.resource.getDrawableId("mc_forum_head"));
        if (SharedPreferencesDB.getInstance(this.context).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(AsyncTaskLoaderImage.formatUrl(imgUrl, "100x100"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    if (image != null) {
                        MsgUserListAdapter.this.mHandler.post(new Runnable() {
                            public void run() {
                                if (image != null) {
                                    userIconImg.setImageBitmap(image);
                                }
                            }
                        });
                    }
                }
            });
        }
    }
}
