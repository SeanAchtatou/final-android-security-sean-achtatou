package com.mobcent.base.android.ui.activity;

import com.mobcent.forum.android.model.ReportModel;
import java.util.List;

public class ReportUserManageActivity extends BaseReportManageActivity {
    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        this.titleText.setText(getResources().getString(this.resource.getStringId("mc_forum_report_user_manage")));
    }

    /* access modifiers changed from: protected */
    public List<ReportModel> getReportManageList(int page, int pageSize) {
        return this.reportService.getReportUserList(page, pageSize);
    }
}
