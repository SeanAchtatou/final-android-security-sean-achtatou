package com.mobcent.base.android.ui.activity.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.ad.android.model.AdModel;
import com.mobcent.ad.android.ui.widget.AdView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.ModeratorActivity;
import com.mobcent.base.android.ui.activity.PostsActivity;
import com.mobcent.base.android.ui.activity.ReplyTopicActivity;
import com.mobcent.base.android.ui.activity.UserBannedActivity;
import com.mobcent.base.android.ui.activity.UserShieldedActivity;
import com.mobcent.base.android.ui.activity.adapter.holder.PostsReplyAdapterHolder;
import com.mobcent.base.android.ui.activity.adapter.holder.PostsTopicAdapterHolder;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.CancelBannedShieldedAsyncTask;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.CloseTopicAsyncTask;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.EssenceTopicAsyncTask;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.FollowUserAsyncTask;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.LongPicAsynTask;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.PhysicalDelTopicAsyncTask;
import com.mobcent.base.android.ui.activity.asyncTaskLoader.TopTopicAsyncTask;
import com.mobcent.base.android.ui.activity.delegate.LongTaskDelegate;
import com.mobcent.base.android.ui.activity.delegate.PostsFavorDelegate;
import com.mobcent.base.android.ui.activity.delegate.TaskExecuteDelegate;
import com.mobcent.base.android.ui.activity.delegate.TopicManageDelegate;
import com.mobcent.base.android.ui.activity.fragmentActivity.ImageViewerFragmentActivity;
import com.mobcent.base.android.ui.activity.fragmentActivity.MsgChatRoomFragmentActivity;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.android.ui.activity.helper.MCForumLaunchShareHelper;
import com.mobcent.base.android.ui.activity.service.LoginInterceptor;
import com.mobcent.base.android.ui.activity.view.MCLevelView;
import com.mobcent.base.android.ui.activity.view.MCPollSelectBar;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;
import com.mobcent.base.android.ui.activity.view.MCTopicManageDialog;
import com.mobcent.base.forum.android.util.MCFaceUtil;
import com.mobcent.base.forum.android.util.MCForumErrorUtil;
import com.mobcent.base.forum.android.util.MCForumReverseList;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.base.forum.android.util.MCStringBundleUtil;
import com.mobcent.base.forum.android.util.MCTouchUtil;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.constant.MCForumConstant;
import com.mobcent.forum.android.constant.MentionFriendConstant;
import com.mobcent.forum.android.db.PollPostsDBUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.PollItemModel;
import com.mobcent.forum.android.model.PollTopicModel;
import com.mobcent.forum.android.model.PostsModel;
import com.mobcent.forum.android.model.ReplyModel;
import com.mobcent.forum.android.model.RepostTopicModel;
import com.mobcent.forum.android.model.RichImageModel;
import com.mobcent.forum.android.model.TopicContentModel;
import com.mobcent.forum.android.model.TopicModel;
import com.mobcent.forum.android.model.UserInfoModel;
import com.mobcent.forum.android.service.ModeratorService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.ModeratorServiceImpl;
import com.mobcent.forum.android.service.impl.PostsServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.DateUtil;
import com.mobcent.forum.android.util.StringUtil;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PostsListAdapter extends BaseSoundListAdapter implements MCConstant, MentionFriendConstant {
    public static int POSTS_STATUS_CLOSE = 2;
    private int MAIN_POSTS_POSITION;
    private int POSTS_POSITION;
    private String TAG = PostsActivity.TAG;
    private HashMap<Integer, List<AdModel>> adHashMap;
    private AsyncTaskLoaderImage asyncTaskLoaderImage;
    /* access modifiers changed from: private */
    public long boardId;
    /* access modifiers changed from: private */
    public String boardName;
    private CancelBannedShieldedAsyncTask cancelBannedShieldedAsyncTask;
    public int close;
    private CloseTopicAsyncTask closeTopicAsyncTask;
    /* access modifiers changed from: private */
    public long currentUserId = 0;
    private PhysicalDelTopicAsyncTask delTopicAsyncTask;
    public int essence;
    private EssenceTopicAsyncTask essenceTopicAsyncTask;
    /* access modifiers changed from: private */
    public PostsFavorDelegate favorDelegate;
    /* access modifiers changed from: private */
    public ProgressDialog loadDialog;
    private LongPicAsynTask longPicAsynTask;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private ModeratorService moderatorService;
    /* access modifiers changed from: private */
    public ProgressDialog myDialog;
    /* access modifiers changed from: private */
    public List<PollItemModel> pollItemList;
    /* access modifiers changed from: private */
    public PollTopicModel pollModel;
    private UserPollTask pollTask;
    private int pollType;
    private List<MCPollSelectBar> pollViewList;
    /* access modifiers changed from: private */
    public PollPostsDBUtil postsDBUtil;
    private List<PostsModel> postsList;
    private RepostTopicModel repostTopicModel;
    /* access modifiers changed from: private */
    public ArrayList<RichImageModel> richImageModelList;
    /* access modifiers changed from: private */
    public List<MCPollSelectBar> selectedPollViewList;
    private String thumbnailUrl;
    public int top;
    private TopTopicAsyncTask topTopicAsyncTask;
    private List<TopicContentModel> topicContentList;
    /* access modifiers changed from: private */
    public PostsTopicAdapterHolder topicHolder;
    /* access modifiers changed from: private */
    public TopicModel topicModel;
    private int topicType = 0;
    /* access modifiers changed from: private */
    public UserService userService;

    public PostsListAdapter(Context context, List<PostsModel> postsList2, long boardId2, String boardName2, String thumbnailUrl2, Handler mHandler2, LayoutInflater inflater, int type, AsyncTaskLoaderImage asyncTaskLoaderImage2, PostsFavorDelegate favorDelegate2) {
        super(context, context.toString(), inflater);
        this.postsList = postsList2;
        this.mHandler = mHandler2;
        this.resource = MCResource.getInstance(this.context);
        this.boardId = boardId2;
        this.boardName = boardName2;
        this.thumbnailUrl = thumbnailUrl2;
        this.richImageModelList = getAllImageUrl(postsList2);
        this.userService = new UserServiceImpl(context);
        this.currentUserId = this.userService.getLoginUserId();
        this.topicType = type;
        this.selectedPollViewList = new ArrayList();
        this.pollViewList = new ArrayList();
        this.postsDBUtil = PollPostsDBUtil.getInstance(context);
        this.pollItemList = new ArrayList();
        this.asyncTaskLoaderImage = asyncTaskLoaderImage2;
        this.MAIN_POSTS_POSITION = new Integer(context.getResources().getString(this.resource.getStringId("mc_forum_main_posts_position"))).intValue();
        this.POSTS_POSITION = new Integer(context.getResources().getString(this.resource.getStringId("mc_forum_posts_position"))).intValue();
        this.moderatorService = new ModeratorServiceImpl(context);
        this.favorDelegate = favorDelegate2;
    }

    public HashMap<Integer, List<AdModel>> getAdHashMap() {
        return this.adHashMap;
    }

    public void setAdHashMap(HashMap<Integer, List<AdModel>> adHashMap2) {
        this.adHashMap = adHashMap2;
    }

    public int getCount() {
        return this.postsList.size();
    }

    public PostsModel getItem(int position) {
        return this.postsList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public List<PostsModel> getPostsList() {
        return this.postsList;
    }

    public void setPostsList(List<PostsModel> postsList2) {
        this.postsList = postsList2;
        if (!postsList2.isEmpty() && postsList2.get(0).getPostType() == 1) {
            this.topicModel = postsList2.get(0).getTopic();
            this.topicContentList = this.topicModel.getTopicContentList();
            if (this.topicContentList != null && this.topicContentList.size() > 0) {
                this.repostTopicModel = this.topicContentList.get(0).getRepostTopicModel();
            }
        }
        this.richImageModelList = getAllImageUrl(postsList2);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (getItem(position).getPostType() == 1) {
            return getTopicView(position, convertView, parent);
        }
        return getReplyView(position, convertView, parent);
    }

    public View getTopicView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_posts_topic_item"), (ViewGroup) null);
            this.topicHolder = new PostsTopicAdapterHolder();
            initPostsTopicAdapterHolder(convertView, this.topicHolder);
            convertView.setTag(this.topicHolder);
        } else {
            try {
                this.topicHolder = (PostsTopicAdapterHolder) convertView.getTag();
            } catch (ClassCastException e) {
                convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_posts_topic_item"), (ViewGroup) null);
                this.topicHolder = new PostsTopicAdapterHolder();
                initPostsTopicAdapterHolder(convertView, this.topicHolder);
                convertView.setTag(this.topicHolder);
            }
        }
        if (this.topicHolder == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_posts_topic_item"), (ViewGroup) null);
            this.topicHolder = new PostsTopicAdapterHolder();
            initPostsTopicAdapterHolder(convertView, this.topicHolder);
            convertView.setTag(this.topicHolder);
        }
        this.topicModel = getItem(position).getTopic();
        this.topicModel.setEssence(this.essence);
        this.topicModel.setTop(this.top);
        if (this.close == POSTS_STATUS_CLOSE) {
            this.topicModel.setStatus(this.close);
        }
        initPostsTopicActions(convertView, this.topicHolder, this.topicModel);
        this.topicHolder.getAdView().setVisibility(0);
        this.topicHolder.getAdView().free();
        this.topicHolder.getAdView().showAd(this.TAG, this.MAIN_POSTS_POSITION, position);
        if (this.topicModel.getStatus() == 0) {
            updatePostTopicDeletedView(convertView, this.topicHolder, this.topicModel);
        } else {
            updatePostTopicNormalView(convertView, this.topicHolder, this.topicModel);
        }
        return convertView;
    }

    private void initPostsTopicAdapterHolder(View convertView, PostsTopicAdapterHolder topicHolder2) {
        topicHolder2.setTopicUserImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_topic_user_img")));
        topicHolder2.setRoleImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_role_img")));
        topicHolder2.setUserRoleText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_role_text")));
        topicHolder2.setTopicTitleText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_topic_title_text")));
        topicHolder2.setTopicUserText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_topic_user_text")));
        topicHolder2.setTopicTimeText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_topic_time_text")));
        topicHolder2.setTopicContentText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_topic_content_text")));
        topicHolder2.setTopicThumbnailImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_topic_thumbnail_img")));
        topicHolder2.setTopicMoreImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_topic_more_img")));
        topicHolder2.setTopicFavoriteBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_topic_favorite_btn")));
        topicHolder2.setTopicShareBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_topic_share_btn")));
        topicHolder2.setTopicReplyBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_topic_reply_btn")));
        topicHolder2.setTopicDeleteBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_topic_delete_btn")));
        topicHolder2.setTopicMoreBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_topic_more_btn")));
        topicHolder2.setTopicContentLayout((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_topic_content_layout")));
        topicHolder2.setTopicMoreBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_topic_more_btn")));
        topicHolder2.setLoadingContainer((RelativeLayout) convertView.findViewById(this.resource.getViewId("mc_forum_loading_container")));
        topicHolder2.setProgressBar((MCProgressBar) convertView.findViewById(this.resource.getViewId("mc_forum_progress_bar")));
        topicHolder2.setPollImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_froum_poll_img")));
        topicHolder2.setAdView((AdView) convertView.findViewById(this.resource.getViewId("mc_ad_box")));
        topicHolder2.setPollLayout((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_poll_layout")));
        topicHolder2.setPollSelectLayout((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_poll_select_layout")));
        topicHolder2.setPollSubmitBtn((Button) convertView.findViewById(this.resource.getViewId("mc_froum_poll_submit_btn")));
        topicHolder2.setPollRsLayout((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_poll_rs_layout")));
        topicHolder2.setPollTitleText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_poll_title_text")));
        topicHolder2.setLocationBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_posts_location_box")));
        topicHolder2.setLocationText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_topic_location_text")));
        topicHolder2.setLevelBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_level_box")));
        topicHolder2.setRepostBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_posts_repost_box")));
        topicHolder2.setRepostUserName((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_posts_repost_user_name")));
        topicHolder2.setRepostContentLayout((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_posts_repost_content_layout")));
        topicHolder2.setRepostBoardName((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_posts_repost_board_name")));
        topicHolder2.setRepostTime((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_posts_repost_time")));
        topicHolder2.setLevelBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_level_box")));
        topicHolder2.setTopicBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_topic_layout")));
        MCTouchUtil.createTouchDelegate(topicHolder2.getTopicFavoriteBtn(), 10);
        MCTouchUtil.createTouchDelegate(topicHolder2.getTopicShareBtn(), 10);
        MCTouchUtil.createTouchDelegate(topicHolder2.getTopicReplyBtn(), 10);
        MCTouchUtil.createTouchDelegate(topicHolder2.getTopicDeleteBtn(), 10);
    }

    private void initPostsTopicActions(View topicView, PostsTopicAdapterHolder topicHolder2, final TopicModel topicModel2) {
        topicHolder2.getTopicFavoriteBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (LoginInterceptor.doInterceptor(PostsListAdapter.this.context, null, null)) {
                    PostsListAdapter.this.favorDelegate.onFavorBtnClick();
                }
            }
        });
        topicHolder2.getTopicShareBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PostsListAdapter.this.shareEvent(PostsListAdapter.this.getShareContent(topicModel2, null), topicModel2, null, !StringUtil.isEmpty(SharedPreferencesDB.getInstance(PostsListAdapter.this.context).getWeiXinAppKey()), WXAPIFactory.createWXAPI(PostsListAdapter.this.context, SharedPreferencesDB.getInstance(PostsListAdapter.this.context).getWeiXinAppKey()));
            }
        });
        topicHolder2.getTopicReplyBtn().setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                HashMap<String, Serializable> param = new HashMap<>();
                param.put("boardId", Long.valueOf(PostsListAdapter.this.boardId));
                param.put("boardName", PostsListAdapter.this.boardName);
                param.put("topicId", Long.valueOf(topicModel2.getTopicId()));
                param.put("toReplyId", -1L);
                param.put(MCConstant.IS_QUOTE_TOPIC, true);
                ArrayList<UserInfoModel> userList = (ArrayList) ((PostsActivity) PostsListAdapter.this.context).getPostsUserList();
                param.put(MCConstant.POSTS_USER_LIST, userList);
                if (LoginInterceptor.doInterceptor(PostsListAdapter.this.context, ReplyTopicActivity.class, param)) {
                    Intent intent = new Intent(PostsListAdapter.this.context, ReplyTopicActivity.class);
                    intent.putExtra("boardId", PostsListAdapter.this.boardId);
                    intent.putExtra("boardName", PostsListAdapter.this.boardName);
                    intent.putExtra("topicId", topicModel2.getTopicId());
                    intent.putExtra("toReplyId", -1L);
                    intent.putExtra(MCConstant.IS_QUOTE_TOPIC, true);
                    intent.putExtra(MCConstant.POSTS_USER_LIST, userList);
                    PostsListAdapter.this.context.startActivity(intent);
                }
            }
        });
        topicHolder2.getTopicMoreBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (LoginInterceptor.doInterceptorByDialog(PostsListAdapter.this.context, PostsListAdapter.this.resource, null, null)) {
                    new MCTopicManageDialog(PostsListAdapter.this.context, PostsListAdapter.this.boardId, PostsListAdapter.this.boardName, topicModel2, null, 1, new TopicManageDelegate() {
                        /* Debug info: failed to restart local var, previous not found, register: 3 */
                        public void topicManageDelegate(int type) {
                            switch (type) {
                                case 1:
                                    topicModel2.setStatus(0);
                                    ((PostsActivity) PostsListAdapter.this.context).getRefreshContentDelegate().refreshContent();
                                    return;
                                case 2:
                                    topicModel2.setTop(1);
                                    return;
                                case 3:
                                    topicModel2.setTop(0);
                                    return;
                                case 4:
                                    topicModel2.setEssence(1);
                                    return;
                                case 5:
                                    topicModel2.setEssence(0);
                                    return;
                                case 6:
                                    topicModel2.setStatus(PostsListAdapter.POSTS_STATUS_CLOSE);
                                    return;
                                case 7:
                                    topicModel2.setStatus(1);
                                    return;
                                default:
                                    return;
                            }
                        }
                    }, true).show();
                }
            }
        });
        topicHolder2.getTopicUserImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PostsListAdapter.this.getUserFunctionDialog(topicModel2);
            }
        });
        topicHolder2.getPollSubmitBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PostsListAdapter.this.userService.isLogin()) {
                    PostsListAdapter.this.publishPoll();
                } else if (LoginInterceptor.doInterceptor(PostsListAdapter.this.context, null, null)) {
                    PostsListAdapter.this.publishPoll();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void publishPoll() {
        int size = this.selectedPollViewList.size();
        if (size == 0) {
            Toast.makeText(this.context, this.context.getText(this.resource.getStringId("mc_forum_poll_select")), 0).show();
        } else if (size > this.pollType) {
            Toast.makeText(this.context, MCStringBundleUtil.resolveString(this.resource.getStringId("mc_forum_poll_single"), this.pollType + "", this.context), 0).show();
        } else {
            String itemId = "";
            for (MCPollSelectBar poll : this.selectedPollViewList) {
                if ("".equals(itemId)) {
                    itemId = poll.getPollItem().getPollItemId() + "";
                } else {
                    itemId = itemId + AdApiConstant.RES_SPLIT_COMMA + poll.getPollItem().getPollItemId();
                }
            }
            this.pollTask = new UserPollTask();
            this.pollTask.execute(this.boardId + "", this.topicModel.getTopicId() + "", itemId);
        }
    }

    private void updatePostTopicDeletedView(View topicView, PostsTopicAdapterHolder topicHolder2, TopicModel topicModel2) {
        topicHolder2.getLevelBox().removeAllViews();
        new MCLevelView(this.context, topicModel2.getLevel(), topicHolder2.getLevelBox(), 0);
        showUserRole(topicHolder2.getUserRoleText(), topicModel2.getRoleNum(), topicModel2.isFollow(), topicModel2.getUserId());
        if (this.topicType == 1) {
            topicHolder2.getPollImg().setVisibility(0);
        }
        topicHolder2.getTopicUserText().setText(topicModel2.getUserNickName());
        topicHolder2.getTopicTimeText().setText(DateUtil.getFormatTimeByYear(topicModel2.getCreateDate()));
        topicHolder2.getTopicTitleText().setText(this.context.getResources().getString(this.resource.getStringId("mc_forum_topic_deleted")));
        topicHolder2.getTopicContentText().setText("");
        topicHolder2.getTopicThumbnailImg().setVisibility(8);
        topicHolder2.getTopicMoreImg().setVisibility(8);
        topicHolder2.getTopicFavoriteBtn().setVisibility(8);
        topicHolder2.getTopicShareBtn().setVisibility(8);
        topicHolder2.getTopicReplyBtn().setVisibility(8);
        topicHolder2.getTopicDeleteBtn().setVisibility(8);
        topicHolder2.getTopicContentLayout().setVisibility(8);
        topicHolder2.getRepostBox().setVisibility(8);
        topicHolder2.getPollLayout().setVisibility(8);
        topicHolder2.getTopicMoreBtn().setVisibility(8);
        if (!StringUtil.isEmpty(topicModel2.getLocation())) {
            topicHolder2.getLocationBox().setVisibility(0);
            topicHolder2.getLocationText().setText(topicModel2.getLocation());
        } else {
            topicHolder2.getLocationBox().setVisibility(8);
            topicHolder2.getLocationText().setText("");
        }
        topicHolder2.getTopicUserImg().setImageResource(this.resource.getDrawableId("mc_forum_head"));
        if (!StringUtil.isEmpty(topicModel2.getIcon())) {
            updateImageView(AsyncTaskLoaderImage.formatUrl(topicModel2.getIconUrl() + topicModel2.getIcon(), "100x100"), topicHolder2.getTopicUserImg());
        }
    }

    private void updatePostTopicNormalView(View topicView, PostsTopicAdapterHolder topicHolder2, TopicModel topicModel2) {
        topicHolder2.getLevelBox().removeAllViews();
        new MCLevelView(this.context, topicModel2.getLevel(), topicHolder2.getLevelBox(), 0);
        showUserRole(topicHolder2.getUserRoleText(), topicModel2.getRoleNum(), topicModel2.isFollow(), topicModel2.getUserId());
        if (this.topicType == 1) {
            topicHolder2.getPollImg().setVisibility(0);
        }
        topicHolder2.getTopicDeleteBtn().setVisibility(8);
        if (topicModel2.getIsFavor() == 1) {
            topicHolder2.getTopicFavoriteBtn().setBackgroundDrawable(this.context.getResources().getDrawable(this.resource.getDrawableId("mc_forum_icon27")));
        } else {
            topicHolder2.getTopicFavoriteBtn().setBackgroundDrawable(this.context.getResources().getDrawable(this.resource.getDrawableId("mc_forum_icon28")));
        }
        topicHolder2.getTopicTitleText().setText(topicModel2.getTitle());
        MCFaceUtil.setStrToFace(topicHolder2.getTopicTitleText(), topicModel2.getTitle(), this.context);
        topicHolder2.getTopicUserText().setText(topicModel2.getUserNickName());
        topicHolder2.getTopicTimeText().setText(DateUtil.getFormatTimeByYear(topicModel2.getCreateDate()));
        if (!StringUtil.isEmpty(this.thumbnailUrl)) {
            topicModel2.setHasImg(true);
        }
        if (!StringUtil.isEmpty(topicModel2.getLocation())) {
            topicHolder2.getLocationBox().setVisibility(0);
            topicHolder2.getLocationText().setText(topicModel2.getLocation());
        } else {
            topicHolder2.getLocationBox().setVisibility(8);
            topicHolder2.getLocationText().setText("");
        }
        topicHolder2.getTopicUserImg().setImageResource(this.resource.getDrawableId("mc_forum_head"));
        if (!StringUtil.isEmpty(topicModel2.getIcon())) {
            updateImageView(AsyncTaskLoaderImage.formatUrl(topicModel2.getIconUrl() + topicModel2.getIcon(), "100x100"), topicHolder2.getTopicUserImg());
        }
        topicHolder2.getTopicContentLayout().setVisibility(0);
        this.topicContentList = topicModel2.getTopicContentList();
        if (this.topicContentList != null && this.topicContentList.size() > 0) {
            updatePostsDetailView(this.topicContentList, topicHolder2.getTopicContentLayout());
            this.pollModel = this.topicContentList.get(0).getPollTopicModel();
            updatePollLayoutView(this.pollModel, topicHolder2);
            this.repostTopicModel = this.topicContentList.get(0).getRepostTopicModel();
            if (this.repostTopicModel != null) {
                updateTopicRepostView(this.repostTopicModel, topicHolder2);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0147  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getReplyView(int r12, android.view.View r13, android.view.ViewGroup r14) {
        /*
            r11 = this;
            r10 = 10
            r9 = 9
            r8 = 0
            r7 = 0
            r6 = 8
            com.mobcent.forum.android.model.PostsModel r3 = r11.getItem(r12)
            com.mobcent.forum.android.model.ReplyModel r2 = r3.getReply()
            r1 = 0
            if (r13 != 0) goto L_0x0093
            android.view.LayoutInflater r3 = r11.inflater
            com.mobcent.base.forum.android.util.MCResource r4 = r11.resource
            java.lang.String r5 = "mc_forum_posts_reply_item"
            int r4 = r4.getLayoutId(r5)
            android.view.View r13 = r3.inflate(r4, r7)
            com.mobcent.base.android.ui.activity.adapter.holder.PostsReplyAdapterHolder r1 = new com.mobcent.base.android.ui.activity.adapter.holder.PostsReplyAdapterHolder
            r1.<init>()
            r11.initReplyListAdapterHolder(r13, r1)
            r13.setTag(r1)
        L_0x002c:
            if (r1 != 0) goto L_0x0047
            android.view.LayoutInflater r3 = r11.inflater
            com.mobcent.base.forum.android.util.MCResource r4 = r11.resource
            java.lang.String r5 = "mc_forum_posts_reply_item"
            int r4 = r4.getLayoutId(r5)
            android.view.View r13 = r3.inflate(r4, r7)
            com.mobcent.base.android.ui.activity.adapter.holder.PostsReplyAdapterHolder r1 = new com.mobcent.base.android.ui.activity.adapter.holder.PostsReplyAdapterHolder
            r1.<init>()
            r11.initReplyListAdapterHolder(r13, r1)
            r13.setTag(r1)
        L_0x0047:
            if (r12 == 0) goto L_0x005c
            boolean r3 = r11.isContainFloorAuther()
            if (r3 == 0) goto L_0x00b7
            r3 = r10
        L_0x0050:
            if (r12 == r3) goto L_0x005c
            boolean r3 = r11.isContainFloorAuther()
            if (r3 == 0) goto L_0x00b9
            r3 = 20
        L_0x005a:
            if (r12 != r3) goto L_0x0129
        L_0x005c:
            com.mobcent.ad.android.ui.widget.AdView r3 = r1.getTopAdViewBox()
            r3.free()
            com.mobcent.ad.android.ui.widget.AdView r3 = r1.getAdView()
            r3.free()
            if (r12 != 0) goto L_0x00bc
            com.mobcent.ad.android.ui.widget.AdView r3 = r1.getTopAdViewBox()
            r3.setVisibility(r8)
            com.mobcent.ad.android.ui.widget.AdView r3 = r1.getAdView()
            r3.setVisibility(r6)
            com.mobcent.ad.android.ui.widget.AdView r3 = r1.getTopAdViewBox()
            java.lang.String r4 = r11.TAG
            int r5 = r11.POSTS_POSITION
            int r6 = r2.getFloor()
            r3.showAd(r4, r5, r6)
        L_0x0089:
            int r3 = r2.getStatus()
            if (r3 != 0) goto L_0x0147
            r11.updateDeletedReplyView(r2, r1, r12)
        L_0x0092:
            return r13
        L_0x0093:
            java.lang.Object r1 = r13.getTag()     // Catch:{ ClassCastException -> 0x009a }
            com.mobcent.base.android.ui.activity.adapter.holder.PostsReplyAdapterHolder r1 = (com.mobcent.base.android.ui.activity.adapter.holder.PostsReplyAdapterHolder) r1     // Catch:{ ClassCastException -> 0x009a }
            goto L_0x002c
        L_0x009a:
            r3 = move-exception
            r0 = r3
            android.view.LayoutInflater r3 = r11.inflater
            com.mobcent.base.forum.android.util.MCResource r4 = r11.resource
            java.lang.String r5 = "mc_forum_posts_reply_item"
            int r4 = r4.getLayoutId(r5)
            android.view.View r13 = r3.inflate(r4, r7)
            com.mobcent.base.android.ui.activity.adapter.holder.PostsReplyAdapterHolder r1 = new com.mobcent.base.android.ui.activity.adapter.holder.PostsReplyAdapterHolder
            r1.<init>()
            r11.initReplyListAdapterHolder(r13, r1)
            r13.setTag(r1)
            goto L_0x002c
        L_0x00b7:
            r3 = r9
            goto L_0x0050
        L_0x00b9:
            r3 = 19
            goto L_0x005a
        L_0x00bc:
            com.mobcent.ad.android.ui.widget.AdView r3 = r1.getAdView()
            r3.setVisibility(r8)
            com.mobcent.ad.android.ui.widget.AdView r3 = r1.getTopAdViewBox()
            r3.setVisibility(r6)
            if (r12 == r10) goto L_0x00ce
            if (r12 != r9) goto L_0x00f7
        L_0x00ce:
            com.mobcent.ad.android.ui.widget.AdView r3 = r1.getAdView()
            java.lang.String r4 = r11.TAG
            java.lang.Integer r5 = new java.lang.Integer
            android.content.Context r6 = r11.context
            android.content.res.Resources r6 = r6.getResources()
            com.mobcent.base.forum.android.util.MCResource r7 = r11.resource
            java.lang.String r8 = "mc_forum_posts_position_middle"
            int r7 = r7.getStringId(r8)
            java.lang.String r6 = r6.getString(r7)
            r5.<init>(r6)
            int r5 = r5.intValue()
            int r6 = r2.getFloor()
            r3.showAd(r4, r5, r6)
            goto L_0x0089
        L_0x00f7:
            r3 = 20
            if (r12 == r3) goto L_0x00ff
            r3 = 19
            if (r12 != r3) goto L_0x0089
        L_0x00ff:
            com.mobcent.ad.android.ui.widget.AdView r3 = r1.getAdView()
            java.lang.String r4 = r11.TAG
            java.lang.Integer r5 = new java.lang.Integer
            android.content.Context r6 = r11.context
            android.content.res.Resources r6 = r6.getResources()
            com.mobcent.base.forum.android.util.MCResource r7 = r11.resource
            java.lang.String r8 = "mc_forum_posts_position_bottom"
            int r7 = r7.getStringId(r8)
            java.lang.String r6 = r6.getString(r7)
            r5.<init>(r6)
            int r5 = r5.intValue()
            int r6 = r2.getFloor()
            r3.showAd(r4, r5, r6)
            goto L_0x0089
        L_0x0129:
            com.mobcent.ad.android.ui.widget.AdView r3 = r1.getTopAdViewBox()
            r3.free()
            com.mobcent.ad.android.ui.widget.AdView r3 = r1.getAdView()
            r3.free()
            com.mobcent.ad.android.ui.widget.AdView r3 = r1.getAdView()
            r3.setVisibility(r6)
            com.mobcent.ad.android.ui.widget.AdView r3 = r1.getTopAdViewBox()
            r3.setVisibility(r6)
            goto L_0x0089
        L_0x0147:
            r11.updateReplyNormalView(r13, r2, r1, r12)
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.base.android.ui.activity.adapter.PostsListAdapter.getReplyView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }

    private void initReplyListAdapterHolder(View convertView, PostsReplyAdapterHolder holder) {
        holder.setReplyUserImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_reply_user_img")));
        holder.setRoleImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_role_img")));
        holder.setUserRoleText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_role_text")));
        holder.setReplyUserText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_reply_user_text")));
        holder.setReplyTimeText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_reply_time_text")));
        holder.setReplyContentText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_reply_content_text")));
        holder.setReplyContentLayout((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_reply_content_layout")));
        holder.setReplyQuoteContentText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_reply_quote_content_text")));
        holder.setReplyShareBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_reply_share_btn")));
        holder.setReplyReplyBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_reply_reply_btn")));
        holder.setReplyMoreBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_reply_more_btn")));
        holder.setReplyDeleteBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_reply_delete_btn")));
        holder.setReplyMoreBtn((ImageButton) convertView.findViewById(this.resource.getViewId("mc_forum_reply_more_btn")));
        holder.setLevelBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_reply_level_box")));
        holder.setReplyLabText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_reply_lab_text")));
        holder.setLocationBox((LinearLayout) convertView.findViewById(this.resource.getViewId("mc_forum_posts_location_box")));
        holder.setLocationText((TextView) convertView.findViewById(this.resource.getViewId("mc_forum_reply_location_text")));
        holder.setReplyBox((RelativeLayout) convertView.findViewById(this.resource.getViewId("mc_forum_posts_reply_item_box")));
        holder.setAdView((AdView) convertView.findViewById(this.resource.getViewId("mc_ad_box")));
        holder.setTopAdViewBox((AdView) convertView.findViewById(this.resource.getViewId("mc_top_ad_box")));
    }

    private void updateDeletedReplyView(final ReplyModel replyMolde, PostsReplyAdapterHolder holder, int position) {
        holder.getLevelBox().removeAllViews();
        new MCLevelView(this.context, replyMolde.getLevel(), holder.getLevelBox(), 0);
        showUserRole(holder.getUserRoleText(), replyMolde.getRoleNum(), replyMolde.isFollow(), replyMolde.getReplyUserId());
        holder.getReplyUserText().setText(replyMolde.getUserNickName());
        holder.getReplyTimeText().setText(DateUtil.getFormatTimeByYear(replyMolde.getPostsDate()));
        holder.getReplyContentText().setVisibility(0);
        holder.getReplyContentText().setText(this.context.getResources().getString(this.resource.getStringId("mc_forum_topic_deleted")));
        holder.getReplyContentLayout().setVisibility(8);
        holder.getReplyQuoteContentText().setVisibility(8);
        holder.getReplyLabText().setText(MCStringBundleUtil.resolveString(this.resource.getStringId("mc_forum_posts_reply_lab"), new String[]{replyMolde.getFloor() + ""}, this.context));
        holder.getReplyReplyBtn().setVisibility(8);
        holder.getReplyShareBtn().setVisibility(8);
        holder.getReplyDeleteBtn().setVisibility(8);
        holder.getReplyDeleteBtn().setVisibility(8);
        holder.getReplyMoreBtn().setVisibility(8);
        holder.getReplyUserImg().setImageResource(this.resource.getDrawableId("mc_forum_head"));
        if (!StringUtil.isEmpty(replyMolde.getIcon())) {
            updateImageView(AsyncTaskLoaderImage.formatUrl(replyMolde.getIconUrl() + replyMolde.getIcon(), "100x100"), holder.getReplyUserImg());
        }
        if (!StringUtil.isEmpty(replyMolde.getLocation())) {
            holder.getLocationBox().setVisibility(0);
            holder.getLocationText().setText(replyMolde.getLocation());
        } else {
            holder.getLocationBox().setVisibility(8);
            holder.getLocationText().setText("");
        }
        holder.getReplyUserImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCForumHelper.gotoUserInfo((Activity) PostsListAdapter.this.context, PostsListAdapter.this.resource, replyMolde.getReplyUserId());
            }
        });
    }

    private void updateReplyNormalView(View view, final ReplyModel replyMolde, PostsReplyAdapterHolder holder, int position) {
        holder.getLevelBox().removeAllViews();
        new MCLevelView(this.context, replyMolde.getLevel(), holder.getLevelBox(), 0);
        if (this.topicType == 1) {
            this.topicHolder.getPollImg().setVisibility(0);
        }
        showUserRole(holder.getUserRoleText(), replyMolde.getRoleNum(), replyMolde.isFollow(), replyMolde.getReplyUserId());
        holder.getReplyUserText().setText(replyMolde.getUserNickName());
        holder.getReplyTimeText().setText(DateUtil.getFormatTimeByYear(replyMolde.getPostsDate()));
        List<TopicContentModel> replyContentList = replyMolde.getReplyContentList();
        if (replyContentList == null || replyContentList.size() <= 0) {
            holder.getReplyContentText().setVisibility(0);
            holder.getReplyContentLayout().setVisibility(8);
            holder.getReplyContentText().setText(replyMolde.getReplyContent());
        } else {
            holder.getReplyContentText().setVisibility(8);
            holder.getReplyContentLayout().setVisibility(0);
            updatePostsDetailView(replyMolde.getReplyContentList(), holder.getReplyContentLayout());
        }
        if (replyMolde.isQuote()) {
            holder.getReplyQuoteContentText().setVisibility(0);
            String s = replyMolde.getQuoteUserName() + ":";
            String quote = s + " " + replyMolde.getQuoteContent();
            holder.getReplyQuoteContentText().setText(quote, TextView.BufferType.SPANNABLE);
            MCFaceUtil.setStrToFace(holder.getReplyQuoteContentText(), quote, this.context);
            ((Spannable) holder.getReplyQuoteContentText().getText()).setSpan(new ForegroundColorSpan(this.resource.getColor("mc_forum_text_hight_color")), 0, s.length() - 1, 33);
        } else {
            holder.getReplyQuoteContentText().setVisibility(8);
        }
        holder.getReplyLabText().setText(MCStringBundleUtil.resolveString(this.resource.getStringId("mc_forum_posts_reply_lab"), new String[]{replyMolde.getFloor() + ""}, this.context));
        if (!StringUtil.isEmpty(replyMolde.getLocation())) {
            holder.getLocationBox().setVisibility(0);
            holder.getLocationText().setText(replyMolde.getLocation());
        } else {
            holder.getLocationBox().setVisibility(8);
            holder.getLocationText().setText("");
        }
        holder.getReplyReplyBtn().setVisibility(0);
        holder.getReplyReplyBtn().setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                HashMap<String, Serializable> param = new HashMap<>();
                param.put("boardId", Long.valueOf(PostsListAdapter.this.boardId));
                param.put("boardName", PostsListAdapter.this.boardName);
                param.put("topicId", Long.valueOf(PostsListAdapter.this.topicModel.getTopicId()));
                param.put("toReplyId", Long.valueOf(replyMolde.getReplyPostsId()));
                param.put(MCConstant.IS_QUOTE_TOPIC, true);
                ArrayList<UserInfoModel> userList = (ArrayList) ((PostsActivity) PostsListAdapter.this.context).getPostsUserList();
                param.put(MCConstant.POSTS_USER_LIST, userList);
                if (LoginInterceptor.doInterceptor(PostsListAdapter.this.context, ReplyTopicActivity.class, param)) {
                    Intent intent = new Intent(PostsListAdapter.this.context, ReplyTopicActivity.class);
                    intent.putExtra("boardId", PostsListAdapter.this.boardId);
                    intent.putExtra("boardName", PostsListAdapter.this.boardName);
                    intent.putExtra("topicId", PostsListAdapter.this.topicModel.getTopicId());
                    intent.putExtra("toReplyId", replyMolde.getReplyPostsId());
                    intent.putExtra(MCConstant.IS_QUOTE_TOPIC, true);
                    intent.putExtra(MCConstant.POSTS_USER_LIST, userList);
                    PostsListAdapter.this.context.startActivity(intent);
                }
            }
        });
        holder.getReplyShareBtn().setVisibility(0);
        holder.getReplyShareBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PostsListAdapter.this.shareEvent(PostsListAdapter.this.getShareContent(PostsListAdapter.this.topicModel, replyMolde), null, replyMolde, !StringUtil.isEmpty(SharedPreferencesDB.getInstance(PostsListAdapter.this.context).getWeiXinAppKey()), WXAPIFactory.createWXAPI(PostsListAdapter.this.context, SharedPreferencesDB.getInstance(PostsListAdapter.this.context).getWeiXinAppKey()));
            }
        });
        MCTouchUtil.createTouchDelegate(holder.getReplyReplyBtn(), 10);
        MCTouchUtil.createTouchDelegate(holder.getReplyShareBtn(), 10);
        MCTouchUtil.createTouchDelegate(holder.getReplyDeleteBtn(), 10);
        holder.getReplyUserImg().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PostsListAdapter.this.getUserFunctionDialog(replyMolde);
            }
        });
        holder.getReplyUserImg().setImageResource(this.resource.getDrawableId("mc_forum_head"));
        if (!StringUtil.isEmpty(replyMolde.getIcon())) {
            updateImageView(AsyncTaskLoaderImage.formatUrl(replyMolde.getIconUrl() + replyMolde.getIcon(), "100x100"), holder.getReplyUserImg());
        }
        holder.getReplyMoreBtn().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (LoginInterceptor.doInterceptorByDialog(PostsListAdapter.this.context, PostsListAdapter.this.resource, null, null)) {
                    new MCTopicManageDialog(PostsListAdapter.this.context, PostsListAdapter.this.boardId, PostsListAdapter.this.boardName, null, replyMolde, 2, null, true).show();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void getUserFunctionDialog(TopicModel topicModel2) {
        List<String> f = new ArrayList<>();
        final String addFriend = this.resource.getString("mc_forum_add_friend");
        final String sendMsg = this.resource.getString("mc_forum_send_msg");
        final String visitHome = this.resource.getString("mc_forum_visit_user_home");
        f.add(addFriend);
        f.add(sendMsg);
        f.add(visitHome);
        if (topicModel2.isFollow()) {
            f.remove(addFriend);
        }
        if (topicModel2.getUserId() == this.currentUserId) {
            f.remove(sendMsg);
            f.remove(addFriend);
        }
        if (isShowAdmin(topicModel2.getRoleNum())) {
            if (!topicModel2.getBannedUser()) {
                f.add(this.resource.getString("mc_forum_banned"));
            } else {
                f.add(this.resource.getString("mc_forum_cancel_banned"));
            }
            if (!topicModel2.getShieldedUser()) {
                f.add(this.resource.getString("mc_forum_shielded"));
            } else {
                f.add(this.resource.getString("mc_forum_cancel_shielded"));
            }
            if (this.userService.currentUserIsAdmin()) {
                f.add(this.resource.getString("mc_forum_moderator_setting"));
            }
        }
        final String[] function = MCForumReverseList.convertListToArray(f);
        final TopicModel topicModel3 = topicModel2;
        new AlertDialog.Builder(this.context).setTitle(this.resource.getString("mc_forum_topic_function")).setItems(function, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (function[which].equals(addFriend)) {
                    if (LoginInterceptor.doInterceptorByDialog(PostsListAdapter.this.context, PostsListAdapter.this.resource, null, new HashMap<>())) {
                        FollowUserAsyncTask followUserAsyncTask = new FollowUserAsyncTask(PostsListAdapter.this.context.getApplicationContext(), PostsListAdapter.this.resource, PostsListAdapter.this.currentUserId, topicModel3.getUserId(), topicModel3.getUserNickName());
                        followUserAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                            public void executeSuccess() {
                                topicModel3.setFollow(true);
                            }

                            public void executeFail() {
                            }
                        });
                        followUserAsyncTask.execute(new Void[0]);
                    }
                } else if (function[which].equals(sendMsg)) {
                    HashMap<String, Serializable> param = new HashMap<>();
                    param.put(MCConstant.CHAT_USER_ID, Long.valueOf(topicModel3.getUserId()));
                    param.put(MCConstant.CHAT_USER_NICKNAME, topicModel3.getUserNickName());
                    if (LoginInterceptor.doInterceptorByDialog(PostsListAdapter.this.context, PostsListAdapter.this.resource, MsgChatRoomFragmentActivity.class, param)) {
                        Intent intent = new Intent(PostsListAdapter.this.context, MsgChatRoomFragmentActivity.class);
                        intent.putExtra(MCConstant.CHAT_USER_ID, topicModel3.getUserId());
                        intent.putExtra(MCConstant.CHAT_USER_NICKNAME, topicModel3.getUserNickName());
                        PostsListAdapter.this.context.startActivity(intent);
                    }
                } else if (function[which].equals(visitHome)) {
                    MCForumHelper.gotoUserInfo((Activity) PostsListAdapter.this.context, PostsListAdapter.this.resource, topicModel3.getUserId());
                } else if (function[which].equals(PostsListAdapter.this.resource.getString("mc_forum_banned"))) {
                    PostsListAdapter.this.setUserBanned(topicModel3);
                } else if (function[which].equals(PostsListAdapter.this.resource.getString("mc_forum_cancel_banned"))) {
                    PostsListAdapter.this.cancelUserBanned(topicModel3);
                } else if (function[which].equals(PostsListAdapter.this.resource.getString("mc_forum_shielded"))) {
                    PostsListAdapter.this.setUserShielded(topicModel3);
                } else if (function[which].equals(PostsListAdapter.this.resource.getString("mc_forum_cancel_shielded"))) {
                    PostsListAdapter.this.cancelUserShielded(topicModel3);
                } else if (function[which].equals(PostsListAdapter.this.resource.getString("mc_forum_moderator_setting"))) {
                    PostsListAdapter.this.setModerator(topicModel3);
                }
            }
        }).show();
    }

    private boolean isShowAdmin(int roleNum) {
        if (this.userService.currentUserIsAdmin()) {
            if (roleNum == 8 || roleNum == 16) {
                return false;
            }
            return true;
        } else if (!this.moderatorService.BoardPermission(this.boardId, this.userService.getLoginUserId())) {
            return false;
        } else {
            if (roleNum == 8 || roleNum == 4 || roleNum == 16) {
                return false;
            }
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void getUserFunctionDialog(ReplyModel replyModel) {
        List<String> f = new ArrayList<>();
        final String addFriend = this.resource.getString("mc_forum_add_friend");
        final String sendMsg = this.resource.getString("mc_forum_send_msg");
        final String visitHome = this.resource.getString("mc_forum_visit_user_home");
        f.add(addFriend);
        f.add(sendMsg);
        f.add(visitHome);
        if (replyModel.isFollow()) {
            f.remove(addFriend);
        }
        if (replyModel.getReplyUserId() == this.currentUserId) {
            f.remove(sendMsg);
            f.remove(addFriend);
        }
        if (isShowAdmin(replyModel.getRoleNum())) {
            if (!replyModel.getBannedUser()) {
                f.add(this.resource.getString("mc_forum_banned"));
            } else {
                f.add(this.resource.getString("mc_forum_cancel_banned"));
            }
            if (!replyModel.getShieldedUser()) {
                f.add(this.resource.getString("mc_forum_shielded"));
            } else {
                f.add(this.resource.getString("mc_forum_cancel_shielded"));
            }
            if (this.userService.currentUserIsAdmin()) {
                f.add(this.resource.getString("mc_forum_moderator_setting"));
            }
        }
        final String[] function = MCForumReverseList.convertListToArray(f);
        final ReplyModel replyModel2 = replyModel;
        new AlertDialog.Builder(this.context).setTitle(this.resource.getString("mc_forum_topic_function")).setItems(function, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (function[which].equals(addFriend)) {
                    if (LoginInterceptor.doInterceptorByDialog(PostsListAdapter.this.context, PostsListAdapter.this.resource, null, new HashMap<>())) {
                        FollowUserAsyncTask followUserAsyncTask = new FollowUserAsyncTask(PostsListAdapter.this.context.getApplicationContext(), PostsListAdapter.this.resource, PostsListAdapter.this.currentUserId, replyModel2.getReplyUserId(), replyModel2.getUserNickName());
                        followUserAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
                            public void executeSuccess() {
                                replyModel2.setFollow(true);
                            }

                            public void executeFail() {
                            }
                        });
                        followUserAsyncTask.execute(new Void[0]);
                    }
                } else if (function[which].equals(sendMsg)) {
                    HashMap<String, Serializable> param = new HashMap<>();
                    param.put(MCConstant.CHAT_USER_ID, Long.valueOf(replyModel2.getReplyUserId()));
                    param.put(MCConstant.CHAT_USER_NICKNAME, replyModel2.getUserNickName());
                    if (LoginInterceptor.doInterceptorByDialog(PostsListAdapter.this.context, PostsListAdapter.this.resource, MsgChatRoomFragmentActivity.class, param)) {
                        Intent intent = new Intent(PostsListAdapter.this.context, MsgChatRoomFragmentActivity.class);
                        intent.putExtra(MCConstant.CHAT_USER_ID, replyModel2.getReplyUserId());
                        intent.putExtra(MCConstant.CHAT_USER_NICKNAME, replyModel2.getUserNickName());
                        PostsListAdapter.this.context.startActivity(intent);
                    }
                } else if (function[which].equals(visitHome)) {
                    MCForumHelper.gotoUserInfo((Activity) PostsListAdapter.this.context, PostsListAdapter.this.resource, replyModel2.getReplyUserId());
                } else if (function[which].equals(PostsListAdapter.this.resource.getString("mc_forum_banned"))) {
                    PostsListAdapter.this.setUserBanned(replyModel2);
                } else if (function[which].equals(PostsListAdapter.this.resource.getString("mc_forum_cancel_banned"))) {
                    PostsListAdapter.this.cancelUserBanned(replyModel2);
                } else if (function[which].equals(PostsListAdapter.this.resource.getString("mc_forum_shielded"))) {
                    PostsListAdapter.this.setUserShielded(replyModel2);
                } else if (function[which].equals(PostsListAdapter.this.resource.getString("mc_forum_cancel_shielded"))) {
                    PostsListAdapter.this.cancelUserShielded(replyModel2);
                } else if (function[which].equals(PostsListAdapter.this.resource.getString("mc_forum_moderator_setting"))) {
                    PostsListAdapter.this.setModerator(replyModel2);
                }
            }
        }).show();
    }

    private LinearLayout updatePostsDetailView(List<TopicContentModel> topicContentList2, LinearLayout topicContentLayout) {
        String imageUrl;
        if (topicContentList2 != null && topicContentList2.size() > 0) {
            topicContentLayout.setVisibility(0);
            topicContentLayout.removeAllViews();
            int j = topicContentList2.size();
            for (int i = 0; i < j; i++) {
                TopicContentModel topicContent = topicContentList2.get(i);
                View view = null;
                if (topicContent.getType() == 0) {
                    view = this.inflater.inflate(this.resource.getLayoutId("mc_forum_posts_topic_text_item"), (ViewGroup) null);
                    TextView topicInfoText = (TextView) view.findViewById(this.resource.getViewId("mc_forum_topic_info_text"));
                    topicInfoText.setText(topicContent.getInfor());
                    MCFaceUtil.setStrToFace(topicInfoText, topicContent.getInfor(), this.context);
                } else if (topicContent.getType() == 1) {
                    view = this.inflater.inflate(this.resource.getLayoutId("mc_forum_posts_topic_img_item"), (ViewGroup) null);
                    ImageView topicInfoImg = (ImageView) view.findViewById(this.resource.getViewId("mc_forum_topic_info_img"));
                    if (topicContent.getBaseUrl() == null || topicContent.getBaseUrl().trim().equals("") || topicContent.getBaseUrl().trim().equals("null")) {
                        imageUrl = topicContent.getInfor();
                    } else {
                        imageUrl = topicContent.getBaseUrl() + topicContent.getInfor();
                    }
                    setAllImageOnClickAction(topicInfoImg, imageUrl);
                    topicInfoImg.setImageResource(this.resource.getDrawableId("mc_forum_x_img3"));
                    if (!StringUtil.isEmpty(topicContent.getInfor())) {
                        updateImageView(AsyncTaskLoaderImage.formatUrl(imageUrl, "320x480"), topicInfoImg);
                    } else {
                        topicInfoImg.setImageResource(this.resource.getDrawableId("mc_forum_x_img3"));
                    }
                } else if (topicContent.getType() == 5) {
                    view = getSoundView(topicContent.getSoundModel());
                }
                if (view != null) {
                    topicContentLayout.addView(view);
                }
            }
            topicContentLayout.setVisibility(0);
        }
        return topicContentLayout;
    }

    private void updatePollLayoutView(PollTopicModel pollModel2, PostsTopicAdapterHolder topicHolder2) {
        if (pollModel2 != null) {
            this.pollItemList = pollModel2.getPooList();
            if (this.pollItemList != null && !this.pollItemList.isEmpty()) {
                topicHolder2.getPollImg().setVisibility(0);
                this.pollType = pollModel2.getPoolType();
                if (this.pollType == 0) {
                    this.pollType = this.pollItemList.size();
                }
                updatePollSelectView(this.pollItemList, topicHolder2, pollModel2.getPollId(), this.pollType);
                if (this.userService.isLogin()) {
                    long deadTime = this.postsDBUtil.getposts(this.topicModel.getTopicId(), this.userService.getLoginUserId());
                    if (deadTime - System.currentTimeMillis() > 0) {
                        topicHolder2.getPollSubmitBtn().setVisibility(8);
                        updatePollResultView(this.pollItemList, topicHolder2);
                    } else if (deadTime == 0) {
                        if (pollModel2.getPollStatus() == 1) {
                            topicHolder2.getPollSubmitBtn().setVisibility(8);
                            updatePollResultView(this.pollItemList, topicHolder2);
                        } else if (pollModel2.getPollStatus() == 2) {
                            topicHolder2.getPollSubmitBtn().setVisibility(0);
                        } else if (pollModel2.getPollStatus() == 3) {
                            topicHolder2.getPollSubmitBtn().setVisibility(0);
                            updatePollResultView(this.pollItemList, topicHolder2);
                        }
                    } else if (deadTime - System.currentTimeMillis() <= 0) {
                        topicHolder2.getPollSubmitBtn().setVisibility(0);
                        updatePollResultView(this.pollItemList, topicHolder2);
                    }
                } else {
                    topicHolder2.getPollSubmitBtn().setVisibility(0);
                }
            }
        }
    }

    private void updatePollSelectView(List<PollItemModel> pollItemList2, PostsTopicAdapterHolder topicHolder2, int[] pollId, int poolType) {
        topicHolder2.getPollLayout().setVisibility(0);
        LinearLayout pollSelectLayout = topicHolder2.getPollSelectLayout();
        topicHolder2.getPollTitleText().setText(MCStringBundleUtil.resolveString(this.resource.getStringId("mc_froum_radio_poll"), poolType + "", this.context));
        pollSelectLayout.removeAllViews();
        this.selectedPollViewList.clear();
        this.pollViewList.clear();
        int j = pollItemList2.size();
        for (int i = 0; i < j; i++) {
            PollItemModel pollItem = pollItemList2.get(i);
            pollItem.setNum(i + 1);
            MCPollSelectBar pollSelectBar = new MCPollSelectBar(this.inflater, this.resource, pollItem, new MCPollSelectBar.SelectPollDelegate() {
                public void onItemSelect(View view, MCPollSelectBar selectBar, boolean select) {
                    if (select) {
                        if (!PostsListAdapter.this.selectedPollViewList.contains(selectBar)) {
                            PostsListAdapter.this.selectedPollViewList.add(selectBar);
                        }
                    } else if (PostsListAdapter.this.selectedPollViewList.contains(selectBar)) {
                        PostsListAdapter.this.selectedPollViewList.remove(selectBar);
                    }
                }
            }, this.context);
            this.pollViewList.add(pollSelectBar);
            if (pollId != null && pollId.length > 0) {
                int k = 0;
                while (true) {
                    if (k >= pollId.length) {
                        break;
                    } else if (pollId[k] == pollItem.getPollItemId()) {
                        pollSelectBar.setSelect(false);
                        if (!this.selectedPollViewList.contains(pollSelectBar)) {
                            this.selectedPollViewList.add(pollSelectBar);
                        }
                    } else {
                        k++;
                    }
                }
            }
            pollSelectLayout.addView(pollSelectBar.getView());
        }
    }

    /* access modifiers changed from: private */
    public void updatePollResultView(List<PollItemModel> pollItemList2, PostsTopicAdapterHolder topicHolder2) {
        if (pollItemList2 != null && pollItemList2.size() > 0) {
            int j = pollItemList2.size();
            for (int i = 0; i < j; i++) {
                this.pollViewList.get(i).updateResultView(pollItemList2.get(i));
            }
        }
    }

    private void updateTopicRepostView(RepostTopicModel repostTopicModel2, PostsTopicAdapterHolder topicHolder2) {
        if (repostTopicModel2 != null) {
            topicHolder2.getRepostBox().setVisibility(0);
            topicHolder2.getRepostUserName().setText(this.context.getResources().getString(this.resource.getStringId("mc_forum_posts_repost")) + "//@" + repostTopicModel2.getNickName() + ":");
            topicHolder2.getRepostTime().setText(DateUtil.getFormatTimeByYear(repostTopicModel2.getRepostTime()));
            topicHolder2.getRepostBoardName().setText(repostTopicModel2.getBoardName());
            updatePostsDetailView(repostTopicModel2.getRepostContentList(), topicHolder2.getRepostContentLayout());
            return;
        }
        topicHolder2.getRepostBox().setVisibility(8);
    }

    public List<TopicContentModel> getTopicContentList() {
        return this.topicContentList;
    }

    public ArrayList<RichImageModel> getAllImageUrl(List<PostsModel> postsList2) {
        ReplyModel reply;
        List<TopicContentModel> replyContentList;
        List<TopicContentModel> repostContentList;
        String imageUrl;
        if (postsList2 == null || postsList2.size() == 0) {
            return null;
        }
        ArrayList<RichImageModel> richImageModelList2 = new ArrayList<>();
        List<TopicContentModel> topicContentList2 = getTopicContentList();
        if (topicContentList2 != null && topicContentList2.size() > 0) {
            for (TopicContentModel topicContent : topicContentList2) {
                if (topicContent.getType() == 1) {
                    RichImageModel model = new RichImageModel();
                    model.setImageUrl(topicContent.getBaseUrl() + topicContent.getInfor());
                    if (this.topicModel != null) {
                        model.setImageDesc(getShareContent(this.topicModel, null));
                    } else {
                        model.setImageDesc(getShareContent(postsList2.get(0).getTopic(), null));
                    }
                    richImageModelList2.add(model);
                }
            }
        }
        if (!(this.repostTopicModel == null || (repostContentList = this.repostTopicModel.getRepostContentList()) == null || repostContentList.size() <= 0)) {
            for (TopicContentModel topicContent2 : repostContentList) {
                if (topicContent2.getType() == 1) {
                    RichImageModel model2 = new RichImageModel();
                    if (topicContent2.getBaseUrl() == null || topicContent2.getBaseUrl().trim().equals("") || topicContent2.getBaseUrl().trim().equals("null")) {
                        imageUrl = topicContent2.getInfor();
                    } else {
                        imageUrl = topicContent2.getBaseUrl() + topicContent2.getInfor();
                    }
                    model2.setImageUrl(imageUrl);
                    if (this.topicModel != null) {
                        model2.setImageDesc(getShareContent(this.topicModel, null));
                    } else {
                        model2.setImageDesc(getShareContent(postsList2.get(0).getTopic(), null));
                    }
                    richImageModelList2.add(model2);
                }
            }
        }
        for (int i = 0; i < postsList2.size(); i++) {
            PostsModel posts = postsList2.get(i);
            if (posts.getPostType() == 0 && (replyContentList = (reply = posts.getReply()).getReplyContentList()) != null && replyContentList.size() > 0) {
                for (TopicContentModel replyContent : replyContentList) {
                    if (replyContent.getType() == 1) {
                        RichImageModel model3 = new RichImageModel();
                        model3.setImageUrl(replyContent.getBaseUrl() + replyContent.getInfor());
                        if (this.topicModel != null) {
                            model3.setImageDesc(getShareContent(this.topicModel, reply));
                        } else {
                            model3.setImageDesc(getShareContent(postsList2.get(0).getTopic(), reply));
                        }
                        richImageModelList2.add(model3);
                    }
                }
            }
        }
        return richImageModelList2;
    }

    public List<String> getAllUserIconURL(List<PostsModel> postsList2) {
        List<String> userIconUrlList = new ArrayList<>();
        if (postsList2 != null && !postsList2.isEmpty()) {
            for (PostsModel postsModel : postsList2) {
                TopicModel topic = postsModel.getTopic();
                if (topic != null && !StringUtil.isEmpty(topic.getIconUrl())) {
                    userIconUrlList.add(topic.getIconUrl() + topic.getIcon());
                }
                ReplyModel reply = postsModel.getReply();
                if (reply != null && !StringUtil.isEmpty(reply.getIconUrl())) {
                    userIconUrlList.add(reply.getIconUrl() + reply.getIcon());
                }
            }
        }
        return userIconUrlList;
    }

    private void setAllImageOnClickAction(ImageView imageView, final String imageUrl) {
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(PostsListAdapter.this.context, ImageViewerFragmentActivity.class);
                intent.putExtra(MCConstant.RICH_IMAGE_LIST, PostsListAdapter.this.richImageModelList);
                intent.putExtra(MCConstant.IMAGE_URL, imageUrl);
                PostsListAdapter.this.context.startActivity(intent);
            }
        });
    }

    public void warnMessageByStr(String str) {
        Toast.makeText(this.context, str, 0).show();
    }

    /* access modifiers changed from: protected */
    public void warnMessageById(String warnMessId) {
        Toast.makeText(this.context, this.resource.getStringId(warnMessId), 0).show();
    }

    private class UserPollTask extends AsyncTask<String, Void, List<PollItemModel>> {
        private UserPollTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<PollItemModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            PostsListAdapter.this.topicHolder.getPollSubmitBtn().setEnabled(false);
            super.onPreExecute();
            ProgressDialog unused = PostsListAdapter.this.loadDialog = ProgressDialog.show(PostsListAdapter.this.context, PostsListAdapter.this.context.getResources().getString(PostsListAdapter.this.resource.getStringId("mc_forum_please_wait")), PostsListAdapter.this.context.getResources().getString(PostsListAdapter.this.resource.getStringId("mc_forum_loading_poll")), true);
        }

        /* access modifiers changed from: protected */
        public List<PollItemModel> doInBackground(String... params) {
            return new PostsServiceImpl(PostsListAdapter.this.context).getUserPolls(Long.parseLong(params[0]), Long.parseLong(params[1]), params[2]);
        }

        /* Debug info: failed to restart local var, previous not found, register: 9 */
        /* access modifiers changed from: protected */
        public void onPostExecute(List<PollItemModel> result) {
            super.onPostExecute((Object) result);
            if (PostsListAdapter.this.loadDialog.isShowing()) {
                PostsListAdapter.this.loadDialog.dismiss();
            }
            if (result != null && result.size() > 0) {
                if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                    PostsListAdapter.this.topicHolder.getPollSubmitBtn().setEnabled(true);
                    Toast.makeText(PostsListAdapter.this.context, MCForumErrorUtil.convertErrorCode(PostsListAdapter.this.context, result.get(0).getErrorCode()), 0).show();
                    return;
                }
                PostsListAdapter.this.postsDBUtil.updatePosts(PostsListAdapter.this.userService.getLoginUserId(), PostsListAdapter.this.topicModel.getTopicId(), System.currentTimeMillis() + 86400000, System.currentTimeMillis());
                PostsListAdapter.this.topicHolder.getPollSubmitBtn().setVisibility(8);
                List unused = PostsListAdapter.this.pollItemList = result;
                PostsListAdapter.this.pollModel.setPooList(PostsListAdapter.this.pollItemList);
                PostsListAdapter.this.updatePollResultView(PostsListAdapter.this.pollItemList, PostsListAdapter.this.topicHolder);
            }
        }
    }

    private void updateImageView(String imgUrl, final ImageView imageView) {
        if (SharedPreferencesDB.getInstance(this.context).getPicModeFlag()) {
            this.asyncTaskLoaderImage.loadAsync(imgUrl, new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    PostsListAdapter.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (image != null) {
                                imageView.setImageBitmap(image);
                            }
                        }
                    });
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void shareEvent(String shareContent, TopicModel topicModel2, ReplyModel replyModel, boolean ExistWeiXinAppKey, IWXAPI api) {
        this.longPicAsynTask = new LongPicAsynTask(this.context);
        final TopicModel topicModel3 = topicModel2;
        final ReplyModel replyModel2 = replyModel;
        final boolean z = ExistWeiXinAppKey;
        final String str = shareContent;
        final IWXAPI iwxapi = api;
        this.longPicAsynTask.setLongTaskDelegate(new LongTaskDelegate() {
            public void executeSuccess(String url) {
                PostsListAdapter.this.myDialog.cancel();
                if (topicModel3 != null) {
                    topicModel3.setPic(url);
                } else if (replyModel2 != null) {
                    replyModel2.setPic(url);
                }
                if (z) {
                    MCForumLaunchShareHelper.shareWay(true, str, "", url, "", "", PostsListAdapter.this.context, iwxapi);
                    return;
                }
                MCForumLaunchShareHelper.shareContentWithImageUrl(str, url, "", "", PostsListAdapter.this.context);
            }

            public void executeFail() {
                PostsListAdapter.this.myDialog.cancel();
                if (z) {
                    MCForumLaunchShareHelper.shareWay(false, str, "", "", "", "", PostsListAdapter.this.context, iwxapi);
                } else {
                    MCForumLaunchShareHelper.shareContent(str, "", "", PostsListAdapter.this.context);
                }
            }
        });
        if (topicModel2 != null) {
            if (topicModel2.isHasImg()) {
                if (StringUtil.isEmpty(topicModel2.getPic())) {
                    getLongPicUrl(topicModel2.getTitle(), topicModel2.getContentAll(), AsyncTaskLoaderImage.formatUrl(topicModel2.getBaseUrl(), MCForumConstant.RESOLUTION_ORIGINAL));
                } else if (ExistWeiXinAppKey) {
                    MCForumLaunchShareHelper.shareWay(true, shareContent, "", topicModel2.getPic(), "", "", this.context, api);
                } else {
                    MCForumLaunchShareHelper.shareContentWithImageUrl(shareContent, topicModel2.getTopicId() + "-" + 1, "", "", this.context);
                }
            } else if (ExistWeiXinAppKey) {
                MCForumLaunchShareHelper.shareWay(false, shareContent, "", "", "", "", this.context, api);
            } else {
                MCForumLaunchShareHelper.shareContent(shareContent, "", "", this.context);
            }
        } else if (replyModel == null) {
        } else {
            if (replyModel.isHasImg()) {
                if (StringUtil.isEmpty(replyModel.getPic())) {
                    getLongPicUrl(replyModel.getTitle(), replyModel.getReplyContent(), AsyncTaskLoaderImage.formatUrl(replyModel.getBaseUrl(), MCForumConstant.RESOLUTION_ORIGINAL));
                } else if (ExistWeiXinAppKey) {
                    MCForumLaunchShareHelper.shareWay(true, shareContent, "", replyModel.getPic(), "", "", this.context, api);
                } else {
                    MCForumLaunchShareHelper.shareContentWithImageUrl(shareContent, replyModel.getReplyPostsId() + "-" + 0, "", "", this.context);
                }
            } else if (ExistWeiXinAppKey) {
                MCForumLaunchShareHelper.shareWay(false, shareContent, "", "", "", "", this.context, api);
            } else {
                MCForumLaunchShareHelper.shareContent(shareContent, "", "", this.context);
            }
        }
    }

    private void getLongPicUrl(String title, String content, String baseUrl) {
        this.myDialog = new ProgressDialog(this.context);
        this.myDialog.setProgressStyle(0);
        this.myDialog.setTitle(this.context.getResources().getString(this.resource.getStringId("mc_forum_dialog_tip")));
        this.myDialog.setMessage(this.context.getResources().getString(this.resource.getStringId("mc_forum_warn_load")));
        this.myDialog.setIndeterminate(false);
        this.myDialog.setCancelable(true);
        this.myDialog.show();
        this.longPicAsynTask.execute(title, content, baseUrl);
    }

    /* access modifiers changed from: private */
    public String getShareContent(TopicModel topic, ReplyModel reply) {
        String shareContent;
        try {
            String shareContent2 = BaseReturnCodeConstant.ERROR_CODE + topic.getTitle() + BaseReturnCodeConstant.ERROR_CODE + "\n";
            if (reply == null) {
                if (topic.getContent().length() <= 70) {
                    shareContent = shareContent2 + topic.getContent();
                } else {
                    shareContent = shareContent2 + topic.getContent().substring(0, 70);
                }
            } else if (reply.getShortContent().length() <= 70) {
                shareContent = shareContent2 + reply.getShortContent();
            } else {
                shareContent = shareContent2 + reply.getShortContent().substring(0, 70);
            }
            return shareContent;
        } catch (Exception e) {
            return "";
        }
    }

    public void destroy() {
        if (!(this.pollTask == null || this.pollTask.getStatus() == AsyncTask.Status.FINISHED)) {
            this.pollTask.cancel(true);
        }
        if (this.cancelBannedShieldedAsyncTask != null) {
            this.cancelBannedShieldedAsyncTask.cancel(true);
        }
        if (this.closeTopicAsyncTask != null) {
            this.closeTopicAsyncTask.cancel(true);
        }
        if (this.essenceTopicAsyncTask != null) {
            this.essenceTopicAsyncTask.cancel(true);
        }
        if (this.delTopicAsyncTask != null) {
            this.delTopicAsyncTask.cancel(true);
        }
        if (this.topTopicAsyncTask != null) {
            this.topTopicAsyncTask.cancel(true);
        }
        if (this.longPicAsynTask != null) {
            this.longPicAsynTask.cancel(true);
        }
    }

    private void showUserRole(TextView textView, int roleNum, boolean follow, long userId) {
        String text = "";
        if (roleNum == 4) {
            text = this.context.getResources().getString(this.resource.getStringId("mc_forum_role_moderator"));
        } else if (roleNum == 8) {
            text = this.context.getResources().getString(this.resource.getStringId("mc_forum_role_administrator"));
        } else if (roleNum == 16) {
            text = this.context.getResources().getString(this.resource.getStringId("mc_forum_role_super_administrator"));
        }
        if (follow && userId != this.currentUserId) {
            if (!StringUtil.isEmpty(text)) {
                text = text + "  " + this.context.getResources().getString(this.resource.getStringId("mc_forum_friend"));
            } else {
                text = text + this.context.getResources().getString(this.resource.getStringId("mc_forum_friend"));
            }
        }
        textView.setText(text);
    }

    public RepostTopicModel getRepostTopicModel() {
        return this.repostTopicModel;
    }

    /* access modifiers changed from: protected */
    public void setUserBanned(TopicModel topicModel2) {
        Intent intent = new Intent(this.context, UserBannedActivity.class);
        intent.putExtra(MCConstant.BANNED_TYPE, 4);
        intent.putExtra("userId", this.currentUserId);
        intent.putExtra("boardId", this.boardId);
        intent.putExtra(MCConstant.BANNED_USER_ID, topicModel2.getUserId());
        intent.putExtra(MCConstant.TOPICMODEL, topicModel2);
        this.context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void setUserBanned(ReplyModel replyModel) {
        Intent intent = new Intent(this.context, UserBannedActivity.class);
        intent.putExtra(MCConstant.BANNED_TYPE, 4);
        intent.putExtra("userId", this.currentUserId);
        intent.putExtra("boardId", this.boardId);
        intent.putExtra(MCConstant.BANNED_USER_ID, replyModel.getReplyUserId());
        intent.putExtra(MCConstant.REPLYMODEL, replyModel);
        this.context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void cancelUserBanned(final TopicModel topicModel2) {
        this.cancelBannedShieldedAsyncTask = new CancelBannedShieldedAsyncTask(this.context, this.resource, 4, topicModel2.getUserId(), (int) this.boardId);
        this.cancelBannedShieldedAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
            public void executeSuccess() {
                topicModel2.setBannedUser(false);
            }

            public void executeFail() {
                topicModel2.setBannedUser(true);
            }
        });
        this.cancelBannedShieldedAsyncTask.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void cancelUserBanned(final ReplyModel replyModel) {
        this.cancelBannedShieldedAsyncTask = new CancelBannedShieldedAsyncTask(this.context, this.resource, 4, replyModel.getReplyUserId(), (int) this.boardId);
        this.cancelBannedShieldedAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
            public void executeSuccess() {
                replyModel.setBannedUser(false);
            }

            public void executeFail() {
                replyModel.setBannedUser(true);
            }
        });
        this.cancelBannedShieldedAsyncTask.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void setUserShielded(TopicModel topicModel2) {
        Intent intent = new Intent(this.context, UserShieldedActivity.class);
        intent.putExtra(MCConstant.SHIELDED_TYPE, 1);
        intent.putExtra("userId", this.currentUserId);
        intent.putExtra("boardId", this.boardId);
        intent.putExtra("shieldedUserId", topicModel2.getUserId());
        intent.putExtra(MCConstant.TOPICMODEL, topicModel2);
        this.context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void setUserShielded(ReplyModel replyModel) {
        Intent intent = new Intent(this.context, UserShieldedActivity.class);
        intent.putExtra(MCConstant.SHIELDED_TYPE, 1);
        intent.putExtra("userId", this.currentUserId);
        intent.putExtra("boardId", this.boardId);
        intent.putExtra("shieldedUserId", replyModel.getReplyUserId());
        intent.putExtra(MCConstant.REPLYMODEL, replyModel);
        this.context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void cancelUserShielded(final TopicModel topicModel2) {
        CancelBannedShieldedAsyncTask cancelShieldedAsyncTask = new CancelBannedShieldedAsyncTask(this.context, this.resource, 1, topicModel2.getUserId(), (int) this.boardId);
        cancelShieldedAsyncTask.setTaskExecuteDelegate(new TaskExecuteDelegate() {
            public void executeSuccess() {
                topicModel2.setShieldedUser(false);
            }

            public void executeFail() {
                topicModel2.setShieldedUser(true);
            }
        });
        cancelShieldedAsyncTask.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void cancelUserShielded(final ReplyModel replyModel) {
        CancelBannedShieldedAsyncTask cancelBannedShieldedAsyncTask2 = new CancelBannedShieldedAsyncTask(this.context, this.resource, 1, replyModel.getReplyUserId(), (int) this.boardId);
        cancelBannedShieldedAsyncTask2.setTaskExecuteDelegate(new TaskExecuteDelegate() {
            public void executeSuccess() {
                replyModel.setShieldedUser(false);
            }

            public void executeFail() {
                replyModel.setShieldedUser(true);
            }
        });
        cancelBannedShieldedAsyncTask2.execute(new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void setModerator(TopicModel topicModel2) {
        Intent intent = new Intent(this.context, ModeratorActivity.class);
        intent.putExtra("userId", topicModel2.getUserId());
        intent.putExtra("topicId", topicModel2.getTopicId());
        this.context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void setModerator(ReplyModel replyModel) {
        Intent intent = new Intent(this.context, ModeratorActivity.class);
        intent.putExtra("userId", replyModel.getReplyUserId());
        intent.putExtra("replyPostsId", replyModel.getReplyPostsId());
        this.context.startActivity(intent);
    }

    public int getClose() {
        return this.close;
    }

    public void setClose(int close2) {
        this.close = close2;
    }

    public int getTop() {
        return this.top;
    }

    public void setTop(int top2) {
        this.top = top2;
    }

    public int getEssence() {
        return this.essence;
    }

    public void setEssence(int essence2) {
        this.essence = essence2;
    }

    private boolean isContainFloorAuther() {
        if (this.postsList.isEmpty() || this.postsList.get(0).getPostType() != 1) {
            return false;
        }
        return true;
    }
}
