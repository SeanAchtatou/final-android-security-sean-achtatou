package com.mobcent.base.android.ui.widget.animmenu;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import java.util.List;

public class BaseAnimButtonMenu extends RelativeLayout {
    protected int DELAYED_TIME = 26;
    protected int END_LENGTH;
    protected int FAR_LENGTH;
    protected int MENU_BAR_HEIGHT = 0;
    protected int MENU_ITEM_HEIGHT;
    protected int MENU_MAIN_HEIGHT;
    protected int NEAR_LENGTH;
    protected List<AnimButtonItem> animButtonItemList = new ArrayList();
    protected AnimButtonMenuListener animButtonMenuListener;
    protected AnimButtonItem animMainButton;
    protected boolean isExpand;
    protected Handler mHandler = new Handler() {
        /* Debug info: failed to restart local var, previous not found, register: 12 */
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int what = msg.what;
            if (BaseAnimButtonMenu.this.animButtonItemList.size() > what) {
                AnimButtonItem eachAnimItem = BaseAnimButtonMenu.this.animButtonItemList.get(what);
                if (BaseAnimButtonMenu.this.isExpand) {
                    eachAnimItem.startAnimation(BaseAnimButtonMenu.this.getAnimationByOtherItemExpend(((Integer) eachAnimItem.getTag()).intValue()));
                    return;
                }
                RotateAnimation animation1 = new RotateAnimation(0.0f, 1080.0f, 1, 0.5f, 1, 0.5f);
                animation1.setDuration(80);
                Animation animation2 = BaseAnimButtonMenu.this.getAnimationByOtherItemClosed(((Integer) eachAnimItem.getTag()).intValue());
                AnimationSet animationSet = new AnimationSet(BaseAnimButtonMenu.this.getContext(), null);
                animationSet.addAnimation(animation1);
                animationSet.addAnimation(animation2);
                animationSet.setDuration(260);
                eachAnimItem.startAnimation(animationSet);
            }
        }
    };
    protected RelativeLayout.LayoutParams menuItemLp;
    protected RelativeLayout.LayoutParams menuMainLp;
    protected ButtonPoint startPoint;

    public interface AnimButtonMenuListener {
        void didSelectedItem(AnimButtonItem animButtonItem, int i);
    }

    public BaseAnimButtonMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void initAllParams(AnimButtonItem pathAnimItem) {
        this.MENU_BAR_HEIGHT = getLayoutParams().height;
        this.MENU_ITEM_HEIGHT = pathAnimItem.getViewHeight();
        this.MENU_MAIN_HEIGHT = this.MENU_ITEM_HEIGHT;
        this.FAR_LENGTH = (this.MENU_BAR_HEIGHT - 10) - this.MENU_MAIN_HEIGHT;
        this.END_LENGTH = (this.FAR_LENGTH / 11) * 10;
        this.NEAR_LENGTH = (this.FAR_LENGTH / 11) * 9;
        this.startPoint = new ButtonPoint(10, (this.MENU_BAR_HEIGHT - this.MENU_MAIN_HEIGHT) - 10);
        this.menuItemLp = new RelativeLayout.LayoutParams(this.MENU_ITEM_HEIGHT, this.MENU_ITEM_HEIGHT);
        this.menuItemLp.topMargin = this.startPoint.y;
        this.menuItemLp.leftMargin = this.startPoint.x;
        this.menuMainLp = this.menuItemLp;
    }

    /* access modifiers changed from: protected */
    public void closedAnim() {
        int size = this.animButtonItemList.size();
        for (int i = 0; i < size; i++) {
            this.mHandler.postDelayed(new ButtonRunnable(i, this.mHandler), (long) (this.DELAYED_TIME * ((size - 1) - i)));
        }
    }

    /* access modifiers changed from: protected */
    public void expendAnim() {
        int size = this.animButtonItemList.size();
        for (int i = 0; i < size; i++) {
            this.mHandler.postDelayed(new ButtonRunnable(i, this.mHandler), (long) (this.DELAYED_TIME * i));
        }
    }

    public void setAnimButtonMenuListener(AnimButtonMenuListener animButtonMenuListener2) {
        this.animButtonMenuListener = animButtonMenuListener2;
    }

    /* access modifiers changed from: protected */
    public RelativeLayout.LayoutParams getLpByButtonPoint(ButtonPoint buttonPoint) {
        RelativeLayout.LayoutParams result = new RelativeLayout.LayoutParams(this.menuItemLp.width, this.menuItemLp.height);
        result.topMargin = buttonPoint.y;
        result.leftMargin = buttonPoint.x;
        return result;
    }

    /* access modifiers changed from: protected */
    public Animation animRotate(float fromDegress, float toDegrees, float pivotXValue, float pivotYValue, long durationMillis) {
        RotateAnimation animation = new RotateAnimation(fromDegress, toDegrees, 1, pivotXValue, 1, pivotYValue);
        animation.setDuration(durationMillis);
        animation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                animation.setFillAfter(true);
            }
        });
        return animation;
    }

    /* access modifiers changed from: protected */
    public Animation animTranslate(ButtonPoint fromPoint, ButtonPoint toPoint, long durationMillis) {
        TranslateAnimation anTransformation = new TranslateAnimation(1, 0.0f, 0, (float) (toPoint.x - fromPoint.x), 1, 0.0f, 0, (float) (toPoint.y - fromPoint.y));
        anTransformation.setDuration(durationMillis);
        return anTransformation;
    }

    /* access modifiers changed from: protected */
    public Animation getAnimationByOtherItemExpend(final int itemIndex) {
        AnimButtonItem eachAnimItem = this.animButtonItemList.get(itemIndex);
        Animation animation = animTranslate(eachAnimItem.getStartPoint(), eachAnimItem.getFarPoint(), 180);
        animation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                AnimButtonItem eachAnimItem = BaseAnimButtonMenu.this.animButtonItemList.get(itemIndex);
                eachAnimItem.clearAnimation();
                eachAnimItem.setLayoutParams(BaseAnimButtonMenu.this.getLpByButtonPoint(eachAnimItem.getFarPoint()));
                Animation animation2 = BaseAnimButtonMenu.this.animTranslate(eachAnimItem.getFarPoint(), eachAnimItem.getNearPoint(), 180);
                animation2.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                        AnimButtonItem eachAnimItem = BaseAnimButtonMenu.this.animButtonItemList.get(itemIndex);
                        eachAnimItem.clearAnimation();
                        eachAnimItem.setLayoutParams(BaseAnimButtonMenu.this.getLpByButtonPoint(eachAnimItem.getNearPoint()));
                        Animation animation2 = BaseAnimButtonMenu.this.animTranslate(eachAnimItem.getNearPoint(), eachAnimItem.getEndPoint(), 180);
                        animation2.setAnimationListener(new Animation.AnimationListener() {
                            public void onAnimationStart(Animation animation) {
                            }

                            public void onAnimationRepeat(Animation animation) {
                            }

                            public void onAnimationEnd(Animation animation) {
                                AnimButtonItem eachAnimItem = BaseAnimButtonMenu.this.animButtonItemList.get(itemIndex);
                                eachAnimItem.setLayoutParams(BaseAnimButtonMenu.this.getLpByButtonPoint(eachAnimItem.getEndPoint()));
                                eachAnimItem.clearAnimation();
                            }
                        });
                        eachAnimItem.startAnimation(animation2);
                    }
                });
                eachAnimItem.startAnimation(animation2);
            }
        });
        return animation;
    }

    /* access modifiers changed from: protected */
    public Animation getAnimationByOtherItemClosed(final int itemIndex) {
        AnimButtonItem eachAnimItem = this.animButtonItemList.get(itemIndex);
        Animation animation = animTranslate(eachAnimItem.getEndPoint(), eachAnimItem.getFarPoint(), 180);
        animation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                AnimButtonItem eachAnimItem = BaseAnimButtonMenu.this.animButtonItemList.get(itemIndex);
                eachAnimItem.clearAnimation();
                eachAnimItem.setLayoutParams(BaseAnimButtonMenu.this.getLpByButtonPoint(eachAnimItem.getFarPoint()));
                Animation animation2 = BaseAnimButtonMenu.this.animTranslate(eachAnimItem.getFarPoint(), eachAnimItem.getStartPoint(), 180);
                animation2.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                        AnimButtonItem eachAnimItem = BaseAnimButtonMenu.this.animButtonItemList.get(itemIndex);
                        eachAnimItem.setLayoutParams(BaseAnimButtonMenu.this.getLpByButtonPoint(eachAnimItem.getStartPoint()));
                        eachAnimItem.clearAnimation();
                    }
                });
                eachAnimItem.startAnimation(animation2);
            }
        });
        return animation;
    }

    /* access modifiers changed from: protected */
    public Animation getScaleAnim(final int itemIndex, float from, float to) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(from, to, from, to, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(500);
        scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                AnimButtonItem pathAnimItem = BaseAnimButtonMenu.this.animButtonItemList.get(itemIndex);
                pathAnimItem.clearAnimation();
                pathAnimItem.setLayoutParams(BaseAnimButtonMenu.this.getLpByButtonPoint(pathAnimItem.getStartPoint()));
            }
        });
        return scaleAnimation;
    }
}
