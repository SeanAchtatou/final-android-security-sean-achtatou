package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.PlatformLoginActivity;
import com.mobcent.base.android.ui.activity.adapter.holder.PlatformLoginAdapterHolder;
import com.mobcent.base.forum.android.util.MCResource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlatformLoginAdapter extends BaseAdapter implements MCConstant {
    /* access modifiers changed from: private */
    public Context context;
    private LayoutInflater inflater;
    private List<String> platformIcons = new ArrayList();
    private HashMap<Long, String> platformInfos;
    private List<Long> platforumIds = new ArrayList();
    private MCResource resource;

    public PlatformLoginAdapter(Context context2, HashMap<Long, String> platformInfos2) {
        this.context = context2;
        this.inflater = LayoutInflater.from(context2);
        this.resource = MCResource.getInstance(context2);
        this.platformInfos = platformInfos2;
        getKeyandValues();
    }

    private void getKeyandValues() {
        if (this.platformInfos != null && !this.platformInfos.isEmpty()) {
            for (Long key : this.platformInfos.keySet()) {
                this.platforumIds.add(key);
                this.platformIcons.add(this.platformInfos.get(key));
            }
        }
    }

    public HashMap<Long, String> getPlatformInfos() {
        return this.platformInfos;
    }

    public void setPlatformInfos(HashMap<Long, String> platformInfos2) {
        this.platformInfos = platformInfos2;
    }

    public int getCount() {
        return getPlatformInfos().size();
    }

    public Object getItem(int position) {
        return getPlatformInfos().get(Integer.valueOf(position));
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        PlatformLoginAdapterHolder platformHolder;
        final long platformId = this.platforumIds.get(position).longValue();
        String platformIcon = this.platformIcons.get(position);
        if (convertView == null) {
            convertView = this.inflater.inflate(this.resource.getLayoutId("mc_forum_user_login_grid_item"), (ViewGroup) null);
            platformHolder = new PlatformLoginAdapterHolder();
            platformHolder.setPlatformImg((ImageView) convertView.findViewById(this.resource.getViewId("mc_forum_login_platform_img")));
            convertView.setTag(platformHolder);
        } else {
            platformHolder = (PlatformLoginAdapterHolder) convertView.getTag();
        }
        platformHolder.getPlatformImg().setBackgroundResource(this.resource.getDrawableId(platformIcon));
        convertView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(PlatformLoginAdapter.this.context, PlatformLoginActivity.class);
                intent.putExtra("platformId", platformId);
                PlatformLoginAdapter.this.context.startActivity(intent);
            }
        });
        return convertView;
    }
}
