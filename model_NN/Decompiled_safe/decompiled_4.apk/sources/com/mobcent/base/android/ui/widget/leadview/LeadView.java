package com.mobcent.base.android.ui.widget.leadview;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import java.util.ArrayList;
import java.util.List;

public class LeadView extends RelativeLayout {
    private Context context;
    /* access modifiers changed from: private */
    public int currentPosition = 0;
    /* access modifiers changed from: private */
    public List<String> imageUrlList;
    private LinearLayout indicateLayout;
    private MCResource resource;
    /* access modifiers changed from: private */
    public List<View> viewList;
    private ViewPager viewPager;

    public LeadView(Context context2) {
        super(context2);
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        initData(context2);
    }

    public LeadView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        initData(context2);
    }

    public LeadView(Context context2, AttributeSet attrs, int defStyle) {
        super(context2, attrs, defStyle);
        initData(context2);
    }

    private void initData(Context context2) {
        this.imageUrlList = new ArrayList();
        this.context = context2;
        this.resource = MCResource.getInstance(context2);
        for (int i = 0; i < 5; i++) {
            if (this.resource.getDrawable("help_page" + i) != null) {
                this.imageUrlList.add("help_page" + i);
            }
        }
    }

    public void initViews() {
        if (isShow()) {
            show();
        }
    }

    public void initViews(List<String> imageUrlList2) {
        if (isShow()) {
            this.imageUrlList = imageUrlList2;
            show();
        }
    }

    private boolean isShow() {
        SharedPreferencesDB sharedPreferencesDB = SharedPreferencesDB.getInstance(this.context);
        int versionCode = sharedPreferencesDB.getVersionCode();
        int lastCode = sharedPreferencesDB.getLeadViewVersionCode();
        if (lastCode != -1 && lastCode >= versionCode) {
            return false;
        }
        sharedPreferencesDB.setLeadViewVersionCode(versionCode);
        return true;
    }

    private void show() {
        this.viewPager = new ViewPager(this.context);
        this.viewList = new ArrayList();
        for (String url : this.imageUrlList) {
            ImageView imageView = new ImageView(this.context);
            imageView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setImageDrawable(this.resource.getDrawable(url));
            this.viewList.add(imageView);
        }
        this.viewPager.setAdapter(new LeadViewAdapter());
        addView(this.viewPager);
        final GestureDetector detector = new GestureDetector(new GestureDetector.OnGestureListener() {
            public boolean onSingleTapUp(MotionEvent e) {
                return false;
            }

            public void onShowPress(MotionEvent e) {
            }

            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return false;
            }

            public void onLongPress(MotionEvent e) {
            }

            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                if (e1.getX() - e2.getX() <= 100.0f || Math.abs(velocityX) <= 200.0f || LeadView.this.currentPosition != LeadView.this.imageUrlList.size() - 1) {
                    return false;
                }
                LeadView.this.setVisibility(8);
                LeadView.this.removeAllViews();
                return false;
            }

            public boolean onDown(MotionEvent e) {
                return false;
            }
        });
        this.viewPager.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return detector.onTouchEvent(event);
            }
        });
        this.viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int position) {
                int unused = LeadView.this.currentPosition = position;
                LeadView.this.drawPosition(LeadView.this.currentPosition);
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            public void onPageScrollStateChanged(int arg0) {
            }
        });
        drawPosition(this.currentPosition);
    }

    private class LeadViewAdapter extends PagerAdapter {
        private LeadViewAdapter() {
        }

        public int getCount() {
            return LeadView.this.viewList.size();
        }

        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        public void destroyItem(View container, int position, Object object) {
            ((ViewPager) container).removeView((View) LeadView.this.viewList.get(position));
        }

        public Object instantiateItem(ViewGroup container, int position) {
            container.addView((View) LeadView.this.viewList.get(position));
            return LeadView.this.viewList.get(position);
        }
    }

    /* access modifiers changed from: private */
    public void drawPosition(int position) {
        if (this.indicateLayout == null) {
            this.indicateLayout = new LinearLayout(this.context);
            RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(-2, -2);
            params2.addRule(12, -1);
            params2.addRule(11, -1);
            addView(this.indicateLayout, params2);
        }
        this.indicateLayout.removeAllViews();
        int size = this.imageUrlList.size();
        for (int i = 0; i < size; i++) {
            ImageView imageView = new ImageView(this.context);
            if (i == position) {
                imageView.setBackgroundResource(MCResource.getInstance(this.context).getDrawableId("mc_forum_adgg_icon_n"));
            } else {
                imageView.setBackgroundResource(MCResource.getInstance(this.context).getDrawableId("mc_forum_adgg_icon_h"));
            }
            this.indicateLayout.addView(imageView);
        }
    }
}
