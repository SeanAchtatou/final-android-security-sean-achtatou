package com.mobcent.base.android.ui.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.forum.android.model.UserBoardInfoModel;
import com.mobcent.forum.android.util.MCResource;
import java.util.ArrayList;
import java.util.List;

public class ModeratorBoardAdapter extends BaseAdapter {
    private List<UserBoardInfoModel> boardList = new ArrayList();
    private Context context;
    private LayoutInflater inflater;
    private MCResource resource;

    public ModeratorBoardAdapter(Context context2) {
        this.context = context2;
        this.resource = MCResource.getInstance(context2);
        this.inflater = LayoutInflater.from(context2);
    }

    public int getCount() {
        return this.boardList.size();
    }

    public Object getItem(int position) {
        return this.boardList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        UserBoardInfoModel boardModel = this.boardList.get(position);
        View convertView2 = this.inflater.inflate(this.resource.getLayoutId("mc_forum_moderator_board_item"), (ViewGroup) null);
        ImageView selectImg = (ImageView) convertView2.findViewById(this.resource.getViewId("mc_forum_select_btn"));
        ((TextView) convertView2.findViewById(this.resource.getViewId("mc_forum_moderator_board"))).setText(boardModel.getBoardName());
        if (boardModel.getIsModerator() == 1) {
            selectImg.setBackgroundResource(this.resource.getDrawableId("mc_forum_select1_2"));
        }
        return convertView2;
    }

    public List<UserBoardInfoModel> getBoardList() {
        return this.boardList;
    }

    public void setBoardList(List<UserBoardInfoModel> boardList2) {
        this.boardList = boardList2;
    }
}
