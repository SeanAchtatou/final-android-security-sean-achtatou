package com.mobcent.base.android.ui.widget.gifview;

import android.graphics.Bitmap;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class GifDecoder extends Thread {
    private static final int MaxStackSize = 4096;
    public static final int STATUS_FINISH = -1;
    public static final int STATUS_FORMAT_ERROR = 1;
    public static final int STATUS_OPEN_ERROR = 2;
    public static final int STATUS_PARSING = 0;
    private int[] act;
    private GifAction action = null;
    private int bgColor;
    private int bgIndex;
    private byte[] block = new byte[AccessibilityEventCompat.TYPE_VIEW_HOVER_EXIT];
    private int blockSize = 0;
    private GifFrame currentFrame = null;
    private int delay = 0;
    private int dispose = 0;
    private int frameCount;
    private int[] gct;
    private boolean gctFlag;
    private int gctSize;
    private byte[] gifData = null;
    private GifFrame gifFrame;
    public int height;
    private int ih;
    private Bitmap image;
    private InputStream in;
    private boolean interlace;
    private boolean isShow = false;
    private int iw;
    private int ix;
    private int iy;
    private int lastBgColor;
    private int lastDispose = 0;
    private Bitmap lastImage;
    private int[] lct;
    private boolean lctFlag;
    private int lctSize;
    private int loopCount = 1;
    private int lrh;
    private int lrw;
    private int lrx;
    private int lry;
    private int pixelAspect;
    private byte[] pixelStack;
    private byte[] pixels;
    private short[] prefix;
    private int status;
    private byte[] suffix;
    private int transIndex;
    private boolean transparency = false;
    public int width;

    public GifDecoder(byte[] data, GifAction act2) {
        this.gifData = data;
        this.action = act2;
    }

    public GifDecoder(InputStream is, GifAction act2) {
        this.in = is;
        this.action = act2;
    }

    public void run() {
        if (this.in != null) {
            readStream();
        } else if (this.gifData != null) {
            readByte();
        }
    }

    public void free() {
        GifFrame fg = this.gifFrame;
        while (fg != null) {
            fg.image = null;
            this.gifFrame = this.gifFrame.nextFrame;
            fg = this.gifFrame;
        }
        if (this.in != null) {
            try {
                this.in.close();
            } catch (Exception e) {
            }
            this.in = null;
        }
        this.gifData = null;
    }

    public int getStatus() {
        return this.status;
    }

    public boolean parseOk() {
        return this.status == -1;
    }

    public int getDelay(int n) {
        GifFrame f;
        this.delay = -1;
        if (n >= 0 && n < this.frameCount && (f = getFrame(n)) != null) {
            this.delay = f.delay;
        }
        return this.delay;
    }

    public int[] getDelays() {
        GifFrame f = this.gifFrame;
        int[] d = new int[this.frameCount];
        int i = 0;
        while (f != null && i < this.frameCount) {
            d[i] = f.delay;
            f = f.nextFrame;
            i++;
        }
        return d;
    }

    public int getFrameCount() {
        return this.frameCount;
    }

    public Bitmap getImage() {
        return getFrameImage(0);
    }

    public int getLoopCount() {
        return this.loopCount;
    }

    private void setPixels() {
        int[] dest = new int[(this.width * this.height)];
        if (this.lastDispose > 0) {
            if (this.lastDispose == 3) {
                int n = this.frameCount - 2;
                if (n > 0) {
                    this.lastImage = getFrameImage(n - 1);
                } else {
                    this.lastImage = null;
                }
            }
            if (this.lastImage != null) {
                this.lastImage.getPixels(dest, 0, this.width, 0, 0, this.width, this.height);
                if (this.lastDispose == 2) {
                    int c = 0;
                    if (!this.transparency) {
                        c = this.lastBgColor;
                    }
                    for (int i = 0; i < this.lrh; i++) {
                        int n1 = ((this.lry + i) * this.width) + this.lrx;
                        int n2 = n1 + this.lrw;
                        for (int k = n1; k < n2; k++) {
                            dest[k] = c;
                        }
                    }
                }
            }
        }
        int pass = 1;
        int inc = 8;
        int iline = 0;
        for (int i2 = 0; i2 < this.ih; i2++) {
            int line = i2;
            if (this.interlace) {
                if (iline >= this.ih) {
                    pass++;
                    switch (pass) {
                        case 2:
                            iline = 4;
                            break;
                        case 3:
                            iline = 2;
                            inc = 4;
                            break;
                        case 4:
                            iline = 1;
                            inc = 2;
                            break;
                    }
                }
                line = iline;
                iline += inc;
            }
            int line2 = line + this.iy;
            if (line2 < this.height) {
                int k2 = line2 * this.width;
                int dx = k2 + this.ix;
                int dlim = dx + this.iw;
                if (this.width + k2 < dlim) {
                    dlim = k2 + this.width;
                }
                int sx = i2 * this.iw;
                while (dx < dlim) {
                    int sx2 = sx + 1;
                    int c2 = this.act[this.pixels[sx] & 255];
                    if (c2 != 0) {
                        dest[dx] = c2;
                    }
                    dx++;
                    sx = sx2;
                }
            }
        }
        this.image = Bitmap.createBitmap(dest, this.width, this.height, Bitmap.Config.ARGB_4444);
    }

    public Bitmap getFrameImage(int n) {
        GifFrame frame = getFrame(n);
        if (frame == null) {
            return null;
        }
        return frame.image;
    }

    public GifFrame getCurrentFrame() {
        return this.currentFrame;
    }

    public GifFrame getFrame(int n) {
        GifFrame frame = this.gifFrame;
        int i = 0;
        while (frame != null) {
            if (i == n) {
                return frame;
            }
            frame = frame.nextFrame;
            i++;
        }
        return null;
    }

    public void reset() {
        this.currentFrame = this.gifFrame;
    }

    public GifFrame next() {
        if (!this.isShow) {
            this.isShow = true;
            return this.gifFrame;
        }
        if (this.status != 0) {
            this.currentFrame = this.currentFrame.nextFrame;
            if (this.currentFrame == null) {
                this.currentFrame = this.gifFrame;
            }
        } else if (this.currentFrame.nextFrame != null) {
            this.currentFrame = this.currentFrame.nextFrame;
        }
        return this.currentFrame;
    }

    private int readByte() {
        this.in = new ByteArrayInputStream(this.gifData);
        this.gifData = null;
        return readStream();
    }

    private int readStream() {
        init();
        if (this.in != null) {
            readHeader();
            if (!err()) {
                readContents();
                if (this.frameCount < 0) {
                    this.status = 1;
                    this.action.parseOk(false, -1);
                } else {
                    this.status = -1;
                    this.action.parseOk(true, -1);
                }
            } else {
                this.action.parseFail(true, true);
            }
            try {
                this.in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            this.status = 2;
            this.action.parseOk(false, -1);
        }
        return this.status;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:72:0x0148 */
    /* JADX WARN: Type inference failed for: r7v3 */
    /* JADX WARN: Type inference failed for: r7v4 */
    /* JADX WARNING: Incorrect type for immutable var: ssa=short, code=int, for r7v5, types: [short] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void decodeImageData() {
        /*
            r25 = this;
            r2 = -1
            r0 = r25
            int r0 = r0.iw
            r23 = r0
            r0 = r25
            int r0 = r0.ih
            r24 = r0
            int r17 = r23 * r24
            r0 = r25
            byte[] r0 = r0.pixels
            r23 = r0
            if (r23 == 0) goto L_0x0028
            r0 = r25
            byte[] r0 = r0.pixels
            r23 = r0
            r0 = r23
            int r0 = r0.length
            r23 = r0
            r0 = r23
            r1 = r17
            if (r0 >= r1) goto L_0x0034
        L_0x0028:
            r0 = r17
            byte[] r0 = new byte[r0]
            r23 = r0
            r0 = r23
            r1 = r25
            r1.pixels = r0
        L_0x0034:
            r0 = r25
            short[] r0 = r0.prefix
            r23 = r0
            if (r23 != 0) goto L_0x004a
            r23 = 4096(0x1000, float:5.74E-42)
            r0 = r23
            short[] r0 = new short[r0]
            r23 = r0
            r0 = r23
            r1 = r25
            r1.prefix = r0
        L_0x004a:
            r0 = r25
            byte[] r0 = r0.suffix
            r23 = r0
            if (r23 != 0) goto L_0x0060
            r23 = 4096(0x1000, float:5.74E-42)
            r0 = r23
            byte[] r0 = new byte[r0]
            r23 = r0
            r0 = r23
            r1 = r25
            r1.suffix = r0
        L_0x0060:
            r0 = r25
            byte[] r0 = r0.pixelStack
            r23 = r0
            if (r23 != 0) goto L_0x0076
            r23 = 4097(0x1001, float:5.741E-42)
            r0 = r23
            byte[] r0 = new byte[r0]
            r23 = r0
            r0 = r23
            r1 = r25
            r1.pixelStack = r0
        L_0x0076:
            int r11 = r25.read()
            r23 = 1
            int r6 = r23 << r11
            int r13 = r6 + 1
            int r3 = r6 + 2
            r18 = r2
            int r9 = r11 + 1
            r23 = 1
            int r23 = r23 << r9
            r24 = 1
            int r8 = r23 - r24
            r7 = 0
        L_0x008f:
            if (r7 >= r6) goto L_0x00aa
            r0 = r25
            short[] r0 = r0.prefix
            r23 = r0
            r24 = 0
            r23[r7] = r24
            r0 = r25
            byte[] r0 = r0.suffix
            r23 = r0
            r0 = r7
            byte r0 = (byte) r0
            r24 = r0
            r23[r7] = r24
            int r7 = r7 + 1
            goto L_0x008f
        L_0x00aa:
            r4 = 0
            r19 = r4
            r21 = r4
            r14 = r4
            r10 = r4
            r5 = r4
            r12 = r4
            r15 = 0
            r20 = r19
            r22 = r21
        L_0x00b8:
            r0 = r15
            r1 = r17
            if (r0 >= r1) goto L_0x01d5
            if (r22 != 0) goto L_0x01d9
            if (r5 >= r9) goto L_0x00f9
            if (r10 != 0) goto L_0x00e0
            int r10 = r25.readBlock()
            if (r10 > 0) goto L_0x00df
            r21 = r22
        L_0x00cb:
            r15 = r20
        L_0x00cd:
            r0 = r15
            r1 = r17
            if (r0 >= r1) goto L_0x01d4
            r0 = r25
            byte[] r0 = r0.pixels
            r23 = r0
            r24 = 0
            r23[r15] = r24
            int r15 = r15 + 1
            goto L_0x00cd
        L_0x00df:
            r4 = 0
        L_0x00e0:
            r0 = r25
            byte[] r0 = r0.block
            r23 = r0
            byte r23 = r23[r4]
            r0 = r23
            r0 = r0 & 255(0xff, float:3.57E-43)
            r23 = r0
            int r23 = r23 << r5
            int r12 = r12 + r23
            int r5 = r5 + 8
            int r4 = r4 + 1
            int r10 = r10 + -1
            goto L_0x00b8
        L_0x00f9:
            r7 = r12 & r8
            int r12 = r12 >> r9
            int r5 = r5 - r9
            if (r7 > r3) goto L_0x01d5
            if (r7 != r13) goto L_0x0104
            r21 = r22
            goto L_0x00cb
        L_0x0104:
            if (r7 != r6) goto L_0x0115
            int r9 = r11 + 1
            r23 = 1
            int r23 = r23 << r9
            r24 = 1
            int r8 = r23 - r24
            int r3 = r6 + 2
            r18 = r2
            goto L_0x00b8
        L_0x0115:
            r0 = r18
            r1 = r2
            if (r0 != r1) goto L_0x0132
            r0 = r25
            byte[] r0 = r0.pixelStack
            r23 = r0
            int r21 = r22 + 1
            r0 = r25
            byte[] r0 = r0.suffix
            r24 = r0
            byte r24 = r24[r7]
            r23[r22] = r24
            r18 = r7
            r14 = r7
            r22 = r21
            goto L_0x00b8
        L_0x0132:
            r16 = r7
            if (r7 != r3) goto L_0x0148
            r0 = r25
            byte[] r0 = r0.pixelStack
            r23 = r0
            int r21 = r22 + 1
            r0 = r14
            byte r0 = (byte) r0
            r24 = r0
            r23[r22] = r24
            r7 = r18
            r22 = r21
        L_0x0148:
            if (r7 <= r6) goto L_0x0167
            r0 = r25
            byte[] r0 = r0.pixelStack
            r23 = r0
            int r21 = r22 + 1
            r0 = r25
            byte[] r0 = r0.suffix
            r24 = r0
            byte r24 = r24[r7]
            r23[r22] = r24
            r0 = r25
            short[] r0 = r0.prefix
            r23 = r0
            short r7 = r23[r7]
            r22 = r21
            goto L_0x0148
        L_0x0167:
            r0 = r25
            byte[] r0 = r0.suffix
            r23 = r0
            byte r23 = r23[r7]
            r0 = r23
            r0 = r0 & 255(0xff, float:3.57E-43)
            r14 = r0
            r23 = 4096(0x1000, float:5.74E-42)
            r0 = r3
            r1 = r23
            if (r0 < r1) goto L_0x017f
            r21 = r22
            goto L_0x00cb
        L_0x017f:
            r0 = r25
            byte[] r0 = r0.pixelStack
            r23 = r0
            int r21 = r22 + 1
            r0 = r14
            byte r0 = (byte) r0
            r24 = r0
            r23[r22] = r24
            r0 = r25
            short[] r0 = r0.prefix
            r23 = r0
            r0 = r18
            short r0 = (short) r0
            r24 = r0
            r23[r3] = r24
            r0 = r25
            byte[] r0 = r0.suffix
            r23 = r0
            r0 = r14
            byte r0 = (byte) r0
            r24 = r0
            r23[r3] = r24
            int r3 = r3 + 1
            r23 = r3 & r8
            if (r23 != 0) goto L_0x01b6
            r23 = 4096(0x1000, float:5.74E-42)
            r0 = r3
            r1 = r23
            if (r0 >= r1) goto L_0x01b6
            int r9 = r9 + 1
            int r8 = r8 + r3
        L_0x01b6:
            r18 = r16
        L_0x01b8:
            int r21 = r21 + -1
            r0 = r25
            byte[] r0 = r0.pixels
            r23 = r0
            int r19 = r20 + 1
            r0 = r25
            byte[] r0 = r0.pixelStack
            r24 = r0
            byte r24 = r24[r21]
            r23[r20] = r24
            int r15 = r15 + 1
            r20 = r19
            r22 = r21
            goto L_0x00b8
        L_0x01d4:
            return
        L_0x01d5:
            r21 = r22
            goto L_0x00cb
        L_0x01d9:
            r21 = r22
            goto L_0x01b8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.base.android.ui.widget.gifview.GifDecoder.decodeImageData():void");
    }

    private boolean err() {
        return this.status != 0;
    }

    private void init() {
        this.status = 0;
        this.frameCount = 0;
        this.gifFrame = null;
        this.gct = null;
        this.lct = null;
    }

    private int read() {
        try {
            return this.in.read();
        } catch (Exception e) {
            this.status = 1;
            return 0;
        }
    }

    private int readBlock() {
        this.blockSize = read();
        int n = 0;
        if (this.blockSize > 0) {
            while (n < this.blockSize) {
                try {
                    int count = this.in.read(this.block, n, this.blockSize - n);
                    if (count == -1) {
                        break;
                    }
                    n += count;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (n < this.blockSize) {
                this.status = 1;
            }
        }
        return n;
    }

    private int[] readColorTable(int ncolors) {
        int nbytes = ncolors * 3;
        int[] tab = null;
        byte[] c = new byte[nbytes];
        int n = 0;
        try {
            n = this.in.read(c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (n < nbytes) {
            this.status = 1;
        } else {
            tab = new int[AccessibilityEventCompat.TYPE_VIEW_HOVER_EXIT];
            int j = 0;
            for (int i = 0; i < ncolors; i++) {
                int j2 = j + 1;
                int r = c[j] & 255;
                int j3 = j2 + 1;
                tab[i] = -16777216 | (r << 16) | ((c[j2] & 255) << 8) | (c[j3] & 255);
                j = j3 + 1;
            }
        }
        return tab;
    }

    private void readContents() {
        boolean done = false;
        while (!done && !err()) {
            switch (read()) {
                case 0:
                    break;
                case 33:
                    switch (read()) {
                        case 249:
                            readGraphicControlExt();
                            continue;
                        case 255:
                            readBlock();
                            String app = "";
                            for (int i = 0; i < 11; i++) {
                                app = app + ((char) this.block[i]);
                            }
                            if (!app.equals("NETSCAPE2.0")) {
                                skip();
                                break;
                            } else {
                                readNetscapeExt();
                                continue;
                            }
                        default:
                            skip();
                            continue;
                    }
                case 44:
                    readImage();
                    break;
                case 59:
                    done = true;
                    break;
                default:
                    this.status = 1;
                    break;
            }
        }
    }

    private void readGraphicControlExt() {
        read();
        int packed = read();
        this.dispose = (packed & 28) >> 2;
        if (this.dispose == 0) {
            this.dispose = 1;
        }
        this.transparency = (packed & 1) != 0;
        int delay_ = readShort();
        this.delay = delay_ < 5 ? 100 : delay_ * 10;
        this.transIndex = read();
        read();
    }

    private void readHeader() {
        String id = "";
        for (int i = 0; i < 6; i++) {
            id = id + ((char) read());
        }
        if (!id.startsWith("GIF")) {
            this.status = 1;
            return;
        }
        readLSD();
        if (this.gctFlag && !err()) {
            this.gct = readColorTable(this.gctSize);
            this.bgColor = this.gct[this.bgIndex];
        }
    }

    private void readImage() {
        boolean z;
        boolean z2;
        this.ix = readShort();
        this.iy = readShort();
        this.iw = readShort();
        this.ih = readShort();
        int packed = read();
        if ((packed & AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.lctFlag = z;
        if ((packed & 64) != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.interlace = z2;
        this.lctSize = 2 << (packed & 7);
        if (this.lctFlag) {
            this.lct = readColorTable(this.lctSize);
            this.act = this.lct;
        } else {
            this.act = this.gct;
            if (this.bgIndex == this.transIndex) {
                this.bgColor = 0;
            }
        }
        int save = 0;
        if (this.transparency) {
            save = this.act[this.transIndex];
            this.act[this.transIndex] = 0;
        }
        if (this.act == null) {
            this.status = 1;
        }
        if (!err()) {
            decodeImageData();
            skip();
            if (!err()) {
                this.frameCount++;
                this.image = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_4444);
                setPixels();
                if (this.gifFrame == null) {
                    this.gifFrame = new GifFrame(this.image, this.delay);
                    this.currentFrame = this.gifFrame;
                } else {
                    GifFrame f = this.gifFrame;
                    while (f.nextFrame != null) {
                        f = f.nextFrame;
                    }
                    f.nextFrame = new GifFrame(this.image, this.delay);
                }
                if (this.transparency) {
                    this.act[this.transIndex] = save;
                }
                resetFrame();
                this.action.parseOk(true, this.frameCount);
            }
        }
    }

    private void readLSD() {
        this.width = readShort();
        this.height = readShort();
        int packed = read();
        this.gctFlag = (packed & AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER) != 0;
        this.gctSize = 2 << (packed & 7);
        this.bgIndex = read();
        this.pixelAspect = read();
    }

    private void readNetscapeExt() {
        do {
            readBlock();
            if (this.block[0] == 1) {
                this.loopCount = ((this.block[2] & 255) << 8) | (this.block[1] & 255);
            }
            if (this.blockSize <= 0) {
                return;
            }
        } while (!err());
    }

    private int readShort() {
        return read() | (read() << 8);
    }

    private void resetFrame() {
        this.lastDispose = this.dispose;
        this.lrx = this.ix;
        this.lry = this.iy;
        this.lrw = this.iw;
        this.lrh = this.ih;
        this.lastImage = this.image;
        this.lastBgColor = this.bgColor;
        this.dispose = 0;
        this.transparency = false;
        this.delay = 0;
        this.lct = null;
    }

    private void skip() {
        do {
            readBlock();
            if (this.blockSize <= 0) {
                return;
            }
        } while (!err());
    }
}
