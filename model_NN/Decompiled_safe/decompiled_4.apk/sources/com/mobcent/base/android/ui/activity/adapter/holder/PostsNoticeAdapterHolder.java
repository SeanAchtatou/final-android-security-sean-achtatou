package com.mobcent.base.android.ui.activity.adapter.holder;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.ad.android.ui.widget.AdView;

public class PostsNoticeAdapterHolder {
    private AdView adTopView;
    private AdView adView;
    private ImageView iconImg;
    private Button noticeCheckBtn;
    private LinearLayout noticeContentLayout;
    private TextView noticeContentText;
    private LinearLayout noticeDetailLayout;
    private RelativeLayout noticeItemBox;
    private TextView noticeMsgNew;
    private TextView noticeMsgReplyed;
    private TextView noticeMsgUnreply;
    private ImageButton noticeReplyBtn1;
    private Button noticeReplyBtn2;
    private ImageButton noticeSelectFaceBtn;
    private ImageButton noticeSelectImgBtn;
    private TextView noticeSubjectText;
    private TextView noticeTimeText;
    private TextView noticeUserText;
    private TextView quoteContentText;
    private EditText replyEdit;
    private TextView replytext;

    public ImageView getIconImg() {
        return this.iconImg;
    }

    public void setIconImg(ImageView iconImg2) {
        this.iconImg = iconImg2;
    }

    public AdView getAdView() {
        return this.adView;
    }

    public void setAdView(AdView adView2) {
        this.adView = adView2;
    }

    public RelativeLayout getNoticeItemBox() {
        return this.noticeItemBox;
    }

    public void setNoticeItemBox(RelativeLayout noticeItemBox2) {
        this.noticeItemBox = noticeItemBox2;
    }

    public TextView getReplytext() {
        return this.replytext;
    }

    public void setReplytext(TextView replytext2) {
        this.replytext = replytext2;
    }

    public TextView getNoticeSubjectText() {
        return this.noticeSubjectText;
    }

    public void setNoticeSubjectText(TextView noticeSubjectText2) {
        this.noticeSubjectText = noticeSubjectText2;
    }

    public TextView getNoticeUserText() {
        return this.noticeUserText;
    }

    public void setNoticeUserText(TextView noticeUserText2) {
        this.noticeUserText = noticeUserText2;
    }

    public TextView getNoticeTimeText() {
        return this.noticeTimeText;
    }

    public void setNoticeTimeText(TextView noticeTimeText2) {
        this.noticeTimeText = noticeTimeText2;
    }

    public TextView getNoticeContentText() {
        return this.noticeContentText;
    }

    public void setNoticeContentText(TextView noticeContentText2) {
        this.noticeContentText = noticeContentText2;
    }

    public TextView getQuoteContentText() {
        return this.quoteContentText;
    }

    public void setQuoteContentText(TextView quoteContentText2) {
        this.quoteContentText = quoteContentText2;
    }

    public Button getNoticeCheckBtn() {
        return this.noticeCheckBtn;
    }

    public void setNoticeCheckBtn(Button noticeCheckBtn2) {
        this.noticeCheckBtn = noticeCheckBtn2;
    }

    public ImageButton getNoticeReplyBtn1() {
        return this.noticeReplyBtn1;
    }

    public void setNoticeReplyBtn1(ImageButton noticeReplyBtn12) {
        this.noticeReplyBtn1 = noticeReplyBtn12;
    }

    public ImageButton getNoticeSelectImgBtn() {
        return this.noticeSelectImgBtn;
    }

    public void setNoticeSelectImgBtn(ImageButton noticeSelectImgBtn2) {
        this.noticeSelectImgBtn = noticeSelectImgBtn2;
    }

    public ImageButton getNoticeSelectFaceBtn() {
        return this.noticeSelectFaceBtn;
    }

    public void setNoticeSelectFaceBtn(ImageButton noticeSelectFaceBtn2) {
        this.noticeSelectFaceBtn = noticeSelectFaceBtn2;
    }

    public EditText getReplyEdit() {
        return this.replyEdit;
    }

    public void setReplyEdit(EditText replyEdit2) {
        this.replyEdit = replyEdit2;
    }

    public Button getNoticeReplyBtn2() {
        return this.noticeReplyBtn2;
    }

    public void setNoticeReplyBtn2(Button noticeReplyBtn22) {
        this.noticeReplyBtn2 = noticeReplyBtn22;
    }

    public LinearLayout getNoticeDetailLayout() {
        return this.noticeDetailLayout;
    }

    public void setNoticeDetailLayout(LinearLayout noticeDetailLayout2) {
        this.noticeDetailLayout = noticeDetailLayout2;
    }

    public TextView getNoticeMsgNew() {
        return this.noticeMsgNew;
    }

    public void setNoticeMsgNew(TextView noticeMsgNew2) {
        this.noticeMsgNew = noticeMsgNew2;
    }

    public TextView getNoticeMsgUnreply() {
        return this.noticeMsgUnreply;
    }

    public void setNoticeMsgUnreply(TextView noticeMsgUnreply2) {
        this.noticeMsgUnreply = noticeMsgUnreply2;
    }

    public TextView getNoticeMsgReplyed() {
        return this.noticeMsgReplyed;
    }

    public void setNoticeMsgReplyed(TextView noticeMsgReplyed2) {
        this.noticeMsgReplyed = noticeMsgReplyed2;
    }

    public LinearLayout getNoticeContentLayout() {
        return this.noticeContentLayout;
    }

    public void setNoticeContentLayout(LinearLayout noticeContentLayout2) {
        this.noticeContentLayout = noticeContentLayout2;
    }

    public AdView getAdTopView() {
        return this.adTopView;
    }

    public void setAdTopView(AdView adTopView2) {
        this.adTopView = adTopView2;
    }
}
