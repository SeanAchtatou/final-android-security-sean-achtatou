package com.mobcent.base.android.ui.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baidu.location.BDLocation;
import com.mobcent.ad.android.model.AdModel;
import com.mobcent.ad.android.ui.widget.AdView;
import com.mobcent.base.android.constant.MCConstant;
import com.mobcent.base.android.ui.activity.adapter.FacePagerAdapter;
import com.mobcent.base.android.ui.activity.adapter.PoiAdapter;
import com.mobcent.base.android.ui.activity.view.MCProgressBar;
import com.mobcent.base.android.ui.activity.view.MCResizeLayout;
import com.mobcent.base.android.ui.activity.view.MCSetVisibleView;
import com.mobcent.base.forum.android.util.MCFaceUtil;
import com.mobcent.base.forum.android.util.MCTouchUtil;
import com.mobcent.forum.android.constant.PermConstant;
import com.mobcent.forum.android.model.LocationModel;
import com.mobcent.forum.android.service.LocationService;
import com.mobcent.forum.android.service.impl.LocationServiceImpl;
import com.mobcent.forum.android.service.impl.PermServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.ImageUtil;
import com.mobcent.forum.android.util.LocationUtil;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public abstract class BasePublishTopicActivity extends BasePhotoPreviewActivity implements MCConstant {
    protected final int AUDIO_STATE_SHOW = 3;
    protected final int FACE_STATE_SHOW = 2;
    protected final int KEYBOARD_STATE_HIDE = 0;
    protected final int KEYBOARD_STATE_SHOW = 1;
    private final int NORMAL_VISIBLE = 1;
    private final int ONLY_MODERATOR_VISIBLE = 3;
    int PUBLIC_TOPIC_POSITION;
    int REPLY_TOPIC_POSITION;
    private final int REPLY_VISIBLE = 2;
    protected final int REQUIRE_LOCATION_OFF = 0;
    protected final int REQUIRE_LOCATION_ON = 1;
    protected final int SELECT_TITLE = 0;
    protected final int UNSELECT_TITLE = 1;
    protected final int VISIBLE_STATE_SHOW = 4;
    private List<AdModel> adList;
    protected AdView adView;
    protected RelativeLayout audioLayout;
    protected Button backBtn;
    protected String baseLocationStr;
    protected long boardId = 0;
    protected ListView boardListview;
    protected boolean canPublishTopic = false;
    protected ImageView chooseFace;
    protected TranslateAnimation closeAnimation;
    protected EditText contentEdit;
    protected final int contentMaxLen = 7000;
    protected LinearLayout contentTransparentLayout;
    protected MCResizeLayout editLayout;
    protected LinearLayout faceLayout;
    protected List<LinkedHashMap> faceList;
    protected ViewPager facePager;
    protected FacePagerAdapter facePagerAdapter;
    protected HashMap<String, WeakReference<Bitmap>> imageCache;
    protected long initBoardId = 0;
    protected boolean isExit = false;
    protected boolean isLocationSucc = false;
    protected int keyboardState = 1;
    protected double latitude;
    protected RelativeLayout loadingBox;
    protected boolean locationComplete = false;
    private LocationService locationService;
    protected String locationStr;
    private LocationUtil locationUtil;
    protected double longitude;
    protected TextView noPoiText;
    protected TranslateAnimation openAnimation;
    protected PoiAdapter poiAdapter;
    protected RelativeLayout poiBox;
    protected ListView poiList;
    protected RelativeLayout poiLoadingContainer;
    protected MCProgressBar poiProgressBar;
    protected TextView poiText;
    protected List<String> pois;
    protected MCProgressBar progressBar;
    protected LinearLayout publishBar;
    protected RelativeLayout publishBarBox;
    protected Button publishBtn;
    protected boolean publishIng = false;
    private RelativeLayout publishLocationBox;
    /* access modifiers changed from: private */
    public TextView publishLocationText;
    protected int requireLocation = 0;
    protected ImageView select1;
    protected ImageView select2;
    protected ImageView select3;
    protected int selectBackground = 0;
    protected EditText selectBoardImg;
    protected int selectVisibleId = 1;
    protected ImageView[] selects;
    protected ImageView setVisible;
    protected RadioGroup setVisibleGroup;
    protected final char splitChar = 223;
    protected final char tagImg = 225;
    protected ImageView takeLocation;
    protected ImageView takePhoto;
    /* access modifiers changed from: private */
    public AlertDialog.Builder takePicDialog;
    protected ImageView takePicture;
    protected ImageView takeUser;
    protected ImageView takeVoice;
    protected EditText titleEdit;
    protected TextView titleLabelText;
    protected final int titleMaxLen = 25;
    /* access modifiers changed from: private */
    public List<RadioButton> visibleList;

    /* access modifiers changed from: protected */
    public abstract boolean publicTopic();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLocation();
    }

    /* access modifiers changed from: protected */
    public void initData() {
        super.initData();
        this.uploadType = 2;
        this.imageCache = new HashMap<>();
        this.faceList = MCFaceUtil.getFaceConstant(this).getFaceList();
        initLocalLocation();
        this.openAnimation = new TranslateAnimation(0.0f, 0.0f, 550.0f, 30.0f);
        this.openAnimation.setDuration(300);
        this.openAnimation.start();
        this.closeAnimation = new TranslateAnimation(0.0f, 0.0f, 30.0f, 500.0f);
        this.closeAnimation.setDuration(300);
        this.closeAnimation.start();
        this.PUBLIC_TOPIC_POSITION = new Integer(this.resource.getString("mc_forum_public_topic_position")).intValue();
        this.REPLY_TOPIC_POSITION = new Integer(this.resource.getString("mc_forum_reply_topic_position")).intValue();
    }

    private void initLocalLocation() {
        this.locationService = new LocationServiceImpl(this);
        LocationModel location = this.locationService.getLocalLocationInfo(new UserServiceImpl(this).getLoginUserId());
        if (location != null) {
            this.longitude = location.getLongitude();
            this.latitude = location.getLatitude();
            this.locationStr = location.getAddrStr();
            this.locationComplete = true;
        }
        this.locationUtil = new LocationUtil(this, true, new LocationUtil.LocationDelegate() {
            public void locationResults(BDLocation location) {
                if (location.getLocType() == 161 || location.getLocType() == 61) {
                    BasePublishTopicActivity.this.hideLoadingBox();
                    BasePublishTopicActivity.this.longitude = location.getLongitude();
                    BasePublishTopicActivity.this.latitude = location.getLatitude();
                    BasePublishTopicActivity.this.baseLocationStr = location.getAddrStr();
                    BasePublishTopicActivity.this.locationStr = BasePublishTopicActivity.this.baseLocationStr;
                    BasePublishTopicActivity.this.publishLocationText.setText(BasePublishTopicActivity.this.locationStr);
                    BasePublishTopicActivity.this.isLocationSucc = true;
                    BasePublishTopicActivity.this.locationComplete = true;
                    if (!BasePublishTopicActivity.this.canPublishTopic || !BasePublishTopicActivity.this.publishIng) {
                    }
                } else if (location.getLocType() == 62) {
                    BasePublishTopicActivity.this.locationComplete = true;
                    BasePublishTopicActivity.this.hideLoadingBox();
                    BasePublishTopicActivity.this.publishLocationText.setText(BasePublishTopicActivity.this.getResources().getString(BasePublishTopicActivity.this.resource.getStringId("mc_forum_location_fail_warn")));
                } else if (location.getLocType() == 167) {
                    BasePublishTopicActivity.this.locationComplete = true;
                    BasePublishTopicActivity.this.hideLoadingBox();
                    BasePublishTopicActivity.this.publishLocationText.setText(BasePublishTopicActivity.this.getResources().getString(BasePublishTopicActivity.this.resource.getStringId("mc_forum_service_location_fail_warn")));
                } else if (location.getLocType() == 63) {
                    BasePublishTopicActivity.this.locationComplete = true;
                    BasePublishTopicActivity.this.hideLoadingBox();
                    BasePublishTopicActivity.this.publishLocationText.setText(BasePublishTopicActivity.this.getResources().getString(BasePublishTopicActivity.this.resource.getStringId("mc_forum_connection_fail")));
                }
            }

            public void onReceivePoi(boolean hasPoi, List<String> poi) {
                BasePublishTopicActivity.this.poiProgressBar.stop();
                BasePublishTopicActivity.this.poiLoadingContainer.setVisibility(8);
                if (!hasPoi || poi == null || poi.isEmpty()) {
                    BasePublishTopicActivity.this.noPoiText.setVisibility(0);
                    return;
                }
                BasePublishTopicActivity.this.pois = poi;
                BasePublishTopicActivity.this.poiAdapter.setPoi(poi);
                BasePublishTopicActivity.this.poiAdapter.notifyDataSetInvalidated();
                BasePublishTopicActivity.this.poiAdapter.notifyDataSetChanged();
            }
        });
        this.locationUtil.setRequestPoi(true);
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        initLoadingBox();
        this.titleLabelText = (TextView) findViewById(this.resource.getViewId("mc_forum_public_label_text"));
        this.editLayout = (MCResizeLayout) findViewById(this.resource.getViewId("mc_forum_edit_layout"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("mc_forum_back_btn"));
        this.publishBtn = (Button) findViewById(this.resource.getViewId("mc_forum_public_btn"));
        this.publishBarBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_public_bar_box"));
        this.publishBar = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_publish_bar"));
        this.chooseFace = (ImageView) findViewById(this.resource.getViewId("mc_forum_choose_face"));
        this.takePhoto = (ImageView) findViewById(this.resource.getViewId("mc_forum_take_photo"));
        this.takePicture = (ImageView) findViewById(this.resource.getViewId("mc_forum_take_picture"));
        this.takeVoice = (ImageView) findViewById(this.resource.getViewId("mc_forum_take_voice"));
        this.takeLocation = (ImageView) findViewById(this.resource.getViewId("mc_forum_take_location"));
        this.setVisible = (ImageView) findViewById(this.resource.getViewId("mc_forum_set_visible"));
        this.setVisibleGroup = (RadioGroup) findViewById(this.resource.getViewId("mc_forum_set_visible_group"));
        if (this.setVisibleGroup != null) {
            this.visibleList = new ArrayList();
            this.visibleList.add(new MCSetVisibleView(this).create(this.resource.getString("mc_fourm_permission_normal_set"), 1));
            this.visibleList.add(new MCSetVisibleView(this).create(this.resource.getString("mc_forum_permission_reply_see"), 2));
            this.visibleList.add(new MCSetVisibleView(this).create(this.resource.getString("mc_forum_permission_moderator_see"), 3));
            for (int i = 0; i < this.visibleList.size(); i++) {
                this.setVisibleGroup.addView(this.visibleList.get(i));
            }
        }
        this.publishLocationBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_publish_location_box"));
        this.publishLocationText = (TextView) findViewById(this.resource.getViewId("mc_forum_publish_location_text"));
        this.titleEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_title_edit"));
        this.boardListview = (ListView) findViewById(this.resource.getViewId("mc_forum_board_lv"));
        this.selectBoardImg = (EditText) findViewById(this.resource.getViewId("mc_forum_publish_board_edit"));
        this.contentEdit = (EditText) findViewById(this.resource.getViewId("mc_forum_content_edit"));
        this.faceLayout = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_face_layout"));
        this.facePager = (ViewPager) findViewById(this.resource.getViewId("mc_forum_face_pager"));
        this.facePagerAdapter = new FacePagerAdapter(this, this.faceList);
        this.facePager.setAdapter(this.facePagerAdapter);
        this.select1 = (ImageView) findViewById(this.resource.getViewId("mc_forum_select1"));
        this.select2 = (ImageView) findViewById(this.resource.getViewId("mc_forum_select2"));
        this.select3 = (ImageView) findViewById(this.resource.getViewId("mc_forum_select3"));
        this.selects = new ImageView[]{this.select1, this.select2, this.select3};
        this.adView = (AdView) findViewById(this.resource.getViewId("mc_ad_box"));
        this.contentTransparentLayout = (LinearLayout) findViewById(this.resource.getViewId("mc_forum_content_transparent_layout"));
        MCTouchUtil.createTouchDelegate(this.chooseFace, 10);
        MCTouchUtil.createTouchDelegate(this.takePhoto, 10);
        MCTouchUtil.createTouchDelegate(this.takePicture, 10);
        MCTouchUtil.createTouchDelegate(this.takeVoice, 10);
        MCTouchUtil.createTouchDelegate(this.takeLocation, 10);
        MCTouchUtil.createTouchDelegate(this.setVisible, 10);
        this.poiBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_froum_poi_box"));
        this.poiText = (TextView) findViewById(this.resource.getViewId("mc_forum_poi_text"));
        this.noPoiText = (TextView) findViewById(this.resource.getViewId("mc_forum_no_poid_Text"));
        this.poiList = (ListView) findViewById(this.resource.getViewId("mc_forum_poi_list"));
        this.poiLoadingContainer = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_poi_loading_container"));
        this.poiProgressBar = (MCProgressBar) findViewById(this.resource.getViewId("mc_forum_poi_progress_bar"));
        this.poiAdapter = new PoiAdapter(this, new ArrayList());
        this.poiList.setAdapter((ListAdapter) this.poiAdapter);
        this.publishLocationBox.setVisibility(0);
        this.boardListview.setVisibility(8);
        this.takePicDialog = new AlertDialog.Builder(this).setTitle(this.resource.getString("mc_forum_function")).setItems(new String[]{this.resource.getString("mc_forum_publish_take_photo"), this.resource.getString("mc_forum_publish_take_picture")}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    BasePublishTopicActivity.this.cameraPhotoListener();
                } else {
                    BasePublishTopicActivity.this.localPhotoListener();
                }
            }
        });
        this.audioLayout = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_audio_layout"));
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        super.initWidgetActions();
        this.editLayout.setOnResizeListener(new MCResizeLayout.OnResizeListener() {
            public void OnResize(int w, int h, int oldw, int oldh) {
            }
        });
        this.contentEdit.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                BasePublishTopicActivity.this.getContentEditText().requestFocus();
                BasePublishTopicActivity.this.changeKeyboardState(1);
                BasePublishTopicActivity.this.boardListview.setVisibility(8);
                return false;
            }
        });
        this.contentEdit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePublishTopicActivity.this.getContentEditText().requestFocus();
                BasePublishTopicActivity.this.changeKeyboardState(1);
                BasePublishTopicActivity.this.boardListview.setVisibility(8);
            }
        });
        this.contentTransparentLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePublishTopicActivity.this.contentEdit.performClick();
                BasePublishTopicActivity.this.boardListview.setVisibility(8);
            }
        });
        this.contentTransparentLayout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                BasePublishTopicActivity.this.boardListview.setVisibility(8);
                return false;
            }
        });
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BasePublishTopicActivity.this.exitCheckChanged()) {
                    BasePublishTopicActivity.this.exitAlertDialog.show();
                } else {
                    BasePublishTopicActivity.this.exitNoSaveEvent();
                }
            }
        });
        this.publishBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePublishTopicActivity.this.poiBox.setVisibility(8);
                BasePublishTopicActivity.this.poiBox.setAnimation(BasePublishTopicActivity.this.closeAnimation);
                if (!BasePublishTopicActivity.this.publishIng) {
                    BasePublishTopicActivity.this.publicTopic();
                }
            }
        });
        this.chooseFace.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BasePublishTopicActivity.this.keyboardState != 2) {
                    BasePublishTopicActivity.this.changeKeyboardState(2);
                } else {
                    BasePublishTopicActivity.this.changeKeyboardState(1);
                }
            }
        });
        this.takePhoto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePublishTopicActivity.this.cameraPhotoListener();
            }
        });
        this.takePicture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (new PermServiceImpl(BasePublishTopicActivity.this).getPermNum(PermConstant.USER_GROUP, PermConstant.UPLOAD, -1) == 1 && new PermServiceImpl(BasePublishTopicActivity.this).getPermNum(PermConstant.BOARDS, PermConstant.UPLOAD, BasePublishTopicActivity.this.boardId) == 1) {
                    BasePublishTopicActivity.this.takePicDialog.show();
                } else {
                    BasePublishTopicActivity.this.warnMessageByStr(BasePublishTopicActivity.this.resource.getString("mc_forum_permission_cannot_upload_pic"));
                }
            }
        });
        this.takeLocation.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BasePublishTopicActivity.this.getLocation();
            }
        });
        this.setVisible.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BasePublishTopicActivity.this.keyboardState != 4) {
                    BasePublishTopicActivity.this.setVisible.setImageResource(BasePublishTopicActivity.this.resource.getDrawableId("mc_forum_ico32_h"));
                    BasePublishTopicActivity.this.changeKeyboardState(4);
                    return;
                }
                BasePublishTopicActivity.this.changeKeyboardState(0);
                BasePublishTopicActivity.this.setVisible.setImageResource(BasePublishTopicActivity.this.resource.getDrawableId("mc_forum_ico32_n"));
            }
        });
        this.setVisibleGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                for (int i = 0; i < BasePublishTopicActivity.this.visibleList.size(); i++) {
                    if (checkedId == ((RadioButton) BasePublishTopicActivity.this.visibleList.get(i)).getId()) {
                        ((RadioButton) BasePublishTopicActivity.this.visibleList.get(i)).setButtonDrawable(BasePublishTopicActivity.this.resource.getDrawable("mc_forum_select2_2"));
                        BasePublishTopicActivity.this.selectVisibleId = checkedId;
                    } else {
                        ((RadioButton) BasePublishTopicActivity.this.visibleList.get(i)).setButtonDrawable(BasePublishTopicActivity.this.resource.getDrawable("mc_forum_select2_1"));
                    }
                }
            }
        });
        this.facePager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                for (int i = 0; i < BasePublishTopicActivity.this.selects.length; i++) {
                    if (i == position) {
                        BasePublishTopicActivity.this.selects[i].setImageDrawable(BasePublishTopicActivity.this.resource.getDrawable("mc_forum_select2_2"));
                    } else {
                        BasePublishTopicActivity.this.selects[i].setImageDrawable(BasePublishTopicActivity.this.resource.getDrawable("mc_forum_select2_1"));
                    }
                }
            }
        });
        this.facePagerAdapter.setOnFaceImageClickListener(new FacePagerAdapter.OnFaceImageClickListener() {
            public void onFaceImageClick(String tag, Drawable drawable) {
                SpannableStringBuilder spannable = new SpannableStringBuilder(tag);
                spannable.setSpan(new ImageSpan(drawable, 1), 0, tag.length(), 33);
                BasePublishTopicActivity.this.getContentEditText().getText().insert(BasePublishTopicActivity.this.getContentEditText().getSelectionEnd(), spannable);
            }
        });
        this.publishLocationBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BasePublishTopicActivity.this.isLocationSucc) {
                    BasePublishTopicActivity.this.openAnimation.start();
                    BasePublishTopicActivity.this.poiBox.setAnimation(BasePublishTopicActivity.this.openAnimation);
                    if (BasePublishTopicActivity.this.pois == null || BasePublishTopicActivity.this.pois.isEmpty()) {
                        BasePublishTopicActivity.this.poiBox.setVisibility(0);
                        BasePublishTopicActivity.this.poiLoadingContainer.setVisibility(0);
                        BasePublishTopicActivity.this.poiProgressBar.show();
                        return;
                    }
                    BasePublishTopicActivity.this.poiBox.setVisibility(0);
                }
            }
        });
        this.poiList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                BasePublishTopicActivity.this.closeAnimation.start();
                BasePublishTopicActivity.this.poiBox.setAnimation(BasePublishTopicActivity.this.closeAnimation);
                BasePublishTopicActivity.this.poiBox.setVisibility(8);
                BasePublishTopicActivity.this.locationStr = BasePublishTopicActivity.this.baseLocationStr + BasePublishTopicActivity.this.pois.get(position);
                BasePublishTopicActivity.this.publishLocationText.setText(BasePublishTopicActivity.this.locationStr);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void getLocation() {
        if (this.requireLocation == 1) {
            this.requireLocation = 0;
            this.publishLocationBox.setVisibility(8);
            this.locationComplete = true;
            this.takeLocation.setImageResource(this.resource.getDrawableId("mc_forum_ico20_n"));
            return;
        }
        this.requireLocation = 1;
        this.publishLocationBox.setVisibility(0);
        showLoadingBox();
        this.publishLocationText.setText(getResources().getString(this.resource.getStringId("mc_forum_obtain_location_info_warn")));
        if (this.locationUtil.getLocationClient() == null) {
            hideLoadingBox();
            this.publishLocationText.setText(getResources().getString(this.resource.getStringId("mc_forum_location_fail_warn")));
            return;
        }
        this.locationComplete = false;
        this.locationUtil.startLocation();
        this.takeLocation.setImageResource(this.resource.getDrawableId("mc_forum_ico20_h"));
    }

    /* access modifiers changed from: protected */
    public void initExitAlertDialog() {
        this.exitAlertDialog = new AlertDialog.Builder(this).setTitle(this.resource.getStringId("mc_forum_dialog_tip")).setMessage(this.resource.getStringId("mc_forum_warn_publish_title"));
        this.exitAlertDialog.setPositiveButton(this.resource.getStringId("mc_forum_warn_photo_ok"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                BasePublishTopicActivity.this.exitSaveEvent();
            }
        });
        this.exitAlertDialog.setNegativeButton(this.resource.getStringId("mc_forum_warn_photo_cancel"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                BasePublishTopicActivity.this.exitNoSaveEvent();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void updateUIAfterUpload() {
        updateUIAfterUpload(this.iconPath, this.bitmap);
    }

    /* access modifiers changed from: protected */
    public void updateUIAfterUpload(String iconPath, Bitmap bitmap) {
        char lastC;
        Bitmap tBitmap = bitmap;
        this.imageCache.put(iconPath, new WeakReference(tBitmap));
        String content = getContentEditText().getText().toString();
        int p1 = getContentEditText().getSelectionEnd();
        try {
            lastC = content.charAt(p1 - 1);
        } catch (Exception e) {
            lastC = 0;
        }
        if (!(lastC == 0 || lastC == 10)) {
            getContentEditText().getText().insert(p1, "\n");
        }
        String s = "ßá" + iconPath + 223;
        SpannableStringBuilder spannable = new SpannableStringBuilder(s);
        spannable.setSpan(new ImageSpan(this, tBitmap, 1), 0, s.length(), 33);
        getContentEditText().getText().insert(getContentEditText().getSelectionEnd(), spannable);
        getContentEditText().getText().insert(getContentEditText().getSelectionEnd(), "\n");
    }

    /* access modifiers changed from: protected */
    public void getUploadingBitmap(Bitmap bitmap) {
        ImageUtil.compressBitmap(this.compressPath, bitmap, 640, 100, 3, this);
    }

    /* access modifiers changed from: protected */
    public Bitmap getUploadedBitmap(Bitmap bitmap) {
        return ImageUtil.compressBitmap(bitmap, 200, 3, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobcent.forum.android.util.ImageUtil.getBitmapFromMedia(android.content.Context, java.lang.String, float, int):android.graphics.Bitmap
     arg types: [com.mobcent.base.android.ui.activity.BasePublishTopicActivity, java.lang.String, int, int]
     candidates:
      com.mobcent.forum.android.util.ImageUtil.getBitmapFromMedia(android.content.Context, java.lang.String, int, int):android.graphics.Bitmap
      com.mobcent.forum.android.util.ImageUtil.getBitmapFromMedia(android.content.Context, java.lang.String, float, int):android.graphics.Bitmap */
    /* access modifiers changed from: protected */
    public Bitmap getPreviewBitmap(String path) {
        return ImageUtil.getBitmapFromMedia((Context) this, path, 0.5f, 640);
    }

    /* access modifiers changed from: protected */
    public void exitNoSaveEvent() {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        recycleImageCache();
        if (this.locationUtil != null) {
            this.locationUtil.stopLocation();
        }
        this.adView.free();
    }

    private void recycleImageCache() {
        Bitmap bitmap;
        if (this.imageCache != null && !this.imageCache.isEmpty()) {
            for (String key : this.imageCache.keySet()) {
                WeakReference<Bitmap> weakReference = this.imageCache.get(key);
                if (!(weakReference == null || (bitmap = (Bitmap) weakReference.get()) == null || bitmap.isRecycled())) {
                    bitmap.recycle();
                }
            }
            this.imageCache.clear();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.poiBox.getVisibility() == 0) {
            this.poiBox.setVisibility(8);
            this.closeAnimation.start();
            this.poiBox.setAnimation(this.closeAnimation);
            return true;
        } else if (this.faceLayout.getVisibility() == 0) {
            this.faceLayout.setVisibility(8);
            this.chooseFace.setImageResource(this.resource.getDrawableId("mc_forum_icon1"));
            this.keyboardState = 1;
            return true;
        } else if (this.audioLayout.getVisibility() == 0) {
            this.audioLayout.setVisibility(8);
            this.takeVoice.setImageResource(this.resource.getDrawableId("mc_forum_ico31_n"));
            return true;
        } else if (this.setVisibleGroup.getVisibility() != 0) {
            return super.onKeyDown(keyCode, event);
        } else {
            this.setVisibleGroup.setVisibility(8);
            this.setVisible.setImageResource(this.resource.getDrawableId("mc_forum_ico32_n"));
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void changeKeyboardState(int kbState) {
        this.keyboardState = kbState;
        switch (this.keyboardState) {
            case 1:
                this.audioLayout.setVisibility(8);
                this.adView.setVisibility(0);
                this.faceLayout.setVisibility(8);
                this.setVisibleGroup.setVisibility(8);
                this.chooseFace.setImageResource(this.resource.getDrawableId("mc_forum_icon1"));
                this.setVisible.setImageResource(this.resource.getDrawableId("mc_forum_ico32_n"));
                showSoftKeyboard();
                return;
            case 2:
                this.audioLayout.setVisibility(8);
                this.setVisibleGroup.setVisibility(8);
                hideSoftKeyboard();
                this.adView.setVisibility(8);
                this.chooseFace.setImageResource(this.resource.getDrawableId("mc_forum_icon21"));
                this.setVisible.setImageResource(this.resource.getDrawableId("mc_forum_ico32_n"));
                this.faceLayout.setVisibility(0);
                return;
            case 3:
                this.audioLayout.setVisibility(0);
                hideSoftKeyboard();
                this.adView.setVisibility(8);
                this.setVisibleGroup.setVisibility(8);
                this.chooseFace.setImageResource(this.resource.getDrawableId("mc_forum_icon1"));
                this.setVisible.setImageResource(this.resource.getDrawableId("mc_forum_ico32_n"));
                this.faceLayout.setVisibility(8);
                return;
            case 4:
                this.setVisibleGroup.setVisibility(0);
                hideSoftKeyboard();
                this.adView.setVisibility(8);
                this.chooseFace.setImageResource(this.resource.getDrawableId("mc_forum_icon1"));
                this.faceLayout.setVisibility(8);
                return;
            default:
                this.audioLayout.setVisibility(8);
                hideSoftKeyboard();
                this.faceLayout.setVisibility(8);
                this.setVisibleGroup.setVisibility(8);
                this.adView.setVisibility(0);
                this.chooseFace.setImageResource(this.resource.getDrawableId("mc_forum_icon1"));
                this.setVisible.setImageResource(this.resource.getDrawableId("mc_forum_ico32_n"));
                return;
        }
    }

    public List<AdModel> getAdList() {
        return this.adList;
    }

    public void setAdList(List<AdModel> adList2) {
        this.adList = adList2;
    }

    /* access modifiers changed from: protected */
    public EditText getContentEditText() {
        return this.contentEdit;
    }

    /* access modifiers changed from: protected */
    public void initLoadingBox() {
        this.loadingBox = (RelativeLayout) findViewById(this.resource.getViewId("mc_forum_loading_container"));
        this.progressBar = (MCProgressBar) findViewById(this.resource.getViewId("mc_forum_progress_bar"));
    }

    /* access modifiers changed from: protected */
    public void showLoadingBox() {
        this.mHandler.post(new Runnable() {
            public void run() {
                BasePublishTopicActivity.this.loadingBox.setVisibility(0);
                BasePublishTopicActivity.this.progressBar.show();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void hideLoadingBox() {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (BasePublishTopicActivity.this.loadingBox.getVisibility() == 0) {
                    BasePublishTopicActivity.this.loadingBox.setVisibility(8);
                    BasePublishTopicActivity.this.progressBar.hide();
                }
            }
        });
    }
}
