package com.mobcent.android.api;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.mobcent.android.constants.MCShareMobCentApiConstant;
import com.mobcent.android.utils.MCLogUtil;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class MCShareHttpClientUtil {
    public static final String FS = File.separator;
    private static final int SET_CONNECTION_TIMEOUT = 50000;
    private static final int SET_SOCKET_TIMEOUT = 200000;
    private static final String TAG = "MCShareHttpClientUtil";

    /* JADX INFO: Multiple debug info for r11v1 org.apache.http.client.HttpClient: [D('context' android.content.Context), D('client' org.apache.http.client.HttpClient)] */
    /* JADX INFO: Multiple debug info for r11v6 org.apache.http.StatusLine: [D('client' org.apache.http.client.HttpClient), D('status' org.apache.http.StatusLine)] */
    /* JADX INFO: Multiple debug info for r11v7 int: [D('statusCode' int), D('status' org.apache.http.StatusLine)] */
    /* JADX INFO: Multiple debug info for r10v13 java.lang.String: [D('response' org.apache.http.HttpResponse), D('result' java.lang.String)] */
    public static String doPostRequest(String urlString, HashMap<String, String> params, Context context) {
        String result;
        HttpPost httpPost;
        String tempURL;
        String str;
        String tempURL2 = String.valueOf(urlString) + "?";
        HttpClient client = getNewHttpClient(context);
        HttpPost httpPost2 = new HttpPost(urlString);
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            if (params != null && !params.isEmpty()) {
                for (String key : params.keySet()) {
                    nameValuePairs.add(new BasicNameValuePair(key, params.get(key)));
                    tempURL2 = String.valueOf(tempURL2) + key + "=" + params.get(key) + "&";
                }
            }
            MCLogUtil.i(TAG, "tempURL = " + tempURL2);
            httpPost2.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));
            HttpPost httpPost3 = httpPost2;
            try {
                HttpResponse response = client.execute(httpPost3);
                if (response.getStatusLine().getStatusCode() != 200) {
                    return "connection_fail";
                }
                String result2 = read(response);
                try {
                    MCLogUtil.i(TAG, "result = " + result2);
                    if (result2 == null) {
                        str = "{}";
                    } else {
                        str = result2;
                    }
                    return str;
                } catch (Exception e) {
                    tempURL = tempURL2;
                    HttpPost httpPost4 = httpPost3;
                    request = e;
                    result = result2;
                    httpPost = httpPost4;
                    request.printStackTrace();
                    return "connection_fail";
                }
            } catch (Exception e2) {
                tempURL = tempURL2;
                result = "";
                HttpUriRequest httpUriRequest = e2;
                httpPost = httpPost3;
                request = httpUriRequest;
                request.printStackTrace();
                return "connection_fail";
            }
        } catch (Exception e3) {
            request = e3;
            httpPost = null;
            tempURL = tempURL2;
            result = "";
            request.printStackTrace();
            return "connection_fail";
        }
    }

    public static synchronized byte[] getFileInByte(String url, Context context) {
        byte[] bArr;
        synchronized (MCShareHttpClientUtil.class) {
            try {
                byte[] imgData = null;
                try {
                    URL myFileUrl = new URL(url);
                    InputStream is = null;
                    try {
                        HttpURLConnection connection = getNewHttpURLConnection(myFileUrl, context);
                        connection.setDoInput(true);
                        connection.connect();
                        is = connection.getInputStream();
                        int length = connection.getContentLength();
                        if (length != -1) {
                            imgData = new byte[length];
                            byte[] temp = new byte[512];
                            int destPos = 0;
                            while (true) {
                                int readLen = is.read(temp);
                                if (readLen <= 0) {
                                    break;
                                }
                                System.arraycopy(temp, 0, imgData, destPos, readLen);
                                destPos += readLen;
                            }
                            byte[] bArr2 = null;
                        }
                        closeStream(is);
                        bArr = imgData;
                    } catch (IOException e) {
                        closeStream(is);
                        URL url2 = myFileUrl;
                        bArr = null;
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                } catch (Exception e2) {
                    bArr = null;
                }
            } catch (Throwable th2) {
                th = th2;
                throw th;
            }
        }
        return bArr;
    }

    public static void downloadFile(String url, File f, Context context) {
        try {
            MCLogUtil.i(TAG, "Image url is " + url);
            URL myFileUrl = new URL(url);
            try {
                HttpURLConnection connection = getNewHttpURLConnection(myFileUrl, context);
                connection.setDoInput(true);
                connection.connect();
                InputStream is = connection.getInputStream();
                if (f.createNewFile()) {
                    FileOutputStream fo = new FileOutputStream(f);
                    byte[] buffer = new byte[AccessibilityEventCompat.TYPE_VIEW_HOVER_EXIT];
                    while (true) {
                        int size = is.read(buffer);
                        if (size <= 0) {
                            break;
                        }
                        fo.write(buffer, 0, size);
                    }
                    fo.flush();
                    fo.close();
                }
            } catch (MalformedURLException e) {
                MCLogUtil.i(TAG, "URL is format error");
            } catch (IOException e2) {
                MCLogUtil.i(TAG, "IO error when download file");
                MCLogUtil.i(TAG, "The URL is " + url + ";the file name " + f.getName());
            }
            URL url2 = myFileUrl;
        } catch (Exception e3) {
            Exception exc = e3;
        }
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }

    /* JADX INFO: Multiple debug info for r9v2 java.net.HttpURLConnection: [D('context' android.content.Context), D('con' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r7v47 byte[]: [D('bufferSize' int), D('buffer' byte[])] */
    /* JADX INFO: Multiple debug info for r7v54 java.lang.String: [D('is' java.io.InputStream), D('jsonStr' java.lang.String)] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:33:0x0146=Splitter:B:33:0x0146, B:22:0x0128=Splitter:B:22:0x0128} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String uploadFile(java.lang.String r7, java.lang.String r8, android.content.Context r9) {
        /*
            r1 = 0
            java.lang.String r0 = "MCShareHttpClientUtil"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "uploadUrl = "
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r3 = ";uploadFile="
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r2 = r2.toString()
            com.mobcent.android.utils.MCLogUtil.i(r0, r2)
            boolean r0 = com.mobcent.android.utils.MCSharePhoneConnectionUtil.isNetworkAvailable(r9)
            if (r0 != 0) goto L_0x0031
            java.lang.String r7 = "MCShareHttpClientUtil"
            java.lang.String r8 = "connnect fail"
            com.mobcent.android.utils.MCLogUtil.i(r7, r8)
            java.lang.String r7 = "connection_fail"
            r8 = r7
            r7 = r1
        L_0x0030:
            return r8
        L_0x0031:
            java.lang.String r2 = "\r\n"
            java.lang.String r4 = "--"
            java.lang.String r0 = "*****"
            java.lang.String r3 = ""
            if (r8 == 0) goto L_0x0050
            java.lang.String r5 = com.mobcent.android.api.MCShareHttpClientUtil.FS
            int r5 = r8.lastIndexOf(r5)
            r6 = -1
            if (r5 <= r6) goto L_0x0050
            java.lang.String r3 = com.mobcent.android.api.MCShareHttpClientUtil.FS
            int r3 = r8.lastIndexOf(r3)
            int r3 = r3 + 1
            java.lang.String r3 = r8.substring(r3)
        L_0x0050:
            java.net.URL r5 = new java.net.URL     // Catch:{ ClientProtocolException -> 0x0173, SocketTimeoutException -> 0x0132, IOException -> 0x0143, Exception -> 0x0150, all -> 0x015a }
            r5.<init>(r7)     // Catch:{ ClientProtocolException -> 0x0173, SocketTimeoutException -> 0x0132, IOException -> 0x0143, Exception -> 0x0150, all -> 0x015a }
            java.net.HttpURLConnection r9 = getNewHttpURLConnection(r5, r9)     // Catch:{ ClientProtocolException -> 0x0173, SocketTimeoutException -> 0x0132, IOException -> 0x0143, Exception -> 0x0150, all -> 0x015a }
            r7 = 1
            r9.setDoInput(r7)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r7 = 1
            r9.setDoOutput(r7)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r7 = 0
            r9.setUseCaches(r7)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r7 = 180000(0x2bf20, float:2.52234E-40)
            r9.setConnectTimeout(r7)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r7 = 180000(0x2bf20, float:2.52234E-40)
            r9.setReadTimeout(r7)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r7 = "POST"
            r9.setRequestMethod(r7)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r7 = "Connection"
            java.lang.String r1 = "Keep-Alive"
            r9.setRequestProperty(r7, r1)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r7 = "Charset"
            java.lang.String r1 = "UTF-8"
            r9.setRequestProperty(r7, r1)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r7 = "Content-Type"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r5 = "multipart/form-data;boundary="
            r1.<init>(r5)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r1 = r1.toString()     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r9.setRequestProperty(r7, r1)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.io.OutputStream r7 = r9.getOutputStream()     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r1.<init>(r7)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r5 = java.lang.String.valueOf(r4)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r7.<init>(r5)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r7 = r7.toString()     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r1.writeBytes(r7)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r5 = "Content-Disposition: form-data; name=\"file1\";filename=\""
            r7.<init>(r5)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.StringBuilder r7 = r7.append(r3)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r3 = "\""
            java.lang.StringBuilder r7 = r7.append(r3)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r7 = r7.toString()     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r1.writeBytes(r7)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r1.writeBytes(r2)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r3.<init>(r8)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r7 = 1024(0x400, float:1.435E-42)
            byte[] r7 = new byte[r7]     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r8 = -1
        L_0x00e2:
            int r8 = r3.read(r7)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r5 = -1
            if (r8 != r5) goto L_0x0120
            r1.writeBytes(r2)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r8 = java.lang.String.valueOf(r4)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r7.<init>(r8)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.StringBuilder r7 = r7.append(r4)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r7 = r7.toString()     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r1.writeBytes(r7)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r3.close()     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r1.flush()     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.io.InputStream r7 = r9.getInputStream()     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            java.lang.String r7 = convertStreamToString(r7)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r1.close()     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            r9.disconnect()
            r8 = r7
            r7 = r9
            goto L_0x0030
        L_0x0120:
            r5 = 0
            r1.write(r7, r5, r8)     // Catch:{ ClientProtocolException -> 0x0125, SocketTimeoutException -> 0x016f, IOException -> 0x016b, Exception -> 0x0167, all -> 0x0161 }
            goto L_0x00e2
        L_0x0125:
            r7 = move-exception
            r8 = r7
            r7 = r9
        L_0x0128:
            r8.printStackTrace()     // Catch:{ all -> 0x0165 }
            r7.disconnect()
            java.lang.String r8 = "connection_fail"
            goto L_0x0030
        L_0x0132:
            r7 = move-exception
            r8 = r7
            r7 = r1
        L_0x0135:
            java.lang.String r8 = "MCShareHttpClientUtil"
            java.lang.String r9 = "SocketTimeoutException"
            com.mobcent.android.utils.MCLogUtil.i(r8, r9)     // Catch:{ all -> 0x0165 }
            r7.disconnect()
            java.lang.String r8 = "connection_fail"
            goto L_0x0030
        L_0x0143:
            r7 = move-exception
            r8 = r7
            r7 = r1
        L_0x0146:
            r8.printStackTrace()     // Catch:{ all -> 0x0165 }
            r7.disconnect()
            java.lang.String r8 = "connection_fail"
            goto L_0x0030
        L_0x0150:
            r7 = move-exception
            r8 = r7
            r7 = r1
        L_0x0153:
            r7.disconnect()
            java.lang.String r8 = "{rs:0,reason:\"upload_images_fail\"}"
            goto L_0x0030
        L_0x015a:
            r7 = move-exception
            r8 = r7
            r7 = r1
        L_0x015d:
            r7.disconnect()
            throw r8
        L_0x0161:
            r7 = move-exception
            r8 = r7
            r7 = r9
            goto L_0x015d
        L_0x0165:
            r8 = move-exception
            goto L_0x015d
        L_0x0167:
            r7 = move-exception
            r8 = r7
            r7 = r9
            goto L_0x0153
        L_0x016b:
            r7 = move-exception
            r8 = r7
            r7 = r9
            goto L_0x0146
        L_0x016f:
            r7 = move-exception
            r8 = r7
            r7 = r9
            goto L_0x0135
        L_0x0173:
            r7 = move-exception
            r8 = r7
            r7 = r1
            goto L_0x0128
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.android.api.MCShareHttpClientUtil.uploadFile(java.lang.String, java.lang.String, android.content.Context):java.lang.String");
    }

    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                try {
                    is.close();
                } catch (IOException e3) {
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    public static HttpClient getNewHttpClient(Context context) {
        Cursor mCursor;
        try {
            KeyStore.getInstance(KeyStore.getDefaultType()).load(null, null);
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, SET_CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, SET_SOCKET_TIMEOUT);
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, "UTF-8");
            HttpClient client = new DefaultHttpClient(params);
            if (!((WifiManager) context.getSystemService("wifi")).isWifiEnabled() && (mCursor = context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null)) != null && mCursor.moveToFirst()) {
                String proxyStr = mCursor.getString(mCursor.getColumnIndex("proxy"));
                if (proxyStr != null && proxyStr.trim().length() > 0) {
                    client.getParams().setParameter("http.route.default-proxy", new HttpHost(proxyStr, 80));
                }
                mCursor.close();
            }
            return client;
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    /* JADX INFO: Multiple debug info for r1v1 java.lang.String: [D('proxyStr' java.lang.String), D('uri' android.net.Uri)] */
    public static HttpURLConnection getNewHttpURLConnection(URL url, Context context) {
        HttpURLConnection connection;
        Exception e;
        HttpURLConnection connection2;
        Cursor mCursor;
        try {
            HttpURLConnection connection3 = (HttpURLConnection) url.openConnection();
            try {
                if (((WifiManager) context.getSystemService("wifi")).isWifiEnabled() || (mCursor = context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null)) == null || !mCursor.moveToFirst()) {
                    connection2 = connection3;
                } else {
                    String proxyStr = mCursor.getString(mCursor.getColumnIndex("proxy"));
                    if (proxyStr == null || proxyStr.trim().length() <= 0) {
                        connection = connection3;
                    } else {
                        connection = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyStr, 80)));
                    }
                    try {
                        mCursor.close();
                        connection2 = connection;
                    } catch (Exception e2) {
                        e = e2;
                        e.printStackTrace();
                        try {
                            return (HttpURLConnection) url.openConnection();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                            return null;
                        }
                    }
                }
                return connection2;
            } catch (Exception e3) {
                e = e3;
                connection = connection3;
                e.printStackTrace();
                return (HttpURLConnection) url.openConnection();
            }
        } catch (Exception e4) {
            Exception exc = e4;
            connection = null;
            e = exc;
            e.printStackTrace();
            return (HttpURLConnection) url.openConnection();
        }
    }

    private static String read(HttpResponse response) throws Exception {
        try {
            InputStream inputStream = response.getEntity().getContent();
            ByteArrayOutputStream content = new ByteArrayOutputStream();
            Header header = response.getFirstHeader("Content-Encoding");
            if (header != null && header.getValue().toLowerCase().indexOf(MCShareMobCentApiConstant.GZIP) > -1) {
                inputStream = new GZIPInputStream(inputStream);
            }
            byte[] sBuffer = new byte[512];
            while (true) {
                int readBytes = inputStream.read(sBuffer);
                if (readBytes == -1) {
                    return new String(content.toByteArray());
                }
                content.write(sBuffer, 0, readBytes);
            }
        } catch (IllegalStateException e) {
            throw new Exception(e);
        } catch (IOException e2) {
            throw new Exception(e2);
        }
    }
}
