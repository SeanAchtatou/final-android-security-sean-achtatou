package com.mobcent.android.api;

import android.content.Context;
import com.mobcent.android.constants.MCShareMobCentApiConstant;
import com.mobcent.android.utils.MCSharePhoneUtil;
import java.util.HashMap;
import java.util.Locale;

public class MCShareSyncSiteRestfulApiRequester implements MCShareMobCentApiConstant {
    public static String doPostRequest(String urlString, HashMap<String, String> params, String appKey, Context context) {
        params.put("imei", MCSharePhoneUtil.getIMEI(context));
        params.put("imsi", MCSharePhoneUtil.getIMSI(context));
        params.put("packageName", context.getPackageName());
        params.put("appKey", appKey);
        if (params.get("lan") == null) {
            params.put("lan", Locale.getDefault().getLanguage());
        }
        if (params.get("cty") == null) {
            params.put("cty", context.getResources().getConfiguration().locale.getCountry());
        }
        params.put("version", MCSharePhoneUtil.getSDKVersion());
        params.put("ua", MCSharePhoneUtil.getPhoneType());
        params.put("platType", "1");
        return MCShareHttpClientUtil.doPostRequest(urlString, params, context);
    }

    public static String shareInfo(long userId, String content, String picPath, String ids, String shareUrl, String appKey, String domainUrl, Context context) {
        String str;
        String url = String.valueOf(domainUrl) + "share/weibo/shareTo.do";
        HashMap<String, String> params = new HashMap<>();
        params.put("content", content);
        params.put("picPath", picPath);
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCShareMobCentApiConstant.IDS, ids);
        if (shareUrl == null) {
            str = "";
        } else {
            str = shareUrl;
        }
        params.put(MCShareMobCentApiConstant.SHARE_URL, str);
        return doPostRequest(url, params, appKey, context);
    }

    public static String uploadShareImage(long userId, String uploadFile, String appKey, String domainUrl, Context context) {
        return MCShareHttpClientUtil.uploadFile(String.valueOf(domainUrl) + "sdk/action/upload.do?" + "userId" + "=" + userId + "&" + "packageName" + "=" + context.getPackageName() + "&" + "appKey" + "=" + appKey + "&" + MCShareMobCentApiConstant.GZIP + "=false", uploadFile, context);
    }

    public static String getAllSites(String appKey, String domainUrl, String lan, String cty, Context context) {
        String url = String.valueOf(domainUrl) + "share/weibo/wbList.do";
        HashMap<String, String> params = new HashMap<>();
        params.put("lan", lan);
        params.put("cty", cty.replaceAll("_", ""));
        return doPostRequest(url, params, appKey, context);
    }

    public static String unbindSite(long userId, int siteId, String appKey, String domainUrl, Context context) {
        String url = String.valueOf(domainUrl) + "share/wb/ub.do";
        HashMap<String, String> params = new HashMap<>();
        params.put("userId", new StringBuilder(String.valueOf(userId)).toString());
        params.put(MCShareMobCentApiConstant.MARK, new StringBuilder(String.valueOf(siteId)).toString());
        return doPostRequest(url, params, appKey, context);
    }
}
