package com.mobcent.android.model;

import android.content.Context;
import java.util.HashMap;

public class MCShareModel {
    private String appKey;
    private String content;
    private Context context;
    private String downloadUrl;
    private String imageFilePath;
    private String linkUrl;
    private HashMap<String, String> params;
    private String picUrl;
    private String skipUrl;
    private String title;
    private int type;

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public String getAppKey() {
        return this.appKey;
    }

    public void setAppKey(String appKey2) {
        this.appKey = appKey2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public String getLinkUrl() {
        return this.linkUrl;
    }

    public void setLinkUrl(String linkUrl2) {
        this.linkUrl = linkUrl2;
    }

    public String getDownloadUrl() {
        return this.downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl2) {
        this.downloadUrl = downloadUrl2;
    }

    public String getSkipUrl() {
        return this.skipUrl;
    }

    public void setSkipUrl(String skipUrl2) {
        this.skipUrl = skipUrl2;
    }

    public String getPicUrl() {
        return this.picUrl;
    }

    public void setPicUrl(String picUrl2) {
        this.picUrl = picUrl2;
    }

    public String getImageFilePath() {
        return this.imageFilePath;
    }

    public void setImageFilePath(String imageFilePath2) {
        this.imageFilePath = imageFilePath2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public HashMap<String, String> getParams() {
        return this.params;
    }

    public void setParams(HashMap<String, String> params2) {
        this.params = params2;
    }
}
