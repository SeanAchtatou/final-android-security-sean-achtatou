package com.mobcent.android.model;

public class MCShareSyncSiteModel {
    public static final int BINDED = 1;
    public static final int UNBIND = 3;
    public static final int UNSELECT = 2;
    private String bindUrl;
    private int siteId;
    private String siteImage;
    private String siteName;
    private String smsUrl;
    private int state = 3;
    private int userId;

    public int getSiteId() {
        return this.siteId;
    }

    public void setSiteId(int siteId2) {
        this.siteId = siteId2;
    }

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId2) {
        this.userId = userId2;
    }

    public String getSiteName() {
        return this.siteName;
    }

    public void setSiteName(String siteName2) {
        this.siteName = siteName2;
    }

    public String getSiteImage() {
        return this.siteImage;
    }

    public void setSiteImage(String siteImage2) {
        this.siteImage = siteImage2;
    }

    public String getSmsUrl() {
        return this.smsUrl;
    }

    public void setSmsUrl(String smsUrl2) {
        this.smsUrl = smsUrl2;
    }

    public String getBindUrl() {
        return this.bindUrl;
    }

    public void setBindUrl(String bindUrl2) {
        this.bindUrl = bindUrl2;
    }

    public int getState() {
        return this.state;
    }

    public void setState(int state2) {
        this.state = state2;
    }
}
