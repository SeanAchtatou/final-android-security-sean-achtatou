package com.mobcent.android.model;

public class MCShareSiteModel {
    public static final int BINDED = 1;
    public static final int UNBIND = 3;
    public static final int UNSELECT = 2;
    private int bindState = 3;
    private String bindUrl;
    private String rsReason;
    private String shareContent;
    private String shareUrl;
    private int siteId;
    private String siteImage;
    private String siteName;
    private String smsUrl;
    private long userId;

    public int getSiteId() {
        return this.siteId;
    }

    public void setSiteId(int siteId2) {
        this.siteId = siteId2;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
    }

    public String getSiteName() {
        return this.siteName;
    }

    public void setSiteName(String siteName2) {
        this.siteName = siteName2;
    }

    public String getSiteImage() {
        return this.siteImage;
    }

    public void setSiteImage(String siteImage2) {
        this.siteImage = siteImage2;
    }

    public String getSmsUrl() {
        return this.smsUrl;
    }

    public void setSmsUrl(String smsUrl2) {
        this.smsUrl = smsUrl2;
    }

    public String getBindUrl() {
        return this.bindUrl;
    }

    public void setBindUrl(String bindUrl2) {
        this.bindUrl = bindUrl2;
    }

    public int getBindState() {
        return this.bindState;
    }

    public void setBindState(int bindState2) {
        this.bindState = bindState2;
    }

    public String getRsReason() {
        return this.rsReason;
    }

    public void setRsReason(String rsReason2) {
        this.rsReason = rsReason2;
    }

    public String getShareContent() {
        return this.shareContent;
    }

    public void setShareContent(String shareContent2) {
        this.shareContent = shareContent2;
    }

    public String getShareUrl() {
        return this.shareUrl;
    }

    public void setShareUrl(String shareUrl2) {
        this.shareUrl = shareUrl2;
    }
}
