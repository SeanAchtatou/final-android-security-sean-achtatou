package com.mobcent.android.model;

public class MCShareUserSitesModel {
    private String bindSiteListJson;
    private String cty;
    private String lan;
    private String siteListJson;
    private int userId;

    public int getUserId() {
        return this.userId;
    }

    public void setUserId(int userId2) {
        this.userId = userId2;
    }

    public String getLan() {
        return this.lan;
    }

    public void setLan(String lan2) {
        this.lan = lan2;
    }

    public String getCty() {
        return this.cty;
    }

    public void setCty(String cty2) {
        this.cty = cty2;
    }

    public String getSiteListJson() {
        return this.siteListJson;
    }

    public void setSiteListJson(String siteListJson2) {
        this.siteListJson = siteListJson2;
    }

    public String getBindSiteListJson() {
        return this.bindSiteListJson;
    }

    public void setBindSiteListJson(String bindSiteListJson2) {
        this.bindSiteListJson = bindSiteListJson2;
    }
}
