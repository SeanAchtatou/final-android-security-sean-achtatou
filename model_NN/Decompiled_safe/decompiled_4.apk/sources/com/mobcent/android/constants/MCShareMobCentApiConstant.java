package com.mobcent.android.constants;

public interface MCShareMobCentApiConstant {
    public static final String APP_KEY = "appKey";
    public static final String BASE_URL = "baseUrl";
    public static final String BIND_URL = "bindUrl";
    public static final String CONTENT = "content";
    public static final String COUNTRY = "cty";
    public static final String DOMAIN_URL = "domainUrl";
    public static final String GZIP = "gzip";
    public static final String IDS = "ids";
    public static final String IMEI = "imei";
    public static final String IMSI = "imsi";
    public static final String IS_BIND = "bind";
    public static final String LAN = "lan";
    public static final String LIST = "list";
    public static final String MARK = "mark";
    public static final String PACKAGE_NAME = "packageName";
    public static final String PHOTO_BASE_PATH = "image_baseUrl";
    public static final String PHOTO_PATH = "photoPath";
    public static final String PIC_PATH = "picPath";
    public static final String PLAT_TYPE = "platType";
    public static final int PLAT_TYPE_VALUE = 1;
    public static final String RS = "rs";
    public static final String RS_FAIL = "Connection fail, pls try again.";
    public static final String RS_REASON = "reason";
    public static final String RS_SUCC = "rs_succ";
    public static final String SELECT_SITE_IDS = "selectSiteIds";
    public static final String SHARE_CONTENT = "content";
    public static final String SHARE_URL = "shareUrl";
    public static final String SITE_ID = "id";
    public static final String SITE_NAME = "name";
    public static final String STATE = "state";
    public static final String SYS_VERSION = "version";
    public static final String UA = "ua";
    public static final String USER_ID = "userId";
}
