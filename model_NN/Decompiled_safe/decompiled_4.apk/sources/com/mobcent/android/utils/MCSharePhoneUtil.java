package com.mobcent.android.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

public class MCSharePhoneUtil {
    public static String getUserAgent() {
        return getPhoneType();
    }

    public static String getIMEI(Context activity) {
        return ((TelephonyManager) activity.getSystemService("phone")).getDeviceId();
    }

    public static String getIMSI(Context activity) {
        return ((TelephonyManager) activity.getSystemService("phone")).getSubscriberId();
    }

    public static String getNetworkOperatorName(Activity activity) {
        return ((TelephonyManager) activity.getSystemService("phone")).getNetworkOperatorName();
    }

    public static String getPhoneType() {
        String type = Build.MODEL;
        if (type != null) {
            type = type.replaceAll(" ", "");
        }
        return type.trim();
    }

    public static String getDevice() {
        return Build.DEVICE;
    }

    public static String getProduct() {
        return Build.PRODUCT;
    }

    public static String getType() {
        return Build.TYPE;
    }

    public static String getSDKVersion() {
        return Build.VERSION.SDK;
    }

    public static String getResolution(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return String.valueOf(dm.widthPixels) + "x" + dm.heightPixels;
    }

    public static int getDisplayWidth(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public static int getDisplayHeight(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    public static float getDisplayDensity(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.density;
    }

    public static float getDisplayWidthPixels(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.xdpi;
    }

    public static int getDisplayDpi(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.densityDpi;
    }
}
