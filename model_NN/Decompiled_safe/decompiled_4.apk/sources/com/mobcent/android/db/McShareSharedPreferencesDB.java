package com.mobcent.android.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import com.mobcent.android.constants.MCShareMobCentApiConstant;

public class McShareSharedPreferencesDB {
    private static McShareSharedPreferencesDB sharedPreferencesDB = null;
    private String PREFS_FILE = "mc_share.prefs";
    private SharedPreferences prefs = null;

    protected McShareSharedPreferencesDB(Context cxt) {
        try {
            this.prefs = cxt.createPackageContext(cxt.getPackageName(), 2).getSharedPreferences(this.PREFS_FILE, 3);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static McShareSharedPreferencesDB getInstance(Context context) {
        if (sharedPreferencesDB == null) {
            sharedPreferencesDB = new McShareSharedPreferencesDB(context);
        }
        return sharedPreferencesDB;
    }

    public static void newInstance(Context context) {
        sharedPreferencesDB = new McShareSharedPreferencesDB(context);
    }

    public void setShareContet(String content) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString("content", content);
        editor.commit();
    }

    public String getShareContet() {
        return this.prefs.getString("content", "");
    }

    public void setShareUrl(String content) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString(MCShareMobCentApiConstant.SHARE_URL, content);
        editor.commit();
    }

    public String getShareUrl() {
        return this.prefs.getString(MCShareMobCentApiConstant.SHARE_URL, "");
    }

    public void setSelectedSiteIds(String ids) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString(MCShareMobCentApiConstant.SELECT_SITE_IDS, ids);
        editor.commit();
    }

    public String getSelectedSiteIds() {
        return this.prefs.getString(MCShareMobCentApiConstant.SELECT_SITE_IDS, "");
    }
}
