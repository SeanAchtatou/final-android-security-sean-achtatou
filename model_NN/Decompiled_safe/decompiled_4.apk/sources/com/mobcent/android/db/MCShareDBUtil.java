package com.mobcent.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.mobcent.android.db.constant.MCShareDBConstant;
import com.mobcent.android.utils.MCShareSnapshotIOUtil;
import java.io.File;

public class MCShareDBUtil implements MCShareDBConstant {
    private static final String CREATE_SITE_TABLE = "CREATE TABLE IF NOT EXISTS ShareSite(userId Long PRIMARY KEY,lan TEXT,cty TEXT,siteListJson TEXT )";
    private static final String DATABASE_NAME = "mc_share";
    private Context ctx;

    private MCShareDBUtil(Context ctx2) {
        this.ctx = ctx2;
        SQLiteDatabase db = null;
        try {
            db = openDB();
            db.execSQL(CREATE_SITE_TABLE);
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    public static MCShareDBUtil getNewInstance(Context context) {
        return new MCShareDBUtil(context);
    }

    /* access modifiers changed from: protected */
    public SQLiteDatabase openDB() {
        String basePath = String.valueOf(MCShareSnapshotIOUtil.getBaseLocalLocation(this.ctx)) + MCShareSnapshotIOUtil.FS + ".mc" + MCShareSnapshotIOUtil.FS;
        File file = new File(basePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return SQLiteDatabase.openOrCreateDatabase(String.valueOf(basePath) + DATABASE_NAME, (SQLiteDatabase.CursorFactory) null);
    }

    public boolean addOrUpdateSite(long userId, String lan, String cty, String siteListJson) {
        SQLiteDatabase db = null;
        try {
            ContentValues values = new ContentValues();
            values.put("lan", lan);
            values.put("cty", cty);
            values.put(MCShareDBConstant.COLUMN_SITE_JSON, siteListJson);
            db = openDB();
            if (!isRowExisted(MCShareDBConstant.TABLE_SITE, "userId", userId)) {
                db.insertOrThrow(MCShareDBConstant.TABLE_SITE, null, values);
            } else {
                db.update(MCShareDBConstant.TABLE_SITE, values, "userId='" + userId + "'", null);
            }
            if (db != null) {
                db.close();
            }
            return true;
        } catch (Exception e) {
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public String getLocalSite() throws Exception {
        String jsonStr = null;
        SQLiteDatabase db = null;
        Cursor cursor = null;
        try {
            db = openDB();
            cursor = db.rawQuery("SELECT * FROM ShareSite", null);
            while (cursor.moveToNext()) {
                jsonStr = cursor.getString(3);
            }
            if (cursor != null) {
                cursor.close();
            }
            if (db != null) {
                db.close();
            }
            return jsonStr;
        } catch (Exception e) {
            throw e;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }

    public boolean isRowExisted(String table, String column, long id) {
        SQLiteDatabase db = null;
        Cursor c = null;
        try {
            db = openDB();
            c = db.query(table, null, String.valueOf(column) + "=" + id, null, null, null, null);
            boolean isExist = false;
            if (c.getCount() > 0) {
                isExist = true;
            }
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return isExist;
        } catch (Exception e) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            return false;
        } catch (Throwable th) {
            if (c != null) {
                c.close();
            }
            if (db != null) {
                db.close();
            }
            throw th;
        }
    }
}
