package com.mobcent.android.service;

import com.mobcent.android.model.MCShareSiteModel;
import java.util.List;

public interface MCShareSyncSiteService {
    boolean addOrUpdateSite(String str, String str2, String str3, String str4);

    List<MCShareSiteModel> getAllSites(String str, String str2, String str3, String str4);

    List<MCShareSiteModel> getAllSitesByNet(String str, String str2, String str3, String str4);

    String shareInfo(long j, String str, String str2, String str3, String str4, String str5, String str6);

    boolean unbindSite(long j, int i, String str, String str2);

    String uploadImage(long j, String str, String str2, String str3);
}
