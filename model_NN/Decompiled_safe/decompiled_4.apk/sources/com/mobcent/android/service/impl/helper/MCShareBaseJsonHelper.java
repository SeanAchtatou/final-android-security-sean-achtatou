package com.mobcent.android.service.impl.helper;

import com.mobcent.android.constants.MCShareMobCentApiConstant;
import org.json.JSONException;
import org.json.JSONObject;

public class MCShareBaseJsonHelper implements MCShareMobCentApiConstant {
    public static String formJsonRS(String jsonStr) {
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.optInt("rs") == 1) {
                return MCShareMobCentApiConstant.RS_SUCC;
            }
            return jsonObj.optString("reason");
        } catch (JSONException e) {
            return MCShareMobCentApiConstant.RS_FAIL;
        }
    }
}
