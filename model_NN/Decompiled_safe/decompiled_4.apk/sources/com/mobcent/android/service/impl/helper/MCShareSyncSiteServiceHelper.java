package com.mobcent.android.service.impl.helper;

import com.mobcent.android.constants.MCShareMobCentApiConstant;
import com.mobcent.android.model.MCShareSiteModel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MCShareSyncSiteServiceHelper extends MCShareBaseJsonHelper implements MCShareMobCentApiConstant {
    /* JADX INFO: Multiple debug info for r19v9 java.util.List<com.mobcent.android.model.MCShareSiteModel>: [D('model' com.mobcent.android.model.MCShareSiteModel), D('array' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r5v3 java.lang.String: [D('bindUrl' java.lang.String), D('siteObj' org.json.JSONObject)] */
    public static List<MCShareSiteModel> formSiteModels(String jsonStr) {
        List<MCShareSiteModel> siteList = new ArrayList<>();
        List<MCShareSiteModel> bindSiteList = new ArrayList<>();
        List<MCShareSiteModel> unBindSiteList = new ArrayList<>();
        try {
            JSONObject jSONObject = new JSONObject(jsonStr);
            String baseUrl = jSONObject.optString("baseUrl");
            String domainUrl = jSONObject.optString(MCShareMobCentApiConstant.DOMAIN_URL);
            long userId = jSONObject.optLong("userId");
            JSONArray array = jSONObject.optJSONArray("list");
            if (array == null) {
                return null;
            }
            for (int i = 0; i < array.length(); i++) {
                MCShareSiteModel model = new MCShareSiteModel();
                JSONObject siteObj = array.getJSONObject(i);
                int siteId = siteObj.optInt("id");
                String name = siteObj.optString("name");
                String icon = siteObj.optString("picPath");
                boolean isBind = siteObj.optBoolean(MCShareMobCentApiConstant.IS_BIND);
                String bindUrl = siteObj.optString(MCShareMobCentApiConstant.BIND_URL);
                model.setSiteId(siteId);
                model.setUserId(userId);
                model.setSiteName(name);
                model.setSiteImage(String.valueOf(baseUrl) + icon);
                model.setBindUrl(String.valueOf(domainUrl) + bindUrl);
                if (isBind) {
                    model.setBindState(1);
                    bindSiteList.add(model);
                } else {
                    model.setBindState(3);
                    unBindSiteList.add(model);
                }
            }
            siteList.addAll(bindSiteList);
            siteList.addAll(unBindSiteList);
            if (siteList.size() > 0) {
                MCShareSiteModel model2 = (MCShareSiteModel) siteList.get(0);
                model2.setShareContent(jSONObject.optString("content"));
                model2.setShareUrl(jSONObject.optString(MCShareMobCentApiConstant.SHARE_URL));
                siteList.set(0, model2);
            }
            return siteList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String formUploadImageJson(String jsonStr) {
        try {
            JSONObject jsonObj = new JSONObject(jsonStr);
            if (jsonObj.optInt("rs") == 0) {
                return null;
            }
            String baseUrl = jsonObj.optString(MCShareMobCentApiConstant.PHOTO_BASE_PATH);
            return String.valueOf(baseUrl) + jsonObj.optString(MCShareMobCentApiConstant.PHOTO_PATH);
        } catch (JSONException e) {
            return null;
        }
    }

    public static boolean isUnbindSucc(String jsonStr) {
        try {
            if (new JSONObject(jsonStr).optInt("rs") == 1) {
                return true;
            }
            return false;
        } catch (JSONException e) {
            return false;
        }
    }
}
