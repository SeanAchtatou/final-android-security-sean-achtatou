package com.mobcent.android.service.impl;

import android.content.Context;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.android.api.MCShareSyncSiteRestfulApiRequester;
import com.mobcent.android.db.MCShareDBUtil;
import com.mobcent.android.db.McShareSharedPreferencesDB;
import com.mobcent.android.model.MCShareSiteModel;
import com.mobcent.android.service.MCShareSyncSiteService;
import com.mobcent.android.service.impl.helper.MCShareBaseJsonHelper;
import com.mobcent.android.service.impl.helper.MCShareSyncSiteServiceHelper;
import java.util.ArrayList;
import java.util.List;

public class MCShareSyncSiteServiceImpl implements MCShareSyncSiteService {
    private Context context;

    public MCShareSyncSiteServiceImpl(Context context2) {
        this.context = context2;
    }

    public String shareInfo(long userId, String content, String picPath, String ids, String shareUrl, String appKey, String domainUrl) {
        return MCShareBaseJsonHelper.formJsonRS(MCShareSyncSiteRestfulApiRequester.shareInfo(userId, content, picPath, ids, shareUrl, appKey, domainUrl, this.context));
    }

    public String uploadImage(long userId, String uploadFile, String appKey, String domainUrl) {
        return MCShareSyncSiteServiceHelper.formUploadImageJson(MCShareSyncSiteRestfulApiRequester.uploadShareImage(userId, uploadFile, appKey, domainUrl, this.context));
    }

    public List<MCShareSiteModel> getAllSites(String appKey, String domainUrl, String lan, String cty) {
        String jsonStr;
        List<MCShareSiteModel> list = getAllSitesByNet(appKey, domainUrl, lan, cty);
        if (list != null && list.size() > 0) {
            return list;
        }
        try {
            jsonStr = MCShareDBUtil.getNewInstance(this.context).getLocalSite();
        } catch (Exception e) {
            e.printStackTrace();
            jsonStr = null;
        }
        if (jsonStr == null || jsonStr.trim().equals("")) {
            return getAllSitesByNet(appKey, domainUrl, lan, cty);
        }
        return MCShareSyncSiteServiceHelper.formSiteModels(jsonStr);
    }

    /* Debug info: failed to restart local var, previous not found, register: 18 */
    public List<MCShareSiteModel> getAllSitesByNet(String appKey, String domainUrl, String lan, String cty) {
        String jsonStr = MCShareSyncSiteRestfulApiRequester.getAllSites(appKey, domainUrl, lan, cty, this.context);
        List<MCShareSiteModel> list = MCShareSyncSiteServiceHelper.formSiteModels(jsonStr);
        if (list == null || list.isEmpty()) {
            List<MCShareSiteModel> list2 = new ArrayList<>();
            String rs = MCShareBaseJsonHelper.formJsonRS(jsonStr);
            MCShareSiteModel model = new MCShareSiteModel();
            model.setRsReason(rs);
            list2.add(model);
            return list2;
        }
        MCShareDBUtil.getNewInstance(this.context).addOrUpdateSite(list.get(0).getUserId(), lan, cty, jsonStr);
        String ids = McShareSharedPreferencesDB.getInstance(this.context).getSelectedSiteIds();
        if (ids.equals("")) {
            return list;
        }
        String[] id = ids.split(AdApiConstant.RES_SPLIT_COMMA);
        if (id.length <= 0) {
            return list;
        }
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getBindState() == 1) {
                int j = 0;
                while (true) {
                    if (j >= id.length) {
                        break;
                    } else if (list.get(i).getSiteId() == Integer.parseInt(id[j])) {
                        list.get(i).setBindState(1);
                        break;
                    } else {
                        list.get(i).setBindState(2);
                        j++;
                    }
                }
            }
        }
        return list;
    }

    public boolean unbindSite(long userId, int siteId, String appKey, String domainUrl) {
        return MCShareSyncSiteServiceHelper.isUnbindSucc(MCShareSyncSiteRestfulApiRequester.unbindSite(userId, siteId, appKey, domainUrl, this.context));
    }

    public boolean addOrUpdateSite(String appKey, String domainUrl, String lan, String cty) {
        getAllSitesByNet(appKey, domainUrl, lan, cty);
        return true;
    }
}
