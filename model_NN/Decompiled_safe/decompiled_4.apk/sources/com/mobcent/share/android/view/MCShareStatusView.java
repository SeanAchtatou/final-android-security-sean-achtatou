package com.mobcent.share.android.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.ad.android.ui.widget.AdView;
import com.mobcent.android.constants.MCShareMobCentApiConstant;
import com.mobcent.android.db.McShareSharedPreferencesDB;
import com.mobcent.android.model.MCShareSiteModel;
import com.mobcent.android.service.MCShareSyncSiteService;
import com.mobcent.android.service.impl.MCShareSyncSiteServiceImpl;
import com.mobcent.forum.android.constant.AnnoConstant;
import com.mobcent.forum.android.constant.BaseReturnCodeConstant;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.util.StringUtil;
import com.mobcent.share.android.activity.adapter.MCShareSitesAdapter;
import com.mobcent.share.android.activity.utils.MCShareImageHelper;
import com.mobcent.share.android.activity.utils.MCShareResourceUtil;
import com.mobcent.share.android.activity.utils.MCShareStringBundleUtil;
import com.mobcent.share.android.activity.utils.MCShareTouchUtil;
import com.mobcent.share.android.constant.MCShareConstant;
import com.mobcent.share.android.widget.MCShareProgressBar;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MCShareStatusView extends LinearLayout implements MCShareConstant {
    private final int SHARE_BOX_HIDE = 0;
    private final int SHARE_BOX_SHOW = 1;
    private AdView adView;
    /* access modifiers changed from: private */
    public MCShareSitesAdapter adapter;
    private Button addShareToSomeOneBtn;
    private Button addShareToTopicBtn;
    /* access modifiers changed from: private */
    public String appKey;
    private Button backBtn;
    private Bitmap bitmap;
    /* access modifiers changed from: private */
    public EditText contentEditText;
    /* access modifiers changed from: private */
    public String cty;
    /* access modifiers changed from: private */
    public String domainUrl;
    /* access modifiers changed from: private */
    public String downloadUrl;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public String lan;
    /* access modifiers changed from: private */
    public String linkUrl;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler();
    private View mView;
    private LinearLayout mcShareBtnLayout1;
    private LinearLayout mcShareBtnLayout2;
    private ImageView mcShareExpandImage;
    private ImageView mcShareHideImage;
    private ImageView mcShareImageView;
    private ImageView mcShareImageViewBox;
    private RelativeLayout mcShareLoadingBox;
    private Button mcShareMailBtn;
    public TextView mcSharePubTitle;
    private McShareSharedPreferencesDB mcShareSharedPreferencesDB;
    private Button mcShareSmsBtn;
    private Button mcShareWebBtn;
    private TextView powerByText;
    protected MCShareProgressBar progressBar;
    /* access modifiers changed from: private */
    public String shareAppContent;
    private int shareBoxSate = 0;
    private Button shareBtn;
    /* access modifiers changed from: private */
    public int shareContentLength = 0;
    /* access modifiers changed from: private */
    public String shareImageFilePath;
    /* access modifiers changed from: private */
    public String sharePic;
    /* access modifiers changed from: private */
    public String shareType;
    /* access modifiers changed from: private */
    public String shareUrl;
    /* access modifiers changed from: private */
    public GridView siteGridView;
    /* access modifiers changed from: private */
    public List<MCShareSiteModel> sites;
    private String smsUrl;
    /* access modifiers changed from: private */
    public int upperLimit = 140;
    public TextView upperLimitText;
    /* access modifiers changed from: private */
    public long userId = -1;

    public MCShareStatusView(Context context) {
        super(context);
        initWidgets();
        this.sites = new ArrayList();
        this.mcShareSharedPreferencesDB = McShareSharedPreferencesDB.getInstance(context);
    }

    public MCShareStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initWidgets();
        this.sites = new ArrayList();
        this.mcShareSharedPreferencesDB = McShareSharedPreferencesDB.getInstance(context);
    }

    private void initWidgets() {
        this.inflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
        this.mView = this.inflater.inflate(MCShareResourceUtil.getR(getContext(), "layout", "mc_share_status"), (ViewGroup) null);
        addView(this.mView);
        this.mcShareLoadingBox = (RelativeLayout) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareLoadingBox"));
        this.progressBar = (MCShareProgressBar) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareProgressBar"));
        this.mcSharePubTitle = (TextView) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcSharePubTitle"));
        this.backBtn = (Button) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareBackBtn"));
        this.shareBtn = (Button) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcSharePublishBtn"));
        this.mcShareBtnLayout1 = (LinearLayout) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareBtnLayout1"));
        this.mcShareBtnLayout2 = (LinearLayout) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareBtnLayout2"));
        this.mcShareExpandImage = (ImageView) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareExpandImage"));
        this.mcShareHideImage = (ImageView) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareHideImage"));
        this.mcShareSmsBtn = (Button) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareSmsBtn"));
        this.mcShareMailBtn = (Button) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareMailBtn"));
        this.mcShareWebBtn = (Button) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareWebBtn"));
        this.addShareToSomeOneBtn = (Button) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareToBtn"));
        this.addShareToTopicBtn = (Button) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareToTopicBtn"));
        this.contentEditText = (EditText) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareContentEditText"));
        this.upperLimitText = (TextView) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareWordsUpperLimit"));
        this.mcShareImageView = (ImageView) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareImageView"));
        this.mcShareImageViewBox = (ImageView) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareImageViewBox"));
        this.siteGridView = (GridView) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareSitesGridView"));
        this.powerByText = (TextView) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcSharePowerByText"));
        this.adView = (AdView) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mc_ad_box"));
        initWidgetAction();
        this.adView.showAd(1002);
        controllPowerByText();
    }

    private void controllPowerByText() {
        if (SharedPreferencesDB.getInstance(getContext()).getPayStatePowerBy()) {
            this.powerByText.setVisibility(8);
        } else {
            this.powerByText.setVisibility(0);
        }
    }

    private void initWidgetAction() {
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((InputMethodManager) MCShareStatusView.this.getContext().getSystemService("input_method")).hideSoftInputFromWindow(MCShareStatusView.this.contentEditText.getWindowToken(), 0);
                ((Activity) MCShareStatusView.this.getContext()).finish();
            }
        });
        this.mcShareExpandImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCShareStatusView.this.showShareBox();
            }
        });
        this.mcShareHideImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCShareStatusView.this.hideShareBox();
            }
        });
        MCShareTouchUtil.createTouchDelegate(this.mcShareExpandImage, 10, 10, 10, 0);
        MCShareTouchUtil.createTouchDelegate(this.mcShareHideImage, 10, 10, 10, 0);
        this.mcShareSmsBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCShareStatusView.this.shareToSms();
            }
        });
        this.mcShareMailBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCShareStatusView.this.shareToMail();
            }
        });
        this.mcShareWebBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCShareStatusView.this.shareToWeb();
            }
        });
        this.addShareToSomeOneBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MCShareStatusView.this.showSoftKeyboard(MCShareStatusView.this.contentEditText);
                MCShareStatusView.this.contentEditText.getText().delete(MCShareStatusView.this.contentEditText.getSelectionStart(), MCShareStatusView.this.contentEditText.getSelectionEnd());
                String defaultString = " @" + MCShareStatusView.this.getContext().getResources().getString(MCShareResourceUtil.getR(MCShareStatusView.this.getContext(), "string", "mc_share_input_some_one"));
                int selectionStart = MCShareStatusView.this.contentEditText.getSelectionEnd();
                int end = selectionStart + defaultString.length();
                MCShareStatusView.this.contentEditText.getText().insert(selectionStart, defaultString);
                MCShareStatusView.this.contentEditText.setSelection(selectionStart + 2, end);
            }
        });
        this.addShareToTopicBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCShareStatusView.this.showSoftKeyboard(MCShareStatusView.this.contentEditText);
                MCShareStatusView.this.contentEditText.getText().delete(MCShareStatusView.this.contentEditText.getSelectionStart(), MCShareStatusView.this.contentEditText.getSelectionEnd());
                String defaultString = " #" + MCShareStatusView.this.getContext().getResources().getString(MCShareResourceUtil.getR(MCShareStatusView.this.getContext(), "string", "mc_share_input_topic")) + BaseReturnCodeConstant.ERROR_CODE;
                int selectionStart = MCShareStatusView.this.contentEditText.getSelectionEnd();
                int start = selectionStart + 2;
                int end = (defaultString.length() + selectionStart) - 1;
                String s = MCShareStatusView.this.contentEditText.getText().toString();
                if (selectionStart >= s.length()) {
                    MCShareStatusView.this.contentEditText.getText().insert(selectionStart, defaultString);
                } else if (!s.substring(selectionStart, selectionStart + 1).equals(BaseReturnCodeConstant.ERROR_CODE)) {
                    MCShareStatusView.this.contentEditText.getText().insert(selectionStart, defaultString);
                } else {
                    try {
                        MCShareStatusView.this.contentEditText.getText().insert(selectionStart + 1, defaultString);
                        start++;
                        end++;
                    } catch (Exception e) {
                        MCShareStatusView.this.contentEditText.getText().insert(selectionStart, defaultString);
                    }
                }
                MCShareStatusView.this.contentEditText.setSelection(start, end);
            }
        });
        this.shareBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCShareStatusView.this.shareToWeb();
            }
        });
        this.contentEditText.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
                MCShareStatusView.this.mHandler.post(new Runnable() {
                    public void run() {
                        int unused = MCShareStatusView.this.shareContentLength = MCShareStatusView.this.contentEditText.getText().length();
                        if (MCShareStatusView.this.upperLimit - MCShareStatusView.this.shareContentLength < 0) {
                            MCShareStatusView.this.upperLimitText.setTextColor(MCShareResourceUtil.getR(MCShareStatusView.this.getContext(), "color", "mc_share_red"));
                        } else {
                            MCShareStatusView.this.upperLimitText.setTextColor(MCShareResourceUtil.getR(MCShareStatusView.this.getContext(), "color", "mc_share_black"));
                        }
                        MCShareStatusView.this.upperLimitText.setText(MCShareStatusView.this.shareContentLength + "/" + MCShareStatusView.this.upperLimit);
                    }
                });
            }
        });
    }

    public void initData(String appKey2, String lan2, String cty2, String shareUrl2, String smsUrl2, String linkUrl2, String downloadUrl2, String shareAppContent2, String sharePic2, String shareImageFilePath2, String shareType2) {
        this.appKey = appKey2;
        this.lan = lan2;
        this.cty = cty2;
        this.shareUrl = shareUrl2;
        this.smsUrl = smsUrl2;
        this.linkUrl = linkUrl2;
        this.downloadUrl = downloadUrl2;
        this.shareAppContent = shareAppContent2;
        this.sharePic = sharePic2;
        this.shareImageFilePath = shareImageFilePath2;
        this.shareType = shareType2;
        this.domainUrl = getResources().getString(MCShareResourceUtil.getR(getContext(), "string", "mc_share_domain_url"));
        fillView();
        new ObtainBindSiteTask().execute(new Void[0]);
        controllPowerByText();
    }

    private void fillView() {
        if (this.shareImageFilePath == null || "".equals(this.shareImageFilePath)) {
            this.mcShareImageViewBox.setVisibility(4);
            this.mcShareImageView.setVisibility(4);
        } else {
            this.bitmap = MCShareImageHelper.compressBitmap(this.shareImageFilePath, 60.0f, (Activity) getContext());
            if (this.bitmap != null && !this.bitmap.isRecycled()) {
                this.mcShareImageViewBox.setVisibility(0);
                this.mcShareImageView.setVisibility(0);
                this.bitmap = MCShareImageHelper.rotateBitmap(this.bitmap, -15.0f);
                this.mcShareImageView.setImageBitmap(this.bitmap);
            }
        }
        if (this.shareAppContent != null && !"".equals(this.shareAppContent)) {
            this.contentEditText.setText(this.shareAppContent);
            this.shareContentLength = this.shareAppContent.length();
            this.contentEditText.setSelection(this.shareContentLength);
        }
        this.upperLimitText.setText(this.shareContentLength + "/" + this.upperLimit);
    }

    public void showProgressBar() {
        this.mcShareLoadingBox.setVisibility(0);
        this.progressBar.show();
    }

    public void hideProgressBar() {
        this.mcShareLoadingBox.setVisibility(4);
    }

    private class ObtainBindSiteTask extends AsyncTask<Void, Void, List<MCShareSiteModel>> {
        private ObtainBindSiteTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<MCShareSiteModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            MCShareStatusView.this.showProgressBar();
        }

        /* access modifiers changed from: protected */
        public List<MCShareSiteModel> doInBackground(Void... params) {
            return new MCShareSyncSiteServiceImpl(MCShareStatusView.this.getContext()).getAllSites(MCShareStatusView.this.appKey, MCShareStatusView.this.domainUrl, MCShareStatusView.this.lan, MCShareStatusView.this.cty);
        }

        /* Debug info: failed to restart local var, previous not found, register: 9 */
        /* access modifiers changed from: protected */
        public void onPostExecute(List<MCShareSiteModel> result) {
            super.onPostExecute((Object) result);
            MCShareStatusView.this.hideProgressBar();
            List unused = MCShareStatusView.this.sites = result;
            if (MCShareStatusView.this.sites != null && !MCShareStatusView.this.sites.isEmpty()) {
                if (((MCShareSiteModel) MCShareStatusView.this.sites.get(0)).getRsReason() == null || ((MCShareSiteModel) MCShareStatusView.this.sites.get(0)).getRsReason().trim().equals("")) {
                    long unused2 = MCShareStatusView.this.userId = ((MCShareSiteModel) MCShareStatusView.this.sites.get(0)).getUserId();
                    if (MCShareStatusView.this.shareType.equals("shareApp")) {
                        String content = ((MCShareSiteModel) MCShareStatusView.this.sites.get(0)).getShareContent();
                        if (!StringUtil.isEmpty(content)) {
                            String unused3 = MCShareStatusView.this.shareAppContent = content;
                        }
                    } else if (StringUtil.isEmpty(MCShareStatusView.this.shareAppContent)) {
                        String content2 = ((MCShareSiteModel) MCShareStatusView.this.sites.get(0)).getShareContent();
                        if (!StringUtil.isEmpty(content2)) {
                            String unused4 = MCShareStatusView.this.shareAppContent = content2;
                        }
                    }
                    String url = ((MCShareSiteModel) MCShareStatusView.this.sites.get(0)).getShareUrl();
                    if (!StringUtil.isEmpty(url)) {
                        String unused5 = MCShareStatusView.this.downloadUrl = url;
                    }
                    MCShareStatusView.this.contentEditText.setText(MCShareStatusView.this.shareAppContent);
                    MCShareSitesAdapter unused6 = MCShareStatusView.this.adapter = new MCShareSitesAdapter(MCShareStatusView.this.getContext(), MCShareStatusView.this.sites, MCShareStatusView.this.userId, MCShareStatusView.this.mHandler);
                    MCShareStatusView.this.siteGridView.setAdapter((ListAdapter) MCShareStatusView.this.adapter);
                    return;
                }
                Toast.makeText(MCShareStatusView.this.getContext(), ((MCShareSiteModel) MCShareStatusView.this.sites.get(0)).getRsReason(), 1).show();
            }
        }
    }

    private class ShareInfoTask extends AsyncTask<String, Void, String> {
        private List<MCShareSiteModel> selectedSites = null;

        public ShareInfoTask(List<MCShareSiteModel> selectedSites2) {
            this.selectedSites = selectedSites2;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(MCShareStatusView.this.getContext(), MCShareResourceUtil.getR(MCShareStatusView.this.getContext(), "string", "mc_share_sharing"), 1).show();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            String content = params[0];
            MCShareSyncSiteService syncSiteService = new MCShareSyncSiteServiceImpl(MCShareStatusView.this.getContext());
            if ((MCShareStatusView.this.sharePic == null || MCShareStatusView.this.sharePic.trim().equals("")) && MCShareStatusView.this.shareImageFilePath != null && !"".equals(MCShareStatusView.this.shareImageFilePath)) {
                String sharePicTemp = syncSiteService.uploadImage(MCShareStatusView.this.userId, MCShareStatusView.this.shareImageFilePath, MCShareStatusView.this.appKey, MCShareStatusView.this.getResources().getString(MCShareResourceUtil.getR(MCShareStatusView.this.getContext(), "string", "mc_share_domain_image_url")));
                if (sharePicTemp != null) {
                    String unused = MCShareStatusView.this.sharePic = sharePicTemp;
                } else {
                    String unused2 = MCShareStatusView.this.sharePic = "";
                }
            }
            String ids = AdApiConstant.RES_SPLIT_COMMA;
            for (int i = 0; i < this.selectedSites.size(); i++) {
                ids = ids + this.selectedSites.get(i).getSiteId() + AdApiConstant.RES_SPLIT_COMMA;
            }
            String unused3 = MCShareStatusView.this.shareUrl = MCShareStatusView.this.linkUrl + " " + MCShareStatusView.this.downloadUrl;
            return syncSiteService.shareInfo(MCShareStatusView.this.userId, content, MCShareStatusView.this.sharePic, ids, MCShareStatusView.this.shareUrl, MCShareStatusView.this.appKey, MCShareStatusView.this.getResources().getString(MCShareResourceUtil.getR(MCShareStatusView.this.getContext(), "string", "mc_share_domain_url")));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            super.onPostExecute((Object) result);
            if (MCShareMobCentApiConstant.RS_SUCC.equals(result)) {
                String ids = "";
                for (int i = 0; i < this.selectedSites.size(); i++) {
                    if (ids.equals("")) {
                        ids = this.selectedSites.get(i).getSiteId() + "";
                    } else {
                        ids = ids + AdApiConstant.RES_SPLIT_COMMA + this.selectedSites.get(i).getSiteId();
                    }
                }
                McShareSharedPreferencesDB.getInstance(MCShareStatusView.this.getContext()).setSelectedSiteIds(ids);
                Toast.makeText(MCShareStatusView.this.getContext(), MCShareResourceUtil.getR(MCShareStatusView.this.getContext(), "string", "mc_share_share_succ"), 1).show();
                return;
            }
            Toast.makeText(MCShareStatusView.this.getContext(), MCShareResourceUtil.getR(MCShareStatusView.this.getContext(), "string", "mc_share_share_fail"), 1).show();
        }
    }

    public void updatAllSites(MCShareSiteModel siteModel) {
        if (this.sites == null) {
            this.sites = new ArrayList();
        }
        int i = 0;
        while (true) {
            if (i >= this.sites.size()) {
                break;
            }
            MCShareSiteModel model = this.sites.get(i);
            if (model.getSiteId() == siteModel.getSiteId()) {
                this.sites.set(i, model);
                break;
            }
            i++;
        }
        this.adapter.setModels(this.sites);
    }

    /* access modifiers changed from: private */
    public void shareToWeb() {
        String content = this.contentEditText.getText().toString();
        if (this.bitmap == null || this.bitmap.isRecycled()) {
            if (content == null || content.equals("")) {
                this.contentEditText.setHint(MCShareResourceUtil.getR(getContext(), "string", "mc_share_say_something"));
                this.contentEditText.setHintTextColor(MCShareResourceUtil.getR(getContext(), "color", "mc_share_red"));
            } else if (this.upperLimit - this.contentEditText.getText().length() < 0) {
                this.upperLimitText.setText(MCShareStringBundleUtil.resolveString(MCShareResourceUtil.getR(getContext(), "string", "mc_share_publish_words_tip"), new String[]{this.upperLimit + ""}, getContext()));
            } else {
                shareInfo(content);
            }
        } else if (this.upperLimit - this.contentEditText.getText().length() < 0) {
            this.upperLimitText.setText(MCShareStringBundleUtil.resolveString(MCShareResourceUtil.getR(getContext(), "string", "mc_share_publish_words_tip"), new String[]{this.upperLimit + ""}, getContext()));
        } else {
            shareInfo(content);
        }
    }

    /* access modifiers changed from: private */
    public void shareToSms() {
        String content;
        if (this.contentEditText.getText().toString() == null) {
            content = "";
        } else {
            content = this.contentEditText.getText().toString();
        }
        if (this.shareUrl != null && !"".equals(this.shareUrl.trim())) {
            content = content + " " + getResources().getString(MCShareResourceUtil.getR(getContext(), "string", "mc_share_content_addr")) + " " + this.shareUrl;
        }
        if (this.smsUrl != null && !"".equals(this.smsUrl.trim())) {
            content = content + " " + getResources().getString(MCShareResourceUtil.getR(getContext(), "string", "mc_share_content_from")) + " " + this.smsUrl;
        }
        if (this.shareImageFilePath == null || "".equals(this.shareImageFilePath)) {
            Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:"));
            intent.putExtra("sms_body", content);
            try {
                getContext().startActivity(intent);
            } catch (Exception e) {
            }
        } else {
            boolean isMmsActivityExist = true;
            Intent mmsTestIntent = new Intent();
            mmsTestIntent.setClassName("com.android.mms", "com.android.mms.ui.ComposeMessageActivity");
            if (getContext().getPackageManager().resolveActivity(mmsTestIntent, 0) == null) {
                isMmsActivityExist = false;
            }
            Intent intent2 = new Intent("android.intent.action.SEND");
            if (isMmsActivityExist) {
                intent2.setClassName("com.android.mms", "com.android.mms.ui.ComposeMessageActivity");
            }
            intent2.putExtra(AnnoConstant.SUBJECT, content);
            intent2.putExtra("sms_body", content);
            intent2.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(this.shareImageFilePath)));
            intent2.setType("image/jpeg");
            try {
                getContext().startActivity(intent2);
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void shareToMail() {
        String content;
        if (this.contentEditText.getText().toString() == null) {
            content = "";
        } else {
            content = this.contentEditText.getText().toString();
        }
        if (this.shareUrl != null && !"".equals(this.shareUrl.trim())) {
            content = content + " " + getResources().getString(MCShareResourceUtil.getR(getContext(), "string", "mc_share_content_addr")) + " " + this.shareUrl;
        }
        if (this.smsUrl != null && !"".equals(this.smsUrl.trim())) {
            content = content + " " + getResources().getString(MCShareResourceUtil.getR(getContext(), "string", "mc_share_content_from")) + " " + this.smsUrl;
        }
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.putExtra("android.intent.extra.TEXT", content);
        if (this.shareImageFilePath == null || "".equals(this.shareImageFilePath)) {
            emailIntent.setType("text/plain");
        } else {
            emailIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(this.shareImageFilePath)));
            emailIntent.setType("img/jpg");
        }
        try {
            getContext().startActivity(emailIntent);
        } catch (Exception e) {
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    private void shareInfo(String content) {
        this.shareBtn.setEnabled(false);
        List<MCShareSiteModel> selectedSites = getSelectSite();
        if (selectedSites.isEmpty()) {
            Toast.makeText(getContext(), MCShareResourceUtil.getR(getContext(), "string", "mc_share_select_at_least_one"), 1).show();
            this.shareBtn.setEnabled(true);
            return;
        }
        new ShareInfoTask(selectedSites).execute(content);
        ((Activity) getContext()).finish();
    }

    /* access modifiers changed from: private */
    public void showShareBox() {
        if (this.shareBoxSate == 0) {
            this.shareBoxSate = 1;
            this.mcShareBtnLayout1.setVisibility(4);
            this.mcSharePubTitle.setVisibility(4);
            this.mcShareBtnLayout2.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void hideShareBox() {
        if (this.shareBoxSate == 1) {
            this.shareBoxSate = 0;
            this.mcSharePubTitle.setVisibility(0);
            this.mcShareBtnLayout1.setVisibility(0);
            this.mcShareBtnLayout2.setVisibility(4);
        }
    }

    public long getUid() {
        return this.userId;
    }

    public List<MCShareSiteModel> getSites() {
        return this.sites;
    }

    /* access modifiers changed from: private */
    public void showSoftKeyboard(View view) {
        view.requestFocus();
        ((InputMethodManager) getContext().getSystemService("input_method")).showSoftInput(view, 1);
    }

    private List<MCShareSiteModel> getSelectSite() {
        List<MCShareSiteModel> selectSite = new ArrayList<>();
        for (MCShareSiteModel model : this.sites) {
            if (model.getBindState() == 1) {
                selectSite.add(model);
            }
        }
        return selectSite;
    }

    public void recycleBitmap() {
        if (this.bitmap != null && !this.bitmap.isRecycled()) {
            this.bitmap.recycle();
            this.bitmap = null;
        }
    }
}
