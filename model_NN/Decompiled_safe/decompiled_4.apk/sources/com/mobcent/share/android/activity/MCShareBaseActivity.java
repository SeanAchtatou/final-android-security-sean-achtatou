package com.mobcent.share.android.activity;

import android.app.Activity;
import android.os.Handler;
import android.widget.ProgressBar;
import com.mobcent.share.android.activity.utils.MCShareResourceUtil;

public abstract class MCShareBaseActivity extends Activity {
    protected ProgressBar progressBar;

    public abstract Handler getCurrentHandler();

    public void hideProgressBar() {
        getCurrentHandler().post(new Runnable() {
            public void run() {
                MCShareBaseActivity.this.progressBar.setVisibility(8);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void showProgressBar() {
        getCurrentHandler().post(new Runnable() {
            public void run() {
                MCShareBaseActivity.this.progressBar.setVisibility(0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void initProgressBox() {
        this.progressBar = (ProgressBar) findViewById(MCShareResourceUtil.getR(this, "id", "mcShareProgressBar"));
    }
}
