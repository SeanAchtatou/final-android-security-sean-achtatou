package com.mobcent.share.android.activity.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import com.mobcent.android.utils.MCLogUtil;
import com.mobcent.android.utils.MCSharePhoneUtil;
import java.io.File;

public class MCShareImageHelper {
    public static Bitmap compressBitmap(String pathName, float targetWidth, Activity act) {
        int maxW;
        int dip = MCSharePhoneUtil.getDisplayDpi(act);
        if (dip == 120) {
            targetWidth *= 1.0f;
        } else if (dip == 160) {
            targetWidth = (float) (((double) targetWidth) * 0.8d);
        } else if (dip == 240) {
            targetWidth *= 2.0f;
        }
        File file = new File(pathName);
        if (!file.exists() || !file.isFile()) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName, options);
        options.inJustDecodeBounds = false;
        int outh = options.outHeight;
        int outw = options.outWidth;
        if (outh > outw) {
            maxW = outh;
        } else {
            maxW = outw;
        }
        int be = (int) (((float) maxW) / targetWidth);
        MCLogUtil.e("", "be=" + be);
        if (be <= 0) {
            be = 1;
        }
        options.inSampleSize = be;
        return BitmapFactory.decodeFile(pathName, options);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap rotateBitmap(Bitmap bitmap, float degrees) {
        if (bitmap == null) {
            return null;
        }
        if (bitmap.isRecycled()) {
            return null;
        }
        try {
            Matrix matrix = new Matrix();
            matrix.postRotate(degrees);
            Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            if (resizedBitmap != bitmap) {
                bitmap.recycle();
                bitmap = resizedBitmap;
            }
            return bitmap;
        } catch (Exception | OutOfMemoryError e) {
            return null;
        }
    }
}
