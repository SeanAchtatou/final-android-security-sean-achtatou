package com.mobcent.share.android.activity.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.mobcent.android.model.MCShareSiteModel;
import com.mobcent.android.service.impl.MCShareSyncSiteServiceImpl;
import com.mobcent.share.android.activity.MCShareAppActivity;
import com.mobcent.share.android.activity.utils.MCBitmapImageCache;
import com.mobcent.share.android.activity.utils.MCShareResourceUtil;
import java.util.List;

public class MCShareSitesAdapter extends BaseAdapter {
    private LayoutInflater inflater = ((LayoutInflater) this.mContext.getSystemService("layout_inflater"));
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private List<MCShareSiteModel> models;
    /* access modifiers changed from: private */
    public long uid;

    public MCShareSitesAdapter(Context context, List<MCShareSiteModel> models2, long uid2, Handler mHandler2) {
        this.mContext = context;
        this.mHandler = mHandler2;
        this.models = models2;
        this.uid = uid2;
    }

    public int getCount() {
        return this.models.size();
    }

    public MCShareSiteModel getItem(int position) {
        return this.models.get(position);
    }

    public long getItemId(int position) {
        return (long) this.models.get(position).getSiteId();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final MCShareSiteModel shareModel = this.models.get(position);
        View convertView2 = this.inflater.inflate(MCShareResourceUtil.getR(this.mContext, "layout", "mc_share_widget_site_grid_item"), (ViewGroup) null);
        final MCShareAppActivity activity = (MCShareAppActivity) this.mContext;
        final ImageView siteBtn = (ImageView) convertView2.findViewById(MCShareResourceUtil.getR(this.mContext, "id", "mcShareSiteBtn"));
        final ImageView siteSelectedIndicator = (ImageView) convertView2.findViewById(MCShareResourceUtil.getR(this.mContext, "id", "mcShareSiteSelectedIndicator"));
        ((TextView) convertView2.findViewById(MCShareResourceUtil.getR(this.mContext, "id", "mcShareSiteNameText"))).setText(shareModel.getSiteName());
        MCBitmapImageCache.getInstance(this.mContext, MCShareAppActivity.TAG).loadAsync(MCBitmapImageCache.formatUrl(shareModel.getSiteImage(), "320x480"), new MCBitmapImageCache.BitmapImageCallback() {
            public void onImageLoaded(final Bitmap image, String url) {
                MCShareSitesAdapter.this.mHandler.post(new Runnable() {
                    public void run() {
                        if (image == null || image.isRecycled()) {
                            siteBtn.setBackgroundResource(MCShareResourceUtil.getR(MCShareSitesAdapter.this.mContext, "drawable", "mc_share_to_default"));
                        } else {
                            siteBtn.setBackgroundDrawable(new BitmapDrawable(image));
                        }
                        MCShareSitesAdapter.this.updateSiteStateUI(shareModel, siteBtn, siteSelectedIndicator);
                    }
                });
            }
        });
        siteBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (shareModel.getBindState() == 1) {
                    shareModel.setBindState(2);
                    activity.getMcShareStatus().updatAllSites(shareModel);
                    MCShareSitesAdapter.this.updateSiteStateUI(shareModel, siteBtn, siteSelectedIndicator);
                } else if (shareModel.getBindState() == 2) {
                    activity.getMcShareStatus().updatAllSites(shareModel);
                    shareModel.setBindState(1);
                    MCShareSitesAdapter.this.updateSiteStateUI(shareModel, siteBtn, siteSelectedIndicator);
                } else if (shareModel.getBindState() == 3) {
                    activity.getMcShareWeb().cleanView();
                    activity.getMcShareWeb().initData(shareModel.getBindUrl(), shareModel.getSiteId(), activity.getMcShareStatus().getUid(), activity.getAppKey(), shareModel, MCShareSitesAdapter.this, activity.getMcShareStatus());
                    activity.showBindSite();
                }
            }
        });
        siteBtn.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                if (shareModel.getBindState() == 1 || shareModel.getBindState() == 2) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MCShareSitesAdapter.this.mContext).setTitle(MCShareResourceUtil.getR(MCShareSitesAdapter.this.mContext, "string", "mc_share_tips")).setMessage(MCShareResourceUtil.getR(MCShareSitesAdapter.this.mContext, "string", "mc_share_unbind_tips"));
                    builder.setPositiveButton(MCShareResourceUtil.getR(MCShareSitesAdapter.this.mContext, "string", "mc_share_confirm"), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            new Thread() {
                                public void run() {
                                    final boolean isSucc = new MCShareSyncSiteServiceImpl(MCShareSitesAdapter.this.mContext).unbindSite(MCShareSitesAdapter.this.uid, shareModel.getSiteId(), activity.getAppKey(), MCShareSitesAdapter.this.mContext.getResources().getString(MCShareResourceUtil.getR(MCShareSitesAdapter.this.mContext, "string", "mc_share_domain_url")));
                                    MCShareSitesAdapter.this.mHandler.post(new Runnable() {
                                        public void run() {
                                            if (isSucc) {
                                                Toast.makeText(MCShareSitesAdapter.this.mContext, MCShareResourceUtil.getR(MCShareSitesAdapter.this.mContext, "string", "mc_share_unbind_succ"), 1).show();
                                                shareModel.setBindState(3);
                                                activity.getMcShareStatus().updatAllSites(shareModel);
                                                MCShareSitesAdapter.this.updateSiteStateUI(shareModel, siteBtn, siteSelectedIndicator);
                                                return;
                                            }
                                            Toast.makeText(MCShareSitesAdapter.this.mContext, MCShareResourceUtil.getR(MCShareSitesAdapter.this.mContext, "string", "mc_share_unbind_fail"), 1).show();
                                        }
                                    });
                                }
                            }.start();
                        }
                    });
                    builder.setNegativeButton(MCShareResourceUtil.getR(MCShareSitesAdapter.this.mContext, "string", "mc_share_cancel"), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    builder.show();
                    return false;
                } else if (shareModel.getBindState() != 3) {
                    return false;
                } else {
                    activity.getMcShareWeb().cleanView();
                    activity.getMcShareWeb().initData(shareModel.getBindUrl(), shareModel.getSiteId(), activity.getMcShareStatus().getUid(), activity.getAppKey(), shareModel, MCShareSitesAdapter.this, activity.getMcShareStatus());
                    activity.showBindSite();
                    return false;
                }
            }
        });
        convertView2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                siteBtn.performClick();
            }
        });
        return convertView2;
    }

    public void updateSiteStateUI(MCShareSiteModel shareModel, ImageView siteBtn, ImageView siteSelectedIndicator) {
        if (shareModel.getBindState() == 1) {
            siteSelectedIndicator.setVisibility(0);
            Drawable bg = siteBtn.getBackground();
            bg.setAlpha(255);
            siteBtn.setBackgroundDrawable(bg);
        } else if (shareModel.getBindState() == 2) {
            siteSelectedIndicator.setVisibility(4);
            Drawable bg2 = siteBtn.getBackground();
            bg2.setAlpha(255);
            siteBtn.setBackgroundDrawable(bg2);
        } else if (shareModel.getBindState() == 3) {
            siteSelectedIndicator.setVisibility(4);
            Drawable bg3 = siteBtn.getBackground();
            bg3.setAlpha(72);
            siteBtn.setBackgroundDrawable(bg3);
        }
    }

    public List<MCShareSiteModel> getModels() {
        return this.models;
    }

    public void setModels(List<MCShareSiteModel> models2) {
        this.models = models2;
    }
}
