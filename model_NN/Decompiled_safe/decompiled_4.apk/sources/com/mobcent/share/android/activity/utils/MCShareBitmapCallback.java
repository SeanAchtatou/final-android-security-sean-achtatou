package com.mobcent.share.android.activity.utils;

import android.graphics.Bitmap;

public interface MCShareBitmapCallback {
    void imageLoaded(Bitmap bitmap, String str);
}
