package com.mobcent.share.android.activity.utils;

import android.content.Context;

public class MCShareStringBundleUtil {
    public static String resolveString(int stringSource, String[] args, Context context) {
        String string = context.getResources().getString(stringSource);
        for (int i = 0; i < args.length; i++) {
            string = string.replace("{" + i + "}", args[i]);
        }
        return string;
    }
}
