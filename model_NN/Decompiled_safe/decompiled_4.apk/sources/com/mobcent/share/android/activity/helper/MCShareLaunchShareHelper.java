package com.mobcent.share.android.activity.helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.widget.Toast;
import com.mobcent.android.constants.MCShareMobCentApiConstant;
import com.mobcent.android.model.MCShareModel;
import com.mobcent.android.utils.MCShareSnapshotIOUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;
import com.mobcent.share.android.activity.MCShareAppActivity;
import com.mobcent.share.android.activity.utils.MCShareResourceUtil;
import com.mobcent.share.android.constant.MCShareConstant;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXImageObject;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXTextObject;
import com.tencent.mm.sdk.openapi.WXWebpageObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

public class MCShareLaunchShareHelper {
    public static final String LOCAL_POSITION_DIR = (File.separator + "mobcent" + File.separator + MCShareConstant.EXHIBITION_SHARE + File.separator);
    private static final int THUMB_SIZE = 150;

    public static void shareAppContent(String appkey, String content, String shareUrl, String smsUrl, Context context) {
        Intent intent = new Intent(context, MCShareAppActivity.class);
        intent.putExtra("appKey", appkey);
        intent.putExtra("shareContent", content);
        intent.putExtra(MCShareMobCentApiConstant.SHARE_URL, shareUrl);
        intent.putExtra("smsUrl", smsUrl);
        intent.putExtra("shareType", "shareApp");
        context.startActivity(intent);
    }

    public static void shareContent(String appkey, String content, String shareUrl, String smsUrl, Context context) {
        Intent intent = new Intent(context, MCShareAppActivity.class);
        intent.putExtra("appKey", appkey);
        intent.putExtra("shareContent", content);
        intent.putExtra(MCShareMobCentApiConstant.SHARE_URL, shareUrl);
        intent.putExtra("smsUrl", smsUrl);
        context.startActivity(intent);
    }

    public static void shareContentWithImageUrl(String appkey, String content, String picUrl, String shareUrl, String smsUrl, Context context) {
        Intent intent = new Intent(context, MCShareAppActivity.class);
        intent.putExtra("appKey", appkey);
        intent.putExtra("shareContent", content);
        MCLogUtil.e("sharePic", picUrl);
        intent.putExtra("sharePic", picUrl);
        intent.putExtra(MCShareMobCentApiConstant.SHARE_URL, shareUrl);
        intent.putExtra("smsUrl", smsUrl);
        context.startActivity(intent);
    }

    public static void shareContentWithBitmap(String appkey, String content, Bitmap shareBitmap, String shareUrl, String smsUrl, Context context) {
        if (shareBitmap != null) {
            MCShareSnapshotIOUtil.saveImageFile(context, shareBitmap, Bitmap.CompressFormat.JPEG, LOCAL_POSITION_DIR + "mcshareimage.jpg", MCShareSnapshotIOUtil.getBaseLocalLocation(context), 90);
            Intent intent = new Intent(context, MCShareAppActivity.class);
            intent.putExtra("appKey", appkey);
            intent.putExtra("shareContent", content);
            intent.putExtra("sharePic", "");
            intent.putExtra(MCShareMobCentApiConstant.SHARE_URL, shareUrl);
            intent.putExtra("smsUrl", smsUrl);
            intent.putExtra("shareImageFilePath", getImageSharePath(context));
            context.startActivity(intent);
            return;
        }
        Toast.makeText(context, MCShareResourceUtil.getR(context, "string", "mc_share_image_empty"), 0).show();
    }

    public static void shareContentWithBitmapAndUrl(String appkey, String content, Bitmap shareBitmap, String picUrl, String shareUrl, String smsUrl, Context context) {
        if (shareBitmap != null) {
            MCShareSnapshotIOUtil.saveImageFile(context, shareBitmap, Bitmap.CompressFormat.JPEG, LOCAL_POSITION_DIR + "mcshareimage.jpg", MCShareSnapshotIOUtil.getBaseLocalLocation(context), 90);
            Intent intent = new Intent(context, MCShareAppActivity.class);
            intent.putExtra("appKey", appkey);
            intent.putExtra("shareContent", content);
            intent.putExtra("sharePic", picUrl);
            intent.putExtra(MCShareMobCentApiConstant.SHARE_URL, shareUrl);
            intent.putExtra("smsUrl", smsUrl);
            intent.putExtra("shareImageFilePath", getImageSharePath(context));
            context.startActivity(intent);
            return;
        }
        Toast.makeText(context, MCShareResourceUtil.getR(context, "string", "mc_share_image_empty"), 0).show();
    }

    public static void shareContentWithImageFile(String appkey, String content, String imageFilePath, String shareUrl, String smsUrl, Context context) {
        File file = new File(imageFilePath);
        if (!file.exists() || !file.isFile()) {
            Toast.makeText(context, MCShareResourceUtil.getR(context, "string", "mc_share_image_empty"), 0).show();
            return;
        }
        Intent intent = new Intent(context, MCShareAppActivity.class);
        intent.putExtra("appKey", appkey);
        intent.putExtra("shareContent", content);
        intent.putExtra("sharePic", "");
        intent.putExtra(MCShareMobCentApiConstant.SHARE_URL, shareUrl);
        intent.putExtra("smsUrl", smsUrl);
        intent.putExtra("shareImageFilePath", imageFilePath);
        context.startActivity(intent);
    }

    public static void shareContentWithImageFileAndUrl(String appkey, String content, String imageFilePath, String picUrl, String shareUrl, String smsUrl, Context context) {
        File file = new File(imageFilePath);
        if (!file.exists() || !file.isFile()) {
            Toast.makeText(context, MCShareResourceUtil.getR(context, "string", "mc_share_image_empty"), 0).show();
            return;
        }
        Intent intent = new Intent(context, MCShareAppActivity.class);
        intent.putExtra("appKey", appkey);
        intent.putExtra("shareContent", content);
        intent.putExtra("sharePic", picUrl);
        intent.putExtra(MCShareMobCentApiConstant.SHARE_URL, shareUrl);
        intent.putExtra("smsUrl", smsUrl);
        intent.putExtra("shareImageFilePath", imageFilePath);
        context.startActivity(intent);
    }

    public static void shareWay(boolean isHasImg, String appKey, String content, String imageFilePath, String picUrl, String shareUrl, String smsUrl, Context context, IWXAPI api) {
        final String[] shareItems = {context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_wx")), context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_friend")), context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_platform"))};
        final boolean z = isHasImg;
        final String str = picUrl;
        final String str2 = imageFilePath;
        final String str3 = appKey;
        final String str4 = content;
        final String str5 = shareUrl;
        final String str6 = smsUrl;
        final Context context2 = context;
        final IWXAPI iwxapi = api;
        new AlertDialog.Builder(context).setItems(shareItems, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (!z || StringUtil.isEmpty(str)) {
                    MCShareLaunchShareHelper.shareContentTo(shareItems[which], str3, str4, str5, str6, context2, iwxapi);
                } else if (!StringUtil.isEmpty(str2)) {
                    MCShareLaunchShareHelper.shareContentWithImageFileAndUrlTo(shareItems[which], str3, str4, str2, str, str5, str6, context2, iwxapi);
                } else {
                    MCShareLaunchShareHelper.shareContentWithImageUrlTo(shareItems[which], str3, str4, str, str5, str6, context2, iwxapi);
                }
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public static void shareContentWithImageFileAndUrlTo(String type, String appKey, String content, String imageFilePath, String picUrl, String shareUrl, String smsUrl, Context context, IWXAPI api) {
        if (!context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_wx")).equals(type)) {
            if (context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_friend")).equals(type)) {
                if (api != null) {
                    shareContentWithImageFileAndUrlToWF(content, imageFilePath, picUrl, shareUrl, smsUrl, context, api, "friend");
                }
            } else if (context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_platform")).equals(type)) {
                shareContentWithImageFileAndUrl(appKey, content, imageFilePath, picUrl, shareUrl, smsUrl, context);
            }
        } else if (api != null) {
            shareContentWithImageFileAndUrlToWF(content, imageFilePath, picUrl, shareUrl, smsUrl, context, api, "wx");
        }
    }

    /* access modifiers changed from: private */
    public static void shareContentTo(String type, String appKey, String content, String shareUrl, String smsUrl, Context context, IWXAPI api) {
        if (context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_wx")).equals(type)) {
            if (api != null) {
                shareContentToWF(content, shareUrl, smsUrl, context, api, "wx");
            }
        } else if (context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_friend")).equals(type)) {
            if (api != null) {
                shareContentToWF(content, shareUrl, smsUrl, context, api, "friend");
            }
        } else if (context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_platform")).equals(type)) {
            shareContent(appKey, content, shareUrl, "", context);
        }
    }

    /* access modifiers changed from: private */
    public static void shareContentWithImageUrlTo(String type, String appKey, String content, String picUrl, String shareUrl, String smsUrl, Context context, IWXAPI api) {
        if (context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_wx")).equals(type)) {
            if (api != null) {
                shareContentWithImageUrlToWF(content, picUrl, shareUrl, smsUrl, context, api, "wx");
            }
        } else if (context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_friend")).equals(type)) {
            if (api != null) {
                shareContentWithImageUrlToWF(content, picUrl, shareUrl, smsUrl, context, api, "friend");
            }
        } else if (context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_platform")).equals(type)) {
            shareContentWithImageUrl(appKey, content, picUrl, shareUrl, "", context);
        }
    }

    public static void shareContentWithImageFileAndUrlToWF(String content, String imageFilePath, String picUrl, String shareUrl, String smsUrl, Context context, IWXAPI api, String type) {
        if (!new File(imageFilePath).exists()) {
            Toast.makeText(context, MCShareResourceUtil.getR(context, "string", "mc_share_image_empty"), 0).show();
            return;
        }
        WXImageObject imgObj = new WXImageObject();
        imgObj.setImagePath(imageFilePath);
        WXMediaMessage msg = new WXMediaMessage();
        msg.mediaObject = imgObj;
        msg.thumbData = getBitmapBytes(BitmapFactory.decodeFile(imageFilePath), true);
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("img");
        req.message = msg;
        if ("friend".equals(type)) {
            req.scene = 1;
        } else if ("wx".equals(type)) {
            req.scene = 0;
        }
        api.sendReq(req);
    }

    public static void shareContentToWF(String content, String shareUrl, String smsUrl, Context context, IWXAPI api, String type) {
        WXTextObject textObj = new WXTextObject();
        textObj.text = content;
        WXMediaMessage msg = new WXMediaMessage();
        msg.mediaObject = textObj;
        msg.description = content;
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("text");
        req.message = msg;
        if ("friend".equals(type)) {
            req.scene = 1;
        } else if ("wx".equals(type)) {
            req.scene = 0;
        }
        api.sendReq(req);
    }

    public static void shareContentWithImageUrlToWF(String content, String picUrl, String shareUrl, String smsUrl, Context context, IWXAPI api, String type) {
        WXWebpageObject webpage = new WXWebpageObject();
        webpage.webpageUrl = picUrl;
        WXMediaMessage msg = new WXMediaMessage(webpage);
        msg.title = content;
        try {
            msg.thumbData = getBitmapBytes(BitmapFactory.decodeStream(new URL(picUrl).openStream()), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = buildTransaction("webpage");
        req.message = msg;
        if ("friend".equals(type)) {
            req.scene = 1;
        } else if ("wx".equals(type)) {
            req.scene = 0;
        }
        api.sendReq(req);
    }

    public static void share(MCShareModel shareModel) {
        share(shareModel.getContext(), shareModel.getAppKey(), shareModel.getTitle(), shareModel.getContent(), shareModel.getLinkUrl(), shareModel.getDownloadUrl(), shareModel.getSkipUrl(), shareModel.getPicUrl(), shareModel.getImageFilePath(), shareModel.getType(), shareModel.getParams());
    }

    public static void share(Context context, String appKey, String title, String content, String linkUrl, String downloadUrl, String skipUrl, String picUrl, String imageFilePath, int type, HashMap<String, String> params) {
        if (!StringUtil.isEmpty(SharedPreferencesDB.getInstance(context).getWeiXinAppKey())) {
            final String[] shareItems = {context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_wx")), context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_friend")), context.getString(MCShareResourceUtil.getR(context, "string", "mc_forum_share_to_platform"))};
            final IWXAPI api = WXAPIFactory.createWXAPI(context, SharedPreferencesDB.getInstance(context).getWeiXinAppKey());
            final String skipUrlStr = getSkipUrlByType(context, skipUrl, type, params);
            final Context context2 = context;
            final String str = appKey;
            final String str2 = content;
            final String str3 = linkUrl;
            final String str4 = downloadUrl;
            final String str5 = picUrl;
            final String str6 = imageFilePath;
            final int i = type;
            final String str7 = title;
            new AlertDialog.Builder(context).setItems(shareItems, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (context2.getString(MCShareResourceUtil.getR(context2, "string", "mc_forum_share_to_platform")).equals(shareItems[which])) {
                        MCShareLaunchShareHelper.shareToPlatform(context2, str, str2, str3, str4, str5, str6, i);
                    } else if (context2.getString(MCShareResourceUtil.getR(context2, "string", "mc_forum_share_to_wx")).equals(shareItems[which])) {
                        MCShareLaunchShareHelper.shareToWeiXinOrFriend(context2, str7, str2, skipUrlStr, str5, str6, i, "wx", api);
                    } else if (context2.getString(MCShareResourceUtil.getR(context2, "string", "mc_forum_share_to_friend")).equals(shareItems[which])) {
                        MCShareLaunchShareHelper.shareToWeiXinOrFriend(context2, str7, str2, skipUrlStr, str5, str6, i, "friend", api);
                    }
                }
            }).show();
            return;
        }
        shareToPlatform(context, appKey, content, linkUrl, downloadUrl, picUrl, imageFilePath, type);
    }

    private static String getSkipUrlByType(Context context, String skipUrl, int type, HashMap<String, String> params) {
        if (type == 2 || type == 5) {
            if (StringUtil.isEmpty(skipUrl)) {
                skipUrl = context.getString(MCShareResourceUtil.getR(context, "string", "mc_share_skip_url")) + "?";
            }
            if (params != null && !params.isEmpty()) {
                for (String key : params.keySet()) {
                    skipUrl = skipUrl + key + "=" + params.get(key) + "&";
                }
            }
        }
        MCLogUtil.e("MCShareLaunchShareHelper", "skipUrl = " + skipUrl);
        return skipUrl;
    }

    /* access modifiers changed from: private */
    public static void shareToWeiXinOrFriend(Context context, String title, String content, String skipUrl, String picUrl, String imageFilePath, int type, String name, IWXAPI api) {
        switch (type) {
            case 1:
                sharePicture(context, picUrl, imageFilePath, name, api);
                return;
            case 2:
                shareFormal(context, title, content, skipUrl, picUrl, imageFilePath, name, api);
                return;
            case 3:
                if (StringUtil.isEmpty(skipUrl) == 0) {
                    shareMusic(context, title, content, skipUrl, picUrl, imageFilePath, name, api);
                    return;
                } else {
                    shareFormal(context, title, content, skipUrl, picUrl, imageFilePath, name, api);
                    return;
                }
            case 4:
                if (StringUtil.isEmpty(skipUrl) == 0) {
                    shareVideo(context, title, content, skipUrl, picUrl, imageFilePath, name, api);
                    return;
                } else {
                    shareFormal(context, title, content, skipUrl, picUrl, imageFilePath, name, api);
                    return;
                }
            case 5:
                shareFormal(context, title, content, skipUrl, picUrl, imageFilePath, name, api);
                return;
            default:
                return;
        }
    }

    private static void sharePicture(Context context, String picUrl, String imageFilePath, String name, IWXAPI api) {
        Exception e;
        WXMediaMessage msg;
        WXMediaMessage msg2;
        if (imageFilePath == null) {
            imageFilePath = "";
        }
        try {
            File file = new File(imageFilePath);
            if (file.exists()) {
                WXImageObject imgObj = new WXImageObject();
                imgObj.setImagePath(imageFilePath);
                msg = new WXMediaMessage();
                try {
                    msg.mediaObject = imgObj;
                    msg.thumbData = getBitmapBytes(BitmapFactory.decodeFile(imageFilePath), true);
                    msg2 = msg;
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                }
            } else if (!file.exists() && !StringUtil.isEmpty(picUrl)) {
                WXImageObject imgObj2 = new WXImageObject();
                imgObj2.imageUrl = picUrl;
                msg = new WXMediaMessage();
                msg.mediaObject = imgObj2;
                msg.thumbData = getBitmapBytes(BitmapFactory.decodeStream(new URL(picUrl).openStream()), true);
                msg2 = msg;
            } else {
                return;
            }
            SendMessageToWX.Req req = new SendMessageToWX.Req();
            req.transaction = buildTransaction("img");
            req.message = msg2;
            if ("friend".equals(name)) {
                req.scene = 1;
            } else if ("wx".equals(name)) {
                req.scene = 0;
            }
            api.sendReq(req);
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void shareFormal(android.content.Context r8, java.lang.String r9, java.lang.String r10, java.lang.String r11, java.lang.String r12, java.lang.String r13, java.lang.String r14, com.tencent.mm.sdk.openapi.IWXAPI r15) {
        /*
            r7 = 1
            com.tencent.mm.sdk.openapi.WXWebpageObject r3 = new com.tencent.mm.sdk.openapi.WXWebpageObject
            r3.<init>()
            r3.webpageUrl = r11
            com.tencent.mm.sdk.openapi.WXMediaMessage r1 = new com.tencent.mm.sdk.openapi.WXMediaMessage
            r1.<init>(r3)
            boolean r4 = com.mobcent.forum.android.util.StringUtil.isEmpty(r9)
            if (r4 != 0) goto L_0x0015
            r1.title = r9
        L_0x0015:
            boolean r4 = com.mobcent.forum.android.util.StringUtil.isEmpty(r10)
            if (r4 != 0) goto L_0x001d
            r1.description = r10
        L_0x001d:
            if (r13 != 0) goto L_0x0021
            java.lang.String r13 = ""
        L_0x0021:
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x0075 }
            r0.<init>(r13)     // Catch:{ IOException -> 0x0075 }
            boolean r4 = r0.exists()     // Catch:{ IOException -> 0x0075 }
            if (r4 != 0) goto L_0x0063
            boolean r4 = com.mobcent.forum.android.util.StringUtil.isEmpty(r12)     // Catch:{ IOException -> 0x0075 }
            if (r4 != 0) goto L_0x0063
            java.net.URL r4 = new java.net.URL     // Catch:{ IOException -> 0x0075 }
            r4.<init>(r12)     // Catch:{ IOException -> 0x0075 }
            java.io.InputStream r4 = r4.openStream()     // Catch:{ IOException -> 0x0075 }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeStream(r4)     // Catch:{ IOException -> 0x0075 }
            r5 = 1
            byte[] r4 = getBitmapBytes(r4, r5)     // Catch:{ IOException -> 0x0075 }
            r1.thumbData = r4     // Catch:{ IOException -> 0x0075 }
        L_0x0046:
            com.tencent.mm.sdk.openapi.SendMessageToWX$Req r2 = new com.tencent.mm.sdk.openapi.SendMessageToWX$Req
            r2.<init>()
            java.lang.String r4 = "webpage"
            java.lang.String r4 = buildTransaction(r4)
            r2.transaction = r4
            r2.message = r1
            java.lang.String r4 = "friend"
            boolean r4 = r4.equals(r14)
            if (r4 == 0) goto L_0x008f
            r2.scene = r7
        L_0x005f:
            r15.sendReq(r2)
            return
        L_0x0063:
            boolean r4 = r0.exists()     // Catch:{ IOException -> 0x0075 }
            if (r4 == 0) goto L_0x0077
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeFile(r13)     // Catch:{ IOException -> 0x0075 }
            r5 = 1
            byte[] r4 = getBitmapBytes(r4, r5)     // Catch:{ IOException -> 0x0075 }
            r1.thumbData = r4     // Catch:{ IOException -> 0x0075 }
            goto L_0x0046
        L_0x0075:
            r4 = move-exception
            goto L_0x0046
        L_0x0077:
            android.content.res.Resources r4 = r8.getResources()     // Catch:{ IOException -> 0x0075 }
            java.lang.String r5 = "drawable"
            java.lang.String r6 = "app_icon128"
            int r5 = com.mobcent.share.android.activity.utils.MCShareResourceUtil.getR(r8, r5, r6)     // Catch:{ IOException -> 0x0075 }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeResource(r4, r5)     // Catch:{ IOException -> 0x0075 }
            r5 = 1
            byte[] r4 = getBitmapBytes(r4, r5)     // Catch:{ IOException -> 0x0075 }
            r1.thumbData = r4     // Catch:{ IOException -> 0x0075 }
            goto L_0x0046
        L_0x008f:
            java.lang.String r4 = "wx"
            boolean r4 = r4.equals(r14)
            if (r4 == 0) goto L_0x005f
            r4 = 0
            r2.scene = r4
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.share.android.activity.helper.MCShareLaunchShareHelper.shareFormal(android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.tencent.mm.sdk.openapi.IWXAPI):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0091  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void shareMusic(android.content.Context r8, java.lang.String r9, java.lang.String r10, java.lang.String r11, java.lang.String r12, java.lang.String r13, java.lang.String r14, com.tencent.mm.sdk.openapi.IWXAPI r15) {
        /*
            r7 = 1
            com.tencent.mm.sdk.openapi.WXMusicObject r2 = new com.tencent.mm.sdk.openapi.WXMusicObject
            r2.<init>()
            r2.musicUrl = r11
            com.tencent.mm.sdk.openapi.WXMediaMessage r1 = new com.tencent.mm.sdk.openapi.WXMediaMessage
            r1.<init>()
            r1.mediaObject = r2
            boolean r4 = com.mobcent.forum.android.util.StringUtil.isEmpty(r9)
            if (r4 != 0) goto L_0x0017
            r1.title = r9
        L_0x0017:
            boolean r4 = com.mobcent.forum.android.util.StringUtil.isEmpty(r10)
            if (r4 != 0) goto L_0x001f
            r1.description = r10
        L_0x001f:
            if (r13 != 0) goto L_0x0023
            java.lang.String r13 = ""
        L_0x0023:
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x0077 }
            r0.<init>(r13)     // Catch:{ IOException -> 0x0077 }
            boolean r4 = r0.exists()     // Catch:{ IOException -> 0x0077 }
            if (r4 != 0) goto L_0x0065
            boolean r4 = com.mobcent.forum.android.util.StringUtil.isEmpty(r12)     // Catch:{ IOException -> 0x0077 }
            if (r4 != 0) goto L_0x0065
            java.net.URL r4 = new java.net.URL     // Catch:{ IOException -> 0x0077 }
            r4.<init>(r12)     // Catch:{ IOException -> 0x0077 }
            java.io.InputStream r4 = r4.openStream()     // Catch:{ IOException -> 0x0077 }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeStream(r4)     // Catch:{ IOException -> 0x0077 }
            r5 = 1
            byte[] r4 = getBitmapBytes(r4, r5)     // Catch:{ IOException -> 0x0077 }
            r1.thumbData = r4     // Catch:{ IOException -> 0x0077 }
        L_0x0048:
            com.tencent.mm.sdk.openapi.SendMessageToWX$Req r3 = new com.tencent.mm.sdk.openapi.SendMessageToWX$Req
            r3.<init>()
            java.lang.String r4 = "music"
            java.lang.String r4 = buildTransaction(r4)
            r3.transaction = r4
            r3.message = r1
            java.lang.String r4 = "friend"
            boolean r4 = r4.equals(r14)
            if (r4 == 0) goto L_0x0091
            r3.scene = r7
        L_0x0061:
            r15.sendReq(r3)
            return
        L_0x0065:
            boolean r4 = r0.exists()     // Catch:{ IOException -> 0x0077 }
            if (r4 == 0) goto L_0x0079
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeFile(r13)     // Catch:{ IOException -> 0x0077 }
            r5 = 1
            byte[] r4 = getBitmapBytes(r4, r5)     // Catch:{ IOException -> 0x0077 }
            r1.thumbData = r4     // Catch:{ IOException -> 0x0077 }
            goto L_0x0048
        L_0x0077:
            r4 = move-exception
            goto L_0x0048
        L_0x0079:
            android.content.res.Resources r4 = r8.getResources()     // Catch:{ IOException -> 0x0077 }
            java.lang.String r5 = "drawable"
            java.lang.String r6 = "app_icon128"
            int r5 = com.mobcent.share.android.activity.utils.MCShareResourceUtil.getR(r8, r5, r6)     // Catch:{ IOException -> 0x0077 }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeResource(r4, r5)     // Catch:{ IOException -> 0x0077 }
            r5 = 1
            byte[] r4 = getBitmapBytes(r4, r5)     // Catch:{ IOException -> 0x0077 }
            r1.thumbData = r4     // Catch:{ IOException -> 0x0077 }
            goto L_0x0048
        L_0x0091:
            java.lang.String r4 = "wx"
            boolean r4 = r4.equals(r14)
            if (r4 == 0) goto L_0x0061
            r4 = 0
            r3.scene = r4
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.share.android.activity.helper.MCShareLaunchShareHelper.shareMusic(android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.tencent.mm.sdk.openapi.IWXAPI):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void shareVideo(android.content.Context r8, java.lang.String r9, java.lang.String r10, java.lang.String r11, java.lang.String r12, java.lang.String r13, java.lang.String r14, com.tencent.mm.sdk.openapi.IWXAPI r15) {
        /*
            r7 = 1
            com.tencent.mm.sdk.openapi.WXVideoObject r3 = new com.tencent.mm.sdk.openapi.WXVideoObject
            r3.<init>()
            r3.videoUrl = r11
            com.tencent.mm.sdk.openapi.WXMediaMessage r1 = new com.tencent.mm.sdk.openapi.WXMediaMessage
            r1.<init>(r3)
            boolean r4 = com.mobcent.forum.android.util.StringUtil.isEmpty(r9)
            if (r4 != 0) goto L_0x0015
            r1.title = r9
        L_0x0015:
            boolean r4 = com.mobcent.forum.android.util.StringUtil.isEmpty(r10)
            if (r4 != 0) goto L_0x001d
            r1.description = r10
        L_0x001d:
            if (r13 != 0) goto L_0x0021
            java.lang.String r13 = ""
        L_0x0021:
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x0075 }
            r0.<init>(r13)     // Catch:{ IOException -> 0x0075 }
            boolean r4 = r0.exists()     // Catch:{ IOException -> 0x0075 }
            if (r4 != 0) goto L_0x0063
            boolean r4 = com.mobcent.forum.android.util.StringUtil.isEmpty(r12)     // Catch:{ IOException -> 0x0075 }
            if (r4 != 0) goto L_0x0063
            java.net.URL r4 = new java.net.URL     // Catch:{ IOException -> 0x0075 }
            r4.<init>(r12)     // Catch:{ IOException -> 0x0075 }
            java.io.InputStream r4 = r4.openStream()     // Catch:{ IOException -> 0x0075 }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeStream(r4)     // Catch:{ IOException -> 0x0075 }
            r5 = 1
            byte[] r4 = getBitmapBytes(r4, r5)     // Catch:{ IOException -> 0x0075 }
            r1.thumbData = r4     // Catch:{ IOException -> 0x0075 }
        L_0x0046:
            com.tencent.mm.sdk.openapi.SendMessageToWX$Req r2 = new com.tencent.mm.sdk.openapi.SendMessageToWX$Req
            r2.<init>()
            java.lang.String r4 = "video"
            java.lang.String r4 = buildTransaction(r4)
            r2.transaction = r4
            r2.message = r1
            java.lang.String r4 = "friend"
            boolean r4 = r4.equals(r14)
            if (r4 == 0) goto L_0x008f
            r2.scene = r7
        L_0x005f:
            r15.sendReq(r2)
            return
        L_0x0063:
            boolean r4 = r0.exists()     // Catch:{ IOException -> 0x0075 }
            if (r4 == 0) goto L_0x0077
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeFile(r13)     // Catch:{ IOException -> 0x0075 }
            r5 = 1
            byte[] r4 = getBitmapBytes(r4, r5)     // Catch:{ IOException -> 0x0075 }
            r1.thumbData = r4     // Catch:{ IOException -> 0x0075 }
            goto L_0x0046
        L_0x0075:
            r4 = move-exception
            goto L_0x0046
        L_0x0077:
            android.content.res.Resources r4 = r8.getResources()     // Catch:{ IOException -> 0x0075 }
            java.lang.String r5 = "drawable"
            java.lang.String r6 = "app_icon128"
            int r5 = com.mobcent.share.android.activity.utils.MCShareResourceUtil.getR(r8, r5, r6)     // Catch:{ IOException -> 0x0075 }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeResource(r4, r5)     // Catch:{ IOException -> 0x0075 }
            r5 = 1
            byte[] r4 = getBitmapBytes(r4, r5)     // Catch:{ IOException -> 0x0075 }
            r1.thumbData = r4     // Catch:{ IOException -> 0x0075 }
            goto L_0x0046
        L_0x008f:
            java.lang.String r4 = "wx"
            boolean r4 = r4.equals(r14)
            if (r4 == 0) goto L_0x005f
            r4 = 0
            r2.scene = r4
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.share.android.activity.helper.MCShareLaunchShareHelper.shareVideo(android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.tencent.mm.sdk.openapi.IWXAPI):void");
    }

    /* access modifiers changed from: private */
    public static void shareToPlatform(Context context, String appKey, String content, String linkUrl, String downloadUrl, String picUrl, String imageFilePath, int type) {
        switch (type) {
            case 5:
                shareAppContent(context, appKey, content, linkUrl, downloadUrl);
                return;
            default:
                if (!StringUtil.isEmpty(imageFilePath)) {
                    shareContentWithImageFileAndUrl(context, appKey, content, linkUrl, downloadUrl, picUrl, imageFilePath);
                    return;
                } else if (!StringUtil.isEmpty(picUrl)) {
                    shareContentWithImageUrl(context, appKey, content, linkUrl, downloadUrl, picUrl);
                    return;
                } else {
                    shareContent(context, appKey, content, linkUrl, downloadUrl);
                    return;
                }
        }
    }

    public static void shareAppContent(Context context, String appKey, String content, String linkUrl, String downloadUrl) {
        Intent intent = new Intent(context, MCShareAppActivity.class);
        intent.putExtra("appKey", appKey);
        intent.putExtra("shareContent", content);
        intent.putExtra("linkUrl", linkUrl);
        intent.putExtra("downloadUrl", downloadUrl);
        intent.putExtra("shareType", "shareApp");
        context.startActivity(intent);
    }

    public static void shareContentWithImageFileAndUrl(Context context, String appKey, String content, String linkUrl, String downloadUrl, String picUrl, String imageFilePath) {
        File file = new File(imageFilePath);
        if (!file.exists() || !file.isFile()) {
            Toast.makeText(context, MCShareResourceUtil.getR(context, "string", "mc_share_image_empty"), 0).show();
            return;
        }
        Intent intent = new Intent(context, MCShareAppActivity.class);
        intent.putExtra("appKey", appKey);
        intent.putExtra("shareContent", content);
        intent.putExtra("linkUrl", linkUrl);
        intent.putExtra("downloadUrl", downloadUrl);
        intent.putExtra("sharePic", picUrl);
        intent.putExtra("shareImageFilePath", imageFilePath);
        context.startActivity(intent);
    }

    public static void shareContentWithImageUrl(Context context, String appKey, String content, String linkUrl, String downloadUrl, String picUrl) {
        Intent intent = new Intent(context, MCShareAppActivity.class);
        intent.putExtra("appKey", appKey);
        intent.putExtra("shareContent", content);
        intent.putExtra("linkUrl", linkUrl);
        intent.putExtra("downloadUrl", downloadUrl);
        intent.putExtra("sharePic", picUrl);
        context.startActivity(intent);
    }

    public static void shareContent(Context context, String appKey, String content, String linkUrl, String downloadUrl) {
        Intent intent = new Intent(context, MCShareAppActivity.class);
        intent.putExtra("appKey", appKey);
        intent.putExtra("shareContent", content);
        intent.putExtra("linkUrl", linkUrl);
        intent.putExtra("downloadUrl", downloadUrl);
        context.startActivity(intent);
    }

    private static byte[] getBitmapBytes(Bitmap bitmap, boolean paramBoolean) {
        int j;
        int i;
        byte[] arrayOfByte;
        Bitmap localBitmap = Bitmap.createBitmap(80, 80, Bitmap.Config.RGB_565);
        Canvas localCanvas = new Canvas(localBitmap);
        if (bitmap.getHeight() > bitmap.getWidth()) {
            i = bitmap.getWidth();
            j = bitmap.getWidth();
        } else {
            i = bitmap.getHeight();
            j = bitmap.getHeight();
        }
        while (true) {
            localCanvas.drawBitmap(bitmap, new Rect(0, 0, i, j), new Rect(0, 0, 80, 80), (Paint) null);
            if (paramBoolean) {
                bitmap.recycle();
            }
            ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
            localBitmap.compress(Bitmap.CompressFormat.JPEG, 100, localByteArrayOutputStream);
            localBitmap.recycle();
            arrayOfByte = localByteArrayOutputStream.toByteArray();
            try {
                localByteArrayOutputStream.close();
                break;
            } catch (Exception e) {
                e.printStackTrace();
                i = bitmap.getHeight();
                j = bitmap.getHeight();
            }
        }
        return arrayOfByte;
    }

    private static String buildTransaction(String type) {
        return type == null ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
    }

    private static String getImageSharePath(Context context) {
        return MCShareSnapshotIOUtil.getBaseLocalLocation(context) + LOCAL_POSITION_DIR + "mcshareimage.jpg";
    }
}
