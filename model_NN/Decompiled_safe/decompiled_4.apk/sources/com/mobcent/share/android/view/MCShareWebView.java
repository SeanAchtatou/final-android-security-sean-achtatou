package com.mobcent.share.android.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CacheManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.mobcent.android.model.MCShareSiteModel;
import com.mobcent.android.utils.MCLogUtil;
import com.mobcent.share.android.activity.MCShareAppActivity;
import com.mobcent.share.android.activity.adapter.MCShareSitesAdapter;
import com.mobcent.share.android.activity.utils.MCShareResourceUtil;
import com.mobcent.share.android.widget.MCShareProgressBar;
import java.io.File;

public class MCShareWebView extends LinearLayout {
    /* access modifiers changed from: private */
    public MCShareSitesAdapter adapter;
    /* access modifiers changed from: private */
    public String appKey;
    private ImageButton backButton;
    /* access modifiers changed from: private */
    public String bindUrl;
    private LayoutInflater inflater;
    private boolean isCleanWebCache = false;
    private View mView;
    private WebView mWebView;
    /* access modifiers changed from: private */
    public int markId;
    /* access modifiers changed from: private */
    public MCShareStatusView mcShareStatusView;
    private RelativeLayout mcShareWebLoadingBox;
    protected MCShareProgressBar progressBar;
    private ImageButton refreshButton;
    /* access modifiers changed from: private */
    public MCShareSiteModel siteModel;
    /* access modifiers changed from: private */
    public long userId;

    public MCShareWebView(Context context) {
        super(context);
        initWidgets();
    }

    public MCShareWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initWidgets();
    }

    public void initWidgets() {
        this.inflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
        this.mView = this.inflater.inflate(MCShareResourceUtil.getR(getContext(), "layout", "mc_share_web_view"), (ViewGroup) null);
        addView(this.mView);
        this.mcShareWebLoadingBox = (RelativeLayout) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareWebLoadingBox"));
        this.progressBar = (MCShareProgressBar) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareWebProgressBar"));
        this.mWebView = (WebView) this.mView.findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareWebView"));
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.requestFocus(130);
        if (this.isCleanWebCache) {
            this.mWebView.getSettings().setCacheMode(2);
        }
        this.backButton = (ImageButton) findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareWebBackBtn"));
        this.refreshButton = (ImageButton) findViewById(MCShareResourceUtil.getR(getContext(), "id", "mcShareWebRefreshBtn"));
        this.backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                MCShareWebView.this.closeWebView();
            }
        });
        this.refreshButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MCShareWebView.this.bindUrl != null && !MCShareWebView.this.bindUrl.equals("")) {
                    MCShareWebView.this.fillWebViewPage(MCShareWebView.this.bindUrl + "?mark=" + MCShareWebView.this.markId + "&userId=" + MCShareWebView.this.userId + "&appKey=" + MCShareWebView.this.appKey + "&packageName=" + MCShareWebView.this.getContext().getPackageName());
                }
            }
        });
    }

    public void initData(String bindUrl2, int markId2, long userId2, String appKey2, MCShareSiteModel siteModel2, MCShareSitesAdapter adapter2, MCShareStatusView mcShareStatusView2) {
        if (this.mWebView == null) {
            initWidgets();
        }
        this.markId = markId2;
        this.userId = userId2;
        this.bindUrl = bindUrl2;
        this.appKey = appKey2;
        this.siteModel = siteModel2;
        this.mcShareStatusView = mcShareStatusView2;
        this.adapter = adapter2;
        if (this.isCleanWebCache) {
            try {
                CookieSyncManager.createInstance(getContext());
                CookieSyncManager.getInstance().startSync();
                CookieManager.getInstance().removeSessionCookie();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.mWebView.clearView();
        this.mWebView.clearHistory();
        clearCacheFolder(getContext().getCacheDir(), System.currentTimeMillis());
        String url = bindUrl2 + "?mark=" + markId2 + "&userId=" + userId2 + "&appKey=" + appKey2 + "&packageName=" + getContext().getPackageName();
        MCLogUtil.i("MCShareHttpClientUtil", "url = " + url);
        fillWebViewPage(url);
    }

    /* access modifiers changed from: protected */
    public void fillWebViewPage(String webUrl) {
        this.mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                MCLogUtil.i("MCShareWebView", "url = " + url);
                if (url != null && url.indexOf("success.jsp") > -1) {
                    MCShareWebView.this.siteModel.setBindState(1);
                    MCShareWebView.this.mcShareStatusView.updatAllSites(MCShareWebView.this.siteModel);
                    MCShareWebView.this.adapter.setModels(MCShareWebView.this.mcShareStatusView.getSites());
                    MCShareWebView.this.adapter.notifyDataSetInvalidated();
                    MCShareWebView.this.adapter.notifyDataSetChanged();
                    MCShareWebView.this.closeWebView();
                    Toast.makeText(MCShareWebView.this.getContext(), "　 " + MCShareWebView.this.getContext().getString(MCShareResourceUtil.getR(MCShareWebView.this.getContext(), "string", "mc_share_bind_succ")) + "\n" + MCShareWebView.this.getContext().getString(MCShareResourceUtil.getR(MCShareWebView.this.getContext(), "string", "mc_share_long_unbind")), 1).show();
                } else if (url == null || url.indexOf("fail.jsp") <= -1) {
                    MCShareWebView.this.showLoading();
                } else {
                    Toast.makeText(MCShareWebView.this.getContext(), MCShareResourceUtil.getR(MCShareWebView.this.getContext(), "string", "mc_share_bind_fail"), 1).show();
                    MCShareWebView.this.closeWebView();
                }
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                MCShareWebView.this.hideLoading();
                MCShareWebView.this.progressBar.setVisibility(4);
            }
        });
        this.mWebView.loadUrl(webUrl);
    }

    public int clearCacheFolder(File dir, long numDays) {
        if (!this.isCleanWebCache) {
            return -1;
        }
        int deletedFiles = 0;
        if (dir != null && dir.isDirectory()) {
            try {
                for (File child : dir.listFiles()) {
                    if (child.isDirectory()) {
                        deletedFiles += clearCacheFolder(child, numDays);
                    }
                    if (child.lastModified() < numDays && child.delete()) {
                        deletedFiles++;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return deletedFiles;
    }

    public void cleanCache() {
        if (this.isCleanWebCache) {
            try {
                File file = CacheManager.getCacheFileBaseDir();
                if (file != null && file.exists() && file.isDirectory()) {
                    for (File item : file.listFiles()) {
                        item.delete();
                    }
                    file.delete();
                }
                getContext().deleteDatabase("webview.db");
                getContext().deleteDatabase("webviewCache.db");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void closeWebView() {
        cleanCache();
        ((MCShareAppActivity) getContext()).hideBindSite();
    }

    public WebView getWebView() {
        return this.mWebView;
    }

    public void setWebView(WebView mWebView2) {
        removeView(this.mWebView);
        this.mWebView = mWebView2;
    }

    public void cleanView() {
        removeAllViews();
        this.mWebView = null;
    }

    /* access modifiers changed from: protected */
    public void showLoading() {
        this.mcShareWebLoadingBox.setVisibility(0);
        this.progressBar.show();
    }

    /* access modifiers changed from: protected */
    public void hideLoading() {
        this.mcShareWebLoadingBox.setVisibility(4);
    }
}
