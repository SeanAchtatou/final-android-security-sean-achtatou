package com.mobcent.share.android.activity;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.view.KeyEvent;
import com.mobcent.android.constants.MCShareMobCentApiConstant;
import com.mobcent.android.service.impl.MCShareSyncSiteServiceImpl;
import com.mobcent.android.utils.MCSharePhoneUtil;
import com.mobcent.share.android.activity.utils.MCBitmapImageCache;
import com.mobcent.share.android.activity.utils.MCShareResourceUtil;
import com.mobcent.share.android.view.MCShareStatusView;
import com.mobcent.share.android.view.MCShareWebView;
import com.mobcent.share.android.widget.MCShareLinearLayout;
import java.util.ArrayList;

public class MCShareAppActivity extends MCShareBaseActivity {
    public static String TAG = "MCShareApp";
    private int SHOW_BIND_WEB = 1;
    private int SHOW_SHARE_STAUTS = 0;
    /* access modifiers changed from: private */
    public String appKey;
    /* access modifiers changed from: private */
    public String cty;
    private String downloadUrl;
    /* access modifiers changed from: private */
    public String lan;
    private String linkUrl;
    private Handler mHandler = new Handler();
    private int mState = this.SHOW_SHARE_STAUTS;
    private MCShareLinearLayout mcShareBox;
    /* access modifiers changed from: private */
    public MCShareStatusView mcShareStatus;
    private MCShareWebView mcShareWeb;
    private int screenWidth = 0;
    private String shareAppContent;
    private String shareImageFilePath;
    private String sharePic;
    private String shareType;
    private String shareUrl;
    private String smsUrl;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(MCShareResourceUtil.getR(this, "layout", "mc_share_share_status"));
        ApplicationInfo ai = null;
        Bundle bundle = null;
        try {
            ai = getPackageManager().getApplicationInfo(getPackageName(), AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER);
            bundle = ai.metaData;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        this.shareType = getIntent().getStringExtra("shareType") == null ? "" : getIntent().getStringExtra("shareType");
        this.shareAppContent = getIntent().getStringExtra("shareContent") == null ? "" : getIntent().getStringExtra("shareContent");
        this.sharePic = getIntent().getStringExtra("sharePic") == null ? "" : getIntent().getStringExtra("sharePic");
        this.shareUrl = getIntent().getStringExtra(MCShareMobCentApiConstant.SHARE_URL) == null ? "" : getIntent().getStringExtra(MCShareMobCentApiConstant.SHARE_URL);
        this.smsUrl = getIntent().getStringExtra("smsUrl") == null ? "" : getIntent().getStringExtra("smsUrl");
        this.linkUrl = getIntent().getStringExtra("linkUrl") == null ? "" : getIntent().getStringExtra("linkUrl");
        this.downloadUrl = getIntent().getStringExtra("downloadUrl") == null ? "" : getIntent().getStringExtra("downloadUrl");
        this.shareImageFilePath = getIntent().getStringExtra("shareImageFilePath") == null ? "" : getIntent().getStringExtra("shareImageFilePath").trim();
        if ((this.smsUrl == null || this.smsUrl.equals("")) && bundle != null) {
            try {
                this.smsUrl = ai.metaData.getString("mc_share_sms_mail_url");
            } catch (Exception e2) {
                this.smsUrl = "";
            }
        }
        String appKeyIn = getIntent().getStringExtra("appKey");
        if (appKeyIn != null && !"".equals(appKeyIn)) {
            this.appKey = appKeyIn;
        } else if (bundle != null) {
            this.appKey = ai.metaData.getString("mc_share_app_key");
        }
        this.lan = getResources().getConfiguration().locale.getLanguage();
        this.cty = getResources().getConfiguration().locale.getCountry();
        initWidgets();
    }

    private void initWidgets() {
        this.screenWidth = MCSharePhoneUtil.getDisplayWidth(this);
        this.mcShareBox = (MCShareLinearLayout) findViewById(MCShareResourceUtil.getR(this, "id", "mcShareBox"));
        this.mcShareBox.getLayoutParams().width = this.screenWidth * 2;
        this.mcShareStatus = (MCShareStatusView) findViewById(MCShareResourceUtil.getR(this, "id", "mcShareStatus"));
        this.mcShareWeb = (MCShareWebView) findViewById(MCShareResourceUtil.getR(this, "id", "mcShareWeb"));
        this.mcShareStatus.getLayoutParams().width = this.screenWidth;
        this.mcShareWeb.getLayoutParams().width = this.screenWidth;
        this.mcShareStatus.initData(this.appKey, this.lan, this.cty, this.shareUrl, this.smsUrl, this.linkUrl, this.downloadUrl, this.shareAppContent, this.sharePic, this.shareImageFilePath, this.shareType);
    }

    public Handler getCurrentHandler() {
        return this.mHandler;
    }

    public MCShareStatusView getMcShareStatus() {
        return this.mcShareStatus;
    }

    public void setMcShareStatus(MCShareStatusView mcShareStatus2) {
        this.mcShareStatus = mcShareStatus2;
    }

    public String getAppKey() {
        return this.appKey;
    }

    public void showBindSite() {
        this.mState = this.SHOW_BIND_WEB;
        this.mcShareBox.scrollerToRight();
    }

    public void hideBindSite() {
        this.mState = this.SHOW_SHARE_STAUTS;
        this.mcShareBox.scrollerToLeft();
    }

    public MCShareWebView getMcShareWeb() {
        return this.mcShareWeb;
    }

    public void setMcShareWeb(MCShareWebView mcShareWeb2) {
        this.mcShareWeb = mcShareWeb2;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.mState != this.SHOW_BIND_WEB) {
            finish();
            return true;
        } else if (this.mcShareWeb.getWebView() == null || !this.mcShareWeb.getWebView().canGoBack()) {
            this.mcShareWeb.closeWebView();
            return true;
        } else {
            this.mcShareWeb.getWebView().goBack();
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        new Thread(new Runnable() {
            public void run() {
                ArrayList<String> urls = new ArrayList<>();
                if (MCShareAppActivity.this.mcShareStatus.getSites() != null) {
                    int j = MCShareAppActivity.this.mcShareStatus.getSites().size();
                    for (int i = 0; i < j; i++) {
                        urls.add(MCShareAppActivity.this.mcShareStatus.getSites().get(i).getSiteImage());
                    }
                }
                MCBitmapImageCache.getInstance(MCShareAppActivity.this.getApplicationContext(), MCShareAppActivity.TAG).clearBitmap(urls);
                MCShareAppActivity.this.mcShareStatus.recycleBitmap();
                new MCShareSyncSiteServiceImpl(MCShareAppActivity.this).addOrUpdateSite(MCShareAppActivity.this.appKey, MCShareAppActivity.this.getResources().getString(MCShareResourceUtil.getR(MCShareAppActivity.this, "string", "mc_share_domain_url")), MCShareAppActivity.this.lan, MCShareAppActivity.this.cty);
            }
        }).start();
    }
}
