package com.mobcent.share.android.constant;

public interface MCShareConstant {
    public static final String EXHIBITION_SHARE = "share";
    public static final String RESOLUTION_320X480 = "320x480";
    public static final int SHARE_APP = 5;
    public static final int SHARE_MUSIC = 3;
    public static final int SHARE_PICTURE = 1;
    public static final int SHARE_PIC_AND_WORD = 2;
    public static final int SHARE_POSITION = 1002;
    public static final int SHARE_VIDEO = 4;
}
