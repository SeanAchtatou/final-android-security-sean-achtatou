package com.mobcent.share.android.activity.utils;

import android.content.Context;

public class MCShareResourceUtil {
    public static int getR(Context context, String type, String name) {
        try {
            return context.getResources().getIdentifier(name, type, context.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
