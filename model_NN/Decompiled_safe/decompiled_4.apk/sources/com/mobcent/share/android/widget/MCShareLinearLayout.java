package com.mobcent.share.android.widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.Scroller;
import com.mobcent.android.utils.MCSharePhoneUtil;

public class MCShareLinearLayout extends LinearLayout {
    private LinearLayout leftLayout;
    private Scroller mScroller;
    private LinearLayout rightLayout;
    private int screenWidth = 0;

    public MCShareLinearLayout(Context context) {
        super(context);
        init();
    }

    public MCShareLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        this.mScroller = new Scroller(getContext());
        this.screenWidth = MCSharePhoneUtil.getDisplayWidth((Activity) getContext());
    }

    public void computeScroll() {
        if (this.mScroller.computeScrollOffset()) {
            scrollTo(this.mScroller.getCurrX(), this.mScroller.getCurrY());
            postInvalidate();
        }
    }

    public void scrollerToLeft() {
        if (this.mScroller.isFinished()) {
            int x = getScrollX();
            if (x == 0) {
                x = this.screenWidth;
            }
            this.mScroller.startScroll(x, 0, -this.screenWidth, 0, 300);
            invalidate();
        }
    }

    public void scrollerToRight() {
        if (this.mScroller.isFinished()) {
            this.mScroller.startScroll(getScrollX(), 0, this.screenWidth, 0, 300);
            invalidate();
        }
    }

    public LinearLayout getLeftLayout() {
        return this.leftLayout;
    }

    public void setLeftLayout(LinearLayout leftLayout2) {
        this.leftLayout = leftLayout2;
    }

    public LinearLayout getRightLayout() {
        return this.rightLayout;
    }

    public void setRightLayout(LinearLayout rightLayout2) {
        this.rightLayout = rightLayout2;
    }
}
