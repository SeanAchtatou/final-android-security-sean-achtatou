package com.mobcent.share.android.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import com.mobcent.share.android.activity.utils.MCShareResourceUtil;

public class MCShareProgressBar extends ImageView {
    private Drawable defaultDrawable = null;
    private int height = 0;
    private RotateAnimation mRotateAnimation;
    private Paint paint = null;
    private int width = 0;

    public MCShareProgressBar(Context context) {
        super(context);
        init();
    }

    public MCShareProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MCShareProgressBar(Context context, boolean isCreate) {
        super(context);
        init();
        this.width = (int) getResources().getDimension(MCShareResourceUtil.getR(context, "dimen", "updatebar_padding"));
        this.height = (int) getResources().getDimension(MCShareResourceUtil.getR(context, "dimen", "updatebar_padding"));
    }

    private void init() {
        this.paint = new Paint();
        this.paint.setAntiAlias(true);
        setFocusable(true);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(0);
        this.defaultDrawable = getBackground();
        if (this.defaultDrawable == null) {
            this.defaultDrawable = getResources().getDrawable(MCShareResourceUtil.getR(getContext(), "drawable", "mc_share_small_loading"));
        }
        if (this.width == 0) {
            this.width = getWidth();
        }
        if (this.height == 0) {
            this.height = getHeight();
        }
        this.defaultDrawable.setBounds(0, 0, this.width, this.height);
        this.defaultDrawable.draw(canvas);
    }

    public void show() {
        this.mRotateAnimation = new RotateAnimation(0.0f, 3.6E8f, 1, 0.5f, 1, 0.5f);
        this.mRotateAnimation.setDuration(1400000000);
        this.mRotateAnimation.setInterpolator(new LinearInterpolator());
        this.mRotateAnimation.setFillAfter(true);
        startAnimation(this.mRotateAnimation);
    }
}
