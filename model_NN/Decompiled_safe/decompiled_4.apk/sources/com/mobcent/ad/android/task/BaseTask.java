package com.mobcent.ad.android.task;

import android.content.Context;
import android.os.AsyncTask;
import com.mobcent.ad.android.service.AdService;
import com.mobcent.ad.android.service.impl.AdServiceImpl;
import com.mobcent.forum.android.util.MCResource;

public abstract class BaseTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
    protected AdService adService;
    protected Context context;
    protected MCResource resource;

    public BaseTask(Context context2) {
        this.context = context2;
        this.resource = MCResource.getInstance(context2);
        this.adService = new AdServiceImpl(context2);
    }
}
