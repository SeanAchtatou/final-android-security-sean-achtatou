package com.mobcent.ad.android.utils;

import com.mobcent.ad.android.api.BaseAdApiRequester;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.ad.android.constant.AdConstant;
import com.mobcent.ad.android.model.AdApkModel;
import com.mobcent.base.android.constant.MCConstant;
import java.net.URL;

public class AdStringUtils implements AdConstant, AdApiConstant {
    public static String parseImgUrl(int dt, String url) {
        if (!url.contains(BaseAdApiRequester.BASE_URL + "img/")) {
            return url;
        }
        String fileType = getUrlLastName(url);
        if (dt == 3) {
            url = url + "." + AdConstant.RESOLUTION_320X50;
        } else if (dt == 4) {
            url = url + "." + AdConstant.RESOLUTION_160X50;
        } else if (dt == 9) {
            url = url + "." + AdConstant.RESOLUTION_320X380;
        }
        return url + fileType;
    }

    private static String getUrlLastName(String url) {
        if (url == null || "".equals(url)) {
            return "";
        }
        String url2 = url.trim();
        return url2.substring(url2.lastIndexOf("."), url2.length());
    }

    public static AdApkModel parseAdApkModel(String url) {
        AdApkModel apkModel = new AdApkModel();
        if (url != null && !url.equals("")) {
            try {
                String[] strs = new URL(url).getQuery().split("&");
                int len = strs.length;
                for (int i = 0; i < len; i++) {
                    if (strs[i].contains("=")) {
                        int index = strs[i].indexOf("=");
                        String key = strs[i].substring(0, index);
                        String value = strs[i].substring(index + 1, strs[i].length());
                        if (key.equals("appName")) {
                            apkModel.setAppName(value);
                        } else if (key.equals("packageName")) {
                            apkModel.setPackageName(value);
                        } else if (key.equals("appSize")) {
                            apkModel.setAppSize(Integer.parseInt(value));
                        } else if (key.equals(AdApiConstant.APP_CLOSE)) {
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
        return apkModel;
    }

    public static boolean isGif(String url) {
        if (url == null || "".equals(url)) {
            return false;
        }
        return getUrlLastName(url.toLowerCase()).contains(MCConstant.PIC_GIF);
    }
}
