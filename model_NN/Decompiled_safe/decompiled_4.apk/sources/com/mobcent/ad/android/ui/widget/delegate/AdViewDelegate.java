package com.mobcent.ad.android.ui.widget.delegate;

import com.mobcent.ad.android.model.AdContainerModel;

public interface AdViewDelegate {
    void freeMemery();

    void setAdContainerModel(AdContainerModel adContainerModel, int i);
}
