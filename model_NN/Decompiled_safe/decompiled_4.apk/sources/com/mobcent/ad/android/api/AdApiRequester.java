package com.mobcent.ad.android.api;

import android.content.Context;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.forum.android.api.util.HttpClientUtil;
import com.mobcent.forum.android.util.MCLogUtil;
import java.util.HashMap;
import org.apache.http.client.methods.HttpGet;

public class AdApiRequester extends BaseAdApiRequester {
    private static String TAG = "AdApiRequester";
    private static String urlString;

    public static String haveAd(Context context) {
        urlString = BASE_URL + "clientapi/m/haveAd.do";
        return doPostRequest(urlString, new HashMap<>(), context);
    }

    public static String getAd(Context context) {
        urlString = BASE_URL + "clientapi/m/getAd.do";
        HashMap<String, String> params = new HashMap<>();
        params.put(AdApiConstant.PO, "99");
        return doPostRequest(urlString, params, context);
    }

    public static String getLinkDoUrl(Context context, long aid, long po, String du) {
        urlString = BASE_URL + "clientapi/m/linkAd.do?";
        urlString = createGetUrl(urlString, aid, po, du, context);
        return urlString;
    }

    public static String downAd(Context context, long aid, int po, String app, String date) {
        urlString = BASE_URL + "clientapi/m/downAd.do";
        HashMap<String, String> params = new HashMap<>();
        params.put("aid", aid + "");
        params.put(AdApiConstant.PO, po + "");
        params.put(AdApiConstant.APP_PN, app);
        params.put("date", date);
        return doPostRequest(urlString, params, context);
    }

    public static String activeAd(Context context, long aid, int po, String app, String date) {
        urlString = BASE_URL + "clientapi/m/activeAd.do";
        HashMap<String, String> params = new HashMap<>();
        params.put("aid", aid + "");
        params.put(AdApiConstant.PO, po + "");
        params.put(AdApiConstant.APP_PN, app);
        params.put("date", date);
        return doPostRequest(urlString, params, context);
    }

    public static void doGetRequest(Context context, String url) {
        try {
            HttpClientUtil.getNewHttpClient(context, -1, -1).execute(new HttpGet(urlString));
            MCLogUtil.i(TAG, url);
        } catch (Exception e) {
            e.printStackTrace();
            MCLogUtil.i(TAG, url);
        }
    }
}
