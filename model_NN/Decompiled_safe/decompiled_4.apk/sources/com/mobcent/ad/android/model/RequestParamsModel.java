package com.mobcent.ad.android.model;

public class RequestParamsModel extends AdBaseModel {
    private static final long serialVersionUID = -6812674013976006162L;
    private String ak;
    private String ch;
    private String im;
    private String mc;
    private String pn;
    private String pt;
    private String ss;
    private String ua;
    private long uid;
    private String zo;

    public String getIm() {
        return this.im;
    }

    public void setIm(String im2) {
        this.im = im2;
    }

    public String getPt() {
        return this.pt;
    }

    public void setPt(String pt2) {
        this.pt = pt2;
    }

    public String getSs() {
        return this.ss;
    }

    public void setSs(String ss2) {
        this.ss = ss2;
    }

    public String getUa() {
        return this.ua;
    }

    public void setUa(String ua2) {
        this.ua = ua2;
    }

    public String getZo() {
        return this.zo;
    }

    public void setZo(String zo2) {
        this.zo = zo2;
    }

    public String getAk() {
        return this.ak;
    }

    public void setAk(String ak2) {
        this.ak = ak2;
    }

    public String getPn() {
        return this.pn;
    }

    public void setPn(String pn2) {
        this.pn = pn2;
    }

    public String getMc() {
        return this.mc;
    }

    public void setMc(String mc2) {
        this.mc = mc2;
    }

    public String getCh() {
        return this.ch;
    }

    public void setCh(String ch2) {
        this.ch = ch2;
    }

    public long getUid() {
        return this.uid;
    }

    public void setUid(long uid2) {
        this.uid = uid2;
    }
}
