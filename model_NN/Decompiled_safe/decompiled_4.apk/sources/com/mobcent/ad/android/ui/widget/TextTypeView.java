package com.mobcent.ad.android.ui.widget;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mobcent.ad.android.model.AdContainerModel;
import com.mobcent.ad.android.model.AdModel;
import com.mobcent.ad.android.ui.widget.delegate.AdViewDelegate;
import com.mobcent.ad.android.ui.widget.helper.AdViewHelper;
import com.mobcent.ad.android.utils.AdViewUtils;
import java.util.Iterator;

public class TextTypeView extends LinearLayout implements AdViewDelegate {
    private AdContainerModel adContainerModel;
    private int adPosition;
    private LinearLayout.LayoutParams lps;
    private TextView tv1;
    private TextView tv2;

    public TextTypeView(Context context) {
        super(context);
        setOrientation(0);
    }

    public void setAdContainerModel(AdContainerModel adContainerModel2, int adPosition2) {
        this.adContainerModel = adContainerModel2;
        this.adPosition = adPosition2;
        initView();
    }

    public void initView() {
        this.tv1 = AdViewUtils.createAdText(getContext());
        this.tv2 = AdViewUtils.createAdText(getContext());
        this.lps = new LinearLayout.LayoutParams(-2, AdViewUtils.dipToPx(getContext(), 50), 1.0f);
        addView(this.tv1, this.lps);
        addView(this.tv2, this.lps);
        Iterator<AdModel> iterator = this.adContainerModel.getAdSet().iterator();
        AdModel adModel1 = iterator.next();
        adModel1.setPo(this.adPosition);
        AdModel adModel2 = iterator.next();
        adModel2.setPo(this.adPosition);
        this.tv1.setText(adModel1.getTx());
        this.tv1.setTag(adModel1);
        this.tv2.setText(adModel2.getTx());
        this.tv2.setTag(adModel2);
        this.tv1.setOnClickListener(AdViewHelper.getInstance(getContext()).getAdViewClickListener());
        this.tv2.setOnClickListener(AdViewHelper.getInstance(getContext()).getAdViewClickListener());
    }

    public void freeMemery() {
    }
}
