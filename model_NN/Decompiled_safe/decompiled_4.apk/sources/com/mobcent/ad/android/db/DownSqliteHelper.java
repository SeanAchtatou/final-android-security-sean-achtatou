package com.mobcent.ad.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.mobcent.ad.android.db.constant.AdDbConstant;
import com.mobcent.ad.android.model.AdDownDbModel;
import java.util.ArrayList;
import java.util.List;

public class DownSqliteHelper extends BaseSqliteOpenHelper {
    private static DownSqliteHelper helper;
    private Object _lock = new Object();

    public static DownSqliteHelper getInstance(Context context) {
        if (helper == null) {
            helper = new DownSqliteHelper(context);
        }
        return helper;
    }

    public DownSqliteHelper(Context context) {
        super(context);
    }

    public synchronized void insert(AdDownDbModel model) {
        getWritableDatabase().insert(AdDbConstant.DOWNLOAD_TABLE, null, parseContentValues(model));
        close();
    }

    public synchronized void update(AdDownDbModel model) {
        ContentValues values = parseContentValues(model);
        getWritableDatabase().update(AdDbConstant.DOWNLOAD_TABLE, values, "download_url = ? ", new String[]{model.getUrl()});
        close();
    }

    public synchronized AdDownDbModel query(String url) {
        AdDownDbModel adDownDbModel;
        if (url != null) {
            if (!url.equals("")) {
                AdDownDbModel adDownDbModel2 = null;
                try {
                    Cursor c = getReadableDatabase().query(AdDbConstant.DOWNLOAD_TABLE, null, "download_url = ? ", new String[]{url}, null, null, null);
                    while (true) {
                        if (c.moveToNext()) {
                            if (c.getString(c.getColumnIndex(AdDbConstant.DOWNLOAD_URL)).equals(url)) {
                                adDownDbModel2 = parseAdDownDbModel(c);
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    c.close();
                    close();
                } catch (Exception e) {
                }
                adDownDbModel = adDownDbModel2;
            }
        }
        adDownDbModel = null;
        return adDownDbModel;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.mobcent.ad.android.model.AdDownDbModel queryByPackageName(java.lang.String r12) {
        /*
            r11 = this;
            monitor-enter(r11)
            r8 = 0
            android.database.sqlite.SQLiteDatabase r0 = r11.getReadableDatabase()     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            java.lang.String r1 = "download_t"
            r2 = 0
            java.lang.String r3 = "download_pn = ? "
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            r5 = 0
            r4[r5] = r12     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r9 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
        L_0x0018:
            boolean r0 = r9.moveToNext()     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            if (r0 == 0) goto L_0x0032
            java.lang.String r0 = "download_pn"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            java.lang.String r10 = r9.getString(r0)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            boolean r0 = r10.equals(r12)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            if (r0 == 0) goto L_0x0018
            com.mobcent.ad.android.model.AdDownDbModel r8 = r11.parseAdDownDbModel(r9)     // Catch:{ Exception -> 0x003d, all -> 0x003a }
        L_0x0032:
            r9.close()     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            r11.close()     // Catch:{ Exception -> 0x003d, all -> 0x003a }
        L_0x0038:
            monitor-exit(r11)
            return r8
        L_0x003a:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        L_0x003d:
            r0 = move-exception
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.ad.android.db.DownSqliteHelper.queryByPackageName(java.lang.String):com.mobcent.ad.android.model.AdDownDbModel");
    }

    public List<AdDownDbModel> queryUnDownDoList() {
        List<AdDownDbModel> adDownModels = new ArrayList<>();
        try {
            Cursor c = getReadableDatabase().query(AdDbConstant.DOWNLOAD_TABLE, null, "downloadDo = ? and download_status = ? ", new String[]{String.valueOf(0), String.valueOf(2)}, null, null, null);
            while (c.moveToNext()) {
                new AdDownDbModel();
                adDownModels.add(parseAdDownDbModel(c));
            }
            c.close();
            close();
        } catch (Exception e) {
        }
        return adDownModels;
    }

    public List<AdDownDbModel> queryUnActiveDoList() {
        List<AdDownDbModel> adDownModels = new ArrayList<>();
        try {
            Cursor c = getReadableDatabase().query(AdDbConstant.DOWNLOAD_TABLE, null, "activeDo = ?  and download_status = ? ", new String[]{String.valueOf(0), String.valueOf(2)}, null, null, null);
            while (c.moveToNext()) {
                new AdDownDbModel();
                adDownModels.add(parseAdDownDbModel(c));
            }
            c.close();
            close();
        } catch (Exception e) {
        }
        return adDownModels;
    }

    private ContentValues parseContentValues(AdDownDbModel model) {
        ContentValues values = new ContentValues();
        values.put(AdDbConstant.DOWNLOAD_AID, Long.valueOf(model.getAid()));
        values.put(AdDbConstant.DOWNLOAD_URL, model.getUrl());
        values.put(AdDbConstant.DOWNLOAD_PN, model.getPn());
        values.put(AdDbConstant.DOWNLOAD_CURRENT_PN, model.getCurrentPn());
        values.put(AdDbConstant.DOWNLOAD_PO, Integer.valueOf(model.getPo()));
        values.put(AdDbConstant.DOWNLOAD_DATE, model.getDate());
        values.put(AdDbConstant.DOWNLOAD_STATUS, Integer.valueOf(model.getStatus()));
        values.put("appName", model.getAppName());
        values.put(AdDbConstant.DOWNLOAD_IS_DOWNLOAD_DO, Integer.valueOf(model.getDownloadDo()));
        values.put(AdDbConstant.DOWNLOAD_IS_ACTIVE_DO, Integer.valueOf(model.getActiveDo()));
        return values;
    }

    private AdDownDbModel parseAdDownDbModel(Cursor c) {
        AdDownDbModel adDownDbModel = new AdDownDbModel();
        adDownDbModel.setId(c.getInt(c.getColumnIndex(AdDbConstant.DOWNLOAD_ID)));
        adDownDbModel.setAid(c.getLong(c.getColumnIndex(AdDbConstant.DOWNLOAD_AID)));
        adDownDbModel.setUrl(c.getString(c.getColumnIndex(AdDbConstant.DOWNLOAD_URL)));
        adDownDbModel.setPn(c.getString(c.getColumnIndex(AdDbConstant.DOWNLOAD_PN)));
        adDownDbModel.setCurrentPn(c.getString(c.getColumnIndex(AdDbConstant.DOWNLOAD_CURRENT_PN)));
        adDownDbModel.setPo(c.getInt(c.getColumnIndex(AdDbConstant.DOWNLOAD_PO)));
        adDownDbModel.setDate(c.getString(c.getColumnIndex(AdDbConstant.DOWNLOAD_DATE)));
        adDownDbModel.setStatus(c.getInt(c.getColumnIndex(AdDbConstant.DOWNLOAD_STATUS)));
        adDownDbModel.setAppName(c.getString(c.getColumnIndex("appName")));
        adDownDbModel.setDownloadDo(c.getInt(c.getColumnIndex(AdDbConstant.DOWNLOAD_IS_DOWNLOAD_DO)));
        adDownDbModel.setActiveDo(c.getInt(c.getColumnIndex(AdDbConstant.DOWNLOAD_IS_ACTIVE_DO)));
        return adDownDbModel;
    }
}
