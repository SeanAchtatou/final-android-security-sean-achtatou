package com.mobcent.ad.android.ui.widget;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mobcent.ad.android.constant.AdConstant;
import com.mobcent.ad.android.model.AdContainerModel;
import com.mobcent.ad.android.model.AdModel;
import com.mobcent.ad.android.ui.widget.delegate.AdViewDelegate;
import com.mobcent.ad.android.ui.widget.helper.AdViewHelper;
import com.mobcent.ad.android.utils.AdStringUtils;
import com.mobcent.ad.android.utils.AdViewUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MultiTypeView extends LinearLayout implements AdViewDelegate, AdConstant {
    private String TAG = "MulityTypeView";
    private AdContainerModel adContainerModel;
    private int adPosition;
    private LinearLayout.LayoutParams lps;
    private List<BasePicView> picViewList = new ArrayList();

    public MultiTypeView(Context context) {
        super(context);
        setOrientation(0);
    }

    public void setAdContainerModel(AdContainerModel adContainerModel2, int adPosition2) {
        this.adContainerModel = adContainerModel2;
        this.adPosition = adPosition2;
        initView();
    }

    private void initView() {
        switch (this.adContainerModel.getType()) {
            case 1:
                createLongText();
                return;
            case 2:
                createThreeText();
                return;
            case 3:
                createLongImg();
                return;
            case 4:
                createTwoImg();
                return;
            default:
                return;
        }
    }

    public void createLongText() {
        TextView tv = AdViewUtils.createAdText(getContext());
        AdModel adModel = this.adContainerModel.getAdSet().iterator().next();
        adModel.setPo(this.adPosition);
        tv.setText(adModel.getTx());
        tv.setTag(adModel);
        tv.setOnClickListener(AdViewHelper.getInstance(getContext()).getAdViewClickListener());
        this.lps = new LinearLayout.LayoutParams(-1, -1);
        addView(tv, this.lps);
    }

    public void createThreeText() {
        this.lps = new LinearLayout.LayoutParams(0, -1, 1.0f);
        Iterator<AdModel> iterator = this.adContainerModel.getAdSet().iterator();
        for (int i = 0; i < 3; i++) {
            AdModel adModel = iterator.next();
            adModel.setPo(this.adPosition);
            TextView tv = AdViewUtils.createAdText(getContext());
            tv.setText(adModel.getTx());
            tv.setTag(adModel);
            tv.setOnClickListener(AdViewHelper.getInstance(getContext()).getAdViewClickListener());
            addView(tv, this.lps);
        }
    }

    public void createLongImg() {
        AdModel adModel = this.adContainerModel.getAdSet().iterator().next();
        adModel.setPo(this.adPosition);
        BasePicView picView = AdViewUtils.createPicView(getContext(), adModel);
        picView.setOnClickListener(AdViewHelper.getInstance(getContext()).getAdViewClickListener());
        addView(picView, new LinearLayout.LayoutParams(-1, -1));
        picView.loadPic(AdStringUtils.parseImgUrl(adModel.getDt(), adModel.getPu()), null);
        this.picViewList.add(picView);
    }

    public void createTwoImg() {
        this.lps = new LinearLayout.LayoutParams(0, -1, 1.0f);
        Iterator<AdModel> iterator = this.adContainerModel.getAdSet().iterator();
        for (int i = 0; i < 2; i++) {
            AdModel adModel = iterator.next();
            adModel.setPo(this.adPosition);
            BasePicView picView = AdViewUtils.createPicView(getContext(), adModel);
            addView(picView, this.lps);
            picView.setOnClickListener(AdViewHelper.getInstance(getContext()).getAdViewClickListener());
            picView.loadPic(AdStringUtils.parseImgUrl(adModel.getDt(), adModel.getPu()), null);
            this.picViewList.add(picView);
        }
    }

    public void freeMemery() {
        for (int i = 0; i < this.picViewList.size(); i++) {
            this.picViewList.get(i).recyclePic();
        }
        this.picViewList.clear();
    }
}
