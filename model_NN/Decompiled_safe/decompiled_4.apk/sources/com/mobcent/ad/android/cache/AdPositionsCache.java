package com.mobcent.ad.android.cache;

import com.mobcent.ad.android.constant.AdConstant;
import com.mobcent.ad.android.model.AdStateModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AdPositionsCache implements AdConstant {
    private int cycle;
    private int[] dt1;
    private int[] dt2;
    private int[] dt3;
    private boolean isConectFailed = false;

    public boolean isConectFailed() {
        return this.isConectFailed;
    }

    public void setConectFailed(boolean isConectFailed2) {
        this.isConectFailed = isConectFailed2;
    }

    public int[] getDt1() {
        return this.dt1;
    }

    public void setDt1(int[] dt12) {
        this.dt1 = dt12;
    }

    public int[] getDt2() {
        return this.dt2;
    }

    public void setDt2(int[] dt22) {
        this.dt2 = dt22;
    }

    public int[] getDt3() {
        return this.dt3;
    }

    public void setDt3(int[] dt32) {
        this.dt3 = dt32;
    }

    public int getCycle() {
        return this.cycle;
    }

    public void setCycle(int cycle2) {
        this.cycle = cycle2;
    }

    public String toString() {
        return "AdPositionsCache [dt1=" + Arrays.toString(this.dt1) + ", dt2=" + Arrays.toString(this.dt2) + ", dt3=" + Arrays.toString(this.dt3) + ", cycle=" + this.cycle + "]";
    }

    public AdStateModel getAdState(int position) {
        AdStateModel adStateModel = haveAd(this.dt1, 1, position);
        if (adStateModel != null) {
            return adStateModel;
        }
        AdStateModel adStateModel2 = haveAd(this.dt2, 2, position);
        if (adStateModel2 != null) {
            return adStateModel2;
        }
        AdStateModel adStateModel3 = haveAd(this.dt3, 3, position);
        if (adStateModel3 != null) {
            return adStateModel3;
        }
        return null;
    }

    private boolean isEmpty(int[] dts) {
        if (dts == null || dts.length == 0) {
            return true;
        }
        return false;
    }

    private AdStateModel haveAd(int[] dt, int dtType, int position) {
        if (isEmpty(dt)) {
            return null;
        }
        AdStateModel adStateModel = null;
        int i = 0;
        int len = dt.length;
        while (true) {
            if (i >= len) {
                break;
            } else if (dt[i] == position) {
                adStateModel = new AdStateModel();
                adStateModel.setDtType(dtType);
                adStateModel.setHaveAd(true);
                break;
            } else {
                i++;
            }
        }
        return adStateModel;
    }

    public int[] isHaveAd(int[] positions) {
        List<Integer> positionList = new ArrayList<>();
        for (int i = 0; i < positions.length; i++) {
            if (isPositionsHaveAd(positions[i])) {
                positionList.add(Integer.valueOf(positions[i]));
            }
        }
        int[] pos = new int[positionList.size()];
        for (int i2 = 0; i2 < positionList.size(); i2++) {
            pos[i2] = ((Integer) positionList.get(i2)).intValue();
        }
        return pos;
    }

    private boolean isPositionsHaveAd(int position) {
        return isInDt(this.dt1, position) || isInDt(this.dt2, position) || isInDt(this.dt3, position);
    }

    private boolean isInDt(int[] dt, int position) {
        if (isEmpty(dt)) {
            return false;
        }
        for (int i : dt) {
            if (position == i) {
                return true;
            }
        }
        return false;
    }
}
