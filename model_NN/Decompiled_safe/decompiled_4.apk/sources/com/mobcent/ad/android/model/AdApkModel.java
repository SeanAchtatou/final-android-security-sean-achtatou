package com.mobcent.ad.android.model;

public class AdApkModel extends AdBaseModel {
    private static final long serialVersionUID = 1945721580361147895L;
    private String appName;
    private int appSize;
    private String packageName;

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String packageName2) {
        this.packageName = packageName2;
    }

    public String getAppName() {
        return this.appName;
    }

    public void setAppName(String appName2) {
        this.appName = appName2;
    }

    public int getAppSize() {
        return this.appSize;
    }

    public void setAppSize(int appSize2) {
        this.appSize = appSize2;
    }
}
