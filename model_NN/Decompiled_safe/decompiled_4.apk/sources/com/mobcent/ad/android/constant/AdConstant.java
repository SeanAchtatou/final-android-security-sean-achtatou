package com.mobcent.ad.android.constant;

public interface AdConstant {
    public static final int AD_CT_CPA = 3;
    public static final int AD_CT_CPC = 2;
    public static final int AD_CT_CPM = 1;
    public static final int AD_CT_CPS = 4;
    public static final int AD_DT_BIG_IMAGE = 9;
    public static final int AD_DT_LONG_IMG = 3;
    public static final int AD_DT_LONG_TEXT = 1;
    public static final int AD_DT_SEARCH_KEY_TEXT = 6;
    public static final int AD_DT_THREE_KEY_TEXT = 5;
    public static final int AD_DT_THREE_SHORT_TEXT = 2;
    public static final int AD_DT_TWO_KEY_TEXT = 8;
    public static final int AD_DT_TWO_SHORT_IMG = 4;
    public static final int AD_DT_TWO_SHORT_TEXT = 7;
    public static final int AD_GET_AD_PO = 99;
    public static final int AD_GT_CALL = 3;
    public static final int AD_GT_DOWNLOAD = 1;
    public static final int AD_GT_SMS = 4;
    public static final int AD_GT_WEBVIEW = 2;
    public static final int AD_TEXT_SIZE = 16;
    public static final int AD_TYPE_BIG_IMAGE = 3;
    public static final int AD_TYPE_MULTI = 1;
    public static final int AD_TYPE_TWO_SHORT_TEXT = 2;
    public static final String ANDROID_OS = "ANDR_";
    public static final int BANNAS_HEIGHT = 50;
    public static final int LINE_HEIGHT = 2;
    public static final String MAC = "1_";
    public static final int PROGRESS_HEIGHT = 40;
    public static final int PROGRESS_WIDTH = 40;
    public static final String RESOLUTION_160X50 = "160x50";
    public static final String RESOLUTION_320X380 = "320x380";
    public static final String RESOLUTION_320X50 = "320x50";
    public static final int THIRD_AD_TYPE_CLICK = 2;
    public static final int THIRD_AD_TYPE_SHOW = 1;
    public static final int VERSION = 5;
}
