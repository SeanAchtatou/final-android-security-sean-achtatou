package com.mobcent.ad.android.ui.widget;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

public abstract class BasePicView extends ImageView {
    protected Handler handler = new Handler();

    /* access modifiers changed from: protected */
    public abstract void loadPic(String str, AdProgress adProgress);

    /* access modifiers changed from: protected */
    public abstract void recyclePic();

    public BasePicView(Context context) {
        super(context);
    }

    public BasePicView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BasePicView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
