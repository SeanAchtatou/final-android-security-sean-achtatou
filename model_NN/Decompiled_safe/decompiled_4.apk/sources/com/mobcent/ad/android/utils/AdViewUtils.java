package com.mobcent.ad.android.utils;

import android.content.Context;
import android.widget.TextView;
import com.mobcent.ad.android.model.AdModel;
import com.mobcent.ad.android.ui.widget.AdImageView;
import com.mobcent.ad.android.ui.widget.BasePicView;
import com.mobcent.ad.android.ui.widget.gif.GifView;
import com.mobcent.forum.android.util.MCResource;
import com.mobcent.forum.android.util.PhoneUtil;

public class AdViewUtils {
    public static int getBannarsHeight(Context context) {
        return dipToPx(context, 50);
    }

    public static int dipToPx(Context context, int dipValue) {
        return PhoneUtil.getRawSize(context, 1, (float) dipValue);
    }

    public static TextView createAdText(Context context) {
        TextView tv = new TextView(context);
        tv.setTextColor(MCResource.getInstance(context).getColor("mc_ad_text_color"));
        tv.setTextSize(16.0f);
        tv.setGravity(17);
        return tv;
    }

    public static AdImageView createAdImage(Context context) {
        return new AdImageView(context);
    }

    public static BasePicView createPicView(Context context, AdModel adModel) {
        BasePicView basePicView;
        if (AdStringUtils.isGif(adModel.getPu())) {
            basePicView = new GifView(context);
        } else {
            basePicView = new AdImageView(context);
        }
        basePicView.setTag(adModel);
        return basePicView;
    }
}
