package com.mobcent.ad.android.model;

public class AdIntentModel extends AdBaseModel {
    private static final long serialVersionUID = -8451573007049048834L;
    private long aid;
    private int po;
    private String url;

    public long getAid() {
        return this.aid;
    }

    public void setAid(long aid2) {
        this.aid = aid2;
    }

    public int getPo() {
        return this.po;
    }

    public void setPo(int po2) {
        this.po = po2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }
}
