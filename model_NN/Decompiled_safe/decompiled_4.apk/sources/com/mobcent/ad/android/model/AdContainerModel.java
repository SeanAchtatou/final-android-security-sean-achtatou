package com.mobcent.ad.android.model;

import java.util.HashSet;

public class AdContainerModel extends AdBaseModel {
    private static final long serialVersionUID = 3591526408525297395L;
    private HashSet<AdModel> adSet = new HashSet<>();
    private int type;

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public HashSet<AdModel> getAdSet() {
        return this.adSet;
    }

    public void setAdSet(HashSet<AdModel> adSet2) {
        this.adSet = adSet2;
    }
}
