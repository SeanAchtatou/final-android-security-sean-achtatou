package com.mobcent.ad.android.model;

import java.util.ArrayList;
import java.util.List;

public class AdModel extends AdBaseModel {
    private static final long serialVersionUID = -870214963810034428L;
    private long aid;
    private int at;
    private int dt;
    private String du;
    private long gid;
    private int gt;
    private boolean isConsumed;
    private boolean isOther;
    private List<AdOtherModel> others = new ArrayList();
    private int po;
    private String pu;
    private String tel;
    private String tx;

    public long getAid() {
        return this.aid;
    }

    public void setAid(long aid2) {
        this.aid = aid2;
    }

    public int getAt() {
        return this.at;
    }

    public void setAt(int at2) {
        this.at = at2;
    }

    public int getDt() {
        return this.dt;
    }

    public void setDt(int dt2) {
        this.dt = dt2;
    }

    public String getDu() {
        return this.du;
    }

    public void setDu(String du2) {
        this.du = du2;
    }

    public long getGid() {
        return this.gid;
    }

    public void setGid(long gid2) {
        this.gid = gid2;
    }

    public int getGt() {
        return this.gt;
    }

    public void setGt(int gt2) {
        this.gt = gt2;
    }

    public String getPu() {
        return this.pu;
    }

    public void setPu(String pu2) {
        this.pu = pu2;
    }

    public String getTel() {
        return this.tel;
    }

    public void setTel(String tel2) {
        this.tel = tel2;
    }

    public String getTx() {
        return this.tx;
    }

    public void setTx(String tx2) {
        this.tx = tx2;
    }

    public boolean isConsumed() {
        return this.isConsumed;
    }

    public void setConsumed(boolean isConsumed2) {
        this.isConsumed = isConsumed2;
    }

    public int getPo() {
        return this.po;
    }

    public void setPo(int po2) {
        this.po = po2;
    }

    public boolean isOther() {
        return this.isOther;
    }

    public void setOther(boolean isOther2) {
        this.isOther = isOther2;
    }

    public List<AdOtherModel> getOthers() {
        return this.others;
    }

    public void setOthers(List<AdOtherModel> others2) {
        this.others = others2;
    }
}
