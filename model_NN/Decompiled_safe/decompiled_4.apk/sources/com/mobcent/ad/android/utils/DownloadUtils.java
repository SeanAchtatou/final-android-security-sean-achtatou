package com.mobcent.ad.android.utils;

import android.content.Context;
import com.mobcent.forum.android.util.MCLibIOUtil;

public class DownloadUtils {
    private static final String AD_DIR = "ad";
    private static String TAG = "DownloadUtils";

    public interface DownProgressDelegate {
        void setMax(int i);

        void setProgress(int i);
    }

    public static String getDownloadDirPath(Context context) {
        return MCLibIOUtil.createDirInCache(context, AD_DIR);
    }

    /* JADX INFO: Multiple debug info for r10v5 java.io.InputStream: [D('is' java.io.InputStream), D('raf' java.io.RandomAccessFile)] */
    /* JADX INFO: Multiple debug info for r9v9 java.lang.Exception: [D('is' java.io.InputStream), D('e' java.lang.Exception)] */
    /* JADX INFO: Multiple debug info for r0v11 int: [D('size' int), D('currentLength' int)] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0192  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0113 A[SYNTHETIC, Splitter:B:78:0x0113] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x011a A[Catch:{ Exception -> 0x0129 }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0120 A[SYNTHETIC, Splitter:B:84:0x0120] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x013d A[SYNTHETIC, Splitter:B:94:0x013d] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0143 A[Catch:{ Exception -> 0x014e }] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0149 A[Catch:{ Exception -> 0x014e }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:37:0x00a7=Splitter:B:37:0x00a7, B:74:0x0107=Splitter:B:74:0x0107} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean downloadFile(android.content.Context r8, java.lang.String r9, java.io.File r10, com.mobcent.ad.android.utils.DownloadUtils.DownProgressDelegate r11) {
        /*
            r0 = 0
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x003c }
            r1.<init>(r9)     // Catch:{ Exception -> 0x003c }
            r9 = 0
            r2 = 0
            r4 = 0
            java.io.File r0 = r10.getParentFile()     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            r0.mkdirs()     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            r10.createNewFile()     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            long r5 = r10.length()     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            int r0 = (int) r5     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            java.net.HttpURLConnection r9 = com.mobcent.forum.android.api.util.HttpClientUtil.getNewHttpURLConnection(r1, r8)     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            int r3 = r9.getContentLength()     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            if (r0 != r3) goto L_0x0051
            r0 = 1
            if (r4 == 0) goto L_0x01a6
            r4.close()     // Catch:{ Exception -> 0x0041 }
            r8 = 0
            r11 = r8
        L_0x002a:
            if (r2 == 0) goto L_0x01a3
            r2.close()     // Catch:{ Exception -> 0x018b }
            r8 = 0
            r10 = r8
        L_0x0031:
            if (r9 == 0) goto L_0x01a0
            r9.disconnect()     // Catch:{ Exception -> 0x018f }
            r8 = 0
        L_0x0037:
            r9 = r10
            r10 = r11
        L_0x0039:
            r8 = r1
            r9 = r0
        L_0x003b:
            return r9
        L_0x003c:
            r8 = move-exception
            r8 = 0
            r9 = r8
            r8 = r0
            goto L_0x003b
        L_0x0041:
            r8 = move-exception
            r11 = r4
            r10 = r2
        L_0x0044:
            java.lang.String r2 = com.mobcent.ad.android.utils.DownloadUtils.TAG
            java.lang.String r8 = r8.toString()
            com.mobcent.forum.android.util.MCLogUtil.e(r2, r8)
            r8 = r9
            r9 = r10
            r10 = r11
            goto L_0x0039
        L_0x0051:
            java.net.HttpURLConnection r9 = com.mobcent.forum.android.api.util.HttpClientUtil.getNewHttpURLConnection(r1, r8)     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            java.lang.String r8 = "Range"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            r5.<init>()     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            java.lang.String r6 = "bytes="
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            java.lang.String r6 = "-"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            java.lang.String r5 = r5.toString()     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            r9.addRequestProperty(r8, r5)     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            r11.setMax(r3)     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            r11.setProgress(r0)     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            java.io.InputStream r2 = r9.getInputStream()     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            java.io.RandomAccessFile r3 = new java.io.RandomAccessFile     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            java.lang.String r8 = "rwd"
            r3.<init>(r10, r8)     // Catch:{ MalformedURLException -> 0x017c, Exception -> 0x0101, all -> 0x0136 }
            long r4 = (long) r0
            r3.seek(r4)     // Catch:{ MalformedURLException -> 0x00a1, Exception -> 0x016f, all -> 0x015f }
            r8 = 102400(0x19000, float:1.43493E-40)
            byte[] r8 = new byte[r8]     // Catch:{ MalformedURLException -> 0x00a1, Exception -> 0x016f, all -> 0x015f }
            r10 = r0
        L_0x0092:
            int r0 = r2.read(r8)     // Catch:{ MalformedURLException -> 0x00a1, Exception -> 0x016f, all -> 0x015f }
            if (r0 <= 0) goto L_0x00c9
            r4 = 0
            r3.write(r8, r4, r0)     // Catch:{ MalformedURLException -> 0x00a1, Exception -> 0x016f, all -> 0x015f }
            int r10 = r10 + r0
            r11.setProgress(r10)     // Catch:{ MalformedURLException -> 0x00a1, Exception -> 0x016f, all -> 0x015f }
            goto L_0x0092
        L_0x00a1:
            r8 = move-exception
            r11 = r3
            r10 = r2
            r7 = r8
            r8 = r9
            r9 = r7
        L_0x00a7:
            java.lang.String r0 = com.mobcent.ad.android.utils.DownloadUtils.TAG     // Catch:{ all -> 0x0165 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0165 }
            com.mobcent.forum.android.util.MCLogUtil.e(r0, r9)     // Catch:{ all -> 0x0165 }
            r0 = 0
            if (r11 == 0) goto L_0x00b8
            r11.close()     // Catch:{ Exception -> 0x00f4 }
            r9 = 0
            r11 = r9
        L_0x00b8:
            if (r10 == 0) goto L_0x0194
            r10.close()     // Catch:{ Exception -> 0x00f4 }
            r9 = 0
        L_0x00be:
            if (r8 == 0) goto L_0x00c4
            r8.disconnect()     // Catch:{ Exception -> 0x0176 }
            r8 = 0
        L_0x00c4:
            r10 = r11
        L_0x00c5:
            r8 = r1
            r9 = r0
            goto L_0x003b
        L_0x00c9:
            r0 = 1
            if (r3 == 0) goto L_0x019d
            r3.close()     // Catch:{ Exception -> 0x00e4 }
            r8 = 0
            r11 = r8
        L_0x00d1:
            if (r2 == 0) goto L_0x019a
            r2.close()     // Catch:{ Exception -> 0x0184 }
            r8 = 0
            r10 = r8
        L_0x00d8:
            if (r9 == 0) goto L_0x0197
            r9.disconnect()     // Catch:{ Exception -> 0x0188 }
            r8 = 0
        L_0x00de:
            r9 = r10
            r10 = r11
        L_0x00e0:
            r8 = r1
            r9 = r0
            goto L_0x003b
        L_0x00e4:
            r8 = move-exception
            r11 = r3
            r10 = r2
        L_0x00e7:
            java.lang.String r2 = com.mobcent.ad.android.utils.DownloadUtils.TAG
            java.lang.String r8 = r8.toString()
            com.mobcent.forum.android.util.MCLogUtil.e(r2, r8)
            r8 = r9
            r9 = r10
            r10 = r11
            goto L_0x00e0
        L_0x00f4:
            r9 = move-exception
        L_0x00f5:
            java.lang.String r2 = com.mobcent.ad.android.utils.DownloadUtils.TAG
            java.lang.String r9 = r9.toString()
            com.mobcent.forum.android.util.MCLogUtil.e(r2, r9)
            r9 = r10
            r10 = r11
            goto L_0x00c5
        L_0x0101:
            r8 = move-exception
            r11 = r4
            r10 = r2
            r7 = r8
            r8 = r9
            r9 = r7
        L_0x0107:
            java.lang.String r0 = com.mobcent.ad.android.utils.DownloadUtils.TAG     // Catch:{ all -> 0x0165 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0165 }
            com.mobcent.forum.android.util.MCLogUtil.e(r0, r9)     // Catch:{ all -> 0x0165 }
            r0 = 0
            if (r11 == 0) goto L_0x0118
            r11.close()     // Catch:{ Exception -> 0x0129 }
            r9 = 0
            r11 = r9
        L_0x0118:
            if (r10 == 0) goto L_0x0192
            r10.close()     // Catch:{ Exception -> 0x0129 }
            r9 = 0
        L_0x011e:
            if (r8 == 0) goto L_0x0124
            r8.disconnect()     // Catch:{ Exception -> 0x016a }
            r8 = 0
        L_0x0124:
            r10 = r11
        L_0x0125:
            r8 = r1
            r9 = r0
            goto L_0x003b
        L_0x0129:
            r9 = move-exception
        L_0x012a:
            java.lang.String r2 = com.mobcent.ad.android.utils.DownloadUtils.TAG
            java.lang.String r9 = r9.toString()
            com.mobcent.forum.android.util.MCLogUtil.e(r2, r9)
            r9 = r10
            r10 = r11
            goto L_0x0125
        L_0x0136:
            r8 = move-exception
            r0 = r8
            r10 = r4
            r8 = r9
            r9 = r2
        L_0x013b:
            if (r10 == 0) goto L_0x0141
            r10.close()     // Catch:{ Exception -> 0x014e }
            r10 = 0
        L_0x0141:
            if (r9 == 0) goto L_0x0147
            r9.close()     // Catch:{ Exception -> 0x014e }
            r9 = 0
        L_0x0147:
            if (r8 == 0) goto L_0x014d
            r8.disconnect()     // Catch:{ Exception -> 0x014e }
            r8 = 0
        L_0x014d:
            throw r0
        L_0x014e:
            r11 = move-exception
            r7 = r11
            r11 = r10
            r10 = r9
            r9 = r7
            java.lang.String r1 = com.mobcent.ad.android.utils.DownloadUtils.TAG
            java.lang.String r9 = r9.toString()
            com.mobcent.forum.android.util.MCLogUtil.e(r1, r9)
            r9 = r10
            r10 = r11
            goto L_0x014d
        L_0x015f:
            r8 = move-exception
            r0 = r8
            r10 = r3
            r8 = r9
            r9 = r2
            goto L_0x013b
        L_0x0165:
            r9 = move-exception
            r0 = r9
            r9 = r10
            r10 = r11
            goto L_0x013b
        L_0x016a:
            r10 = move-exception
            r7 = r10
            r10 = r9
            r9 = r7
            goto L_0x012a
        L_0x016f:
            r8 = move-exception
            r11 = r3
            r10 = r2
            r7 = r8
            r8 = r9
            r9 = r7
            goto L_0x0107
        L_0x0176:
            r10 = move-exception
            r7 = r10
            r10 = r9
            r9 = r7
            goto L_0x00f5
        L_0x017c:
            r8 = move-exception
            r11 = r4
            r10 = r2
            r7 = r8
            r8 = r9
            r9 = r7
            goto L_0x00a7
        L_0x0184:
            r8 = move-exception
            r10 = r2
            goto L_0x00e7
        L_0x0188:
            r8 = move-exception
            goto L_0x00e7
        L_0x018b:
            r8 = move-exception
            r10 = r2
            goto L_0x0044
        L_0x018f:
            r8 = move-exception
            goto L_0x0044
        L_0x0192:
            r9 = r10
            goto L_0x011e
        L_0x0194:
            r9 = r10
            goto L_0x00be
        L_0x0197:
            r8 = r9
            goto L_0x00de
        L_0x019a:
            r10 = r2
            goto L_0x00d8
        L_0x019d:
            r11 = r3
            goto L_0x00d1
        L_0x01a0:
            r8 = r9
            goto L_0x0037
        L_0x01a3:
            r10 = r2
            goto L_0x0031
        L_0x01a6:
            r11 = r4
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.ad.android.utils.DownloadUtils.downloadFile(android.content.Context, java.lang.String, java.io.File, com.mobcent.ad.android.utils.DownloadUtils$DownProgressDelegate):boolean");
    }
}
