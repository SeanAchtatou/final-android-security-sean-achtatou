package com.mobcent.ad.android.task.manager;

import android.content.Context;
import android.os.AsyncTask;
import com.mobcent.ad.android.model.AdDownDbModel;
import com.mobcent.ad.android.task.DownTask;
import com.mobcent.forum.android.util.StringUtil;
import java.util.HashMap;
import java.util.Map;

public class DownTaskManager {
    private static Map<String, DownTask> downloadTaskMap;
    private static DownTaskManager manager;
    private String TAG = "DownloadTaskManager";

    public static DownTaskManager getInastance() {
        if (manager == null) {
            manager = new DownTaskManager();
            downloadTaskMap = new HashMap();
        }
        return manager;
    }

    public synchronized void downloadApk(Context context, AdDownDbModel adDownDbModel) {
        String key = StringUtil.getMD5Str(adDownDbModel.getUrl());
        if (downloadTaskMap.get(key) == null || downloadTaskMap.get(key).getStatus() != AsyncTask.Status.RUNNING) {
            if (downloadTaskMap.get(key) == null || downloadTaskMap.get(key).getStatus() == AsyncTask.Status.FINISHED) {
                if (downloadTaskMap.get(key) != null) {
                    downloadTaskMap.remove(key);
                }
                DownTask task = new DownTask(context, adDownDbModel);
                downloadTaskMap.put(key, task);
                task.execute(new Void[0]);
            } else {
                downloadTaskMap.get(key).execute(new Void[0]);
            }
        }
    }

    public void removeTask(String downloadUrl) {
        try {
            String key = StringUtil.getMD5Str(downloadUrl);
            if (downloadTaskMap.get(key) != null) {
                downloadTaskMap.remove(key);
            }
        } catch (Exception e) {
        }
    }

    public boolean isHaveTaskRunning() {
        for (String key : downloadTaskMap.keySet()) {
            DownTask task = downloadTaskMap.get(key);
            if (task != null && task.getStatus() == AsyncTask.Status.RUNNING) {
                return true;
            }
        }
        return false;
    }
}
