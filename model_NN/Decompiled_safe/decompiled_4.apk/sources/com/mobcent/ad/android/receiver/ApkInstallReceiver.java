package com.mobcent.ad.android.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.mobcent.ad.android.db.DownSqliteHelper;
import com.mobcent.ad.android.model.AdDownDbModel;
import com.mobcent.ad.android.task.ActiveDoTask;
import com.mobcent.ad.android.utils.ApkUtil;

public class ApkInstallReceiver extends BroadcastReceiver {
    private String TAG = "ApkInstallReceiver";

    public void onReceive(Context context, Intent intent) {
        AdDownDbModel adDownModel;
        String action = intent.getAction();
        String packageName = intent.getData().getSchemeSpecificPart();
        if ("android.intent.action.PACKAGE_ADDED".equals(action) && (adDownModel = DownSqliteHelper.getInstance(context).queryByPackageName(packageName)) != null) {
            new ActiveDoTask(context, adDownModel).execute(new Void[0]);
            ApkUtil.launchApk(context, packageName);
        }
    }
}
