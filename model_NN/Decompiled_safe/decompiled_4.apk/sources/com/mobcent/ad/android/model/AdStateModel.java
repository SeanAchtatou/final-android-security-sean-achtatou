package com.mobcent.ad.android.model;

public class AdStateModel {
    private int dtType;
    private boolean haveAd;

    public boolean isHaveAd() {
        return this.haveAd;
    }

    public void setHaveAd(boolean haveAd2) {
        this.haveAd = haveAd2;
    }

    public int getDtType() {
        return this.dtType;
    }

    public void setDtType(int dtType2) {
        this.dtType = dtType2;
    }
}
