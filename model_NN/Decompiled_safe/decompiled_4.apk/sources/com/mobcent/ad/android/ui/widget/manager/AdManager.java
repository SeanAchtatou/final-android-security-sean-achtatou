package com.mobcent.ad.android.ui.widget.manager;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import com.baidu.location.LocationClientOption;
import com.mobcent.ad.android.cache.AdDatasCache;
import com.mobcent.ad.android.cache.AdPositionsCache;
import com.mobcent.ad.android.constant.AdConstant;
import com.mobcent.ad.android.db.DownSqliteHelper;
import com.mobcent.ad.android.model.AdContainerModel;
import com.mobcent.ad.android.model.AdDownDbModel;
import com.mobcent.ad.android.model.RequestParamsModel;
import com.mobcent.ad.android.service.impl.AdServiceImpl;
import com.mobcent.ad.android.task.ActiveDoTask;
import com.mobcent.ad.android.task.DownDoTask;
import com.mobcent.ad.android.ui.widget.delegate.AdManagerListener;
import com.mobcent.forum.android.util.AppUtil;
import com.mobcent.forum.android.util.PhoneUtil;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdManager {
    private static AdManager adHelper;
    public String TAG = "AdManager";
    private Map<String, Map<Integer, AdContainerModel>> activityAdCache = new HashMap();
    private AdDatasCache adDatasCache;
    private AdPositionsCache adPositionsCache;
    private Set<Long> exposureSet = new HashSet();
    /* access modifiers changed from: private */
    public AsyncTask<Context, Void, AdPositionsCache> haveAdTask = null;
    /* access modifiers changed from: private */
    public boolean haveNoNet = false;
    private AsyncTask<Context, Void, AdDatasCache> requestAdTask = null;
    /* access modifiers changed from: private */
    public long requestAdTimeMillis;
    private RequestParamsModel requestParams;

    public static AdManager getInstance() {
        if (adHelper == null) {
            adHelper = new AdManager();
        }
        return adHelper;
    }

    public boolean isHaveNoNet() {
        return this.haveNoNet;
    }

    public void setHaveNoNet(boolean haveNoNet2) {
        this.haveNoNet = haveNoNet2;
    }

    public Set<Long> getExposureSet() {
        return this.exposureSet;
    }

    public AsyncTask<Context, Void, AdPositionsCache> getHaveAdTask() {
        return this.haveAdTask;
    }

    public void setHaveAdTask(AsyncTask<Context, Void, AdPositionsCache> haveAdTask2) {
        this.haveAdTask = haveAdTask2;
    }

    public synchronized AsyncTask<Context, Void, AdDatasCache> getRequestAdTask() {
        return this.requestAdTask;
    }

    public void setRequestAdTask(AsyncTask<Context, Void, AdDatasCache> requestAdTask2) {
        this.requestAdTask = requestAdTask2;
    }

    public RequestParamsModel getRequestParams() {
        return this.requestParams;
    }

    public AdPositionsCache getAdPositionsCache() {
        return this.adPositionsCache;
    }

    public void setAdPositionsCache(AdPositionsCache adPositionsCache2) {
        this.adPositionsCache = adPositionsCache2;
    }

    public AdDatasCache getAdDatasCache() {
        return this.adDatasCache;
    }

    public void setAdDatasCache(AdDatasCache adDatasCache2) {
        this.adDatasCache = adDatasCache2;
        if (adDatasCache2 != null) {
            this.requestAdTimeMillis = System.currentTimeMillis();
        }
    }

    public Map<String, Map<Integer, AdContainerModel>> getActivityAdCache() {
        return this.activityAdCache;
    }

    public void setActivityAdCache(Map<String, Map<Integer, AdContainerModel>> activityAdCache2) {
        this.activityAdCache = activityAdCache2;
    }

    public void recyclAdByTag(String tag) {
        if (this.activityAdCache.get(tag) != null) {
            this.activityAdCache.remove(tag);
        }
    }

    public void init(Context context, String appKey, String chanelNum, long userId) {
        this.requestParams = new RequestParamsModel();
        this.requestParams.setIm(PhoneUtil.getIMEI(context));
        this.requestParams.setPt(AdConstant.ANDROID_OS + PhoneUtil.getSDKVersion());
        this.requestParams.setMc(AdConstant.MAC + PhoneUtil.getLocalMacAddress(context));
        this.requestParams.setSs(PhoneUtil.getResolution(context));
        this.requestParams.setUa(PhoneUtil.getPhoneType());
        this.requestParams.setZo(PhoneUtil.getPhoneLanguage());
        this.requestParams.setPn(AppUtil.getPackageName(context));
        this.requestParams.setAk(appKey);
        this.requestParams.setCh(chanelNum);
        this.requestParams.setUid(userId);
        requestHaveAd(context);
        sendDownDo(context);
        sendActiveDo(context);
    }

    public void requestHaveAd(Context context) {
        if (this.adPositionsCache == null && this.haveAdTask == null) {
            this.haveAdTask = new HaveAdTask().execute(context);
        }
    }

    public void requestAdDatas(Context context, boolean isForce) {
        if (this.requestAdTask == null || this.adDatasCache == null || (isForce && this.requestAdTask.getStatus() != AsyncTask.Status.RUNNING)) {
            this.requestAdTask = new GetAdTask().execute(context);
        }
    }

    private void sendActiveDo(Context context) {
        for (AdDownDbModel dbModel : DownSqliteHelper.getInstance(context).queryUnActiveDoList()) {
            new ActiveDoTask(context, dbModel).execute(new Void[0]);
        }
    }

    private void sendDownDo(Context context) {
        for (AdDownDbModel dbModel : DownSqliteHelper.getInstance(context).queryUnDownDoList()) {
            new DownDoTask(context, dbModel).execute(new Void[0]);
        }
    }

    public void getHaveAdPositions(final int[] positions, final AdManagerListener listener) {
        if (this.haveAdTask.getStatus() != AsyncTask.Status.FINISHED) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    AdManager.this.getHaveAdPositions(positions, listener);
                }
            }, 1000);
        } else if (this.adPositionsCache != null && listener != null) {
            listener.setPositions(this.adPositionsCache.isHaveAd(positions));
        }
    }

    class HaveAdTask extends AsyncTask<Context, Void, AdPositionsCache> {
        private Context context;

        HaveAdTask() {
        }

        /* access modifiers changed from: protected */
        public AdPositionsCache doInBackground(Context... params) {
            this.context = params[0];
            return new AdServiceImpl(this.context).haveAd();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(AdPositionsCache result) {
            if (result == null) {
                boolean unused = AdManager.this.haveNoNet = false;
            } else if (result.isConectFailed()) {
                AsyncTask unused2 = AdManager.this.haveAdTask = null;
                boolean unused3 = AdManager.this.haveNoNet = true;
            } else {
                AdManager.this.setAdPositionsCache(result);
                boolean unused4 = AdManager.this.haveNoNet = false;
            }
        }
    }

    class GetAdTask extends AsyncTask<Context, Void, AdDatasCache> {
        GetAdTask() {
        }

        /* access modifiers changed from: protected */
        public AdDatasCache doInBackground(Context... params) {
            return new AdServiceImpl(params[0]).getAd();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(AdDatasCache result) {
            if (result != null) {
                AdManager.this.setAdDatasCache(null);
                AdManager.this.getExposureSet().clear();
                AdManager.this.setAdDatasCache(result);
                return;
            }
            long unused = AdManager.this.requestAdTimeMillis = System.currentTimeMillis();
        }
    }

    public boolean isOverDue() {
        if (this.haveAdTask == null || this.haveAdTask.getStatus() != AsyncTask.Status.FINISHED || System.currentTimeMillis() - this.requestAdTimeMillis <= ((long) (this.adPositionsCache.getCycle() * LocationClientOption.MIN_SCAN_SPAN * 60))) {
            return false;
        }
        return true;
    }
}
