package com.mobcent.ad.android.ui.widget;

import android.content.Context;
import android.graphics.Bitmap;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;

public class AdImageView extends BasePicView {
    private String TAG = "AdImageView";

    public AdImageView(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void loadPic(String url, final AdProgress progress) {
        if (url != null && !"".equals(url)) {
            if (progress != null) {
                progress.show();
            }
            AsyncTaskLoaderImage.getInstance(getContext(), this.TAG).loadAsync(url, new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap bitmap, String url) {
                    AdImageView.this.handler.post(new Runnable() {
                        public void run() {
                            if (progress != null) {
                                progress.hide();
                            }
                            if (bitmap != null && !bitmap.isRecycled()) {
                                AdImageView.this.setImageBitmap(bitmap);
                            }
                        }
                    });
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void recyclePic() {
        setImageBitmap(null);
    }
}
