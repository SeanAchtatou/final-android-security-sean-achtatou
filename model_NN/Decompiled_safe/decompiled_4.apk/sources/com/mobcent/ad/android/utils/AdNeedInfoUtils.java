package com.mobcent.ad.android.utils;

import android.content.Context;
import android.os.Environment;
import com.mobcent.forum.android.db.LocationDBUtil;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.model.LocationModel;
import com.mobcent.forum.android.util.PhoneConnectionUtil;
import com.mobcent.forum.android.util.StringUtil;

public class AdNeedInfoUtils {
    public static String getNetworkType(Context context) {
        if (PhoneConnectionUtil.getNetworkType(context).equals("wifi")) {
            return "1";
        }
        if (PhoneConnectionUtil.isConnectChinaMobile(context)) {
            return "2";
        }
        if (PhoneConnectionUtil.isConnectChinaUnicom(context)) {
            return "3";
        }
        if (PhoneConnectionUtil.isConnectChinaTelecom(context)) {
            return "4";
        }
        return "";
    }

    public static String getJWD(Context context) {
        LocationModel location = LocationDBUtil.getInstance(context).getLocationInfo(0);
        if (location == null) {
            return "";
        }
        return location.getLongitude() + "_" + location.getLatitude();
    }

    public static String getLocation(Context context) {
        LocationModel location = LocationDBUtil.getInstance(context).getLocationInfo(SharedPreferencesDB.getInstance(context).getUserId());
        if (location == null || StringUtil.isEmpty(location.getAddrStr())) {
            return "";
        }
        return location.getAddrStr();
    }

    public static String getNet(Context context) {
        String netWorkType = PhoneConnectionUtil.getNetworkType(context).toLowerCase();
        if (netWorkType.equals("cmwap")) {
            return "1";
        }
        if (netWorkType.equals("cmnet")) {
            return "2";
        }
        if (netWorkType.equals("ctwap")) {
            return "3";
        }
        if (netWorkType.equals("ctnet")) {
            return "4";
        }
        if (netWorkType.equals("uniwap")) {
            return "5";
        }
        if (netWorkType.equals("uninet")) {
            return "6";
        }
        if (netWorkType.equals("3gwap")) {
            return "7";
        }
        if (netWorkType.equals("3gnet")) {
            return "8";
        }
        return "0";
    }

    public static int getSd() {
        String state = Environment.getExternalStorageState();
        if ("mounted".equals(state)) {
            return 1;
        }
        if ("mounted_ro".equals(state)) {
            return 0;
        }
        return 0;
    }
}
