package com.mobcent.ad.android.service.impl;

import android.content.Context;
import com.mobcent.ad.android.api.AdApiRequester;
import com.mobcent.ad.android.cache.AdDatasCache;
import com.mobcent.ad.android.cache.AdPositionsCache;
import com.mobcent.ad.android.service.AdService;
import com.mobcent.ad.android.service.impl.helper.AdServiceImplHelper;

public class AdServiceImpl implements AdService {
    private Context context;

    public AdServiceImpl(Context context2) {
        this.context = context2;
    }

    public AdPositionsCache haveAd() {
        String jsonStr = AdApiRequester.haveAd(this.context);
        if (!jsonStr.equals("connection_fail")) {
            return AdServiceImplHelper.parseAdPositions(jsonStr);
        }
        AdPositionsCache ac = new AdPositionsCache();
        ac.setConectFailed(true);
        return ac;
    }

    public AdDatasCache getAd() {
        return AdServiceImplHelper.parseAdDatas(AdApiRequester.getAd(this.context));
    }

    public String getLinkUrl(long aid, long po, String du) {
        return AdApiRequester.getLinkDoUrl(this.context, aid, po, du);
    }

    public boolean downAdDo(long aid, int po, String app, String date) {
        return AdServiceImplHelper.parseSucc(AdApiRequester.downAd(this.context, aid, po, app, date));
    }

    public boolean activeAdDo(long aid, int po, String app, String date) {
        return AdServiceImplHelper.parseSucc(AdApiRequester.activeAd(this.context, aid, po, app, date));
    }

    public void exposureAd(String url) {
        AdApiRequester.doGetRequest(this.context, url);
    }
}
