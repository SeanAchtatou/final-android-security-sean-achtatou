package com.mobcent.ad.android.ui.widget;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.mobcent.ad.android.cache.AdDatasCache;
import com.mobcent.ad.android.constant.AdConstant;
import com.mobcent.ad.android.model.AdContainerModel;
import com.mobcent.ad.android.model.AdStateModel;
import com.mobcent.ad.android.ui.widget.delegate.AdViewDelegate;
import com.mobcent.ad.android.ui.widget.helper.AdViewHelper;
import com.mobcent.ad.android.ui.widget.manager.AdManager;
import com.mobcent.ad.android.utils.AdViewUtils;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.MCResource;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class AdView extends RelativeLayout implements AdConstant {
    private String TAG = "AdView";
    private AdContainerModel adContainerModel = null;
    private AdManager adManager;
    /* access modifiers changed from: private */
    public int adPosition;
    private AdStateModel adStateModel;
    private int contentViewId = 1;
    /* access modifiers changed from: private */
    public long delay = -1;
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    private boolean hideExtendImg;
    private boolean hideLineView;
    private boolean isTimerTaskRun = false;
    private int itemPosition = -1;
    private String listTag = null;
    private RelativeLayout.LayoutParams lps;
    private MCResource resource;
    private Timer timer = null;
    private TimerTask timerTask = new TimerTask() {
        public void run() {
            AdView.this.handler.post(new Runnable() {
                public void run() {
                    AdView.this.showAd(AdView.this.adPosition, AdView.this.delay);
                }
            });
        }
    };

    public AdView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(Context context) {
        setBackgroundColor(-1);
        this.adManager = AdManager.getInstance();
        this.resource = MCResource.getInstance(context);
        this.lps = new RelativeLayout.LayoutParams(-1, AdViewUtils.getBannarsHeight(getContext()));
        setVisibility(8);
    }

    public synchronized void showAd(int adPosition2) {
        this.adPosition = adPosition2;
        clearSource();
        try {
            requestAd();
        } catch (Exception e) {
            MCLogUtil.e(this.TAG, e.toString());
        }
        return;
    }

    public synchronized void showAd(int adPosition2, long delay2) {
        this.adPosition = adPosition2;
        this.delay = delay2;
        clearSource();
        if (this.timer == null) {
            this.timer = new Timer();
        }
        try {
            requestAd();
        } catch (Exception e) {
            MCLogUtil.e(this.TAG, e.toString());
        }
        return;
    }

    public synchronized void showAd(String tag, int adPosition2, int itemPosition2) {
        this.adPosition = adPosition2;
        this.itemPosition = itemPosition2;
        this.listTag = tag;
        clearSource();
        Map<String, Map<Integer, AdContainerModel>> activityCache = this.adManager.getActivityAdCache();
        if (activityCache.get(this.listTag) == null) {
            activityCache.put(this.listTag, new HashMap());
        }
        Map<Integer, AdContainerModel> adCache = activityCache.get(this.listTag);
        if (adCache.get(Integer.valueOf(itemPosition2)) != null) {
            this.adContainerModel = (AdContainerModel) adCache.get(Integer.valueOf(itemPosition2));
            createAdViewByCache(this.adContainerModel);
        } else {
            try {
                requestAd();
            } catch (Exception e) {
                MCLogUtil.e(this.TAG, e.toString());
            }
        }
        return;
    }

    private void requestAd() {
        if (this.adManager.getAdPositionsCache() != null) {
            this.adStateModel = this.adManager.getAdPositionsCache().getAdState(this.adPosition);
            if (this.adStateModel == null) {
                return;
            }
            if (this.adManager.getAdDatasCache() == null) {
                AsyncTask<Context, Void, AdDatasCache> adTask = this.adManager.getRequestAdTask();
                if (adTask == null) {
                    this.adManager.requestAdDatas(getContext(), false);
                } else if (adTask.getStatus() == AsyncTask.Status.FINISHED) {
                    return;
                }
                postDelayRequestAd();
            } else if (this.adManager.isOverDue()) {
                this.adManager.requestAdDatas(getContext(), true);
                postDelayRequestAd();
            } else if (!this.adManager.getAdDatasCache().getAdDataMap().isEmpty()) {
                createAdView(this.adStateModel.getDtType());
            }
        } else if ((this.adManager.getHaveAdTask() != null && this.adManager.getHaveAdTask().getStatus() == AsyncTask.Status.RUNNING) || this.adManager.isHaveNoNet()) {
            if (this.adManager.isHaveNoNet()) {
                this.adManager.requestHaveAd(getContext());
            }
            postDelayRequestAd();
        }
    }

    private void createAdView(int adType) {
        View view = null;
        switch (adType) {
            case 1:
                this.adContainerModel = this.adManager.getAdDatasCache().createAdContainerModel(1);
                view = createMulityView(this.adContainerModel);
                break;
            case 2:
                this.adContainerModel = this.adManager.getAdDatasCache().createAdContainerModel(2);
                view = createTwoShortTextView(this.adContainerModel);
                break;
            case 3:
                this.adContainerModel = this.adManager.getAdDatasCache().createAdContainerModel(3);
                view = createBigImgView(this.adContainerModel);
                break;
        }
        if (!(this.adContainerModel == null || this.itemPosition == -1 || this.listTag == null)) {
            Map<Integer, AdContainerModel> adCache = this.adManager.getActivityAdCache().get(this.listTag);
            if (adCache == null) {
                this.adManager.getActivityAdCache().put(this.listTag, new HashMap());
            }
            try {
                adCache.put(Integer.valueOf(this.itemPosition), this.adContainerModel);
            } catch (Exception e) {
                MCLogUtil.e(this.TAG, e.toString());
                free();
                return;
            }
        }
        initAdExtendView(view);
        AdViewHelper.getInstance(getContext()).exposureAd(this.adContainerModel);
    }

    private void createAdViewByCache(AdContainerModel adContainerModel2) {
        View view = null;
        int type = adContainerModel2.getType();
        if (type == 3 || type == 1 || type == 2 || type == 4) {
            view = createMulityView(adContainerModel2);
        } else if (type == 7) {
            view = createTwoShortTextView(adContainerModel2);
        } else if (type == 9) {
            view = createBigImgView(adContainerModel2);
        }
        initAdExtendView(view);
        AdViewHelper.getInstance(getContext()).exposureAd(adContainerModel2);
    }

    private View createMulityView(AdContainerModel adContainerModel2) {
        if (adContainerModel2 == null) {
            return null;
        }
        MultiTypeView miltiView = new MultiTypeView(getContext());
        miltiView.setAdContainerModel(adContainerModel2, this.adPosition);
        this.lps = new RelativeLayout.LayoutParams(-1, AdViewUtils.getBannarsHeight(getContext()));
        addView(miltiView, this.lps);
        return miltiView;
    }

    private View createBigImgView(AdContainerModel adContainerModel2) {
        if (adContainerModel2 == null) {
            return null;
        }
        BigPicTypeView bigImg = new BigPicTypeView(getContext());
        bigImg.setAdContainerModel(adContainerModel2, this.adPosition);
        this.lps = new RelativeLayout.LayoutParams(-1, -1);
        addView(bigImg, this.lps);
        return bigImg;
    }

    private View createTwoShortTextView(AdContainerModel adContainerModel2) {
        if (adContainerModel2 == null) {
            return null;
        }
        TextTypeView twoShortText = new TextTypeView(getContext());
        twoShortText.setAdContainerModel(adContainerModel2, this.adPosition);
        this.lps = new RelativeLayout.LayoutParams(-1, -1);
        addView(twoShortText, this.lps);
        return twoShortText;
    }

    private void initAdExtendView(View view) {
        if (view == null) {
            setVisibility(8);
            return;
        }
        if (!this.hideExtendImg) {
            ImageView extendImg = new ImageView(getContext());
            extendImg.setBackgroundResource(this.resource.getDrawableId("mc_ad_img"));
            RelativeLayout.LayoutParams lps2 = new RelativeLayout.LayoutParams(AdViewUtils.dipToPx(getContext(), 35), AdViewUtils.dipToPx(getContext(), 12));
            lps2.addRule(11);
            addView(extendImg, lps2);
        }
        if (view != null && isTextAd()) {
            view.setId(this.contentViewId);
            if (!this.hideLineView) {
                View lineView = new View(getContext());
                lineView.setBackgroundResource(this.resource.getDrawableId("mc_ad_line"));
                this.lps = new RelativeLayout.LayoutParams(-1, AdViewUtils.dipToPx(getContext(), 2));
                this.lps.addRule(8, view.getId());
                addView(lineView, this.lps);
            }
        }
        if (!(view == null || this.delay == -1 || this.isTimerTaskRun)) {
            this.isTimerTaskRun = true;
            try {
                this.timer.schedule(this.timerTask, 2000, this.delay);
            } catch (Exception e) {
                MCLogUtil.e(this.TAG, "" + e.toString());
            }
        }
        view.postInvalidate();
        postInvalidate();
        setVisibility(0);
    }

    private boolean isTextAd() {
        int type;
        if (this.adContainerModel == null || ((type = this.adContainerModel.getType()) != 1 && type != 2 && type != 7)) {
            return false;
        }
        return true;
    }

    private void postDelayRequestAd() {
    }

    private void clearSource() {
        View contentView = getChildAt(0);
        if (((contentView instanceof BigPicTypeView) || (contentView instanceof MultiTypeView)) && contentView != null) {
            ((AdViewDelegate) contentView).freeMemery();
        }
        removeAllViews();
    }

    public void hideExtendImg(boolean isHide) {
        this.hideExtendImg = isHide;
    }

    public void hideLineView(boolean isHide) {
        this.hideLineView = isHide;
    }

    public void free() {
        if (this.timer != null) {
            this.timer.cancel();
            this.timerTask.cancel();
            this.isTimerTaskRun = false;
        }
        View contentView = getChildAt(0);
        if (((contentView instanceof BigPicTypeView) || (contentView instanceof MultiTypeView)) && contentView != null) {
            ((AdViewDelegate) contentView).freeMemery();
        }
        removeAllViews();
        setVisibility(8);
    }
}
