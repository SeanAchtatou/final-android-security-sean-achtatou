package com.mobcent.ad.android.ui.widget.delegate;

public interface PicViewOption {
    void loadPic();

    void recyclePic();
}
