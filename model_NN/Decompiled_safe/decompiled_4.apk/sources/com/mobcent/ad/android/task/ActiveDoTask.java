package com.mobcent.ad.android.task;

import android.content.Context;
import com.mobcent.ad.android.db.DownSqliteHelper;
import com.mobcent.ad.android.model.AdDownDbModel;
import com.mobcent.forum.android.util.MCLogUtil;

public class ActiveDoTask extends BaseTask<Void, Void, Boolean> {
    private String TAG = "ActiveDoTask";
    private AdDownDbModel adDbModel;
    private DownSqliteHelper sqliteHelper;

    public ActiveDoTask(Context context, AdDownDbModel adDbModel2) {
        super(context);
        this.adDbModel = adDbModel2;
        this.sqliteHelper = DownSqliteHelper.getInstance(context);
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(Void... params) {
        this.adDbModel = this.sqliteHelper.queryByPackageName(this.adDbModel.getPn());
        MCLogUtil.e(this.TAG, this.adDbModel.toString());
        if (this.adDbModel == null || this.adDbModel.getActiveDo() == 1) {
            return false;
        }
        return Boolean.valueOf(this.adService.activeAdDo(this.adDbModel.getAid(), this.adDbModel.getPo(), this.adDbModel.getPn(), this.adDbModel.getDate()));
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean result) {
        if (result.booleanValue()) {
            this.adDbModel.setActiveDo(1);
            this.sqliteHelper.update(this.adDbModel);
        }
    }
}
