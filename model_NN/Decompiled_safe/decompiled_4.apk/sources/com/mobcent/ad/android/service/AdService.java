package com.mobcent.ad.android.service;

import com.mobcent.ad.android.cache.AdDatasCache;
import com.mobcent.ad.android.cache.AdPositionsCache;

public interface AdService {
    boolean activeAdDo(long j, int i, String str, String str2);

    boolean downAdDo(long j, int i, String str, String str2);

    void exposureAd(String str);

    AdDatasCache getAd();

    String getLinkUrl(long j, long j2, String str);

    AdPositionsCache haveAd();
}
