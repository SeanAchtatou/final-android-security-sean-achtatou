package com.mobcent.ad.android.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.mobcent.ad.android.model.AdDownDbModel;
import com.mobcent.ad.android.task.manager.DownTaskManager;
import com.mobcent.forum.android.util.AppUtil;

public class DownFailedReceiver extends BroadcastReceiver {
    public static final String AD_DOWNLOAD_MODEL = "adDownloadModel";
    private String TAG = "DownFailedReceiver";
    private AdDownDbModel adDownDbModel = null;

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(getAction(context))) {
            this.adDownDbModel = null;
            this.adDownDbModel = (AdDownDbModel) intent.getSerializableExtra(AD_DOWNLOAD_MODEL);
            DownTaskManager.getInastance().downloadApk(context, this.adDownDbModel);
        }
    }

    public static String getAction(Context context) {
        return AppUtil.getPackageName(context) + "." + DownFailedReceiver.class.getName();
    }
}
