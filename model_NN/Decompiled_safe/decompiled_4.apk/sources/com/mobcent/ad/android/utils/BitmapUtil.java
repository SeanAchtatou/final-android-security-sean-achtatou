package com.mobcent.ad.android.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import com.mobcent.forum.android.model.BitmapModel;
import com.mobcent.forum.android.util.PhoneUtil;

public class BitmapUtil {
    public static BitmapModel getBitmapWidHeightByScreen(Context context, Bitmap bitmap, boolean doOverScreen) {
        return getBitmapWidHeightByScreen(context, bitmap.getWidth(), bitmap.getHeight(), doOverScreen, 1);
    }

    public static BitmapModel getBitmapWidHeightByScreen(Context context, int width, int height, boolean doOverScreen, int num) {
        int screenWidth = PhoneUtil.getDisplayWidth((Activity) context) / num;
        int screenHeight = PhoneUtil.getDisplayHeight((Activity) context) / num;
        float widthPoint = ((float) width) / ((float) screenWidth);
        float heightPoint = ((float) height) / ((float) screenHeight);
        float h_w = ((float) height) / ((float) width);
        float w_h = ((float) width) / ((float) height);
        BitmapModel bitmapModel = new BitmapModel();
        if (widthPoint > 1.0f || heightPoint > 1.0f || !doOverScreen) {
            if (widthPoint <= 1.0f && heightPoint <= 1.0f && !doOverScreen) {
                bitmapModel.setWidth(width);
                bitmapModel.setHeight(height);
            } else if (widthPoint > 1.0f && heightPoint < 1.0f) {
                bitmapModel.setWidth(screenWidth);
                bitmapModel.setHeight((int) (((float) screenWidth) * h_w));
            } else if (widthPoint < 1.0f && heightPoint > 1.0f) {
                bitmapModel.setHeight(screenHeight);
                bitmapModel.setWidth((int) (((float) screenHeight) * w_h));
            } else if (widthPoint > 1.0f && heightPoint > 1.0f) {
                if (widthPoint >= heightPoint) {
                    bitmapModel.setWidth(screenWidth);
                    bitmapModel.setHeight((int) (((float) screenWidth) * h_w));
                } else {
                    bitmapModel.setHeight(screenHeight);
                    bitmapModel.setWidth((int) (((float) screenHeight) * w_h));
                }
            }
        } else if (widthPoint >= heightPoint) {
            bitmapModel.setWidth(screenWidth);
            bitmapModel.setHeight((int) (((float) screenWidth) * h_w));
        } else if (widthPoint < heightPoint) {
            bitmapModel.setHeight(screenHeight);
            bitmapModel.setWidth((int) (((float) screenHeight) * w_h));
        }
        return bitmapModel;
    }
}
