package com.mobcent.ad.android.ui.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.mobcent.ad.android.db.DownSqliteHelper;
import com.mobcent.ad.android.model.AdApkModel;
import com.mobcent.ad.android.model.AdDownDbModel;
import com.mobcent.ad.android.model.AdIntentModel;
import com.mobcent.ad.android.task.manager.DownTaskManager;
import com.mobcent.ad.android.utils.AdStringUtils;
import com.mobcent.ad.android.utils.ApkUtil;
import com.mobcent.forum.android.util.AppUtil;
import com.mobcent.forum.android.util.MCLibIOUtil;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.MCResource;
import com.mobcent.forum.android.util.StringUtil;

public class WebViewActivity extends Activity {
    public static final String AD_INTENT_MODEL = "adModel";
    private String TAG = "WebViewActivity";
    private AdIntentModel adIntentModel;
    private Button backBtn;
    private Button closeBtn;
    private DownTaskManager downTaskManager;
    private Button forwardBtn;
    private Button refreshBtn;
    private MCResource resource;
    private DownSqliteHelper sqliteHelper;
    /* access modifiers changed from: private */
    public WebView webView;
    /* access modifiers changed from: private */
    public ProgressBar webviewProgressbar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        initData();
        initViews();
        initWidgetActions();
        overridePendingTransition(17432576, 17432577);
    }

    /* access modifiers changed from: protected */
    public void initData() {
        try {
            this.adIntentModel = (AdIntentModel) getIntent().getSerializableExtra(AD_INTENT_MODEL);
        } catch (Exception e) {
            MCLogUtil.e(this.TAG, e.toString());
        }
        if (this.adIntentModel == null || StringUtil.isEmpty(this.adIntentModel.getUrl())) {
            finish();
            overridePendingTransition(0, 17432577);
        }
        this.resource = MCResource.getInstance(this);
        this.sqliteHelper = DownSqliteHelper.getInstance(this);
        this.downTaskManager = DownTaskManager.getInastance();
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        requestWindowFeature(1);
        setContentView(this.resource.getLayoutId("mc_ad_web_view"));
        this.webviewProgressbar = (ProgressBar) findViewById(this.resource.getViewId("progress_bar"));
        this.backBtn = (Button) findViewById(this.resource.getViewId("back_btn"));
        this.forwardBtn = (Button) findViewById(this.resource.getViewId("forward_btn"));
        this.refreshBtn = (Button) findViewById(this.resource.getViewId("refresh_btn"));
        this.closeBtn = (Button) findViewById(this.resource.getViewId("close_btn"));
        this.webView = (WebView) findViewById(this.resource.getViewId("web_view"));
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setPluginsEnabled(true);
        this.webView.getSettings().setSupportZoom(true);
        this.webView.getSettings().setBuiltInZoomControls(true);
        this.webView.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        this.webView.setWebViewClient(new MyWebViewClient());
        this.webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    WebViewActivity.this.webviewProgressbar.setVisibility(8);
                } else {
                    WebViewActivity.this.webviewProgressbar.setVisibility(0);
                }
                WebViewActivity.this.webviewProgressbar.setProgress(progress);
            }
        });
        this.webView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                WebViewActivity.this.downApk(url);
            }
        });
        if (this.adIntentModel != null) {
            this.webView.loadUrl(this.adIntentModel.getUrl());
        }
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.forwardBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (WebViewActivity.this.webView != null && WebViewActivity.this.webView.canGoForward()) {
                    WebViewActivity.this.webView.goForward();
                }
            }
        });
        this.backBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (WebViewActivity.this.webView != null && WebViewActivity.this.webView.canGoBack()) {
                    WebViewActivity.this.webView.goBack();
                }
            }
        });
        this.closeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebViewActivity.this.webView.setWebViewClient(new WebViewClient() {
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    }

                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        return false;
                    }
                });
                WebViewActivity.this.finish();
                WebViewActivity.this.overridePendingTransition(0, 17432577);
            }
        });
        this.refreshBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebViewActivity.this.webView.reload();
            }
        });
    }

    /* access modifiers changed from: private */
    public void downApk(String downloadUrl) {
        if (!MCLibIOUtil.getExternalStorageState()) {
            warnByName("mc_ad_warn_sd_not_exist");
            finish();
            return;
        }
        AdApkModel adApkModel = AdStringUtils.parseAdApkModel(downloadUrl);
        if (StringUtil.isEmpty(adApkModel.getPackageName()) || !ApkUtil.isInstallApk(adApkModel.getPackageName(), this)) {
            AdDownDbModel adDbModel = this.sqliteHelper.query(downloadUrl);
            if (adDbModel == null) {
                AdDownDbModel adDbModel2 = new AdDownDbModel();
                adDbModel2.setAid(this.adIntentModel.getAid());
                adDbModel2.setCurrentPn(AppUtil.getPackageName(this));
                adDbModel2.setPn(adApkModel.getPackageName());
                adDbModel2.setStatus(1);
                adDbModel2.setUrl(downloadUrl);
                adDbModel2.setPo(this.adIntentModel.getPo());
                adDbModel2.setAppName(adApkModel.getAppName());
                this.sqliteHelper.insert(adDbModel2);
                adDbModel = this.sqliteHelper.query(downloadUrl);
            }
            this.downTaskManager.downloadApk(this, adDbModel);
            finish();
            return;
        }
        ApkUtil.launchApk(this, adApkModel.getPackageName());
        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.webView == null || !this.webView.canGoBack()) {
            finish();
            overridePendingTransition(0, 17432577);
            return super.onKeyDown(keyCode, event);
        }
        this.webView.goBack();
        return true;
    }

    final class MyWebViewClient extends WebViewClient {
        MyWebViewClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }

    final class InJavaScriptLocalObj {
        InJavaScriptLocalObj() {
        }

        public void showSource(String html) {
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        this.webView.stopLoading();
        this.webView.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.webView.setVisibility(8);
        this.webView.loadUrl("about:blank");
        this.webView.stopLoading();
        this.webView.clearView();
        this.webView.freeMemory();
        this.webView.destroy();
    }

    private void warnByName(String name) {
        Toast.makeText(this, this.resource.getString(name), 0).show();
    }

    private void warnByMsg(String msg) {
        Toast.makeText(this, msg, 0).show();
    }
}
