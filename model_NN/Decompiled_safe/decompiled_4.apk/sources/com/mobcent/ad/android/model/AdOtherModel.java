package com.mobcent.ad.android.model;

public class AdOtherModel extends AdBaseModel {
    private static final long serialVersionUID = 1846790527484842875L;
    private long delay;
    private int type;
    private String url;

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public long getDelay() {
        return this.delay;
    }

    public void setDelay(long delay2) {
        this.delay = delay2;
    }
}
