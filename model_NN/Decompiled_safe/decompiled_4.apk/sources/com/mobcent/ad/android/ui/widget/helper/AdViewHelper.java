package com.mobcent.ad.android.ui.widget.helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;
import com.mobcent.ad.android.cache.AdDatasCache;
import com.mobcent.ad.android.constant.AdConstant;
import com.mobcent.ad.android.model.AdContainerModel;
import com.mobcent.ad.android.model.AdIntentModel;
import com.mobcent.ad.android.model.AdModel;
import com.mobcent.ad.android.model.AdOtherModel;
import com.mobcent.ad.android.service.AdService;
import com.mobcent.ad.android.service.impl.AdServiceImpl;
import com.mobcent.ad.android.ui.activity.WebViewActivity;
import com.mobcent.ad.android.ui.widget.manager.AdManager;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class AdViewHelper implements AdConstant {
    private static AdViewHelper helper;
    private String TAG = "AdViewHelper";
    /* access modifiers changed from: private */
    public AdService adService;
    private View.OnClickListener adViewClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = null;
            if (v.getTag() instanceof AdModel) {
                AdModel adModel = (AdModel) v.getTag();
                Context context = v.getContext();
                switch (adModel.getGt()) {
                    case 1:
                        String url = AdViewHelper.this.adService.getLinkUrl(adModel.getAid(), (long) adModel.getPo(), adModel.getDu());
                        intent = new Intent(context, WebViewActivity.class);
                        AdIntentModel adIntentModel = new AdIntentModel();
                        adIntentModel.setAid(adModel.getAid());
                        adIntentModel.setPo(adModel.getPo());
                        adIntentModel.setUrl(url);
                        intent.putExtra(WebViewActivity.AD_INTENT_MODEL, adIntentModel);
                        break;
                    case 2:
                        String url2 = AdViewHelper.this.adService.getLinkUrl(adModel.getAid(), (long) adModel.getPo(), adModel.getDu());
                        intent = new Intent(context, WebViewActivity.class);
                        AdIntentModel adIntentModel2 = new AdIntentModel();
                        adIntentModel2.setAid(adModel.getAid());
                        adIntentModel2.setPo(adModel.getPo());
                        adIntentModel2.setUrl(url2);
                        intent.putExtra(WebViewActivity.AD_INTENT_MODEL, adIntentModel2);
                        break;
                    case 3:
                        AdViewHelper.this.tip(context, "AD_GT_CALL");
                        intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + adModel.getTel()));
                        break;
                    case 4:
                        AdViewHelper.this.tip(context, "AD_GT_SMS");
                        intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + adModel.getTel()));
                        intent.putExtra("sms_body", adModel.getTx());
                        break;
                }
                if (intent != null) {
                    context.startActivity(intent);
                }
                AdDatasCache adCache = AdManager.getInstance().getAdDatasCache();
                if (!(adCache == null || adCache.getAdDataMap().get(Integer.valueOf(adModel.getDt())) == null)) {
                    HashMap<Long, AdModel> adMap = adCache.getAdDataMap().get(Integer.valueOf(adModel.getDt()));
                    if (adMap.get(Long.valueOf(adModel.getAid())) != null) {
                        ((AdModel) adMap.get(Long.valueOf(adModel.getAid()))).setConsumed(true);
                    }
                }
                if (adModel.isOther() && adModel.getOthers().size() > 0) {
                    List<AdOtherModel> others = adModel.getOthers();
                    for (int i = 0; i < others.size(); i++) {
                        AdOtherModel other = others.get(i);
                        if (other.getType() == 2) {
                            AdViewHelper.this.exposureAdDelay(other);
                        }
                    }
                }
            }
        }
    };

    public AdViewHelper(Context context) {
        this.adService = new AdServiceImpl(context);
    }

    public static AdViewHelper getInstance(Context context) {
        if (helper == null) {
            helper = new AdViewHelper(context);
        }
        return helper;
    }

    public View.OnClickListener getAdViewClickListener() {
        return this.adViewClickListener;
    }

    public void setAdViewClickListener(View.OnClickListener adViewClickListener2) {
        this.adViewClickListener = adViewClickListener2;
    }

    /* access modifiers changed from: private */
    public void tip(Context context, String msg) {
        Toast.makeText(context, msg, 500).show();
    }

    public void exposureAd(AdContainerModel adContainerModel) {
        if (adContainerModel != null && adContainerModel.getAdSet().size() != 0) {
            Iterator<AdModel> iterator = adContainerModel.getAdSet().iterator();
            while (iterator.hasNext()) {
                AdModel adModel = iterator.next();
                Set<Long> aidSet = AdManager.getInstance().getExposureSet();
                if (!aidSet.contains(Long.valueOf(adModel.getAid())) && adModel.isOther() && adModel.getOthers().size() > 0) {
                    aidSet.add(Long.valueOf(adModel.getAid()));
                    List<AdOtherModel> others = adModel.getOthers();
                    for (int i = 0; i < others.size(); i++) {
                        AdOtherModel other = others.get(i);
                        if (other.getType() == 1) {
                            exposureAdDelay(other);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void exposureAdDelay(final AdOtherModel other) {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                AdViewHelper.this.adService.exposureAd(other.getUrl());
            }
        }, other.getDelay());
    }
}
