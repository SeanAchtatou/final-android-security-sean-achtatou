package com.mobcent.ad.android.ui.widget;

import android.content.Context;
import android.widget.RelativeLayout;
import com.mobcent.ad.android.model.AdContainerModel;
import com.mobcent.ad.android.model.AdModel;
import com.mobcent.ad.android.ui.widget.delegate.AdViewDelegate;
import com.mobcent.ad.android.ui.widget.helper.AdViewHelper;
import com.mobcent.ad.android.utils.AdStringUtils;
import com.mobcent.ad.android.utils.AdViewUtils;

public class BigPicTypeView extends RelativeLayout implements AdViewDelegate {
    private String TAG = "BigPicTypeView";
    private AdContainerModel adContainerModel;
    private AdModel adModel;
    private int adPosition;
    private RelativeLayout.LayoutParams lps = null;
    private BasePicView picView = null;
    private AdProgress progress = null;

    public BigPicTypeView(Context context) {
        super(context);
    }

    public void setAdContainerModel(AdContainerModel adContainerModel2, int position) {
        this.adContainerModel = adContainerModel2;
        this.adPosition = position;
        initView(getContext());
    }

    private void initView(Context context) {
        this.adModel = this.adContainerModel.getAdSet().iterator().next();
        this.adModel.setPo(this.adPosition);
        this.lps = new RelativeLayout.LayoutParams(-1, -1);
        this.picView = AdViewUtils.createPicView(context, this.adModel);
        addView(this.picView, this.lps);
        this.picView.setOnClickListener(AdViewHelper.getInstance(getContext()).getAdViewClickListener());
        this.progress = new AdProgress(context);
        this.lps = new RelativeLayout.LayoutParams(AdViewUtils.dipToPx(getContext(), 40), AdViewUtils.dipToPx(getContext(), 40));
        this.lps.addRule(13, -1);
        addView(this.progress, this.lps);
        this.picView.loadPic(AdStringUtils.parseImgUrl(this.adModel.getDt(), this.adModel.getPu()), this.progress);
    }

    public void freeMemery() {
        this.picView.recyclePic();
    }
}
