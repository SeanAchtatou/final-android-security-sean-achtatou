package com.mobcent.ad.android.service.impl.helper;

import com.mobcent.ad.android.cache.AdPositionsCache;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;
import org.json.JSONObject;

public class AdServiceImplHelper implements AdApiConstant {
    private static String TAG = "AdServiceImplHelper";

    public static AdPositionsCache parseAdPositions(String jsonStr) {
        Exception e;
        try {
            AdPositionsCache positionsModel = new AdPositionsCache();
            try {
                JSONObject jsonObject = new JSONObject(jsonStr);
                if (jsonObject.optInt("rs") == 0) {
                    AdPositionsCache adPositionsCache = positionsModel;
                    return positionsModel;
                }
                String[] dt1s = null;
                String[] dt2s = null;
                String[] dt3s = null;
                String dt1 = jsonObject.optString(AdApiConstant.RES_DT1, "").trim();
                if (!StringUtil.isEmpty(dt1)) {
                    dt1s = dt1.split(AdApiConstant.RES_SPLIT_COMMA);
                }
                String dt2 = jsonObject.optString(AdApiConstant.RES_DT2, "").trim();
                if (!StringUtil.isEmpty(dt2)) {
                    dt2s = dt2.split(AdApiConstant.RES_SPLIT_COMMA);
                }
                String dt3 = jsonObject.optString(AdApiConstant.RES_DT3, "").trim();
                if (!StringUtil.isEmpty(dt3)) {
                    dt3s = dt3.split(AdApiConstant.RES_SPLIT_COMMA);
                }
                positionsModel.setDt1(ssToInts(dt1s));
                positionsModel.setDt2(ssToInts(dt2s));
                positionsModel.setDt3(ssToInts(dt3s));
                positionsModel.setCycle(jsonObject.optInt(AdApiConstant.RES_CYCLE));
                return positionsModel;
            } catch (Exception e2) {
                e = e2;
                MCLogUtil.e(TAG, "[parseAdPositions]" + e.toString());
                return null;
            }
        } catch (Exception e3) {
            e = e3;
            MCLogUtil.e(TAG, "[parseAdPositions]" + e.toString());
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r14v16 org.json.JSONObject: [D('poList' org.json.JSONArray), D('poObject' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r14v21 int: [D('adModel' com.mobcent.ad.android.model.AdModel), D('i' int)] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0143  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.mobcent.ad.android.cache.AdDatasCache parseAdDatas(java.lang.String r14) {
        /*
            r2 = -1
            com.mobcent.ad.android.cache.AdDatasCache r0 = new com.mobcent.ad.android.cache.AdDatasCache
            r0.<init>()
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x011b }
            r1.<init>(r14)     // Catch:{ Exception -> 0x011b }
            java.lang.String r14 = "rs"
            int r10 = r1.optInt(r14)     // Catch:{ Exception -> 0x011b }
            r14 = 1
            r0.setRequestSucc(r14)     // Catch:{ Exception -> 0x0148 }
            if (r10 != 0) goto L_0x001e
            r14 = r10
        L_0x001d:
            return r0
        L_0x001e:
            java.lang.String r14 = "polist"
            org.json.JSONArray r14 = r1.optJSONArray(r14)     // Catch:{ Exception -> 0x0148 }
            r1 = 0
            org.json.JSONObject r14 = r14.optJSONObject(r1)     // Catch:{ Exception -> 0x0148 }
            java.lang.String r1 = "list"
            org.json.JSONArray r7 = r14.optJSONArray(r1)     // Catch:{ Exception -> 0x0148 }
            r14 = 0
            int r6 = r7.length()     // Catch:{ Exception -> 0x0148 }
            r3 = r14
        L_0x0035:
            if (r3 >= r6) goto L_0x0118
            org.json.JSONObject r1 = r7.getJSONObject(r3)     // Catch:{ Exception -> 0x0148 }
            java.lang.String r14 = "dt"
            int r2 = r1.optInt(r14)     // Catch:{ Exception -> 0x0148 }
            com.mobcent.ad.android.model.AdModel r14 = new com.mobcent.ad.android.model.AdModel     // Catch:{ Exception -> 0x0148 }
            r14.<init>()     // Catch:{ Exception -> 0x0148 }
            java.lang.String r4 = "aid"
            long r4 = r1.optLong(r4)     // Catch:{ Exception -> 0x0148 }
            r14.setAid(r4)     // Catch:{ Exception -> 0x0148 }
            java.lang.String r4 = "at"
            int r4 = r1.optInt(r4)     // Catch:{ Exception -> 0x0148 }
            r14.setAt(r4)     // Catch:{ Exception -> 0x0148 }
            r14.setDt(r2)     // Catch:{ Exception -> 0x0148 }
            java.lang.String r4 = "du"
            java.lang.String r4 = r1.optString(r4)     // Catch:{ Exception -> 0x0148 }
            r14.setDu(r4)     // Catch:{ Exception -> 0x0148 }
            java.lang.String r4 = "gid"
            long r4 = r1.optLong(r4)     // Catch:{ Exception -> 0x0148 }
            r14.setGid(r4)     // Catch:{ Exception -> 0x0148 }
            java.lang.String r4 = "gt"
            int r4 = r1.optInt(r4)     // Catch:{ Exception -> 0x0148 }
            r14.setGt(r4)     // Catch:{ Exception -> 0x0148 }
            java.lang.String r4 = "pu"
            java.lang.String r4 = r1.optString(r4)     // Catch:{ Exception -> 0x0148 }
            r14.setPu(r4)     // Catch:{ Exception -> 0x0148 }
            java.lang.String r4 = "tel"
            java.lang.String r4 = r1.optString(r4)     // Catch:{ Exception -> 0x0148 }
            r14.setTel(r4)     // Catch:{ Exception -> 0x0148 }
            java.lang.String r4 = "tx"
            java.lang.String r4 = r1.optString(r4)     // Catch:{ Exception -> 0x0148 }
            r14.setTx(r4)     // Catch:{ Exception -> 0x0148 }
            java.lang.String r4 = "tracks"
            org.json.JSONArray r5 = r1.optJSONArray(r4)     // Catch:{ Exception -> 0x0148 }
            if (r5 == 0) goto L_0x00cd
            r9.clear()     // Catch:{ Exception -> 0x0148 }
            r1 = 0
            int r11 = r5.length()     // Catch:{ Exception -> 0x0148 }
        L_0x00a1:
            if (r1 >= r11) goto L_0x00cd
            org.json.JSONObject r4 = r5.getJSONObject(r1)     // Catch:{ Exception -> 0x0148 }
            com.mobcent.ad.android.model.AdOtherModel r8 = new com.mobcent.ad.android.model.AdOtherModel     // Catch:{ Exception -> 0x0148 }
            r8.<init>()     // Catch:{ Exception -> 0x0148 }
            java.lang.String r12 = "type"
            int r12 = r4.optInt(r12)     // Catch:{ Exception -> 0x0148 }
            r8.setType(r12)     // Catch:{ Exception -> 0x0148 }
            java.lang.String r12 = "url"
            java.lang.String r12 = r4.optString(r12)     // Catch:{ Exception -> 0x0148 }
            r8.setUrl(r12)     // Catch:{ Exception -> 0x0148 }
            java.lang.String r12 = "delay"
            long r12 = r4.optLong(r12)     // Catch:{ Exception -> 0x0148 }
            r8.setDelay(r12)     // Catch:{ Exception -> 0x0148 }
            r9.add(r8)     // Catch:{ Exception -> 0x0148 }
            int r1 = r1 + 1
            goto L_0x00a1
        L_0x00cd:
            if (r9 == 0) goto L_0x00dc
            int r1 = r9.size()     // Catch:{ Exception -> 0x0148 }
            if (r1 <= 0) goto L_0x00dc
            r1 = 1
            r14.setOther(r1)     // Catch:{ Exception -> 0x0148 }
            r14.setOthers(r9)     // Catch:{ Exception -> 0x0148 }
        L_0x00dc:
            java.util.Map r1 = r0.getAdDataMap()     // Catch:{ Exception -> 0x0148 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0148 }
            java.lang.Object r1 = r1.get(r4)     // Catch:{ Exception -> 0x0148 }
            if (r1 != 0) goto L_0x00fa
            java.util.Map r1 = r0.getAdDataMap()     // Catch:{ Exception -> 0x0148 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0148 }
            java.util.HashMap r5 = new java.util.HashMap     // Catch:{ Exception -> 0x0148 }
            r5.<init>()     // Catch:{ Exception -> 0x0148 }
            r1.put(r4, r5)     // Catch:{ Exception -> 0x0148 }
        L_0x00fa:
            java.util.Map r1 = r0.getAdDataMap()     // Catch:{ Exception -> 0x0148 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0148 }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Exception -> 0x0148 }
            java.util.HashMap r1 = (java.util.HashMap) r1     // Catch:{ Exception -> 0x0148 }
            long r4 = r14.getAid()     // Catch:{ Exception -> 0x0148 }
            java.lang.Long r2 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x0148 }
            r1.put(r2, r14)     // Catch:{ Exception -> 0x0148 }
            int r14 = r3 + 1
            r3 = r14
            goto L_0x0035
        L_0x0118:
            r14 = r10
            goto L_0x001d
        L_0x011b:
            r14 = move-exception
            r1 = r2
        L_0x011d:
            java.lang.String r2 = com.mobcent.ad.android.service.impl.helper.AdServiceImplHelper.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "[parseAdDatas]"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r14 = r14.toString()
            java.lang.StringBuilder r14 = r3.append(r14)
            java.lang.String r14 = r14.toString()
            com.mobcent.forum.android.util.MCLogUtil.e(r2, r14)
            r14 = 1
            if (r1 != r14) goto L_0x0143
            r14 = 1
            r0.setRequestSucc(r14)
            r14 = r1
            goto L_0x001d
        L_0x0143:
            r14 = 0
            r0 = r14
            r14 = r1
            goto L_0x001d
        L_0x0148:
            r14 = move-exception
            r1 = r10
            goto L_0x011d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.ad.android.service.impl.helper.AdServiceImplHelper.parseAdDatas(java.lang.String):com.mobcent.ad.android.cache.AdDatasCache");
    }

    public static boolean parseSucc(String jsonStr) {
        try {
            return new JSONObject(jsonStr).optInt("rs") != 0;
        } catch (Exception e) {
            MCLogUtil.e(TAG, "[parseSucc]" + e.toString());
            return false;
        }
    }

    public static int[] ssToInts(String[] strs) {
        if (strs == null || strs.length == 0) {
            return null;
        }
        int count = strs.length;
        int[] inters = new int[count];
        for (int i = 0; i < count; i++) {
            inters[i] = Integer.parseInt(strs[i]);
        }
        return inters;
    }
}
