package com.mobcent.ad.android.task;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import com.mobcent.ad.android.db.DownSqliteHelper;
import com.mobcent.ad.android.model.AdDownDbModel;
import com.mobcent.ad.android.receiver.DownFailedReceiver;
import com.mobcent.ad.android.ui.widget.helper.AdNotificationHelper;
import com.mobcent.ad.android.utils.ApkUtil;
import com.mobcent.ad.android.utils.DownloadUtils;
import com.mobcent.forum.android.util.DateUtil;
import com.mobcent.forum.android.util.MCResource;
import com.mobcent.forum.android.util.StringUtil;
import java.io.File;

public class DownTask extends BaseTask<Void, Integer, Boolean> {
    private String TAG = "DownloadTask";
    /* access modifiers changed from: private */
    public AdDownDbModel adDownDbModel;
    /* access modifiers changed from: private */
    public int currentProgress;
    private File file;
    private String filePath = null;
    private NotificationManager manager;
    private Notification notification;
    /* access modifiers changed from: private */
    public int progressMax;
    private DownSqliteHelper sqliteHelper;

    public DownTask(Context context, AdDownDbModel adDownDbModel2) {
        super(context);
        this.manager = AdNotificationHelper.getManager(context);
        this.adDownDbModel = adDownDbModel2;
        String dir = DownloadUtils.getDownloadDirPath(context);
        this.filePath = dir + File.separator + StringUtil.getMD5Str(adDownDbModel2.getUrl());
        this.file = new File(this.filePath);
        this.sqliteHelper = DownSqliteHelper.getInstance(context);
        adDownDbModel2.setStatus(1);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.notification = AdNotificationHelper.createNotification(this.context);
        this.manager.notify(this.adDownDbModel.getId(), this.notification);
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(Void... params) {
        return Boolean.valueOf(DownloadUtils.downloadFile(this.context, this.adDownDbModel.getUrl(), this.file, new DownloadUtils.DownProgressDelegate() {
            public void setProgress(int progress) {
                int unused = DownTask.this.currentProgress = progress;
                DownTask.this.updateNotification(false, DownTask.this.adDownDbModel);
            }

            public void setMax(int max) {
                int unused = DownTask.this.progressMax = max;
                setProgress(0);
            }
        }));
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean result) {
        if (result.booleanValue()) {
            this.adDownDbModel.setStatus(2);
            this.adDownDbModel.setDate(DateUtil.getFormatTimeToSecond(System.currentTimeMillis()));
            this.adDownDbModel.setPn(ApkUtil.getPackageName(this.context, this.filePath));
            this.sqliteHelper.update(this.adDownDbModel);
            ApkUtil.installApk(this.context, this.filePath);
            this.manager.cancel(this.adDownDbModel.getId());
            new DownDoTask(this.context, this.adDownDbModel).execute(new Void[0]);
            return;
        }
        updateNotification(true, this.adDownDbModel);
        this.adDownDbModel.setStatus(3);
        this.sqliteHelper.update(this.adDownDbModel);
    }

    /* access modifiers changed from: private */
    public void updateNotification(boolean isBtnVisible, AdDownDbModel adDownDbModel2) {
        RemoteViews remoteViews = new RemoteViews(this.context.getPackageName(), MCResource.getInstance(this.context).getLayoutId("mc_ad_widget_notification"));
        remoteViews.setProgressBar(this.resource.getViewId("download_progress_bar"), this.progressMax, this.currentProgress, false);
        if (adDownDbModel2.getAppName() != null) {
            remoteViews.setTextViewText(this.resource.getViewId("download_title_text"), adDownDbModel2.getAppName());
        } else {
            remoteViews.setTextViewText(this.resource.getViewId("download_title_text"), getFileName(adDownDbModel2.getUrl()));
        }
        if (isBtnVisible) {
            this.notification.flags = 16;
            Intent intent = new Intent(DownFailedReceiver.getAction(this.context));
            intent.putExtra(DownFailedReceiver.AD_DOWNLOAD_MODEL, adDownDbModel2);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this.context, adDownDbModel2.getId(), intent, 134217728);
            remoteViews.setViewVisibility(this.resource.getViewId("continue_btn"), 0);
            remoteViews.setOnClickPendingIntent(this.resource.getViewId("continue_btn"), pendingIntent);
            this.notification.contentIntent = pendingIntent;
        } else {
            this.notification.flags = 32;
            remoteViews.setViewVisibility(this.resource.getViewId("continue_btn"), 8);
        }
        this.notification.contentView = remoteViews;
        this.manager.notify(adDownDbModel2.getId(), this.notification);
    }

    private String getFileName(String url) {
        if (StringUtil.isEmpty(url)) {
            return null;
        }
        return url.substring(url.lastIndexOf("/") + 1, url.length());
    }
}
