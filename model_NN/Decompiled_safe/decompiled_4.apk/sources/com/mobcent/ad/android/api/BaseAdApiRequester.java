package com.mobcent.ad.android.api;

import android.content.Context;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.ad.android.constant.AdConstant;
import com.mobcent.ad.android.model.RequestParamsModel;
import com.mobcent.ad.android.ui.widget.manager.AdManager;
import com.mobcent.ad.android.utils.AdNeedInfoUtils;
import com.mobcent.forum.android.api.util.HttpClientUtil;
import java.util.HashMap;

public class BaseAdApiRequester implements AdApiConstant, AdConstant {
    public static String BASE_URL = "http://adapi.mobcent.com/";
    public static String TAG = "BaseAdApiRequester";

    public static String doPostRequest(String urlString, HashMap<String, String> params, Context context) {
        RequestParamsModel requestParams = AdManager.getInstance().getRequestParams();
        if (requestParams == null) {
            return "{}";
        }
        params.put(AdApiConstant.IM, requestParams.getIm());
        params.put("pt", requestParams.getPt());
        params.put(AdApiConstant.SS, requestParams.getSs());
        params.put("ua", requestParams.getUa());
        params.put(AdApiConstant.ZO, requestParams.getZo());
        params.put(AdApiConstant.MC, requestParams.getMc());
        params.put(AdApiConstant.PN, requestParams.getPn());
        params.put(AdApiConstant.AK, requestParams.getAk());
        params.put("uid", requestParams.getUid() + "");
        params.put(AdApiConstant.CHANNEL, requestParams.getCh());
        params.put(AdApiConstant.NW, AdNeedInfoUtils.getNetworkType(context));
        params.put(AdApiConstant.JWD, AdNeedInfoUtils.getJWD(context));
        params.put(AdApiConstant.LO, AdNeedInfoUtils.getLocation(context));
        params.put(AdApiConstant.NET, AdNeedInfoUtils.getNet(context));
        params.put(AdApiConstant.SD, AdNeedInfoUtils.getSd() + "");
        params.put(AdApiConstant.VER, "5");
        return HttpClientUtil.doPostRequest(urlString, params, context);
    }

    public static String createGetUrl(String urlString, long aid, long po, String du, Context context) {
        RequestParamsModel requestParams = AdManager.getInstance().getRequestParams();
        if (requestParams == null) {
            return urlString;
        }
        return ((((((((((((((((((urlString + createParamsStr(AdApiConstant.IM, requestParams.getIm())) + createParamsStr("pt", requestParams.getPt())) + createParamsStr(AdApiConstant.SS, requestParams.getSs())) + createParamsStr("ua", requestParams.getUa())) + createParamsStr(AdApiConstant.ZO, requestParams.getZo())) + createParamsStr(AdApiConstant.MC, requestParams.getMc())) + createParamsStr(AdApiConstant.PN, requestParams.getPn())) + createParamsStr(AdApiConstant.AK, requestParams.getAk())) + createParamsStr("uid", requestParams.getUid() + "")) + createParamsStr(AdApiConstant.CHANNEL, requestParams.getCh())) + createParamsStr(AdApiConstant.NW, AdNeedInfoUtils.getNetworkType(context))) + createParamsStr(AdApiConstant.JWD, AdNeedInfoUtils.getJWD(context))) + createParamsStr(AdApiConstant.LO, AdNeedInfoUtils.getLocation(context))) + createParamsStr(AdApiConstant.NET, AdNeedInfoUtils.getNet(context))) + createParamsStr(AdApiConstant.SD, AdNeedInfoUtils.getSd() + "")) + createParamsStr(AdApiConstant.PO, po + "")) + createParamsStr("aid", aid + "")) + createParamsStr("du", du)) + createParamsStr(AdApiConstant.VER, "5");
    }

    public static String createParamsStr(String key, String value) {
        return key + "=" + value + "&";
    }
}
