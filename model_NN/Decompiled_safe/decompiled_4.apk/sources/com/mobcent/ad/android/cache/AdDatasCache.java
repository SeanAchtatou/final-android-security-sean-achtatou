package com.mobcent.ad.android.cache;

import com.mobcent.ad.android.constant.AdConstant;
import com.mobcent.ad.android.model.AdContainerModel;
import com.mobcent.ad.android.model.AdModel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

public class AdDatasCache implements AdConstant {
    private String TAG = "AdDatasCache";
    private Map<Integer, HashMap<Long, AdModel>> adDataMap = new HashMap();
    private boolean requestSucc;
    private Integer[] types = {1, 2, 3, 4};

    public boolean isRequestSucc() {
        return this.requestSucc;
    }

    public void setRequestSucc(boolean requestSucc2) {
        this.requestSucc = requestSucc2;
    }

    public Map<Integer, HashMap<Long, AdModel>> getAdDataMap() {
        return this.adDataMap;
    }

    public void setAdDataMap(Map<Integer, HashMap<Long, AdModel>> adDataMap2) {
        this.adDataMap = adDataMap2;
    }

    public String toString() {
        return "AdDatasCache [adDataMap=" + this.adDataMap + "]";
    }

    public AdContainerModel createAdContainerModel(int type) {
        switch (type) {
            case 1:
                AdContainerModel adContainerModel = randomMultiViewData(false);
                if (adContainerModel == null) {
                    return randomMultiViewData(true);
                }
                return adContainerModel;
            case 2:
                AdContainerModel adContainerModel2 = randomShortTextViewData(false);
                if (adContainerModel2 == null) {
                    return randomShortTextViewData(true);
                }
                return adContainerModel2;
            case 3:
                AdContainerModel adContainerModel3 = randomBigImgViewData(false);
                if (adContainerModel3 == null) {
                    return randomBigImgViewData(true);
                }
                return adContainerModel3;
            default:
                return null;
        }
    }

    private AdContainerModel randomMultiViewData(boolean isForce) {
        confuseArray(this.types);
        AdContainerModel multiAdModel = null;
        int i = 0;
        while (true) {
            if (i >= this.types.length) {
                break;
            }
            multiAdModel = getAdDataByType(this.types[i].intValue(), isForce);
            if (multiAdModel != null) {
                multiAdModel.setType(this.types[i].intValue());
                break;
            }
            i++;
        }
        return multiAdModel;
    }

    private AdContainerModel randomShortTextViewData(boolean isForce) {
        HashMap<Long, AdModel> adMap;
        if (!isForce) {
            adMap = getUnConsumedMap(this.adDataMap.get(2));
        } else {
            adMap = this.adDataMap.get(2);
        }
        if (adMap == null || adMap.size() < 2) {
            return null;
        }
        Object[] keys = adMap.keySet().toArray();
        confuseArray(keys);
        AdContainerModel multiAdModel = new AdContainerModel();
        HashSet<AdModel> adSet = new HashSet<>();
        adSet.add(adMap.get(keys[0]));
        adSet.add(adMap.get(keys[1]));
        multiAdModel.setType(7);
        multiAdModel.setAdSet(adSet);
        return multiAdModel;
    }

    private AdContainerModel randomBigImgViewData(boolean isForce) {
        HashMap<Long, AdModel> adMap;
        if (!isForce) {
            adMap = getUnConsumedMap(this.adDataMap.get(9));
        } else {
            adMap = this.adDataMap.get(9);
        }
        if (adMap == null || adMap.isEmpty()) {
            return null;
        }
        Object[] keys = adMap.keySet().toArray();
        confuseArray(keys);
        AdContainerModel multiAdModel = new AdContainerModel();
        HashSet<AdModel> adSet = new HashSet<>();
        adSet.add(adMap.get(keys[0]));
        multiAdModel.setType(9);
        multiAdModel.setAdSet(adSet);
        return multiAdModel;
    }

    private AdContainerModel getAdDataByType(int type, boolean isForce) {
        HashMap<Long, AdModel> adMap;
        if (!isForce) {
            adMap = getUnConsumedMap(this.adDataMap.get(Integer.valueOf(type)));
        } else {
            adMap = this.adDataMap.get(Integer.valueOf(type));
        }
        if (adMap == null || !hasEnoughData(adMap.size(), type)) {
            return null;
        }
        AdContainerModel multiAdModel = new AdContainerModel();
        multiAdModel.setAdSet(getMultiData(type));
        multiAdModel.setType(type);
        return multiAdModel;
    }

    private boolean hasEnoughData(int count, int type) {
        return (type == 1 || type == 3) ? count >= 1 : type == 4 ? count >= 2 : count >= 3;
    }

    private HashSet<AdModel> getMultiData(int type) {
        int count;
        HashSet<AdModel> adSet = new HashSet<>();
        if (type == 1 || type == 3) {
            count = 1;
        } else if (type == 4) {
            count = 2;
        } else {
            count = 3;
        }
        HashMap<Long, AdModel> adMap = this.adDataMap.get(Integer.valueOf(type));
        Object[] keys = adMap.keySet().toArray();
        confuseArray(keys);
        for (int i = 0; i < count; i++) {
            adSet.add(adMap.get(keys[i]));
        }
        return adSet;
    }

    private HashMap<Long, AdModel> getUnConsumedMap(HashMap<Long, AdModel> adMap) {
        HashMap<Long, AdModel> unConsumedMap = new HashMap<>();
        if (adMap != null && !adMap.isEmpty()) {
            Object[] keys = adMap.keySet().toArray();
            confuseArray(keys);
            for (Object key : keys) {
                AdModel adModel = adMap.get(key);
                if (!adModel.isConsumed() && !isHaveSameGidAd(unConsumedMap, adModel)) {
                    unConsumedMap.put((Long) key, adModel);
                }
            }
        }
        return unConsumedMap;
    }

    private boolean isHaveSameGidAd(HashMap<Long, AdModel> adMap, AdModel adModel) {
        for (Long key : adMap.keySet()) {
            if (adMap.get(key).getGid() == adModel.getGid()) {
                return true;
            }
        }
        return false;
    }

    private void confuseArray(Object[] args) {
        int length = args.length;
        Random r = new Random();
        for (int i = 0; i < length; i++) {
            int index = r.nextInt(length);
            Object temp = args[i];
            args[i] = args[index];
            args[index] = temp;
        }
    }
}
