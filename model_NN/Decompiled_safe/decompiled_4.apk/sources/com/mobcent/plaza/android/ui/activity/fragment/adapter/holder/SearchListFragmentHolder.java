package com.mobcent.plaza.android.ui.activity.fragment.adapter.holder;

import android.widget.ImageView;
import android.widget.TextView;

public class SearchListFragmentHolder {
    private TextView describeText;
    private ImageView thumbImg;
    private ImageView thumbImgRight;
    private TextView titleText;

    public TextView getTitleText() {
        return this.titleText;
    }

    public void setTitleText(TextView titleText2) {
        this.titleText = titleText2;
    }

    public TextView getDescribeText() {
        return this.describeText;
    }

    public void setDescribeText(TextView describeText2) {
        this.describeText = describeText2;
    }

    public ImageView getThumbImg() {
        return this.thumbImg;
    }

    public void setThumbImg(ImageView thumbImg2) {
        this.thumbImg = thumbImg2;
    }

    public ImageView getThumbImgRight() {
        return this.thumbImgRight;
    }

    public void setThumbImgRight(ImageView thumbImgRight2) {
        this.thumbImgRight = thumbImgRight2;
    }
}
