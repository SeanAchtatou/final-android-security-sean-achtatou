package com.mobcent.plaza.android.ui.activity.fragment.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.MCResource;
import com.mobcent.plaza.android.service.model.PlazaAppModel;
import com.mobcent.plaza.android.ui.activity.fragment.adapter.holder.PlazaFragmentHolder;
import java.util.ArrayList;
import java.util.List;

public class PlazaFragmentAdapter extends BaseAdapter {
    private String TAG = "PlazaFragmentAdapter";
    private MCResource adResource;
    private Context context;
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    private LayoutInflater inflater;
    private List<PlazaAppModel> plazaAppModelList;
    private List<String> recyleUrls = new ArrayList();

    public PlazaFragmentAdapter(Context context2, Handler handler2, List<PlazaAppModel> plazaAppModelList2) {
        this.handler = handler2;
        this.plazaAppModelList = plazaAppModelList2;
        this.inflater = LayoutInflater.from(context2);
        this.adResource = MCResource.getInstance(context2);
        this.context = context2;
    }

    public int getCount() {
        return this.plazaAppModelList.size();
    }

    public Object getItem(int position) {
        return this.plazaAppModelList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup viewGroup) {
        PlazaFragmentHolder holder;
        PlazaAppModel plazaAppModel = this.plazaAppModelList.get(position);
        if (convertView == null) {
            convertView = this.inflater.inflate(this.adResource.getLayoutId("mc_plaza_fragment_item"), (ViewGroup) null);
            holder = new PlazaFragmentHolder();
            initGridView(convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (PlazaFragmentHolder) convertView.getTag();
        }
        initGridViewData(holder, plazaAppModel);
        return convertView;
    }

    private void initGridView(View convertView, PlazaFragmentHolder holder) {
        holder.setAppImg((ImageView) convertView.findViewById(this.adResource.getViewId("plaza_gird_item_img")));
        holder.setAppText((TextView) convertView.findViewById(this.adResource.getViewId("plaza_grid_item_text")));
    }

    private void initGridViewData(PlazaFragmentHolder holder, PlazaAppModel plazaAppModel) {
        holder.getAppText().setText(plazaAppModel.getModelName());
        loadImgByUrl(holder.getAppImg(), plazaAppModel);
    }

    private void loadImgByUrl(final ImageView img, PlazaAppModel plazaAppModel) {
        img.setImageBitmap(null);
        if (plazaAppModel.getModelAction() == 0) {
            img.setImageResource(this.adResource.getDrawableId(plazaAppModel.getModelDrawable()));
            return;
        }
        this.recyleUrls.add(plazaAppModel.getModelDrawable());
        AsyncTaskLoaderImage.getInstance(this.context, this.TAG).loadAsync(plazaAppModel.getModelDrawable(), new AsyncTaskLoaderImage.BitmapImageCallback() {
            public void onImageLoaded(final Bitmap bitmap, String url) {
                PlazaFragmentAdapter.this.handler.post(new Runnable() {
                    public void run() {
                        if (bitmap != null && !bitmap.isRecycled()) {
                            img.setImageBitmap(bitmap);
                        }
                    }
                });
            }
        });
    }

    public void recyleAllBitmap() {
        AsyncTaskLoaderImage.getInstance(this.context, this.TAG).recycleBitmaps(this.recyleUrls);
    }
}
