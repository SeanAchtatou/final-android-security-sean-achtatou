package com.mobcent.plaza.android.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.plaza.android.ui.activity.fragment.PlazaSearchFallWallFragment;
import com.mobcent.plaza.android.ui.activity.fragment.PlazaSearchListFragment;
import com.mobcent.plaza.android.ui.activity.model.PlazaSearchChannelModel;
import com.mobcent.plaza.android.ui.activity.model.PlazaSearchKeyModel;
import java.util.ArrayList;
import java.util.List;

public class PlazaSearchActivity extends BaseFragmentActivity {
    private String TAG = "SearchActivity";
    /* access modifiers changed from: private */
    public Button backBtn;
    /* access modifiers changed from: private */
    public int baikeType;
    /* access modifiers changed from: private */
    public RelativeLayout channelBox;
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener channelListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            int unused = PlazaSearchActivity.this.currentItem = which;
            int unused2 = PlazaSearchActivity.this.baikeType = ((PlazaSearchChannelModel) PlazaSearchActivity.this.searchBoards.get(which)).getBaikeType();
            PlazaSearchActivity.this.channelText.setText(((PlazaSearchChannelModel) PlazaSearchActivity.this.searchBoards.get(which)).getTypeName());
            PlazaSearchActivity.this.onSearchBtnClick();
        }
    };
    /* access modifiers changed from: private */
    public TextView channelText;
    private RelativeLayout containerBox;
    /* access modifiers changed from: private */
    public int currentItem;
    /* access modifiers changed from: private */
    public AlertDialog.Builder dialog;
    private PlazaSearchFallWallFragment fallWallFragment;
    private IntentPlazaModel intentPlazaModel;
    /* access modifiers changed from: private */
    public String[] items;
    /* access modifiers changed from: private */
    public EditText keyWordEdit;
    private PlazaSearchListFragment listFragment;
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (v == PlazaSearchActivity.this.channelBox) {
                PlazaSearchActivity.this.dialog.setTitle(PlazaSearchActivity.this.getString(PlazaSearchActivity.this.adResource.getStringId("mc_plaza_search_category_title")));
                PlazaSearchActivity.this.dialog.setItems(PlazaSearchActivity.this.items, PlazaSearchActivity.this.channelListener);
                PlazaSearchActivity.this.dialog.setSingleChoiceItems(PlazaSearchActivity.this.items, PlazaSearchActivity.this.currentItem, PlazaSearchActivity.this.channelListener);
                PlazaSearchActivity.this.dialog.show();
            } else if (v == PlazaSearchActivity.this.searchBtn) {
                PlazaSearchActivity.this.onSearchBtnClick();
            } else if (v == PlazaSearchActivity.this.backBtn) {
                PlazaSearchActivity.this.onBackPressed();
            }
        }
    };
    /* access modifiers changed from: private */
    public List<PlazaSearchChannelModel> searchBoards;
    /* access modifiers changed from: private */
    public Button searchBtn;
    private String[] typeNames;
    private int[] types;

    /* access modifiers changed from: protected */
    public void initData() {
        this.searchBoards = new ArrayList();
        this.typeNames = getResources().getStringArray(this.adResource.getArrayId("mc_plaza_search_channel"));
        this.types = getResources().getIntArray(this.adResource.getArrayId("mc_plaza_search_types_order"));
        for (int i = 0; i < this.typeNames.length; i++) {
            PlazaSearchChannelModel searchBoard = new PlazaSearchChannelModel();
            searchBoard.setBaikeType(this.types[i]);
            searchBoard.setTypeName(this.typeNames[i]);
            searchBoard.setShow(false);
            this.searchBoards.add(searchBoard);
        }
        this.intentPlazaModel = (IntentPlazaModel) getIntent().getSerializableExtra("plazaIntentModel");
        if (this.intentPlazaModel.getSearchTypes() == null || this.intentPlazaModel.getSearchTypes().length == 0) {
            finish();
            return;
        }
        MCLogUtil.e(this.TAG, this.intentPlazaModel.toString());
        dealSearchList(this.intentPlazaModel.getSearchTypes());
        this.items = new String[this.searchBoards.size()];
        for (int i2 = 0; i2 < this.searchBoards.size(); i2++) {
            this.items[i2] = this.searchBoards.get(i2).getTypeName();
        }
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        requestWindowFeature(1);
        setContentView(this.adResource.getLayoutId("mc_plaza_search_activity"));
        this.containerBox = (RelativeLayout) findViewById(this.adResource.getViewId("container_layout"));
        this.channelBox = (RelativeLayout) findViewById(this.adResource.getViewId("mc_plaza_channel_layout"));
        this.channelText = (TextView) findViewById(this.adResource.getViewId("channel_text"));
        this.keyWordEdit = (EditText) findViewById(this.adResource.getViewId("key_word_edit"));
        this.searchBtn = (Button) findViewById(this.adResource.getViewId("search_btn"));
        this.backBtn = (Button) findViewById(this.adResource.getViewId("back_btn"));
        this.dialog = new AlertDialog.Builder(this);
        this.dialog.create();
        this.channelText.setText(this.searchBoards.get(0).getTypeName());
        this.listFragment = new PlazaSearchListFragment(this.mHandler);
        addFragment(this.listFragment);
        this.fallWallFragment = new PlazaSearchFallWallFragment(this.mHandler);
        addFragment(this.fallWallFragment);
        showFragment(this.listFragment);
        if (this.searchBoards.size() == 1) {
            this.channelBox.setVisibility(8);
        }
        if (this.searchBoards.size() > 0) {
            this.baikeType = this.searchBoards.get(0).getBaikeType();
        }
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.channelBox.setOnClickListener(this.onClickListener);
        this.backBtn.setOnClickListener(this.onClickListener);
        this.searchBtn.setOnClickListener(this.onClickListener);
        this.keyWordEdit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66) {
                    PlazaSearchActivity.this.keyWordEdit.setFocusable(false);
                    PlazaSearchActivity.this.keyWordEdit.setFocusableInTouchMode(true);
                    PlazaSearchActivity.this.hideSoftKeyboard();
                    PlazaSearchActivity.this.searchBtn.performClick();
                }
                PlazaSearchActivity.this.keyWordEdit.setFocusable(true);
                return false;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(17432576, 17432577);
        this.keyWordEdit.setFocusable(true);
        showSoftKeyboard(this.keyWordEdit);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        hideSoftKeyboard();
    }

    private void addFragment(Fragment fragment) {
        FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
        tran.add(this.containerBox.getId(), fragment);
        tran.addToBackStack(null);
        tran.commit();
    }

    private void showFragment(Fragment fragment) {
        FragmentTransaction tran = getSupportFragmentManager().beginTransaction();
        tran.show(fragment);
        if (fragment instanceof PlazaSearchListFragment) {
            tran.hide(this.fallWallFragment);
        } else {
            tran.hide(this.listFragment);
        }
        tran.addToBackStack(null);
        tran.commit();
    }

    private void dealSearchList(int[] showTypes) {
        for (int i = 0; i < showTypes.length; i++) {
            int j = 0;
            while (true) {
                if (j >= this.searchBoards.size()) {
                    break;
                }
                PlazaSearchChannelModel searchBoard = this.searchBoards.get(j);
                if (showTypes[i] == searchBoard.getBaikeType()) {
                    searchBoard.setShow(true);
                    break;
                }
                j++;
            }
        }
        List<PlazaSearchChannelModel> searchBoardsTemp = new ArrayList<>();
        for (int i2 = 0; i2 < this.searchBoards.size(); i2++) {
            if (this.searchBoards.get(i2).isShow()) {
                searchBoardsTemp.add(this.searchBoards.get(i2));
            }
        }
        this.searchBoards.clear();
        this.searchBoards.addAll(searchBoardsTemp);
    }

    public void onSearchBtnClick() {
        PlazaSearchKeyModel searchKeyModel = new PlazaSearchKeyModel();
        searchKeyModel.setForumId(this.intentPlazaModel.getForumId());
        searchKeyModel.setForumKey(this.intentPlazaModel.getForumKey());
        searchKeyModel.setUserId(this.intentPlazaModel.getUserId());
        searchKeyModel.setBaikeType(this.baikeType);
        searchKeyModel.setSearchMode(1);
        searchKeyModel.setKeyWord(this.keyWordEdit.getText().toString());
        if (this.baikeType == 2) {
            showFragment(this.fallWallFragment);
            this.fallWallFragment.requestData(searchKeyModel);
            return;
        }
        showFragment(this.listFragment);
        this.listFragment.requestData(searchKeyModel);
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0, 17432577);
    }
}
