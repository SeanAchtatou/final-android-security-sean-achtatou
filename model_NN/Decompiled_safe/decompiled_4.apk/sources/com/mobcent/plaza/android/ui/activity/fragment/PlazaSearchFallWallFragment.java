package com.mobcent.plaza.android.ui.activity.fragment;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.StringUtil;
import com.mobcent.plaza.android.service.SearchService;
import com.mobcent.plaza.android.service.impl.SearchServiceImpl;
import com.mobcent.plaza.android.service.model.SearchModel;
import com.mobcent.plaza.android.ui.activity.PlazaSearchActivity;
import com.mobcent.plaza.android.ui.activity.delegate.PlazaConfig;
import com.mobcent.plaza.android.ui.activity.model.FlowTag;
import com.mobcent.plaza.android.ui.activity.model.PlazaSearchKeyModel;
import com.mobcent.plaza.android.ui.activity.widget.PullToRefreshBase;
import com.mobcent.plaza.android.ui.activity.widget.PullToRefreshWaterFall;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlazaSearchFallWallFragment extends BaseFragment {
    /* access modifiers changed from: private */
    public String TAG = "SearchFallWallFragment";
    /* access modifiers changed from: private */
    public PullToRefreshWaterFall fallwallBox;
    /* access modifiers changed from: private */
    public Handler handler;
    /* access modifiers changed from: private */
    public Map<String, ImageView> imageMap;
    /* access modifiers changed from: private */
    public boolean isSearchBtnClick = false;
    private PullToRefreshWaterFall.OnLoadItemListener loadItemListener = new PullToRefreshWaterFall.OnLoadItemListener() {
        public void loadLayout(LinearLayout linearLayout, String imageUrl) {
            if (PlazaSearchFallWallFragment.this.getActivity() != null) {
                ImageView imageView = new ImageView(PlazaSearchFallWallFragment.this.getActivity());
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-1, -1);
                imageView.setBackgroundResource(PlazaSearchFallWallFragment.this.mcResource.getDrawableId("mc_forum_list9_li_bg"));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                linearLayout.addView(imageView, lp);
                PlazaSearchFallWallFragment.this.imageMap.put(imageUrl, imageView);
            }
        }

        public void loadImage(final String imageUrl) {
            AsyncTaskLoaderImage.getInstance(PlazaSearchFallWallFragment.this.getActivity(), PlazaSearchFallWallFragment.this.TAG).loadAsync(AsyncTaskLoaderImage.formatUrl(imageUrl, "200x200"), new AsyncTaskLoaderImage.BitmapImageCallback() {
                public void onImageLoaded(final Bitmap image, String url) {
                    PlazaSearchFallWallFragment.this.handler.post(new Runnable() {
                        public void run() {
                            ImageView imgView;
                            if (image != null && !image.isRecycled() && (imgView = (ImageView) PlazaSearchFallWallFragment.this.imageMap.get(imageUrl)) != null) {
                                imgView.setImageBitmap(image);
                            }
                        }
                    });
                }
            });
        }

        public void onItemClick(int currentPosition, FlowTag flowTag) {
            if (PlazaConfig.getInstance().getPlazaDelegate() != null) {
                PlazaConfig.getInstance().getPlazaDelegate().onSearchItemClick(PlazaSearchFallWallFragment.this.getActivity(), (SearchModel) PlazaSearchFallWallFragment.this.searchList.get(currentPosition));
            }
        }

        public void recycleImage(String imageUrl) {
            ImageView imageView = (ImageView) PlazaSearchFallWallFragment.this.imageMap.get(imageUrl);
            if (imageView != null) {
                imageView.setImageBitmap(null);
            }
            AsyncTaskLoaderImage.getInstance(PlazaSearchFallWallFragment.this.getActivity().getApplicationContext(), PlazaSearchFallWallFragment.this.TAG).recycleBitmap(AsyncTaskLoaderImage.formatUrl(imageUrl, "200x200"));
        }
    };
    /* access modifiers changed from: private */
    public int page = 1;
    private RelativeLayout rootView;
    /* access modifiers changed from: private */
    public PlazaSearchKeyModel searchKeyModel;
    /* access modifiers changed from: private */
    public List<SearchModel> searchList;
    /* access modifiers changed from: private */
    public SearchService searchService;

    static /* synthetic */ int access$508(PlazaSearchFallWallFragment x0) {
        int i = x0.page;
        x0.page = i + 1;
        return i;
    }

    static /* synthetic */ int access$510(PlazaSearchFallWallFragment x0) {
        int i = x0.page;
        x0.page = i - 1;
        return i;
    }

    public PlazaSearchFallWallFragment(Handler handler2) {
        this.handler = handler2;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.searchService = new SearchServiceImpl(getActivity());
        this.searchList = new ArrayList();
        this.imageMap = new HashMap();
    }

    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.rootView = (RelativeLayout) inflater.inflate(this.mcResource.getLayoutId("mc_plaza_search_fallwall_fragment"), (ViewGroup) null);
        this.fallwallBox = (PullToRefreshWaterFall) this.rootView.findViewById(this.mcResource.getViewId("fallwall_layout"));
        this.fallwallBox.initView(getActivity(), null);
        this.fallwallBox.onBottomRefreshComplete(3, getString(this.mcResource.getStringId("mc_plaza_search_no_search_data")));
        return this.rootView;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.fallwallBox.setOnLoadItemListener(this.loadItemListener);
        this.fallwallBox.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener() {
            /* Debug info: failed to restart local var, previous not found, register: 3 */
            public void onRefresh() {
                if (PlazaSearchFallWallFragment.this.isSearchBtnClick) {
                    PlazaSearchFallWallFragment.this.addAsyncTask(new GetData().execute(new Void[0]));
                } else {
                    ((PlazaSearchActivity) PlazaSearchFallWallFragment.this.getActivity()).onSearchBtnClick();
                }
            }
        });
        this.fallwallBox.setOnBottomRefreshListener(new PullToRefreshBase.OnBottomRefreshListener() {
            public void onRefresh() {
                new GetMore().execute(new Void[0]);
            }
        });
    }

    public void onDestroyView() {
        super.onDestroyView();
        for (String key : this.imageMap.keySet()) {
            ImageView img = this.imageMap.get(key);
            if (img != null) {
                img.setImageBitmap(null);
                AsyncTaskLoaderImage.getInstance(getActivity().getApplicationContext(), this.TAG).recycleBitmap(AsyncTaskLoaderImage.formatUrl(key, "200x200"));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void cleanBitmapByUrl(List<String> list) {
    }

    /* access modifiers changed from: protected */
    public List<String> getImageUrls(int startLen, int endLen) {
        return null;
    }

    public void requestData(PlazaSearchKeyModel searchKeyModel2) {
        this.searchKeyModel = searchKeyModel2;
        this.isSearchBtnClick = true;
        this.searchList.clear();
        this.fallwallBox.onDrawWaterFall(galleryModels2FlowTags(this.searchList), 0);
        if (searchKeyModel2 == null || searchKeyModel2.getKeyWord() == null || "".equals(searchKeyModel2.getKeyWord())) {
            this.isSearchBtnClick = false;
            this.fallwallBox.onRefreshComplete();
            this.fallwallBox.onBottomRefreshComplete(3, getString(this.mcResource.getStringId("mc_plaza_search_no_search_data")));
            return;
        }
        this.fallwallBox.onRefresh();
    }

    class GetData extends AsyncTask<Void, Void, List<SearchModel>> {
        GetData() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<SearchModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            int unused = PlazaSearchFallWallFragment.this.page = 1;
        }

        /* access modifiers changed from: protected */
        public List<SearchModel> doInBackground(Void... params) {
            if (PlazaSearchFallWallFragment.this.searchKeyModel == null) {
                return null;
            }
            return PlazaSearchFallWallFragment.this.searchService.getSearchList(PlazaSearchFallWallFragment.this.searchKeyModel.getForumId(), PlazaSearchFallWallFragment.this.searchKeyModel.getForumKey(), PlazaSearchFallWallFragment.this.searchKeyModel.getUserId(), PlazaSearchFallWallFragment.this.searchKeyModel.getBaikeType(), PlazaSearchFallWallFragment.this.searchKeyModel.getSearchMode(), PlazaSearchFallWallFragment.this.searchKeyModel.getKeyWord(), PlazaSearchFallWallFragment.this.page, 20);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<SearchModel> result) {
            PlazaSearchFallWallFragment.this.fallwallBox.onRefreshComplete();
            boolean unused = PlazaSearchFallWallFragment.this.isSearchBtnClick = false;
            if (result != null) {
                if (result.isEmpty()) {
                    PlazaSearchFallWallFragment.this.fallwallBox.onBottomRefreshComplete(3, PlazaSearchFallWallFragment.this.getString(PlazaSearchFallWallFragment.this.mcResource.getStringId("mc_plaza_no_data")));
                    PlazaSearchFallWallFragment.this.clearSearchData();
                } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                    PlazaSearchFallWallFragment.this.fallwallBox.onBottomRefreshComplete(3, PlazaSearchFallWallFragment.this.getString(PlazaSearchFallWallFragment.this.mcResource.getStringId("mc_plaza_load_error")));
                    PlazaSearchFallWallFragment.this.clearSearchData();
                } else {
                    PlazaSearchFallWallFragment.this.searchList.clear();
                    PlazaSearchFallWallFragment.this.searchList.addAll(result);
                    PlazaSearchFallWallFragment.this.imageMap.clear();
                    PlazaSearchFallWallFragment.this.fallwallBox.onDrawWaterFall(PlazaSearchFallWallFragment.this.galleryModels2FlowTags(result), 0);
                    if (result.get(0).isHasNext()) {
                        PlazaSearchFallWallFragment.this.fallwallBox.onBottomRefreshComplete(0);
                    } else {
                        PlazaSearchFallWallFragment.this.fallwallBox.onBottomRefreshComplete(3);
                    }
                }
                result.clear();
                return;
            }
            PlazaSearchFallWallFragment.this.fallwallBox.onBottomRefreshComplete(3, PlazaSearchFallWallFragment.this.getString(PlazaSearchFallWallFragment.this.mcResource.getStringId("mc_plaza_search_no_search_data")));
            PlazaSearchFallWallFragment.this.clearSearchData();
        }
    }

    class GetMore extends AsyncTask<Void, Void, List<SearchModel>> {
        GetMore() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<SearchModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            PlazaSearchFallWallFragment.access$508(PlazaSearchFallWallFragment.this);
        }

        /* access modifiers changed from: protected */
        public List<SearchModel> doInBackground(Void... params) {
            if (PlazaSearchFallWallFragment.this.searchKeyModel == null) {
                return null;
            }
            return PlazaSearchFallWallFragment.this.searchService.getSearchList(PlazaSearchFallWallFragment.this.searchKeyModel.getForumId(), PlazaSearchFallWallFragment.this.searchKeyModel.getForumKey(), PlazaSearchFallWallFragment.this.searchKeyModel.getUserId(), PlazaSearchFallWallFragment.this.searchKeyModel.getBaikeType(), PlazaSearchFallWallFragment.this.searchKeyModel.getSearchMode(), PlazaSearchFallWallFragment.this.searchKeyModel.getKeyWord(), PlazaSearchFallWallFragment.this.page, 20);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<SearchModel> result) {
            if (result != null) {
                if (result.isEmpty()) {
                    PlazaSearchFallWallFragment.access$510(PlazaSearchFallWallFragment.this);
                    PlazaSearchFallWallFragment.this.fallwallBox.onBottomRefreshComplete(2);
                } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                    PlazaSearchFallWallFragment.access$510(PlazaSearchFallWallFragment.this);
                    PlazaSearchFallWallFragment.this.fallwallBox.onBottomRefreshComplete(0);
                } else {
                    PlazaSearchFallWallFragment.this.searchList.addAll(result);
                    PlazaSearchFallWallFragment.this.fallwallBox.onDrawWaterFall(PlazaSearchFallWallFragment.this.galleryModels2FlowTags(result), 1);
                    if (result.get(0).isHasNext()) {
                        PlazaSearchFallWallFragment.this.fallwallBox.onBottomRefreshComplete(0);
                    } else {
                        PlazaSearchFallWallFragment.this.fallwallBox.onBottomRefreshComplete(3);
                    }
                }
                result.clear();
                return;
            }
            PlazaSearchFallWallFragment.access$510(PlazaSearchFallWallFragment.this);
            PlazaSearchFallWallFragment.this.fallwallBox.onBottomRefreshComplete(0);
        }
    }

    /* access modifiers changed from: private */
    public void clearSearchData() {
        this.searchList.clear();
        this.fallwallBox.onDrawWaterFall(galleryModels2FlowTags(this.searchList), 0);
    }

    /* access modifiers changed from: private */
    public ArrayList<FlowTag> galleryModels2FlowTags(List<SearchModel> searchModels) {
        ArrayList<FlowTag> flowTags = new ArrayList<>();
        for (int i = 0; i < searchModels.size(); i++) {
            SearchModel searchModel = searchModels.get(i);
            FlowTag flowTag = new FlowTag();
            flowTag.setRatio(searchModel.getRatio());
            flowTag.setThumbnailUrl(searchModel.getBaseUrl() + searchModel.getPicPath());
            flowTags.add(flowTag);
        }
        return flowTags;
    }
}
