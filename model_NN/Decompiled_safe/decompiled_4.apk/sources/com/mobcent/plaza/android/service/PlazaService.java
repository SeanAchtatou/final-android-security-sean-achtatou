package com.mobcent.plaza.android.service;

import com.mobcent.plaza.android.service.model.PlazaAppModel;
import java.util.List;

public interface PlazaService {
    List<PlazaAppModel> getPlazaAppModelListByLocal();

    List<PlazaAppModel> getPlazaAppModelListByNet(String str, long j);

    String getPlazaLinkUrl(String str, long j, long j2);
}
