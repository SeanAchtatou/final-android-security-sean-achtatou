package com.mobcent.plaza.android.ui.activity.delegate;

import android.content.Context;
import com.mobcent.plaza.android.service.model.PlazaAppModel;
import com.mobcent.plaza.android.service.model.SearchModel;

public class PlazaConfig {
    public static String BASE_SEARCH_REQUEST_URL = null;
    private static PlazaConfig plazaConfig;
    private PlazaDelegate plazaDelegate;

    public interface PlazaDelegate {
        long getUserId(Context context);

        void onAboutClick(Context context);

        void onAppItemClick(Context context, PlazaAppModel plazaAppModel);

        void onPersonalClick(Context context);

        boolean onPlazaBackPressed(Context context);

        void onSearchItemClick(Context context, SearchModel searchModel);

        void onSetClick(Context context);
    }

    public static PlazaConfig getInstance() {
        if (plazaConfig == null) {
            plazaConfig = new PlazaConfig();
        }
        return plazaConfig;
    }

    public PlazaDelegate getPlazaDelegate() {
        return this.plazaDelegate;
    }

    public void setPlazaDelegate(PlazaDelegate plazaDelegate2) {
        this.plazaDelegate = plazaDelegate2;
    }
}
