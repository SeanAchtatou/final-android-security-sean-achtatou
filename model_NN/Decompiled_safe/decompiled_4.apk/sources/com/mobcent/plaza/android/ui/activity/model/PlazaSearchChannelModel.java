package com.mobcent.plaza.android.ui.activity.model;

public class PlazaSearchChannelModel {
    int baikeType;
    boolean isShow;
    String typeName;

    public boolean isShow() {
        return this.isShow;
    }

    public void setShow(boolean isShow2) {
        this.isShow = isShow2;
    }

    public int getBaikeType() {
        return this.baikeType;
    }

    public void setBaikeType(int baikeType2) {
        this.baikeType = baikeType2;
    }

    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(String typeName2) {
        this.typeName = typeName2;
    }
}
