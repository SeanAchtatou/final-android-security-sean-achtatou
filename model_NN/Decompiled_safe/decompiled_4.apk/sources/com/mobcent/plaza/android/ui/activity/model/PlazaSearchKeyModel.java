package com.mobcent.plaza.android.ui.activity.model;

public class PlazaSearchKeyModel {
    private int baikeType;
    private int forumId;
    private String forumKey;
    private String keyWord;
    private int searchMode;
    private long userId;

    public int getForumId() {
        return this.forumId;
    }

    public void setForumId(int forumId2) {
        this.forumId = forumId2;
    }

    public String getForumKey() {
        return this.forumKey;
    }

    public void setForumKey(String forumKey2) {
        this.forumKey = forumKey2;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
    }

    public int getBaikeType() {
        return this.baikeType;
    }

    public void setBaikeType(int baikeType2) {
        this.baikeType = baikeType2;
    }

    public int getSearchMode() {
        return this.searchMode;
    }

    public void setSearchMode(int searchMode2) {
        this.searchMode = searchMode2;
    }

    public String getKeyWord() {
        return this.keyWord;
    }

    public void setKeyWord(String keyWord2) {
        this.keyWord = keyWord2;
    }

    public String toString() {
        return "SearchKeyModel [forumId=" + this.forumId + ", forumKey=" + this.forumKey + ", userId=" + this.userId + ", baikeType=" + this.baikeType + ", searchMode=" + this.searchMode + ", keyWord=" + this.keyWord + "]";
    }
}
