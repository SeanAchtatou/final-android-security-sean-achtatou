package com.mobcent.plaza.android.ui.activity.constant;

public interface PlazaConstant {
    public static final int ACTION_APP_INCLUDE = 0;
    public static final int ACTION_BOTTOM_WEBVIEW = 2;
    public static final int ACTION_DOWNLOAD = 3;
    public static final int ACTION_SYSTEM_BROWER = 4;
    public static final int ACTION_TOP_WEBVIEW = 1;
    public static final int DEFAULT_PAGE_SIZE = 20;
    public static final String RESOLUTION_100X100 = "100x100";
    public static final String RESOLUTION_200X200 = "200x200";
    public static final String WEB_VIEW_TOP = "web_view_is_top";
    public static final String WEB_VIEW_URL = "web_view_url";
}
