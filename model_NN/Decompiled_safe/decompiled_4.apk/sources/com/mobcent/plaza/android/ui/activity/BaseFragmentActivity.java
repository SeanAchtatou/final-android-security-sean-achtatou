package com.mobcent.plaza.android.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import com.mobcent.forum.android.util.MCResource;

public abstract class BaseFragmentActivity extends FragmentActivity {
    protected MCResource adResource;
    protected InputMethodManager imm;
    protected Handler mHandler = new Handler();

    /* access modifiers changed from: protected */
    public abstract void initData();

    /* access modifiers changed from: protected */
    public abstract void initViews();

    /* access modifiers changed from: protected */
    public abstract void initWidgetActions();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        this.adResource = MCResource.getInstance(getApplicationContext());
        this.imm = (InputMethodManager) getSystemService("input_method");
        initData();
        initViews();
        initWidgetActions();
    }

    /* access modifiers changed from: protected */
    public void warById(String name) {
        Toast.makeText(getApplicationContext(), this.adResource.getStringId(name), 0).show();
    }

    /* access modifiers changed from: protected */
    public void showSoftKeyboard(View view) {
        view.requestFocus();
        this.imm.showSoftInput(view, 1);
    }

    public void showSoftKeyboard() {
        this.imm.showSoftInput(getCurrentFocus(), 1);
    }

    public void hideSoftKeyboard() {
        this.imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 2);
    }
}
