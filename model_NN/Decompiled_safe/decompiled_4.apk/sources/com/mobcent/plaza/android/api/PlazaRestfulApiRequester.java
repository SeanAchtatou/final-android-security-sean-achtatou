package com.mobcent.plaza.android.api;

import android.content.Context;
import com.mobcent.ad.android.api.BaseAdApiRequester;
import com.mobcent.ad.android.constant.AdApiConstant;
import com.mobcent.plaza.android.api.constant.PlazaApiConstant;
import java.util.HashMap;

public class PlazaRestfulApiRequester extends BaseAdApiRequester implements PlazaApiConstant, AdApiConstant {
    static String urlString = null;

    public static String getPlazaAppModelList(Context context, String appKey, long userId, long ut) {
        urlString = BASE_URL + "clientapi/m/getSquare.do";
        HashMap<String, String> params = new HashMap<>();
        params.put(AdApiConstant.AK, appKey);
        params.put(AdApiConstant.PO, "7001");
        params.put("uid", userId + "");
        params.put(PlazaApiConstant.UT, ut + "");
        return doPostRequest(urlString, params, context);
    }

    public static String getPlazaLinkUrl(Context context, String appKey, long mid, long userId) {
        return createGetUrl(BASE_URL + "clientapi/m/linkModule.do?", 0, 7001, null, context) + createParamsStr(PlazaApiConstant.MID, mid + "");
    }
}
