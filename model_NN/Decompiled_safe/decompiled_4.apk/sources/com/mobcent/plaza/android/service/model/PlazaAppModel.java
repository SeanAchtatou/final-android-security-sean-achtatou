package com.mobcent.plaza.android.service.model;

import com.mobcent.forum.android.model.BaseModel;

public class PlazaAppModel extends BaseModel {
    private static final long serialVersionUID = -2326276971887911801L;
    private String activityName;
    private boolean isEmpty = false;
    private int modelAction = -1;
    private String modelDrawable;
    private long modelId;
    private String modelName;
    private int rs;
    private int type;
    private long ut;

    public boolean isEmpty() {
        return this.isEmpty;
    }

    public void setEmpty(boolean isEmpty2) {
        this.isEmpty = isEmpty2;
    }

    public int getRs() {
        return this.rs;
    }

    public void setRs(int rs2) {
        this.rs = rs2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public String getActivityName() {
        return this.activityName;
    }

    public void setActivityName(String activityName2) {
        this.activityName = activityName2;
    }

    public String getModelDrawable() {
        return this.modelDrawable;
    }

    public void setModelDrawable(String modelDrawable2) {
        this.modelDrawable = modelDrawable2;
    }

    public String getModelName() {
        return this.modelName;
    }

    public void setModelName(String modelName2) {
        this.modelName = modelName2;
    }

    public long getModelId() {
        return this.modelId;
    }

    public void setModelId(long modelId2) {
        this.modelId = modelId2;
    }

    public int getModelAction() {
        return this.modelAction;
    }

    public void setModelAction(int modelAction2) {
        this.modelAction = modelAction2;
    }

    public long getUt() {
        return this.ut;
    }

    public void setUt(long ut2) {
        this.ut = ut2;
    }
}
