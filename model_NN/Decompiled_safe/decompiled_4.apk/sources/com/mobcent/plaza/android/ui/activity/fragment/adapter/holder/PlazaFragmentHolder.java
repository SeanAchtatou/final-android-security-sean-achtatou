package com.mobcent.plaza.android.ui.activity.fragment.adapter.holder;

import android.widget.ImageView;
import android.widget.TextView;

public class PlazaFragmentHolder {
    private ImageView appImg;
    private TextView appText;

    public ImageView getAppImg() {
        return this.appImg;
    }

    public void setAppImg(ImageView appImg2) {
        this.appImg = appImg2;
    }

    public TextView getAppText() {
        return this.appText;
    }

    public void setAppText(TextView appText2) {
        this.appText = appText2;
    }
}
