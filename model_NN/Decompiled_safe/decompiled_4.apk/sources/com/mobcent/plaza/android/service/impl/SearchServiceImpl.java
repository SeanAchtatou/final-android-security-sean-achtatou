package com.mobcent.plaza.android.service.impl;

import android.content.Context;
import com.mobcent.forum.android.service.impl.helper.BaseJsonHelper;
import com.mobcent.forum.android.util.StringUtil;
import com.mobcent.plaza.android.api.SearchRestfulApiRequester;
import com.mobcent.plaza.android.service.SearchService;
import com.mobcent.plaza.android.service.impl.helper.SearchServiceImplHelper;
import com.mobcent.plaza.android.service.model.SearchModel;
import java.util.ArrayList;
import java.util.List;

public class SearchServiceImpl implements SearchService {
    private String TAG = "SearchServiceImpl";
    private Context context;

    public SearchServiceImpl(Context context2) {
        this.context = context2;
    }

    public List<SearchModel> getSearchList(int forumId, String forumKey, long userId, int baikeType, int searchMode, String keyWord, int page, int pageSize) {
        String jsonStr = SearchRestfulApiRequester.getSearchList(this.context, forumId, forumKey, userId, baikeType, searchMode, keyWord, page, pageSize);
        List<SearchModel> searchList = SearchServiceImplHelper.parseSearchList(jsonStr);
        if (searchList == null) {
            searchList = new ArrayList<>();
            String errorCode = BaseJsonHelper.formJsonRS(jsonStr);
            if (!StringUtil.isEmpty(errorCode)) {
                SearchModel searchModel = new SearchModel();
                searchModel.setErrorCode(errorCode);
                searchList.add(searchModel);
            }
        }
        return searchList;
    }
}
