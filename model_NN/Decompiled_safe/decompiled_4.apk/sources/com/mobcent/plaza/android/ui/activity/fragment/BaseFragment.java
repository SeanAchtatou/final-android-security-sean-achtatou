package com.mobcent.plaza.android.ui.activity.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Toast;
import com.mobcent.ad.android.ui.widget.gif.GifView;
import com.mobcent.forum.android.model.BaseModel;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.MCResource;
import com.mobcent.forum.android.util.StringUtil;
import com.mobcent.plaza.android.ui.activity.constant.PlazaConstant;
import com.mobcent.plaza.android.ui.activity.widget.PullToRefreshListView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseFragment extends Fragment implements PlazaConstant {
    protected String TAG;
    protected Activity activity;
    protected String appKey;
    protected Context context;
    protected List<GifView> gifViews;
    protected LayoutInflater inflater;
    private boolean isCache;
    private boolean isCreateView;
    private boolean isLocal;
    protected PullToRefreshListView.OnScrollListener listOnScrollListener = new PullToRefreshListView.OnScrollListener() {
        private int firstVisibleItem;
        private int totalItemCount;
        private int visibleItemCount;

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0) {
                recycleUrls();
            }
        }

        public void onScroll(AbsListView view, int firstVisibleItem2, int visibleItemCount2, int totalItemCount2) {
            this.firstVisibleItem = firstVisibleItem2;
            this.visibleItemCount = visibleItemCount2;
            this.totalItemCount = totalItemCount2 - 3;
        }

        private void recycleUrls() {
            new ArrayList();
            int firstIndex = 0;
            int endIndex = this.firstVisibleItem + this.visibleItemCount + ((BaseFragment.this.pageSize * 2) / 3);
            if (this.firstVisibleItem - ((BaseFragment.this.pageSize * 2) / 3) > 0) {
                firstIndex = this.firstVisibleItem - ((BaseFragment.this.pageSize * 2) / 3);
            }
            if (endIndex >= this.totalItemCount) {
                endIndex = this.totalItemCount;
            }
            List<String> imgUrls = BaseFragment.this.getImageUrls(firstIndex, endIndex);
            int len = imgUrls.size();
            for (int i = 0; i < len; i++) {
                BaseFragment.this.loaderImageMap.remove(imgUrls.get(i));
            }
            List<String> recycleUrls = new ArrayList<>();
            for (String add : BaseFragment.this.loaderImageMap.keySet()) {
                recycleUrls.add(add);
            }
            AsyncTaskLoaderImage.getInstance(BaseFragment.this.getActivity(), BaseFragment.this.TAG).recycleBitmaps(recycleUrls);
            BaseFragment.this.loaderImageMap.clear();
            int len2 = imgUrls.size();
            for (int i2 = 0; i2 < len2; i2++) {
                BaseFragment.this.loaderImageMap.put(imgUrls.get(i2), null);
            }
        }
    };
    protected List<AsyncTask<?, ?, ?>> loadDataAsyncTasks;
    protected List<String> loaderImagUrls;
    protected Map<String, String> loaderImageMap;
    protected Handler mHandler;
    protected MCResource mcResource;
    protected ProgressDialog myDialog;
    protected int page;
    protected int pageSize;
    protected View view;

    /* access modifiers changed from: protected */
    public abstract void cleanBitmapByUrl(List<String> list);

    /* access modifiers changed from: protected */
    public abstract List<String> getImageUrls(int i, int i2);

    /* access modifiers changed from: protected */
    public abstract void initData();

    /* access modifiers changed from: protected */
    public abstract View initViews(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle);

    /* access modifiers changed from: protected */
    public abstract void initWidgetActions();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.TAG = toString();
        this.loadDataAsyncTasks = new ArrayList();
        this.mcResource = MCResource.getInstance(getActivity());
        this.mHandler = new Handler();
        this.activity = getActivity();
        this.inflater = LayoutInflater.from(getActivity());
    }

    public View onCreateView(LayoutInflater inflater2, ViewGroup container, Bundle savedInstanceState) {
        this.loaderImagUrls = new ArrayList();
        this.loaderImageMap = new HashMap();
        this.gifViews = new ArrayList();
        initData();
        this.view = initViews(inflater2, container, savedInstanceState);
        initWidgetActions();
        return this.view;
    }

    public void warnMessageById(String warnMessId) {
        Toast.makeText(getActivity(), this.mcResource.getStringId(warnMessId), 0).show();
    }

    public void warnMessageByStr(String str) {
        Toast.makeText(getActivity(), str, 0).show();
    }

    protected static int checkGetData(boolean isRefresh, Object object, boolean isCreateView2) {
        List<Object> objects = null;
        if (object != null) {
            objects = new ArrayList<>();
            objects.add(object);
        }
        return checkGetData(isRefresh, objects, isCreateView2);
    }

    /* access modifiers changed from: protected */
    public boolean isServerMode(List<BaseModel> objects) {
        if (objects == null || objects.isEmpty() || !StringUtil.isEmpty(objects.get(0).getErrorCode())) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isServerMode(BaseModel object) {
        if (object == null || !StringUtil.isEmpty(object.getErrorCode())) {
            return false;
        }
        return true;
    }

    public void onDestroyView() {
        super.onDestroyView();
        for (String add : this.loaderImageMap.keySet()) {
            this.loaderImagUrls.add(add);
        }
        AsyncTaskLoaderImage.getInstance(getActivity(), this.TAG).recycleBitmaps(this.loaderImagUrls);
        this.loaderImageMap.clear();
        clearGifViews();
        clearImageUrls();
    }

    public void onDestroy() {
        super.onDestroy();
        clearAsyncTask();
    }

    private void clearAsyncTask() {
        int size = this.loadDataAsyncTasks.size();
        for (int i = 0; i < size; i++) {
            this.loadDataAsyncTasks.get(i).cancel(true);
        }
        this.loadDataAsyncTasks.clear();
    }

    private void clearGifViews() {
        for (GifView gifView : this.gifViews) {
            if (gifView != null) {
                gifView.free();
            }
        }
        this.gifViews.clear();
    }

    private void clearImageUrls() {
        if (this.loaderImagUrls != null && !this.loaderImagUrls.isEmpty()) {
            this.loaderImagUrls.clear();
        }
    }

    public void addLoaderImageUrl(String url) {
        this.loaderImageMap.put(url, null);
    }

    public void addAsyncTask(AsyncTask<?, ?, ?> asyncTask) {
        this.loadDataAsyncTasks.add(asyncTask);
    }

    /* access modifiers changed from: protected */
    public void setDBMode(boolean isLocal2) {
        this.isLocal = isLocal2;
    }

    /* access modifiers changed from: protected */
    public boolean getDBMode() {
        return this.isLocal;
    }

    /* access modifiers changed from: protected */
    public void setMemoryMode(boolean isCache2) {
        this.isCache = isCache2;
    }

    /* access modifiers changed from: protected */
    public boolean getMemory() {
        return this.isCache;
    }

    /* access modifiers changed from: protected */
    public void setCreateView(boolean isCreateView2) {
        this.isCreateView = isCreateView2;
    }

    /* access modifiers changed from: protected */
    public boolean getCreateView() {
        return this.isCreateView;
    }

    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }
}
