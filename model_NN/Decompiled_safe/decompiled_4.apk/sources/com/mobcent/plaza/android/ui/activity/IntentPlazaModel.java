package com.mobcent.plaza.android.ui.activity;

import com.mobcent.forum.android.model.BaseModel;
import com.mobcent.plaza.android.service.model.PlazaAppModel;
import java.util.Arrays;
import java.util.List;

public class IntentPlazaModel extends BaseModel {
    private static final long serialVersionUID = -6529123640276225577L;
    private int forumId;
    private String forumKey;
    private List<PlazaAppModel> plazaAppModels = null;
    private int[] searchTypes;
    private long userId;

    public int getForumId() {
        return this.forumId;
    }

    public void setForumId(int forumId2) {
        this.forumId = forumId2;
    }

    public String getForumKey() {
        return this.forumKey;
    }

    public void setForumKey(String forumKey2) {
        this.forumKey = forumKey2;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
    }

    public int[] getSearchTypes() {
        return this.searchTypes;
    }

    public void setSearchTypes(int[] searchTypes2) {
        this.searchTypes = searchTypes2;
    }

    public List<PlazaAppModel> getPlazaAppModels() {
        return this.plazaAppModels;
    }

    public void setPlazaAppModels(List<PlazaAppModel> plazaAppModels2) {
        this.plazaAppModels = plazaAppModels2;
    }

    public String toString() {
        return "IntentPlazaModel [forumId=" + this.forumId + ", forumKey=" + this.forumKey + ", userId=" + this.userId + ", searchTypes=" + Arrays.toString(this.searchTypes) + ", plazaAppModels=" + this.plazaAppModels + "]";
    }
}
