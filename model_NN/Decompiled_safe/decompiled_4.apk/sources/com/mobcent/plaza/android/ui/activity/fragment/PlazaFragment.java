package com.mobcent.plaza.android.ui.activity.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.MCResource;
import com.mobcent.plaza.android.service.model.PlazaAppModel;
import com.mobcent.plaza.android.ui.activity.fragment.adapter.PlazaFragmentAdapter;
import java.util.ArrayList;
import java.util.List;

public class PlazaFragment extends Fragment {
    /* access modifiers changed from: private */
    public String TAG = "PlazaFragment";
    private MCResource adResource;
    private GridView gird;
    private PlazaFragmentAdapter gridAdapter;
    private Handler handler = null;
    /* access modifiers changed from: private */
    public PlazaFragmentListener listener;
    private AdapterView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            MCLogUtil.e(PlazaFragment.this.TAG, "onItemClickListener  --- " + position);
            if (PlazaFragment.this.listener != null) {
                PlazaFragment.this.listener.plazaAppImgClick((PlazaAppModel) PlazaFragment.this.plazaAppModels.get(position));
            }
        }
    };
    /* access modifiers changed from: private */
    public List<PlazaAppModel> plazaAppModels;

    public interface PlazaFragmentListener {
        void plazaAppImgClick(PlazaAppModel plazaAppModel);
    }

    public PlazaFragment() {
    }

    public PlazaFragment(Handler handler2, List<PlazaAppModel> plazaAppModels2) {
        this.handler = handler2;
        this.plazaAppModels = plazaAppModels2;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.listener = (PlazaFragmentListener) activity;
        } catch (Exception e) {
            MCLogUtil.e(this.TAG, e.toString());
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.adResource = MCResource.getInstance(getActivity().getApplicationContext());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(this.adResource.getLayoutId("mc_plaza_fragment"), (ViewGroup) null);
        this.gird = (GridView) view.findViewById(this.adResource.getViewId("plaza_gird"));
        if (this.plazaAppModels == null) {
            this.plazaAppModels = new ArrayList();
        }
        this.gridAdapter = new PlazaFragmentAdapter(getActivity(), this.handler, this.plazaAppModels);
        this.gird.setAdapter((ListAdapter) this.gridAdapter);
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.gird.setOnItemClickListener(this.onItemClickListener);
    }

    public Handler getHandler() {
        return this.handler;
    }

    public void setHandler(Handler handler2) {
        this.handler = handler2;
    }

    public List<PlazaAppModel> getPlazaAppModels() {
        return this.plazaAppModels;
    }

    public void setPlazaAppModels(List<PlazaAppModel> plazaAppModels2) {
        this.plazaAppModels = plazaAppModels2;
    }
}
