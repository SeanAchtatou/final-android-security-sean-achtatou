package com.mobcent.plaza.android.api.constant;

public interface PlazaApiConstant {
    public static final String ACT = "act";
    public static final String MID = "mid";
    public static final String MN = "mn";
    public static final String MPIC = "mpic";
    public static final int PLAZA_POSITION = 7001;
    public static final String SDLIST = "sdList";
    public static final String UT = "ut";
}
