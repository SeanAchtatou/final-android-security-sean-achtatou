package com.mobcent.plaza.android.service.model;

import com.mobcent.forum.android.model.BaseModel;

public class SearchModel extends BaseModel {
    private static final long serialVersionUID = 4353917048542507681L;
    private int baikeType;
    private long boardId;
    private long forumId;
    private boolean isEssence;
    private boolean isHot;
    private boolean isTop;
    private String picPath;
    private float ratio;
    private String singer;
    private int sourceType;
    private int status;
    private String summary;
    private String title;
    private long topicId;
    private long userId;
    private String userNickName;

    public int getSourceType() {
        return this.sourceType;
    }

    public void setSourceType(int sourceType2) {
        this.sourceType = sourceType2;
    }

    public String getPicPath() {
        return this.picPath;
    }

    public void setPicPath(String picPath2) {
        this.picPath = picPath2;
    }

    public String getSummary() {
        return this.summary;
    }

    public void setSummary(String summary2) {
        this.summary = summary2;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status2) {
        this.status = status2;
    }

    public String getSinger() {
        return this.singer;
    }

    public void setSinger(String singer2) {
        this.singer = singer2;
    }

    public int getBaikeType() {
        return this.baikeType;
    }

    public void setBaikeType(int baikeType2) {
        this.baikeType = baikeType2;
    }

    public long getBoardId() {
        return this.boardId;
    }

    public void setBoardId(long boardId2) {
        this.boardId = boardId2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public float getRatio() {
        return this.ratio;
    }

    public void setRatio(float ratio2) {
        this.ratio = ratio2;
    }

    public long getForumId() {
        return this.forumId;
    }

    public void setForumId(long forumId2) {
        this.forumId = forumId2;
    }

    public boolean isTop() {
        return this.isTop;
    }

    public void setTop(boolean isTop2) {
        this.isTop = isTop2;
    }

    public boolean isHot() {
        return this.isHot;
    }

    public void setHot(boolean isHot2) {
        this.isHot = isHot2;
    }

    public String getUserNickName() {
        return this.userNickName;
    }

    public void setUserNickName(String userNickName2) {
        this.userNickName = userNickName2;
    }

    public long getUserId() {
        return this.userId;
    }

    public void setUserId(long userId2) {
        this.userId = userId2;
    }

    public long getTopicId() {
        return this.topicId;
    }

    public void setTopicId(long topicId2) {
        this.topicId = topicId2;
    }

    public boolean isEssence() {
        return this.isEssence;
    }

    public void setEssence(boolean isEssence2) {
        this.isEssence = isEssence2;
    }
}
