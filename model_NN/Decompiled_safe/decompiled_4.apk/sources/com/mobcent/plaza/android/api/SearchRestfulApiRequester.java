package com.mobcent.plaza.android.api;

import android.content.Context;
import com.mobcent.forum.android.api.util.HttpClientUtil;
import com.mobcent.forum.android.constant.BaseRestfulApiConstant;
import com.mobcent.forum.android.util.MCResource;
import com.mobcent.forum.android.util.PhoneUtil;
import com.mobcent.plaza.android.api.constant.SearchApiConstant;
import com.mobcent.plaza.android.ui.activity.delegate.PlazaConfig;
import java.util.HashMap;

public class SearchRestfulApiRequester implements SearchApiConstant {
    private static String urlString = null;

    public static String getSearchList(Context context, int forumId, String forumKey, long userId, int baikeType, int searchMode, String keyWord, int page, int pageSize) {
        if (PlazaConfig.BASE_SEARCH_REQUEST_URL == null) {
            PlazaConfig.BASE_SEARCH_REQUEST_URL = context.getString(MCResource.getInstance(context).getStringId("mc_plaza_search_base_request_url"));
        }
        urlString = PlazaConfig.BASE_SEARCH_REQUEST_URL + "search/searchKeyword.do";
        HashMap<String, String> params = new HashMap<>();
        params.put("forumId", forumId + "");
        params.put("forumKey", forumKey + "");
        params.put("baikeType", baikeType + "");
        params.put(SearchApiConstant.PARAM_SEARCH_MODE, searchMode + "");
        params.put("keyword", keyWord + "");
        params.put("userId", userId + "");
        params.put("page", page + "");
        params.put("pageSize", pageSize + "");
        params.put("imei", PhoneUtil.getIMEI(context));
        params.put("imsi", PhoneUtil.getIMSI(context));
        params.put("platType", "1");
        params.put("sdkVersion", BaseRestfulApiConstant.SDK_VERSION_VALUE);
        return HttpClientUtil.doPostRequest(urlString, params, context);
    }
}
