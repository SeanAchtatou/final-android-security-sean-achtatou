package com.mobcent.plaza.android.service;

import com.mobcent.plaza.android.service.model.SearchModel;
import java.util.List;

public interface SearchService {
    List<SearchModel> getSearchList(int i, String str, long j, int i2, int i3, String str2, int i4, int i5);
}
