package com.mobcent.plaza.android.service.impl;

import android.content.Context;
import com.mobcent.plaza.android.api.PlazaRestfulApiRequester;
import com.mobcent.plaza.android.api.constant.PlazaApiConstant;
import com.mobcent.plaza.android.db.PlazaDbHelper;
import com.mobcent.plaza.android.db.constant.PlazaDbColumnConstant;
import com.mobcent.plaza.android.service.PlazaService;
import com.mobcent.plaza.android.service.impl.helper.PlazaServiceImplHelper;
import com.mobcent.plaza.android.service.model.PlazaAppModel;
import java.util.List;

public class PlazaServiceImpl implements PlazaService, PlazaApiConstant, PlazaDbColumnConstant {
    private String SHARED_PRE_NAME = "mc_plaza.prefs";
    private String TAG = "PlazaServiceImpl";
    private Context context;

    public PlazaServiceImpl(Context context2) {
        this.context = context2;
    }

    public List<PlazaAppModel> getPlazaAppModelListByLocal() {
        List<PlazaAppModel> plazaAppModelList = PlazaServiceImplHelper.parsePlazaAppModelList(new PlazaDbHelper(this.context, PlazaDbColumnConstant.PLAZA_DB_NAME).getJsonStr());
        if (plazaAppModelList == null || plazaAppModelList.isEmpty() || !plazaAppModelList.get(0).isEmpty()) {
            return plazaAppModelList;
        }
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 12 */
    public List<PlazaAppModel> getPlazaAppModelListByNet(String appKey, long userId) {
        String jsonStr = PlazaRestfulApiRequester.getPlazaAppModelList(this.context, appKey, userId, getLocalUt());
        List<PlazaAppModel> plazaAppModelList = PlazaServiceImplHelper.parsePlazaAppModelList(jsonStr);
        PlazaDbHelper dbHelper = new PlazaDbHelper(this.context, PlazaDbColumnConstant.PLAZA_DB_NAME);
        if (plazaAppModelList == null || plazaAppModelList.isEmpty()) {
            return null;
        }
        if (plazaAppModelList.get(0).getRs() == 2) {
            plazaAppModelList = PlazaServiceImplHelper.parsePlazaAppModelList(dbHelper.getJsonStr());
        } else {
            setLocalUt(plazaAppModelList.get(0).getUt());
            dbHelper.setJsonStr(jsonStr);
        }
        if (plazaAppModelList.get(0).isEmpty()) {
            return null;
        }
        return plazaAppModelList;
    }

    public String getPlazaLinkUrl(String appKey, long mid, long userId) {
        return PlazaRestfulApiRequester.getPlazaLinkUrl(this.context, appKey, mid, userId);
    }

    public long getLocalUt() {
        return this.context.getSharedPreferences(this.SHARED_PRE_NAME, 3).getLong(PlazaApiConstant.UT, 1);
    }

    public void setLocalUt(long ut) {
        this.context.getSharedPreferences(this.SHARED_PRE_NAME, 3).edit().putLong(PlazaApiConstant.UT, ut).commit();
    }
}
