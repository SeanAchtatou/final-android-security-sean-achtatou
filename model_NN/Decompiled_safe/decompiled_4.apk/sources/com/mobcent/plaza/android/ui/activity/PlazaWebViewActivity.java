package com.mobcent.plaza.android.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.mobcent.plaza.android.ui.activity.constant.PlazaConstant;

public class PlazaWebViewActivity extends BaseFragmentActivity implements PlazaConstant {
    private RelativeLayout bottomBox;
    private boolean isTop = false;
    private RelativeLayout topBox;
    private String webUrl;
    private Button webviewBackBtn;
    /* access modifiers changed from: private */
    public WebView webviewBrowser;
    private Button webviewCloseBtn;
    /* access modifiers changed from: private */
    public ProgressBar webviewProgressbar;
    private Button webviewRefreshBtn;

    /* access modifiers changed from: protected */
    public void initData() {
        Intent intent = getIntent();
        this.webUrl = intent.getStringExtra(PlazaConstant.WEB_VIEW_URL);
        this.isTop = intent.getBooleanExtra(PlazaConstant.WEB_VIEW_TOP, false);
        if (this.webUrl == null) {
            warById("mc_forum_webview_url_error");
            finish();
            overridePendingTransition(0, 17432577);
        }
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        requestWindowFeature(1);
        setContentView(this.adResource.getLayoutId("mc_plaza_web_view"));
        this.topBox = (RelativeLayout) findViewById(this.adResource.getViewId("top_layout"));
        this.bottomBox = (RelativeLayout) findViewById(this.adResource.getViewId("bottom_layout"));
        this.webviewProgressbar = (ProgressBar) findViewById(this.adResource.getViewId("progress_bar"));
        if (this.isTop) {
            this.topBox.setVisibility(0);
            this.bottomBox.setVisibility(8);
            this.webviewRefreshBtn = (Button) findViewById(this.adResource.getViewId("top_refresh_btn"));
            this.webviewCloseBtn = (Button) findViewById(this.adResource.getViewId("top_close_btn"));
            this.webviewBackBtn = (Button) findViewById(this.adResource.getViewId("top_back_btn"));
        } else {
            this.topBox.setVisibility(8);
            this.bottomBox.setVisibility(0);
            this.webviewRefreshBtn = (Button) findViewById(this.adResource.getViewId("bottom_refresh_btn"));
            this.webviewCloseBtn = (Button) findViewById(this.adResource.getViewId("bottom_close_btn"));
            this.webviewBackBtn = (Button) findViewById(this.adResource.getViewId("bottom_back_btn"));
        }
        this.webviewBackBtn.setVisibility(8);
        this.webviewBrowser = (WebView) findViewById(this.adResource.getViewId("web_view"));
        this.webviewBrowser.getSettings().setJavaScriptEnabled(true);
        this.webviewBrowser.getSettings().setPluginsEnabled(true);
        this.webviewBrowser.getSettings().setSupportZoom(true);
        this.webviewBrowser.getSettings().setBuiltInZoomControls(true);
        this.webviewBrowser.addJavascriptInterface(new InJavaScriptLocalObj(), "local_obj");
        this.webviewBrowser.setWebViewClient(new MyWebViewClient());
        this.webviewBrowser.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    PlazaWebViewActivity.this.webviewProgressbar.setVisibility(8);
                } else {
                    PlazaWebViewActivity.this.webviewProgressbar.setVisibility(0);
                }
                PlazaWebViewActivity.this.webviewProgressbar.setProgress(progress);
            }
        });
        this.webviewBrowser.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                PlazaWebViewActivity.this.downApk(url);
            }
        });
        this.webviewBrowser.loadUrl(this.webUrl);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.webviewBackBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PlazaWebViewActivity.this.webviewBrowser != null && PlazaWebViewActivity.this.webviewBrowser.canGoBack()) {
                    PlazaWebViewActivity.this.webviewBrowser.goBack();
                }
            }
        });
        this.webviewCloseBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlazaWebViewActivity.this.webviewBrowser.setWebViewClient(new WebViewClient() {
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    }

                    public boolean shouldOverrideUrlLoading(WebView view, String url) {
                        return false;
                    }
                });
                PlazaWebViewActivity.this.finish();
                PlazaWebViewActivity.this.overridePendingTransition(0, 17432577);
            }
        });
        this.webviewRefreshBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlazaWebViewActivity.this.webviewBrowser.reload();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        overridePendingTransition(17432576, 17432577);
    }

    /* access modifiers changed from: private */
    public void downApk(String downurl) {
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.setData(Uri.parse(downurl));
            intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
            startActivity(intent);
        } catch (Exception e) {
            try {
                Intent intent2 = new Intent();
                intent2.setAction("android.intent.action.VIEW");
                intent2.setData(Uri.parse(downurl));
                startActivity(intent2);
            } catch (Exception e2) {
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.webviewBrowser == null || !this.webviewBrowser.canGoBack()) {
            finish();
            overridePendingTransition(0, 17432577);
            return super.onKeyDown(keyCode, event);
        }
        this.webviewBrowser.goBack();
        return true;
    }

    final class MyWebViewClient extends WebViewClient {
        MyWebViewClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }

    final class InJavaScriptLocalObj {
        InJavaScriptLocalObj() {
        }

        public void showSource(String html) {
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        this.webviewBrowser.stopLoading();
        this.webviewBrowser.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.webviewBrowser.setVisibility(8);
        this.webviewBrowser.stopLoading();
        this.webviewBrowser.clearView();
        this.webviewBrowser.freeMemory();
        this.webviewBrowser.destroy();
    }
}
