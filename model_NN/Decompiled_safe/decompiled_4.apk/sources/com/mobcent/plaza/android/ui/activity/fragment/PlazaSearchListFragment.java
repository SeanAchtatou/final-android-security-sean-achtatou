package com.mobcent.plaza.android.ui.activity.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.forum.android.util.StringUtil;
import com.mobcent.plaza.android.service.SearchService;
import com.mobcent.plaza.android.service.impl.SearchServiceImpl;
import com.mobcent.plaza.android.service.model.SearchModel;
import com.mobcent.plaza.android.ui.activity.PlazaSearchActivity;
import com.mobcent.plaza.android.ui.activity.fragment.adapter.PlazaSearchListFragmentAdapter;
import com.mobcent.plaza.android.ui.activity.fragment.adapter.holder.SearchListFragmentHolder;
import com.mobcent.plaza.android.ui.activity.model.PlazaSearchKeyModel;
import com.mobcent.plaza.android.ui.activity.widget.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

public class PlazaSearchListFragment extends BaseFragment {
    private ImageView backTopBtn;
    private Handler handler;
    private List<String> imgUrls = new ArrayList();
    /* access modifiers changed from: private */
    public boolean isSearchBtnClick = false;
    private PlazaSearchListFragmentAdapter listAdapter;
    /* access modifiers changed from: private */
    public PullToRefreshListView mPullRefreshListView;
    /* access modifiers changed from: private */
    public int page = 1;
    /* access modifiers changed from: private */
    public PlazaSearchKeyModel searchKeyModel;
    /* access modifiers changed from: private */
    public List<SearchModel> searchList;
    PullToRefreshListView.OnScrollListener searchScrollListener = new PullToRefreshListView.OnScrollListener() {
        private List<String> recyleUrls;

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0) {
                int length = view.getChildCount();
                this.recyleUrls = PlazaSearchListFragment.this.getRecyleUrls();
                for (int i = 0; i < length; i++) {
                    Object tag = view.getChildAt(i).getTag();
                    if (tag instanceof SearchListFragmentHolder) {
                        String visUrl = PlazaSearchListFragment.this.formatUrl100_100((String) ((SearchListFragmentHolder) tag).getThumbImg().getTag());
                        if (this.recyleUrls.contains(visUrl)) {
                            this.recyleUrls.remove(visUrl);
                        }
                    }
                }
                AsyncTaskLoaderImage.getInstance(PlazaSearchListFragment.this.getActivity(), PlazaSearchListFragment.this.TAG).recycleBitmaps(this.recyleUrls);
            }
        }

        public void onScroll(AbsListView arg0, int firstVisiableItem, int visibleItemCount, int totalItemCount) {
        }
    };
    /* access modifiers changed from: private */
    public SearchService searchService;

    static /* synthetic */ int access$108(PlazaSearchListFragment x0) {
        int i = x0.page;
        x0.page = i + 1;
        return i;
    }

    static /* synthetic */ int access$110(PlazaSearchListFragment x0) {
        int i = x0.page;
        x0.page = i - 1;
        return i;
    }

    public PlazaSearchListFragment() {
    }

    public PlazaSearchListFragment(Handler handler2) {
        this.handler = handler2;
    }

    /* access modifiers changed from: protected */
    public void initData() {
        this.searchService = new SearchServiceImpl(getActivity());
        this.searchList = new ArrayList();
    }

    /* access modifiers changed from: protected */
    public View initViews(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(this.mcResource.getLayoutId("mc_plaza_search_list_fragment"), (ViewGroup) null);
        this.backTopBtn = (ImageView) view.findViewById(this.mcResource.getViewId("lv_backtotop"));
        this.listAdapter = new PlazaSearchListFragmentAdapter(getActivity(), this.TAG, this.searchList, this.handler);
        this.mPullRefreshListView = (PullToRefreshListView) view.findViewById(this.mcResource.getViewId("pull_refresh_list"));
        this.mPullRefreshListView.setBackToTopView(this.backTopBtn);
        this.mPullRefreshListView.setAdapter((ListAdapter) this.listAdapter);
        this.mPullRefreshListView.onBottomRefreshComplete(3, getString(this.mcResource.getStringId("mc_plaza_search_no_search_data")));
        return view;
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.mPullRefreshListView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {
            /* Debug info: failed to restart local var, previous not found, register: 3 */
            public void onRefresh() {
                if (PlazaSearchListFragment.this.isSearchBtnClick) {
                    PlazaSearchListFragment.this.addAsyncTask(new GetData().execute(new Void[0]));
                } else {
                    ((PlazaSearchActivity) PlazaSearchListFragment.this.getActivity()).onSearchBtnClick();
                }
            }
        });
        this.mPullRefreshListView.setOnBottomRefreshListener(new PullToRefreshListView.OnBottomRefreshListener() {
            public void onRefresh() {
                PlazaSearchListFragment.this.addAsyncTask(new GetMore().execute(new Void[0]));
            }
        });
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    /* access modifiers changed from: protected */
    public void cleanBitmapByUrl(List<String> urlStrs) {
        AsyncTaskLoaderImage.getInstance(getActivity(), this.TAG).recycleBitmaps(urlStrs);
    }

    /* access modifiers changed from: protected */
    public List<String> getImageUrls(int startLen, int endLen) {
        List<String> strs = new ArrayList<>();
        for (int i = startLen; i < endLen; i++) {
            SearchModel searchModel = this.searchList.get(i);
            strs.add(AsyncTaskLoaderImage.formatUrl(searchModel.getBaseUrl() + searchModel.getPicPath(), "100x100"));
        }
        return strs;
    }

    public void requestData(PlazaSearchKeyModel searchKeyModel2) {
        this.searchKeyModel = searchKeyModel2;
        this.isSearchBtnClick = true;
        if (searchKeyModel2 == null || searchKeyModel2.getKeyWord() == null || "".equals(searchKeyModel2.getKeyWord())) {
            this.isSearchBtnClick = false;
            this.mPullRefreshListView.onRefreshComplete();
            this.mPullRefreshListView.onBottomRefreshComplete(3, getString(this.mcResource.getStringId("mc_plaza_search_no_search_data")));
            return;
        }
        this.mPullRefreshListView.onRefresh();
    }

    class GetData extends AsyncTask<Void, Void, List<SearchModel>> {
        GetData() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<SearchModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            int unused = PlazaSearchListFragment.this.page = 1;
        }

        /* access modifiers changed from: protected */
        public List<SearchModel> doInBackground(Void... params) {
            if (PlazaSearchListFragment.this.searchKeyModel == null) {
                return null;
            }
            return PlazaSearchListFragment.this.searchService.getSearchList(PlazaSearchListFragment.this.searchKeyModel.getForumId(), PlazaSearchListFragment.this.searchKeyModel.getForumKey(), PlazaSearchListFragment.this.searchKeyModel.getUserId(), PlazaSearchListFragment.this.searchKeyModel.getBaikeType(), PlazaSearchListFragment.this.searchKeyModel.getSearchMode(), PlazaSearchListFragment.this.searchKeyModel.getKeyWord(), PlazaSearchListFragment.this.page, 20);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<SearchModel> result) {
            PlazaSearchListFragment.this.mPullRefreshListView.onRefreshComplete();
            if (result != null) {
                if (result.isEmpty()) {
                    PlazaSearchListFragment.this.searchList.clear();
                    PlazaSearchListFragment.this.notifyListView();
                    PlazaSearchListFragment.this.mPullRefreshListView.onBottomRefreshComplete(3, PlazaSearchListFragment.this.getString(PlazaSearchListFragment.this.mcResource.getStringId("mc_plaza_no_data")));
                } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                    PlazaSearchListFragment.this.mPullRefreshListView.onBottomRefreshComplete(3, PlazaSearchListFragment.this.getString(PlazaSearchListFragment.this.mcResource.getStringId("mc_plaza_load_error")));
                } else {
                    PlazaSearchListFragment.this.searchList.clear();
                    PlazaSearchListFragment.this.searchList.addAll(result);
                    PlazaSearchListFragment.this.notifyListView();
                    if (result.get(0).isHasNext()) {
                        PlazaSearchListFragment.this.mPullRefreshListView.onBottomRefreshComplete(0);
                    } else {
                        PlazaSearchListFragment.this.mPullRefreshListView.onBottomRefreshComplete(3);
                    }
                }
                result.clear();
            } else {
                PlazaSearchListFragment.this.searchList.clear();
                PlazaSearchListFragment.this.notifyListView();
                PlazaSearchListFragment.this.mPullRefreshListView.onBottomRefreshComplete(3, PlazaSearchListFragment.this.getString(PlazaSearchListFragment.this.mcResource.getStringId("mc_plaza_search_no_search_data")));
            }
            boolean unused = PlazaSearchListFragment.this.isSearchBtnClick = false;
        }
    }

    class GetMore extends AsyncTask<Void, Void, List<SearchModel>> {
        GetMore() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<SearchModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            PlazaSearchListFragment.access$108(PlazaSearchListFragment.this);
        }

        /* access modifiers changed from: protected */
        public List<SearchModel> doInBackground(Void... params) {
            if (PlazaSearchListFragment.this.searchKeyModel == null) {
                return null;
            }
            return PlazaSearchListFragment.this.searchService.getSearchList(PlazaSearchListFragment.this.searchKeyModel.getForumId(), PlazaSearchListFragment.this.searchKeyModel.getForumKey(), PlazaSearchListFragment.this.searchKeyModel.getUserId(), PlazaSearchListFragment.this.searchKeyModel.getBaikeType(), PlazaSearchListFragment.this.searchKeyModel.getSearchMode(), PlazaSearchListFragment.this.searchKeyModel.getKeyWord(), PlazaSearchListFragment.this.page, 20);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<SearchModel> result) {
            if (result != null) {
                if (result.isEmpty()) {
                    PlazaSearchListFragment.access$110(PlazaSearchListFragment.this);
                    PlazaSearchListFragment.this.mPullRefreshListView.onBottomRefreshComplete(2);
                } else if (!StringUtil.isEmpty(result.get(0).getErrorCode())) {
                    PlazaSearchListFragment.access$110(PlazaSearchListFragment.this);
                    PlazaSearchListFragment.this.mPullRefreshListView.onBottomRefreshComplete(0);
                } else {
                    PlazaSearchListFragment.this.searchList.addAll(result);
                    if (result.get(0).isHasNext()) {
                        PlazaSearchListFragment.this.mPullRefreshListView.onBottomRefreshComplete(0);
                    } else {
                        PlazaSearchListFragment.this.mPullRefreshListView.onBottomRefreshComplete(3);
                    }
                }
                result.clear();
                return;
            }
            PlazaSearchListFragment.access$110(PlazaSearchListFragment.this);
            PlazaSearchListFragment.this.mPullRefreshListView.onBottomRefreshComplete(0);
        }
    }

    /* access modifiers changed from: private */
    public List<String> getRecyleUrls() {
        List<String> recyleUrls = new ArrayList<>();
        for (int i = 0; i < this.searchList.size(); i++) {
            SearchModel searchModel = this.searchList.get(i);
            String url = formatUrl100_100(searchModel.getBaseUrl() + searchModel.getPicPath());
            if (!recyleUrls.contains(url)) {
                recyleUrls.add(url);
            }
        }
        MCLogUtil.e(this.TAG, recyleUrls.toArray().toString());
        return recyleUrls;
    }

    /* access modifiers changed from: private */
    public String formatUrl100_100(String url) {
        if (url == null || url.equals("")) {
            return null;
        }
        return AsyncTaskLoaderImage.formatUrl(url, "100x100");
    }

    /* access modifiers changed from: private */
    public void notifyListView() {
        this.listAdapter.notifyDataSetChanged();
    }
}
