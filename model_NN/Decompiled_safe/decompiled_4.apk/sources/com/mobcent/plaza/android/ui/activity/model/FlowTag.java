package com.mobcent.plaza.android.ui.activity.model;

import java.io.Serializable;

public class FlowTag implements Serializable {
    private static final long serialVersionUID = 7732693584051165961L;
    private String baseUrl;
    private long boardId;
    private String errorCode;
    private boolean hasNext;
    private String iconUrl;
    private long lastUpdateTime;
    private int marginTop;
    private String middleUrl;
    private String originalUrl;
    private int page;
    private float ratio;
    private int rs = 1;
    private String thumbnailUrl;
    private String title;
    private long topicId;
    private int totalNum;

    public long getBoardId() {
        return this.boardId;
    }

    public void setBoardId(long boardId2) {
        this.boardId = boardId2;
    }

    public long getTopicId() {
        return this.topicId;
    }

    public void setTopicId(long topicId2) {
        this.topicId = topicId2;
    }

    public String getThumbnailUrl() {
        return this.thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl2) {
        this.thumbnailUrl = thumbnailUrl2;
    }

    public String getMiddleUrl() {
        return this.middleUrl;
    }

    public void setMiddleUrl(String middleUrl2) {
        this.middleUrl = middleUrl2;
    }

    public String getOriginalUrl() {
        return this.originalUrl;
    }

    public void setOriginalUrl(String originalUrl2) {
        this.originalUrl = originalUrl2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public float getRatio() {
        return this.ratio;
    }

    public void setRatio(float ratio2) {
        this.ratio = ratio2;
    }

    public int getMarginTop() {
        return this.marginTop;
    }

    public void setMarginTop(int marginTop2) {
        this.marginTop = marginTop2;
    }

    public int getRs() {
        return this.rs;
    }

    public void setRs(int rs2) {
        this.rs = rs2;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode2) {
        this.errorCode = errorCode2;
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    public void setBaseUrl(String baseUrl2) {
        this.baseUrl = baseUrl2;
    }

    public String getIconUrl() {
        return this.iconUrl;
    }

    public void setIconUrl(String iconUrl2) {
        this.iconUrl = iconUrl2;
    }

    public int getTotalNum() {
        return this.totalNum;
    }

    public void setTotalNum(int totalNum2) {
        this.totalNum = totalNum2;
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(int page2) {
        this.page = page2;
    }

    public long getLastUpdateTime() {
        return this.lastUpdateTime;
    }

    public void setLastUpdateTime(long lastUpdateTime2) {
        this.lastUpdateTime = lastUpdateTime2;
    }

    public boolean isHasNext() {
        return this.hasNext;
    }

    public void setHasNext(boolean hasNext2) {
        this.hasNext = hasNext2;
    }
}
