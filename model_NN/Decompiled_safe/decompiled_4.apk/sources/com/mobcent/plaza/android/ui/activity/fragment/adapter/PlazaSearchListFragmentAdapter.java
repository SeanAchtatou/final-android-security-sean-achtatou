package com.mobcent.plaza.android.ui.activity.fragment.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mobcent.forum.android.util.AsyncTaskLoaderImage;
import com.mobcent.forum.android.util.MCResource;
import com.mobcent.forum.android.util.StringUtil;
import com.mobcent.plaza.android.service.model.SearchModel;
import com.mobcent.plaza.android.ui.activity.delegate.PlazaConfig;
import com.mobcent.plaza.android.ui.activity.fragment.adapter.holder.SearchListFragmentHolder;
import java.util.List;

public class PlazaSearchListFragmentAdapter extends BaseAdapter {
    private String TAG;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public Handler handler;
    private LayoutInflater inflater;
    private MCResource mcResource;
    private List<SearchModel> searchList;

    public PlazaSearchListFragmentAdapter(Context context2, String TAG2, List<SearchModel> searchList2, Handler handler2) {
        this.inflater = LayoutInflater.from(context2);
        this.context = context2;
        this.TAG = TAG2;
        this.searchList = searchList2;
        this.handler = handler2;
        this.mcResource = MCResource.getInstance(context2);
    }

    public int getCount() {
        return this.searchList.size();
    }

    public Object getItem(int position) {
        return this.searchList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup arg2) {
        SearchListFragmentHolder holder;
        final SearchModel searchModel = this.searchList.get(position);
        if (convertView == null) {
            convertView = this.inflater.inflate(this.mcResource.getLayoutId("mc_plaza_search_list_fragment_item"), (ViewGroup) null);
            holder = new SearchListFragmentHolder();
            initHodlerView(convertView, holder);
            convertView.setTag(holder);
        } else {
            holder = (SearchListFragmentHolder) convertView.getTag();
        }
        initHodlerData(searchModel, holder);
        convertView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PlazaConfig.getInstance().getPlazaDelegate() != null) {
                    PlazaConfig.getInstance().getPlazaDelegate().onSearchItemClick(PlazaSearchListFragmentAdapter.this.context, searchModel);
                }
            }
        });
        return convertView;
    }

    private void initHodlerView(View view, SearchListFragmentHolder holder) {
        holder.setTitleText((TextView) view.findViewById(this.mcResource.getViewId("title_text")));
        holder.setDescribeText((TextView) view.findViewById(this.mcResource.getViewId("describe_text")));
        holder.setThumbImg((ImageView) view.findViewById(this.mcResource.getViewId("thumb_img")));
        holder.setThumbImgRight((ImageView) view.findViewById(this.mcResource.getViewId("thumb_img_right")));
    }

    private void initHodlerData(SearchModel searchModel, SearchListFragmentHolder holder) {
        ImageView tempImageView;
        holder.getTitleText().setText(searchModel.getTitle());
        if (searchModel.getBaikeType() == 4) {
            holder.getDescribeText().setText(searchModel.getSinger());
            ((LinearLayout.LayoutParams) holder.getDescribeText().getLayoutParams()).setMargins(0, 5, 0, 0);
        } else {
            holder.getDescribeText().setText(searchModel.getSummary());
            ((LinearLayout.LayoutParams) holder.getDescribeText().getLayoutParams()).setMargins(0, 0, 0, 0);
        }
        holder.getThumbImg().setImageDrawable(null);
        holder.getThumbImgRight().setImageDrawable(null);
        if (StringUtil.isEmpty(searchModel.getPicPath())) {
            holder.getThumbImg().setVisibility(8);
            holder.getThumbImgRight().setVisibility(8);
            return;
        }
        if (searchModel.getBaikeType() == 3) {
            holder.getThumbImg().setVisibility(8);
            holder.getThumbImgRight().setVisibility(0);
            tempImageView = holder.getThumbImgRight();
        } else {
            holder.getThumbImgRight().setVisibility(8);
            holder.getThumbImg().setVisibility(0);
            tempImageView = holder.getThumbImg();
        }
        loadImageByUrl(tempImageView, searchModel.getBaseUrl() + searchModel.getPicPath());
    }

    private void loadImageByUrl(final ImageView img, String url) {
        AsyncTaskLoaderImage.getInstance(this.context).loadAsync(AsyncTaskLoaderImage.formatUrl(url, "100x100"), new AsyncTaskLoaderImage.BitmapImageCallback() {
            public void onImageLoaded(final Bitmap bitmap, String url) {
                PlazaSearchListFragmentAdapter.this.handler.post(new Runnable() {
                    public void run() {
                        if (bitmap != null && !bitmap.isRecycled() && img.isShown()) {
                            TransitionDrawable td = new TransitionDrawable(new Drawable[]{new ColorDrawable(17170445), new BitmapDrawable(PlazaSearchListFragmentAdapter.this.context.getResources(), bitmap)});
                            td.startTransition(350);
                            img.setImageDrawable(td);
                        }
                    }
                });
            }
        });
    }

    public void setSearchList(List<SearchModel> searchList2) {
        this.searchList = searchList2;
    }
}
