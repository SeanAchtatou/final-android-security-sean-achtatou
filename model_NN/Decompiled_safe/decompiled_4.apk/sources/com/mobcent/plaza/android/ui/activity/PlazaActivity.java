package com.mobcent.plaza.android.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import com.mobcent.ad.android.model.AdIntentModel;
import com.mobcent.ad.android.ui.activity.WebViewActivity;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.plaza.android.api.constant.PlazaApiConstant;
import com.mobcent.plaza.android.service.PlazaService;
import com.mobcent.plaza.android.service.impl.PlazaServiceImpl;
import com.mobcent.plaza.android.service.model.PlazaAppModel;
import com.mobcent.plaza.android.ui.activity.adapter.PlazaViewPagerAdapter;
import com.mobcent.plaza.android.ui.activity.constant.PlazaConstant;
import com.mobcent.plaza.android.ui.activity.delegate.PlazaConfig;
import com.mobcent.plaza.android.ui.activity.fragment.PlazaFragment;
import java.util.ArrayList;
import java.util.List;

public class PlazaActivity extends BaseFragmentActivity implements PlazaFragment.PlazaFragmentListener, DialogInterface.OnDismissListener, PlazaApiConstant {
    public static final String APP_KEY = "appKey";
    public static final String INTENT_PLAZA_MODEL = "plazaIntentModel";
    public static final String SEARCH_TYPES = "searchTypes";
    public static final String USER_ID = "userId";
    private String TAG = "PlazaActivity";
    /* access modifiers changed from: private */
    public RelativeLayout aboutBox;
    /* access modifiers changed from: private */
    public Button aboutBtn;
    private RelativeLayout baseSearchBox;
    private View beforeView;
    private RelativeLayout channelBox;
    private AsyncTask<Void, Void, List<PlazaAppModel>> currentTask;
    /* access modifiers changed from: private */
    public IntentPlazaModel intentPlazaModel;
    /* access modifiers changed from: private */
    public RelativeLayout loadingBox;
    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (v == PlazaActivity.this.personalBox) {
                if (event.getAction() == 1) {
                    PlazaActivity.this.personalBtn.setPressed(false);
                } else {
                    PlazaActivity.this.personalBtn.setPressed(true);
                }
            } else if (v == PlazaActivity.this.setBox) {
                if (event.getAction() == 1) {
                    PlazaActivity.this.setBtn.setPressed(false);
                } else {
                    PlazaActivity.this.setBtn.setPressed(true);
                }
            } else if (v == PlazaActivity.this.aboutBox) {
                if (event.getAction() == 1) {
                    PlazaActivity.this.aboutBtn.setPressed(false);
                } else {
                    PlazaActivity.this.aboutBtn.setPressed(true);
                }
            }
            return false;
        }
    };
    /* access modifiers changed from: private */
    public RelativeLayout personalBox;
    /* access modifiers changed from: private */
    public Button personalBtn;
    /* access modifiers changed from: private */
    public List<PlazaAppModel> plazaAppModels;
    /* access modifiers changed from: private */
    public PlazaService plazaService;
    /* access modifiers changed from: private */
    public RelativeLayout setBox;
    /* access modifiers changed from: private */
    public Button setBtn;
    private ViewPager viewPager;
    /* access modifiers changed from: private */
    public PlazaViewPagerAdapter viewPagerAdapter;

    /* access modifiers changed from: protected */
    public void initData() {
        this.plazaAppModels = new ArrayList();
        this.plazaService = new PlazaServiceImpl(this);
        this.intentPlazaModel = (IntentPlazaModel) getIntent().getSerializableExtra("plazaIntentModel");
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        requestWindowFeature(1);
        setContentView(this.adResource.getLayoutId("mc_plaza_activity"));
        this.viewPager = (ViewPager) findViewById(this.adResource.getViewId("view_pager"));
        this.viewPagerAdapter = new PlazaViewPagerAdapter(getSupportFragmentManager(), this.plazaAppModels);
        this.viewPager.setAdapter(this.viewPagerAdapter);
        this.viewPager.setPageMargin(20);
        ((EditText) findViewById(this.adResource.getViewId("key_word_edit"))).setFocusable(false);
        this.channelBox = (RelativeLayout) findViewById(this.adResource.getViewId("mc_plaza_channel_layout"));
        this.beforeView = findViewById(this.adResource.getViewId("mc_plaza_before_view"));
        this.loadingBox = (RelativeLayout) findViewById(this.adResource.getViewId("loading_layout"));
        this.baseSearchBox = (RelativeLayout) findViewById(this.adResource.getViewId("base_search_layout"));
        this.personalBox = (RelativeLayout) findViewById(this.adResource.getViewId("personal_layout"));
        this.setBox = (RelativeLayout) findViewById(this.adResource.getViewId("set_layout"));
        this.aboutBox = (RelativeLayout) findViewById(this.adResource.getViewId("about_layout"));
        this.personalBtn = (Button) findViewById(this.adResource.getViewId("personal_btn"));
        this.setBtn = (Button) findViewById(this.adResource.getViewId("set_btn"));
        this.aboutBtn = (Button) findViewById(this.adResource.getViewId("about_btn"));
        if (this.intentPlazaModel != null) {
            if (this.intentPlazaModel.getSearchTypes() == null || this.intentPlazaModel.getSearchTypes().length == 0) {
                this.baseSearchBox.setVisibility(8);
            } else if (this.intentPlazaModel.getSearchTypes().length == 1) {
                this.channelBox.setVisibility(8);
            }
            getDataTask();
            return;
        }
        this.baseSearchBox.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void initWidgetActions() {
        this.beforeView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PlazaConfig.getInstance().getPlazaDelegate() != null) {
                    long userId = PlazaConfig.getInstance().getPlazaDelegate().getUserId(PlazaActivity.this);
                    if (userId > 0) {
                        PlazaActivity.this.intentPlazaModel.setUserId(userId);
                        Intent intent = new Intent(PlazaActivity.this, PlazaSearchActivity.class);
                        intent.putExtra("plazaIntentModel", PlazaActivity.this.intentPlazaModel);
                        PlazaActivity.this.startActivity(intent);
                    }
                }
            }
        });
        this.loadingBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PlazaActivity.this.loadingBox.setVisibility(8);
                PlazaActivity.this.setProgressBarVisibility(false);
            }
        });
        this.personalBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PlazaConfig.getInstance().getPlazaDelegate() != null) {
                    PlazaConfig.getInstance().getPlazaDelegate().onPersonalClick(PlazaActivity.this);
                }
            }
        });
        this.setBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PlazaConfig.getInstance().getPlazaDelegate() != null) {
                    PlazaConfig.getInstance().getPlazaDelegate().onSetClick(PlazaActivity.this);
                }
            }
        });
        this.aboutBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PlazaConfig.getInstance().getPlazaDelegate() != null) {
                    PlazaConfig.getInstance().getPlazaDelegate().onAboutClick(PlazaActivity.this);
                }
            }
        });
        this.personalBox.setOnTouchListener(this.onTouchListener);
        this.setBox.setOnTouchListener(this.onTouchListener);
        this.aboutBox.setOnTouchListener(this.onTouchListener);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    private void getDataTask() {
        this.currentTask = new GetDataTask().execute(new Void[0]);
    }

    class GetDataTask extends AsyncTask<Void, Void, List<PlazaAppModel>> {
        GetDataTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<PlazaAppModel>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            PlazaActivity.this.loadingBox.setVisibility(0);
            PlazaActivity.this.setProgressBarVisibility(true);
        }

        /* access modifiers changed from: protected */
        public List<PlazaAppModel> doInBackground(Void... params) {
            List<PlazaAppModel> listTemp = PlazaActivity.this.plazaService.getPlazaAppModelListByLocal();
            if (listTemp == null) {
                return PlazaActivity.this.plazaService.getPlazaAppModelListByNet(PlazaActivity.this.intentPlazaModel.getForumKey(), PlazaActivity.this.intentPlazaModel.getUserId());
            }
            new GetNetDataUpdateLocal().execute(new Void[0]);
            return listTemp;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<PlazaAppModel> result) {
            PlazaActivity.this.loadingBox.setVisibility(8);
            PlazaActivity.this.setProgressBarVisibility(false);
            if (result != null) {
                if (!result.isEmpty()) {
                    PlazaActivity.this.plazaAppModels.clear();
                    if (PlazaActivity.this.intentPlazaModel.getPlazaAppModels() != null) {
                        PlazaActivity.this.plazaAppModels.addAll(PlazaActivity.this.intentPlazaModel.getPlazaAppModels());
                    }
                    PlazaActivity.this.plazaAppModels.addAll(result);
                    PlazaActivity.this.viewPagerAdapter.setPlazaAppModels(PlazaActivity.this.plazaAppModels);
                    PlazaActivity.this.viewPagerAdapter.notifyDataSetChanged();
                } else if (PlazaActivity.this.intentPlazaModel.getPlazaAppModels() != null) {
                    PlazaActivity.this.plazaAppModels.addAll(PlazaActivity.this.intentPlazaModel.getPlazaAppModels());
                }
                result.clear();
            } else if (PlazaActivity.this.intentPlazaModel.getPlazaAppModels() != null) {
                PlazaActivity.this.plazaAppModels.addAll(PlazaActivity.this.intentPlazaModel.getPlazaAppModels());
            }
        }
    }

    class GetNetDataUpdateLocal extends AsyncTask<Void, Void, List<PlazaAppModel>> {
        GetNetDataUpdateLocal() {
        }

        /* access modifiers changed from: protected */
        public List<PlazaAppModel> doInBackground(Void... params) {
            return PlazaActivity.this.plazaService.getPlazaAppModelListByNet(PlazaActivity.this.intentPlazaModel.getForumKey(), PlazaActivity.this.intentPlazaModel.getUserId());
        }
    }

    public void onDismiss(DialogInterface dialog) {
        cancelTask();
    }

    private void cancelTask() {
        if (this.currentTask != null && !this.currentTask.isCancelled()) {
            this.currentTask.cancel(true);
        }
    }

    public void onBackPressed() {
        if (this.loadingBox.getVisibility() == 0) {
            this.loadingBox.setVisibility(8);
            setProgressBarVisibility(false);
        } else if (PlazaConfig.getInstance().getPlazaDelegate() == null) {
            super.onBackPressed();
        } else if (!PlazaConfig.getInstance().getPlazaDelegate().onPlazaBackPressed(this)) {
            super.onBackPressed();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void plazaAppImgClick(PlazaAppModel paAppModel) {
        Intent intent;
        String url = this.plazaService.getPlazaLinkUrl(this.intentPlazaModel.getForumKey(), paAppModel.getModelId(), this.intentPlazaModel.getUserId());
        if (paAppModel.getModelAction() == 0) {
            if (PlazaConfig.getInstance().getPlazaDelegate() != null) {
                PlazaConfig.getInstance().getPlazaDelegate().onAppItemClick(this, paAppModel);
            }
        } else if (paAppModel.getModelAction() == 3) {
            Intent intent2 = new Intent(this, WebViewActivity.class);
            AdIntentModel adIntentModel = new AdIntentModel();
            adIntentModel.setAid(paAppModel.getModelId());
            adIntentModel.setPo(PlazaApiConstant.PLAZA_POSITION);
            adIntentModel.setUrl(url);
            intent2.putExtra(WebViewActivity.AD_INTENT_MODEL, adIntentModel);
            startActivity(intent2);
        } else {
            MCLogUtil.e(this.TAG, "request url -- " + url);
            if (paAppModel.getModelAction() == 4) {
                intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse(url));
            } else {
                intent = new Intent(this, PlazaWebViewActivity.class);
                intent.putExtra(PlazaConstant.WEB_VIEW_URL, url);
                if (paAppModel.getModelAction() == 1) {
                    intent.putExtra(PlazaConstant.WEB_VIEW_TOP, true);
                } else if (paAppModel.getModelAction() == 2) {
                    intent.putExtra(PlazaConstant.WEB_VIEW_TOP, false);
                }
            }
            startActivity(intent);
        }
    }
}
