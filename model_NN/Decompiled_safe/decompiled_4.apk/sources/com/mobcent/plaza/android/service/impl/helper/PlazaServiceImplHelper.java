package com.mobcent.plaza.android.service.impl.helper;

import com.mobcent.ad.android.constant.AdConstant;
import com.mobcent.forum.android.constant.BaseRestfulApiConstant;
import com.mobcent.forum.android.util.MCLogUtil;
import com.mobcent.plaza.android.api.constant.PlazaApiConstant;
import com.mobcent.plaza.android.service.model.PlazaAppModel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class PlazaServiceImplHelper implements BaseRestfulApiConstant, AdConstant, PlazaApiConstant {
    private static String TAG = "PlazaServiceImplHelper";

    public static List<PlazaAppModel> parsePlazaAppModelList(String jsonStr) {
        List<PlazaAppModel> plazaAppModelList = new ArrayList<>();
        try {
            JSONObject jsonRoot = new JSONObject(jsonStr);
            int rs = jsonRoot.optInt("rs");
            if (rs == 0) {
                return null;
            }
            if (rs == 2) {
                PlazaAppModel plazaAppModel = new PlazaAppModel();
                plazaAppModel.setRs(rs);
                plazaAppModelList.add(plazaAppModel);
                return plazaAppModelList;
            }
            long ut = jsonRoot.optLong(PlazaApiConstant.UT);
            JSONArray jsonArray = jsonRoot.optJSONArray(PlazaApiConstant.SDLIST);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                PlazaAppModel plazaAppModel2 = new PlazaAppModel();
                plazaAppModel2.setModelName(json.optString(PlazaApiConstant.MN));
                plazaAppModel2.setModelDrawable(json.optString(PlazaApiConstant.MPIC));
                plazaAppModel2.setModelId(json.optLong(PlazaApiConstant.MID));
                plazaAppModel2.setModelAction(json.optInt(PlazaApiConstant.ACT));
                plazaAppModelList.add(plazaAppModel2);
            }
            if (plazaAppModelList.isEmpty() || ut == 0) {
                PlazaAppModel plazaAppModel3 = new PlazaAppModel();
                plazaAppModel3.setRs(1);
                plazaAppModel3.setUt(ut);
                plazaAppModel3.setEmpty(true);
                plazaAppModelList.add(plazaAppModel3);
            } else {
                ((PlazaAppModel) plazaAppModelList.get(0)).setUt(ut);
                ((PlazaAppModel) plazaAppModelList.get(0)).setRs(rs);
            }
            return plazaAppModelList;
        } catch (Exception e) {
            MCLogUtil.e(TAG, e.toString());
            return null;
        }
    }
}
