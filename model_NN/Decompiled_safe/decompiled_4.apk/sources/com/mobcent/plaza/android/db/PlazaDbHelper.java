package com.mobcent.plaza.android.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.mobcent.plaza.android.db.constant.PlazaDbColumnConstant;

public class PlazaDbHelper extends SQLiteOpenHelper implements PlazaDbColumnConstant {
    private SQLiteDatabase sqLiteDatabase;

    public PlazaDbHelper(Context context, String name) {
        super(context, name, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public PlazaDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PlazaDbColumnConstant.CREATE_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(PlazaDbColumnConstant.DROP_TABLE);
        onCreate(db);
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x001d A[LOOP:0: B:8:0x001d->B:11:0x002d, LOOP_START, PHI: r10 
      PHI: (r10v1 'jsonStr' java.lang.String) = (r10v0 'jsonStr' java.lang.String), (r10v3 'jsonStr' java.lang.String) binds: [B:4:0x0015, B:11:0x002d] A[DONT_GENERATE, DONT_INLINE]] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getJsonStr() {
        /*
            r11 = this;
            r2 = 0
            r10 = 0
            android.database.sqlite.SQLiteDatabase r0 = r11.getReadableDatabase()     // Catch:{ Exception -> 0x0019 }
            r11.sqLiteDatabase = r0     // Catch:{ Exception -> 0x0019 }
            android.database.sqlite.SQLiteDatabase r0 = r11.sqLiteDatabase
            java.lang.String r1 = "t_plaza"
            r3 = r2
            r4 = r2
            r5 = r2
            r6 = r2
            r7 = r2
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5, r6, r7)
            if (r8 != 0) goto L_0x001d
            r0 = r2
        L_0x0018:
            return r0
        L_0x0019:
            r0 = move-exception
            r9 = r0
            r0 = r2
            goto L_0x0018
        L_0x001d:
            boolean r0 = r8.moveToNext()
            if (r0 == 0) goto L_0x002f
            java.lang.String r0 = "jsonStr"
            int r0 = r8.getColumnIndex(r0)
            java.lang.String r10 = r8.getString(r0)
            if (r10 == 0) goto L_0x001d
        L_0x002f:
            r8.close()
            android.database.sqlite.SQLiteDatabase r0 = r11.sqLiteDatabase
            r0.close()
            r11.sqLiteDatabase = r2
            r0 = r10
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobcent.plaza.android.db.PlazaDbHelper.getJsonStr():java.lang.String");
    }

    public void setJsonStr(String jsonStr) {
        try {
            this.sqLiteDatabase = getWritableDatabase();
            this.sqLiteDatabase.delete(PlazaDbColumnConstant.TABLE_NAME, "1 = 1", null);
            ContentValues values = new ContentValues();
            values.put("jsonStr", jsonStr);
            this.sqLiteDatabase.insert(PlazaDbColumnConstant.TABLE_NAME, null, values);
            this.sqLiteDatabase.close();
            this.sqLiteDatabase = null;
        } catch (Exception e) {
        }
    }
}
