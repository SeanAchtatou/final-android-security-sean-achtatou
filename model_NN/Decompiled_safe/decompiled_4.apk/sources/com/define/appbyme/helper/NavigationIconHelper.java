package com.define.appbyme.helper;

import android.content.Context;
import android.content.Intent;
import com.appbyme.activity.application.AutogenApplication;
import com.appbyme.activity.constant.ConfigConstant;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.appbyme.classify.activity.ClassifyActivity;
import com.appbyme.forum.activity.AutogenHomeTopicFragmentActivity;
import com.appbyme.hot.activity.HotActivity;
import com.appbyme.newest.activity.NewestActivity;
import com.appbyme.plaza.activity.AutogenPlazaActivity;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import com.mobcent.forum.android.service.ForumService;
import com.mobcent.forum.android.service.UserService;
import com.mobcent.forum.android.service.impl.ForumServiceImpl;
import com.mobcent.forum.android.service.impl.UserServiceImpl;
import com.mobcent.forum.android.util.StringUtil;
import com.mobcent.plaza.android.ui.activity.IntentPlazaModel;
import java.util.List;

public class NavigationIconHelper implements ConfigConstant {
    public static int getTabImageResId(MCResource resource, AutogenModuleModel moduleModel) {
        switch (moduleModel.getModuleType()) {
            case 1:
                return resource.getDrawableId(!StringUtil.isEmpty(moduleModel.getModuleIcon()) ? moduleModel.getModuleIcon() : "mc_forum_main_bar_button4");
            case 9:
                return resource.getDrawableId(!StringUtil.isEmpty(moduleModel.getModuleIcon()) ? moduleModel.getModuleIcon() : "mc_forum_main_bar_button24");
            case ConfigConstant.HOT_ID:
                return resource.getDrawableId(!StringUtil.isEmpty(moduleModel.getModuleIcon()) ? moduleModel.getModuleIcon() : "mc_forum_main_bar_button47");
            case 15:
                return resource.getDrawableId(!StringUtil.isEmpty(moduleModel.getModuleIcon()) ? moduleModel.getModuleIcon() : "mc_forum_main_bar_button3");
            case 16:
                return resource.getDrawableId(!StringUtil.isEmpty(moduleModel.getModuleIcon()) ? moduleModel.getModuleIcon() : "mc_forum_main_bar_button14");
            default:
                return 0;
        }
    }

    public static Intent dispatchListPage(Context context, AutogenApplication application, AutogenModuleModel moduleModel) {
        switch (moduleModel.getModuleType()) {
            case 1:
                return new Intent(context, AutogenHomeTopicFragmentActivity.class);
            case 9:
                return initPlazaActivity(context, application);
            case ConfigConstant.HOT_ID:
                return new Intent(context, HotActivity.class);
            case 15:
                return new Intent(context, NewestActivity.class);
            case 16:
                return new Intent(context, ClassifyActivity.class);
            default:
                return null;
        }
    }

    public static Intent dispatchChannelPage(Context context, AutogenApplication application, AutogenModuleModel moduleModel) {
        switch (moduleModel.getModuleType()) {
            case ConfigConstant.HOT_ID:
                return null;
            default:
                return new Intent("android.intent.action.VIEW");
        }
    }

    private static Intent initPlazaActivity(Context context, AutogenApplication application) {
        Intent tabIntent = new Intent(context, AutogenPlazaActivity.class);
        ForumService forumService = new ForumServiceImpl();
        UserService userService = new UserServiceImpl(context);
        SharedPreferencesDB shareDB = SharedPreferencesDB.getInstance(context);
        IntentPlazaModel intentModel = new IntentPlazaModel();
        intentModel.setForumId(shareDB.getForumid());
        intentModel.setForumKey(forumService.getForumKey(context));
        intentModel.setUserId(userService.getLoginUserId());
        List<Integer> searchTypeList = application.getConfigModel().getSearchTypes();
        int typeSize = searchTypeList.size();
        Integer[] array = new Integer[typeSize];
        searchTypeList.toArray(array);
        int[] typeArray = new int[typeSize];
        int len = array.length;
        for (int i = 0; i < len; i++) {
            typeArray[i] = array[i].intValue();
        }
        intentModel.setSearchTypes(new int[]{16});
        tabIntent.putExtra("plazaIntentModel", intentModel);
        return tabIntent;
    }
}
