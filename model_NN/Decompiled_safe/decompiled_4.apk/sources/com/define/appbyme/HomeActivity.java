package com.define.appbyme;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import com.appbyme.activity.application.AutogenApplication;
import com.appbyme.activity.constant.IntentConstant;
import com.appbyme.activity.observable.ActivityObserver;
import com.appbyme.android.base.model.AutogenModuleModel;
import com.define.appbyme.helper.NavigationIconHelper;
import com.mobcent.base.android.ui.activity.helper.MCForumHelper;
import com.mobcent.base.forum.android.util.MCPhoneUtil;
import com.mobcent.base.forum.android.util.MCResource;
import com.mobcent.forum.android.db.SharedPreferencesDB;
import java.util.List;
import java.util.Map;

public class HomeActivity extends TabActivity implements IntentConstant {
    public String ACTIVITY_TAG = getClass().getSimpleName();
    private ActivityObserver activityObserver;
    /* access modifiers changed from: private */
    public AutogenApplication application;
    private LinearLayout homeNavigate;
    private LayoutInflater inflater;
    private MCResource mcResource;
    private Map.Entry<Integer, List<AutogenModuleModel>>[] moduleMap;
    /* access modifiers changed from: private */
    public TabHost myTabHost;
    /* access modifiers changed from: private */
    public Intent tabIntent = null;
    /* access modifiers changed from: private */
    public String tabTag = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.mcResource = MCResource.getInstance(getApplicationContext());
        setContentView(this.mcResource.getLayoutId("home_activity"));
        this.application = AutogenApplication.getInstance();
        this.application.addActivity(this);
        this.activityObserver = new ActivityObserver(this);
        this.application.getActivityObservable().registerObserver(this.activityObserver);
        this.application.getActivityObservable().notifyActivityCreate(this.activityObserver, savedInstanceState);
        this.myTabHost = getTabHost();
        this.homeNavigate = (LinearLayout) findViewById(this.mcResource.getViewId("navigate"));
        this.inflater = LayoutInflater.from(this);
        TextView bottomBar = new TextView(getApplicationContext());
        if (!SharedPreferencesDB.getInstance(getApplicationContext()).getPayStatePowerBy()) {
            RelativeLayout.LayoutParams bottomBarLp = new RelativeLayout.LayoutParams(-2, -2);
            bottomBarLp.addRule(11);
            bottomBarLp.addRule(12);
            bottomBarLp.bottomMargin = MCPhoneUtil.getRawSize(getApplicationContext(), 1, 50.0f);
            bottomBarLp.rightMargin = MCPhoneUtil.getRawSize(getApplicationContext(), 1, 5.0f);
            bottomBar.setText(this.mcResource.getString("mc_forum_power_by"));
            bottomBar.setTextColor(-16777216);
            bottomBar.setTextSize(8.0f);
            ((RelativeLayout) this.homeNavigate.getParent()).addView(bottomBar, bottomBarLp);
        }
        MCForumHelper.prepareToLaunchForum(this);
        loadNavigateView();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.application.getActivityObservable().notifyActivitySaveInstanceState(this.activityObserver, outState);
    }

    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(this.mcResource.getAnimId("next_in"), this.mcResource.getAnimId("prev_out"));
    }

    public void finish() {
        super.finish();
        overridePendingTransition(this.mcResource.getAnimId("prev_in"), this.mcResource.getAnimId("next_out"));
    }

    private void loadNavigateView() {
        this.moduleMap = this.application.getModuleList();
        if (this.moduleMap != null) {
            for (Map.Entry<Integer, List<AutogenModuleModel>> value : this.moduleMap) {
                List<AutogenModuleModel> moduleList = value.getValue();
                if (moduleList.size() == 1) {
                    AutogenModuleModel moduleModel = (AutogenModuleModel) moduleList.get(0);
                    RelativeLayout homeNavigateItem = (RelativeLayout) this.inflater.inflate(this.mcResource.getLayoutId("home_navigate_item"), (ViewGroup) null);
                    Button navigateBtn = (Button) homeNavigateItem.findViewById(this.mcResource.getViewId("navigate_btn"));
                    navigateBtn.setBackgroundResource(NavigationIconHelper.getTabImageResId(this.mcResource, moduleModel));
                    navigateBtn.setText(moduleModel.getModuleName());
                    navigateBtn.setOnClickListener(new OnTabItemClickListener(navigateBtn, moduleModel));
                    this.homeNavigate.addView(homeNavigateItem, new LinearLayout.LayoutParams(-2, -2, 1.0f));
                }
            }
            if (this.homeNavigate.getChildCount() > 0) {
                Button firstBtn = (Button) this.homeNavigate.getChildAt(0).findViewById(this.mcResource.getViewId("navigate_btn"));
                firstBtn.performClick();
                firstBtn.setSelected(true);
            }
        }
    }

    private class OnTabItemClickListener implements View.OnClickListener {
        private AutogenModuleModel moduleModel;
        private Button tabBtn;

        public OnTabItemClickListener(Button tabBtn2, AutogenModuleModel moduleModel2) {
            this.tabBtn = tabBtn2;
            this.moduleModel = moduleModel2;
        }

        public void onClick(View arg0) {
            String unused = HomeActivity.this.tabTag = "TAB" + this.moduleModel.getModuleType() + "MODULEID" + this.moduleModel.getModuleId();
            Intent unused2 = HomeActivity.this.tabIntent = NavigationIconHelper.dispatchListPage(HomeActivity.this.getApplicationContext(), HomeActivity.this.application, this.moduleModel);
            if (HomeActivity.this.tabIntent != null) {
                HomeActivity.this.tabIntent.putExtra(IntentConstant.INTENT_MODULEMODEL, this.moduleModel);
                HomeActivity.this.myTabHost.addTab(HomeActivity.this.buildTabSpec(HomeActivity.this.tabTag, HomeActivity.this.tabIntent));
                HomeActivity.this.myTabHost.setCurrentTabByTag(HomeActivity.this.tabTag);
            }
            HomeActivity.this.selectTab(this.tabBtn);
        }
    }

    /* access modifiers changed from: protected */
    public TabHost.TabSpec buildTabSpec(String tag, Intent content) {
        return this.myTabHost.newTabSpec(tag).setIndicator(null, null).setContent(content);
    }

    /* access modifiers changed from: private */
    public void selectTab(Button btn) {
        int len = this.homeNavigate.getChildCount();
        for (int i = 0; i < len; i++) {
            ((Button) this.homeNavigate.getChildAt(i).findViewById(this.mcResource.getViewId("navigate_btn"))).setSelected(false);
        }
        btn.setSelected(true);
    }
}
