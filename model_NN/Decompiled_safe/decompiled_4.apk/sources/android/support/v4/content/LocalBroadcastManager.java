package android.support.v4.content;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import java.util.ArrayList;
import java.util.HashMap;

public class LocalBroadcastManager {
    private static final boolean DEBUG = false;
    static final int MSG_EXEC_PENDING_BROADCASTS = 1;
    private static final String TAG = "LocalBroadcastManager";
    private static LocalBroadcastManager mInstance;
    private static final Object mLock = new Object();
    private final HashMap<String, ArrayList<ReceiverRecord>> mActions = new HashMap<>();
    private final Context mAppContext;
    private final Handler mHandler;
    private final ArrayList<BroadcastRecord> mPendingBroadcasts = new ArrayList<>();
    private final HashMap<BroadcastReceiver, ArrayList<IntentFilter>> mReceivers = new HashMap<>();

    private static class ReceiverRecord {
        boolean broadcasting;
        final IntentFilter filter;
        final BroadcastReceiver receiver;

        ReceiverRecord(IntentFilter _filter, BroadcastReceiver _receiver) {
            this.filter = _filter;
            this.receiver = _receiver;
        }

        public String toString() {
            StringBuilder builder = new StringBuilder((int) AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER);
            builder.append("Receiver{");
            builder.append(this.receiver);
            builder.append(" filter=");
            builder.append(this.filter);
            builder.append("}");
            return builder.toString();
        }
    }

    private static class BroadcastRecord {
        final Intent intent;
        final ArrayList<ReceiverRecord> receivers;

        BroadcastRecord(Intent _intent, ArrayList<ReceiverRecord> _receivers) {
            this.intent = _intent;
            this.receivers = _receivers;
        }
    }

    public static LocalBroadcastManager getInstance(Context context) {
        LocalBroadcastManager localBroadcastManager;
        synchronized (mLock) {
            if (mInstance == null) {
                mInstance = new LocalBroadcastManager(context.getApplicationContext());
            }
            localBroadcastManager = mInstance;
        }
        return localBroadcastManager;
    }

    private LocalBroadcastManager(Context context) {
        this.mAppContext = context;
        this.mHandler = new Handler(context.getMainLooper()) {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        LocalBroadcastManager.this.executePendingBroadcasts();
                        return;
                    default:
                        super.handleMessage(msg);
                        return;
                }
            }
        };
    }

    public void registerReceiver(BroadcastReceiver receiver, IntentFilter filter) {
        synchronized (this.mReceivers) {
            ReceiverRecord entry = new ReceiverRecord(filter, receiver);
            ArrayList<IntentFilter> filters = this.mReceivers.get(receiver);
            if (filters == null) {
                filters = new ArrayList<>(1);
                this.mReceivers.put(receiver, filters);
            }
            filters.add(filter);
            for (int i = 0; i < filter.countActions(); i++) {
                String action = filter.getAction(i);
                ArrayList<ReceiverRecord> entries = this.mActions.get(action);
                if (entries == null) {
                    entries = new ArrayList<>(1);
                    this.mActions.put(action, entries);
                }
                entries.add(entry);
            }
        }
    }

    public void unregisterReceiver(BroadcastReceiver receiver) {
        synchronized (this.mReceivers) {
            ArrayList<IntentFilter> filters = this.mReceivers.remove(receiver);
            if (filters != null) {
                for (int i = 0; i < filters.size(); i++) {
                    IntentFilter filter = (IntentFilter) filters.get(i);
                    for (int j = 0; j < filter.countActions(); j++) {
                        String action = filter.getAction(j);
                        ArrayList<ReceiverRecord> receivers = this.mActions.get(action);
                        if (receivers != null) {
                            int k = 0;
                            while (k < receivers.size()) {
                                if (((ReceiverRecord) receivers.get(k)).receiver == receiver) {
                                    receivers.remove(k);
                                    k--;
                                }
                                k++;
                            }
                            if (receivers.size() <= 0) {
                                this.mActions.remove(action);
                            }
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0177, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean sendBroadcast(android.content.Intent r19) {
        /*
            r18 = this;
            r0 = r18
            java.util.HashMap<android.content.BroadcastReceiver, java.util.ArrayList<android.content.IntentFilter>> r0 = r0.mReceivers
            r17 = r0
            monitor-enter(r17)
            java.lang.String r4 = r19.getAction()     // Catch:{ all -> 0x0105 }
            r0 = r18
            android.content.Context r0 = r0.mAppContext     // Catch:{ all -> 0x0105 }
            r3 = r0
            android.content.ContentResolver r3 = r3.getContentResolver()     // Catch:{ all -> 0x0105 }
            r0 = r19
            r1 = r3
            java.lang.String r5 = r0.resolveTypeIfNeeded(r1)     // Catch:{ all -> 0x0105 }
            android.net.Uri r7 = r19.getData()     // Catch:{ all -> 0x0105 }
            java.lang.String r6 = r19.getScheme()     // Catch:{ all -> 0x0105 }
            java.util.Set r8 = r19.getCategories()     // Catch:{ all -> 0x0105 }
            int r3 = r19.getFlags()     // Catch:{ all -> 0x0105 }
            r3 = r3 & 8
            if (r3 == 0) goto L_0x00c8
            r3 = 1
            r10 = r3
        L_0x0031:
            if (r10 == 0) goto L_0x0062
            java.lang.String r3 = "LocalBroadcastManager"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0105 }
            r9.<init>()     // Catch:{ all -> 0x0105 }
            java.lang.String r11 = "Resolving type "
            java.lang.StringBuilder r9 = r9.append(r11)     // Catch:{ all -> 0x0105 }
            java.lang.StringBuilder r9 = r9.append(r5)     // Catch:{ all -> 0x0105 }
            java.lang.String r11 = " scheme "
            java.lang.StringBuilder r9 = r9.append(r11)     // Catch:{ all -> 0x0105 }
            java.lang.StringBuilder r9 = r9.append(r6)     // Catch:{ all -> 0x0105 }
            java.lang.String r11 = " of intent "
            java.lang.StringBuilder r9 = r9.append(r11)     // Catch:{ all -> 0x0105 }
            r0 = r9
            r1 = r19
            java.lang.StringBuilder r9 = r0.append(r1)     // Catch:{ all -> 0x0105 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0105 }
            android.util.Log.v(r3, r9)     // Catch:{ all -> 0x0105 }
        L_0x0062:
            r0 = r18
            java.util.HashMap<java.lang.String, java.util.ArrayList<android.support.v4.content.LocalBroadcastManager$ReceiverRecord>> r0 = r0.mActions     // Catch:{ all -> 0x0105 }
            r3 = r0
            java.lang.String r9 = r19.getAction()     // Catch:{ all -> 0x0105 }
            java.lang.Object r11 = r3.get(r9)     // Catch:{ all -> 0x0105 }
            java.util.ArrayList r11 = (java.util.ArrayList) r11     // Catch:{ all -> 0x0105 }
            if (r11 == 0) goto L_0x0176
            if (r10 == 0) goto L_0x008d
            java.lang.String r3 = "LocalBroadcastManager"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0105 }
            r9.<init>()     // Catch:{ all -> 0x0105 }
            java.lang.String r12 = "Action list: "
            java.lang.StringBuilder r9 = r9.append(r12)     // Catch:{ all -> 0x0105 }
            java.lang.StringBuilder r9 = r9.append(r11)     // Catch:{ all -> 0x0105 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0105 }
            android.util.Log.v(r3, r9)     // Catch:{ all -> 0x0105 }
        L_0x008d:
            r16 = 0
            r12 = 0
        L_0x0090:
            int r3 = r11.size()     // Catch:{ all -> 0x0105 }
            if (r12 >= r3) goto L_0x0134
            java.lang.Object r15 = r11.get(r12)     // Catch:{ all -> 0x0105 }
            android.support.v4.content.LocalBroadcastManager$ReceiverRecord r15 = (android.support.v4.content.LocalBroadcastManager.ReceiverRecord) r15     // Catch:{ all -> 0x0105 }
            if (r10 == 0) goto L_0x00b8
            java.lang.String r3 = "LocalBroadcastManager"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0105 }
            r9.<init>()     // Catch:{ all -> 0x0105 }
            java.lang.String r13 = "Matching against filter "
            java.lang.StringBuilder r9 = r9.append(r13)     // Catch:{ all -> 0x0105 }
            android.content.IntentFilter r13 = r15.filter     // Catch:{ all -> 0x0105 }
            java.lang.StringBuilder r9 = r9.append(r13)     // Catch:{ all -> 0x0105 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0105 }
            android.util.Log.v(r3, r9)     // Catch:{ all -> 0x0105 }
        L_0x00b8:
            boolean r3 = r15.broadcasting     // Catch:{ all -> 0x0105 }
            if (r3 == 0) goto L_0x00cc
            if (r10 == 0) goto L_0x00c5
            java.lang.String r3 = "LocalBroadcastManager"
            java.lang.String r9 = "  Filter's target already added"
            android.util.Log.v(r3, r9)     // Catch:{ all -> 0x0105 }
        L_0x00c5:
            int r12 = r12 + 1
            goto L_0x0090
        L_0x00c8:
            r3 = 0
            r10 = r3
            goto L_0x0031
        L_0x00cc:
            android.content.IntentFilter r3 = r15.filter     // Catch:{ all -> 0x0105 }
            java.lang.String r9 = "LocalBroadcastManager"
            int r13 = r3.match(r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x0105 }
            if (r13 < 0) goto L_0x0108
            if (r10 == 0) goto L_0x00f4
            java.lang.String r3 = "LocalBroadcastManager"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0105 }
            r9.<init>()     // Catch:{ all -> 0x0105 }
            java.lang.String r14 = "  Filter matched!  match=0x"
            java.lang.StringBuilder r9 = r9.append(r14)     // Catch:{ all -> 0x0105 }
            java.lang.String r13 = java.lang.Integer.toHexString(r13)     // Catch:{ all -> 0x0105 }
            java.lang.StringBuilder r9 = r9.append(r13)     // Catch:{ all -> 0x0105 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0105 }
            android.util.Log.v(r3, r9)     // Catch:{ all -> 0x0105 }
        L_0x00f4:
            if (r16 != 0) goto L_0x00fb
            java.util.ArrayList r16 = new java.util.ArrayList     // Catch:{ all -> 0x0105 }
            r16.<init>()     // Catch:{ all -> 0x0105 }
        L_0x00fb:
            r0 = r16
            r1 = r15
            r0.add(r1)     // Catch:{ all -> 0x0105 }
            r3 = 1
            r15.broadcasting = r3     // Catch:{ all -> 0x0105 }
            goto L_0x00c5
        L_0x0105:
            r3 = move-exception
            monitor-exit(r17)     // Catch:{ all -> 0x0105 }
            throw r3
        L_0x0108:
            if (r10 == 0) goto L_0x00c5
            switch(r13) {
                case -4: goto L_0x012b;
                case -3: goto L_0x0128;
                case -2: goto L_0x012e;
                case -1: goto L_0x0131;
                default: goto L_0x010d;
            }
        L_0x010d:
            java.lang.String r14 = "unknown reason"
        L_0x010f:
            java.lang.String r3 = "LocalBroadcastManager"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0105 }
            r9.<init>()     // Catch:{ all -> 0x0105 }
            java.lang.String r13 = "  Filter did not match: "
            java.lang.StringBuilder r9 = r9.append(r13)     // Catch:{ all -> 0x0105 }
            java.lang.StringBuilder r9 = r9.append(r14)     // Catch:{ all -> 0x0105 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0105 }
            android.util.Log.v(r3, r9)     // Catch:{ all -> 0x0105 }
            goto L_0x00c5
        L_0x0128:
            java.lang.String r14 = "action"
            goto L_0x010f
        L_0x012b:
            java.lang.String r14 = "category"
            goto L_0x010f
        L_0x012e:
            java.lang.String r14 = "data"
            goto L_0x010f
        L_0x0131:
            java.lang.String r14 = "type"
            goto L_0x010f
        L_0x0134:
            if (r16 == 0) goto L_0x0176
            r12 = 0
        L_0x0137:
            int r3 = r16.size()     // Catch:{ all -> 0x0105 }
            if (r12 >= r3) goto L_0x014c
            r0 = r16
            r1 = r12
            java.lang.Object r3 = r0.get(r1)     // Catch:{ all -> 0x0105 }
            android.support.v4.content.LocalBroadcastManager$ReceiverRecord r3 = (android.support.v4.content.LocalBroadcastManager.ReceiverRecord) r3     // Catch:{ all -> 0x0105 }
            r4 = 0
            r3.broadcasting = r4     // Catch:{ all -> 0x0105 }
            int r12 = r12 + 1
            goto L_0x0137
        L_0x014c:
            r0 = r18
            java.util.ArrayList<android.support.v4.content.LocalBroadcastManager$BroadcastRecord> r0 = r0.mPendingBroadcasts     // Catch:{ all -> 0x0105 }
            r3 = r0
            android.support.v4.content.LocalBroadcastManager$BroadcastRecord r4 = new android.support.v4.content.LocalBroadcastManager$BroadcastRecord     // Catch:{ all -> 0x0105 }
            r0 = r4
            r1 = r19
            r2 = r16
            r0.<init>(r1, r2)     // Catch:{ all -> 0x0105 }
            r3.add(r4)     // Catch:{ all -> 0x0105 }
            r0 = r18
            android.os.Handler r0 = r0.mHandler     // Catch:{ all -> 0x0105 }
            r3 = r0
            r4 = 1
            boolean r3 = r3.hasMessages(r4)     // Catch:{ all -> 0x0105 }
            if (r3 != 0) goto L_0x0173
            r0 = r18
            android.os.Handler r0 = r0.mHandler     // Catch:{ all -> 0x0105 }
            r3 = r0
            r4 = 1
            r3.sendEmptyMessage(r4)     // Catch:{ all -> 0x0105 }
        L_0x0173:
            r3 = 1
            monitor-exit(r17)     // Catch:{ all -> 0x0105 }
        L_0x0175:
            return r3
        L_0x0176:
            monitor-exit(r17)     // Catch:{ all -> 0x0105 }
            r3 = 0
            goto L_0x0175
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.content.LocalBroadcastManager.sendBroadcast(android.content.Intent):boolean");
    }

    public void sendBroadcastSync(Intent intent) {
        if (sendBroadcast(intent)) {
            executePendingBroadcasts();
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r3 >= r2.length) goto L_0x0000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        r1 = r2[r3];
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        if (r4 >= r1.receivers.size()) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002a, code lost:
        r1.receivers.get(r4).receiver.onReceive(r8.mAppContext, r1.intent);
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0041, code lost:
        r3 = r3 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001b, code lost:
        r3 = 0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void executePendingBroadcasts() {
        /*
            r8 = this;
        L_0x0000:
            r2 = 0
            java.util.HashMap<android.content.BroadcastReceiver, java.util.ArrayList<android.content.IntentFilter>> r5 = r8.mReceivers
            monitor-enter(r5)
            java.util.ArrayList<android.support.v4.content.LocalBroadcastManager$BroadcastRecord> r6 = r8.mPendingBroadcasts     // Catch:{ all -> 0x003e }
            int r0 = r6.size()     // Catch:{ all -> 0x003e }
            if (r0 > 0) goto L_0x000e
            monitor-exit(r5)     // Catch:{ all -> 0x003e }
            return
        L_0x000e:
            android.support.v4.content.LocalBroadcastManager$BroadcastRecord[] r2 = new android.support.v4.content.LocalBroadcastManager.BroadcastRecord[r0]     // Catch:{ all -> 0x003e }
            java.util.ArrayList<android.support.v4.content.LocalBroadcastManager$BroadcastRecord> r6 = r8.mPendingBroadcasts     // Catch:{ all -> 0x003e }
            r6.toArray(r2)     // Catch:{ all -> 0x003e }
            java.util.ArrayList<android.support.v4.content.LocalBroadcastManager$BroadcastRecord> r6 = r8.mPendingBroadcasts     // Catch:{ all -> 0x003e }
            r6.clear()     // Catch:{ all -> 0x003e }
            monitor-exit(r5)     // Catch:{ all -> 0x003e }
            r3 = 0
        L_0x001c:
            int r5 = r2.length
            if (r3 >= r5) goto L_0x0000
            r1 = r2[r3]
            r4 = 0
        L_0x0022:
            java.util.ArrayList<android.support.v4.content.LocalBroadcastManager$ReceiverRecord> r5 = r1.receivers
            int r5 = r5.size()
            if (r4 >= r5) goto L_0x0041
            java.util.ArrayList<android.support.v4.content.LocalBroadcastManager$ReceiverRecord> r5 = r1.receivers
            java.lang.Object r5 = r5.get(r4)
            android.support.v4.content.LocalBroadcastManager$ReceiverRecord r5 = (android.support.v4.content.LocalBroadcastManager.ReceiverRecord) r5
            android.content.BroadcastReceiver r5 = r5.receiver
            android.content.Context r6 = r8.mAppContext
            android.content.Intent r7 = r1.intent
            r5.onReceive(r6, r7)
            int r4 = r4 + 1
            goto L_0x0022
        L_0x003e:
            r6 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x003e }
            throw r6
        L_0x0041:
            int r3 = r3 + 1
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.content.LocalBroadcastManager.executePendingBroadcasts():void");
    }
}
