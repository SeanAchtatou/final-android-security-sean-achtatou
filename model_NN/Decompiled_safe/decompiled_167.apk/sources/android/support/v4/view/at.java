package android.support.v4.view;

import android.content.Context;
import android.text.method.SingleLineTransformationMethod;
import android.view.View;
import java.util.Locale;

/* compiled from: ProGuard */
class at extends SingleLineTransformationMethod {

    /* renamed from: a  reason: collision with root package name */
    private Locale f84a;

    public at(Context context) {
        this.f84a = context.getResources().getConfiguration().locale;
    }

    public CharSequence getTransformation(CharSequence charSequence, View view) {
        CharSequence transformation = super.getTransformation(charSequence, view);
        if (transformation != null) {
            return transformation.toString().toUpperCase(this.f84a);
        }
        return null;
    }
}
