package android.support.v4.view;

import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.a.a;
import android.support.v4.view.a.g;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

/* compiled from: ProGuard */
public class ViewCompat {
    private static final long FAKE_FRAME_TIME = 10;
    static final be IMPL;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_AUTO = 0;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_NO = 2;
    public static final int IMPORTANT_FOR_ACCESSIBILITY_YES = 1;
    public static final int LAYER_TYPE_HARDWARE = 2;
    public static final int LAYER_TYPE_NONE = 0;
    public static final int LAYER_TYPE_SOFTWARE = 1;
    public static final int OVER_SCROLL_ALWAYS = 0;
    public static final int OVER_SCROLL_IF_CONTENT_SCROLLS = 1;
    public static final int OVER_SCROLL_NEVER = 2;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 17) {
            IMPL = new bd();
        } else if (i >= 16) {
            IMPL = new bc();
        } else if (i >= 14) {
            IMPL = new bb();
        } else if (i >= 11) {
            IMPL = new ba();
        } else if (i >= 9) {
            IMPL = new az();
        } else {
            IMPL = new ay();
        }
    }

    public static boolean canScrollHorizontally(View view, int i) {
        return IMPL.a(view, i);
    }

    public static boolean canScrollVertically(View view, int i) {
        return IMPL.b(view, i);
    }

    public static int getOverScrollMode(View view) {
        return IMPL.a(view);
    }

    public static void setOverScrollMode(View view, int i) {
        IMPL.c(view, i);
    }

    public static void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        IMPL.a(view, accessibilityEvent);
    }

    public static void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        IMPL.b(view, accessibilityEvent);
    }

    public static void onInitializeAccessibilityNodeInfo(View view, a aVar) {
        IMPL.a(view, aVar);
    }

    public static void setAccessibilityDelegate(View view, AccessibilityDelegateCompat accessibilityDelegateCompat) {
        IMPL.a(view, accessibilityDelegateCompat);
    }

    public static boolean hasTransientState(View view) {
        return IMPL.b(view);
    }

    public static void setHasTransientState(View view, boolean z) {
        IMPL.a(view, z);
    }

    public static void postInvalidateOnAnimation(View view) {
        IMPL.c(view);
    }

    public static void postInvalidateOnAnimation(View view, int i, int i2, int i3, int i4) {
        IMPL.a(view, i, i2, i3, i4);
    }

    public static void postOnAnimation(View view, Runnable runnable) {
        IMPL.a(view, runnable);
    }

    public static void postOnAnimationDelayed(View view, Runnable runnable, long j) {
        IMPL.a(view, runnable, j);
    }

    public static int getImportantForAccessibility(View view) {
        return IMPL.d(view);
    }

    public static void setImportantForAccessibility(View view, int i) {
        IMPL.d(view, i);
    }

    public static boolean performAccessibilityAction(View view, int i, Bundle bundle) {
        return IMPL.a(view, i, bundle);
    }

    public static g getAccessibilityNodeProvider(View view) {
        return IMPL.e(view);
    }

    public static void setLayerType(View view, int i, Paint paint) {
        IMPL.a(view, i, paint);
    }

    public static int getLayerType(View view) {
        return IMPL.f(view);
    }

    public static int getLabelFor(View view) {
        return IMPL.g(view);
    }

    public static void setLabelFor(View view, int i) {
        IMPL.e(view, i);
    }
}
