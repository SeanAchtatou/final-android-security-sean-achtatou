package com.tencent.assistantv2.component;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.DownloadActivity;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.protocol.jce.HotWordItem;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.List;

/* compiled from: ProGuard */
public class HotwordsCardView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2996a;
    private RelativeLayout[] b;
    private TextView[] c;
    private TextView[] d;
    private TextView e;
    private RelativeLayout f;
    /* access modifiers changed from: private */
    public int g;

    public HotwordsCardView(Context context) {
        this(context, null);
    }

    public HotwordsCardView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2996a = false;
        this.b = new RelativeLayout[5];
        this.c = new TextView[5];
        this.d = new TextView[5];
        this.g = 0;
        setOrientation(1);
        LayoutInflater from = LayoutInflater.from(context);
        try {
            from.inflate((int) R.layout.common_hotwords, this);
        } catch (Throwable th) {
            cq.a().b();
            from.inflate((int) R.layout.common_hotwords, this);
        }
        this.e = (TextView) findViewById(R.id.more);
        this.f = (RelativeLayout) findViewById(R.id.moreLayout);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.hotwordLayout1);
        this.b[0] = relativeLayout;
        this.c[0] = (TextView) relativeLayout.findViewById(R.id.title);
        this.d[0] = (TextView) relativeLayout.findViewById(R.id.desc);
        RelativeLayout relativeLayout2 = (RelativeLayout) findViewById(R.id.hotwordLayout2);
        this.b[1] = relativeLayout2;
        this.c[1] = (TextView) relativeLayout2.findViewById(R.id.title);
        this.d[1] = (TextView) relativeLayout2.findViewById(R.id.desc);
        RelativeLayout relativeLayout3 = (RelativeLayout) findViewById(R.id.hotwordLayout3);
        this.b[2] = relativeLayout3;
        this.c[2] = (TextView) relativeLayout3.findViewById(R.id.title);
        this.d[2] = (TextView) relativeLayout3.findViewById(R.id.desc);
        RelativeLayout relativeLayout4 = (RelativeLayout) findViewById(R.id.hotwordLayout4);
        this.b[3] = relativeLayout4;
        this.c[3] = (TextView) relativeLayout4.findViewById(R.id.title);
        this.d[3] = (TextView) relativeLayout4.findViewById(R.id.desc);
        RelativeLayout relativeLayout5 = (RelativeLayout) findViewById(R.id.hotwordLayout5);
        this.b[4] = relativeLayout5;
        this.c[4] = (TextView) relativeLayout5.findViewById(R.id.title);
        this.d[4] = (TextView) relativeLayout5.findViewById(R.id.desc);
        setWillNotDraw(false);
    }

    public void a(int i) {
        this.g = i;
    }

    public boolean a(int i, String str, String str2, List<HotWordItem> list) {
        int i2;
        if (list == null || list.size() == 0) {
            return false;
        }
        int size = list.size();
        if (size > this.c.length) {
            i2 = this.c.length;
        } else {
            i2 = size;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            HotWordItem hotWordItem = list.get(i3);
            this.c[i3].setText(hotWordItem.a());
            this.d[i3].setText(String.format(getContext().getString(R.string.hotwords_card_search_count), ct.a((long) hotWordItem.b(), 2)));
            this.b[i3].setOnClickListener(new as(this, hotWordItem, i, i3));
        }
        this.e.setText(str);
        this.f.setOnClickListener(new at(this, str2, i));
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f2996a) {
            a();
            this.f2996a = true;
        }
    }

    private void a() {
        if (getContext() instanceof DownloadActivity) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), 100);
            buildSTInfo.slotId = a.a("04", 0);
            k.a(buildSTInfo);
        }
    }
}
