package com.tencent.assistantv2.component.banner;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.Banner;
import com.tencent.assistant.protocol.jce.BannerRollNode;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class j extends f {
    /* access modifiers changed from: private */
    public static String j = "福利中心";
    TXImageView e = null;
    TextView f = null;
    TXImageView g = null;
    TextView h = null;
    TextView i = null;

    public j(Banner banner) {
        super(banner);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(Context context, ViewGroup viewGroup, int i2, long j2, int i3) {
        int i4;
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.banner_three_node_view, viewGroup, false);
        a(context, inflate);
        this.e = (TXImageView) inflate.findViewById(R.id.title_img);
        this.f = (TextView) inflate.findViewById(R.id.title);
        this.f.setTextColor(d());
        this.e.updateImageView(this.f3103a.c, 0, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        if (this.f3103a.b.equals(j)) {
            b(context);
        }
        this.f.setText(this.f3103a.b);
        inflate.setOnClickListener(new k(this, context, i3));
        this.g = (TXImageView) inflate.findViewById(R.id.roll_img);
        this.h = (TextView) inflate.findViewById(R.id.roll_title);
        this.i = (TextView) inflate.findViewById(R.id.roll_desc);
        BannerRollNode bannerRollNode = null;
        if (this.f3103a.b() != null && this.f3103a.b().size() > 0) {
            int a2 = a(i2, j2, i3);
            if (a2 < 0 || a2 >= this.f3103a.b().size()) {
                i4 = 0;
            } else {
                i4 = a2;
            }
            bannerRollNode = this.f3103a.b().get(i4);
            TemporaryThreadManager.get().start(new l(this, i2, j2, i3, i4 + 1));
        }
        if (bannerRollNode != null) {
            XLog.d("banner", "**** bannerRollNode.img=" + bannerRollNode.f2020a);
            this.g.updateImageView(bannerRollNode.f2020a, 0, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            int b = b(bannerRollNode.d);
            this.h.setTextColor(b);
            this.h.setText(bannerRollNode.b);
            this.i.setTextColor(b);
            this.i.setText(bannerRollNode.c);
        }
        return inflate;
    }

    public void a(Context context) {
        int i2;
        int i3 = 0;
        if (context != null && this.c != null && !this.c.isRecycled()) {
            this.d.setBackgroundDrawable(new BitmapDrawable(context.getResources(), this.c));
            if (this.e != null) {
                this.e.setVisibility((this.f3103a.h & 2) == 0 ? 0 : 8);
            }
            if (this.f != null) {
                if ((this.f3103a.h & 1) == 0) {
                    i2 = 0;
                } else {
                    i2 = 8;
                }
                this.f.setVisibility(i2);
            }
            if ((this.f3103a.h & 4) != 0) {
                i3 = 8;
            }
            if (this.g != null) {
                this.g.setVisibility(i3);
            }
            if (this.h != null) {
                this.h.setVisibility(i3);
            }
            if (this.i != null) {
                this.i.setVisibility(i3);
            }
        }
    }

    public int a() {
        return 3;
    }

    private void b(Context context) {
        if (context != null) {
            TemporaryThreadManager.get().start(new m(this, context));
        }
    }
}
