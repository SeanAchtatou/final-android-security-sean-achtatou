package com.tencent.assistantv2.component.search;

import android.text.TextUtils;
import android.view.View;

/* compiled from: ProGuard */
class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NativeSearchResultPage f3233a;

    a(NativeSearchResultPage nativeSearchResultPage) {
        this.f3233a = nativeSearchResultPage;
    }

    public void onClick(View view) {
        this.f3233a.n();
        if (!TextUtils.isEmpty(this.f3233a.f3224a) && this.f3233a.r != null) {
            this.f3233a.r.b(this.f3233a.f3224a, this.f3233a.d);
        }
    }
}
