package com.tencent.assistantv2.component;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

/* compiled from: ProGuard */
public class BreakTextView extends TextView {

    /* renamed from: a  reason: collision with root package name */
    private o f2982a;

    public void a(o oVar) {
        this.f2982a = oVar;
    }

    public BreakTextView(Context context) {
        super(context);
    }

    public BreakTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    private int b() {
        return (getWidth() - getPaddingLeft()) - getPaddingRight();
    }

    public boolean a() {
        if (getPaint().measureText(getText().toString()) > ((float) b())) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.f2982a != null) {
            this.f2982a.a(a());
        }
    }
}
