package com.tencent.assistantv2.component.banner;

import android.graphics.Bitmap;
import com.tencent.assistant.manager.notification.a.a.e;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class g implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f3107a;

    g(f fVar) {
        this.f3107a = fVar;
    }

    public void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            XLog.d("banner", "**** onLoadImageFinish bgImgUrl=" + this.f3107a.f3103a.g);
            this.f3107a.c = bitmap;
        }
    }
}
