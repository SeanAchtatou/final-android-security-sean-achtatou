package com.tencent.assistantv2.component.topbanner;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.notification.a.a.c;
import com.tencent.assistant.manager.notification.a.a.g;
import com.tencent.assistant.model.r;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class TopBannerView extends LinearLayout {
    private static int u = 2;

    /* renamed from: a  reason: collision with root package name */
    protected int f3250a;
    protected String b;
    ImageView c;
    /* access modifiers changed from: private */
    public a d;
    /* access modifiers changed from: private */
    public r e;
    /* access modifiers changed from: private */
    public Bitmap f;
    /* access modifiers changed from: private */
    public Bitmap g;
    private boolean h;
    private boolean i;
    private g j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private float v;

    /* compiled from: ProGuard */
    enum ImageType {
        BANNER,
        BANNERBG
    }

    private void b() {
        this.p = (int) getContext().getResources().getDimension(R.dimen.topbanner_layout_content_height);
        this.o = (int) getContext().getResources().getDimension(R.dimen.refresh_header_loading_layout_max_height);
        this.v = (((float) this.o) * 1.0f) / ((float) this.p);
        this.q = (int) getContext().getResources().getDimension(R.dimen.topbanner_layout_content_width);
        this.r = (int) Math.round(((double) ((df.c() - ((int) getContext().getResources().getDimension(R.dimen.tab_activity_header_height))) / 2)) * 1.2d);
        this.t = df.b();
        this.s = this.p;
    }

    /* access modifiers changed from: private */
    public void a(Bitmap bitmap, ImageType imageType) {
        XLog.d("jiaxinwu", "reset");
        if (imageType == ImageType.BANNER && this.q != bitmap.getWidth()) {
            this.p = bitmap.getHeight();
            this.q = bitmap.getWidth();
            this.o = (int) (((float) this.p) * this.v);
        }
        if (imageType == ImageType.BANNERBG && this.q != bitmap.getWidth()) {
            this.o = bitmap.getHeight();
            this.q = bitmap.getWidth();
            this.p = (int) (((float) this.o) / this.v);
        }
    }

    /* access modifiers changed from: private */
    public Bitmap b(Bitmap bitmap, ImageType imageType) {
        if (bitmap == null || bitmap.isRecycled()) {
            return null;
        }
        if (Math.abs(this.q - this.t) >= u || Math.abs(this.o - this.r) >= u) {
            if (((Math.abs(this.q - this.t) < u || this.t < this.q) && this.r < this.o) || (this.t < this.q && (Math.abs(this.o - this.r) < u || this.r < this.o))) {
                XLog.d("jiaxinwu", "case 2");
                if (imageType == ImageType.BANNER) {
                    return a(bitmap, this.s, this.t);
                }
                if (imageType == ImageType.BANNERBG) {
                    return b(bitmap, this.r, this.t);
                }
            } else {
                float max = Math.max((((float) this.t) * 1.0f) / ((float) this.q), (((float) this.r) * 1.0f) / ((float) this.o));
                int i2 = (int) (((float) this.q) * max);
                if (imageType == ImageType.BANNER) {
                    Bitmap createScaledBitmap = Bitmap.createScaledBitmap(bitmap, i2, (int) (max * ((float) this.p)), true);
                    bitmap.recycle();
                    return a(createScaledBitmap, this.s, this.t);
                } else if (imageType == ImageType.BANNERBG) {
                    Bitmap createScaledBitmap2 = Bitmap.createScaledBitmap(bitmap, i2, (int) (max * ((float) this.o)), true);
                    bitmap.recycle();
                    return b(createScaledBitmap2, this.r, this.t);
                }
            }
            XLog.d("jiaxinwu", "case 7");
            return bitmap;
        }
        XLog.d("jiaxinwu", "case 1");
        return bitmap;
    }

    private Bitmap a(Bitmap bitmap, int i2, int i3) {
        this.n = i2;
        this.m = i3;
        this.k = (bitmap.getWidth() - this.m) / 2;
        this.l = 0;
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, this.k, this.l, this.m, this.n);
        bitmap.recycle();
        return createBitmap;
    }

    private Bitmap b(Bitmap bitmap, int i2, int i3) {
        this.n = i2;
        this.m = i3;
        this.k = (bitmap.getWidth() - this.m) / 2;
        this.l = bitmap.getHeight() - this.n;
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, this.k, this.l, this.m, this.n);
        bitmap.recycle();
        return createBitmap;
    }

    public TopBannerView(Context context) {
        this(context, null, 0);
    }

    public TopBannerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TopBannerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        this.b = Constants.STR_EMPTY;
        this.f = null;
        this.g = null;
        this.c = null;
        this.h = false;
        this.i = true;
        this.j = new g();
        this.k = 0;
        this.l = 0;
        this.m = 0;
        this.n = 0;
        this.o = 0;
        this.p = 0;
        this.q = 0;
        this.r = 0;
        this.s = 0;
        this.t = 0;
        this.v = 0.0f;
        this.f3250a = i2;
        d();
    }

    public void a(a aVar) {
        this.d = aVar;
    }

    /* access modifiers changed from: private */
    public String c() {
        return Constants.VIA_REPORT_TYPE_SHARE_TO_QQ;
    }

    private void d() {
        removeAllViews();
        setOrientation(1);
        setGravity(17);
        this.c = new ImageView(getContext());
        ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new LinearLayout.LayoutParams(-2, -2);
        }
        this.c.setLayoutParams(layoutParams);
        this.c.setOnClickListener(new g(this));
        addView(this.c);
        a(c());
        b();
    }

    public void a(r rVar) {
        b(rVar);
    }

    public void a(boolean z, boolean z2) {
        if (z || z2) {
            this.h = z2;
            this.i = z;
            e();
        }
    }

    private void b(r rVar) {
        if (rVar != null && rVar.f1671a != 0) {
            if (this.e != null && (!(this.e.f1671a == rVar.f1671a && this.e.m == rVar.m) && !TextUtils.equals(this.e.d, rVar.d))) {
                a();
            }
            this.e = rVar;
            if (b.a().d(this.e)) {
                if (!i()) {
                    h();
                }
                if (!f()) {
                    g();
                }
                this.j.b();
                return;
            }
            a();
            this.d.b();
        }
    }

    private void e() {
        if (this.e != null && this.e.f1671a != 0) {
            if (!b.a().d(this.e)) {
                a();
                this.d.b();
            } else if (!this.i && !this.h) {
            } else {
                if (this.i) {
                    XLog.d("topbanner", "refreshTopBanner");
                    this.e.k++;
                    b.a().c(this.e);
                    j();
                    this.i = false;
                } else if (this.h) {
                    XLog.d("topbanner", "refreshTopBannerWith BG");
                    j();
                    this.h = false;
                }
            }
        }
    }

    public void a() {
        if (this.g != null && !this.g.isRecycled()) {
            this.g.recycle();
            this.g = null;
        }
        if (this.f != null && !this.f.isRecycled()) {
            this.f.recycle();
            this.f = null;
        }
        if (this.c != null) {
            this.c.setImageBitmap(null);
        }
    }

    /* access modifiers changed from: private */
    public boolean f() {
        if (this.g == null || this.g.isRecycled()) {
            return false;
        }
        return true;
    }

    private void g() {
        if (this.g != null) {
            this.g = null;
        }
        if (this.e != null && !TextUtils.isEmpty(this.e.d)) {
            c cVar = new c(this.e.d, 2);
            cVar.a(new h(this));
            this.j.a(cVar);
            XLog.d("topbanner", "run");
        }
    }

    private void h() {
        if (this.f != null) {
            this.f = null;
        }
        if (this.e != null && !TextUtils.isEmpty(this.e.e)) {
            c cVar = new c(this.e.e, 2);
            cVar.a(new k(this));
            this.j.a(cVar);
        }
    }

    /* access modifiers changed from: private */
    public boolean i() {
        if (this.f == null || this.f.isRecycled()) {
            return false;
        }
        return true;
    }

    public void a(String str) {
        this.b = str;
    }

    private void j() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), 100);
        buildSTInfo.slotId = c();
        if (this.e != null) {
            if (this.h) {
                buildSTInfo.status = "02";
            } else {
                buildSTInfo.status = "01";
            }
        }
        k.a(buildSTInfo);
    }
}
