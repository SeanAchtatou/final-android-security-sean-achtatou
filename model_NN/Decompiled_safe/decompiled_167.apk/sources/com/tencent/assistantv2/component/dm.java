package com.tencent.assistantv2.component;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class dm implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ dw f3178a;
    final /* synthetic */ TxManagerCommContainView b;

    dm(TxManagerCommContainView txManagerCommContainView, dw dwVar) {
        this.b = txManagerCommContainView;
        this.f3178a = dwVar;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.TxManagerCommContainView.a(com.tencent.assistantv2.component.TxManagerCommContainView, boolean):boolean
     arg types: [com.tencent.assistantv2.component.TxManagerCommContainView, int]
     candidates:
      com.tencent.assistantv2.component.TxManagerCommContainView.a(java.lang.String, int):android.text.SpannableString
      com.tencent.assistantv2.component.TxManagerCommContainView.a(com.tencent.assistantv2.component.TxManagerCommContainView, float):void
      com.tencent.assistantv2.component.TxManagerCommContainView.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.component.TxManagerCommContainView.a(android.app.Activity, com.tencent.assistant.model.PluginStartEntry):void
      com.tencent.assistantv2.component.TxManagerCommContainView.a(android.view.View, android.widget.RelativeLayout$LayoutParams):void
      com.tencent.assistantv2.component.TxManagerCommContainView.a(java.lang.String, android.text.SpannableString):void
      com.tencent.assistantv2.component.TxManagerCommContainView.a(com.tencent.assistantv2.component.TxManagerCommContainView, boolean):boolean */
    public void onAnimationEnd(Animation animation) {
        boolean unused = this.b.z = true;
        if (this.b.z) {
            this.b.g();
        }
        if (this.f3178a != null) {
            this.f3178a.b();
        }
    }
}
