package com.tencent.assistantv2.component;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.login.a.a;
import com.tencent.assistant.login.a.b;
import com.tencent.assistant.login.d;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.df;
import com.tencent.connect.common.Constants;
import java.lang.ref.WeakReference;

/* compiled from: ProGuard */
public class UcAccountView extends LinearLayout implements TXImageView.ITXImageViewListener {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3022a = UcAccountView.class.getSimpleName();
    private TXImageView b;
    private ImageView c;
    private TextView d;
    private LayoutInflater e;
    private WeakReference<Activity> f;
    private Context g = null;

    public UcAccountView(Context context) {
        super(context);
        this.g = context;
        this.e = LayoutInflater.from(this.g);
        c();
    }

    public UcAccountView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g = context;
        this.e = LayoutInflater.from(this.g);
        c();
    }

    private void c() {
        View inflate = this.e.inflate((int) R.layout.user_center_account_view, this);
        this.b = (TXImageView) inflate.findViewById(R.id.profile_icon);
        this.b.setListener(this);
        this.c = (ImageView) inflate.findViewById(R.id.profile_icon_ring);
        this.d = (TextView) inflate.findViewById(R.id.nick_name);
    }

    public void a(Activity activity) {
        this.f = new WeakReference<>(activity);
    }

    public void a() {
        b();
    }

    public void onTXImageViewLoadImageFinish(TXImageView tXImageView, Bitmap bitmap) {
        XLog.i(f3022a, "*** onTXImageViewLoadImageFinish ***");
        if (this.c != null) {
            this.c.setVisibility(0);
        }
    }

    public void b() {
        b f2 = a.f();
        if (this.b != null) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.b.getLayoutParams();
            if (d.a().j()) {
                this.b.updateImageView(f2.f1456a, R.drawable.tab_my_face, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                XLog.i(f3022a, "profileInfo.iconUrl = " + f2.f1456a);
                if (layoutParams != null) {
                    layoutParams.width = df.a(getContext(), 75.0f);
                    layoutParams.height = df.a(getContext(), 75.0f);
                    this.b.setLayoutParams(layoutParams);
                }
            } else {
                this.b.updateImageView(Constants.STR_EMPTY, R.drawable.tab_my_face, TXImageView.TXImageViewType.LOCAL_IMAGE);
                if (layoutParams != null) {
                    layoutParams.width = df.a(getContext(), 75.0f);
                    layoutParams.height = df.a(getContext(), 75.0f);
                    this.b.setLayoutParams(layoutParams);
                }
            }
        }
        if (this.d == null) {
            return;
        }
        if (d.a().j()) {
            this.d.setText(f2.b);
        } else {
            this.d.setText(getContext().getString(R.string.hint_login));
        }
    }
}
