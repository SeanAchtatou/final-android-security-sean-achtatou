package com.tencent.assistantv2.component;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.smtt.sdk.DownloadListener;

/* compiled from: ProGuard */
class dy implements DownloadListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxWebView f3189a;

    dy(TxWebView txWebView) {
        this.f3189a = txWebView;
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f3189a.d.a());
            if (!(this.f3189a.e instanceof Activity)) {
                intent.addFlags(268435456);
            }
            this.f3189a.e.startActivity(intent);
        } catch (Exception e) {
        }
    }
}
