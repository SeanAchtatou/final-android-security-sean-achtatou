package com.tencent.assistantv2.component;

import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.cy;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.m;
import com.tencent.assistantv2.manager.i;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class ap extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameRankNormalListView f3041a;

    ap(GameRankNormalListView gameRankNormalListView) {
        this.f3041a = gameRankNormalListView;
    }

    public void onTMAClick(View view) {
        boolean z = true;
        this.f3041a.n++;
        if (this.f3041a.n < 10) {
            int i = this.f3041a.n - 1;
            this.f3041a.m[i] = true;
            if (i > 0) {
                this.f3041a.m[i - 1] = false;
            }
            this.f3041a.j.postDelayed(new aq(this, i), 2000);
            if (this.f3041a.n == 5) {
                this.f3041a.i.setBackgroundResource(R.color.apk_list_bg);
            }
            if (this.f3041a.n > 5) {
                this.f3041a.l.setText(GameRankNormalListView.w[this.f3041a.n % 5]);
                this.f3041a.l.setVisibility(0);
                return;
            }
            SwitchButton switchButton = this.f3041a.j;
            if (this.f3041a.j.getSwitchState()) {
                z = false;
            }
            switchButton.updateSwitchStateWithAnim(z);
            m.a().s(this.f3041a.j.getSwitchState());
            i.a().b().a(this.f3041a.j.getSwitchState());
            if (this.f3041a.j.getSwitchState()) {
                Toast.makeText(this.f3041a.getContext(), (int) R.string.hide_installed_apps_in_list, 4).show();
            } else if (this.f3041a.o) {
                Toast.makeText(this.f3041a.getContext(), (int) R.string.hide_installed_apps_in_list_cancel_rank, 4).show();
            } else {
                Toast.makeText(this.f3041a.getContext(), (int) R.string.hide_installed_apps_in_list_cancel, 4).show();
            }
            this.f3041a.c.c();
            if (this.f3041a.c.getCount() > 0 && this.f3041a.b != null && !this.f3041a.b.h()) {
                this.f3041a.updateFootViewText();
            }
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 sTInfoV2 = new STInfoV2(this.f3041a.g.J(), RankNormalListView.ST_HIDE_INSTALLED_APPS, this.f3041a.g.J(), RankNormalListView.ST_HIDE_INSTALLED_APPS, 200);
        if (!this.f3041a.o || !(this.f3041a.g instanceof cy)) {
            sTInfoV2.slotId = a.a(RankNormalListView.ST_HIDE_INSTALLED_APPS, 0);
        } else {
            sTInfoV2.slotId = a.a(((cy) this.f3041a.g).K(), 0);
        }
        if (!i.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        sTInfoV2.actionId = 200;
        return sTInfoV2;
    }
}
