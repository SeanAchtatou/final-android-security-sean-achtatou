package com.tencent.assistantv2.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.DownloadActivity;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.m;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class aa extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadCenterButton f3028a;

    aa(DownloadCenterButton downloadCenterButton) {
        this.f3028a = downloadCenterButton;
    }

    public void onTMAClick(View view) {
        boolean z;
        Intent intent = new Intent(this.f3028a.e, DownloadActivity.class);
        if (this.f3028a.d.getVisibility() == 0) {
            z = true;
        } else {
            z = false;
        }
        intent.putExtra("has_new_app_to_install", z);
        this.f3028a.e.startActivity(intent);
        m.a().b(Constants.STR_EMPTY, "key_download_center_red_dot", false);
    }

    public STInfoV2 getStInfo(View view) {
        if (view == null) {
            return null;
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3028a.getContext(), 200);
        buildSTInfo.slotId = this.f3028a.k;
        return buildSTInfo;
    }
}
