package com.tencent.assistantv2.component.appdetail;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.CustomTextView;
import com.tencent.assistant.protocol.jce.AppTagInfo;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import java.util.List;

/* compiled from: ProGuard */
public class DetailTagView extends RelativeLayout implements CustomTextView.CustomTextViewInterface {

    /* renamed from: a  reason: collision with root package name */
    int f3052a = 0;
    private Context b;
    private CustomTextView c;
    private LinearLayout d;
    private DetailTagItemView[] e;
    private final int f = 4;

    public DetailTagView(Context context) {
        super(context);
        a(context);
    }

    public DetailTagView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public DetailTagView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        this.b = context;
        LayoutInflater.from(this.b).inflate((int) R.layout.appdetail_tag_view, this);
        this.c = (CustomTextView) findViewById(R.id.title);
        this.d = (LinearLayout) findViewById(R.id.content);
        this.e = new DetailTagItemView[8];
        this.e[0] = (DetailTagItemView) this.d.findViewById(R.id.tag1);
        this.e[1] = (DetailTagItemView) this.d.findViewById(R.id.tag2);
        this.e[2] = (DetailTagItemView) this.d.findViewById(R.id.tag3);
        this.e[3] = (DetailTagItemView) this.d.findViewById(R.id.tag4);
        this.e[4] = (DetailTagItemView) this.d.findViewById(R.id.tag5);
        this.e[5] = (DetailTagItemView) this.d.findViewById(R.id.tag6);
        this.e[6] = (DetailTagItemView) this.d.findViewById(R.id.tag7);
        this.e[7] = (DetailTagItemView) this.d.findViewById(R.id.tag8);
        this.c.setStListener(this);
    }

    public void a(String str, List<AppTagInfo> list, long j, String str2) {
        if (list == null || list.size() == 0) {
            setVisibility(8);
            return;
        }
        if (!TextUtils.isEmpty(str)) {
            this.c.setText(str);
            this.c.setVisibility(0);
        } else {
            this.c.setVisibility(8);
        }
        this.f3052a = Math.min(list.size(), 4);
        if (this.f3052a > 0) {
            for (int i = 0; i < this.f3052a; i++) {
                this.e[i].a(list.get(i), j, str2, i);
            }
            setVisibility(0);
            for (int i2 = this.f3052a; i2 < 4; i2++) {
                this.e[i2].setVisibility(4);
            }
        } else {
            setVisibility(8);
        }
        if (this.f3052a <= 4) {
            this.d.findViewById(R.id.area2).setVisibility(8);
        }
    }

    public void viewExposureST() {
        if (this.b instanceof AppDetailActivityV5) {
            for (int i = 0; i < this.f3052a; i++) {
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b, 100);
                buildSTInfo.slotId = a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, i);
                k.a(buildSTInfo);
            }
        }
    }
}
