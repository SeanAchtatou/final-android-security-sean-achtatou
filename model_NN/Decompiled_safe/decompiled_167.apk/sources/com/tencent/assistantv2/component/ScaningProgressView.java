package com.tencent.assistantv2.component;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.RollTextView;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.beacon.event.a;
import com.tencent.smtt.sdk.WebView;
import java.util.HashMap;

/* compiled from: ProGuard */
public class ScaningProgressView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    public Context f3011a;
    public LayoutInflater b;
    public View c;
    public Handler d;
    /* access modifiers changed from: private */
    public RollTextView e;
    private TextView f;
    private TextView g;
    /* access modifiers changed from: private */
    public ProgressBar h;
    /* access modifiers changed from: private */
    public ProgressResultView i;
    private boolean j;
    private float k;

    public ScaningProgressView(Context context) {
        this(context, null);
    }

    public ScaningProgressView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = true;
        this.k = 1.0f;
        this.f3011a = context;
        this.b = (LayoutInflater) this.f3011a.getSystemService("layout_inflater");
        this.d = new Handler();
        a();
        setOnClickListener(new cv(this));
    }

    /* access modifiers changed from: private */
    public void e() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        a.a("ScaningProgressViewClick", true, -1, -1, hashMap, false);
        XLog.d("beacon", "beacon report >> event: ScaningProgressViewClick, totalTime : 0, params : " + hashMap.toString());
    }

    public void a() {
        try {
            this.c = this.b.inflate((int) R.layout.scan_progress_layout, this);
        } catch (Throwable th) {
            cq.a().b();
        }
        if (Build.VERSION.SDK_INT <= 10) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeResource(getResources(), R.drawable.common_scan_big_01, options));
            View findViewById = this.c.findViewById(R.id.layout_view);
            View findViewById2 = this.c.findViewById(R.id.scanning_result);
            findViewById.setBackgroundDrawable(bitmapDrawable);
            findViewById2.setBackgroundDrawable(bitmapDrawable);
        } else {
            View findViewById3 = this.c.findViewById(R.id.layout_view);
            View findViewById4 = this.c.findViewById(R.id.scanning_result);
            findViewById3.setBackgroundResource(R.drawable.common_scan_big_01);
            findViewById4.setBackgroundResource(R.drawable.common_scan_big_01);
        }
        this.h = (ProgressBar) this.c.findViewById(R.id.pb_scanning);
        this.i = (ProgressResultView) this.c.findViewById(R.id.scanning_result);
        this.e = (RollTextView) this.c.findViewById(R.id.header_size);
        this.f = (TextView) this.c.findViewById(R.id.header_size_unit);
        this.g = (TextView) this.c.findViewById(R.id.header_size_tip);
        this.e.setUnitView(this.f);
        d();
        if (t.u().startsWith("Xiaomi")) {
            this.e.setTextSize(2, 28.0f);
        }
    }

    public void b() {
        this.h.setVisibility(0);
        this.i.setVisibility(4);
    }

    public void c() {
        this.h.setVisibility(4);
        this.i.setVisibility(0);
    }

    public void a(float f2, br brVar) {
        this.i.setVisibility(0);
        this.h.setVisibility(4);
        this.i.a(f2, brVar);
        this.e.setTextColor((int) WebView.NIGHT_MODE_COLOR);
    }

    public void a(long j2) {
        this.d.post(new cw(this, j2));
    }

    public void b(long j2) {
        a(j2, 2);
    }

    public void a(long j2, int i2) {
        this.d.post(new cx(this, i2, j2));
    }

    public void d() {
        this.e.setValue(0.0d);
    }
}
