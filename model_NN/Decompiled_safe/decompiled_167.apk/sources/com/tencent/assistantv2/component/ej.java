package com.tencent.assistantv2.component;

import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class ej implements ec {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxWebViewContainer f3198a;

    ej(TxWebViewContainer txWebViewContainer) {
        this.f3198a = txWebViewContainer;
    }

    public int a() {
        try {
            return ((BaseActivity) this.f3198a.t()).f();
        } catch (Exception e) {
            return 0;
        }
    }

    public void b() {
        if (!c.a()) {
            this.f3198a.c(30);
        } else {
            this.f3198a.c(20);
        }
    }

    public void c() {
        if (this.f3198a.g != null && !this.f3198a.g.c) {
            this.f3198a.d.setVisibility(0);
            XLog.d("TxWebViewContainer", "mLoadingView.setVisibility(View.VISIBLE)", new Throwable("donaldxu"));
        }
        if (this.f3198a.h != null) {
            this.f3198a.h.a();
        }
    }

    public void d() {
        this.f3198a.d.setVisibility(4);
        XLog.d("TxWebViewContainer", "mLoadingView.setVisibility(View.INVISIBLE)", new Throwable("donaldxu"));
        if (this.f3198a.h != null) {
            this.f3198a.h.b();
        }
    }
}
