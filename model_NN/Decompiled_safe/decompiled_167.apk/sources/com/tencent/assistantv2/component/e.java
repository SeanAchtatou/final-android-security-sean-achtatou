package com.tencent.assistantv2.component;

import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.m;
import com.tencent.assistantv2.manager.i;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class e extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppRankListView f3191a;

    e(AppRankListView appRankListView) {
        this.f3191a = appRankListView;
    }

    public void onTMAClick(View view) {
        boolean z = true;
        this.f3191a.l++;
        if (this.f3191a.l < 10) {
            int i = this.f3191a.l - 1;
            this.f3191a.k[i] = true;
            if (i > 0) {
                this.f3191a.k[i - 1] = false;
            }
            this.f3191a.h.postDelayed(new f(this, i), 2000);
            if (this.f3191a.l == 5) {
                this.f3191a.g.setBackgroundResource(R.color.apk_list_bg);
            }
            if (this.f3191a.l > 5) {
                this.f3191a.i.setText(AppRankListView.s[this.f3191a.l % 5]);
                this.f3191a.i.setVisibility(0);
                return;
            }
            SwitchButton switchButton = this.f3191a.h;
            if (this.f3191a.h.getSwitchState()) {
                z = false;
            }
            switchButton.updateSwitchStateWithAnim(z);
            m.a().s(this.f3191a.h.getSwitchState());
            i.a().b().a(this.f3191a.h.getSwitchState());
            if (this.f3191a.h.getSwitchState()) {
                Toast.makeText(this.f3191a.getContext(), (int) R.string.hide_installed_apps_in_list, 4).show();
            } else {
                Toast.makeText(this.f3191a.getContext(), (int) R.string.hide_installed_apps_in_list_cancel_rank, 4).show();
            }
            this.f3191a.b.c();
            if (this.f3191a.b.getCount() > 0 && this.f3191a.f2978a != null && !this.f3191a.f2978a.h()) {
                this.f3191a.updateFootViewText();
            }
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 sTInfoV2 = new STInfoV2(((BaseActivity) this.f3191a.getContext()).f(), RankNormalListView.ST_HIDE_INSTALLED_APPS, ((BaseActivity) this.f3191a.getContext()).f(), RankNormalListView.ST_HIDE_INSTALLED_APPS, 200);
        sTInfoV2.slotId = a.a(RankNormalListView.ST_HIDE_INSTALLED_APPS, 0);
        if (!i.a().b().c()) {
            sTInfoV2.status = "01";
        } else {
            sTInfoV2.status = "02";
        }
        sTInfoV2.actionId = 200;
        return sTInfoV2;
    }
}
