package com.tencent.assistantv2.component.appdetail;

import android.os.Handler;
import com.tencent.assistant.model.c;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class m implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CustomRelateAppViewV5 f3077a;

    m(CustomRelateAppViewV5 customRelateAppViewV5) {
        this.f3077a = customRelateAppViewV5;
    }

    public void a(int i, int i2, c cVar, int i3) {
        if (i2 != 0 || cVar == null || !AppDetailActivityV5.a(cVar)) {
            for (int i4 = 0; i4 < this.f3077a.m.size(); i4++) {
                if (this.f3077a.k[i4].getTag() != null && this.f3077a.k[i4].getTag().toString().equals(Constants.STR_EMPTY + i)) {
                    this.f3077a.k[i4].c();
                }
            }
            return;
        }
        new Handler().post(new n(this, i, cVar));
    }
}
