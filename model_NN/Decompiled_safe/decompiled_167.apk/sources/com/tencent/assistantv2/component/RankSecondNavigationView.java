package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.fps.FPSRankCutlineView;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class RankSecondNavigationView extends RelativeLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3009a;
    private List<RelativeLayout> b;
    /* access modifiers changed from: private */
    public co c;
    private int d;
    private ImageView e;
    private FPSRankCutlineView f;

    public RankSecondNavigationView(Context context) {
        this(context, null);
    }

    public RankSecondNavigationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = new ArrayList();
        this.d = -1;
        this.f3009a = context;
        a();
    }

    private void a() {
        this.b.clear();
        try {
            inflate(this.f3009a, R.layout.second_navigation_view, this);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.e = (ImageView) findViewById(R.id.cursor);
        this.f = (FPSRankCutlineView) findViewById(R.id.cutline);
        this.b.add((RelativeLayout) findViewById(R.id.item_layout_first));
        this.b.add((RelativeLayout) findViewById(R.id.item_layout_second));
        this.b.add((RelativeLayout) findViewById(R.id.item_layout_third));
        this.b.add((RelativeLayout) findViewById(R.id.item_layout_fourth));
    }

    public void a(List<String> list) {
        if (list != null && list.size() > 0) {
            setVisibility(0);
            for (int i = 0; i < list.size(); i++) {
                if (!(this.b == null || this.b.get(i) == null)) {
                    this.b.get(i).removeAllViews();
                    TextView textView = new TextView(this.f3009a);
                    textView.setText(list.get(i));
                    textView.setTextColor(this.f3009a.getResources().getColor(R.color.navigation_title_normal));
                    textView.setGravity(17);
                    textView.setTextSize(13.0f);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                    layoutParams.addRule(13, 1);
                    textView.setLayoutParams(layoutParams);
                    textView.setTag(Integer.valueOf((int) R.id.hot_navigation_text));
                    textView.setTag(R.id.hot_navigation_pos, Integer.valueOf(i));
                    this.b.get(i).addView(textView, layoutParams);
                    this.b.get(i).setVisibility(0);
                    this.b.get(i).setOnClickListener(new cm(this, i));
                }
            }
            int size = this.b.size() - 1;
            while (true) {
                int i2 = size;
                if (i2 < list.size()) {
                    break;
                }
                this.b.get(i2).removeAllViews();
                this.b.get(i2).setVisibility(8);
                size = i2 - 1;
            }
        } else {
            setVisibility(8);
        }
        forceLayout();
        invalidate();
    }

    public void a(int i) {
        int i2;
        TextView textView;
        View view;
        int i3;
        if (this.d == -1 || (view = this.b.get(this.d)) == null) {
            i2 = -1;
        } else {
            TextView textView2 = (TextView) view.findViewWithTag(Integer.valueOf((int) R.id.hot_navigation_text));
            if (textView2 != null) {
                i3 = view.getLeft() + textView2.getLeft();
            } else {
                i3 = -1;
            }
            textView2.setTextColor(this.f3009a.getResources().getColor(R.color.navigation_title_normal));
            i2 = i3;
        }
        View view2 = this.b.get(i);
        if (view2.getVisibility() == 0 && (textView = (TextView) view2.findViewWithTag(Integer.valueOf((int) R.id.hot_navigation_text))) != null && ((Integer) textView.getTag(R.id.hot_navigation_pos)).intValue() == i) {
            if (this.d != -1) {
                Animation a2 = a(i2, view2.getLeft() + textView.getLeft());
                if (a2 != null) {
                    this.e.clearAnimation();
                    a2.setFillEnabled(true);
                    a2.setFillAfter(false);
                    a2.setFillAfter(false);
                    this.e.startAnimation(a2);
                    a2.setAnimationListener(new cn(this, textView, view2));
                } else {
                    textView.setTextColor(this.f3009a.getResources().getColor(R.color.navigation_title_selected));
                    a(view2, textView);
                }
            } else {
                this.e.setBackgroundResource(R.drawable.common_two_navigation_01);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.e.getLayoutParams();
                layoutParams.leftMargin = view2.getLeft() + textView.getLeft();
                layoutParams.topMargin = textView.getTop();
                layoutParams.width = textView.getWidth();
                layoutParams.height = textView.getHeight();
                this.e.setLayoutParams(layoutParams);
                requestLayout();
                invalidate();
            }
        }
        this.d = i;
    }

    /* access modifiers changed from: private */
    public void a(View view, TextView textView) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.e.getLayoutParams();
        if (textView.getText().length() <= 3) {
            layoutParams.leftMargin = view.getLeft() + ((view.getWidth() - df.a(this.f3009a, 52.0f)) / 2);
            layoutParams.width = df.a(this.f3009a, 52.0f);
        } else {
            layoutParams.leftMargin = (view.getLeft() + textView.getLeft()) - df.a(this.f3009a, 8.0f);
            layoutParams.width = textView.getWidth() + df.a(this.f3009a, 16.0f);
        }
        layoutParams.topMargin = df.a(this.f3009a, 6.0f);
        layoutParams.height = view.getHeight() - df.a(this.f3009a, 12.0f);
        this.e.setLayoutParams(layoutParams);
    }

    private Animation a(int i, int i2) {
        new TranslateAnimation(0.0f, (float) (i2 - i), 0.0f, 0.0f).setDuration(150);
        return null;
    }

    public void a(co coVar) {
        this.c = coVar;
    }

    public void b(int i) {
        if (i == 0) {
            this.f.setVisibility(8);
        } else {
            this.f.setVisibility(0);
        }
    }
}
