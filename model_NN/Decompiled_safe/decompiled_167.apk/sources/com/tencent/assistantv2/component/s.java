package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class s implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommonCategoryView f3223a;

    s(CommonCategoryView commonCategoryView) {
        this.f3223a = commonCategoryView;
    }

    public void onClick(View view) {
        int i;
        com.tencent.assistant.adapter.s sVar = (com.tencent.assistant.adapter.s) view.getTag(R.id.category_data);
        try {
            i = ((Integer) view.getTag(R.id.category_pos)).intValue();
        } catch (Exception e) {
            e.printStackTrace();
            i = 0;
        }
        this.f3223a.a(sVar, i, 200);
        if (this.f3223a.b != null) {
            this.f3223a.b.onClick(view);
        }
    }
}
