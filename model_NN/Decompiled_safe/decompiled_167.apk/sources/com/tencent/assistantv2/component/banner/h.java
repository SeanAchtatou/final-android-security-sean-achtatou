package com.tencent.assistantv2.component.banner;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.Banner;

/* compiled from: ProGuard */
public class h extends f {
    TXImageView e = null;
    TextView f = null;

    public h(Banner banner) {
        super(banner);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(Context context, ViewGroup viewGroup, int i, long j, int i2) {
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.banner_one_node_view, viewGroup, false);
        a(context, inflate);
        this.e = (TXImageView) inflate.findViewById(R.id.title_img);
        this.f = (TextView) inflate.findViewById(R.id.title);
        this.f.setTextColor(d());
        this.e.updateImageView(this.f3103a.c, 0, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        this.f.setText(this.f3103a.b);
        inflate.setOnClickListener(new i(this, context, i2));
        return inflate;
    }

    public void a(Context context) {
        int i = 0;
        if (context != null && this.c != null && !this.c.isRecycled()) {
            this.d.setBackgroundDrawable(new BitmapDrawable(context.getResources(), this.c));
            if (this.e != null) {
                this.e.setVisibility((this.f3103a.h & 2) == 0 ? 0 : 8);
            }
            if (this.f != null) {
                if ((this.f3103a.h & 1) != 0) {
                    i = 8;
                }
                this.f.setVisibility(i);
            }
        }
    }

    public int a() {
        return 1;
    }
}
