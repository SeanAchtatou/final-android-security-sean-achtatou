package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistantv2.manager.i;

/* compiled from: ProGuard */
public class RankHeaderLayout extends TXLoadingLayoutBase {

    /* renamed from: a  reason: collision with root package name */
    LinearLayout f3008a;
    View b;
    private Context c;
    private SwitchButton d;
    private TextView e;
    private boolean f = true;

    public RankHeaderLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(TXScrollViewBase.ScrollMode.PULL_FROM_END);
    }

    public RankHeaderLayout(Context context, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, scrollDirection, scrollMode);
        this.c = context;
        a(scrollMode);
    }

    private void a(TXScrollViewBase.ScrollMode scrollMode) {
        LayoutInflater.from(this.c).inflate((int) R.layout.v5_hide_installed_apps_area, this);
        this.f3008a = (LinearLayout) findViewById(R.id.header_layout);
        this.b = findViewById(R.id.header_container);
        this.d = (SwitchButton) findViewById(R.id.hide_btn);
        this.e = (TextView) findViewById(R.id.text);
        this.d.setSwitchState(i.a().b().c());
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.f3008a.getLayoutParams();
        if (scrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            layoutParams.gravity = 80;
        } else {
            layoutParams.gravity = 48;
        }
        reset();
    }

    public void a(boolean z) {
        this.d.setSwitchState(z);
    }

    public void a(int i) {
        this.b.setPadding(this.b.getPaddingLeft(), this.b.getPaddingTop(), this.b.getPaddingRight(), this.b.getPaddingBottom() + i);
    }

    public void reset() {
        if (this.mScrollMode == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            a();
        } else {
            this.f3008a.setVisibility(0);
        }
    }

    public void pullToRefresh() {
        a();
    }

    public void releaseToRefresh() {
        this.f3008a.setVisibility(0);
    }

    public void refreshing() {
        a();
    }

    public void loadFinish(String str) {
        this.f3008a.setVisibility(0);
    }

    public void refreshSuc() {
        this.f3008a.setVisibility(0);
    }

    public void refreshFail(String str) {
        this.f3008a.setVisibility(0);
    }

    public void onPull(int i) {
        a();
    }

    public void hideAllSubViews() {
        if (getVisibility() == 0) {
        }
    }

    public void showAllSubViews() {
        if (this.f3008a.getVisibility() != 0) {
            this.f3008a.setVisibility(0);
        } else {
            this.f3008a.setVisibility(0);
        }
    }

    public void setWidth(int i) {
        getLayoutParams().width = i;
        requestLayout();
    }

    public void setHeight(int i) {
        getLayoutParams().height = i;
        requestLayout();
    }

    public int getContentSize() {
        return this.f3008a.getHeight();
    }

    public int getTriggerSize() {
        return getResources().getDimensionPixelSize(R.dimen.tx_pull_to_refresh_trigger_size);
    }

    private void a() {
        if (!this.f || !i.a().b().c()) {
            this.f3008a.setVisibility(0);
        } else {
            this.f3008a.setVisibility(0);
        }
    }

    public void b(boolean z) {
        this.f = z;
    }

    public void loadSuc() {
    }

    public void loadFail() {
    }
}
