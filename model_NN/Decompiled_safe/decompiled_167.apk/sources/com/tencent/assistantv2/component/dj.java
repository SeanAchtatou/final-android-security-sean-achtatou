package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistantv2.activity.MainActivity;

/* compiled from: ProGuard */
class dj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BaseActivity f3176a;
    final /* synthetic */ int b;
    final /* synthetic */ TopTabWidget c;

    dj(TopTabWidget topTabWidget, BaseActivity baseActivity, int i) {
        this.c = topTabWidget;
        this.f3176a = baseActivity;
        this.b = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.activity.MainActivity.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, int):int
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.db.table.z):com.tencent.assistant.db.table.z
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, java.util.ArrayList):java.util.ArrayList
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.GetPhoneUserAppListResponse, boolean):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, android.content.Intent):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, com.tencent.assistant.protocol.jce.DesktopShortCut):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistantv2.activity.MainActivity, boolean):void
      com.tencent.assistantv2.activity.MainActivity.a(com.tencent.assistant.protocol.jce.DesktopShortCut, com.tencent.assistant.protocol.jce.DesktopShortCut):boolean
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistantv2.activity.MainActivity.a(int, boolean):void */
    public void onClick(View view) {
        if (this.f3176a instanceof MainActivity) {
            ((MainActivity) this.f3176a).a(this.b, true);
        }
    }
}
