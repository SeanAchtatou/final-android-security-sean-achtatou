package com.tencent.assistantv2.component.categorydetail;

import android.view.View;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
class i implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameCategoryDetailListView f3147a;

    i(GameCategoryDetailListView gameCategoryDetailListView) {
        this.f3147a = gameCategoryDetailListView;
    }

    public void a() {
        int f;
        int a2 = df.a(this.f3147a.getContext(), 4.0f);
        int i = 0;
        for (int i2 = 0; i2 < this.f3147a.m; i2++) {
            View childAt = this.f3147a.getListView().getChildAt(i2);
            if (childAt != null) {
                i += childAt.getHeight();
            }
        }
        if (this.f3147a.m <= 1) {
            f = a2 + 0;
        } else {
            f = this.f3147a.n - i;
        }
        if (this.f3147a.e != null) {
            this.f3147a.e.d();
        }
        this.f3147a.getListView().setSelectionFromTop(this.f3147a.m, f);
        this.f3147a.getListView().postInvalidate();
    }

    public void b() {
    }

    public void a(int i) {
    }
}
