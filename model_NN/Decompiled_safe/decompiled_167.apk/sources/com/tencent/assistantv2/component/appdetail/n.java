package com.tencent.assistantv2.component.appdetail;

import com.tencent.assistant.model.c;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3078a;
    final /* synthetic */ c b;
    final /* synthetic */ m c;

    n(m mVar, int i, c cVar) {
        this.c = mVar;
        this.f3078a = i;
        this.b = cVar;
    }

    public void run() {
        int i = -1;
        for (int i2 = 0; i2 < this.c.f3077a.m.size(); i2++) {
            if (this.c.f3077a.k[i2].getTag() != null && this.c.f3077a.k[i2].getTag().toString().equals(Constants.STR_EMPTY + this.f3078a)) {
                this.c.f3077a.k[i2].a(this.b.a());
                this.c.f3077a.m.set(i2, this.b.a());
                this.c.f3077a.l[i2].a(this.b.a(), this.c.f3077a.j[i2]);
                this.c.f3077a.l[i2].b(this.b.a());
                this.c.f3077a.k[i2].c();
                i = i2;
            }
        }
        if (i >= 0) {
            this.c.f3077a.a(this.b.a(), null, i);
        }
    }
}
