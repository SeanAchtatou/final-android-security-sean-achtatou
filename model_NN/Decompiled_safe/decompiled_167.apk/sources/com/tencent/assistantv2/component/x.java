package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.model.SimpleAppModel;

/* compiled from: ProGuard */
public interface x {
    int a();

    void a(View view);

    void a(View view, AppConst.AppState appState);

    void a(DownloadInfo downloadInfo);

    void a(SimpleAppModel simpleAppModel, View view);
}
