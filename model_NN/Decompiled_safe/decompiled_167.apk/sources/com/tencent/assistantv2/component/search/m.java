package com.tencent.assistantv2.component.search;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.SearchWebCfg;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.activity.SearchActivity;
import com.tencent.assistantv2.component.TabBarView;
import com.tencent.assistantv2.component.search.ISearchResultPage;
import com.tencent.assistantv2.component.search.SearchResultTabPagesBase;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class m extends SearchResultTabPagesBase {
    private final String f = "SearchResultTabPages";
    private SearchResultTabPagesBase.TabType g = SearchResultTabPagesBase.TabType.b;
    private String h;
    private int i = STConst.ST_PAGE_SEARCH;

    public m(Context context, TabBarView tabBarView, ViewPager viewPager) {
        super(context, tabBarView, viewPager);
    }

    private void l() {
        if (this.f3230a != null && this.c != null) {
            if (this.b == null || this.b.size() <= 0) {
                if (this.b == null) {
                    this.b = new ArrayList();
                }
                if (this.b.size() <= 0) {
                    this.b.clear();
                    this.b.add(new k());
                    this.b.add(new k());
                    this.b.add(new k());
                    this.b.add(new k());
                }
                ((k) this.b.get(0)).a(k(), a(SearchResultTabPagesBase.TabType.b));
                ((k) this.b.get(1)).a(k(), a(SearchResultTabPagesBase.TabType.APP));
                ((k) this.b.get(2)).a(k(), a(SearchResultTabPagesBase.TabType.VIDEO));
                ((k) this.b.get(3)).a(k(), a(SearchResultTabPagesBase.TabType.EBOOK));
            }
        }
    }

    /* access modifiers changed from: protected */
    public int[] a() {
        return new int[]{R.string.search_result_tab_all, R.string.search_result_tab_app, R.string.search_result_tab_video, R.string.search_result_tab_ebook};
    }

    /* access modifiers changed from: protected */
    public void a(int i2) {
        f(i2);
    }

    private void f(int i2) {
        if (b() != c(i2) && (k() instanceof SearchActivity)) {
            ((SearchActivity) k()).i();
        }
        l();
        if (i2 >= 0 && this.b != null && i2 < this.b.size()) {
            b(i2, false);
            switch (i2) {
                case 0:
                    this.g = SearchResultTabPagesBase.TabType.b;
                    break;
                case 1:
                    this.g = SearchResultTabPagesBase.TabType.APP;
                    break;
                case 2:
                    this.g = SearchResultTabPagesBase.TabType.VIDEO;
                    break;
                case 3:
                    this.g = SearchResultTabPagesBase.TabType.EBOOK;
                    break;
            }
            ((k) this.b.get(i2)).a().a(this.h, this.i);
        }
    }

    public void b(int i2) {
        switch (i2) {
            case 0:
                f(0);
                return;
            case 1:
                f(1);
                return;
            case 2:
                f(2);
                return;
            case 3:
                f(3);
                return;
            default:
                return;
        }
    }

    public void a(String str, int i2, SimpleAppModel simpleAppModel) {
        a(e(), str, i2, simpleAppModel);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistantv2.component.search.SearchResultTabPagesBase.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistantv2.component.search.m.a(java.lang.String, int):void
      com.tencent.assistantv2.component.search.SearchResultTabPagesBase.a(int, boolean):void */
    public void a(SearchResultTabPagesBase.TabType tabType, String str, int i2, SimpleAppModel simpleAppModel) {
        l();
        c();
        this.g = tabType;
        this.h = str;
        this.i = i2;
        if (this.b != null) {
            switch (n.f3243a[tabType.ordinal()]) {
                case 1:
                    a(0, false);
                    if (this.b.size() > 0) {
                        ((k) this.b.get(0)).a().a(str, this.i, simpleAppModel);
                        return;
                    }
                    return;
                case 2:
                    a(1, false);
                    if (this.b.size() > 1) {
                        ((k) this.b.get(1)).a().a(str, this.i, simpleAppModel);
                        return;
                    }
                    return;
                case 3:
                    a(2, false);
                    if (this.b.size() > 2) {
                        ((k) this.b.get(2)).a().a(str, this.i, simpleAppModel);
                        return;
                    }
                    return;
                case 4:
                    a(3, false);
                    if (this.b.size() > 3) {
                        ((k) this.b.get(3)).a().a(str, this.i, simpleAppModel);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void a(SearchResultTabPagesBase.TabType tabType, String str, int i2) {
        this.g = tabType;
        this.h = str;
        this.i = i2;
        f(f());
    }

    public void a(String str, int i2) {
        a(this.g, str, i2);
    }

    public int b() {
        switch (n.f3243a[this.g.ordinal()]) {
            case 1:
            default:
                return STConst.ST_PAGE_SEARCH_RESULT_ALL;
            case 2:
                return STConst.ST_PAGE_SEARCH_RESULT_APP;
            case 3:
                return STConst.ST_PAGE_SEARCH_RESULT_VIDEO;
            case 4:
                return STConst.ST_PAGE_SEARCH_RESULT_EBOOK;
        }
    }

    public int c(int i2) {
        switch (i2) {
            case 0:
            default:
                return STConst.ST_PAGE_SEARCH_RESULT_ALL;
            case 1:
                return STConst.ST_PAGE_SEARCH_RESULT_APP;
            case 2:
                return STConst.ST_PAGE_SEARCH_RESULT_VIDEO;
            case 3:
                return STConst.ST_PAGE_SEARCH_RESULT_EBOOK;
        }
    }

    public void c() {
        if (this.b != null) {
            for (k kVar : this.b) {
                if (!(kVar == null || kVar.a() == null)) {
                    kVar.a().d();
                }
            }
        }
    }

    public void d() {
        if (this.b != null) {
            for (k kVar : this.b) {
                if (!(kVar == null || kVar.a() == null)) {
                    kVar.a().e();
                }
            }
        }
    }

    public void a(String str) {
        if (this.b != null) {
            for (k kVar : this.b) {
                if (!(kVar == null || kVar.a() == null)) {
                    kVar.a().a(str);
                }
            }
        }
    }

    public void b(String str) {
        if (this.b != null) {
            for (k kVar : this.b) {
                if (!(kVar == null || kVar.a() == null)) {
                    kVar.a().b(str);
                }
            }
        }
    }

    public SearchResultTabPagesBase.TabType e() {
        return this.g;
    }

    public int f() {
        switch (n.f3243a[this.g.ordinal()]) {
            case 1:
            default:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
        }
    }

    public void d(int i2) {
        switch (i2) {
            case 0:
                this.g = SearchResultTabPagesBase.TabType.b;
                return;
            case 1:
                this.g = SearchResultTabPagesBase.TabType.APP;
                return;
            case 2:
                this.g = SearchResultTabPagesBase.TabType.VIDEO;
                return;
            case 3:
                this.g = SearchResultTabPagesBase.TabType.EBOOK;
                return;
            default:
                return;
        }
    }

    private j a(SearchResultTabPagesBase.TabType tabType) {
        int i2;
        j jVar = new j();
        jVar.a(ISearchResultPage.PageType.NATIVE);
        switch (n.f3243a[tabType.ordinal()]) {
            case 1:
                jVar.a(4);
                jVar.b(STConst.ST_PAGE_SEARCH_RESULT_ALL);
                i2 = 0;
                break;
            case 2:
                jVar.a(5);
                jVar.b(STConst.ST_PAGE_SEARCH_RESULT_APP);
                i2 = 1;
                break;
            case 3:
                jVar.a(6);
                jVar.b(STConst.ST_PAGE_SEARCH_RESULT_VIDEO);
                i2 = 2;
                break;
            case 4:
                jVar.a(7);
                jVar.b(STConst.ST_PAGE_SEARCH_RESULT_EBOOK);
                i2 = 3;
                break;
            default:
                jVar.a(4);
                jVar.b(STConst.ST_PAGE_SEARCH_RESULT_ALL);
                i2 = 0;
                break;
        }
        SearchWebCfg b = t.a().b();
        if (b != null) {
            if (b.b != null && b.b.size() > i2 && !TextUtils.isEmpty(b.b.get(i2))) {
                int i3 = b.f2298a & 31;
                if ((i3 & 1) == 0) {
                    switch (c.c()) {
                        case 1:
                            if ((i3 & 2) != 0) {
                                jVar.a(ISearchResultPage.PageType.WEB);
                                jVar.a(b.b.get(i2));
                                break;
                            }
                            break;
                        case 2:
                            if ((i3 & 4) != 0) {
                                jVar.a(ISearchResultPage.PageType.WEB);
                                jVar.a(b.b.get(i2));
                                break;
                            }
                            break;
                        case 3:
                            if ((i3 & 16) != 0) {
                                jVar.a(ISearchResultPage.PageType.WEB);
                                jVar.a(b.b.get(i2));
                                break;
                            }
                            break;
                        case 4:
                        default:
                            jVar.a(ISearchResultPage.PageType.NONE);
                            break;
                        case 5:
                            if ((i3 & 8) != 0) {
                                jVar.a(ISearchResultPage.PageType.WEB);
                                jVar.a(b.b.get(i2));
                                break;
                            }
                            break;
                    }
                } else {
                    jVar.a(ISearchResultPage.PageType.WEB);
                    jVar.a(b.b.get(i2));
                }
            } else {
                return jVar;
            }
        }
        return jVar;
    }
}
