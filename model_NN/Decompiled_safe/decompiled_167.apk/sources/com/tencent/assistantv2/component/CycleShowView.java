package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

/* compiled from: ProGuard */
public abstract class CycleShowView extends TextView {
    public CycleShowView(Context context) {
        this(context, null);
    }

    public CycleShowView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void a(Context context, AttributeSet attributeSet) {
    }

    public void a() {
    }

    public View b() {
        return this;
    }
}
