package com.tencent.assistantv2.component;

import android.view.View;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.widget.RelativeLayout;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class du extends ScaleAnimation {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxManagerCommContainView f3186a;
    private long b = 0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public du(TxManagerCommContainView txManagerCommContainView, float f, float f2, float f3, float f4, int i, float f5, int i2, float f6) {
        super(f, f2, f3, f4, i, f5, i2, f6);
        this.f3186a = txManagerCommContainView;
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f, Transformation transformation) {
        this.b++;
        float c = TxManagerCommContainView.k * f;
        this.f3186a.a(f);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.f3186a.h.getLayoutParams();
        layoutParams.height = (int) ((1.0f - c) * ((float) (this.f3186a.getMeasuredHeight() - this.f3186a.j.getMeasuredHeight())));
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.f3186a.i.getLayoutParams();
        layoutParams2.height = (this.f3186a.getMeasuredHeight() - this.f3186a.j.getMeasuredHeight()) - layoutParams.height;
        float measuredHeight = (float) this.f3186a.h.getMeasuredHeight();
        View g = this.f3186a.x;
        XLog.d("Donaldxuuu", "topHeight = " + measuredHeight + " marginTop = ");
        this.f3186a.h.setLayoutParams(layoutParams);
        this.f3186a.i.setLayoutParams(layoutParams2);
        if ((measuredHeight - ((float) df.a(this.f3186a.getContext(), 144.0f))) / 2.0f >= ((float) df.a(this.f3186a.getContext(), 140.0f))) {
            return;
        }
        if (this.f3186a.y) {
            RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) g.getLayoutParams();
            layoutParams3.addRule(13);
            layoutParams3.width = df.a(this.f3186a.getContext(), 144.0f);
            layoutParams3.height = df.a(this.f3186a.getContext(), 144.0f);
            layoutParams3.topMargin = 0;
            layoutParams3.bottomMargin = 0;
            g.setLayoutParams(layoutParams3);
            boolean unused = this.f3186a.y = false;
            super.applyTransformation(f, transformation);
            return;
        }
        super.applyTransformation(f, transformation);
    }
}
