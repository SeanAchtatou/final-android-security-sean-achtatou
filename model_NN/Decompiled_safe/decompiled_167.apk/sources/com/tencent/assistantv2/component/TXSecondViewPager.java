package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

/* compiled from: ProGuard */
public class TXSecondViewPager extends TXViewPager {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3014a = true;

    public TXSecondViewPager(Context context) {
        super(context);
    }

    public TXSecondViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void a(boolean z) {
        this.f3014a = false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.f3014a) {
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }
}
