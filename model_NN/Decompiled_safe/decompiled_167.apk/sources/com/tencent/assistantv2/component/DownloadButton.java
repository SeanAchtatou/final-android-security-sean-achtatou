package com.tencent.assistantv2.component;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Pair;
import android.widget.Button;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.link.b;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.manager.f;
import com.tencent.assistant.manager.g;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.u;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.cp;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.model.c;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class DownloadButton extends Button implements g {

    /* renamed from: a  reason: collision with root package name */
    public boolean f2987a;
    private Context b;
    private c c;
    private DownloadInfo d;
    private boolean e;
    private boolean f;
    private z g;

    public DownloadButton(Context context) {
        this(context, null);
    }

    public DownloadButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2987a = false;
        this.b = context;
        c();
    }

    private void c() {
        setHeight(this.b.getResources().getDimensionPixelSize(R.dimen.download_button_height));
        setMinWidth(this.b.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        setGravity(17);
        setTextSize(0, (float) this.b.getResources().getDimensionPixelSize(R.dimen.download_button_font_size));
        setSingleLine(true);
    }

    public void a(c cVar) {
        if (cVar != null) {
            this.c = cVar;
            d b2 = b(cVar);
            a(b2.c);
            f.a().a(b2.f1661a, this);
        }
    }

    private d b(c cVar) {
        if (cVar == null || !(cVar instanceof SimpleAppModel)) {
            return null;
        }
        return u.e((SimpleAppModel) cVar);
    }

    public void a(c cVar, d dVar) {
        if (cVar != null) {
            if (dVar == null) {
                dVar = b(cVar);
            }
            this.c = cVar;
            a(dVar.c);
            f.a().a(dVar.f1661a, this);
        }
    }

    public void a(STCommonInfo sTCommonInfo) {
        b(sTCommonInfo, null, null, this);
    }

    public void a(STCommonInfo sTCommonInfo, x xVar) {
        b(sTCommonInfo, xVar, null, this);
    }

    public void a(STCommonInfo sTCommonInfo, x xVar, d dVar) {
        b(sTCommonInfo, xVar, dVar, this);
    }

    public void a(STCommonInfo sTCommonInfo, x xVar, d dVar, g... gVarArr) {
        b(sTCommonInfo, xVar, dVar, gVarArr);
    }

    private void b(STCommonInfo sTCommonInfo, x xVar, d dVar, g... gVarArr) {
        if (this.c != null) {
            if (dVar != null) {
                AppConst.AppState appState = dVar.c;
            }
            SimpleAppModel simpleAppModel = (SimpleAppModel) this.c;
            f.a().a(dVar == null ? this.c.q() : dVar.f1661a, this);
            setOnClickListener(new u(this, simpleAppModel, sTCommonInfo, xVar, gVarArr));
        }
    }

    public void onAppStateChange(String str, AppConst.AppState appState) {
        String d2;
        if (!TextUtils.isEmpty(str) && (d2 = d()) != null && d2.equals(str)) {
            if (appState == AppConst.AppState.ILLEGAL) {
                d c2 = u.c(DownloadProxy.a().d(str));
                if (c2 == null) {
                    c2 = b(this.c);
                }
                if (c2 != null) {
                    appState = c2.c;
                }
            }
            ba.a().post(new v(this, str, appState));
        }
    }

    private void a(AppConst.AppState appState) {
        b(appState);
    }

    /* access modifiers changed from: private */
    public void b(AppConst.AppState appState) {
        boolean z = true;
        boolean z2 = false;
        z c2 = c(appState);
        String a2 = c2.a(this.b);
        if (!TextUtils.isEmpty(a2) && (this.g == null || !a2.equals(this.g.f3267a))) {
            a(a2);
            z2 = true;
        }
        if (!this.f2987a) {
            setTextColor(this.b.getResources().getColor(c2.d));
            if (this.g == null || c2.e != this.g.e) {
                try {
                    setBackgroundResource(c2.e);
                } catch (Throwable th) {
                }
            }
        } else {
            z = z2;
        }
        if (z) {
            this.g = c2;
        }
    }

    /* access modifiers changed from: private */
    public String d() {
        if (this.c != null) {
            return this.c.q();
        }
        if (this.d != null) {
            return this.d.downloadTicket;
        }
        return null;
    }

    private z c(AppConst.AppState appState) {
        z zVar = new z(null);
        zVar.e = R.drawable.state_bg_common_selector;
        zVar.d = R.color.state_normal;
        zVar.b = R.string.appbutton_unknown;
        if (this.c instanceof SimpleAppModel) {
            SimpleAppModel simpleAppModel = (SimpleAppModel) this.c;
            if (s.b(simpleAppModel, appState)) {
                zVar.b = R.string.jionbeta;
                zVar.d = R.color.state_install;
                zVar.e = R.drawable.state_bg_install_selector;
                return zVar;
            } else if (s.c(simpleAppModel, appState)) {
                zVar.b = R.string.jionfirstrelease_with_login;
                zVar.d = R.color.state_install;
                zVar.e = R.drawable.state_bg_install_selector;
                return zVar;
            }
        }
        switch (w.f3266a[appState.ordinal()]) {
            case 1:
                if (this.c != null && (this.c instanceof SimpleAppModel)) {
                    SimpleAppModel simpleAppModel2 = (SimpleAppModel) this.c;
                    if (!simpleAppModel2.c()) {
                        if (!simpleAppModel2.h()) {
                            if (!cp.a().a(simpleAppModel2.f1634a)) {
                                zVar.b = R.string.appbutton_download;
                                break;
                            } else {
                                zVar.a(cp.a().b(simpleAppModel2.f1634a));
                                zVar.d = cp.a().c(simpleAppModel2.f1634a);
                                zVar.e = cp.a().a(zVar.d);
                                break;
                            }
                        } else {
                            zVar.b = R.string.jionbeta;
                            zVar.d = R.color.state_install;
                            zVar.e = R.drawable.state_bg_install_selector;
                            break;
                        }
                    } else {
                        zVar.b = R.string.appbutton_download;
                        zVar.d = R.color.state_normal;
                        zVar.e = R.drawable.state_bg_common_selector;
                        break;
                    }
                }
            case 2:
                zVar.b = R.string.appbutton_update;
                zVar.d = R.color.state_update;
                zVar.e = R.drawable.state_bg_update_selector;
                break;
            case 3:
                zVar.b = R.string.downloading_display_pause;
                break;
            case 4:
                zVar.b = R.string.queuing;
                break;
            case 5:
            case 6:
                zVar.b = R.string.appbutton_continuing;
                zVar.d = R.color.state_install;
                zVar.e = R.drawable.state_bg_install_selector;
                break;
            case 7:
                zVar.b = R.string.appbutton_install;
                zVar.d = R.color.state_install;
                zVar.e = R.drawable.state_bg_install_selector;
                break;
            case 8:
                String str = Constants.STR_EMPTY;
                String str2 = Constants.STR_EMPTY;
                if (this.c != null && (this.c instanceof SimpleAppModel)) {
                    String str3 = ((SimpleAppModel) this.c).aD;
                    str = ((SimpleAppModel) this.c).c;
                    str2 = str3;
                } else if (this.d != null) {
                    str = this.d.packageName;
                }
                if (!TextUtils.isEmpty(str2) && b(str2)) {
                    zVar.b = R.string.appbutton_open;
                    zVar.d = R.color.state_open;
                    zVar.e = R.drawable.state_bg_open_selector;
                    Pair<Integer, Integer> f2 = f();
                    if (f2 != null) {
                        zVar.e = ((Integer) f2.first).intValue();
                        zVar.d = ((Integer) f2.second).intValue();
                    }
                    zVar.a(e());
                    break;
                } else {
                    if (be.a().c(str)) {
                        be.a();
                        if (!TextUtils.isEmpty(be.h())) {
                            be.a();
                            if (be.h().equals(str)) {
                                zVar.b = R.string.qube_apk_using;
                            }
                        }
                        zVar.b = R.string.qube_apk_use;
                    } else {
                        zVar.b = R.string.appbutton_open;
                    }
                    zVar.e = R.drawable.state_bg_open_selector;
                    zVar.d = R.color.state_open;
                    break;
                }
                break;
            case 9:
                zVar.b = R.string.not_support;
                break;
            case 10:
                break;
            case 11:
                zVar.b = R.string.installing;
                zVar.e = R.drawable.state_bg_disable_bd_pressed;
                zVar.d = R.color.state_disable;
                break;
            case 12:
                zVar.b = R.string.uninstalling;
                break;
            default:
                zVar.b = R.string.appbutton_unknown;
                break;
        }
        return zVar;
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        if (str.length() >= 4) {
            setMinWidth(this.b.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_4));
        } else {
            setMinWidth(this.b.getResources().getDimensionPixelSize(R.dimen.download_button_minwidth_2));
        }
        setText(str);
    }

    public void a(boolean z) {
        this.f = z;
    }

    public void b(boolean z) {
        this.e = z;
    }

    public void a(DownloadInfo downloadInfo) {
        this.d = downloadInfo;
        d dVar = new d();
        dVar.f1661a = downloadInfo.downloadTicket;
        dVar.b = downloadInfo;
        dVar.c = u.a(this.d, this.f, this.e);
        a(dVar.c);
        f.a().a(dVar.f1661a, this);
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, STCommonInfo sTCommonInfo, x xVar, g... gVarArr) {
        if (simpleAppModel != null) {
            AppConst.AppState d2 = u.d(simpleAppModel);
            if (s.a(simpleAppModel, d2)) {
                Intent intent = new Intent(getContext(), AppDetailActivityV5.class);
                intent.putExtra(CommentDetailTabView.PARAMS_SIMPLE_MODEL_INFO, simpleAppModel);
                getContext().startActivity(intent);
                return;
            }
            DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
            StatInfo a3 = a.a(sTCommonInfo);
            if (a2 != null && a2.needReCreateInfo(simpleAppModel)) {
                DownloadProxy.a().b(a2.downloadTicket);
                a2 = null;
            }
            if (a2 == null) {
                a2 = DownloadInfo.createDownloadInfo(simpleAppModel, a3, gVarArr);
                if (xVar != null) {
                    a2.isUpdate = xVar.a();
                }
            } else {
                a2.updateDownloadInfoStatInfo(a3);
            }
            a2.appLinkActionUrl = simpleAppModel.aD;
            if (xVar != null) {
                xVar.a(this);
            }
            switch (w.f3266a[d2.ordinal()]) {
                case 1:
                case 2:
                    if (xVar != null) {
                        xVar.a(a2);
                        xVar.a(simpleAppModel, this);
                        break;
                    } else {
                        com.tencent.assistant.download.a.a().a(a2);
                        break;
                    }
                case 3:
                case 4:
                    com.tencent.assistant.download.a.a().b(a2.downloadTicket);
                    break;
                case 5:
                    com.tencent.assistant.download.a.a().b(a2);
                    if (xVar != null) {
                        xVar.a(simpleAppModel, this);
                        break;
                    }
                    break;
                case 6:
                case 10:
                    com.tencent.assistant.download.a.a().a(a2);
                    if (xVar != null) {
                        xVar.a(simpleAppModel, this);
                        break;
                    }
                    break;
                case 7:
                    com.tencent.assistant.download.a.a().d(a2);
                    break;
                case 8:
                    com.tencent.assistant.download.a.a().c(a2);
                    break;
                case 9:
                    Toast.makeText(this.b, (int) R.string.unsupported, 0).show();
                    break;
                case 11:
                    Toast.makeText(this.b, (int) R.string.tips_slicent_install, 0).show();
                    break;
                case 12:
                    Toast.makeText(this.b, (int) R.string.tips_slicent_uninstall, 0).show();
                    break;
            }
            if (xVar != null) {
                xVar.a(this, d2);
            }
        }
    }

    public void a() {
        setTextSize(11.0f);
        this.f2987a = true;
        setBackgroundResource(R.drawable.guanjia_style_btn_backround_selector);
        setTextColor(-1);
    }

    public void b() {
        this.f2987a = false;
        this.g = null;
        a(this.c);
    }

    private boolean b(String str) {
        if (!TextUtils.isEmpty(str)) {
            if (b.a(AstApp.i(), new Intent("android.intent.action.VIEW", Uri.parse(str)))) {
                return true;
            }
        }
        return false;
    }

    private String e() {
        if (this.c == null || !(this.c instanceof SimpleAppModel)) {
            return Constants.STR_EMPTY;
        }
        return ((SimpleAppModel) this.c).aE.b;
    }

    private Pair<Integer, Integer> f() {
        if (this.c != null && (this.c instanceof SimpleAppModel)) {
            int i = ((SimpleAppModel) this.c).aE.c;
            if (i == 1) {
                return Pair.create(Integer.valueOf((int) R.drawable.state_bg_common_selector), Integer.valueOf((int) R.color.state_normal));
            }
            if (i == 2) {
                return Pair.create(Integer.valueOf((int) R.drawable.state_bg_install_selector), Integer.valueOf((int) R.color.state_install));
            }
        }
        return null;
    }
}
