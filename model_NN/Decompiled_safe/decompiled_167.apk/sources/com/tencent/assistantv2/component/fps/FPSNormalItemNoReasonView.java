package com.tencent.assistantv2.component.fps;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.a.c;
import com.tencent.assistant.component.HorizonMultiImageView;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistantv2.adapter.smartlist.v;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.ListItemRelateNewsView;

/* compiled from: ProGuard */
public class FPSNormalItemNoReasonView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    public v f3210a = new v();
    private Context b;
    private IViewInvalidater c;

    public FPSNormalItemNoReasonView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.b = context;
        a();
    }

    public FPSNormalItemNoReasonView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        a();
    }

    public FPSNormalItemNoReasonView(Context context) {
        super(context);
        this.b = context;
        a();
    }

    public FPSNormalItemNoReasonView(Context context, IViewInvalidater iViewInvalidater) {
        super(context);
        this.b = context;
        this.c = iViewInvalidater;
        a();
    }

    private void a() {
        View inflate = LayoutInflater.from(this.b).inflate((int) R.layout.app_universal_item_no_reason, this);
        this.f3210a.j = (TextView) inflate.findViewById(R.id.download_rate_desc);
        this.f3210a.k = (TXAppIconView) inflate.findViewById(R.id.app_icon_img);
        this.f3210a.k.setInvalidater(this.c);
        this.f3210a.l = (TextView) inflate.findViewById(R.id.title);
        this.f3210a.m = (DownloadButton) inflate.findViewById(R.id.state_app_btn);
        this.f3210a.n = (ListItemInfoView) inflate.findViewById(R.id.download_info);
        this.f3210a.q = (HorizonMultiImageView) inflate.findViewById(R.id.snap_shot_pics);
        this.f3210a.r = inflate.findViewById(R.id.empty_padding_bottom);
        this.f3210a.v = (ListItemRelateNewsView) inflate.findViewById(R.id.relate_news);
        this.f3210a.s = new c();
        this.f3210a.s.f686a = (ListView) inflate.findViewById(R.id.one_more_list);
        this.f3210a.s.b = (RelativeLayout) inflate.findViewById(R.id.one_more_list_parent);
        this.f3210a.s.f686a.setDivider(null);
        this.f3210a.s.e = (ImageView) inflate.findViewById(R.id.one_more_app_line_top_long);
        this.f3210a.s.j = (RelativeLayout) inflate.findViewById(R.id.app_one_more_loading_parent);
        this.f3210a.s.i = (TextView) inflate.findViewById(R.id.app_one_more_loading);
        this.f3210a.s.f = (ProgressBar) inflate.findViewById(R.id.app_one_more_loading_gif);
        this.f3210a.s.g = (ImageView) inflate.findViewById(R.id.one_more_app_error_img);
        this.f3210a.s.d = (ImageView) inflate.findViewById(R.id.one_more_app_arrow);
        this.f3210a.s.h = (TextView) inflate.findViewById(R.id.app_one_more_desc);
        this.f3210a.u = (TextView) inflate.findViewById(R.id.sort_text);
        setBackgroundResource(R.drawable.bg_card_selector_padding);
    }
}
