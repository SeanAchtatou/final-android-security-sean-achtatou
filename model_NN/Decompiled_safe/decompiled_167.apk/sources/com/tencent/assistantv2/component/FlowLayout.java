package com.tencent.assistantv2.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.android.qqdownloader.R;
import com.tencent.android.qqdownloader.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class FlowLayout extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2992a = false;
    private int b = 2;
    private int c = 3;
    private al d;
    private List<List<View>> e = new ArrayList();
    private List<Integer> f = new ArrayList();
    private List<List<View>> g = new ArrayList();
    private boolean h = true;

    public FlowLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, b.g);
        if (obtainStyledAttributes != null) {
            this.b = obtainStyledAttributes.getInt(0, 2);
            this.c = obtainStyledAttributes.getInt(1, 3);
        }
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new ViewGroup.MarginLayoutParams(layoutParams);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-1, -1);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        super.onMeasure(i, i2);
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int i8 = 0;
        int i9 = 0;
        int i10 = 0;
        int i11 = 0;
        int i12 = 0;
        int childCount = getChildCount();
        int i13 = 0;
        while (i13 < childCount) {
            View childAt = getChildAt(i13);
            measureChild(childAt, i, i2);
            if (i12 > this.c - 1) {
                i6 = i12;
                i7 = i11;
                i3 = i10;
                i4 = i9;
                i5 = i8;
            } else if (this.f2992a || i12 <= this.b - 1) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin;
                int measuredHeight = childAt.getMeasuredHeight() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin;
                if (i10 + measuredWidth > (size - getPaddingLeft()) - getPaddingRight()) {
                    int max = Math.max(i8, i10);
                    i4 = i9 + i11;
                    i6 = i12 + 1;
                    i7 = measuredHeight;
                    i3 = measuredWidth;
                    i5 = max;
                } else {
                    int max2 = Math.max(i11, measuredHeight);
                    i3 = i10 + measuredWidth;
                    i4 = i9;
                    i5 = i8;
                    int i14 = max2;
                    i6 = i12;
                    i7 = i14;
                }
                if (i13 == childCount - 1) {
                    i5 = Math.max(i5, i3);
                    i4 += i7;
                }
            } else {
                i6 = i12;
                i7 = i11;
                i3 = i10;
                i4 = i9;
                i5 = i8;
            }
            i13++;
            i8 = i5;
            i9 = i4;
            i10 = i3;
            i11 = i7;
            i12 = i6;
        }
        setMeasuredDimension(mode == 1073741824 ? size : getPaddingLeft() + i8 + getPaddingRight(), mode2 == 1073741824 ? size2 : getPaddingTop() + i9 + getPaddingBottom());
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        this.e.clear();
        this.f.clear();
        int width = getWidth();
        int i6 = 0;
        int i7 = 0;
        ArrayList arrayList = new ArrayList();
        int childCount = getChildCount();
        for (int i8 = 0; i8 < childCount; i8++) {
            View childAt = getChildAt(i8);
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) childAt.getLayoutParams();
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            if (marginLayoutParams.leftMargin + measuredWidth + marginLayoutParams.rightMargin + i6 > (width - getPaddingLeft()) - getPaddingRight()) {
                this.f.add(Integer.valueOf(i7));
                this.e.add(arrayList);
                i6 = 0;
                i7 = marginLayoutParams.bottomMargin + marginLayoutParams.topMargin + measuredHeight;
                arrayList = new ArrayList();
            }
            i6 += measuredWidth + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin;
            i7 = Math.max(i7, marginLayoutParams.bottomMargin + marginLayoutParams.topMargin + measuredHeight);
            arrayList.add(childAt);
        }
        this.f.add(Integer.valueOf(i7));
        this.e.add(arrayList);
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int size = this.e.size();
        int i9 = 0;
        int i10 = paddingTop;
        int i11 = paddingLeft;
        while (i9 < size) {
            List list = this.e.get(i9);
            int intValue = this.f.get(i9).intValue();
            int i12 = 0;
            while (true) {
                int i13 = i12;
                int i14 = i5;
                if (i13 >= list.size()) {
                    break;
                }
                View view = (View) list.get(i13);
                view.setTag(R.id.commenttag_lineinfo, Integer.valueOf(i9));
                if (this.f2992a) {
                    view.setVisibility(0);
                } else if (i9 >= this.b) {
                    view.setVisibility(8);
                } else {
                    view.setVisibility(0);
                }
                if (view.getVisibility() == 8) {
                    i5 = i14;
                } else {
                    ViewGroup.MarginLayoutParams marginLayoutParams2 = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
                    int i15 = marginLayoutParams2.leftMargin + i14;
                    int i16 = marginLayoutParams2.topMargin + i10;
                    view.layout(i15, i16, view.getMeasuredWidth() + i15, view.getMeasuredHeight() + i16);
                    i5 = i14 + view.getMeasuredWidth() + marginLayoutParams2.rightMargin + marginLayoutParams2.leftMargin;
                }
                i12 = i13 + 1;
            }
            i9++;
            i10 += intValue;
            i11 = getPaddingLeft();
        }
        if (this.h) {
            this.g.addAll(this.e);
            this.h = false;
        }
        if (this.d != null) {
            this.d.a(this.g.size());
        }
    }

    public void a(boolean z) {
        this.f2992a = z;
    }

    public void a(al alVar) {
        this.d = alVar;
    }

    public void a(int i) {
        int childCount = getChildCount();
        ArrayList<View> arrayList = new ArrayList<>();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (!(childAt == null || childAt.getTag(R.id.commenttag_lineinfo) == null || ((Integer) childAt.getTag(R.id.commenttag_lineinfo)).intValue() <= i - 1)) {
                arrayList.add(childAt);
            }
        }
        for (View removeView : arrayList) {
            removeView(removeView);
        }
        requestLayout();
    }
}
