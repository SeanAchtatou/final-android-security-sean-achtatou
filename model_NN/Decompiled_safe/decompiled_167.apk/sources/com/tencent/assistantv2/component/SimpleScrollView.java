package com.tencent.assistantv2.component;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.ScrollView;
import com.tencent.assistant.utils.XLog;
import java.lang.reflect.InvocationTargetException;

/* compiled from: ProGuard */
public class SimpleScrollView extends ScrollView {

    /* renamed from: a  reason: collision with root package name */
    private String f3013a = "Jie_SimpleScrollView";
    private float b;
    private float c;
    private float d;
    private float e;
    private float f;
    private float g;
    private int h = 0;
    private boolean i = false;
    private float j = 0.0f;
    private boolean k = false;
    private boolean l = false;
    private boolean m = false;
    private boolean n = false;
    private de o;

    public SimpleScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        try {
            getClass().getMethod("setOverScrollMode", Integer.TYPE).invoke(this, 2);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException e2) {
        }
        this.h = ViewConfiguration.get(getContext()).getScaledTouchSlop();
    }

    public void a(boolean z) {
        this.n = z;
    }

    public void draw(Canvas canvas) {
        try {
            super.draw(canvas);
        } catch (Exception e2) {
        }
    }

    public void a(de deVar) {
        this.o = deVar;
    }

    public void a() {
        this.o = null;
    }

    public void a(float f2) {
        this.j = f2;
    }

    public void b(float f2) {
        if (f2 >= this.j) {
            this.c = this.j;
            this.i = this.n;
        } else if (f2 <= 0.0f) {
            this.c = 0.0f;
            this.i = false;
        } else {
            this.c = f2;
            this.i = false;
        }
        this.b = 0.0f;
    }

    public void b(boolean z) {
        this.i = z;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.m = false;
                this.e = 0.0f;
                this.g = motionEvent.getRawY();
                break;
            case 2:
                this.f = motionEvent.getRawY();
                this.b = this.g - this.f;
                this.g = this.f;
                this.e += this.b;
                if (Math.abs(this.e) > ((float) this.h)) {
                    this.k = true;
                    this.m = true;
                    break;
                }
                break;
        }
        if (this.n) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        return this.k;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.m = false;
                break;
            case 1:
                this.m = this.k;
                if (this.k && this.o != null) {
                    this.o.b(this.b, this.c, this.h, this.j);
                }
                this.k = false;
                break;
            case 2:
                if (this.l) {
                    this.l = false;
                    float rawY = motionEvent.getRawY();
                    this.f = rawY;
                    this.g = rawY;
                }
                this.f = motionEvent.getRawY();
                this.b = this.g - this.f;
                this.g = this.f;
                if (this.i) {
                    if (this.b < 0.0f && getScrollY() == 0) {
                        this.i = false;
                        break;
                    }
                } else {
                    this.c += this.b;
                    this.e += this.b;
                    if (Math.abs(this.e) > ((float) this.h)) {
                        this.k = true;
                        this.m = true;
                    }
                    if (this.o != null) {
                        if (this.b > 0.0f) {
                            this.o.a();
                        } else {
                            this.o.b();
                        }
                    }
                    if (this.o != null) {
                        this.o.a(this.b, this.c, this.h, this.j);
                    }
                    b(this.c);
                    break;
                }
        }
        if (!this.i) {
            return this.m;
        }
        try {
            return super.onTouchEvent(motionEvent);
        } catch (Exception e2) {
            XLog.e(this.f3013a, e2.getMessage());
            return this.m;
        }
    }

    public void b() {
        this.d = this.c;
        this.l = true;
    }

    public void c() {
        b(this.d);
    }
}
