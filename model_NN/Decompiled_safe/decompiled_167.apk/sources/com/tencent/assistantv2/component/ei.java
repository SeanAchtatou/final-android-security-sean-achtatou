package com.tencent.assistantv2.component;

import android.app.Activity;
import com.tencent.assistant.utils.XLog;
import com.tencent.smtt.sdk.WebView;

/* compiled from: ProGuard */
class ei implements eb {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxWebViewContainer f3197a;

    ei(TxWebViewContainer txWebViewContainer) {
        this.f3197a = txWebViewContainer;
    }

    public Activity a() {
        return this.f3197a.t();
    }

    public void a(WebView webView, int i) {
        this.f3197a.d.setProgress(i);
        if (this.f3197a.h != null) {
            this.f3197a.h.a(webView, i);
        }
    }

    public void a(WebView webView, String str) {
        XLog.d("TxWebViewContainer", "onReceivedTitle--title = " + str);
        if (this.f3197a.t() != null) {
            this.f3197a.t().setTitle(str);
        }
    }
}
