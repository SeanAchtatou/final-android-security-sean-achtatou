package com.tencent.assistantv2.st.business;

import android.os.Handler;
import com.tencent.assistant.protocol.jce.StatCSChannelData;
import com.tencent.assistant.st.h;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
public class g extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private static g f3334a;
    private static Handler c = null;

    private g() {
    }

    public static synchronized g a() {
        g gVar;
        synchronized (g.class) {
            if (f3334a == null) {
                f3334a = new g();
            }
            gVar = f3334a;
        }
        return gVar;
    }

    public byte getSTType() {
        return 21;
    }

    public void flush() {
    }

    public void a(StatCSChannelData statCSChannelData) {
        if (statCSChannelData != null) {
            statCSChannelData.e = h.a();
            b().postDelayed(new h(this, statCSChannelData), 50);
        }
    }

    private synchronized Handler b() {
        if (c == null) {
            c = ba.a("CSChannelSTHandler");
        }
        return c;
    }
}
