package com.tencent.assistantv2.st.business;

import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.assistant.db.a.b;
import com.tencent.assistant.db.table.w;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.module.wisedownload.m;
import com.tencent.assistant.protocol.jce.StatAppInstall;
import com.tencent.assistant.protocol.jce.StatReportItem;
import com.tencent.assistant.protocol.l;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.f;
import com.tencent.assistant.st.g;
import com.tencent.assistant.st.h;
import com.tencent.assistant.st.model.AppInstallDetail;
import com.tencent.assistant.st.model.StatAppInstallWithDetail;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.st.i;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.http.util.ByteArrayBuffer;

/* compiled from: ProGuard */
public class o extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    public static o f3338a = null;
    private final String c;
    private final String d;
    private ConcurrentHashMap<String, StatAppInstallWithDetail> e;
    private ApkResCallback.Stub f;

    private o() {
        this.c = "StatAppInstallLog";
        this.d = "StatAppInstallDetailLog";
        this.e = null;
        this.f = new p(this);
        this.e = new ConcurrentHashMap<>();
        ApkResourceManager.getInstance().registerApkResCallback(this.f);
    }

    public static synchronized o a() {
        o oVar;
        synchronized (o.class) {
            if (f3338a == null) {
                f3338a = new o();
            }
            oVar = f3338a;
        }
        return oVar;
    }

    public byte a(int i) {
        switch (i) {
            case 0:
            default:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
        }
    }

    /* access modifiers changed from: private */
    public StatAppInstallWithDetail a(String str, int i) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String b = r.b(str, i);
        if (this.e.containsKey(b)) {
            return this.e.get(b);
        }
        return null;
    }

    private StatAppInstall b(String str, int i) {
        StatAppInstallWithDetail a2;
        if (!TextUtils.isEmpty(str) && (a2 = a(str, i)) != null) {
            return a2.c;
        }
        return null;
    }

    private List<AppInstallDetail> c(String str, int i) {
        StatAppInstallWithDetail a2;
        if (!TextUtils.isEmpty(str) && (a2 = a(str, i)) != null) {
            return a2.d;
        }
        return null;
    }

    private void d(String str, int i) {
        if (!TextUtils.isEmpty(str)) {
            this.e.remove(r.b(str, i));
        }
    }

    public void a(DownloadInfo downloadInfo, byte b, boolean z) {
        a(downloadInfo.packageName, downloadInfo.versionCode, downloadInfo.isSslUpdate(), b, z ? (byte) 0 : 1, ApkResourceManager.getInstance().getLocalApkInfo(downloadInfo.packageName) == null ? (byte) 0 : 1, downloadInfo.statInfo.scene, downloadInfo.statInfo.extraData, downloadInfo.appId, downloadInfo.apkId, downloadInfo.statInfo.callerUin, downloadInfo.statInfo.callerVia, downloadInfo.channelId, downloadInfo.statInfo.d, downloadInfo.statInfo.getFinalSlotId(), downloadInfo.statInfo.recommendId, 0, downloadInfo.statInfo.contentId, downloadInfo.uiType, downloadInfo.fileType, 0, downloadInfo.statInfo.callerVersionCode, downloadInfo.downloadEndTime, downloadInfo.statInfo.sourceScene, downloadInfo.statInfo.expatiation, downloadInfo.statInfo.pushInfo, downloadInfo.statInfo.sourceSceneSlotId, downloadInfo.statInfo.traceId);
    }

    private void a(String str, int i, boolean z, byte b, byte b2, byte b3, int i2, String str2, long j, long j2, String str3, String str4, String str5, int i3, String str6, byte[] bArr, long j3, String str7, SimpleDownloadInfo.UIType uIType, SimpleDownloadInfo.DownloadType downloadType, long j4, String str8, long j5, int i4, String str9, String str10, String str11, String str12) {
        XLog.d("InstallStManager", "logTmpRecord begin, packageName=" + str + ", versionCode=" + i + ", preInstallType=" + ((int) b));
        if (!TextUtils.isEmpty(str)) {
            StatAppInstall statAppInstall = new StatAppInstall();
            statAppInstall.f2353a = str;
            statAppInstall.j = i;
            StatAppInstallWithDetail statAppInstallWithDetail = new StatAppInstallWithDetail();
            statAppInstallWithDetail.f2539a = str;
            statAppInstallWithDetail.b = i;
            statAppInstallWithDetail.c = statAppInstall;
            this.e.put(r.b(str, i), statAppInstallWithDetail);
            statAppInstallWithDetail.d = Collections.synchronizedList(new ArrayList());
            statAppInstall.c = z;
            statAppInstall.e = 4;
            statAppInstall.g = b;
            statAppInstall.f = b2;
            statAppInstall.d = b3;
            statAppInstall.h = i2;
            statAppInstall.i = str2;
            statAppInstall.k = j;
            statAppInstall.l = j2;
            statAppInstall.o = str4;
            statAppInstall.n = ct.c(str3);
            statAppInstall.K = ct.d(str8);
            statAppInstall.s = str5;
            statAppInstall.t = i3;
            statAppInstall.u = str6;
            statAppInstall.T = str11;
            statAppInstall.y = bArr;
            statAppInstall.U = str7;
            statAppInstall.H = f.a();
            statAppInstall.L = i4;
            statAppInstall.M = j5;
            statAppInstall.B = f.b();
            statAppInstall.C = str10;
            if (TextUtils.isEmpty(str12)) {
                str12 = "yyb_i_" + (statAppInstall.f2353a + "_" + statAppInstall.j).hashCode();
            }
            statAppInstall.W = str12;
            statAppInstall.w = f();
            statAppInstall.E = uIType.ordinal();
            if (DownloadInfo.isUiTypeWiseDownload(uIType)) {
                statAppInstall.h = m.a(uIType);
            }
            if (downloadType == SimpleDownloadInfo.DownloadType.PLUGIN) {
                statAppInstall.D = 6;
                statAppInstall.h = STConst.ST_PAGE_PLUGIN;
            }
            statAppInstall.J = str9;
            statAppInstall.N = com.tencent.assistant.m.a().j() ? 1 : 0;
            statAppInstall.V = Global.getAppVersion() + "_" + Global.getBuildNo();
            statAppInstall.b = h.a();
            a(statAppInstallWithDetail);
            b(statAppInstallWithDetail);
        }
    }

    public void a(String str, int i, byte b, byte b2, String str2) {
        List<AppInstallDetail> c2;
        XLog.d("InstallStManager", "recordAppInstallDetailLog begin, packageName=" + str + ", versionCode=" + i + ", detailType=" + ((int) b) + ", result=" + ((int) b2) + ", resultDesc=" + str2);
        if (!TextUtils.isEmpty(str) && b(str, i) != null && (c2 = c(str, i)) != null) {
            AppInstallDetail appInstallDetail = new AppInstallDetail();
            appInstallDetail.f2538a = b;
            appInstallDetail.b = h.a();
            appInstallDetail.c = b2;
            appInstallDetail.d = str2;
            c2.add(appInstallDetail);
        }
    }

    public void a(String str, int i, byte b, byte b2, String str2, boolean z) {
        StatAppInstallWithDetail a2;
        StatAppInstall statAppInstall;
        List<AppInstallDetail> list;
        LocalApkInfo localApkInfo;
        String str3;
        byte b3 = 0;
        XLog.d("InstallStManager", "reportAppInstallEndLog begin, packageName=" + str + ", versionCode=" + i + ", installType=" + ((int) b) + ", result=" + ((int) b2) + ", failDesc=" + str2 + ", checkPassed=" + z);
        if (!TextUtils.isEmpty(str) && (a2 = a(str, i)) != null && (statAppInstall = a2.c) != null && (list = a2.d) != null) {
            String str4 = Constants.STR_EMPTY;
            int i2 = 0;
            while (i2 < list.size()) {
                AppInstallDetail appInstallDetail = list.get(i2);
                if (appInstallDetail != null) {
                    str3 = str4 + appInstallDetail.d + ";";
                } else {
                    str3 = str4;
                }
                i2++;
                str4 = str3;
            }
            statAppInstall.b = h.a();
            statAppInstall.e = b2;
            statAppInstall.m = ct.a(str4);
            if (z) {
                b3 = 1;
            }
            statAppInstall.G = b3;
            if (b2 == 1 && (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(str)) != null) {
                statAppInstall.p = localApkInfo.signature;
                if (k.b().a(str) != null) {
                    statAppInstall.q = localApkInfo.manifestMd5;
                }
                statAppInstall.r = localApkInfo.getAppType();
            }
            a(a2);
            b(a2);
            d(str, i);
        }
    }

    public byte getSTType() {
        return 5;
    }

    public void flush() {
    }

    public void a(String str, int i, byte b) {
        StatAppInstallWithDetail a2;
        StatAppInstall b2;
        XLog.d("InstallStManager", "saveInstallLogToDb begin, packageName=" + str + ", versionCode=" + i + ", installType=" + ((int) b));
        if (!TextUtils.isEmpty(str) && b == 0 && (a2 = a(str, i)) != null && (b2 = b(str, i)) != null) {
            b2.b = h.a();
            b2.G = 1;
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                new ObjectOutputStream(byteArrayOutputStream).writeObject(a2);
                w.a().a(getSTType(), str, i, byteArrayOutputStream.toByteArray());
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public ArrayList<StatReportItem> b() {
        StatAppInstall statAppInstall;
        String str;
        ArrayList<StatReportItem> arrayList = new ArrayList<>();
        List<b> a2 = w.a().a(System.currentTimeMillis() - 360000, 20);
        XLog.d("InstallStManager", "loadInstallLogFromDb begin, sTIntallLogList.size=" + (a2 == null ? null : Integer.valueOf(a2.size())));
        if (a2 == null || a2.size() == 0) {
            return arrayList;
        }
        byte sTType = getSTType();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        for (b next : a2) {
            try {
                arrayList3.add(Long.valueOf(next.f1228a));
                StatAppInstallWithDetail statAppInstallWithDetail = (StatAppInstallWithDetail) new ObjectInputStream(new ByteArrayInputStream(next.f)).readObject();
                if (!(statAppInstallWithDetail == null || (statAppInstall = statAppInstallWithDetail.c) == null)) {
                    AppInstallDetail appInstallDetail = new AppInstallDetail();
                    appInstallDetail.f2538a = 0;
                    appInstallDetail.b = h.a();
                    appInstallDetail.c = 1;
                    appInstallDetail.d = Constants.STR_EMPTY;
                    List<AppInstallDetail> list = statAppInstallWithDetail.d;
                    list.add(appInstallDetail);
                    arrayList2.add(Long.valueOf(next.f1228a));
                    String str2 = Constants.STR_EMPTY;
                    int i = 0;
                    while (i < list.size()) {
                        AppInstallDetail appInstallDetail2 = list.get(i);
                        if (appInstallDetail2 != null) {
                            str = str2 + appInstallDetail2.d + ";";
                        } else {
                            str = str2;
                        }
                        i++;
                        str2 = str;
                    }
                    statAppInstall.b = h.a();
                    statAppInstall.e = 1;
                    statAppInstall.m = ct.a(str2);
                    arrayList4.add(l.b(statAppInstall));
                    b(statAppInstall.f2353a, statAppInstall.j, (byte) 0, statAppInstall.e, Constants.STR_EMPTY);
                    b(statAppInstallWithDetail);
                    d(statAppInstall.f2353a, statAppInstall.j);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        w.a().a(arrayList3);
        StatReportItem statReportItem = new StatReportItem();
        statReportItem.f2374a = sTType;
        statReportItem.c = h.a(arrayList4);
        statReportItem.b = i.a();
        g gVar = new g();
        gVar.b = statReportItem;
        gVar.f2537a = arrayList2;
        arrayList.add(gVar.b);
        if (this.b != null) {
            this.b.a(sTType, arrayList4);
        }
        return arrayList;
    }

    public void b(String str, int i, byte b, byte b2, String str2) {
        StatAppInstall b3;
        XLog.d("InstallStManager", "detailLogReportToBeacon begin, packageName=" + str + ", versionCode=" + i + ", installType=" + ((int) b) + ", result=" + ((int) b2) + ", resultDesc=" + str2);
        if (!TextUtils.isEmpty(str) && (b3 = b(str, i)) != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("B1", Global.getQUA());
            hashMap.put("B2", Global.getPhoneGuid());
            hashMap.put("B3", b3.f2353a);
            hashMap.put("B4", String.valueOf(b3.j));
            hashMap.put("B5", b3.W);
            hashMap.put("B6", String.valueOf(h.a()));
            hashMap.put("B7", String.valueOf((int) b));
            hashMap.put("B8", String.valueOf((int) b2));
            hashMap.put("B9", str2);
            a.a("StatAppInstallDetailLog", true, 0, 0, hashMap, true);
        }
    }

    private void a(StatAppInstallWithDetail statAppInstallWithDetail) {
        if (statAppInstallWithDetail == null || statAppInstallWithDetail.c == null || statAppInstallWithDetail.d == null) {
            XLog.w("InstallStManager", "logReportToServer begin, data=null");
            return;
        }
        StatAppInstall statAppInstall = statAppInstallWithDetail.c;
        XLog.d("InstallStManager", "logReportToServer begin, packageName=" + statAppInstall.f2353a + ", versionCode=" + statAppInstall.j + ", installType=" + ((int) statAppInstall.g) + ", result=" + ((int) statAppInstall.e) + ", resultDesc=" + statAppInstall.m);
        byte[] b = l.b(statAppInstallWithDetail.c);
        byte[] a2 = r.a(b.length);
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(b.length + 4);
        byteArrayBuffer.append(a2, 0, a2.length);
        byteArrayBuffer.append(b, 0, b.length);
        if (this.b != null) {
            this.b.a(getSTType(), byteArrayBuffer.buffer());
        }
    }

    private void b(StatAppInstallWithDetail statAppInstallWithDetail) {
        String str;
        String str2;
        String str3;
        String str4;
        if (statAppInstallWithDetail == null || statAppInstallWithDetail.c == null || statAppInstallWithDetail.d == null) {
            XLog.w("InstallStManager", "logReportToBeacon begin, data=null");
            return;
        }
        StatAppInstall statAppInstall = statAppInstallWithDetail.c;
        XLog.d("InstallStManager", "logReportToBeacon begin, packageName=" + statAppInstall.f2353a + ", versionCode=" + statAppInstall.j + ", installType=" + ((int) statAppInstall.g) + ", result=" + ((int) statAppInstall.e) + ", resultDesc=" + statAppInstall.m);
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getQUA());
        hashMap.put("B2", Global.getPhoneGuid());
        hashMap.put("B3", statAppInstall.f2353a);
        hashMap.put("B4", String.valueOf(statAppInstall.b));
        hashMap.put("B5", String.valueOf(statAppInstall.c));
        hashMap.put("B6", String.valueOf((int) statAppInstall.d));
        hashMap.put("B7", String.valueOf((int) statAppInstall.e));
        hashMap.put("B8", String.valueOf((int) statAppInstall.f));
        hashMap.put("B9", String.valueOf((int) statAppInstall.g));
        hashMap.put("B10", String.valueOf(statAppInstall.h));
        hashMap.put("B11", statAppInstall.i);
        hashMap.put("B12", String.valueOf(statAppInstall.j));
        hashMap.put("B13", String.valueOf(statAppInstall.k));
        hashMap.put("B14", String.valueOf(statAppInstall.l));
        hashMap.put("B15", statAppInstall.m);
        hashMap.put("B16", String.valueOf(statAppInstall.n));
        hashMap.put("B17", statAppInstall.o);
        hashMap.put("B18", statAppInstall.p);
        hashMap.put("B19", statAppInstall.q);
        hashMap.put("B20", String.valueOf((int) statAppInstall.r));
        hashMap.put("B21", statAppInstall.s);
        hashMap.put("B22", String.valueOf(statAppInstall.t));
        hashMap.put("B23", statAppInstall.u);
        hashMap.put("B24", statAppInstall.w != null ? statAppInstall.w.a() + "-" + statAppInstall.w.b() : Constants.STR_EMPTY);
        hashMap.put("B25", String.valueOf(statAppInstall.B));
        hashMap.put("B26", statAppInstall.C);
        hashMap.put("B27", String.valueOf(statAppInstall.D));
        hashMap.put("B28", String.valueOf(statAppInstall.E));
        hashMap.put("B29", String.valueOf((int) statAppInstall.G));
        hashMap.put("B30", statAppInstall.H);
        hashMap.put("B31", statAppInstall.I);
        hashMap.put("B32", statAppInstall.J);
        hashMap.put("B33", String.valueOf(statAppInstall.K));
        hashMap.put("B34", String.valueOf(statAppInstall.L));
        hashMap.put("B35", String.valueOf(statAppInstall.M));
        hashMap.put("B36", String.valueOf(statAppInstall.N));
        hashMap.put("B37", String.valueOf(statAppInstall.O));
        hashMap.put("B38", String.valueOf(statAppInstall.R));
        hashMap.put("B39", statAppInstall.T);
        hashMap.put("B40", statAppInstall.U);
        hashMap.put("B41", statAppInstall.V);
        hashMap.put("B42", statAppInstall.W);
        String str5 = Constants.STR_EMPTY;
        String str6 = Constants.STR_EMPTY;
        String str7 = Constants.STR_EMPTY;
        String str8 = Constants.STR_EMPTY;
        List<AppInstallDetail> list = statAppInstallWithDetail.d;
        if (list != null && !list.isEmpty()) {
            int i = 0;
            while (i < list.size()) {
                AppInstallDetail appInstallDetail = list.get(i);
                if (appInstallDetail != null) {
                    str2 = a(appInstallDetail);
                    switch (appInstallDetail.f2538a) {
                        case -10:
                            str2 = str7;
                            str = str5 + str2;
                            str4 = str8;
                            str3 = str6;
                            continue;
                            i++;
                            str5 = str;
                            str7 = str2;
                            str6 = str3;
                            str8 = str4;
                        case 0:
                            str4 = str2;
                            str3 = str6;
                            str2 = str7;
                            str = str5;
                            continue;
                            i++;
                            str5 = str;
                            str7 = str2;
                            str6 = str3;
                            str8 = str4;
                        case 1:
                            str4 = str8;
                            str = str5;
                            str3 = str6;
                            continue;
                            i++;
                            str5 = str;
                            str7 = str2;
                            str6 = str3;
                            str8 = str4;
                        case 2:
                            str4 = str8;
                            str3 = str2;
                            str2 = str7;
                            str = str5;
                            continue;
                            i++;
                            str5 = str;
                            str7 = str2;
                            str6 = str3;
                            str8 = str4;
                    }
                }
                str4 = str8;
                str2 = str7;
                str3 = str6;
                str = str5;
                i++;
                str5 = str;
                str7 = str2;
                str6 = str3;
                str8 = str4;
            }
        }
        hashMap.put("B43", str5);
        hashMap.put("B44", str6);
        hashMap.put("B45", str7);
        hashMap.put("B46", str8);
        a.a("StatAppInstallLog", true, 0, 0, hashMap, true);
    }

    private String a(AppInstallDetail appInstallDetail) {
        if (appInstallDetail != null) {
            return ((int) appInstallDetail.f2538a) + "," + appInstallDetail.b + "," + ((int) appInstallDetail.c) + "," + appInstallDetail.d + ";";
        }
        return null;
    }
}
