package com.tencent.assistantv2.st.business;

import com.tencent.assistant.db.table.x;
import com.tencent.assistant.protocol.jce.StatPhotoBackup;
import com.tencent.assistant.st.h;
import com.tencent.assistant.utils.bh;

/* compiled from: ProGuard */
public class t extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private StatPhotoBackup f3342a;

    public t() {
        this.f3342a = null;
        this.f3342a = new StatPhotoBackup();
    }

    public void a(byte b, boolean z, short s) {
        if (this.f3342a != null) {
            this.f3342a.f2372a = b;
            this.f3342a.b = z;
            this.f3342a.c = s;
            this.f3342a.d = h.a();
        }
        a();
    }

    public void a() {
        byte[] a2;
        if (this.f3342a != null && (a2 = bh.a(this.f3342a)) != null && a2.length > 0) {
            x.a().a(getSTType(), a2);
        }
    }

    public byte getSTType() {
        return 9;
    }

    public void flush() {
        a();
    }
}
