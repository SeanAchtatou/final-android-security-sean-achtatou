package com.tencent.assistantv2.b;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.model.a.g;
import java.util.ArrayList;

/* compiled from: ProGuard */
class u implements CallbackHelper.Caller<g> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f2971a;
    final /* synthetic */ int b;
    final /* synthetic */ ArrayList c;
    final /* synthetic */ r d;

    u(r rVar, boolean z, int i, ArrayList arrayList) {
        this.d = rVar;
        this.f2971a = z;
        this.b = i;
        this.c = arrayList;
    }

    /* renamed from: a */
    public void call(g gVar) {
        XLog.d("GetHomeEngine", "to caller has next:" + this.f2971a + ",pagerContext:" + this.d.j);
        gVar.a(this.b, 0, this.f2971a, this.d.j, false, null, null, 0, this.c, this.d.g);
    }
}
