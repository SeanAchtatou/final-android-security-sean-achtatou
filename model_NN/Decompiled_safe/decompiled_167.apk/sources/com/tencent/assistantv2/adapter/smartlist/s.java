package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.adapter.a.a;
import com.tencent.assistant.adapter.a.d;
import com.tencent.assistant.adapter.c;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.e;
import com.tencent.assistant.module.callback.w;
import com.tencent.assistant.module.en;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.component.fps.FPSNormalItemNoReasonView;
import com.tencent.assistantv2.component.fps.FPSRankNormalItem;

/* compiled from: ProGuard */
public class s extends a {
    static w c;
    d d = new t(this);
    private IViewInvalidater e;

    public s(Context context, ab abVar, IViewInvalidater iViewInvalidater) {
        super(context, abVar);
        this.e = iViewInvalidater;
    }

    public Pair<View, Object> a() {
        FPSNormalItemNoReasonView fPSNormalItemNoReasonView = new FPSNormalItemNoReasonView(this.f2908a, this.e);
        return Pair.create(fPSNormalItemNoReasonView, fPSNormalItemNoReasonView.f3210a);
    }

    public void a(View view, Object obj, int i, e eVar) {
        SimpleAppModel simpleAppModel;
        v vVar = (v) obj;
        if (eVar != null) {
            simpleAppModel = eVar.c();
        } else {
            simpleAppModel = null;
        }
        view.setOnClickListener(new w(this.f2908a, i, simpleAppModel, this.b));
        a(vVar, simpleAppModel, i, this.b.a());
        com.tencent.assistant.adapter.a.e.a(this.f2908a, this.b.j(), this.d, obj, simpleAppModel, i);
    }

    private void a(v vVar, SimpleAppModel simpleAppModel, int i, com.tencent.assistant.model.d dVar) {
        AppConst.AppState appState;
        if (simpleAppModel != null && vVar != null) {
            if (!TextUtils.isEmpty(simpleAppModel.ae)) {
                vVar.j.setVisibility(0);
                vVar.j.setText(simpleAppModel.ae);
            } else {
                vVar.j.setVisibility(8);
            }
            if (dVar != null) {
                appState = dVar.c;
            } else {
                appState = null;
            }
            vVar.n.a(simpleAppModel, dVar);
            vVar.l.setText(simpleAppModel.d);
            if (this.b.g()) {
                if (1 == (((int) (simpleAppModel.B >> 2)) & 3)) {
                    Drawable drawable = this.f2908a.getResources().getDrawable(R.drawable.appdownload_icon_original);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    vVar.l.setCompoundDrawablePadding(df.b(6.0f));
                    vVar.l.setCompoundDrawables(null, null, drawable, null);
                } else {
                    vVar.l.setCompoundDrawables(null, null, null, null);
                }
            }
            vVar.k.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            vVar.m.a(simpleAppModel, dVar);
            if (vVar.o != null) {
                if (!this.b.k()) {
                    vVar.o.setVisibility(8);
                } else if (TextUtils.isEmpty(simpleAppModel.X)) {
                    vVar.o.setVisibility(8);
                } else {
                    vVar.o.setText(simpleAppModel.X);
                    vVar.o.setVisibility(0);
                }
            }
            if (vVar.p != null) {
                vVar.p.a(simpleAppModel, this.e);
            }
            if (com.tencent.assistant.component.appdetail.process.s.a(simpleAppModel, appState)) {
                vVar.m.setClickable(false);
            } else {
                vVar.m.setClickable(true);
                vVar.m.a(this.b.e(), new u(this, appState, vVar), dVar, vVar.m, vVar.n);
            }
            if (simpleAppModel.ao == null || simpleAppModel.ao.size() <= 0) {
                vVar.q.setVisibility(8);
                vVar.r.setVisibility(0);
            } else {
                vVar.q.setVisibility(0);
                vVar.q.setImageViewUrls(simpleAppModel.ao);
                vVar.q.requestLayout();
                vVar.r.setVisibility(8);
            }
            if (this.b.d()) {
                this.b.a(false);
                a(simpleAppModel, vVar.m);
            }
            if (vVar.v == null || simpleAppModel.aw == null) {
                vVar.v.setVisibility(8);
            } else {
                vVar.r.setVisibility(0);
                vVar.v.setVisibility(0);
                vVar.v.a(simpleAppModel);
            }
            if (this.b.f() == SmartListAdapter.SmartListType.SearchPage) {
                c.b(this.f2908a, simpleAppModel, vVar.l, true);
            } else {
                c.a(this.f2908a, simpleAppModel, vVar.l, false);
            }
            if (this.b.l() && vVar.u != null) {
                vVar.u.setVisibility(0);
                FPSRankNormalItem.a(vVar.u, this.b.m());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(SimpleAppModel simpleAppModel, View view) {
        boolean j = simpleAppModel.j();
        if (c == null) {
            c = new a(this.f2908a, this.b.j(), this.d);
            en.a().register(c);
        } else if (c instanceof a) {
            ((a) c).a(this.f2908a, this.b.j(), this.d);
        }
        if (com.tencent.assistant.adapter.a.e.a(this.f2908a, simpleAppModel, this.b.j(), this.d, (View) view.getParent(), !j)) {
            Object tag = ((View) view.getParent()).getTag();
            if (tag instanceof v) {
                simpleAppModel.ao = null;
                ((v) tag).q.setVisibility(8);
            }
        }
    }
}
