package com.tencent.assistantv2.adapter;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class h extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2892a;
    final /* synthetic */ int b;
    final /* synthetic */ CategoryDetailListAdapter c;

    h(CategoryDetailListAdapter categoryDetailListAdapter, SimpleAppModel simpleAppModel, int i) {
        this.c = categoryDetailListAdapter;
        this.f2892a = simpleAppModel;
        this.b = i;
    }

    public void onTMAClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("com.tencent.assistant.ACTION_URL", this.f2892a.aa);
        b.b(this.c.e, this.f2892a.aa.f1970a, bundle);
    }

    public STInfoV2 getStInfo(View view) {
        String str = "01";
        if (s.a(this.f2892a)) {
            str = "02";
        }
        return this.c.a(this.f2892a, this.b, str, 200);
    }
}
