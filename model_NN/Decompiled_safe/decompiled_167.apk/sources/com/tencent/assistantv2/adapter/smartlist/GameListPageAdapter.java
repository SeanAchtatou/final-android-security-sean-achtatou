package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.model.b;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;

/* compiled from: ProGuard */
public class GameListPageAdapter extends SmartListAdapter {
    public GameListPageAdapter(Context context, View view, b bVar) {
        super(context, view, bVar);
    }

    /* access modifiers changed from: protected */
    public SmartListAdapter.SmartListType a() {
        return SmartListAdapter.SmartListType.GamePage;
    }

    /* access modifiers changed from: protected */
    public String a(int i) {
        return "07";
    }
}
