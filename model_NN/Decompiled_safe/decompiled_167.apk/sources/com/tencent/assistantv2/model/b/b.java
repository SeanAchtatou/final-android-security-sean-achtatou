package com.tencent.assistantv2.model.b;

import android.text.TextUtils;
import com.tencent.assistant.protocol.jce.ExplicitHotWord;
import com.tencent.assistant.utils.XLog;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class b extends e<Integer, a> {

    /* renamed from: a  reason: collision with root package name */
    private final String f3317a = "ExplicitHotwordsPool";

    public void a(int i, ExplicitHotWord explicitHotWord) {
        a aVar = (a) a(Integer.valueOf(i));
        if (aVar == null) {
            aVar = new a();
            a(Integer.valueOf(i), aVar);
        }
        aVar.a(explicitHotWord);
    }

    public int a(String str) {
        ConcurrentHashMap a2;
        a aVar;
        XLog.d("ExplicitHotwordsPool", "removeHotwords:" + str);
        int i = 0;
        if (TextUtils.isEmpty(str) || (a2 = a()) == null || a2.size() <= 0) {
            return 0;
        }
        XLog.d("ExplicitHotwordsPool", "removeHotwords:data size > 0");
        Iterator it = a2.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            XLog.d("ExplicitHotwordsPool", "removeHotwords: has next");
            Map.Entry entry = (Map.Entry) it.next();
            if (!(entry == null || (aVar = (a) entry.getValue()) == null)) {
                XLog.d("ExplicitHotwordsPool", "removeHotwords: model not null");
                i2 += aVar.a(str);
            }
            i = i2;
        }
    }
}
