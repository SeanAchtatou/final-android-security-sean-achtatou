package com.tencent.assistantv2.model;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.module.u;

/* compiled from: ProGuard */
public abstract class c {
    public abstract String q();

    public abstract SimpleDownloadInfo.DownloadType r();

    public AppConst.AppState t() {
        return u.a(this);
    }
}
