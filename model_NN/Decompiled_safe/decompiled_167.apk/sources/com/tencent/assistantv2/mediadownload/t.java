package com.tencent.assistantv2.mediadownload;

import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.model.AbstractDownloadInfo;
import com.tencent.assistantv2.model.a;
import com.tencent.assistantv2.model.h;
import com.tencent.downloadsdk.ae;

/* compiled from: ProGuard */
class t implements ae {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f3302a;

    t(o oVar) {
        this.f3302a = oVar;
    }

    public void a(int i, String str, String str2) {
        h hVar = (h) this.f3302a.f3297a.get(str);
        if (hVar != null) {
            hVar.g = str2;
            hVar.h = str2;
            hVar.i = AbstractDownloadInfo.DownState.SUCC;
            hVar.j = 0;
            hVar.f = System.currentTimeMillis();
            this.f3302a.c.sendMessage(this.f3302a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, hVar));
            this.f3302a.b.a(hVar);
        }
    }

    public void a(int i, String str) {
        h hVar = (h) this.f3302a.f3297a.get(str);
        if (hVar != null) {
            hVar.i = AbstractDownloadInfo.DownState.DOWNLOADING;
            hVar.j = 0;
            this.f3302a.c.sendMessage(this.f3302a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, hVar));
            this.f3302a.b.a(hVar);
        }
    }

    public void a(int i, String str, long j, String str2, String str3) {
        h hVar = (h) this.f3302a.f3297a.get(str);
        if (hVar != null) {
            if (hVar.k == null) {
                hVar.k = new a();
            }
            hVar.k.f3314a = j;
            hVar.d = j;
            this.f3302a.b.a(hVar);
        }
    }

    public void a(int i, String str, long j, long j2, double d) {
        h hVar = (h) this.f3302a.f3297a.get(str);
        if (hVar != null) {
            if (hVar.i != AbstractDownloadInfo.DownState.DOWNLOADING) {
                this.f3302a.c.sendMessage(this.f3302a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, hVar));
            }
            hVar.i = AbstractDownloadInfo.DownState.DOWNLOADING;
            hVar.j = 0;
            hVar.k.b = j2;
            hVar.k.f3314a = j;
            hVar.k.c = ct.a(d);
            this.f3302a.c.sendMessage(this.f3302a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, hVar));
        }
    }

    public void a(int i, String str, String str2, String str3) {
        h hVar = (h) this.f3302a.f3297a.get(str);
        if (hVar != null) {
            hVar.h = str2;
            hVar.i = AbstractDownloadInfo.DownState.SUCC;
            hVar.j = 0;
            hVar.f = System.currentTimeMillis();
            hVar.k.b = hVar.k.f3314a;
            this.f3302a.c.sendMessage(this.f3302a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, hVar));
            this.f3302a.b.a(hVar);
        }
    }

    public void a(int i, String str, int i2, byte[] bArr, String str2) {
        h hVar = (h) this.f3302a.f3297a.get(str);
        if (hVar != null) {
            hVar.i = AbstractDownloadInfo.DownState.FAIL;
            hVar.j = i2;
            this.f3302a.c.sendMessage(this.f3302a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL, hVar));
            this.f3302a.b.a(hVar);
        }
        if (i2 == -12) {
            TemporaryThreadManager.get().start(new u(this));
        } else if (i2 == -11) {
            ba.a().post(new v(this));
        }
        FileUtil.tryRefreshPath(i2);
    }

    public void b(int i, String str) {
        h hVar = (h) this.f3302a.f3297a.get(str);
        if (hVar == null) {
            return;
        }
        if (hVar.i == AbstractDownloadInfo.DownState.QUEUING || hVar.i == AbstractDownloadInfo.DownState.DOWNLOADING) {
            hVar.i = AbstractDownloadInfo.DownState.PAUSED;
            hVar.j = 0;
            this.f3302a.c.sendMessage(this.f3302a.c.obtainMessage(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE, hVar));
            this.f3302a.b.a(hVar);
        }
    }

    public void b(int i, String str, String str2) {
    }
}
