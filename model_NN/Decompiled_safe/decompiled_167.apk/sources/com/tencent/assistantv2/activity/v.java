package com.tencent.assistantv2.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXRefreshGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.manager.h;
import com.tencent.assistant.model.e;
import com.tencent.assistant.module.callback.ai;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.adapter.smartlist.SoftwareListPageAdapter;
import com.tencent.assistantv2.component.banner.f;
import com.tencent.assistantv2.component.banner.floatheader.FloatBannerViewSwitcher;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.k;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class v extends ay implements ITXRefreshListViewListener, NetworkMonitor.ConnectivityChangeListener, ai {
    boolean P = true;
    private final String S = "AppTabActivity:";
    private LinearLayout T;
    private TXRefreshGetMoreListView U;
    private ViewStub V;
    /* access modifiers changed from: private */
    public NormalErrorRecommendPage W = null;
    /* access modifiers changed from: private */
    public LoadingView X;
    private h Y = new h();
    /* access modifiers changed from: private */
    public SoftwareListPageAdapter Z = null;
    private byte[] aa = null;
    private boolean ab = false;
    private int ac = 1;
    /* access modifiers changed from: private */
    public SmartListAdapter.BannerType ad = SmartListAdapter.BannerType.None;
    /* access modifiers changed from: private */
    public FloatBannerViewSwitcher ae;
    private APN af = APN.NO_NETWORK;

    public void d(Bundle bundle) {
        super.d(bundle);
        this.T = new LinearLayout(this.Q);
        a(this.T);
        this.Y.a(this);
    }

    private void b(View view) {
        this.U = (TXRefreshGetMoreListView) view.findViewById(R.id.list);
        this.Z = new SoftwareListPageAdapter(this.Q, this.U, this.Y.d());
        this.Z.a(true);
        this.U.setAdapter(this.Z);
        this.U.setRefreshListViewListener(this);
        this.U.setVisibility(8);
        this.U.setDivider(null);
        this.U.setListSelector(17170445);
        x xVar = new x(this, null);
        this.U.setIScrollerListener(xVar);
        this.Z.a(xVar);
        this.V = (ViewStub) view.findViewById(R.id.error_stub);
        this.X = (LoadingView) view.findViewById(R.id.loading);
        this.ae = (FloatBannerViewSwitcher) view.findViewById(R.id.float_banner_switcher);
        this.ae.a(this.U.getListView(), SmartListAdapter.SmartListType.AppPage.ordinal());
    }

    /* access modifiers changed from: private */
    public void A() {
        k.a(200501, CostTimeSTManager.TIMETYPE.START, System.currentTimeMillis());
        this.Y.a();
    }

    private void b(int i) {
        if (this.W == null) {
            B();
        }
        this.W.setErrorType(i);
        if (this.U != null) {
            this.U.setVisibility(8);
        }
        this.W.setVisibility(0);
    }

    private void B() {
        this.V.inflate();
        this.W = (NormalErrorRecommendPage) h().findViewById(R.id.error);
        this.W.setButtonClickListener(new w(this));
    }

    public void d(boolean z) {
        if (this.P) {
            this.P = false;
            this.T.removeAllViews();
            View inflate = this.R.inflate((int) R.layout.act4, (ViewGroup) null);
            this.T.addView(inflate);
            this.T.requestLayout();
            this.T.forceLayout();
            this.T.invalidate();
            b(inflate);
            A();
            cq.a().a(this);
        } else {
            this.Y.e();
        }
        if (this.Z != null) {
            this.Z.l();
            this.Z.notifyDataSetChanged();
        }
        if (this.ae != null) {
            this.ae.f();
        }
        Log.d("YYB5_0", "AppTabActivity:onResume------------3");
    }

    public void k() {
        super.k();
        if (this.Z != null) {
            this.Z.k();
        }
        if (this.ae != null) {
            this.ae.g();
        }
        cq.a().b(this);
        Log.d("YYB5_0", "AppTabActivity:onDestroy------------3");
    }

    public v() {
        super(MainActivity.i());
    }

    public void C() {
        if (this.Z != null) {
            this.Z.k();
        }
        Log.e("YYB5_0", "AppTabActivity:onPageTurnBackground------------3:::");
    }

    public int D() {
        return 1;
    }

    public int E() {
        return 3;
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        if (TXScrollViewBase.ScrollState.ScrollState_FromEnd == scrollState) {
            this.Y.b();
        }
        if (TXScrollViewBase.ScrollState.ScrollState_FromStart == scrollState) {
            A();
        }
    }

    public void a(int i, int i2, boolean z, byte[] bArr, boolean z2, ArrayList<ColorCardItem> arrayList, List<e> list) {
        if (this.X != null) {
            this.X.setVisibility(8);
        }
        if (i2 == 0) {
            if (this.W != null) {
                this.W.setVisibility(8);
            }
            this.U.setVisibility(0);
            this.aa = bArr;
            if (list == null || list.size() == 0) {
                k.a(200501, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
                b(10);
                return;
            }
            this.Z.a(z2, list, (List<f>) null, arrayList);
            if (z2) {
                this.ae.a(arrayList);
                if (i == -1) {
                    k.a(200501, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                    this.U.onRefreshComplete(false, z, a((int) R.string.refresh_fail));
                    return;
                }
                k.a(200501, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                this.U.onRefreshComplete(true, z, null);
                return;
            }
            this.U.onRefreshComplete(z, true);
        } else if (z2) {
            k.a(200501, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
            if (-800 == i2) {
                b(30);
            } else if (this.ac <= 0) {
                b(20);
            } else {
                this.ac--;
                this.Y.c();
            }
        } else {
            this.U.onRefreshComplete(z, false);
        }
    }

    public int J() {
        return 200501;
    }

    public void I() {
        M();
    }

    public void onConnected(APN apn) {
        if (this.Z != null) {
            XLog.d("leobi", "app onConnected" + this.Z.h());
            if (this.Z.h() <= 0) {
                A();
            }
        }
    }

    public void onDisconnected(APN apn) {
        this.af = apn;
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        this.af = apn2;
    }
}
