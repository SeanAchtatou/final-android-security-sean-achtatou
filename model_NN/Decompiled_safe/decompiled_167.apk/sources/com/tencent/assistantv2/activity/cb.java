package com.tencent.assistantv2.activity;

import android.support.v4.view.ViewPager;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class cb implements ViewPager.OnPageChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GuideActivity f2799a;

    public cb(GuideActivity guideActivity) {
        this.f2799a = guideActivity;
    }

    public void onPageSelected(int i) {
        int unused = this.f2799a.T = i;
        XLog.e("zhangyuanchao", "-----mCurrentIndex---:" + this.f2799a.T);
        if (this.f2799a.T == 0) {
            this.f2799a.j();
        }
    }

    public void onPageScrollStateChanged(int i) {
    }

    public void onPageScrolled(int i, float f, int i2) {
    }
}
