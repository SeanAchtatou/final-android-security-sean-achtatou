package com.tencent.assistantv2.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class dt extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f2843a;

    dt(SearchActivity searchActivity) {
        this.f2843a = searchActivity;
    }

    public void onTMAClick(View view) {
        this.f2843a.finish();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2843a, 200);
        buildSTInfo.slotId = a.a("03", "001");
        return buildSTInfo;
    }
}
