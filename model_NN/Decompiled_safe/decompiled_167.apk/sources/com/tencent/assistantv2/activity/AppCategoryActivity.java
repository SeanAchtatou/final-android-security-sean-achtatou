package com.tencent.assistantv2.activity;

import android.os.Bundle;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.adapter.AppCategoryListAdapter;
import com.tencent.assistant.component.CategoryListPage;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.module.az;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import java.util.List;

/* compiled from: ProGuard */
public class AppCategoryActivity extends BaseActivity {
    CategoryListPage n;
    private SecondNavigationTitleViewV5 t;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.act_app_category);
        w();
        ba.a().postDelayed(new a(this), 500);
    }

    private void w() {
        this.t = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.t.i();
        this.t.d(false);
        this.t.a(this);
        this.t.b(getResources().getString(R.string.app_category_title));
        this.t.d(v());
        this.n = (CategoryListPage) findViewById(R.id.clp_act_list);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.t.l();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.t.m();
    }

    /* access modifiers changed from: private */
    public void x() {
        this.n.setCategoryType(i());
        this.n.onResume();
        this.n.setVisibility(0);
        this.n.setViewPageListener(new ViewPageScrollListener());
        List<ColorCardItem> c = az.a().c(i());
        List<AppCategory> a2 = az.a().a(i());
        List<AppCategory> b = az.a().b(i());
        AppCategoryListAdapter appCategoryListAdapter = new AppCategoryListAdapter(this, this.n, j(), null, null, az.a().b());
        appCategoryListAdapter.a(c, a2, b);
        this.n.setAdapter(appCategoryListAdapter);
        this.n.refreshData();
    }

    /* access modifiers changed from: protected */
    public long i() {
        return -1;
    }

    /* access modifiers changed from: protected */
    public AppCategoryListAdapter.CategoryType j() {
        return AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE;
    }

    /* access modifiers changed from: protected */
    public int v() {
        return 1;
    }

    public int f() {
        return STConst.ST_PAGE_SOFTWARE_CATEGORY;
    }
}
