package com.tencent.assistantv2.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import com.qq.AppService.AstApp;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public abstract class ay extends Fragment {
    private View P;
    protected Context Q;
    protected LayoutInflater R;
    private boolean S;

    public abstract void C();

    public abstract int D();

    public abstract int E();

    public abstract void I();

    public abstract void d(boolean z);

    public void d(Bundle bundle) {
        super.d(bundle);
        this.S = true;
    }

    public boolean L() {
        return this.S;
    }

    public ay(Activity activity) {
        this.P = null;
        this.S = false;
        this.Q = activity;
        this.R = LayoutInflater.from(this.Q);
    }

    public ay() {
        this.P = null;
        this.S = false;
        this.Q = MainActivity.i();
        if (this.Q == null) {
            this.Q = AstApp.i();
        }
        this.R = LayoutInflater.from(this.Q);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void d(int i) {
        this.P = this.R.inflate(i, (ViewGroup) null, false);
    }

    public View h() {
        View h = super.h();
        if (h != null) {
            return h;
        }
        XLog.d("BaseGragment", "get View return null.");
        return this.P;
    }

    public View e(int i) {
        if (this.P != null) {
            return this.P.findViewById(i);
        }
        return null;
    }

    public void a(View view) {
        this.P = view;
    }

    public void M() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.Q, 100);
        if (buildSTInfo != null) {
            buildSTInfo.scene = J();
            if (this.Q instanceof BaseActivity) {
                buildSTInfo.updateWithExternalPara(((BaseActivity) this.Q).p);
            }
            k.a(buildSTInfo);
        }
    }

    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ViewParent parent = this.P.getParent();
        if (parent != null && (parent instanceof ViewGroup)) {
            ((ViewGroup) parent).removeView(this.P);
        }
        return this.P;
    }

    public final void j() {
        super.j();
    }

    public int J() {
        return 2000;
    }

    public void c(boolean z) {
        super.c(z);
        if (z) {
            d(true);
        }
    }
}
