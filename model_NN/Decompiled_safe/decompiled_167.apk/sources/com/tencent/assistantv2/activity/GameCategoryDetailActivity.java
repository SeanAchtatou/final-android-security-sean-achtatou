package com.tencent.assistantv2.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.adapter.AppCategoryListAdapter;
import com.tencent.assistant.b.a;
import com.tencent.assistant.component.invalidater.TXRefreshGetMoreListViewScrollListener;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.manager.ao;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.component.categorydetail.FloatTagHeader;
import com.tencent.assistantv2.component.categorydetail.GameCategoryDetailListPage;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class GameCategoryDetailActivity extends BaseActivity {
    private String A = null;
    /* access modifiers changed from: private */
    public SecondNavigationTitleViewV5 B;
    private View.OnClickListener C = new bl(this);
    private View.OnClickListener D = new bm(this);
    private View.OnClickListener E = new bn(this);
    protected ViewPageScrollListener n = new ViewPageScrollListener();
    private ao t;
    /* access modifiers changed from: private */
    public GameCategoryDetailListPage u;
    /* access modifiers changed from: private */
    public SmartListAdapter v;
    private int w = 0;
    /* access modifiers changed from: private */
    public long x = -2;
    private AppCategoryListAdapter.CategoryType y = AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE;
    /* access modifiers changed from: private */
    public long z = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.act_tab_cat_detail);
            v();
            i();
            h();
        } catch (Throwable th) {
            cq.a().b();
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.u != null) {
            this.u.h();
        }
        if (this.B != null) {
            this.B.m();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.u != null) {
            this.u.i();
        }
        if (this.B != null) {
            this.B.l();
        }
    }

    /* access modifiers changed from: protected */
    public void i() {
        w();
        j();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.x = -1;
        super.onDestroy();
        if (this.u != null) {
            this.u.g();
        }
    }

    private void v() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.w = extras.getInt(a.X);
            this.x = extras.getLong("com.tencent.assistant.CATATORY_ID");
            this.A = extras.getString("activityTitleName");
            this.z = extras.getLong("com.tencent.assistant.TAG_ID", 0);
            if (extras.getInt("com.tencent.assistant.CATATORY_TYPE", AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE.ordinal()) == AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE.ordinal()) {
                this.y = AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE;
            } else {
                this.y = AppCategoryListAdapter.CategoryType.CATEGORYTYPEGAME;
            }
        }
    }

    private void w() {
        this.B = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.B.a(this);
        this.B.d(false);
        this.B.a(this.A, 4);
        this.B.i();
        this.B.d(this.E);
        this.B.d(this.w);
    }

    public void j() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.main_ll);
        this.t = new ao(this.x, 2);
        FloatTagHeader floatTagHeader = new FloatTagHeader(getApplicationContext());
        this.u = new GameCategoryDetailListPage(this, TXScrollViewBase.ScrollMode.PULL_FROM_END, this.t, floatTagHeader);
        this.v = new SmartListAdapter(this, this.u.a(), this.t.d()) {
            /* access modifiers changed from: protected */
            public SmartListAdapter.SmartListType a() {
                return SmartListAdapter.SmartListType.CategoryDetailPage;
            }

            /* access modifiers changed from: protected */
            public String a(int i) {
                return "06";
            }

            public boolean b() {
                return false;
            }
        };
        this.v.a(f(), this.x, this.z);
        TXRefreshGetMoreListViewScrollListener tXRefreshGetMoreListViewScrollListener = new TXRefreshGetMoreListViewScrollListener();
        this.u.a(tXRefreshGetMoreListViewScrollListener);
        this.v.a(tXRefreshGetMoreListViewScrollListener);
        linearLayout.addView(floatTagHeader);
        linearLayout.addView(this.u);
        this.u.b(this.C);
        this.u.a(this.D);
        this.u.a(this.v);
        this.u.a(this.z);
    }

    public int f() {
        if (this.y == AppCategoryListAdapter.CategoryType.CATEGORYTYPESOFTWARE) {
            return STConst.ST_PAGE_SOFTWARE_CATEGORY_DETAIL;
        }
        return 20060301;
    }

    public boolean g() {
        return false;
    }

    public void h() {
        STInfoV2 s = s();
        if (s != null) {
            s.updateContentId(STCommonInfo.ContentIdType.CATEGORY, this.x + "_" + this.z);
        }
        k.a(s);
    }
}
