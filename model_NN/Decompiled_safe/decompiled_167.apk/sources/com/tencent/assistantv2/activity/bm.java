package com.tencent.assistantv2.activity;

import android.view.View;
import com.tencent.assistant.protocol.jce.TagGroup;

/* compiled from: ProGuard */
class bm implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameCategoryDetailActivity f2784a;

    bm(GameCategoryDetailActivity gameCategoryDetailActivity) {
        this.f2784a = gameCategoryDetailActivity;
    }

    public void onClick(View view) {
        TagGroup tagGroup = (TagGroup) view.getTag();
        if (tagGroup != null && this.f2784a.B != null) {
            this.f2784a.B.k();
            this.f2784a.B.d(tagGroup.b());
        }
    }
}
