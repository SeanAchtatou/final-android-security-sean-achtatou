package com.tencent.tmsecurelite.optimize;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* compiled from: ProGuard */
public abstract class d extends Binder implements a {
    public d() {
        attachInterface(this, "com.tencent.tmsecurelite.IMemoryListener");
    }

    public static a a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.tencent.tmsecurelite.IMemoryListener");
        if (queryLocalInterface == null || !(queryLocalInterface instanceof a)) {
            return new c(iBinder);
        }
        return (a) queryLocalInterface;
    }

    /* access modifiers changed from: protected */
    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                a();
                parcel2.writeNoException();
                return true;
            case 2:
                a(parcel.readLong());
                parcel2.writeNoException();
                return true;
            default:
                return true;
        }
    }
}
