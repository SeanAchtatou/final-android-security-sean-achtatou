package com.tencent.tmsecurelite.b;

import android.os.IInterface;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.b;
import java.util.ArrayList;

/* compiled from: ProGuard */
public interface c extends IInterface {
    @Deprecated
    int a();

    @Deprecated
    ArrayList<DataEntity> a(int i);

    void a(int i, int i2, b bVar);

    @Deprecated
    void a(int i, int i2, boolean z);

    void a(int i, int i2, boolean z, b bVar);

    void a(int i, b bVar);

    void a(b bVar);

    void a(String str, String str2, int i, b bVar);

    @Deprecated
    boolean a(int i, int i2);

    @Deprecated
    boolean a(String str, String str2, int i);

    @Deprecated
    int b();

    @Deprecated
    void b(int i);

    @Deprecated
    void b(int i, int i2);

    void b(int i, int i2, b bVar);

    @Deprecated
    void b(int i, int i2, boolean z);

    void b(int i, int i2, boolean z, b bVar);

    void b(int i, b bVar);

    void b(b bVar);

    @Deprecated
    int c();

    @Deprecated
    ArrayList<DataEntity> c(int i);

    @Deprecated
    void c(int i, int i2);

    void c(int i, int i2, b bVar);

    void c(int i, b bVar);

    void c(b bVar);

    @Deprecated
    void d(int i);

    void d(int i, b bVar);

    void d(b bVar);
}
