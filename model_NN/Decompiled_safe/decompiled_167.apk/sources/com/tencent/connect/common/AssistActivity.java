package com.tencent.connect.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.assistant.AppConst;
import com.tencent.open.SocialConstants;
import com.tencent.open.a.n;
import com.tencent.open.b.d;
import com.tencent.open.utils.SystemUtils;
import com.tencent.open.utils.TemporaryStorage;
import com.tencent.open.utils.Util;
import com.tencent.tauth.AuthActivity;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class AssistActivity extends Activity {
    private static final String RESTART_FLAG = "RESTART_FLAG";
    private static final String TAG = "AssistActivity";
    public static boolean hackShareSend = false;
    public static boolean isQQMobileShare = false;
    private static BaseApi sApiObject;
    private BaseApi mAPiObject;

    public static Intent getAssistActivityIntent(Context context) {
        return new Intent(context, AssistActivity.class);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int intExtra;
        boolean z = false;
        super.onCreate(bundle);
        requestWindowFeature(1);
        n.b(TAG, "AssistActivity--onCreate--");
        if (sApiObject != null) {
            this.mAPiObject = sApiObject;
            sApiObject = null;
            Intent activityIntent = this.mAPiObject.getActivityIntent();
            if (activityIntent == null) {
                intExtra = 0;
            } else {
                intExtra = activityIntent.getIntExtra(Constants.KEY_REQUEST_CODE, 0);
            }
            Bundle bundleExtra = getIntent().getBundleExtra(SystemUtils.H5_SHARE_DATA);
            if (bundle != null) {
                z = bundle.getBoolean(RESTART_FLAG);
            }
            if (z) {
                return;
            }
            if (bundleExtra == null) {
                startActivityForResult(activityIntent, intExtra);
            } else {
                openBrowser(bundleExtra);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        n.b(TAG, "-->onStart");
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        n.b(TAG, "-->onResume");
        super.onResume();
        Intent intent = getIntent();
        if (!intent.getBooleanExtra(SystemUtils.IS_LOGIN, false) && !intent.getBooleanExtra(SystemUtils.IS_QQ_MOBILE_SHARE, false)) {
            if (!hackShareSend && !isFinishing()) {
                finish();
            }
            hackShareSend = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        n.b(TAG, "-->onPause");
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        n.b(TAG, "-->onStop");
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        n.b(TAG, "-->onDestroy");
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Object obj = TemporaryStorage.get(intent.getStringExtra(AuthActivity.ACTION_KEY));
        n.b(TAG, "AssistActivity--onNewIntent--" + (obj == null ? "mAPiObject = null" : "mAPiObject != null"));
        intent.putExtra(Constants.KEY_ACTION, SystemUtils.ACTION_SHARE);
        if (obj != null) {
            BaseApi.handleDataToListener(intent, (IUiListener) obj);
        } else {
            setResult(-1, intent);
        }
        if (!isFinishing()) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        n.b(TAG, "AssistActivity--onSaveInstanceState--");
        bundle.putBoolean(RESTART_FLAG, true);
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        n.b(TAG, "AssistActivity--onActivityResult--" + i2 + " data=" + intent);
        n.b(TAG, "--requestCode: " + i + " | resultCode: " + i2 + " | data: " + intent);
        super.onActivityResult(i, i2, intent);
        if (i != 0) {
            if (intent != null) {
                intent.putExtra(Constants.KEY_ACTION, SystemUtils.ACTION_LOGIN);
            }
            if (this.mAPiObject != null) {
                n.b(TAG, "AssistActivity--onActivityResult-- mAPiObject != null");
                this.mAPiObject.onActivityResult(this, i, i2, intent);
            } else {
                n.b(TAG, "AssistActivity--onActivityResult-- mAPiObject == null");
                setResultDataForLogin(this, intent);
            }
            finish();
        } else if (!isFinishing()) {
            finish();
        }
    }

    public static void setApiObject(BaseApi baseApi) {
        sApiObject = baseApi;
    }

    public static void setResultDataForLogin(Activity activity, Intent intent) {
        if (intent == null) {
            activity.setResult(Constants.RESULT_LOGIN, intent);
            return;
        }
        try {
            String stringExtra = intent.getStringExtra(Constants.KEY_RESPONSE);
            n.b(TAG, "AssistActivity--setResultDataForLogin-- " + stringExtra);
            if (!TextUtils.isEmpty(stringExtra)) {
                JSONObject jSONObject = new JSONObject(stringExtra);
                String optString = jSONObject.optString("openid");
                String optString2 = jSONObject.optString(Constants.PARAM_ACCESS_TOKEN);
                if (TextUtils.isEmpty(optString) || TextUtils.isEmpty(optString2)) {
                    activity.setResult(12345, intent);
                } else {
                    activity.setResult(Constants.RESULT_LOGIN, intent);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openBrowser(Bundle bundle) {
        String string = bundle.getString("viaShareType");
        String string2 = bundle.getString("callbackAction");
        String string3 = bundle.getString(SocialConstants.PARAM_URL);
        String string4 = bundle.getString(AppConst.KEY_OPENID);
        String string5 = bundle.getString("appId");
        String str = Constants.STR_EMPTY;
        String str2 = Constants.STR_EMPTY;
        if (SystemUtils.QQ_SHARE_CALLBACK_ACTION.equals(string2)) {
            str = Constants.VIA_SHARE_TO_QQ;
            str2 = Constants.VIA_REPORT_TYPE_SHARE_TO_QQ;
        } else if (SystemUtils.QZONE_SHARE_CALLBACK_ACTION.equals(string2)) {
            str = Constants.VIA_SHARE_TO_QZONE;
            str2 = Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE;
        }
        if (!Util.openBrowser(this, string3)) {
            IUiListener iUiListener = (IUiListener) TemporaryStorage.get(string2);
            if (iUiListener != null) {
                iUiListener.onError(new UiError(-6, Constants.MSG_OPEN_BROWSER_ERROR, null));
            }
            d.a().a(string4, string5, str, str2, "3", "1", string, "0", "2", "0");
            finish();
        } else {
            d.a().a(string4, string5, str, str2, "3", "0", string, "0", "2", "0");
        }
        getIntent().removeExtra("shareH5");
    }
}
