package com.tencent.smtt.sdk;

public class CookieManager {
    private static CookieManager mInstance;
    private android.webkit.CookieManager sysCookieManager = android.webkit.CookieManager.getInstance();

    private CookieManager() {
    }

    public static synchronized CookieManager getInstance() {
        CookieManager cookieManager;
        synchronized (CookieManager.class) {
            if (mInstance == null) {
                mInstance = new CookieManager();
            }
            cookieManager = mInstance;
        }
        return cookieManager;
    }

    public void removeSessionCookie() {
        SDKEngine sdkEngine = SDKEngine.getInstance(false);
        if (sdkEngine == null || !sdkEngine.isX5Core()) {
            this.sysCookieManager.removeSessionCookie();
        } else {
            sdkEngine.wizard().cookieManager_removeSessionCookie();
        }
    }

    public void removeAllCookie() {
        SDKEngine sdkEngine = SDKEngine.getInstance(false);
        if (sdkEngine == null || !sdkEngine.isX5Core()) {
            this.sysCookieManager.removeAllCookie();
        } else {
            sdkEngine.wizard().cookieManager_removeAllCookie();
        }
    }

    public void removeExpiredCookie() {
        SDKEngine sdkEngine = SDKEngine.getInstance(false);
        if (sdkEngine == null || !sdkEngine.isX5Core()) {
            this.sysCookieManager.removeExpiredCookie();
        } else {
            sdkEngine.wizard().cookieManager_removeExpiredCookie();
        }
    }

    public synchronized void setAcceptCookie(boolean accept) {
        SDKEngine sdkEngine = SDKEngine.getInstance(false);
        if (sdkEngine == null || !sdkEngine.isX5Core()) {
            this.sysCookieManager.setAcceptCookie(accept);
        } else {
            sdkEngine.wizard().cookieManager_setAcceptCookie(accept);
        }
    }

    public void setCookie(String url, String value) {
        SDKEngine sdkEngine = SDKEngine.getInstance(false);
        if (sdkEngine == null || !sdkEngine.isX5Core()) {
            this.sysCookieManager.setCookie(url, value);
        } else {
            sdkEngine.wizard().cookieManager_setCookie(url, value);
        }
    }

    public boolean hasCookies() {
        SDKEngine sdkEngine = SDKEngine.getInstance(false);
        if (sdkEngine == null || !sdkEngine.isX5Core()) {
            return this.sysCookieManager.hasCookies();
        }
        return sdkEngine.wizard().cookieManager_hasCookies();
    }

    public boolean acceptCookie() {
        SDKEngine sdkEngine = SDKEngine.getInstance(false);
        if (sdkEngine == null || !sdkEngine.isX5Core()) {
            return this.sysCookieManager.acceptCookie();
        }
        return sdkEngine.wizard().cookieManager_acceptCookie();
    }

    public String getCookie(String url) {
        SDKEngine sdkEngine = SDKEngine.getInstance(false);
        if (sdkEngine == null || !sdkEngine.isX5Core()) {
            return this.sysCookieManager.getCookie(url);
        }
        return sdkEngine.wizard().getCookie(url);
    }
}
