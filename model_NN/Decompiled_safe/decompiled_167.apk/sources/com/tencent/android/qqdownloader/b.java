package com.tencent.android.qqdownloader;

/* compiled from: ProGuard */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    public static final int[] f378a = {R.attr.openEyeSrc, R.attr.closeEyeSrc, R.attr.bodyBg, R.attr.blinkOffset, R.attr.blinkDuration, R.attr.blinkMode};
    public static final int[] b = {16842948, 16842964, R.attr.centered, R.attr.strokeWidth, R.attr.fillColor, R.attr.pageColor, R.attr.radius, R.attr.snap, R.attr.strokeColor};
    public static final int[] c = {16842948};
    public static final int[] d = {R.attr.isvertical};
    public static final int[] e = {R.attr.maxLines, R.attr.needEllipsized};
    public static final int[] f = {R.attr.orientation, R.attr.animationBitmapFormat};
    public static final int[] g = {R.attr.lineCount, R.attr.maxLineCount};
    public static final int[] h = {R.attr.maxTextSize, R.attr.minTextSize};
    public static final int[] i = {R.attr.AppInfoVisiable, R.attr.infoType};
    public static final int[] j = {R.attr.imgDrawable, R.attr.emptyText};
    public static final int[] k = {R.attr.lineColor, R.attr.lineWidth, R.attr.lineOffset};
    public static final int[] l = {R.attr.roundColor, R.attr.roundProgressColor, R.attr.roundWidth, R.attr.textColor, R.attr.textSize, R.attr.max, R.attr.showProgressText, R.attr.style};
    public static final int[] m = {R.attr.ImageUrl, R.attr.ImageType, R.attr.ImageShape};
    public static final int[] n = {R.attr.ScrollMode, R.attr.ScrollDirection};
    public static final int[] o = {R.attr.waveViewStyle};
    public static final int[] p = {16842901, 16842904, 16842964, R.attr.selectedColor, R.attr.clipPadding, R.attr.footerColor, R.attr.footerLineHeight, R.attr.footerIndicatorStyle, R.attr.footerIndicatorHeight, R.attr.footerIndicatorUnderlinePadding, R.attr.footerPadding, R.attr.linePosition, R.attr.selectedBold, R.attr.titlePadding, R.attr.topPadding};
    public static final int[] q = {16842964, R.attr.selectedColor, R.attr.fades, R.attr.fadeDelay, R.attr.fadeLength};
    public static final int[] r = {R.attr.vpiCirclePageIndicatorStyle, R.attr.vpiIconPageIndicatorStyle, R.attr.vpiLinePageIndicatorStyle, R.attr.vpiTitlePageIndicatorStyle, R.attr.vpiTabPageIndicatorStyle, R.attr.vpiUnderlinePageIndicatorStyle};
    public static final int[] s = {R.attr.above_wave_color, R.attr.blow_wave_color, R.attr.progress, R.attr.wave_length, R.attr.wave_height, R.attr.wave_hz};
}
