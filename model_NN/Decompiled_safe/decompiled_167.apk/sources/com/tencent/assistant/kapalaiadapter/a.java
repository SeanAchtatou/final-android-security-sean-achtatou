package com.tencent.assistant.kapalaiadapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import com.tencent.assistant.kapalaiadapter.a.b;
import com.tencent.assistant.kapalaiadapter.a.c;
import com.tencent.assistant.kapalaiadapter.a.d;
import com.tencent.assistant.kapalaiadapter.a.e;
import com.tencent.assistant.kapalaiadapter.a.f;
import com.tencent.assistant.kapalaiadapter.a.g;
import com.tencent.assistant.kapalaiadapter.a.h;
import com.tencent.assistant.kapalaiadapter.a.i;
import com.tencent.assistant.kapalaiadapter.a.j;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static j[] f1378a = {new com.tencent.assistant.kapalaiadapter.a.a(), new b(), new c(), new d(), new e(), new f(), new g(), new h(), new i()};

    private a() {
    }

    public static a a() {
        return c.f1388a;
    }

    public String b() {
        String lowerCase = Build.MODEL.toLowerCase();
        if (lowerCase.equals("gt-i9300")) {
            return "content://com.sec.android.app.launcher.settings/favorites?notify=true";
        }
        if (lowerCase.equals("mi 1s")) {
            return "content://com.miui.home.launcher.settings/favorites?notify=true";
        }
        return "content://com.android.launcher.settings/favorites?notify=true";
    }

    public String[] a(Context context) {
        String[] strArr = new String[2];
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            strArr[i] = f1378a[d.e[i]].b(d.h[i], context);
        }
        return strArr;
    }

    public String[] b(Context context) {
        String[] strArr = new String[2];
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            for (j b : f1378a) {
                strArr[i] = b.b(d.h[i], context);
                if (strArr[i] != null && strArr[i].length() != 0) {
                    break;
                }
            }
        }
        return strArr;
    }

    public String[] c(Context context) {
        String[] strArr = new String[2];
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            strArr[i] = f1378a[d.g[i]].c(d.i[i], context);
        }
        return strArr;
    }

    public String[] d(Context context) {
        int i;
        String[] strArr = new String[2];
        int length = d.j.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length && i3 < 2) {
            int length2 = f1378a.length;
            int i4 = 0;
            while (true) {
                if (i4 < length2) {
                    String c = f1378a[i4].c(d.j[i2], context);
                    if (c != null && c.length() != 0 && !c.equals(strArr[0])) {
                        i = i3 + 1;
                        strArr[i3] = c;
                        break;
                    }
                    i4++;
                } else {
                    i = i3;
                    break;
                }
            }
            i2++;
            i3 = i;
        }
        return strArr;
    }

    public String[] e(Context context) {
        String[] strArr = new String[2];
        String str = Constants.STR_EMPTY;
        String str2 = Constants.STR_EMPTY;
        String lowerCase = Build.MANUFACTURER.toLowerCase();
        String lowerCase2 = Build.MODEL.toLowerCase();
        int i = Build.VERSION.SDK_INT;
        if (lowerCase2.equals("galaxy nexus") && i >= 14) {
            Account[] accounts = AccountManager.get(context).getAccounts();
            if (accounts.length > 0) {
                strArr[0] = accounts[0].name;
                strArr[1] = accounts[0].type;
                return strArr;
            }
        }
        if (lowerCase.indexOf("samsung") >= 0 || lowerCase.indexOf("samsng") >= 0) {
            str = "vnd.sec.contact.phone";
            str2 = "vnd.sec.contact.phone";
        } else if (lowerCase.indexOf("htc") >= 0) {
            str = "pcsc";
            str2 = "com.htc.android.pcsc";
        } else if (lowerCase.indexOf("sony ericsson") >= 0) {
            str = "Phone contacts";
            str2 = "com.sonyericsson.localcontacts";
        } else if (lowerCase.indexOf("zte") >= 0) {
            str = "local@ztespecial_local.com";
            str2 = "ztespecial_local.com";
        } else if (lowerCase.indexOf("moto") >= 0) {
            str = "phone-contacts";
            str2 = "com.contacts.phone";
        }
        strArr[0] = str;
        strArr[1] = str2;
        return strArr;
    }

    public Uri a(long j) {
        return ContentUris.withAppendedId(ContactsContract.Groups.CONTENT_URI, j).buildUpon().appendQueryParameter("caller_is_syncadapter", "true").build();
    }
}
