package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.z;
import com.tencent.assistant.protocol.jce.GetRecommendAppListResponse;

/* compiled from: ProGuard */
class ey implements CallbackHelper.Caller<z> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1810a;
    final /* synthetic */ GetRecommendAppListResponse b;
    final /* synthetic */ ex c;

    ey(ex exVar, int i, GetRecommendAppListResponse getRecommendAppListResponse) {
        this.c = exVar;
        this.f1810a = i;
        this.b = getRecommendAppListResponse;
    }

    /* renamed from: a */
    public void call(z zVar) {
        zVar.a(this.f1810a, 0, this.b.a(), this.b);
    }
}
