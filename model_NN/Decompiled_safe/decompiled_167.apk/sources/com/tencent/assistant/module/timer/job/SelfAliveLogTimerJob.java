package com.tencent.assistant.module.timer.job;

import android.os.Process;
import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;
import com.tencent.assistant.module.timer.SimpleBaseScheduleJob;
import com.tencent.assistant.st.h;
import com.tencent.beacon.event.a;
import com.tencent.connect.common.Constants;
import java.util.HashMap;

/* compiled from: ProGuard */
public class SelfAliveLogTimerJob extends SimpleBaseScheduleJob {
    public int h() {
        return m.a().a("self_alive_log_interval", 300);
    }

    public void d_() {
        int myPid = Process.myPid();
        int[] e = e();
        if (e.length != 3) {
            m.a().b("alive_log", Constants.STR_EMPTY);
            return;
        }
        int i = e[0];
        int i2 = e[1];
        int i3 = e[2];
        if (i <= 0) {
            a(myPid);
        } else if (i == myPid) {
            try {
                int a2 = m.a().a("self_alive_log_trigger", 3600);
                int h = i3 + h();
                if (h >= a2) {
                    a(i, i2, h);
                    a(myPid);
                    return;
                }
                b(h);
            } catch (Exception e2) {
                m.a().b("alive_log", Constants.STR_EMPTY);
            }
        } else {
            a(i, i2, i3);
            a(myPid);
        }
    }

    private int[] e() {
        int[] iArr = new int[3];
        String a2 = m.a().a("alive_log", Constants.STR_EMPTY);
        if (!TextUtils.isEmpty(a2)) {
            String[] split = a2.split("_");
            if (split.length == 3) {
                iArr[0] = Integer.valueOf(split[0]).intValue();
                iArr[1] = Integer.valueOf(split[1]).intValue();
                iArr[2] = Integer.valueOf(split[2]).intValue();
            }
        }
        return iArr;
    }

    private void a(int i) {
        m.a().b("alive_log", i + "_" + ((int) (h.a() / 1000)) + "_0");
    }

    private void b(int i) {
        int[] e = e();
        if (e.length != 3 || e[0] <= 0) {
            m.a().b("alive_log", Constants.STR_EMPTY);
        } else {
            m.a().b("alive_log", e[0] + "_" + e[1] + "_" + i);
        }
    }

    private void a(int i, int i2, int i3) {
        if (i3 > 0) {
            HashMap hashMap = new HashMap();
            hashMap.put("B1", String.valueOf(i));
            hashMap.put("B2", String.valueOf(i2));
            hashMap.put("B3", String.valueOf(i3));
            hashMap.put("B4", Global.getPhoneGuidAndGen());
            a.a("SelfAliveLog", true, 0, 0, hashMap, true);
        }
    }
}
