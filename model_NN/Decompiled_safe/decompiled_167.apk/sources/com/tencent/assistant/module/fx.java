package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;

/* compiled from: ProGuard */
class fx implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ fw f1834a;

    fx(fw fwVar) {
        this.f1834a = fwVar;
    }

    public void run() {
        AppUpdateInfo appUpdateInfo;
        LocalApkInfo localApkInfo;
        JceStruct a2;
        if (this.f1834a.c.get() < this.f1834a.b.size() && (appUpdateInfo = (AppUpdateInfo) this.f1834a.b.get(this.f1834a.c.getAndIncrement())) != null && (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(appUpdateInfo.f2006a)) != null && (a2 = this.f1834a.a(localApkInfo, appUpdateInfo)) != null) {
            this.f1834a.send(a2);
        }
    }
}
