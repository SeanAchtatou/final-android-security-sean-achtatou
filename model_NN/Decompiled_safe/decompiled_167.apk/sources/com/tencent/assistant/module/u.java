package com.tencent.assistant.module;

import android.net.Uri;
import android.text.Html;
import android.text.TextUtils;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.model.j;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.module.update.s;
import com.tencent.assistant.module.wisedownload.r;
import com.tencent.assistant.module.wisedownload.t;
import com.tencent.assistant.net.c;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.protocol.jce.ApkDownUrl;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.protocol.jce.BackupApp;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.protocol.jce.RecommendAppInfo;
import com.tencent.assistant.protocol.jce.SimpleAppInfo;
import com.tencent.assistant.protocol.jce.SnapshotsPic;
import com.tencent.assistant.sdk.param.entity.BatchDownloadParam;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.g;
import com.tencent.assistant.utils.h;
import com.tencent.connect.common.Constants;
import com.tencent.securemodule.impl.AppInfo;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

/* compiled from: ProGuard */
public class u {

    /* renamed from: a  reason: collision with root package name */
    public static ArrayList<String> f1875a = new ArrayList<>();
    static Random b = new Random(System.currentTimeMillis());

    public static boolean a(String str) {
        return !TextUtils.isEmpty(str) && f1875a != null && f1875a.contains(str);
    }

    public static List<AppUpdateInfo> a(List<AppUpdateInfo> list) {
        if (list == null || list.isEmpty()) {
            return list;
        }
        ArrayList arrayList = new ArrayList();
        s sVar = new s();
        Set<s> e = k.b().e();
        for (AppUpdateInfo next : list) {
            sVar.a(next.f2006a, next.n, next.d, h.b(next.q));
            if (!e.contains(sVar)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public static ArrayList<CardItem> a(ArrayList<CardItem> arrayList) {
        if (arrayList == null || arrayList.size() == 0) {
            return arrayList;
        }
        ArrayList<CardItem> arrayList2 = new ArrayList<>();
        Iterator<CardItem> it = arrayList.iterator();
        while (it.hasNext()) {
            CardItem next = it.next();
            List<DownloadInfo> e = DownloadProxy.a().e(next.f.f);
            if ((e == null || e.size() == 0) && ApkResourceManager.getInstance().getLocalApkInfo(next.f.f) == null) {
                arrayList2.add(next);
            }
        }
        return arrayList2;
    }

    public static List<RecommendAppInfo> b(List<RecommendAppInfo> list) {
        if (list == null || list.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (RecommendAppInfo next : list) {
            List<DownloadInfo> e = DownloadProxy.a().e(next.b);
            if ((e == null || e.size() == 0) && ApkResourceManager.getInstance().getLocalApkInfo(next.b) == null) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public static List<SimpleAppInfo> c(List<SimpleAppInfo> list) {
        if (list == null || list.size() == 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (SimpleAppInfo next : list) {
            List<DownloadInfo> e = DownloadProxy.a().e(next.f);
            if ((e == null || e.size() == 0) && ApkResourceManager.getInstance().getLocalApkInfo(next.f) == null) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public static void d(List<AutoDownloadInfo> list) {
        if (list != null && list.size() != 0) {
            for (AutoDownloadInfo next : list) {
                if (e.a(next.f2010a, next.d)) {
                    list.remove(next);
                }
            }
        }
    }

    public static ArrayList<SimpleAppModel> b(ArrayList<CardItem> arrayList) {
        return a(arrayList, (w) null, 0);
    }

    public static ArrayList<SimpleAppModel> a(ArrayList<CardItem> arrayList, w wVar, int i) {
        ArrayList<SimpleAppModel> arrayList2 = new ArrayList<>();
        if (arrayList == null || arrayList.size() == 0) {
            return arrayList2;
        }
        Iterator<CardItem> it = arrayList.iterator();
        while (it.hasNext()) {
            SimpleAppModel a2 = a(it.next());
            if (a2 != null) {
                a(a2);
                if (wVar == null || !wVar.a(a2)) {
                    arrayList2.add(a2);
                    if (i > 0 && arrayList2.size() >= i) {
                        break;
                    }
                }
            }
        }
        return arrayList2;
    }

    public static ArrayList<SimpleAppModel> c(ArrayList<BackupApp> arrayList) {
        ArrayList<SimpleAppModel> arrayList2 = new ArrayList<>();
        if (arrayList == null || arrayList.size() == 0) {
            return arrayList2;
        }
        Iterator<BackupApp> it = arrayList.iterator();
        while (it.hasNext()) {
            SimpleAppModel a2 = a(it.next());
            if (a2 != null) {
                arrayList2.add(a2);
            }
        }
        return arrayList2;
    }

    public static SimpleAppModel a(BackupApp backupApp) {
        SimpleAppModel b2 = b(backupApp);
        if (b2 != null) {
            a(b2);
        }
        return b2;
    }

    public static SimpleAppModel b(BackupApp backupApp) {
        if (backupApp == null) {
            return null;
        }
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.f1634a = backupApp.a();
        simpleAppModel.b = backupApp.b();
        simpleAppModel.d = backupApp.c();
        simpleAppModel.c = backupApp.d();
        simpleAppModel.j = a((byte) 1, backupApp.e());
        simpleAppModel.e = backupApp.f().a();
        simpleAppModel.m = backupApp.g();
        simpleAppModel.g = backupApp.h();
        simpleAppModel.f = backupApp.i();
        simpleAppModel.l = backupApp.j();
        simpleAppModel.k = backupApp.k();
        return simpleAppModel;
    }

    public static void a(SimpleAppModel simpleAppModel) {
        LocalApkInfo localApkInfo;
        if (simpleAppModel != null && (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c)) != null) {
            a(ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c), simpleAppModel);
            if (k.a()) {
                AppUpdateInfo a2 = k.b().a(simpleAppModel.c);
                boolean z = a2 != null && simpleAppModel.g > localApkInfo.mVersionCode && a2.r == simpleAppModel.b;
                simpleAppModel.ab = z;
                if (z) {
                    simpleAppModel.m = a2.c;
                    simpleAppModel.l = a2.h;
                    simpleAppModel.i = a2.i;
                    simpleAppModel.j = a((byte) 1, a2.s);
                    simpleAppModel.k = a2.j;
                    simpleAppModel.t = a2.l;
                    simpleAppModel.u = a((byte) 2, a2.s);
                    simpleAppModel.v = a2.m;
                    simpleAppModel.w = a2.k;
                    simpleAppModel.z = a2.E;
                    simpleAppModel.A = a2.C;
                    simpleAppModel.ag = a2.I;
                    XLog.d("jimluo", "updateInfo AppName " + a2.b + " overwriteChannelid is:" + ((int) a2.I));
                }
            }
        }
    }

    public static SimpleAppModel a(AppSimpleDetail appSimpleDetail) {
        if (appSimpleDetail == null) {
            return null;
        }
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.f1634a = appSimpleDetail.f2003a;
        simpleAppModel.b = appSimpleDetail.b;
        simpleAppModel.c = appSimpleDetail.e;
        simpleAppModel.g = appSimpleDetail.g;
        simpleAppModel.f = appSimpleDetail.f;
        simpleAppModel.d = appSimpleDetail.c;
        simpleAppModel.e = appSimpleDetail.d;
        simpleAppModel.ac = appSimpleDetail.n;
        simpleAppModel.B = appSimpleDetail.o;
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c);
        simpleAppModel.m = appSimpleDetail.h;
        simpleAppModel.l = appSimpleDetail.i;
        simpleAppModel.k = appSimpleDetail.j;
        simpleAppModel.j = a((byte) 1, appSimpleDetail.k);
        if (localApkInfo != null) {
            simpleAppModel.v = appSimpleDetail.m;
            simpleAppModel.w = appSimpleDetail.l;
            simpleAppModel.u = a((byte) 2, appSimpleDetail.k);
            simpleAppModel.z = appSimpleDetail.q;
            simpleAppModel.A = appSimpleDetail.p;
        }
        if (!TextUtils.isEmpty(simpleAppModel.l) && !TextUtils.isEmpty(simpleAppModel.w)) {
            simpleAppModel.a(ApkResourceManager.getInstance().getLocalApkInfo(appSimpleDetail.e));
        } else if (localApkInfo != null) {
            a(localApkInfo, simpleAppModel);
            AppUpdateInfo a2 = k.b().a(simpleAppModel.c);
            simpleAppModel.ab = a2 != null;
            if (simpleAppModel.ab && simpleAppModel.g > localApkInfo.mVersionCode && a2.r == simpleAppModel.b) {
                simpleAppModel.m = a2.c;
                simpleAppModel.l = a2.h;
                simpleAppModel.j = a((byte) 1, a2.s);
                simpleAppModel.k = a2.j;
                simpleAppModel.u = a((byte) 2, a2.s);
                simpleAppModel.v = a2.m;
                simpleAppModel.w = a2.k;
                simpleAppModel.z = a2.E;
                simpleAppModel.A = a2.C;
                simpleAppModel.ag = a2.I;
            }
        }
        return simpleAppModel;
    }

    public static SimpleAppModel a(DownloadInfo downloadInfo) {
        if (downloadInfo == null) {
            return null;
        }
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.f1634a = downloadInfo.appId;
        simpleAppModel.b = downloadInfo.apkId;
        simpleAppModel.c = downloadInfo.packageName;
        simpleAppModel.g = downloadInfo.versionCode;
        simpleAppModel.f = downloadInfo.versionName;
        simpleAppModel.d = downloadInfo.name;
        simpleAppModel.e = downloadInfo.iconUrl;
        simpleAppModel.m = downloadInfo.signatrue;
        simpleAppModel.l = downloadInfo.fileMd5;
        simpleAppModel.k = downloadInfo.fileSize;
        simpleAppModel.j = (ArrayList) downloadInfo.apkUrlList;
        simpleAppModel.v = downloadInfo.sllFileSize;
        simpleAppModel.w = downloadInfo.sllFileMd5;
        simpleAppModel.u = (ArrayList) downloadInfo.sllApkUrlList;
        simpleAppModel.ac = downloadInfo.channelId;
        simpleAppModel.Q = downloadInfo.actionFlag;
        simpleAppModel.ad = downloadInfo.grayVersionCode;
        simpleAppModel.z = downloadInfo.sllLocalManifestMd5;
        simpleAppModel.A = downloadInfo.sllLocalVersionCode;
        return simpleAppModel;
    }

    public static SimpleAppModel a(AutoDownloadInfo autoDownloadInfo) {
        if (autoDownloadInfo == null || TextUtils.isEmpty(autoDownloadInfo.f2010a)) {
            return null;
        }
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(autoDownloadInfo.f2010a);
        if (localApkInfo != null) {
            boolean z = autoDownloadInfo.d > localApkInfo.mVersionCode;
            simpleAppModel.ab = z;
            if (z) {
                a(localApkInfo, simpleAppModel);
            }
        }
        simpleAppModel.m = autoDownloadInfo.c;
        simpleAppModel.f1634a = autoDownloadInfo.j;
        simpleAppModel.b = autoDownloadInfo.k;
        simpleAppModel.d = autoDownloadInfo.b;
        simpleAppModel.c = autoDownloadInfo.f2010a;
        simpleAppModel.g = autoDownloadInfo.d;
        simpleAppModel.f = autoDownloadInfo.i;
        simpleAppModel.y = autoDownloadInfo.m;
        simpleAppModel.k = autoDownloadInfo.f;
        simpleAppModel.l = autoDownloadInfo.e;
        simpleAppModel.j = a((byte) 1, autoDownloadInfo.l);
        simpleAppModel.v = autoDownloadInfo.h;
        simpleAppModel.w = autoDownloadInfo.g;
        simpleAppModel.u = a((byte) 2, autoDownloadInfo.l);
        simpleAppModel.z = autoDownloadInfo.u;
        simpleAppModel.A = autoDownloadInfo.t;
        if (autoDownloadInfo.n != null) {
            simpleAppModel.e = autoDownloadInfo.n.f2252a;
        }
        return simpleAppModel;
    }

    public static SimpleAppModel b(AutoDownloadInfo autoDownloadInfo) {
        if (autoDownloadInfo == null || TextUtils.isEmpty(autoDownloadInfo.f2010a)) {
            return null;
        }
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(autoDownloadInfo.f2010a);
        if (localApkInfo != null && autoDownloadInfo.d <= localApkInfo.mVersionCode) {
            return null;
        }
        if (localApkInfo != null) {
            boolean z = autoDownloadInfo.d > localApkInfo.mVersionCode;
            simpleAppModel.ab = z;
            if (z) {
                a(localApkInfo, simpleAppModel);
            }
        }
        simpleAppModel.m = autoDownloadInfo.c;
        simpleAppModel.f1634a = autoDownloadInfo.j;
        simpleAppModel.b = autoDownloadInfo.k;
        simpleAppModel.d = autoDownloadInfo.b;
        simpleAppModel.c = autoDownloadInfo.f2010a;
        simpleAppModel.g = autoDownloadInfo.d;
        simpleAppModel.f = autoDownloadInfo.i;
        simpleAppModel.y = autoDownloadInfo.m;
        simpleAppModel.k = autoDownloadInfo.f;
        simpleAppModel.l = autoDownloadInfo.e;
        simpleAppModel.j = a((byte) 1, autoDownloadInfo.l);
        simpleAppModel.v = autoDownloadInfo.h;
        simpleAppModel.w = autoDownloadInfo.g;
        simpleAppModel.u = a((byte) 2, autoDownloadInfo.l);
        simpleAppModel.z = autoDownloadInfo.u;
        simpleAppModel.A = autoDownloadInfo.t;
        if (autoDownloadInfo.n != null) {
            simpleAppModel.e = autoDownloadInfo.n.f2252a;
        }
        return simpleAppModel;
    }

    public static ArrayList<SimpleAppModel> e(List<AutoDownloadInfo> list) {
        SimpleAppModel b2;
        ArrayList<SimpleAppModel> arrayList = new ArrayList<>();
        if (list == null || list.size() == 0) {
            return arrayList;
        }
        ArrayList<AutoDownloadInfo> arrayList2 = new ArrayList<>();
        arrayList2.addAll(list);
        for (AutoDownloadInfo autoDownloadInfo : arrayList2) {
            if (g.a(autoDownloadInfo) && (b2 = b(autoDownloadInfo)) != null) {
                arrayList.add(b2);
            }
        }
        return arrayList;
    }

    public static ArrayList<SimpleAppModel> f(List<AppInfo> list) {
        LocalApkInfo localApkInfo;
        ArrayList<SimpleAppModel> arrayList = new ArrayList<>();
        if (list == null || list.size() == 0) {
            return arrayList;
        }
        for (AppInfo next : list) {
            if (!(next == null || (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(next.pkgName)) == null)) {
                SimpleAppModel simpleAppModel = new SimpleAppModel();
                simpleAppModel.c = localApkInfo.mPackageName;
                simpleAppModel.d = localApkInfo.mAppName;
                simpleAppModel.f = localApkInfo.mVersionName;
                simpleAppModel.g = localApkInfo.mVersionCode;
                arrayList.add(c(simpleAppModel));
            }
        }
        return arrayList;
    }

    public static SimpleAppModel b(String str) {
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(str);
        if (localApkInfo == null) {
            return null;
        }
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.c = localApkInfo.mPackageName;
        c(simpleAppModel);
        return simpleAppModel;
    }

    public static SimpleAppModel b(SimpleAppModel simpleAppModel) {
        AppUpdateInfo a2;
        if (!(simpleAppModel == null || ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c) == null || (a2 = k.b().a(simpleAppModel.c)) == null || ((simpleAppModel.g != -1 && simpleAppModel.g <= simpleAppModel.D) || (simpleAppModel.b != -99 && simpleAppModel.b != a2.r)))) {
            a(a2, simpleAppModel);
        }
        return simpleAppModel;
    }

    public static SimpleAppModel c(SimpleAppModel simpleAppModel) {
        LocalApkInfo localApkInfo;
        if (!(simpleAppModel == null || (localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c)) == null)) {
            a(localApkInfo, simpleAppModel);
            AppUpdateInfo a2 = k.b().a(simpleAppModel.c);
            if (a2 != null && ((simpleAppModel.g == -1 || simpleAppModel.g > simpleAppModel.D) && (simpleAppModel.b == -99 || simpleAppModel.b == a2.r))) {
                a(a2, simpleAppModel);
            }
        }
        return simpleAppModel;
    }

    public static List<SimpleAppModel> g(List<SimpleAppModel> list) {
        if (list != null && !list.isEmpty()) {
            for (SimpleAppModel c : list) {
                c(c);
            }
        }
        return list;
    }

    public static SimpleAppModel a(CardItem cardItem) {
        if (cardItem == null) {
            return null;
        }
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.V = cardItem.b;
        if (!TextUtils.isEmpty(cardItem.e)) {
            try {
                simpleAppModel.X = Html.fromHtml(cardItem.e);
            } catch (Exception e) {
                simpleAppModel.X = cardItem.e;
                e.printStackTrace();
            }
        }
        simpleAppModel.Y = cardItem.c;
        simpleAppModel.Z = cardItem.d;
        simpleAppModel.aa = cardItem.g;
        simpleAppModel.y = cardItem.h;
        simpleAppModel.ae = cardItem.j;
        simpleAppModel.ah = cardItem.m;
        simpleAppModel.ai = cardItem.n;
        simpleAppModel.aj = cardItem.o;
        simpleAppModel.S = cardItem.t;
        simpleAppModel.ay = cardItem.A;
        simpleAppModel.az = cardItem.B;
        if (cardItem.k != null && cardItem.k.size() > 0) {
            if (simpleAppModel.an == null) {
                simpleAppModel.an = new ArrayList<>();
            }
            Iterator<SnapshotsPic> it = cardItem.k.iterator();
            while (it.hasNext()) {
                simpleAppModel.an.add(it.next().b);
            }
            if (simpleAppModel.ao == null) {
                simpleAppModel.ao = new ArrayList<>();
            }
            Iterator<SnapshotsPic> it2 = cardItem.k.iterator();
            while (it2.hasNext()) {
                simpleAppModel.ao.add(it2.next().c);
            }
            if (simpleAppModel.ap == null) {
                simpleAppModel.ap = new ArrayList<>();
            }
            Iterator<SnapshotsPic> it3 = cardItem.k.iterator();
            while (it3.hasNext()) {
                simpleAppModel.ap.add(it3.next().f2343a);
            }
        }
        if (cardItem.p != null) {
            com.tencent.assistant.model.h hVar = new com.tencent.assistant.model.h();
            hVar.f1665a = cardItem.p.f2276a;
            hVar.c = cardItem.p.c;
            hVar.d = cardItem.p.d;
            if (!TextUtils.isEmpty(cardItem.p.b)) {
                hVar.b = Html.fromHtml(cardItem.p.b);
            }
            simpleAppModel.aq = hVar;
        }
        if (cardItem.r != null) {
            simpleAppModel.av = new com.tencent.assistant.model.g(cardItem.r);
        }
        simpleAppModel.al = cardItem.s;
        simpleAppModel.am = cardItem.l;
        simpleAppModel.au = cardItem.q;
        simpleAppModel.aw = cardItem.u;
        simpleAppModel.aA = cardItem.w;
        SimpleAppInfo simpleAppInfo = cardItem.f;
        if (simpleAppInfo != null) {
            simpleAppModel.f1634a = simpleAppInfo.f2310a;
            simpleAppModel.b = simpleAppInfo.p;
            simpleAppModel.d = simpleAppInfo.b;
            simpleAppModel.c = simpleAppInfo.f;
            simpleAppModel.g = simpleAppInfo.h;
            simpleAppModel.f = simpleAppInfo.g;
            simpleAppModel.h = simpleAppInfo.o;
            simpleAppModel.e = simpleAppInfo.c;
            simpleAppModel.k = simpleAppInfo.d;
            simpleAppModel.i = simpleAppInfo.e;
            simpleAppModel.j = a((byte) 1, simpleAppInfo.q);
            simpleAppModel.p = simpleAppInfo.i;
            simpleAppModel.ax = simpleAppInfo.A;
            simpleAppModel.r = simpleAppInfo.j;
            if (simpleAppInfo.k != null) {
                simpleAppModel.q = simpleAppInfo.k.b;
            }
            if (simpleAppInfo.l != null) {
                simpleAppModel.s = simpleAppInfo.l.b;
            }
            simpleAppModel.B = simpleAppInfo.m;
            simpleAppModel.P = simpleAppInfo.r;
            simpleAppModel.ac = simpleAppInfo.s;
            simpleAppModel.W = simpleAppInfo.n;
            simpleAppModel.ad = simpleAppInfo.t;
            simpleAppModel.R = (long) simpleAppInfo.v;
            simpleAppModel.aB = simpleAppInfo.B;
            simpleAppModel.aF = simpleAppInfo.C;
            simpleAppModel.aG = simpleAppInfo.D;
            simpleAppModel.aH = simpleAppInfo.E;
            simpleAppModel.aI = simpleAppInfo.F;
            simpleAppModel.aJ = simpleAppInfo.G;
            simpleAppModel.aK = simpleAppInfo.H;
            simpleAppModel.aL = simpleAppInfo.I;
        }
        simpleAppModel.aC = cardItem.D;
        if (!TextUtils.isEmpty(cardItem.D)) {
            Uri parse = Uri.parse(cardItem.D);
            if (a(parse, simpleAppModel.g)) {
                String queryParameter = parse.getQueryParameter("actionurl");
                if (!TextUtils.isEmpty(queryParameter)) {
                    try {
                        simpleAppModel.aD = URLDecoder.decode(queryParameter, "UTF-8");
                    } catch (UnsupportedEncodingException e2) {
                    }
                }
                simpleAppModel.aE.f1668a = parse.getQueryParameter("downloadTitle");
                simpleAppModel.aE.b = parse.getQueryParameter("openTitle");
                simpleAppModel.aE.c = ct.d(parse.getQueryParameter("openStyle"));
            }
        }
        switch (cardItem.f2027a) {
            case 1:
                simpleAppModel.U = SimpleAppModel.CARD_TYPE.NORMAL;
                break;
            case 2:
                simpleAppModel.U = SimpleAppModel.CARD_TYPE.QUALITY;
                break;
            case 3:
                simpleAppModel.U = SimpleAppModel.CARD_TYPE.SIMPLE;
                break;
            default:
                simpleAppModel.U = SimpleAppModel.CARD_TYPE.UNKNOWN;
                break;
        }
        return simpleAppModel;
    }

    private static void a(LocalApkInfo localApkInfo, SimpleAppModel simpleAppModel) {
        if (localApkInfo != null && simpleAppModel != null) {
            simpleAppModel.c = localApkInfo.mPackageName;
            simpleAppModel.E = localApkInfo.mVersionName;
            simpleAppModel.D = localApkInfo.mVersionCode;
            simpleAppModel.F = localApkInfo.launchCount;
            simpleAppModel.C = localApkInfo.mSortKey;
            simpleAppModel.G = localApkInfo.mLocalFilePath;
            simpleAppModel.H = localApkInfo.occupySize;
            simpleAppModel.I = localApkInfo.mInstallDate;
            simpleAppModel.J = localApkInfo.signature;
            simpleAppModel.K = (long) localApkInfo.flags;
        }
    }

    public static void a(AppUpdateInfo appUpdateInfo, SimpleAppModel simpleAppModel) {
        if (appUpdateInfo != null && simpleAppModel != null) {
            if (appUpdateInfo.o != 0) {
                simpleAppModel.f1634a = appUpdateInfo.o;
            }
            if (appUpdateInfo.r != 0) {
                simpleAppModel.b = appUpdateInfo.r;
            }
            if (!TextUtils.isEmpty(appUpdateInfo.b)) {
                simpleAppModel.d = appUpdateInfo.b;
            }
            if (!TextUtils.isEmpty(appUpdateInfo.c)) {
                simpleAppModel.m = appUpdateInfo.c;
            }
            if (appUpdateInfo.e != null && !TextUtils.isEmpty(appUpdateInfo.e.f2252a)) {
                simpleAppModel.e = appUpdateInfo.e.f2252a;
            }
            simpleAppModel.f = appUpdateInfo.n;
            simpleAppModel.g = appUpdateInfo.d;
            simpleAppModel.o = appUpdateInfo.f;
            simpleAppModel.ad = appUpdateInfo.w;
            simpleAppModel.r = appUpdateInfo.p;
            if (appUpdateInfo.g != null) {
                simpleAppModel.s = appUpdateInfo.g.b;
            }
            simpleAppModel.ab = true;
            simpleAppModel.y = appUpdateInfo.u;
            if (appUpdateInfo.y > 0) {
                simpleAppModel.R = appUpdateInfo.y;
            }
            simpleAppModel.l = appUpdateInfo.h;
            simpleAppModel.i = appUpdateInfo.i;
            simpleAppModel.j = a((byte) 1, appUpdateInfo.s);
            simpleAppModel.k = appUpdateInfo.j;
            simpleAppModel.t = appUpdateInfo.l;
            simpleAppModel.u = a((byte) 2, appUpdateInfo.s);
            simpleAppModel.v = appUpdateInfo.m;
            simpleAppModel.w = appUpdateInfo.k;
            simpleAppModel.z = appUpdateInfo.E;
            simpleAppModel.A = appUpdateInfo.C;
            simpleAppModel.c = appUpdateInfo.f2006a;
            simpleAppModel.ag = appUpdateInfo.I;
        }
    }

    public static void a(AppUpdateInfo appUpdateInfo, DownloadInfo downloadInfo) {
        if (appUpdateInfo != null && downloadInfo != null) {
            if (appUpdateInfo.o != 0) {
                downloadInfo.appId = appUpdateInfo.o;
            }
            if (appUpdateInfo.r != 0) {
                downloadInfo.apkId = appUpdateInfo.r;
            }
            if (!TextUtils.isEmpty(appUpdateInfo.b)) {
                downloadInfo.name = appUpdateInfo.b;
            }
            if (!TextUtils.isEmpty(appUpdateInfo.c)) {
                downloadInfo.signatrue = appUpdateInfo.c;
            }
            if (appUpdateInfo.e != null && !TextUtils.isEmpty(appUpdateInfo.e.f2252a)) {
                downloadInfo.iconUrl = appUpdateInfo.e.f2252a;
            }
            downloadInfo.versionName = appUpdateInfo.n;
            downloadInfo.versionCode = appUpdateInfo.d;
            downloadInfo.grayVersionCode = appUpdateInfo.w;
            downloadInfo.fileMd5 = appUpdateInfo.h;
            ArrayList<String> a2 = a((byte) 1, appUpdateInfo.s);
            if (ct.b(a2)) {
                downloadInfo.apkUrlList = a2;
            } else {
                downloadInfo.apkUrlList.clear();
                downloadInfo.apkUrlList.add(appUpdateInfo.i);
            }
            if (ct.b(a2)) {
                downloadInfo.sllApkUrlList = a2;
            } else {
                downloadInfo.sllApkUrlList.clear();
                downloadInfo.sllApkUrlList.add(appUpdateInfo.l);
            }
            downloadInfo.sllFileSize = appUpdateInfo.m;
            downloadInfo.sllFileMd5 = appUpdateInfo.k;
            downloadInfo.sllLocalManifestMd5 = appUpdateInfo.E;
            downloadInfo.sllLocalVersionCode = appUpdateInfo.C;
            downloadInfo.packageName = appUpdateInfo.f2006a;
            downloadInfo.overWriteChannelId = appUpdateInfo.I;
        }
    }

    public static ArrayList<String> a(byte b2, ArrayList<ApkDownUrl> arrayList) {
        ArrayList<String> arrayList2 = new ArrayList<>();
        if (arrayList != null && !arrayList.isEmpty()) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= arrayList.size()) {
                    break;
                }
                ApkDownUrl apkDownUrl = arrayList.get(i2);
                if (apkDownUrl.a() != b2) {
                    i = i2 + 1;
                } else if (apkDownUrl.b() != null && !apkDownUrl.b().isEmpty()) {
                    arrayList2.addAll(apkDownUrl.b());
                }
            }
        }
        return arrayList2;
    }

    public static ArrayList<AutoDownloadInfo> a() {
        ArrayList<AutoDownloadInfo> d = d();
        d(d);
        return d;
    }

    public static ArrayList<AutoDownloadInfo> b() {
        ArrayList<AutoDownloadInfo> c = c();
        d(c);
        return c;
    }

    public static ArrayList<AutoDownloadInfo> c() {
        List<DownloadInfo> d = r.d();
        if (d == null || d.isEmpty()) {
            return null;
        }
        ArrayList<AutoDownloadInfo> arrayList = new ArrayList<>();
        for (DownloadInfo next : d) {
            AutoDownloadInfo autoDownloadInfo = new AutoDownloadInfo();
            autoDownloadInfo.f2010a = next.packageName;
            autoDownloadInfo.j = next.appId;
            autoDownloadInfo.k = next.apkId;
            autoDownloadInfo.d = next.versionCode;
            autoDownloadInfo.b = next.name;
            arrayList.add(autoDownloadInfo);
        }
        return arrayList;
    }

    public static ArrayList<AutoDownloadInfo> d() {
        List<AutoDownloadInfo> a2 = t.a();
        if (a2 == null || a2.isEmpty()) {
            return null;
        }
        ArrayList<AutoDownloadInfo> arrayList = new ArrayList<>();
        for (AutoDownloadInfo next : a2) {
            AutoDownloadInfo autoDownloadInfo = new AutoDownloadInfo();
            autoDownloadInfo.f2010a = next.f2010a;
            autoDownloadInfo.j = next.j;
            autoDownloadInfo.k = next.k;
            autoDownloadInfo.d = next.d;
            autoDownloadInfo.b = next.b;
            arrayList.add(autoDownloadInfo);
        }
        return arrayList;
    }

    public static List<SimpleAppModel> a(boolean z) {
        List<AppUpdateInfo> g;
        if (z) {
            g = k.b().d();
        } else {
            g = g();
        }
        if (g == null || g.isEmpty()) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (AppUpdateInfo appUpdateInfo : g) {
            SimpleAppModel b2 = b(appUpdateInfo.f2006a);
            if (b2 != null) {
                arrayList.add(b2);
            }
        }
        return arrayList;
    }

    public static List<SimpleAppModel> h(List<String> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (String b2 : list) {
            SimpleAppModel b3 = b(b2);
            if (b3 != null) {
                arrayList.add(b3);
            }
        }
        return arrayList;
    }

    public static List<SimpleAppModel> e() {
        List<AppUpdateInfo> d = k.b().d();
        if (d == null || d.isEmpty()) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (AppUpdateInfo appUpdateInfo : d) {
            SimpleAppModel b2 = b(appUpdateInfo.f2006a);
            if (b2 != null) {
                arrayList.add(b2);
            }
        }
        return arrayList;
    }

    public static List<SimpleAppModel> i(List<BatchDownloadParam> list) {
        boolean z;
        ArrayList arrayList = new ArrayList();
        List<SimpleAppModel> e = e();
        ArrayList<DownloadInfo> c = DownloadProxy.a().c();
        int size = e == null ? 0 : e.size();
        int size2 = c == null ? 0 : c.size();
        int size3 = list == null ? 0 : list.size();
        for (int i = 0; i < size; i++) {
            SimpleAppModel simpleAppModel = e.get(i);
            if (simpleAppModel != null && !TextUtils.isEmpty(simpleAppModel.c)) {
                int i2 = 0;
                boolean z2 = false;
                while (i2 < size2) {
                    DownloadInfo downloadInfo = c.get(i2);
                    AppConst.AppState b2 = b(downloadInfo);
                    if (downloadInfo == null || !simpleAppModel.c.equals(downloadInfo.packageName) || !(b2 == AppConst.AppState.QUEUING || b2 == AppConst.AppState.DOWNLOADING || b2 == AppConst.AppState.DOWNLOADED || b2 == AppConst.AppState.PAUSED || b2 == AppConst.AppState.INSTALLING)) {
                        z = z2;
                    } else {
                        arrayList.add(simpleAppModel);
                        z = true;
                    }
                    i2++;
                    z2 = z;
                }
                if (!z2) {
                    for (int i3 = 0; i3 < size3; i3++) {
                        BatchDownloadParam batchDownloadParam = list.get(i3);
                        if (batchDownloadParam != null && simpleAppModel.c.equals(batchDownloadParam.d)) {
                            arrayList.add(simpleAppModel);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    public static List<SimpleAppModel> f() {
        try {
            HashSet<s> hashSet = new HashSet<>(k.b().e());
            if (hashSet == null || hashSet.isEmpty()) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (s sVar : hashSet) {
                SimpleAppModel b2 = b(sVar.f1896a);
                if (b2 != null) {
                    arrayList.add(b2);
                }
            }
            return arrayList;
        } catch (ConcurrentModificationException e) {
            return null;
        }
    }

    public static List<AppUpdateInfo> g() {
        return a(k.b().d());
    }

    public static int h() {
        return k.b().d().size();
    }

    public static AppConst.AppState d(SimpleAppModel simpleAppModel) {
        AppConst.AppState b2;
        if (simpleAppModel == null) {
            return AppConst.AppState.ILLEGAL;
        }
        if (simpleAppModel.h > com.tencent.assistant.utils.r.d()) {
            return AppConst.AppState.SDKUNSUPPORT;
        }
        DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
        if (be.a().c(simpleAppModel.c)) {
            if (be.a().d(simpleAppModel.c)) {
                return AppConst.AppState.INSTALLED;
            }
            if (a2 != null && a2.isDownloaded() && a2.isDownloadFileExist()) {
                a2.downloadState = SimpleDownloadInfo.DownloadState.INSTALLED;
                DownloadProxy.a().d(a2);
                return AppConst.AppState.INSTALLED;
            }
        }
        if (a2 != null && (b2 = b(a2)) != AppConst.AppState.ILLEGAL) {
            return b2;
        }
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad);
        AppConst.AppState appState = AppConst.AppState.ILLEGAL;
        if (localApkInfo != null) {
            if (a(simpleAppModel.q())) {
                appState = AppConst.AppState.INSTALLING;
            } else {
                appState = AppConst.AppState.DOWNLOADED;
            }
        }
        LocalApkInfo localApkInfo2 = ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c);
        if (localApkInfo2 == null) {
            return appState == AppConst.AppState.ILLEGAL ? AppConst.AppState.DOWNLOAD : appState;
        }
        if (localApkInfo2.mVersionCode == simpleAppModel.g) {
            if (simpleAppModel.ad <= localApkInfo2.mGrayVersionCode) {
                return AppConst.AppState.INSTALLED;
            }
            if (appState == AppConst.AppState.ILLEGAL) {
                return AppConst.AppState.UPDATE;
            }
            return appState;
        } else if (localApkInfo2.mVersionCode > simpleAppModel.g) {
            return AppConst.AppState.INSTALLED;
        } else {
            if (appState == AppConst.AppState.ILLEGAL) {
                return AppConst.AppState.UPDATE;
            }
            return appState;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    public static AppConst.AppState b(DownloadInfo downloadInfo) {
        return a(downloadInfo, false, false);
    }

    public static int a(DownloadInfo downloadInfo, AppConst.AppState appState) {
        if (SimpleDownloadInfo.isProgressShowFake(downloadInfo, appState)) {
            return downloadInfo.response.f;
        }
        return SimpleDownloadInfo.getPercent(downloadInfo);
    }

    public static AppConst.AppState a(DownloadInfo downloadInfo, boolean z, boolean z2) {
        if (downloadInfo == null) {
            return AppConst.AppState.ILLEGAL;
        }
        if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INIT || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.ILLEGAL) {
            return AppConst.AppState.ILLEGAL;
        }
        if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED && z2) {
            return AppConst.AppState.INSTALLED;
        }
        if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.SUCC) {
            LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(downloadInfo.packageName);
            boolean z3 = z || downloadInfo.isDownloadFileExist() || ApkResourceManager.getInstance().getLocalApkInfo(downloadInfo.packageName, downloadInfo.versionCode, downloadInfo.grayVersionCode) != null;
            if (localApkInfo != null) {
                if (downloadInfo.versionCode == localApkInfo.mVersionCode) {
                    if (downloadInfo.grayVersionCode <= localApkInfo.mGrayVersionCode) {
                        return AppConst.AppState.INSTALLED;
                    }
                    return z3 ? AppConst.AppState.DOWNLOADED : AppConst.AppState.UPDATE;
                } else if (downloadInfo.versionCode > localApkInfo.mVersionCode) {
                    return z3 ? AppConst.AppState.DOWNLOADED : AppConst.AppState.UPDATE;
                } else {
                    return AppConst.AppState.INSTALLED;
                }
            } else if (z3) {
                return AppConst.AppState.DOWNLOADED;
            } else {
                return AppConst.AppState.DOWNLOAD;
            }
        } else if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLING) {
            return AppConst.AppState.INSTALLING;
        } else {
            if (downloadInfo.isUiTypeWiseDownload()) {
                LocalApkInfo localApkInfo2 = ApkResourceManager.getInstance().getLocalApkInfo(downloadInfo.packageName);
                if (localApkInfo2 == null || downloadInfo.versionCode <= localApkInfo2.mVersionCode) {
                    return AppConst.AppState.DOWNLOAD;
                }
                return AppConst.AppState.UPDATE;
            }
            switch (v.f1904a[downloadInfo.downloadState.ordinal()]) {
                case 1:
                    return AppConst.AppState.QUEUING;
                case 2:
                    return AppConst.AppState.DOWNLOADING;
                case 3:
                case 4:
                    return AppConst.AppState.PAUSED;
                case 5:
                    return AppConst.AppState.FAIL;
                case 6:
                    return AppConst.AppState.DOWNLOADING;
                default:
                    return AppConst.AppState.ILLEGAL;
            }
        }
    }

    public static int i() {
        return g().size();
    }

    public static int b(boolean z) {
        List<SimpleAppModel> f;
        if (!z && (f = f()) != null) {
            return f.size();
        }
        return 0;
    }

    public static boolean a(AppUpdateInfo appUpdateInfo) {
        if (appUpdateInfo != null && !TextUtils.isEmpty(appUpdateInfo.f2006a)) {
            return true;
        }
        return false;
    }

    public static boolean a(String str, String str2) {
        if (str2 == null) {
            str2 = Constants.STR_EMPTY;
        }
        if ("0".equals(str)) {
            return str2.length() == 0 || str2.contains("0");
        }
        return str2.contains(str);
    }

    public static boolean a(DownloadInfo downloadInfo, String str) {
        downloadInfo.autoInstall = !a("2", str);
        if ((!a("3", str) || !c.d()) && !a("1", str)) {
            return false;
        }
        a.a().a(downloadInfo);
        return true;
    }

    public static AppConst.AppState a(DownloadInfo downloadInfo, j jVar, PluginInfo pluginInfo) {
        if (downloadInfo == null) {
            return AppConst.AppState.ILLEGAL;
        }
        if (downloadInfo.uiType != SimpleDownloadInfo.UIType.PLUGIN_PREDOWNLOAD) {
            switch (v.f1904a[downloadInfo.downloadState.ordinal()]) {
                case 1:
                    return AppConst.AppState.QUEUING;
                case 2:
                    return AppConst.AppState.DOWNLOADING;
                case 3:
                case 4:
                    return AppConst.AppState.PAUSED;
                case 5:
                    return AppConst.AppState.FAIL;
                case 6:
                case 8:
                    return AppConst.AppState.DOWNLOADED;
                case 7:
                    if (jVar == null || pluginInfo == null || jVar.d == pluginInfo.getVersion()) {
                        return AppConst.AppState.DOWNLOAD;
                    }
                    return AppConst.AppState.UPDATE;
                case 9:
                    return AppConst.AppState.INSTALLED;
                default:
                    return AppConst.AppState.DOWNLOAD;
            }
        } else if (jVar == null || pluginInfo == null || jVar.d <= pluginInfo.getVersion()) {
            return AppConst.AppState.DOWNLOAD;
        } else {
            return AppConst.AppState.UPDATE;
        }
    }

    public static AppConst.AppState a(com.tencent.assistantv2.model.c cVar) {
        if (cVar.r() == SimpleDownloadInfo.DownloadType.APK) {
            return d((SimpleAppModel) cVar);
        }
        return AppConst.AppState.DOWNLOAD;
    }

    public static d e(SimpleAppModel simpleAppModel) {
        if (simpleAppModel == null) {
            return null;
        }
        d dVar = new d();
        DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
        dVar.b = a2;
        dVar.f1661a = a2 != null ? a2.downloadTicket : String.valueOf(simpleAppModel.b);
        dVar.c = d(simpleAppModel);
        return dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    public static d a(SimpleAppModel simpleAppModel, AppConst.AppState appState) {
        if (simpleAppModel == null) {
            return null;
        }
        d dVar = new d();
        DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
        dVar.b = a2;
        dVar.f1661a = a2 != null ? a2.downloadTicket : String.valueOf(simpleAppModel.b);
        dVar.c = appState;
        if (dVar.c == AppConst.AppState.ILLEGAL) {
            if (a2 != null) {
                dVar.c = a(a2, false, false);
            }
            if (dVar.c == AppConst.AppState.ILLEGAL) {
                dVar.c = d(simpleAppModel);
            }
        }
        return dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.assistant.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.model.j, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.u.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.w, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.u.a(com.tencent.assistant.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    public static d c(DownloadInfo downloadInfo) {
        if (downloadInfo == null) {
            return null;
        }
        d dVar = new d();
        dVar.b = downloadInfo;
        dVar.f1661a = downloadInfo.downloadTicket;
        dVar.c = a(downloadInfo, false, false);
        return dVar;
    }

    private static boolean a(Uri uri, int i) {
        if (uri == null) {
            return false;
        }
        String host = uri.getHost();
        if (TextUtils.isEmpty(host) || !host.equalsIgnoreCase("applink")) {
            return false;
        }
        String queryParameter = uri.getQueryParameter("minversion");
        if (TextUtils.isEmpty(queryParameter) || ct.a(queryParameter, -1) <= i) {
            return true;
        }
        return false;
    }
}
