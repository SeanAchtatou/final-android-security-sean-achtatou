package com.tencent.assistant.module;

import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.ad;
import com.tencent.assistant.protocol.jce.CheckSelfUpdateResponse;

/* compiled from: ProGuard */
class fg implements CallbackHelper.Caller<ad> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CheckSelfUpdateResponse f1819a;
    final /* synthetic */ int b;
    final /* synthetic */ fa c;

    fg(fa faVar, CheckSelfUpdateResponse checkSelfUpdateResponse, int i) {
        this.c = faVar;
        this.f1819a = checkSelfUpdateResponse;
        this.b = i;
    }

    /* renamed from: a */
    public void call(ad adVar) {
        SelfUpdateManager.SelfUpdateInfo a2 = this.c.a(this.f1819a);
        if (a2 != null) {
            SelfUpdateManager.a().a(a2);
        }
        adVar.onCheckSelfUpdateFinish(this.b, 0, a2);
    }
}
