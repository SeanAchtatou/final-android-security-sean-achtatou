package com.tencent.assistant.module.timer.job;

import com.tencent.assistant.m;
import com.tencent.assistant.module.timer.SimpleBaseScheduleJob;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
public class SelfUpdateTimerJob extends SimpleBaseScheduleJob {

    /* renamed from: a  reason: collision with root package name */
    private static SelfUpdateTimerJob f1873a;

    public static synchronized SelfUpdateTimerJob e() {
        SelfUpdateTimerJob selfUpdateTimerJob;
        synchronized (SelfUpdateTimerJob.class) {
            if (f1873a == null) {
                f1873a = new SelfUpdateTimerJob();
            }
            selfUpdateTimerJob = f1873a;
        }
        return selfUpdateTimerJob;
    }

    public int h() {
        return m.a().a("self_update_check_interval", 10800);
    }

    public void d_() {
        TemporaryThreadManager.get().start(new a(this));
    }
}
