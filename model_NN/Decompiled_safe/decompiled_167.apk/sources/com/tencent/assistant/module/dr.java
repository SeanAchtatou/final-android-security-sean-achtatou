package com.tencent.assistant.module;

import android.util.Log;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.db.table.e;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.manager.smartcard.o;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.manager.u;
import com.tencent.assistant.module.update.aa;
import com.tencent.assistant.module.wisedownload.i;
import com.tencent.assistant.protocol.jce.AutoDownloadCfg;
import com.tencent.assistant.protocol.jce.AutoDownloadPushCfg;
import com.tencent.assistant.protocol.jce.AutoStartCfg;
import com.tencent.assistant.protocol.jce.CSProtocolCfg;
import com.tencent.assistant.protocol.jce.CommonCfg;
import com.tencent.assistant.protocol.jce.DownloadButtonSpecailInfoList;
import com.tencent.assistant.protocol.jce.DownloadCfg;
import com.tencent.assistant.protocol.jce.DownloadCheckCfg;
import com.tencent.assistant.protocol.jce.ExternalCallYYBCfg;
import com.tencent.assistant.protocol.jce.FloatWindowCfg;
import com.tencent.assistant.protocol.jce.GetSettingRequest;
import com.tencent.assistant.protocol.jce.GetSettingResponse;
import com.tencent.assistant.protocol.jce.InTimePushCfg;
import com.tencent.assistant.protocol.jce.NpcListCfg;
import com.tencent.assistant.protocol.jce.SearchWebCfg;
import com.tencent.assistant.protocol.jce.SettingCfg;
import com.tencent.assistant.protocol.jce.SmartCardCfgList;
import com.tencent.assistant.protocol.jce.StatCfg;
import com.tencent.assistant.protocol.jce.TempRootCfg;
import com.tencent.assistant.protocol.jce.TimerCfg;
import com.tencent.assistant.protocol.jce.UpdateCfg;
import com.tencent.assistant.protocol.jce.UserTaskCfg;
import com.tencent.assistant.protocol.jce.WebviewCfg;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bh;
import com.tencent.assistant.utils.cp;
import com.tencent.assistantv2.manager.k;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class dr extends aw {
    private static dr b;

    /* renamed from: a  reason: collision with root package name */
    public final byte f1783a = 30;

    private dr() {
    }

    public static synchronized dr a() {
        dr drVar;
        synchronized (dr.class) {
            if (b == null) {
                b = new dr();
            }
            drVar = b;
        }
        return drVar;
    }

    public int b() {
        return send(new GetSettingRequest());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.d("linmg", getClass().getSimpleName() + " onRequestSuccessed....");
        GetSettingResponse getSettingResponse = (GetSettingResponse) jceStruct2;
        HashMap hashMap = new HashMap();
        ArrayList<SettingCfg> arrayList = getSettingResponse.b;
        Map<String, String> a2 = getSettingResponse.a();
        if (a2 != null) {
            u.a().a(a2, hashMap);
        }
        for (SettingCfg next : arrayList) {
            Log.v("cfg", "cfg.type : " + ((int) next.f2307a));
            if (next.f2307a == 1) {
                TimerCfg timerCfg = (TimerCfg) bh.b(next.b, TimerCfg.class);
                if (timerCfg != null) {
                    if (timerCfg.f2404a == 1) {
                        u.a().a(timerCfg, hashMap);
                    } else if (timerCfg.f2404a == 2) {
                        u.a().d(timerCfg, hashMap);
                    } else if (timerCfg.f2404a == 3) {
                        u.a().e(timerCfg, hashMap);
                    } else if (timerCfg.f2404a == 4) {
                        u.a().f(timerCfg, hashMap);
                    } else if (timerCfg.f2404a == 5) {
                        u.a().b(timerCfg, hashMap);
                    } else if (timerCfg.f2404a == 30) {
                        u.a().c(timerCfg, hashMap);
                    }
                }
            } else if (next.f2307a == 2) {
                u.a().a((StatCfg) bh.b(next.b, StatCfg.class), hashMap);
            } else if (next.f2307a == 3) {
                u.a().a((DownloadCfg) bh.b(next.b, DownloadCfg.class), hashMap);
            } else if (next.f2307a == 4) {
                aa.a(next.c, (UpdateCfg) bh.b(next.b, UpdateCfg.class));
            } else if (next.f2307a == 5) {
                u.a().a((WebviewCfg) bh.b(next.b, WebviewCfg.class), hashMap);
            } else if (next.f2307a == 6) {
                AutoDownloadCfg autoDownloadCfg = (AutoDownloadCfg) bh.b(next.b, AutoDownloadCfg.class);
                if (autoDownloadCfg != null) {
                    as.w().a(autoDownloadCfg);
                    i.a().e();
                    u.a().a(autoDownloadCfg.a(), hashMap);
                    u.a().b(autoDownloadCfg.b(), hashMap);
                }
            } else if (next.f2307a == 7) {
                SmartCardCfgList smartCardCfgList = (SmartCardCfgList) bh.b(next.b, SmartCardCfgList.class);
                if (smartCardCfgList != null) {
                    o.a().a(smartCardCfgList);
                }
            } else if (next.f2307a == 8) {
                u.a().a(next.b, hashMap);
            } else if (next.f2307a == 9) {
                CommonCfg commonCfg = (CommonCfg) bh.b(next.b, CommonCfg.class);
                if (commonCfg != null) {
                    u.a().a(commonCfg, hashMap);
                }
            } else if (next.f2307a == 10) {
                UserTaskCfg userTaskCfg = (UserTaskCfg) bh.b(next.b, UserTaskCfg.class);
                if (userTaskCfg != null) {
                    Log.i("ig", "<<UserTask=" + userTaskCfg.toString());
                    as.w().a(userTaskCfg);
                }
            } else if (next.f2307a == 12) {
                CSProtocolCfg cSProtocolCfg = (CSProtocolCfg) bh.b(next.b, CSProtocolCfg.class);
                if (cSProtocolCfg != null) {
                    if (cSProtocolCfg.f2025a != m.a().a("auth_protocol_update_period", 0)) {
                        m.a().b("auth_protocol_update_period", Integer.valueOf(cSProtocolCfg.f2025a));
                    }
                    if (cSProtocolCfg.b != m.a().a("auth_protocol_event_cause_period", 0)) {
                        m.a().b("auth_protocol_event_cause_period", Integer.valueOf(cSProtocolCfg.b));
                    }
                    if (cSProtocolCfg.c != m.a().a("auth_protocol_fail_nottry_period", 0)) {
                        m.a().b("auth_protocol_fail_nottry_period", Integer.valueOf(cSProtocolCfg.c));
                    }
                    if (cSProtocolCfg.d != m.a().a("auth_protocol_log_st", false)) {
                        m.a().b("auth_protocol_log_st", Boolean.valueOf(cSProtocolCfg.d));
                    }
                }
            } else if (next.f2307a == 11) {
                XLog.d("donaldxu", "get autostart setting");
                new e().b((AutoStartCfg) bh.b(next.b, AutoStartCfg.class));
            } else if (next.f2307a == 13) {
                NpcListCfg npcListCfg = (NpcListCfg) bh.b(next.b, NpcListCfg.class);
                if (npcListCfg != null) {
                    as.w().a(npcListCfg);
                }
            } else if (next.f2307a == 15) {
                DownloadCheckCfg downloadCheckCfg = (DownloadCheckCfg) bh.b(next.b, DownloadCheckCfg.class);
                if (downloadCheckCfg != null) {
                    u.a().a(downloadCheckCfg, hashMap);
                }
            } else if (next.f2307a == 16) {
                u.a().b(next.b, hashMap);
            } else if (next.f2307a == 17) {
                t.a().a((SearchWebCfg) bh.b(next.b, SearchWebCfg.class));
            } else if (next.f2307a == 18) {
                u.a().a(next.b);
            } else if (next.f2307a == 24) {
                DownloadButtonSpecailInfoList downloadButtonSpecailInfoList = (DownloadButtonSpecailInfoList) bh.b(next.b, DownloadButtonSpecailInfoList.class);
                if (downloadButtonSpecailInfoList != null) {
                    cp.a().a(true, downloadButtonSpecailInfoList);
                }
            } else if (next.f2307a == 19) {
                k.a().a((InTimePushCfg) bh.b(next.b, InTimePushCfg.class));
            } else if (next.f2307a == 20) {
                u.a().a((AutoDownloadPushCfg) bh.b(next.b, AutoDownloadPushCfg.class));
            } else if (next.f2307a == 21) {
                XLog.i("BackgroundScan", "<settings> get background scan settings success !");
                u.a().b(next.b);
            } else if (next.f2307a == 23) {
                XLog.i("TempRoot", "<settings> get temp root settings success !");
                TempRootCfg tempRootCfg = (TempRootCfg) bh.b(next.b, TempRootCfg.class);
                if (tempRootCfg != null) {
                    u.a().a(tempRootCfg);
                }
            } else if (next.f2307a == 26) {
                XLog.i("apkAutoOpenConfig", "<settings> get apk auto open config settings success !");
                u.a().c(next.b);
            } else if (next.f2307a == 25) {
                XLog.d("ExternalCallYYBCfg", "<settings> get ExternalCallYYBCfg settings success !");
                ExternalCallYYBCfg externalCallYYBCfg = (ExternalCallYYBCfg) bh.b(next.b, ExternalCallYYBCfg.class);
                if (externalCallYYBCfg != null) {
                    u.a().a(externalCallYYBCfg);
                }
            } else if (next.f2307a == 27) {
                XLog.i("floatingwindow", "<settings> get float window settings success !");
                FloatWindowCfg floatWindowCfg = (FloatWindowCfg) bh.b(next.b, FloatWindowCfg.class);
                if (floatWindowCfg != null) {
                    u.a().a(floatWindowCfg);
                }
            }
        }
        u.a().a(hashMap);
        m.a().c(getSettingResponse.c - System.currentTimeMillis());
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.d("linmg", getClass().getSimpleName() + " onRequestFailed...." + i2);
    }
}
