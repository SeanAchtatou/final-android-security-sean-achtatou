package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.module.callback.t;
import com.tencent.assistant.protocol.jce.AppGroup;
import com.tencent.assistant.protocol.jce.GetSubjectRequest;
import com.tencent.assistant.protocol.jce.GetSubjectResponse;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class dv extends BaseEngine<t> {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f1786a = null;
    private boolean b = false;
    private long c = -1;
    private int d = -1;
    private int e = -1;
    private String f = null;
    private List<SimpleAppModel> g = new ArrayList();
    private b h = new b();

    public dv() {
    }

    public dv(int i, String str) {
        this.e = i;
        this.f = str;
    }

    public int a() {
        if (this.d > 0) {
            cancel(this.d);
        }
        this.d = d();
        return this.d;
    }

    public int b() {
        if (this.f1786a == null || this.f1786a.length == 0) {
            return -1;
        }
        this.d = d();
        return this.d;
    }

    private int d() {
        GetSubjectRequest getSubjectRequest = new GetSubjectRequest();
        getSubjectRequest.f2169a = this.e;
        getSubjectRequest.b = this.f;
        getSubjectRequest.c = 10;
        getSubjectRequest.d = this.f1786a;
        return send(getSubjectRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        boolean z = true;
        GetSubjectRequest getSubjectRequest = (GetSubjectRequest) jceStruct;
        GetSubjectResponse getSubjectResponse = (GetSubjectResponse) jceStruct2;
        boolean z2 = getSubjectResponse.e != this.c || getSubjectRequest.d == null || getSubjectRequest.d.length == 0;
        if (getSubjectResponse.d != 1) {
            z = false;
        }
        this.b = z;
        ArrayList<SimpleAppModel> b2 = u.b(getSubjectResponse.b);
        this.c = getSubjectResponse.e;
        this.h.b(this.c);
        this.f1786a = getSubjectResponse.c;
        if (z2) {
            this.g.clear();
        }
        this.g.addAll(b2);
        AppGroupInfo appGroupInfo = new AppGroupInfo();
        if (getSubjectResponse.f != null) {
            AppGroup appGroup = getSubjectResponse.f;
            appGroupInfo.a(appGroup.f1995a);
            appGroupInfo.b(appGroup.c);
            appGroupInfo.c(appGroup.d);
            appGroupInfo.a(appGroup.b);
            appGroupInfo.a(appGroup.f);
            appGroupInfo.b(appGroup.g);
            if (appGroup.h != null) {
                appGroupInfo.a(appGroup.h);
            }
        }
        notifyDataChangedInMainThread(new dw(this, i, z2, b2, appGroupInfo));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        GetSubjectRequest getSubjectRequest = (GetSubjectRequest) jceStruct;
        notifyDataChangedInMainThread(new dx(this, i, i2, getSubjectRequest.d == null || getSubjectRequest.d.length == 0));
    }

    public boolean c() {
        return this.b;
    }
}
