package com.tencent.assistant.module.timer.job;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.m;
import com.tencent.assistant.module.timer.BaseScheduleJob;
import com.tencent.assistant.usagestats.UsagestatsSTManager;
import com.tencent.assistantv2.st.k;

/* compiled from: ProGuard */
public class GetAppExInfoScheduleJob extends BaseScheduleJob {
    private static GetAppExInfoScheduleJob b;

    /* renamed from: a  reason: collision with root package name */
    boolean f1867a = true;

    public static synchronized GetAppExInfoScheduleJob i() {
        GetAppExInfoScheduleJob getAppExInfoScheduleJob;
        synchronized (GetAppExInfoScheduleJob.class) {
            if (b == null) {
                b = new GetAppExInfoScheduleJob();
            }
            getAppExInfoScheduleJob = b;
        }
        return getAppExInfoScheduleJob;
    }

    public void e() {
        ApkResourceManager.loadAppLauncherTime();
        ApkResourceManager.loadTraffic();
        if (this.f1867a) {
            this.f1867a = false;
        } else {
            UsagestatsSTManager.a().a(UsagestatsSTManager.ReportScene.timer);
        }
        k.a(UsagestatsSTManager.ReportScene.timer);
    }

    public int h() {
        return m.a().a("get_appextinfo", 3180);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* access modifiers changed from: protected */
    public long f() {
        int h = h() * 1000;
        long j = (long) h;
        long a2 = m.a().a(g(), 0L);
        if (a2 == 0) {
            return 0;
        }
        long currentTimeMillis = System.currentTimeMillis() - a2;
        if (h <= 0) {
            return 3600000;
        }
        if (currentTimeMillis <= 0 || currentTimeMillis / ((long) h) >= 1) {
            return 5000;
        }
        return ((long) h) - (currentTimeMillis % ((long) h));
    }
}
