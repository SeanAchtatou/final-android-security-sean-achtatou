package com.tencent.assistant.module.wisedownload;

import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.notification.a;
import com.tencent.assistant.manager.notification.z;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.module.update.s;
import com.tencent.assistant.module.update.u;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.assistant.utils.installuninstall.p;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class j implements UIEventListener {
    private static j b = null;

    /* renamed from: a  reason: collision with root package name */
    private AstApp f1914a = AstApp.i();
    private ConcurrentHashMap<String, Integer> c = new ConcurrentHashMap<>();
    private ArrayList<String> d = new ArrayList<>();
    private final int e = 3;

    public static synchronized j a() {
        j jVar;
        synchronized (j.class) {
            if (b == null) {
                b = new j();
            }
            jVar = b;
        }
        return jVar;
    }

    private j() {
        e();
    }

    private void e() {
        b();
    }

    public void b() {
        this.f1914a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_START, this);
        this.f1914a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_PAUSE, this);
        this.f1914a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        this.f1914a.k().addUIEventListener(1007, this);
        this.f1914a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        this.f1914a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        this.f1914a.k().addUIEventListener(1027, this);
    }

    public void handleUIEvent(Message message) {
        DownloadInfo d2;
        DownloadInfo d3;
        DownloadInfo d4;
        DownloadInfo d5;
        DownloadInfo d6;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING:
                if (message.obj instanceof String) {
                    String str = (String) message.obj;
                    if (!TextUtils.isEmpty(str) && (d2 = DownloadProxy.a().d(str)) != null && d2.isUiTypeWiseDownload()) {
                        int percent = DownloadInfo.getPercent(d2);
                        if (percent % 5 == 0) {
                            XLog.d("WiseDownload", "downloading " + d2.name + " " + percent);
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC:
                if (message.obj instanceof String) {
                    String str2 = (String) message.obj;
                    if (!TextUtils.isEmpty(str2) && (d6 = DownloadProxy.a().d(str2)) != null && d6.isUiTypeWiseDownload()) {
                        if (d6.uiType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD && !p.a().b()) {
                            a.a().a(d6.downloadTicket);
                        } else if (d6.uiType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD) {
                            u.a();
                            if (!u.a().b(d6.uiType, d6.packageName, d6.versionCode) || !p.a().b()) {
                                z.a().a(d6.downloadTicket);
                            } else {
                                m.a().h(d6.downloadTicket);
                            }
                        } else if (d6.uiType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD) {
                            m.a().g(d6.downloadTicket);
                        }
                    } else {
                        return;
                    }
                }
                i.a().o();
                return;
            case 1007:
                if (message.obj instanceof String) {
                    String str3 = (String) message.obj;
                    if (!TextUtils.isEmpty(str3) && (d5 = DownloadProxy.a().d(str3)) != null && d5.isUiTypeWiseDownload()) {
                        b(str3);
                        if (!b(d5)) {
                            return;
                        }
                        if (d5.response == null || d5.response.f1263a == 0) {
                            TemporaryThreadManager.get().start(new k(this, str3));
                            return;
                        } else {
                            i.a().p();
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    String str4 = ((InstallUninstallTaskBean) message.obj).downloadTicket;
                    if (!TextUtils.isEmpty(str4) && (d4 = DownloadProxy.a().d(str4)) != null && d4.isUiTypeWiseBookingDownload() && d4.isUiTypeWiseSubscribtionDownloadAutoInstall()) {
                        if (d4.uiType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD) {
                            XLog.d("BookingDownload", "Download SUCC ticket " + str4);
                            a.a().a(d4.downloadTicket);
                            return;
                        } else if (d4.uiType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD) {
                            String ad = m.a().ad();
                            if (!TextUtils.isEmpty(ad) && ad.equals(d4.downloadTicket)) {
                                z.a().a(d4.downloadTicket);
                                return;
                            }
                            return;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case 1027:
                if (message.obj != null && (message.obj instanceof InstallUninstallTaskBean)) {
                    String str5 = ((InstallUninstallTaskBean) message.obj).downloadTicket;
                    if (!TextUtils.isEmpty(str5) && (d3 = DownloadProxy.a().d(str5)) != null && d3.isUiTypeWiseBookingDownload() && d3.isUiTypeWiseSubscribtionDownloadAutoInstall()) {
                        if (d3.uiType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD) {
                            String ad2 = m.a().ad();
                            if (!TextUtils.isEmpty(ad2) && ad2.equals(d3.downloadTicket)) {
                                z.a().a(d3.downloadTicket);
                                return;
                            }
                            return;
                        }
                        String ac = m.a().ac();
                        if (!TextUtils.isEmpty(ac) && ac.equals(d3.downloadTicket)) {
                            a.a().a(d3.downloadTicket);
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_START:
                a(message.arg1, message.arg2);
                return;
            case EventDispatcherEnum.UI_EVENT_AUTO_DOWNLOAD_PAUSE:
                a(message.arg1);
                return;
            default:
                return;
        }
    }

    private void b(String str) {
        if (!this.c.containsKey(str)) {
            this.c.put(str, 1);
        } else {
            this.c.put(str, Integer.valueOf(this.c.get(str).intValue() + 1));
        }
        this.d.add(str);
    }

    private boolean c(String str) {
        if (TextUtils.isEmpty(str) || !this.c.containsKey(str)) {
            return false;
        }
        int intValue = this.c.get(str).intValue();
        if (this.d.contains(str) || intValue >= 3) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):void
      com.tencent.assistant.manager.DownloadProxy.b(java.lang.String, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:102:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0128 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r11, int r12) {
        /*
            r10 = this;
            r9 = 0
            r2 = 1
            r5 = 0
            java.lang.String r0 = "WiseDownload"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = " startWifiAutoDownload type = "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            com.tencent.assistant.utils.XLog.d(r0, r1)
            switch(r11) {
                case 1: goto L_0x001f;
                case 2: goto L_0x00a6;
                case 3: goto L_0x00ab;
                case 4: goto L_0x001e;
                case 5: goto L_0x00b0;
                case 6: goto L_0x00b5;
                default: goto L_0x001e;
            }
        L_0x001e:
            return
        L_0x001f:
            com.tencent.assistant.download.SimpleDownloadInfo$UIType r0 = com.tencent.assistant.download.SimpleDownloadInfo.UIType.WISE_SELF_UPDAET
            r1 = r0
        L_0x0022:
            com.tencent.assistant.module.wisedownload.condition.ThresholdCondition$CONDITION_TRIGGER_ACTION r0 = com.tencent.assistant.module.wisedownload.condition.ThresholdCondition.CONDITION_TRIGGER_ACTION.DOWNLOAD_FAIL
            int r0 = r0.ordinal()
            if (r0 == r12) goto L_0x0037
            com.tencent.assistant.module.wisedownload.condition.ThresholdCondition$CONDITION_TRIGGER_ACTION r0 = com.tencent.assistant.module.wisedownload.condition.ThresholdCondition.CONDITION_TRIGGER_ACTION.DOWNLOAD_SUCC
            int r0 = r0.ordinal()
            if (r0 == r12) goto L_0x0037
            java.util.ArrayList<java.lang.String> r0 = r10.d
            r0.clear()
        L_0x0037:
            com.tencent.assistant.download.a.a()
            com.tencent.assistant.manager.DownloadProxy r0 = com.tencent.assistant.manager.DownloadProxy.a()
            com.tencent.assistant.download.SimpleDownloadInfo$DownloadType r3 = com.tencent.assistant.download.SimpleDownloadInfo.DownloadType.APK
            java.util.ArrayList r6 = r0.a(r3)
            if (r6 == 0) goto L_0x01d8
            int r0 = r6.size()
            if (r0 <= 0) goto L_0x01d8
            java.util.Iterator r3 = r6.iterator()
        L_0x0050:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x01d8
            java.lang.Object r0 = r3.next()
            com.tencent.assistant.download.DownloadInfo r0 = (com.tencent.assistant.download.DownloadInfo) r0
            if (r0 == 0) goto L_0x0050
            com.tencent.assistant.download.SimpleDownloadInfo$UIType r4 = r0.uiType
            if (r4 != r1) goto L_0x0050
            long r7 = r0.createTime
            boolean r4 = com.tencent.assistant.utils.cv.b(r7)
            if (r4 == 0) goto L_0x0050
            com.tencent.assistant.download.SimpleDownloadInfo$UIType r4 = com.tencent.assistant.download.SimpleDownloadInfo.UIType.WISE_APP_UPDATE
            if (r1 != r4) goto L_0x00ba
            boolean r4 = r10.c(r0)
            if (r4 != 0) goto L_0x007c
            java.lang.String r4 = r0.packageName
            boolean r4 = r10.d(r4)
            if (r4 == 0) goto L_0x00ba
        L_0x007c:
            java.lang.String r4 = "million"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "WiseDownloadManager.startWifiAutoDownload, 应用"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = r0.packageName
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "下载到一半由于被忽略或被卸载的原因，Wifi智能下载记录删除并中止它的下载"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            com.tencent.assistant.utils.XLog.d(r4, r7)
            com.tencent.assistant.manager.DownloadProxy r4 = com.tencent.assistant.manager.DownloadProxy.a()
            java.lang.String r0 = r0.downloadTicket
            r4.b(r0, r5)
            goto L_0x0050
        L_0x00a6:
            com.tencent.assistant.download.SimpleDownloadInfo$UIType r0 = com.tencent.assistant.download.SimpleDownloadInfo.UIType.WISE_APP_UPDATE
            r1 = r0
            goto L_0x0022
        L_0x00ab:
            com.tencent.assistant.download.SimpleDownloadInfo$UIType r0 = com.tencent.assistant.download.SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD
            r1 = r0
            goto L_0x0022
        L_0x00b0:
            com.tencent.assistant.download.SimpleDownloadInfo$UIType r0 = com.tencent.assistant.download.SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD
            r1 = r0
            goto L_0x0022
        L_0x00b5:
            com.tencent.assistant.download.SimpleDownloadInfo$UIType r0 = com.tencent.assistant.download.SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD
            r1 = r0
            goto L_0x0022
        L_0x00ba:
            boolean r4 = r10.a(r0, r1)
            if (r4 == 0) goto L_0x0050
            com.tencent.assistant.download.a r3 = com.tencent.assistant.download.a.a()
            r3.b(r0, r1)
            com.tencent.assistant.module.wisedownload.condition.ThresholdCondition$CONDITION_RESULT_CODE r0 = com.tencent.assistant.module.wisedownload.condition.ThresholdCondition.CONDITION_RESULT_CODE.OK_CONTINUE_DOWNLOAD
            com.tencent.assistant.module.wisedownload.m.b(r11, r0, r12)
            r3 = r2
        L_0x00cd:
            if (r3 != 0) goto L_0x01d5
            switch(r11) {
                case 1: goto L_0x00d4;
                case 2: goto L_0x0180;
                case 3: goto L_0x018a;
                case 4: goto L_0x00d2;
                case 5: goto L_0x0194;
                case 6: goto L_0x019e;
                default: goto L_0x00d2;
            }
        L_0x00d2:
            goto L_0x001e
        L_0x00d4:
            com.tencent.assistant.module.update.u r0 = com.tencent.assistant.module.update.u.a()
            java.util.List r0 = r0.g()
        L_0x00dc:
            java.util.ArrayList r0 = com.tencent.assistant.module.u.e(r0)
            java.util.Iterator r7 = r0.iterator()
        L_0x00e4:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x01d5
            java.lang.Object r0 = r7.next()
            com.tencent.assistant.model.SimpleAppModel r0 = (com.tencent.assistant.model.SimpleAppModel) r0
            com.tencent.assistant.manager.DownloadProxy r4 = com.tencent.assistant.manager.DownloadProxy.a()
            com.tencent.assistant.download.DownloadInfo r4 = r4.a(r0)
            if (r4 == 0) goto L_0x010d
            boolean r8 = r4.needReCreateInfo(r0)
            if (r8 == 0) goto L_0x010d
            com.tencent.assistant.manager.DownloadProxy r8 = com.tencent.assistant.manager.DownloadProxy.a()
            java.lang.String r4 = r4.downloadTicket
            r8.b(r4)
            com.tencent.assistant.download.DownloadInfo r4 = com.tencent.assistant.download.DownloadInfo.createDownloadInfo(r0, r9)
        L_0x010d:
            if (r4 == 0) goto L_0x01a8
            com.tencent.assistant.download.SimpleDownloadInfo$UIType r0 = r4.uiType
            if (r0 != r1) goto L_0x00e4
            boolean r0 = r10.a(r4, r1)
            if (r0 == 0) goto L_0x00e4
            com.tencent.assistant.download.a r0 = com.tencent.assistant.download.a.a()
            r0.b(r4, r1)
            com.tencent.assistant.module.wisedownload.condition.ThresholdCondition$CONDITION_RESULT_CODE r0 = com.tencent.assistant.module.wisedownload.condition.ThresholdCondition.CONDITION_RESULT_CODE.OK_CONTINUE_DOWNLOAD
            com.tencent.assistant.module.wisedownload.m.b(r11, r0, r12)
            r0 = r2
        L_0x0126:
            if (r0 != 0) goto L_0x001e
            if (r6 == 0) goto L_0x001e
            int r0 = r6.size()
            if (r0 <= 0) goto L_0x001e
            java.util.Iterator r2 = r6.iterator()
        L_0x0134:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x001e
            java.lang.Object r0 = r2.next()
            com.tencent.assistant.download.DownloadInfo r0 = (com.tencent.assistant.download.DownloadInfo) r0
            com.tencent.assistant.download.SimpleDownloadInfo$UIType r3 = r0.uiType
            if (r3 != r1) goto L_0x0134
            com.tencent.assistant.download.SimpleDownloadInfo$UIType r3 = com.tencent.assistant.download.SimpleDownloadInfo.UIType.WISE_APP_UPDATE
            if (r1 != r3) goto L_0x01c1
            boolean r3 = r10.c(r0)
            if (r3 != 0) goto L_0x0156
            java.lang.String r3 = r0.packageName
            boolean r3 = r10.d(r3)
            if (r3 == 0) goto L_0x01c1
        L_0x0156:
            java.lang.String r3 = "million"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = "WiseDownloadManager.startWifiAutoDownload, 应用"
            java.lang.StringBuilder r4 = r4.append(r6)
            java.lang.String r6 = r0.packageName
            java.lang.StringBuilder r4 = r4.append(r6)
            java.lang.String r6 = "接着下今天以前的任务下载发现它是己忽略的或己卸载的原因，Wifi智能下载记录删除并中止它的下载"
            java.lang.StringBuilder r4 = r4.append(r6)
            java.lang.String r4 = r4.toString()
            com.tencent.assistant.utils.XLog.d(r3, r4)
            com.tencent.assistant.manager.DownloadProxy r3 = com.tencent.assistant.manager.DownloadProxy.a()
            java.lang.String r0 = r0.downloadTicket
            r3.b(r0, r5)
            goto L_0x0134
        L_0x0180:
            com.tencent.assistant.module.update.u r0 = com.tencent.assistant.module.update.u.a()
            java.util.List r0 = r0.d()
            goto L_0x00dc
        L_0x018a:
            com.tencent.assistant.module.update.u r0 = com.tencent.assistant.module.update.u.a()
            java.util.List r0 = r0.f()
            goto L_0x00dc
        L_0x0194:
            com.tencent.assistant.module.update.u r0 = com.tencent.assistant.module.update.u.a()
            java.util.List r0 = r0.h()
            goto L_0x00dc
        L_0x019e:
            com.tencent.assistant.module.update.u r0 = com.tencent.assistant.module.update.u.a()
            java.util.List r0 = r0.i()
            goto L_0x00dc
        L_0x01a8:
            com.tencent.assistant.download.DownloadInfo r0 = com.tencent.assistant.download.DownloadInfo.createDownloadInfo(r0, r9, r1)
            boolean r4 = r10.a(r0, r1)
            if (r4 == 0) goto L_0x00e4
            com.tencent.assistant.download.a r3 = com.tencent.assistant.download.a.a()
            r3.a(r0, r1)
            com.tencent.assistant.module.wisedownload.condition.ThresholdCondition$CONDITION_RESULT_CODE r0 = com.tencent.assistant.module.wisedownload.condition.ThresholdCondition.CONDITION_RESULT_CODE.OK_BEGIN_DOWNLOAD
            com.tencent.assistant.module.wisedownload.m.b(r11, r0, r12)
            r0 = r2
            goto L_0x0126
        L_0x01c1:
            boolean r3 = r10.a(r0, r1)
            if (r3 == 0) goto L_0x0134
            com.tencent.assistant.download.a r2 = com.tencent.assistant.download.a.a()
            r2.b(r0, r1)
            com.tencent.assistant.module.wisedownload.condition.ThresholdCondition$CONDITION_RESULT_CODE r0 = com.tencent.assistant.module.wisedownload.condition.ThresholdCondition.CONDITION_RESULT_CODE.OK_CONTINUE_DOWNLOAD
            com.tencent.assistant.module.wisedownload.m.b(r11, r0, r12)
            goto L_0x001e
        L_0x01d5:
            r0 = r3
            goto L_0x0126
        L_0x01d8:
            r3 = r5
            goto L_0x00cd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.module.wisedownload.j.a(int, int):void");
    }

    public boolean a(DownloadInfo downloadInfo, SimpleDownloadInfo.UIType uIType) {
        if (downloadInfo != null && (uIType == SimpleDownloadInfo.UIType.WISE_SELF_UPDAET || uIType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD || uIType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD || uIType == SimpleDownloadInfo.UIType.WISE_APP_UPDATE || uIType == SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD)) {
            if (uIType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD || uIType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD) {
                if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.SUCC || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLING || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.INSTALLED || a(downloadInfo)) {
                    return false;
                }
                return true;
            } else if (!downloadInfo.isDownloaded() && !a(downloadInfo)) {
                if (uIType != SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD || ApkResourceManager.getInstance().getLocalApkInfo(downloadInfo.packageName) == null) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    public boolean a(DownloadInfo downloadInfo) {
        if (downloadInfo == null || !downloadInfo.isUiTypeWiseDownload()) {
            return false;
        }
        return c(downloadInfo.downloadTicket);
    }

    private boolean b(DownloadInfo downloadInfo) {
        if (downloadInfo == null || !downloadInfo.isUiTypeWiseDownload() || DownloadProxy.a().b(downloadInfo.errorCode) || DownloadProxy.a().c(downloadInfo.errorCode) || DownloadProxy.a().d(downloadInfo.errorCode)) {
            return false;
        }
        return true;
    }

    private boolean c(DownloadInfo downloadInfo) {
        Set<s> e2 = k.b().e();
        s sVar = new s();
        sVar.a(downloadInfo.packageName, downloadInfo.versionName, downloadInfo.versionCode, false);
        if (e2.contains(sVar)) {
            return true;
        }
        return false;
    }

    private boolean d(String str) {
        if (ApkResourceManager.getInstance().getLocalApkInfo(str) == null) {
            return true;
        }
        return false;
    }

    public void a(int i) {
        for (DownloadInfo next : DownloadProxy.a().a(SimpleDownloadInfo.DownloadType.APK)) {
            if (next != null) {
                if (i == 1 && next.uiType == SimpleDownloadInfo.UIType.WISE_SELF_UPDAET) {
                    XLog.d("WiseDownload", " pauseWifiAutoDownload TYPE_WISE_CONTROL_SELF_DOWNLOAD");
                    com.tencent.assistant.download.a.a().b(next.downloadTicket);
                } else if (i == 2 && next.uiType == SimpleDownloadInfo.UIType.WISE_APP_UPDATE) {
                    XLog.d("WiseDownload", " pauseWifiAutoDownload TYPE_WISE_CONTROL_APP_UPDATE ");
                    com.tencent.assistant.download.a.a().b(next.downloadTicket);
                } else if (i == 3 && next.uiType == SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD) {
                    XLog.d("WiseDownload ", " pauseWifiAutoDownload TYPE_WISE_CONTROL_APP_DOWNLOAD ");
                    com.tencent.assistant.download.a.a().b(next.downloadTicket);
                } else if (i == 5 && next.uiType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD) {
                    XLog.d("WiseDownload ", " pauseWifiAutoDownload TYPE_WISE_CONTROL_BOOKING_DOWNLOAD ");
                    com.tencent.assistant.download.a.a().b(next.downloadTicket);
                } else if (i == 6 && next.uiType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD) {
                    XLog.d("WiseDownload ", " pauseWifiAutoDownload TYPE_WISE_CONTROL_SUBCRIPTION_DOWNLOAD ");
                    com.tencent.assistant.download.a.a().b(next.downloadTicket);
                }
            }
        }
    }

    public static int c() {
        DownloadInfo downloadInfo;
        ArrayList<DownloadInfo> c2 = DownloadProxy.a().c();
        if (c2 == null || c2.isEmpty()) {
            return -1;
        }
        Iterator<DownloadInfo> it = c2.iterator();
        while (true) {
            if (!it.hasNext()) {
                downloadInfo = null;
                break;
            }
            downloadInfo = it.next();
            if (downloadInfo != null && downloadInfo.isUiTypeWiseDownload()) {
                if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.QUEUING || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                    break;
                }
            }
        }
        if (downloadInfo == null) {
            return -1;
        }
        if (downloadInfo.uiType == SimpleDownloadInfo.UIType.WISE_SELF_UPDAET) {
            return 1;
        }
        if (downloadInfo.uiType == SimpleDownloadInfo.UIType.WISE_APP_UPDATE) {
            return 2;
        }
        if (downloadInfo.uiType == SimpleDownloadInfo.UIType.WISE_NEW_DOWNLOAD) {
            return 3;
        }
        if (downloadInfo.uiType == SimpleDownloadInfo.UIType.WISE_BOOKING_DOWNLOAD) {
            return 5;
        }
        return downloadInfo.uiType == SimpleDownloadInfo.UIType.WISE_SUBSCRIPTION_DOWNLOAD ? 6 : -1;
    }

    public boolean d() {
        ArrayList<DownloadInfo> d2 = DownloadProxy.a().d();
        if (d2 == null || d2.isEmpty()) {
            return false;
        }
        for (DownloadInfo next : d2) {
            if (next != null && next.isUiTypeWiseDownload()) {
                if (next.downloadState == SimpleDownloadInfo.DownloadState.QUEUING || next.downloadState == SimpleDownloadInfo.DownloadState.DOWNLOADING) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean a(String str) {
        boolean z = false;
        Iterator<SimpleAppModel> it = com.tencent.assistant.module.u.e(u.a().g()).iterator();
        while (true) {
            if (it.hasNext()) {
                if (it.next().c.equals(str)) {
                    z = true;
                    break;
                }
            } else {
                break;
            }
        }
        if (!z) {
            Iterator<SimpleAppModel> it2 = com.tencent.assistant.module.u.e(u.a().c()).iterator();
            while (it2.hasNext()) {
                if (it2.next().c.equals(str)) {
                    return true;
                }
            }
        }
        return z;
    }
}
