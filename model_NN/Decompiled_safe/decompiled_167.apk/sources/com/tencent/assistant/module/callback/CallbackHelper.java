package com.tencent.assistant.module.callback;

import com.tencent.assistant.module.callback.ActionCallback;
import com.tencent.assistant.utils.ba;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;

/* compiled from: ProGuard */
public class CallbackHelper<ICallback extends ActionCallback> {
    protected ReferenceQueue<ICallback> mCallbackReferenceQueue = new ReferenceQueue<>();
    protected ArrayList<WeakReference<ICallback>> mWeakCallbackArrayList = new ArrayList<>();

    /* compiled from: ProGuard */
    public interface Caller<T> {
        void call(T t);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r2 = r3.mWeakCallbackArrayList.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0021, code lost:
        if (r2.hasNext() == false) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        if (((com.tencent.assistant.module.callback.ActionCallback) r2.next().get()) != r4) goto L_0x001d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void register(ICallback r4) {
        /*
            r3 = this;
            if (r4 != 0) goto L_0x0003
        L_0x0002:
            return
        L_0x0003:
            java.util.ArrayList<java.lang.ref.WeakReference<ICallback>> r1 = r3.mWeakCallbackArrayList
            monitor-enter(r1)
        L_0x0006:
            java.lang.ref.ReferenceQueue<ICallback> r0 = r3.mCallbackReferenceQueue     // Catch:{ all -> 0x0014 }
            java.lang.ref.Reference r0 = r0.poll()     // Catch:{ all -> 0x0014 }
            if (r0 == 0) goto L_0x0017
            java.util.ArrayList<java.lang.ref.WeakReference<ICallback>> r2 = r3.mWeakCallbackArrayList     // Catch:{ all -> 0x0014 }
            r2.remove(r0)     // Catch:{ all -> 0x0014 }
            goto L_0x0006
        L_0x0014:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0014 }
            throw r0
        L_0x0017:
            java.util.ArrayList<java.lang.ref.WeakReference<ICallback>> r0 = r3.mWeakCallbackArrayList     // Catch:{ ConcurrentModificationException -> 0x0033 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ ConcurrentModificationException -> 0x0033 }
        L_0x001d:
            boolean r0 = r2.hasNext()     // Catch:{ ConcurrentModificationException -> 0x0033 }
            if (r0 == 0) goto L_0x0034
            java.lang.Object r0 = r2.next()     // Catch:{ ConcurrentModificationException -> 0x0033 }
            java.lang.ref.WeakReference r0 = (java.lang.ref.WeakReference) r0     // Catch:{ ConcurrentModificationException -> 0x0033 }
            java.lang.Object r0 = r0.get()     // Catch:{ ConcurrentModificationException -> 0x0033 }
            com.tencent.assistant.module.callback.ActionCallback r0 = (com.tencent.assistant.module.callback.ActionCallback) r0     // Catch:{ ConcurrentModificationException -> 0x0033 }
            if (r0 != r4) goto L_0x001d
            monitor-exit(r1)     // Catch:{ all -> 0x0014 }
            goto L_0x0002
        L_0x0033:
            r0 = move-exception
        L_0x0034:
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference     // Catch:{ all -> 0x0014 }
            java.lang.ref.ReferenceQueue<ICallback> r2 = r3.mCallbackReferenceQueue     // Catch:{ all -> 0x0014 }
            r0.<init>(r4, r2)     // Catch:{ all -> 0x0014 }
            java.util.ArrayList<java.lang.ref.WeakReference<ICallback>> r2 = r3.mWeakCallbackArrayList     // Catch:{ all -> 0x0014 }
            r2.add(r0)     // Catch:{ all -> 0x0014 }
            monitor-exit(r1)     // Catch:{ all -> 0x0014 }
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.module.callback.CallbackHelper.register(com.tencent.assistant.module.callback.ActionCallback):void");
    }

    public void unregister(ICallback icallback) {
        if (icallback != null) {
            synchronized (this.mWeakCallbackArrayList) {
                try {
                    Iterator<WeakReference<ICallback>> it = this.mWeakCallbackArrayList.iterator();
                    while (it.hasNext()) {
                        WeakReference next = it.next();
                        if (((ActionCallback) next.get()) == icallback) {
                            this.mWeakCallbackArrayList.remove(next);
                            return;
                        }
                    }
                } catch (ConcurrentModificationException e) {
                }
            }
        }
    }

    public void unregisterAll() {
        synchronized (this.mWeakCallbackArrayList) {
            this.mWeakCallbackArrayList.clear();
        }
    }

    public void broadcast(Caller<ICallback> caller) {
        synchronized (this.mWeakCallbackArrayList) {
            try {
                Iterator<WeakReference<ICallback>> it = this.mWeakCallbackArrayList.iterator();
                while (it.hasNext()) {
                    ActionCallback actionCallback = (ActionCallback) it.next().get();
                    if (actionCallback != null) {
                        try {
                            caller.call(actionCallback);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (ConcurrentModificationException e2) {
            }
        }
    }

    public void broadcastInMainThread(Caller<ICallback> caller) {
        ba.a().post(new h(this, caller));
    }

    public void delayBroadcastInMainThread(Caller<ICallback> caller, long j) {
        ba.a().postDelayed(new i(this, caller), j);
    }
}
