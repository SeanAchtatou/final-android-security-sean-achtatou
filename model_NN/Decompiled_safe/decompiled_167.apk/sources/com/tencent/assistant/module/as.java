package com.tencent.assistant.module;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.TemporaryThreadManager;
import java.util.ArrayList;

/* compiled from: ProGuard */
class as implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ al f1703a;
    private int b = 0;
    /* access modifiers changed from: private */
    public byte[] c;
    private long d;
    /* access modifiers changed from: private */
    public boolean e = true;
    private ArrayList<SimpleAppModel> f;
    private byte[] g;
    /* access modifiers changed from: private */
    public int h = -1;

    public as(al alVar) {
        this.f1703a = alVar;
    }

    public void a(byte[] bArr) {
        if (bArr != null && bArr.length != 0) {
            b();
            this.b = 1;
            this.c = bArr;
            this.h = this.f1703a.getUniqueId();
            TemporaryThreadManager.get().start(new at(this));
        }
    }

    public int a() {
        return this.h;
    }

    public void b() {
        this.h = -1;
        this.c = null;
        this.e = true;
        this.g = null;
        this.f = null;
        this.b = 0;
    }

    public boolean c() {
        return this.b == 1;
    }

    public byte[] d() {
        return this.c;
    }

    public ArrayList<SimpleAppModel> e() {
        return this.f;
    }

    public boolean f() {
        return this.e;
    }

    public byte[] g() {
        return this.g;
    }

    public long h() {
        return this.d;
    }

    public void a(long j, ArrayList<SimpleAppModel> arrayList, boolean z, byte[] bArr) {
        this.d = j;
        this.f = arrayList;
        this.e = z;
        this.g = bArr;
        this.b = 2;
    }

    public void i() {
        this.b = 2;
    }

    /* renamed from: j */
    public as clone() {
        try {
            as asVar = (as) super.clone();
            if (this.f == null) {
                return asVar;
            }
            asVar.f = (ArrayList) this.f.clone();
            return asVar;
        } catch (CloneNotSupportedException e2) {
            return this;
        }
    }
}
