package com.tencent.assistant.module.timer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.b.am;

/* compiled from: ProGuard */
public class RecoverAppListReceiver extends BroadcastReceiver {
    static a b;

    /* renamed from: a  reason: collision with root package name */
    Context f1858a;

    /* compiled from: ProGuard */
    public enum RecoverAppListState {
        NOMAL,
        FIRST_SHORT,
        SECONDLY_SHORT
    }

    public void onReceive(Context context, Intent intent) {
        this.f1858a = context;
        XLog.e("zhangyuanchao", "---------RecoverAppListReceiver----------");
        if (intent.getAction().equals("com.tencent.assistant.timer.revcover.app")) {
            a();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        am amVar = new am();
        b = new a(this);
        amVar.register(b);
        amVar.a(false);
        amVar.a();
    }
}
