package com.tencent.assistant.module;

import android.os.Build;
import android.util.Pair;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.Global;
import com.tencent.assistant.db.table.q;
import com.tencent.assistant.m;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.model.j;
import com.tencent.assistant.module.callback.r;
import com.tencent.assistant.plugin.mgr.a;
import com.tencent.assistant.plugin.mgr.e;
import com.tencent.assistant.protocol.jce.GetPluginListRequest;
import com.tencent.assistant.protocol.jce.GetPluginListResponse;
import com.tencent.assistant.protocol.jce.MAPlugin;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: ProGuard */
public class dm extends BaseEngine<r> implements eb {

    /* renamed from: a  reason: collision with root package name */
    private static dm f1778a;
    private long b = m.a().v();
    private int c;
    private q d;
    /* access modifiers changed from: private */
    public List<j> e = null;
    private Set<String> f = null;

    public static synchronized dm a() {
        dm dmVar;
        synchronized (dm.class) {
            if (f1778a == null) {
                f1778a = new dm();
            }
            dmVar = f1778a;
        }
        return dmVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006a, code lost:
        if (com.qq.AppService.AstApp.d() != false) goto L_0x006c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private dm() {
        /*
            r4 = this;
            r0 = 0
            r4.<init>()
            r4.e = r0
            r4.f = r0
            com.tencent.assistant.m r0 = com.tencent.assistant.m.a()
            long r0 = r0.v()
            r4.b = r0
            com.tencent.assistant.module.dy r0 = com.tencent.assistant.module.dy.a()
            r0.a(r4)
            com.tencent.assistant.db.table.q r0 = new com.tencent.assistant.db.table.q
            r0.<init>()
            r4.d = r0
            com.tencent.assistant.db.table.q r0 = r4.d
            java.util.List r0 = r0.a()
            if (r0 == 0) goto L_0x003e
            int r1 = r0.size()
            if (r1 <= 0) goto L_0x003e
            android.util.Pair r1 = r4.a(r0)
            java.lang.Object r0 = r1.first
            java.util.List r0 = (java.util.List) r0
            r4.e = r0
            java.lang.Object r0 = r1.second
            java.util.Set r0 = (java.util.Set) r0
            r4.f = r0
        L_0x003e:
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            boolean r0 = r0.s()
            if (r0 != 0) goto L_0x0049
        L_0x0048:
            return
        L_0x0049:
            java.util.List<com.tencent.assistant.model.j> r0 = r4.e
            if (r0 == 0) goto L_0x006c
            java.util.List<com.tencent.assistant.model.j> r0 = r4.e
            int r0 = r0.size()
            if (r0 == 0) goto L_0x006c
            long r0 = r4.b
            r2 = -1
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x006c
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            if (r0 == 0) goto L_0x0072
            com.qq.AppService.AstApp.i()
            boolean r0 = com.qq.AppService.AstApp.d()
            if (r0 == 0) goto L_0x0072
        L_0x006c:
            r0 = 0
            r4.a(r0)
            goto L_0x0048
        L_0x0072:
            long r0 = r4.b
            r4.a(r0)
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.module.dm.<init>():void");
    }

    private Pair<List<j>, Set<String>> a(List<j> list) {
        ArrayList arrayList = new ArrayList(list.size());
        HashSet hashSet = new HashSet(0);
        for (j next : list) {
            if (a.a(next)) {
                arrayList.add(next);
            } else {
                hashSet.add(next.c);
            }
        }
        return Pair.create(arrayList, hashSet);
    }

    public int a(long j) {
        this.b = j;
        return b(j);
    }

    public int b(long j) {
        notifyDataChangedInMainThread(new dn(this));
        GetPluginListRequest getPluginListRequest = new GetPluginListRequest();
        getPluginListRequest.d((int) j);
        getPluginListRequest.b(Global.getAppVersionCode());
        getPluginListRequest.c(Build.VERSION.SDK_INT);
        getPluginListRequest.a(e.b());
        this.c = send(getPluginListRequest);
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        String str;
        if (i == this.c) {
            GetPluginListResponse getPluginListResponse = (GetPluginListResponse) jceStruct2;
            ArrayList<MAPlugin> a2 = getPluginListResponse.a();
            long b2 = getPluginListResponse.b();
            if (a2 != null && a2.size() != 0 && (b2 != this.b || a2.size() != 0)) {
                if (a2 != null && a2.size() > 0) {
                    List<j> list = this.e;
                    HashSet hashSet = new HashSet(a2.size());
                    ArrayList arrayList = new ArrayList(a2.size());
                    HashSet hashSet2 = new HashSet(0);
                    for (MAPlugin next : a2) {
                        j jVar = new j();
                        jVar.f1666a = next.f2233a;
                        jVar.h = next.b;
                        jVar.e = next.c;
                        jVar.f = next.d;
                        jVar.j = next.f;
                        jVar.d = next.g;
                        jVar.l = next.h;
                        jVar.o = next.i;
                        jVar.r = next.j;
                        if (next.u == null) {
                            str = Constants.STR_EMPTY;
                        } else {
                            str = next.u.f1970a;
                        }
                        jVar.s = str;
                        jVar.p = next.l;
                        jVar.c = next.m;
                        jVar.g = next.n;
                        jVar.k = next.o;
                        jVar.m = next.p;
                        jVar.n = next.q;
                        jVar.i = next.r;
                        jVar.b = jVar.a();
                        jVar.q = next.t;
                        this.d.a(jVar);
                        hashSet.add(Integer.valueOf(next.f2233a));
                        if (a.a(jVar)) {
                            arrayList.add(jVar);
                        } else {
                            hashSet2.add(jVar.c);
                        }
                    }
                    try {
                        Collections.sort(arrayList);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    if (list != null && list.size() > 0) {
                        for (j next2 : list) {
                            if (!hashSet.contains(Integer.valueOf(next2.f1666a))) {
                                this.d.a(next2.f1666a);
                            }
                        }
                    }
                    this.e = arrayList;
                    this.f = hashSet2;
                }
                this.b = b2;
                m.a().a(this.b);
                notifyDataChangedInMainThread(new dp(this));
            } else if (this.e != null && this.e.size() > 0) {
                notifyDataChangedInMainThread(new Cdo(this));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new dq(this, i2));
    }

    public void d() {
        long a2 = m.a().a((byte) 6);
        if (a2 != -11 && this.b != -1 && this.b != a2) {
            b(this.b);
        }
    }

    public void a(ArrayList<QuickEntranceNotify> arrayList) {
    }

    public List<j> b() {
        return this.e;
    }

    public j a(int i) {
        if (this.e != null && this.e.size() > 0) {
            for (j next : this.e) {
                if (next.f1666a == i) {
                    return next;
                }
            }
        }
        return null;
    }

    public j a(String str) {
        if (this.e != null && this.e.size() > 0) {
            for (j next : this.e) {
                if (next.a() != null && next.a().equals(str)) {
                    return next;
                }
            }
        }
        return null;
    }

    public j b(String str) {
        if (this.e != null && this.e.size() > 0) {
            for (j next : this.e) {
                if (next.c != null && next.c.equals(str)) {
                    return next;
                }
            }
        }
        return null;
    }

    public boolean c(String str) {
        return this.f == null || !this.f.contains(str);
    }
}
