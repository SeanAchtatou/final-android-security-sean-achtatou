package com.tencent.assistant.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.k;
import com.tencent.assistant.protocol.jce.ModifyAppCommentRequest;

/* compiled from: ProGuard */
class el implements CallbackHelper.Caller<k> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1798a;
    final /* synthetic */ ModifyAppCommentRequest b;
    final /* synthetic */ ek c;

    el(ek ekVar, int i, ModifyAppCommentRequest modifyAppCommentRequest) {
        this.c = ekVar;
        this.f1798a = i;
        this.b = modifyAppCommentRequest;
    }

    /* renamed from: a */
    public void call(k kVar) {
        kVar.a(this.f1798a, 0, this.b.f2235a, this.b.b, this.b.d, System.currentTimeMillis());
    }
}
