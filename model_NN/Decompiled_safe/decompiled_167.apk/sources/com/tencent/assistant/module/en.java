package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.w;
import com.tencent.assistant.protocol.jce.CardItem;
import com.tencent.assistant.protocol.jce.GetOneMoreAppRequest;
import com.tencent.assistant.protocol.jce.GetOneMoreAppResponse;
import com.tencent.assistant.utils.ba;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class en extends BaseEngine<w> {

    /* renamed from: a  reason: collision with root package name */
    private static en f1800a;
    /* access modifiers changed from: private */
    public int b;

    public static synchronized en a() {
        en enVar;
        synchronized (en.class) {
            if (f1800a == null) {
                f1800a = new en();
            }
            enVar = f1800a;
        }
        return enVar;
    }

    public int a(int i, long j) {
        this.b = send(new GetOneMoreAppRequest(i, j));
        ba.a().postDelayed(new eo(this, this.b), 5000);
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        GetOneMoreAppResponse getOneMoreAppResponse;
        ArrayList<CardItem> arrayList;
        if (i == this.b && (getOneMoreAppResponse = (GetOneMoreAppResponse) jceStruct2) != null && (arrayList = getOneMoreAppResponse.b) != null) {
            notifyDataChangedInMainThread(new er(this, u.a(arrayList, new eq(this), getOneMoreAppResponse.a())));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        if (i == this.b) {
            notifyDataChangedInMainThread(new es(this, i2));
        }
    }
}
