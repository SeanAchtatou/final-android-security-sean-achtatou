package com.tencent.assistant.module;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.k;
import com.tencent.assistant.protocol.jce.AnswerAppCommentRequest;
import com.tencent.assistant.utils.XLog;
import java.util.HashMap;

/* compiled from: ProGuard */
public class a extends BaseEngine<k> {

    /* renamed from: a  reason: collision with root package name */
    private static a f1679a;
    private HashMap<Long, Integer> b = new HashMap<>();

    public static a a() {
        if (f1679a == null) {
            f1679a = new a();
        }
        return f1679a;
    }

    public void a(int i, long j, String str, String str2, String str3, long j2, String str4) {
        if (this.b.containsKey(Long.valueOf(j))) {
            cancel(this.b.get(Long.valueOf(j)).intValue());
            this.b.remove(Long.valueOf(j));
        }
        AnswerAppCommentRequest answerAppCommentRequest = new AnswerAppCommentRequest();
        answerAppCommentRequest.b = i;
        answerAppCommentRequest.f1974a = j;
        answerAppCommentRequest.e = str;
        answerAppCommentRequest.d = str2;
        answerAppCommentRequest.f = str3;
        answerAppCommentRequest.g = j2;
        answerAppCommentRequest.h = str4;
        XLog.d("comment", "AnswerAppCommentEngine.sendRequest, AnswerAppCommentRequest=" + answerAppCommentRequest.toString());
        this.b.put(Long.valueOf(j), Integer.valueOf(send(answerAppCommentRequest)));
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        AnswerAppCommentRequest answerAppCommentRequest = (AnswerAppCommentRequest) jceStruct;
        this.b.remove(Long.valueOf(answerAppCommentRequest.f1974a));
        XLog.d("comment", "AnswerAppCommentEngine.onRequestSuccessed, AnswerAppCommentRequest=" + answerAppCommentRequest.toString());
        notifyDataChangedInMainThread(new b(this, i, answerAppCommentRequest));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        XLog.d("comment", "AnswerAppCommentEngine.onRequestFailed, errorCode=" + i2);
        this.b.remove(Long.valueOf(((AnswerAppCommentRequest) jceStruct).f1974a));
        notifyDataChangedInMainThread(new c(this, i, i2));
    }
}
