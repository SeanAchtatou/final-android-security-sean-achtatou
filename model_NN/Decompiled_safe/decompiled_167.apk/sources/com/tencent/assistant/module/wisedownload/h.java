package com.tencent.assistant.module.wisedownload;

import com.tencent.assistant.module.wisedownload.condition.d;
import com.tencent.assistant.module.wisedownload.condition.l;

/* compiled from: ProGuard */
public class h extends b {
    public h() {
        a();
    }

    public void a() {
        this.d = new l(this);
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.b = new d(this);
    }

    public boolean b() {
        return true;
    }

    public void a(o oVar) {
        int i;
        int i2;
        int i3;
        boolean z;
        boolean z2;
        int i4 = 0;
        if (oVar != null) {
            if (this.d == null || !(this.d instanceof l)) {
                i = 0;
                i2 = 0;
                i3 = 0;
                z = false;
                z2 = false;
            } else {
                l lVar = (l) this.d;
                z2 = lVar.h();
                z = lVar.i();
                i3 = lVar.l();
                i2 = lVar.m();
                i = lVar.j();
                i4 = lVar.k();
            }
            oVar.b(z2, z, i3, i2, i, i4);
        }
    }
}
