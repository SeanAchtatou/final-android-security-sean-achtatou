package com.tencent.assistant.module;

import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.m;
import com.tencent.assistant.protocol.jce.AppDetailParam;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.protocol.jce.GetAppSimpleDetailRequest;
import com.tencent.assistant.protocol.jce.GetAppSimpleDetailResponse;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class ct extends BaseEngine<m> {
    public int a(List<SimpleAppModel> list) {
        if (list == null) {
            return -1;
        }
        GetAppSimpleDetailRequest getAppSimpleDetailRequest = new GetAppSimpleDetailRequest();
        ArrayList<AppDetailParam> arrayList = new ArrayList<>();
        for (SimpleAppModel next : list) {
            AppDetailParam appDetailParam = new AppDetailParam();
            appDetailParam.f1992a = next.f1634a;
            appDetailParam.b = next.c;
            if (!TextUtils.isEmpty(next.m)) {
                appDetailParam.c = next.m;
            }
            appDetailParam.h = next.g;
            appDetailParam.g = next.b;
            appDetailParam.i = next.ac;
            appDetailParam.k = next.ad;
            appDetailParam.j = next.Q;
            appDetailParam.d = next.D;
            if (!TextUtils.isEmpty(next.ak)) {
                appDetailParam.e = next.ak;
            }
            arrayList.add(appDetailParam);
        }
        if (arrayList == null || arrayList.size() <= 0) {
            return -1;
        }
        getAppSimpleDetailRequest.f2085a = arrayList;
        return send(getAppSimpleDetailRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new cu(this, i, a((GetAppSimpleDetailResponse) jceStruct2)));
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new cv(this, i, i2));
    }

    private List<AppSimpleDetail> a(GetAppSimpleDetailResponse getAppSimpleDetailResponse) {
        if (getAppSimpleDetailResponse != null) {
            return getAppSimpleDetailResponse.b;
        }
        return null;
    }
}
