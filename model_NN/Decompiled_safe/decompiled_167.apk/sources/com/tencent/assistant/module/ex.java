package com.tencent.assistant.module;

import android.text.TextUtils;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.z;
import com.tencent.assistant.protocol.jce.GetRecommendAppListRequest;
import com.tencent.assistant.protocol.jce.GetRecommendAppListResponse;
import com.tencent.assistant.protocol.jce.InstalledAppItem;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class ex extends BaseEngine<z> {
    public int a(int i, InstalledAppItem installedAppItem, ArrayList<InstalledAppItem> arrayList, ArrayList<InstalledAppItem> arrayList2, byte b, String str) {
        GetRecommendAppListRequest getRecommendAppListRequest = new GetRecommendAppListRequest();
        getRecommendAppListRequest.f2155a = i;
        getRecommendAppListRequest.b = installedAppItem;
        getRecommendAppListRequest.c = arrayList;
        getRecommendAppListRequest.d = arrayList2;
        getRecommendAppListRequest.e = b;
        if (!TextUtils.isEmpty(str)) {
            try {
                getRecommendAppListRequest.g = Byte.valueOf(str).byteValue();
            } catch (NumberFormatException e) {
            }
        }
        return send(getRecommendAppListRequest);
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        if (jceStruct2 != null) {
            notifyDataChangedInMainThread(new ey(this, i, (GetRecommendAppListResponse) jceStruct2));
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new ez(this, i, i2));
    }
}
