package com.tencent.assistant.component.slidingdrawer;

import android.view.View;

/* compiled from: ProGuard */
class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SlidingDrawerFrameLayout f1112a;

    private c(SlidingDrawerFrameLayout slidingDrawerFrameLayout) {
        this.f1112a = slidingDrawerFrameLayout;
    }

    /* synthetic */ c(SlidingDrawerFrameLayout slidingDrawerFrameLayout, a aVar) {
        this(slidingDrawerFrameLayout);
    }

    public void onClick(View view) {
        if (!this.f1112a.p) {
            int intValue = ((Integer) view.getTag()).intValue();
            if (this.f1112a.k > -1) {
                this.f1112a.closeGroup(intValue, true);
                return;
            }
            if (this.f1112a.k == intValue) {
                this.f1112a.closeGroup(intValue, true);
            } else {
                this.f1112a.expandGroup(intValue);
            }
            if (this.f1112a.h != null) {
                this.f1112a.h.onClick(view);
            }
        }
    }
}
