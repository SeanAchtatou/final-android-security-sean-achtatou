package com.tencent.assistant.component;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.connector.ipc.a;

/* compiled from: ProGuard */
public class PhotoBackupBottomView extends LinearLayout {
    /* access modifiers changed from: private */
    public Activity activity;
    private Button btn_backup;
    private Context context;
    private View.OnClickListener listener = new bl(this);

    public PhotoBackupBottomView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        init();
    }

    public PhotoBackupBottomView(Context context2) {
        super(context2);
        this.context = context2;
        init();
    }

    public void setActivity(Activity activity2) {
        this.activity = activity2;
    }

    public void show(boolean z) {
        if (z) {
            setVisibility(0);
        } else {
            setVisibility(8);
        }
    }

    public void updateBackupCount(int i) {
        if (i > 0) {
            if (this.btn_backup != null) {
                this.btn_backup.setText(String.format(this.context.getString(R.string.photo_backup_x), Integer.valueOf(i)));
                this.btn_backup.setEnabled(true);
            }
        } else if (this.btn_backup != null) {
            this.btn_backup.setText(this.context.getString(R.string.photo_backup));
            this.btn_backup.setEnabled(false);
        }
    }

    public void setBackupStart() {
        if (this.btn_backup != null) {
            this.btn_backup.setText(this.context.getString(R.string.photo_backuping));
            this.btn_backup.setEnabled(false);
        }
    }

    public void setBackupFailed() {
        reset();
    }

    public void setBackupFinish() {
        reset();
    }

    public void resetBackupStatus() {
        reset();
    }

    public void reset() {
        if (this.btn_backup != null) {
            this.btn_backup.setEnabled(false);
            this.btn_backup.setText(this.context.getString(R.string.photo_backup));
        }
    }

    public void setBacupEnable(boolean z) {
        if (this.btn_backup == null) {
            return;
        }
        if (!a.f3550a) {
            if (z) {
                this.btn_backup.setEnabled(true);
            } else {
                this.btn_backup.setEnabled(false);
            }
            this.btn_backup.setText(this.context.getString(R.string.photo_backup));
        } else if (z) {
            this.btn_backup.setEnabled(false);
            this.btn_backup.setText(this.context.getString(R.string.photo_backuping));
        }
    }

    private void init() {
        this.btn_backup = (Button) LayoutInflater.from(this.context).inflate((int) R.layout.photo_backup_bottom_function, this).findViewById(R.id.btn_backup);
        this.btn_backup.setOnClickListener(this.listener);
    }
}
