package com.tencent.assistant.component;

import android.view.MotionEvent;
import com.tencent.assistant.component.TouchAnalizer;

/* compiled from: ProGuard */
public class SingleClickBehavior extends TouchBehavior {
    private static final int CLICK_AREA = 400;
    private float lastPosX = -1.0f;
    private float lastPosY = -1.0f;

    public SingleClickBehavior(TouchAnalizer touchAnalizer) {
        super(touchAnalizer);
        this.type = TouchAnalizer.BehaviorType.SINGLE_CLICK;
    }

    private boolean checkOutside(MotionEvent motionEvent) {
        if (motionEvent == null || this.lastPosX == -1.0f || this.lastPosY == -1.0f) {
            return true;
        }
        float x = this.lastPosX - motionEvent.getX();
        float y = this.lastPosY - motionEvent.getY();
        if ((x * x) + (y * y) <= 400.0f) {
            return false;
        }
        return true;
    }

    public int analizeTouchEvent(MotionEvent motionEvent) {
        int i = 0;
        switch (motionEvent.getAction()) {
            case 0:
                this.lastPosX = motionEvent.getX();
                this.lastPosY = motionEvent.getY();
                if (this.judger != null) {
                    i = this.judger.judgeEvent(this.type, this.lastPosX, this.lastPosY, 0);
                    break;
                }
                break;
            case 1:
                if (!checkOutside(motionEvent)) {
                    i = 2;
                    break;
                } else {
                    i = 1;
                    break;
                }
            case 2:
                if (checkOutside(motionEvent)) {
                    i = 1;
                    break;
                }
                break;
            default:
                i = 1;
                break;
        }
        if (i == 1 || i == 2) {
            this.lastPosY = -1.0f;
            this.lastPosX = -1.0f;
        }
        return i;
    }
}
