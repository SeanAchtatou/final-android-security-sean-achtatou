package com.tencent.assistant.component;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.AppUpdateIgnoreListAdapter;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.protocol.jce.StatUpdateManageAction;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class UpdateIgnoreListView extends RelativeLayout {
    private final int INIT_DATA;
    private final int REFRESH_DATA;
    private AstApp mApp;
    private Context mContext;
    private LayoutInflater mInflater;
    private View mRootView;
    public AppUpdateIgnoreListAdapter mUpdateIgnoreListAdapter;
    private TXGetMoreListView mUpdateIgnoreListView;
    private Handler myHandler;
    private StatUpdateManageAction updateAction;

    public UpdateIgnoreListView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, null);
    }

    public UpdateIgnoreListView(Context context, StatUpdateManageAction statUpdateManageAction) {
        this(context, null, statUpdateManageAction);
    }

    public UpdateIgnoreListView(Context context, AttributeSet attributeSet, StatUpdateManageAction statUpdateManageAction) {
        super(context, attributeSet);
        this.INIT_DATA = 1;
        this.REFRESH_DATA = 2;
        this.myHandler = new dw(this);
        this.updateAction = statUpdateManageAction;
        this.mApp = AstApp.i();
        this.mContext = context;
        this.mApp.j();
        this.mInflater = (LayoutInflater) this.mContext.getSystemService("layout_inflater");
        initView();
    }

    private void initView() {
        this.mRootView = this.mInflater.inflate((int) R.layout.updateignorelist_component, this);
        this.mUpdateIgnoreListView = (TXGetMoreListView) this.mRootView.findViewById(R.id.updateignorelist);
        this.mUpdateIgnoreListAdapter = new AppUpdateIgnoreListAdapter(this.mContext, this.mUpdateIgnoreListView, this.updateAction);
        ImageView imageView = new ImageView(getContext());
        imageView.setLayoutParams(new AbsListView.LayoutParams(-1, df.a(getContext(), 4.0f)));
        imageView.setBackgroundColor(getResources().getColor(17170445));
        this.mUpdateIgnoreListView.addHeaderView(imageView);
        this.mUpdateIgnoreListView.setDivider(null);
        this.mUpdateIgnoreListView.setAdapter(this.mUpdateIgnoreListAdapter);
    }

    public void notifyChange() {
        if (this.mUpdateIgnoreListAdapter != null) {
            this.mUpdateIgnoreListAdapter.notifyDataSetChanged();
        }
    }

    public void initData(String str) {
        this.mUpdateIgnoreListAdapter.a(str);
        Message obtainMessage = this.myHandler.obtainMessage();
        obtainMessage.what = 1;
        this.myHandler.removeMessages(1);
        this.myHandler.sendMessage(obtainMessage);
    }

    public void refreshData() {
        Message obtainMessage = this.myHandler.obtainMessage();
        obtainMessage.what = 2;
        this.myHandler.removeMessages(2);
        this.myHandler.sendMessage(obtainMessage);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (this.mUpdateIgnoreListAdapter.c == null || !this.mUpdateIgnoreListAdapter.c.isShowing()) {
                    return false;
                }
                this.mUpdateIgnoreListAdapter.c.dismiss();
                return true;
            default:
                return false;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return true;
    }
}
