package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.AppListView;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.module.k;

/* compiled from: ProGuard */
public class AppListPage extends RelativeLayout implements AppListView.ApplistRefreshListener {
    private NormalErrorRecommendPage errorPage;
    private LayoutInflater inflater;
    private AppListView listView;
    private LoadingView loadingView;
    private Context mContext;
    private View.OnClickListener refreshClick = new i(this);

    public AppListPage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initView(context);
        this.mContext = context;
    }

    public AppListPage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initView(context);
        this.mContext = context;
    }

    public AppListPage(Context context, TXScrollViewBase.ScrollMode scrollMode, k kVar) {
        super(context);
        initView(context);
        this.mContext = context;
        this.listView.setmAppEngine(kVar);
        this.listView.registerRefreshListener(this);
        this.listView.setDivider(null);
    }

    private void initView(Context context) {
        this.inflater = LayoutInflater.from(context);
        View inflate = this.inflater.inflate((int) R.layout.applist_component_view, this);
        this.listView = (AppListView) inflate.findViewById(R.id.applist);
        this.listView.setVisibility(8);
        this.loadingView = (LoadingView) inflate.findViewById(R.id.loading_view);
        this.loadingView.setVisibility(0);
        this.errorPage = (NormalErrorRecommendPage) inflate.findViewById(R.id.error_page);
        this.errorPage.setButtonClickListener(this.refreshClick);
    }

    public ListView getListView() {
        return this.listView.getListView();
    }

    public boolean isScrollStateIdle() {
        return this.listView.isScrollStateIdle();
    }

    public void setAdapter(BaseAdapter baseAdapter) {
        this.listView.setAdapter(baseAdapter);
    }

    public void loadFirstPage() {
        this.listView.loadFirstPage();
    }

    public void setViewPageListener(ViewPageScrollListener viewPageScrollListener) {
        this.listView.setViewPageListener(viewPageScrollListener);
    }

    public void onErrorHappened(int i) {
        this.loadingView.setVisibility(8);
        this.listView.setVisibility(8);
        this.errorPage.setVisibility(0);
        this.errorPage.setErrorType(i);
    }

    public void onNetworkNoError() {
        this.listView.setVisibility(0);
        this.errorPage.setVisibility(8);
        this.loadingView.setVisibility(8);
    }

    public void onNextPageLoadFailed() {
    }

    public void onNetworkLoading() {
        this.loadingView.setVisibility(0);
        this.listView.setVisibility(8);
        this.errorPage.setVisibility(8);
    }

    public void recycleData() {
        this.listView.recycleData();
    }

    public void onPause() {
        this.listView.onPause();
    }

    public void onResume() {
        this.listView.onResume();
    }

    public void removeTopPadding() {
        this.listView.removeTopPadding();
    }
}
