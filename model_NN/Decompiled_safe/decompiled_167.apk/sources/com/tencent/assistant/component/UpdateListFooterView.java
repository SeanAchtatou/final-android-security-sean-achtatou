package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.utils.df;

/* compiled from: ProGuard */
public class UpdateListFooterView extends TXLoadingLayoutBase {
    private static final String TMA_ST_UPDATE_BTN_TAG = "08_001";
    public static final int TYPE_HIDE = 3;
    public static final int TYPE_SHOW = 1;
    public static final int TYPE_SHOW_UPDATELIST_EMPTY = 2;
    private int ViewHeightDp = 60;
    private Context context;
    private LinearLayout layout;
    private TextView viewIgnores;

    public UpdateListFooterView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
    }

    public UpdateListFooterView(Context context2, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context2, scrollDirection, scrollMode);
        this.context = context2;
        initLayout(context2);
    }

    private void initLayout(Context context2) {
        View inflate = LayoutInflater.from(context2).inflate((int) R.layout.updatelist_footer_layout, this);
        this.viewIgnores = (TextView) inflate.findViewById(R.id.updatelist_footer_text);
        this.viewIgnores.setTag(R.id.tma_st_slot_tag, TMA_ST_UPDATE_BTN_TAG);
        this.layout = (LinearLayout) inflate.findViewById(R.id.layout_id);
        reset();
    }

    public void setFooterViewText(String str) {
        this.viewIgnores.setText(str);
    }

    public void setFooterViewBackground(int i) {
        this.layout.setBackgroundResource(i);
        this.ViewHeightDp = 50;
    }

    public void setFooterViewOnclickListner(View.OnClickListener onClickListener) {
        this.layout.setOnClickListener(onClickListener);
    }

    public void freshState(int i) {
        switch (i) {
            case 1:
            case 2:
                setClickable(false);
                setEnabled(false);
                this.viewIgnores.setVisibility(0);
                this.layout.getLayoutParams().height = df.a(this.context, (float) this.ViewHeightDp);
                this.layout.setVisibility(0);
                setVisibility(0);
                break;
            case 3:
                setClickable(false);
                setEnabled(false);
                this.viewIgnores.setVisibility(8);
                this.layout.setVisibility(8);
                setVisibility(8);
                break;
        }
        requestLayout();
    }

    public void reset() {
    }

    public void pullToRefresh() {
    }

    public void releaseToRefresh() {
    }

    public void refreshing() {
    }

    public void loadFinish(String str) {
    }

    public void onPull(int i) {
    }

    public void hideAllSubViews() {
    }

    public void showAllSubViews() {
    }

    public void setWidth(int i) {
        getLayoutParams().width = i;
        requestLayout();
    }

    public void setHeight(int i) {
        getLayoutParams().height = i;
        requestLayout();
    }

    public int getContentSize() {
        return this.layout.getHeight();
    }

    public int getTriggerSize() {
        return getResources().getDimensionPixelSize(R.dimen.tx_pull_to_refresh_trigger_size);
    }

    public void refreshSuc() {
    }

    public void refreshFail(String str) {
    }

    public void loadSuc() {
    }

    public void loadFail() {
    }
}
