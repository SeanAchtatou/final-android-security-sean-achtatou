package com.tencent.assistant.component;

import android.view.View;

/* compiled from: ProGuard */
class du implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TotalTabLayout f1029a;

    private du(TotalTabLayout totalTabLayout) {
        this.f1029a = totalTabLayout;
    }

    /* synthetic */ du(TotalTabLayout totalTabLayout, dr drVar) {
        this(totalTabLayout);
    }

    public void onClick(View view) {
        int id = view.getId();
        for (int i = 0; i < this.f1029a.mTabStrArray.length; i++) {
            if (this.f1029a.getTabView(i) != null && id == this.f1029a.getTabView(i).getId()) {
                this.f1029a.selectTab(i, true);
                if (this.f1029a.viewClickListener != null) {
                    this.f1029a.viewClickListener.onClick(view);
                }
            }
        }
    }
}
