package com.tencent.assistant.component.listview;

import com.tencent.assistant.component.listview.AnimationExpandableListView;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class n implements AnimationExpandableListView.ItemAnimatorLisenter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f1094a;

    n(m mVar) {
        this.f1094a = mVar;
    }

    public void onAnimatorEnd() {
        XLog.d("ManagerGeneralController", "onAnimatorEnd--onOneItemFinished", new Throwable("xxx"));
        this.f1094a.b.onOneItemFinished(this.f1094a.c);
        this.f1094a.d.f1079a.addNextIndex();
        this.f1094a.d.a(this.f1094a.b);
    }
}
