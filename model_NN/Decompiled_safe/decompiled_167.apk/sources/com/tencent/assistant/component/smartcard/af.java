package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistantv2.model.SimpleVideoModel;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class af extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchSmartCardVideoRelateItem f1131a;
    private Context b;
    private int c;
    private SimpleVideoModel d;

    public af(SearchSmartCardVideoRelateItem searchSmartCardVideoRelateItem, Context context, int i, SimpleVideoModel simpleVideoModel) {
        this.f1131a = searchSmartCardVideoRelateItem;
        this.b = context;
        this.c = i;
        this.d = simpleVideoModel;
    }

    public void onTMAClick(View view) {
        if (this.d != null) {
            b.a(this.b, this.d.l);
        }
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f1131a.getContext(), 200);
        buildSTInfo.slotId = a.a(Constants.VIA_REPORT_TYPE_SHARE_TO_QQ, this.c);
        if (this.f1131a.m != null) {
            buildSTInfo.searchId = this.f1131a.m.c();
            buildSTInfo.extraData = this.f1131a.m.d();
        }
        buildSTInfo.status = "0" + (this.c + 2);
        return buildSTInfo;
    }
}
