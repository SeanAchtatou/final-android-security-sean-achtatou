package com.tencent.assistant.component;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
class ar extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HomePageBanner f947a;

    ar(HomePageBanner homePageBanner) {
        this.f947a = homePageBanner;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 888:
                if (this.f947a.playing) {
                    this.f947a.mHorizonScrollLayout.displayNextScreen();
                }
                if (this.f947a.handler != null) {
                    this.f947a.handler.sendEmptyMessageDelayed(888, 5000);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
