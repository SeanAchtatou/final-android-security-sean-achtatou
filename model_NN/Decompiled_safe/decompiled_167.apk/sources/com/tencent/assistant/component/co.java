package com.tencent.assistant.component;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.m;
import com.tencent.assistant.model.QuickEntranceNotify;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.Iterator;

/* compiled from: ProGuard */
class co extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ImageView f997a;
    final /* synthetic */ int b;
    final /* synthetic */ ActionUrl c;
    final /* synthetic */ String d;
    final /* synthetic */ QuickEntranceView e;

    co(QuickEntranceView quickEntranceView, ImageView imageView, int i, ActionUrl actionUrl, String str) {
        this.e = quickEntranceView;
        this.f997a = imageView;
        this.b = i;
        this.c = actionUrl;
        this.d = str;
    }

    public void onTMAClick(View view) {
        this.f997a.clearAnimation();
        if (this.e.notifyCache != null) {
            Iterator it = this.e.notifyCache.iterator();
            while (it.hasNext()) {
                QuickEntranceNotify quickEntranceNotify = (QuickEntranceNotify) it.next();
                if (quickEntranceNotify.f1631a == this.b) {
                    quickEntranceNotify.d = 2;
                    quickEntranceNotify.c = System.currentTimeMillis() + m.a().A();
                    this.e.saveToDB(this.e.notifyCache);
                }
            }
        }
        Bundle bundle = new Bundle();
        if (this.e.context instanceof BaseActivity) {
            bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.e.context).f());
        }
        bundle.putSerializable("com.tencent.assistant.ACTION_URL", this.c);
        b.b(this.e.context, this.d, bundle);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e.context, 200);
        if (buildSTInfo != null) {
            buildSTInfo.extraData = (this.f997a.getAnimation() != null ? "0" : "2") + "|" + this.b;
        }
        return buildSTInfo;
    }
}
