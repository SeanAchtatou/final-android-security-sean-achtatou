package com.tencent.assistant.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.business.CostTimeSTManager;
import com.tencent.assistantv2.st.k;
import java.util.Map;

/* compiled from: ProGuard */
class ct extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankRecommendListView f1002a;

    ct(RankRecommendListView rankRecommendListView) {
        this.f1002a = rankRecommendListView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        if (viewInvalidateMessage.what == 1) {
            int i = viewInvalidateMessage.arg1;
            int i2 = viewInvalidateMessage.arg2;
            Map map = (Map) viewInvalidateMessage.params;
            boolean booleanValue = ((Boolean) map.get("isFirstPage")).booleanValue();
            boolean booleanValue2 = ((Boolean) map.get("hasNext")).booleanValue();
            Map map2 = (Map) map.get("key_data");
            if (map2 != null && map2.size() > 0 && this.f1002a.mAdapter != null) {
                long j = -1;
                if (this.f1002a.engine != null) {
                    j = this.f1002a.engine.c();
                }
                if (booleanValue) {
                    this.f1002a.mAdapter.a(map2, j);
                    k.a((int) STConst.ST_PAGE_RANK_RECOMMEND, CostTimeSTManager.TIMETYPE.END, System.currentTimeMillis());
                } else {
                    this.f1002a.mAdapter.b(map2, j);
                }
                for (int i3 = 0; i3 < this.f1002a.mAdapter.getGroupCount(); i3++) {
                    this.f1002a.expandGroup(i3);
                }
            } else if (booleanValue) {
                k.a((int) STConst.ST_PAGE_RANK_RECOMMEND, CostTimeSTManager.TIMETYPE.CANCEL, System.currentTimeMillis());
            }
            this.f1002a.onAppListLoadedFinishedHandler(i2, i, booleanValue, booleanValue2);
            return;
        }
        this.f1002a.mListener.onNetworkNoError();
        if (this.f1002a.mAdapter != null) {
            this.f1002a.mAdapter.notifyDataSetChanged();
        }
    }
}
