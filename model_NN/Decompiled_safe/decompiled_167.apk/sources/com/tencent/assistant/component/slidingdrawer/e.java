package com.tencent.assistant.component.slidingdrawer;

import android.os.Message;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

/* compiled from: ProGuard */
class e extends Animation {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SlidingDrawerFrameLayout f1114a;
    private LinearLayout b;
    private LinearLayout.LayoutParams c;
    private int d = 0;
    private int e;
    private int f;

    public e(SlidingDrawerFrameLayout slidingDrawerFrameLayout, LinearLayout linearLayout, int i) {
        this.f1114a = slidingDrawerFrameLayout;
        setDuration((long) i);
        this.b = linearLayout;
        this.c = (LinearLayout.LayoutParams) this.b.getLayoutParams();
        this.c.height = 0;
        this.d = ((Integer) this.b.getTag()).intValue();
        this.e = slidingDrawerFrameLayout.c.getScrollY();
        this.f = slidingDrawerFrameLayout.l - this.e;
        this.b.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f2, Transformation transformation) {
        super.applyTransformation(f2, transformation);
        if (f2 < 1.0f) {
            this.c.height = (int) (((float) this.d) * f2);
            this.f1114a.c.scrollTo(0, this.e + ((int) (((float) this.f) * f2)));
            this.b.requestLayout();
            return;
        }
        this.c.height = this.d;
        this.f1114a.c.scrollTo(0, this.e + this.f);
        View childAt = this.b.getChildAt(0);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) childAt.getLayoutParams();
        layoutParams.width = this.b.getWidth();
        layoutParams.height = this.b.getHeight();
        this.b.requestLayout();
        this.f1114a.q.requestLayout();
        Message obtainMessage = this.f1114a.r.obtainMessage();
        obtainMessage.obj = childAt;
        this.f1114a.r.sendMessageDelayed(obtainMessage, 0);
    }
}
