package com.tencent.assistant.component;

import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.module.u;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class ee extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListView f1040a;

    ee(UpdateListView updateListView) {
        this.f1040a = updateListView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.ac.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, boolean):void
     arg types: [java.util.List<com.tencent.assistant.model.SimpleAppModel>, int]
     candidates:
      com.tencent.assistant.adapter.ac.a(int, int):java.lang.String
      com.tencent.assistant.adapter.ac.a(com.tencent.assistant.adapter.ac, java.lang.String):java.lang.String
      com.tencent.assistant.adapter.ac.a(com.tencent.assistant.adapter.ac, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.assistant.adapter.ac.a(com.tencent.assistant.model.SimpleAppModel, android.view.View):void
      com.tencent.assistant.adapter.ac.a(com.tencent.assistantv2.st.page.STExternalInfo, java.lang.String):void
      com.tencent.assistant.adapter.ac.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, boolean):void */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.f1040a.mUpdateListAdapter.a(u.a(this.f1040a.mUpdateListAdapter.b), true);
                this.f1040a.updateSizeLayout();
                this.f1040a.updateIgnoreListNum();
                this.f1040a.expandAllListView();
                int unused = this.f1040a.expandCount = this.f1040a.mUpdateListAdapter.getGroupCount();
                if (this.f1040a.callFrom == 1) {
                    if (this.f1040a.userMode == 1 || this.f1040a.userMode == 2) {
                        TemporaryThreadManager.get().start(new ef(this));
                        return;
                    }
                    return;
                } else if (this.f1040a.mAutoStartAppList != null && this.f1040a.mAutoStartAppList.size() > 0) {
                    this.f1040a.updateAppList(u.h(this.f1040a.mAutoStartAppList));
                    return;
                } else {
                    return;
                }
            case 2:
                this.f1040a.mUpdateListAdapter.a(u.a(this.f1040a.mUpdateListAdapter.b), false);
                this.f1040a.updateSizeLayout();
                this.f1040a.updateIgnoreListNum();
                this.f1040a.expandAllListView();
                int unused2 = this.f1040a.expandCount = this.f1040a.mUpdateListAdapter.getGroupCount();
                return;
            default:
                return;
        }
    }
}
