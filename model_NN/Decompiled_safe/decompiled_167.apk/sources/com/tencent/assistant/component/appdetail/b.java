package com.tencent.assistant.component.appdetail;

import android.content.Intent;
import android.net.Uri;
import android.webkit.DownloadListener;
import com.tencent.assistant.plugin.PluginActivity;

/* compiled from: ProGuard */
class b implements DownloadListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBarTabView f911a;

    b(AppBarTabView appBarTabView) {
        this.f911a = appBarTabView;
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f911a.c.f());
        this.f911a.c.startActivity(intent);
    }
}
