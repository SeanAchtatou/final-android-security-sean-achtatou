package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.a.t;
import com.tencent.assistant.model.d;
import com.tencent.assistant.utils.bt;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.appdetail.TXDwonloadProcessBar;
import com.tencent.assistantv2.component.x;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class SmartSquareAppItem extends RelativeLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1120a;
    private LayoutInflater b;
    private TXAppIconView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private DownloadButton g;
    private TXDwonloadProcessBar h;
    /* access modifiers changed from: private */
    public t i;

    public void setViewInvalidater(IViewInvalidater iViewInvalidater) {
        this.c.setInvalidater(iViewInvalidater);
    }

    public SmartSquareAppItem(Context context) {
        this(context, null);
    }

    public SmartSquareAppItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f1120a = context;
        this.b = (LayoutInflater) this.f1120a.getSystemService("layout_inflater");
        a();
    }

    private void a() {
        this.b.inflate((int) R.layout.smartcard_square_item_app, this);
        this.c = (TXAppIconView) findViewById(R.id.icon);
        this.d = (TextView) findViewById(R.id.name);
        this.e = (TextView) findViewById(R.id.size);
        this.f = (TextView) findViewById(R.id.desc);
        this.g = (DownloadButton) findViewById(R.id.downloadButton);
        this.h = (TXDwonloadProcessBar) findViewById(R.id.progress);
    }

    public void setData(t tVar, STInfoV2 sTInfoV2) {
        this.i = tVar;
        if (this.i != null) {
            if (this.i.f1653a != null) {
                this.c.updateImageView(this.i.f1653a.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            }
            this.d.setText(this.i.f1653a.d);
            this.e.setText(bt.a(this.i.f1653a.k));
            this.f.setText(this.i.a());
            this.g.a(this.i.f1653a);
            this.h.a(this.i.f1653a, new View[]{this.e, this.f});
            this.g.a(sTInfoV2, (x) null, (d) null, this.g, this.h);
            setOnClickListener(new ak(this, sTInfoV2));
        }
    }
}
