package com.tencent.assistant.component;

import android.view.View;
import com.tencent.assistant.activity.SelfUpdateActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class dc extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelfForceUpdateView f1012a;

    dc(SelfForceUpdateView selfForceUpdateView) {
        this.f1012a = selfForceUpdateView;
    }

    public void onTMAClick(View view) {
        this.f1012a.update();
    }

    public STInfoV2 getStInfo() {
        if (!(this.f1012a.getContext() instanceof SelfUpdateActivity)) {
            return null;
        }
        STInfoV2 i = ((SelfUpdateActivity) this.f1012a.getContext()).i();
        i.slotId = SecondNavigationTitleView.TMA_ST_NAVBAR_SEARCH_TAG;
        if (!SelfUpdateManager.a().a(this.f1012a.mUpdateInfo.e)) {
            i.actionId = 900;
        } else {
            i.actionId = STConstAction.ACTION_HIT_INSTALL;
        }
        i.extraData = "02";
        return i;
    }
}
