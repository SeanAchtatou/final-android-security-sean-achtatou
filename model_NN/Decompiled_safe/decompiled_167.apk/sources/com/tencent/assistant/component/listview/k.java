package com.tencent.assistant.component.listview;

import com.tencent.assistant.component.listview.AnimationExpandableListView;
import com.tencent.assistant.component.listview.ManagerGeneralController;

/* compiled from: ProGuard */
class k implements AnimationExpandableListView.ItemAnimatorLisenter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ManagerGeneralController.BussinessListener f1091a;
    final /* synthetic */ ManagerGeneralController b;

    k(ManagerGeneralController managerGeneralController, ManagerGeneralController.BussinessListener bussinessListener) {
        this.b = managerGeneralController;
        this.f1091a = bussinessListener;
    }

    public void onAnimatorEnd() {
        this.f1091a.onHandlerFinished();
    }
}
