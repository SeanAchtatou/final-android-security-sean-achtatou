package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;

/* compiled from: ProGuard */
public class WifiTipsView extends RelativeLayout implements NetworkMonitor.ConnectivityChangeListener {
    private static final String TMA_ST_UPDATE_WIFI_TAG = "03_001";
    private ImageView imageClose;
    private RelativeLayout layout;
    private TextView tvWifiTips;

    public WifiTipsView(Context context) {
        this(context, null);
    }

    public WifiTipsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initView(context);
    }

    private void initView(Context context) {
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.wifi_tips_layout, this);
        this.layout = (RelativeLayout) inflate.findViewById(R.id.layout_id);
        this.tvWifiTips = (TextView) inflate.findViewById(R.id.tvTips);
        this.imageClose = (ImageView) inflate.findViewById(R.id.imageClose);
        this.imageClose.setTag(R.id.tma_st_slot_tag, "03_001");
        if (!c.d() || !m.a().m()) {
            setVisibility(8);
            return;
        }
        setVisibility(0);
        cq.a().a(this);
        this.imageClose.setOnClickListener(new em(this, context));
    }

    /* access modifiers changed from: private */
    public void closeWifiTips() {
        this.layout.setVisibility(8);
        m.a().e(false);
        cq.a().b(this);
    }

    public void onConnected(APN apn) {
    }

    public void onDisconnected(APN apn) {
        if (apn == APN.WIFI) {
            closeWifiTips();
        }
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
        if (apn == APN.WIFI && apn2 != APN.WIFI) {
            closeWifiTips();
        }
    }
}
