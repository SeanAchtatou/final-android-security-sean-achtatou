package com.tencent.assistant.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.RootUtilInstallActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class cf extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PreInstallAppListView f988a;

    cf(PreInstallAppListView preInstallAppListView) {
        this.f988a = preInstallAppListView;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.f988a.mContext, RootUtilInstallActivity.class);
        intent.putExtra("king_root_channelid", PreInstallAppListView.KING_ROOT_CHANNELID_FROM_APP_UNINSTALL);
        this.f988a.mContext.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f988a.mContext, 200);
        buildSTInfo.scene = STConst.ST_PAGE_PRE_APP_UNINSTALL_NO_ROOT;
        buildSTInfo.slotId = "03_001";
        return buildSTInfo;
    }
}
