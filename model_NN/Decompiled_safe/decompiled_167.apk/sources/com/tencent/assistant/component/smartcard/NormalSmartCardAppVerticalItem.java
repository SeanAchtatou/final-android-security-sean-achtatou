package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.u;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.b.d;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class NormalSmartCardAppVerticalItem extends NormalSmartcardBaseItem {
    private TextView f;
    private LinearLayout i;

    public NormalSmartCardAppVerticalItem(Context context) {
        super(context);
    }

    public NormalSmartCardAppVerticalItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartCardAppVerticalItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.hasInit) {
            this.hasInit = true;
            d();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        int i2;
        this.c = this.b.inflate((int) R.layout.smartcard_applist, this);
        this.f = (TextView) this.c.findViewById(R.id.card_title);
        this.i = (LinearLayout) this.c.findViewById(R.id.card_list);
        u uVar = (u) this.smartcardModel;
        if (uVar == null || uVar.a() == null) {
            i2 = 0;
        } else {
            i2 = uVar.a().size();
        }
        if (i2 > 3) {
            i2 = 3;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            f();
        }
        setClickable(false);
        g();
    }

    private void f() {
        NormalSmartCardAppNode normalSmartCardAppNode = new NormalSmartCardAppNode(this.f1115a);
        normalSmartCardAppNode.setMinimumHeight(df.a(this.f1115a, 87.0f));
        this.i.addView(normalSmartCardAppNode);
    }

    private void g() {
        int i2;
        u uVar = (u) this.smartcardModel;
        if (uVar == null || uVar.a() == null || uVar.a().size() == 0) {
            c(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        c(0);
        this.f.setText(uVar.j());
        if (uVar.b()) {
            try {
                this.f.setBackgroundResource(R.drawable.common_index_tag);
            } catch (Throwable th) {
                cq.a().b();
            }
            this.f.setTextColor(getResources().getColor(R.color.smart_card_applist_title_font_color));
        } else {
            this.f.setBackgroundResource(0);
            this.f.setTextColor(getResources().getColor(R.color.apk_name_v5));
        }
        int size = uVar.a().size();
        if (size > 3) {
            i2 = 3;
        } else {
            i2 = size;
        }
        int childCount = this.i.getChildCount();
        if (childCount < i2) {
            for (int i3 = 0; i3 < i2 - childCount; i3++) {
                f();
            }
        }
        int childCount2 = this.i.getChildCount();
        for (int i4 = 0; i4 < childCount2; i4++) {
            NormalSmartCardAppNode normalSmartCardAppNode = (NormalSmartCardAppNode) this.i.getChildAt(i4);
            if (normalSmartCardAppNode != null) {
                if (i4 < i2) {
                    a(uVar, i4);
                    normalSmartCardAppNode.setVisibility(0);
                } else {
                    normalSmartCardAppNode.setVisibility(8);
                }
            }
        }
        int i5 = 0;
        while (i5 < i2) {
            ((NormalSmartCardAppNode) this.i.getChildAt(i5)).setData(uVar.a().get(i5).f1653a, uVar.a().get(i5).a(), a(uVar, i5), a(contextToPageId(this.f1115a)), i5 < i2 + -1, ListItemInfoView.InfoType.CATEGORY_SIZE);
            i5++;
        }
    }

    private STInfoV2 a(u uVar, int i2) {
        STInfoV2 a2 = a(com.tencent.assistant.st.i.a(uVar, i2 + 1), 100);
        if (!(a2 == null || uVar == null || uVar.a() == null || uVar.a().size() <= i2)) {
            a2.updateWithSimpleAppModel(uVar.a().get(i2).f1653a);
        }
        if (this.e == null) {
            this.e = new d();
        }
        this.e.a(a2);
        return a2;
    }

    private void c(int i2) {
        this.c.setVisibility(i2);
        this.f.setVisibility(i2);
        this.i.setVisibility(i2);
    }

    /* access modifiers changed from: protected */
    public void b() {
        g();
    }
}
