package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.util.AttributeSet;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.d;
import com.tencent.assistant.model.a.i;

/* compiled from: ProGuard */
public class NormalSmartcardCpaAdvertiseItem extends NormalSmartcardTopicItem {
    public NormalSmartcardCpaAdvertiseItem(Context context) {
        super(context);
    }

    public NormalSmartcardCpaAdvertiseItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardCpaAdvertiseItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void f() {
        d dVar = null;
        if (this.smartcardModel instanceof d) {
            dVar = (d) this.smartcardModel;
        }
        if (dVar != null) {
            setOnClickListener(new q(this, dVar.b));
            this.i.setInvalidater(this.d);
            this.i.updateImageView(dVar.f1638a, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
            this.k.setVisibility(8);
            dVar.a(false);
            if (dVar.a()) {
                this.f.setVisibility(0);
            } else {
                this.f.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: protected */
    public SimpleAppModel c() {
        if (this.smartcardModel instanceof d) {
            return ((d) this.smartcardModel).b;
        }
        return null;
    }
}
