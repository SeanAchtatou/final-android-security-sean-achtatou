package com.tencent.assistant.component.txscrollview;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.manager.cq;
import java.util.HashMap;

/* compiled from: ProGuard */
class j extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXImageView f1206a;

    j(TXImageView tXImageView) {
        this.f1206a = tXImageView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        HashMap hashMap;
        if (viewInvalidateMessage != null) {
            if (viewInvalidateMessage.params != null) {
                hashMap = (HashMap) viewInvalidateMessage.params;
            } else {
                hashMap = null;
            }
            if (hashMap != null) {
                String str = (String) hashMap.get("URL");
                if (viewInvalidateMessage.what == 1) {
                    if (str.equals(this.f1206a.mImageUrlString)) {
                        this.f1206a.sendImageRequestToInvalidater();
                    }
                } else if (((Integer) hashMap.get("TYPE")).intValue() == this.f1206a.mImageType.getThumbnailRequestType() && str.equals(this.f1206a.mImageUrlString)) {
                    if (this.f1206a.mBitmap == null || this.f1206a.mBitmap.isRecycled()) {
                        try {
                            this.f1206a.setImageResource(this.f1206a.defaultResId);
                        } catch (Throwable th) {
                            cq.a().b();
                        }
                        boolean unused = this.f1206a.mIsLoadFinish = true;
                        this.f1206a.onImageLoadFinishCallListener(this.f1206a.mBitmap);
                        return;
                    }
                    try {
                        this.f1206a.setImageBitmap(this.f1206a.mBitmap);
                    } catch (Throwable th2) {
                        cq.a().b();
                    }
                    boolean unused2 = this.f1206a.mIsLoadFinish = true;
                    this.f1206a.onImageLoadFinishCallListener(this.f1206a.mBitmap);
                }
            }
        }
    }
}
