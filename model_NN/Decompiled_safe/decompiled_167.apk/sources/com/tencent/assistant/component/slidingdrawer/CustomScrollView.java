package com.tencent.assistant.component.slidingdrawer;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/* compiled from: ProGuard */
public class CustomScrollView extends ScrollView {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1108a = false;

    public CustomScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public CustomScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CustomScrollView(Context context) {
        super(context);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f1108a) {
            return false;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setScrollFlag(boolean z) {
        this.f1108a = z;
    }
}
