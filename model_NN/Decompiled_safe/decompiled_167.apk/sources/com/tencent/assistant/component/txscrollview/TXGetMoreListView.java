package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.utils.cj;

/* compiled from: ProGuard */
public class TXGetMoreListView extends TXRefreshListView implements AbsListView.OnScrollListener {
    private final String TAG = "TXGetMoreListView";
    private boolean end = false;
    protected TXRefreshScrollViewBase.RefreshState mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.RESET;
    private IScrollListener mIScrollListener;
    protected int mScrollState = 0;

    public TXGetMoreListView(Context context) {
        super(context, TXScrollViewBase.ScrollMode.NONE);
    }

    public TXGetMoreListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void updateUIFroMode() {
        this.mFooterLayout = null;
        refreshLoadingLayoutSize();
    }

    public void onRefreshComplete(boolean z, boolean z2) {
        if (this.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.REFRESHING) {
            if (z) {
                this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.RESET;
            } else {
                this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
            }
        } else if (this.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH) {
            this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.RESET;
        } else if (this.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.RESET && !z) {
            this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
        }
        updateFootViewState(z2);
    }

    public void onRefreshComplete(boolean z) {
        onRefreshComplete(z, true);
    }

    public void addHeaderView(View view) {
        ((ListView) this.mScrollContentView).addHeaderView(view);
    }

    public void removeHeaderView(View view) {
        ((ListView) this.mScrollContentView).removeHeaderView(view);
    }

    public int getHeaderViewsCount() {
        return ((ListView) this.mScrollContentView).getHeaderViewsCount();
    }

    public void addFooterView(TXLoadingLayoutBase tXLoadingLayoutBase) {
        if (tXLoadingLayoutBase != null) {
            if (this.mFooterLoadingView != null) {
                ((ListView) this.mScrollContentView).removeFooterView(this.mFooterLoadingView);
            }
            this.mFooterLoadingView = tXLoadingLayoutBase;
            ((ListView) this.mScrollContentView).addFooterView(this.mFooterLoadingView);
            this.mFooterLoadingView.setVisibility(0);
            updateFootViewState();
        }
    }

    /* access modifiers changed from: protected */
    public void updateFootViewState() {
        updateFootViewState(true);
    }

    /* access modifiers changed from: protected */
    public void updateFootViewState(boolean z) {
        if (this.mFooterLoadingView != null) {
            switch (f.f1202a[this.mGetMoreRefreshState.ordinal()]) {
                case 1:
                    if (z) {
                        this.mFooterLoadingView.loadSuc();
                        return;
                    } else {
                        this.mFooterLoadingView.loadFail();
                        return;
                    }
                case 2:
                    int i = 0;
                    if (getRawAdapter() != null) {
                        i = getRawAdapter().getCount();
                    }
                    this.mFooterLoadingView.loadFinish(cj.b(getContext(), i));
                    return;
                case 3:
                    this.mFooterLoadingView.refreshing();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public ListView createScrollContentView(Context context) {
        ListView listView = new ListView(context);
        if (this.mScrollMode != TXScrollViewBase.ScrollMode.NONE) {
            this.mFooterLoadingView = createLoadingLayout(context, TXScrollViewBase.ScrollMode.PULL_FROM_END);
            this.mFooterLoadingView.setVisibility(0);
            listView.addFooterView(this.mFooterLoadingView);
            listView.setOnScrollListener(this);
        }
        return listView;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        this.mScrollState = i;
        if (this.mIScrollListener != null) {
            this.mIScrollListener.onScrollStateChanged(absListView, i);
        }
        if (i == 0 && this.end && this.mGetMoreRefreshState == TXRefreshScrollViewBase.RefreshState.RESET) {
            if (this.mRefreshListViewListener != null) {
                this.mRefreshListViewListener.onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState.ScrollState_FromEnd);
            }
            this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.REFRESHING;
            updateFootViewState();
        }
    }

    public boolean isScrollStateIdle() {
        return this.mScrollState == 0;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.mScrollContentView != null) {
            this.end = isReadyForScrollEnd();
            if (this.mIScrollListener != null) {
                this.mIScrollListener.onScroll(absListView, i, i2, i3);
            }
        }
    }

    public void addClickLoadMore() {
        this.mFooterLoadingView.setOnClickListener(new e(this));
    }

    public void reset() {
        this.mGetMoreRefreshState = TXRefreshScrollViewBase.RefreshState.RESET;
        this.mScrollState = 0;
    }

    public void setIScrollerListener(IScrollListener iScrollListener) {
        this.mIScrollListener = iScrollListener;
    }
}
