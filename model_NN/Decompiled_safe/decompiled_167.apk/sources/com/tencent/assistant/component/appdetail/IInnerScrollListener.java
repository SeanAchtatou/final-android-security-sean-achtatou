package com.tencent.assistant.component.appdetail;

/* compiled from: ProGuard */
public interface IInnerScrollListener {
    boolean canScrollDown();

    boolean canScrollUp();

    void fling(int i);

    void scrollDeltaY(int i);

    void scrollTopFinish(boolean z);
}
