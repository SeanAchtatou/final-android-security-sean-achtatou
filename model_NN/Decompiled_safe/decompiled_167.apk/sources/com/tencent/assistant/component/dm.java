package com.tencent.assistant.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class dm extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShareQzView f1022a;

    dm(ShareQzView shareQzView) {
        this.f1022a = shareQzView;
    }

    public void onTMAClick(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel /*2131165969*/:
                if (this.f1022a.mCallback != null) {
                    this.f1022a.mCallback.onCancelClick();
                    return;
                }
                return;
            case R.id.btn_submit /*2131165970*/:
                if (this.f1022a.mCallback != null) {
                    if (this.f1022a.mShareAppModel != null) {
                        this.f1022a.mShareAppModel.b = this.f1022a.et_reason.getText().toString();
                    }
                    if (this.f1022a.mShareBaseModel != null) {
                        this.f1022a.mShareBaseModel.e = this.f1022a.et_reason.getText().toString();
                    }
                    this.f1022a.mCallback.onSubmitClick();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.f1022a.mContext instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 i = ((AppDetailActivityV5) this.f1022a.mContext).i();
        i.actionId = 200;
        if (this.clickViewId == R.id.btn_cancel) {
            i.slotId = a.a(ShareQzView.ST_SLOT_ID_POPVIEW_SHARE_QZ, "001");
            return i;
        } else if (this.clickViewId != R.id.btn_submit) {
            return i;
        } else {
            i.slotId = a.a(ShareQzView.ST_SLOT_ID_POPVIEW_SHARE_QZ, "002");
            return i;
        }
    }
}
