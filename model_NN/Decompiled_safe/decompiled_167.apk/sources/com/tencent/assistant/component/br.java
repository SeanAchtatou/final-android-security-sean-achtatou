package com.tencent.assistant.component;

import android.view.View;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.manager.av;
import com.tencent.assistant.model.j;
import com.tencent.assistant.module.u;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class br implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PluginDownStateButton f973a;

    br(PluginDownStateButton pluginDownStateButton) {
        this.f973a = pluginDownStateButton;
    }

    public void onClick(View view) {
        j jVar;
        j a2 = av.a().a(this.f973a.pluginStartEntry.d());
        if (a2 == null) {
            jVar = av.a().a(this.f973a.pluginStartEntry.a());
        } else {
            jVar = a2;
        }
        if (jVar != null) {
            PluginInfo a3 = d.b().a(this.f973a.pluginStartEntry.a());
            boolean unused = this.f973a.isPreDownload = false;
            if (a3 == null || a3.getVersion() < jVar.d) {
                DownloadInfo b = av.a().b(jVar);
                if (b == null) {
                    b = av.a().a(jVar);
                }
                AppConst.AppState a4 = u.a(b, jVar, (PluginInfo) null);
                if (a4 == AppConst.AppState.DOWNLOADING || a4 == AppConst.AppState.QUEUING) {
                    av.a().e(jVar);
                    this.f973a.pluginSTReport(STConstAction.ACTION_HIT_PAUSE);
                } else if (a4 == AppConst.AppState.DOWNLOADED) {
                    String downloadingPath = b.getDownloadingPath();
                    if (jVar != null) {
                        TemporaryThreadManager.get().start(new bs(this, downloadingPath, jVar.c));
                    }
                    this.f973a.pluginSTReport(STConstAction.ACTION_HIT_INSTALL);
                } else {
                    av.a().c(jVar);
                    int i = STConstAction.ACTION_HIT_PLUGIN_DOWNLOAD;
                    if (this.f973a.isUpdate) {
                        i = STConstAction.ACTION_HIT_PLUGIN_UPDATE;
                    }
                    if (a4 == AppConst.AppState.PAUSED) {
                        i = STConstAction.ACTION_HIT_CONTINUE;
                    }
                    this.f973a.pluginSTReport(i);
                }
            } else {
                this.f973a.pluginStartEntry.a(a3.getVersion());
                TemporaryThreadManager.get().start(new bt(this));
            }
        }
    }
}
