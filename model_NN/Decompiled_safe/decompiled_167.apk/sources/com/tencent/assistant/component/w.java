package com.tencent.assistant.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessageHandler;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
class w extends ViewInvalidateMessageHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentDetailView f1219a;

    w(CommentDetailView commentDetailView) {
        this.f1219a = commentDetailView;
    }

    public void handleMessage(ViewInvalidateMessage viewInvalidateMessage) {
        switch (viewInvalidateMessage.what) {
            case 1:
                int i = viewInvalidateMessage.arg1;
                Map map = (Map) viewInvalidateMessage.params;
                CommentTagInfo commentTagInfo = (CommentTagInfo) map.get("reqTagInfo");
                this.f1219a.onCommentListResponseHandle(i, ((Boolean) map.get("isFirstPage")).booleanValue(), commentTagInfo, (List) map.get("data"), (List) map.get("selectedData"), (List) map.get("taglist"), ((Boolean) map.get("hasNext")).booleanValue(), (CommentDetail) map.get("curVerComment"));
                return;
            case 2:
                int i2 = viewInvalidateMessage.arg1;
                HashMap hashMap = (HashMap) viewInvalidateMessage.params;
                long longValue = ((Long) hashMap.get("oldCommentId")).longValue();
                this.f1219a.onWriteCommentFinishHandle(i2, (CommentDetail) hashMap.get("comment"), longValue);
                return;
            default:
                return;
        }
    }
}
