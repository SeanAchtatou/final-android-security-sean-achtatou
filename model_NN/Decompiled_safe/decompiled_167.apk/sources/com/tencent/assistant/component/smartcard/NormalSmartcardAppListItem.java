package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.b;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.d;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.appdetail.TXDwonloadProcessBar;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class NormalSmartcardAppListItem extends NormalSmartcardBaseItem {
    private View f;
    private TextView i;
    private TextView j;
    private LinearLayout k;
    /* access modifiers changed from: private */
    public int l = 2000;
    private String m = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public boolean n = false;
    private boolean o = true;
    public ArrayList<SimpleAppModel> showApps;

    public NormalSmartcardAppListItem(Context context) {
        super(context);
    }

    public NormalSmartcardAppListItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardAppListItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.hasInit) {
            this.hasInit = true;
            d();
            Iterator<SimpleAppModel> it = this.showApps.iterator();
            while (it.hasNext()) {
                SimpleAppModel next = it.next();
                long j2 = -1;
                if (next != null) {
                    j2 = next.f1634a;
                }
                a(STConst.ST_DEFAULT_SLOT, 100, this.smartcardModel.r, j2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_app, this);
        this.f = findViewById(R.id.title_ly);
        this.i = (TextView) findViewById(R.id.title);
        this.j = (TextView) findViewById(R.id.show_more);
        this.k = (LinearLayout) findViewById(R.id.app_list);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    public void setIsShowSize(boolean z) {
        this.o = z;
    }

    private void f() {
        this.k.removeAllViews();
        b bVar = (b) this.smartcardModel;
        if (bVar == null || bVar.f1636a <= 0 || bVar.c == null || bVar.c.size() == 0 || bVar.c.size() < bVar.f1636a) {
            setAllVisibility(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        setAllVisibility(0);
        ArrayList arrayList = new ArrayList(bVar.c);
        this.i.setText(bVar.k);
        List subList = arrayList.subList(0, arrayList.size() > bVar.f1636a ? bVar.f1636a : arrayList.size());
        if (this.showApps == null) {
            this.showApps = new ArrayList<>();
        }
        this.showApps.clear();
        this.showApps.addAll(subList);
        this.k.addView(a(subList));
        if (bVar.b <= bVar.f1636a || TextUtils.isEmpty(bVar.n)) {
            this.j.setVisibility(8);
            return;
        }
        this.j.setText(bVar.o);
        this.j.setOnClickListener(this.h);
        this.j.setVisibility(0);
    }

    public void setAllVisibility(int i2) {
        this.c.setVisibility(i2);
        this.f.setVisibility(i2);
        this.k.setVisibility(i2);
    }

    private View a(List<SimpleAppModel> list) {
        int i2 = 0;
        LinearLayout linearLayout = new LinearLayout(this.f1115a);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.weight = 1.0f;
        while (true) {
            int i3 = i2;
            if (i3 >= list.size()) {
                return linearLayout;
            }
            View inflate = this.b.inflate((int) R.layout.smartcard_app_item, (ViewGroup) null);
            a(inflate, list.get(i3), i3);
            linearLayout.addView(inflate, layoutParams);
            i2 = i3 + 1;
        }
    }

    public void setActivityPageId(int i2) {
        this.l = i2;
    }

    public void setSlot(String str) {
        this.m = str;
    }

    private String c(int i2) {
        return (TextUtils.isEmpty(this.m) ? "03_" : this.m.endsWith("_") ? this.m : this.m + "_") + ct.a(i2 + 1);
    }

    public void resetCurrentItems() {
        if (this.k != null && this.k.getChildCount() != 0) {
            View childAt = this.k.getChildAt(0);
            if (childAt instanceof LinearLayout) {
                LinearLayout linearLayout = (LinearLayout) childAt;
                b bVar = (b) this.smartcardModel;
                int size = bVar.c.size() > bVar.f1636a ? bVar.f1636a : bVar.c.size();
                if (linearLayout.getChildCount() >= size) {
                    List<SimpleAppModel> subList = bVar.c.subList(0, size);
                    for (int i2 = 0; i2 < subList.size(); i2++) {
                        a(linearLayout.getChildAt(i2), subList.get(i2), i2);
                    }
                }
            }
        }
    }

    private void a(View view, SimpleAppModel simpleAppModel, int i2) {
        TXImageView tXImageView = (TXImageView) view.findViewById(R.id.icon);
        tXImageView.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        ((TextView) view.findViewById(R.id.name)).setText(simpleAppModel.d);
        TextView textView = (TextView) view.findViewById(R.id.size);
        textView.setText(bt.a(simpleAppModel.k));
        if (!this.o) {
            textView.setVisibility(8);
        }
        TXDwonloadProcessBar tXDwonloadProcessBar = (TXDwonloadProcessBar) view.findViewById(R.id.progress);
        tXDwonloadProcessBar.a(simpleAppModel, new View[]{textView});
        DownloadButton downloadButton = (DownloadButton) view.findViewById(R.id.btn);
        downloadButton.a(simpleAppModel);
        tXImageView.setTag(simpleAppModel.q());
        downloadButton.setTag(R.id.tma_st_smartcard_tag, e());
        STInfoV2 a2 = a(c(i2), 100);
        if (a2 != null) {
            a2.updateWithSimpleAppModel(simpleAppModel);
        }
        a(simpleAppModel, a2);
        if (s.a(simpleAppModel)) {
            downloadButton.setClickable(false);
        } else {
            downloadButton.setClickable(true);
            downloadButton.a(a2, new l(this), (d) null, downloadButton, tXDwonloadProcessBar);
        }
        view.setTag(R.id.tma_st_smartcard_tag, e());
        view.setOnClickListener(new m(this, simpleAppModel, a2));
    }
}
