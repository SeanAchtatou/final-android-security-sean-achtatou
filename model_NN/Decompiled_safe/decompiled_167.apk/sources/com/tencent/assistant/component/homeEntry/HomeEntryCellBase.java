package com.tencent.assistant.component.homeEntry;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.RelativeLayout;
import com.tencent.assistant.protocol.jce.EntranceBlock;
import com.tencent.assistant.thumbnailCache.k;
import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
public abstract class HomeEntryCellBase extends RelativeLayout implements p {

    /* renamed from: a  reason: collision with root package name */
    protected Context f1052a;
    protected EntranceBlock b;

    public abstract void fillValue();

    public abstract void init();

    public abstract void setImg(Bitmap bitmap);

    public HomeEntryCellBase(Context context, EntranceBlock entranceBlock) {
        super(context);
        this.f1052a = context;
        if (!(entranceBlock == null || entranceBlock == this.b)) {
            this.b = entranceBlock;
        }
        init();
        XLog.v("home_entry", "HomeEntryCellBase---oncreate--data changed= " + ((entranceBlock == null || entranceBlock == this.b) ? false : true));
        requestImg();
        fillValue();
        setOnClickListener(new a(this));
    }

    public void thumbnailRequestStarted(o oVar) {
    }

    public void thumbnailRequestCompleted(o oVar) {
        if (oVar != null) {
            String c = oVar.c();
            if (!TextUtils.isEmpty(c) && c.endsWith(this.b.c) && oVar.f != null && !oVar.f.isRecycled()) {
                Bitmap bitmap = oVar.f;
                XLog.v("home_entry", "cell---requestImg--cell name:" + getClass().getSimpleName() + "--bitmap get------------netWork");
                ba.a().postDelayed(new b(this, bitmap), 10);
            }
        }
    }

    public void thumbnailRequestCancelled(o oVar) {
    }

    public void thumbnailRequestFailed(o oVar) {
    }

    public void requestImg() {
        if (this.b == null || TextUtils.isEmpty(this.b.c)) {
            XLog.v("home_entry", "cell---requestImg--cell name:" + getClass().getSimpleName() + "--no image data");
            return;
        }
        Bitmap a2 = k.b().a(this.b.c, 1, this);
        if (a2 != null && !a2.isRecycled()) {
            XLog.v("home_entry", "cell---requestImg--cell name:" + getClass().getSimpleName() + "--bitmap get------------local");
            ba.a().postDelayed(new c(this, a2), 10);
        }
    }
}
