package com.tencent.assistant.component.smartcard;

import com.tencent.assistant.model.SimpleAppModel;

/* compiled from: ProGuard */
public interface SmartcardListener {
    void onActionClose(int i, int i2);

    void onSmartcardContentAppExposure(int i, int i2, SimpleAppModel simpleAppModel);

    void onSmartcardExposure(int i, int i2);
}
