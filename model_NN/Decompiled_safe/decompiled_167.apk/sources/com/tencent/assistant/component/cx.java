package com.tencent.assistant.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class cx implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleView f1006a;

    cx(SecondNavigationTitleView secondNavigationTitleView) {
        this.f1006a = secondNavigationTitleView;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_layout /*2131166315*/:
                if (this.f1006a.hostActivity != null) {
                    this.f1006a.hostActivity.finish();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
