package com.tencent.assistant.component.invalidater;

/* compiled from: ProGuard */
class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1067a;
    final /* synthetic */ CommonViewInvalidater b;

    a(CommonViewInvalidater commonViewInvalidater, int i) {
        this.b = commonViewInvalidater;
        this.f1067a = i;
    }

    public void run() {
        ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(this.f1067a, null, null);
        if (this.b.canHandleMessage()) {
            this.b.handleMessage(viewInvalidateMessage);
            this.b.handleQueueMsg();
            return;
        }
        this.b.mq.add(viewInvalidateMessage);
    }
}
