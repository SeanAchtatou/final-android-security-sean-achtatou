package com.tencent.assistant.component.smartcard;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.o;
import com.tencent.assistantv2.component.BreakTextView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class NormalSmartCardSelfUpdateItem extends NormalSmartcardBaseItem {
    private View f;
    private TextView i;
    private RelativeLayout j;
    private TXAppIconView k;
    private DownloadButton l;
    private TextView m;
    private ListItemInfoView n;
    private TextView o;
    /* access modifiers changed from: private */
    public ImageView p;
    /* access modifiers changed from: private */
    public RelativeLayout q;
    /* access modifiers changed from: private */
    public BreakTextView r;
    /* access modifiers changed from: private */
    public TextView s;
    /* access modifiers changed from: private */
    public long t;

    public NormalSmartCardSelfUpdateItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartCardSelfUpdateItem(Context context, i iVar, SmartcardListener smartcardListener, IViewInvalidater iViewInvalidater) {
        super(context, iVar, smartcardListener, iViewInvalidater);
    }

    public NormalSmartCardSelfUpdateItem(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.hasInit) {
            this.hasInit = true;
            d();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.app_updatelist_item_merge, this);
        this.f = this.c;
        this.i = (TextView) findViewById(R.id.update_title);
        this.k = (TXAppIconView) findViewById(R.id.app_icon_img);
        this.k.setInvalidater(this.d);
        this.m = (TextView) findViewById(R.id.title);
        this.l = (DownloadButton) findViewById(R.id.state_app_btn);
        this.n = (ListItemInfoView) findViewById(R.id.download_info);
        this.o = (TextView) findViewById(R.id.desc);
        this.j = (RelativeLayout) findViewById(R.id.title_layout);
        this.q = (RelativeLayout) findViewById(R.id.description_bottomlayout);
        this.r = (BreakTextView) findViewById(R.id.new_feature);
        this.s = (TextView) findViewById(R.id.new_feature_all);
        this.p = (ImageView) findViewById(R.id.show_more);
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        o oVar = (o) this.smartcardModel;
        if (oVar == null || oVar.f1648a == null || oVar.b == null) {
            setAllVisibility(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        setAllVisibility(0);
        this.i.setText(oVar.k);
        SimpleAppModel a2 = SelfUpdateManager.a(oVar.b, oVar.f1648a);
        STInfoV2 a3 = a("03_001", 200);
        if (a3 != null) {
            a3.updateWithSimpleAppModel(a2);
        }
        View a4 = a(oVar, a3);
        if (a4 != null) {
            a4.setTag(R.id.tma_st_smartcard_tag, e());
            a4.setOnClickListener(new i(this, a3));
        }
    }

    private View a(o oVar, STInfoV2 sTInfoV2) {
        if (oVar == null || oVar.f1648a == null || oVar.b == null) {
            return null;
        }
        SelfUpdateManager.SelfUpdateInfo selfUpdateInfo = oVar.f1648a;
        LocalApkInfo localApkInfo = oVar.b;
        SimpleAppModel a2 = SelfUpdateManager.a(localApkInfo, selfUpdateInfo);
        this.k.updateImageView(localApkInfo.mPackageName, R.drawable.pic_defaule, TXImageView.TXImageViewType.INSTALL_APK_ICON);
        this.m.setText(selfUpdateInfo.d);
        this.n.a(a2);
        SelfUpdateManager.a();
        SimpleAppModel a3 = SelfUpdateManager.a(localApkInfo, selfUpdateInfo);
        this.l.a(a3);
        if (s.a(a3)) {
            this.l.setClickable(false);
        } else {
            this.l.setClickable(true);
            this.l.a(sTInfoV2);
        }
        this.l.setTag(R.id.tma_st_smartcard_tag, e());
        String str = selfUpdateInfo.q;
        if (!TextUtils.isEmpty(str)) {
            this.o.setText(Html.fromHtml(str));
        } else {
            this.o.setText(Constants.STR_EMPTY);
        }
        String str2 = selfUpdateInfo.h;
        if (TextUtils.isEmpty(str2)) {
            this.q.setVisibility(8);
        } else {
            String format = String.format(this.f1115a.getResources().getString(R.string.update_feature), "\n" + str2);
            this.s.setText(format);
            String replace = format.replaceAll("\r\n", "\n").replaceAll("\n\n", "\n").replace("\n", Constants.STR_EMPTY);
            this.q.setVisibility(0);
            this.q.setOnClickListener(new j(this, selfUpdateInfo, sTInfoV2));
            this.r.setTag(Long.valueOf(a3.f1634a));
            this.r.a(new k(this, a3.f1634a));
            this.r.setText(replace);
            if (this.t != selfUpdateInfo.f1469a) {
                this.r.setVisibility(0);
                this.s.setVisibility(8);
                this.p.setImageDrawable(this.f1115a.getResources().getDrawable(R.drawable.icon_open));
            } else {
                this.r.setVisibility(8);
                this.s.setVisibility(0);
                this.p.setImageDrawable(this.f1115a.getResources().getDrawable(R.drawable.icon_close));
            }
        }
        return this.f;
    }

    public void setAllVisibility(int i2) {
        this.f.setVisibility(i2);
    }
}
