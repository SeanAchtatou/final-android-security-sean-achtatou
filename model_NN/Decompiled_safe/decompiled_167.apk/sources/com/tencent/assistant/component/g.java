package com.tencent.assistant.component;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.manager.DownloadProxy;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1050a;
    final /* synthetic */ AppConst.AppState b;
    final /* synthetic */ AppIconView c;

    g(AppIconView appIconView, String str, AppConst.AppState appState) {
        this.c = appIconView;
        this.f1050a = str;
        this.b = appState;
    }

    public void run() {
        if (this.c.mAppModel != null && this.c.mAppModel.q().equals(this.f1050a)) {
            this.c.updateProgressView(DownloadProxy.a().d(this.f1050a), this.b);
            this.c.updateAppIcon(this.f1050a, this.b);
        }
    }
}
