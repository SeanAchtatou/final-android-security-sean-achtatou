package com.tencent.assistant.component.spaceclean;

import android.widget.TextView;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.model.spaceclean.SubRubbishInfo;

/* compiled from: ProGuard */
class e extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TextView f1171a;
    final /* synthetic */ SubRubbishInfo b;
    final /* synthetic */ RubbishItemView c;

    e(RubbishItemView rubbishItemView, TextView textView, SubRubbishInfo subRubbishInfo) {
        this.c = rubbishItemView;
        this.f1171a = textView;
        this.b = subRubbishInfo;
    }

    public void onRightBtnClick() {
        this.c.b(this.f1171a, this.b);
    }

    public void onLeftBtnClick() {
    }

    public void onCancell() {
    }
}
