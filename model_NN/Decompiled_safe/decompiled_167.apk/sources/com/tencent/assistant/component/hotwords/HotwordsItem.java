package com.tencent.assistant.component.hotwords;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.AdvancedHotWord;

/* compiled from: ProGuard */
public class HotwordsItem extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    HotwordsItemCallback f1061a;
    /* access modifiers changed from: private */
    public Context b;
    private View c;
    private AppIconView d;
    private TextView e;
    public String expatiation;
    private TextView f;
    /* access modifiers changed from: private */
    public AdvancedHotWord g;
    public int pageId;
    public int position;
    public String searchPreId;
    public String slotId;

    public HotwordsItem(Context context) {
        super(context);
    }

    public HotwordsItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public HotwordsItem(Context context, AdvancedHotWord advancedHotWord, int i) {
        super(context, null);
        this.b = context;
        this.g = advancedHotWord;
        this.position = i;
        a();
    }

    public void resetHotwordsItem(AdvancedHotWord advancedHotWord, int i) {
        this.g = advancedHotWord;
        this.position = i;
        b();
    }

    public void setHotwordsItemCallback(HotwordsItemCallback hotwordsItemCallback) {
        this.f1061a = hotwordsItemCallback;
    }

    public void setSTReportParameter(String str, int i, String str2) {
        this.searchPreId = str;
        this.pageId = i;
        this.slotId = str2;
    }

    public AdvancedHotWord getAdvancedHotWord() {
        return this.g;
    }

    private void a() {
        inflate(this.b, R.layout.hotwords_item, this);
        this.c = findViewById(R.id.hot_words_app);
        this.d = (AppIconView) findViewById(R.id.app_icon);
        this.e = (TextView) findViewById(R.id.app_hotwords);
        this.f = (TextView) findViewById(R.id.hot_words_txt);
        if (this.g != null && this.g.b == 4) {
            this.f.setMaxEms(8);
        }
        c();
    }

    private void b() {
        c();
    }

    private void c() {
        if (this.g != null) {
            SimpleAppModel a2 = u.a(this.g.d);
            setOnClickListener(new a(this, a2));
            if (this.g.b == 2) {
                this.f.setVisibility(8);
                this.c.setVisibility(0);
                this.d.setSimpleAppModel(a2, null, 0);
                this.e.setText(a2.d);
                if (this.g.d == null || this.g.d.f == null) {
                    this.expatiation = this.position + "_" + this.g.b + "_";
                } else {
                    this.expatiation = this.position + "_" + this.g.b + "_" + this.g.d.f.f2310a;
                }
            } else {
                this.c.setVisibility(8);
                this.f.setVisibility(0);
                this.f.setText(this.g.f1971a);
                a(this.f, this.g.f1971a);
                this.expatiation = this.position + "_" + this.g.b + "_" + this.g.f1971a;
            }
            this.expatiation += "_0_0";
        }
    }

    private void a(TextView textView, String str) {
        switch (str.length()) {
            case 1:
            case 2:
                textView.setTextSize(20.0f);
                textView.setTextColor(this.b.getResources().getColor(R.color.hot_word_2));
                return;
            case 3:
                textView.setTextSize(18.0f);
                textView.setTextColor(this.b.getResources().getColor(R.color.hot_word_3));
                return;
            case 4:
                textView.setTextSize(16.0f);
                textView.setTextColor(this.b.getResources().getColor(R.color.hot_word_4));
                return;
            default:
                textView.setTextSize(14.0f);
                textView.setTextColor(this.b.getResources().getColor(R.color.hot_word_5));
                return;
        }
    }
}
