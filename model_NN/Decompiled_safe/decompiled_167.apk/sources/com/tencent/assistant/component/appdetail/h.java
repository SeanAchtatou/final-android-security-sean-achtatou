package com.tencent.assistant.component.appdetail;

import android.graphics.drawable.Drawable;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.appdetail.process.AppdetailActionUIListener;

/* compiled from: ProGuard */
class h implements AppdetailActionUIListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailDownloadBar f917a;

    h(AppdetailDownloadBar appdetailDownloadBar) {
        this.f917a = appdetailDownloadBar;
    }

    public void a(String str, int i) {
        this.f917a.updateBarText(str, i);
    }

    public void a(String str, String str2, String str3) {
        this.f917a.updateSlimBarText(str, str2, str3);
    }

    public void a(int i, int i2) {
        this.f917a.setProgressAndDownloading(i, i2);
    }

    public void a(int i) {
        if (this.f917a.n != null) {
            this.f917a.n.setTextColor(i);
        }
    }

    public void b(int i) {
        if (this.f917a.j != null) {
            this.f917a.j.setVisibility(i);
        }
    }

    public void c(int i) {
        if (this.f917a.o != null) {
            this.f917a.o.setVisibility(i);
        }
    }

    public void a(Drawable drawable) {
        if (this.f917a.o != null && drawable != null) {
            this.f917a.o.setBackgroundDrawable(drawable);
        }
    }

    public void a(boolean z) {
        if (this.f917a.A == null) {
            return;
        }
        if (z) {
            this.f917a.A.setVisibility(0);
        } else {
            this.f917a.A.setVisibility(4);
        }
    }

    public void b(boolean z) {
        if (this.f917a.B != null) {
            this.f917a.B.setEnabled(z);
            if (z) {
                if (!(this.f917a.D == null || this.f917a.E == null)) {
                    this.f917a.D.setImageResource(R.drawable.icon_wx);
                    this.f917a.E.setText((int) R.string.wx_vie_number);
                    this.f917a.E.setTextColor(this.f917a.getResources().getColorStateList(17170443));
                }
                this.f917a.B.setBackgroundResource(R.drawable.appdetail_bar_btn_selector_v5);
                return;
            }
            if (!(this.f917a.D == null || this.f917a.E == null)) {
                this.f917a.D.setImageResource(R.drawable.icon_wx_disable);
                this.f917a.E.setText((int) R.string.vie_number_end);
                this.f917a.E.setTextColor(this.f917a.getResources().getColorStateList(R.color.state_disable));
            }
            this.f917a.B.setBackgroundResource(R.drawable.state_bg_disable);
        }
    }

    public void c(boolean z) {
        if (this.f917a.C != null) {
            this.f917a.C.setEnabled(z);
            if (z) {
                if (!(this.f917a.F == null || this.f917a.G == null)) {
                    this.f917a.F.setImageResource(R.drawable.icon_qq);
                    this.f917a.G.setText((int) R.string.qq_vie_number);
                    this.f917a.G.setTextColor(this.f917a.getResources().getColorStateList(17170443));
                }
                this.f917a.C.setBackgroundResource(R.drawable.appdetail_bar_btn_selector_v5);
                return;
            }
            if (!(this.f917a.F == null || this.f917a.G == null)) {
                this.f917a.F.setImageResource(R.drawable.icon_qq_disable);
                this.f917a.G.setText((int) R.string.vie_number_end);
                this.f917a.G.setTextColor(this.f917a.getResources().getColorStateList(R.color.state_disable));
            }
            this.f917a.C.setBackgroundResource(R.drawable.state_bg_disable);
        }
    }

    public void a(String str) {
    }

    public void a(boolean z, AppdetailActionUIListener.AuthType authType) {
        if (this.f917a.o != null) {
            this.f917a.o.setEnabled(z);
            this.f917a.m.setVisibility(0);
            if (z) {
                if (!(this.f917a.m == null || this.f917a.n == null)) {
                    if (authType == AppdetailActionUIListener.AuthType.QQ) {
                        this.f917a.m.setImageResource(R.drawable.icon_qq);
                        this.f917a.n.setText((int) R.string.qq_vie_number);
                    } else {
                        this.f917a.m.setImageResource(R.drawable.icon_wx);
                        this.f917a.n.setText((int) R.string.wx_vie_number);
                    }
                    this.f917a.n.setTextColor(this.f917a.getResources().getColorStateList(17170443));
                }
                this.f917a.o.setBackgroundResource(R.drawable.appdetail_bar_btn_selector_v5);
                return;
            }
            if (!(this.f917a.m == null || this.f917a.n == null)) {
                this.f917a.n.setText((int) R.string.vie_number_end);
                if (authType == AppdetailActionUIListener.AuthType.QQ) {
                    this.f917a.m.setImageResource(R.drawable.icon_qq_disable);
                } else {
                    this.f917a.m.setImageResource(R.drawable.icon_wx_disable);
                }
                this.f917a.n.setTextColor(this.f917a.getResources().getColorStateList(R.color.state_disable));
            }
            this.f917a.o.setBackgroundResource(R.drawable.state_bg_disable);
        }
    }

    public void a(String str, String str2) {
        this.f917a.updateDownloadState2Bar(str, str2);
    }
}
