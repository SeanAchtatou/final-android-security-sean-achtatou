package com.tencent.assistant.component;

import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f982a;
    final /* synthetic */ AppConst.AppState b;
    final /* synthetic */ AppBackupAppItemInfo c;

    c(AppBackupAppItemInfo appBackupAppItemInfo, String str, AppConst.AppState appState) {
        this.c = appBackupAppItemInfo;
        this.f982a = str;
        this.b = appState;
    }

    public void run() {
        if (this.c.downloadableModel != null && this.f982a.equals(this.c.downloadableModel.q())) {
            this.c.initViewVisibility(this.b);
        }
    }
}
