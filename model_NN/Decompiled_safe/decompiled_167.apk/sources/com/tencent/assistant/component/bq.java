package com.tencent.assistant.component;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class bq implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PicView f972a;

    bq(PicView picView) {
        this.f972a = picView;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        boolean unused = this.f972a.animationFlag = false;
        if (this.f972a.request != null) {
            this.f972a.mHandler.obtainMessage(0, this.f972a.request).sendToTarget();
        } else if (this.f972a.failFlag) {
            this.f972a.mHandler.sendEmptyMessageDelayed(1, 0);
        }
    }
}
