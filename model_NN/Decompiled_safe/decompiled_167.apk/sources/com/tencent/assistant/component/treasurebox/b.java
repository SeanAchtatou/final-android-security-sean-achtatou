package com.tencent.assistant.component.treasurebox;

import android.view.animation.Animation;
import com.tencent.assistant.utils.ba;

/* compiled from: ProGuard */
class b implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppTreasureBoxCell1 f1181a;

    b(AppTreasureBoxCell1 appTreasureBoxCell1) {
        this.f1181a = appTreasureBoxCell1;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        if (this.f1181a.m) {
            ba.a().postDelayed(new c(this), 2000);
        }
    }
}
