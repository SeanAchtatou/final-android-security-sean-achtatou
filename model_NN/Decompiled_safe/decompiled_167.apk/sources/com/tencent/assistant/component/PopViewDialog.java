package com.tencent.assistant.component;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.g.e;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.login.d;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.manager.bn;
import com.tencent.assistant.model.ShareAppModel;
import com.tencent.assistant.module.bf;
import com.tencent.assistant.module.ek;
import com.tencent.assistant.protocol.jce.CommentDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.activity.AppDetailActivityV5;
import com.tencent.assistantv2.activity.u;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class PopViewDialog extends Dialog implements UIEventListener {
    private static final String ST_CANCEL_SLOT = "03_001";
    private static final String ST_LOGIN = "04";
    private static final String ST_NOT_LOGIN = "03";
    private static final String ST_QZONE_SUBMIT_SLOT = "04_001";
    private static final String ST_SUBMIT_SLOT = "03_002";
    private static final String ST_WX_SUBMIT_SLOT = "04_002";
    private static final int TOTAL_COUNT = 100;
    /* access modifiers changed from: private */
    public long apkId = 0;
    /* access modifiers changed from: private */
    public long appId = 0;
    /* access modifiers changed from: private */
    public String appName;
    private u cStoplistener;
    private TextView cancel;
    /* access modifiers changed from: private */
    public bf commEngine = null;
    /* access modifiers changed from: private */
    public EditText contentView;
    /* access modifiers changed from: private */
    public TextView errorTips;
    /* access modifiers changed from: private */
    public boolean hasCommented;
    /* access modifiers changed from: private */
    public d loginProxy;
    private CommentDetailTabView mCommentTabView;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public CommentDetail mDetail;
    private ek modifyEngine;
    private boolean needScrollToTop = true;
    private long oldCommentId = -1;
    /* access modifiers changed from: private */
    public String oldContent;
    /* access modifiers changed from: private */
    public int oldScore = -1;
    /* access modifiers changed from: private */
    public int oldVersion = -1;
    /* access modifiers changed from: private */
    public TextView overMoreText;
    /* access modifiers changed from: private */
    public String pkgName;
    private View.OnClickListener popViewTextClick = new ce(this);
    /* access modifiers changed from: private */
    public RatingView ratingView;
    /* access modifiers changed from: private */
    public IPopViewSaveParams saveListener;
    /* access modifiers changed from: private */
    public ScrollView scrollView;
    /* access modifiers changed from: private */
    public ShareAppModel shareAppModel;
    /* access modifiers changed from: private */
    public ImageView shareBtnView;
    /* access modifiers changed from: private */
    public ImageView shareBtnView2;
    /* access modifiers changed from: private */
    public e shareEngine;
    private ImageView shareImageView;
    private ImageView shareImageView2;
    private TextView shareTextView;
    /* access modifiers changed from: private */
    public boolean shareToQZ;
    /* access modifiers changed from: private */
    public boolean shareToWX;
    private TextView submit;
    /* access modifiers changed from: private */
    public TextView tipsText;
    /* access modifiers changed from: private */
    public int versionCode = 0;
    /* access modifiers changed from: private */
    public String versionName;
    private Window window = null;

    /* compiled from: ProGuard */
    public interface IPopViewSaveParams {
        void onSavedParams(String str, int i, int i2, boolean z);
    }

    public PopViewDialog(Context context, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        super(context, z, onCancelListener);
        this.mContext = context;
    }

    public PopViewDialog(Context context, int i, String str, CommentDetailTabView commentDetailTabView) {
        super(context, i);
        this.mContext = context;
        this.mDetail = new CommentDetail();
        this.commEngine = commentDetailTabView.getCommentEngine();
        this.modifyEngine = commentDetailTabView.getModifyAppCommentEngine();
        this.pkgName = str;
        this.mCommentTabView = commentDetailTabView;
        setCanceledOnTouchOutside(false);
    }

    public PopViewDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.comment_detail_popview);
        this.loginProxy = d.a();
        windowDeploy(0, 0, r.b() - 50, r.c());
        this.overMoreText = (TextView) findViewById(R.id.over_text_tips);
        this.ratingView = (RatingView) findViewById(R.id.comment_score);
        this.tipsText = (TextView) findViewById(R.id.starCount);
        this.errorTips = (TextView) findViewById(R.id.errorTips);
        this.ratingView.setEnable(true);
        this.ratingView.setOnRatingBarChangeListener(new bw(this));
        if (this.oldScore > 0) {
            this.ratingView.setRating((float) this.oldScore);
        }
        this.contentView = (EditText) findViewById(R.id.input_more_advice);
        this.contentView.setLongClickable(false);
        if (r.d() >= 11) {
            this.contentView.setCustomSelectionActionModeCallback(new bx(this));
            this.contentView.setTextIsSelectable(false);
        }
        this.contentView.setOnFocusChangeListener(new by(this));
        this.contentView.addTextChangedListener(new bz(this));
        if (!TextUtils.isEmpty(this.oldContent)) {
            this.contentView.setText(this.oldContent);
        }
        this.cancel = (TextView) findViewById(R.id.cancel);
        this.submit = (TextView) findViewById(R.id.submit);
        this.cancel.setOnClickListener(this.popViewTextClick);
        this.submit.setOnClickListener(this.popViewTextClick);
        this.scrollView = (ScrollView) findViewById(R.id.scroller_view);
        scrollDialogToBottom();
        this.shareTextView = (TextView) findViewById(R.id.share_txt);
        this.shareBtnView = (ImageView) findViewById(R.id.share_btn);
        this.shareImageView = (ImageView) findViewById(R.id.share_img);
        this.shareBtnView2 = (ImageView) findViewById(R.id.share_btn2);
        this.shareImageView2 = (ImageView) findViewById(R.id.share_img2);
        this.shareBtnView.setOnClickListener(new ca(this));
        this.shareBtnView2.setOnClickListener(new cb(this));
    }

    public void onResumeSetShareVisibility() {
        if (!this.loginProxy.j()) {
            this.shareImageView.setVisibility(0);
            this.shareImageView.setImageResource(R.drawable.common_pinglun_icon_kongjian);
            this.shareBtnView.setVisibility(0);
            if (this.loginProxy.v()) {
                this.shareImageView2.setVisibility(0);
                this.shareImageView2.setImageResource(R.drawable.common_pinglun_icon_pengyouquan);
                this.shareBtnView2.setVisibility(0);
            } else {
                this.shareImageView2.setVisibility(8);
                this.shareBtnView2.setVisibility(8);
            }
        } else if (this.loginProxy.k()) {
            this.shareImageView.setVisibility(0);
            this.shareImageView.setImageResource(R.drawable.common_pinglun_icon_kongjian);
            this.shareBtnView.setVisibility(0);
            this.shareImageView2.setVisibility(8);
            this.shareBtnView2.setVisibility(8);
        } else if (this.loginProxy.v()) {
            this.shareImageView2.setVisibility(0);
            this.shareImageView2.setImageResource(R.drawable.common_pinglun_icon_pengyouquan);
            this.shareBtnView2.setVisibility(0);
            this.shareImageView.setVisibility(8);
            this.shareBtnView.setVisibility(8);
        }
        this.shareBtnView.setSelected(false);
        this.shareBtnView2.setSelected(false);
        this.errorTips.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        onResumeSetShareVisibility();
        this.shareToQZ = false;
        this.shareToWX = false;
    }

    public void scrollDialogToBottom() {
        if (this.scrollView != null) {
            this.scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new cc(this));
        }
    }

    public void windowDeploy(int i, int i2, int i3, int i4) {
        this.window = getWindow();
        WindowManager.LayoutParams attributes = this.window.getAttributes();
        attributes.x = i;
        attributes.y = i2;
        attributes.width = i3;
        this.window.setAttributes(attributes);
    }

    public RatingView getRatingView() {
        return this.ratingView;
    }

    public void setRatingView(RatingView ratingView2) {
        this.ratingView = ratingView2;
    }

    public EditText getContentView() {
        return this.contentView;
    }

    public void setContentView(EditText editText) {
        this.contentView = editText;
    }

    public TextView getCancel() {
        return this.cancel;
    }

    public void setCancel(TextView textView) {
        this.cancel = textView;
    }

    public TextView getSubmit() {
        return this.submit;
    }

    public void setSubmit(TextView textView) {
        this.submit = textView;
    }

    public void setData(boolean z, long j, long j2, long j3, String str, int i, int i2, IPopViewSaveParams iPopViewSaveParams, String str2, boolean z2) {
        this.appId = j;
        this.apkId = j2;
        this.hasCommented = z;
        this.appName = str2;
        if (iPopViewSaveParams != null) {
            this.saveListener = iPopViewSaveParams;
        }
        int i3 = 0;
        if (!be.a().c(this.pkgName) || !be.a().d(this.pkgName)) {
            LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.pkgName);
            if (localApkInfo != null) {
                i3 = localApkInfo.mVersionCode;
            }
        } else {
            bn a2 = be.a().a(this.pkgName);
            if (a2 != null) {
                i3 = a2.c;
            }
        }
        if (i <= 0 || i3 <= 0 || i3 != i2) {
            this.oldVersion = -1;
            if (this.contentView != null) {
                this.contentView.setText(Constants.STR_EMPTY);
            }
            this.oldContent = Constants.STR_EMPTY;
            if (this.ratingView != null) {
                this.ratingView.setRating(0.0f);
            }
            this.oldCommentId = -1;
            this.oldScore = -1;
        } else {
            this.oldVersion = i2;
            if (this.contentView != null) {
                this.contentView.setText(str);
            }
            this.oldContent = str;
            if (this.ratingView != null) {
                this.ratingView.setRating((float) i);
            }
            this.oldCommentId = j3;
            this.oldScore = i;
        }
        scrollDialogToBottom();
    }

    public void setVersion(int i, String str) {
        this.versionCode = i;
        this.versionName = str;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            hiddenKeyboard();
        }
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: private */
    public void hiddenKeyboard() {
        View currentFocus;
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null && inputMethodManager.isActive() && (currentFocus = getCurrentFocus()) != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    public void dismiss() {
        this.tipsText.setText((int) R.string.comment_pop_star_text);
        this.tipsText.setTextColor(this.mContext.getResources().getColor(R.color.appadmin_risk_tips));
        this.contentView.clearFocus();
        this.errorTips.setVisibility(8);
        super.dismiss();
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS /*1081*/:
                if (this.cStoplistener != null) {
                    this.cStoplistener.a(true);
                }
                this.mCommentTabView.clickCommentTag(null);
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL /*1082*/:
            default:
                return;
        }
    }

    public void setHasComment(boolean z) {
        if (z) {
            if (isShowing()) {
                this.errorTips.setVisibility(0);
                this.errorTips.setText("你已提交过该版本评论");
                this.cancel.setClickable(true);
                this.submit.setClickable(false);
                this.submit.setTextColor(Color.parseColor("#858b8d"));
                this.submit.setBackgroundResource(R.drawable.common_btn_big_disabled);
            }
        } else if (this.submit != null) {
            this.popViewTextClick.onClick(this.submit);
            dismiss();
        }
    }

    public void registerOnGetCommentListReqStart(u uVar) {
        this.cStoplistener = uVar;
    }

    private void setShareVisibility(int i) {
        this.shareTextView.setVisibility(i);
        this.shareImageView.setVisibility(i);
        this.shareBtnView.setVisibility(i);
    }

    private int getPageId() {
        if (this.loginProxy.j()) {
            return STConst.ST_PAGE_COMMENT_DIALOG_LOGIN;
        }
        return STConst.ST_PAGE_COMMENT_DIALOG_NOT_LOGIN;
    }

    /* access modifiers changed from: private */
    public String getColumn() {
        if (this.loginProxy.j()) {
            return "04";
        }
        return "03";
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        k.a(buildFloatingSTInfo());
    }

    /* access modifiers changed from: private */
    public STInfoV2 buildFloatingSTInfo() {
        int i = 2000;
        if (!(this.mContext instanceof AppDetailActivityV5)) {
            return new STInfoV2(getPageId(), STConst.ST_DEFAULT_SLOT, 2000, STConst.ST_DEFAULT_SLOT, 100);
        }
        STInfoV2 i2 = ((AppDetailActivityV5) this.mContext).i();
        if (i2 != null) {
            i = i2.scene;
        }
        return new STInfoV2(getPageId(), STConst.ST_DEFAULT_SLOT, i, a.b(i2 != null ? i2.slotId : STConst.ST_DEFAULT_SLOT, STConst.ST_STATUS_DEFAULT), 100);
    }

    public void onDetachedFromWindow() {
        this.needScrollToTop = true;
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        super.onDetachedFromWindow();
    }

    public void setShareAppModel(ShareAppModel shareAppModel2) {
        if (shareAppModel2 != null) {
            this.shareAppModel = shareAppModel2;
        }
    }

    /* access modifiers changed from: private */
    public void showLoginDialog() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 2);
        d.a().a(AppConst.IdentityType.MOBILEQ, bundle);
    }

    public void setShareEngine(e eVar) {
        this.shareEngine = eVar;
    }

    public void setPkgName(String str) {
        this.pkgName = str;
    }
}
