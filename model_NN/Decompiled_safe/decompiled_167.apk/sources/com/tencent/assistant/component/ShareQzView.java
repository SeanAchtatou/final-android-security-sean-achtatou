package com.tencent.assistant.component;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.ShareAppModel;
import com.tencent.assistant.model.ShareBaseModel;
import com.tencent.assistant.model.ShareModel;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;

/* compiled from: ProGuard */
public class ShareQzView extends RelativeLayout {
    /* access modifiers changed from: private */
    public static String ST_SLOT_ID_POPVIEW_SHARE_QZ = "04";
    private Button btn_cancel;
    private Button btn_submit;
    private View.OnClickListener clickListener = new dm(this);
    /* access modifiers changed from: private */
    public EditText et_reason;
    private TXImageView iv_icon;
    /* access modifiers changed from: private */
    public ICallback mCallback;
    /* access modifiers changed from: private */
    public Context mContext;
    private LayoutInflater mLayoutInflater;
    /* access modifiers changed from: private */
    public ShareAppModel mShareAppModel;
    /* access modifiers changed from: private */
    public ShareBaseModel mShareBaseModel;
    private RatingView ratingView;
    private TextView tv_app_name;
    private TextView tv_otherInfo;

    /* compiled from: ProGuard */
    public interface ICallback {
        void onCancelClick();

        void onSubmitClick();
    }

    public ShareQzView(Context context) {
        super(context);
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(context);
        initView();
    }

    public void setClickCallBack(ICallback iCallback) {
        this.mCallback = iCallback;
    }

    public void setShareModel(ShareModel shareModel) {
        if (shareModel != null) {
            if (shareModel instanceof ShareAppModel) {
                this.mShareAppModel = (ShareAppModel) shareModel;
            }
            if (shareModel instanceof ShareBaseModel) {
                this.mShareBaseModel = (ShareBaseModel) shareModel;
            }
            initViewData();
        }
    }

    private void initView() {
        this.mLayoutInflater.inflate((int) R.layout.dialog_share_to_qz, this);
        this.iv_icon = (TXImageView) findViewById(R.id.iv_icon);
        this.tv_app_name = (TextView) findViewById(R.id.tv_app_name);
        this.ratingView = (RatingView) findViewById(R.id.app_ratingview);
        this.tv_otherInfo = (TextView) findViewById(R.id.tv_other_info);
        this.et_reason = (EditText) findViewById(R.id.et_reason);
        this.btn_cancel = (Button) findViewById(R.id.btn_cancel);
        this.btn_submit = (Button) findViewById(R.id.btn_submit);
        this.btn_cancel.setOnClickListener(this.clickListener);
        this.btn_submit.setOnClickListener(this.clickListener);
    }

    private void initViewData() {
        if (this.mShareAppModel != null) {
            this.iv_icon.updateImageView(this.mShareAppModel.f, R.drawable.pic_default_app, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.tv_app_name.setText(this.mShareAppModel.g);
            this.ratingView.setRating(this.mShareAppModel.h);
            TextView textView = this.tv_otherInfo;
            Context context = this.mContext;
            Object[] objArr = new Object[2];
            objArr[0] = TextUtils.isEmpty(this.mShareAppModel.f1632a) ? bt.a(this.mShareAppModel.j) : this.mShareAppModel.f1632a;
            objArr[1] = ct.a(this.mShareAppModel.i, 0);
            textView.setText(context.getString(R.string.share_otherInfo, objArr));
        }
        if (this.mShareBaseModel != null) {
            this.iv_icon.updateImageView(this.mShareBaseModel.c, R.drawable.pic_default_app, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.tv_app_name.setText(this.mShareBaseModel.f1633a);
            this.ratingView.setVisibility(8);
            this.tv_otherInfo.setText(this.mShareBaseModel.b);
        }
    }
}
