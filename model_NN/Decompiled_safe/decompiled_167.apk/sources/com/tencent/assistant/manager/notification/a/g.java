package com.tencent.assistant.manager.notification.a;

import android.text.Html;
import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.ActionLink;
import com.tencent.assistant.protocol.jce.PushIconInfo;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.utils.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class g extends i {
    public g(int i, PushInfo pushInfo, byte[] bArr) {
        super(i, pushInfo, bArr);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        if (super.c() && this.c.l != null && this.c.l.d != null && this.c.l.d.size() > 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        ActionLink l = l();
        if (l != null) {
            this.j = b(R.layout.notification_card1_right2);
            this.j.setTextViewText(R.id.rightButton, Html.fromHtml(l.b()));
            this.e = l.a();
            this.j.setOnClickPendingIntent(R.id.rightButton, k());
            this.i.removeAllViews(R.id.rightContainer);
            this.i.addView(R.id.rightContainer, this.j);
            this.i.setViewVisibility(R.id.rightContainer, 0);
        }
        if (this.j != null) {
            return true;
        }
        return false;
    }

    private ActionLink l() {
        if (this.c == null || this.c.l == null || this.p == null) {
            return null;
        }
        Map<Byte, ActionLink> map = this.c.l.d;
        if (map == null) {
            return null;
        }
        boolean a2 = a(this.p);
        a(a2, this.p);
        ActionLink actionLink = map.get(Byte.valueOf(a2 ? (byte) 4 : 3));
        if (actionLink == null || TextUtils.isEmpty(actionLink.b()) || actionLink.a() == null) {
            return null;
        }
        return actionLink;
    }

    private boolean a(PushIconInfo pushIconInfo) {
        if (pushIconInfo == null || pushIconInfo.a() != 2) {
            return false;
        }
        return f.b(pushIconInfo.b, pushIconInfo.c);
    }

    private boolean a(List<PushIconInfo> list) {
        boolean z;
        if (list == null) {
            return false;
        }
        Iterator<PushIconInfo> it = list.iterator();
        while (true) {
            if (it.hasNext()) {
                if (!a(it.next())) {
                    z = false;
                    break;
                }
            } else {
                z = true;
                break;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public void a(boolean z, List<PushIconInfo> list) {
        this.f = z ? 11 : 10;
        if (!z) {
            this.h = new ArrayList();
            for (PushIconInfo next : list) {
                if (next != null && !TextUtils.isEmpty(next.b) && !a(next)) {
                    this.h.add(next.b);
                }
            }
        }
        this.g = e();
    }
}
