package com.tencent.assistant.manager.spaceclean;

import android.os.RemoteException;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceScanManager f1619a;

    e(SpaceScanManager spaceScanManager) {
        this.f1619a = spaceScanManager;
    }

    public void run() {
        if (this.f1619a.y != null) {
            try {
                XLog.d("miles", "SpaceScanManager >> cancelScanRubbish called.");
                this.f1619a.y.cancelScanRubbish();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
}
