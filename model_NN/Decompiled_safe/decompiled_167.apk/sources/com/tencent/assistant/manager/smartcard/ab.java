package com.tencent.assistant.manager.smartcard;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.t;
import com.tencent.assistant.model.a.u;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class ab extends y {
    public boolean a(i iVar, List<Long> list) {
        if (iVar == null || !(iVar instanceof u)) {
            return false;
        }
        return a((u) iVar, list);
    }

    private boolean a(u uVar, List<Long> list) {
        if (uVar.c() == 0) {
            return false;
        }
        uVar.a(list);
        if (!((Boolean) b(uVar).first).booleanValue()) {
            return false;
        }
        a(uVar);
        if (uVar.a() == null || uVar.a().size() == 0) {
            return false;
        }
        if ((uVar.c() == 2 || uVar.c() == 5) && uVar.a().size() < 2) {
            return false;
        }
        if (((uVar.i == 14 || uVar.i == 15) && uVar.c() == 1 && uVar.a().size() < 3) || (uVar.i == 27 && uVar.c() == 1 && uVar.a().size() < 3)) {
            return false;
        }
        return true;
    }

    private void a(u uVar) {
        LocalApkInfo installedApkInfo;
        if (uVar != null && uVar.a() != null && uVar.a().size() != 0) {
            ArrayList arrayList = new ArrayList();
            for (t next : uVar.f1654a) {
                if (!(next.f1653a == null || (installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(next.f1653a.c)) == null || installedApkInfo.mVersionCode < next.f1653a.g)) {
                    arrayList.add(next);
                }
            }
            uVar.f1654a.removeAll(arrayList);
        }
    }
}
