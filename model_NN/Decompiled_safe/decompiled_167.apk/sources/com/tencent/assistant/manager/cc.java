package com.tencent.assistant.manager;

import com.qq.AppService.AstApp;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.s;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.model.StatInfo;
import java.util.List;

/* compiled from: ProGuard */
class cc implements s {

    /* renamed from: a  reason: collision with root package name */
    String f1524a = null;
    final /* synthetic */ bo b;

    cc(bo boVar) {
        this.b = boVar;
    }

    public void a(String str) {
        this.f1524a = str;
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        DownloadInfo downloadInfo;
        SimpleDownloadInfo.DownloadState downloadState;
        if (this.b.p != null && this.f1524a != null) {
            SimpleAppModel a2 = u.a(appSimpleDetail);
            DownloadInfo a3 = DownloadProxy.a().a(a2);
            if (a3 == null) {
                downloadInfo = DownloadInfo.createDownloadInfo(a2, new StatInfo(a2.b, AstApp.m() != null ? AstApp.m().f() : STConst.ST_PAGE_DOWNLOAD, 0, null, 0));
            } else {
                downloadInfo = a3;
            }
            XLog.d("RecommendDownloadManager", "<install> 服务器拉取到的DownloadInfo = " + downloadInfo.toString());
            boolean a4 = this.b.p.a(downloadInfo.appId, downloadInfo.packageName, downloadInfo.categoryId);
            XLog.d("RecommendDownloadManager", "<install> " + this.f1524a + (a4 ? "<成功命中>安装推荐逻辑" : "<未命中>安装推荐逻辑"));
            if (a4) {
                try {
                    if (this.b.d.getPackageInfo(this.b.p.g, 0) != null) {
                        XLog.e("RecommendDownloadManager", "<install> 推荐的应用<" + downloadInfo.name + "> 已安装，不再弹出安装推荐");
                        return;
                    }
                } catch (Exception e) {
                }
                List<DownloadInfo> e2 = DownloadProxy.a().e(this.b.p.g);
                if (e2 == null || e2.size() <= 0 || !(SimpleDownloadInfo.DownloadState.DOWNLOADING == (downloadState = e2.get(0).downloadState) || SimpleDownloadInfo.DownloadState.QUEUING == downloadState || SimpleDownloadInfo.DownloadState.INSTALLING == downloadState)) {
                    this.b.a(downloadInfo.name, this.b.p);
                } else {
                    XLog.e("RecommendDownloadManager", "<install> 推荐的应用<" + downloadInfo.name + "> 正在下载中，不再弹出安装推荐");
                }
            }
        }
    }

    public void onGetAppInfoFail(int i, int i2) {
        XLog.e("RecommendDownloadManager", "<install> 服务器拉刚才已安装应用信息<失败> !!");
    }
}
