package com.tencent.assistant.manager;

import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.download.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/* compiled from: ProGuard */
class ad implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadProxy f1475a;

    ad(DownloadProxy downloadProxy) {
        this.f1475a = downloadProxy;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>
     arg types: [com.tencent.assistant.download.SimpleDownloadInfo$DownloadType, int]
     candidates:
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.manager.DownloadProxy, com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager):com.tencent.assistant.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>):java.lang.String
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.assistant.manager.DownloadProxy.a(java.lang.String, int):com.tencent.assistant.download.DownloadInfo
      com.tencent.assistant.manager.DownloadProxy.a(java.lang.String, boolean):void
      com.tencent.assistant.manager.DownloadProxy.a(com.tencent.assistant.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.assistant.download.DownloadInfo> */
    public void run() {
        ArrayList<DownloadInfo> a2 = this.f1475a.a(SimpleDownloadInfo.DownloadType.APK, true);
        Collections.sort(a2);
        Iterator<DownloadInfo> it = a2.iterator();
        while (it.hasNext()) {
            DownloadInfo next = it.next();
            if (next.downloadState == SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI || (next.downloadState == SimpleDownloadInfo.DownloadState.FAIL && next.errorCode != -10)) {
                a.a().a(next);
            }
        }
    }
}
