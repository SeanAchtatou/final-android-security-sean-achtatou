package com.tencent.assistant.manager;

import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.e;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.protocol.jce.TagGroup;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class r implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ p f1586a;

    private r(p pVar) {
        this.f1586a = pVar;
    }

    public void a(int i, int i2, boolean z, List<SimpleAppModel> list, List<TagGroup> list2) {
        ArrayList arrayList = null;
        if (list != null && list.size() > 0) {
            arrayList = new ArrayList(list.size());
            for (SimpleAppModel eVar : list) {
                arrayList.add(new e(eVar));
            }
        }
        this.f1586a.a(new s(this, i, i2, z, arrayList, list2));
    }
}
