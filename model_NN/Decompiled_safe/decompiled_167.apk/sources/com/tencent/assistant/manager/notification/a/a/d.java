package com.tencent.assistant.manager.notification.a.a;

import com.tencent.assistant.thumbnailCache.o;
import com.tencent.assistant.thumbnailCache.p;

/* compiled from: ProGuard */
class d implements p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f1552a;

    d(c cVar) {
        this.f1552a = cVar;
    }

    public void thumbnailRequestCompleted(o oVar) {
        if (oVar == null) {
            this.f1552a.a(-4, null);
        } else if (oVar.f == null || oVar.f.isRecycled()) {
            this.f1552a.a(-5, oVar.f);
        } else {
            this.f1552a.a(0, oVar.f);
        }
    }

    public void thumbnailRequestFailed(o oVar) {
        this.f1552a.a(-2, null);
    }
}
