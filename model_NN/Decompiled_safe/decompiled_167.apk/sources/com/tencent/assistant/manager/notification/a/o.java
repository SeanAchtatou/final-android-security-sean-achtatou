package com.tencent.assistant.manager.notification.a;

import android.text.Html;
import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.PushInfo;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class o extends h {
    public o(int i, PushInfo pushInfo, byte[] bArr) {
        super(i, pushInfo, bArr);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        if (super.c() && this.c.l != null && !TextUtils.isEmpty(this.c.l.f2266a)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        a((int) R.layout.notification_card_7);
        if (this.i == null) {
            return false;
        }
        i();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        if (this.c == null || this.i == null || this.c.l == null || TextUtils.isEmpty(this.c.l.f2266a)) {
            return false;
        }
        if (this.m != null) {
            this.i.setTextColor(R.id.big_text, this.m.intValue());
        }
        this.i.setFloat(R.id.big_text, "setTextSize", this.o);
        this.i.setTextViewText(R.id.big_text, Html.fromHtml(this.c.l.f2266a));
        this.i.setViewVisibility(R.id.big_text, 0);
        this.k = this.i;
        return a(this.b, this.k);
    }

    /* access modifiers changed from: protected */
    public void a(int i, String str) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
                this.f = 7;
                this.h = new ArrayList();
                this.h.add(str);
                break;
            case 4:
                this.f = 8;
                break;
        }
        this.g = e();
    }
}
