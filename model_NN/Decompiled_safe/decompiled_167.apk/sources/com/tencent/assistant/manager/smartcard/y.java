package com.tencent.assistant.manager.smartcard;

import android.util.Pair;
import com.tencent.assistant.model.a.i;
import com.tencent.assistant.model.a.p;
import com.tencent.assistant.model.a.q;
import com.tencent.assistant.model.a.r;
import com.tencent.assistant.model.a.s;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.cv;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public abstract class y {
    private static long d = 0;
    private static long e = TesDownloadConfig.TES_CONFIG_CHECK_PERIOD;

    /* renamed from: a  reason: collision with root package name */
    protected Map<Integer, r> f1610a = Collections.synchronizedMap(new HashMap());
    protected Map<Integer, s> b = Collections.synchronizedMap(new HashMap());
    protected Map<Integer, List<p>> c = Collections.synchronizedMap(new HashMap());

    public abstract boolean a(i iVar, List<Long> list);

    public void a(q qVar) {
        r rVar;
        int i;
        if (qVar != null) {
            r rVar2 = this.f1610a.get(Integer.valueOf(qVar.a()));
            if (rVar2 == null) {
                r rVar3 = new r();
                rVar3.e = qVar.f1650a;
                rVar3.f = qVar.b;
                rVar = rVar3;
            } else {
                rVar = rVar2;
            }
            if (qVar.d) {
                rVar.d = true;
            }
            if (qVar.c) {
                if (a(qVar.e * 1000)) {
                    rVar.f1651a++;
                }
                s sVar = this.b.get(Integer.valueOf(qVar.a()));
                if (sVar == null || sVar.i <= 0) {
                    i = 7;
                } else {
                    i = sVar.i;
                }
                if (a(qVar.e * 1000, (long) i)) {
                    rVar.b++;
                }
                rVar.c++;
            }
            this.f1610a.put(Integer.valueOf(rVar.a()), rVar);
        }
    }

    public void a(p pVar) {
        if (pVar != null) {
            Object obj = this.c.get(Integer.valueOf(pVar.f1649a));
            if (obj == null) {
                obj = new ArrayList();
            }
            obj.add(pVar);
            this.c.put(Integer.valueOf(pVar.f1649a), obj);
        }
    }

    public void a(s sVar) {
        this.b.put(Integer.valueOf(sVar.a()), sVar);
    }

    public void a(i iVar) {
    }

    public void a(int i, String str, int i2) {
        STInfoV2 sTInfoV2 = new STInfoV2(202900 + i2, STConst.ST_DEFAULT_SLOT, i, STConst.ST_DEFAULT_SLOT, 100);
        sTInfoV2.extraData = str;
        k.a(sTInfoV2);
    }

    public Pair<Boolean, r> b(i iVar) {
        r rVar = this.f1610a.get(Integer.valueOf(iVar.k()));
        s sVar = this.b.get(Integer.valueOf(iVar.k()));
        if (rVar == null) {
            rVar = new r();
            rVar.f = iVar.j;
            rVar.e = iVar.i;
            this.f1610a.put(Integer.valueOf(rVar.a()), rVar);
        }
        if (sVar == null) {
            return Pair.create(false, rVar);
        }
        if (rVar.b >= sVar.b) {
            a(iVar.s, iVar.j + "||" + iVar.i + "|" + 1, iVar.i);
            return Pair.create(false, rVar);
        } else if (rVar.f1651a < sVar.f1652a) {
            return Pair.create(true, rVar);
        } else {
            a(iVar.s, iVar.j + "||" + iVar.i + "|" + 2, iVar.i);
            return Pair.create(false, rVar);
        }
    }

    private static void a() {
        if (System.currentTimeMillis() - d > e) {
            d = cv.d();
        }
    }

    protected static boolean a(long j) {
        a();
        long j2 = j - d;
        if (j2 <= 0 || j2 >= e) {
            return false;
        }
        return true;
    }

    protected static boolean a(long j, long j2) {
        a();
        if (j - (d - ((j2 - 1) * e)) > 0) {
            return true;
        }
        return false;
    }
}
