package com.tencent.assistant.manager;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.c;
import java.util.List;

/* compiled from: ProGuard */
class ar implements CallbackHelper.Caller<c> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f1488a;
    final /* synthetic */ int b;
    final /* synthetic */ boolean c;
    final /* synthetic */ List d;
    final /* synthetic */ List e;
    final /* synthetic */ aq f;

    ar(aq aqVar, int i, int i2, boolean z, List list, List list2) {
        this.f = aqVar;
        this.f1488a = i;
        this.b = i2;
        this.c = z;
        this.d = list;
        this.e = list2;
    }

    /* renamed from: a */
    public void call(c cVar) {
        cVar.a(this.f1488a, this.b, this.c, this.d, this.e);
    }
}
