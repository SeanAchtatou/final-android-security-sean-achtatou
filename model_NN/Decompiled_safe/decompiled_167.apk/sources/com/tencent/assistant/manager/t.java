package com.tencent.assistant.manager;

import com.tencent.assistant.protocol.jce.SearchWebCfg;

/* compiled from: ProGuard */
public class t {

    /* renamed from: a  reason: collision with root package name */
    private static t f1622a;
    private SearchWebCfg b;

    public static synchronized t a() {
        t tVar;
        synchronized (t.class) {
            if (f1622a == null) {
                f1622a = new t();
            }
            tVar = f1622a;
        }
        return tVar;
    }

    public void a(SearchWebCfg searchWebCfg) {
        this.b = searchWebCfg;
    }

    public SearchWebCfg b() {
        return this.b;
    }
}
