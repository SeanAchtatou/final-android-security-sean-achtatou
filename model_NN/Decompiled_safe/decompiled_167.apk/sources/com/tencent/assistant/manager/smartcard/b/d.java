package com.tencent.assistant.manager.smartcard.b;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.a.a;
import com.tencent.assistant.model.a.s;
import com.tencent.assistant.model.a.t;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.SmartCardPicDownloadNode;
import com.tencent.assistant.protocol.jce.SmartCardPicTemplate;
import com.tencent.assistant.protocol.jce.SmartCardTitle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class d extends a {

    /* renamed from: a  reason: collision with root package name */
    public List<t> f1592a;
    private byte b;
    private boolean c = false;
    private int d;
    private int e;
    private boolean f = true;
    private int g = 0;
    private String h;

    public boolean a(byte b2, JceStruct jceStruct) {
        if (!(jceStruct instanceof SmartCardPicTemplate)) {
            return false;
        }
        SmartCardPicTemplate smartCardPicTemplate = (SmartCardPicTemplate) jceStruct;
        SmartCardTitle smartCardTitle = smartCardPicTemplate.b;
        this.i = b2;
        if (smartCardTitle != null) {
            this.b = smartCardTitle.f2339a;
            this.k = smartCardTitle.b;
            this.l = smartCardTitle.e;
            this.o = smartCardTitle.c;
            this.n = smartCardTitle.d;
        }
        this.m = smartCardPicTemplate.c;
        this.j = smartCardPicTemplate.f2326a;
        if (this.f1592a == null) {
            this.f1592a = new ArrayList();
        } else {
            this.f1592a.clear();
        }
        if (smartCardPicTemplate.e != null) {
            Iterator<SmartCardPicDownloadNode> it = smartCardPicTemplate.e.iterator();
            while (it.hasNext()) {
                SmartCardPicDownloadNode next = it.next();
                t tVar = new t();
                tVar.b = next.b;
                tVar.f1653a = u.a(next.f2324a);
                if (tVar.f1653a != null) {
                    u.a(tVar.f1653a);
                }
                this.f1592a.add(tVar);
            }
        }
        this.c = smartCardPicTemplate.a();
        this.d = smartCardPicTemplate.c();
        this.e = smartCardPicTemplate.d();
        this.f = !smartCardPicTemplate.k;
        this.h = smartCardPicTemplate.f;
        this.g = smartCardPicTemplate.e();
        return true;
    }

    public s b() {
        if (this.d <= 0 || this.e <= 0) {
            return null;
        }
        s sVar = new s();
        sVar.f = this.j;
        sVar.e = this.i;
        sVar.f1652a = this.d;
        sVar.b = this.e;
        return sVar;
    }

    public List<SimpleAppModel> a() {
        if (this.f1592a == null || this.f1592a.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(this.f1592a.size());
        for (t tVar : this.f1592a) {
            arrayList.add(tVar.f1653a);
        }
        return arrayList;
    }

    public String d() {
        return i() + "_" + (this.f1592a == null ? 0 : this.f1592a.size());
    }

    public void c() {
        if (this.f && this.f1592a != null && this.f1592a.size() > 0) {
            Iterator<t> it = this.f1592a.iterator();
            while (it.hasNext()) {
                t next = it.next();
                if (!(next.f1653a == null || ApkResourceManager.getInstance().getInstalledApkInfo(next.f1653a.c) == null)) {
                    it.remove();
                }
            }
        }
    }

    public byte e() {
        return this.b;
    }

    public boolean f() {
        return this.c;
    }

    public int g() {
        return this.g;
    }
}
