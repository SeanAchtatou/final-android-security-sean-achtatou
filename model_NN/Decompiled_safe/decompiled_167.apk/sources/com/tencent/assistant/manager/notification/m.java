package com.tencent.assistant.manager.notification;

import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class m {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<n> f1572a = new ArrayList<>();

    public int a() {
        return this.f1572a.size();
    }

    public boolean b() {
        return this.f1572a.isEmpty();
    }

    public void a(String str, String str2, int i, String str3) {
        n nVar = new n(this, str, str2, i, str3);
        Iterator<n> it = this.f1572a.iterator();
        while (it.hasNext()) {
            if (str.equals(it.next().c)) {
                return;
            }
        }
        this.f1572a.add(nVar);
    }

    public n a(String str) {
        Iterator<n> it = this.f1572a.iterator();
        while (it.hasNext()) {
            n next = it.next();
            if (str.equals(next.c)) {
                this.f1572a.remove(next);
                return next;
            }
        }
        return null;
    }

    public n a(int i) {
        if (i < 0 || this.f1572a.size() <= i) {
            return null;
        }
        return this.f1572a.remove(i);
    }

    public n b(int i) {
        if (i < 0 || this.f1572a.size() <= i) {
            return null;
        }
        return this.f1572a.get(i);
    }

    public n b(String str) {
        Iterator<n> it = this.f1572a.iterator();
        while (it.hasNext()) {
            n next = it.next();
            if (str.equals(next.c)) {
                return next;
            }
        }
        return null;
    }
}
