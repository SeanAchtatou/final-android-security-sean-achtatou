package com.tencent.assistant.manager;

import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class ba {

    /* renamed from: a  reason: collision with root package name */
    private static ba f1497a = null;
    private final int b = 10;
    private PushInfo c = null;

    public static synchronized ba a() {
        ba baVar;
        synchronized (ba.class) {
            if (f1497a == null) {
                f1497a = new ba();
            }
            baVar = f1497a;
        }
        return baVar;
    }

    private ba() {
    }

    public ArrayList<Long> b() {
        ArrayList<Long> arrayList = new ArrayList<>();
        String[] split = m.a().a("key_push_info_id_list", Constants.STR_EMPTY).split(" ");
        if (split.length > 0) {
            for (int length = split.length - 1; length >= 0; length--) {
                try {
                    long parseLong = Long.parseLong(split[length]);
                    if (!arrayList.contains(Long.valueOf(parseLong))) {
                        arrayList.add(Long.valueOf(parseLong));
                        if (arrayList.size() >= 10) {
                            break;
                        }
                    } else {
                        continue;
                    }
                } catch (Exception e) {
                }
            }
        }
        return arrayList;
    }

    public void a(PushInfo pushInfo) {
        c(pushInfo);
    }

    private void c(PushInfo pushInfo) {
        TemporaryThreadManager.get().start(new bb(this, pushInfo));
    }

    public void b(PushInfo pushInfo) {
        this.c = pushInfo;
    }

    public PushInfo c() {
        return this.c;
    }
}
