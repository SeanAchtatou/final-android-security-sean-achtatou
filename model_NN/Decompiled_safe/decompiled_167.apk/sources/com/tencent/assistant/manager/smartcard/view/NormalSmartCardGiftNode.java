package com.tencent.assistant.manager.smartcard.view;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.smartcard.b.a;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class NormalSmartCardGiftNode extends RelativeLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1602a;
    private TXImageView b;
    private DownloadButton c;
    private TextView d;
    private ListItemInfoView e;
    private TextView f;
    private ViewGroup g;
    private View h;
    private View i;
    private TextView j;
    private TextView k;
    private View l;
    /* access modifiers changed from: private */
    public a m;

    public NormalSmartCardGiftNode(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f1602a = context;
        a();
    }

    public NormalSmartCardGiftNode(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f1602a = context;
        a();
    }

    public NormalSmartCardGiftNode(Context context) {
        super(context);
        this.f1602a = context;
        a();
    }

    private void a() {
        LayoutInflater.from(this.f1602a).inflate((int) R.layout.smartcard_gift_template_item_layout, this);
        this.b = (TXImageView) findViewById(R.id.app_icon_img);
        this.c = (DownloadButton) findViewById(R.id.state_app_btn);
        this.d = (TextView) findViewById(R.id.app_name);
        this.e = (ListItemInfoView) findViewById(R.id.download_info);
        this.e.a(ListItemInfoView.InfoType.DOWNTIMES_SIZE);
        this.f = (TextView) findViewById(R.id.recommend_reason);
        this.h = findViewById(R.id.cutline);
        this.i = findViewById(R.id.arrow);
        this.g = (ViewGroup) findViewById(R.id.explain_area);
        a(false);
        this.g.setOnClickListener(new a(this));
        this.j = (TextView) findViewById(R.id.explain_title);
        this.k = (TextView) findViewById(R.id.explain_des);
        this.l = findViewById(R.id.m_cutline);
        setBackgroundResource(R.drawable.v2_button_background_light_selector);
    }

    public void a(a aVar, STInfoV2 sTInfoV2, boolean z) {
        a(false);
        if (aVar != null) {
            this.m = aVar;
            this.b.updateImageView(this.m.f1589a.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.c.a(this.m.f1589a);
            this.d.setText(this.m.f1589a.d);
            this.e.a(this.m.f1589a);
            this.f.setText(this.m.c);
            this.c.setOnClickListener(new b(this, sTInfoV2));
            c cVar = new c(this, aVar, sTInfoV2);
            setOnClickListener(cVar);
            this.g.setOnClickListener(cVar);
        }
        if (z) {
            this.l.setVisibility(8);
        } else {
            this.l.setVisibility(0);
        }
    }

    private void a(boolean z) {
        if (z) {
            this.g.setVisibility(0);
            this.h.setVisibility(0);
            this.i.setVisibility(0);
            return;
        }
        this.h.setVisibility(8);
        this.i.setVisibility(8);
        this.g.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void a(SimpleAppModel simpleAppModel, STCommonInfo sTCommonInfo) {
        if (simpleAppModel != null) {
            DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel);
            StatInfo a3 = com.tencent.assistantv2.st.page.a.a(sTCommonInfo);
            if (a2 != null && a2.needReCreateInfo(simpleAppModel)) {
                DownloadProxy.a().b(a2.downloadTicket);
                a2 = null;
            }
            if (a2 == null) {
                a2 = DownloadInfo.createDownloadInfo(simpleAppModel, a3);
            } else {
                a2.updateDownloadInfoStatInfo(a3);
            }
            switch (d.f1607a[u.d(simpleAppModel).ordinal()]) {
                case 1:
                case 2:
                    if (this.m != null && !TextUtils.isEmpty(this.m.d) && !TextUtils.isEmpty(this.m.e)) {
                        a(true);
                        this.j.setText(this.m.d);
                        this.k.setText(Html.fromHtml(this.m.e));
                    }
                    com.tencent.assistant.download.a.a().a(a2);
                    return;
                case 3:
                case 4:
                    com.tencent.assistant.download.a.a().b(a2.downloadTicket);
                    return;
                case 5:
                    if (this.m != null && !TextUtils.isEmpty(this.m.d) && !TextUtils.isEmpty(this.m.e)) {
                        a(true);
                        this.j.setText(this.m.d);
                        this.k.setText(Html.fromHtml(this.m.e));
                    }
                    com.tencent.assistant.download.a.a().b(a2);
                    return;
                case 6:
                    com.tencent.assistant.download.a.a().d(a2);
                    return;
                case 7:
                    com.tencent.assistant.download.a.a().c(a2);
                    return;
                case 8:
                case 9:
                    com.tencent.assistant.download.a.a().a(a2);
                    return;
                case 10:
                    Toast.makeText(this.f1602a, (int) R.string.tips_slicent_install, 0).show();
                    return;
                case 11:
                    Toast.makeText(this.f1602a, (int) R.string.tips_slicent_uninstall, 0).show();
                    return;
                case 12:
                    Toast.makeText(this.f1602a, (int) R.string.unsupported, 0).show();
                    return;
                default:
                    return;
            }
        }
    }
}
