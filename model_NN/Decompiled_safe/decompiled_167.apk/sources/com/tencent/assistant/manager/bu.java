package com.tencent.assistant.manager;

import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.download.a;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.assistant.utils.v;

/* compiled from: ProGuard */
class bu implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallUninstallTaskBean f1515a;
    final /* synthetic */ bo b;

    bu(bo boVar, InstallUninstallTaskBean installUninstallTaskBean) {
        this.b = boVar;
        this.f1515a = installUninstallTaskBean;
    }

    public void onClick(View view) {
        try {
            this.b.b.setVisibility(8);
            a.a().a(this.f1515a.packageName, this.f1515a.applinkActionUrl);
        } catch (Exception e) {
            bv bvVar = new bv(this);
            bvVar.titleRes = AstApp.i().getResources().getString(R.string.down_uninstall_title);
            bvVar.contentRes = AstApp.i().getResources().getString(R.string.down_cannot_open_tips);
            bvVar.btnTxtRes = AstApp.i().getResources().getString(R.string.down_uninstall_tips_close);
            v.a(bvVar);
        }
    }
}
