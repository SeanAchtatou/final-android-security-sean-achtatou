package com.tencent.assistant.manager.smartcard;

import android.util.SparseArray;
import com.tencent.assistant.manager.smartcard.a.a;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static Map<Integer, c> f1588a = new ConcurrentHashMap(5);

    static {
        f1588a.put(34, new com.tencent.cloud.smartcard.a.b());
        f1588a.put(36, new a());
        f1588a.put(38, new com.tencent.assistant.manager.smartcard.a.b());
        f1588a.put(37, new com.tencent.cloud.smartcard.a.a());
    }

    public static c a(int i) {
        return f1588a.get(Integer.valueOf(i));
    }

    public static void a(SparseArray<y> sparseArray) {
        if (f1588a != null && f1588a.size() > 0) {
            for (Map.Entry next : f1588a.entrySet()) {
                sparseArray.put(((Integer) next.getKey()).intValue(), ((c) next.getValue()).d());
            }
        }
    }
}
