package com.tencent.assistant.localres;

import com.tencent.assistant.model.SimpleAppModel;
import java.util.Comparator;

/* compiled from: ProGuard */
final class m implements Comparator<SimpleAppModel> {
    m() {
    }

    /* renamed from: a */
    public int compare(SimpleAppModel simpleAppModel, SimpleAppModel simpleAppModel2) {
        if (simpleAppModel == null || simpleAppModel2 == null) {
            return 0;
        }
        long j = (long) ((int) (simpleAppModel2.H - simpleAppModel.H));
        if (j != 0) {
            return (int) j;
        }
        return simpleAppModel2.C.charAt(0) - simpleAppModel.C.charAt(0);
    }
}
