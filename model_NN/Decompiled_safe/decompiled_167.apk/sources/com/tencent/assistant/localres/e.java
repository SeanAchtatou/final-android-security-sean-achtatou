package com.tencent.assistant.localres;

import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkResourceManager f1429a;

    e(ApkResourceManager apkResourceManager) {
        this.f1429a = apkResourceManager;
    }

    public void run() {
        XLog.d("AppUpdateEngine", "***** startHeavyLoad启动扫描");
        Map<String, LocalApkInfo> a2 = this.f1429a.f.a(this.f1429a.h, false);
        if (a2 != null) {
            this.f1429a.h.clear();
            this.f1429a.h.putAll(a2);
            boolean unused = ApkResourceManager.l = true;
            this.f1429a.updateAppLauncherTime();
            XLog.d("AppUpdateEngine", "***** startHeavyLoad启动扫描成功");
            this.f1429a.f();
            this.f1429a.g();
            if (this.f1429a.f.a()) {
                ArrayList arrayList = new ArrayList(this.f1429a.h.values());
                this.f1429a.c.b(arrayList, 1);
                this.f1429a.c.a(arrayList);
            }
        } else {
            boolean unused2 = ApkResourceManager.l = true;
            this.f1429a.i();
        }
        boolean unused3 = ApkResourceManager.j = false;
    }
}
