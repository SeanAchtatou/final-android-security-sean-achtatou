package com.tencent.assistant.localres;

import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.e;
import java.lang.ref.WeakReference;
import java.util.Iterator;

/* compiled from: ProGuard */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1432a;
    final /* synthetic */ boolean b;
    final /* synthetic */ ApkResourceManager c;

    h(ApkResourceManager apkResourceManager, String str, boolean z) {
        this.c = apkResourceManager;
        this.f1432a = str;
        this.b = z;
    }

    public void run() {
        LocalApkInfo b2 = e.b(this.f1432a);
        if (b2 != null) {
            this.c.h.put(b2.mPackageName, b2);
            Iterator it = this.c.e.iterator();
            while (it.hasNext()) {
                ApkResCallback apkResCallback = (ApkResCallback) ((WeakReference) it.next()).get();
                if (apkResCallback != null) {
                    if (apkResCallback.onInstallApkChangedNeedReplacedEvent()) {
                        apkResCallback.onInstalledApkDataChanged(b2, 3, this.b);
                    } else {
                        apkResCallback.onInstalledApkDataChanged(b2, 3);
                    }
                }
            }
            this.c.c.b(b2, 1);
        }
    }
}
