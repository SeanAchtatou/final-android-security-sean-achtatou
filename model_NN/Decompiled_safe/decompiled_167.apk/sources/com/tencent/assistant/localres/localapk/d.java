package com.tencent.assistant.localres.localapk;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Messenger;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class d implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f1440a;

    d(a aVar) {
        this.f1440a = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.localapk.a.a(com.tencent.assistant.localres.localapk.a, boolean):boolean
     arg types: [com.tencent.assistant.localres.localapk.a, int]
     candidates:
      com.tencent.assistant.localres.localapk.a.a(com.tencent.assistant.localres.localapk.a, android.os.Messenger):android.os.Messenger
      com.tencent.assistant.localres.localapk.a.a(int, android.os.Bundle):void
      com.tencent.assistant.localres.localapk.a.a(com.tencent.assistant.localres.localapk.a, int):void
      com.tencent.assistant.localres.localapk.a.a(com.tencent.assistant.localres.localapk.a, com.tencent.assistant.localres.model.LocalApkInfo):void
      com.tencent.assistant.localres.localapk.a.a(java.lang.String, int):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.assistant.localres.localapk.a.a(com.tencent.assistant.localres.localapk.a, boolean):boolean */
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        XLog.d(a.c, "onServiceConnected");
        Messenger unused = this.f1440a.j = new Messenger(iBinder);
        boolean unused2 = this.f1440a.k = true;
        if (this.f1440a.b) {
            try {
                TemporaryThreadManager.get().start(new e(this));
            } catch (OutOfMemoryError e) {
            }
            this.f1440a.b = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.localapk.a.a(com.tencent.assistant.localres.localapk.a, boolean):boolean
     arg types: [com.tencent.assistant.localres.localapk.a, int]
     candidates:
      com.tencent.assistant.localres.localapk.a.a(com.tencent.assistant.localres.localapk.a, android.os.Messenger):android.os.Messenger
      com.tencent.assistant.localres.localapk.a.a(int, android.os.Bundle):void
      com.tencent.assistant.localres.localapk.a.a(com.tencent.assistant.localres.localapk.a, int):void
      com.tencent.assistant.localres.localapk.a.a(com.tencent.assistant.localres.localapk.a, com.tencent.assistant.localres.model.LocalApkInfo):void
      com.tencent.assistant.localres.localapk.a.a(java.lang.String, int):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.assistant.localres.localapk.a.a(com.tencent.assistant.localres.localapk.a, boolean):boolean */
    public void onServiceDisconnected(ComponentName componentName) {
        XLog.d(a.c, "onServiceDisconnected");
        Messenger unused = this.f1440a.j = (Messenger) null;
        boolean unused2 = this.f1440a.k = false;
        if (a.c(this.f1440a) < 20) {
            this.f1440a.b = true;
            this.f1440a.c();
        }
    }
}
