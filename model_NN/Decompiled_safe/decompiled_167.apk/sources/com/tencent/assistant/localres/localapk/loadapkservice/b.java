package com.tencent.assistant.localres.localapk.loadapkservice;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tencent.assistant.utils.XLog;
import java.lang.ref.WeakReference;

/* compiled from: ProGuard */
class b extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference<GetApkInfoService> f1448a;

    b(Looper looper, GetApkInfoService getApkInfoService) {
        super(looper);
        this.f1448a = new WeakReference<>(getApkInfoService);
    }

    public void handleMessage(Message message) {
        XLog.d(GetApkInfoService.f1446a, "[GetApkInfoService] handleMessage msg.what: " + message.what);
        if (this.f1448a.get() != null) {
            switch (message.what) {
                case 1:
                    Bundle bundle = new Bundle();
                    bundle.putString("s", "Hello!");
                    this.f1448a.get().a(message.replyTo, 1, message.arg1, bundle);
                    return;
                case 2:
                    this.f1448a.get().a(message);
                    return;
                case 3:
                    this.f1448a.get().b(message);
                    return;
                default:
                    super.handleMessage(message);
                    return;
            }
        }
    }
}
