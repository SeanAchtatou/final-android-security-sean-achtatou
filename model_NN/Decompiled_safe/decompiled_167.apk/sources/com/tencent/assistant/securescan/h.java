package com.tencent.assistant.securescan;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class h extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f2506a;
    final /* synthetic */ int b;
    final /* synthetic */ e c;

    h(e eVar, SimpleAppModel simpleAppModel, int i) {
        this.c = eVar;
        this.f2506a = simpleAppModel;
        this.b = i;
    }

    public void onTMAClick(View view) {
        this.c.a(this.f2506a, this.b, view);
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c.f, this.f2506a, this.c.a(this.b), 200, null);
        if (buildSTInfo != null) {
            buildSTInfo.updateStatus(this.f2506a);
        }
        return buildSTInfo;
    }
}
