package com.tencent.assistant.securescan;

import android.util.Log;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.securemodule.impl.AppInfo;
import com.tencent.securemodule.service.CloudScanListener;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class y implements CloudScanListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartScanActivity f2523a;

    private y(StartScanActivity startScanActivity) {
        this.f2523a = startScanActivity;
    }

    /* synthetic */ y(StartScanActivity startScanActivity, q qVar) {
        this(startScanActivity);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.securescan.StartScanActivity.a(com.tencent.assistant.securescan.StartScanActivity, java.lang.Boolean):java.lang.Boolean
     arg types: [com.tencent.assistant.securescan.StartScanActivity, int]
     candidates:
      com.tencent.assistant.securescan.StartScanActivity.a(com.tencent.assistant.securescan.StartScanActivity, long):long
      com.tencent.assistant.securescan.StartScanActivity.a(com.tencent.assistant.securescan.StartScanActivity, com.tencent.assistant.localres.model.LocalApkInfo):java.lang.Boolean
      com.tencent.assistant.securescan.StartScanActivity.a(long, java.util.List<com.tencent.securemodule.impl.AppInfo>):void
      com.tencent.assistant.securescan.StartScanActivity.a(com.tencent.assistant.securescan.StartScanActivity, int):void
      com.tencent.assistant.securescan.StartScanActivity.a(int, int):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.module.callback.m.a(int, int):void
      com.tencent.assistant.securescan.StartScanActivity.a(com.tencent.assistant.securescan.StartScanActivity, java.lang.Boolean):java.lang.Boolean */
    public void onFinish(int i) {
        long j = 0;
        long unused = this.f2523a.Y = System.currentTimeMillis();
        if (this.f2523a.X != 0) {
            j = this.f2523a.Y - this.f2523a.X;
        }
        this.f2523a.a(j, (List) null);
        Boolean unused2 = this.f2523a.R = (Boolean) true;
        this.f2523a.n.cancel();
        Log.i("StartScanActivity", "onFinish");
        if (i != 0) {
            this.f2523a.d(10);
        } else if (!this.f2523a.R.booleanValue() || !StartScanActivity.a(this.f2523a.w)) {
            this.f2523a.d(10);
        } else {
            this.f2523a.d(11);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.securescan.StartScanActivity.a(com.tencent.assistant.securescan.StartScanActivity, java.lang.Boolean):java.lang.Boolean
     arg types: [com.tencent.assistant.securescan.StartScanActivity, int]
     candidates:
      com.tencent.assistant.securescan.StartScanActivity.a(com.tencent.assistant.securescan.StartScanActivity, long):long
      com.tencent.assistant.securescan.StartScanActivity.a(com.tencent.assistant.securescan.StartScanActivity, com.tencent.assistant.localres.model.LocalApkInfo):java.lang.Boolean
      com.tencent.assistant.securescan.StartScanActivity.a(long, java.util.List<com.tencent.securemodule.impl.AppInfo>):void
      com.tencent.assistant.securescan.StartScanActivity.a(com.tencent.assistant.securescan.StartScanActivity, int):void
      com.tencent.assistant.securescan.StartScanActivity.a(int, int):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.module.callback.m.a(int, int):void
      com.tencent.assistant.securescan.StartScanActivity.a(com.tencent.assistant.securescan.StartScanActivity, java.lang.Boolean):java.lang.Boolean */
    public void onRiskFoud(List<AppInfo> list) {
        long j = 0;
        Log.i("StartScanActivity", "onRiskFoun");
        long unused = this.f2523a.Y = System.currentTimeMillis();
        if (this.f2523a.X != 0) {
            j = this.f2523a.Y - this.f2523a.X;
        }
        this.f2523a.a(j, list);
        Boolean unused2 = this.f2523a.R = (Boolean) true;
        this.f2523a.I.clear();
        this.f2523a.I.addAll(list);
        if (this.f2523a.I != null && this.f2523a.I.size() > 0) {
            ArrayList<SimpleAppModel> f = u.f(this.f2523a.I);
            this.f2523a.K.clear();
            this.f2523a.K.addAll(f);
            int unused3 = this.f2523a.V = this.f2523a.N.a(f);
        } else if (this.f2523a.R.booleanValue()) {
            this.f2523a.n.cancel();
            this.f2523a.d(11);
        }
    }

    @Deprecated
    public void onRiskFound() {
    }
}
