package com.tencent.assistant.usagestats;

import android.os.Parcel;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.st.STConstAction;

/* compiled from: ProGuard */
public class n {
    private static final int[] c = {250, STConstAction.ACTION_HIT_OPEN, 750, 1000, 1500, 2000, EventDispatcherEnum.CACHE_EVENT_START, EventDispatcherEnum.CACHE_EVENT_END, EventDispatcherEnum.CONNECTION_EVENT_START};

    /* renamed from: a  reason: collision with root package name */
    int f2602a;
    int[] b = new int[10];

    n() {
    }

    n(Parcel parcel) {
        this.f2602a = parcel.readInt();
        int[] iArr = this.b;
        for (int i = 0; i < 10; i++) {
            iArr[i] = parcel.readInt();
        }
    }
}
