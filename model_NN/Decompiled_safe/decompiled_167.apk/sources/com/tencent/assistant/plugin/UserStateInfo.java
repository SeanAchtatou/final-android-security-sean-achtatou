package com.tencent.assistant.plugin;

/* compiled from: ProGuard */
public class UserStateInfo {

    /* renamed from: a  reason: collision with root package name */
    private int f1940a;
    private UserLoginInfo b;

    public UserStateInfo(int i) {
        this.f1940a = i;
    }

    public int getStateChangeType() {
        return this.f1940a;
    }

    public void setStateChangeType(int i) {
        this.f1940a = i;
    }

    public UserLoginInfo getUserLoginInfo() {
        return this.b;
    }

    public void setUserLoginInfo(UserLoginInfo userLoginInfo) {
        this.b = userLoginInfo;
    }

    public String toString() {
        return "UserStateInfo{stateChangeType=" + this.f1940a + ", userLoginInfo=" + this.b + '}';
    }
}
