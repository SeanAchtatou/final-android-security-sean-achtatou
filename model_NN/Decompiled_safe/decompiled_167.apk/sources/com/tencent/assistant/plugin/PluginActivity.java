package com.tencent.assistant.plugin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.qq.ndk.NativeFileObject;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.st.f;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.assistantv2.st.page.a;
import java.lang.reflect.Field;
import java.util.List;

/* compiled from: ProGuard */
public class PluginActivity extends Activity {
    public static final String PARAMS_PRE_ACTIVITY_SLOT_TAG_NAME = "preActivitySlotTagName";
    public static final String PARAMS_PRE_ACTIVITY_TAG_NAME = "preActivityTagName";

    /* renamed from: a  reason: collision with root package name */
    protected boolean f1928a = false;
    protected Activity b = null;
    boolean c = false;
    protected PluginInfo d = null;
    protected PluginLoaderInfo e;
    protected STPageInfo f = new STPageInfo();
    private Context g = null;
    private Activity h = null;
    private int i;
    private Intent j;
    private boolean k = false;
    private int l = 2000;
    public View mContentView = null;

    public void onCreate(Bundle bundle) {
        if (this.f1928a) {
            this.h = this.b;
            int intExtra = getIntent().getIntExtra(PARAMS_PRE_ACTIVITY_TAG_NAME, 2000);
            if (intExtra == -1000) {
                f.a(intExtra);
            } else {
                f.a(getActivityPageId());
            }
            a();
            if (b()) {
                activityExposureReport();
                return;
            }
            return;
        }
        super.onCreate(bundle);
        this.h = this;
    }

    public void setContentView(int i2) {
        if (this.f1928a) {
            this.mContentView = LayoutInflater.from(this.g).inflate(i2, (ViewGroup) null);
            this.h.setContentView(this.mContentView);
            return;
        }
        this.h.setContentView(i2);
    }

    public void setContentView(View view) {
        this.mContentView = view;
        this.h.setContentView(view);
    }

    public View findViewById(int i2) {
        if (!this.f1928a || this.mContentView == null) {
            return super.findViewById(i2);
        }
        return this.mContentView.findViewById(i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void startActivity(Intent intent) {
        List<ResolveInfo> queryIntentActivities = this.h.getPackageManager().queryIntentActivities(intent, NativeFileObject.S_IFIFO);
        if (queryIntentActivities != null && queryIntentActivities.size() != 0) {
            this.h.startActivity(intent);
        } else if (this.f1928a) {
            intent.putExtra("_plugin_IsPluginActivity", true);
            intent.putExtra("_plugin_PluginPakcageName", this.d.getPackageName());
            intent.putExtra("_plugin_PluginVersionCode", this.d.getVersion());
            this.h.startActivity(intent);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void startActivityForResult(Intent intent, int i2) {
        List<ResolveInfo> queryIntentActivities = this.h.getPackageManager().queryIntentActivities(intent, NativeFileObject.S_IFIFO);
        if (queryIntentActivities != null && queryIntentActivities.size() != 0) {
            this.h.startActivityForResult(intent, i2);
        } else if (this.f1928a) {
            intent.putExtra("_plugin_IsPluginActivity", true);
            intent.putExtra("_plugin_PluginPakcageName", this.d.getPackageName());
            intent.putExtra("_plugin_PluginVersionCode", this.d.getVersion());
            this.h.startActivityForResult(intent, i2);
        }
    }

    public void onResume() {
        if (!this.f1928a) {
            super.onResume();
        }
    }

    public void onPause() {
        if (!this.f1928a) {
            super.onPause();
        }
    }

    public void onStart() {
        if (!this.f1928a) {
            super.onStart();
        }
    }

    public void onRestart() {
        if (!this.f1928a) {
            super.onRestart();
        }
    }

    public void onStop() {
        if (!this.f1928a) {
            super.onStop();
        }
    }

    public void onDestroy() {
        if (this.f1928a) {
            this.e = null;
            this.mContentView = null;
            return;
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.f1928a) {
            return false;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (this.f1928a) {
            return false;
        }
        return super.onKeyUp(i2, keyEvent);
    }

    public boolean onKeyMultiple(int i2, int i3, KeyEvent keyEvent) {
        if (this.f1928a) {
            return false;
        }
        return super.onKeyMultiple(i2, i3, keyEvent);
    }

    public void finish() {
        if (!this.f1928a || this.b == null) {
            super.finish();
            return;
        }
        synchronized (this) {
            try {
                if (!this.k) {
                    int i2 = 0;
                    Field declaredField = Activity.class.getDeclaredField("mResultCode");
                    declaredField.setAccessible(true);
                    try {
                        i2 = ((Integer) declaredField.get(this)).intValue();
                    } catch (Exception e2) {
                    }
                    Field declaredField2 = Activity.class.getDeclaredField("mResultData");
                    declaredField2.setAccessible(true);
                    this.i = i2;
                    this.j = (Intent) declaredField2.get(this);
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        this.b.finish();
        this.c = true;
    }

    public boolean isFinishing() {
        if (this.c) {
            return true;
        }
        if (!this.f1928a || this.b == null) {
            return super.isFinishing();
        }
        return this.b.isFinishing();
    }

    public boolean onPluginBackPressed() {
        return true;
    }

    public void onPluginRestoreInstanceState(Bundle bundle) {
        onRestoreInstanceState(bundle);
    }

    public void onRestoreInstanceState(Bundle bundle) {
    }

    public void onPluginSaveInstanceState(Bundle bundle) {
        onSaveInstanceState(bundle);
    }

    public void onSaveInstanceState(Bundle bundle) {
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f1928a) {
            return false;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void init(PluginInfo pluginInfo, Activity activity, PluginLoaderInfo pluginLoaderInfo) {
        this.f1928a = true;
        this.b = activity;
        this.d = pluginInfo;
        this.e = pluginLoaderInfo;
        this.g = this.e.getContext();
        attachBaseContext(this.g);
    }

    public LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(this.g);
    }

    public Resources getPluginResource() {
        if (this.g != null) {
            return this.g.getResources();
        }
        return this.h.getResources();
    }

    public Resources getOutResources() {
        if (!this.f1928a || this.b == null) {
            return this.h.getResources();
        }
        return this.b.getResources();
    }

    public Activity getOutActivity() {
        return this.b;
    }

    public Object getSystemService(String str) {
        if (("window".equals(str) || "search".equals(str)) && this.h != null) {
            return this.h.getSystemService(str);
        }
        if (this.g != null) {
            return this.g.getSystemService(str);
        }
        return AstApp.i().getSystemService(str);
    }

    public Context getPluginFrameContext() {
        return this.g;
    }

    public WindowManager getWindowManager() {
        return this.h.getWindowManager();
    }

    public int getChangingConfigurations() {
        return this.h.getChangingConfigurations();
    }

    public Window getWindow() {
        return this.h.getWindow();
    }

    public void setRequestedOrientation(int i2) {
        if (this.h != null) {
            this.h.setRequestedOrientation(i2);
        }
    }

    public String getPackageName() {
        return this.d.getPackageName();
    }

    public ApplicationInfo getApplicationInfo() {
        return this.d.getApplicationInfo();
    }

    public void onNewIntent(Intent intent) {
        if (!this.f1928a) {
            super.onNewIntent(intent);
        }
    }

    public int getTaskId() {
        if (!this.f1928a || this.b == null) {
            return super.getTaskId();
        }
        return this.b.getTaskId();
    }

    public void onPluginActivityResult(int i2, int i3, Intent intent) {
        onActivityResult(i2, i3, intent);
    }

    public void setPluginResult(int i2) {
        synchronized (this) {
            this.i = i2;
            this.j = null;
            this.k = true;
        }
    }

    public void setPluginResult(int i2, Intent intent) {
        synchronized (this) {
            this.i = i2;
            this.j = intent;
            this.k = true;
        }
    }

    public int getResultCode() {
        int i2;
        synchronized (this) {
            i2 = this.i;
        }
        return i2;
    }

    public Intent getResultData() {
        Intent intent;
        synchronized (this) {
            intent = this.j;
        }
        return intent;
    }

    public void onActivityException(Throwable th) {
        th.printStackTrace();
        Toast.makeText(this.b, (int) R.string.plugin_activity_exception, 0).show();
    }

    public Context getApplicationContext() {
        if (getOutActivity() == null || this.d == null || TextUtils.isEmpty(this.d.getPackageName()) || !"com.qqreader".equals(this.d.getPackageName())) {
            return super.getApplicationContext();
        }
        return getOutActivity().getApplicationContext();
    }

    public int getActivityPageId() {
        return 2000;
    }

    public int getActivityPrePageId() {
        return this.l;
    }

    public void setActivityPrePageId(int i2) {
        this.l = i2;
        be.a().b(this.l);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.f.c = getIntent().getIntExtra(PARAMS_PRE_ACTIVITY_TAG_NAME, 2000);
        this.f.d = getIntent().getStringExtra(PARAMS_PRE_ACTIVITY_SLOT_TAG_NAME);
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return true;
    }

    public STPageInfo getStPageInfo() {
        this.f.f3358a = getActivityPageId();
        return this.f;
    }

    public void setActivityStatus(String str, String str2) {
        this.f.e = a.b(str, str2);
    }

    public void activityExposureReport() {
        k.a(STInfoBuilder.buildSTInfo(this, 100));
    }
}
