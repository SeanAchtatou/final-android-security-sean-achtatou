package com.tencent.assistant.plugin;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Process;
import android.util.Log;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.qq.AppService.SmsSentReceiver;
import com.tencent.assistant.manager.av;
import com.tencent.assistant.manager.notification.y;
import com.tencent.assistant.model.j;
import com.tencent.assistant.plugin.a.b;
import com.tencent.assistant.plugin.mgr.c;
import com.tencent.assistant.plugin.mgr.d;
import com.tencent.assistant.plugin.mgr.e;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import com.tencent.assistant.plugin.system.PluginBackToBaoReceiver;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ba;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.Map;

/* compiled from: ProGuard */
public class PluginHelper {
    public static final int PLUGIN_REQUIRE_GETTING_PLUGINSLIST = -1;
    public static final int PLUGIN_REQUIRE_INSTALL_DOING = 0;
    public static final int PLUGIN_REQUIRE_INSTALL_INSTALLED = 1;

    /* renamed from: a  reason: collision with root package name */
    private static String f1931a = "PluginHelper";

    public static PluginContext getPluginContext(String str, int i) {
        PluginLoaderInfo a2;
        PluginInfo a3 = d.b().a(str, i);
        if (a3 == null || (a2 = c.a(AstApp.i(), a3)) == null) {
            return null;
        }
        return a2.getContext();
    }

    public static ClassLoader getPluginClassLoader(String str, int i) {
        PluginLoaderInfo a2;
        PluginInfo a3 = d.b().a(str, i);
        if (a3 == null || (a2 = c.a(AstApp.i(), a3)) == null) {
            return null;
        }
        return a2.getClassLoader();
    }

    public static Notification createPluginNotification(String str, int i, boolean z, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, PendingIntent pendingIntent, boolean z2, Bitmap bitmap) {
        return y.a(AstApp.i(), charSequence, charSequence2, charSequence3, pendingIntent, z, z2);
    }

    public static boolean startPluginService(String str, int i, Intent intent, String str2) {
        PluginContext pluginContext = getPluginContext(str, i);
        if (pluginContext != null) {
            return pluginContext.startPluginService(intent, str2);
        }
        return false;
    }

    public static void startActivity(String str, int i, Intent intent, String str2) {
        if (intent != null && AstApp.i() != null) {
            if ((intent.getFlags() & 268435456) != 268435456) {
                intent.addFlags(268435456);
            }
            PluginInfo a2 = d.b().a(str, i);
            if (a2 != null) {
                PluginProxyActivity.a(AstApp.i(), str, i, str2, a2.getInProcess(), intent, a2.getLaunchApplication());
            }
        }
    }

    public static void startActivity(Context context, String str, int i, Intent intent, String str2) {
        Context context2;
        if (intent != null) {
            if (context == null) {
                context2 = AstApp.i();
            } else {
                context2 = context;
            }
            if ((intent.getFlags() & 268435456) != 268435456 && !(context2 instanceof Activity)) {
                intent.addFlags(268435456);
            }
            PluginInfo a2 = d.b().a(str, i);
            if (a2 == null) {
                Log.d("p.com.qq.connect", "startActivity,but pluginInfo is null,pid: " + Process.myPid());
                return;
            }
            Log.d("p.com.qq.connect", "startActivity, pluginInfo is :" + a2 + " ,pid: " + Process.myPid());
            if (AstApp.i() != null) {
            }
            PluginProxyActivity.a(context2, str, i, str2, a2.getInProcess(), intent, a2.getLaunchApplication());
        }
    }

    public static PendingIntent getPluginActivityPendingIntent(String str, int i, int i2, Intent intent, int i3) {
        PluginInfo a2 = d.b().a(str, i);
        PluginLoaderInfo a3 = c.a(AstApp.i(), a2);
        ComponentName component = intent.getComponent();
        if (component != null) {
            String className = component.getClassName();
            intent.putExtra("_plugin_LaunchActivity", className);
            Class<? extends PluginProxyActivity> a4 = c.a(a2.getInProcess(), c.a(a3, className));
            if (a4 != null) {
                intent.putExtra("_plugin_PluginPakcageName", str);
                intent.putExtra("_plugin_PluginVersionCode", i);
                intent.putExtra("_plugin_InProcess", a2.getInProcess());
                intent.setClass(AstApp.i(), a4);
            }
        }
        return PendingIntent.getActivity(AstApp.i(), i2, intent, i3);
    }

    public static String getPluginSavePath(String str, int i) {
        return e.a(str, i);
    }

    public static String getPluginLibDir(String str, int i) {
        return e.a(str);
    }

    public static String getMainFrameworkPkg() {
        return AstApp.i().getPackageName();
    }

    public static int getMainVersionCode() {
        try {
            PackageInfo packageInfo = AstApp.i().getPackageManager().getPackageInfo(AstApp.i().getPackageName(), 0);
            if (packageInfo != null) {
                return packageInfo.versionCode;
            }
            return 0;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String getSMSSentReceiver() {
        return SmsSentReceiver.class.getName();
    }

    public static void userStateChangeCallback(UserStateInfo userStateInfo) {
        b.a(userStateInfo);
    }

    public static int requireInstall(String str) {
        j a2 = av.a().a(str);
        PluginInfo a3 = d.b().a(str);
        if (a2 == null) {
            if (a3 != null) {
                return 1;
            }
            av.a().b(str);
            return -1;
        } else if (a3 != null && (a3.getVersion() >= a2.d || c.a(str) >= 0)) {
            return 1;
        } else {
            av.a().c(a2);
            return 0;
        }
    }

    public static int getPcConnectState() {
        Log.d("p.com.qq.connect", "PluginHelper getPcConnectState...");
        return PluginBackToBaoReceiver.b();
    }

    public static void browserReport(int i, int i2, Map<String, String> map) {
        k.a(new STInfoV2(i, STConst.ST_DEFAULT_SLOT, i2, STConst.ST_DEFAULT_SLOT, 100), map);
    }

    public static void actionReport(int i, int i2, String str, int i3, Map<String, String> map) {
        k.a(new STInfoV2(i, str, i2, STConst.ST_DEFAULT_SLOT, i3), map);
    }

    public static Toast makeText(Context context, CharSequence charSequence, int i) {
        return Toast.makeText(AstApp.i(), charSequence, i);
    }

    public static boolean debugLog() {
        return false;
    }

    public static Toast makeText(Context context, int i, int i2) {
        return Toast.makeText(AstApp.i(), context.getString(i), i2);
    }

    private static void a(PluginInfo pluginInfo) {
        if (pluginInfo != null) {
            try {
                Class<?> loadClass = c.a(AstApp.i().getApplicationContext(), pluginInfo).loadClass(pluginInfo.getPackageName() + ".PluginDao");
                Object newInstance = loadClass.newInstance();
                ba.a().post(new a(loadClass.getDeclaredMethod("init", new Class[0]), newInstance));
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void execFreeWifiInit() {
        a(d.b().a("com.tencent.assistant.freewifi"));
    }
}
