package com.tencent.assistant.plugin.system;

import com.tencent.assistant.plugin.PluginInfo;

/* compiled from: ProGuard */
public class DockDeepAccelerationService extends BaseAppService {
    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: com.tencent.assistant.plugin.system.BaseAppService.onStartCommand(android.content.Intent, int, int):int in method: com.tencent.assistant.plugin.system.DockDeepAccelerationService.onStartCommand(android.content.Intent, int, int):int, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: com.tencent.assistant.plugin.system.BaseAppService.onStartCommand(android.content.Intent, int, int):int
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:538)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public int onStartCommand(android.content.Intent r1, int r2, int r3) {
        /*
            r1 = this;
            super.onStartCommand(r2, r3, r4)
            r0 = 2
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.plugin.system.DockDeepAccelerationService.onStartCommand(android.content.Intent, int, int):int");
    }

    public int a() {
        return 0;
    }

    public String a(PluginInfo pluginInfo) {
        if (pluginInfo != null) {
            return pluginInfo.getExtendServiceImpl(PluginInfo.META_DATA_DEEP_ACC_SERVICE);
        }
        return null;
    }
}
