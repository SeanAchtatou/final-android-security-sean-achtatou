package com.tencent.assistant.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.tencent.assistant.module.wisedownload.i;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
public class WiseDownloadService extends Service {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static i f2527a = null;

    public int onStartCommand(Intent intent, int i, int i2) {
        Log.v("WiseDownloadService", "onStartCommand conditionControler == null: " + (f2527a == null));
        TemporaryThreadManager.get().start(new d(this));
        return 1;
    }

    public void onDestroy() {
        if (f2527a != null) {
            f2527a.b();
        }
        f2527a = null;
        super.onDestroy();
    }

    public void onLowMemory() {
        if (f2527a != null) {
            f2527a.b();
        }
        f2527a = null;
        super.onLowMemory();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
