package com.tencent.assistant.service;

import android.os.IBinder;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.optimize.f;

/* compiled from: ProGuard */
class b extends f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DockRubbishRelateService f2529a;

    b(DockRubbishRelateService dockRubbishRelateService) {
        this.f2529a = dockRubbishRelateService;
    }

    public IBinder asBinder() {
        return null;
    }

    public void c() {
        XLog.d("DockRubbish", "DockRubbishRelateService onScanStarted.");
    }

    public void a(int i) {
    }

    public void b() {
        XLog.d("DockRubbish", "DockRubbishRelateService onScanFinished.");
        XLog.d("DockRubbish", "垃圾总大小：" + bt.c(this.f2529a.c));
        XLog.d("DockRubbish", "软件缓存：" + bt.c(this.f2529a.d));
        XLog.d("DockRubbish", "多余安装包：" + bt.c(this.f2529a.e));
        XLog.d("DockRubbish", "卸载残留：" + bt.c(this.f2529a.f));
        XLog.d("DockRubbish", "垃圾文件：" + bt.c(this.f2529a.g));
        this.f2529a.b();
        StringBuilder sb = new StringBuilder();
        sb.append("[status]\nresultcode=").append(0).append("\n").append("[rubbishsize]\ntotalsize=").append(this.f2529a.c).append("\n").append("1=").append(this.f2529a.d).append("\n").append("2=").append(this.f2529a.e).append("\n").append("3=").append(this.f2529a.f).append("\n").append("4=").append(this.f2529a.g);
        this.f2529a.a(sb.toString());
        this.f2529a.a(0, true);
        this.f2529a.stopSelf();
    }

    public void a() {
        XLog.d("DockRubbish", "DockRubbishRelateService onScanCanceled");
    }

    public void a(int i, DataEntity dataEntity) {
        try {
            if (dataEntity.getBoolean("rubbish.suggest")) {
                long j = dataEntity.getLong("rubbish.size");
                switch (i) {
                    case 1:
                        DockRubbishRelateService.f(this.f2529a, j);
                        break;
                    case 2:
                        DockRubbishRelateService.g(this.f2529a, j);
                        break;
                    case 3:
                        DockRubbishRelateService.h(this.f2529a, j);
                        break;
                    case 4:
                        DockRubbishRelateService.i(this.f2529a, j);
                        break;
                }
                DockRubbishRelateService.j(this.f2529a, j);
            }
        } catch (Exception e) {
            XLog.d("DockRubbish", "DockRubbishRelateService. onRubbishFound Exception.");
            e.printStackTrace();
        }
    }
}
