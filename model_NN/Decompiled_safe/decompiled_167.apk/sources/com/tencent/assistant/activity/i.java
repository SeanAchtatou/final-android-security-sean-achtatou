package com.tencent.assistant.activity;

import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.installuninstall.p;

/* compiled from: ProGuard */
class i extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMgrActivity f640a;

    i(ApkMgrActivity apkMgrActivity) {
        this.f640a = apkMgrActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.aa.a(com.tencent.assistant.localres.model.LocalApkInfo, boolean):void
     arg types: [com.tencent.assistant.localres.model.LocalApkInfo, int]
     candidates:
      com.tencent.assistant.localres.aa.a(com.tencent.assistant.localres.aa, java.util.List):void
      com.tencent.assistant.localres.aa.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, java.lang.String):void
      com.tencent.assistant.localres.aa.a(boolean, int):void
      com.tencent.assistant.localres.aa.a(java.lang.String, int):void
      com.tencent.assistant.localres.aa.a(com.tencent.assistant.localres.model.LocalApkInfo, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, boolean):boolean
     arg types: [com.tencent.assistant.download.DownloadInfo, int]
     candidates:
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, java.util.List):java.util.List
      com.tencent.assistant.download.a.a(java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>, boolean):void
      com.tencent.assistant.download.a.a(long, long):boolean
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, boolean):boolean
      com.tencent.assistant.download.a.a(java.lang.String, int):boolean
      com.tencent.assistant.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.download.SimpleDownloadInfo$UIType):void
      com.tencent.assistant.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.assistant.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.download.a.a(java.lang.String, boolean):boolean
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, boolean):boolean
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long):long
      com.tencent.assistant.activity.ApkMgrActivity.a(long, int):void
      com.tencent.assistant.activity.ApkMgrActivity.a(long, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, boolean):void
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, long, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(int, long, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, int):void
      com.tencent.assistant.activity.BaseActivity.a(com.tencent.assistantv2.st.page.STPageInfo, java.lang.String, java.lang.String):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.FragmentActivity.a(java.lang.String, boolean, boolean):android.support.v4.app.z
      android.support.v4.app.FragmentActivity.a(android.support.v4.app.Fragment, android.content.Intent, int):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.b(com.tencent.assistant.activity.ApkMgrActivity, boolean):boolean
     arg types: [com.tencent.assistant.activity.ApkMgrActivity, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.b(com.tencent.assistant.activity.ApkMgrActivity, long):long
      com.tencent.assistant.activity.ApkMgrActivity.b(com.tencent.assistant.activity.ApkMgrActivity, boolean):boolean */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 110001:
                if (message.obj instanceof LocalApkInfo) {
                    LocalApkInfo localApkInfo = (LocalApkInfo) message.obj;
                    if (!e.e(localApkInfo.mLocalFilePath)) {
                        Toast.makeText(this.f640a.t, (int) R.string.apkmgr_apk_not_exist, 0).show();
                        this.f640a.A.a(localApkInfo, false);
                        this.f640a.a(this.f640a.A.f(), false, false, 2, true);
                        return;
                    }
                    localApkInfo.mApkState = 2;
                    String ddownloadTicket = ApkResourceManager.getInstance().getDdownloadTicket(localApkInfo);
                    DownloadInfo d = DownloadProxy.a().d(ddownloadTicket);
                    if (d == null) {
                        d = new DownloadInfo();
                        d.packageName = localApkInfo != null ? localApkInfo.mPackageName : null;
                        d.versionCode = localApkInfo != null ? localApkInfo.mVersionCode : 0;
                        d.scene = STConst.ST_PAGE_APK_MANAGER;
                    }
                    if (be.a().c(localApkInfo.mPackageName)) {
                        a.a().a(d, false);
                        return;
                    } else {
                        p.a().a(ddownloadTicket, localApkInfo.mPackageName, localApkInfo.mAppName, localApkInfo.mLocalFilePath, localApkInfo.mVersionCode, localApkInfo.signature, ddownloadTicket, localApkInfo.occupySize, false, d);
                        return;
                    }
                } else {
                    return;
                }
            case 110002:
                TemporaryThreadManager.get().start(new j(this, (LocalApkInfo) message.obj));
                return;
            case 110003:
                this.f640a.w.refreshData(this.f640a.A.d(), true, false);
                return;
            case 110004:
            case 110005:
                this.f640a.A.a((LocalApkInfo) message.obj);
                return;
            case 110006:
            case 110008:
            default:
                return;
            case 110007:
                this.f640a.A.c();
                return;
            case 110009:
                boolean unused = this.f640a.M = true;
                if (message != null) {
                    long unused2 = this.f640a.P = ((Long) message.obj).longValue();
                }
                if (this.f640a.M) {
                    boolean unused3 = this.f640a.M = false;
                    this.f640a.a(this.f640a.P, true);
                    return;
                }
                return;
            case 110010:
                boolean unused4 = this.f640a.O = true;
                return;
            case 110011:
                boolean unused5 = this.f640a.N = true;
                SpaceScanManager.a().h();
                return;
        }
    }
}
