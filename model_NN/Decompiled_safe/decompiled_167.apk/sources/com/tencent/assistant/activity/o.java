package com.tencent.assistant.activity;

import android.os.Message;
import com.tencent.assistant.component.listview.ManagerGeneralController;

/* compiled from: ProGuard */
class o implements ManagerGeneralController.BussinessListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ long f646a;
    final /* synthetic */ l b;

    o(l lVar, long j) {
        this.b = lVar;
        this.f646a = j;
    }

    public void onOneItemStart(Object obj) {
    }

    public void onOneItemFinished(Object obj) {
    }

    public void onHandlerFinished() {
        Message obtainMessage = this.b.f643a.R.obtainMessage(110009);
        obtainMessage.obj = Long.valueOf(this.f646a);
        this.b.f643a.R.sendMessageDelayed(obtainMessage, 1000);
    }
}
