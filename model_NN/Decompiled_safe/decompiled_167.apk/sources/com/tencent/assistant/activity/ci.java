package com.tencent.assistant.activity;

import com.tencent.assistant.component.UserTaskView;
import com.tencent.assistant.manager.as;
import com.tencent.assistant.protocol.jce.UserTaskCfg;

/* compiled from: ProGuard */
class ci implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadActivity f450a;

    ci(DownloadActivity downloadActivity) {
        this.f450a = downloadActivity;
    }

    public void run() {
        UserTaskCfg unused = this.f450a.Z = as.w().n();
        if (this.f450a.Z != null && this.f450a.Y.booleanValue() && UserTaskView.canOpenUserTaskView(this.f450a.Z).booleanValue()) {
            this.f450a.af.sendEmptyMessage(2);
        }
    }
}
