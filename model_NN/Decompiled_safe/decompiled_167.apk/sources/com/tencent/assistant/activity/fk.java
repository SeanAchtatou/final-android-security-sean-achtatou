package com.tencent.assistant.activity;

import android.view.View;
import com.tencent.assistant.adapter.dv;
import com.tencent.assistantv2.model.ItemElement;

/* compiled from: ProGuard */
class fk implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ItemElement f580a;
    final /* synthetic */ View b;
    final /* synthetic */ int c;
    final /* synthetic */ dv d;
    final /* synthetic */ SettingActivity e;

    fk(SettingActivity settingActivity, ItemElement itemElement, View view, int i, dv dvVar) {
        this.e = settingActivity;
        this.f580a = itemElement;
        this.b = view;
        this.c = i;
        this.d = dvVar;
    }

    public void run() {
        this.e.v.a(this.f580a.d, this.b, this.c, this.d.g.getSwitchState());
    }
}
