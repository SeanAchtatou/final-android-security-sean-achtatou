package com.tencent.assistant.activity;

import com.tencent.assistant.adapter.DownloadInfoMultiAdapter;

/* compiled from: ProGuard */
class cb implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f443a;
    final /* synthetic */ DownloadActivity b;

    cb(DownloadActivity downloadActivity, int i) {
        this.b = downloadActivity;
        this.f443a = i;
    }

    public void run() {
        this.b.w.a(((long) this.f443a) == this.b.T ? DownloadInfoMultiAdapter.CreatingTaskStatusEnum.NONE : DownloadInfoMultiAdapter.CreatingTaskStatusEnum.CREATING);
        this.b.w.notifyDataSetChanged();
    }
}
