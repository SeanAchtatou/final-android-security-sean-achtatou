package com.tencent.assistant.activity.debug;

import android.view.View;
import com.tencent.assistant.db.table.r;
import com.tencent.assistant.plugin.PluginInfo;
import java.util.List;

/* compiled from: ProGuard */
class ac implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f477a;

    ac(DActivity dActivity) {
        this.f477a = dActivity;
    }

    public void onClick(View view) {
        r rVar = new r();
        List<PluginInfo> a2 = rVar.a();
        if (a2 != null && a2.size() > 0) {
            for (PluginInfo packageName : a2) {
                rVar.b(packageName.getPackageName());
            }
        }
    }
}
