package com.tencent.assistant.activity;

import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class by extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyActivity f439a;

    by(CommentReplyActivity commentReplyActivity) {
        this.f439a = commentReplyActivity;
    }

    public void onTMAClick(View view) {
        if (this.f439a.v != null && this.f439a.w != null && !TextUtils.isEmpty(this.f439a.t.getText())) {
            this.f439a.u.a(0, this.f439a.v.h, this.f439a.t.getText().toString(), this.f439a.v.c, this.f439a.v.i, this.f439a.w.f1634a, this.f439a.w.c);
            this.f439a.finish();
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 i = this.f439a.i();
        i.slotId = a.a("05", "003");
        i.actionId = 200;
        return i;
    }
}
