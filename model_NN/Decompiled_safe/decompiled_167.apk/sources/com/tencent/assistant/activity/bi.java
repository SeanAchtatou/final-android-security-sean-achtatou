package com.tencent.assistant.activity;

import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.module.callback.ae;

/* compiled from: ProGuard */
class bi extends ae {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BaseActivity f423a;

    bi(BaseActivity baseActivity) {
        this.f423a = baseActivity;
    }

    public void onCheckSelfUpdateFinish(int i, int i2, SelfUpdateManager.SelfUpdateInfo selfUpdateInfo) {
        if (SelfUpdateManager.a().j()) {
            this.f423a.j();
        } else {
            SelfUpdateManager.a().a(SelfUpdateManager.SelfUpdateType.SILENT);
        }
    }
}
