package com.tencent.assistant.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.appbakcup.BackupAppListAdapter;
import com.tencent.assistant.appbakcup.BackupApplistDialog;
import com.tencent.assistant.appbakcup.BackupDeviceAdapter;
import com.tencent.assistant.appbakcup.DeviceListDialog;
import com.tencent.assistant.appbakcup.k;
import com.tencent.assistant.appbakcup.m;
import com.tencent.assistant.appbakcup.q;
import com.tencent.assistant.appbakcup.r;
import com.tencent.assistant.component.TouchAnalizer;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.callback.ApkResCallback;
import com.tencent.assistant.login.a.b;
import com.tencent.assistant.login.d;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.BackupApp;
import com.tencent.assistant.protocol.jce.BackupDevice;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bg;
import com.tencent.assistant.utils.cv;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.t;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.activity.AssistantTabActivity;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.beacon.event.a;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: ProGuard */
public class AppBackupActivity extends BaseActivity implements View.OnClickListener, UIEventListener, NetworkMonitor.ConnectivityChangeListener {
    private static int B = 280;
    private static int[] af = {EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL, EventDispatcherEnum.UI_EVENT_BACKUP_APPLIST_SUCCESS, EventDispatcherEnum.UI_EVENT_BACKUP_APPLIST_FAIL, EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_FAIL, EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_FAIL};
    private int A = -1;
    private Dialog C;
    /* access modifiers changed from: private */
    public DeviceListDialog D;
    /* access modifiers changed from: private */
    public BackupApplistDialog E;
    private Dialog F;
    private k G;
    private int H = -1;
    private int I = -1;
    private boolean J = false;
    private int K = -1;
    private int L = -1;
    /* access modifiers changed from: private */
    public TXImageView M;
    private ImageView N;
    /* access modifiers changed from: private */
    public TextView O;
    private TextView P;
    /* access modifiers changed from: private */
    public TextView Q;
    /* access modifiers changed from: private */
    public ImageView R;
    /* access modifiers changed from: private */
    public ImageView S;
    private ImageView T;
    private ImageView U;
    /* access modifiers changed from: private */
    public Animation V;
    /* access modifiers changed from: private */
    public Animation W;
    private Animation X;
    private Animation Y;
    /* access modifiers changed from: private */
    public RelativeLayout Z;
    /* access modifiers changed from: private */
    public ImageView aa;
    /* access modifiers changed from: private */
    public ImageView ab;
    /* access modifiers changed from: private */
    public boolean ac = true;
    private boolean ad;
    /* access modifiers changed from: private */
    public Context ae;
    private int ag = 0;
    private ApkResCallback.Stub ah = new af(this);
    private Animation.AnimationListener ai = new ai(this);
    private Animation.AnimationListener aj = new aj(this);
    /* access modifiers changed from: private */
    public Animation.AnimationListener ak = new al(this);
    private boolean al = true;
    /* access modifiers changed from: private */
    public Handler am = new ae(this);
    /* access modifiers changed from: private */
    public Button n;
    private Button t;
    private SecondNavigationTitleViewV5 u;
    /* access modifiers changed from: private */
    public r v;
    /* access modifiers changed from: private */
    public q w;
    /* access modifiers changed from: private */
    public int x = -1;
    private int y = -1;
    /* access modifiers changed from: private */
    public int z = -1;

    public int f() {
        if (d.a().j()) {
            return STConst.ST_PAGE_APP_BACKUP_LOGIN;
        }
        return STConst.ST_PAGE_APP_BACKUP_NO_LOGIN;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_app_backup_layout);
        x();
        j();
        cq.a().a((NetworkMonitor.ConnectivityChangeListener) this);
        ApkResourceManager.getInstance().registerApkResCallback(this.ah);
        this.v = new r();
        this.w = new q();
        this.ag = bg.a(getIntent(), "notification_id", 0);
        this.ae = this;
        new Handler().postDelayed(new ab(this), 500);
        i();
    }

    private void i() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        XLog.d("beacon", "beacon report >> expose_appbackup. " + hashMap.toString());
        a.a("expose_appbackup", true, -1, -1, hashMap, true);
    }

    private void j() {
        for (int addUIEventListener : af) {
            AstApp.i().k().addUIEventListener(addUIEventListener, this);
        }
    }

    private void v() {
        for (int removeUIEventListener : af) {
            AstApp.i().k().removeUIEventListener(removeUIEventListener, this);
        }
    }

    private void w() {
        B = 200;
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.top_layout);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) relativeLayout.getLayoutParams();
        layoutParams.width = TouchAnalizer.CLICK_AREA;
        layoutParams.height = TouchAnalizer.CLICK_AREA;
        relativeLayout.setLayoutParams(layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) ((RelativeLayout) findViewById(R.id.cloud_mask)).getLayoutParams();
        layoutParams2.width = TouchAnalizer.CLICK_AREA;
        layoutParams2.height = TouchAnalizer.CLICK_AREA;
        relativeLayout.setLayoutParams(layoutParams2);
        RelativeLayout relativeLayout2 = (RelativeLayout) findViewById(R.id.auto_backup_layout);
        RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) relativeLayout2.getLayoutParams();
        layoutParams3.topMargin = 20;
        relativeLayout2.setLayoutParams(layoutParams3);
        Button button = (Button) findViewById(R.id.backup_button);
        RelativeLayout.LayoutParams layoutParams4 = (RelativeLayout.LayoutParams) button.getLayoutParams();
        layoutParams4.topMargin = 10;
        button.setLayoutParams(layoutParams4);
        Button button2 = (Button) findViewById(R.id.restore_button);
        RelativeLayout.LayoutParams layoutParams5 = (RelativeLayout.LayoutParams) button2.getLayoutParams();
        layoutParams5.topMargin = 20;
        button2.setLayoutParams(layoutParams5);
        View findViewById = findViewById(R.id.cloud1);
        View findViewById2 = findViewById(R.id.cloud2);
        View findViewById3 = findViewById(R.id.cloud3);
        View findViewById4 = findViewById(R.id.cloud4);
        RelativeLayout.LayoutParams layoutParams6 = (RelativeLayout.LayoutParams) findViewById.getLayoutParams();
        layoutParams6.topMargin -= 100;
        findViewById.setLayoutParams(layoutParams6);
        RelativeLayout.LayoutParams layoutParams7 = (RelativeLayout.LayoutParams) findViewById2.getLayoutParams();
        layoutParams7.topMargin -= 100;
        findViewById2.setLayoutParams(layoutParams7);
        RelativeLayout.LayoutParams layoutParams8 = (RelativeLayout.LayoutParams) findViewById3.getLayoutParams();
        layoutParams8.topMargin -= 100;
        findViewById3.setLayoutParams(layoutParams8);
        RelativeLayout.LayoutParams layoutParams9 = (RelativeLayout.LayoutParams) findViewById4.getLayoutParams();
        layoutParams9.topMargin -= 100;
        findViewById4.setLayoutParams(layoutParams9);
    }

    private void x() {
        if (df.e()) {
            w();
        }
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.u.a((Activity) this);
        this.u.b(getResources().getString(R.string.app_backup_title));
        this.u.d();
        this.u.i();
        this.u.c(new ah(this));
        this.n = (Button) findViewById(R.id.backup_button);
        this.t = (Button) findViewById(R.id.restore_button);
        this.O = (TextView) findViewById(R.id.nick_name);
        this.O.setClickable(true);
        this.O.setOnClickListener(this);
        this.Z = (RelativeLayout) findViewById(R.id.profile_icon_container);
        this.M = (TXImageView) findViewById(R.id.profile_icon);
        this.N = (ImageView) findViewById(R.id.profile_icon_no_login);
        this.aa = (ImageView) findViewById(R.id.backup_success);
        this.ab = (ImageView) findViewById(R.id.backup_failed);
        this.Q = (TextView) findViewById(R.id.checkbox);
        this.P = (TextView) findViewById(R.id.last_backup_time);
        this.R = (ImageView) findViewById(R.id.cloud1);
        this.S = (ImageView) findViewById(R.id.cloud2);
        this.T = (ImageView) findViewById(R.id.cloud3);
        this.U = (ImageView) findViewById(R.id.cloud4);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, (float) df.a(this, (float) (B + 14)), 0.0f, 0.0f);
        TranslateAnimation translateAnimation2 = new TranslateAnimation(0.0f, (float) df.a(this, (float) (B + 27)), 0.0f, 0.0f);
        TranslateAnimation translateAnimation3 = new TranslateAnimation(0.0f, (float) df.a(this, 90.0f), 0.0f, 0.0f);
        TranslateAnimation translateAnimation4 = new TranslateAnimation(0.0f, (float) df.a(this, 85.0f), 0.0f, 0.0f);
        translateAnimation.setInterpolator(this, 17432587);
        translateAnimation2.setInterpolator(this, 17432587);
        translateAnimation3.setInterpolator(this, 17432587);
        translateAnimation4.setInterpolator(this, 17432587);
        translateAnimation.setFillAfter(true);
        translateAnimation2.setFillAfter(true);
        translateAnimation3.setFillAfter(true);
        translateAnimation4.setFillAfter(true);
        translateAnimation.setDuration(27800);
        translateAnimation2.setDuration(17000);
        translateAnimation3.setDuration(4500);
        translateAnimation4.setDuration(2125);
        translateAnimation.setAnimationListener(this.ai);
        this.R.startAnimation(translateAnimation);
        this.S.startAnimation(translateAnimation2);
        this.T.startAnimation(translateAnimation3);
        this.U.startAnimation(translateAnimation4);
        this.Q.setOnClickListener(this);
        this.n.setOnClickListener(this);
        this.t.setOnClickListener(this);
        this.N.setOnClickListener(this);
        K();
        J();
    }

    /* access modifiers changed from: private */
    public void y() {
        if (this.al) {
            if (!this.ad) {
                int i = -df.a(this, 114.0f);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.rightMargin = 0;
                layoutParams.leftMargin = i;
                layoutParams.topMargin = df.a(this, 130.0f);
                layoutParams.addRule(9);
                layoutParams.addRule(10);
                this.R.setLayoutParams(layoutParams);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.rightMargin = 0;
                layoutParams2.leftMargin = i;
                layoutParams2.topMargin = df.a(this, 90.0f);
                layoutParams2.addRule(9);
                layoutParams2.addRule(10);
                this.S.setLayoutParams(layoutParams2);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams3.rightMargin = 0;
                layoutParams3.leftMargin = i;
                layoutParams3.topMargin = df.a(this, 95.0f);
                layoutParams3.addRule(9);
                layoutParams3.addRule(10);
                this.T.setLayoutParams(layoutParams3);
                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams4.rightMargin = 0;
                layoutParams4.leftMargin = i;
                layoutParams4.topMargin = df.a(this, 65.0f);
                layoutParams4.addRule(9);
                layoutParams4.addRule(10);
                this.U.setLayoutParams(layoutParams4);
                this.ad = true;
            }
            if (this.V == null) {
                XLog.d("AppBackupActivity", "startRepeatAnimation. cloudAnimation1 == null. create...");
                int a2 = df.a(this, (float) (B + 114));
                this.V = new TranslateAnimation(0.0f, (float) a2, 0.0f, 0.0f);
                this.W = new TranslateAnimation(0.0f, (float) a2, 0.0f, 0.0f);
                this.X = new TranslateAnimation(0.0f, (float) a2, 0.0f, 0.0f);
                this.Y = new TranslateAnimation(0.0f, (float) a2, 0.0f, 0.0f);
                this.V.setInterpolator(this, 17432587);
                this.W.setInterpolator(this, 17432587);
                this.X.setInterpolator(this, 17432587);
                this.Y.setInterpolator(this, 17432587);
                this.V.setDuration(34000);
                this.W.setDuration(20000);
                this.X.setDuration(18000);
                this.Y.setDuration(9000);
                this.V.setAnimationListener(this.ai);
            }
            this.T.requestLayout();
            this.U.requestLayout();
            this.T.setVisibility(0);
            this.U.setVisibility(0);
            this.T.startAnimation(this.X);
            this.U.startAnimation(this.Y);
            if (this.am.hasMessages(11902)) {
                this.am.removeMessages(11902);
            }
            this.am.sendEmptyMessageDelayed(11902, 9000);
        }
    }

    /* access modifiers changed from: private */
    public void a(View view, float f, float f2, boolean z2, Animation.AnimationListener animationListener) {
        at atVar = new at(this, f, f2, ((float) view.getWidth()) / 2.0f, ((float) view.getHeight()) / 2.0f, 0.0f, false);
        atVar.setDuration(200);
        atVar.setAnimationListener(animationListener);
        view.startAnimation(atVar);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.u != null) {
            this.u.l();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.u != null) {
            this.u.m();
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (z2 && !this.al) {
            this.al = true;
            y();
        } else if (!z2) {
            this.al = false;
            z();
        }
    }

    private void z() {
        if (this.am.hasMessages(11902)) {
            this.am.removeMessages(11902);
        }
        this.R.clearAnimation();
        this.S.clearAnimation();
        this.T.clearAnimation();
        this.U.clearAnimation();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        v();
        cq.a().b(this);
        ApkResourceManager.getInstance().unRegisterApkResCallback(this.ah);
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        A();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void A() {
        if (!this.r) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(com.tencent.assistant.b.a.G, true);
        startActivity(intent);
        this.r = false;
        finish();
        XLog.i("AppBackupActivity", "AppBackupActivity >> key back finish");
    }

    /* access modifiers changed from: private */
    public void b(int i) {
        if (this.F != null && this.F.isShowing()) {
            this.F.dismiss();
        }
        switch (i) {
            case 3:
                this.D = new DeviceListDialog(this);
                BackupDeviceAdapter backupDeviceAdapter = new BackupDeviceAdapter(this);
                this.G = new as(this, null);
                backupDeviceAdapter.a(this.G);
                backupDeviceAdapter.a(this.v.b());
                this.D.a(backupDeviceAdapter);
                this.D.setOnDismissListener(new an(this));
                if (!isFinishing()) {
                    try {
                        this.D.show();
                        return;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                } else {
                    return;
                }
            case 4:
                this.E = new BackupApplistDialog(this);
                BackupAppListAdapter backupAppListAdapter = new BackupAppListAdapter(this);
                backupAppListAdapter.a(this.w.a());
                this.E.a(new aq(this, null));
                this.E.a(backupAppListAdapter);
                this.E.a(this.w.b());
                this.E.setOnDismissListener(new ao(this));
                if (!isFinishing()) {
                    try {
                        this.E.show();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                com.tencent.assistantv2.st.k.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_APPLIST, "001", p(), STConst.ST_DEFAULT_SLOT, 100));
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profile_icon_no_login /*2131165330*/:
            case R.id.nick_name /*2131165331*/:
                if (d.a().j()) {
                    return;
                }
                if (!c.a()) {
                    B();
                    this.L = 3;
                    return;
                }
                this.L = -1;
                Bundle bundle = new Bundle();
                bundle.putInt(AppConst.KEY_LOGIN_TYPE, 2);
                bundle.putInt(AppConst.KEY_FROM_TYPE, 10);
                d.a().a(AppConst.IdentityType.MOBILEQ, bundle);
                this.K = 1;
                com.tencent.assistantv2.st.k.a(new STInfoV2(f(), "03_001", p(), STConst.ST_DEFAULT_SLOT, 200));
                return;
            case R.id.cloud_mask /*2131165332*/:
            case R.id.auto_backup_layout /*2131165333*/:
            case R.id.last_backup_time /*2131165334*/:
            default:
                return;
            case R.id.checkbox /*2131165335*/:
                m.e();
                this.Q.setSelected(m.a());
                if (!this.J && this.Q.isSelected()) {
                    this.I = C();
                }
                com.tencent.assistantv2.st.k.a(new STInfoV2(f(), "03_001", p(), STConst.ST_DEFAULT_SLOT, this.Q.isSelected() ? STConstAction.ACTION_HIT_APK_CHECK : STConstAction.ACTION_HIT_APK_UNCHECK));
                return;
            case R.id.backup_button /*2131165336*/:
                if (!c.a()) {
                    B();
                    this.L = 1;
                    return;
                }
                this.L = -1;
                if (!d.a().j()) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putInt(AppConst.KEY_LOGIN_TYPE, 2);
                    bundle2.putInt(AppConst.KEY_FROM_TYPE, 10);
                    d.a().a(AppConst.IdentityType.MOBILEQ, bundle2);
                    this.K = 2;
                } else if (this.v.d() || m.g()) {
                    H();
                } else {
                    G();
                }
                com.tencent.assistantv2.st.k.a(new STInfoV2(f(), "04_001", p(), STConst.ST_DEFAULT_SLOT, 200));
                return;
            case R.id.restore_button /*2131165337*/:
                if (!c.a()) {
                    B();
                    this.L = 2;
                    return;
                }
                this.L = -1;
                if (!d.a().j()) {
                    Bundle bundle3 = new Bundle();
                    bundle3.putInt(AppConst.KEY_LOGIN_TYPE, 2);
                    bundle3.putInt(AppConst.KEY_FROM_TYPE, 10);
                    d.a().a(AppConst.IdentityType.MOBILEQ, bundle3);
                    this.K = 3;
                } else if (D()) {
                    this.t.setEnabled(false);
                }
                com.tencent.assistantv2.st.k.a(new STInfoV2(f(), "05_001", p(), STConst.ST_DEFAULT_SLOT, 200));
                return;
        }
    }

    private void B() {
        AppConst.TwoBtnDialogInfo a2 = v.a(this);
        a2.titleRes = getResources().getString(R.string.backup_tips);
        this.F = v.b(a2);
        if (this.F != null && !isFinishing()) {
            try {
                this.F.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private int C() {
        if (d.a().j()) {
            return com.tencent.assistant.module.update.k.b().c();
        }
        return -1;
    }

    private boolean D() {
        if (!d.a().j()) {
            return false;
        }
        this.A = 2;
        this.y = this.v.a();
        return true;
    }

    /* access modifiers changed from: private */
    public void E() {
        a(this.v.e());
    }

    private void a(BackupDevice backupDevice) {
        this.z = this.w.a(backupDevice);
        this.x = 2;
    }

    private void b(boolean z2) {
        this.t.setEnabled(z2);
    }

    private void c(boolean z2) {
        if (this.A == 1) {
            I();
            if (this.v.c()) {
                if (this.v.d()) {
                    if (m.g()) {
                        if (this.K == 2 && this.v.d()) {
                            H();
                        }
                        this.K = -1;
                    } else if (!m.h()) {
                        this.z = this.w.a(this.v.e());
                        this.x = 1;
                    }
                    BackupDevice e = this.v.e();
                    if (e != null) {
                        long c = e.c();
                        m.a(c);
                        a(c);
                    }
                } else {
                    if (this.ag != 1) {
                        c(1);
                    } else {
                        b(3);
                    }
                    this.K = -1;
                }
                b(true);
                return;
            }
            if (z2) {
                b(false);
                if (this.K == 2) {
                    G();
                } else {
                    try {
                        Toast.makeText(this, getString(R.string.have_no_backup_yet), 0).show();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
            this.K = -1;
        } else if (this.v.c()) {
            if (this.v.e() == null || this.v.b().size() != 1) {
                b(3);
                b(true);
            } else {
                this.z = this.w.a(this.v.e());
                this.x = 2;
            }
            BackupDevice e3 = this.v.e();
            if (e3 != null) {
                long c2 = e3.c();
                m.a(c2);
                a(c2);
            }
        } else if (z2) {
            try {
                b(false);
                Toast.makeText(this, getString(R.string.have_no_backup_yet), 0).show();
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        } else {
            Toast.makeText(this, getString(R.string.get_backup_device_list_failed), 0).show();
            b(true);
        }
    }

    private void c(int i) {
        if (this.F != null && this.F.isShowing()) {
            this.F.dismiss();
        }
        ap apVar = new ap(this, i, this.K);
        apVar.blockCaller = true;
        apVar.titleRes = getString(R.string.restore_backup_apps);
        apVar.rBtnTxtRes = getString(R.string.restore_backup_apps);
        if (this.K == 2) {
            apVar.lBtnTxtRes = getString(R.string.continue_backup);
        } else {
            apVar.lBtnTxtRes = getString(R.string.cancel);
        }
        if (i == 1) {
            apVar.contentRes = getString(R.string.switch_device_restore_tips);
        } else if (i == 2) {
            apVar.contentRes = getString(R.string.refresh_device_restore_tips);
        }
        v.a(apVar);
        if (i == 1) {
            com.tencent.assistantv2.st.k.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_SWITCHDEVICE, "001", 0, STConst.ST_DEFAULT_SLOT, 100));
        } else if (i == 2) {
            com.tencent.assistantv2.st.k.a(new STInfoV2(STConst.ST_PAGE_APP_BACKUP_REFRESHDEVICE, "001", 0, STConst.ST_DEFAULT_SLOT, 100));
        }
    }

    private void d(int i) {
        if (this.F != null && this.F.isShowing()) {
            this.F.dismiss();
        }
        ac acVar = new ac(this);
        this.n.setEnabled(true);
        this.n.setText(getResources().getString(R.string.app_backup_list));
        acVar.blockCaller = true;
        acVar.lBtnTxtRes = getString(R.string.do_not_open);
        acVar.rBtnTxtRes = getString(R.string.open_auto_backup);
        acVar.titleRes = getString(R.string.backup_success);
        acVar.contentRes = String.format(getString(R.string.auto_backup_message), t.u(), Integer.valueOf(i));
        v.a(acVar);
    }

    /* access modifiers changed from: private */
    public void F() {
        AppConst.LoadingDialogInfo loadingDialogInfo = new AppConst.LoadingDialogInfo();
        loadingDialogInfo.loadingText = "加载中";
        loadingDialogInfo.blockCaller = true;
        this.C = v.a(loadingDialogInfo);
        if (this.C != null) {
            this.C.setCancelable(true);
        }
    }

    /* access modifiers changed from: private */
    public void G() {
        this.H = C();
        if (this.H != -1) {
            this.n.setEnabled(false);
            this.n.setText(getString(R.string.backuping));
        }
    }

    /* access modifiers changed from: private */
    public void H() {
        if (this.F != null && this.F.isShowing()) {
            this.F.dismiss();
        }
        ad adVar = new ad(this);
        adVar.blockCaller = true;
        adVar.rBtnTxtRes = getString(R.string.continue_backup);
        adVar.lBtnTxtRes = getString(R.string.cancel);
        adVar.titleRes = getString(R.string.app_backup_list);
        adVar.contentRes = getString(R.string.continue_backup_content);
        v.a(adVar);
    }

    private void d(boolean z2) {
        I();
        ArrayList<BackupApp> a2 = this.w.a();
        if (this.x == 1) {
            if (a2 == null || a2.size() < 10) {
                if (this.K == 2 && this.v.d()) {
                    H();
                }
            } else if (this.ag != 2) {
                c(2);
            } else {
                b(4);
            }
            this.K = -1;
        } else if (z2) {
            try {
                if (this.w.c()) {
                    if (this.D != null && this.D.isShowing()) {
                        this.D.a(0);
                    }
                    Toast.makeText(this, "备份的应用本地已全部安装", 0).show();
                } else {
                    if (this.D != null && this.D.isShowing()) {
                        this.D.a(1);
                    }
                    b(4);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (this.D != null && this.D.isShowing()) {
                this.D.a(-1);
            }
            Toast.makeText(this, getString(R.string.get_backup_applist_failed), 0).show();
        }
        b(true);
    }

    private void I() {
        if (this.C != null && this.C.isShowing()) {
            this.C.dismiss();
        }
    }

    private void J() {
        if (d.a().j()) {
            this.Q.setVisibility(0);
            this.Q.setSelected(m.a());
        } else {
            this.Q.setVisibility(4);
        }
        long j = 0;
        if (d.a().j()) {
            j = m.i();
        }
        a(j);
    }

    private void a(long j) {
        if (j == 0) {
            this.P.setVisibility(4);
            return;
        }
        int f = cv.f(j);
        if (f != 0) {
            String valueOf = String.valueOf(f);
            SpannableString spannableString = new SpannableString(getString(R.string.last_backup_time, new Object[]{valueOf}));
            if (f >= 20) {
                spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.app_backup_day_text_color)), 0, valueOf.length(), 33);
            }
            this.P.setVisibility(0);
            this.P.setText(spannableString);
            return;
        }
        this.P.setVisibility(0);
        this.P.setText(getString(R.string.just_backuped));
    }

    /* access modifiers changed from: private */
    public void K() {
        b f = com.tencent.assistant.login.a.a.f();
        if (this.M != null) {
            if (d.a().j()) {
                this.M.updateImageView(f.f1456a, R.drawable.common_owner_icon_01, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                this.M.setVisibility(0);
                this.N.setVisibility(8);
            } else {
                this.M.setVisibility(8);
                this.N.setVisibility(0);
            }
        }
        if (this.O == null) {
            return;
        }
        if (d.a().j()) {
            this.O.setText(f.b);
        } else {
            this.O.setText(getString(R.string.hint_login));
        }
    }

    /* access modifiers changed from: private */
    public void L() {
        this.A = 1;
        this.y = this.v.a();
        F();
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS /*1077*/:
                K();
                return;
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL /*1078*/:
                K();
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS /*1081*/:
                XLog.d("AppBackupActivity", "LOGIN_SUCCESS");
                if (this.K == 3) {
                    if (D()) {
                        this.t.setEnabled(false);
                    }
                    this.K = -1;
                } else if (this.K == 2 || this.K == 1) {
                    L();
                }
                J();
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL /*1082*/:
                XLog.d("AppBackupActivity", "LOGIN_FAIL");
                J();
                this.K = -1;
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL /*1083*/:
                this.K = -1;
                return;
            case EventDispatcherEnum.UI_EVENT_BACKUP_APPLIST_SUCCESS /*1122*/:
                int i = message.arg1;
                int i2 = message.arg2;
                if (i == this.H) {
                    this.ac = true;
                    this.J = true;
                    if (m.g() || m.a()) {
                        this.M.setVisibility(0);
                        this.aa.setVisibility(8);
                        this.ab.setVisibility(8);
                        a(this.Z, 0.0f, 90.0f, false, this.aj);
                    } else {
                        d(i2);
                    }
                    this.H = -1;
                } else if (i == this.I) {
                    this.J = true;
                    this.I = -1;
                }
                m.f();
                b(true);
                return;
            case EventDispatcherEnum.UI_EVENT_BACKUP_APPLIST_FAIL /*1123*/:
                int i3 = message.arg1;
                if (i3 == this.H) {
                    this.ac = false;
                    this.M.setVisibility(0);
                    this.aa.setVisibility(8);
                    this.ab.setVisibility(8);
                    a(this.Z, 0.0f, 90.0f, false, this.aj);
                    this.H = -1;
                    com.tencent.assistant.login.a.a.c(message.arg2);
                    return;
                } else if (i3 == this.I) {
                    this.I = -1;
                    return;
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_SUCCESS /*1124*/:
                Log.d("AppBackupActivity", "UI_EVENT_GET_BACKUP_DEVICELIST_SUCCESS msg.arg1 = " + message.arg1 + " deviceRequestId = " + this.y);
                if (message.arg1 == this.y) {
                    c(true);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_DEVICELIST_FAIL /*1125*/:
                Log.d("AppBackupActivity", "UI_EVENT_GET_BACKUP_DEVICELIST_FAIL msg.arg1 = " + message.arg1 + " deviceRequestId = " + this.y);
                if (message.arg1 == this.y) {
                    c(false);
                    if (this.A == 2) {
                        com.tencent.assistant.login.a.a.b(message.arg2);
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_SUCCESS /*1126*/:
                if (message.arg1 == this.z) {
                    I();
                    d(true);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_GET_BACKUP_APPS_FAIL /*1127*/:
                if (message.arg1 == this.z) {
                    I();
                    d(false);
                    if (this.x == 2) {
                        com.tencent.assistant.login.a.a.b(message.arg2);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onConnected(APN apn) {
        if (this.L == 1) {
            this.L = -1;
            onClick(this.n);
        } else if (this.L == 2) {
            this.L = -1;
            onClick(this.t);
        } else if (this.L == 3) {
            this.L = -1;
            onClick(this.N);
        }
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }
}
