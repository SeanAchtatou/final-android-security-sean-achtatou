package com.tencent.assistant.activity;

import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class gr extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartPopWindowActivity f614a;

    gr(StartPopWindowActivity startPopWindowActivity) {
        this.f614a = startPopWindowActivity;
    }

    public void onTMAClick(View view) {
        this.f614a.D();
        this.f614a.i();
        Toast.makeText(this.f614a.J, (int) R.string.pop_title_download_wifi_toast, 4).show();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f614a.J, 200);
        buildSTInfo.slotId = a.a("04", "003");
        if (this.f614a.I != null && this.f614a.E.a() == this.f614a.I.size()) {
            buildSTInfo.actionId = 200;
            buildSTInfo.status = "02";
        }
        return buildSTInfo;
    }
}
