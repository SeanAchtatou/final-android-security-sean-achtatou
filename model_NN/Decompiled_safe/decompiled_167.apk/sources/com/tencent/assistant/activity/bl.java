package com.tencent.assistant.activity;

import android.os.Message;
import com.tencent.assistant.Global;
import com.tencent.assistant.model.spaceclean.SubRubbishInfo;
import com.tencent.assistant.model.spaceclean.a;
import com.tencent.assistant.module.callback.al;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/* compiled from: ProGuard */
class bl extends al {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BigFileCleanActivity f426a;

    bl(BigFileCleanActivity bigFileCleanActivity) {
        this.f426a = bigFileCleanActivity;
    }

    public void a(long j, SubRubbishInfo subRubbishInfo) {
        XLog.d("miles", "BigFileCleanActivity >> onRubbishFound. " + subRubbishInfo.b);
        BigFileCleanActivity.a(this.f426a);
        if (this.f426a.D % 1 == 0) {
            int unused = this.f426a.D = 0;
            if (this.f426a.O.hasMessages(24)) {
                this.f426a.O.removeMessages(24);
            }
            Message obtain = Message.obtain(this.f426a.O, 24);
            obtain.obj = Long.valueOf(j);
            obtain.sendToTarget();
        }
    }

    public void a(ArrayList<SubRubbishInfo> arrayList) {
        XLog.d("miles", "BigFileCleanActivity >> onScanFinished.");
        a aVar = null;
        a aVar2 = null;
        a aVar3 = null;
        a aVar4 = null;
        a aVar5 = null;
        Iterator<SubRubbishInfo> it = arrayList.iterator();
        while (it.hasNext()) {
            SubRubbishInfo next = it.next();
            next.d = false;
            switch (bn.f428a[next.f1673a.ordinal()]) {
                case 1:
                    if (aVar == null) {
                        aVar = new a();
                        aVar.b = "视频";
                        aVar.h = 0;
                        this.f426a.C.add(aVar);
                    }
                    aVar.d += next.c;
                    aVar.a(next);
                    break;
                case 2:
                    if (aVar2 == null) {
                        aVar2 = new a();
                        aVar2.b = "音乐";
                        aVar2.h = 1;
                        this.f426a.C.add(aVar2);
                    }
                    aVar2.d += next.c;
                    aVar2.a(next);
                    break;
                case 3:
                    if (aVar3 == null) {
                        aVar3 = new a();
                        aVar3.b = "文档";
                        aVar3.h = 2;
                        this.f426a.C.add(aVar3);
                    }
                    aVar3.d += next.c;
                    aVar3.a(next);
                    break;
                case 4:
                    if (aVar4 == null) {
                        aVar4 = new a();
                        aVar4.b = "压缩文件";
                        aVar4.h = 3;
                        this.f426a.C.add(aVar4);
                    }
                    aVar4.d += next.c;
                    aVar4.a(next);
                    break;
                default:
                    if (aVar5 == null) {
                        aVar5 = new a();
                        aVar5.b = "其他文件";
                        aVar5.h = 4;
                        this.f426a.C.add(aVar5);
                    }
                    aVar5.d += next.c;
                    aVar5.a(next);
                    break;
            }
            a aVar6 = aVar5;
            a aVar7 = aVar4;
            aVar = aVar;
            aVar2 = aVar2;
            aVar3 = aVar3;
            aVar4 = aVar7;
            aVar5 = aVar6;
        }
        long j = 0;
        Iterator it2 = this.f426a.C.iterator();
        while (true) {
            long j2 = j;
            if (it2.hasNext()) {
                j = j2 + ((a) it2.next()).d;
            } else {
                if (this.f426a.O.hasMessages(25)) {
                    this.f426a.O.removeMessages(25);
                }
                Message obtain = Message.obtain(this.f426a.O, 25);
                obtain.obj = Long.valueOf(j2);
                obtain.sendToTarget();
                long unused = this.f426a.K = System.currentTimeMillis();
                HashMap hashMap = new HashMap();
                hashMap.put("B1", ((j2 / 1024) / 1024) + Constants.STR_EMPTY);
                hashMap.put("B2", j2 + Constants.STR_EMPTY);
                hashMap.put("B3", Global.getPhoneGuidAndGen());
                hashMap.put("B4", Global.getQUAForBeacon());
                hashMap.put("B5", t.g());
                com.tencent.beacon.event.a.a("BigFileScan", true, this.f426a.K - this.f426a.J, -1, hashMap, true);
                XLog.d("beacon", "beacon report >> event: BigFileScan, totalTime : " + (this.f426a.K - this.f426a.J) + ", params : " + hashMap.toString());
                return;
            }
        }
    }

    public void a(boolean z) {
        Message.obtain(this.f426a.O, 27).sendToTarget();
        long unused = this.f426a.M = System.currentTimeMillis();
        HashMap hashMap = new HashMap();
        hashMap.put("B1", ((this.f426a.E / 1024) / 1024) + Constants.STR_EMPTY);
        hashMap.put("B2", this.f426a.E + Constants.STR_EMPTY);
        hashMap.put("B3", Global.getPhoneGuidAndGen());
        hashMap.put("B4", Global.getQUAForBeacon());
        hashMap.put("B5", t.g());
        com.tencent.beacon.event.a.a("BigFileClean", true, this.f426a.M - this.f426a.L, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: BigFileClean, totalTime : " + (this.f426a.M - this.f426a.L) + ", params : " + hashMap.toString());
    }
}
