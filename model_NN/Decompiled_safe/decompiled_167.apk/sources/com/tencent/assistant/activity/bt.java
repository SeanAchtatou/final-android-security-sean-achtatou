package com.tencent.assistant.activity;

import android.app.ActivityManager;
import android.content.pm.PackageManager;
import android.os.Message;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.utils.XLog;
import java.util.List;

/* compiled from: ProGuard */
class bt implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BigFileCleanActivity f434a;

    bt(BigFileCleanActivity bigFileCleanActivity) {
        this.f434a = bigFileCleanActivity;
    }

    public void run() {
        boolean z = false;
        PackageManager packageManager = this.f434a.getPackageManager();
        XLog.d("miles", "loadData---run");
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) this.f434a.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            int i = 0;
            while (true) {
                if (i >= runningAppProcesses.size()) {
                    break;
                }
                String str = runningAppProcesses.get(i).pkgList[0];
                if (!AppConst.TENCENT_MOBILE_MANAGER_PKGNAME.equals(str) && !this.f434a.n.getPackageName().equals(str) && this.f434a.a(packageManager, str)) {
                    z = true;
                    break;
                }
                i++;
            }
        }
        boolean unused = this.f434a.F = z;
        boolean unused2 = this.f434a.G = true;
        Message.obtain(this.f434a.O, 29).sendToTarget();
    }
}
