package com.tencent.assistant.activity;

import com.tencent.assistant.component.PCListLinearLayout;
import com.tencent.assistant.model.OnlinePCListItemModel;
import com.tencent.connector.ipc.a;

/* compiled from: ProGuard */
class et implements PCListLinearLayout.PCListClickCallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PhotoBackupNewActivity f562a;

    et(PhotoBackupNewActivity photoBackupNewActivity) {
        this.f562a = photoBackupNewActivity;
    }

    public void call(OnlinePCListItemModel onlinePCListItemModel) {
        this.f562a.d(1);
        if (onlinePCListItemModel == null) {
            this.f562a.v();
            return;
        }
        String g = a.a().g();
        if (g != null && g.equals(onlinePCListItemModel.f1629a)) {
            return;
        }
        if (!a.a().d()) {
            this.f562a.b(onlinePCListItemModel.b, onlinePCListItemModel.f1629a);
            a.a().a(onlinePCListItemModel.f1629a);
            return;
        }
        this.f562a.c(5);
    }
}
