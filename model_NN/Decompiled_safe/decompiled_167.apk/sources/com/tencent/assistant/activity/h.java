package com.tencent.assistant.activity;

import android.content.DialogInterface;
import android.graphics.Color;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

/* compiled from: ProGuard */
class h extends ClickableSpan implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AboutDeclareActivity f623a;
    private final View.OnClickListener b;

    public h(AboutDeclareActivity aboutDeclareActivity, View.OnClickListener onClickListener) {
        this.f623a = aboutDeclareActivity;
        this.b = onClickListener;
    }

    public void onClick(View view) {
        this.b.onClick(view);
    }

    public void updateDrawState(TextPaint textPaint) {
        textPaint.setColor(Color.argb(255, 0, 163, 239));
        textPaint.setUnderlineText(false);
    }

    public void onClick(DialogInterface dialogInterface, int i) {
    }
}
