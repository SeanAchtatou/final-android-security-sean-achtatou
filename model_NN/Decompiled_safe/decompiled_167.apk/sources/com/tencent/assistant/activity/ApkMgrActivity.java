package com.tencent.assistant.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.view.KeyEvent;
import android.view.ViewStub;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.b.a;
import com.tencent.assistant.component.FooterView;
import com.tencent.assistant.component.NormalErrorPage;
import com.tencent.assistant.component.listview.ApkResultListView;
import com.tencent.assistant.component.listview.ManagerGeneralController;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.aa;
import com.tencent.assistant.localres.callback.b;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.model.PluginStartEntry;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.activity.AssistantTabActivity;
import com.tencent.assistantv2.component.ScaningProgressView;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.component.TxManagerCommContainView;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.tmsecurelite.optimize.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class ApkMgrActivity extends BaseActivity implements UIEventListener {
    /* access modifiers changed from: private */
    public aa A = null;
    private boolean B = false;
    /* access modifiers changed from: private */
    public boolean C = false;
    private ViewStub D = null;
    /* access modifiers changed from: private */
    public volatile boolean E = false;
    /* access modifiers changed from: private */
    public boolean F = false;
    /* access modifiers changed from: private */
    public boolean G = false;
    /* access modifiers changed from: private */
    public long H = 0;
    /* access modifiers changed from: private */
    public long I = 0;
    private PluginStartEntry J = null;
    /* access modifiers changed from: private */
    public boolean K = false;
    /* access modifiers changed from: private */
    public boolean L = false;
    /* access modifiers changed from: private */
    public boolean M = false;
    /* access modifiers changed from: private */
    public boolean N = false;
    /* access modifiers changed from: private */
    public boolean O = false;
    /* access modifiers changed from: private */
    public long P = 0;
    /* access modifiers changed from: private */
    public ManagerGeneralController Q;
    /* access modifiers changed from: private */
    public Handler R = new i(this);
    private boolean S = false;
    private ManagerGeneralController.BussinessInterface T = new v(this);
    private b U = new l(this);
    /* access modifiers changed from: private */
    public f V = new q(this);
    ExpandableListView.OnChildClickListener n = new p(this);
    /* access modifiers changed from: private */
    public Context t;
    private SecondNavigationTitleViewV5 u;
    private NormalErrorPage v;
    /* access modifiers changed from: private */
    public ApkResultListView w = null;
    /* access modifiers changed from: private */
    public FooterView x = null;
    /* access modifiers changed from: private */
    public TxManagerCommContainView y;
    /* access modifiers changed from: private */
    public ScaningProgressView z;

    public int f() {
        return STConst.ST_PAGE_APK_MANAGER;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            setContentView((int) R.layout.activity_apk_manager_new);
            this.t = this;
            w();
            i();
            A();
            j();
        } catch (Throwable th) {
            this.S = true;
            cq.a().b();
            finish();
        }
    }

    private void i() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.B = extras.getBoolean(a.O, false);
        }
    }

    private void j() {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", Global.getPhoneGuidAndGen());
        hashMap.put("B2", Global.getQUAForBeacon());
        hashMap.put("B3", t.g());
        XLog.d("beacon", "beacon report >> expose_apkmgr. " + hashMap.toString());
        com.tencent.beacon.event.a.a("expose_apkmgr", true, -1, -1, hashMap, true);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.S) {
            super.onResume();
            return;
        }
        if (this.u != null) {
            this.u.l();
        }
        super.onResume();
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(1027, this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.S) {
            if (this.u != null) {
                this.u.m();
            }
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
            AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
            AstApp.i().k().removeUIEventListener(1027, this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.S) {
            super.onDestroy();
            return;
        }
        this.A.b(this.U);
        com.tencent.assistant.utils.b.a();
        if (this.B && this.C) {
            XLog.d("ApkMgrActivity", "set has run clean to true");
            m.a().b("key_space_clean_has_run_clean", (Object) true);
        }
        super.onDestroy();
    }

    public void onBackPressed() {
        super.onBackPressed();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return super.onKeyDown(i, keyEvent);
        }
        v();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void v() {
        if (!this.r) {
            finish();
            return;
        }
        Intent intent = new Intent(this, AssistantTabActivity.class);
        intent.setFlags(67108864);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
        intent.putExtra(a.G, true);
        startActivity(intent);
        this.r = false;
        finish();
        XLog.i("ApkMgrActivity", "ApkMgrActivity >> key back finish");
    }

    private void w() {
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.u.a(this);
        this.u.b(getString(R.string.apkmgr_title));
        this.u.d();
        this.u.c(new r(this));
        B();
        y();
        this.D = (ViewStub) findViewById(R.id.error_stub);
    }

    /* access modifiers changed from: private */
    public void x() {
        this.Q.deleteAllSelectItem(new s(this));
    }

    private void y() {
        this.x = new FooterView(this.t);
        this.y.a(this.x, new RelativeLayout.LayoutParams(-1, -2));
        this.x.updateContent(getString(R.string.apkmgr_delete));
        this.x.setFooterViewEnable(false);
        this.x.setTag(R.id.tma_st_slot_tag, "05_001");
        this.x.setOnFooterViewClickListener(new t(this));
    }

    /* access modifiers changed from: private */
    public void z() {
        TemporaryThreadManager.get().start(new u(this));
    }

    private void A() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.J = (PluginStartEntry) extras.getSerializable("dock_plugin");
        }
        this.A = ApkResourceManager.getInstance().getLocalApkLoader();
        this.A.a(this.U);
        this.A.c();
        this.A.a();
        this.H = System.currentTimeMillis();
    }

    private void B() {
        this.y = (TxManagerCommContainView) findViewById(R.id.contentView);
        this.z = new ScaningProgressView(this.t);
        this.w = new ApkResultListView(this.t);
        this.w.setApkChildListenerToAdapter(this.n);
        this.w.setApkHandleToAdapter(this.R);
        this.y.a(this.z);
        this.y.b(this.w);
        this.Q = new ManagerGeneralController(this.w.getAdapter(), this.T, this.w.getAnimationListView());
    }

    /* access modifiers changed from: private */
    public void a(long j, boolean z2) {
        if (z2) {
            String c = bt.c(j);
            String.format(getString(R.string.space_clean_apk_finish_size), c);
            b(c);
            return;
        }
        C();
    }

    private void C() {
        if (this.v == null) {
            this.D.inflate();
            this.v = (NormalErrorPage) findViewById(R.id.error);
        }
        this.v.setErrorType(1);
        this.v.setErrorHint(getResources().getString(R.string.no_apk));
        this.v.setErrorImage(R.drawable.emptypage_pic_01);
        this.v.setErrorHintTextColor(getResources().getColor(R.color.common_listiteminfo));
        this.v.setErrorHintTextSize(getResources().getDimension(R.dimen.appadmin_empty_page_text_size));
        this.v.setErrorTextVisibility(8);
        this.v.setErrorHintVisibility(0);
        this.v.setFreshButtonVisibility(8);
        this.y.setVisibility(8);
        this.x.setVisibility(8);
        this.v.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void a(int i, long j, boolean z2) {
        String c = bt.c(j);
        String string = getString(R.string.apkmgr_delete);
        if (i > 0) {
            this.x.setFooterViewEnable(true);
            if (z2) {
                string = getString(R.string.apkmgr_delete_auto);
            }
            this.x.updateContent(string, " " + String.format(getString(R.string.apkmgr_delete_format), Integer.valueOf(i), c));
            return;
        }
        this.x.setFooterViewEnable(false);
        this.x.updateContent(string);
    }

    /* access modifiers changed from: private */
    public void a(long j, int i, boolean z2, int i2) {
        if (z2) {
            XLog.d("ApkMgrActivity", "ApkMgrActivity--updateHeaderView---scanning ing");
            this.z.a(j);
            return;
        }
        XLog.d("ApkMgrActivity", "ApkMgrActivity--updateHeaderView---scanning end");
        if (i2 == 1 && !this.F) {
            this.z.b(j);
            this.z.c();
            this.R.post(new w(this, i2));
        } else if (i2 == 2) {
            this.z.a(j);
            this.R.postDelayed(new z(this), 1000);
        } else {
            this.z.b(j);
            this.R.post(new aa(this));
        }
    }

    private void b(String str) {
        XLog.i("ApkMgrActivity", "showClearAllPage");
        this.G = true;
        String string = getString(R.string.apkmgr_clear_success);
        int length = 5 + str.length();
        try {
            this.y.a(string, new SpannableString(String.format(getString(R.string.apkmgr_clear_size_tip), str)));
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
            buildSTInfo.scene = STConst.ST_PAGE_APK_MANAGER_EMPTY;
            k.a(buildSTInfo);
            if (this.K) {
                this.y.a(R.drawable.icon_apk_manager_xiaobao, getString(R.string.apkmgr_skip_to_space_clean_tip), 30, 30, 3);
                this.y.a(this, SpaceCleanActivity.class, this.J);
                buildSTInfo.slotId = "03_001";
                k.a(buildSTInfo);
            } else if (this.L) {
                buildSTInfo.slotId = "04_001";
                k.a(buildSTInfo);
                this.y.a(R.drawable.icon_apk_manager_xiaobao, getString(R.string.apkmgr_skip_to_installapp_manager_tip), 11, 11, 3);
                this.y.a(this, InstalledAppManagerActivity.class, (PluginStartEntry) null);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void D() {
        TemporaryThreadManager.get().start(new k(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.ApkMgrActivity.a(long, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, long):long
      com.tencent.assistant.activity.ApkMgrActivity.a(long, int):void
      com.tencent.assistant.activity.ApkMgrActivity.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean):void
      com.tencent.assistant.activity.ApkMgrActivity.a(com.tencent.assistant.activity.ApkMgrActivity, boolean):boolean
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.ApkMgrActivity.a(long, boolean):void */
    private void a(List<LocalApkInfo> list, boolean z2) {
        Map<Integer, ArrayList<LocalApkInfo>> d = this.A.d();
        if (d == null || d.isEmpty()) {
            a(0L, false);
            return;
        }
        this.w.refreshData(d, true, z2);
        if (this.v != null) {
            this.v.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void a(List<LocalApkInfo> list, boolean z2, boolean z3, int i, boolean z4) {
        if (list != null && !list.isEmpty()) {
            this.y.setVisibility(0);
            this.x.setVisibility(0);
            if (this.v != null) {
                this.v.setVisibility(4);
            }
            long j = 0;
            long j2 = 0;
            int i2 = 0;
            int i3 = 0;
            while (i3 < list.size()) {
                LocalApkInfo localApkInfo = list.get(i3);
                if (localApkInfo != null) {
                    if (localApkInfo.mIsSelect) {
                        i2++;
                        j2 += localApkInfo.occupySize;
                    }
                    j += localApkInfo.occupySize;
                }
                i3++;
                i2 = i2;
                j2 = j2;
            }
            XLog.d("ApkMgrActivity", "totalSize = " + j);
            a(j, list.size(), z2, i);
            a(i2, j2, z3);
            if (z4 && !z2) {
                a(list, i == 1 || i == 3);
            }
        }
    }

    public void handleUIEvent(Message message) {
        InstallUninstallTaskBean installUninstallTaskBean;
        if ((message.obj instanceof InstallUninstallTaskBean) && (installUninstallTaskBean = (InstallUninstallTaskBean) message.obj) != null) {
            Map<Integer, ArrayList<LocalApkInfo>> d = this.A.d();
            ArrayList arrayList = d.get(2);
            ArrayList arrayList2 = d.get(1);
            ArrayList arrayList3 = new ArrayList();
            if (arrayList != null) {
                arrayList3.addAll(arrayList);
            }
            if (arrayList2 != null) {
                arrayList3.addAll(arrayList2);
            }
            if (arrayList3.size() != 0) {
                Iterator it = arrayList3.iterator();
                while (it.hasNext()) {
                    LocalApkInfo localApkInfo = (LocalApkInfo) it.next();
                    if (localApkInfo.mPackageName.equals(installUninstallTaskBean.packageName)) {
                        switch (message.what) {
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START /*1025*/:
                                localApkInfo.mApkState = 3;
                                continue;
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC /*1026*/:
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL /*1030*/:
                                localApkInfo.mApkState = 1;
                                continue;
                            case 1027:
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START /*1028*/:
                            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC /*1029*/:
                            default:
                                localApkInfo.mApkState = 1;
                                continue;
                        }
                    }
                }
                this.w.refreshData(d, true, true);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(long j, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put("B1", String.valueOf(i));
        hashMap.put("B2", Global.getPhoneGuidAndGen());
        hashMap.put("B3", Global.getQUAForBeacon());
        hashMap.put("B4", t.g());
        com.tencent.beacon.event.a.a("ApkScan", true, j, -1, hashMap, true);
        XLog.d("beacon", "beacon report >> event: ApkScan, totalTime : " + j + ", params : " + hashMap.toString());
    }
}
