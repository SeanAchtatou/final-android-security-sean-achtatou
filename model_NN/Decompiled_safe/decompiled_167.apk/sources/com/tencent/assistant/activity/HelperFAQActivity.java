package com.tencent.assistant.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.FAQFootView;
import com.tencent.assistant.component.WebViewFooter;
import com.tencent.assistant.module.callback.o;
import com.tencent.assistant.module.cz;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;

/* compiled from: ProGuard */
public class HelperFAQActivity extends BrowserActivity {
    private Handler A = new Cdo(this);
    private o B = new dp(this);
    /* access modifiers changed from: private */
    public Context y;
    /* access modifiers changed from: private */
    public FAQFootView z;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.y = this;
        D();
        cz.a().register(this.B);
        this.A.sendEmptyMessageDelayed(0, 50);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        cz.a().unregister(this.B);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(getString(R.string.help_title));
    }

    public void j() {
        this.v = (WebViewFooter) findViewById(R.id.browser_footer_view);
        this.v.setVisibility(8);
    }

    public void c(boolean z2) {
    }

    private void D() {
        this.z = new FAQFootView(this);
        ((RelativeLayout) findViewById(R.id.browser_footer_layout)).addView(this.z, new RelativeLayout.LayoutParams(-1, -2));
        this.z.setOnClickListener(new dq(this));
        SecondNavigationTitleViewV5 secondNavigationTitleViewV5 = (SecondNavigationTitleViewV5) findViewById(R.id.browser_header_view);
        secondNavigationTitleViewV5.d();
        secondNavigationTitleViewV5.j();
    }
}
