package com.tencent.assistant.activity;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class ba extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppTreasureBoxActivity f416a;

    ba(AppTreasureBoxActivity appTreasureBoxActivity) {
        this.f416a = appTreasureBoxActivity;
    }

    public void onLeftBtnClick() {
        if (this.f416a.x != null) {
            this.f416a.x.setVisibility(0);
        }
        if (!this.f416a.isFinishing()) {
            this.f416a.z.show();
        }
        STInfoV2 a2 = this.f416a.a((int) STConst.ST_PAGE_APP_TREASURE_BOX_EXIT_DIALOG, 200);
        a2.slotId = a.a("03", "002");
        k.a(a2);
    }

    public void onRightBtnClick() {
        this.f416a.j();
        STInfoV2 a2 = this.f416a.a((int) STConst.ST_PAGE_APP_TREASURE_BOX_EXIT_DIALOG, 200);
        a2.slotId = a.a("03", "001");
        k.a(a2);
    }

    public void onCancell() {
        if (this.f416a.x != null) {
            this.f416a.x.setVisibility(0);
        }
        if (!this.f416a.isFinishing()) {
            this.f416a.z.show();
        }
        STInfoV2 a2 = this.f416a.a((int) STConst.ST_PAGE_APP_TREASURE_BOX_EXIT_DIALOG, 200);
        a2.slotId = a.a("03", "002");
        k.a(a2);
    }
}
