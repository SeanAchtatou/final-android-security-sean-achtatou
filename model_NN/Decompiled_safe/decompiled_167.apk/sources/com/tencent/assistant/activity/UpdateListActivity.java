package com.tencent.assistant.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.b.a;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.UpdateListFooterView;
import com.tencent.assistant.component.UpdateListView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.ct;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.wisedownload.e;
import com.tencent.assistant.module.wisedownload.r;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.protocol.jce.PicInfo;
import com.tencent.assistant.protocol.jce.StatUpdateManageAction;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class UpdateListActivity extends BaseActivity implements UIEventListener {
    private String A = null;
    private ct B = new ct();
    private hi C = new hi(this, null);
    /* access modifiers changed from: private */
    public int D = -1;
    /* access modifiers changed from: private */
    public LinearLayout E;
    private TXImageView F;
    private TextView G;
    private STInfoV2 H;
    /* access modifiers changed from: private */
    public boolean I = false;
    private boolean J = false;
    private View.OnClickListener K = new he(this);
    /* access modifiers changed from: private */
    public Context n = this;
    private AstApp t;
    private SecondNavigationTitleViewV5 u;
    private RelativeLayout v;
    /* access modifiers changed from: private */
    public UpdateListView w = null;
    private NormalErrorRecommendPage x;
    private UpdateListFooterView y;
    private StatUpdateManageAction z = null;

    public int f() {
        return STConst.ST_PAGE_UPDATE;
    }

    public boolean g() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_updatelist);
        w();
        x();
        s.postDelayed(new hd(this), 500);
        v();
        this.t.k().addUIEventListener(1016, this);
        this.t.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
        this.t.k().addUIEventListener(1019, this);
        this.t.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
        this.t.k().addUIEventListener(1013, this);
        AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_ENTRANCE_UPDATE_PAGE);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.J = true;
        setIntent(intent);
        w();
        x();
    }

    /* access modifiers changed from: private */
    public STInfoV2 b(String str) {
        if (this.H == null) {
            this.H = STInfoBuilder.buildSTInfo(this, 200);
        }
        if (this.H != null) {
            this.H.slotId = str;
            this.H.actionId = 200;
        }
        return this.H;
    }

    private void v() {
        byte[] bArr;
        List<SimpleAppModel> a2 = u.a(this.I);
        byte[] bArr2 = null;
        String str = Constants.STR_EMPTY;
        if (a2 != null && !a2.isEmpty()) {
            String str2 = str;
            byte[] bArr3 = null;
            int i = 0;
            while (i < a2.size()) {
                SimpleAppModel simpleAppModel = a2.get(i);
                if (simpleAppModel != null) {
                    if (str2.length() > 0) {
                        str2 = str2 + "|";
                    }
                    str2 = str2 + simpleAppModel.f1634a + "_" + simpleAppModel.b;
                }
                if (i == 0) {
                    bArr = simpleAppModel.y;
                } else {
                    bArr = bArr3;
                }
                i++;
                bArr3 = bArr;
            }
            bArr2 = bArr3;
            str = str2;
        }
        STInfoV2 s = s();
        if (s != null) {
            s.updateWithExternalPara(this.p);
            s.extraData = str;
            s.recommendId = bArr2;
            s.pushInfo = getIntent().getStringExtra("preActivityPushInfo");
            k.a(s);
        }
    }

    private void w() {
        this.z = new StatUpdateManageAction();
        this.A = getIntent().getStringExtra(STConst.ST_PUSH_TO_UPDATE_KEY);
        try {
            if (!TextUtils.isEmpty(this.A)) {
                a(Integer.valueOf(this.A).intValue());
            } else {
                this.A = String.valueOf(p());
            }
        } catch (Throwable th) {
            this.A = String.valueOf(p());
        }
        this.B.register(this.C);
    }

    @SuppressLint({"ResourceAsColor"})
    private void x() {
        boolean z2 = true;
        this.t = AstApp.i();
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.u.b(this.K);
        this.v = (RelativeLayout) findViewById(R.id.layout_container);
        this.x = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.E = (LinearLayout) findViewById(R.id.tips_layout);
        this.F = (TXImageView) findViewById(R.id.tips_img);
        this.G = (TextView) findViewById(R.id.tips);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String string = extras.getString(a.D);
            if (extras.getBoolean(a.t) && TextUtils.isEmpty(string)) {
                this.u.a(getResources().getString(R.string.app_update));
            }
            this.I = AppConst.TENCENT_MOBILE_MANAGER_PKGNAME.equals(extras.getString(a.ae)) || AppConst.TENCENT_MOBILE_MANAGER_PKGNAME.equals(extras.getString(a.n));
        }
        if (this.w == null) {
            try {
                this.w = new UpdateListView(this.n, this.z);
                this.w.setActivity(this);
            } catch (Throwable th) {
            }
        } else {
            z2 = false;
        }
        if (this.I) {
            y();
        } else {
            z();
        }
        this.y = new UpdateListFooterView(this.n, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.NONE);
        this.y.setFooterViewOnclickListner(new hf(this));
        B();
        this.u.d(false);
        this.u.a(this);
        this.u.i();
        this.u.d();
        if (this.w != null) {
            this.w.initSTParam(this.p, getIntent().getStringExtra("preActivityPushInfo"));
            this.w.initData(this.A, getIntent());
            if (z2) {
                this.v.addView(this.w);
                A();
            }
            this.D = this.B.a(this.w.getUncheckSimpleAppModels());
        }
    }

    private void y() {
        this.u.b();
        if (this.w != null) {
            this.w.setGuanJiaStyle();
        }
    }

    private void z() {
        this.u.c();
        if (this.w != null) {
            this.w.setNormalStyle();
        }
    }

    private void A() {
        DownloadInfo c;
        e Z = m.a().Z();
        if (Z != null && (c = DownloadProxy.a().c(Z.f1912a)) != null && c.versionCode == Z.c) {
            this.E.setVisibility(0);
            this.G.setText(String.format(getResources().getString(R.string.wise_important_update_tips), c.name, c.versionName));
            this.F.updateImageView(c.iconUrl, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            this.E.setOnClickListener(new hg(this, c));
            m.a().a((e) null);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        i();
        if (this.w != null) {
            this.w.notifyChange();
        }
        this.u.l();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.w != null) {
            this.w.mUpdateListAdapter.a();
        }
        super.onPause();
        this.u.m();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.t.k().removeUIEventListener(1016, this);
        this.t.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
        this.t.k().removeUIEventListener(1019, this);
        this.t.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
        this.t.k().removeUIEventListener(1013, this);
        if (this.w != null) {
            this.w.mUpdateListAdapter.b();
        }
        super.onDestroy();
    }

    public void handleUIEvent(Message message) {
        int i;
        switch (message.what) {
            case 1013:
            case 1016:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED /*1018*/:
            case 1019:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE /*1020*/:
                if (this.w != null) {
                    this.w.refreshData();
                }
                i();
                if (this.I) {
                    i = u.h();
                } else {
                    i = u.i();
                }
                be.a().a(i);
                return;
            case EventDispatcherEnum.UI_EVENT_APP_STATE_UNINSTALL /*1014*/:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD /*1015*/:
            case EventDispatcherEnum.UI_EVENT_CHECKUPDATE_FAIL /*1017*/:
            default:
                return;
        }
    }

    public void i() {
        int i;
        if (this.I) {
            i = u.h();
        } else {
            i = u.i();
        }
        this.u.b(getResources().getString(R.string.app_update));
        if (i > 0) {
            this.u.c("(" + i + ")");
        } else {
            this.u.c(Constants.STR_EMPTY);
        }
        B();
    }

    public void j() {
        if (!m.a().D() && r.e()) {
            hh hhVar = new hh(this);
            Context baseContext = AstApp.i().getBaseContext();
            hhVar.titleRes = baseContext.getString(R.string.dialog_title_auto_download_tips);
            hhVar.contentRes = baseContext.getString(R.string.dialog_content_auto_download_tips);
            hhVar.btnTxtRes = baseContext.getString(R.string.dialog_btn_auto_download_tips);
            hhVar.blockCaller = true;
            v.a(hhVar);
            m.a().o(true);
        }
    }

    private void B() {
        int i;
        int i2;
        if (this.I) {
            i = u.h();
        } else {
            i = u.i();
        }
        if (this.w != null) {
            i2 = this.w.getUpdateListPageDataSize();
        } else {
            i2 = 0;
        }
        if (i > 0 || i2 > 0) {
            this.v.setVisibility(0);
            this.x.setVisibility(8);
            return;
        }
        int b = u.b(this.I);
        if (b > 0) {
            if (this.x.getVisibility() != 0) {
                this.v.setVisibility(8);
                this.x.setActivityPageId(STConst.ST_PAGE_UPDATE_ERROR_PAGE);
                this.x.setErrorType(60);
                this.x.setErrorText(getString(R.string.update_ingore_list_empty_txt));
                this.x.setErrorImage(R.drawable.emptypage_pic_05);
                this.x.setVisibility(0);
            }
            if (b > 0) {
                this.y.setFooterViewBackground(R.drawable.bg_card_selector);
                this.y.freshState(b > 0 ? 2 : 3);
                this.y.setFooterViewText(String.format(getResources().getString(R.string.view_ignorelist), Integer.valueOf(com.tencent.assistant.module.update.k.b().e().size())));
            }
            this.x.setErrorTextSetting(this.y);
        } else if (this.x.getVisibility() != 0) {
            this.v.setVisibility(8);
            this.x.setVisibility(0);
            this.x.setActivityPageId(STConst.ST_PAGE_UPDATE_ERROR_PAGE);
            this.x.setErrorType(60);
            this.x.setErrorText(getString(R.string.update_list_empty_txt));
        }
    }

    /* access modifiers changed from: private */
    public List<AppUpdateInfo> a(List<AppSimpleDetail> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            for (AppSimpleDetail next : list) {
                if (next != null) {
                    AppUpdateInfo appUpdateInfo = new AppUpdateInfo();
                    appUpdateInfo.o = next.f2003a;
                    appUpdateInfo.r = next.b;
                    appUpdateInfo.b = next.c;
                    appUpdateInfo.e = new PicInfo(next.d);
                    appUpdateInfo.f2006a = next.e;
                    appUpdateInfo.n = next.f;
                    appUpdateInfo.d = next.g;
                    appUpdateInfo.c = next.h;
                    appUpdateInfo.h = next.i;
                    appUpdateInfo.j = next.j;
                    appUpdateInfo.s = next.k;
                    appUpdateInfo.k = next.l;
                    appUpdateInfo.m = next.m;
                    arrayList.add(appUpdateInfo);
                }
            }
        }
        return arrayList;
    }
}
