package com.tencent.assistant.activity;

import android.content.Intent;
import android.view.View;

/* compiled from: ProGuard */
class ga implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpaceCleanActivity f597a;

    ga(SpaceCleanActivity spaceCleanActivity) {
        this.f597a = spaceCleanActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.f597a, BigFileCleanActivity.class);
        intent.putExtra("dock_plugin", this.f597a.P);
        this.f597a.startActivity(intent);
        this.f597a.finish();
    }
}
