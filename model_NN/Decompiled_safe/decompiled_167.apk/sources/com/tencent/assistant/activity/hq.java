package com.tencent.assistant.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.tencent.assistant.js.JsBridge;
import com.tencent.assistant.link.b;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ct;

/* compiled from: ProGuard */
class hq extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ VideoActivityV2 f639a;

    private hq(VideoActivityV2 videoActivityV2) {
        this.f639a = videoActivityV2;
    }

    /* synthetic */ hq(VideoActivityV2 videoActivityV2, hl hlVar) {
        this(videoActivityV2);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        XLog.i(VideoActivityV2.f387a, "[onPageStarted] ---> url : " + str);
        if (((str != null && str.startsWith("http")) || str.startsWith("https")) && this.f639a.j != null) {
            this.f639a.j.loadAuthorization(str);
        }
    }

    public void onPageFinished(WebView webView, String str) {
        XLog.i(VideoActivityV2.f387a, "[onPageFinished] ---> url : " + str);
        if (this.f639a.j != null) {
            this.f639a.j.doPageLoadFinished();
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        XLog.i(VideoActivityV2.f387a, "[shouldOverrideUrlLoading] ---> url : " + str);
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (str.startsWith("http") || str.startsWith("https")) {
            return super.shouldOverrideUrlLoading(webView, str);
        }
        if (str.startsWith(JsBridge.JS_BRIDGE_SCHEME)) {
            XLog.i("VideoDetailView", "url = " + str);
            this.f639a.j.invoke(str);
            return true;
        } else if (!str.equals("about:blank;") && !str.equals("about:blank")) {
            Uri parse = Uri.parse(str);
            Intent intent = new Intent("android.intent.action.VIEW", parse);
            if (!b.a(webView.getContext(), intent)) {
                return true;
            }
            String scheme = intent.getScheme();
            if (scheme == null || !scheme.equals("tmast")) {
                intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, 2000);
                if (!(this.f639a.f instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                this.f639a.f.startActivity(intent);
                return true;
            }
            Bundle bundle = new Bundle();
            int a2 = ct.a(parse.getQueryParameter("scene"), 0);
            if (a2 != 0) {
                bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, a2);
            } else {
                bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, 2000);
            }
            b.b(this.f639a.f, str, bundle);
            return true;
        } else if (Build.VERSION.SDK_INT >= 11) {
            return false;
        } else {
            return true;
        }
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        this.f639a.a(true);
    }
}
