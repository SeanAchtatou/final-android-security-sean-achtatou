package com.tencent.assistant.uninstall;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* compiled from: ProGuard */
public class WakeUpReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.Wake".equals(intent.getAction())) {
            e.a().a(intent);
        }
    }
}
