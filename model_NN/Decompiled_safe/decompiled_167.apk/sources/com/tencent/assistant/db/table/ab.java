package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.model.a.p;

/* compiled from: ProGuard */
public class ab implements IBaseTable {
    public synchronized boolean a(p pVar) {
        boolean z = false;
        synchronized (this) {
            if (pVar != null) {
                SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
                ContentValues contentValues = new ContentValues();
                contentValues.put("card_type", Integer.valueOf(pVar.f1649a));
                contentValues.put("card_id", Integer.valueOf(pVar.b));
                contentValues.put("app_package_name", pVar.c);
                contentValues.put("app_version_code", Integer.valueOf(pVar.d));
                contentValues.put("app_gray_version_code", Integer.valueOf(pVar.e));
                contentValues.put("record_time", Long.valueOf(pVar.f));
                if (((int) (writableDatabaseWrapper.insert("smart_card_show_app_record_table", null, contentValues) + ((long) 0))) > 0) {
                    z = true;
                }
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002d A[SYNTHETIC, Splitter:B:13:0x002d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.tencent.assistant.model.a.p> a() {
        /*
            r5 = this;
            r1 = 0
            monitor-enter(r5)
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x0042 }
            r2.<init>()     // Catch:{ all -> 0x0042 }
            com.tencent.assistant.db.helper.SqliteHelper r0 = r5.getHelper()     // Catch:{ all -> 0x0042 }
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()     // Catch:{ all -> 0x0042 }
            java.lang.String r3 = "select * from smart_card_show_app_record_table"
            r4 = 0
            android.database.Cursor r1 = r0.rawQuery(r3, r4)     // Catch:{ Exception -> 0x0032 }
            if (r1 == 0) goto L_0x002b
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0032 }
            if (r0 == 0) goto L_0x002b
        L_0x001e:
            com.tencent.assistant.model.a.p r0 = r5.a(r1)     // Catch:{ Exception -> 0x0032 }
            r2.add(r0)     // Catch:{ Exception -> 0x0032 }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0032 }
            if (r0 != 0) goto L_0x001e
        L_0x002b:
            if (r1 == 0) goto L_0x0030
            r1.close()     // Catch:{ all -> 0x0042 }
        L_0x0030:
            monitor-exit(r5)
            return r2
        L_0x0032:
            r0 = move-exception
            java.lang.String r3 = "SmartCardShowAppRecordTable"
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0045 }
            com.tencent.assistant.utils.XLog.d(r3, r0)     // Catch:{ all -> 0x0045 }
            if (r1 == 0) goto L_0x0030
            r1.close()     // Catch:{ all -> 0x0042 }
            goto L_0x0030
        L_0x0042:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0045:
            r0 = move-exception
            if (r1 == 0) goto L_0x004b
            r1.close()     // Catch:{ all -> 0x0042 }
        L_0x004b:
            throw r0     // Catch:{ all -> 0x0042 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.ab.a():java.util.List");
    }

    private p a(Cursor cursor) {
        p pVar = new p();
        pVar.f1649a = cursor.getInt(cursor.getColumnIndexOrThrow("card_type"));
        pVar.b = cursor.getInt(cursor.getColumnIndexOrThrow("card_id"));
        pVar.c = cursor.getString(cursor.getColumnIndexOrThrow("app_package_name"));
        pVar.d = cursor.getInt(cursor.getColumnIndexOrThrow("app_version_code"));
        pVar.e = cursor.getInt(cursor.getColumnIndexOrThrow("app_gray_version_code"));
        pVar.f = (long) cursor.getInt(cursor.getColumnIndexOrThrow("record_time"));
        return pVar;
    }

    public synchronized int a(long j) {
        return getHelper().getWritableDatabaseWrapper().delete("smart_card_show_app_record_table", "record_time < ?", new String[]{Long.toString(j / 1000)}) + 0;
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "smart_card_show_app_record_table";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists smart_card_show_app_record_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, card_type INTEGER, card_id INTEGER, app_package_name TEXT, app_version_code INTEGER, app_gray_version_code INTEGER, record_time INTEGER); ";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 != 9) {
            return null;
        }
        return new String[]{"CREATE TABLE if not exists smart_card_show_app_record_table( _id INTEGER PRIMARY KEY AUTOINCREMENT, card_type INTEGER, card_id INTEGER, app_package_name TEXT, app_version_code INTEGER, app_gray_version_code INTEGER, record_time INTEGER); "};
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }
}
