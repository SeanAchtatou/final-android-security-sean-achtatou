package com.tencent.assistant.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.assistant.db.table.SwitchPhoneTable;
import com.tencent.assistant.db.table.a;
import com.tencent.assistant.db.table.aa;
import com.tencent.assistant.db.table.ab;
import com.tencent.assistant.db.table.ac;
import com.tencent.assistant.db.table.ad;
import com.tencent.assistant.db.table.ae;
import com.tencent.assistant.db.table.b;
import com.tencent.assistant.db.table.c;
import com.tencent.assistant.db.table.d;
import com.tencent.assistant.db.table.f;
import com.tencent.assistant.db.table.i;
import com.tencent.assistant.db.table.j;
import com.tencent.assistant.db.table.n;
import com.tencent.assistant.db.table.p;
import com.tencent.assistant.db.table.q;
import com.tencent.assistant.db.table.s;
import com.tencent.assistant.db.table.v;
import com.tencent.assistant.db.table.z;

/* compiled from: ProGuard */
public class AstDbHelper extends SqliteHelper {
    private static final String DB_NAME = "mobile_ast.db";
    private static final int DB_VERSION = 15;
    private static final Class<?>[] TABLESS = {b.class, d.class, c.class, n.class, v.class, a.class, s.class, ad.class, q.class, aa.class, ac.class, ab.class, z.class, i.class, p.class, j.class, SwitchPhoneTable.class, f.class, com.tencent.assistantv2.db.a.c.class, ae.class};
    private static volatile SqliteHelper instance;

    public static synchronized SqliteHelper get(Context context) {
        SqliteHelper sqliteHelper;
        synchronized (AstDbHelper.class) {
            if (instance == null) {
                instance = new AstDbHelper(context, DB_NAME, null, 15);
            }
            sqliteHelper = instance;
        }
        return sqliteHelper;
    }

    public AstDbHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, DB_NAME, null, i);
    }

    public Class<?>[] getTables() {
        return TABLESS;
    }

    public int getDBVersion() {
        return 15;
    }
}
