package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.protocol.jce.DesktopShortCut;

/* compiled from: ProGuard */
public class z implements IBaseTable {
    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "short_cut_table";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists short_cut_table(_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,pkgName TEXT,iconUrl TEXT,channelId TEXT,canDelete INTEGER,actionUrl TEXT);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 != 7) {
            return null;
        }
        return new String[]{"CREATE TABLE if not exists short_cut_table(_id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,pkgName TEXT,iconUrl TEXT,channelId TEXT,canDelete INTEGER,actionUrl TEXT);"};
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }

    public long a(DesktopShortCut desktopShortCut) {
        SQLiteDatabase writableDatabase = getHelper().getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", desktopShortCut.b());
        if (!TextUtils.isEmpty(desktopShortCut.c())) {
            contentValues.put("pkgName", desktopShortCut.c());
        }
        if (!TextUtils.isEmpty(desktopShortCut.a())) {
            contentValues.put("iconUrl", desktopShortCut.a());
        }
        if (!TextUtils.isEmpty(desktopShortCut.d())) {
            contentValues.put("channelId", desktopShortCut.d());
        }
        contentValues.put("canDelete", Byte.valueOf(desktopShortCut.f()));
        if (!TextUtils.isEmpty(desktopShortCut.e().a())) {
            contentValues.put("actionUrl", desktopShortCut.e().a());
        }
        return writableDatabase.insert("short_cut_table", null, contentValues);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00c2, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00c9, code lost:
        r8 = r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00aa A[SYNTHETIC, Splitter:B:19:0x00aa] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00b3 A[SYNTHETIC, Splitter:B:26:0x00b3] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00be A[SYNTHETIC, Splitter:B:34:0x00be] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c2 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:8:0x0018] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.ArrayList<com.tencent.assistant.protocol.jce.DesktopShortCut> a() {
        /*
            r9 = this;
            r8 = 0
            monitor-enter(r9)
            com.tencent.assistant.db.helper.SqliteHelper r0 = r9.getHelper()     // Catch:{ all -> 0x00b7 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ all -> 0x00b7 }
            java.lang.String r1 = "short_cut_table"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00af, all -> 0x00ba }
            if (r1 == 0) goto L_0x00cb
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x00c4, all -> 0x00c2 }
            if (r0 == 0) goto L_0x00cb
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x00c4, all -> 0x00c2 }
            r0.<init>()     // Catch:{ Exception -> 0x00c4, all -> 0x00c2 }
        L_0x0023:
            com.tencent.assistant.protocol.jce.DesktopShortCut r2 = new com.tencent.assistant.protocol.jce.DesktopShortCut     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            r2.<init>()     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r3 = "name"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            r2.b = r3     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r3 = "pkgName"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            r2.d = r3     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r3 = "iconUrl"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            if (r3 == 0) goto L_0x0065
            com.tencent.assistant.protocol.jce.ActionUrl r3 = new com.tencent.assistant.protocol.jce.ActionUrl     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            r3.<init>()     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            r2.f = r3     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            com.tencent.assistant.protocol.jce.ActionUrl r3 = r2.f     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r4 = "iconUrl"
            int r4 = r1.getColumnIndex(r4)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r4 = r1.getString(r4)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            r3.f1970a = r4     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
        L_0x0065:
            java.lang.String r3 = "channelId"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            r2.e = r3     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r3 = "iconUrl"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            r2.f2051a = r3     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r3 = "canDelete"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            int r3 = r1.getInt(r3)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            byte r3 = (byte) r3     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            r2.g = r3     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            com.tencent.assistant.protocol.jce.ActionUrl r3 = new com.tencent.assistant.protocol.jce.ActionUrl     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            r3.<init>()     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            r2.f = r3     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            com.tencent.assistant.protocol.jce.ActionUrl r3 = r2.f     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r4 = "actionUrl"
            int r4 = r1.getColumnIndex(r4)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            java.lang.String r4 = r1.getString(r4)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            r3.f1970a = r4     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            r0.add(r2)     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            boolean r2 = r1.moveToNext()     // Catch:{ Exception -> 0x00c8, all -> 0x00c2 }
            if (r2 != 0) goto L_0x0023
        L_0x00a8:
            if (r1 == 0) goto L_0x00ad
            r1.close()     // Catch:{ all -> 0x00b7 }
        L_0x00ad:
            monitor-exit(r9)
            return r0
        L_0x00af:
            r0 = move-exception
            r0 = r8
        L_0x00b1:
            if (r8 == 0) goto L_0x00ad
            r8.close()     // Catch:{ all -> 0x00b7 }
            goto L_0x00ad
        L_0x00b7:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x00ba:
            r0 = move-exception
            r1 = r8
        L_0x00bc:
            if (r1 == 0) goto L_0x00c1
            r1.close()     // Catch:{ all -> 0x00b7 }
        L_0x00c1:
            throw r0     // Catch:{ all -> 0x00b7 }
        L_0x00c2:
            r0 = move-exception
            goto L_0x00bc
        L_0x00c4:
            r0 = move-exception
            r0 = r8
            r8 = r1
            goto L_0x00b1
        L_0x00c8:
            r2 = move-exception
            r8 = r1
            goto L_0x00b1
        L_0x00cb:
            r0 = r8
            goto L_0x00a8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.z.a():java.util.ArrayList");
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }
}
