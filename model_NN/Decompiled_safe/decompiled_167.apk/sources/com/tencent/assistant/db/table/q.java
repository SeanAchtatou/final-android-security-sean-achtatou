package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.DownloadDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.model.j;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
public class q implements IBaseTable {
    public void a(j jVar) {
        if (jVar != null) {
            try {
                SQLiteDatabaseWrapper writableDatabaseWrapper = getHelper().getWritableDatabaseWrapper();
                if (a(jVar, writableDatabaseWrapper) <= 0) {
                    ContentValues contentValues = new ContentValues();
                    a(contentValues, jVar);
                    writableDatabaseWrapper.insert("plugin_downloadinfos", null, contentValues);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private int a(j jVar, SQLiteDatabaseWrapper sQLiteDatabaseWrapper) {
        if (jVar == null) {
            return -1;
        }
        try {
            ContentValues contentValues = new ContentValues();
            a(contentValues, jVar);
            int update = sQLiteDatabaseWrapper.update("plugin_downloadinfos", contentValues, "plugin_id = ?", new String[]{String.valueOf(jVar.f1666a)});
            if (update <= 0) {
                return 0;
            }
            return update;
        } catch (Exception e) {
            e.printStackTrace();
            return -2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.assistant.model.j> a() {
        /*
            r5 = this;
            r1 = 0
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            com.tencent.assistant.db.helper.SqliteHelper r0 = r5.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r3 = "select * from plugin_downloadinfos order by plugin_displayDrder asc"
            r4 = 0
            android.database.Cursor r1 = r0.rawQuery(r3, r4)     // Catch:{ Exception -> 0x0030 }
            if (r1 == 0) goto L_0x002a
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0030 }
            if (r0 == 0) goto L_0x002a
        L_0x001d:
            com.tencent.assistant.model.j r0 = r5.a(r1)     // Catch:{ Exception -> 0x0030 }
            r2.add(r0)     // Catch:{ Exception -> 0x0030 }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0030 }
            if (r0 != 0) goto L_0x001d
        L_0x002a:
            if (r1 == 0) goto L_0x002f
            r1.close()
        L_0x002f:
            return r2
        L_0x0030:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x003a }
            if (r1 == 0) goto L_0x002f
            r1.close()
            goto L_0x002f
        L_0x003a:
            r0 = move-exception
            if (r1 == 0) goto L_0x0040
            r1.close()
        L_0x0040:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.q.a():java.util.List");
    }

    public void a(int i) {
        getHelper().getWritableDatabaseWrapper().delete("plugin_downloadinfos", "plugin_id = ?", new String[]{String.valueOf(i)});
    }

    public j a(Cursor cursor) {
        j jVar = new j();
        if (cursor != null) {
            jVar.f1666a = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_id"));
            jVar.b = cursor.getString(cursor.getColumnIndexOrThrow("plugin_downloadTicket"));
            jVar.c = cursor.getString(cursor.getColumnIndexOrThrow("plugin_packagename"));
            jVar.d = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_version"));
            jVar.e = cursor.getString(cursor.getColumnIndexOrThrow("plugin_name"));
            jVar.g = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_displayDrder"));
            jVar.f = cursor.getString(cursor.getColumnIndexOrThrow("plugin_desc"));
            jVar.h = cursor.getString(cursor.getColumnIndexOrThrow("plugin_iconUrl"));
            jVar.i = cursor.getString(cursor.getColumnIndexOrThrow("plugin_imgUrl"));
            jVar.j = cursor.getLong(cursor.getColumnIndexOrThrow("plugin_fileSize"));
            jVar.k = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_minApiLevel"));
            jVar.l = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_minPluginVersion"));
            jVar.m = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_minBaoVersion"));
            jVar.n = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_needPreDownload"));
            jVar.o = cursor.getString(cursor.getColumnIndexOrThrow("plugin_downUrl"));
            jVar.p = cursor.getString(cursor.getColumnIndexOrThrow("plugin_startActivity"));
            jVar.q = cursor.getInt(cursor.getColumnIndexOrThrow("plugin_for_bao_type"));
            jVar.r = cursor.getInt(cursor.getColumnIndexOrThrow(SocialConstants.PARAM_TYPE));
            jVar.s = cursor.getString(cursor.getColumnIndexOrThrow("actionUrl"));
        }
        return jVar;
    }

    public void a(ContentValues contentValues, j jVar) {
        if (jVar != null) {
            contentValues.put("plugin_id", Integer.valueOf(jVar.f1666a));
            contentValues.put("plugin_downloadTicket", jVar.b);
            contentValues.put("plugin_packagename", jVar.c);
            contentValues.put("plugin_version", Integer.valueOf(jVar.d));
            contentValues.put("plugin_name", jVar.e);
            contentValues.put("plugin_displayDrder", Integer.valueOf(jVar.g));
            contentValues.put("plugin_desc", jVar.f);
            contentValues.put("plugin_iconUrl", jVar.h);
            contentValues.put("plugin_imgUrl", jVar.i);
            contentValues.put("plugin_fileSize", Long.valueOf(jVar.j));
            contentValues.put("plugin_minApiLevel", Integer.valueOf(jVar.k));
            contentValues.put("plugin_minPluginVersion", Integer.valueOf(jVar.l));
            contentValues.put("plugin_minBaoVersion", Integer.valueOf(jVar.m));
            contentValues.put("plugin_needPreDownload", Integer.valueOf(jVar.n));
            contentValues.put("plugin_downUrl", jVar.o);
            contentValues.put("plugin_startActivity", jVar.p);
            contentValues.put("plugin_for_bao_type", Integer.valueOf(jVar.q));
            contentValues.put(SocialConstants.PARAM_TYPE, Integer.valueOf(jVar.r));
            contentValues.put("actionUrl", jVar.s);
        }
    }

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "plugin_downloadinfos";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists plugin_downloadinfos (row_id INTEGER PRIMARY KEY AUTOINCREMENT,plugin_id INTEGER UNIQUE,plugin_downloadTicket TEXT UINQUE,plugin_packagename TEXT,plugin_version INTEGER,plugin_displayDrder INTEGER,plugin_name TEXT,plugin_desc TEXT,plugin_iconUrl TEXT,plugin_imgUrl TEXT,plugin_fileSize INTEGER,plugin_minApiLevel INTEGER,plugin_minPluginVersion INTEGER,plugin_minBaoVersion INTEGER,plugin_needPreDownload INTEGER,plugin_for_bao_type INTEGER,plugin_downUrl TEXT,plugin_startActivity TEXT,type INTEGER,actionUrl TEXT);";
    }

    public String[] getAlterSQL(int i, int i2) {
        if (i2 == 4) {
            return new String[]{"CREATE TABLE if not exists plugin_downloadinfos (row_id INTEGER PRIMARY KEY AUTOINCREMENT,plugin_id INTEGER UNIQUE,plugin_downloadTicket TEXT UINQUE,plugin_packagename TEXT,plugin_version INTEGER,plugin_displayDrder INTEGER,plugin_name TEXT,plugin_desc TEXT,plugin_iconUrl TEXT,plugin_imgUrl TEXT,plugin_fileSize INTEGER,plugin_minApiLevel INTEGER,plugin_minPluginVersion INTEGER,plugin_minBaoVersion INTEGER,plugin_needPreDownload INTEGER,plugin_for_bao_type INTEGER,plugin_downUrl TEXT,plugin_startActivity TEXT,type INTEGER,actionUrl TEXT);"};
        } else if (i != 10 || i2 != 11) {
            return null;
        } else {
            return new String[]{"alter table plugin_downloadinfos add column type INTEGER;", "alter table plugin_downloadinfos add column actionUrl TEXT;"};
        }
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return DownloadDbHelper.get(AstApp.i());
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }
}
