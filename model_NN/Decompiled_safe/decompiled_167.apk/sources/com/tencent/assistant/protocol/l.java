package com.tencent.assistant.protocol;

import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.protocol.a.a.c;
import com.tencent.assistant.protocol.a.e;
import com.tencent.assistant.protocol.jce.JceCmd;
import com.tencent.assistant.protocol.jce.Net;
import com.tencent.assistant.protocol.jce.PkgReq;
import com.tencent.assistant.protocol.jce.PkgReqHead;
import com.tencent.assistant.protocol.jce.PkgRsp;
import com.tencent.assistant.protocol.jce.ReqHead;
import com.tencent.assistant.protocol.jce.Request;
import com.tencent.assistant.protocol.jce.Response;
import com.tencent.assistant.protocol.jce.StatReportItem;
import com.tencent.assistant.protocol.jce.StatReportRequest;
import com.tencent.assistant.protocol.scu.cscomm.CsCommManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import org.apache.http.util.ByteArrayBuffer;

/* compiled from: ProGuard */
public class l {
    public static Request a(Request request, Net net) {
        if (request == null) {
            return null;
        }
        if (request.head == null) {
            return null;
        }
        ReqHead reqHead = request.head;
        reqHead.f = net;
        c d = e.a().d();
        if (d == null) {
            return request;
        }
        reqHead.l = d.f1959a;
        reqHead.j = d.b;
        reqHead.g = d.c;
        reqHead.c = d.d;
        reqHead.e = d.e;
        reqHead.h = d.f;
        reqHead.d = d.g;
        reqHead.m = d.h;
        reqHead.o = d.i;
        reqHead.p = d.j;
        reqHead.q = d.k;
        long l = e.a().l();
        if (l != 0) {
            reqHead.r = l + (System.currentTimeMillis() / 1000);
        }
        reqHead.s = System.currentTimeMillis() / 1000;
        return request;
    }

    public static byte[] a(PkgReq pkgReq) {
        if (pkgReq == null) {
            return null;
        }
        byte[] b = b(pkgReq);
        byte[] a2 = a();
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(a2.length + b.length);
        byteArrayBuffer.append(a2, 0, a2.length);
        byteArrayBuffer.append(b, 0, b.length);
        return byteArrayBuffer.toByteArray();
    }

    public static byte[] a() {
        byte[] bytes = "YYB".getBytes();
        byte[] bArr = {0, 0, 0, 2};
        ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(bytes.length + bArr.length);
        byteArrayBuffer.append(bytes, 0, bytes.length);
        byteArrayBuffer.append(bArr, 0, bArr.length);
        return byteArrayBuffer.toByteArray();
    }

    public static Response a(byte[] bArr) {
        if (bArr == null || bArr.length < 4) {
            return null;
        }
        Response response = new Response();
        try {
            JceInputStream jceInputStream = new JceInputStream(bArr);
            jceInputStream.setServerEncoding("utf-8");
            response.readFrom(jceInputStream);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static PkgRsp b(byte[] bArr) {
        if (bArr == null || bArr.length < 4) {
            return null;
        }
        PkgRsp pkgRsp = new PkgRsp();
        try {
            JceInputStream jceInputStream = new JceInputStream(bArr);
            jceInputStream.setServerEncoding("utf-8");
            pkgRsp.readFrom(jceInputStream);
            return pkgRsp;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int a(JceStruct jceStruct) {
        String simpleName = jceStruct.getClass().getSimpleName();
        return JceCmd.a(simpleName.substring(0, simpleName.length() - "Request".length())).a();
    }

    public static JceStruct a(JceStruct jceStruct, byte[] bArr, ClassLoader classLoader) {
        JceStruct a2;
        if (jceStruct == null || bArr == null || (a2 = a(jceStruct, classLoader)) == null) {
            return null;
        }
        try {
            JceInputStream jceInputStream = new JceInputStream(bArr);
            jceInputStream.setServerEncoding("utf-8");
            a2.readFrom(jceInputStream);
            return a2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] b(JceStruct jceStruct) {
        if (jceStruct == null) {
            return null;
        }
        JceOutputStream jceOutputStream = new JceOutputStream();
        jceOutputStream.setServerEncoding("utf-8");
        try {
            jceStruct.writeTo(jceOutputStream);
            return jceOutputStream.toByteArray();
        } catch (Throwable th) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            th.printStackTrace(new PrintStream(byteArrayOutputStream));
            try {
                byteArrayOutputStream.close();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    private static JceStruct a(JceStruct jceStruct, ClassLoader classLoader) {
        Class<?> cls;
        JceStruct jceStruct2;
        if (jceStruct == null) {
            return null;
        }
        String name = jceStruct.getClass().getName();
        String str = name.substring(0, name.length() - "Request".length()) + "Response";
        if (classLoader == null) {
            try {
                cls = Class.forName(str);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                jceStruct2 = null;
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
                jceStruct2 = null;
            } catch (InstantiationException e3) {
                e3.printStackTrace();
                jceStruct2 = null;
            }
        } else {
            cls = Class.forName(str, true, classLoader);
        }
        jceStruct2 = (JceStruct) cls.newInstance();
        return jceStruct2;
    }

    public static o a(PkgRsp pkgRsp) {
        if (pkgRsp == null) {
            return null;
        }
        o oVar = new o();
        Response response = new Response();
        oVar.f2430a = CsCommManager.a().decryptResponse(pkgRsp, response);
        oVar.b = response;
        return oVar;
    }

    public static o a(Request request) {
        o oVar = new o();
        PkgReq pkgReq = new PkgReq();
        PkgReqHead pkgReqHead = new PkgReqHead();
        pkgReqHead.f2254a = request.head.f2284a;
        pkgReq.pkgReqHead = pkgReqHead;
        int encryptRequest = CsCommManager.a().encryptRequest(request, pkgReq);
        if (!(pkgReq == null || pkgReq.pkgReqHead == null)) {
            pkgReq.pkgReqHead.d = e.a().e();
        }
        i.a("ProtocolPackage", "GUID IS: " + e.a().e());
        oVar.f2430a = encryptRequest;
        oVar.b = pkgReq;
        return oVar;
    }

    public static boolean a(List<JceStruct> list) {
        StatReportItem statReportItem;
        if (list != null && list.size() > 0) {
            for (JceStruct next : list) {
                if (a(next) == 24) {
                    StatReportRequest statReportRequest = (StatReportRequest) next;
                    if (!(statReportRequest.f2375a == null || statReportRequest.f2375a.size() == 0 || (statReportItem = statReportRequest.f2375a.get(0)) == null || statReportItem.f2374a != 21)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
