package com.tencent.assistant.download;

import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.be;

/* compiled from: ProGuard */
class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f1253a;
    final /* synthetic */ SimpleDownloadInfo.UIType b;
    final /* synthetic */ a c;

    c(a aVar, DownloadInfo downloadInfo, SimpleDownloadInfo.UIType uIType) {
        this.c = aVar;
        this.f1253a = downloadInfo;
        this.b = uIType;
    }

    public void run() {
        if (this.f1253a.uiType != this.b) {
            this.f1253a.uiType = this.b;
            DownloadProxy.a().d(this.f1253a);
        }
        if (!be.a().b(this.f1253a)) {
            DownloadProxy.a().c(this.f1253a);
        } else if (this.f1253a.downloadState == SimpleDownloadInfo.DownloadState.FAIL) {
            DownloadProxy.a().c(this.f1253a);
        } else {
            be.a().d(this.f1253a);
        }
    }
}
