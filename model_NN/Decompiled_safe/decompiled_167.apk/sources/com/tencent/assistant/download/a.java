package com.tencent.assistant.download;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.link.b;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.installuninstall.p;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class a implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    static a f1251a;
    private static AstApp c = AstApp.i();
    /* access modifiers changed from: private */
    public static EventDispatcher d = c.j();
    private static ConcurrentHashMap<String, String> e = new ConcurrentHashMap<>();
    public ConcurrentHashMap<String, j> b;
    private Handler f;
    /* access modifiers changed from: private */
    public boolean g;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f1251a == null) {
                f1251a = new a();
            }
            aVar = f1251a;
        }
        return aVar;
    }

    private a() {
        this.f = null;
        this.b = new ConcurrentHashMap<>();
        this.f = k.a();
        c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        c.k().addUIEventListener(1010, this);
        c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
    }

    public boolean a(String str) {
        DownloadInfo d2 = DownloadProxy.a().d(str);
        if (d2 == null) {
            return false;
        }
        DownloadManager.a().d(d2.getDownloadSubType(), str);
        if (d2.fileType == SimpleDownloadInfo.DownloadType.APK) {
            d2.sllUpdate = 0;
        }
        d2.downloadState = SimpleDownloadInfo.DownloadState.FAIL;
        DownloadProxy.a().d(d2);
        if (d2.fileType == SimpleDownloadInfo.DownloadType.APK && d2.isUpdate == 1) {
            d.sendMessage(d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START));
        }
        DownloadProxy.a().c(d2);
        return true;
    }

    public boolean a(String str, boolean z) {
        DownloadInfo d2;
        if (!TextUtils.isEmpty(str) && (d2 = DownloadProxy.a().d(str)) != null) {
            if (be.a().b(d2)) {
                be.a().e(d2);
                return true;
            }
            String filePath = d2.getFilePath();
            if (!TextUtils.isEmpty(filePath)) {
                if (!new File(filePath).exists()) {
                    d2.downloadState = SimpleDownloadInfo.DownloadState.FAIL;
                    DownloadProxy.a().d(d2);
                    d.sendMessage(d.obtainMessage(1007, d2.downloadTicket));
                } else {
                    p.a().a(d2, z);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean a(DownloadInfo downloadInfo, boolean z) {
        if (downloadInfo != null) {
            if (be.a().b(downloadInfo)) {
                be.a().e(downloadInfo);
            } else {
                if (downloadInfo.checkCurrentDownloadSucc() == 0) {
                    d.sendMessage(d.obtainMessage(1007, downloadInfo.downloadTicket));
                }
                p.a().a(downloadInfo, z);
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo, boolean):void
     arg types: [com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo, int]
     candidates:
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, java.util.List, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.assistant.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo, boolean):void */
    public void a(SimpleAppModel simpleAppModel, StatInfo statInfo) {
        a(simpleAppModel, statInfo, false);
    }

    public void a(SimpleAppModel simpleAppModel, StatInfo statInfo, boolean z) {
        this.f.post(new b(this, simpleAppModel, statInfo, z));
    }

    public void a(DownloadInfo downloadInfo) {
        if (a(downloadInfo.downloadTicket, 1)) {
            a(downloadInfo, SimpleDownloadInfo.UIType.NORMAL);
        }
    }

    public void a(DownloadInfo downloadInfo, SimpleDownloadInfo.UIType uIType) {
        this.f.post(new c(this, downloadInfo, uIType));
    }

    public void b(String str) {
        if (a(str, 2)) {
            this.f.post(new d(this, str));
        }
    }

    public void b(DownloadInfo downloadInfo) {
        if (a(downloadInfo.downloadTicket, 1)) {
            b(downloadInfo, SimpleDownloadInfo.UIType.NORMAL);
        }
    }

    public void b(DownloadInfo downloadInfo, SimpleDownloadInfo.UIType uIType) {
        this.f.post(new e(this, downloadInfo, uIType));
    }

    public void c(DownloadInfo downloadInfo) {
        if (downloadInfo != null && !TextUtils.isEmpty(downloadInfo.packageName)) {
            if (be.a().b(downloadInfo)) {
                be.a().e(downloadInfo);
                return;
            }
            if (!TextUtils.isEmpty(downloadInfo.appLinkActionUrl)) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(downloadInfo.appLinkActionUrl));
                if (b.a(AstApp.i(), intent)) {
                    intent.setFlags(268435456);
                    AstApp.i().startActivity(intent);
                    return;
                }
            }
            d(downloadInfo.packageName);
        }
    }

    public void c(String str) {
        if (be.a().c(str)) {
            be.a().e(str);
            return;
        }
        DownloadInfo d2 = DownloadProxy.a().d(str);
        if (d2 != null && !TextUtils.isEmpty(d2.appLinkActionUrl)) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(d2.appLinkActionUrl));
            if (b.a(AstApp.i(), intent)) {
                intent.setFlags(268435456);
                AstApp.i().startActivity(intent);
                return;
            }
        }
        d(str);
    }

    public void a(String str, String str2) {
        if (be.a().c(str)) {
            be.a().e(str);
            return;
        }
        if (!TextUtils.isEmpty(str2)) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
            if (b.a(AstApp.i(), intent)) {
                intent.setFlags(268435456);
                AstApp.i().startActivity(intent);
                return;
            }
        }
        d(str);
    }

    private void d(String str) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantsAPI.Token.WX_TOKEN_PLATFORMID_KEY, "myapp_m");
        if (!r.a(str, bundle)) {
            ba.a().post(new f(this));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.download.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, java.util.List):java.util.List
      com.tencent.assistant.download.a.a(java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>, boolean):void
      com.tencent.assistant.download.a.a(long, long):boolean
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, boolean):boolean
      com.tencent.assistant.download.a.a(java.lang.String, int):boolean
      com.tencent.assistant.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.download.SimpleDownloadInfo$UIType):void
      com.tencent.assistant.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.assistant.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, boolean):boolean
      com.tencent.assistant.download.a.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, boolean):boolean
     arg types: [com.tencent.assistant.download.DownloadInfo, int]
     candidates:
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, java.util.List):java.util.List
      com.tencent.assistant.download.a.a(java.util.ArrayList<com.tencent.assistant.download.DownloadInfo>, boolean):void
      com.tencent.assistant.download.a.a(long, long):boolean
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.a, boolean):boolean
      com.tencent.assistant.download.a.a(java.lang.String, int):boolean
      com.tencent.assistant.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, com.tencent.assistant.download.SimpleDownloadInfo$UIType):void
      com.tencent.assistant.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.assistant.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.download.a.a(java.lang.String, boolean):boolean
      com.tencent.assistant.download.a.a(com.tencent.assistant.download.DownloadInfo, boolean):boolean */
    public void d(DownloadInfo downloadInfo) {
        if (downloadInfo != null && downloadInfo.fileType == SimpleDownloadInfo.DownloadType.APK) {
            if (DownloadProxy.a().d(downloadInfo.downloadTicket) != null) {
                a(downloadInfo.downloadTicket, false);
            } else {
                a(downloadInfo, false);
            }
        }
    }

    public void handleUIEvent(Message message) {
        String str = Constants.STR_EMPTY;
        if (message.obj instanceof String) {
            str = (String) message.obj;
        }
        if (!TextUtils.isEmpty(str)) {
            switch (message.what) {
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC /*1006*/:
                    DownloadInfo d2 = DownloadProxy.a().d(str);
                    if (be.a().b(d2)) {
                        d2.downloadState = SimpleDownloadInfo.DownloadState.INSTALLED;
                        DownloadProxy.a().d(d2);
                        be.a().c(d2);
                        TemporaryThreadManager.get().start(new h(this, d2));
                        return;
                    } else if (d2 == null) {
                        return;
                    } else {
                        if ((d2.isUiTypeWiseAppUpdateDownload() && (TextUtils.isEmpty(d2.packageName) || !d2.packageName.equals(AstApp.i().getPackageName()))) || d2.isUiTypeWiseBookingDownload() || !d2.isUiTypeWiseDownload() || d2.isUiTypeWiseSubscribtionDownloadAutoInstall()) {
                            a().a(d2.downloadTicket, d2.autoInstall);
                            return;
                        }
                        return;
                    }
                case 1007:
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING /*1008*/:
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE /*1009*/:
                default:
                    return;
                case 1010:
                    TemporaryThreadManager.get().start(new g(this, str));
                    return;
                case EventDispatcherEnum.UI_EVENT_APP_INSTALL /*1011*/:
                    String str2 = (String) message.obj;
                    try {
                        if (e.containsValue(str2) && (r2 = e.keySet().iterator()) != null) {
                            for (String str3 : e.keySet()) {
                                String str4 = e.get(str3);
                                if (str2.equals(e.get(str4))) {
                                    e.remove(str4);
                                    return;
                                }
                            }
                            return;
                        }
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
            }
        }
    }

    public int a(List<SimpleAppModel> list, StatInfo statInfo) {
        if (list == null || list.isEmpty()) {
            return -1;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(list);
        if (this.g) {
            return 1;
        }
        this.g = true;
        TemporaryThreadManager.get().start(new i(this, arrayList, statInfo));
        return 0;
    }

    /* access modifiers changed from: private */
    public int c() {
        return 0;
    }

    /* access modifiers changed from: private */
    public List<SimpleAppModel> a(List<SimpleAppModel> list) {
        DownloadInfo downloadInfo;
        ArrayList arrayList = new ArrayList();
        ApkResourceManager instance = ApkResourceManager.getInstance();
        k b2 = k.b();
        DownloadProxy a2 = DownloadProxy.a();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return arrayList;
            }
            SimpleAppModel simpleAppModel = list.get(i2);
            DownloadInfo a3 = a2.a(simpleAppModel);
            if (a3 != null) {
                AppConst.AppState b3 = u.b(a3);
                if (AppConst.AppState.PAUSED == b3) {
                    if (a3 == null || !a3.needReCreateInfo(simpleAppModel)) {
                        downloadInfo = a3;
                    } else {
                        DownloadProxy.a().b(a3.downloadTicket);
                        downloadInfo = DownloadInfo.createDownloadInfo(simpleAppModel, null);
                    }
                    b(downloadInfo);
                } else if (AppConst.AppState.FAIL == b3) {
                    if (a3 != null && a3.needReCreateInfo(simpleAppModel)) {
                        DownloadProxy.a().b(a3.downloadTicket);
                        a3 = DownloadInfo.createDownloadInfo(simpleAppModel, null);
                    }
                    a(a3);
                } else if (AppConst.AppState.DOWNLOADED == b3) {
                    d(a3);
                }
            } else if (instance.getLocalApkInfo(simpleAppModel.c) == null || b2.b(simpleAppModel.c)) {
                arrayList.add(simpleAppModel);
            }
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: private */
    public void b(List<SimpleAppModel> list, StatInfo statInfo) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                SimpleAppModel simpleAppModel = list.get(i2);
                if (simpleAppModel != null) {
                    StatInfo statInfo2 = new StatInfo(statInfo);
                    statInfo2.recommendId = simpleAppModel.y;
                    a(simpleAppModel, statInfo2);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public static void a(ArrayList<DownloadInfo> arrayList, boolean z) {
        if (arrayList != null && !arrayList.isEmpty()) {
            p.a().a(arrayList, z);
        }
    }

    private boolean a(String str, int i) {
        j jVar = new j(this, str, i);
        j jVar2 = null;
        if (this.b.containsKey(str)) {
            jVar2 = this.b.get(str);
        }
        if (jVar2 == null || !jVar2.equals(jVar)) {
            this.b.put(str, jVar);
            return true;
        }
        try {
            if (a(jVar.a(), this.b.get(str).a())) {
                this.b.remove(str);
                return true;
            }
        } catch (Exception e2) {
        }
        return false;
    }

    private boolean a(long j, long j2) {
        return j - j2 >= 500;
    }
}
