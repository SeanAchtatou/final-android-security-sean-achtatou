package com.tencent.assistant.event;

import android.os.Handler;
import android.os.Message;

/* compiled from: ProGuard */
public class EventDispatcher extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private static EventDispatcher f1267a = null;
    private a b = null;

    public static EventDispatcher getInstance(a aVar) {
        if (f1267a == null) {
            f1267a = new EventDispatcher(aVar);
        }
        return f1267a;
    }

    private EventDispatcher(a aVar) {
        this.b = aVar;
    }

    public void handleMessage(Message message) {
        if (this.b != null) {
            this.b.handleEvent(message);
        }
        super.handleMessage(message);
    }

    public void setListener(a aVar) {
        this.b = aVar;
    }
}
