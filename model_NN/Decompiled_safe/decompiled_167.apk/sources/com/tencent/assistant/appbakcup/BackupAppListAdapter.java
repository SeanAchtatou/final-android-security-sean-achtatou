package com.tencent.assistant.appbakcup;

import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.AppBackupAppItemInfo;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.BackupApp;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.ct;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.x;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class BackupAppListAdapter extends BaseAdapter implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f825a;
    private ArrayList<BackupApp> b;
    /* access modifiers changed from: private */
    public BackupApplistDialog c;
    private int d = STConst.ST_PAGE_APP_BACKUP_APPLIST;
    /* access modifiers changed from: private */
    public boolean[] e;

    public BackupAppListAdapter(Context context) {
        this.f825a = context;
    }

    public int getCount() {
        if (this.b == null) {
            return 0;
        }
        return this.b.size();
    }

    /* renamed from: a */
    public BackupApp getItem(int i) {
        if (this.b == null || this.b.size() == 0) {
            return null;
        }
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        e eVar;
        if (view == null || view.getTag() == null || !(view.getTag() instanceof e)) {
            view = View.inflate(this.f825a, R.layout.backup_app_item, null);
            e eVar2 = new e(null);
            eVar2.f833a = view;
            eVar2.g = (DownloadButton) view.findViewById(R.id.down_btn);
            eVar2.f = (TextView) view.findViewById(R.id.description);
            eVar2.c = (AppIconView) view.findViewById(R.id.app_icon_img);
            eVar2.d = (TextView) view.findViewById(R.id.app_name);
            eVar2.i = (AppBackupAppItemInfo) view.findViewById(R.id.sub_layout);
            eVar2.e = (TextView) view.findViewById(R.id.apk_size);
            eVar2.h = (ListItemInfoView) view.findViewById(R.id.download_item_info);
            eVar2.b = (TextView) view.findViewById(R.id.select_image);
            view.setTag(eVar2);
            eVar = eVar2;
        } else {
            eVar = (e) view.getTag();
        }
        a(eVar, i);
        return view;
    }

    private void a(e eVar, int i) {
        BackupApp a2 = getItem(i);
        if (a2 != null) {
            SimpleAppModel a3 = u.a(a2);
            eVar.b.setSelected(this.e[i]);
            a aVar = new a(this, eVar.b, i);
            eVar.b.setOnClickListener(aVar);
            eVar.f833a.setOnClickListener(aVar);
            if (!TextUtils.isEmpty(a2.h)) {
                eVar.f.setVisibility(0);
                eVar.f.setText(a2.h);
            } else {
                eVar.f.setVisibility(8);
            }
            eVar.d.setText(a2.c);
            eVar.g.a(a3);
            eVar.h.a(a3);
            eVar.h.a(ListItemInfoView.InfoType.DOWNLOAD_PROGRESS_ONLY);
            eVar.i.setDownloadModel(a3);
            a(a3, eVar.h, eVar.i);
            eVar.g.setClickable(true);
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f825a, a3, b(i), 200, null);
            eVar.g.a(buildSTInfo, (x) null, (d) null, eVar.g, eVar.h);
            eVar.c.setSimpleAppModel(a3, new StatInfo(a3.b, this.d, 0, null, 0), -100);
            eVar.c.getIconImageView().setClickable(false);
            eVar.c.setClickable(false);
            eVar.e.setText(bt.a(a3.k));
        }
    }

    private void a(SimpleAppModel simpleAppModel, ListItemInfoView listItemInfoView, LinearLayout linearLayout) {
        switch (c.f831a[u.d(simpleAppModel).ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
                linearLayout.setVisibility(8);
                listItemInfoView.setVisibility(0);
                return;
            default:
                linearLayout.setVisibility(0);
                listItemInfoView.setVisibility(8);
                return;
        }
    }

    private String b(int i) {
        return "03_" + ct.a(i + 1);
    }

    public void a(ArrayList<BackupApp> arrayList) {
        this.b = arrayList;
        if (arrayList != null) {
            this.e = new boolean[arrayList.size()];
            Iterator<BackupApp> it = arrayList.iterator();
            int i = 0;
            while (it.hasNext()) {
                int i2 = i + 1;
                this.e[i] = it.next().g == 1;
                i = i2;
            }
        }
    }

    public void a(boolean z) {
        if (this.e != null) {
            int length = this.e.length;
            for (int i = 0; i < length; i++) {
                this.e[i] = z;
            }
        }
        notifyDataSetChanged();
    }

    public d a() {
        int i;
        boolean z;
        if (this.e == null) {
            return null;
        }
        long j = 0;
        boolean z2 = true;
        int length = this.e.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            if (this.e[i2]) {
                int i4 = i3 + 1;
                j += this.b.get(i2).m;
                z = z2;
                i = i4;
            } else {
                i = i3;
                z = false;
            }
            i2++;
            boolean z3 = z;
            i3 = i;
            z2 = z3;
        }
        return new d(i3, z2, bt.a(j), null);
    }

    public ArrayList<BackupApp> b() {
        ArrayList<BackupApp> arrayList = new ArrayList<>();
        if (!(this.b == null || this.e == null)) {
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                if (this.e[i]) {
                    arrayList.add(this.b.get(i));
                }
            }
        }
        return arrayList;
    }

    public void a(BackupApplistDialog backupApplistDialog) {
        this.c = backupApplistDialog;
    }

    public void c() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().removeUIEventListener(1016, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
    }

    public void d() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().addUIEventListener(1016, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
    }

    public void handleUIEvent(Message message) {
        DownloadInfo downloadInfo;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE /*1009*/:
                if (message.obj instanceof DownloadInfo) {
                    DownloadInfo downloadInfo2 = (DownloadInfo) message.obj;
                    if (downloadInfo2 != null && !TextUtils.isEmpty(downloadInfo2.downloadTicket)) {
                        downloadInfo = downloadInfo2;
                    } else {
                        return;
                    }
                } else {
                    downloadInfo = null;
                }
                Iterator<BackupApp> it = this.b.iterator();
                while (it.hasNext()) {
                    SimpleAppModel a2 = u.a(it.next());
                    if (downloadInfo != null && a2 != null && a2.q().equals(downloadInfo.downloadTicket)) {
                        e();
                        return;
                    }
                }
                return;
            case 1016:
                e();
                return;
            case EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY /*1038*/:
                e();
                return;
            default:
                return;
        }
    }

    private void e() {
        ba.a().post(new b(this));
    }
}
