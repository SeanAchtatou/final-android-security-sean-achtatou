package com.tencent.assistant.guessfavor;

import android.app.Activity;
import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.u;
import com.tencent.assistant.protocol.jce.RecommendAppInfoEx;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.GuessListItemInfoView;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class GuessFavorAdapter extends BaseAdapter implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private AstApp f1333a;
    private Context b;
    private List<SimpleAppModel> c = new ArrayList();
    private List<RecommendAppInfoEx> d = new ArrayList();
    /* access modifiers changed from: private */
    public View e;
    private int f;
    private String g = "GuessFavor";
    private b h = null;

    public GuessFavorAdapter(Context context, View view) {
        this.b = context;
        this.e = view;
        this.f1333a = AstApp.i();
    }

    public void a(List<RecommendAppInfoEx> list, List<SimpleAppModel> list2) {
        this.d.clear();
        this.c.clear();
        this.d.addAll(list);
        this.c.addAll(list2);
        notifyDataSetChanged();
    }

    public int getCount() {
        return a();
    }

    public int a() {
        if (this.c == null) {
            return 0;
        }
        XLog.i(this.g, "count=" + this.f);
        int i = t.d < 1.0f ? 4 : 5;
        if (this.f > 0) {
            i = this.f;
        } else {
            int i2 = t.c;
            if (this.b instanceof Activity) {
                i2 = ((Activity) this.b).getWindow().getDecorView().getMeasuredHeight();
            }
            XLog.i(this.g, ">height=" + i2);
            float f2 = t.d;
            this.f = (int) (((double) (((float) df.b(this.b, (float) i2)) - 160.0f)) * 0.0119d);
            if (f2 != 0.0f) {
                i = this.f;
            }
        }
        if (this.c.size() <= i) {
            return this.c.size();
        }
        return i;
    }

    /* renamed from: a */
    public SimpleAppModel getItem(int i) {
        if (this.c == null || i >= this.c.size()) {
            return null;
        }
        return this.c.get(i);
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        h hVar;
        SimpleAppModel a2 = getItem(i);
        if (a2 == null) {
            return null;
        }
        if (view == null || view.getTag() == null) {
            h hVar2 = new h(this);
            view = View.inflate(this.b, R.layout.item_guess_fav2, null);
            hVar2.f1341a = view.findViewById(R.id.content);
            hVar2.c = (DownloadButton) view.findViewById(R.id.down_btn);
            hVar2.c.a(false);
            hVar2.b = (AppIconView) view.findViewById(R.id.app_icon_img);
            hVar2.d = (TextView) view.findViewById(R.id.app_name);
            hVar2.e = (ImageView) view.findViewById(R.id.iv_divider);
            hVar2.f = (GuessListItemInfoView) view.findViewById(R.id.guessItemInfo);
            view.setTag(hVar2);
            hVar = hVar2;
        } else {
            hVar = (h) view.getTag();
        }
        view.setTag(R.id.tma_st_slot_tag, b(i));
        a(hVar, a2, i, a(a2, i));
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.guessfavor.GuessFavorAdapter.a(com.tencent.assistant.guessfavor.h, boolean):void
     arg types: [com.tencent.assistant.guessfavor.h, int]
     candidates:
      com.tencent.assistant.guessfavor.GuessFavorAdapter.a(com.tencent.assistant.model.SimpleAppModel, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.guessfavor.GuessFavorAdapter.a(java.util.List<com.tencent.assistant.protocol.jce.RecommendAppInfoEx>, java.util.List<com.tencent.assistant.model.SimpleAppModel>):void
      com.tencent.assistant.guessfavor.GuessFavorAdapter.a(com.tencent.assistant.guessfavor.h, boolean):void */
    private void a(h hVar, SimpleAppModel simpleAppModel, int i, STInfoV2 sTInfoV2) {
        if (hVar != null) {
            if (simpleAppModel != null) {
                hVar.b.setSimpleAppModel(simpleAppModel, new StatInfo(simpleAppModel.b, f(), 0, null, 0), -100);
                hVar.c.a(simpleAppModel);
                hVar.c.a(sTInfoV2);
                if (s.a(simpleAppModel)) {
                    hVar.c.setClickable(false);
                } else {
                    hVar.c.setClickable(true);
                    hVar.c.a(sTInfoV2, new f(this), (d) null, hVar.c, hVar.f.a());
                }
                hVar.f.a(simpleAppModel, this.d.get(i));
                hVar.d.setText(simpleAppModel.d);
            } else {
                a(hVar, false);
            }
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) hVar.f1341a.getLayoutParams();
            if (i == getCount() - 1) {
                if (layoutParams != null) {
                    layoutParams.height = df.a(this.b, 86.5f);
                    hVar.f1341a.setLayoutParams(layoutParams);
                }
                if (hVar.e != null) {
                    hVar.e.setVisibility(8);
                }
                hVar.f1341a.setBackgroundResource(R.drawable.common_cardbg_bottom_selector);
            } else if (i == 0) {
                if (t.b > 320) {
                    hVar.f1341a.setBackgroundResource(R.drawable.common_cardbg_top_selector);
                } else {
                    hVar.f1341a.setBackgroundResource(R.drawable.common_cardbg_middle_selector);
                }
                if (layoutParams != null) {
                    layoutParams.height = df.a(this.b, 86.5f);
                    hVar.f1341a.setLayoutParams(layoutParams);
                }
                if (hVar.e != null) {
                    hVar.e.setVisibility(0);
                }
            } else if (i != getCount() - 2) {
                hVar.f1341a.setBackgroundResource(R.drawable.common_cardbg_middle_selector);
                if (hVar.e != null) {
                    hVar.e.setVisibility(0);
                }
            } else if (com.tencent.assistant.login.d.a().j() || this.c == null || this.c.size() < this.f - 1) {
                hVar.f1341a.setBackgroundResource(R.drawable.common_cardbg_middle_selector);
                if (hVar.e != null) {
                    hVar.e.setVisibility(0);
                }
            } else {
                hVar.f1341a.setVisibility(8);
                if (hVar.e != null) {
                    hVar.e.setVisibility(8);
                }
            }
        }
    }

    private String b(int i) {
        return "03_" + ct.a(i + 1);
    }

    private void a(h hVar, boolean z) {
        if (z) {
            hVar.f1341a.setVisibility(0);
        } else {
            hVar.f1341a.setVisibility(8);
        }
    }

    public void b() {
        this.f1333a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.f1333a.k().removeUIEventListener(1016, this);
        this.f1333a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
    }

    public void c() {
        this.f1333a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.f1333a.k().addUIEventListener(1016, this);
        this.f1333a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY, this);
        notifyDataSetChanged();
    }

    public void handleUIEvent(Message message) {
        DownloadInfo downloadInfo;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                if (message.obj instanceof DownloadInfo) {
                    DownloadInfo downloadInfo2 = (DownloadInfo) message.obj;
                    if (downloadInfo2 != null && !TextUtils.isEmpty(downloadInfo2.downloadTicket)) {
                        downloadInfo = downloadInfo2;
                    } else {
                        return;
                    }
                } else {
                    downloadInfo = null;
                }
                for (SimpleAppModel next : this.c) {
                    if (downloadInfo != null && next.q().equals(downloadInfo.downloadTicket)) {
                        e();
                        return;
                    }
                }
                return;
            case 1016:
                u.g(this.c);
                e();
                return;
            case EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY:
                e();
                return;
            default:
                return;
        }
    }

    private void e() {
        ba.a().post(new g(this));
    }

    private int f() {
        if (com.tencent.assistant.login.d.a().j()) {
            return STConst.ST_PAGE_GUESS_FAVOR_LOGIN;
        }
        return STConst.ST_PAGE_GUESS_FAVOR_NOT_LOGIN;
    }

    public void d() {
        if (this.d != null) {
            this.d.clear();
            this.d = null;
        }
        if (this.c != null) {
            this.c.clear();
            this.c = null;
        }
    }

    private STInfoV2 a(SimpleAppModel simpleAppModel, int i) {
        if (simpleAppModel == null) {
            return STInfoBuilder.buildSTInfo(this.b, 100);
        }
        if (i == getCount() - 2 && !com.tencent.assistant.login.d.a().j() && this.c != null && this.c.size() >= this.f - 1) {
            return STInfoBuilder.buildSTInfo(this.b, 100);
        }
        String b2 = b(i);
        if (!(this.b instanceof BaseActivity)) {
            return STInfoBuilder.buildSTInfo(this.b, 100);
        }
        if (this.h == null) {
            this.h = new b();
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b, simpleAppModel, b2, 100, null);
        this.h.exposure(buildSTInfo);
        return buildSTInfo;
    }
}
