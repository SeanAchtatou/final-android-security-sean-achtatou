package com.tencent.assistant.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: ProGuard */
class bl implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Method f2652a;
    final /* synthetic */ Object b;
    final /* synthetic */ bk c;

    bl(bk bkVar, Method method, Object obj) {
        this.c = bkVar;
        this.f2652a = method;
        this.b = obj;
    }

    public void run() {
        try {
            this.f2652a.invoke(this.b, false);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        } catch (InvocationTargetException e3) {
            e3.printStackTrace();
        }
    }
}
