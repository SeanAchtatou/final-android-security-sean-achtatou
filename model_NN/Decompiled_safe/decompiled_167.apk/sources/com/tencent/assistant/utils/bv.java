package com.tencent.assistant.utils;

import com.tencent.connect.common.Constants;
import com.tencent.jni.JNI;

/* compiled from: ProGuard */
public class bv {
    public static String a(String str) {
        if (str == null || str.length() == 0) {
            return Constants.STR_EMPTY;
        }
        String str2 = Constants.STR_EMPTY;
        int i = 0;
        while (i < str.length()) {
            try {
                char charAt = str.charAt(i);
                if (charAt >= 19968 && charAt <= 40869) {
                    String GetUcs2Pinyin = JNI.GetUcs2Pinyin(charAt);
                    if (GetUcs2Pinyin != null && GetUcs2Pinyin.length() > 0) {
                        str2 = (str2 + GetUcs2Pinyin.substring(0, 1)) + charAt;
                    }
                } else if (charAt > ' ' && charAt < 127) {
                    str2 = (str2 + str.substring(i, i + 1).toLowerCase()) + 19968;
                }
                i++;
            } catch (Throwable th) {
                return str2;
            }
        }
        return str2;
    }
}
