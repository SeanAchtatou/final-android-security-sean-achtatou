package com.tencent.assistant.utils;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/* compiled from: ProGuard */
public class cq {

    /* renamed from: a  reason: collision with root package name */
    private int f2672a;
    private String b;
    private Queue<String> c = new LinkedList();

    public cq(int i) {
        this.f2672a = i;
    }

    public void a(String str) {
        if (!TextUtils.isEmpty(str) && !str.equals(this.b)) {
            this.c.offer(str);
            this.b = str;
            if (this.c.size() > this.f2672a) {
                this.c.poll();
            }
        }
    }

    public String toString() {
        if (this.c == null || this.c.size() == 0) {
            return "empty";
        }
        StringBuilder sb = new StringBuilder();
        ArrayList<String> arrayList = new ArrayList<>(this.c);
        if (arrayList != null && arrayList.size() > 0) {
            for (String append : arrayList) {
                sb.append(append).append("-");
            }
        }
        return sb.toString();
    }
}
