package com.tencent.assistant.utils;

import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.module.callback.g;

/* compiled from: ProGuard */
class bm implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bk f2653a;

    bm(bk bkVar) {
        this.f2653a = bkVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.bk.a(com.tencent.assistant.utils.bk, boolean):boolean
     arg types: [com.tencent.assistant.utils.bk, int]
     candidates:
      com.tencent.assistant.utils.bk.a(com.tencent.assistant.utils.bk, long):long
      com.tencent.assistant.utils.bk.a(com.tencent.assistant.utils.bk, java.util.List):void
      com.tencent.assistant.utils.bk.a(com.tencent.assistant.utils.bk, boolean):boolean */
    public void a() {
        boolean unused = this.f2653a.i = true;
        SpaceScanManager.a().a(this.f2653a.f2651a);
        SpaceScanManager.a().b(this.f2653a.j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.utils.bk.a(com.tencent.assistant.utils.bk, boolean):boolean
     arg types: [com.tencent.assistant.utils.bk, int]
     candidates:
      com.tencent.assistant.utils.bk.a(com.tencent.assistant.utils.bk, long):long
      com.tencent.assistant.utils.bk.a(com.tencent.assistant.utils.bk, java.util.List):void
      com.tencent.assistant.utils.bk.a(com.tencent.assistant.utils.bk, boolean):boolean */
    public void b() {
        SpaceScanManager.a().b(this.f2653a.j);
        boolean unused = this.f2653a.i = false;
        AstApp.i().j().dispatchMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_MGR_SPACE_CLEAN_FAIL));
    }

    public void c() {
    }
}
