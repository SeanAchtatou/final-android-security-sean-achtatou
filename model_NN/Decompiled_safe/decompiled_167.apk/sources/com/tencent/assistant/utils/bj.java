package com.tencent.assistant.utils;

import com.tencent.connect.common.Constants;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: ProGuard */
public class bj {

    /* renamed from: a  reason: collision with root package name */
    private static char[] f2650a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static byte[] a(String str) {
        try {
            try {
                return MessageDigest.getInstance("MD5").digest(str.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                XLog.e("andygzyu-MD5", "toMD5Byte, source.getBytes crash!");
                return null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            XLog.e("andygzyu-MD5", "toMD5Byte, MessageDigest.getInstance crash!");
            return null;
        }
    }

    public static String b(String str) {
        return a(a(str));
    }

    public static String a(InputStream inputStream) {
        int i = 0;
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            byte[] bArr = new byte[4196];
            while (true) {
                int read = inputStream.read(bArr, 0, bArr.length);
                if (read == -1) {
                    break;
                } else if (read > 0) {
                    instance.update(bArr, 0, read);
                    i += read;
                }
            }
            if (i == 0) {
                return Constants.STR_EMPTY;
            }
            return a(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return Constants.STR_EMPTY;
        } catch (IOException e2) {
            e2.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x003a A[SYNTHETIC, Splitter:B:24:0x003a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.io.File r5) {
        /*
            java.lang.String r0 = ""
            boolean r1 = r5.exists()
            if (r1 == 0) goto L_0x0026
            long r1 = r5.length()
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x0026
            r3 = 0
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ FileNotFoundException -> 0x0027, all -> 0x0037 }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0027, all -> 0x0037 }
            r1.<init>(r5)     // Catch:{ FileNotFoundException -> 0x0027, all -> 0x0037 }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0027, all -> 0x0037 }
            java.lang.String r0 = a(r2)     // Catch:{ FileNotFoundException -> 0x0048 }
            if (r2 == 0) goto L_0x0026
            r2.close()     // Catch:{ IOException -> 0x0043 }
        L_0x0026:
            return r0
        L_0x0027:
            r1 = move-exception
            r2 = r3
        L_0x0029:
            r1.printStackTrace()     // Catch:{ all -> 0x0045 }
            if (r2 == 0) goto L_0x0026
            r2.close()     // Catch:{ IOException -> 0x0032 }
            goto L_0x0026
        L_0x0032:
            r1 = move-exception
        L_0x0033:
            r1.printStackTrace()
            goto L_0x0026
        L_0x0037:
            r0 = move-exception
        L_0x0038:
            if (r3 == 0) goto L_0x003d
            r3.close()     // Catch:{ IOException -> 0x003e }
        L_0x003d:
            throw r0
        L_0x003e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003d
        L_0x0043:
            r1 = move-exception
            goto L_0x0033
        L_0x0045:
            r0 = move-exception
            r3 = r2
            goto L_0x0038
        L_0x0048:
            r1 = move-exception
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.utils.bj.a(java.io.File):java.lang.String");
    }

    private static String a(byte[] bArr) {
        if (bArr == null || bArr.length != 16) {
            return Constants.STR_EMPTY;
        }
        char[] cArr = new char[32];
        int i = 0;
        for (int i2 = 0; i2 < 16; i2++) {
            byte b = bArr[i2];
            int i3 = i + 1;
            cArr[i] = f2650a[(b >>> 4) & 15];
            i = i3 + 1;
            cArr[i3] = f2650a[b & 15];
        }
        return new String(cArr);
    }
}
