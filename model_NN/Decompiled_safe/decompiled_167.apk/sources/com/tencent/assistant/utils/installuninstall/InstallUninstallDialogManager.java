package com.tencent.assistant.utils.installuninstall;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.FunctionUtils;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.v;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class InstallUninstallDialogManager {
    /* access modifiers changed from: private */
    public static boolean h = false;
    private static Handler i = new b(AstApp.i().getMainLooper());

    /* renamed from: a  reason: collision with root package name */
    private final Object f2682a = new Object();
    private final long b = 90000;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public DIALOG_DEALWITH_RESULT d = DIALOG_DEALWITH_RESULT.RESULT_CANCEL;
    /* access modifiers changed from: private */
    public InstallUninstallTaskBean e;
    private boolean f = true;
    private boolean g = true;

    /* compiled from: ProGuard */
    public enum DIALOG_DEALWITH_RESULT {
        RESULT_CANCEL,
        RESULT_LEFT_BUTTON,
        RESULT_RIGHT_BUTTON
    }

    /* compiled from: ProGuard */
    public enum DIALOG_DEALWITH_TYPE {
        HAS_INSTALLED,
        DOWNLOAD_SPACE_NOT_ENOUGH,
        INSTALL_SPACE_NOT_ENOUGH,
        BROKEN,
        ROM_UNSUPPORT,
        DIFF_SIGNATURE
    }

    private void b() {
        synchronized (this.f2682a) {
            try {
                this.f2682a.wait(90000);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.f) {
            synchronized (this.f2682a) {
                this.f2682a.notifyAll();
            }
        }
        if (this.g) {
            h = false;
        }
    }

    public void a(boolean z) {
        this.f = z;
    }

    public boolean a(DIALOG_DEALWITH_TYPE dialog_dealwith_type, InstallUninstallTaskBean installUninstallTaskBean) {
        this.c = false;
        this.e = installUninstallTaskBean;
        AppConst.DialogInfo dialogInfo = null;
        switch (d.f2698a[dialog_dealwith_type.ordinal()]) {
            case 1:
                dialogInfo = d();
                break;
            case 2:
                dialogInfo = a();
                break;
            case 3:
                dialogInfo = e();
                break;
            case 4:
                dialogInfo = f();
                break;
            case 5:
                dialogInfo = g();
                dialogInfo.pageId = STConst.ST_DIALOG_UPDATE_DIFF_SIGNATURE;
                break;
        }
        if (dialogInfo == null) {
            return this.c;
        }
        dialogInfo.blockCaller = true;
        b(dialogInfo);
        if (this.f) {
            b();
        }
        return this.c;
    }

    public DIALOG_DEALWITH_RESULT b(DIALOG_DEALWITH_TYPE dialog_dealwith_type, InstallUninstallTaskBean installUninstallTaskBean) {
        this.d = DIALOG_DEALWITH_RESULT.RESULT_CANCEL;
        if (this.g && h) {
            return this.d;
        }
        this.e = installUninstallTaskBean;
        AppConst.TwoBtnDialogInfo twoBtnDialogInfo = null;
        switch (d.f2698a[dialog_dealwith_type.ordinal()]) {
            case 6:
                twoBtnDialogInfo = a(FunctionUtils.InstallSpaceNotEnoughForwardPage.INSTALLED_APP_MANAGER);
                twoBtnDialogInfo.pageId = STConst.ST_DIALOG_SPACE_NOT_ENOUGH_INSTALL_APP_MANAGER;
                break;
        }
        if (twoBtnDialogInfo == null) {
            return this.d;
        }
        twoBtnDialogInfo.blockCaller = true;
        b(twoBtnDialogInfo);
        if (this.f) {
            b();
        }
        return this.d;
    }

    private void b(AppConst.DialogInfo dialogInfo) {
        m mVar = new m(this);
        mVar.b = this;
        mVar.f2707a = dialogInfo;
        Message message = new Message();
        message.obj = mVar;
        i.sendMessageDelayed(message, 50);
    }

    public void a(AppConst.DialogInfo dialogInfo) {
        try {
            if (AstApp.m() != null) {
                if (dialogInfo instanceof AppConst.TwoBtnDialogInfo) {
                    v.a((AppConst.TwoBtnDialogInfo) dialogInfo);
                } else if (dialogInfo instanceof AppConst.OneBtnDialogInfo) {
                    v.a((AppConst.OneBtnDialogInfo) dialogInfo);
                }
                a(dialogInfo.pageId, AstApp.m().f(), STConst.ST_DEFAULT_SLOT, 100);
                return;
            }
            this.c = false;
            c();
        } catch (Exception e2) {
            this.c = false;
            c();
        }
    }

    private o d() {
        e eVar = new e(this);
        eVar.b = this.e;
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(this.e.packageName);
        if (localApkInfo != null) {
            if (this.e.versionCode == localApkInfo.mVersionCode) {
                eVar.contentRes = a((int) R.string.dialog_content_check_apk__equal_version_installed);
            } else if (this.e.versionCode < localApkInfo.mVersionCode) {
                eVar.contentRes = a((int) R.string.dialog_content_check_apk__higher_version_installed);
            }
        }
        if (!TextUtils.isEmpty(this.e.appName)) {
            eVar.titleRes = this.e.appName;
        } else {
            eVar.titleRes = a((int) R.string.dialog_title_check_apk_installed);
        }
        eVar.lBtnTxtRes = a((int) R.string.dialog_left_btn_txt_check_apk__has_installed);
        eVar.rBtnTxtRes = a((int) R.string.dialog_right_btn_txt_check_apk__has_installed);
        eVar.blockCaller = true;
        return eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public AppConst.DialogInfo a() {
        if (m.a().a("key_space_clean_has_run_clean", false)) {
            f fVar = new f(this);
            fVar.pageId = STConst.ST_DIALOG_SPACE_NOT_ENOUGH_DOWNLOAD_NOTHING;
            fVar.titleRes = a((int) R.string.mobile_rubbish_no_space_tip_title);
            fVar.contentRes = a((int) R.string.mobile_rubbish_no_space_tip_do_nothing);
            fVar.btnTxtRes = a((int) R.string.mobile_rubbish_no_space_tip_close);
            fVar.blockCaller = true;
            return fVar;
        }
        g gVar = new g(this);
        if (SpaceScanManager.a().i()) {
            gVar.contentRes = a((int) R.string.mobile_rubbish_no_space_tip_to_spaceclean);
            gVar.pageId = STConst.ST_DIALOG_SPACE_NOT_ENOUGH_DOWNLOAD_RUBBISH_CLEAR;
        } else {
            gVar.contentRes = a((int) R.string.mobile_rubbish_no_space_tip_to_apkmgr);
            gVar.pageId = STConst.ST_DIALOG_SPACE_NOT_ENOUGH_DOWNLOAD_APKMGR;
        }
        gVar.titleRes = a((int) R.string.mobile_rubbish_no_space_tip_title);
        gVar.rBtnTxtRes = a((int) R.string.mobile_rubbish_no_space_tip_right_button);
        gVar.lBtnTxtRes = a((int) R.string.mobile_rubbish_no_space_tip_left_button);
        gVar.blockCaller = true;
        return gVar;
    }

    private AppConst.TwoBtnDialogInfo a(FunctionUtils.InstallSpaceNotEnoughForwardPage installSpaceNotEnoughForwardPage) {
        h hVar = new h(this, installSpaceNotEnoughForwardPage);
        hVar.titleRes = a((int) R.string.dialog_title_check_space_no_enough);
        hVar.contentRes = a((int) R.string.dialog_content_check_space_no_enough_app_uninstall);
        hVar.lBtnTxtRes = a((int) R.string.dialog_left_btn_txt_check_space_no_enough);
        hVar.rBtnTxtRes = a((int) R.string.dialog_right_btn_txt_check_space_no_enough_app_uninstall);
        hVar.blockCaller = true;
        return hVar;
    }

    private o e() {
        i iVar = new i(this);
        iVar.b = this.e;
        iVar.titleRes = a((int) R.string.dialog_title_check_apk_broken);
        iVar.contentRes = a((int) R.string.dialog_content_check_apk_broken);
        iVar.lBtnTxtRes = a((int) R.string.dialog_left_btn_txt_check_apk_broken);
        iVar.rBtnTxtRes = a((int) R.string.dialog_right_btn_txt_check_apk_broken);
        iVar.blockCaller = true;
        return iVar;
    }

    private n f() {
        j jVar = new j(this);
        jVar.b = this.e;
        if (this.e == null || TextUtils.isEmpty(this.e.appName)) {
            jVar.titleRes = a((int) R.string.dialog_title_check_rom_unsupport);
        } else {
            jVar.titleRes = this.e.appName;
        }
        jVar.contentRes = a((int) R.string.dialog_content_check_rom_unsupport);
        jVar.btnTxtRes = a((int) R.string.dialog_one_btn_txt_check_rom_unsupport);
        jVar.blockCaller = true;
        return jVar;
    }

    private o g() {
        k kVar = new k(this);
        kVar.b = this.e;
        if (!TextUtils.isEmpty(this.e.appName)) {
            kVar.titleRes = this.e.appName;
        } else {
            kVar.titleRes = a((int) R.string.dialog_title_check_signature_diff);
        }
        kVar.contentRes = a((int) R.string.dialog_content_check_signature_diff);
        kVar.lBtnTxtRes = a((int) R.string.dialog_left_btn_txt_check_signature_diff);
        kVar.rBtnTxtRes = a((int) R.string.dialog_right_btn_txt_check_signature_diff);
        kVar.blockCaller = true;
        return kVar;
    }

    public void a(String str) {
        TemporaryThreadManager.get().start(new l(this, str));
    }

    public void b(String str) {
        TemporaryThreadManager.get().start(new c(this, str));
    }

    private String a(int i2) {
        return AstApp.i().getBaseContext().getString(i2);
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3, String str, int i4) {
        k.a(new STInfoV2(i2, str, i3, STConst.ST_DEFAULT_SLOT, i4));
    }
}
