package com.tencent.assistant.utils;

import android.util.Printer;

/* compiled from: ProGuard */
public class bi implements Printer {

    /* renamed from: a  reason: collision with root package name */
    private final int f2649a;
    private final String b;

    public bi(int i, String str) {
        this.f2649a = i;
        this.b = str;
    }

    public void println(String str) {
        switch (this.f2649a) {
            case 2:
                XLog.d(this.b, str);
                return;
            case 3:
                XLog.d(this.b, str);
                return;
            case 4:
                XLog.i(this.b, str);
                return;
            case 5:
                XLog.w(this.b, str);
                return;
            default:
                XLog.e(this.b, str);
                return;
        }
    }
}
