package com.tencent.assistant.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.link.b;
import com.tencent.assistant.sdk.SDKSupportService;
import com.tencent.assistant.utils.bg;

/* compiled from: ProGuard */
public class SDKRelatedReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String a2 = bg.a(intent, "command");
        if (!TextUtils.isEmpty(a2) && a2.equals("cmd_updatedownload")) {
            b.a(context, bg.a(intent, "relatedurl"));
            AstApp.i().startService(new Intent(AstApp.i(), SDKSupportService.class));
        }
    }
}
