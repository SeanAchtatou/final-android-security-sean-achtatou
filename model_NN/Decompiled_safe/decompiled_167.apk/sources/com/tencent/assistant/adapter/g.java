package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class g extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f797a;
    final /* synthetic */ LocalApkInfo b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ d d;

    g(d dVar, l lVar, LocalApkInfo localApkInfo, STInfoV2 sTInfoV2) {
        this.d = dVar;
        this.f797a = lVar;
        this.b = localApkInfo;
        this.c = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.f797a.b.setSelected(!this.f797a.b.isSelected());
        this.d.notifyDataSetChanged();
        this.b.mIsSelect = this.f797a.b.isSelected();
        if (this.d.f759a != null) {
            this.d.f759a.sendMessage(this.d.f759a.obtainMessage(110005, this.b));
        }
    }

    public STInfoV2 getStInfo() {
        this.c.actionId = 200;
        if (this.b.mIsSelect) {
            this.c.status = "02";
        } else {
            this.c.status = "03";
        }
        return this.c;
    }
}
