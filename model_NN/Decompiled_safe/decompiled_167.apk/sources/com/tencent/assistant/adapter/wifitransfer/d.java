package com.tencent.assistant.adapter.wifitransfer;

import com.tencent.assistant.component.txscrollview.TXImageView;

/* compiled from: ProGuard */
/* synthetic */ class d {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f816a = new int[TXImageView.TXImageViewType.values().length];

    static {
        try {
            f816a[TXImageView.TXImageViewType.UNKNOWN_IMAGE_TYPE.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f816a[TXImageView.TXImageViewType.LOCAL_AUDIO_COVER.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f816a[TXImageView.TXImageViewType.LOCAL_VIDEO_THUMBNAIL.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f816a[TXImageView.TXImageViewType.LOCAL_LARGER_IMAGE_THUMBNAIL.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f816a[TXImageView.TXImageViewType.UNINSTALL_APK_ICON.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
    }
}
