package com.tencent.assistant.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.cq;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.module.u;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class SearchMatchAdapter extends BaseAdapter implements UIEventListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static int f678a = 0;
    /* access modifiers changed from: private */
    public static int b = 1;
    /* access modifiers changed from: private */
    public Context c;
    private LayoutInflater d = null;
    private ArrayList<SimpleAppModel> e = null;
    private ArrayList<String> f = null;
    /* access modifiers changed from: private */
    public View.OnClickListener g = null;
    /* access modifiers changed from: private */
    public View h = null;
    /* access modifiers changed from: private */
    public String i = null;
    private b j = null;
    private int k = STConst.ST_PAGE_SEARCH_SUGGEST;

    public SearchMatchAdapter(Context context, View view, View.OnClickListener onClickListener) {
        this.c = context;
        this.d = LayoutInflater.from(context);
        this.g = onClickListener;
        this.h = view;
    }

    public void a(int i2) {
        this.k = i2;
    }

    public void a(ArrayList<SimpleAppModel> arrayList, ArrayList<String> arrayList2, String str) {
        this.i = str;
        if (arrayList != null) {
            if (this.e == null) {
                this.e = new ArrayList<>();
                this.e.addAll(arrayList);
            } else {
                this.e.clear();
                this.e.addAll(arrayList);
            }
        }
        if (arrayList2 != null) {
            if (this.f == null) {
                this.f = new ArrayList<>();
                this.f.addAll(arrayList2);
            } else {
                this.f.clear();
                this.f.addAll(arrayList2);
            }
        }
        notifyDataSetChanged();
    }

    public Object getItem(int i2) {
        if (this.e != null) {
            if (i2 < this.e.size()) {
                return this.e.get(i2);
            }
            if (!(this.f == null || this.f.size() == 0 || i2 - this.e.size() >= this.f.size())) {
                return this.f.get(i2 - this.e.size());
            }
        } else if (!(this.f == null || this.f.size() == 0 || i2 >= this.f.size())) {
            return this.f.get(i2);
        }
        return null;
    }

    public int getViewTypeCount() {
        return 2;
    }

    public int getItemViewType(int i2) {
        if (this.e == null || i2 >= this.e.size()) {
            return b;
        }
        return f678a;
    }

    public int getCount() {
        int i2 = 0;
        if (this.e != null) {
            i2 = 0 + this.e.size();
        }
        if (this.f != null) {
            return i2 + this.f.size();
        }
        return i2;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        dn dnVar;
        dl dlVar;
        if (f678a == getItemViewType(i2)) {
            if (view == null || ((dm) view.getTag()).f772a == null) {
                dlVar = new dl(this, null);
                view = this.d.inflate((int) R.layout.search_app_item, (ViewGroup) null);
                dlVar.c = (TXAppIconView) view.findViewById(R.id.app_icon_img);
                dlVar.d = (TextView) view.findViewById(R.id.app_name_txt);
                dlVar.e = (ListItemInfoView) view.findViewById(R.id.download_info);
                dlVar.f = (DownloadButton) view.findViewById(R.id.state_app_btn);
                dlVar.f771a = (ImageView) view.findViewById(R.id.sort_num_image);
                dlVar.b = (TextView) view.findViewById(R.id.text_sort);
                dlVar.g = (ImageView) view.findViewById(R.id.line);
                dm dmVar = new dm(this, null);
                dmVar.f772a = dlVar;
                view.setTag(dmVar);
            } else {
                dlVar = ((dm) view.getTag()).f772a;
            }
            dlVar.b.setVisibility(8);
            SimpleAppModel simpleAppModel = (SimpleAppModel) getItem(i2);
            a(dlVar, simpleAppModel, i2);
            if (i2 < getCount() - 1) {
                dlVar.g.setVisibility(0);
            } else {
                dlVar.g.setVisibility(8);
            }
            view.setTag(R.id.list_item_pos, Integer.valueOf(i2));
            view.setOnClickListener(new dh(this, simpleAppModel));
        } else {
            if (view == null || ((dm) view.getTag()).b == null) {
                view = this.d.inflate((int) R.layout.search_match_word, (ViewGroup) null);
                dnVar = new dn(this, null);
                dnVar.f773a = (TextView) view.findViewById(R.id.content);
                dnVar.b = (ImageView) view.findViewById(R.id.line);
                dm dmVar2 = new dm(this, null);
                dmVar2.b = dnVar;
                view.setTag(dmVar2);
            } else {
                dnVar = ((dm) view.getTag()).b;
            }
            view.setId(i2);
            a(dnVar, (String) getItem(i2), i2);
            if (i2 < getCount() - 1) {
                dnVar.b.setVisibility(0);
            } else {
                dnVar.b.setVisibility(8);
            }
            view.setTag(R.id.search_key_word, getItem(i2));
            view.setOnClickListener(new di(this));
        }
        return view;
    }

    private void a(dl dlVar, SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel != null && dlVar != null) {
            dlVar.d.setText(simpleAppModel.d);
            if (1 == (((int) (simpleAppModel.B >> 2)) & 3)) {
                try {
                    Drawable drawable = this.c.getResources().getDrawable(R.drawable.appdownload_icon_original);
                    drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
                    dlVar.d.setCompoundDrawablePadding(df.b(6.0f));
                    dlVar.d.setCompoundDrawables(null, null, drawable, null);
                } catch (Throwable th) {
                    cq.a().b();
                }
            } else {
                dlVar.d.setCompoundDrawables(null, null, null, null);
            }
            dlVar.e.a(simpleAppModel);
            dlVar.c.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            dlVar.f.a(simpleAppModel);
            dlVar.f.setTag(R.id.search_app_info, simpleAppModel);
            dlVar.f.setTag(R.id.search_key_word, simpleAppModel.d);
            if (s.a(simpleAppModel)) {
                dlVar.f.setClickable(false);
            } else {
                dlVar.f.setClickable(true);
                STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.c, 200);
                if (buildSTInfo != null) {
                    buildSTInfo.scene = this.k;
                    buildSTInfo.slotId = a.a("03", i2);
                    if (simpleAppModel != null) {
                        buildSTInfo.extraData = this.i + ";" + simpleAppModel.f1634a;
                    } else {
                        buildSTInfo.extraData = this.i;
                    }
                    buildSTInfo.updateStatus(simpleAppModel);
                    buildSTInfo.updateWithSimpleAppModel(simpleAppModel);
                }
                dlVar.f.a(buildSTInfo, new dj(this), (d) null, dlVar.f, dlVar.e);
            }
            c.b(this.c, simpleAppModel, dlVar.d, true);
            a(simpleAppModel, i2, null, 100, f678a);
        }
    }

    private void a(dn dnVar, String str, int i2) {
        dnVar.f773a.setText(str);
        a(null, i2, null, 100, b);
    }

    public void a() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().addUIEventListener(1016, this);
    }

    public void b() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().removeUIEventListener(1016, this);
    }

    public void handleUIEvent(Message message) {
        DownloadInfo downloadInfo;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE /*1009*/:
                if (message.obj instanceof DownloadInfo) {
                    DownloadInfo downloadInfo2 = (DownloadInfo) message.obj;
                    if (downloadInfo2 != null && !TextUtils.isEmpty(downloadInfo2.downloadTicket)) {
                        downloadInfo = downloadInfo2;
                    } else {
                        return;
                    }
                } else {
                    downloadInfo = null;
                }
                if (this.e != null) {
                    Iterator<SimpleAppModel> it = this.e.iterator();
                    while (it.hasNext()) {
                        SimpleAppModel next = it.next();
                        if (downloadInfo != null && next.q().equals(downloadInfo.downloadTicket)) {
                            f();
                        }
                    }
                    return;
                }
                return;
            case 1016:
                u.g(this.e);
                f();
                return;
            default:
                return;
        }
    }

    private void f() {
        ba.a().post(new dk(this));
    }

    public void c() {
        if (this.f != null) {
            this.f.clear();
        }
        if (this.e != null) {
            this.e.clear();
        }
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel, int i2, String str, int i3, int i4) {
        STInfoV2 buildSTInfo;
        if ((this.c instanceof BaseActivity) && (buildSTInfo = STInfoBuilder.buildSTInfo(this.c, i3)) != null) {
            buildSTInfo.scene = this.k;
            if (i4 == f678a) {
                buildSTInfo.slotId = a.a("03", i2);
                if (simpleAppModel != null) {
                    buildSTInfo.extraData = this.i + ";" + simpleAppModel.f1634a;
                } else {
                    buildSTInfo.extraData = this.i;
                }
                buildSTInfo.updateWithSimpleAppModel(simpleAppModel);
            } else {
                buildSTInfo.slotId = a.a("04", i2);
                buildSTInfo.extraData = this.i;
            }
            if (str != null) {
                buildSTInfo.status = str;
            }
            if (i3 == 100) {
                if (this.j == null) {
                    this.j = new b();
                }
                this.j.exposure(buildSTInfo);
                return;
            }
            k.a(buildSTInfo);
        }
    }
}
