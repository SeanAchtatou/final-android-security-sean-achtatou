package com.tencent.assistant.adapter;

import android.view.View;
import android.widget.ImageView;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistantv2.component.y;

/* compiled from: ProGuard */
class dj extends y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchMatchAdapter f769a;

    dj(SearchMatchAdapter searchMatchAdapter) {
        this.f769a = searchMatchAdapter;
    }

    public void a(DownloadInfo downloadInfo) {
        a.a().a(downloadInfo);
        com.tencent.assistant.utils.a.a((ImageView) this.f769a.h.findViewWithTag(downloadInfo.downloadTicket));
    }

    public void a(View view, AppConst.AppState appState) {
        if (this.f769a.g != null) {
            this.f769a.g.onClick(view);
        }
    }
}
