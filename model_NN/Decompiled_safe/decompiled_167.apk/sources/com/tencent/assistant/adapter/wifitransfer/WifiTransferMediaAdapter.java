package com.tencent.assistant.adapter.wifitransfer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.tencent.assistant.localres.LocalMediaLoader;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: ProGuard */
public abstract class WifiTransferMediaAdapter<T> extends BaseAdapter implements LocalMediaLoader.ILocalMediaLoaderListener<T> {

    /* renamed from: a  reason: collision with root package name */
    protected LocalMediaLoader<T> f812a;
    protected ArrayList<T> b = null;
    protected boolean c = true;
    protected Context d = null;
    private j e = null;
    private HashMap<Integer, WifiTransferMediaAdapter<T>.k> f;

    /* access modifiers changed from: protected */
    public abstract void a(View view, Object obj, int i);

    /* access modifiers changed from: protected */
    public abstract View e();

    public WifiTransferMediaAdapter(Context context) {
        this.d = context;
        this.b = new ArrayList<>();
        this.f = new HashMap<>();
    }

    public void f() {
        if (this.f812a != null) {
            this.f812a.getLocalMediaList();
        }
    }

    public void a(boolean z) {
        this.c = z;
        f();
    }

    public boolean d(int i) {
        k kVar = this.f.get(Integer.valueOf(i));
        if (kVar == null) {
            return false;
        }
        if (kVar.f820a == 0) {
            return false;
        }
        return true;
    }

    public void e(int i) {
        if (i >= 0 && i < this.b.size()) {
            this.b.remove(this.b.get(i));
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        return this.b.size();
    }

    public Object getItem(int i) {
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = e();
        }
        T t = this.b.get(i);
        i iVar = (i) view.getTag();
        if (iVar != null) {
            if (d(i)) {
                iVar.c.setVisibility(0);
            } else {
                iVar.c.setVisibility(8);
            }
        }
        a(view, t, i);
        return view;
    }

    public void onLocalMediaLoaderFinish(LocalMediaLoader<T> localMediaLoader, ArrayList<T> arrayList, boolean z) {
        a(arrayList, z);
        if (this.e != null && this.c) {
            this.e.a(this, getCount());
        }
    }

    public void onLocalMediaLoaderOver() {
        if (this.e != null) {
            this.e.a();
        }
    }

    public synchronized void a(ArrayList<T> arrayList, boolean z) {
        if (arrayList != null) {
            if (arrayList.size() > 0) {
                if (z) {
                    this.b.clear();
                }
                this.b.addAll(arrayList);
                if (this.c) {
                    notifyDataSetChanged();
                }
            }
        }
    }

    public void a(j jVar) {
        this.e = jVar;
    }
}
