package com.tencent.assistant.adapter;

import com.tencent.assistant.download.DownloadInfoWrapper;
import java.util.Comparator;

/* compiled from: ProGuard */
public class bt implements Comparator<DownloadInfoWrapper> {
    /* renamed from: a */
    public int compare(DownloadInfoWrapper downloadInfoWrapper, DownloadInfoWrapper downloadInfoWrapper2) {
        return (int) (downloadInfoWrapper2.a() - downloadInfoWrapper.a());
    }
}
