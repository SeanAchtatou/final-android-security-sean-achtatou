package com.tencent.assistant.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.adapter.a.a;
import com.tencent.assistant.adapter.a.d;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.e;
import com.tencent.assistant.module.callback.w;
import com.tencent.assistant.module.en;
import com.tencent.assistant.module.u;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.adapter.smartlist.SmartItemType;
import com.tencent.assistantv2.adapter.smartlist.ab;
import com.tencent.assistantv2.adapter.smartlist.ac;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class SpecailTopicDetailAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private static int f679a = 0;
    private static int b = (f679a + 1);
    private static int c = (f679a + 1);
    private static int d = (c + 1);
    private Context e;
    private LayoutInflater f;
    private AppGroupInfo g;
    private ArrayList<SimpleAppModel> h = new ArrayList<>();
    /* access modifiers changed from: private */
    public int i = 0;
    private int j = 0;
    private View k;
    private ListView l;
    private w m;
    private IViewInvalidater n;
    private d o = new dw(this);
    private b p = null;
    private String q = "03";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.adapter.SpecailTopicDetailAdapter.a(boolean, java.util.List<com.tencent.assistant.model.SimpleAppModel>):void
     arg types: [int, java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>]
     candidates:
      com.tencent.assistant.adapter.SpecailTopicDetailAdapter.a(com.tencent.assistant.model.SimpleAppModel, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.adapter.SpecailTopicDetailAdapter.a(boolean, java.util.List<com.tencent.assistant.model.SimpleAppModel>):void */
    public SpecailTopicDetailAdapter(Context context, View view, AppGroupInfo appGroupInfo, ArrayList<SimpleAppModel> arrayList) {
        this.e = context;
        this.f = LayoutInflater.from(context);
        this.g = appGroupInfo;
        a(true, (List<SimpleAppModel>) arrayList);
        this.k = view;
        if (this.k instanceof TXGetMoreListView) {
            this.l = ((TXGetMoreListView) this.k).getListView();
        }
        this.m = new a(context, this.l, this.o);
    }

    public void a(AppGroupInfo appGroupInfo) {
        this.g = appGroupInfo;
    }

    public void a(int i2) {
        this.i = i2;
    }

    public void b(int i2) {
        this.j = i2;
    }

    public void a(IViewInvalidater iViewInvalidater) {
        this.n = iViewInvalidater;
    }

    public void a(boolean z, List<SimpleAppModel> list) {
        if (list != null && list.size() != 0) {
            if (z) {
                this.h.clear();
            }
            this.h.addAll(list);
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        return this.h.size() + 1;
    }

    public int a() {
        if (this.h != null) {
            return this.h.size();
        }
        return 0;
    }

    public Object getItem(int i2) {
        if (i2 == 0) {
            return this.g;
        }
        if (this.h.size() <= 0 || i2 <= 0) {
            return null;
        }
        return this.h.get(i2 - 1);
    }

    public void b() {
        en.a().unregister(this.m);
    }

    public void c() {
        en.a().register(this.m);
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (f679a == getItemViewType(i2)) {
            View inflate = this.f.inflate((int) R.layout.item_special_topic_detail_header, (ViewGroup) null);
            TXImageView tXImageView = (TXImageView) inflate.findViewById(R.id.topic_detail_pic);
            TextView textView = (TextView) inflate.findViewById(R.id.topic_detail_introduce_title);
            tXImageView.setLayoutParams(new LinearLayout.LayoutParams(-1, (df.b() * 270) / 690));
            if (this.g != null && !TextUtils.isEmpty(this.g.d)) {
                tXImageView.updateImageView(this.g.d, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
            }
            textView.setVisibility(this.j == 1 ? 8 : 0);
            TextView textView2 = (TextView) inflate.findViewById(this.j == 1 ? R.id.introduce_txt_2 : R.id.introduce_txt);
            textView2.setVisibility(0);
            if (this.g != null && !TextUtils.isEmpty(this.g.c)) {
                textView2.setText(this.g.c);
            }
            return inflate;
        } else if (b != getItemViewType(i2)) {
            return null;
        } else {
            SimpleAppModel simpleAppModel = this.h.get(i2 - 1);
            ab a2 = new ab().a(this.l);
            SmartItemType smartItemType = SmartItemType.NORMAL;
            if (TextUtils.isEmpty(simpleAppModel.X)) {
                smartItemType = SmartItemType.NORMAL_NO_REASON;
            }
            e eVar = new e();
            eVar.b = 1;
            eVar.c = simpleAppModel;
            a2.a(a(simpleAppModel, i2));
            return ac.a(this.e, a2, view, smartItemType, i2, eVar, this.n);
        }
    }

    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return f679a;
        }
        Object item = getItem(i2);
        if (!(item instanceof SimpleAppModel) || !TextUtils.isEmpty(((SimpleAppModel) item).X)) {
            return b;
        }
        return c;
    }

    public int getViewTypeCount() {
        return d;
    }

    private STInfoV2 a(SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel == null) {
            return null;
        }
        if (this.p == null) {
            this.p = new b();
        }
        AppConst.AppState d2 = u.d(simpleAppModel);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e, simpleAppModel, c(i2), 100, com.tencent.assistantv2.st.page.a.a(d2, simpleAppModel));
        if (buildSTInfo != null) {
            buildSTInfo.updateContentId(STCommonInfo.ContentIdType.SPECIAL, String.valueOf(this.i));
        }
        this.p.exposure(buildSTInfo);
        return buildSTInfo;
    }

    private String c(int i2) {
        return com.tencent.assistantv2.st.page.a.a(this.q, i2);
    }
}
