package com.tencent.assistant.adapter;

import android.view.View;

/* compiled from: ProGuard */
class di implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchMatchAdapter f768a;

    di(SearchMatchAdapter searchMatchAdapter) {
        this.f768a = searchMatchAdapter;
    }

    public void onClick(View view) {
        if (this.f768a.g != null) {
            this.f768a.g.onClick(view);
        }
        int i = 0;
        try {
            i = view.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.f768a.a(null, i, null, 200, SearchMatchAdapter.b);
    }
}
