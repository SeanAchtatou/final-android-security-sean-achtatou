package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class r implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppCategoryListAdapter f807a;

    r(AppCategoryListAdapter appCategoryListAdapter) {
        this.f807a = appCategoryListAdapter;
    }

    public void onClick(View view) {
        int i;
        try {
            i = ((Integer) view.getTag(R.id.category_pos)).intValue();
        } catch (Exception e) {
            e.printStackTrace();
            i = 0;
        }
        this.f807a.a((s) view.getTag(R.id.category_data), i / 20, i % 20, 200);
        if (this.f807a.z != null) {
            this.f807a.z.onClick(view);
        }
    }
}
