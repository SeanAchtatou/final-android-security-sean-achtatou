package com.tencent.assistant.adapter;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.model.SimpleAppModel;

/* compiled from: ProGuard */
class da implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SimpleAppModel f760a;
    final /* synthetic */ cw b;

    da(cw cwVar, SimpleAppModel simpleAppModel) {
        this.b = cwVar;
        this.f760a = simpleAppModel;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.b.c, BrowserActivity.class);
        intent.putExtra("com.tencent.assistant.BROWSER_URL", this.f760a.Z);
        this.b.c.startActivity(intent);
    }
}
