package com.tencent.assistant.adapter;

import android.view.View;
import com.tencent.assistant.component.SwitchButton;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class dq extends du {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f776a;
    final /* synthetic */ int b;
    final /* synthetic */ int c;
    final /* synthetic */ Cdo d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    dq(Cdo doVar, int i, int i2, int i3) {
        super(doVar, null);
        this.d = doVar;
        this.f776a = i;
        this.b = i2;
        this.c = i3;
    }

    public void onSwitchButtonClick(View view, boolean z) {
        this.d.a(this.f776a, view, this.b, z);
    }

    public STInfoV2 getStInfo(View view) {
        if (!(view instanceof SwitchButton)) {
            return null;
        }
        return this.d.a(this.d.d(this.c, this.b), this.d.b(!((SwitchButton) view).getSwitchState()), 200);
    }
}
