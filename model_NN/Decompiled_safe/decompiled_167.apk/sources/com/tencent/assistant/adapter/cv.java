package com.tencent.assistant.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.component.RatingView;
import com.tencent.assistantv2.component.DownloadButton;

/* compiled from: ProGuard */
class cv {

    /* renamed from: a  reason: collision with root package name */
    View f754a;
    ImageView b;
    TextView c;
    AppIconView d;
    TextView e;
    RatingView f;
    TextView g;
    DownloadButton h;
    View i;
    TextView j;
    TextView k;
    TextView l;
    ImageView m;
    final /* synthetic */ RankClassicListAdapter n;

    private cv(RankClassicListAdapter rankClassicListAdapter) {
        this.n = rankClassicListAdapter;
    }

    /* synthetic */ cv(RankClassicListAdapter rankClassicListAdapter, cn cnVar) {
        this(rankClassicListAdapter);
    }
}
