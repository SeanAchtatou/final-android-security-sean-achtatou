package com.tencent.assistant.adapter;

import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.qq.AppService.AstApp;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.b;
import com.tencent.assistant.module.u;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.adapter.smartlist.SmartItemType;
import com.tencent.assistantv2.adapter.smartlist.ab;
import com.tencent.assistantv2.adapter.smartlist.ac;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* compiled from: ProGuard */
public class AppAdapter extends BaseAdapter implements UIEventListener {
    private static int c = 0;
    private static int d = (c + 1);
    private static int e = (d + 1);
    private static int f = (e + 1);

    /* renamed from: a  reason: collision with root package name */
    public String f663a;
    public String b;
    private ListType g;
    private boolean h;
    private AstApp i;
    private Context j;
    private LayoutInflater k;
    private List<SimpleAppModel> l;
    private List<SimpleAppModel> m;
    private View n;
    private int o;
    private long p;
    private long q;
    private b r;
    private boolean s;
    private com.tencent.assistantv2.st.b.b t;

    /* compiled from: ProGuard */
    public enum ListType {
        LISTTYPENORMAL,
        LISTTYPEGAMESORT
    }

    public AppAdapter(Context context, View view, b bVar) {
        this(context, view, bVar, false);
    }

    public AppAdapter(Context context, View view, b bVar, boolean z) {
        this.g = ListType.LISTTYPENORMAL;
        this.h = false;
        this.l = new ArrayList();
        this.m = null;
        this.o = 2000;
        this.p = -100;
        this.q = 0;
        this.r = null;
        this.f663a = Constants.STR_EMPTY;
        this.b = Constants.STR_EMPTY;
        this.s = false;
        this.t = null;
        this.s = z;
        this.i = AstApp.i();
        this.j = context;
        if (!(bVar == null || bVar.c() == null)) {
            this.l.addAll(bVar.c());
            notifyDataSetChanged();
        }
        this.k = LayoutInflater.from(context);
        this.n = view;
        this.r = bVar;
        if (this.s) {
            this.m = new ArrayList(5);
        }
    }

    public void a(boolean z, List<SimpleAppModel> list) {
        if (list != null) {
            if (z) {
                this.l.clear();
            }
            this.l.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void a(boolean z, List<SimpleAppModel> list, boolean z2) {
        if (list != null) {
            if (z) {
                this.l.clear();
            }
            if (this.s) {
                Pair<List<SimpleAppModel>, List<SimpleAppModel>> a2 = a(list);
                this.l.addAll((Collection) a2.first);
                this.m.addAll((Collection) a2.second);
            } else {
                this.l.addAll(list);
            }
            if (!z2) {
                this.l.addAll(this.m);
            }
            notifyDataSetChanged();
        }
    }

    private Pair<List<SimpleAppModel>, List<SimpleAppModel>> a(List<SimpleAppModel> list) {
        ArrayList arrayList = new ArrayList(list.size());
        ArrayList arrayList2 = new ArrayList(4);
        for (SimpleAppModel next : list) {
            if (!e.a(next.c, next.g)) {
                arrayList.add(next);
            } else {
                arrayList2.add(next);
            }
        }
        return Pair.create(arrayList, arrayList2);
    }

    public void a(int i2, long j2, String str) {
        this.o = i2;
        this.p = j2;
        this.f663a = str;
    }

    public int getCount() {
        if (this.l == null) {
            return 0;
        }
        return this.l.size();
    }

    /* renamed from: a */
    public SimpleAppModel getItem(int i2) {
        if (this.l == null) {
            return null;
        }
        return this.l.get(i2);
    }

    public View getView(int i2, View view, ViewGroup viewGroup) {
        SimpleAppModel simpleAppModel;
        if (this.l == null || i2 >= this.l.size()) {
            simpleAppModel = null;
        } else {
            simpleAppModel = this.l.get(i2);
        }
        ab abVar = new ab();
        SmartItemType smartItemType = SmartItemType.NORMAL;
        int itemViewType = getItemViewType(i2);
        if (c == itemViewType) {
            smartItemType = SmartItemType.NORMAL;
        } else if (d == itemViewType) {
            smartItemType = SmartItemType.COMPETITIVE;
        } else if (e == itemViewType) {
            smartItemType = SmartItemType.NORMAL_NO_REASON;
        }
        com.tencent.assistant.model.e eVar = new com.tencent.assistant.model.e();
        eVar.b = 1;
        eVar.c = simpleAppModel;
        abVar.a(a(simpleAppModel, i2));
        return ac.a(this.j, abVar, view, smartItemType, i2, eVar, null);
    }

    public int getItemViewType(int i2) {
        SimpleAppModel simpleAppModel = this.l.get(i2);
        if (!b()) {
            if (TextUtils.isEmpty(simpleAppModel.X)) {
                return e;
            }
            return c;
        } else if (SimpleAppModel.CARD_TYPE.NORMAL == simpleAppModel.U) {
            if (TextUtils.isEmpty(simpleAppModel.X)) {
                return e;
            }
            return c;
        } else if (SimpleAppModel.CARD_TYPE.QUALITY == simpleAppModel.U) {
            return d;
        } else {
            return c;
        }
    }

    public int getViewTypeCount() {
        return f;
    }

    public void handleUIEvent(Message message) {
        DownloadInfo downloadInfo;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE /*1009*/:
                if (message.obj instanceof DownloadInfo) {
                    downloadInfo = (DownloadInfo) message.obj;
                } else {
                    downloadInfo = null;
                }
                if (downloadInfo != null && !TextUtils.isEmpty(downloadInfo.downloadTicket)) {
                    for (SimpleAppModel q2 : this.l) {
                        if (downloadInfo.downloadTicket.equals(q2.q())) {
                            a();
                            return;
                        }
                    }
                    return;
                }
                return;
            case 1016:
                u.g(this.l);
                a();
                return;
            case EventDispatcherEnum.UI_EVENT_DOWNLOADLIST_READY /*1038*/:
                a();
                return;
            default:
                return;
        }
    }

    private void a() {
        ba.a().post(new o(this));
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private boolean b() {
        if ((!c.d() || c.k()) && m.a().p()) {
            return false;
        }
        return true;
    }

    private STInfoV2 a(SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel == null) {
            return null;
        }
        if (this.t == null) {
            this.t = new com.tencent.assistantv2.st.b.b();
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.j, simpleAppModel, a.a(this.f663a, i2), 100, a.a(u.d(simpleAppModel), simpleAppModel));
        if (buildSTInfo != null) {
            buildSTInfo.contentId = this.b;
        }
        this.t.exposure(buildSTInfo);
        return buildSTInfo;
    }
}
