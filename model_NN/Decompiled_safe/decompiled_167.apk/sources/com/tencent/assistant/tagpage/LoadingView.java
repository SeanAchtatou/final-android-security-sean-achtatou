package com.tencent.assistant.tagpage;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
public class LoadingView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private TextView f2540a = null;
    /* access modifiers changed from: private */
    public RoundProgressBar b = null;
    /* access modifiers changed from: private */
    public int c = 0;
    private boolean d = false;

    static /* synthetic */ int a(LoadingView loadingView, int i) {
        int i2 = loadingView.c + i;
        loadingView.c = i2;
        return i2;
    }

    public LoadingView(Context context) {
        super(context);
        a(context);
    }

    public LoadingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public LoadingView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context);
    }

    private void a(Context context) {
        LayoutInflater.from(context).inflate((int) R.layout.tag_page_loading_layout, this);
        this.f2540a = (TextView) findViewById(R.id.loading_info);
        this.b = (RoundProgressBar) findViewById(R.id.progress_bar);
        this.d = false;
    }

    public void a(boolean z) {
        if (this.f2540a != null) {
            this.f2540a.setVisibility(z ? 0 : 8);
        }
    }

    public void a() {
        this.d = true;
        TemporaryThreadManager.get().start(new f(this));
    }

    public void b() {
        this.d = false;
        if (this.c < 100) {
            TemporaryThreadManager.get().start(new g(this));
        }
    }

    public void c() {
        this.c = 0;
    }
}
