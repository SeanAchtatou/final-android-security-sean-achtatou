package com.tencent.assistant.tagpage;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.appdetail.process.s;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.net.c;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.df;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.ListRecommendReasonView;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.k;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class TagPageCardAdapter extends BaseAdapter implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, SurfaceHolder.Callback, TXImageView.ITXImageViewListener, UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    public static int f2542a = -1;
    /* access modifiers changed from: private */
    public Context b = null;
    private ArrayList<SimpleAppModel> c = new ArrayList<>();
    private b d = null;
    private int e = STConst.ST_TAG_DETAIL_PAGE;
    private String f = Constants.STR_EMPTY;
    private boolean g = false;
    private u h = null;
    private String i = "http://122.228.215.139/music.qqvideo.tc.qq.com/h00162vyzpv.mp4?type=mp4&fmt=mp4&vkey=7A8C0BACEA0A87D0DFB5C43961C7ADA28AD53421B18835AA9F734CE82944AE4D073470DB1A028516AE58EDB34DFCDFB5C311B6970F1AF0A5&locid=2a8d303d-51ca-48bf-a2d7-0ff12245c4c9&size=16765963&ocid=313139116";
    private String j = "http://122.228.215.144/music.qqvideo.tc.qq.com/z0016sgwevh.mp4?type=mp4&fmt=mp4&vkey=EB89AF8F05998E5E99BD9A7765064C7F4CE78F0658BD07DC327BB37D0800B4CAC0DA5D90911455B19597774F6F8BE2C3584733E9A4675B1A&locid=4e255534-12f9-481c-890f-040290b6bb5e&size=26665772&ocid=212475820";
    /* access modifiers changed from: private */
    public MediaPlayer k = null;
    /* access modifiers changed from: private */
    public r l = null;
    private boolean m = true;
    private TextureView n = null;
    private SurfaceView o = null;
    private s p = null;
    private int q = 0;
    private int r = 0;
    /* access modifiers changed from: private */
    public int s = 0;
    /* access modifiers changed from: private */
    public boolean t = false;

    public TagPageCardAdapter(Context context, ArrayList<SimpleAppModel> arrayList) {
        this.b = context;
        if (arrayList != null) {
            this.c = arrayList;
        }
    }

    public void a(ArrayList<SimpleAppModel> arrayList) {
        if (arrayList != null) {
            this.c = arrayList;
        }
    }

    public int getCount() {
        if (this.c != null) {
            return this.c.size();
        }
        return 0;
    }

    public Object getItem(int i2) {
        if (this.c == null || i2 >= this.c.size()) {
            return null;
        }
        return this.c.get(i2);
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        r rVar;
        if (view == null) {
            rVar = new r();
            view = LayoutInflater.from(this.b).inflate((int) R.layout.tap_page_list_item_v2, (ViewGroup) null, false);
            rVar.j = (TextView) view.findViewById(R.id.download_rate_desc);
            rVar.k = (TXAppIconView) view.findViewById(R.id.app_icon_img);
            rVar.l = (TextView) view.findViewById(R.id.title);
            rVar.m = (DownloadButton) view.findViewById(R.id.state_app_btn);
            rVar.n = (ListItemInfoView) view.findViewById(R.id.download_info);
            rVar.o = (TextView) view.findViewById(R.id.desc);
            rVar.p = (ListRecommendReasonView) view.findViewById(R.id.reasonView);
            rVar.p.setVisibility(8);
            rVar.u = (TextView) view.findViewById(R.id.sort_text);
            rVar.f2562a = (TagPageThumbnailView) view.findViewById(R.id.snap_shot_pics);
            rVar.b = (RelativeLayout) view.findViewById(R.id.id_rl_video_layout);
            rVar.c = (RelativeLayout) view.findViewById(R.id.id_rl_video_container);
            rVar.d = (RelativeLayout) view.findViewById(R.id.id_rl_play_icon_layout);
            rVar.e = (ImageView) view.findViewById(R.id.iv_video_play);
            rVar.f = (LoadingView) view.findViewById(R.id.loading);
            rVar.f.a(false);
            rVar.g = (TXImageView) view.findViewById(R.id.iv_video_snapshot);
            rVar.h = (TextView) view.findViewById(R.id.tv_video_src);
            rVar.i = (ImageView) view.findViewById(R.id.iv_play_finish_anim);
            rVar.i.setVisibility(8);
            view.setTag(rVar);
        } else {
            rVar = (r) view.getTag();
        }
        rVar.c.setTag(Integer.valueOf(i2));
        if (TagPageActivity.n != i2) {
            rVar.c.setVisibility(8);
            rVar.f.setVisibility(8);
            rVar.d.setVisibility(0);
            rVar.g.setVisibility(0);
            if (Build.VERSION.SDK_INT >= 16) {
                rVar.g.setImageAlpha(255);
            } else {
                rVar.g.setAlpha(255);
            }
            rVar.g.clearAnimation();
        }
        SimpleAppModel simpleAppModel = (SimpleAppModel) getItem(i2);
        a(rVar, simpleAppModel, i2, a(simpleAppModel, i2));
        view.setOnClickListener(new j(this, simpleAppModel, i2));
        return view;
    }

    private void a(r rVar, SimpleAppModel simpleAppModel, int i2, STInfoV2 sTInfoV2) {
        boolean z;
        if (rVar != null && simpleAppModel != null) {
            if (c.d()) {
                rVar.e.setVisibility(8);
            } else {
                rVar.e.setVisibility(0);
            }
            rVar.k.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            rVar.k.setTag(Integer.valueOf(i2));
            if (!this.g && i2 == 0) {
                this.g = true;
                rVar.k.setListener(this);
            }
            rVar.l.setText(simpleAppModel.d);
            rVar.n.a(simpleAppModel);
            if (i2 == 0 && this.m && c.d()) {
                this.m = false;
                a(rVar, i2);
                TagPageActivity.n = i2;
            }
            if (!TextUtils.isEmpty(simpleAppModel.aF)) {
                XLog.i("TagPageCradAdapter", "position = " + i2 + ", appInfo.miniVideoUrl = " + simpleAppModel.aF + ", appid = " + simpleAppModel.f1634a);
                rVar.b.setVisibility(0);
                rVar.f2562a.setVisibility(8);
                rVar.g.updateImageView(simpleAppModel.aH, R.drawable.tag_page_video_default_bg, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
                a(rVar, simpleAppModel.aG);
                rVar.g.setOnClickListener(new k(this, rVar, i2));
                rVar.d.setOnClickListener(new l(this, rVar, i2));
                rVar.c.setOnClickListener(new m(this, rVar, i2));
                if (!TextUtils.isEmpty(simpleAppModel.aJ)) {
                    rVar.h.setText("视频来自  " + simpleAppModel.aJ);
                    rVar.h.setVisibility(0);
                    z = false;
                } else {
                    rVar.h.setVisibility(8);
                    z = false;
                }
            } else {
                rVar.b.setVisibility(8);
                rVar.h.setVisibility(8);
                if (simpleAppModel.an == null || simpleAppModel.an.size() < 2) {
                    rVar.f2562a.setVisibility(8);
                    z = false;
                } else {
                    rVar.f2562a.a(simpleAppModel, i2);
                    rVar.f2562a.setVisibility(0);
                    rVar.f2562a.requestLayout();
                    z = true;
                }
            }
            if (!TextUtils.isEmpty(simpleAppModel.ay)) {
                if (simpleAppModel.az != null) {
                    rVar.o.setClickable(false);
                } else {
                    rVar.o.setClickable(false);
                }
                rVar.o.setSingleLine(false);
                rVar.o.setMaxLines(3);
                int a2 = df.a(this.b, 8.0f);
                rVar.o.setPadding(a2, a2, a2, a2);
                rVar.o.setLineSpacing((float) df.a(this.b, 3.0f), 1.2f);
                rVar.o.setBackgroundResource(R.drawable.tag_page_media_recommend_bg);
                try {
                    rVar.o.setText(Html.fromHtml(simpleAppModel.ay));
                    rVar.o.setVisibility(0);
                } catch (Exception e2) {
                    rVar.o.setVisibility(8);
                }
                int a3 = df.a(this.b, 6.0f);
                rVar.f2562a.setPadding(df.a(this.b, 1.0f), a3, df.a(this.b, 10.0f), a3);
                rVar.b.setPadding(df.a(this.b, 1.0f), df.a(this.b, 12.0f), df.a(this.b, 10.0f), df.a(this.b, 8.0f));
            } else if (!TextUtils.isEmpty(simpleAppModel.X)) {
                rVar.o.setClickable(false);
                rVar.o.setSingleLine(false);
                rVar.o.setMaxLines(2);
                if (z) {
                    rVar.o.setPadding(0, 0, 0, 0);
                } else {
                    rVar.o.setPadding(0, 0, 0, df.a(this.b, 10.0f));
                }
                rVar.o.setLineSpacing((float) df.a(this.b, 1.0f), 1.0f);
                rVar.o.setBackgroundResource(R.drawable.tag_page_editor_recommend_bg);
                rVar.o.setText(simpleAppModel.X);
                rVar.o.setVisibility(0);
                rVar.f2562a.setPadding(df.a(this.b, 1.0f), df.a(this.b, 5.0f), df.a(this.b, 10.0f), df.a(this.b, 6.0f));
                rVar.b.setPadding(df.a(this.b, 1.0f), 0, df.a(this.b, 10.0f), df.a(this.b, 8.0f));
            } else {
                rVar.o.setVisibility(8);
            }
            rVar.m.a(simpleAppModel);
            if (s.a(simpleAppModel)) {
                rVar.m.setClickable(false);
                return;
            }
            rVar.m.setClickable(true);
            rVar.m.a(sTInfoV2, new n(this), (d) null, rVar.m, rVar.n);
        }
    }

    /* access modifiers changed from: private */
    public String a(int i2) {
        return "07_" + ct.a(i2 + 1);
    }

    public void handleUIEvent(Message message) {
    }

    public void a(String str, String str2, int i2) {
        XLog.i("TagPageActivity", "[logReport] ---> actionId = " + i2 + ", slotId = " + str + ", extraData = " + str2);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b, i2);
        buildSTInfo.slotId = str;
        buildSTInfo.extraData = str2;
        k.a(buildSTInfo);
    }

    public void onTXImageViewLoadImageFinish(TXImageView tXImageView, Bitmap bitmap) {
        int i2;
        if (this.h != null && tXImageView != null) {
            Object tag = tXImageView.getTag();
            if (tag instanceof Integer) {
                i2 = ((Integer) tag).intValue();
            } else {
                i2 = -1;
            }
            this.h.a(tXImageView, bitmap, i2);
        }
    }

    public void a(u uVar) {
        this.h = uVar;
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        XLog.i("TagPageCradAdapter", "*** surfaceCreated ***");
        if (this.k == null) {
            this.k = new MediaPlayer();
        }
        TemporaryThreadManager.get().start(new o(this, surfaceHolder));
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        XLog.i("TagPageCradAdapter", "*** surfaceChanged ***");
        if (this.l != null) {
            this.l.f.b();
        }
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        XLog.i("TagPageCradAdapter", "### surfaceDestroyed ###");
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        RelativeLayout.LayoutParams layoutParams;
        int videoWidth = mediaPlayer.getVideoWidth();
        int videoHeight = mediaPlayer.getVideoHeight();
        XLog.i("TagPageCradAdapter", "[onPrepared] ---> videoWidth = " + videoWidth + ", videoHeight = " + videoHeight);
        if (videoHeight != 0 && videoWidth != 0) {
            SimpleAppModel simpleAppModel = (SimpleAppModel) getItem(f2542a);
            String str = null;
            if (simpleAppModel != null) {
                str = simpleAppModel.aG;
            }
            if (Build.VERSION.SDK_INT >= 14) {
                layoutParams = (RelativeLayout.LayoutParams) this.n.getLayoutParams();
                if (layoutParams != null) {
                    if (1 == a(str)) {
                        layoutParams.width = df.a(this.b, 230.0f);
                        layoutParams.height = df.a(this.b, 130.0f);
                    } else {
                        layoutParams.width = df.a(this.b, 130.0f);
                        layoutParams.height = df.a(this.b, 230.0f);
                    }
                }
                if (this.n != null) {
                    this.n.setLayoutParams(layoutParams);
                }
            } else {
                layoutParams = (RelativeLayout.LayoutParams) this.o.getLayoutParams();
                if (1 == a(str)) {
                    layoutParams.width = df.a(this.b, 230.0f);
                    layoutParams.height = df.a(this.b, 130.0f);
                } else {
                    layoutParams.width = df.a(this.b, 130.0f);
                    layoutParams.height = df.a(this.b, 230.0f);
                }
                if (this.o != null) {
                    this.o.setLayoutParams(layoutParams);
                }
            }
            mediaPlayer.start();
            if (layoutParams != null) {
                XLog.i("TagPageCradAdapter", "[onPrepared] --> mVideoWidth = " + layoutParams.width + ", mVideoHeight = " + layoutParams.height);
            }
            if (this.l != null) {
                ba.a().postDelayed(new p(this), 1100);
                this.l.c.setVisibility(0);
                this.l.d.setVisibility(8);
            }
        }
    }

    public void a(r rVar, int i2) {
        if (rVar != null && i2 >= 0) {
            try {
                SimpleAppModel simpleAppModel = (SimpleAppModel) getItem(i2);
                if (simpleAppModel == null) {
                    return;
                }
                if (simpleAppModel == null || !TextUtils.isEmpty(simpleAppModel.aF)) {
                    XLog.i("TagPageCradAdapter", "[addSurfaceView] ---> position = " + i2 + ", (Integer)viewHolder.rlContainer.getTag() = " + ((Integer) rVar.c.getTag()));
                    if (i2 == ((Integer) rVar.c.getTag()).intValue()) {
                        if (f2542a != i2) {
                            this.s = 0;
                        }
                        f2542a = i2;
                        if (this.l != null) {
                            this.l.f.setVisibility(8);
                            this.l.d.setVisibility(0);
                            this.l.i.clearAnimation();
                            this.l.i.setVisibility(8);
                            if (this.k == null || !this.k.isPlaying()) {
                                this.l.g.setVisibility(0);
                                if (Build.VERSION.SDK_INT >= 16) {
                                    this.l.g.setImageAlpha(255);
                                } else {
                                    this.l.g.setAlpha(255);
                                }
                                this.l.g.clearAnimation();
                            } else {
                                this.l.g.setVisibility(0);
                                if (Build.VERSION.SDK_INT >= 16) {
                                    this.l.g.setImageAlpha(255);
                                } else {
                                    this.l.g.setAlpha(255);
                                }
                                this.l.g.clearAnimation();
                            }
                        }
                        rVar.f.setVisibility(0);
                        rVar.f.c();
                        if (this.s == 0 || this.t) {
                            this.t = false;
                            rVar.f.a();
                        }
                        rVar.c.setVisibility(0);
                        rVar.d.setVisibility(8);
                        rVar.g.setVisibility(0);
                        if (Build.VERSION.SDK_INT >= 16) {
                            rVar.g.setImageAlpha(255);
                        } else {
                            rVar.g.setAlpha(255);
                        }
                        rVar.g.clearAnimation();
                        rVar.i.setVisibility(8);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(1, 1);
                        layoutParams.addRule(13, -1);
                        if (Build.VERSION.SDK_INT >= 14) {
                            this.n = new TextureView(this.b);
                            rVar.c.removeAllViews();
                            rVar.c.addView(this.n, layoutParams);
                            if (this.p == null) {
                                this.p = new s(this, null);
                            }
                            this.n.setSurfaceTextureListener(this.p);
                        } else {
                            this.o = new SurfaceView(this.b);
                            this.o.getHolder().setFormat(-3);
                            rVar.c.removeAllViews();
                            rVar.c.addView(this.o, layoutParams);
                            SurfaceHolder holder = this.o.getHolder();
                            if (holder != null) {
                                holder.addCallback(this);
                                if (Build.VERSION.SDK_INT < 11) {
                                    holder.setType(3);
                                }
                            }
                        }
                        this.l = rVar;
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void a() {
        if (this.k != null && this.k.isPlaying()) {
            this.k.stop();
            f2542a = -1;
            if (this.l != null) {
                this.l.c.setVisibility(8);
                this.l.f.setVisibility(8);
                this.l.d.setVisibility(0);
                this.l.g.setVisibility(0);
                if (Build.VERSION.SDK_INT >= 16) {
                    this.l.g.setImageAlpha(255);
                } else {
                    this.l.g.setAlpha(255);
                }
                this.l.g.clearAnimation();
            }
        }
    }

    public void b() {
        XLog.i("TagPageCradAdapter", "*** onResume ***");
        this.s++;
        if (this.l != null) {
            if (!c.d()) {
                this.l.d.setVisibility(0);
            }
            this.l.g.setVisibility(0);
            if (Build.VERSION.SDK_INT >= 16) {
                this.l.g.setImageAlpha(255);
            } else {
                this.l.g.setAlpha(255);
            }
            this.l.g.clearAnimation();
            a(this.l, f2542a);
        }
    }

    public void c() {
        XLog.i("TagPageCradAdapter", "*** onPause ***");
        if (this.k != null && this.k.isPlaying()) {
            this.k.stop();
        }
    }

    public void d() {
        if (this.k != null) {
            if (this.k.isPlaying()) {
                this.k.stop();
            }
            this.k.release();
            this.k = null;
        }
    }

    private int a(String str) {
        if (TextUtils.isEmpty(str) || !str.contains("x")) {
            return 1;
        }
        try {
            String[] split = str.split("x");
            if (split == null || 2 != split.length || Integer.valueOf(split[0]).intValue() > Integer.valueOf(split[1]).intValue()) {
                return 1;
            }
            return 2;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 1;
        }
    }

    private void a(r rVar, String str) {
        if (rVar != null) {
            this.q = df.a(this.b, 230.0f);
            this.r = df.a(this.b, 130.0f);
            if (2 == a(str)) {
                this.q = df.a(this.b, 130.0f);
                this.r = df.a(this.b, 230.0f);
            }
            XLog.i("TagPageCradAdapter", "[adjustVideoSize] --> mVideoWidth = " + this.q + ", mVideoHeight = " + this.r);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) rVar.c.getLayoutParams();
            layoutParams.width = this.q;
            layoutParams.height = this.r;
            rVar.c.setLayoutParams(layoutParams);
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) rVar.d.getLayoutParams();
            layoutParams2.width = this.q;
            layoutParams2.height = this.r;
            rVar.d.setLayoutParams(layoutParams2);
            RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) rVar.f.getLayoutParams();
            layoutParams3.width = this.q;
            layoutParams3.height = this.r;
            rVar.f.setLayoutParams(layoutParams3);
            RelativeLayout.LayoutParams layoutParams4 = (RelativeLayout.LayoutParams) rVar.g.getLayoutParams();
            layoutParams4.width = this.q;
            layoutParams4.height = this.r;
            rVar.g.setLayoutParams(layoutParams4);
            RelativeLayout.LayoutParams layoutParams5 = (RelativeLayout.LayoutParams) rVar.i.getLayoutParams();
            layoutParams5.width = this.q;
            layoutParams5.height = this.r;
            rVar.i.setLayoutParams(layoutParams5);
        }
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        if (this.l != null) {
            this.l.i.setVisibility(0);
            Animation loadAnimation = AnimationUtils.loadAnimation(this.b, R.anim.tag_page_video_play_finish);
            loadAnimation.setAnimationListener(new q(this, mediaPlayer));
            this.l.i.startAnimation(loadAnimation);
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        XLog.i("TagPageCradAdapter", "[onError] ---> what = " + i2 + ", extra = " + i3);
        mediaPlayer.reset();
        return false;
    }

    private STInfoV2 a(SimpleAppModel simpleAppModel, int i2) {
        if (simpleAppModel == null) {
            return STInfoBuilder.buildSTInfo(this.b, 100);
        }
        String a2 = a(i2);
        if (!(this.b instanceof BaseActivity)) {
            return STInfoBuilder.buildSTInfo(this.b, 100);
        }
        if (this.d == null) {
            this.d = new b();
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b, simpleAppModel, a2, 100, null);
        this.d.exposure(buildSTInfo);
        return buildSTInfo;
    }
}
