package com.tencent.assistant.tagpage;

import android.view.View;
import android.widget.Toast;
import com.tencent.assistant.net.c;

/* compiled from: ProGuard */
class k implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f2555a;
    final /* synthetic */ int b;
    final /* synthetic */ TagPageCardAdapter c;

    k(TagPageCardAdapter tagPageCardAdapter, r rVar, int i) {
        this.c = tagPageCardAdapter;
        this.f2555a = rVar;
        this.b = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.tagpage.TagPageCardAdapter, boolean):boolean
     arg types: [com.tencent.assistant.tagpage.TagPageCardAdapter, int]
     candidates:
      com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.tagpage.TagPageCardAdapter, android.media.MediaPlayer):android.media.MediaPlayer
      com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.model.SimpleAppModel, int):com.tencent.assistantv2.st.page.STInfoV2
      com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.tagpage.TagPageCardAdapter, int):java.lang.String
      com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.tagpage.r, java.lang.String):void
      com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.tagpage.r, int):void
      com.tencent.assistant.tagpage.TagPageCardAdapter.a(com.tencent.assistant.tagpage.TagPageCardAdapter, boolean):boolean */
    public void onClick(View view) {
        if (c.a()) {
            boolean unused = this.c.t = true;
            this.c.a(this.f2555a, this.b);
            TagPageActivity.n = this.b;
            return;
        }
        Toast.makeText(this.c.b, "请检查网络连接", 0).show();
    }
}
