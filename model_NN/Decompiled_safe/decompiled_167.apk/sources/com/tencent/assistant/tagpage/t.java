package com.tencent.assistant.tagpage;

import android.graphics.SurfaceTexture;
import android.text.TextUtils;
import android.view.Surface;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class t implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SurfaceTexture f2564a;
    final /* synthetic */ s b;

    t(s sVar, SurfaceTexture surfaceTexture) {
        this.b = sVar;
        this.f2564a = surfaceTexture;
    }

    public void run() {
        try {
            this.b.f2563a.k.reset();
            SimpleAppModel simpleAppModel = (SimpleAppModel) this.b.f2563a.getItem(TagPageCardAdapter.f2542a);
            if (simpleAppModel != null && !TextUtils.isEmpty(simpleAppModel.aF)) {
                this.b.f2563a.k.setDataSource(simpleAppModel.aF);
            }
            this.b.f2563a.k.setAudioStreamType(3);
            this.b.f2563a.k.setOnPreparedListener(this.b.f2563a);
            this.b.f2563a.k.setOnCompletionListener(this.b.f2563a);
            this.b.f2563a.k.setOnBufferingUpdateListener(this.b.f2563a);
            this.b.f2563a.k.setOnErrorListener(this.b.f2563a);
            this.b.f2563a.k.setVolume(0.0f, 0.0f);
            this.b.f2563a.k.setSurface(new Surface(this.f2564a));
            this.b.f2563a.k.prepare();
        } catch (Exception e) {
            XLog.e("TagPageCradAdapter", "[onSurfaceTextureAvailable] ---> e.printStackTrace() = " + e.toString());
            e.printStackTrace();
        }
    }
}
