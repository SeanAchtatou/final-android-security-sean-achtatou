package com.tencent.assistant.backgroundscan;

import android.content.Intent;
import com.tencent.open.SocialConstants;

/* compiled from: ProGuard */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Intent f854a;
    final /* synthetic */ BackgroundPushReceiver b;

    e(BackgroundPushReceiver backgroundPushReceiver, Intent intent) {
        this.b = backgroundPushReceiver;
        this.f854a = intent;
    }

    public void run() {
        byte byteExtra = this.f854a.getByteExtra(SocialConstants.PARAM_TYPE, (byte) 0);
        if (byteExtra > 0) {
            a.b().a(byteExtra, 2);
            a.b().a();
            n.a().a("b_new_scan_push_cancel", byteExtra);
        }
    }
}
