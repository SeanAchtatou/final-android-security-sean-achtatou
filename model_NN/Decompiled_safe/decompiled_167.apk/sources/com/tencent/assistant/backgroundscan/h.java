package com.tencent.assistant.backgroundscan;

import com.tencent.assistant.backgroundscan.BackgroundScanManager;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.securemodule.impl.SecureModuleService;
import java.util.Map;

/* compiled from: ProGuard */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ BackgroundScanManager f857a;

    h(BackgroundScanManager backgroundScanManager) {
        this.f857a = backgroundScanManager;
    }

    public void run() {
        for (Map.Entry entry : this.f857a.i.entrySet()) {
            BackgroundScanManager.SStatus sStatus = (BackgroundScanManager.SStatus) entry.getValue();
            if (sStatus == BackgroundScanManager.SStatus.prepare || sStatus == BackgroundScanManager.SStatus.running) {
                switch (((Byte) entry.getKey()).byteValue()) {
                    case 1:
                        XLog.d("BackgroundScan", "try to cancel health scan");
                        this.f857a.i.put((byte) 1, BackgroundScanManager.SStatus.none);
                        continue;
                    case 2:
                        XLog.d("BackgroundScan", "try to cancel mem scan");
                        this.f857a.i.put((byte) 2, BackgroundScanManager.SStatus.none);
                        continue;
                    case 3:
                        XLog.d("BackgroundScan", "try to cancel pkg scan");
                        this.f857a.f.c();
                        this.f857a.f.b(this.f857a.j);
                        this.f857a.i.put((byte) 3, BackgroundScanManager.SStatus.none);
                        continue;
                    case 4:
                        XLog.d("BackgroundScan", "try to cancel rubbish scan");
                        SpaceScanManager.a().h();
                        this.f857a.i.put((byte) 4, BackgroundScanManager.SStatus.none);
                        continue;
                    case 5:
                        XLog.d("BackgroundScan", "try to cancel bigfile scan");
                        this.f857a.i.put((byte) 5, BackgroundScanManager.SStatus.none);
                        continue;
                    case 6:
                        XLog.d("BackgroundScan", "try to cancel virus scan");
                        if (this.f857a.f847a != null) {
                            SecureModuleService.getInstance(this.f857a.e).unregisterCloudScanListener(this.f857a.e, this.f857a.f847a);
                        }
                        this.f857a.i.put((byte) 6, BackgroundScanManager.SStatus.none);
                        continue;
                }
            }
        }
    }
}
