package com.tencent.assistant;

import java.util.Properties;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private final Properties f382a = new Properties();

    /* JADX WARNING: Removed duplicated region for block: B:11:0x002a A[SYNTHETIC, Splitter:B:11:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0033 A[SYNTHETIC, Splitter:B:16:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private a() {
        /*
            r5 = this;
            r5.<init>()
            java.util.Properties r0 = new java.util.Properties
            r0.<init>()
            r5.f382a = r0
            r1 = 0
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0026, all -> 0x0030 }
            java.io.File r0 = android.os.Environment.getRootDirectory()     // Catch:{ Exception -> 0x0026, all -> 0x0030 }
            java.lang.String r3 = "build.prop"
            r2.<init>(r0, r3)     // Catch:{ Exception -> 0x0026, all -> 0x0030 }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0026, all -> 0x0030 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0026, all -> 0x0030 }
            java.util.Properties r1 = r5.f382a     // Catch:{ Exception -> 0x0040, all -> 0x003b }
            r1.load(r0)     // Catch:{ Exception -> 0x0040, all -> 0x003b }
            if (r0 == 0) goto L_0x0025
            r0.close()     // Catch:{ Exception -> 0x0037 }
        L_0x0025:
            return
        L_0x0026:
            r0 = move-exception
            r0 = r1
        L_0x0028:
            if (r0 == 0) goto L_0x0025
            r0.close()     // Catch:{ Exception -> 0x002e }
            goto L_0x0025
        L_0x002e:
            r0 = move-exception
            goto L_0x0025
        L_0x0030:
            r0 = move-exception
        L_0x0031:
            if (r1 == 0) goto L_0x0036
            r1.close()     // Catch:{ Exception -> 0x0039 }
        L_0x0036:
            throw r0
        L_0x0037:
            r0 = move-exception
            goto L_0x0025
        L_0x0039:
            r1 = move-exception
            goto L_0x0036
        L_0x003b:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0031
        L_0x0040:
            r1 = move-exception
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.a.<init>():void");
    }

    public String a(String str, String str2) {
        return this.f382a.getProperty(str, str2);
    }

    public static a a() {
        return new a();
    }
}
