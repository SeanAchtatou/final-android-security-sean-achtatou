package com.tencent.assistant.thumbnailCache;

import android.graphics.Bitmap;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public class o {

    /* renamed from: a  reason: collision with root package name */
    protected ReferenceQueue<p> f2580a;
    protected ArrayList<WeakReference<p>> b;
    public long c;
    public int d;
    public int e;
    public Bitmap f;
    private String g;
    private int h;
    private int i = -1;

    public o(String str, int i2) {
        this.g = str;
        this.h = c(i2);
        this.f2580a = new ReferenceQueue<>();
        this.b = new ArrayList<>();
        this.c = System.currentTimeMillis();
        this.d = 0;
        this.e = 0;
        this.f = null;
    }

    public void a(p pVar) {
        if (pVar != null) {
            while (true) {
                Reference<? extends p> poll = this.f2580a.poll();
                if (poll == null) {
                    break;
                }
                this.b.remove(poll);
            }
            Iterator<WeakReference<p>> it = this.b.iterator();
            while (it.hasNext()) {
                if (((p) it.next().get()) == pVar) {
                    return;
                }
            }
            this.b.add(new WeakReference(pVar, this.f2580a));
        }
    }

    public void b(p pVar) {
        if (pVar != null) {
            Iterator<WeakReference<p>> it = this.b.iterator();
            while (it.hasNext()) {
                WeakReference next = it.next();
                if (((p) next.get()) == pVar) {
                    this.b.remove(next);
                    return;
                }
            }
        }
    }

    public boolean a(o oVar) {
        if (oVar == null) {
            return false;
        }
        Iterator<WeakReference<p>> it = oVar.b().iterator();
        while (it.hasNext()) {
            a((p) it.next().get());
        }
        return true;
    }

    public int a() {
        return this.b.size();
    }

    private void e() {
        this.b.clear();
    }

    private void f() {
        Iterator<WeakReference<p>> it = this.b.iterator();
        while (it.hasNext()) {
            p pVar = (p) it.next().get();
            if (pVar != null) {
                pVar.thumbnailRequestCompleted(this);
            }
        }
    }

    private void g() {
        Iterator<WeakReference<p>> it = this.b.iterator();
        while (it.hasNext()) {
            p pVar = (p) it.next().get();
            if (pVar != null) {
                pVar.thumbnailRequestFailed(this);
            }
        }
    }

    public void a(int i2) {
        switch (i2) {
            case -1:
            case 0:
            case 1:
            case 2:
            case 3:
                this.i = i2;
                return;
            default:
                this.i = 2;
                return;
        }
    }

    public void b(int i2) {
        a(i2);
        switch (i2) {
            case 0:
                f();
                break;
            case 2:
                g();
                break;
        }
        e();
    }

    public static int c(int i2) {
        switch (i2) {
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                return i2;
            default:
                return 1;
        }
    }

    public ArrayList<WeakReference<p>> b() {
        return this.b;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        return this.g.equals(((o) obj).g);
    }

    public int hashCode() {
        return this.g.hashCode();
    }

    public String c() {
        return this.g;
    }

    public int d() {
        return this.h;
    }
}
