package com.tencent.assistant.thumbnailCache;

import android.graphics.Bitmap;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: ProGuard */
class ThumbnailCache$1 extends LinkedHashMap<String, Bitmap> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f2565a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ThumbnailCache$1(b bVar, int i, float f, boolean z) {
        super(i, f, z);
        this.f2565a = bVar;
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(Map.Entry<String, Bitmap> entry) {
        if (this.f2565a.m / 1024 > this.f2565a.n) {
            if (!(entry == null || entry.getValue() == null)) {
                b.a(this.f2565a, (long) (entry.getValue().getHeight() * entry.getValue().getRowBytes()));
            }
            return true;
        } else if (size() <= this.f2565a.e) {
            return false;
        } else {
            this.f2565a.b.put(entry.getKey(), new a(entry.getKey(), entry.getValue(), this.f2565a.c));
            return true;
        }
    }
}
