package com.tencent.assistant.f;

import android.util.Log;
import com.qq.AppService.AppService;
import com.qq.AppService.AstApp;
import com.qq.m.c;
import com.qq.m.d;
import com.qq.m.e;
import com.tencent.assistant.model.OnlinePCListItemModel;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.connector.ipc.ConnectionType;
import com.tencent.connector.ipc.a;
import com.tencent.wcs.jce.MInviteResponse;
import com.tencent.wcs.jce.PCInfo;
import com.tencent.wcs.jce.PingPCSResponse;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class b implements c, e {

    /* renamed from: a  reason: collision with root package name */
    private static b f1268a;
    private CallbackHelper<a> b = new CallbackHelper<>();

    private b() {
    }

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (f1268a == null) {
                f1268a = new b();
            }
            bVar = f1268a;
        }
        return bVar;
    }

    public void b() {
        if (a.a().d()) {
            ConnectionType e = a.a().e();
            if (e == ConnectionType.USB) {
                d();
                return;
            } else if (e == ConnectionType.WIFI) {
                e();
                return;
            } else if (e == ConnectionType.TRANS) {
                e();
                return;
            }
        }
        f();
    }

    public void c() {
        new d(AstApp.i(), this).start();
    }

    public void a(String str) {
        new com.qq.m.b(AstApp.i(), str, null, this).start();
    }

    public void a(a aVar) {
        this.b.register(aVar);
    }

    public void b(a aVar) {
        this.b.unregister(aVar);
    }

    public void d() {
        this.b.broadcastInMainThread(new c(this));
    }

    public void e() {
        this.b.broadcastInMainThread(new d(this));
    }

    public void f() {
        this.b.broadcastInMainThread(new e(this));
    }

    public void a(int i) {
        this.b.broadcastInMainThread(new f(this, i));
    }

    public void a(Object obj) {
        this.b.broadcastInMainThread(new g(this, obj));
    }

    public void b(Object obj) {
        this.b.broadcastInMainThread(new h(this, obj));
    }

    public void b(int i) {
        this.b.broadcastInMainThread(new i(this, i));
    }

    public void a(int i, int i2, PingPCSResponse pingPCSResponse) {
        Log.d("ConnectionPCManager", "response: " + i + " errcode: " + i2);
        if (pingPCSResponse != null) {
            ArrayList<PCInfo> arrayList = pingPCSResponse.c;
            ArrayList arrayList2 = new ArrayList();
            if (arrayList != null) {
                int size = arrayList.size();
                for (int i3 = 0; i3 < size; i3++) {
                    PCInfo pCInfo = arrayList.get(i3);
                    if (pCInfo != null) {
                        arrayList2.add(new OnlinePCListItemModel(pCInfo));
                    }
                }
            }
            a(arrayList2);
            return;
        }
        a(i2);
    }

    public void a(int i, int i2, MInviteResponse mInviteResponse) {
        Log.d("ConnectionPCManager", "invite response: " + i + " errcode: " + i2);
        if (i != 200 || mInviteResponse == null || mInviteResponse.f3871a == null || !mInviteResponse.f3871a.equals(AppService.l)) {
            b(69904);
        } else if (mInviteResponse.b == 0) {
            b(mInviteResponse);
        } else {
            b(mInviteResponse.b);
        }
    }
}
