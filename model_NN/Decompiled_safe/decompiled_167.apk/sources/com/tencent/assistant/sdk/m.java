package com.tencent.assistant.sdk;

import com.tencent.assistant.sdk.param.a;
import com.tencent.assistant.sdk.param.jce.IPCHead;
import com.tencent.assistant.sdk.param.jce.ServiceFreeActionResponse;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/* compiled from: ProGuard */
public class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SDKIPCBroadcaster f2467a;
    private boolean b = false;
    private Object c = new Object();

    public m(SDKIPCBroadcaster sDKIPCBroadcaster) {
        this.f2467a = sDKIPCBroadcaster;
    }

    public void run() {
        long j;
        while (true) {
            synchronized (this.c) {
                while (!this.b) {
                    try {
                        this.c.wait();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                try {
                    Iterator it = new HashSet(SDKIPCBroadcaster.c.keySet()).iterator();
                    while (it.hasNext()) {
                        String str = (String) it.next();
                        ArrayList arrayList = (ArrayList) SDKIPCBroadcaster.c.get(str);
                        ArrayList arrayList2 = new ArrayList();
                        if (arrayList != null) {
                            arrayList2.addAll(arrayList);
                        }
                        if (!this.f2467a.a(arrayList2) && !this.f2467a.a(str) && this.f2467a.e != null && str != null) {
                            Long l = (Long) this.f2467a.e.get(str);
                            if (l != null) {
                                j = l.longValue();
                            } else {
                                j = 0;
                            }
                            if (System.currentTimeMillis() - j > 30000) {
                                IPCHead iPCHead = new IPCHead();
                                iPCHead.b = 8;
                                RequestHandler.a().a(str, a.a(a.a(iPCHead, new ServiceFreeActionResponse())));
                                this.f2467a.b(str);
                            }
                        }
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                try {
                    Thread.sleep(5000);
                } catch (Exception e2) {
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (this.b != z) {
            this.b = z;
            if (this.b) {
                synchronized (this.c) {
                    this.c.notify();
                }
            }
        }
    }
}
