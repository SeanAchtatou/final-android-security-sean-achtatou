package com.tencent.assistant.js;

import com.tencent.assistant.utils.XLog;
import com.tencent.b.b.a;
import com.tencent.b.b.c;
import org.json.JSONObject;

/* compiled from: ProGuard */
class g implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ JSONObject f1354a;
    final /* synthetic */ String b;
    final /* synthetic */ int c;
    final /* synthetic */ String d;
    final /* synthetic */ JsBridge e;

    g(JsBridge jsBridge, JSONObject jSONObject, String str, int i, String str2) {
        this.e = jsBridge;
        this.f1354a = jSONObject;
        this.b = str;
        this.c = i;
        this.d = str2;
    }

    public void a(Object obj) {
        String str = (String) obj;
        if (c.a(str)) {
            try {
                this.f1354a.put("mid", str);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            this.e.response(this.b, this.c, this.d, this.f1354a.toString());
            return;
        }
        this.e.responseFail(this.b, this.c, this.d, -9);
    }

    public void a(int i, String str) {
        XLog.i("Jie", "requestMid failed, error code:" + i + " ,error msg:" + str);
        this.e.responseFail(this.b, this.c, this.d, i);
    }
}
