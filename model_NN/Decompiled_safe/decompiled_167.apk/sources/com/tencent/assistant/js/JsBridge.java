package com.tencent.assistant.js;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.SparseArray;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.BrowserActivity;
import com.tencent.assistant.activity.ExternalCallActivity;
import com.tencent.assistant.activity.pictureprocessor.ShowPictureActivity;
import com.tencent.assistant.component.appdetail.CommentDetailTabView;
import com.tencent.assistant.d.b;
import com.tencent.assistant.d.e;
import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.download.SimpleDownloadInfo;
import com.tencent.assistant.download.a;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.login.d;
import com.tencent.assistant.manager.BookingManager;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.manager.SelfUpdateManager;
import com.tencent.assistant.manager.be;
import com.tencent.assistant.model.ShareBaseModel;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.ad;
import com.tencent.assistant.module.callback.n;
import com.tencent.assistant.module.callback.s;
import com.tencent.assistant.module.callback.v;
import com.tencent.assistant.module.cw;
import com.tencent.assistant.module.ds;
import com.tencent.assistant.module.ei;
import com.tencent.assistant.module.fa;
import com.tencent.assistant.module.u;
import com.tencent.assistant.module.update.k;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.plugin.mgr.f;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.protocol.jce.AppUpdateInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.st.STConstAction;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ap;
import com.tencent.assistant.utils.ba;
import com.tencent.assistant.utils.ce;
import com.tencent.assistant.utils.ct;
import com.tencent.assistant.utils.de;
import com.tencent.assistant.utils.i;
import com.tencent.assistant.utils.installuninstall.InstallUninstallTaskBean;
import com.tencent.assistant.utils.installuninstall.p;
import com.tencent.assistant.utils.r;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.component.TxWebViewContainer;
import com.tencent.assistantv2.mediadownload.o;
import com.tencent.assistantv2.model.AbstractDownloadInfo;
import com.tencent.assistantv2.model.h;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.connect.common.Constants;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.open.SocialConstants;
import com.tencent.smtt.sdk.WebView;
import com.tencent.tauth.AuthActivity;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class JsBridge implements UIEventListener, ad, n, s, v {
    public static final String ACTIVITY_STATE_CALLBACK_FUNCTION_NAME = "activityStateCallback";
    public static final String APP_INSTALLED_AND_ACTION_COMPLETE_FUNCTION = "appInstalledAndActionCompleteCallback";
    public static final String APP_INSTALL_UNINSTALL = "appInstallUninstall";
    public static final String BUTTON_CLICK_CALLBACK_FUNCTION_NAME = "clickCallback";
    private static final String CALL_BATCH_NAME = "callBatch";
    public static final String FILE_CHOOSER_CALLBACK_FUNCTION_NAME = "fileChooserCallback";
    public static final String IS_INTERFACE_READY_NAME = "isInterfaceReady";
    public static final int JSBRIDGE_VERSION = 3;
    public static final String JS_BRIDGE_SCHEME = "jsb://";
    private static final String JS_CONFIG = "js_config";
    public static final String LOGIN_CALLBACK_FUNCTION_NAME = "loginCallback";
    private static final String LOGIN_TYPE_DEFAULT = "DEFAULT";
    private static final String LOGIN_TYPE_MOBILEQ = "MOBILEQ";
    private static final String LOGIN_TYPE_QUICK_MOBILEQ = "QMOBILEQ";
    private static final String LOGIN_TYPE_QUICK_WX = "QWX";
    private static final String LOGIN_TYPE_WX = "WX";
    public static final String READY_CALLBACK_FUNCTION_NAME = "readyCallback";
    public static final String SHARE_CALLBACK_FUNCTION_NAME = "shareCallback";
    public static final String STATE_CALLBACK_FUNCTION_NAME = "stateCallback";
    private static final long STORE_DATA_MAX_LENGTH = 1024;
    private static final String TAG = JsBridge.class.getSimpleName();
    public static final String USERFIT_CALLBACK_FUNCTION_NAME = "userFitCallback";
    public static final String VIDEO_DOWNLOAD_STATE_CALLBACK = "videoDownloadStateCallback";
    public static final String WB_REPORT = "wb_report";
    private final String STATUS_NO;
    private final String STATUS_OK;
    private String authFuction;
    com.tencent.assistant.manager.n authManager;
    private int authSeq;
    private long createTime;
    /* access modifiers changed from: private */
    public String currentUrl;
    private int externalCallTicketStatus;
    private cw getDomainCapabilityEngine;
    private ds getSimpleAppInfoEngine;
    private boolean isRefreshTicket;
    /* access modifiers changed from: private */
    public b lbsEngine;
    private long loadedTime;
    private TreeSet<HashMap<String, String>> localThemeSet;
    /* access modifiers changed from: private */
    public WeakReference<Activity> mActivityRef;
    private Context mContext;
    private Bundle mExternalCallTicketReqBundle;
    private SparseArray<Bundle> mHttpReqMap;
    private e mLBSNotification;
    /* access modifiers changed from: private */
    public Bundle mLBSReqBundle;
    private Bundle mSelfUpdateReqBundle;
    private HashMap<String, String> mStoreData;
    private WebView mWebView;
    private WeakReference<TxWebViewContainer> mwebViewContainRef;
    private long startLoadTime;
    private android.webkit.WebView sysWebView;
    private Map<Integer, j> wantQueryAppModelHolderMap;

    public JsBridge(Activity activity, WebView webView, TxWebViewContainer txWebViewContainer) {
        this(activity, webView);
        this.mwebViewContainRef = new WeakReference<>(txWebViewContainer);
    }

    public JsBridge(Activity activity, WebView webView) {
        this.sysWebView = null;
        this.mHttpReqMap = new SparseArray<>();
        this.lbsEngine = null;
        this.mLBSReqBundle = null;
        this.mSelfUpdateReqBundle = null;
        this.mExternalCallTicketReqBundle = null;
        this.externalCallTicketStatus = 0;
        this.isRefreshTicket = false;
        this.authManager = new com.tencent.assistant.manager.n();
        this.authSeq = -1;
        this.authFuction = null;
        this.getSimpleAppInfoEngine = new ds();
        this.wantQueryAppModelHolderMap = new ConcurrentHashMap();
        this.getDomainCapabilityEngine = new cw();
        this.localThemeSet = new TreeSet<>();
        this.STATUS_OK = "1";
        this.STATUS_NO = "0";
        this.mLBSNotification = new c(this);
        this.mActivityRef = new WeakReference<>(activity);
        this.mContext = AstApp.i();
        registerAuthEvent();
        this.getSimpleAppInfoEngine.register(this);
        this.getDomainCapabilityEngine.register(this);
        fa.a().register(this);
        this.lbsEngine = new b(this.mContext.getApplicationContext(), this.mLBSNotification);
        this.createTime = System.currentTimeMillis();
        this.mWebView = webView;
        if (this.mWebView != null) {
            try {
                Method method = this.mWebView.getClass().getMethod("removeJavascriptInterface", String.class);
                if (method != null) {
                    method.invoke(this.mWebView, "searchBoxJavaBridge_");
                }
            } catch (Exception e) {
            }
        }
    }

    public JsBridge(Activity activity, android.webkit.WebView webView, TxWebViewContainer txWebViewContainer) {
        this.sysWebView = null;
        this.mHttpReqMap = new SparseArray<>();
        this.lbsEngine = null;
        this.mLBSReqBundle = null;
        this.mSelfUpdateReqBundle = null;
        this.mExternalCallTicketReqBundle = null;
        this.externalCallTicketStatus = 0;
        this.isRefreshTicket = false;
        this.authManager = new com.tencent.assistant.manager.n();
        this.authSeq = -1;
        this.authFuction = null;
        this.getSimpleAppInfoEngine = new ds();
        this.wantQueryAppModelHolderMap = new ConcurrentHashMap();
        this.getDomainCapabilityEngine = new cw();
        this.localThemeSet = new TreeSet<>();
        this.STATUS_OK = "1";
        this.STATUS_NO = "0";
        this.mLBSNotification = new c(this);
        this.mwebViewContainRef = new WeakReference<>(txWebViewContainer);
    }

    public JsBridge(Activity activity, android.webkit.WebView webView) {
        this.sysWebView = null;
        this.mHttpReqMap = new SparseArray<>();
        this.lbsEngine = null;
        this.mLBSReqBundle = null;
        this.mSelfUpdateReqBundle = null;
        this.mExternalCallTicketReqBundle = null;
        this.externalCallTicketStatus = 0;
        this.isRefreshTicket = false;
        this.authManager = new com.tencent.assistant.manager.n();
        this.authSeq = -1;
        this.authFuction = null;
        this.getSimpleAppInfoEngine = new ds();
        this.wantQueryAppModelHolderMap = new ConcurrentHashMap();
        this.getDomainCapabilityEngine = new cw();
        this.localThemeSet = new TreeSet<>();
        this.STATUS_OK = "1";
        this.STATUS_NO = "0";
        this.mLBSNotification = new c(this);
        this.mActivityRef = new WeakReference<>(activity);
        this.mContext = AstApp.i();
        registerAuthEvent();
        this.getSimpleAppInfoEngine.register(this);
        this.getDomainCapabilityEngine.register(this);
        fa.a().register(this);
        this.createTime = System.currentTimeMillis();
        this.sysWebView = webView;
        if (this.sysWebView != null) {
            try {
                Method method = this.sysWebView.getClass().getMethod("removeJavascriptInterface", String.class);
                if (method != null) {
                    method.invoke(this.sysWebView, "searchBoxJavaBridge_");
                }
            } catch (Exception e) {
            }
        }
    }

    public void onResume() {
        HashMap hashMap = new HashMap();
        hashMap.put("state", String.valueOf(k.b()));
        response(ACTIVITY_STATE_CALLBACK_FUNCTION_NAME, 0, null, "onResume", hashMap);
    }

    public void onPause() {
        response(ACTIVITY_STATE_CALLBACK_FUNCTION_NAME, 0, null, "onPause");
    }

    private void registerAuthEvent() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_AUTH_FAIL_FOR_WEBVIEW, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_AUTH_SUCCESS_FOR_WEBVIEW, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_SUCCESS_FOR_WEBVIEW, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL_FOR_WEBVIEW, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        AstApp.i().k().addUIEventListener(1013, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        AstApp.i().k().addUIEventListener(1007, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(1027, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_GET_TOKENTICKET_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_GET_TOKENTICKET_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.APP_LINK_EVENT_INSTALLED_AND_ACTION_COMPLETE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WTLOGIN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_WTLOGIN_FAIL, this);
    }

    private void unRegisterAuthEvent() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_AUTH_FAIL_FOR_WEBVIEW, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_AUTH_SUCCESS_FOR_WEBVIEW, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_SUCCESS_FOR_WEBVIEW, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL_FOR_WEBVIEW, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        AstApp.i().k().removeUIEventListener(1013, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        AstApp.i().k().removeUIEventListener(1007, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC, this);
        AstApp.i().k().removeUIEventListener(1027, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_GET_TOKENTICKET_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_GET_TOKENTICKET_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.APP_LINK_EVENT_INSTALLED_AND_ACTION_COMPLETE, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_WTLOGIN_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_WTLOGIN_FAIL, this);
    }

    public void recycle() {
        unRegisterAuthEvent();
        if (this.mStoreData != null) {
            this.mStoreData.clear();
            this.mStoreData = null;
        }
        if (this.getDomainCapabilityEngine != null) {
            this.getDomainCapabilityEngine.unregister(this);
        }
        if (this.getSimpleAppInfoEngine != null) {
            this.getSimpleAppInfoEngine.unregister(this);
        }
        fa.a().unregister(this);
    }

    public void getAppInfo(Uri uri, int i, String str, String str2) {
        AppUpdateInfo a2;
        String queryParameter = uri.getQueryParameter("packagenames");
        if (!TextUtils.isEmpty(queryParameter) && !TextUtils.isEmpty(str2)) {
            String queryParameter2 = uri.getQueryParameter("noupdateinfo");
            String[] split = queryParameter.split(",");
            if (split != null && split.length != 0) {
                JSONObject jSONObject = new JSONObject();
                for (String trim : split) {
                    String trim2 = trim.trim();
                    LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(trim2);
                    try {
                        JSONObject jSONObject2 = new JSONObject();
                        if (localApkInfo != null) {
                            jSONObject2.put("install", 1);
                            jSONObject2.put("appName", localApkInfo.mAppName);
                            jSONObject2.put("verCode", localApkInfo.mVersionCode);
                            jSONObject2.put("verName", localApkInfo.mVersionName);
                            jSONObject2.put("manifestMd5", localApkInfo.manifestMd5);
                            if (queryParameter2 == null && (a2 = k.b().a(trim2)) != null) {
                                jSONObject2.put("canUpdate", 1);
                                if (a2.m > 0) {
                                    jSONObject2.put("saveByte", a2.j - a2.m);
                                } else {
                                    jSONObject2.put("saveByte", 0);
                                }
                            }
                        } else {
                            jSONObject2.put("install", 0);
                        }
                        jSONObject.put(trim2, jSONObject2);
                    } catch (Exception e) {
                        responseFail(str2, i, str, -3);
                    }
                }
                response(str2, i, str, jSONObject.toString());
            }
        }
    }

    public void sendHttpRequest(Uri uri, int i, String str, String str2) {
        String queryParameter = uri.getQueryParameter(SocialConstants.PARAM_URL);
        String queryParameter2 = uri.getQueryParameter("httpmethod");
        if (queryParameter2 == null) {
            queryParameter2 = Constants.HTTP_GET;
        }
        ei eiVar = new ei(AstApp.i().getApplicationContext(), queryParameter, queryParameter2, i, this);
        Bundle bundle = new Bundle();
        bundle.putInt("seqid", i);
        bundle.putString("method", str);
        bundle.putString("callbackFun", str2);
        this.mHttpReqMap.append(i, bundle);
        eiVar.execute(new Bundle[0]);
    }

    public void startOpenApp(Uri uri, int i, String str, String str2) {
        boolean z;
        int i2 = 2000;
        String queryParameter = uri.getQueryParameter("packageName");
        String queryParameter2 = uri.getQueryParameter("activityClassName");
        String queryParameter3 = uri.getQueryParameter("extra");
        int d = ct.d(uri.getQueryParameter("scene"));
        int d2 = ct.d(uri.getQueryParameter("sourceScene"));
        if (d == 0) {
            d = 2000;
        }
        if (d2 != 0) {
            i2 = d2;
        }
        Bundle bundle = new Bundle();
        try {
            if (!TextUtils.isEmpty(queryParameter3)) {
                JSONObject jSONObject = new JSONObject(queryParameter3);
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    Object opt = jSONObject.opt(next);
                    if (opt instanceof Integer) {
                        bundle.putInt(next, Integer.valueOf(String.valueOf(opt)).intValue());
                    } else if (opt instanceof String) {
                        bundle.putString(next, String.valueOf(opt));
                    } else if (opt instanceof Long) {
                        bundle.putLong(next, Long.valueOf(String.valueOf(opt)).longValue());
                    } else if (opt instanceof Byte) {
                        bundle.putByte(next, Byte.valueOf(String.valueOf(opt)).byteValue());
                    } else if (opt instanceof Short) {
                        bundle.putShort(next, Short.valueOf(String.valueOf(opt)).shortValue());
                    } else if (opt instanceof Double) {
                        bundle.putDouble(next, Double.valueOf(String.valueOf(opt)).doubleValue());
                    } else if (opt instanceof Float) {
                        bundle.putFloat(next, Float.valueOf(String.valueOf(opt)).floatValue());
                    }
                }
            }
        } catch (Exception e) {
            try {
                e.printStackTrace();
            } catch (Exception e2) {
                responseFail(str2, i, str, -3);
                return;
            }
        }
        if (!TextUtils.isEmpty(queryParameter)) {
            z = r.a(queryParameter, bundle);
        } else if (!TextUtils.isEmpty(queryParameter2)) {
            z = r.b(queryParameter2, bundle);
        } else {
            z = false;
        }
        com.tencent.assistantv2.st.k.a(new STInfoV2(d, STConst.ST_DEFAULT_SLOT, i2, STConst.ST_DEFAULT_SLOT, STConstAction.ACTION_HIT_OPEN));
        if (z) {
            response(str2, i, str, Constants.STR_EMPTY);
        } else {
            responseFail(str2, i, str, -2);
        }
    }

    public void getNetInfo(Uri uri, int i, String str, String str2) {
        JSONObject jSONObject = new JSONObject();
        try {
            com.tencent.assistant.net.b h = c.h();
            if (h.f1925a == APN.UN_DETECT) {
                c.j();
            }
            jSONObject.put("apn", h.f1925a);
            jSONObject.put("isWap", h.d ? 1 : 0);
            jSONObject.put("networkOperator", h.b);
            jSONObject.put("networkType", h.c);
            response(str2, i, str, jSONObject.toString());
        } catch (Exception e) {
            responseFail(str2, i, str, -3);
        }
    }

    public void getMobileInfo(Uri uri, int i, String str, String str2) {
        boolean z;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("osVer", Build.VERSION.SDK_INT);
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("width", t.b);
            jSONObject2.put("height", t.c);
            jSONObject.put("resolution", jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            com.tencent.assistant.net.b h = c.h();
            if (h.f1925a == APN.UN_DETECT) {
                c.j();
            }
            jSONObject3.put("apn", h.f1925a);
            jSONObject3.put("isWap", h.d ? 1 : 0);
            jSONObject3.put("networkOperator", h.b);
            jSONObject3.put("networkType", h.c);
            jSONObject.put("network", jSONObject3);
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("freeMemory", t.r());
            jSONObject4.put("totalMemory", t.q());
            jSONObject.put("memory", jSONObject4);
            JSONObject jSONObject5 = new JSONObject();
            long c = t.c();
            long d = t.d();
            boolean z2 = (FileUtil.isSDCardExistAndCanWrite() && !t.p()) || t.f() > d;
            if (ap.e().size() > 0) {
                z = true;
            } else {
                z = false;
            }
            XLog.i(TAG, "手机可用 : " + (((double) c) / 1.073741824E9d) + " GB");
            XLog.i(TAG, "手机总共 : " + (((double) d) / 1.073741824E9d) + " GB");
            jSONObject5.put("availableInternalStorage", c);
            jSONObject5.put("totalInternalStorage", d);
            if (z2 || z) {
                long j = 0;
                long j2 = 0;
                if (z2) {
                    j = 0 + t.e();
                    j2 = 0 + t.f();
                }
                if (z) {
                    j += ap.c();
                    j2 += ap.d();
                }
                XLog.i(TAG, "SDCard 可用 : " + (((double) j) / 1.073741824E9d) + " GB");
                XLog.i(TAG, "SDCard 总共: " + (((double) j2) / 1.073741824E9d) + " GB");
                jSONObject5.put("availableExternalStorage", j);
                jSONObject5.put("totalExternalStorage", j2);
            } else {
                jSONObject5.put("availableExternalStorage", 0);
                jSONObject5.put("totalExternalStorage", 0);
            }
            if ("1".equals(uri.getQueryParameter("needLocalApkList"))) {
                JSONArray jSONArray = new JSONArray();
                List<LocalApkInfo> localApkInfos = ApkResourceManager.getInstance().getLocalApkInfos();
                if (localApkInfos != null && localApkInfos.size() > 0) {
                    for (LocalApkInfo next : localApkInfos) {
                        JSONObject jSONObject6 = new JSONObject();
                        jSONObject6.put("packageName", next.mPackageName);
                        jSONObject6.put(CommentDetailTabView.PARAMS_VERSION_CODE, next.mVersionCode);
                        jSONObject6.put("versionName", next.mVersionName);
                        jSONArray.put(jSONObject6);
                    }
                    jSONObject.put("apkList", jSONArray);
                }
            }
            jSONObject.put("storage", jSONObject5);
            jSONObject.put("extSDAvailable", ap.a() ? 1 : 0);
            response(str2, i, str, jSONObject.toString());
        } catch (Exception e) {
            responseFail(str2, i, str, -3);
        }
    }

    public void getPrivateMobileInfo(Uri uri, int i, String str, String str2) {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("androidId", t.l());
            jSONObject2.put("androidIdSdCard", t.m());
            jSONObject2.put("imei", t.g());
            jSONObject2.put("imsi", t.h());
            jSONObject2.put("macAdress", t.k());
            jSONObject.put("terminal", jSONObject2);
            jSONObject.put("channelId", Global.getChannelId());
            jSONObject.put("realChannelId", Global.getRealChannelId());
            jSONObject.put("qua", Global.getQUA());
            jSONObject.put("versionName", Global.getAppVersionName());
            jSONObject.put(CommentDetailTabView.PARAMS_VERSION_CODE, Global.getAppVersionCode());
            jSONObject.put("phoneGuid", Global.getPhoneGuid());
            jSONObject.put("mark", Build.MANUFACTURER + "_" + Build.MODEL);
            response(str2, i, str, jSONObject.toString());
        } catch (Exception e) {
            responseFail(str2, i, str, -3);
        }
    }

    public void getLBSInfo(Uri uri, int i, String str, String str2) {
        if (this.mLBSReqBundle == null) {
            this.mLBSReqBundle = new Bundle();
        }
        this.mLBSReqBundle.putInt("seqId", i);
        this.mLBSReqBundle.putString("method", str);
        this.mLBSReqBundle.putString("callbackFun", str2);
        this.lbsEngine.a();
    }

    public void getExternalCallTicketStatus(Uri uri, int i, String str, String str2) {
        if (this.mExternalCallTicketReqBundle == null) {
            this.mExternalCallTicketReqBundle = new Bundle();
        }
        this.mExternalCallTicketReqBundle.putInt("seqId", i);
        this.mExternalCallTicketReqBundle.putString("method", str);
        this.mExternalCallTicketReqBundle.putString("callbackFun", str2);
        if (this.externalCallTicketStatus > 0) {
            response(str2, i, str, String.valueOf(this.externalCallTicketStatus));
        } else if (this.externalCallTicketStatus < 0) {
            responseFail(str2, i, str, -1);
        } else {
            Activity activity = this.mActivityRef.get();
            if (this.mActivityRef != null && activity != null && (activity instanceof ExternalCallActivity)) {
                String D = ((ExternalCallActivity) activity).D();
                if (!TextUtils.isEmpty(D) && String.valueOf(d.a().p()).equals(D)) {
                    this.externalCallTicketStatus = 1;
                    response(str2, i, str, String.valueOf(this.externalCallTicketStatus));
                }
            }
        }
    }

    public void setWebView(Uri uri, int i, String str, String str2) {
        try {
            ba.a().post(new d(this, uri.getQueryParameter("title"), ct.a(uri.getQueryParameter("toolbar"), -1), ct.a(uri.getQueryParameter("titlebar"), -1), ct.a(uri.getQueryParameter("buttonVisible"), 0), str2, i, str));
        } catch (Exception e) {
            responseFail(str2, i, str, -3);
        }
    }

    public void closeWebView(Uri uri, int i, String str, String str2) {
        if (this.mActivityRef != null && this.mActivityRef.get() != null) {
            this.mActivityRef.get().finish();
        }
    }

    public void pageControl(Uri uri, int i, String str, String str2) {
        int d = ct.d(uri.getQueryParameter(SocialConstants.PARAM_TYPE));
        if (this.mWebView != null) {
            if (d == 1) {
                this.mWebView.goBack();
            } else if (d == 2) {
                this.mWebView.goForward();
            } else {
                this.mWebView.reload();
            }
        }
        response(str2, i, str, Constants.STR_EMPTY);
    }

    public void store(Uri uri, int i, String str, String str2) {
        ct.d(uri.getQueryParameter(SocialConstants.PARAM_TYPE));
        String queryParameter = uri.getQueryParameter("key");
        String queryParameter2 = uri.getQueryParameter("value");
        if (((long) queryParameter2.getBytes().length) > STORE_DATA_MAX_LENGTH) {
            responseFail(str2, i, str, -4);
            return;
        }
        if (this.mStoreData == null) {
            this.mStoreData = new HashMap<>();
        }
        this.mStoreData.put(queryParameter, queryParameter2);
        response(str2, i, str, Constants.STR_EMPTY);
    }

    public void getStoreByKey(Uri uri, int i, String str, String str2) {
        String queryParameter = uri.getQueryParameter("key");
        if (this.mStoreData == null) {
            response(str2, i, str, Constants.STR_EMPTY);
            return;
        }
        String str3 = this.mStoreData.get(queryParameter);
        if (str3 != null) {
            response(str2, i, str, str3);
        } else {
            response(str2, i, str, Constants.STR_EMPTY);
        }
    }

    public void getAllStore(Uri uri, int i, String str, String str2) {
        if (this.mStoreData == null) {
            response(str2, i, str, Constants.STR_EMPTY);
            return;
        }
        JSONObject jSONObject = new JSONObject();
        for (Map.Entry next : this.mStoreData.entrySet()) {
            try {
                jSONObject.put((String) next.getKey(), next.getValue());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        response(str2, i, str, jSONObject.toString());
    }

    public void queryDownload(Uri uri, int i, String str, String str2) {
        try {
            long c = ct.c(uri.getQueryParameter("appid"));
            long c2 = ct.c(uri.getQueryParameter("apkid"));
            String queryParameter = uri.getQueryParameter("packagename");
            int d = ct.d(uri.getQueryParameter("versioncode"));
            int d2 = ct.d(uri.getQueryParameter("grayversioncode"));
            DownloadInfo downloadInfo = null;
            if (c2 > 0) {
                downloadInfo = DownloadProxy.a().d(String.valueOf(c2));
            }
            if (downloadInfo == null) {
                downloadInfo = DownloadProxy.a().a(queryParameter, d, d2);
            }
            if (downloadInfo != null) {
                AppConst.AppState b = u.b(downloadInfo);
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("appid", c);
                jSONObject.put("apkid", c2);
                jSONObject.put("packagename", queryParameter);
                jSONObject.put("versioncode", d);
                jSONObject.put("grayversioncode", d2);
                jSONObject.put("appstate", b);
                jSONObject.put("downpercent", u.a(downloadInfo, b));
                response(str2, i, str, jSONObject.toString());
                return;
            }
            AppConst.AppState localApkState = getLocalApkState(queryParameter, d, d2);
            if (AppConst.AppState.ILLEGAL == localApkState || AppConst.AppState.DOWNLOAD == localApkState) {
                responseFail(str2, i, str, -2);
                return;
            }
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("appid", c);
            jSONObject2.put("apkid", c2);
            jSONObject2.put("packagename", queryParameter);
            jSONObject2.put("versioncode", d);
            jSONObject2.put("grayversioncode", d2);
            jSONObject2.put("appstate", localApkState);
            if (AppConst.AppState.DOWNLOADED == localApkState) {
                jSONObject2.put("downpercent", 100);
            } else {
                jSONObject2.put("downpercent", 0);
            }
            String jSONObject3 = jSONObject2.toString();
            XLog.i(TAG, "[queryDownload] ---> ret = " + jSONObject3);
            response(str2, i, str, jSONObject3);
        } catch (Exception e) {
            responseFail(str2, i, str, -3);
        }
    }

    public void queryAppState(Uri uri, int i, String str, String str2) {
        try {
            String queryParameter = uri.getQueryParameter("pkgapkids");
            if (!TextUtils.isEmpty(queryParameter) && !TextUtils.isEmpty(str2)) {
                JSONArray jSONArray = new JSONArray(queryParameter);
                int length = jSONArray.length();
                JSONObject jSONObject = new JSONObject();
                for (int i2 = 0; i2 < length; i2++) {
                    JSONObject jSONObject2 = (JSONObject) jSONArray.get(i2);
                    JSONObject jSONObject3 = new JSONObject();
                    String string = jSONObject2.getString("packagename");
                    long c = ct.c(jSONObject2.getString("apkid"));
                    jSONObject3.put("packagename", string);
                    jSONObject3.put("apkid", c);
                    LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(string);
                    if (localApkInfo != null) {
                        jSONObject3.put("install", 1);
                        jSONObject3.put("appid", localApkInfo.mAppid);
                        jSONObject3.put("appName", localApkInfo.mAppName);
                        jSONObject3.put("verCode", localApkInfo.mVersionCode);
                        jSONObject3.put("verName", localApkInfo.mVersionName);
                        jSONObject3.put("grayversioncode", localApkInfo.mGrayVersionCode);
                        jSONObject3.put("manifestMd5", localApkInfo.manifestMd5);
                        AppUpdateInfo a2 = k.b().a(string);
                        if (a2 != null) {
                            jSONObject3.put("canUpdate", 1);
                            if (a2.m > 0) {
                                jSONObject3.put("saveByte", a2.j - a2.m);
                            } else {
                                jSONObject3.put("saveByte", 0);
                            }
                            DownloadInfo downloadInfo = null;
                            if (c > 0) {
                                downloadInfo = DownloadProxy.a().d(String.valueOf(c));
                            }
                            if (downloadInfo == null) {
                                downloadInfo = DownloadProxy.a().a(string, localApkInfo.mVersionCode, localApkInfo.mGrayVersionCode);
                            }
                            AppConst.AppState b = u.b(downloadInfo);
                            jSONObject3.put("appstate", b);
                            jSONObject3.put("downpercent", u.a(downloadInfo, b));
                        }
                    } else {
                        jSONObject3.put("install", 0);
                        DownloadInfo downloadInfo2 = null;
                        if (c > 0) {
                            downloadInfo2 = DownloadProxy.a().d(String.valueOf(c));
                        }
                        if (downloadInfo2 != null) {
                            AppConst.AppState b2 = u.b(downloadInfo2);
                            jSONObject3.put("appstate", b2);
                            jSONObject3.put("downpercent", u.a(downloadInfo2, b2));
                        }
                    }
                    jSONObject.put(string, jSONObject3);
                }
                if (jSONObject == null || jSONObject.length() <= 0) {
                    responseFail(str2, i, str, -2);
                    return;
                }
                response(str2, i, str, jSONObject.toString());
            }
        } catch (Exception e) {
            responseFail(str2, i, str, -3);
        }
    }

    public void refreshTicket(Uri uri, int i, String str, String str2) {
        this.isRefreshTicket = true;
        d.a().h();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.manager.SelfUpdateManager.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.assistant.manager.SelfUpdateManager.a(com.tencent.assistant.manager.SelfUpdateManager, com.tencent.assistant.localres.model.LocalApkInfo):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.assistant.manager.SelfUpdateManager.a(com.tencent.assistant.localres.model.LocalApkInfo, com.tencent.assistant.manager.SelfUpdateManager$SelfUpdateInfo):com.tencent.assistant.model.SimpleAppModel
      com.tencent.assistant.manager.SelfUpdateManager.a(boolean, boolean):void */
    public void checkSelfUpdate(Uri uri, int i, String str, String str2) {
        if (this.mSelfUpdateReqBundle == null) {
            this.mSelfUpdateReqBundle = new Bundle();
        }
        this.mSelfUpdateReqBundle.putInt("seqId", i);
        this.mSelfUpdateReqBundle.putString("method", str);
        this.mSelfUpdateReqBundle.putString("callbackFun", str2);
        SelfUpdateManager.a().a(false, true);
    }

    public void startDownload(Uri uri, int i, String str, String str2) {
        LocalApkInfo localApkInfo;
        DownloadInfo downloadInfo = null;
        long c = ct.c(uri.getQueryParameter("apkid"));
        int d = ct.d(uri.getQueryParameter("scene"));
        ct.d(uri.getQueryParameter("sourceScene"));
        String queryParameter = uri.getQueryParameter("packagename");
        int d2 = ct.d(uri.getQueryParameter("versioncode"));
        int d3 = ct.d(uri.getQueryParameter("grayversioncode"));
        if (c > 0) {
            downloadInfo = DownloadProxy.a().d(String.valueOf(c));
        }
        if (downloadInfo != null) {
            AppConst.AppState b = u.b(downloadInfo);
            if (b == AppConst.AppState.DOWNLOADED) {
                a.a().d(downloadInfo);
            } else if (b != AppConst.AppState.DOWNLOADING) {
                DownloadProxy.a().c(downloadInfo);
            }
            response(str2, i, str, Constants.STR_EMPTY);
        } else if (getLocalApkState(queryParameter, d2, d3) == AppConst.AppState.DOWNLOADED) {
            LocalApkInfo localApkInfo2 = ApkResourceManager.getInstance().getLocalApkInfo(queryParameter, d2, d3);
            if (localApkInfo2 != null) {
                localApkInfo = localApkInfo2;
            } else {
                localApkInfo = null;
            }
            DownloadInfo downloadInfo2 = new DownloadInfo();
            downloadInfo2.packageName = localApkInfo != null ? localApkInfo.mPackageName : null;
            downloadInfo2.versionCode = localApkInfo != null ? localApkInfo.mVersionCode : 0;
            downloadInfo2.scene = d > 0 ? d : STConst.ST_PAGE_FROM_WEBVIEW;
            String ddownloadTicket = ApkResourceManager.getInstance().getDdownloadTicket(localApkInfo);
            p.a().a(ddownloadTicket, localApkInfo.mPackageName, localApkInfo.mAppName, localApkInfo.mLocalFilePath, localApkInfo.mVersionCode, localApkInfo.signature, ddownloadTicket, localApkInfo.occupySize, false, downloadInfo2);
            response(str2, i, str, Constants.STR_EMPTY);
        } else {
            responseFail(str2, i, str, -2);
        }
    }

    public void pauseDownload(Uri uri, int i, String str, String str2) {
        long c = ct.c(uri.getQueryParameter("apkid"));
        if (c > 0) {
            a.a().b(String.valueOf(c));
            response(str2, i, str, Constants.STR_EMPTY);
            return;
        }
        responseFail(str2, i, str, -4);
    }

    public void createDownload(Uri uri, int i, String str, String str2) {
        try {
            SimpleDownloadInfo.DownloadState c = be.c();
            if (SimpleDownloadInfo.DownloadState.DOWNLOADING == c || SimpleDownloadInfo.DownloadState.QUEUING == c || SimpleDownloadInfo.DownloadState.INSTALLING == c) {
                Toast.makeText(this.mContext, this.mContext.getResources().getString(R.string.qube_wait_qube_install), 1).show();
                return;
            }
            long c2 = ct.c(uri.getQueryParameter("appid"));
            long c3 = ct.c(uri.getQueryParameter("apkid"));
            String queryParameter = uri.getQueryParameter("packagename");
            int d = ct.d(uri.getQueryParameter("versioncode"));
            int d2 = ct.d(uri.getQueryParameter("grayversioncode"));
            byte d3 = (byte) ct.d(uri.getQueryParameter("actionflag"));
            String queryParameter2 = uri.getQueryParameter("channelid");
            String queryParameter3 = uri.getQueryParameter("oplist");
            int d4 = ct.d(uri.getQueryParameter("scene"));
            int d5 = ct.d(uri.getQueryParameter("sourceScene"));
            String queryParameter4 = uri.getQueryParameter("via");
            int d6 = ct.d(uri.getQueryParameter("reCreate"));
            long c4 = ct.c(uri.getQueryParameter("searchId"));
            String queryParameter5 = uri.getQueryParameter("recommendId");
            String queryParameter6 = uri.getQueryParameter("hostpname");
            SimpleAppModel simpleAppModel = new SimpleAppModel();
            simpleAppModel.f1634a = c2;
            simpleAppModel.c = queryParameter;
            simpleAppModel.b = c3;
            simpleAppModel.ac = queryParameter2;
            simpleAppModel.g = d;
            simpleAppModel.Q = d3;
            simpleAppModel.ad = d2;
            if (c2 != 0 || !TextUtils.isEmpty(queryParameter)) {
                DownloadInfo downloadInfo = null;
                if (simpleAppModel.b > 0) {
                    downloadInfo = DownloadProxy.a().d(String.valueOf(simpleAppModel.b));
                }
                if (downloadInfo == null) {
                    downloadInfo = DownloadProxy.a().a(simpleAppModel.c, simpleAppModel.g, d2);
                }
                if ((downloadInfo != null && downloadInfo.needReCreateInfo(simpleAppModel)) || d6 == 1) {
                    DownloadProxy.a().b(downloadInfo.downloadTicket);
                    downloadInfo = null;
                }
                if (downloadInfo != null) {
                    u.a(downloadInfo, queryParameter3);
                    response(str2, i, str, getCreateDownloadSuccResString(simpleAppModel, downloadInfo));
                    return;
                }
                int a2 = this.getSimpleAppInfoEngine.a(simpleAppModel);
                j jVar = new j(i, a2, queryParameter3, simpleAppModel, str, str2, d4, d5, queryParameter4, null);
                jVar.k = c4;
                jVar.l = queryParameter6;
                if (!TextUtils.isEmpty(queryParameter5)) {
                    jVar.j = i.a(queryParameter5, 0);
                }
                this.wantQueryAppModelHolderMap.put(Integer.valueOf(a2), jVar);
                return;
            }
            responseFail(str2, i, str, -4);
        } catch (Exception e) {
            responseFail(str2, i, str, -3);
        }
    }

    public void deleteDownload(Uri uri, int i, String str, String str2) {
        e eVar = new e(this, uri, str2, i, str);
        eVar.titleRes = this.mContext.getResources().getString(R.string.downloaded_delete_confirm_title);
        eVar.contentRes = this.mContext.getResources().getString(R.string.downloaded_delete_confirm);
        eVar.rBtnTxtRes = this.mContext.getResources().getString(R.string.downloaded_delete_confirm_btn);
        com.tencent.assistant.utils.v.a(eVar);
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        DownloadInfo downloadInfo;
        DownloadInfo downloadInfo2;
        int i3;
        int i4 = STConst.ST_PAGE_FROM_WEBVIEW;
        j remove = this.wantQueryAppModelHolderMap.remove(Integer.valueOf(i));
        if (remove != null && appSimpleDetail != null) {
            SimpleAppModel a2 = u.a(appSimpleDetail);
            DownloadInfo a3 = DownloadProxy.a().a(a2);
            StatInfo statInfo = new StatInfo(a2.b, STConst.ST_PAGE_FROM_WEBVIEW, 0, null, 0);
            if (a3 == null || !a3.needReCreateInfo(a2)) {
                downloadInfo = a3;
            } else {
                DownloadProxy.a().b(a3.downloadTicket);
                downloadInfo = null;
            }
            if (downloadInfo == null) {
                DownloadInfo createDownloadInfo = DownloadInfo.createDownloadInfo(a2, statInfo);
                createDownloadInfo.scene = remove.g != 0 ? remove.g : 201004;
                if (createDownloadInfo.statInfo != null) {
                    StatInfo statInfo2 = createDownloadInfo.statInfo;
                    if (remove.g != 0) {
                        i3 = remove.g;
                    } else {
                        i3 = 201004;
                    }
                    statInfo2.scene = i3;
                    StatInfo statInfo3 = createDownloadInfo.statInfo;
                    if (remove.h != 0) {
                        i4 = remove.h;
                    } else if (!(this.mActivityRef == null || this.mActivityRef.get() == null)) {
                        i4 = ((BaseActivity) this.mActivityRef.get()).p();
                    }
                    statInfo3.sourceScene = i4;
                    createDownloadInfo.statInfo.callerVia = remove.i;
                    createDownloadInfo.statInfo.searchId = remove.k;
                    if (remove.j != null) {
                        createDownloadInfo.statInfo.recommendId = remove.j;
                    }
                    if (remove.l != null) {
                        createDownloadInfo.hostPackageName = remove.l;
                    }
                }
                AppConst.AppState localApkState = getLocalApkState(createDownloadInfo.packageName, createDownloadInfo.versionCode, createDownloadInfo.grayVersionCode);
                if (localApkState == AppConst.AppState.DOWNLOADED) {
                    createDownloadInfo.downloadState = SimpleDownloadInfo.DownloadState.SUCC;
                } else if (localApkState == AppConst.AppState.INSTALLED) {
                    createDownloadInfo.downloadState = SimpleDownloadInfo.DownloadState.INSTALLED;
                } else {
                    createDownloadInfo.downloadState = SimpleDownloadInfo.DownloadState.PAUSED;
                }
                createDownloadInfo.autoInstall = !u.a("2", remove.c);
                if (!u.a(createDownloadInfo, remove.c)) {
                    DownloadProxy.a().d(createDownloadInfo);
                    AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, createDownloadInfo));
                }
                downloadInfo2 = createDownloadInfo;
            } else {
                downloadInfo.updateDownloadInfoStatInfo(statInfo);
                u.a(downloadInfo, remove.c);
                downloadInfo2 = downloadInfo;
            }
            response(remove.e, remove.f1357a, remove.f, getCreateDownloadSuccResString(remove.d, downloadInfo2));
        } else if (remove != null) {
            responseFail(remove.e, remove.f1357a, remove.f, -1);
        }
    }

    public void onGetAppInfoFail(int i, int i2) {
        j remove = this.wantQueryAppModelHolderMap.remove(Integer.valueOf(i));
        if (remove != null) {
            responseFailWithData(remove.e, remove.f1357a, -6, getCreateDownloadSuccResString(remove.d, null));
        }
    }

    private String getCreateDownloadSuccResString(SimpleAppModel simpleAppModel, DownloadInfo downloadInfo) {
        long j = 0;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("appid", getValidLong(simpleAppModel.f1634a, downloadInfo == null ? 0 : downloadInfo.appId));
            jSONObject.put("apkid", getValidLong(simpleAppModel.b, downloadInfo == null ? 0 : downloadInfo.apkId));
            jSONObject.put("packagename", getValidString(simpleAppModel.c, downloadInfo == null ? null : downloadInfo.packageName));
            long j2 = (long) simpleAppModel.g;
            if (downloadInfo != null) {
                j = (long) downloadInfo.versionCode;
            }
            jSONObject.put("versioncode", (int) getValidLong(j2, j));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    private String getValidString(String str, String str2) {
        return !TextUtils.isEmpty(str) ? str : str2;
    }

    private long getValidLong(long j, long j2) {
        return j != 0 ? j : j2;
    }

    public void gray(Uri uri, int i, String str, String str2) {
        if (this.authSeq > 0) {
            responseFail(str2, i, str, -100);
            return;
        }
        try {
            int d = ct.d(uri.getQueryParameter(SocialConstants.PARAM_TYPE));
            long c = ct.c(uri.getQueryParameter("appid"));
            long c2 = ct.c(uri.getQueryParameter("apkid"));
            String queryParameter = uri.getQueryParameter("packagename");
            int d2 = ct.d(uri.getQueryParameter("versioncode"));
            SimpleAppModel simpleAppModel = new SimpleAppModel();
            simpleAppModel.f1634a = c;
            simpleAppModel.b = c2;
            simpleAppModel.c = queryParameter;
            simpleAppModel.Q = (byte) ct.d(uri.getQueryParameter("actionflag"));
            simpleAppModel.g = d2;
            this.authSeq = this.authManager.a(d, simpleAppModel, this.mActivityRef.get());
            if (this.authSeq == -1) {
                responseFail(str2, i, str, -4);
            } else {
                this.authFuction = str2;
            }
        } catch (Exception e) {
            responseFail(str2, i, str, -3);
        }
    }

    public void loadByAnotherWebBrowser(Uri uri, int i, String str, String str2) {
        if (this.mActivityRef.get() != null && (this.mActivityRef.get() instanceof BrowserActivity)) {
            ((BrowserActivity) this.mActivityRef.get()).v();
            response(str2, i, str, Constants.STR_EMPTY);
        }
    }

    public void getVersion(Uri uri, int i, String str, String str2) {
        response(str2, i, str, String.valueOf(3));
    }

    public void isInterfaceReady(Uri uri, int i, String str, String str2) {
        if (a.a().a(this.currentUrl)) {
            response(str2, i, str, "true");
            return;
        }
        this.getDomainCapabilityEngine.a(this.currentUrl);
        response(str2, i, str, "false");
    }

    public void scrollToTop(Uri uri, int i, String str, String str2) {
        if (this.mWebView != null) {
            this.mWebView.scrollTo(0, 0);
        }
    }

    public void toast(Uri uri, int i, String str, String str2) {
        int d = ct.d(uri.getQueryParameter("duration"));
        Toast.makeText(this.mContext, uri.getQueryParameter("text"), d).show();
    }

    public void bookOp(Uri uri, int i, String str, String str2) {
        int d = ct.d(uri.getQueryParameter(AuthActivity.ACTION_KEY));
        String queryParameter = uri.getQueryParameter("id");
        String queryParameter2 = uri.getQueryParameter("name");
        String queryParameter3 = uri.getQueryParameter("author");
        int d2 = ct.d(uri.getQueryParameter("maxchap"));
        int d3 = ct.d(uri.getQueryParameter("finish"));
        long d4 = (long) ct.d(uri.getQueryParameter("uin"));
        int d5 = ct.d(uri.getQueryParameter("pay_requestcode"));
        int d6 = ct.d(uri.getQueryParameter("chap"));
        switch (d) {
            case 1000:
                f.a().a(getActivityContextPri(), queryParameter, queryParameter2, d2, d6, d3, queryParameter3);
                return;
            case 1001:
                f.a().a(getActivityContextPri(), queryParameter, queryParameter2, d2, d3);
                return;
            case 1002:
                f.a().a(getActivityContextPri(), queryParameter, queryParameter2);
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING:
                f.a().a(getActivityContextPri(), queryParameter);
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START:
                if (d4 == 0) {
                    d4 = d.a().p();
                }
                f.a().a(getActivityContextPri(), d4, d5);
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE:
                f.a().b(getActivityContextPri(), queryParameter, queryParameter2, d2, d3, queryParameter3);
                return;
            default:
                return;
        }
    }

    public void openQQReader(Uri uri, int i, String str, String str2) {
        String queryParameter = uri.getQueryParameter("openUrl");
        Bundle bundle = new Bundle();
        bundle.putString("OPENURL", queryParameter);
        r.a("com.qq.reader", bundle);
    }

    private Context getActivityContextPri() {
        if (this.mActivityRef.get() != null) {
            return this.mActivityRef.get();
        }
        return this.mContext;
    }

    public void clearNumEx(Uri uri, int i, String str, String str2) {
        XLog.i("xjp", "****** [JsBridge] : clearNumEx ********");
        com.tencent.assistant.usercenter.b.b(ct.a(uri.getQueryParameter("groupid"), 0), ct.a(uri.getQueryParameter("index"), 0));
    }

    public void clearPrompt(Uri uri, int i, String str, String str2) {
        XLog.i("xjp", "****** [JsBridge] : clearPrompt ********");
        com.tencent.assistant.usercenter.b.a(ct.a(uri.getQueryParameter("groupid"), 0), ct.a(uri.getQueryParameter("index"), 0));
    }

    public void setClipboard(Uri uri, int i, String str, String str2) {
        String queryParameter = uri.getQueryParameter("content");
        if (Build.VERSION.SDK_INT > 10) {
            ClipboardManager clipboardManager = (ClipboardManager) this.mContext.getSystemService("clipboard");
            if (clipboardManager != null) {
                if (TextUtils.isEmpty(queryParameter)) {
                    clipboardManager.setPrimaryClip(ClipData.newPlainText(null, Constants.STR_EMPTY));
                } else {
                    clipboardManager.setPrimaryClip(ClipData.newPlainText(null, queryParameter));
                }
                response(str2, i, str, Constants.STR_EMPTY);
                return;
            }
            responseFail(str2, i, str, -2);
            return;
        }
        android.text.ClipboardManager clipboardManager2 = (android.text.ClipboardManager) this.mContext.getSystemService("clipboard");
        if (clipboardManager2 != null) {
            if (TextUtils.isEmpty(queryParameter)) {
                clipboardManager2.setText(Constants.STR_EMPTY);
            } else {
                clipboardManager2.setText(queryParameter);
            }
            response(str2, i, str, Constants.STR_EMPTY);
            return;
        }
        responseFail(str2, i, str, -2);
    }

    public void getClipboard(Uri uri, int i, String str, String str2) {
        String str3;
        String str4 = Constants.STR_EMPTY;
        if (Build.VERSION.SDK_INT > 10) {
            ClipboardManager clipboardManager = (ClipboardManager) this.mContext.getSystemService("clipboard");
            if (clipboardManager != null) {
                if (clipboardManager.hasPrimaryClip()) {
                    str3 = clipboardManager.getPrimaryClip().getItemAt(0).getText().toString();
                } else {
                    str3 = str4;
                }
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("content", str3);
                } catch (Exception e) {
                    e.printStackTrace();
                    responseFail(str2, i, str, -3);
                }
                response(str2, i, str, jSONObject.toString());
                return;
            }
            responseFail(str2, i, str, -2);
            return;
        }
        android.text.ClipboardManager clipboardManager2 = (android.text.ClipboardManager) this.mContext.getSystemService("clipboard");
        if (clipboardManager2 != null) {
            if (clipboardManager2.hasText()) {
                str4 = clipboardManager2.getText().toString();
            }
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("content", str4);
            } catch (Exception e2) {
                e2.printStackTrace();
                responseFail(str2, i, str, -3);
            }
            response(str2, i, str, jSONObject2.toString());
            return;
        }
        responseFail(str2, i, str, -2);
    }

    public static SharedPreferences getPreferences() {
        return AstApp.i().getSharedPreferences(JS_CONFIG, 0);
    }

    public void saveData(Uri uri, int i, String str, String str2) {
        String queryParameter = uri.getQueryParameter("key");
        String queryParameter2 = uri.getQueryParameter("value");
        XLog.i(TAG, "[saveData] ---> key=" + queryParameter + " , value=" + queryParameter2);
        if (!TextUtils.isEmpty(queryParameter) && !TextUtils.isEmpty(queryParameter2)) {
            SharedPreferences.Editor edit = getPreferences().edit();
            edit.putString(queryParameter, queryParameter2);
            edit.commit();
        }
    }

    public void getData(Uri uri, int i, String str, String str2) {
        String queryParameter = uri.getQueryParameter("key");
        XLog.i(TAG, "[getData] ---> key=" + queryParameter);
        if (!TextUtils.isEmpty(queryParameter) && !TextUtils.isEmpty(str2)) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(queryParameter, getPreferences().getString(queryParameter, Constants.STR_EMPTY));
            } catch (Exception e) {
                e.printStackTrace();
                responseFail(str2, i, str, -3);
            }
            XLog.i(TAG, "[getData] ---> result=" + jSONObject.toString());
            response(str2, i, str, jSONObject.toString());
        }
    }

    public void playVideoByApp(Uri uri, int i, String str, String str2) {
        String queryParameter = uri.getQueryParameter("activityClassName");
        String queryParameter2 = uri.getQueryParameter("extra");
        String queryParameter3 = uri.getQueryParameter("videoSrc");
        String queryParameter4 = uri.getQueryParameter("intentUri");
        XLog.i(TAG, "[playVideoByApp] ---> activityClassName:" + queryParameter);
        XLog.i(TAG, "[playVideoByApp] ---> extra:" + queryParameter2);
        XLog.i(TAG, "[playVideoByApp] ---> videoSrc:" + queryParameter3);
        XLog.i(TAG, "[playVideoByApp] ---> intentUri:" + queryParameter4);
        Bundle bundle = new Bundle();
        try {
            if (!TextUtils.isEmpty(queryParameter2)) {
                JSONObject jSONObject = new JSONObject(queryParameter2);
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    Object opt = jSONObject.opt(next);
                    if (opt instanceof Integer) {
                        bundle.putInt(next, Integer.valueOf(String.valueOf(opt)).intValue());
                    } else if (opt instanceof String) {
                        bundle.putString(next, String.valueOf(opt));
                    } else if (opt instanceof Long) {
                        bundle.putLong(next, Long.valueOf(String.valueOf(opt)).longValue());
                    } else if (opt instanceof Byte) {
                        bundle.putByte(next, Byte.valueOf(String.valueOf(opt)).byteValue());
                    } else if (opt instanceof Short) {
                        bundle.putShort(next, Short.valueOf(String.valueOf(opt)).shortValue());
                    } else if (opt instanceof Double) {
                        bundle.putDouble(next, Double.valueOf(String.valueOf(opt)).doubleValue());
                    } else if (opt instanceof Float) {
                        bundle.putFloat(next, Float.valueOf(String.valueOf(opt)).floatValue());
                    }
                }
            }
        } catch (Exception e) {
            try {
                e.printStackTrace();
            } catch (Exception e2) {
                e2.printStackTrace();
                responseFail(str2, i, str, -3);
                return;
            }
        }
        if (!TextUtils.isEmpty(queryParameter)) {
            if (r.b(queryParameter, bundle)) {
                response(str2, i, str, Constants.STR_EMPTY);
            } else {
                responseFail(str2, i, str, -2);
            }
        } else if (!TextUtils.isEmpty(queryParameter4)) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(queryParameter4));
            intent.setFlags(268435456);
            AstApp.i().startActivity(intent);
            response(str2, i, str, Constants.STR_EMPTY);
        }
    }

    public void playVideoByWebView(Uri uri, int i, String str, String str2) {
        String queryParameter = uri.getQueryParameter("videoUrl");
        if (TextUtils.isEmpty(queryParameter)) {
            XLog.i(TAG, "[playVideoByWebView] ---> videoUrl 为空");
            return;
        }
        XLog.i(TAG, "[playVideoByWebView] ---> videoUrl is:" + queryParameter);
        String queryParameter2 = uri.getQueryParameter("videoSrc");
        XLog.i(TAG, "[playVideoByWebView] ---> videoSrc is:" + queryParameter2);
        try {
            String str3 = "tmast://videoplay?url=" + URLEncoder.encode(queryParameter, "utf-8");
            Bundle bundle = new Bundle();
            if (!TextUtils.isEmpty(queryParameter2)) {
                bundle.putString("com.tencent.assistant.videoSrc", queryParameter2);
            }
            com.tencent.assistant.link.b.a(AstApp.i(), bundle, new ActionUrl(str3, 0));
            response(str2, i, str, Constants.STR_EMPTY);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            XLog.i(TAG, "[playVideoByWebView] ---> UnsupportedEncodingException");
        }
    }

    public void playLocalVideo(Uri uri, int i, String str, String str2) {
        String str3;
        String queryParameter = uri.getQueryParameter("channelid");
        String queryParameter2 = uri.getQueryParameter("videoId");
        h hVar = null;
        if (!TextUtils.isEmpty(queryParameter2)) {
            hVar = o.c().c(queryParameter2);
        }
        if (hVar != null) {
            XLog.i(TAG, "[playLocalVideo] ---> videoDownInfo.savePath : " + hVar.h);
            str3 = hVar.h;
        } else {
            str3 = Constants.STR_EMPTY;
        }
        if (TextUtils.isEmpty(queryParameter) || TextUtils.isEmpty(str3)) {
            XLog.i(TAG, "[playLocalVideo] ---> channelid 或 videoPath参数为空");
            return;
        }
        XLog.i(TAG, "[playLocalVideo] ---> videoSrc is:" + uri.getQueryParameter("videoSrc"));
        XLog.i(TAG, "[playLocalVideo] ---> channelid is:" + queryParameter);
        XLog.i(TAG, "[playLocalVideo] ---> videoPath is:" + str3);
        Intent intent = new Intent("android.intent.action.START_SOHUTV");
        intent.putExtra("channelid", queryParameter);
        intent.putExtra("videopath", str3);
        intent.putExtra("appname", "应用宝");
        intent.setFlags(268435456);
        AstApp.i().startActivity(intent);
        response(str2, i, str, Constants.STR_EMPTY);
    }

    public void downloadVideo(Uri uri, int i, String str, String str2) {
        XLog.i(TAG, "[downloadVideo] ---> start");
        String queryParameter = uri.getQueryParameter("videoId");
        String queryParameter2 = uri.getQueryParameter("downloadUrl");
        String queryParameter3 = uri.getQueryParameter("coverUrl");
        String queryParameter4 = uri.getQueryParameter("fileSize");
        String queryParameter5 = uri.getQueryParameter("name");
        String queryParameter6 = uri.getQueryParameter("volume");
        String queryParameter7 = uri.getQueryParameter("videoSrc");
        String queryParameter8 = uri.getQueryParameter("videoSaveName");
        String queryParameter9 = uri.getQueryParameter("openPackageName");
        String queryParameter10 = uri.getQueryParameter("openActivity");
        String queryParameter11 = uri.getQueryParameter("openUri");
        String queryParameter12 = uri.getQueryParameter("minVersionCode");
        String queryParameter13 = uri.getQueryParameter("playerName");
        XLog.i(TAG, "[downloadVideo] ---> videoId : " + queryParameter);
        XLog.i(TAG, "[downloadVideo] ---> downloadUrl : " + queryParameter2);
        XLog.i(TAG, "[downloadVideo] ---> coverUrl : " + queryParameter3);
        XLog.i(TAG, "[downloadVideo] ---> fileSize : " + queryParameter4);
        XLog.i(TAG, "[downloadVideo] ---> name : " + queryParameter5);
        XLog.i(TAG, "[downloadVideo] ---> volume : " + queryParameter6);
        XLog.i(TAG, "[downloadVideo] ---> videoSrc : " + queryParameter7);
        XLog.i(TAG, "[downloadVideo] ---> videoSaveName : " + queryParameter8);
        XLog.i(TAG, "[downloadVideo] ---> openPackageName : " + queryParameter9);
        XLog.i(TAG, "[downloadVideo] ---> openActivity : " + queryParameter10);
        XLog.i(TAG, "[downloadVideo] ---> openUri : " + queryParameter11);
        XLog.i(TAG, "[downloadVideo] ---> minVersionCode : " + queryParameter12);
        XLog.i(TAG, "[downloadVideo] ---> playerName : " + queryParameter13);
        if (!TextUtils.isEmpty(queryParameter) && !TextUtils.isEmpty(queryParameter2) && !TextUtils.isEmpty(queryParameter8)) {
            h c = o.c().c(queryParameter);
            if (c == null) {
                c = new h();
                c.c = queryParameter;
                c.b = queryParameter2;
                c.p = queryParameter3;
                c.d = 0;
                if (!TextUtils.isEmpty(queryParameter4)) {
                    try {
                        c.d = Long.parseLong(queryParameter4);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
                c.n = queryParameter5;
                c.q = queryParameter7;
                c.f3307a = queryParameter8;
                c.r = queryParameter9;
                c.s = queryParameter10;
                c.t = queryParameter11;
                c.u = 0;
                if (!TextUtils.isEmpty(queryParameter12)) {
                    try {
                        c.u = Integer.parseInt(queryParameter12);
                    } catch (NumberFormatException e2) {
                        e2.printStackTrace();
                    }
                }
                c.v = queryParameter13;
                XLog.i(TAG, "[downloadVideo] ---> videoDownInfo : " + c.toString());
            }
            if (o.c().b(c)) {
                XLog.i(TAG, "[downloadVideo] ---> startDownload succeeded");
            } else {
                XLog.i(TAG, "[downloadVideo] ---> startDownload failed");
            }
            XLog.i(TAG, "[downloadVideo] ---> end");
        }
    }

    public void pauseDownloadVideo(Uri uri, int i, String str, String str2) {
        XLog.i(TAG, "[pauseDownloadVideo] ---> start");
        String queryParameter = uri.getQueryParameter("videoId");
        XLog.i(TAG, "[pauseDownloadVideo] ---> videoId : " + queryParameter);
        if (!TextUtils.isEmpty(queryParameter)) {
            o.c().a(queryParameter);
            XLog.i(TAG, "[pauseDownloadVideo] ---> end");
        }
    }

    public void deleteVideo(Uri uri, int i, String str, String str2) {
        XLog.i(TAG, "[deleteVideo] ---> start");
        String queryParameter = uri.getQueryParameter("videoId");
        XLog.i(TAG, "[deleteVideo] ---> videoId : " + queryParameter);
        if (!TextUtils.isEmpty(queryParameter)) {
            boolean z = false;
            h c = o.c().c(queryParameter);
            if (c != null && AbstractDownloadInfo.DownState.SUCC == c.i) {
                z = true;
            }
            if (o.c().a(queryParameter, z)) {
                XLog.i(TAG, "[deleteVideo] ---> deleteVideo succeeded");
            } else {
                XLog.i(TAG, "[deleteVideo] ---> deleteVideo failed");
            }
            XLog.i(TAG, "[deleteVideo] ---> end");
        }
    }

    public void queryVideoDownloadState(Uri uri, int i, String str, String str2) {
        XLog.i(TAG, "[queryVideoDownloadState] ---> start");
        String queryParameter = uri.getQueryParameter("videoId");
        XLog.i(TAG, "[queryVideoDownloadState] ---> videoId : " + queryParameter);
        if (!TextUtils.isEmpty(queryParameter)) {
            h c = o.c().c(queryParameter);
            XLog.i(TAG, "[queryVideoDownloadState] ---> videoDownInfo : " + c);
            if (c != null) {
                try {
                    String b = de.b(c);
                    XLog.i(TAG, "[queryVideoDownloadState] ---> str : " + b);
                    response(VIDEO_DOWNLOAD_STATE_CALLBACK, 0, null, b);
                } catch (JSONException e) {
                    e.printStackTrace();
                    responseFail(str2, i, str, -3);
                }
            } else {
                responseFail(str2, i, str, -2);
            }
            XLog.i(TAG, "[queryVideoDownloadState] ---> end");
        }
    }

    public void getVideoDownloadInfo(Uri uri, int i, String str, String str2) {
        XLog.i(TAG, "[getVideoDownloadInfo] ---> start");
        String queryParameter = uri.getQueryParameter("videoId");
        List arrayList = new ArrayList();
        if (TextUtils.isEmpty(queryParameter)) {
            arrayList = o.c().a();
        } else {
            h c = o.c().c(queryParameter);
            if (c != null) {
                arrayList.add(c);
            }
        }
        try {
            String a2 = de.a(arrayList);
            XLog.i(TAG, "[getVideoDownloadInfo] ---> str : " + a2);
            response(str2, i, str, a2);
        } catch (JSONException e) {
            e.printStackTrace();
            responseFail(str2, i, str, -3);
        }
        XLog.i(TAG, "[getVideoDownloadInfo] ---> end");
    }

    public void getBrowserSignature(Uri uri, int i, String str, String str2) {
        XLog.i(TAG, "[getBrowserSignature] ---> start");
        String queryParameter = uri.getQueryParameter("vid");
        if (TextUtils.isEmpty(queryParameter)) {
            XLog.i(TAG, "[getBrowserSignature] ---> vid is null");
            return;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("key", "++01" + de.a(queryParameter));
            jSONObject.put("ver", "4.1");
            jSONObject.put(Constants.PARAM_PLATFORM, "91001");
            response(str2, i, str, jSONObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            responseFail(str2, i, str, -3);
        }
        XLog.i(TAG, "[getBrowserSignature] ---> end");
    }

    public void openQubeTheme(Uri uri, int i, String str, String str2) {
        XLog.i(TAG, "[openQubeTheme] ---> start");
        String queryParameter = uri.getQueryParameter("packageName");
        if (TextUtils.isEmpty(queryParameter)) {
            XLog.i(TAG, "[openQubeTheme] ---> packageName is null");
            return;
        }
        if (ce.c(queryParameter)) {
            XLog.i(TAG, "[openQubeTheme] ---> " + queryParameter + " is installed");
            try {
                Intent intent = new Intent();
                intent.setClassName("com.tencent.qlauncher", "com.tencent.qlauncher.home.Launcher");
                intent.putExtra("theme_packageName", queryParameter);
                intent.setFlags(268435456);
                this.mContext.startActivity(intent);
                response(str2, i, str, Constants.STR_EMPTY);
            } catch (Exception e) {
                e.printStackTrace();
                responseFail(str2, i, str, -3);
            }
        } else {
            XLog.i(TAG, "[openQubeTheme] ---> " + queryParameter + " is not installed");
            this.localThemeSet = ce.a();
            if (this.localThemeSet != null) {
                Iterator<HashMap<String, String>> it = this.localThemeSet.iterator();
                while (true) {
                    if (it == null || !it.hasNext()) {
                        break;
                    }
                    HashMap next = it.next();
                    if (next != null && queryParameter.equals(next.get("packageName"))) {
                        XLog.i(TAG, "[openQubeTheme] ---> start installQubeTheme");
                        int a2 = ce.a(next);
                        XLog.i(TAG, "[openQubeTheme] ---> nRet = " + a2);
                        if (a2 == 0) {
                            try {
                                Intent intent2 = new Intent();
                                intent2.setClassName("com.tencent.qlauncher", "com.tencent.qlauncher.home.Launcher");
                                intent2.putExtra("theme_packageName", queryParameter);
                                intent2.setFlags(268435456);
                                this.mContext.startActivity(intent2);
                                response(str2, i, str, Constants.STR_EMPTY);
                            } catch (Exception e2) {
                                e2.printStackTrace();
                                responseFail(str2, i, str, -3);
                            }
                        }
                    }
                }
            }
        }
        XLog.i(TAG, "[openQubeTheme] ---> end");
    }

    public void deleteLocalTheme(Uri uri, int i, String str, String str2) {
        boolean z;
        String queryParameter = uri.getQueryParameter("packageName");
        if (TextUtils.isEmpty(queryParameter)) {
            XLog.i(TAG, "[deleteLocalTheme] ---> packageName is null");
            return;
        }
        if (this.localThemeSet != null) {
            Iterator<HashMap<String, String>> it = this.localThemeSet.iterator();
            while (true) {
                if (it == null || !it.hasNext()) {
                    break;
                }
                HashMap next = it.next();
                if (next != null && queryParameter.equals(next.get("packageName"))) {
                    File file = new File((String) next.get("themeFilePath"));
                    XLog.i(TAG, "[deleteLocalTheme] ---> themeFilePath = " + ((String) next.get("themeFilePath")));
                    if (file.exists()) {
                        boolean delete = file.delete();
                        if (delete) {
                            ce.c(next);
                            z = delete;
                        } else {
                            z = delete;
                        }
                    }
                }
            }
        }
        z = true;
        if (z) {
            Toast.makeText(this.mContext, "删除成功", 0).show();
        } else {
            Toast.makeText(this.mContext, "删除失败", 0).show();
        }
        response(str2, i, str, Constants.STR_EMPTY);
    }

    public void getAllLocalThemes(Uri uri, int i, String str, String str2) {
        this.localThemeSet = ce.a();
        String h = be.h();
        XLog.e(TAG, "strCurUsedThemePkgName : " + h);
        try {
            String a2 = ce.a(this.localThemeSet, h);
            XLog.i(TAG, "[getLocalQubeThemeJsonString] ---> str : " + a2);
            response(str2, i, str, a2);
        } catch (JSONException e) {
            e.printStackTrace();
            responseFail(str2, i, str, -3);
        }
    }

    public void webViewCompatibilityReport(Uri uri, int i, String str, String str2) {
        String str3;
        String queryParameter = uri.getQueryParameter("canPlayType1");
        String queryParameter2 = uri.getQueryParameter("canPlayType2");
        String queryParameter3 = uri.getQueryParameter("canPlayType3");
        TreeMap treeMap = new TreeMap();
        treeMap.put("B1", Build.VERSION.SDK);
        treeMap.put("B2", Build.VERSION.RELEASE);
        treeMap.put("B3", Build.MODEL);
        if (TextUtils.isEmpty(queryParameter)) {
            queryParameter = "NoSupport";
        }
        treeMap.put("B4", queryParameter);
        treeMap.put("B5", TextUtils.isEmpty(queryParameter2) ? "NoSupport" : queryParameter2);
        if (TextUtils.isEmpty(queryParameter3)) {
            str3 = "NoSupport";
        } else {
            str3 = queryParameter3;
        }
        treeMap.put("B6", str3);
        treeMap.put("B7", Global.getPhoneGuidAndGen());
        treeMap.put("B8", Global.getQUAForBeacon());
        treeMap.put("B9", t.g());
        XLog.d(TAG, "[webViewCompatibilityReport] ---> params : " + treeMap.toString());
        if (com.tencent.beacon.event.a.a("webViewCompatibilityReport", true, -1, -1, treeMap, true)) {
            XLog.d(TAG, "[webViewCompatibilityReport] ---> report succeed");
            getPreferences().edit().putBoolean(WB_REPORT, true).commit();
        }
    }

    public void addAppLinkActionTask(Uri uri, int i, String str, String str2) {
        String queryParameter = uri.getQueryParameter("packageName");
        String queryParameter2 = uri.getQueryParameter(CommentDetailTabView.PARAMS_VERSION_CODE);
        String queryParameter3 = uri.getQueryParameter("actionTask");
        if (!TextUtils.isEmpty(queryParameter) && !TextUtils.isEmpty(queryParameter2) && !TextUtils.isEmpty(queryParameter3)) {
            com.tencent.assistant.link.a.a.a().a(queryParameter, queryParameter2, queryParameter3);
        }
    }

    public void deteleAppLinkActionTask(Uri uri, int i, String str, String str2) {
        String queryParameter = uri.getQueryParameter("packageName");
        String queryParameter2 = uri.getQueryParameter(CommentDetailTabView.PARAMS_VERSION_CODE);
        if (!TextUtils.isEmpty(queryParameter) && !TextUtils.isEmpty(queryParameter2)) {
            com.tencent.assistant.link.a.a.a().a(queryParameter, queryParameter2);
        }
    }

    public void openNewWindow(Uri uri, int i, String str, String str2) {
        Intent intent;
        Context context;
        String queryParameter = uri.getQueryParameter(SocialConstants.PARAM_URL);
        if ("1".equals(uri.getQueryParameter("browser"))) {
            getActivityContextPri().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(queryParameter)));
            return;
        }
        int i2 = STConst.ST_PAGE_MY_APPBAR;
        try {
            String queryParameter2 = uri.getQueryParameter("mode");
            String queryParameter3 = uri.getQueryParameter("accelerate");
            String queryParameter4 = uri.getQueryParameter("goback");
            String queryParameter5 = uri.getQueryParameter("supportZoom");
            if (this.mActivityRef == null || this.mActivityRef.get() == null) {
                context = AstApp.i();
                intent = new Intent(AstApp.i(), BrowserActivity.class);
                intent.addFlags(268435456);
            } else {
                context = this.mActivityRef.get();
                int f = ((BaseActivity) this.mActivityRef.get()).f();
                intent = new Intent(this.mActivityRef.get(), BrowserActivity.class);
                i2 = f;
            }
            intent.putExtra("com.tencent.assistant.BROWSER_URL", queryParameter);
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
            if (!TextUtils.isEmpty(queryParameter2) && (queryParameter2.equals("0") || queryParameter2.equals("1"))) {
                intent.putExtra("com.tencent.assistant.activity.BROWSER_TYPE", queryParameter2);
            }
            if (!TextUtils.isEmpty(queryParameter3) && (queryParameter3.equals("0") || queryParameter3.equals("1"))) {
                intent.putExtra("com.tencent.assistant.activity.BROWSER_ACCELERATE", queryParameter3);
            }
            if (!TextUtils.isEmpty(queryParameter5) && (queryParameter5.equals("0") || queryParameter5.equals("1"))) {
                intent.putExtra("suport.zoom", queryParameter5);
            }
            if (!TextUtils.isEmpty(queryParameter4)) {
                intent.putExtra("goback", queryParameter4);
            }
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPics(Uri uri, int i, String str, String str2) {
        int i2 = 0;
        int a2 = ct.a(uri.getQueryParameter("position"), 0);
        String queryParameter = uri.getQueryParameter("urls");
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONArray(queryParameter);
            int length = jSONArray.length();
            for (int i3 = 0; i3 < length; i3++) {
                arrayList.add(jSONArray.optString(i3));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (arrayList.size() == 0) {
            arrayList.add("about:blank");
        }
        if (a2 >= 0 && a2 <= arrayList.size()) {
            i2 = a2;
        }
        Intent intent = new Intent(this.mActivityRef.get(), ShowPictureActivity.class);
        intent.putStringArrayListExtra("picUrls", arrayList);
        intent.putExtra("startPos", i2);
        this.mActivityRef.get().startActivity(intent);
    }

    public void openFileChooser(Uri uri, int i, String str, String str2) {
        if (!(this.mActivityRef.get() instanceof BrowserActivity)) {
            responseFail(FILE_CHOOSER_CALLBACK_FUNCTION_NAME, 0, null, -7);
        } else {
            ((BrowserActivity) this.mActivityRef.get()).B();
        }
    }

    public void showErrorPage(Uri uri, int i, String str, String str2) {
        if (this.mwebViewContainRef != null && this.mwebViewContainRef.get() != null) {
            int d = ct.d(uri.getQueryParameter("flag"));
            try {
                Method declaredMethod = this.mwebViewContainRef.get().getClass().getDeclaredMethod("showErrorPage", Boolean.TYPE);
                if (d == 0) {
                    declaredMethod.invoke(this.mwebViewContainRef.get(), false);
                }
                if (d == 1) {
                    declaredMethod.invoke(this.mwebViewContainRef.get(), true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void report(Uri uri, int i, String str, String str2) {
        XLog.i(TAG, "[report] ---> uri : " + uri.toString());
        STPageInfo q = this.mActivityRef.get() instanceof BaseActivity ? ((BaseActivity) this.mActivityRef.get()).q() : new STPageInfo();
        int i2 = q.f3358a;
        int i3 = q.c;
        int a2 = ct.a(uri.getQueryParameter("scene"), i2);
        int a3 = ct.a(uri.getQueryParameter("sourceScene"), i3);
        String queryParameter = uri.getQueryParameter("isImmediately");
        String queryParameter2 = uri.getQueryParameter("status");
        String queryParameter3 = uri.getQueryParameter("slotId");
        String queryParameter4 = uri.getQueryParameter("sourceSceneSlotId");
        long c = ct.c(uri.getQueryParameter("appid"));
        String queryParameter5 = uri.getQueryParameter("packageName");
        String queryParameter6 = uri.getQueryParameter("recommendId");
        long c2 = ct.c(uri.getQueryParameter("searchId"));
        if (TextUtils.isEmpty(queryParameter4)) {
            queryParameter4 = STConst.ST_DEFAULT_SLOT;
        }
        STInfoV2 sTInfoV2 = new STInfoV2(a2, queryParameter3, a3, queryParameter4, ct.a(uri.getQueryParameter(AuthActivity.ACTION_KEY), -1));
        sTInfoV2.status = queryParameter2;
        sTInfoV2.extraData = uri.getQueryParameter("params");
        sTInfoV2.searchId = c2;
        sTInfoV2.appId = c;
        if (!TextUtils.isEmpty(queryParameter5)) {
            sTInfoV2.packageName = queryParameter5;
        }
        if (!TextUtils.isEmpty(queryParameter6)) {
            sTInfoV2.recommendId = i.a(queryParameter6, 0);
        }
        if ("1".equals(queryParameter)) {
            sTInfoV2.isImmediately = true;
        }
        com.tencent.assistantv2.st.k.a(sTInfoV2);
    }

    public void share(Uri uri, int i, String str, String str2) {
        String str3;
        if (!(this.mActivityRef.get() instanceof BrowserActivity)) {
            responseFail(SHARE_CALLBACK_FUNCTION_NAME, 0, null, -7);
            return;
        }
        String queryParameter = uri.getQueryParameter("title");
        String queryParameter2 = uri.getQueryParameter("summary");
        String queryParameter3 = uri.getQueryParameter("iconUrl");
        String queryParameter4 = uri.getQueryParameter("jumpUrl");
        String queryParameter5 = uri.getQueryParameter("message");
        if ("1".equals(uri.getQueryParameter("useDefaultIcon"))) {
            queryParameter3 = "http://shp.qpic.cn/ma_icon/0/icon_5848_17266235_1390032639/72";
        }
        if (TextUtils.isEmpty(queryParameter4) || TextUtils.isEmpty(queryParameter3)) {
            responseFail(SHARE_CALLBACK_FUNCTION_NAME, 0, null, -4);
            return;
        }
        String queryParameter6 = uri.getQueryParameter("appBarInfo");
        int a2 = ct.a(uri.getQueryParameter(SocialConstants.PARAM_TYPE), 0);
        ShareBaseModel shareBaseModel = new ShareBaseModel();
        shareBaseModel.f1633a = queryParameter;
        shareBaseModel.b = queryParameter2;
        shareBaseModel.c = queryParameter3;
        shareBaseModel.d = queryParameter4;
        if (TextUtils.isEmpty(queryParameter5)) {
            str3 = this.mContext.getString(R.string.share_specail_app_default_local_content);
        } else {
            str3 = queryParameter5;
        }
        shareBaseModel.f = str3;
        ((BrowserActivity) this.mActivityRef.get()).a(shareBaseModel);
        com.tencent.assistant.g.o.a(queryParameter6);
        switch (a2) {
            case 1:
                if (!ap.b()) {
                    responseFail(SHARE_CALLBACK_FUNCTION_NAME, 0, null, -7);
                    return;
                } else if (d.a().u()) {
                    ((BrowserActivity) this.mActivityRef.get()).w();
                    return;
                } else {
                    Toast.makeText(AstApp.i().getApplicationContext(), (int) R.string.need_install_qq, 0).show();
                    responseFailWithData(SHARE_CALLBACK_FUNCTION_NAME, 0, -8, a2 + Constants.STR_EMPTY);
                    return;
                }
            case 2:
                ((BrowserActivity) this.mActivityRef.get()).x();
                return;
            case 3:
                if (WXAPIFactory.createWXAPI(AstApp.i().getApplicationContext(), "wx3909f6add1206543", false).isWXAppInstalled()) {
                    ((BrowserActivity) this.mActivityRef.get()).y();
                    return;
                }
                Toast.makeText(AstApp.i().getApplicationContext(), (int) R.string.need_install_wx, 0).show();
                responseFailWithData(SHARE_CALLBACK_FUNCTION_NAME, 0, -8, a2 + Constants.STR_EMPTY);
                return;
            case 4:
                IWXAPI createWXAPI = WXAPIFactory.createWXAPI(AstApp.i().getApplicationContext(), "wx3909f6add1206543", false);
                if (!createWXAPI.isWXAppInstalled() || createWXAPI.getWXAppSupportAPI() <= 553779201) {
                    Toast.makeText(AstApp.i().getApplicationContext(), (int) R.string.need_install_wx, 0).show();
                    responseFailWithData(SHARE_CALLBACK_FUNCTION_NAME, 0, -8, a2 + Constants.STR_EMPTY);
                    return;
                }
                ((BrowserActivity) this.mActivityRef.get()).z();
                return;
            case 5:
                ((BrowserActivity) this.mActivityRef.get()).A();
                return;
            default:
                ((BrowserActivity) this.mActivityRef.get()).b(true);
                ((BrowserActivity) this.mActivityRef.get()).i();
                return;
        }
    }

    public void getMid(Uri uri, int i, String str, String str2) {
        JSONObject jSONObject = new JSONObject();
        String b = com.tencent.b.b.c.b(this.mContext.getApplicationContext());
        if ("0".equals(b)) {
            com.tencent.b.b.c.a(this.mContext.getApplicationContext(), new g(this, jSONObject, str2, i, str));
            return;
        }
        try {
            jSONObject.put("mid", b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        response(str2, i, str, jSONObject.toString());
    }

    public void clickCallback() {
        response(BUTTON_CLICK_CALLBACK_FUNCTION_NAME, 0, null, Constants.STR_EMPTY);
    }

    public void getUserLoginToken(Uri uri, int i, String str, String str2) {
        AppConst.IdentityType d = d.a().d();
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("isRefreshTicket", this.isRefreshTicket);
            if (this.isRefreshTicket) {
                this.isRefreshTicket = false;
            }
            jSONObject.put("logintype", d);
            jSONObject.put("qskey", d.a().m());
            jSONObject.put("quin", d.a().p());
            jSONObject.put("wopenid", d.a().s());
            jSONObject.put("waccesstoken", d.a().r());
        } catch (Exception e) {
            e.printStackTrace();
        }
        response(str2, i, str, jSONObject.toString());
    }

    public void openLoginActivity(Uri uri, int i, String str, String str2) {
        Exception e;
        AppConst.IdentityType identityType;
        String queryParameter = uri.getQueryParameter("logintype");
        String queryParameter2 = uri.getQueryParameter("uin");
        AppConst.IdentityType identityType2 = AppConst.IdentityType.MOBILEQ;
        Bundle bundle = new Bundle();
        try {
            if (LOGIN_TYPE_WX.equals(queryParameter)) {
                identityType = AppConst.IdentityType.WX;
            } else if (LOGIN_TYPE_DEFAULT.equals(queryParameter)) {
                bundle.putInt(AppConst.KEY_LOGIN_TYPE, 2);
                identityType = identityType2;
            } else if (LOGIN_TYPE_QUICK_MOBILEQ.equals(queryParameter)) {
                bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
                identityType = identityType2;
            } else {
                bundle.putInt(AppConst.KEY_LOGIN_TYPE, 6);
                identityType = identityType2;
            }
            try {
                if (!TextUtils.isEmpty(queryParameter2)) {
                    bundle.putString("uin", queryParameter2);
                }
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                d.a().a(identityType, bundle);
            }
        } catch (Exception e3) {
            Exception exc = e3;
            identityType = identityType2;
            e = exc;
            e.printStackTrace();
            d.a().a(identityType, bundle);
        }
        d.a().a(identityType, bundle);
    }

    public void getStatTime(Uri uri, int i, String str, String str2) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("createTime", this.createTime);
            jSONObject.put("startLoadTime", this.startLoadTime);
            jSONObject.put("loadedTime", this.loadedTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        response(str2, i, str, jSONObject.toString());
    }

    public void notifyBookingStateChange(Uri uri, int i, String str, String str2) {
        BookingManager.a().a(ct.c(uri.getQueryParameter("subscriptionId")), ct.d(uri.getQueryParameter("status")));
    }

    private void callback(String str, String str2) {
        if (this.mWebView != null) {
            StringBuffer stringBuffer = new StringBuffer("javascript:");
            stringBuffer.append("if(!!").append("window." + str).append("){");
            stringBuffer.append(str);
            stringBuffer.append("(");
            stringBuffer.append(str2);
            stringBuffer.append(")}");
            this.mWebView.loadUrl(stringBuffer.toString());
        }
        if (this.sysWebView != null) {
            StringBuffer stringBuffer2 = new StringBuffer("javascript:");
            stringBuffer2.append("if(!!").append("window." + str).append("){");
            stringBuffer2.append(str);
            stringBuffer2.append("(");
            stringBuffer2.append(str2);
            stringBuffer2.append(")}");
            this.sysWebView.loadUrl(stringBuffer2.toString());
        }
    }

    /* access modifiers changed from: private */
    public void response(String str, int i, String str2, String str3) {
        response(str, i, str2, str3, null);
    }

    private void response(String str, int i, String str2, String str3, Map<String, String> map) {
        if (!TextUtils.isEmpty(str)) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("result", 0);
                jSONObject.put("data", str3);
                if (!TextUtils.isEmpty(str2)) {
                    jSONObject.put("method", str2);
                }
                jSONObject.put("seqid", i);
                if (map != null) {
                    for (String next : map.keySet()) {
                        jSONObject.put(next, map.get(next));
                    }
                }
                callback(str, jSONObject.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void responseFileChooser(String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("path", str2);
        response(FILE_CHOOSER_CALLBACK_FUNCTION_NAME, 0, null, str, hashMap);
    }

    /* access modifiers changed from: private */
    public void responseFail(String str, int i, String str2, int i2) {
        if (!TextUtils.isEmpty(str)) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("result", -1);
                jSONObject.put("code", i2);
                jSONObject.put("method", str2);
                jSONObject.put("seqid", i);
                callback(str, jSONObject.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void responseFail(String str, int i, String str2, int i2, Map<String, String> map) {
        if (!TextUtils.isEmpty(str)) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("result", -1);
                jSONObject.put("code", i2);
                jSONObject.put("method", str2);
                jSONObject.put("seqid", i);
                if (map != null) {
                    for (String next : map.keySet()) {
                        jSONObject.put(next, map.get(next));
                    }
                }
                callback(str, jSONObject.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void responseFailWithData(String str, int i, int i2, String str2) {
        if (!TextUtils.isEmpty(str)) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("result", -1);
                jSONObject.put("code", i2);
                jSONObject.put("seqid", i);
                jSONObject.put("data", str2);
                callback(str, jSONObject.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void handleUIEvent(Message message) {
        DownloadInfo d;
        int i = 1;
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC:
            case 1007:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING:
            case 1013:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_SUCC:
            case 1027:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_START:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_SUCC:
            case EventDispatcherEnum.UI_EVENT_ROOT_SILENT_UNINSTALL_FAIL:
                if (authorize(STATE_CALLBACK_FUNCTION_NAME)) {
                    String str = Constants.STR_EMPTY;
                    if (message.obj instanceof String) {
                        str = (String) message.obj;
                    } else if (message.obj instanceof InstallUninstallTaskBean) {
                        str = ((InstallUninstallTaskBean) message.obj).downloadTicket;
                    }
                    if (!TextUtils.isEmpty(str) && (d = DownloadProxy.a().d(str)) != null) {
                        if (message.what == 1025) {
                            d.downloadState = SimpleDownloadInfo.DownloadState.INSTALLING;
                        } else if (message.what == 1026) {
                            d.downloadState = SimpleDownloadInfo.DownloadState.INSTALLED;
                        } else if (message.what == 1027) {
                            d.downloadState = SimpleDownloadInfo.DownloadState.SUCC;
                        }
                        response(STATE_CALLBACK_FUNCTION_NAME, 0, null, getAppStateJsString(d, message.what));
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD:
                if (authorize(STATE_CALLBACK_FUNCTION_NAME) && (message.obj instanceof DownloadInfo)) {
                    response(STATE_CALLBACK_FUNCTION_NAME, 0, null, getAppStateJsString((DownloadInfo) message.obj, message.what));
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_APP_INSTALL:
            case EventDispatcherEnum.UI_EVENT_APP_UNINSTALL:
                if (message.obj instanceof String) {
                    String str2 = (String) message.obj;
                    JSONObject jSONObject = new JSONObject();
                    try {
                        if (message.what != 1011) {
                            i = 2;
                        }
                        jSONObject.put("state", i);
                        jSONObject.put("packageName", str2);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    response(APP_INSTALL_UNINSTALL, 0, null, jSONObject.toString());
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_QQ_AUTH_FAIL_FOR_WEBVIEW:
                if (this.authSeq == message.arg2) {
                    this.authSeq = -1;
                    if (message.obj != null) {
                        int i2 = message.arg1;
                        SimpleAppModel simpleAppModel = (SimpleAppModel) message.obj;
                        response(this.authFuction, 0, null, Constants.STR_EMPTY);
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_QQ_AUTH_SUCCESS_FOR_WEBVIEW:
                if (this.authSeq == message.arg2) {
                    this.authSeq = -1;
                    if (message.obj != null) {
                        int i3 = message.arg1;
                        SimpleAppModel simpleAppModel2 = (SimpleAppModel) message.obj;
                        response(this.authFuction, 0, null, Constants.STR_EMPTY);
                        return;
                    }
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_WX_AUTH_SUCCESS_FOR_WEBVIEW:
                if (this.authSeq == message.arg2) {
                    this.authSeq = -1;
                    if (message.obj != null) {
                        int i4 = message.arg1;
                        SimpleAppModel simpleAppModel3 = (SimpleAppModel) message.obj;
                        if (simpleAppModel3.Q == 1) {
                            response(this.authFuction, 0, null, Constants.STR_EMPTY);
                            return;
                        } else if (simpleAppModel3.Q == 2) {
                            response(this.authFuction, 0, null, Constants.STR_EMPTY);
                            return;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_WX_AUTH_FAIL_FOR_WEBVIEW:
                if (this.authSeq == message.arg2) {
                    this.authSeq = -1;
                    if (message.obj != null) {
                        int i5 = message.arg1;
                        SimpleAppModel simpleAppModel4 = (SimpleAppModel) message.obj;
                        if (simpleAppModel4.Q == 1) {
                            responseFail(this.authFuction, 0, null, i5);
                            return;
                        } else if (simpleAppModel4.Q == 2) {
                            responseFail(this.authFuction, 0, null, i5);
                            return;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_WTLOGIN_GET_IMAGE:
                XLog.d(TAG, "UI_EVENT_WTLOGIN_GET_IMAGE!");
                this.externalCallTicketStatus = 2;
                if (this.mExternalCallTicketReqBundle != null) {
                    response(this.mExternalCallTicketReqBundle.getString("callbackFun"), this.mExternalCallTicketReqBundle.getInt("seqId"), this.mExternalCallTicketReqBundle.getString("method"), String.valueOf(this.externalCallTicketStatus));
                    this.mExternalCallTicketReqBundle = null;
                    break;
                }
                break;
            case EventDispatcherEnum.UI_EVENT_WTLOGIN_SUCCESS:
                break;
            case EventDispatcherEnum.UI_EVENT_WTLOGIN_FAIL:
                XLog.d(TAG, "UI_EVENT_WTLOGIN_FAIL!");
                this.externalCallTicketStatus = -1;
                if (this.mExternalCallTicketReqBundle != null) {
                    responseFail(this.mExternalCallTicketReqBundle.getString("callbackFun"), this.mExternalCallTicketReqBundle.getInt("seqId"), this.mExternalCallTicketReqBundle.getString("method"), -1);
                    this.mExternalCallTicketReqBundle = null;
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                if (authorize(LOGIN_CALLBACK_FUNCTION_NAME)) {
                    ba.a().post(new h(this));
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL:
                responseFail(LOGIN_CALLBACK_FUNCTION_NAME, 0, null, -5);
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL:
                responseFail(LOGIN_CALLBACK_FUNCTION_NAME, 0, null, -2);
                return;
            case EventDispatcherEnum.UI_EVENT_SHARE_SUCCESS:
                if (message.obj instanceof Integer) {
                    HashMap hashMap = new HashMap();
                    hashMap.put(SocialConstants.PARAM_TYPE, ((Integer) message.obj).intValue() + Constants.STR_EMPTY);
                    response(SHARE_CALLBACK_FUNCTION_NAME, 0, null, "1", hashMap);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_SHARE_FAIL:
                if (message.obj instanceof Integer) {
                    HashMap hashMap2 = new HashMap();
                    hashMap2.put(SocialConstants.PARAM_TYPE, ((Integer) message.obj).intValue() + Constants.STR_EMPTY);
                    hashMap2.put("data", "0");
                    responseFail(SHARE_CALLBACK_FUNCTION_NAME, 0, null, -1, hashMap2);
                    return;
                }
                return;
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE:
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC:
                if (authorize(VIDEO_DOWNLOAD_STATE_CALLBACK)) {
                    h hVar = (h) message.obj;
                    XLog.i(TAG, "[videoDownloadStateCallback] : " + hVar);
                    if (hVar != null) {
                        try {
                            response(VIDEO_DOWNLOAD_STATE_CALLBACK, 0, null, de.b(hVar));
                            return;
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                            responseFail(VIDEO_DOWNLOAD_STATE_CALLBACK, 0, null, -3);
                            return;
                        }
                    } else {
                        responseFail(VIDEO_DOWNLOAD_STATE_CALLBACK, 0, null, -2);
                        return;
                    }
                } else {
                    return;
                }
            case EventDispatcherEnum.APP_LINK_EVENT_INSTALLED_AND_ACTION_COMPLETE:
                if (message.obj != null && (message.obj instanceof String)) {
                    response(APP_INSTALLED_AND_ACTION_COMPLETE_FUNCTION, 0, null, (String) message.obj);
                    return;
                }
                return;
            default:
                return;
        }
        XLog.d(TAG, "userAccount = " + message.obj.toString());
        this.externalCallTicketStatus = 1;
        if (this.mExternalCallTicketReqBundle != null) {
            response(this.mExternalCallTicketReqBundle.getString("callbackFun"), this.mExternalCallTicketReqBundle.getInt("seqId"), this.mExternalCallTicketReqBundle.getString("method"), String.valueOf(this.externalCallTicketStatus));
            this.mExternalCallTicketReqBundle = null;
        }
    }

    private String getAppStateJsString(DownloadInfo downloadInfo, int i) {
        AppConst.AppState b = u.b(downloadInfo);
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("apkid", downloadInfo.apkId);
            jSONObject.put("appstate", b);
            jSONObject.put("packageName", downloadInfo.packageName);
            jSONObject.put("speed", downloadInfo.response != null ? downloadInfo.response.c : 0);
            if (b == AppConst.AppState.DOWNLOADING) {
                jSONObject.put("down_percent", u.a(downloadInfo, b));
            }
            jSONObject.put(AuthActivity.ACTION_KEY, i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    public void loadAuthorization(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.currentUrl = str;
            if (!a.a().a(this.currentUrl)) {
                this.getDomainCapabilityEngine.a(str);
            }
        }
    }

    public void invoke(String str) {
        int i;
        Uri parse = Uri.parse(str);
        String host = parse.getHost();
        if (!TextUtils.isEmpty(host)) {
            List<String> pathSegments = parse.getPathSegments();
            String str2 = null;
            if (pathSegments == null || pathSegments.size() <= 0) {
                i = 0;
            } else {
                i = ct.d(pathSegments.get(0));
                if (pathSegments.size() > 1) {
                    str2 = pathSegments.get(1);
                }
            }
            if (host.equals(CALL_BATCH_NAME)) {
                try {
                    JSONArray jSONArray = new JSONArray(parse.getQueryParameter("param"));
                    int length = jSONArray.length();
                    for (int i2 = 0; i2 < length; i2++) {
                        JSONObject jSONObject = jSONArray.getJSONObject(i2);
                        String string = jSONObject.getString("method");
                        int i3 = jSONObject.getInt("seqid");
                        String optString = jSONObject.optString("callback");
                        JSONObject jSONObject2 = jSONObject.getJSONObject("args");
                        StringBuilder sb = new StringBuilder();
                        sb.append(JS_BRIDGE_SCHEME).append(string).append("/").append(i3).append("/").append(!TextUtils.isEmpty(optString) ? optString : Constants.STR_EMPTY).append("?");
                        if (jSONObject2 != null) {
                            Iterator<String> keys = jSONObject2.keys();
                            while (keys.hasNext()) {
                                String next = keys.next();
                                sb.append(next).append("=").append(Uri.encode(Uri.decode(jSONObject2.getString(next)))).append("&");
                            }
                        }
                        callAMethod(Uri.parse(sb.toString()), string, i3, optString);
                    }
                } catch (Exception e) {
                    if (!TextUtils.isEmpty(str2)) {
                        responseFail(str2, i, host, -5);
                    }
                } catch (OutOfMemoryError e2) {
                    if (!TextUtils.isEmpty(str2)) {
                        responseFail(str2, i, host, -5);
                    }
                }
            } else {
                callAMethod(parse, host, i, str2);
            }
        }
    }

    private void callAMethod(Uri uri, String str, int i, String str2) {
        if (authorize(str)) {
            Class<JsBridge> cls = JsBridge.class;
            try {
                cls.getDeclaredMethod(str, Uri.class, Integer.TYPE, String.class, String.class).invoke(this, uri, Integer.valueOf(i), str, str2);
            } catch (Exception e) {
                e.printStackTrace();
                if (!TextUtils.isEmpty(str2)) {
                    responseFail(str2, i, str, -3);
                }
            }
        } else {
            XLog.i(TAG, "authorize failed");
            if (!TextUtils.isEmpty(str2)) {
                responseFail(str2, i, str, -5);
            }
        }
    }

    public int getVersion() {
        return 3;
    }

    private boolean authorize(String str) {
        return a.a().a(this.currentUrl, str);
    }

    public void onGetDomainCapabilitySuccess(int i, int i2, i iVar) {
        a.a().a(new b(1, iVar.b, iVar.f1356a));
        response(READY_CALLBACK_FUNCTION_NAME, 0, null, "true");
    }

    public void onGetDomainCapabilityFail(int i, int i2) {
    }

    public void updateStartLoadTime() {
        this.startLoadTime = System.currentTimeMillis();
    }

    public void updateLoadedTime() {
        this.loadedTime = System.currentTimeMillis();
    }

    public void doPageLoadFinished() {
        if (a.a().a(this.currentUrl)) {
            response(READY_CALLBACK_FUNCTION_NAME, 1, null, "true");
        }
    }

    public void onRequestFinished(com.tencent.assistant.utils.be beVar) {
        Bundle bundle = this.mHttpReqMap.get(beVar.c);
        if (bundle != null) {
            this.mHttpReqMap.delete(beVar.c);
            String string = bundle.getString("callbackFun");
            String string2 = bundle.getString("method");
            if (beVar.d == 0) {
                response(string, beVar.c, string2, beVar.f2647a);
            } else {
                responseFail(string, beVar.c, string2, -3);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public void onCheckSelfUpdateFinish(int i, int i2, SelfUpdateManager.SelfUpdateInfo selfUpdateInfo) {
        if (this.mSelfUpdateReqBundle != null) {
            int i3 = this.mSelfUpdateReqBundle.getInt("seqId");
            String string = this.mSelfUpdateReqBundle.getString("callbackFun");
            String string2 = this.mSelfUpdateReqBundle.getString("method");
            if (selfUpdateInfo != null) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("canUpdate", true);
                    jSONObject.put("appid", selfUpdateInfo.f1469a);
                    jSONObject.put("apkid", selfUpdateInfo.b);
                    response(string, i3, string2, jSONObject.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                    responseFail(string, i3, string2, -3);
                }
            } else {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("canUpdate", false);
                response(string, i3, string2, jSONObject2.toString());
            }
        }
    }

    private AppConst.AppState getLocalApkState(String str, int i, int i2) {
        AppConst.AppState appState;
        XLog.i(TAG, "[getLocalApkInfo] ---> downloadInfo is null");
        AppConst.AppState appState2 = AppConst.AppState.ILLEGAL;
        LocalApkInfo localApkInfo = ApkResourceManager.getInstance().getLocalApkInfo(str);
        boolean z = ApkResourceManager.getInstance().getLocalApkInfo(str, i, i2) != null;
        if (localApkInfo != null) {
            if (i == localApkInfo.mVersionCode) {
                appState = i2 <= localApkInfo.mGrayVersionCode ? AppConst.AppState.INSTALLED : z ? AppConst.AppState.DOWNLOADED : AppConst.AppState.UPDATE;
            } else if (i > localApkInfo.mVersionCode) {
                appState = z ? AppConst.AppState.DOWNLOADED : AppConst.AppState.UPDATE;
            } else {
                appState = AppConst.AppState.INSTALLED;
            }
        } else if (z) {
            appState = AppConst.AppState.DOWNLOADED;
        } else {
            appState = AppConst.AppState.DOWNLOAD;
        }
        XLog.i(TAG, "[getLocalApkState] ---> localAppState = " + appState.ordinal());
        return appState;
    }
}
