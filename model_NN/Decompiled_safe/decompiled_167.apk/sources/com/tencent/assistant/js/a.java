package com.tencent.assistant.js;

import android.net.Uri;
import android.text.TextUtils;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.u;
import com.tencent.assistant.manager.v;
import com.tencent.open.SocialConstants;
import com.tencent.open.utils.ServerSetting;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class a implements v {

    /* renamed from: a  reason: collision with root package name */
    private static final b[] f1348a = {new b(1, "gift.app.qq.com", Long.MAX_VALUE), new b(1, "maweb.3g.qq.com", Long.MAX_VALUE), new b(1, ServerSetting.KEY_HOST_QZS_QQ, Long.MAX_VALUE), new b(1, "mq.wsq.qq.com", Long.MAX_VALUE), new b(1, "m.wsq.qq.com", Long.MAX_VALUE), new b(1, "appicsh.qq.com", Long.MAX_VALUE), new b(1, "*.kf0309.3g.qq.com", Long.MAX_VALUE), new b(1, "dev-qzs.yybv.qq.com", Long.MAX_VALUE), new b(1, "pre-qzs.yybv.qq.com", Long.MAX_VALUE), new b(1, "test-qzs.yybv.qq.com", Long.MAX_VALUE), new b(1, "m.tv.sohu.com", Long.MAX_VALUE), new b(1, "test.qzs.qq.com", Long.MAX_VALUE), new b(1, "appicsh.qq.com", Long.MAX_VALUE), new b(1, "qtheme.cs0309.html5.qq.com", Long.MAX_VALUE), new b(1, "yyb.html5.qq.com", Long.MAX_VALUE)};
    private static a d;
    private static final Map<String, Integer> e = new HashMap();
    private static final Map<String, Integer> f = new HashMap();
    private List<b> b = new ArrayList();
    private final List<b> c = Collections.synchronizedList(new ArrayList());

    static {
        e.put("startDownload", 0);
        e.put("pauseDownload", 1);
        e.put("createDownload", 2);
        e.put("queryDownload", 3);
        e.put("getAppInfo", 4);
        e.put("startOpenApp", 5);
        e.put("getNetInfo", 6);
        e.put("getMobileInfo", 7);
        e.put("getPrivateMobileInfo", 8);
        e.put("setWebView", 9);
        e.put("closeWebView", 10);
        e.put("pageControl", 11);
        e.put("store", 12);
        e.put("getStoreByKey", 13);
        e.put("getAllStore", 14);
        e.put("gray", 15);
        e.put("loadByAnotherWebBrowser", 16);
        e.put("getVersion", 17);
        e.put("checkSelfUpdate", 18);
        e.put("getUserLoginToken", 19);
        e.put("openLoginActivity", 20);
        e.put("getStatTime", 21);
        e.put("refreshTicket", 22);
        e.put("toast", 23);
        e.put("showPics", 24);
        e.put("share", 25);
        e.put("openNewWindow", 26);
        e.put("getMid", 27);
        e.put("showGiftGameItem", 28);
        e.put("hideGiftGameItem", 29);
        e.put("showErrorPage", 30);
        e.put("report", 31);
        e.put("queryAppState", 32);
        e.put("sendHttpRequest", 33);
        e.put("clearPrompt", 34);
        e.put("clearNumEx", 35);
        e.put("saveData", 36);
        e.put("getData", 37);
        e.put("playVideoByApp", 38);
        e.put("playVideoByWebView", 39);
        e.put("playLocalVideo", 40);
        e.put("bookOp", 41);
        e.put("downloadVideo", 42);
        e.put("pauseDownloadVideo", 43);
        e.put("deleteVideo", 44);
        e.put("queryVideoDownloadState", 45);
        e.put("openQQReader", 46);
        e.put("getVideoDownloadInfo", 47);
        e.put(JsBridge.APP_INSTALL_UNINSTALL, 48);
        e.put("getBrowserSignature", 49);
        e.put("notifyBookingStateChange", 50);
        e.put("openQubeTheme", 51);
        e.put("deleteLocalTheme", 52);
        e.put("getAllLocalThemes", 53);
        e.put("obtainMissionFreeTime", 54);
        e.put("showFreewifiDialog", 55);
        e.put("setClipboard", 56);
        e.put("getClipboard", 57);
        e.put("webViewCompatibilityReport", 58);
        e.put("addAppLinkActionTask", 59);
        e.put("deleteDownload", 60);
        e.put("getLBSInfo", 61);
        e.put("getExternalCallTicketStatus", 62);
        f.put("toast", 0);
        f.put("clearNum", 0);
        f.put("scrollToTop", 0);
        f.put("queryAppState", 0);
        f.put("sendHttpRequest", 0);
        f.put(JsBridge.LOGIN_CALLBACK_FUNCTION_NAME, 0);
        f.put(JsBridge.STATE_CALLBACK_FUNCTION_NAME, 0);
        f.put("clearNum", 0);
        f.put(JsBridge.VIDEO_DOWNLOAD_STATE_CALLBACK, 0);
        f.put("deteleAppLinkActionTask", 0);
        f.put(JsBridge.APP_INSTALLED_AND_ACTION_COMPLETE_FUNCTION, 0);
    }

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (d == null) {
                d = new a();
                u.a().a(d);
            }
            aVar = d;
        }
        return aVar;
    }

    private a() {
        b(m.a().z());
    }

    private void b(String str) {
        ArrayList arrayList = new ArrayList();
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONArray jSONArray = new JSONArray(str);
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    arrayList.add(new b(jSONObject.getInt(SocialConstants.PARAM_TYPE), jSONObject.getString(SocialConstants.PARAM_URL), jSONObject.getLong("mask")));
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        this.b = arrayList;
    }

    public void a(b bVar) {
        this.c.add(bVar);
    }

    public boolean a(String str, String str2) {
        if (str2.equals(JsBridge.IS_INTERFACE_READY_NAME) || f.containsKey(str2)) {
            return true;
        }
        long c2 = c(str);
        Integer num = e.get(str2);
        if (num == null) {
            return false;
        }
        return ((c2 >>> num.intValue()) & 1) == 1;
    }

    public boolean a(String str) {
        return c(str) != 0;
    }

    private long c(String str) {
        if (TextUtils.isEmpty(str)) {
            return 0;
        }
        String host = Uri.parse(str).getHost();
        if (TextUtils.isEmpty(host)) {
            return 0;
        }
        String lowerCase = host.toLowerCase();
        for (b next : this.c) {
            if (next.a(str, lowerCase)) {
                return next.b;
            }
        }
        for (b next2 : this.b) {
            if (next2.a(str, lowerCase)) {
                return next2.b;
            }
        }
        for (b bVar : f1348a) {
            if (bVar.a(str, lowerCase)) {
                return bVar.b;
            }
        }
        return 0;
    }

    public void a(HashMap<String, Object> hashMap) {
        String str = (String) hashMap.get("key_webview_config_json");
        if (!TextUtils.isEmpty(str)) {
            b(str);
        }
    }

    public void b() {
    }
}
