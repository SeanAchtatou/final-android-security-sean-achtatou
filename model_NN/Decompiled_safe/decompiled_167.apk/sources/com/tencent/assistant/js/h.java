package com.tencent.assistant.js;

import android.content.Context;
import com.tencent.assistant.component.appdetail.AppBarTabView;

/* compiled from: ProGuard */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ JsBridge f1355a;

    h(JsBridge jsBridge) {
        this.f1355a = jsBridge;
    }

    public void run() {
        k.a((Context) this.f1355a.mActivityRef.get(), this.f1355a.currentUrl, AppBarTabView.AUTH_TYPE_ALL);
        this.f1355a.getUserLoginToken(null, 0, null, JsBridge.LOGIN_CALLBACK_FUNCTION_NAME);
    }
}
