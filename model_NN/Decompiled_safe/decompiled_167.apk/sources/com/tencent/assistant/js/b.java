package com.tencent.assistant.js;

import java.util.regex.Pattern;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    int f1349a;
    long b;
    String c;
    Pattern d;

    public b() {
    }

    public b(int i, String str, long j) {
        this.f1349a = i;
        this.b = j;
        this.c = str;
        if (this.f1349a == 3) {
            this.d = Pattern.compile(str);
        }
    }

    public boolean a(String str, String str2) {
        if (this.f1349a == 1) {
            if (this.c.startsWith("*")) {
                if (str2.endsWith(this.c.substring(1))) {
                    return true;
                }
            } else if (str2 == null || !str2.equals(this.c)) {
                return false;
            } else {
                return true;
            }
        } else if (this.f1349a != 2) {
            return this.d.matcher(str).matches();
        } else {
            if (str.equals(this.c)) {
                return true;
            }
        }
        return false;
    }
}
