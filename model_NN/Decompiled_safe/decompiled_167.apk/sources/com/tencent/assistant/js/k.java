package com.tencent.assistant.js;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.Global;
import com.tencent.assistant.component.appdetail.AppBarTabView;
import com.tencent.assistant.login.d;
import com.tencent.assistant.utils.t;
import com.tencent.connect.common.Constants;
import com.tencent.smtt.sdk.CookieManager;
import com.tencent.smtt.sdk.CookieSyncManager;

/* compiled from: ProGuard */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private static int f1358a = 1;

    public static synchronized void a(Context context, String str, String str2) {
        synchronized (k.class) {
            if (!TextUtils.isEmpty(str)) {
                CookieSyncManager.createInstance(context);
                CookieManager instance = CookieManager.getInstance();
                instance.setAcceptCookie(true);
                Uri parse = Uri.parse(str);
                if (!(parse == null || parse.getHost() == null)) {
                    String lowerCase = parse.getHost().toLowerCase();
                    String str3 = null;
                    if (lowerCase.endsWith(".qq.com")) {
                        str3 = ".qq.com";
                    }
                    if ((Constants.SOURCE_QQ.equals(str2) || AppBarTabView.AUTH_TYPE_ALL.equals(str2)) && d.a().k()) {
                        instance.setCookie(str, a("logintype", d.a().d().name(), str3));
                        instance.setCookie(str, a("skey", d.a().m(), str3));
                        instance.setCookie(str, a("uin", "o0" + d.a().p(), str3));
                        instance.setCookie(str, a("sid", d.a().n(), str3));
                        instance.setCookie(str, a("vkey", d.a().o(), str3));
                    } else if (("WX".equals(str2) || AppBarTabView.AUTH_TYPE_ALL.equals(str2)) && d.a().l()) {
                        instance.setCookie(str, a("logintype", d.a().d().name(), str3));
                        instance.setCookie(str, a("openid", d.a().s(), str3));
                        instance.setCookie(str, a("accesstoken", d.a().r(), str3));
                    } else {
                        instance.setCookie(str, a("logintype", AppConst.IdentityType.NONE.name(), str3));
                        instance.setCookie(str, a("skey", Constants.STR_EMPTY, str3));
                        instance.setCookie(str, a("uin", Constants.STR_EMPTY, str3));
                        instance.setCookie(str, a("openid", Constants.STR_EMPTY, str3));
                        instance.setCookie(str, a("accesstoken", Constants.STR_EMPTY, str3));
                        instance.setCookie(str, a("sid", Constants.STR_EMPTY, str3));
                        instance.setCookie(str, a("vkey", Constants.STR_EMPTY, str3));
                    }
                    instance.setCookie(str, a("imei", t.g(), str3));
                    instance.setCookie(str, a("guid", Global.getPhoneGuid(), str3));
                    CookieSyncManager.getInstance().sync();
                }
            }
        }
    }

    public static synchronized void a(Context context, String str, l lVar) {
        synchronized (k.class) {
            if (!TextUtils.isEmpty(str)) {
                CookieSyncManager.createInstance(context);
                CookieManager instance = CookieManager.getInstance();
                instance.setAcceptCookie(true);
                Uri parse = Uri.parse(str);
                if (!(parse == null || parse.getHost() == null)) {
                    String lowerCase = parse.getHost().toLowerCase();
                    String str2 = null;
                    if (lowerCase.endsWith(".qq.com")) {
                        str2 = ".qq.com";
                    }
                    instance.setCookie(str, a("qopenid", lVar.f1359a, str2));
                    instance.setCookie(str, a("qaccesstoken", lVar.b, str2));
                    instance.setCookie(str, a("openappid", lVar.c + Constants.STR_EMPTY, str2));
                    CookieSyncManager.getInstance().sync();
                }
            }
        }
    }

    private static String a(String str, String str2, String str3) {
        String str4 = str + "=" + str2;
        if (str3 == null) {
            return str4;
        }
        return (str4 + "; path=/") + "; domain=" + str3;
    }

    public static boolean a(String str) {
        if (str != null) {
            return str.toLowerCase().endsWith(".qq.com");
        }
        return false;
    }

    public static synchronized void a() {
        synchronized (k.class) {
            CookieSyncManager.createInstance(AstApp.i().getApplicationContext());
            CookieManager.getInstance().removeAllCookie();
            CookieSyncManager.getInstance().sync();
        }
    }

    public static void a(int i) {
        f1358a = i;
    }

    public static int b() {
        int i = f1358a;
        f1358a = 1;
        return i;
    }
}
