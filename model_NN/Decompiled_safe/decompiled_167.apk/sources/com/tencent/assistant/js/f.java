package com.tencent.assistant.js;

import com.tencent.assistant.download.DownloadInfo;
import com.tencent.assistant.manager.DownloadProxy;
import com.tencent.assistant.utils.ct;

/* compiled from: ProGuard */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f1353a;

    f(e eVar) {
        this.f1353a = eVar;
    }

    public void run() {
        try {
            ct.c(this.f1353a.f1352a.getQueryParameter("appid"));
            long c = ct.c(this.f1353a.f1352a.getQueryParameter("apkid"));
            String queryParameter = this.f1353a.f1352a.getQueryParameter("packagename");
            int d = ct.d(this.f1353a.f1352a.getQueryParameter("versioncode"));
            int d2 = ct.d(this.f1353a.f1352a.getQueryParameter("grayversioncode"));
            DownloadInfo downloadInfo = null;
            if (c > 0) {
                downloadInfo = DownloadProxy.a().d(String.valueOf(c));
            }
            if (downloadInfo == null) {
                downloadInfo = DownloadProxy.a().a(queryParameter, d, d2);
            }
            if (downloadInfo != null) {
                DownloadProxy.a().b(downloadInfo.downloadTicket);
                this.f1353a.e.response(this.f1353a.b, this.f1353a.c, this.f1353a.d, String.valueOf(0));
                return;
            }
            this.f1353a.e.responseFail(this.f1353a.b, this.f1353a.c, this.f1353a.d, -2);
        } catch (Exception e) {
            this.f1353a.e.responseFail(this.f1353a.b, this.f1353a.c, this.f1353a.d, -3);
        }
    }
}
