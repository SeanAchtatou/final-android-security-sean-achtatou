package com.tencent.assistant.floatingwindow;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.manager.spaceclean.SpaceScanManager;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.bt;
import com.tencent.assistant.utils.df;
import com.tencent.assistant.utils.t;

/* compiled from: ProGuard */
public class FloatWindowSmallView extends RelativeLayout implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    public static int f1280a;
    public static int b;
    private long A = 0;
    private long B = 0;
    /* access modifiers changed from: private */
    public WindowManager c;
    /* access modifiers changed from: private */
    public LinearLayout d;
    /* access modifiers changed from: private */
    public ImageView e;
    /* access modifiers changed from: private */
    public WindowManager.LayoutParams f;
    /* access modifiers changed from: private */
    public RelativeLayout.LayoutParams g;
    private float h;
    private float i;
    /* access modifiers changed from: private */
    public float j;
    /* access modifiers changed from: private */
    public float k;
    /* access modifiers changed from: private */
    public float l;
    /* access modifiers changed from: private */
    public float m;
    private int n;
    private int o;
    private long p = 0;
    private boolean q;
    /* access modifiers changed from: private */
    public RelativeLayout r;
    private AnimationDrawable s;
    /* access modifiers changed from: private */
    public boolean t = true;
    private boolean u = true;
    /* access modifiers changed from: private */
    public boolean v = true;
    /* access modifiers changed from: private */
    public boolean w = true;
    private long x;
    private RocketLauncher y;
    private RelativeLayout.LayoutParams z;

    public FloatWindowSmallView(Context context) {
        super(context);
        this.c = (WindowManager) context.getSystemService("window");
        LayoutInflater.from(context).inflate((int) R.layout.float_window_small, this);
        this.r = (RelativeLayout) findViewById(R.id.small_floating_layout);
        this.d = (LinearLayout) findViewById(R.id.small_window_layout);
        f1280a = (int) context.getResources().getDimension(R.dimen.floating_small_window_width);
        b = f1280a;
        this.e = (ImageView) findViewById(R.id.rocket_img);
        this.e.setBackgroundResource(R.drawable.small_filcker_rocket);
        this.g = new RelativeLayout.LayoutParams(-2, -2);
        this.s = (AnimationDrawable) this.e.getBackground();
        this.n = (int) context.getResources().getDimension(R.dimen.floating_window_rocket_width);
        this.o = (int) context.getResources().getDimension(R.dimen.floating_window_rocket_height);
        ((TextView) findViewById(R.id.percent)).setText(j.a().f());
        c();
    }

    private void c() {
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL, this);
    }

    public void a() {
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL, this);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.t && this.v && this.w) {
            switch (motionEvent.getAction()) {
                case 0:
                    this.q = true;
                    this.l = motionEvent.getX();
                    this.m = motionEvent.getY();
                    this.j = motionEvent.getRawX();
                    this.k = motionEvent.getRawY() - ((float) df.a());
                    this.h = motionEvent.getRawX();
                    this.i = motionEvent.getRawY() - ((float) df.a());
                    this.A = System.currentTimeMillis();
                    break;
                case 1:
                    this.d.setVisibility(8);
                    this.q = false;
                    if (!j()) {
                        a(false);
                        if (Math.abs(this.j - this.h) < 12.0f && Math.abs(this.k - this.i) < 12.0f) {
                            k();
                            break;
                        } else {
                            g();
                            break;
                        }
                    } else {
                        e();
                        break;
                    }
                case 2:
                    this.B = System.currentTimeMillis();
                    this.h = motionEvent.getRawX();
                    this.i = motionEvent.getRawY() - ((float) df.a());
                    if (Math.abs(this.j - this.h) >= 12.0f || Math.abs(this.k - this.i) >= 12.0f || this.B - this.A >= 400) {
                        d();
                        g();
                        a(true);
                        break;
                    }
            }
        }
        return true;
    }

    private void d() {
        this.f.width = -1;
        this.f.height = -1;
        this.d.setVisibility(8);
        if (this.c != null && j.a().e()) {
            try {
                this.c.updateViewLayout(this, this.f);
            } catch (Throwable th) {
                th.printStackTrace();
                XLog.d("floatingwindow", "FloatWindowSmallView >> <setToFullScreen> updateViewLayout exception");
            }
        }
    }

    public void a(WindowManager.LayoutParams layoutParams) {
        this.f = layoutParams;
    }

    private void e() {
        XLog.d("floatingwindow", "launchRocket");
        this.t = false;
        this.v = false;
        this.w = false;
        this.u = false;
        this.x = 0;
        SpaceScanManager.a().p();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(12);
        layoutParams.addRule(14);
        layoutParams.topMargin = 0;
        layoutParams.leftMargin = 0;
        updateViewLayout(this.r, layoutParams);
        this.y.a();
        this.r.startAnimation(f());
        j.a().a(STConst.ST_PAGE_DRAG_TO_ACCELERATE_PAGEID, STConst.ST_DEFAULT_SLOT, 100);
    }

    private Animation f() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) ((-this.o) / 2), (float) ((-df.c()) - this.o));
        translateAnimation.setDuration(1200);
        translateAnimation.setInterpolator(new AnticipateInterpolator());
        translateAnimation.setAnimationListener(new e(this));
        return translateAnimation;
    }

    private void a(boolean z2) {
        int i2;
        int i3 = 0;
        if (j.a().e()) {
            int i4 = ((int) (this.i - this.m)) - this.o;
            if (i4 > (df.c() - df.a()) - this.g.height) {
                i2 = (df.c() - df.a()) - this.g.height;
            } else if (i4 < 0) {
                i2 = 0;
            } else {
                i2 = i4;
            }
            if (!z2) {
                int b2 = df.b();
                if (this.h < ((float) (b2 / 2))) {
                    b2 = 0;
                }
                int i5 = ((int) (((float) b2) - this.l)) - (this.n / 2);
                if (i5 > df.b() - this.g.width) {
                    i3 = df.b() - this.g.width;
                } else if (i5 >= 0) {
                    i3 = i5;
                }
                this.g.topMargin = i2;
                this.g.leftMargin = i3;
                updateViewLayout(this.r, this.g);
                this.f.width = f1280a;
                this.f.height = b;
                this.f.y = (int) (this.i - this.m);
                this.f.x = (int) (((float) b2) - this.l);
                try {
                    this.c.updateViewLayout(this, this.f);
                    postDelayed(new g(this), 100);
                } catch (Throwable th) {
                    th.printStackTrace();
                    XLog.d("floatingwindow", "FloatWindowSmallView >> <updateViewPosition> updateViewLayout exception");
                    return;
                }
            } else {
                int i6 = (int) this.h;
                if (this.q) {
                    this.g.width = this.n;
                    this.g.height = this.o;
                } else {
                    this.g.width = f1280a;
                    this.g.height = b;
                }
                int i7 = ((int) (((float) i6) - this.l)) - (this.n / 2);
                if (i7 > df.b() - this.g.width) {
                    i3 = df.b() - this.g.width;
                } else if (i7 >= 0) {
                    i3 = i7;
                }
                this.g.topMargin = i2;
                this.g.leftMargin = i3;
                updateViewLayout(this.r, this.g);
            }
            b();
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.q && this.e.getVisibility() != 0) {
            this.d.setVisibility(4);
            this.e.setVisibility(0);
            if (!this.s.isRunning()) {
                this.s.start();
            }
            h();
        } else if (!this.q) {
            this.d.setVisibility(0);
            this.e.setVisibility(8);
            if (this.s.isRunning()) {
                this.s.stop();
            }
            i();
        }
    }

    private void h() {
        if (this.y == null) {
            this.y = new RocketLauncher(AstApp.i());
            if (this.z == null) {
                this.z = new RelativeLayout.LayoutParams(RocketLauncher.f1282a, RocketLauncher.c);
                this.z.addRule(12);
                this.z.addRule(14);
            }
            addView(this.y, this.z);
        }
    }

    private void i() {
        if (this.y != null) {
            removeView(this.y);
            this.y = null;
        }
    }

    private boolean j() {
        int b2 = df.b();
        int c2 = df.c();
        if (this.g.leftMargin <= (b2 / 2) - (RocketLauncher.f1282a / 2) || this.g.leftMargin + this.g.width >= (b2 / 2) + (RocketLauncher.f1282a / 2) || this.g.topMargin + this.g.height <= ((c2 - df.d()) - df.a()) - RocketLauncher.b) {
            return false;
        }
        return true;
    }

    public void b() {
        if (this.y != null) {
            this.y.a(j());
        }
    }

    private void k() {
        j.a().c(getContext());
        j.a().b(getContext());
        j.a().a(STConst.ST_PAGE_FLOAT_BIG_WINDOW_PAGEID, STConst.ST_DEFAULT_SLOT, 100);
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_SUCCESS:
                this.t = true;
                this.u = true;
                this.x = ((Long) message.obj).longValue();
                if (this.v) {
                    l();
                } else {
                    this.d.setVisibility(8);
                }
                XLog.d("floatingwindow", "加速成功，释放内存：" + bt.c(this.x));
                return;
            case EventDispatcherEnum.UI_EVENT_MGR_MEMORY_CLEAN_FAIL:
                this.t = true;
                this.u = false;
                if (this.v) {
                    l();
                    return;
                } else {
                    this.d.setVisibility(8);
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        String string;
        String str = null;
        if (this.u) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.p < 30000 || this.x < 31457280) {
                string = AstApp.i().getResources().getString(R.string.floating_window_best_state_tips);
            } else {
                this.p = currentTimeMillis;
                string = AstApp.i().getResources().getString(R.string.floating_window_result_release_memory_tips, bt.c(this.x));
                str = AstApp.i().getResources().getString(R.string.floating_window_result_speed_up_tips, ((this.x * 100) / t.q()) + "%");
            }
        } else {
            string = AstApp.i().getResources().getString(R.string.floating_window_do_well_tips);
        }
        j.a().a(string, str);
        postDelayed(new h(this), 4000);
    }
}
