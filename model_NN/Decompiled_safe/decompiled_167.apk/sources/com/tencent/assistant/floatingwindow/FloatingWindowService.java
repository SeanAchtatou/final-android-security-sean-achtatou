package com.tencent.assistant.floatingwindow;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.IBinder;
import com.qq.AppService.AstApp;
import com.qq.ndk.NativeFileObject;
import com.tencent.assistant.localres.ApkResourceManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

/* compiled from: ProGuard */
public class FloatingWindowService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private Handler f1281a = new Handler();
    private Timer b;
    private List<String> c = new ArrayList();
    /* access modifiers changed from: private */
    public List<String> d = new ArrayList();
    private long e = 0;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (this.b == null) {
            this.b = new Timer();
            this.b.scheduleAtFixedRate(new r(this), 0, 1500);
        }
        return super.onStartCommand(intent, i, i2);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.b != null) {
            this.b.cancel();
            this.b = null;
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        boolean c2 = c();
        if (c2 && !j.a().e()) {
            this.f1281a.post(new o(this));
        } else if (!c2 && j.a().e()) {
            this.f1281a.post(new p(this));
        } else if (c2 && j.a().e()) {
            long currentTimeMillis = System.currentTimeMillis();
            if (this.e == 0 || currentTimeMillis - this.e >= 4500) {
                this.e = currentTimeMillis;
                this.f1281a.post(new q(this));
            }
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!AstApp.i().l()) {
            if (this.c.isEmpty() || !this.c.equals(this.d)) {
                this.c.clear();
                this.c.addAll(this.d);
                ApkResourceManager.getInstance().updateAppLauncherTime(this.d);
            }
        }
    }

    private boolean c() {
        List<String> d2 = d();
        for (int i = 0; i < d2.size(); i++) {
            if (this.d.contains(d2.get(i))) {
                return true;
            }
        }
        return false;
    }

    private List<String> d() {
        ArrayList arrayList = new ArrayList();
        try {
            PackageManager packageManager = getPackageManager();
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.HOME");
            for (ResolveInfo resolveInfo : packageManager.queryIntentActivities(intent, NativeFileObject.S_IFIFO)) {
                arrayList.add(resolveInfo.activityInfo.packageName);
            }
        } catch (Throwable th) {
        }
        return arrayList;
    }
}
