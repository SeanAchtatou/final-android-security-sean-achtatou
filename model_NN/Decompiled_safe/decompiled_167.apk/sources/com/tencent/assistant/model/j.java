package com.tencent.assistant.model;

/* compiled from: ProGuard */
public class j implements Comparable<j> {

    /* renamed from: a  reason: collision with root package name */
    public int f1666a;
    public String b;
    public String c;
    public int d;
    public String e;
    public String f;
    public int g;
    public String h;
    public String i;
    public long j;
    public int k;
    public int l;
    public int m;
    public int n;
    public String o;
    public String p;
    public int q;
    public int r;
    public String s;

    public String a() {
        return String.valueOf(this.c);
    }

    /* renamed from: a */
    public int compareTo(j jVar) {
        return this.g - jVar.g;
    }

    public String toString() {
        return "PluginDownloadInfo{pluginId=" + this.f1666a + ", downloadTicket='" + this.b + '\'' + ", pluginPackageName='" + this.c + '\'' + ", version=" + this.d + ", name='" + this.e + '\'' + ", desc='" + this.f + '\'' + ", displayOrder=" + this.g + ", iconUrl='" + this.h + '\'' + ", imgUrl='" + this.i + '\'' + ", fileSize=" + this.j + ", minApiLevel=" + this.k + ", minPluginVersion=" + this.l + ", minBaoVersion=" + this.m + ", needPreDownload=" + this.n + ", downUrl='" + this.o + '\'' + ", startActivity='" + this.p + '\'' + '}';
    }
}
