package com.tencent.wcs.proxy.e;

import com.tencent.assistant.utils.q;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private ScheduledExecutorService f3914a;

    private a() {
        this.f3914a = Executors.newScheduledThreadPool(7, new q("AsyncService"));
    }

    public static a a() {
        return c.f3915a;
    }

    public void a(Runnable runnable, long j, TimeUnit timeUnit) {
        if (this.f3914a != null) {
            this.f3914a.schedule(runnable, j, timeUnit);
        }
    }

    public void a(Runnable runnable) {
        if (this.f3914a != null) {
            this.f3914a.submit(runnable);
        }
    }
}
