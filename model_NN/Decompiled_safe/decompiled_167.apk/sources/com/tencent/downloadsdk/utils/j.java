package com.tencent.downloadsdk.utils;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.text.TextUtils;
import java.util.concurrent.atomic.AtomicInteger;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private static Object f3650a = new Object();
    private static Handler[] b = new Handler[3];
    private static final AtomicInteger c = new AtomicInteger(0);

    static {
        for (int i = 0; i < 3; i++) {
            HandlerThread handlerThread = new HandlerThread("download-handler-thread-" + i);
            handlerThread.start();
            try {
                b[i] = new Handler(handlerThread.getLooper());
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    b[i] = new Handler(handlerThread.getLooper());
                } catch (Exception e2) {
                }
            }
        }
    }

    public static Handler a() {
        Handler handler = b[c.getAndIncrement() % 3];
        return handler == null ? b[0] : handler;
    }

    public static Handler a(String str) {
        if (TextUtils.isEmpty(str)) {
            str = "default-thread";
        }
        HandlerThread handlerThread = new HandlerThread(str);
        handlerThread.start();
        Looper looper = handlerThread.getLooper();
        if (looper != null) {
            return new Handler(looper);
        }
        return null;
    }
}
