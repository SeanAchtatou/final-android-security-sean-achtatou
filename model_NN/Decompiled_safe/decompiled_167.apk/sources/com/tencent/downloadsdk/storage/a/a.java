package com.tencent.downloadsdk.storage.a;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.DownloadSettingInfo;
import com.tencent.downloadsdk.storage.helper.SDKDBHelper;
import com.tencent.downloadsdk.storage.helper.SqliteHelper;
import com.tencent.downloadsdk.utils.n;
import java.util.List;

public class a implements d {

    /* renamed from: a  reason: collision with root package name */
    public final String f3638a = "download_setting_table";
    private final String b = "DownloadSettingTable";
    private final String c = "network_type";
    private final String d = "version";
    private final String e = "setting";
    private final String[] f = {"setting"};
    private final byte[] g = new byte[0];

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0062 A[Catch:{ Exception -> 0x0053, all -> 0x005f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long a(int r15) {
        /*
            r14 = this;
            r10 = 0
            r11 = 0
            byte[] r13 = r14.g
            monitor-enter(r13)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0053, all -> 0x005f }
            r4.<init>()     // Catch:{ Exception -> 0x0053, all -> 0x005f }
            java.lang.String r0 = "network_type"
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x0053, all -> 0x005f }
            java.lang.String r1 = "='"
            r0.append(r1)     // Catch:{ Exception -> 0x0053, all -> 0x005f }
            java.lang.StringBuilder r0 = r4.append(r15)     // Catch:{ Exception -> 0x0053, all -> 0x005f }
            java.lang.String r1 = "'"
            r0.append(r1)     // Catch:{ Exception -> 0x0053, all -> 0x005f }
            com.tencent.downloadsdk.storage.helper.SqliteHelper r0 = r14.e()     // Catch:{ Exception -> 0x0053, all -> 0x005f }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x0053, all -> 0x005f }
            r1 = 1
            java.lang.String r2 = "download_setting_table"
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x0053, all -> 0x005f }
            r5 = 0
            java.lang.String r6 = "version"
            r3[r5] = r6     // Catch:{ Exception -> 0x0053, all -> 0x005f }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0053, all -> 0x005f }
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "1"
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0053, all -> 0x005f }
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x006c }
            if (r0 == 0) goto L_0x0072
            r0 = 0
            long r2 = r1.getLong(r0)     // Catch:{ Exception -> 0x006c }
        L_0x004b:
            if (r1 == 0) goto L_0x0070
            r1.close()     // Catch:{ all -> 0x0066 }
            r0 = r2
        L_0x0051:
            monitor-exit(r13)     // Catch:{ all -> 0x0066 }
            return r0
        L_0x0053:
            r0 = move-exception
            r1 = r10
        L_0x0055:
            r0.printStackTrace()     // Catch:{ all -> 0x0069 }
            if (r1 == 0) goto L_0x006e
            r1.close()     // Catch:{ all -> 0x0066 }
            r0 = r11
            goto L_0x0051
        L_0x005f:
            r0 = move-exception
        L_0x0060:
            if (r10 == 0) goto L_0x0065
            r10.close()     // Catch:{ all -> 0x0066 }
        L_0x0065:
            throw r0     // Catch:{ all -> 0x0066 }
        L_0x0066:
            r0 = move-exception
            monitor-exit(r13)     // Catch:{ all -> 0x0066 }
            throw r0
        L_0x0069:
            r0 = move-exception
            r10 = r1
            goto L_0x0060
        L_0x006c:
            r0 = move-exception
            goto L_0x0055
        L_0x006e:
            r0 = r11
            goto L_0x0051
        L_0x0070:
            r0 = r2
            goto L_0x0051
        L_0x0072:
            r2 = r11
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.storage.a.a.a(int):long");
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x004b A[SYNTHETIC, Splitter:B:26:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0056 A[SYNTHETIC, Splitter:B:34:0x0056] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.tencent.downloadsdk.DownloadSettingInfo> a() {
        /*
            r12 = this;
            r10 = 0
            byte[] r11 = r12.g
            monitor-enter(r11)
            com.tencent.downloadsdk.storage.helper.SqliteHelper r0 = r12.e()     // Catch:{ Exception -> 0x0044, all -> 0x0052 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x0044, all -> 0x0052 }
            r1 = 1
            java.lang.String r2 = "download_setting_table"
            java.lang.String[] r3 = r12.f     // Catch:{ Exception -> 0x0044, all -> 0x0052 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0044, all -> 0x0052 }
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x005c }
            if (r0 == 0) goto L_0x003d
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Exception -> 0x005c }
            r2.<init>()     // Catch:{ Exception -> 0x005c }
        L_0x0026:
            r0 = 0
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x005e }
            java.lang.Object r0 = com.tencent.downloadsdk.utils.n.a(r0)     // Catch:{ Exception -> 0x005e }
            com.tencent.downloadsdk.DownloadSettingInfo r0 = (com.tencent.downloadsdk.DownloadSettingInfo) r0     // Catch:{ Exception -> 0x005e }
            if (r0 == 0) goto L_0x0036
            r2.add(r0)     // Catch:{ Exception -> 0x005e }
        L_0x0036:
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x005e }
            if (r0 != 0) goto L_0x0026
            r10 = r2
        L_0x003d:
            if (r1 == 0) goto L_0x0042
            r1.close()     // Catch:{ all -> 0x004f }
        L_0x0042:
            monitor-exit(r11)     // Catch:{ all -> 0x004f }
            return r10
        L_0x0044:
            r0 = move-exception
            r1 = r10
        L_0x0046:
            r0.printStackTrace()     // Catch:{ all -> 0x005a }
            if (r1 == 0) goto L_0x0042
            r1.close()     // Catch:{ all -> 0x004f }
            goto L_0x0042
        L_0x004f:
            r0 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x004f }
            throw r0
        L_0x0052:
            r0 = move-exception
            r1 = r10
        L_0x0054:
            if (r1 == 0) goto L_0x0059
            r1.close()     // Catch:{ all -> 0x004f }
        L_0x0059:
            throw r0     // Catch:{ all -> 0x004f }
        L_0x005a:
            r0 = move-exception
            goto L_0x0054
        L_0x005c:
            r0 = move-exception
            goto L_0x0046
        L_0x005e:
            r0 = move-exception
            r10 = r2
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.storage.a.a.a():java.util.ArrayList");
    }

    public void a(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
    }

    public synchronized boolean a(List<DownloadSettingInfo> list) {
        boolean z = false;
        synchronized (this) {
            synchronized (this.g) {
                if (list != null) {
                    if (list.size() > 0) {
                        try {
                            boolean z2 = true;
                            for (DownloadSettingInfo next : list) {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("network_type", Integer.valueOf(next.b));
                                contentValues.put("version", Long.valueOf(next.f3579a));
                                contentValues.put("setting", n.a(next));
                                z2 = z2 && e().getWritableDatabase().replace("download_setting_table", null, contentValues) > 0;
                            }
                            z = z2;
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                }
            }
        }
        return z;
    }

    public String[] a(int i, int i2) {
        if (i != 1 || i2 != 2) {
            return null;
        }
        return new String[]{d()};
    }

    public int b() {
        return 0;
    }

    public void b(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public String c() {
        return "download_setting_table";
    }

    public String d() {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE if not exists ").append("download_setting_table");
        sb.append(" (").append("network_type").append(" INTEGER PRIMARY KEY, ");
        sb.append("version").append(" INTEGER, ");
        sb.append("setting").append(" VARCHAR);");
        return sb.toString();
    }

    public SqliteHelper e() {
        return SDKDBHelper.getDBHelper(DownloadManager.a().b());
    }
}
