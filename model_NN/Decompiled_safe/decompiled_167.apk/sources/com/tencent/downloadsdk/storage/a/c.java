package com.tencent.downloadsdk.storage.a;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.storage.helper.SDKDBHelper;
import com.tencent.downloadsdk.storage.helper.SqliteHelper;

public class c implements d {

    /* renamed from: a  reason: collision with root package name */
    private final String f3640a = "globalkv";
    private final String b = "key";
    private final String c = "value";
    private final String[] d = {"value"};

    public long a() {
        return Long.parseLong(a("setting_ts"));
    }

    public String a(String str) {
        return b(str, STConst.ST_DEFAULT_SLOT);
    }

    public void a(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
    }

    public boolean a(long j) {
        return a("setting_ts", j + Constants.STR_EMPTY);
    }

    public boolean a(String str, String str2) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return false;
        }
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("key", str);
            contentValues.put("value", str2);
            return e().getWritableDatabase().replace("globalkv", null, contentValues) > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String[] a(int i, int i2) {
        if (i != 1 || i2 != 2) {
            return null;
        }
        return new String[]{d()};
    }

    public int b() {
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0052  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String b(java.lang.String r12, java.lang.String r13) {
        /*
            r11 = this;
            r10 = 0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0044, all -> 0x004f }
            r4.<init>()     // Catch:{ SQLiteException -> 0x0044, all -> 0x004f }
            java.lang.String r0 = "key"
            r4.append(r0)     // Catch:{ SQLiteException -> 0x0044, all -> 0x004f }
            java.lang.String r0 = "='"
            r4.append(r0)     // Catch:{ SQLiteException -> 0x0044, all -> 0x004f }
            r4.append(r12)     // Catch:{ SQLiteException -> 0x0044, all -> 0x004f }
            java.lang.String r0 = "'"
            r4.append(r0)     // Catch:{ SQLiteException -> 0x0044, all -> 0x004f }
            com.tencent.downloadsdk.storage.helper.SqliteHelper r0 = r11.e()     // Catch:{ SQLiteException -> 0x0044, all -> 0x004f }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0044, all -> 0x004f }
            r1 = 1
            java.lang.String r2 = "globalkv"
            java.lang.String[] r3 = r11.d     // Catch:{ SQLiteException -> 0x0044, all -> 0x004f }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLiteException -> 0x0044, all -> 0x004f }
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = "1"
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ SQLiteException -> 0x0044, all -> 0x004f }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0059 }
            if (r0 == 0) goto L_0x003e
            r0 = 0
            java.lang.String r13 = r1.getString(r0)     // Catch:{ SQLiteException -> 0x0059 }
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.close()
        L_0x0043:
            return r13
        L_0x0044:
            r0 = move-exception
            r1 = r10
        L_0x0046:
            r0.printStackTrace()     // Catch:{ all -> 0x0056 }
            if (r1 == 0) goto L_0x0043
            r1.close()
            goto L_0x0043
        L_0x004f:
            r0 = move-exception
        L_0x0050:
            if (r10 == 0) goto L_0x0055
            r10.close()
        L_0x0055:
            throw r0
        L_0x0056:
            r0 = move-exception
            r10 = r1
            goto L_0x0050
        L_0x0059:
            r0 = move-exception
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.storage.a.c.b(java.lang.String, java.lang.String):java.lang.String");
    }

    public void b(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public String c() {
        return "globalkv";
    }

    public String d() {
        StringBuilder sb = new StringBuilder(200);
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append("globalkv");
        sb.append(" (");
        sb.append("key");
        sb.append(" VARCHAR primary key,");
        sb.append("value");
        sb.append(" VARCHAR)");
        return sb.toString();
    }

    public SqliteHelper e() {
        return SDKDBHelper.getDBHelper(DownloadManager.a().b());
    }
}
