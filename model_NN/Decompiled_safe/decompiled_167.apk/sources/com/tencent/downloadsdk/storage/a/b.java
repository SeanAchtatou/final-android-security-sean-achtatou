package com.tencent.downloadsdk.storage.a;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.downloadsdk.storage.helper.SDKDBHelper;
import com.tencent.downloadsdk.storage.helper.SqliteHelper;
import com.tencent.open.SocialConstants;
import java.util.List;

public class b implements d {

    /* renamed from: a  reason: collision with root package name */
    public static String f3639a = "DownloadTaskTable";
    private final byte[] b = new byte[0];

    public SqliteHelper a() {
        return SDKDBHelper.getDBHelper(DownloadManager.a().b());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        return null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x008d A[SYNTHETIC, Splitter:B:38:0x008d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(int r11, java.lang.String r12) {
        /*
            r10 = this;
            r8 = 0
            byte[] r9 = r10.b
            monitor-enter(r9)
            com.tencent.downloadsdk.storage.helper.SqliteHelper r0 = r10.a()     // Catch:{ Exception -> 0x007b, all -> 0x0089 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x007b, all -> 0x0089 }
            java.lang.String r1 = "tbl_download"
            r2 = 0
            java.lang.String r3 = "type=? and id=?"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x007b, all -> 0x0089 }
            r5 = 0
            java.lang.String r6 = java.lang.String.valueOf(r11)     // Catch:{ Exception -> 0x007b, all -> 0x0089 }
            r4[r5] = r6     // Catch:{ Exception -> 0x007b, all -> 0x0089 }
            r5 = 1
            r4[r5] = r12     // Catch:{ Exception -> 0x007b, all -> 0x0089 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x007b, all -> 0x0089 }
            if (r1 == 0) goto L_0x0073
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0093 }
            if (r0 == 0) goto L_0x0073
            java.lang.String r0 = "save_dir"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r2 = "save_name"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0093 }
            boolean r3 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x0093 }
            if (r3 != 0) goto L_0x006b
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x0093 }
            if (r3 != 0) goto L_0x006b
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0093 }
            r3.<init>()     // Catch:{ Exception -> 0x0093 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x0093 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0093 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0093 }
            if (r1 == 0) goto L_0x0069
            r1.close()     // Catch:{ all -> 0x0086 }
        L_0x0069:
            monitor-exit(r9)     // Catch:{ all -> 0x0086 }
        L_0x006a:
            return r0
        L_0x006b:
            if (r1 == 0) goto L_0x0070
            r1.close()     // Catch:{ all -> 0x0086 }
        L_0x0070:
            monitor-exit(r9)     // Catch:{ all -> 0x0086 }
            r0 = r8
            goto L_0x006a
        L_0x0073:
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ all -> 0x0086 }
        L_0x0078:
            monitor-exit(r9)     // Catch:{ all -> 0x0086 }
            r0 = r8
            goto L_0x006a
        L_0x007b:
            r0 = move-exception
            r1 = r8
        L_0x007d:
            r0.printStackTrace()     // Catch:{ all -> 0x0091 }
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ all -> 0x0086 }
            goto L_0x0078
        L_0x0086:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0086 }
            throw r0
        L_0x0089:
            r0 = move-exception
            r1 = r8
        L_0x008b:
            if (r1 == 0) goto L_0x0090
            r1.close()     // Catch:{ all -> 0x0086 }
        L_0x0090:
            throw r0     // Catch:{ all -> 0x0086 }
        L_0x0091:
            r0 = move-exception
            goto L_0x008b
        L_0x0093:
            r0 = move-exception
            goto L_0x007d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.storage.a.b.a(int, java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x007e A[SYNTHETIC, Splitter:B:31:0x007e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.tencent.downloadsdk.ac> a(int r11) {
        /*
            r10 = this;
            r8 = 0
            byte[] r9 = r10.b
            monitor-enter(r9)
            com.tencent.downloadsdk.storage.helper.SqliteHelper r0 = r10.a()     // Catch:{ Exception -> 0x006c, all -> 0x007a }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x006c, all -> 0x007a }
            java.lang.String r1 = "tbl_download"
            r2 = 0
            java.lang.String r3 = "type=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x006c, all -> 0x007a }
            r5 = 0
            java.lang.String r6 = java.lang.String.valueOf(r11)     // Catch:{ Exception -> 0x006c, all -> 0x007a }
            r4[r5] = r6     // Catch:{ Exception -> 0x006c, all -> 0x007a }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x006c, all -> 0x007a }
            if (r1 == 0) goto L_0x0065
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0084 }
            if (r0 == 0) goto L_0x0065
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Exception -> 0x0084 }
            r2.<init>()     // Catch:{ Exception -> 0x0084 }
        L_0x002f:
            com.tencent.downloadsdk.ac r0 = new com.tencent.downloadsdk.ac     // Catch:{ Exception -> 0x0086 }
            r0.<init>()     // Catch:{ Exception -> 0x0086 }
            long r3 = (long) r11     // Catch:{ Exception -> 0x0086 }
            r0.f3588a = r3     // Catch:{ Exception -> 0x0086 }
            java.lang.String r3 = "id"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x0086 }
            java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x0086 }
            r0.b = r3     // Catch:{ Exception -> 0x0086 }
            java.lang.String r3 = "total_length"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x0086 }
            long r3 = r1.getLong(r3)     // Catch:{ Exception -> 0x0086 }
            r0.d = r3     // Catch:{ Exception -> 0x0086 }
            java.lang.String r3 = "received_length"
            int r3 = r1.getColumnIndex(r3)     // Catch:{ Exception -> 0x0086 }
            long r3 = r1.getLong(r3)     // Catch:{ Exception -> 0x0086 }
            r0.c = r3     // Catch:{ Exception -> 0x0086 }
            r2.add(r0)     // Catch:{ Exception -> 0x0086 }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0086 }
            if (r0 != 0) goto L_0x002f
            r8 = r2
        L_0x0065:
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ all -> 0x0077 }
        L_0x006a:
            monitor-exit(r9)     // Catch:{ all -> 0x0077 }
            return r8
        L_0x006c:
            r0 = move-exception
            r1 = r8
        L_0x006e:
            r0.printStackTrace()     // Catch:{ all -> 0x0082 }
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ all -> 0x0077 }
            goto L_0x006a
        L_0x0077:
            r0 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0077 }
            throw r0
        L_0x007a:
            r0 = move-exception
            r1 = r8
        L_0x007c:
            if (r1 == 0) goto L_0x0081
            r1.close()     // Catch:{ all -> 0x0077 }
        L_0x0081:
            throw r0     // Catch:{ all -> 0x0077 }
        L_0x0082:
            r0 = move-exception
            goto L_0x007c
        L_0x0084:
            r0 = move-exception
            goto L_0x006e
        L_0x0086:
            r0 = move-exception
            r8 = r2
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.storage.a.b.a(int):java.util.ArrayList");
    }

    public void a(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void a(int i, String str, List<String> list, int i2, int i3, String str2, String str3) {
        int i4 = 0;
        synchronized (this.b) {
            try {
                SQLiteDatabase writableDatabase = a().getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                String str4 = Constants.STR_EMPTY;
                if (list != null && list.size() > 0) {
                    StringBuilder sb = new StringBuilder();
                    while (i4 < list.size() && i4 < 3) {
                        sb.append(list.get(i4));
                        sb.append(",");
                        i4++;
                    }
                    str4 = sb.toString();
                }
                contentValues.put("urls", str4);
                contentValues.put("status", Integer.valueOf(i2));
                contentValues.put("priority", Integer.valueOf(i3));
                contentValues.put("save_dir", str2);
                contentValues.put("save_name", str3);
                if (writableDatabase.update("tbl_download", contentValues, "type=? and id=?", new String[]{String.valueOf(i), str}) == 0) {
                    contentValues.put("id", str);
                    contentValues.put(SocialConstants.PARAM_TYPE, Integer.valueOf(i));
                    writableDatabase.insert("tbl_download", null, contentValues);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void a(int i, String str, byte[] bArr) {
        synchronized (this.b) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("urls_mapping", bArr);
                a().getReadableDatabase().update("tbl_download", contentValues, "type=? and id=?", new String[]{String.valueOf(i), str});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
    }

    public boolean a(int i, String str, int i2) {
        boolean z = false;
        synchronized (this.b) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("status", Integer.valueOf(i2));
                if (a().getReadableDatabase().update("tbl_download", contentValues, "type=? and id=?", new String[]{String.valueOf(i), str}) > 0) {
                    z = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return z;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(int r9, java.lang.String r10, long r11, long r13) {
        /*
            r8 = this;
            byte[] r1 = r8.b
            monitor-enter(r1)
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ Exception -> 0x003e }
            r0.<init>()     // Catch:{ Exception -> 0x003e }
            java.lang.String r2 = "received_length"
            java.lang.Long r3 = java.lang.Long.valueOf(r11)     // Catch:{ Exception -> 0x003e }
            r0.put(r2, r3)     // Catch:{ Exception -> 0x003e }
            java.lang.String r2 = "last_modify"
            java.lang.Long r3 = java.lang.Long.valueOf(r13)     // Catch:{ Exception -> 0x003e }
            r0.put(r2, r3)     // Catch:{ Exception -> 0x003e }
            com.tencent.downloadsdk.storage.helper.SqliteHelper r2 = r8.a()     // Catch:{ Exception -> 0x003e }
            android.database.sqlite.SQLiteDatabase r2 = r2.getReadableDatabase()     // Catch:{ Exception -> 0x003e }
            java.lang.String r3 = "tbl_download"
            java.lang.String r4 = "type=? and id=?"
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x003e }
            r6 = 0
            java.lang.String r7 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x003e }
            r5[r6] = r7     // Catch:{ Exception -> 0x003e }
            r6 = 1
            r5[r6] = r10     // Catch:{ Exception -> 0x003e }
            int r0 = r2.update(r3, r0, r4, r5)     // Catch:{ Exception -> 0x003e }
            if (r0 <= 0) goto L_0x003c
            r0 = 1
        L_0x003a:
            monitor-exit(r1)     // Catch:{ all -> 0x006b }
        L_0x003b:
            return r0
        L_0x003c:
            r0 = 0
            goto L_0x003a
        L_0x003e:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x006b }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x006b }
            r2.<init>()     // Catch:{ all -> 0x006b }
            java.lang.String r3 = "saveReceivedLength fail."
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x006b }
            java.lang.Class r3 = r0.getClass()     // Catch:{ all -> 0x006b }
            java.lang.String r3 = r3.getName()     // Catch:{ all -> 0x006b }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x006b }
            java.lang.String r0 = r0.getLocalizedMessage()     // Catch:{ all -> 0x006b }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x006b }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x006b }
            com.tencent.downloadsdk.utils.f.a(r0)     // Catch:{ all -> 0x006b }
            monitor-exit(r1)     // Catch:{ all -> 0x006b }
            r0 = 0
            goto L_0x003b
        L_0x006b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x006b }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.storage.a.b.a(int, java.lang.String, long, long):boolean");
    }

    public boolean a(int i, String str, long j, boolean z) {
        boolean z2 = false;
        synchronized (this.b) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("total_length", Long.valueOf(j));
                contentValues.put("rangeable", Integer.valueOf(z ? 1 : 0));
                if (a().getReadableDatabase().update("tbl_download", contentValues, "type=? and id=?", new String[]{String.valueOf(i), str}) > 0) {
                    z2 = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return z2;
    }

    public boolean a(int i, String str, String str2) {
        boolean z = false;
        synchronized (this.b) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("save_name", str2);
                if (a().getReadableDatabase().update("tbl_download", contentValues, "type=? and id=?", new String[]{String.valueOf(i), str}) > 0) {
                    z = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return true;
     */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x010d A[SYNTHETIC, Splitter:B:46:0x010d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.tencent.downloadsdk.DownloadTask r13) {
        /*
            r12 = this;
            r8 = 1
            r9 = 0
            r10 = 0
            byte[] r11 = r12.b
            monitor-enter(r11)
            com.tencent.downloadsdk.storage.helper.SqliteHelper r0 = r12.a()     // Catch:{ Exception -> 0x00fb, all -> 0x0109 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ Exception -> 0x00fb, all -> 0x0109 }
            java.lang.String r1 = "tbl_download"
            r2 = 0
            java.lang.String r3 = "type=? and id=?"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x00fb, all -> 0x0109 }
            r5 = 0
            int r6 = r13.c     // Catch:{ Exception -> 0x00fb, all -> 0x0109 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x00fb, all -> 0x0109 }
            r4[r5] = r6     // Catch:{ Exception -> 0x00fb, all -> 0x0109 }
            r5 = 1
            java.lang.String r6 = r13.d     // Catch:{ Exception -> 0x00fb, all -> 0x0109 }
            r4[r5] = r6     // Catch:{ Exception -> 0x00fb, all -> 0x0109 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00fb, all -> 0x0109 }
            if (r1 == 0) goto L_0x00f3
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0113 }
            if (r0 == 0) goto L_0x00f3
            java.lang.String r0 = "urls"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x0113 }
            boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x0113 }
            if (r2 != 0) goto L_0x006a
            java.lang.String r2 = ","
            java.lang.String[] r2 = r0.split(r2)     // Catch:{ Exception -> 0x0113 }
            java.util.List<java.lang.String> r0 = r13.g     // Catch:{ Exception -> 0x0113 }
            if (r0 != 0) goto L_0x0054
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x0113 }
            r0.<init>()     // Catch:{ Exception -> 0x0113 }
            r13.g = r0     // Catch:{ Exception -> 0x0113 }
        L_0x0054:
            int r3 = r2.length     // Catch:{ Exception -> 0x0113 }
            r0 = r9
        L_0x0056:
            if (r0 >= r3) goto L_0x006a
            r4 = r2[r0]     // Catch:{ Exception -> 0x0113 }
            java.util.List<java.lang.String> r5 = r13.g     // Catch:{ Exception -> 0x0113 }
            boolean r5 = r5.contains(r4)     // Catch:{ Exception -> 0x0113 }
            if (r5 != 0) goto L_0x0067
            java.util.List<java.lang.String> r5 = r13.g     // Catch:{ Exception -> 0x0113 }
            r5.add(r4)     // Catch:{ Exception -> 0x0113 }
        L_0x0067:
            int r0 = r0 + 1
            goto L_0x0056
        L_0x006a:
            java.lang.String r0 = "urls_mapping"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0113 }
            byte[] r0 = r1.getBlob(r0)     // Catch:{ Exception -> 0x0113 }
            java.util.List r0 = com.tencent.downloadsdk.ao.b(r0)     // Catch:{ Exception -> 0x0113 }
            r13.p = r0     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = "total_length"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0113 }
            long r2 = r1.getLong(r0)     // Catch:{ Exception -> 0x0113 }
            r13.k = r2     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = "received_length"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0113 }
            long r2 = r1.getLong(r0)     // Catch:{ Exception -> 0x0113 }
            r13.i = r2     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = "save_dir"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x0113 }
            r13.l = r0     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = "save_name"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x0113 }
            r13.m = r0     // Catch:{ Exception -> 0x0113 }
            com.tencent.downloadsdk.DownloadTask$PRIORITY[] r0 = com.tencent.downloadsdk.DownloadTask.PRIORITY.values()     // Catch:{ Exception -> 0x0113 }
            java.lang.String r2 = "priority"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0113 }
            int r2 = r1.getInt(r2)     // Catch:{ Exception -> 0x0113 }
            r0 = r0[r2]     // Catch:{ Exception -> 0x0113 }
            r13.b = r0     // Catch:{ Exception -> 0x0113 }
            com.tencent.downloadsdk.DownloadTask$STATUS[] r0 = com.tencent.downloadsdk.DownloadTask.STATUS.values()     // Catch:{ Exception -> 0x0113 }
            java.lang.String r2 = "status"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0113 }
            int r2 = r1.getInt(r2)     // Catch:{ Exception -> 0x0113 }
            r0 = r0[r2]     // Catch:{ Exception -> 0x0113 }
            r13.f3580a = r0     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = "last_modify"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0113 }
            long r2 = r1.getLong(r0)     // Catch:{ Exception -> 0x0113 }
            r13.n = r2     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = "rangeable"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0113 }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x0113 }
            if (r0 != r8) goto L_0x00f1
            r0 = r8
        L_0x00e7:
            r13.o = r0     // Catch:{ Exception -> 0x0113 }
            if (r1 == 0) goto L_0x00ee
            r1.close()     // Catch:{ all -> 0x0106 }
        L_0x00ee:
            monitor-exit(r11)     // Catch:{ all -> 0x0106 }
            r0 = r8
        L_0x00f0:
            return r0
        L_0x00f1:
            r0 = r9
            goto L_0x00e7
        L_0x00f3:
            if (r1 == 0) goto L_0x00f8
            r1.close()     // Catch:{ all -> 0x0106 }
        L_0x00f8:
            monitor-exit(r11)     // Catch:{ all -> 0x0106 }
            r0 = r9
            goto L_0x00f0
        L_0x00fb:
            r0 = move-exception
            r1 = r10
        L_0x00fd:
            r0.printStackTrace()     // Catch:{ all -> 0x0111 }
            if (r1 == 0) goto L_0x00f8
            r1.close()     // Catch:{ all -> 0x0106 }
            goto L_0x00f8
        L_0x0106:
            r0 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x0106 }
            throw r0
        L_0x0109:
            r0 = move-exception
            r1 = r10
        L_0x010b:
            if (r1 == 0) goto L_0x0110
            r1.close()     // Catch:{ all -> 0x0106 }
        L_0x0110:
            throw r0     // Catch:{ all -> 0x0106 }
        L_0x0111:
            r0 = move-exception
            goto L_0x010b
        L_0x0113:
            r0 = move-exception
            goto L_0x00fd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.downloadsdk.storage.a.b.a(com.tencent.downloadsdk.DownloadTask):boolean");
    }

    public String[] a(int i, int i2) {
        if (i != 1 || i2 != 2) {
            return null;
        }
        return new String[]{d()};
    }

    public int b() {
        return 0;
    }

    public void b(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void b(int i, String str) {
        synchronized (this.b) {
            try {
                a().getReadableDatabase().delete("tbl_download", "type=? and id=?", new String[]{String.valueOf(i), str});
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String c() {
        return "tbl_download";
    }

    public String d() {
        return "CREATE TABLE IF NOT EXISTS tbl_download (_id INTEGER PRIMARY KEY AUTOINCREMENT,type INTEGER,id TEXT,urls TEXT,urls_mapping BLOB,status INTEGER,last_modify INTEGER,priority INTEGER,save_dir TEXT,save_name TEXT,total_length INTEGER,rangeable INTEGER default 1,received_length INTEGER,rangeable2 INTEGER default 1);";
    }
}
