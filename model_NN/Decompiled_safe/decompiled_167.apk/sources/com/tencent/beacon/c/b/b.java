package com.tencent.beacon.c.b;

import com.tencent.beacon.e.a;
import com.tencent.beacon.e.c;
import com.tencent.beacon.e.d;
import java.util.ArrayList;
import java.util.Collection;

/* compiled from: ProGuard */
public final class b extends c implements Cloneable {
    private static ArrayList<a> b;

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<a> f3412a = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.d.a(java.util.Collection, int):void
     arg types: [java.util.ArrayList<com.tencent.beacon.c.b.a>, int]
     candidates:
      com.tencent.beacon.e.d.a(byte, int):void
      com.tencent.beacon.e.d.a(int, int):void
      com.tencent.beacon.e.d.a(long, int):void
      com.tencent.beacon.e.d.a(com.tencent.beacon.e.c, int):void
      com.tencent.beacon.e.d.a(java.lang.Object, int):void
      com.tencent.beacon.e.d.a(java.lang.String, int):void
      com.tencent.beacon.e.d.a(java.util.Map, int):void
      com.tencent.beacon.e.d.a(short, int):void
      com.tencent.beacon.e.d.a(boolean, int):void
      com.tencent.beacon.e.d.a(byte[], int):void
      com.tencent.beacon.e.d.a(java.util.Collection, int):void */
    public final void a(d dVar) {
        dVar.a((Collection) this.f3412a, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<com.tencent.beacon.c.b.a>, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object */
    public final void a(a aVar) {
        if (b == null) {
            b = new ArrayList<>();
            b.add(new a());
        }
        this.f3412a = (ArrayList) aVar.a((Object) b, 0, true);
    }
}
