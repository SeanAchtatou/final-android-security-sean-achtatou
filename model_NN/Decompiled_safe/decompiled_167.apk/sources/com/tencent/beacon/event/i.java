package com.tencent.beacon.event;

import java.io.Serializable;
import java.util.Map;

/* compiled from: ProGuard */
public class i implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private long f3436a = -1;
    private String b;
    private long c;
    private String d;
    private Map<String, String> e;
    private boolean f = false;
    private long g;
    private boolean h;
    private long i;

    public final synchronized long a() {
        return this.f3436a;
    }

    public final synchronized void a(long j) {
        this.f3436a = j;
    }

    public final synchronized String b() {
        return this.b;
    }

    public final synchronized void a(String str) {
        this.b = str;
    }

    public final synchronized long c() {
        return this.c;
    }

    public final synchronized void b(long j) {
        this.c = j;
    }

    public final synchronized String d() {
        return this.d;
    }

    public final synchronized void b(String str) {
        this.d = str;
    }

    public final synchronized Map<String, String> e() {
        return this.e;
    }

    public final synchronized void a(Map<String, String> map) {
        this.e = map;
    }

    public final synchronized boolean f() {
        return this.f;
    }

    public final synchronized long g() {
        return this.g;
    }

    public final synchronized void c(long j) {
        this.g = j;
    }

    public final synchronized boolean h() {
        return this.h;
    }

    public final synchronized void a(boolean z) {
        this.h = z;
    }

    public final synchronized long i() {
        return this.i;
    }

    public final synchronized void d(long j) {
        this.i = j;
    }
}
