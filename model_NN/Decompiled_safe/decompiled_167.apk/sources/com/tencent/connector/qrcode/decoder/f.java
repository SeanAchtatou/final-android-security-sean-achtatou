package com.tencent.connector.qrcode.decoder;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ProGuard */
final class f implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private static final AtomicInteger f3571a = new AtomicInteger(1);
    private final ThreadGroup b;
    private final AtomicInteger c = new AtomicInteger(1);
    private final String d;

    f() {
        SecurityManager securityManager = System.getSecurityManager();
        this.b = securityManager != null ? securityManager.getThreadGroup() : Thread.currentThread().getThreadGroup();
        this.d = "pool-" + f3571a.getAndIncrement() + "-InactivityTimer-thread-";
    }

    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(this.b, runnable, this.d + this.c.getAndIncrement(), 0);
        thread.setDaemon(true);
        return thread;
    }
}
