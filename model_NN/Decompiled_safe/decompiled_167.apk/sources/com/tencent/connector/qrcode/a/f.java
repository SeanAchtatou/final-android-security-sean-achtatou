package com.tencent.connector.qrcode.a;

import android.graphics.Point;
import android.hardware.Camera;
import android.os.Handler;

/* compiled from: ProGuard */
public final class f implements Camera.PreviewCallback {

    /* renamed from: a  reason: collision with root package name */
    private final b f3562a;
    private final boolean b;
    private Handler c;
    private int d;

    f(b bVar, boolean z) {
        this.f3562a = bVar;
        this.b = z;
    }

    /* access modifiers changed from: package-private */
    public void a(Handler handler, int i) {
        this.c = handler;
        this.d = i;
    }

    public void onPreviewFrame(byte[] bArr, Camera camera) {
        Point a2 = this.f3562a.a();
        if (!this.b) {
            camera.setPreviewCallback(null);
        }
        if (this.c != null) {
            this.c.obtainMessage(this.d, a2.x, a2.y, bArr).sendToTarget();
            this.c = null;
        }
    }
}
