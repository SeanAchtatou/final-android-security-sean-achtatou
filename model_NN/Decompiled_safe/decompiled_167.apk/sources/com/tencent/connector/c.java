package com.tencent.connector;

import android.media.MediaPlayer;

/* compiled from: ProGuard */
class c implements MediaPlayer.OnErrorListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CaptureActivity f3518a;

    c(CaptureActivity captureActivity) {
        this.f3518a = captureActivity;
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        mediaPlayer.release();
        MediaPlayer unused = this.f3518a.i = (MediaPlayer) null;
        return true;
    }
}
