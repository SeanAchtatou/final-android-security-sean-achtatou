package com.tencent.connector.component;

import android.view.View;

/* compiled from: ProGuard */
class l implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContentQQConnectionRequest f3541a;

    l(ContentQQConnectionRequest contentQQConnectionRequest) {
        this.f3541a = contentQQConnectionRequest;
    }

    public void onClick(View view) {
        this.f3541a.denyRequest();
        if (this.f3541a.b != null) {
            this.f3541a.b.finish();
        }
    }
}
