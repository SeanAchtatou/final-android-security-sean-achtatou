package com.tencent.connector.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;

/* compiled from: ProGuard */
public class BannerQQStatus extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private RoundCornerImageView f3519a;
    private RoundCornerImageView b;
    private ImageView c;

    public BannerQQStatus(Context context) {
        super(context);
    }

    public BannerQQStatus(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public BannerQQStatus(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.c = (ImageView) findViewById(R.id.wrapper);
        this.f3519a = (RoundCornerImageView) findViewById(R.id.small_qq);
        this.b = (RoundCornerImageView) findViewById(R.id.big_qq);
    }

    public void updateWraper(int i) {
        if (this.c != null) {
            this.c.setImageResource(i);
        }
    }

    public void updateSmallQQImage(String str) {
        if (this.f3519a != null) {
            this.f3519a.updateImageView(str, R.drawable.logo_small_qq, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
    }

    public void setSmallQQImageBitmap(Bitmap bitmap) {
        if (this.f3519a != null) {
            this.f3519a.setImageBitmap(bitmap);
        }
    }

    public void setSmallQQImageResource(int i) {
        if (this.f3519a != null) {
            this.f3519a.setImageResource(i);
        }
    }

    public void updateBigQQImage(String str) {
        String str2;
        if (this.b != null) {
            StringBuilder append = new StringBuilder().append("updateBigQQImage: ");
            if (TextUtils.isEmpty(str)) {
                str2 = "null";
            } else {
                str2 = str;
            }
            Log.d("Longer", append.append(str2).toString());
            this.b.updateImageView(str, R.drawable.logo_big_qq, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
    }

    public void setBigQQImageBitmap(Bitmap bitmap) {
        if (this.b != null) {
            this.b.setImageBitmap(bitmap);
        }
    }

    public void setBigQQImageResource(int i) {
        if (this.b != null) {
            this.b.setImageResource(i);
        }
    }
}
