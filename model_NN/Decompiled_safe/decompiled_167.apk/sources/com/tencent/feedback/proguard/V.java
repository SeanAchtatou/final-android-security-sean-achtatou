package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;

public final class V extends C0008j {

    /* renamed from: a  reason: collision with root package name */
    public int f3715a = 0;
    public String b = Constants.STR_EMPTY;
    public boolean c = true;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.eup.f, java.nio.ByteBuffer):int
      com.tencent.feedback.proguard.ag.a(int, boolean):boolean */
    public final void a(ag agVar) {
        this.f3715a = agVar.a(this.f3715a, 0, true);
        this.b = agVar.b(1, true);
        boolean z = this.c;
        this.c = agVar.a(2, false);
    }

    public final void a(ah ahVar) {
        ahVar.a(this.f3715a, 0);
        ahVar.a(this.b, 1);
        ahVar.a(this.c, 2);
    }

    public final void a(StringBuilder sb, int i) {
    }
}
