package com.tencent.feedback.proguard;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

public final class aa implements ab {

    /* renamed from: a  reason: collision with root package name */
    private String f3719a = null;

    public final void a(String str) {
        if (str != null) {
            this.f3719a = str;
        }
    }

    public final byte[] a(byte[] bArr) {
        if (this.f3719a == null || bArr == null) {
            return null;
        }
        Cipher instance = Cipher.getInstance("DES/CBC/PKCS5Padding");
        instance.init(2, SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(this.f3719a.getBytes("UTF-8"))), new IvParameterSpec(this.f3719a.getBytes("UTF-8")));
        return instance.doFinal(bArr);
    }

    public final byte[] b(byte[] bArr) {
        if (this.f3719a == null || bArr == null) {
            return null;
        }
        Cipher instance = Cipher.getInstance("DES/CBC/PKCS5Padding");
        instance.init(1, SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(this.f3719a.getBytes("UTF-8"))), new IvParameterSpec(this.f3719a.getBytes("UTF-8")));
        return instance.doFinal(bArr);
    }
}
