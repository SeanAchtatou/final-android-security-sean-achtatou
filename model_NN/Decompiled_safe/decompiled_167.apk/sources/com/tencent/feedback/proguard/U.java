package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;
import java.util.HashMap;
import java.util.Map;

public final class U extends C0008j {
    private static Map<String, byte[]> f;

    /* renamed from: a  reason: collision with root package name */
    public boolean f3714a = true;
    public boolean b = true;
    public int c = 0;
    public int d = 0;
    private Map<String, byte[]> e = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.eup.f, java.nio.ByteBuffer):int
      com.tencent.feedback.proguard.ag.a(int, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, byte[]>, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    public final void a(ag agVar) {
        boolean z = this.f3714a;
        this.f3714a = agVar.a(0, true);
        if (f == null) {
            f = new HashMap();
            byte[] bArr = new byte[1];
            bArr[0] = 0;
            f.put(Constants.STR_EMPTY, bArr);
        }
        this.e = (Map) agVar.a((Object) f, 1, false);
        boolean z2 = this.b;
        this.b = agVar.a(2, false);
        this.c = agVar.a(this.c, 3, false);
        this.d = agVar.a(this.d, 4, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, byte[]>, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void */
    public final void a(ah ahVar) {
        ahVar.a(this.f3714a, 0);
        if (this.e != null) {
            ahVar.a((Map) this.e, 1);
        }
        ahVar.a(this.b, 2);
        ahVar.a(this.c, 3);
        ahVar.a(this.d, 4);
    }

    public final void a(StringBuilder sb, int i) {
    }
}
