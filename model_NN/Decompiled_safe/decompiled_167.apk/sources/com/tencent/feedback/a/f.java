package com.tencent.feedback.a;

import android.content.Context;
import android.net.Proxy;
import com.tencent.feedback.common.g;
import com.tencent.feedback.common.i;
import java.util.Locale;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

/* compiled from: ProGuard */
public final class f extends e {

    /* renamed from: a  reason: collision with root package name */
    private Context f3670a;

    public f(Context context) {
        this.f3670a = context.getApplicationContext();
    }

    public final byte[] a(String str, byte[] bArr, d dVar) {
        long length;
        boolean z;
        int i;
        int i2;
        if (str == null) {
            g.d("rqdp{  no destUrl!}", new Object[0]);
            return null;
        }
        int i3 = 0;
        int i4 = 0;
        if (bArr == null) {
            length = 0;
        } else {
            length = (long) bArr.length;
        }
        g.h("rqdp{  start req} %s rqdp{  sz:}%d", str, Long.valueOf(length));
        boolean z2 = false;
        while (true) {
            int i5 = i3 + 1;
            if (i3 >= 3 || i4 >= 2) {
                return null;
            }
            if (z2) {
                z2 = false;
            } else if (i5 > 1) {
                g.h("rqdp{  try time} " + i5, new Object[0]);
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            String c = i.c(this.f3670a);
            if (dVar != null) {
                dVar.a(str, length, c);
            }
            HttpResponse a2 = a(str, bArr, c);
            if (a2 != null) {
                HttpEntity entity = a2.getEntity();
                int statusCode = a2.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    byte[] a3 = a(a2);
                    if (dVar != null) {
                        dVar.a(a3 == null ? 0 : (long) a3.length);
                    }
                    return a3;
                }
                if (statusCode == 301 || statusCode == 302 || statusCode == 303 || statusCode == 307) {
                    z = true;
                    Header firstHeader = a2.getFirstHeader("Location");
                    if (firstHeader == null) {
                        g.d("rqdp{  redirect code:}" + statusCode + "rqdp{   Location is null! return}", new Object[0]);
                        return null;
                    }
                    i2 = i4 + 1;
                    i = 0;
                    str = firstHeader.getValue();
                    g.h("rqdp{  redirect code:}%d rqdp{  , to:}%s", Integer.valueOf(statusCode), str);
                } else {
                    z = z2;
                    i = i5;
                    i2 = i4;
                }
                long j = 0;
                if (entity != null) {
                    j = entity.getContentLength();
                    if (j < 0) {
                        j = 0;
                    }
                }
                if (dVar != null) {
                    dVar.a(j);
                }
                z2 = z;
                i4 = i2;
                i3 = i;
            } else {
                if (dVar != null) {
                    dVar.a(0);
                }
                i3 = i5;
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.apache.http.HttpResponse a(java.lang.String r8, byte[] r9, java.lang.String r10) {
        /*
            r7 = this;
            r0 = 0
            r2 = 0
            if (r8 != 0) goto L_0x000c
            java.lang.String r1 = "rqdp{  no destUrl!}"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.d(r1, r2)
        L_0x000b:
            return r0
        L_0x000c:
            if (r9 == 0) goto L_0x003c
            org.apache.http.entity.ByteArrayEntity r1 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ Throwable -> 0x0022, all -> 0x0048 }
            r1.<init>(r9)     // Catch:{ Throwable -> 0x0022, all -> 0x0048 }
        L_0x0013:
            org.apache.http.client.HttpClient r3 = r7.a(r10)     // Catch:{ Throwable -> 0x0022, all -> 0x0048 }
            if (r3 != 0) goto L_0x0051
            java.lang.String r1 = "rqdp{  no httpClient!}"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0022, all -> 0x0048 }
            com.tencent.feedback.common.g.d(r1, r2)     // Catch:{ Throwable -> 0x0022, all -> 0x0048 }
            goto L_0x000b
        L_0x0022:
            r1 = move-exception
            r2 = r0
        L_0x0024:
            r1.printStackTrace()     // Catch:{ all -> 0x0089 }
            java.lang.String r3 = "rqdp{  execute error }%s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0089 }
            r5 = 0
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0089 }
            r4[r5] = r1     // Catch:{ all -> 0x0089 }
            com.tencent.feedback.common.g.d(r3, r4)     // Catch:{ all -> 0x0089 }
            if (r2 == 0) goto L_0x000b
            r2.abort()
            goto L_0x000b
        L_0x003c:
            org.apache.http.entity.ByteArrayEntity r1 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ Throwable -> 0x0022, all -> 0x0048 }
            java.lang.String r2 = ""
            byte[] r2 = r2.getBytes()     // Catch:{ Throwable -> 0x0022, all -> 0x0048 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0022, all -> 0x0048 }
            goto L_0x0013
        L_0x0048:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x004b:
            if (r2 == 0) goto L_0x0050
            r2.abort()
        L_0x0050:
            throw r0
        L_0x0051:
            org.apache.http.client.methods.HttpPost r2 = new org.apache.http.client.methods.HttpPost     // Catch:{ Throwable -> 0x0022, all -> 0x0048 }
            r2.<init>(r8)     // Catch:{ Throwable -> 0x0022, all -> 0x0048 }
            java.lang.String r4 = "wup_version"
            java.lang.String r5 = "3.0"
            r2.setHeader(r4, r5)     // Catch:{ Throwable -> 0x008b }
            r2.setEntity(r1)     // Catch:{ Throwable -> 0x008b }
            org.apache.http.protocol.BasicHttpContext r4 = new org.apache.http.protocol.BasicHttpContext     // Catch:{ Throwable -> 0x008b }
            r4.<init>()     // Catch:{ Throwable -> 0x008b }
            org.apache.http.HttpResponse r1 = r3.execute(r2, r4)     // Catch:{ Throwable -> 0x008b }
            java.lang.String r0 = "http.request"
            java.lang.Object r0 = r4.getAttribute(r0)     // Catch:{ Throwable -> 0x008d }
            org.apache.http.HttpRequest r0 = (org.apache.http.HttpRequest) r0     // Catch:{ Throwable -> 0x008d }
            java.lang.String r3 = "rqdp{  execute request:\n} %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x008d }
            r5 = 0
            org.apache.http.RequestLine r0 = r0.getRequestLine()     // Catch:{ Throwable -> 0x008d }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x008d }
            r4[r5] = r0     // Catch:{ Throwable -> 0x008d }
            com.tencent.feedback.common.g.h(r3, r4)     // Catch:{ Throwable -> 0x008d }
            r2.abort()
            r0 = r1
            goto L_0x000b
        L_0x0089:
            r0 = move-exception
            goto L_0x004b
        L_0x008b:
            r1 = move-exception
            goto L_0x0024
        L_0x008d:
            r0 = move-exception
            r6 = r0
            r0 = r1
            r1 = r6
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.a.f.a(java.lang.String, byte[], java.lang.String):org.apache.http.HttpResponse");
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0086 A[SYNTHETIC, Splitter:B:33:0x0086] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] a(org.apache.http.HttpResponse r7) {
        /*
            r6 = 1
            r5 = 0
            r0 = 0
            if (r7 != 0) goto L_0x0006
        L_0x0005:
            return r0
        L_0x0006:
            org.apache.http.StatusLine r1 = r7.getStatusLine()
            int r1 = r1.getStatusCode()
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 == r2) goto L_0x0027
            org.apache.http.StatusLine r2 = r7.getStatusLine()
            java.lang.String r3 = "rqdp{  request failure code:}%d rqdp{  , line:}%s "
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r4[r5] = r1
            r4[r6] = r2
            com.tencent.feedback.common.g.c(r3, r4)
            goto L_0x0005
        L_0x0027:
            org.apache.http.HttpEntity r1 = r7.getEntity()
            if (r1 != 0) goto L_0x0035
            java.lang.String r1 = "rqdp{  no response datas!}"
            java.lang.Object[] r2 = new java.lang.Object[r5]
            com.tencent.feedback.common.g.d(r1, r2)
            goto L_0x0005
        L_0x0035:
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ Throwable -> 0x0091, all -> 0x0081 }
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0091, all -> 0x0081 }
            java.io.InputStream r1 = r1.getContent()     // Catch:{ Throwable -> 0x0091, all -> 0x0081 }
            r3.<init>(r1)     // Catch:{ Throwable -> 0x0091, all -> 0x0081 }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x0091, all -> 0x0081 }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x0053 }
            r1.<init>()     // Catch:{ Throwable -> 0x0053 }
        L_0x0048:
            int r3 = r2.read()     // Catch:{ Throwable -> 0x0053 }
            r4 = -1
            if (r3 == r4) goto L_0x0071
            r1.write(r3)     // Catch:{ Throwable -> 0x0053 }
            goto L_0x0048
        L_0x0053:
            r1 = move-exception
        L_0x0054:
            r1.printStackTrace()     // Catch:{ all -> 0x008f }
            java.lang.String r3 = "rqdp{  read error} %s"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x008f }
            r5 = 0
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x008f }
            r4[r5] = r1     // Catch:{ all -> 0x008f }
            com.tencent.feedback.common.g.d(r3, r4)     // Catch:{ all -> 0x008f }
            if (r2 == 0) goto L_0x0005
            r2.close()     // Catch:{ Throwable -> 0x006c }
            goto L_0x0005
        L_0x006c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0005
        L_0x0071:
            r1.flush()     // Catch:{ Throwable -> 0x0053 }
            byte[] r0 = r1.toByteArray()     // Catch:{ Throwable -> 0x0053 }
            r2.close()     // Catch:{ Throwable -> 0x007c }
            goto L_0x0005
        L_0x007c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0005
        L_0x0081:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0084:
            if (r2 == 0) goto L_0x0089
            r2.close()     // Catch:{ Throwable -> 0x008a }
        L_0x0089:
            throw r0
        L_0x008a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0089
        L_0x008f:
            r0 = move-exception
            goto L_0x0084
        L_0x0091:
            r1 = move-exception
            r2 = r0
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.a.f.a(org.apache.http.HttpResponse):byte[]");
    }

    private HttpClient a(String str) {
        try {
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 30000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 10000);
            HttpConnectionParams.setSocketBufferSize(basicHttpParams, 2000);
            basicHttpParams.setBooleanParameter("http.protocol.handle-redirects", false);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            defaultHttpClient.setHttpRequestRetryHandler(new g(this));
            if (str == null || !str.toLowerCase(Locale.US).contains("wap")) {
                defaultHttpClient.getParams().removeParameter("http.route.default-proxy");
                return defaultHttpClient;
            }
            g.a("rqdp{  use proxy} %s", str);
            defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(Proxy.getDefaultHost(), Proxy.getDefaultPort()));
            return defaultHttpClient;
        } catch (Throwable th) {
            th.printStackTrace();
            g.d("rqdp{  httpclient error!}", new Object[0]);
            return null;
        }
    }
}
