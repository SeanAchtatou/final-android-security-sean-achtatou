package com.tencent.feedback.a;

import android.content.Context;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.g;
import com.tencent.feedback.proguard.C;
import com.tencent.feedback.proguard.ac;
import com.tencent.feedback.proguard.ao;
import com.tencent.feedback.proguard.at;

/* compiled from: ProGuard */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected final int f3667a;
    protected final int b;
    protected Context c;

    public abstract C a();

    public abstract void a(boolean z);

    public a(Context context, int i, int i2) {
        this.c = context;
        this.f3667a = i2;
        this.b = i;
    }

    public final int b() {
        return this.f3667a;
    }

    public final String c() {
        try {
            if (this.b == 1111) {
                return ao.a(this.c).b().a();
            }
            return ao.a(this.c).b().e(this.b).a();
        } catch (Throwable th) {
            g.c("rqdp{  imposiable comStrategy error }%s", th.toString());
            th.printStackTrace();
            return null;
        }
    }

    public static C a(Context context, int i, byte[] bArr) {
        int i2;
        int i3 = -1;
        if (bArr != null) {
            try {
                at b2 = ao.a(context).b();
                i2 = b2.f();
                i3 = b2.g();
                bArr = ac.a(bArr, i3, i2, b2.d());
                if (bArr == null) {
                    g.c("rqdp{  enzp error! }%d %d ", Integer.valueOf(i3), Integer.valueOf(i2));
                    return null;
                }
            } catch (Throwable th) {
                g.c("rqdp{  imposiable comStrategy error} %s", th.toString());
                th.printStackTrace();
                return null;
            }
        } else {
            i2 = -1;
        }
        return ac.a(i, e.a(context), bArr, i3, i2);
    }

    public static void d() {
        g.c("rqdp{  encode failed, clear db data}", new Object[0]);
    }
}
