package com.tencent.feedback.eup.jni;

import com.tencent.feedback.common.g;
import java.io.File;
import java.io.FilenameFilter;
import java.util.List;

/* compiled from: ProGuard */
final class f implements FilenameFilter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ int f3701a;
    private /* synthetic */ int b;
    private /* synthetic */ List c;
    private /* synthetic */ d d;

    f(d dVar, int i, int i2, List list) {
        this.d = dVar;
        this.f3701a = i;
        this.b = i2;
        this.c = list;
    }

    public final boolean accept(File file, String str) {
        g.b("rqdp{  check dir} %s rqdp{  , filename} %s", file, str);
        if (str.startsWith("tomb_")) {
            d.a(this.d);
            g.b("rqdp{  accept }%s", str);
            try {
                long parseLong = Long.parseLong(str.substring(this.f3701a, str.length() - this.b));
                g.b("rqdp{  mRemoveBeforeDate }%d", Long.valueOf(this.d.b));
                if (parseLong <= this.d.b) {
                    g.b("rqdp{  recordTime} %d rqdp{  is old}", Long.valueOf(parseLong));
                    return true;
                }
                g.b("rqdp{  newFileTimeList add} %d", Long.valueOf(parseLong));
                this.c.add(Long.valueOf(parseLong));
            } catch (Throwable th) {
                g.c("rqdp{  filename is not formatted ,shoud do delete! \n path:}%s", str);
                th.printStackTrace();
                return true;
            }
        }
        return false;
    }
}
