package com.tencent.feedback.eup;

import java.util.Comparator;

/* compiled from: ProGuard */
final class h implements Comparator<e> {
    h() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        int i = 1;
        e eVar = (e) obj;
        e eVar2 = (e) obj2;
        int i2 = eVar.b() ? 1 : 0;
        if (!eVar2.b()) {
            i = 0;
        }
        return i2 == i ? (int) (eVar.i() - eVar2.i()) : i2 - i;
    }
}
