package com.tencent.feedback.eup;

import android.content.Context;
import com.tencent.feedback.a.b;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.f;
import com.tencent.feedback.common.g;
import com.tencent.feedback.eup.jni.NativeExceptionUpload;
import com.tencent.feedback.eup.jni.a;
import com.tencent.feedback.eup.jni.d;
import com.tencent.feedback.proguard.ac;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class c {
    public static void a(Context context, boolean z) {
        k.a(context, "10000", false, k.a(context, z), null, null, null);
    }

    public static void a(Context context, b bVar, b bVar2, boolean z, d dVar) {
        k.a(context, "10000", false, k.a(context, z), bVar2, bVar, dVar);
    }

    public static void a(Context context, String str, boolean z) {
        a(context, str, z, (List<File>) null, (File) null);
    }

    public static void a(Context context, String str, boolean z, List<File> list, File file) {
        boolean z2;
        if (file == null) {
            if (list != null) {
                z2 = NativeExceptionUpload.loadRQDNativeLib(list);
                if (z2) {
                    g.a("load lib sucess from list!", new Object[0]);
                }
            } else {
                z2 = false;
            }
            if (!z2) {
                if (!NativeExceptionUpload.loadRQDNativeLib()) {
                    g.d("load lib fail default close native return!", new Object[0]);
                    return;
                }
                g.a("load lib sucess default!", new Object[0]);
            }
        } else if (!NativeExceptionUpload.loadRQDNativeLib(file)) {
            g.d("load lib fail %s close native return!", file.getAbsoluteFile());
            return;
        } else {
            g.a("load lib sucess from specify!", new Object[0]);
        }
        NativeExceptionUpload.setmHandler(com.tencent.feedback.eup.jni.b.a(context));
        if (file != null) {
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(file);
        }
        if (context == null || str == null) {
            g.c("rqdp{  nreg param!}", new Object[0]);
        } else {
            d q = k.k().q();
            com.tencent.feedback.common.c.a().a(new d(context, str, ac.b() - ((long) (((q.d() * 24) * 3600) * 1000)), q.a()));
            com.tencent.feedback.common.c.a().a(new a(context, "/data/data/" + context.getPackageName() + "/lib/", list));
        }
        f.a(context);
        String d = f.d();
        f.a(context);
        NativeExceptionUpload.registEUP(str, d, Integer.parseInt(f.c()));
        NativeExceptionUpload.enableNativeEUP(true);
        if (z) {
            NativeExceptionUpload.setNativeLogMode(3);
        } else {
            NativeExceptionUpload.setNativeLogMode(5);
        }
    }

    public static d a() {
        try {
            return k.k().q();
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static boolean a(Thread thread, Throwable th, String str, byte[] bArr) {
        return k.a(thread, th, str, bArr);
    }

    public static void a(boolean z) {
        k k = k.k();
        if (k != null) {
            k.a(z);
        }
    }

    public static void b(boolean z) {
        NativeExceptionUpload.enableNativeEUP(z);
    }

    public static void a(boolean z, boolean z2) {
        g.f3681a = z;
        g.b = z2;
    }

    public static void a(Context context, String str) {
        e.a(context).b(str);
    }
}
