package com.tencent.feedback.anr;

import com.tencent.feedback.common.g;
import com.tencent.feedback.eup.jni.c;
import java.util.HashMap;

/* compiled from: ProGuard */
final class d implements e {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ c f3675a;
    private /* synthetic */ boolean b;

    d(c cVar, boolean z) {
        this.f3675a = cVar;
        this.b = z;
    }

    public final boolean a(String str, int i, String str2, String str3) {
        g.b("new thread %s", str);
        if (this.f3675a.d == null) {
            this.f3675a.d = new HashMap();
        }
        this.f3675a.d.put(str, new String[]{str2, str3, new StringBuilder().append(i).toString()});
        return true;
    }

    public final boolean a(long j, long j2, String str) {
        g.b("new process %s", str);
        this.f3675a.f3699a = j;
        this.f3675a.b = str;
        this.f3675a.c = j2;
        if (!this.b) {
            return false;
        }
        return true;
    }

    public final boolean a(long j) {
        g.b("process end %d", Long.valueOf(j));
        return false;
    }
}
