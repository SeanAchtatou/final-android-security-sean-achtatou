package com.tencent.cloud.b;

import android.content.Context;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.manager.u;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.XLog;
import com.tencent.cloud.adapter.UpdateRecOneMoreAdapter;
import com.tencent.cloud.updaterec.g;
import com.tencent.cloud.updaterec.h;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class a {
    public static void a(Context context, ListView listView, h hVar, Object obj, SimpleAppModel simpleAppModel, int i) {
        g b;
        if (listView != null && hVar != null && obj != null && (b = hVar.b(obj)) != null) {
            int firstVisiblePosition = listView.getFirstVisiblePosition();
            int lastVisiblePosition = listView.getLastVisiblePosition() + 1;
            String str = (String) b.d.getTag();
            if (firstVisiblePosition != lastVisiblePosition && ((i != 0 && i < firstVisiblePosition) || i > lastVisiblePosition || (str != null && (simpleAppModel == null || !str.equals(simpleAppModel.q()))))) {
                b.c = null;
                b.l = 0;
                b.d.setTag(null);
            }
            if (b.l == 2) {
                b.h.setVisibility(8);
                b.j.setVisibility(8);
                b.d.setVisibility(0);
                b.e.setVisibility(0);
                b.b.setVisibility(0);
                b.f3474a.setVisibility(0);
                BaseAdapter baseAdapter = (BaseAdapter) b.f3474a.getAdapter();
                if (baseAdapter != null) {
                    baseAdapter.notifyDataSetChanged();
                }
            } else if (b.l == 3) {
                a(context, b);
            } else if (b.l == 1) {
                b(context, b);
            } else if (b.l == 0) {
                a(b);
            }
        }
    }

    public static boolean a(Context context, SimpleAppModel simpleAppModel, ListView listView, h hVar, View view, boolean z) {
        boolean z2;
        g a2;
        if (listView == null) {
            return false;
        }
        int a3 = u.a().a("oneMoreApp", 0);
        if (!z || a3 != 1) {
            z2 = false;
        } else {
            z2 = true;
        }
        int childCount = listView.getChildCount();
        boolean z3 = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = listView.getChildAt(i);
            g a4 = hVar.a(childAt.getTag());
            if (a4 != null) {
                if (childAt == view && a4.l == 2 && z2) {
                    z3 = true;
                } else if (a4.l != 0) {
                    a(a4);
                }
            }
        }
        if (!z2 || z3 || view == null || (a2 = hVar.a(view.getTag())) == null || simpleAppModel == null) {
            return false;
        }
        a2.f3474a.setTag(Integer.valueOf(com.tencent.cloud.a.a.a().a(2, simpleAppModel.f1634a)));
        a2.d.setTag(simpleAppModel.q());
        b(context, a2);
        return true;
    }

    public static void a(Context context, g gVar, ArrayList<SimpleAppModel> arrayList, String str) {
        gVar.d.setVisibility(0);
        UpdateRecOneMoreAdapter updateRecOneMoreAdapter = new UpdateRecOneMoreAdapter(context, gVar.f3474a, arrayList);
        updateRecOneMoreAdapter.a(str);
        gVar.f3474a.setAdapter((ListAdapter) updateRecOneMoreAdapter);
        gVar.f3474a.setVisibility(0);
        gVar.b.setVisibility(0);
        gVar.j.setVisibility(8);
        gVar.e.setVisibility(0);
        gVar.h.setVisibility(0);
        gVar.l = 2;
        XLog.d("zhangyuanchao", "-------updateOneMoreAppHolderToShowAppList--------");
    }

    public static void a(g gVar) {
        gVar.f3474a.setAdapter((ListAdapter) null);
        gVar.f3474a.setTag(null);
        gVar.b.setVisibility(8);
        gVar.j.setVisibility(8);
        gVar.h.setVisibility(8);
        gVar.d.setVisibility(8);
        gVar.e.setVisibility(8);
        gVar.l = 0;
        gVar.c = null;
        XLog.d("zhangyuanchao", "-------updateOneMoreAppHolderToHide--------");
    }

    public static void a(Context context, g gVar) {
        gVar.l = 3;
        XLog.d("zhangyuanchao", "-------updateOneMoreAppHolderToFail--------");
    }

    public static void b(Context context, g gVar) {
        gVar.l = 1;
        XLog.d("zhangyuanchao", "-------updateOneMoreAppHolderToLoading--------");
    }
}
