package com.tencent.b.b;

import android.util.Log;
import com.tencent.b.d.k;
import com.tencent.connect.common.Constants;
import org.json.JSONException;
import org.json.JSONObject;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private String f3369a = null;
    private String b = null;
    private String c = "0";
    private long d = 0;

    public static b a(String str) {
        b bVar = new b();
        if (k.b(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.isNull("ui")) {
                    bVar.c(jSONObject.getString("ui"));
                }
                if (!jSONObject.isNull("mc")) {
                    bVar.d(jSONObject.getString("mc"));
                }
                if (!jSONObject.isNull("mid")) {
                    bVar.b(jSONObject.getString("mid"));
                }
                if (!jSONObject.isNull("ts")) {
                    bVar.a(jSONObject.getLong("ts"));
                }
            } catch (JSONException e) {
                Log.w("MID", Constants.STR_EMPTY, e);
            }
        }
        return bVar;
    }

    public int a(b bVar) {
        if (bVar == null) {
            return 1;
        }
        if (!a() || !bVar.a()) {
            return !a() ? -1 : 1;
        }
        if (this.c.equals(bVar.c)) {
            return 0;
        }
        return this.d < bVar.d ? -1 : 1;
    }

    public void a(long j) {
        this.d = j;
    }

    public boolean a() {
        return k.c(this.c);
    }

    /* access modifiers changed from: package-private */
    public JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            k.a(jSONObject, "ui", this.f3369a);
            k.a(jSONObject, "mc", this.b);
            k.a(jSONObject, "mid", this.c);
            jSONObject.put("ts", this.d);
        } catch (JSONException e) {
            k.a(e);
        }
        return jSONObject;
    }

    public void b(String str) {
        this.c = str;
    }

    public String c() {
        return this.c;
    }

    public void c(String str) {
        this.f3369a = str;
    }

    public void d(String str) {
        this.b = str;
    }

    public String toString() {
        return b().toString();
    }
}
