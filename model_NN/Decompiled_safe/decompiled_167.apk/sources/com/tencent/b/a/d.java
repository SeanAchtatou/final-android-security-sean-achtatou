package com.tencent.b.a;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.tencent.b.b.a;
import com.tencent.b.b.b;
import com.tencent.b.c.g;
import com.tencent.b.d.j;
import com.tencent.b.d.k;
import java.util.HashMap;
import org.json.JSONObject;

public class d {
    private static String b = "iikVs3FGzEQ23RaD1JlHsSWSI5Z26m2hX3gO51mH3ag=";
    private static d c = null;
    private static Context d = null;

    /* renamed from: a  reason: collision with root package name */
    Handler f3363a = null;

    private d(Context context) {
        try {
            HandlerThread handlerThread = new HandlerThread("HttpManager");
            handlerThread.start();
            this.f3363a = new Handler(handlerThread.getLooper());
            d = context.getApplicationContext();
        } catch (Throwable th) {
            k.a(th);
        }
    }

    static Context a() {
        return d;
    }

    public static synchronized d a(Context context) {
        d dVar;
        synchronized (d.class) {
            if (c == null) {
                c = new d(context);
            }
            dVar = c;
        }
        return dVar;
    }

    private String a(f fVar, a aVar) {
        int i;
        int i2 = -1;
        int a2 = fVar.a();
        String b2 = fVar.b();
        String str = "0";
        if (a2 == 200) {
            boolean z = false;
            if (k.b(b2)) {
                JSONObject jSONObject = new JSONObject(b2);
                if (!jSONObject.isNull("mid")) {
                    str = jSONObject.optString("mid");
                    if (k.c(str)) {
                        b bVar = new b();
                        bVar.b(str);
                        bVar.d(k.d(d));
                        bVar.c(k.c(d));
                        if (!jSONObject.isNull("ts")) {
                            long optLong = jSONObject.optLong("ts");
                            if (optLong > 0) {
                                bVar.a(optLong);
                            }
                        } else {
                            bVar.a(System.currentTimeMillis());
                        }
                        k.a("new mid midEntity:" + bVar.toString());
                        aVar.a(bVar.toString());
                        g.a(d).a(bVar);
                        z = true;
                    }
                }
                i = !jSONObject.isNull(com.tencent.b.c.a.c) ? jSONObject.getInt(com.tencent.b.c.a.c) : -1;
                if (!jSONObject.isNull(com.tencent.b.c.a.d)) {
                    i2 = jSONObject.getInt(com.tencent.b.c.a.d);
                }
            } else {
                i = -1;
            }
            g.a(d).a(i, i2);
            if (!z) {
                aVar.a(g.a(d).a());
            }
        } else {
            String str2 = "Server response error code:" + a2 + ", error:" + b2;
            k.a(str2);
            aVar.a(a2, str2);
        }
        return str;
    }

    private String b() {
        return k.d(b);
    }

    /* access modifiers changed from: private */
    public void b(g gVar, a aVar) {
        try {
            String a2 = k.a();
            f a3 = b.a(a2);
            if (a3.a() != 200) {
                String str = "response code invalid:" + a3.a();
                k.a(str);
                aVar.a(a3.a(), str);
                b.b();
                return;
            }
            int i = 0;
            String str2 = null;
            JSONObject jSONObject = new JSONObject(a3.b());
            if (!jSONObject.isNull("rand")) {
                i = jSONObject.getInt("rand");
                str2 = j.a(k.a(b(), String.valueOf(i)));
            }
            if (str2 == null || i == 0) {
                k.a("hmac == null");
                b.b();
                return;
            }
            HashMap hashMap = new HashMap();
            hashMap.put("k", str2);
            hashMap.put("s", String.valueOf(i));
            String str3 = a2 + b.a(hashMap);
            f a4 = b.a(str3);
            if (a4.a() != 200) {
                k.a("hmac invalid.");
                aVar.a(a4.a(), "hmac invalid.");
                b.b();
                return;
            }
            JSONObject jSONObject2 = new JSONObject();
            gVar.a(jSONObject2);
            k.a(jSONObject2, "rip", k.f(k.a()));
            String a5 = a(b.a(str3, "[" + jSONObject2.toString() + "]"), aVar);
            if (k.c(jSONObject2.optString("mid")) || k.c(a5)) {
                b.b();
                return;
            }
            throw new Exception("get Mid failed, something wrong");
        } catch (Throwable th) {
            aVar.a(-10030, th.toString());
            Log.e("MID", "request MID  failed", th);
        }
        b.b();
    }

    /* access modifiers changed from: package-private */
    public void a(g gVar, a aVar) {
        if (gVar == null || this.f3363a == null || aVar == null) {
            if (aVar != null) {
                aVar.a(-10000, "packet == null || handler == null");
            }
        } else if (Thread.currentThread().getId() == this.f3363a.getLooper().getThread().getId()) {
            b(gVar, aVar);
        } else {
            this.f3363a.post(new e(this, gVar, aVar));
        }
    }
}
