package com.tencent.open.utils;

import android.support.v4.view.MotionEventCompat;

/* compiled from: ProGuard */
public final class ZipLong implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private long f3797a;

    public ZipLong(byte[] bArr) {
        this(bArr, 0);
    }

    public ZipLong(byte[] bArr, int i) {
        this.f3797a = ((long) (bArr[i + 3] << 24)) & 4278190080L;
        this.f3797a += (long) ((bArr[i + 2] << 16) & 16711680);
        this.f3797a += (long) ((bArr[i + 1] << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK);
        this.f3797a += (long) (bArr[i] & 255);
    }

    public ZipLong(long j) {
        this.f3797a = j;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ZipLong) || this.f3797a != ((ZipLong) obj).getValue()) {
            return false;
        }
        return true;
    }

    public byte[] getBytes() {
        return new byte[]{(byte) ((int) (this.f3797a & 255)), (byte) ((int) ((this.f3797a & 65280) >> 8)), (byte) ((int) ((this.f3797a & 16711680) >> 16)), (byte) ((int) ((this.f3797a & 4278190080L) >> 24))};
    }

    public long getValue() {
        return this.f3797a;
    }

    public int hashCode() {
        return (int) this.f3797a;
    }
}
