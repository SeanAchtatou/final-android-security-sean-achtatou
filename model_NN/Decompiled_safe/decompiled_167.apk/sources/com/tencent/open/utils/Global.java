package com.tencent.open.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.tencent.connect.common.Constants;
import java.io.File;

/* compiled from: ProGuard */
public final class Global {

    /* renamed from: a  reason: collision with root package name */
    private static Context f3783a;

    public static final Context getContext() {
        if (f3783a == null) {
            return null;
        }
        return f3783a;
    }

    public static final void setContext(Context context) {
        f3783a = context;
    }

    public static final String getPackageName() {
        if (getContext() == null) {
            return Constants.STR_EMPTY;
        }
        return getContext().getPackageName();
    }

    public static final SharedPreferences getSharedPreferences(String str, int i) {
        if (getContext() == null) {
            return null;
        }
        return getContext().getSharedPreferences(str, i);
    }

    public static final File getFilesDir() {
        if (getContext() == null) {
            return null;
        }
        return getContext().getFilesDir();
    }

    public static void saveVersionCode() {
        Context context = getContext();
        if (context != null) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                if (packageInfo != null) {
                    SharedPreferences.Editor edit = context.getSharedPreferences("openSdk.pref", 0).edit();
                    edit.putInt("app.vercode", packageInfo.versionCode);
                    edit.commit();
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static int getVersionCode() {
        return f3783a.getSharedPreferences("openSdk.pref", 0).getInt("app.vercode", 0);
    }
}
