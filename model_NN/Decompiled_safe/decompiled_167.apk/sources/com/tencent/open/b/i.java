package com.tencent.open.b;

import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import com.tencent.connect.common.Constants;
import com.tencent.open.a.n;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.OpenConfig;
import com.tencent.open.utils.Util;
import java.util.TimeZone;

/* compiled from: ProGuard */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Bundle f3768a;
    final /* synthetic */ boolean b;
    final /* synthetic */ g c;

    i(g gVar, Bundle bundle, boolean z) {
        this.c = gVar;
        this.f3768a = bundle;
        this.b = z;
    }

    public void run() {
        try {
            Bundle bundle = new Bundle();
            bundle.putString("uin", Constants.DEFAULT_UIN);
            bundle.putString("imei", c.b(Global.getContext()));
            bundle.putString("imsi", c.c(Global.getContext()));
            bundle.putString("android_id", c.d(Global.getContext()));
            bundle.putString("mac", c.a());
            bundle.putString(Constants.PARAM_PLATFORM, "1");
            bundle.putString("os_ver", Build.VERSION.RELEASE);
            bundle.putString("position", Util.getLocation(Global.getContext()));
            bundle.putString("network", a.a(Global.getContext()));
            bundle.putString("language", c.b());
            bundle.putString("resolution", c.a(Global.getContext()));
            bundle.putString("apn", a.b(Global.getContext()));
            bundle.putString("model_name", Build.MODEL);
            bundle.putString("timezone", TimeZone.getDefault().getID());
            bundle.putString("sdk_ver", Constants.SDK_VERSION);
            bundle.putString("qz_ver", Util.getAppVersionName(Global.getContext(), Constants.PACKAGE_QZONE));
            bundle.putString("qq_ver", Util.getVersionName(Global.getContext(), "com.tencent.mobileqq"));
            bundle.putString("qua", Util.getQUA3(Global.getContext(), Global.getPackageName()));
            bundle.putString("packagename", Global.getPackageName());
            bundle.putString("app_ver", Util.getAppVersionName(Global.getContext(), Global.getPackageName()));
            if (this.f3768a != null) {
                bundle.putAll(this.f3768a);
            }
            this.c.e.add(new b(bundle));
            int size = this.c.e.size();
            int i = OpenConfig.getInstance(Global.getContext(), null).getInt("Agent_ReportTimeInterval");
            if (i == 0) {
                i = 10000;
            }
            if (this.c.a("report_via", size) || this.b) {
                this.c.e();
                this.c.g.removeMessages(1001);
            } else if (!this.c.g.hasMessages(1001)) {
                Message obtain = Message.obtain();
                obtain.what = 1001;
                this.c.g.sendMessageDelayed(obtain, (long) i);
            }
        } catch (Exception e) {
            n.b(g.f3766a, "--> reporVia, exception in sub thread.", e);
        }
    }
}
