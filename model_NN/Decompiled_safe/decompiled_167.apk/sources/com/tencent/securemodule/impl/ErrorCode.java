package com.tencent.securemodule.impl;

public class ErrorCode {
    public static final int ERR_ARGUMENT = -6;
    public static final int ERR_CANCEL = -3;
    public static final int ERR_FILE_OP = -7000;
    public static final int ERR_GENERAL = -2;
    public static final int ERR_GET = -3000;
    public static final int ERR_ILLEGAL_ACCESS = -60;
    public static final int ERR_ILLEGAL_ARG = -57;
    public static final int ERR_ILLEGAL_STATE = -61;
    public static final int ERR_IO_EXCEPTION = -56;
    public static final int ERR_NONE = 0;
    public static final int ERR_NOT_COMPLETED = -7;
    public static final int ERR_NOT_FOUND = -1;
    public static final int ERR_NOT_SUPPORTED = -5;
    public static final int ERR_NO_CONNECTION = -52;
    public static final int ERR_NO_MEMORY = -4;
    public static final int ERR_OPEN_CONNECTION = -1000;
    public static final int ERR_POST = -2000;
    public static final int ERR_PROTOCOL = -51;
    public static final int ERR_RECEIVE = -5000;
    public static final int ERR_RESPONSE = -4000;
    public static final int ERR_SECURITY = -58;
    public static final int ERR_SOCKET = -54;
    public static final int ERR_SOCKET_TIMEOUT = -55;
    public static final int ERR_UNSUPPORTED_OP = -59;
    public static final int ERR_URL_MALFORMED = -53;
    public static final int ERR_WUP = -6000;

    public enum a {
        NETWORK,
        WUP,
        CANCEL,
        OTHER
    }

    public static a judgeErrorCode(int i) {
        a aVar = a.OTHER;
        if ((-((-i) % 100)) == -3) {
            return a.CANCEL;
        }
        switch (-(((-i) / 1000) * 1000)) {
            case ERR_RECEIVE /*-5000*/:
            case ERR_RESPONSE /*-4000*/:
            case ERR_GET /*-3000*/:
            case ERR_POST /*-2000*/:
            case -1000:
                return a.NETWORK;
            default:
                return aVar;
        }
    }
}
