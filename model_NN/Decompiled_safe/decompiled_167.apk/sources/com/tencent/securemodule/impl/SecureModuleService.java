package com.tencent.securemodule.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.tencent.securemodule.service.ApkDownLoadListener;
import com.tencent.securemodule.service.CloudScanListener;
import com.tencent.securemodule.service.ISecureModuleService;
import com.tencent.securemodule.service.ProductInfo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SecureModuleService implements ISecureModuleService {
    private static SecureModuleService mInstance = null;
    private Context mContext;
    private DownLoadBroadcastReceiver mDownLoadBroadcastReceiver;
    private List<CloudScanBroadcastReceiver> mObserverList = new ArrayList();

    public class CloudScanBroadcastReceiver extends BroadcastReceiver {
        private CloudScanListener b;

        public CloudScanBroadcastReceiver(CloudScanListener cloudScanListener) {
            this.b = cloudScanListener;
        }

        public boolean a(CloudScanListener cloudScanListener) {
            return this.b != null ? this.b.equals(cloudScanListener) : cloudScanListener == null;
        }

        public void onReceive(Context context, Intent intent) {
            if ("1000030".equals(intent.getAction())) {
                this.b.onRiskFound();
                this.b.onRiskFoud((List) intent.getSerializableExtra("key_rise"));
            } else if ("1000031".equals(intent.getAction())) {
                this.b.onFinish(intent.getIntExtra("key_errcode", 0));
            }
        }
    }

    public class DownLoadBroadcastReceiver extends BroadcastReceiver {
        private ApkDownLoadListener b;

        public DownLoadBroadcastReceiver(ApkDownLoadListener apkDownLoadListener) {
            this.b = apkDownLoadListener;
        }

        public void onReceive(Context context, Intent intent) {
            Bundle bundleExtra;
            if ("1000024".equals(intent.getAction())) {
                if (this.b != null) {
                    this.b.onDownloadStart();
                }
            } else if ("1000025".equals(intent.getAction())) {
                if (this.b != null && (bundleExtra = intent.getBundleExtra("data")) != null) {
                    long j = bundleExtra.getLong("key_total");
                    this.b.onRefreshProgress(bundleExtra.getInt("key_progress"), bundleExtra.getLong("key_completed"), j);
                }
            } else if ("1000027".equals(intent.getAction())) {
                if (this.b != null) {
                    this.b.onDownloadSuccess();
                }
                SecureModuleService.this.removeDownloadReceiver();
            } else if ("1000026".equals(intent.getAction())) {
                if (this.b != null) {
                    this.b.onDownLoadError();
                }
                SecureModuleService.this.removeDownloadReceiver();
            }
        }
    }

    private SecureModuleService(Context context) {
        this.mContext = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ad.a(java.lang.String, boolean):int
     arg types: [java.lang.String, int]
     candidates:
      ac.a(int, android.os.Bundle):void
      ac.a(java.lang.String, int):void
      ad.a(java.lang.String, boolean):int */
    public static String downloadFile(Context context, String str, String str2, int i) {
        ad adVar = new ad(context);
        adVar.b(str2);
        adVar.a(i);
        do {
        } while (adVar.a(str, false) == -7);
        return adVar.b();
    }

    public static String getCachePath(Context context) {
        return context.getCacheDir().getAbsolutePath();
    }

    public static String getFilesPath(Context context) {
        return context.getFilesDir().getAbsolutePath();
    }

    public static SecureModuleService getInstance(Context context) {
        if (mInstance == null) {
            synchronized (SecureModuleService.class) {
                if (mInstance == null) {
                    mInstance = new SecureModuleService(context);
                }
            }
        }
        return mInstance;
    }

    /* access modifiers changed from: private */
    public void removeDownloadReceiver() {
        if (this.mDownLoadBroadcastReceiver != null) {
            this.mContext.unregisterReceiver(this.mDownLoadBroadcastReceiver);
            this.mDownLoadBroadcastReceiver = null;
        }
    }

    public void cloudScan() {
        SecureService.a(this.mContext, "1000010");
    }

    public void downLoadTMSeucreApk(ApkDownLoadListener apkDownLoadListener) {
        if (this.mDownLoadBroadcastReceiver == null) {
            if (apkDownLoadListener != null) {
                this.mDownLoadBroadcastReceiver = new DownLoadBroadcastReceiver(apkDownLoadListener);
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("1000024");
                intentFilter.addAction("1000025");
                intentFilter.addAction("1000027");
                intentFilter.addAction("1000026");
                this.mContext.registerReceiver(this.mDownLoadBroadcastReceiver, intentFilter);
            }
            Intent intent = new Intent("1000011");
            intent.setClass(this.mContext, SecureService.class);
            intent.putExtra("key_download_listener", apkDownLoadListener != null);
            this.mContext.startService(intent);
        }
    }

    public int register(ProductInfo productInfo) {
        if (productInfo == null) {
            return -6;
        }
        as.a(this.mContext, productInfo);
        return !ay.a(this.mContext, "sm_mq") ? -1 : 0;
    }

    public void registerCloudScanListener(Context context, CloudScanListener cloudScanListener) {
        boolean z;
        boolean z2 = false;
        Iterator<CloudScanBroadcastReceiver> it = this.mObserverList.iterator();
        while (true) {
            z = z2;
            if (!it.hasNext()) {
                break;
            }
            z2 = it.next().a(cloudScanListener) ? true : z;
        }
        if (!z) {
            CloudScanBroadcastReceiver cloudScanBroadcastReceiver = new CloudScanBroadcastReceiver(cloudScanListener);
            this.mObserverList.add(cloudScanBroadcastReceiver);
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("1000030");
            intentFilter.addAction("1000031");
            context.registerReceiver(cloudScanBroadcastReceiver, intentFilter);
        }
    }

    public void setNotificationUIEnable(boolean z) {
        as.b(this.mContext, 10002, z);
    }

    public void unregisterCloudScanListener(Context context, CloudScanListener cloudScanListener) {
        CloudScanBroadcastReceiver cloudScanBroadcastReceiver = null;
        for (CloudScanBroadcastReceiver next : this.mObserverList) {
            if (!next.a(cloudScanListener)) {
                next = cloudScanBroadcastReceiver;
            }
            cloudScanBroadcastReceiver = next;
        }
        if (cloudScanBroadcastReceiver != null) {
            this.mObserverList.remove(cloudScanBroadcastReceiver);
            context.unregisterReceiver(cloudScanBroadcastReceiver);
        }
    }
}
