package com.qq.c;

import com.qq.b.a;
import com.qq.g.c;

/* compiled from: ProGuard */
public class f {
    public static boolean a(String str, boolean z, int i, c cVar) {
        String str2 = "/system/bin/pm install ";
        if (z) {
            str2 = str2 + " -r ";
        }
        if (i == 1) {
            str2 = str2 + " -f ";
        } else if (i == 2) {
            str2 = str2 + " -s ";
        }
        return a(str2 + str, cVar);
    }

    public static boolean a(String str, boolean z) {
        String str2 = "/system/bin/pm uninstall";
        if (z) {
            str2 = str2 + " -k ";
        }
        return a(str2 + " " + str);
    }

    private static boolean a(String str, c cVar) {
        a aVar = new a();
        com.qq.b.c d = aVar.d(str, 150000);
        aVar.a();
        if (!(d == null || d.c == null)) {
            cVar.b = d.c;
            if (d.c == null || !new String(d.c).contains("Success")) {
                return false;
            }
            return true;
        }
        return false;
    }

    private static boolean a(String str) {
        a aVar = new a();
        com.qq.b.c d = aVar.d(str, 150000);
        aVar.a();
        if (d == null || d.c == null || !new String(d.c).contains("Success")) {
            return false;
        }
        return true;
    }
}
