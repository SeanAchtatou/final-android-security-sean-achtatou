package com.qq.c;

import android.util.Log;
import com.qq.AppService.s;
import com.qq.b.a;
import com.qq.b.c;
import java.io.File;

/* compiled from: ProGuard */
public final class e {
    public static boolean a(String str, int i) {
        if (d.f267a != null && d.a(d.f267a, b.b() + " " + i + " " + str, 0).d <= 0) {
            return true;
        }
        return false;
    }

    public static boolean a(File file, int i) {
        a aVar = new a();
        if (!file.exists() || !file.isDirectory()) {
            if (file.exists()) {
                c a2 = aVar.a(103, file.getAbsolutePath(), i, 2222);
                aVar.a();
                if (a2 != null && a2.f263a == 200) {
                    return true;
                }
            }
            return false;
        }
        c a3 = aVar.a(104, file.getAbsolutePath(), i, 2222);
        aVar.a();
        if (a3 == null || a3.f263a != 200) {
            return false;
        }
        return true;
    }

    public static boolean b(String str, int i) {
        if (d.f267a != null && d.a(d.f267a, b.b() + " -R  " + i + " " + str, 0).d <= 0) {
            return true;
        }
        return false;
    }

    public static byte[] a(String str) {
        if (d.f267a == null) {
            return null;
        }
        String a2 = b.a();
        if (!s.b(a2)) {
            return d.b(a2 + " -l " + str).b;
        }
        return null;
    }

    public static boolean b(String str) {
        boolean z = false;
        if (d.f267a != null) {
            String b = b.b();
            String str2 = d.f267a;
            if (b == null || str2 == null) {
                Log.e("com.qq.connect", "error: no su or no chomd");
            } else {
                c a2 = d.a(str2, b + " 777 " + str, 0);
                if (a2.d == 0) {
                    z = true;
                }
                if (a2.c != null) {
                    Log.e("com.qq.connect", "std-err:" + new String(a2.c));
                }
                if (a2.b != null) {
                    Log.e("com.qq.connect", "std-out:" + new String(a2.b));
                }
            }
        }
        return z;
    }
}
