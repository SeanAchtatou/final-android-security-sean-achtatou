package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.provider.CallLog;
import com.qq.AppService.s;
import com.qq.g.c;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class g {

    /* renamed from: a  reason: collision with root package name */
    private static g f341a = null;

    private g() {
    }

    public static g a() {
        if (f341a == null) {
            f341a = new g();
        }
        return f341a;
    }

    public void a(Context context, c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h <= 0) {
            cVar.a(1);
        } else if (cVar.e() < (h * 7) + 1) {
            cVar.a(1);
        } else {
            int i = 0;
            ContentValues[] contentValuesArr = new ContentValues[h];
            ArrayList arrayList = new ArrayList();
            while (i < h) {
                int h2 = cVar.h();
                int h3 = cVar.h();
                long i2 = cVar.i();
                int h4 = cVar.h();
                String j = cVar.j();
                String j2 = cVar.j();
                String j3 = cVar.j();
                if (s.b(j)) {
                    i++;
                } else {
                    contentValuesArr[i] = new ContentValues();
                    contentValuesArr[i].put(SocialConstants.PARAM_TYPE, Integer.valueOf(h2));
                    contentValuesArr[i].put("duration", Integer.valueOf(h3));
                    contentValuesArr[i].put("date", Long.valueOf(i2));
                    contentValuesArr[i].put("numbertype", Integer.valueOf(h4));
                    contentValuesArr[i].put("number", j);
                    if (s.b(j2)) {
                        contentValuesArr[i].put("name", j2);
                    }
                    if (h4 == 0 || !s.b(j3)) {
                        contentValuesArr[i].put("numberlabel", j3);
                    }
                    i++;
                }
            }
            try {
                context.getContentResolver().bulkInsert(CallLog.Calls.CONTENT_URI, contentValuesArr);
                arrayList.add(s.a(0));
                cVar.a(arrayList);
                cVar.a(0);
            } catch (Exception e) {
                cVar.a(8);
            }
        }
    }
}
