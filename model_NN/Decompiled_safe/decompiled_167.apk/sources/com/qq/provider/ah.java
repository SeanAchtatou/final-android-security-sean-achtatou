package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import com.qq.AppService.s;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class ah extends Thread {

    /* renamed from: a  reason: collision with root package name */
    public static File f326a = null;
    private static final String[] b = {"_id", "image_id", "_data"};
    private Context c = null;
    private boolean d = true;

    public ah(Context context) {
        this.c = context;
    }

    public void run() {
        Cursor query;
        Cursor query2;
        super.run();
        long currentTimeMillis = System.currentTimeMillis();
        if (this.c != null) {
            try {
                if (Environment.getExternalStorageState().equals("mounted")) {
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    query = this.c.getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, new String[]{"_id", "_data", "kind"}, null, null, null);
                    if (query != null) {
                        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
                            String string = query.getString(1);
                            int i = query.getInt(0);
                            if (string == null || !new File(string).exists()) {
                                arrayList2.add(Integer.valueOf(i));
                            }
                        }
                        query.close();
                    }
                    int size = arrayList.size();
                    for (int i2 = 0; i2 < size; i2++) {
                        new File((String) arrayList.get(i2)).delete();
                    }
                    int size2 = arrayList2.size();
                    if (size2 > 0) {
                        StringBuilder sb = new StringBuilder();
                        for (int i3 = 0; i3 < size2; i3++) {
                            if (i3 > 0) {
                                sb.append(" , ");
                            }
                            sb.append(arrayList2.get(i3));
                        }
                        this.c.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "_id in (" + sb.toString() + ")", null);
                    }
                    File externalStorageDirectory = Environment.getExternalStorageDirectory();
                    if (externalStorageDirectory != null && externalStorageDirectory.exists() && n.a(externalStorageDirectory.getAbsolutePath()) >= 16777216) {
                        f326a = new File(externalStorageDirectory.getAbsolutePath() + File.separator + "DCIM" + File.separator + ".thumbnails");
                        if (!f326a.exists()) {
                            f326a.mkdirs();
                        } else if (!f326a.isDirectory()) {
                            f326a.delete();
                            f326a.mkdirs();
                        }
                        if (f326a.exists() && f326a != null) {
                            String absolutePath = f326a.getAbsolutePath();
                            query2 = this.c.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "_data"}, "_id not in ( select image_id from  thumbnails   where  1  = 1)", null, "_id");
                            if (query2 != null) {
                                ArrayList arrayList3 = new ArrayList();
                                ArrayList arrayList4 = new ArrayList();
                                boolean moveToFirst2 = query2.moveToFirst();
                                while (moveToFirst2) {
                                    int i4 = query2.getInt(0);
                                    String string2 = query2.getString(1);
                                    if (this.d && string2 != null && new File(string2).exists()) {
                                        arrayList3.add(string2);
                                        arrayList4.add(Integer.valueOf(i4));
                                        moveToFirst2 = query2.moveToNext();
                                    }
                                }
                                query2.close();
                                int size3 = arrayList3.size();
                                if (this.d) {
                                    for (int i5 = 0; i5 < size3; i5++) {
                                        a(this.c, ((Integer) arrayList4.get(i5)).intValue(), (String) arrayList3.get(i5), absolutePath);
                                    }
                                }
                                Log.d("com.qq.connect", "thumb thread time spent" + (System.currentTimeMillis() - currentTimeMillis));
                            }
                        }
                    }
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0106, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x010e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x010f, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0112, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x010e A[ExcHandler: all (r0v8 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:9:0x0064] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x011b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r9, int r10, java.lang.String r11, java.lang.String r12) {
        /*
            r7 = 135956(0x21314, float:1.90515E-40)
            r6 = 0
            r4 = 0
            if (r11 == 0) goto L_0x0012
            java.io.File r0 = new java.io.File
            r0.<init>(r11)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0033
        L_0x0012:
            android.net.Uri r0 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = ""
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r10)
            java.lang.String r1 = r1.toString()
            android.net.Uri r0 = android.net.Uri.withAppendedPath(r0, r1)
            android.content.ContentResolver r1 = r9.getContentResolver()
            r1.delete(r0, r4, r4)
        L_0x0032:
            return r4
        L_0x0033:
            java.io.File r0 = new java.io.File
            r0.<init>(r11)
            long r0 = r0.length()
            r2 = 28672(0x7000, double:1.4166E-319)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 < 0) goto L_0x0032
            android.content.ContentResolver r0 = r9.getContentResolver()
            android.net.Uri r1 = android.provider.MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI
            java.lang.String[] r2 = com.qq.provider.ah.b
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "image_id = "
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.StringBuilder r3 = r3.append(r10)
            java.lang.String r3 = r3.toString()
            r5 = r4
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5)
            if (r2 == 0) goto L_0x0032
            boolean r0 = r2.moveToFirst()     // Catch:{ Throwable -> 0x0105, all -> 0x010e }
            if (r0 == 0) goto L_0x01aa
            r0 = 0
            int r0 = r2.getInt(r0)     // Catch:{ Throwable -> 0x0105, all -> 0x010e }
            r1 = 2
            java.lang.String r1 = r2.getString(r1)     // Catch:{ Throwable -> 0x01a7, all -> 0x010e }
        L_0x0074:
            r2.close()
            r8 = r0
            r0 = r1
            r1 = r8
        L_0x007a:
            if (r0 == 0) goto L_0x0087
            java.io.File r2 = new java.io.File
            r2.<init>(r0)
            boolean r2 = r2.exists()
            if (r2 != 0) goto L_0x0113
        L_0x0087:
            android.net.Uri r0 = android.provider.MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            android.net.Uri r0 = android.net.Uri.withAppendedPath(r0, r1)
            android.content.ContentResolver r1 = r9.getContentResolver()
            r1.delete(r0, r4, r4)
            java.lang.String r0 = "com.qq.connect"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = " thumb for "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r0, r1)
            long r0 = java.lang.System.currentTimeMillis()
            java.io.File r2 = new java.io.File
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r12)
            java.lang.String r5 = java.io.File.separator
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r1 = "+"
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = r11.hashCode()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ".jpg"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            r2.createNewFile()     // Catch:{ IOException -> 0x0116 }
        L_0x00f6:
            boolean r0 = r2.exists()
            if (r0 != 0) goto L_0x011b
            java.lang.String r0 = "com.qq.connect"
            java.lang.String r1 = "create thumb file failed"
            android.util.Log.d(r0, r1)
            goto L_0x0032
        L_0x0105:
            r0 = move-exception
            r0 = r6
        L_0x0107:
            r2.close()
            r1 = r0
            r0 = r4
            goto L_0x007a
        L_0x010e:
            r0 = move-exception
            r2.close()
            throw r0
        L_0x0113:
            r4 = r0
            goto L_0x0032
        L_0x0116:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00f6
        L_0x011b:
            r0 = 135956(0x21314, float:1.90515E-40)
            android.graphics.Bitmap r0 = a(r11, r0)     // Catch:{ Throwable -> 0x0130 }
        L_0x0122:
            if (r0 != 0) goto L_0x0136
            java.lang.String r0 = "com.qq.connect"
            java.lang.String r1 = "create thumb bitmap failed"
            android.util.Log.d(r0, r1)
            r2.delete()
            goto L_0x0032
        L_0x0130:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r4
            goto L_0x0122
        L_0x0136:
            int r1 = r0.getWidth()
            int r3 = r0.getHeight()
            android.graphics.Bitmap$CompressFormat r5 = android.graphics.Bitmap.CompressFormat.JPEG
            r6 = 100
            byte[] r0 = com.qq.AppService.s.a(r0, r5, r6)
            if (r0 != 0) goto L_0x0154
            r2.delete()
            java.lang.String r0 = "com.qq.connect"
            java.lang.String r1 = "compress thumf failed!"
            android.util.Log.d(r0, r1)
            goto L_0x0032
        L_0x0154:
            boolean r0 = com.qq.provider.n.a(r2, r0)
            if (r0 == 0) goto L_0x019b
            android.content.ContentValues r0 = new android.content.ContentValues
            r0.<init>()
            java.lang.String r4 = "image_id"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r10)
            r0.put(r4, r5)
            java.lang.String r4 = "kind"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r7)
            r0.put(r4, r5)
            java.lang.String r4 = "_data"
            java.lang.String r5 = r2.getAbsolutePath()
            r0.put(r4, r5)
            java.lang.String r4 = "height"
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r0.put(r4, r3)
            java.lang.String r3 = "width"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r0.put(r3, r1)
            android.content.ContentResolver r1 = r9.getContentResolver()
            android.net.Uri r3 = android.provider.MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI
            r1.insert(r3, r0)
            java.lang.String r4 = r2.getAbsolutePath()
            goto L_0x0032
        L_0x019b:
            java.lang.String r0 = "com.qq.connect"
            java.lang.String r1 = "write file failed"
            android.util.Log.d(r0, r1)
            r2.delete()
            goto L_0x0032
        L_0x01a7:
            r1 = move-exception
            goto L_0x0107
        L_0x01aa:
            r0 = r6
            r1 = r4
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.ah.a(android.content.Context, int, java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static String a(Context context, int i, String str, String str2, boolean z) {
        Bitmap bitmap;
        byte[] a2;
        if (str == null || !new File(str).exists()) {
            Uri withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + i);
            if (z) {
                return null;
            }
            context.getContentResolver().delete(withAppendedPath, null, null);
            return null;
        }
        Cursor query = context.getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, b, "image_id = " + i, null, null);
        if (query == null) {
            return null;
        }
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            int i2 = query.getInt(0);
            String string = query.getString(2);
            if (string == null || !new File(string).exists()) {
                context.getContentResolver().delete(Uri.withAppendedPath(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + i2), null, null);
            } else if (z) {
                new File(string).delete();
            }
        }
        query.close();
        if (z) {
            context.getContentResolver().delete(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "image_id = " + i, null);
        }
        File file = new File(str2 + File.separator + System.currentTimeMillis() + "-" + str.hashCode() + ".jpg");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!file.exists()) {
            return null;
        }
        try {
            bitmap = ThumbnailUtils.createImageThumbnail(str, 1);
        } catch (Throwable th) {
            th.printStackTrace();
            bitmap = null;
        }
        if (bitmap == null || (a2 = s.a(bitmap, Bitmap.CompressFormat.JPEG, 100)) == null) {
            return null;
        }
        if (n.b(file, a2)) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("image_id", Integer.valueOf(i));
            contentValues.put("kind", (Integer) 3);
            contentValues.put("_data", file.getAbsolutePath());
            contentValues.put("height", Integer.valueOf(bitmap.getHeight()));
            contentValues.put("width", Integer.valueOf(bitmap.getWidth()));
            context.getContentResolver().insert(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, contentValues);
            return file.getAbsolutePath();
        }
        file.delete();
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x00ce, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00cf, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x00da, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x00db, code lost:
        r3 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x00e9, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x00ea, code lost:
        r2 = r4;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00ca A[SYNTHETIC, Splitter:B:67:0x00ca] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x00da A[ExcHandler: all (th java.lang.Throwable), Splitter:B:50:0x0096] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(java.lang.String r10, int r11) {
        /*
            r9 = 19
            r8 = -1
            r1 = 0
            r0 = 1
            r2 = 0
            if (r11 != r0) goto L_0x003c
        L_0x0008:
            if (r0 == 0) goto L_0x003e
            r1 = 320(0x140, float:4.48E-43)
        L_0x000c:
            if (r0 == 0) goto L_0x0041
            r0 = 196608(0x30000, float:2.75506E-40)
        L_0x0010:
            r3 = 135956(0x21314, float:1.90515E-40)
            if (r11 != r3) goto L_0x001a
            r1 = 480(0x1e0, float:6.73E-43)
            r0 = 153600(0x25800, float:2.1524E-40)
        L_0x001a:
            java.io.File r3 = new java.io.File
            r3.<init>(r10)
            boolean r4 = r3.exists()
            if (r4 == 0) goto L_0x003a
            long r4 = r3.length()
            r6 = 128(0x80, double:6.32E-322)
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 < 0) goto L_0x003a
            long r3 = r3.length()
            r5 = 6291456(0x600000, double:3.1083923E-317)
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 < 0) goto L_0x0044
        L_0x003a:
            r0 = r2
        L_0x003b:
            return r0
        L_0x003c:
            r0 = r1
            goto L_0x0008
        L_0x003e:
            r1 = 96
            goto L_0x000c
        L_0x0041:
            r0 = 16384(0x4000, float:2.2959E-41)
            goto L_0x0010
        L_0x0044:
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x00e0, all -> 0x00c6 }
            r3.<init>(r10)     // Catch:{ Throwable -> 0x00e0, all -> 0x00c6 }
            java.io.FileDescriptor r4 = r3.getFD()     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
            android.graphics.BitmapFactory$Options r5 = new android.graphics.BitmapFactory$Options     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
            r5.<init>()     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
            r6 = 1
            r5.inSampleSize = r6     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
            r6 = 1
            r5.inJustDecodeBounds = r6     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
            if (r6 < r9) goto L_0x0073
            r4 = 0
            android.graphics.BitmapFactory.decodeStream(r3, r4, r5)     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
        L_0x0060:
            boolean r4 = r5.mCancel     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
            if (r4 != 0) goto L_0x006c
            int r4 = r5.outWidth     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
            if (r4 == r8) goto L_0x006c
            int r4 = r5.outHeight     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
            if (r4 != r8) goto L_0x008e
        L_0x006c:
            if (r3 == 0) goto L_0x0071
            r3.close()     // Catch:{ IOException -> 0x00d3 }
        L_0x0071:
            r0 = r2
            goto L_0x003b
        L_0x0073:
            r6 = 0
            android.graphics.BitmapFactory.decodeFileDescriptor(r4, r6, r5)     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
            goto L_0x0060
        L_0x0078:
            r0 = move-exception
            r1 = r0
            r0 = r2
            r2 = r3
        L_0x007c:
            java.lang.String r3 = "com.qq.connect"
            java.lang.String r4 = ""
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x00dd }
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ IOException -> 0x0089 }
            goto L_0x003b
        L_0x0089:
            r1 = move-exception
        L_0x008a:
            r1.printStackTrace()
            goto L_0x003b
        L_0x008e:
            r3.close()     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
            r4.<init>(r10)     // Catch:{ Throwable -> 0x0078, all -> 0x00d8 }
            java.io.FileDescriptor r3 = r4.getFD()     // Catch:{ Throwable -> 0x00e4, all -> 0x00da }
            int r0 = a(r5, r1, r0)     // Catch:{ Throwable -> 0x00e4, all -> 0x00da }
            r5.inSampleSize = r0     // Catch:{ Throwable -> 0x00e4, all -> 0x00da }
            r0 = 0
            r5.inJustDecodeBounds = r0     // Catch:{ Throwable -> 0x00e4, all -> 0x00da }
            r0 = 0
            r5.inDither = r0     // Catch:{ Throwable -> 0x00e4, all -> 0x00da }
            android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ Throwable -> 0x00e4, all -> 0x00da }
            r5.inPreferredConfig = r0     // Catch:{ Throwable -> 0x00e4, all -> 0x00da }
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ Throwable -> 0x00e4, all -> 0x00da }
            if (r0 < r9) goto L_0x00c0
            r0 = 0
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r4, r0, r5)     // Catch:{ Throwable -> 0x00e4, all -> 0x00da }
        L_0x00b3:
            r4.close()     // Catch:{ Throwable -> 0x00e9, all -> 0x00da }
            r1 = 0
            if (r2 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x00be }
            goto L_0x003b
        L_0x00be:
            r1 = move-exception
            goto L_0x008a
        L_0x00c0:
            r0 = 0
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeFileDescriptor(r3, r0, r5)     // Catch:{ Throwable -> 0x00e4, all -> 0x00da }
            goto L_0x00b3
        L_0x00c6:
            r0 = move-exception
            r3 = r2
        L_0x00c8:
            if (r3 == 0) goto L_0x00cd
            r3.close()     // Catch:{ IOException -> 0x00ce }
        L_0x00cd:
            throw r0
        L_0x00ce:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00cd
        L_0x00d3:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0071
        L_0x00d8:
            r0 = move-exception
            goto L_0x00c8
        L_0x00da:
            r0 = move-exception
            r3 = r4
            goto L_0x00c8
        L_0x00dd:
            r0 = move-exception
            r3 = r2
            goto L_0x00c8
        L_0x00e0:
            r0 = move-exception
            r1 = r0
            r0 = r2
            goto L_0x007c
        L_0x00e4:
            r0 = move-exception
            r1 = r0
            r0 = r2
            r2 = r4
            goto L_0x007c
        L_0x00e9:
            r1 = move-exception
            r2 = r4
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.ah.a(java.lang.String, int):android.graphics.Bitmap");
    }

    private static int a(BitmapFactory.Options options, int i, int i2) {
        int b2 = b(options, i, i2);
        if (b2 > 8) {
            return ((b2 + 7) / 8) * 8;
        }
        int i3 = 1;
        while (i3 < b2) {
            i3 <<= 1;
        }
        return i3;
    }

    private static int b(BitmapFactory.Options options, int i, int i2) {
        double d2 = (double) options.outWidth;
        double d3 = (double) options.outHeight;
        int ceil = i2 == -1 ? 1 : (int) Math.ceil(Math.sqrt((d2 * d3) / ((double) i2)));
        int min = i == -1 ? 128 : (int) Math.min(Math.floor(d2 / ((double) i)), Math.floor(d3 / ((double) i)));
        if (min < ceil) {
            return ceil;
        }
        if (i2 == -1 && i == -1) {
            return 1;
        }
        if (i != -1) {
            return min;
        }
        return ceil;
    }
}
