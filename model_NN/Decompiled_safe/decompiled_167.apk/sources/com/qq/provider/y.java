package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.telephony.SmsMessage;
import com.qq.AppService.AppService;
import com.qq.AppService.s;
import com.qq.a.a.d;
import com.qq.g.c;
import com.qq.j.a;
import com.qq.util.j;
import com.qq.util.x;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.Date;

/* compiled from: ProGuard */
public class y {
    private static y b;

    /* renamed from: a  reason: collision with root package name */
    public Context f357a = null;

    public y(Context context) {
        this.f357a = context;
    }

    public static y a(Context context) {
        if (b == null) {
            b = new y(context);
        }
        return b;
    }

    public boolean a(String str) {
        int length = str.length();
        if (length < 3) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if ((charAt != '+' || i != 0) && !Character.isDigit(charAt)) {
                return false;
            }
        }
        return true;
    }

    public void a(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Uri a2 = x.a(cVar.h());
        if (a2 == null) {
            cVar.a(1);
            return;
        }
        Cursor query = this.f357a.getContentResolver().query(a2, j.b, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        query.close();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(count));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void a(Context context, c cVar) {
        Cursor query = context.getContentResolver().query(d.b, j.b, "read = 0", null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        query.close();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(count));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void b(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Uri a2 = x.a(cVar.h());
        if (a2 == null) {
            cVar.a(1);
            return;
        }
        Cursor query = this.f357a.getContentResolver().query(a2, j.b, null, null, "date DESC");
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(count));
        query.moveToFirst();
        for (int i = 0; i < count; i++) {
            arrayList.add(s.a(query.getInt(query.getColumnIndex("_id"))));
            query.moveToNext();
        }
        query.close();
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void c(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Cursor query = this.f357a.getContentResolver().query(Uri.withAppendedPath(d.f259a, Constants.STR_EMPTY + cVar.h()), null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        if (!query.moveToFirst()) {
            cVar.a(1);
        } else {
            arrayList.add(s.a(query.getInt(query.getColumnIndex("_id"))));
            byte[] blob = query.getBlob(query.getColumnIndex("address"));
            if (blob == null) {
                blob = s.hr;
            }
            arrayList.add(blob);
            arrayList.add(s.a(query.getLong(query.getColumnIndex("date"))));
            arrayList.add(s.a(query.getInt(query.getColumnIndex("read"))));
            arrayList.add(s.a(query.getInt(query.getColumnIndex(SocialConstants.PARAM_TYPE))));
            byte[] blob2 = query.getBlob(query.getColumnIndex("body"));
            if (blob2 == null) {
                blob2 = s.hr;
            }
            arrayList.add(blob2);
        }
        query.close();
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void d(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Uri a2 = x.a(cVar.h());
        if (a2 == null) {
            cVar.a(1);
            return;
        }
        Cursor query = this.f357a.getContentResolver().query(a2, null, null, null, "date DESC");
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(count));
        query.moveToFirst();
        for (int i = 0; i < count; i++) {
            arrayList.add(s.a(query.getInt(query.getColumnIndex("_id"))));
            byte[] blob = query.getBlob(query.getColumnIndex("address"));
            if (blob == null) {
                blob = s.hr;
            }
            arrayList.add(blob);
            arrayList.add(s.a(query.getInt(query.getColumnIndex("read"))));
            arrayList.add(s.a(query.getInt(query.getColumnIndex(SocialConstants.PARAM_TYPE))));
            byte[] blob2 = query.getBlob(query.getColumnIndex("body"));
            if (blob2 == null) {
                blob2 = s.hr;
            }
            arrayList.add(blob2);
            query.moveToNext();
        }
        query.close();
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void e(c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h2 < 0) {
            cVar.a(1);
            return;
        }
        Cursor query = this.f357a.getContentResolver().query(d.f259a, null, "_id>" + h, null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        if (count > h2) {
            count = h2;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(count));
        boolean moveToFirst = query.moveToFirst();
        int i = 0;
        while (i < count && moveToFirst) {
            arrayList.add(s.a(query.getInt(query.getColumnIndex("_id"))));
            byte[] blob = query.getBlob(query.getColumnIndex("address"));
            if (blob == null) {
                blob = s.hr;
            }
            arrayList.add(blob);
            arrayList.add(s.a(query.getLong(query.getColumnIndex("date"))));
            arrayList.add(s.a(query.getInt(query.getColumnIndex("read"))));
            arrayList.add(s.a(query.getInt(query.getColumnIndex(SocialConstants.PARAM_TYPE))));
            byte[] blob2 = query.getBlob(query.getColumnIndex("body"));
            if (blob2 == null) {
                blob2 = s.hr;
            }
            arrayList.add(blob2);
            i++;
            moveToFirst = query.moveToNext();
        }
        query.close();
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void f(c cVar) {
        int i;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h <= 0) {
            Cursor query = this.f357a.getContentResolver().query(d.f259a, j.b, null, null, "_id");
            if (query == null) {
                cVar.a(7);
                return;
            }
            if (!query.moveToLast()) {
                i = 0;
            } else {
                i = query.getInt(query.getColumnIndex("_id")) + 1;
            }
            query.close();
            h = i;
        }
        if (h2 < 0) {
            cVar.a(1);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(h2));
        int i2 = 0;
        int i3 = h;
        int i4 = (h - h2) - 300;
        while (i2 < h2 && i3 > 0) {
            Cursor query2 = this.f357a.getContentResolver().query(d.f259a, null, "_id < " + i3 + " and " + "_id" + " >= " + i4, null, "_id");
            if (query2 == null) {
                cVar.a(8);
                return;
            }
            int count = query2.getCount();
            int i5 = i2;
            int i6 = 0;
            boolean moveToLast = query2.moveToLast();
            while (i6 < count && moveToLast) {
                arrayList.add(s.a(query2.getInt(query2.getColumnIndex("_id"))));
                arrayList.add(s.a(query2.getInt(query2.getColumnIndex("thread_id"))));
                byte[] blob = query2.getBlob(query2.getColumnIndex("address"));
                if (blob == null) {
                    blob = s.hr;
                }
                arrayList.add(blob);
                arrayList.add(s.a(query2.getLong(query2.getColumnIndex("date"))));
                arrayList.add(s.a(query2.getInt(query2.getColumnIndex("read"))));
                arrayList.add(s.a(query2.getInt(query2.getColumnIndex(SocialConstants.PARAM_TYPE))));
                byte[] blob2 = query2.getBlob(query2.getColumnIndex("body"));
                if (blob2 == null) {
                    blob2 = s.hr;
                }
                arrayList.add(blob2);
                i6++;
                moveToLast = query2.moveToPrevious();
                i5++;
            }
            query2.close();
            i2 = i5;
            i3 = i4;
            i4 = (i4 - h2) - 300;
        }
        arrayList.set(0, s.a(i2));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void b(Context context, c cVar) {
        int i;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        int h = cVar.h();
        long a2 = x.a(context, j);
        if (h <= 0) {
            i = 10;
        } else {
            i = h;
        }
        Cursor query = context.getContentResolver().query(x.a(0), null, "thread_id = " + a2, null, "date");
        if (query == null) {
            cVar.a(1);
            return;
        }
        int count = query.getCount();
        if (count < i) {
            i = count;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(i));
        boolean moveToLast = query.moveToLast();
        int i2 = 0;
        while (i2 < i && moveToLast) {
            arrayList.add(s.a(query.getInt(query.getColumnIndex("_id"))));
            byte[] blob = query.getBlob(query.getColumnIndex("address"));
            if (blob == null) {
                blob = s.hr;
            }
            arrayList.add(blob);
            arrayList.add(s.a(query.getLong(query.getColumnIndex("date"))));
            arrayList.add(s.a(query.getInt(query.getColumnIndex("read"))));
            arrayList.add(s.a(query.getInt(query.getColumnIndex(SocialConstants.PARAM_TYPE))));
            byte[] blob2 = query.getBlob(query.getColumnIndex("body"));
            if (blob2 == null) {
                blob2 = s.hr;
            }
            arrayList.add(blob2);
            i2++;
            moveToLast = query.moveToPrevious();
        }
        query.close();
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void g(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        if (this.f357a.getContentResolver().delete(Uri.withAppendedPath(d.f259a, Constants.STR_EMPTY + cVar.h()), null, null) <= 0) {
            cVar.a(7);
        } else {
            cVar.a(0);
        }
    }

    public void h(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        this.f357a.getContentResolver().delete(d.f259a, "thread_id=" + cVar.h(), null);
        cVar.a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void c(Context context, c cVar) {
        long a2;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (cVar.e() < (h * 5) + 1) {
            cVar.a(1);
            return;
        }
        ArrayList arrayList = new ArrayList();
        ContentValues[] contentValuesArr = new ContentValues[h];
        Context b2 = AppService.b();
        if (b2 == null) {
            b2 = context;
        }
        String str = null;
        long j = 0;
        int i = 0;
        while (i < h) {
            contentValuesArr[i] = new ContentValues();
            String j2 = cVar.j();
            String j3 = cVar.j();
            long i2 = cVar.i();
            int h2 = cVar.h();
            int h3 = cVar.h();
            if (!s.b(j2)) {
                contentValuesArr[i].put("address", j2);
            }
            if (!s.b(j3)) {
                contentValuesArr[i].put("body", j3);
            }
            if (j2 == null || str == null || !j2.equals(str)) {
                a2 = (long) ((int) x.a(b2, j2));
                if (a2 >= 0) {
                    j = a2;
                    str = j2;
                } else {
                    long j4 = a2;
                    a2 = j;
                    j = j4;
                }
            } else {
                a2 = j;
            }
            contentValuesArr[i].put("thread_id", Long.valueOf(j));
            contentValuesArr[i].put("date", Long.valueOf(i2));
            if (h2 == 1) {
                contentValuesArr[i].put("read", Integer.valueOf(h3));
            }
            contentValuesArr[i].put("status", (Integer) -1);
            contentValuesArr[i].put(SocialConstants.PARAM_TYPE, Integer.valueOf(h2));
            context.getContentResolver().insert(d.f259a, contentValuesArr[i]);
            i++;
            j = a2;
        }
        arrayList.add(s.a(0));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void d(Context context, c cVar) {
        int i;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (s.b(j)) {
            cVar.a(1);
            return;
        }
        try {
            i = context.getContentResolver().delete(d.f259a, "address =  '" + j + "'", null);
        } catch (SQLiteException e) {
            e.printStackTrace();
            i = 1;
        }
        if (i > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void i(c cVar) {
        if (cVar.e() < 10) {
            cVar.a(1);
            return;
        }
        cVar.g();
        cVar.g();
        String j = cVar.j();
        cVar.g();
        String j2 = cVar.j();
        cVar.h();
        cVar.h();
        cVar.h();
        cVar.g();
        String j3 = cVar.j();
        if (s.b(j) || !a(j)) {
            cVar.a(7);
            return;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("thread_id", Integer.valueOf((int) a.a(this.f357a, j)));
        if (!s.b(j)) {
            contentValues.put("address", j);
        }
        if (!s.b(j2)) {
            contentValues.put("date", Long.valueOf(s.c(j2).getTime()));
        } else {
            contentValues.put("date", Long.valueOf(new Date().getTime()));
        }
        contentValues.put("read", (Integer) 1);
        contentValues.put("status", (Integer) 64);
        contentValues.put(SocialConstants.PARAM_TYPE, (Integer) 2);
        if (!s.b(j3)) {
            contentValues.put("body", j3);
        }
        Uri insert = this.f357a.getContentResolver().insert(d.f259a, contentValues);
        if (insert == null) {
            cVar.a(8);
            return;
        }
        int parseInt = Integer.parseInt(insert.getLastPathSegment());
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(parseInt));
        a(this.f357a, j, j3, parseInt);
        cVar.a(0);
        cVar.a(arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void}
     arg types: [android.net.Uri, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, int):void}
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void} */
    public void j(c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(d.f259a, Constants.STR_EMPTY + h);
        Cursor query = this.f357a.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            query.close();
            cVar.a(1);
        } else {
            int i = query.getInt(query.getColumnIndex(SocialConstants.PARAM_TYPE));
            String string = query.getString(query.getColumnIndex("address"));
            String string2 = query.getString(query.getColumnIndex("body"));
            query.close();
            if (s.b(string) || !a(string)) {
                cVar.a(7);
                return;
            }
            if (i != 6) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(SocialConstants.PARAM_TYPE, (Integer) 2);
                contentValues.put("status", (Integer) 64);
                contentValues.put("read", (Integer) 1);
                this.f357a.getContentResolver().update(withAppendedPath, contentValues, null, null);
                this.f357a.getContentResolver().notifyChange(withAppendedPath, (ContentObserver) null, false);
            }
            a(this.f357a, string, string2, h);
            cVar.a(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void}
     arg types: [android.net.Uri, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, int):void}
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void} */
    public void k(c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        Uri withAppendedPath = Uri.withAppendedPath(d.f259a, Constants.STR_EMPTY + cVar.h());
        Cursor query = this.f357a.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            cVar.a(1);
            query.close();
        } else {
            query.close();
            int h = cVar.h();
            ContentValues contentValues = new ContentValues();
            if (h > 0) {
                contentValues.put("read", (Integer) 1);
            } else {
                contentValues.put("read", (Integer) 0);
            }
            if (this.f357a.getContentResolver().update(withAppendedPath, contentValues, null, null) < 1) {
                cVar.a(1);
                return;
            }
            this.f357a.getContentResolver().notifyChange(withAppendedPath, (ContentObserver) null, false);
            cVar.a(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void}
     arg types: [android.net.Uri, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, int):void}
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void} */
    public void l(c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(d.f259a, Constants.STR_EMPTY + h);
        ContentValues contentValues = new ContentValues();
        if (h2 >= 0 && h2 <= 6) {
            contentValues.put(SocialConstants.PARAM_TYPE, Integer.valueOf(h2));
        }
        if (this.f357a.getContentResolver().update(withAppendedPath, contentValues, null, null) < 1) {
            cVar.a(1);
            return;
        }
        this.f357a.getContentResolver().notifyChange(withAppendedPath, (ContentObserver) null, false);
        cVar.a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void}
     arg types: [android.net.Uri, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, int):void}
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void} */
    public void m(c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        Uri withAppendedPath = Uri.withAppendedPath(d.f259a, Constants.STR_EMPTY + cVar.h());
        int h = cVar.h();
        ContentValues contentValues = new ContentValues();
        contentValues.put("status", Integer.valueOf(h));
        if (this.f357a.getContentResolver().update(withAppendedPath, contentValues, null, null) < 1) {
            cVar.a(1);
            return;
        }
        this.f357a.getContentResolver().notifyChange(withAppendedPath, (ContentObserver) null, false);
        cVar.a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void}
     arg types: [android.net.Uri, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, int):void}
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void} */
    public void e(Context context, c cVar) {
        if (cVar.e() < 10) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        cVar.g();
        String j = cVar.j();
        cVar.g();
        String j2 = cVar.j();
        int h2 = cVar.h();
        int h3 = cVar.h();
        int h4 = cVar.h();
        String j3 = cVar.j();
        String j4 = cVar.j();
        ContentValues contentValues = new ContentValues();
        if (!s.b(j)) {
            contentValues.put("address", j);
        } else {
            contentValues.putNull("address");
        }
        contentValues.put("thread_id", Long.valueOf(x.a(context, j)));
        contentValues.put("date", Long.valueOf(s.c(j2).getTime()));
        contentValues.put("read", Integer.valueOf(h2));
        contentValues.put("status", Integer.valueOf(h3));
        contentValues.put(SocialConstants.PARAM_TYPE, Integer.valueOf(h4));
        if (!s.b(j3)) {
            contentValues.put("subject", j3);
        } else {
            contentValues.putNull("subject");
        }
        if (!s.b(j4)) {
            contentValues.put("body", j4);
        } else {
            contentValues.putNull("body");
        }
        Uri withAppendedPath = Uri.withAppendedPath(d.f259a, Constants.STR_EMPTY + h);
        if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) < 1) {
            cVar.a(1);
            return;
        }
        context.getContentResolver().notifyChange(withAppendedPath, (ContentObserver) null, false);
        cVar.a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void f(Context context, c cVar) {
        int i;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String[] strArr = null;
        if (cVar.e() < h + 2) {
            cVar.a(1);
            return;
        }
        if (h > 0) {
            strArr = new String[h];
        }
        for (int i2 = 0; i2 < h; i2++) {
            strArr[i2] = cVar.j();
        }
        String j = cVar.j();
        z zVar = new z(this);
        ArrayList arrayList = new ArrayList();
        arrayList.add(s.a(h));
        for (int i3 = 0; i3 < h; i3++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("thread_id", Integer.valueOf((int) x.a(context, strArr[i3])));
            if (!s.b(strArr[i3])) {
                contentValues.put("address", strArr[i3]);
            } else {
                contentValues.putNull("address");
            }
            contentValues.put("date", Long.valueOf(new Date().getTime()));
            contentValues.put("read", (Integer) 1);
            contentValues.put("status", (Integer) 64);
            contentValues.put(SocialConstants.PARAM_TYPE, (Integer) 2);
            contentValues.putNull("subject");
            if (!s.b(j)) {
                contentValues.put("body", j);
            } else {
                contentValues.putNull("body");
            }
            try {
                i = Integer.parseInt(context.getContentResolver().insert(d.f259a, contentValues).getLastPathSegment());
            } catch (Exception e) {
                e.printStackTrace();
                i = 0;
            }
            arrayList.add(s.a(i));
            if (!s.b(strArr[i3]) && a(strArr[i3])) {
                zVar.a(strArr[i3], j, i);
            }
        }
        zVar.a();
        cVar.a(arrayList);
        cVar.a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[Catch:{ Exception -> 0x00b6 }, ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.content.Context r11, java.lang.String r12, java.lang.String r13, int r14) {
        /*
            r10 = this;
            r6 = 1
            android.telephony.SmsManager r0 = android.telephony.SmsManager.getDefault()
            java.util.ArrayList r3 = r0.divideMessage(r13)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            android.content.Intent r1 = new android.content.Intent
            java.lang.Class<com.qq.AppService.SmsSentReceiver> r2 = com.qq.AppService.SmsSentReceiver.class
            r1.<init>(r11, r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r5 = "ACTION_SMS_SENT"
            java.lang.StringBuilder r2 = r2.append(r5)
            long r7 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r2 = r2.toString()
            r1.setAction(r2)
            java.lang.String r2 = "_id"
            r1.putExtra(r2, r14)
            r2 = 0
            r5 = 0
            android.app.PendingIntent r7 = android.app.PendingIntent.getBroadcast(r11, r2, r1, r5)
            r4.add(r7)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            android.content.Intent r1 = new android.content.Intent
            java.lang.Class<com.qq.AppService.SmsSentReceiver> r2 = com.qq.AppService.SmsSentReceiver.class
            r1.<init>(r11, r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r8 = "ACTION_SMS_DELIVERY"
            java.lang.StringBuilder r2 = r2.append(r8)
            long r8 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r2 = r2.toString()
            r1.setAction(r2)
            java.lang.String r2 = "_id"
            r1.putExtra(r2, r14)
            r2 = 0
            r8 = 0
            android.app.PendingIntent r8 = android.app.PendingIntent.getBroadcast(r11, r2, r1, r8)
            r5.add(r8)
            java.lang.String r1 = "com.qq.connect.smsHandler"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r9 = "sms "
            java.lang.StringBuilder r2 = r2.append(r9)
            java.lang.StringBuilder r2 = r2.append(r14)
            java.lang.String r9 = " sent"
            java.lang.StringBuilder r2 = r2.append(r9)
            java.lang.String r2 = r2.toString()
            android.util.Log.e(r1, r2)
            if (r12 == 0) goto L_0x00a3
            if (r3 == 0) goto L_0x00a3
            int r1 = r3.size()     // Catch:{ Exception -> 0x00b6 }
            r2 = 1
            if (r1 <= r2) goto L_0x00a3
            r2 = 0
            r1 = r12
            r0.sendMultipartTextMessage(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00b6 }
        L_0x009f:
            r0 = r6
        L_0x00a0:
            if (r0 == 0) goto L_0x00c4
        L_0x00a2:
            return
        L_0x00a3:
            if (r12 == 0) goto L_0x00bc
            if (r3 == 0) goto L_0x00bc
            r2 = 0
            r1 = 0
            java.lang.Object r3 = r3.get(r1)     // Catch:{ Exception -> 0x00b6 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x00b6 }
            r1 = r12
            r4 = r7
            r5 = r8
            r0.sendTextMessage(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00b6 }
            goto L_0x009f
        L_0x00b6:
            r0 = move-exception
            r0.printStackTrace()
            r0 = 0
            goto L_0x00a0
        L_0x00bc:
            java.lang.String r0 = "com.qq.connect"
            java.lang.String r1 = "sendSMS failed!    "
            android.util.Log.d(r0, r1)     // Catch:{ Exception -> 0x00b6 }
            goto L_0x009f
        L_0x00c4:
            android.net.Uri r0 = com.qq.a.a.d.f259a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = ""
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r14)
            java.lang.String r1 = r1.toString()
            android.net.Uri r0 = android.net.Uri.withAppendedPath(r0, r1)
            android.content.ContentValues r1 = new android.content.ContentValues
            r1.<init>()
            java.lang.String r2 = "type"
            r3 = 6
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r1.put(r2, r3)
            java.lang.String r2 = "status"
            r3 = 128(0x80, float:1.794E-43)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r1.put(r2, r3)
            android.content.ContentResolver r2 = r11.getContentResolver()
            r3 = 0
            r4 = 0
            r2.update(r0, r1, r3, r4)
            goto L_0x00a2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.y.a(android.content.Context, java.lang.String, java.lang.String, int):void");
    }

    public static int a(byte[] bArr) {
        SmsMessage smsMessage = null;
        try {
            smsMessage = SmsMessage.createFromPdu(bArr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (smsMessage == null) {
            return -1;
        }
        return smsMessage.getStatus();
    }
}
