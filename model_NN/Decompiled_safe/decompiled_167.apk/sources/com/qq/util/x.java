package com.qq.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import java.util.HashSet;
import java.util.Set;

/* compiled from: ProGuard */
public final class x implements y {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f377a = Uri.withAppendedPath(w.f376a, "conversations");
    public static final Uri b = Uri.withAppendedPath(f377a, "obsolete");
    private static final String[] c = {"_id"};
    private static final Uri d = Uri.parse("content://mms-sms/threadID");

    private x() {
    }

    public static long a(Context context, String str) {
        HashSet hashSet = new HashSet();
        hashSet.add(str);
        return a(context, hashSet);
    }

    public static long a(Context context, Set<String> set) {
        Uri.Builder buildUpon = d.buildUpon();
        for (String next : set) {
            if (v.b(next)) {
                next = v.a(next);
            }
            buildUpon.appendQueryParameter("recipient", next);
        }
        Context context2 = context;
        Cursor a2 = r.a(context2, context.getContentResolver(), buildUpon.build(), c, null, null, null);
        if (a2 != null) {
            if (a2.moveToFirst()) {
                long j = a2.getLong(0);
                a2.close();
                return j;
            }
            Log.e("Telephony", "getOrCreateThreadId returned no rows!");
            a2.close();
        }
        throw new IllegalArgumentException("Unable to find or allocate a thread ID.");
    }
}
