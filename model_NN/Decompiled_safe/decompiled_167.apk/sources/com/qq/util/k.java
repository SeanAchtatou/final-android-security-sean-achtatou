package com.qq.util;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

/* compiled from: ProGuard */
public class k {
    public static byte[] a(byte[] bArr, byte[] bArr2) {
        SecretKey generateSecret = SecretKeyFactory.getInstance("desede").generateSecret(new DESedeKeySpec(bArr));
        Cipher instance = Cipher.getInstance("desede/ECB/PKCS5Padding");
        instance.init(1, generateSecret);
        return instance.doFinal(bArr2);
    }

    public static byte[] b(byte[] bArr, byte[] bArr2) {
        SecretKey generateSecret = SecretKeyFactory.getInstance("desede").generateSecret(new DESedeKeySpec(bArr));
        Cipher instance = Cipher.getInstance("desede/ECB/PKCS5Padding");
        instance.init(2, generateSecret);
        return instance.doFinal(bArr2);
    }
}
