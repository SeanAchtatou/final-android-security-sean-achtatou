package com.qq.AppService;

import com.tencent.assistant.Global;
import com.tencent.assistant.m;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f245a;
    final /* synthetic */ int b;
    final /* synthetic */ AstApp c;

    l(AstApp astApp, int i, int i2) {
        this.c = astApp;
        this.f245a = i;
        this.b = i2;
    }

    public void run() {
        if (this.f245a > m.a().a("bao_history_version_code", -1)) {
            String a2 = m.a().a("key_current_qua", Constants.STR_EMPTY);
            m.a().b("bao_history_version_code", Integer.valueOf(this.f245a));
            m.a().b("key_history_qua", a2);
        }
        try {
            this.c.F();
            m.a().b("bao_current_version_code", Integer.valueOf(this.b));
            if (!Global.hasInit()) {
                Global.init();
            }
            m.a().b("key_current_qua", Global.getQUA());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
