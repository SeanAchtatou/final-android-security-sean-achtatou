package com.qq.AppService;

import android.content.Context;
import android.util.Log;
import com.qq.ndk.NativeFileObject;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

/* compiled from: ProGuard */
public final class w extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private volatile boolean f254a = false;
    private Context b = null;
    private ServerSocket c = null;

    public void a() {
        this.f254a = false;
        if (this.c != null) {
            try {
                this.c.close();
            } catch (Throwable th) {
                th.printStackTrace();
            }
            this.c = null;
        }
        v.a();
    }

    public w(Context context) {
        this.b = context;
    }

    public void a(Context context) {
        this.f254a = true;
        this.b = context;
        try {
            this.c = new ServerSocket();
            this.c.bind(new InetSocketAddress(14087), 25);
            this.c.setPerformancePreferences(0, 0, 3);
            this.c.setReceiveBufferSize(NativeFileObject.S_IFCHR);
            this.c.setReuseAddress(true);
        } catch (Exception e) {
            e.printStackTrace();
            this.f254a = false;
        }
        if (!this.f254a) {
            try {
                if (this.c != null) {
                    this.c.close();
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
            this.c = null;
            return;
        }
        v.f253a = true;
        start();
    }

    public void run() {
        Socket socket;
        Log.d("com.qq.connect", "startFileTCPService");
        while (this.f254a) {
            Socket socket2 = null;
            try {
                if (this.c != null) {
                    socket2 = this.c.accept();
                }
                socket = socket2;
            } catch (Exception e) {
                e.printStackTrace();
                socket = null;
            }
            if (socket != null) {
                try {
                    socket.setSendBufferSize(NativeFileObject.S_IFDIR);
                    socket.setReceiveBufferSize(NativeFileObject.S_IFDIR);
                    String inetAddress = socket.getInetAddress().toString();
                    if (AppService.g == null) {
                        if (!inetAddress.contains("127.0.0.1")) {
                            socket.close();
                        }
                    } else if (!AppService.g.contains(inetAddress) && !inetAddress.contains("127.0.0.1")) {
                        socket.close();
                    }
                    if (this.b != null) {
                        new v(this.b.getApplicationContext(), socket).start();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    Log.i("com.qq.connect.io", "accept failed return");
                    if (socket != null) {
                        try {
                            socket.close();
                            return;
                        } catch (IOException e3) {
                            e3.printStackTrace();
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
