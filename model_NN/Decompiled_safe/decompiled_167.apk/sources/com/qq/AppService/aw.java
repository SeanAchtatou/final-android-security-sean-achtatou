package com.qq.AppService;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;
import com.qq.provider.h;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

/* compiled from: ProGuard */
public class aw extends Thread {

    /* renamed from: a  reason: collision with root package name */
    Context f228a = null;
    public boolean b = false;
    private ServerSocket c = null;
    private ba d = null;
    private ba e = null;
    private ba f = null;

    public aw(Context context) {
        this.f228a = context;
    }

    public void a() {
        this.b = true;
        try {
            this.c = new ServerSocket();
            this.c.bind(new InetSocketAddress(14092), 4);
            this.c.setReuseAddress(true);
        } catch (Throwable th) {
            th.printStackTrace();
            this.c = null;
            this.b = false;
        }
        if (!this.b) {
            if (this.c != null) {
                try {
                    this.c.close();
                } catch (Throwable th2) {
                    th2.printStackTrace();
                }
                this.c = null;
            }
            Log.d("com.qq.connect", " TCP WifiService start failed");
        }
        try {
            start();
        } catch (Throwable th3) {
            th3.printStackTrace();
        }
    }

    public void b() {
        this.b = false;
        if (this.c != null) {
            try {
                this.c.close();
                this.c = null;
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        if (this.d != null) {
            this.d.a();
            this.d = null;
        }
        if (this.e != null) {
            this.e.a();
            this.e = null;
        }
        if (this.f != null) {
            this.f.a();
            this.f = null;
        }
        t.b();
        h.a(false);
        IPCService g = AppService.g();
        if (g != null && g.b != null) {
            try {
                g.b.a(2, 0, (ParcelObject) null);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void run() {
        while (this.b) {
            Socket socket = null;
            try {
                if (this.c != null) {
                    socket = this.c.accept();
                }
                if (socket != null) {
                    if (this.d == null || !this.d.c) {
                        this.d = new ba(this.f228a, socket);
                        this.d.start();
                    } else if (this.e == null || !this.e.c) {
                        this.e = new ba(this.f228a, socket);
                        this.e.start();
                    } else if (this.f == null || !this.f.c) {
                        this.f = new ba(this.f228a, socket);
                        this.f.start();
                    } else {
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e2) {
                            e2.printStackTrace();
                        }
                        Log.d("com.qq.connect", "TcpWifiAdminServer thread is full ,wait!");
                        new ba(this.f228a, socket).start();
                    }
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }
}
