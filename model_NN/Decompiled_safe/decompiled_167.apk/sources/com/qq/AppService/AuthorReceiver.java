package com.qq.AppService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.qq.b.a;
import com.tencent.assistant.utils.bg;

/* compiled from: ProGuard */
public class AuthorReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static volatile String f202a = null;
    private static volatile String b = null;
    private static volatile String c = null;
    private static volatile String d = null;

    public static void a() {
        a.c = false;
        synchronized (AuthorReceiver.class) {
            f202a = null;
        }
    }

    public static void a(int i) {
        synchronized (AuthorReceiver.class) {
            if (f202a == null && b == null) {
                try {
                    AuthorReceiver.class.wait((long) i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static synchronized String b() {
        String str;
        synchronized (AuthorReceiver.class) {
            str = f202a;
        }
        return str;
    }

    public static synchronized String c() {
        String str;
        synchronized (AuthorReceiver.class) {
            str = b;
        }
        return str;
    }

    public static synchronized void a(String str) {
        synchronized (AuthorReceiver.class) {
            b = str;
        }
    }

    public static synchronized void b(String str) {
        synchronized (AuthorReceiver.class) {
            f202a = str;
        }
    }

    public static synchronized String b(int i) {
        String str;
        synchronized (AuthorReceiver.class) {
            if (i == 0) {
                str = f202a;
            } else if (i == 1) {
                str = c;
            } else if (i == 2) {
                str = b;
            } else if (i == 3) {
                str = d;
            } else {
                str = null;
            }
        }
        return str;
    }

    public void onReceive(Context context, Intent intent) {
        if (context != null && intent != null) {
            String a2 = bg.a(intent, "rootKey");
            if (a2 != null) {
                String a3 = bg.a(intent, "version");
                if (a3 == null || !a3.equals("101")) {
                    new q(this, a2).start();
                } else {
                    a.c = true;
                    a.f261a = true;
                    synchronized (AuthorReceiver.class) {
                        f202a = a2;
                        AuthorReceiver.class.notifyAll();
                    }
                }
            }
            String a4 = bg.a(intent, "usbKey");
            if (a4 != null) {
                a.f261a = true;
                a.b = true;
                synchronized (AuthorReceiver.class) {
                    b = a4;
                    AuthorReceiver.class.notifyAll();
                }
            }
            String a5 = bg.a(intent, "systemKey");
            if (a5 != null) {
                synchronized (AuthorReceiver.class) {
                    c = a5;
                }
            }
            String a6 = bg.a(intent, "otherKey");
            if (a6 != null) {
                synchronized (AuthorReceiver.class) {
                    d = a6;
                }
            }
        }
    }
}
