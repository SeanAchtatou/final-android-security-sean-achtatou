package com.qq.h;

import android.util.Log;
import com.qq.AppService.s;
import com.qq.i.a;
import com.tencent.assistant.net.c;
import com.tencent.assistant.utils.bh;
import com.tencent.wcs.jce.ServerInfo;
import com.tencent.wcs.jce.ServerList;

/* compiled from: ProGuard */
public class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public static volatile boolean f296a = false;
    private a b = null;

    public void run() {
        ServerInfo c;
        ServerList serverList;
        f296a = true;
        while (true) {
            if (!c.d() || (c = b.a().c()) == null) {
                break;
            }
            Log.d("com.qq.connect", "looper ip --> " + c.f3883a);
            this.b = new a("http://" + c.f3883a + ":" + c.b, true, null, null, null);
            this.b.a("text/json;charset=utf-8");
            this.b.a(10000);
            this.b.b(15000);
            this.b.b();
            this.b.a(s.a(1013));
            this.b.c();
            byte[] a2 = this.b.a(true);
            if (a2 != null) {
                Log.d("com.qq.connect", "ret len: " + a2.length);
            }
            if (a2 == null || a2.length <= 4) {
                serverList = null;
            } else {
                Log.d("com.qq.connect", "response: " + s.a(a2));
                serverList = (ServerList) bh.b(s.b(a2, 4), ServerList.class);
            }
            a();
            if (serverList != null && serverList.f3884a != null && !serverList.f3884a.isEmpty()) {
                Log.d("com.qq.connect", "getIPList: success");
                b.a().a(serverList);
                break;
            }
            b.a().e();
        }
        f296a = false;
    }

    public void a() {
        if (this.b != null) {
            this.b.f();
            this.b = null;
        }
    }
}
