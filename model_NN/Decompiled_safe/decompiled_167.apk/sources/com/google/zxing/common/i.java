package com.google.zxing.common;

import com.google.zxing.j;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    private final b f162a;
    private final j[] b;

    public i(b bVar, j[] jVarArr) {
        this.f162a = bVar;
        this.b = jVarArr;
    }

    public b a() {
        return this.f162a;
    }

    public j[] b() {
        return this.b;
    }
}
