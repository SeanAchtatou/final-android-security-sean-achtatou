package com.google.zxing.c.a;

import com.google.zxing.FormatException;
import com.google.zxing.common.g;
import com.tencent.assistant.st.STConst;

final class b {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f154a = {';', '<', '>', '@', '[', '\\', '}', '_', '`', '~', '!', 13, 9, ',', ':', 10, '-', '.', '$', '/', '\"', '|', '*', '(', ')', '?', '{', '}', '\''};
    private static final char[] b = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '&', 13, 9, ',', ':', '#', '-', '.', '$', '/', '+', '%', '*', '=', '^'};
    private static final String[] c = {"000000000000000000000000000000000000000000001", "000000000000000000000000000000000000000000900", "000000000000000000000000000000000000000810000", "000000000000000000000000000000000000729000000", "000000000000000000000000000000000656100000000", "000000000000000000000000000000590490000000000", "000000000000000000000000000531441000000000000", "000000000000000000000000478296900000000000000", "000000000000000000000430467210000000000000000", "000000000000000000387420489000000000000000000", "000000000000000348678440100000000000000000000", "000000000000313810596090000000000000000000000", "000000000282429536481000000000000000000000000", "000000254186582832900000000000000000000000000", "000228767924549610000000000000000000000000000", "205891132094649000000000000000000000000000000"};

    private b() {
    }

    private static int a(int i, int[] iArr, int i2, StringBuffer stringBuffer) {
        if (i == 901) {
            int i3 = 0;
            long j = 0;
            char[] cArr = new char[6];
            int[] iArr2 = new int[6];
            boolean z = false;
            while (i2 < iArr[0] && !z) {
                int i4 = i2 + 1;
                int i5 = iArr[i2];
                if (i5 < 900) {
                    iArr2[i3] = i5;
                    i3++;
                    j = (j * 900) + ((long) i5);
                    i2 = i4;
                } else if (i5 == 900 || i5 == 901 || i5 == 902 || i5 == 924 || i5 == 928 || i5 == 923 || i5 == 922) {
                    i2 = i4 - 1;
                    z = true;
                } else {
                    i2 = i4;
                }
                if (i3 % 5 == 0 && i3 > 0) {
                    int i6 = 0;
                    while (i6 < 6) {
                        cArr[5 - i6] = (char) ((int) (j % 256));
                        i6++;
                        j >>= 8;
                    }
                    stringBuffer.append(cArr);
                    i3 = 0;
                }
            }
            for (int i7 = (i3 / 5) * 5; i7 < i3; i7++) {
                stringBuffer.append((char) iArr2[i7]);
            }
        } else if (i == 924) {
            int i8 = 0;
            long j2 = 0;
            boolean z2 = false;
            while (i2 < iArr[0] && !z2) {
                int i9 = i2 + 1;
                int i10 = iArr[i2];
                if (i10 < 900) {
                    i8++;
                    j2 = (j2 * 900) + ((long) i10);
                    r12 = i9;
                } else if (i10 == 900 || i10 == 901 || i10 == 902 || i10 == 924 || i10 == 928 || i10 == 923 || i10 == 922) {
                    r12 = i9 - 1;
                    z2 = true;
                } else {
                    r12 = i9;
                }
                if (i8 % 5 == 0 && i8 > 0) {
                    char[] cArr2 = new char[6];
                    int i11 = 0;
                    while (i11 < 6) {
                        cArr2[5 - i11] = (char) ((int) (255 & j2));
                        i11++;
                        j2 >>= 8;
                    }
                    stringBuffer.append(cArr2);
                }
            }
        }
        return i2;
    }

    private static int a(int[] iArr, int i, StringBuffer stringBuffer) {
        int[] iArr2 = new int[(iArr[0] << 1)];
        int[] iArr3 = new int[(iArr[0] << 1)];
        boolean z = false;
        int i2 = 0;
        while (i < iArr[0] && !z) {
            int i3 = i + 1;
            int i4 = iArr[i];
            if (i4 < 900) {
                iArr2[i2] = i4 / 30;
                iArr2[i2 + 1] = i4 % 30;
                i2 += 2;
                i = i3;
            } else {
                switch (i4) {
                    case 900:
                        i = i3 - 1;
                        z = true;
                        continue;
                    case 901:
                        i = i3 - 1;
                        z = true;
                        continue;
                    case 902:
                        i = i3 - 1;
                        z = true;
                        continue;
                    case 913:
                        iArr2[i2] = 913;
                        iArr3[i2] = i4;
                        i2++;
                        i = i3;
                        continue;
                    case 924:
                        i = i3 - 1;
                        z = true;
                        continue;
                    default:
                        i = i3;
                        continue;
                }
            }
        }
        a(iArr2, iArr3, i2, stringBuffer);
        return i;
    }

    static g a(int[] iArr) {
        int a2;
        StringBuffer stringBuffer = new StringBuffer(100);
        int i = 2;
        int i2 = iArr[1];
        while (i < iArr[0]) {
            switch (i2) {
                case 900:
                    a2 = a(iArr, i, stringBuffer);
                    break;
                case 901:
                    a2 = a(i2, iArr, i, stringBuffer);
                    break;
                case 902:
                    a2 = b(iArr, i, stringBuffer);
                    break;
                case 913:
                    a2 = a(i2, iArr, i, stringBuffer);
                    break;
                case 924:
                    a2 = a(i2, iArr, i, stringBuffer);
                    break;
                default:
                    a2 = a(iArr, i - 1, stringBuffer);
                    break;
            }
            if (a2 < iArr.length) {
                i = a2 + 1;
                i2 = iArr[a2];
            } else {
                throw FormatException.a();
            }
        }
        return new g(null, stringBuffer.toString(), null, null);
    }

    private static String a(int[] iArr, int i) {
        String str;
        int i2 = 0;
        StringBuffer stringBuffer = null;
        while (i2 < i) {
            StringBuffer a2 = a(c[(i - i2) - 1], iArr[i2]);
            if (stringBuffer != null) {
                a2 = a(stringBuffer.toString(), a2.toString());
            }
            i2++;
            stringBuffer = a2;
        }
        int i3 = 0;
        while (true) {
            if (i3 >= stringBuffer.length()) {
                str = null;
                break;
            } else if (stringBuffer.charAt(i3) == '1') {
                str = stringBuffer.toString().substring(i3 + 1);
                break;
            } else {
                i3++;
            }
        }
        return str == null ? stringBuffer.toString() : str;
    }

    private static StringBuffer a(String str, int i) {
        StringBuffer stringBuffer = new StringBuffer(str.length());
        for (int i2 = 0; i2 < str.length(); i2++) {
            stringBuffer.append('0');
        }
        int i3 = i / 100;
        int i4 = (i / 10) % 10;
        int i5 = i % 10;
        StringBuffer stringBuffer2 = stringBuffer;
        int i6 = 0;
        while (i6 < i5) {
            i6++;
            stringBuffer2 = a(stringBuffer2.toString(), str);
        }
        int i7 = 0;
        while (i7 < i4) {
            i7++;
            stringBuffer2 = a(stringBuffer2.toString(), new StringBuffer().append(str).append('0').toString().substring(1));
        }
        for (int i8 = 0; i8 < i3; i8++) {
            stringBuffer2 = a(stringBuffer2.toString(), new StringBuffer().append(str).append(STConst.ST_STATUS_DEFAULT).toString().substring(2));
        }
        return stringBuffer2;
    }

    private static StringBuffer a(String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer(5);
        StringBuffer stringBuffer2 = new StringBuffer(5);
        StringBuffer stringBuffer3 = new StringBuffer(str.length());
        for (int i = 0; i < str.length(); i++) {
            stringBuffer3.append('0');
        }
        int i2 = 0;
        for (int length = str.length() - 3; length > -1; length -= 3) {
            stringBuffer.setLength(0);
            stringBuffer.append(str.charAt(length));
            stringBuffer.append(str.charAt(length + 1));
            stringBuffer.append(str.charAt(length + 2));
            stringBuffer2.setLength(0);
            stringBuffer2.append(str2.charAt(length));
            stringBuffer2.append(str2.charAt(length + 1));
            stringBuffer2.append(str2.charAt(length + 2));
            int parseInt = Integer.parseInt(stringBuffer.toString());
            int parseInt2 = Integer.parseInt(stringBuffer2.toString());
            int i3 = ((parseInt + parseInt2) + i2) % 1000;
            i2 = (i2 + (parseInt + parseInt2)) / 1000;
            stringBuffer3.setCharAt(length + 2, (char) ((i3 % 10) + 48));
            stringBuffer3.setCharAt(length + 1, (char) (((i3 / 10) % 10) + 48));
            stringBuffer3.setCharAt(length, (char) ((i3 / 100) + 48));
        }
        return stringBuffer3;
    }

    /* JADX INFO: additional move instructions added (13) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v2 */
    /* JADX WARN: Type inference failed for: r3v3 */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r3v7 */
    /* JADX WARN: Type inference failed for: r3v8 */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARN: Type inference failed for: r3v10 */
    /* JADX WARN: Type inference failed for: r3v11 */
    /* JADX WARN: Type inference failed for: r3v12 */
    /* JADX WARN: Type inference failed for: r3v13 */
    /* JADX WARN: Type inference failed for: r3v14 */
    /* JADX WARN: Type inference failed for: r3v15 */
    /* JADX WARN: Type inference failed for: r3v16 */
    /* JADX WARN: Type inference failed for: r3v17 */
    /* JADX WARN: Type inference failed for: r3v18 */
    /* JADX WARN: Type inference failed for: r3v19 */
    /* JADX WARN: Type inference failed for: r3v20 */
    /* JADX WARN: Type inference failed for: r3v21 */
    /* JADX WARN: Type inference failed for: r3v22 */
    /* JADX WARN: Type inference failed for: r3v23 */
    /* JADX WARN: Type inference failed for: r3v24 */
    /* JADX WARN: Type inference failed for: r3v25 */
    /* JADX WARN: Type inference failed for: r3v26 */
    /* JADX WARN: Type inference failed for: r3v27 */
    /* JADX WARN: Type inference failed for: r3v28 */
    /* JADX WARN: Type inference failed for: r3v29 */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(int[] r11, int[] r12, int r13, java.lang.StringBuffer r14) {
        /*
            r9 = 913(0x391, float:1.28E-42)
            r8 = 28
            r7 = 26
            r6 = 29
            r1 = 0
            r4 = r1
            r2 = r1
            r3 = r1
        L_0x000c:
            if (r4 >= r13) goto L_0x00c8
            r0 = r11[r4]
            switch(r3) {
                case 0: goto L_0x001d;
                case 1: goto L_0x0046;
                case 2: goto L_0x006d;
                case 3: goto L_0x00a0;
                case 4: goto L_0x00b9;
                default: goto L_0x0013;
            }
        L_0x0013:
            r0 = r1
        L_0x0014:
            if (r0 == 0) goto L_0x0019
            r14.append(r0)
        L_0x0019:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x000c
        L_0x001d:
            if (r0 >= r7) goto L_0x0023
            int r0 = r0 + 65
            char r0 = (char) r0
            goto L_0x0014
        L_0x0023:
            if (r0 != r7) goto L_0x0028
            r0 = 32
            goto L_0x0014
        L_0x0028:
            r5 = 27
            if (r0 != r5) goto L_0x002f
            r3 = 1
            r0 = r1
            goto L_0x0014
        L_0x002f:
            if (r0 != r8) goto L_0x0034
            r3 = 2
            r0 = r1
            goto L_0x0014
        L_0x0034:
            if (r0 != r6) goto L_0x003c
            r2 = 4
            r0 = r1
            r10 = r3
            r3 = r2
            r2 = r10
            goto L_0x0014
        L_0x003c:
            if (r0 != r9) goto L_0x0013
            r0 = r12[r4]
            char r0 = (char) r0
            r14.append(r0)
            r0 = r1
            goto L_0x0014
        L_0x0046:
            if (r0 >= r7) goto L_0x004c
            int r0 = r0 + 97
            char r0 = (char) r0
            goto L_0x0014
        L_0x004c:
            if (r0 != r7) goto L_0x0051
            r0 = 32
            goto L_0x0014
        L_0x0051:
            if (r0 != r8) goto L_0x0056
            r0 = r1
            r3 = r1
            goto L_0x0014
        L_0x0056:
            if (r0 != r8) goto L_0x005b
            r3 = 2
            r0 = r1
            goto L_0x0014
        L_0x005b:
            if (r0 != r6) goto L_0x0063
            r2 = 4
            r0 = r1
            r10 = r3
            r3 = r2
            r2 = r10
            goto L_0x0014
        L_0x0063:
            if (r0 != r9) goto L_0x0013
            r0 = r12[r4]
            char r0 = (char) r0
            r14.append(r0)
            r0 = r1
            goto L_0x0014
        L_0x006d:
            r5 = 25
            if (r0 >= r5) goto L_0x0076
            char[] r5 = com.google.zxing.c.a.b.b
            char r0 = r5[r0]
            goto L_0x0014
        L_0x0076:
            r5 = 25
            if (r0 != r5) goto L_0x007d
            r3 = 3
            r0 = r1
            goto L_0x0014
        L_0x007d:
            if (r0 != r7) goto L_0x0082
            r0 = 32
            goto L_0x0014
        L_0x0082:
            r5 = 27
            if (r0 != r5) goto L_0x0088
            r0 = r1
            goto L_0x0014
        L_0x0088:
            if (r0 != r8) goto L_0x008d
            r0 = r1
            r3 = r1
            goto L_0x0014
        L_0x008d:
            if (r0 != r6) goto L_0x0095
            r2 = 4
            r0 = r1
            r10 = r3
            r3 = r2
            r2 = r10
            goto L_0x0014
        L_0x0095:
            if (r0 != r9) goto L_0x0013
            r0 = r12[r4]
            char r0 = (char) r0
            r14.append(r0)
            r0 = r1
            goto L_0x0014
        L_0x00a0:
            if (r0 >= r6) goto L_0x00a8
            char[] r5 = com.google.zxing.c.a.b.f154a
            char r0 = r5[r0]
            goto L_0x0014
        L_0x00a8:
            if (r0 != r6) goto L_0x00ae
            r0 = r1
            r3 = r1
            goto L_0x0014
        L_0x00ae:
            if (r0 != r9) goto L_0x0013
            r0 = r12[r4]
            char r0 = (char) r0
            r14.append(r0)
            r0 = r1
            goto L_0x0014
        L_0x00b9:
            if (r0 >= r6) goto L_0x00c2
            char[] r3 = com.google.zxing.c.a.b.f154a
            char r0 = r3[r0]
            r3 = r2
            goto L_0x0014
        L_0x00c2:
            if (r0 != r6) goto L_0x00c9
            r0 = r1
            r3 = r1
            goto L_0x0014
        L_0x00c8:
            return
        L_0x00c9:
            r0 = r1
            r3 = r2
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.c.a.b.a(int[], int[], int, java.lang.StringBuffer):void");
    }

    private static int b(int[] iArr, int i, StringBuffer stringBuffer) {
        int[] iArr2 = new int[15];
        boolean z = false;
        int i2 = 0;
        while (i < iArr[0] && !z) {
            int i3 = i + 1;
            int i4 = iArr[i];
            if (i3 == iArr[0]) {
                z = true;
            }
            if (i4 < 900) {
                iArr2[i2] = i4;
                i2++;
                i = i3;
            } else if (i4 == 900 || i4 == 901 || i4 == 924 || i4 == 928 || i4 == 923 || i4 == 922) {
                i = i3 - 1;
                z = true;
            } else {
                i = i3;
            }
            if (i2 % 15 == 0 || i4 == 902 || z) {
                stringBuffer.append(a(iArr2, i2));
                i2 = 0;
            }
        }
        return i;
    }
}
