package com.google.zxing.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.c;
import com.google.zxing.common.a;
import com.google.zxing.d;
import com.google.zxing.g;
import com.google.zxing.h;
import com.google.zxing.i;
import com.google.zxing.j;
import java.util.Hashtable;

public abstract class k implements g {
    protected static int a(int[] iArr, int[] iArr2, int i) {
        int length = iArr.length;
        int i2 = 0;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            i3 += iArr[i4];
            i2 += iArr2[i4];
        }
        if (i3 < i2) {
            return Integer.MAX_VALUE;
        }
        int i5 = (i3 << 8) / i2;
        int i6 = (i * i5) >> 8;
        int i7 = 0;
        for (int i8 = 0; i8 < length; i8++) {
            int i9 = iArr[i8] << 8;
            int i10 = iArr2[i8] * i5;
            int i11 = i9 > i10 ? i9 - i10 : i10 - i9;
            if (i11 > i6) {
                return Integer.MAX_VALUE;
            }
            i7 += i11;
        }
        return i7 / i3;
    }

    protected static void a(a aVar, int i, int[] iArr) {
        int i2;
        boolean z;
        int length = iArr.length;
        for (int i3 = 0; i3 < length; i3++) {
            iArr[i3] = 0;
        }
        int a2 = aVar.a();
        if (i >= a2) {
            throw NotFoundException.a();
        }
        boolean z2 = !aVar.a(i);
        int i4 = 0;
        while (true) {
            if (i >= a2) {
                i2 = i4;
                break;
            }
            if (aVar.a(i) ^ z2) {
                iArr[i4] = iArr[i4] + 1;
                z = z2;
            } else {
                i2 = i4 + 1;
                if (i2 == length) {
                    break;
                }
                iArr[i2] = 1;
                int i5 = i2;
                z = !z2;
                i4 = i5;
            }
            i++;
            z2 = z;
        }
        if (i2 == length) {
            return;
        }
        if (i2 != length - 1 || i != a2) {
            throw NotFoundException.a();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.zxing.h b(com.google.zxing.c r18, java.util.Hashtable r19) {
        /*
            r17 = this;
            int r8 = r18.a()
            int r2 = r18.b()
            com.google.zxing.common.a r4 = new com.google.zxing.common.a
            r4.<init>(r8)
            int r9 = r2 >> 1
            if (r19 == 0) goto L_0x0049
            com.google.zxing.d r1 = com.google.zxing.d.d
            r0 = r19
            boolean r1 = r0.containsKey(r1)
            if (r1 == 0) goto L_0x0049
            r1 = 1
            r3 = r1
        L_0x001d:
            r5 = 1
            if (r3 == 0) goto L_0x004c
            r1 = 8
        L_0x0022:
            int r1 = r2 >> r1
            int r10 = java.lang.Math.max(r5, r1)
            if (r3 == 0) goto L_0x004e
            r1 = r2
        L_0x002b:
            r3 = 0
            r7 = r3
            r3 = r4
            r4 = r19
        L_0x0030:
            if (r7 >= r1) goto L_0x0044
            int r5 = r7 + 1
            int r5 = r5 >> 1
            r6 = r7 & 1
            if (r6 != 0) goto L_0x0051
            r6 = 1
        L_0x003b:
            if (r6 == 0) goto L_0x0053
        L_0x003d:
            int r5 = r5 * r10
            int r11 = r9 + r5
            if (r11 < 0) goto L_0x0044
            if (r11 < r2) goto L_0x0055
        L_0x0044:
            com.google.zxing.NotFoundException r1 = com.google.zxing.NotFoundException.a()
            throw r1
        L_0x0049:
            r1 = 0
            r3 = r1
            goto L_0x001d
        L_0x004c:
            r1 = 5
            goto L_0x0022
        L_0x004e:
            r1 = 15
            goto L_0x002b
        L_0x0051:
            r6 = 0
            goto L_0x003b
        L_0x0053:
            int r5 = -r5
            goto L_0x003d
        L_0x0055:
            r0 = r18
            com.google.zxing.common.a r3 = r0.a(r11, r3)     // Catch:{ NotFoundException -> 0x00f2 }
            r5 = 0
            r6 = r5
        L_0x005d:
            r5 = 2
            if (r6 >= r5) goto L_0x00f3
            r5 = 1
            if (r6 != r5) goto L_0x0094
            r3.c()
            if (r4 == 0) goto L_0x0094
            com.google.zxing.d r5 = com.google.zxing.d.h
            boolean r5 = r4.containsKey(r5)
            if (r5 == 0) goto L_0x0094
            java.util.Hashtable r5 = new java.util.Hashtable
            r5.<init>()
            java.util.Enumeration r12 = r4.keys()
        L_0x0079:
            boolean r13 = r12.hasMoreElements()
            if (r13 == 0) goto L_0x0093
            java.lang.Object r13 = r12.nextElement()
            com.google.zxing.d r14 = com.google.zxing.d.h
            boolean r14 = r13.equals(r14)
            if (r14 != 0) goto L_0x0079
            java.lang.Object r14 = r4.get(r13)
            r5.put(r13, r14)
            goto L_0x0079
        L_0x0093:
            r4 = r5
        L_0x0094:
            r0 = r17
            com.google.zxing.h r5 = r0.a(r11, r3, r4)     // Catch:{ ReaderException -> 0x00ec }
            r12 = 1
            if (r6 != r12) goto L_0x00eb
            com.google.zxing.i r12 = com.google.zxing.i.b     // Catch:{ ReaderException -> 0x00ec }
            java.lang.Integer r13 = new java.lang.Integer     // Catch:{ ReaderException -> 0x00ec }
            r14 = 180(0xb4, float:2.52E-43)
            r13.<init>(r14)     // Catch:{ ReaderException -> 0x00ec }
            r5.a(r12, r13)     // Catch:{ ReaderException -> 0x00ec }
            com.google.zxing.j[] r12 = r5.b()     // Catch:{ ReaderException -> 0x00ec }
            r13 = 0
            com.google.zxing.j r14 = new com.google.zxing.j     // Catch:{ ReaderException -> 0x00ec }
            float r15 = (float) r8     // Catch:{ ReaderException -> 0x00ec }
            r16 = 0
            r16 = r12[r16]     // Catch:{ ReaderException -> 0x00ec }
            float r16 = r16.a()     // Catch:{ ReaderException -> 0x00ec }
            float r15 = r15 - r16
            r16 = 1065353216(0x3f800000, float:1.0)
            float r15 = r15 - r16
            r16 = 0
            r16 = r12[r16]     // Catch:{ ReaderException -> 0x00ec }
            float r16 = r16.b()     // Catch:{ ReaderException -> 0x00ec }
            r14.<init>(r15, r16)     // Catch:{ ReaderException -> 0x00ec }
            r12[r13] = r14     // Catch:{ ReaderException -> 0x00ec }
            r13 = 1
            com.google.zxing.j r14 = new com.google.zxing.j     // Catch:{ ReaderException -> 0x00ec }
            float r15 = (float) r8     // Catch:{ ReaderException -> 0x00ec }
            r16 = 1
            r16 = r12[r16]     // Catch:{ ReaderException -> 0x00ec }
            float r16 = r16.a()     // Catch:{ ReaderException -> 0x00ec }
            float r15 = r15 - r16
            r16 = 1065353216(0x3f800000, float:1.0)
            float r15 = r15 - r16
            r16 = 1
            r16 = r12[r16]     // Catch:{ ReaderException -> 0x00ec }
            float r16 = r16.b()     // Catch:{ ReaderException -> 0x00ec }
            r14.<init>(r15, r16)     // Catch:{ ReaderException -> 0x00ec }
            r12[r13] = r14     // Catch:{ ReaderException -> 0x00ec }
        L_0x00eb:
            return r5
        L_0x00ec:
            r5 = move-exception
            int r5 = r6 + 1
            r6 = r5
            goto L_0x005d
        L_0x00f2:
            r5 = move-exception
        L_0x00f3:
            int r5 = r7 + 1
            r7 = r5
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.b.k.b(com.google.zxing.c, java.util.Hashtable):com.google.zxing.h");
    }

    protected static void b(a aVar, int i, int[] iArr) {
        int length = iArr.length;
        boolean a2 = aVar.a(i);
        while (i > 0 && length >= 0) {
            i--;
            if (aVar.a(i) != a2) {
                length--;
                a2 = !a2;
            }
        }
        if (length >= 0) {
            throw NotFoundException.a();
        }
        a(aVar, i + 1, iArr);
    }

    public abstract h a(int i, a aVar, Hashtable hashtable);

    public h a(c cVar, Hashtable hashtable) {
        try {
            return b(cVar, hashtable);
        } catch (NotFoundException e) {
            if (!(hashtable != null && hashtable.containsKey(d.d)) || !cVar.d()) {
                throw e;
            }
            c e2 = cVar.e();
            h b = b(e2, hashtable);
            Hashtable d = b.d();
            b.a(i.b, new Integer((d == null || !d.containsKey(i.b)) ? 270 : (((Integer) d.get(i.b)).intValue() + 270) % 360));
            j[] b2 = b.b();
            int b3 = e2.b();
            for (int i = 0; i < b2.length; i++) {
                b2[i] = new j((((float) b3) - b2[i].b()) - 1.0f, b2[i].a());
            }
            return b;
        }
    }

    public void a() {
    }
}
