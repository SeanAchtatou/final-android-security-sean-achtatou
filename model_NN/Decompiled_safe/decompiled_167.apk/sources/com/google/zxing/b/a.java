package com.google.zxing.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.h;
import com.google.zxing.j;
import java.util.Hashtable;

public final class a extends k {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f126a = "0123456789-$:/.+ABCDTN".toCharArray();
    private static final int[] b = {3, 6, 9, 96, 18, 66, 33, 36, 48, 72, 12, 24, 37, 81, 84, 21, 26, 41, 11, 14, 26, 41};
    private static final char[] c = {'E', '*', 'A', 'B', 'C', 'D', 'T', 'N'};

    private static char a(int[] iArr) {
        int length = iArr.length;
        int i = Integer.MAX_VALUE;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            if (iArr[i3] < i) {
                i = iArr[i3];
            }
            if (iArr[i3] > i2) {
                i2 = iArr[i3];
            }
        }
        do {
            int i4 = 0;
            int i5 = 0;
            for (int i6 = 0; i6 < length; i6++) {
                if (iArr[i6] > i2) {
                    i4 |= 1 << ((length - 1) - i6);
                    i5++;
                }
            }
            if (i5 == 2 || i5 == 3) {
                for (int i7 = 0; i7 < b.length; i7++) {
                    if (b[i7] == i4) {
                        return f126a[i7];
                    }
                }
            }
            i2--;
        } while (i2 > i);
        return '!';
    }

    private static boolean a(char[] cArr, char c2) {
        if (cArr == null) {
            return false;
        }
        for (char c3 : cArr) {
            if (c3 == c2) {
                return true;
            }
        }
        return false;
    }

    private static int[] a(com.google.zxing.common.a aVar) {
        int i;
        int a2 = aVar.a();
        int i2 = 0;
        while (i2 < a2 && !aVar.a(i2)) {
            i2++;
        }
        int[] iArr = new int[7];
        int length = iArr.length;
        int i3 = i2;
        boolean z = false;
        int i4 = 0;
        while (i3 < a2) {
            if (aVar.a(i3) ^ z) {
                iArr[i] = iArr[i] + 1;
            } else {
                if (i == length - 1) {
                    try {
                        if (a(c, a(iArr)) && aVar.a(Math.max(0, i2 - ((i3 - i2) / 2)), i2, false)) {
                            return new int[]{i2, i3};
                        }
                    } catch (IllegalArgumentException e) {
                    }
                    i2 += iArr[0] + iArr[1];
                    for (int i5 = 2; i5 < length; i5++) {
                        iArr[i5 - 2] = iArr[i5];
                    }
                    iArr[length - 2] = 0;
                    iArr[length - 1] = 0;
                    i--;
                } else {
                    i++;
                }
                iArr[i] = 1;
                z = !z;
            }
            boolean z2 = z;
            i3++;
            i2 = i2;
            i4 = i;
            z = z2;
        }
        throw NotFoundException.a();
    }

    public h a(int i, com.google.zxing.common.a aVar, Hashtable hashtable) {
        int[] a2 = a(aVar);
        a2[1] = 0;
        int i2 = a2[1];
        int a3 = aVar.a();
        while (i2 < a3 && !aVar.a(i2)) {
            i2++;
        }
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            int[] iArr = {0, 0, 0, 0, 0, 0, 0};
            a(aVar, i2, iArr);
            char a4 = a(iArr);
            if (a4 == '!') {
                throw NotFoundException.a();
            }
            stringBuffer.append(a4);
            int i3 = i2;
            for (int i4 : iArr) {
                i3 += i4;
            }
            int i5 = i3;
            while (i5 < a3 && !aVar.a(i5)) {
                i5++;
            }
            if (i5 >= a3) {
                int i6 = 0;
                for (int i7 : iArr) {
                    i6 += i7;
                }
                int i8 = (i5 - i2) - i6;
                if (i5 != a3 && i8 / 2 < i6) {
                    throw NotFoundException.a();
                } else if (stringBuffer.length() < 2) {
                    throw NotFoundException.a();
                } else {
                    char charAt = stringBuffer.charAt(0);
                    if (!a(c, charAt)) {
                        throw NotFoundException.a();
                    }
                    int i9 = 1;
                    while (i9 < stringBuffer.length()) {
                        if (stringBuffer.charAt(i9) == charAt && i9 + 1 != stringBuffer.length()) {
                            stringBuffer.delete(i9 + 1, stringBuffer.length() - 1);
                            i9 = stringBuffer.length();
                        }
                        i9++;
                    }
                    if (stringBuffer.length() > 6) {
                        stringBuffer.deleteCharAt(stringBuffer.length() - 1);
                        stringBuffer.deleteCharAt(0);
                        return new h(stringBuffer.toString(), null, new j[]{new j(((float) (a2[1] + a2[0])) / 2.0f, (float) i), new j(((float) (i2 + i5)) / 2.0f, (float) i)}, com.google.zxing.a.k);
                    }
                    throw NotFoundException.a();
                }
            } else {
                i2 = i5;
            }
        }
    }
}
