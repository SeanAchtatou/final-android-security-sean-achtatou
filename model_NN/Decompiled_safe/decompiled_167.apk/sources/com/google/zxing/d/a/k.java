package com.google.zxing.d.a;

class k extends c {
    private k() {
        super(null);
    }

    k(d dVar) {
        this();
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, int i2) {
        int i3 = i * i2;
        return (((i3 % 3) + (i3 & 1)) & 1) == 0;
    }
}
