package defpackage;

import android.os.Bundle;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

/* renamed from: ac  reason: default package */
public class ac {

    /* renamed from: a  reason: collision with root package name */
    protected HttpParams f5a = null;
    protected a b = null;
    private boolean c = false;
    private String d = null;
    private int e = 0;

    /* renamed from: ac$a */
    public interface a {
        void a(Bundle bundle);

        void b(Bundle bundle);
    }

    /* access modifiers changed from: protected */
    public HttpClient a() {
        if (this.f5a == null) {
            this.f5a = new BasicHttpParams();
        }
        HttpConnectionParams.setConnectionTimeout(this.f5a, 10000);
        HttpConnectionParams.setSoTimeout(this.f5a, 20000);
        HttpConnectionParams.setSocketBufferSize(this.f5a, 4096);
        HttpClientParams.setRedirecting(this.f5a, true);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(this.f5a);
        if (this.c) {
            defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(this.d, this.e));
        }
        return defaultHttpClient;
    }

    /* access modifiers changed from: protected */
    public void a(int i, Bundle bundle) {
        if (this.b == null) {
            return;
        }
        if (i == 1) {
            this.b.b(bundle);
        } else if (i == 2) {
            this.b.a(bundle);
        }
    }

    public void a(a aVar) {
        this.b = aVar;
    }

    public void a(String str, int i) {
        this.d = str;
        this.e = i;
    }

    public void a(boolean z) {
        this.c = z;
    }
}
