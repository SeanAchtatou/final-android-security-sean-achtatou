package defpackage;

import com.tencent.securemodule.impl.SecureService;
import com.tencent.securemodule.service.ICallback;

/* renamed from: ab  reason: default package */
public class ab implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f4a;
    final /* synthetic */ ICallback b;
    final /* synthetic */ SecureService.a c;

    public ab(SecureService.a aVar, String str, ICallback iCallback) {
        this.c = aVar;
        this.f4a = str;
        this.b = iCallback;
    }

    public void run() {
        int a2 = SecureService.this.a(this.f4a);
        if (this.b != null) {
            this.b.onTaskFinished(a2);
        }
        SecureService.this.stopSelf();
        SecureService.this.c();
    }
}
