package defpackage;

import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;
import com.tencent.connect.common.Constants;

/* renamed from: a  reason: default package */
public final class a extends JceStruct implements Cloneable {
    static final /* synthetic */ boolean b = (!a.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    public String f2a = Constants.STR_EMPTY;

    public a() {
        a(this.f2a);
    }

    public void a(String str) {
        this.f2a = str;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            if (b) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void display(StringBuilder sb, int i) {
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        return JceUtil.equals(this.f2a, ((a) obj).f2a);
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void readFrom(JceInputStream jceInputStream) {
        a(jceInputStream.readString(0, true));
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.f2a, 0);
    }
}
