package defpackage;

import android.os.Message;
import com.tencent.securemodule.service.ICallback;
import com.tencent.securemodule.ui.TransparentActivity;

/* renamed from: aq  reason: default package */
public class aq implements ICallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TransparentActivity.a f106a;

    public aq(TransparentActivity.a aVar) {
        this.f106a = aVar;
    }

    public void onTaskFinished(int i) {
        if (i == 0) {
            Message obtainMessage = TransparentActivity.this.l.obtainMessage(2);
            obtainMessage.arg1 = 1;
            TransparentActivity.this.l.sendMessage(obtainMessage);
            return;
        }
        this.f106a.a();
    }
}
