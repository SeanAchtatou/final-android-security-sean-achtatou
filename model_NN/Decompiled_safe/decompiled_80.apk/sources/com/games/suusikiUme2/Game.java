package com.games.suusikiUme2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.admob.android.ads.AdListener;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;
import jp.co.microad.smartphone.sdk.MicroAdLayout;

public class Game extends Activity implements View.OnClickListener {
    private static final int ADDITION = 1;
    private static final int DIFFICULTY_EASY = 0;
    private static final int DIFFICULTY_HARD = 2;
    private static final int DIFFICULTY_MEDIUM = 1;
    private static final int DIFFICULTY_SURVIVAL = 3;
    private static final int DIVISION = 4;
    private static final int EASY_MAX_OPERATERS = 2;
    private static final int EASY_MAX_PROBLEMS = 10;
    private static final int HARD_MAX_OPERATERS = 4;
    private static final int HARD_MAX_PROBLEMS = 20;
    static final String KEY_DIFFICULTY = "com.games.suusikiUme2.difficulty";
    static final String KEY_GAME_MODE = "com.games.suusikiUme2.gameMode";
    private static final int MEDIUM_MAX_OPERATERS = 3;
    private static final int MEDIUM_MAX_PROBLEMS = 15;
    private static final int MODE_GAME = 0;
    private static final int MODE_RECORD = 2;
    private static final int MODE_SURVIVAL = 1;
    private static final int MULTIPLICATION = 3;
    public static final String[] SCORE = {"q1", "w1", "e1", "r1", "t1", "y1", "u1", "i1", "o1", "p1", "a1", "s1", "d1", "f1", "g1", "h1", "j1", "k1", "l1", "z1", "x1", "c1", "v1", "b1", "n1", "m1", "qq1", "qw1", "qe1", "qr1", "qt1", "qy1", "qu1", "qi1", "qo1", "qp1", "qa1", "qs1", "qd1", "qf1"};
    private static final int SUBTRACTION = 2;
    private static final String TAG = "Game";
    public static final String[] TIME = {"q2", "w2", "e2", "r2", "t2", "y2", "u2", "i2", "o2", "p2", "a2", "s2", "d2", "f2", "g2", "h2", "j2", "k2", "l2", "z2", "x2", "c2", "v2", "b2", "n2", "m2", "qq2", "qw2", "qe2", "qr2", "qt2", "qy2", "qu2", "qi2", "qo2", "qp2", "qa2", "qs2", "qd2", "qf2"};
    private static final int VERY_EASY_MAX_OPERATERS = 2;
    private static final int VERY_EASY_MAX_PROBLEMS = 10;
    private static final int VERY_HARD_MAX_OPERATERS = 4;
    private static final int VERY_HARD_MAX_PROBLEMS = 20;
    private static int diff;
    private static int mode;
    private static boolean playNow = false;
    private static Problem problem;
    private static boolean suv;
    private TextView[] ScoreView = new TextView[40];
    private TextView[] TimeView = new TextView[40];
    private int answerCount = 0;
    private TextView answerTextView;
    private Chronometer chronometer;
    private int correct = 0;
    private TextView counterTextView;
    private int elapsedTime = 0;
    private int maxOperaters = 2;
    private int maxProblems = 10;
    private int problemCount = 0;
    private TextView problemTextView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        mode = getIntent().getIntExtra(KEY_GAME_MODE, 0);
        diff = getIntent().getIntExtra(KEY_DIFFICULTY, 1);
        playNow = false;
        setXml();
        setDifficulty();
    }

    private void setDifficulty() {
        switch (diff) {
            case 0:
                this.maxProblems = 10;
                this.maxOperaters = 2;
                return;
            case 1:
                this.maxProblems = 15;
                this.maxOperaters = 3;
                return;
            case 2:
                this.maxProblems = 20;
                this.maxOperaters = 4;
                return;
            case 3:
                this.maxProblems = 99;
                this.maxOperaters = 3;
                suv = false;
                return;
            default:
                return;
        }
    }

    private void setXml() {
        if (mode == 0 || mode == 1) {
            setContentView((int) R.layout.game);
            ((MicroAdLayout) findViewById(R.id.adview)).init(this);
            if (diff == 0) {
                setTitle("Quick Calculator(Easy)");
            } else if (diff == 1) {
                setTitle("Quick Calculator(Normal)");
            } else if (diff == 2) {
                setTitle("Quick Calculator(Hard)");
            }
            if (mode == 1) {
                setTitle("Quick Calculator(Survival)");
            }
            this.chronometer = (Chronometer) findViewById(R.id.chronometer);
            this.chronometer.setFormat("Elapsed time, %s");
            this.counterTextView = (TextView) findViewById(R.id.caunter);
            this.answerTextView = (TextView) findViewById(R.id.answer);
            this.problemTextView = (TextView) findViewById(R.id.problem);
            findViewById(R.id.div_Button).setOnClickListener(this);
            findViewById(R.id.mul_Button).setOnClickListener(this);
            findViewById(R.id.sub_Button).setOnClickListener(this);
            findViewById(R.id.add_Button).setOnClickListener(this);
            findViewById(R.id.start_Button).setOnClickListener(this);
            findViewById(R.id.return_title_button).setOnClickListener(this);
        } else if (mode == 2) {
            setContentView((int) R.layout.records);
            ((MicroAdLayout) findViewById(R.id.adview)).init(this);
            setScore();
            findViewById(R.id.easy_button).setOnClickListener(this);
            findViewById(R.id.normal_button).setOnClickListener(this);
            findViewById(R.id.hard_button).setOnClickListener(this);
            findViewById(R.id.survival_button).setOnClickListener(this);
            findViewById(R.id.clear_button).setOnClickListener(this);
            findViewById(R.id.return_title_button).setOnClickListener(this);
        }
    }

    public void onClick(View v) {
        if (mode == 2) {
            switch (v.getId()) {
                case R.id.return_title_button:
                    finish();
                    return;
                case R.id.survival_button:
                    diff = 3;
                    setScore();
                    return;
                case R.id.easy_button:
                    diff = 0;
                    setScore();
                    return;
                case R.id.normal_button:
                    diff = 1;
                    setScore();
                    return;
                case R.id.hard_button:
                    diff = 2;
                    setScore();
                    return;
                case R.id.clear_button:
                    openClearAlert();
                    return;
                default:
                    return;
            }
        } else if (mode == 0 || mode == 1) {
            switch (v.getId()) {
                case R.id.add_Button:
                    if (playNow) {
                        chekHideOperater(1);
                        return;
                    }
                    return;
                case R.id.sub_Button:
                    if (playNow) {
                        chekHideOperater(2);
                        return;
                    }
                    return;
                case R.id.mul_Button:
                    if (playNow) {
                        chekHideOperater(3);
                        return;
                    }
                    return;
                case R.id.div_Button:
                    if (playNow) {
                        chekHideOperater(4);
                        return;
                    }
                    return;
                case R.id.adview:
                default:
                    return;
                case R.id.start_Button:
                    View startButton = findViewById(R.id.start_Button);
                    startButton.setOnClickListener(this);
                    if (!playNow) {
                        playNow = true;
                        startChronometer(this.chronometer);
                        ((TextView) startButton).setText("More");
                        suv = false;
                        startGames(diff);
                        return;
                    }
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://androida.me")));
                    return;
                case R.id.return_title_button:
                    finish();
                    return;
            }
        }
    }

    private void openClearAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("    Clear    ");
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Game.this.clearRecords();
            }
        });
        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.create().show();
    }

    private void chekHideOperater(int i) {
        if (this.correct == i) {
            this.answerCount++;
            this.answerTextView.setText("correct ○");
        } else {
            this.answerTextView.setText("wrong ×");
            if (mode == 1) {
                suv = true;
            }
        }
        this.correct = 0;
        startGames(diff);
    }

    private void startGames(int diff2) {
        this.problemTextView.setText(" ");
        this.counterTextView.setText(String.valueOf(this.answerCount) + "/" + this.maxProblems);
        if ((mode != 0 || this.problemCount >= this.maxProblems) && (mode != 1 || suv || this.problemCount >= this.maxProblems)) {
            playNow = false;
            this.elapsedTime = stopChronometer(this.chronometer);
            View startButton = findViewById(R.id.start_Button);
            startButton.setOnClickListener(this);
            ((TextView) startButton).setText("More");
            this.answerTextView.setText(" ");
            setClearScreen();
            Log.d(TAG, "Record " + diff2);
            scoreRecord();
            setClearScore();
            return;
        }
        this.problemCount++;
        problem = new Problem(diff2);
        this.problemTextView.setText(problem.getStringProblem());
        setCandidature();
    }

    private void setCandidature() {
        View divButton = findViewById(R.id.div_Button);
        divButton.setOnClickListener(this);
        ((TextView) divButton).setText(problem.getCandidature(0));
        View mulButton = findViewById(R.id.mul_Button);
        mulButton.setOnClickListener(this);
        ((TextView) mulButton).setText(problem.getCandidature(1));
        View subButton = findViewById(R.id.sub_Button);
        subButton.setOnClickListener(this);
        ((TextView) subButton).setText(problem.getCandidature(2));
        View addButton = findViewById(R.id.add_Button);
        addButton.setOnClickListener(this);
        ((TextView) addButton).setText(problem.getCandidature(3));
        switch (problem.getRand(5)) {
            case 1:
                ((TextView) divButton).setText(problem.getAnswer());
                this.correct = 4;
                return;
            case 2:
                ((TextView) subButton).setText(problem.getAnswer());
                this.correct = 2;
                return;
            case 3:
                ((TextView) addButton).setText(problem.getAnswer());
                this.correct = 1;
                return;
            default:
                ((TextView) mulButton).setText(problem.getAnswer());
                this.correct = 3;
                return;
        }
    }

    private void stopGame() {
        setClearScore();
        setClearScreen();
    }

    private void setClearScore() {
        this.elapsedTime = 0;
        this.problemCount = 0;
        this.answerCount = 0;
    }

    private void setClearScreen() {
        this.counterTextView.setText(String.valueOf(this.answerCount) + "/" + this.maxProblems);
        this.answerTextView.setText(" ");
        this.problemTextView.setText("Ready ?");
    }

    private void startChronometer(Chronometer c) {
        c.setBase(SystemClock.elapsedRealtime());
        c.start();
    }

    private int stopChronometer(Chronometer c) {
        c.stop();
        return (int) (SystemClock.elapsedRealtime() - c.getBase());
    }

    private void scoreRecord() {
        boolean f = false;
        int scd = (this.elapsedTime / 1000) % 60;
        int mini = ((this.elapsedTime / 1000) / 60) % 60;
        int hour = ((this.elapsedTime / 1000) / 60) / 60;
        String time = hour >= 10 ? String.valueOf("") + hour + ":" : String.valueOf("") + "0" + hour + ":";
        String time2 = mini >= 10 ? String.valueOf(time) + mini + ":" : String.valueOf(time) + "0" + mini + ":";
        String time3 = scd >= 10 ? String.valueOf(time2) + scd : String.valueOf(time2) + "0" + scd;
        String lastScore = "";
        if (this.answerCount < 10) {
            lastScore = String.valueOf(lastScore) + "0";
        }
        String lastScore2 = String.valueOf(lastScore) + this.answerCount + "/" + this.maxProblems;
        String[] scores = new String[11];
        String[] times = new String[11];
        String[] noScore = {"00/10", "00/15", "00/20", "00/99"};
        for (int i = 0; i < 10; i++) {
            scores[i] = getPreferences(0).getString(SCORE[(diff * 10) + i], noScore[diff]);
            times[i] = getPreferences(0).getString(TIME[(diff * 10) + i], "99:99:99");
        }
        scores[10] = lastScore2;
        times[10] = time3;
        for (int i2 = 10; i2 > 0; i2--) {
            if (scores[i2].compareTo(scores[i2 - 1]) > 0 || (scores[i2].compareTo(scores[i2 - 1]) == 0 && times[i2].compareTo(times[i2 - 1]) < 0)) {
                String temp = scores[i2];
                scores[i2] = scores[i2 - 1];
                scores[i2 - 1] = temp;
                String temp2 = times[i2];
                times[i2] = times[i2 - 1];
                times[i2 - 1] = temp2;
                f = true;
            }
        }
        for (int i3 = 0; i3 < 10; i3++) {
            getPreferences(0).edit().putString(SCORE[(diff * 10) + i3], scores[i3]).commit();
            getPreferences(0).edit().putString(TIME[(diff * 10) + i3], times[i3]).commit();
        }
        openFinishAlert(f);
    }

    private void openFinishAlert(boolean f) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Finish");
        String massage = "Last score is " + this.answerCount + "/" + this.maxProblems + ".";
        if (f) {
            massage = String.valueOf(massage) + " NEW!!";
        }
        alertDialogBuilder.setMessage(massage);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialogBuilder.setCancelable(true);
        alertDialogBuilder.create().show();
    }

    /* access modifiers changed from: private */
    public void clearRecords() {
        Log.d(TAG, "clearRecords");
        String[] noScore = {"00/10", "00/15", "00/20", "00/99"};
        for (int i = 0; i < 10; i++) {
            getPreferences(0).edit().putString(SCORE[(diff * 10) + i], noScore[diff]).commit();
            getPreferences(0).edit().putString(TIME[(diff * 10) + i], "99:99:99").commit();
        }
        setScore();
    }

    private void setScore() {
        if (diff == 0) {
            setTitle("Records(Easy)");
        } else if (diff == 1) {
            setTitle("Records(Normal)");
        } else if (diff == 2) {
            setTitle("Records(Hard)");
        } else if (diff == 3) {
            setTitle("Records(Survival)");
        }
        String[] noScore = {"00/10", "00/15", "00/20", "00/99"};
        this.ScoreView[(diff * 10) + 0] = (TextView) findViewById(R.id.score1);
        this.ScoreView[(diff * 10) + 0].setText(getPreferences(0).getString(SCORE[(diff * 10) + 0], noScore[diff]));
        this.TimeView[(diff * 10) + 0] = (TextView) findViewById(R.id.time1);
        this.TimeView[(diff * 10) + 0].setText(getPreferences(0).getString(TIME[(diff * 10) + 0], "99:99:99"));
        this.ScoreView[(diff * 10) + 1] = (TextView) findViewById(R.id.score2);
        this.ScoreView[(diff * 10) + 1].setText(getPreferences(0).getString(SCORE[(diff * 10) + 1], noScore[diff]));
        this.TimeView[(diff * 10) + 1] = (TextView) findViewById(R.id.time2);
        this.TimeView[(diff * 10) + 1].setText(getPreferences(0).getString(TIME[(diff * 10) + 1], "99:99:99"));
        this.ScoreView[(diff * 10) + 2] = (TextView) findViewById(R.id.score3);
        this.ScoreView[(diff * 10) + 2].setText(getPreferences(0).getString(SCORE[(diff * 10) + 2], noScore[diff]));
        this.TimeView[(diff * 10) + 2] = (TextView) findViewById(R.id.time3);
        this.TimeView[(diff * 10) + 2].setText(getPreferences(0).getString(TIME[(diff * 10) + 2], "99:99:99"));
        this.ScoreView[(diff * 10) + 3] = (TextView) findViewById(R.id.score4);
        this.ScoreView[(diff * 10) + 3].setText(getPreferences(0).getString(SCORE[(diff * 10) + 3], noScore[diff]));
        this.TimeView[(diff * 10) + 3] = (TextView) findViewById(R.id.time4);
        this.TimeView[(diff * 10) + 3].setText(getPreferences(0).getString(TIME[(diff * 10) + 3], "99:99:99"));
        this.ScoreView[(diff * 10) + 4] = (TextView) findViewById(R.id.score5);
        this.ScoreView[(diff * 10) + 4].setText(getPreferences(0).getString(SCORE[(diff * 10) + 4], noScore[diff]));
        this.TimeView[(diff * 10) + 4] = (TextView) findViewById(R.id.time5);
        this.TimeView[(diff * 10) + 4].setText(getPreferences(0).getString(TIME[(diff * 10) + 4], "99:99:99"));
        this.ScoreView[(diff * 10) + 5] = (TextView) findViewById(R.id.score6);
        this.ScoreView[(diff * 10) + 5].setText(getPreferences(0).getString(SCORE[(diff * 10) + 5], noScore[diff]));
        this.TimeView[(diff * 10) + 5] = (TextView) findViewById(R.id.time6);
        this.TimeView[(diff * 10) + 5].setText(getPreferences(0).getString(TIME[(diff * 10) + 5], "99:99:99"));
        this.ScoreView[(diff * 10) + 6] = (TextView) findViewById(R.id.score7);
        this.ScoreView[(diff * 10) + 6].setText(getPreferences(0).getString(SCORE[(diff * 10) + 6], noScore[diff]));
        this.TimeView[(diff * 10) + 6] = (TextView) findViewById(R.id.time7);
        this.TimeView[(diff * 10) + 6].setText(getPreferences(0).getString(TIME[(diff * 10) + 6], "99:99:99"));
        this.ScoreView[(diff * 10) + 7] = (TextView) findViewById(R.id.score8);
        this.ScoreView[(diff * 10) + 7].setText(getPreferences(0).getString(SCORE[(diff * 10) + 7], noScore[diff]));
        this.TimeView[(diff * 10) + 7] = (TextView) findViewById(R.id.time8);
        this.TimeView[(diff * 10) + 7].setText(getPreferences(0).getString(TIME[(diff * 10) + 7], "99:99:99"));
        this.ScoreView[(diff * 10) + 8] = (TextView) findViewById(R.id.score9);
        this.ScoreView[(diff * 10) + 8].setText(getPreferences(0).getString(SCORE[(diff * 10) + 8], noScore[diff]));
        this.TimeView[(diff * 10) + 8] = (TextView) findViewById(R.id.time9);
        this.TimeView[(diff * 10) + 8].setText(getPreferences(0).getString(TIME[(diff * 10) + 8], "99:99:99"));
        this.ScoreView[(diff * 10) + 9] = (TextView) findViewById(R.id.score10);
        this.ScoreView[(diff * 10) + 9].setText(getPreferences(0).getString(SCORE[(diff * 10) + 9], noScore[diff]));
        this.TimeView[(diff * 10) + 9] = (TextView) findViewById(R.id.time10);
        this.TimeView[(diff * 10) + 9].setText(getPreferences(0).getString(TIME[(diff * 10) + 9], "99:99:99"));
    }

    private void setAdmob() {
        AdView adView = new AdView(this);
        adView.setAdListener(new AdListener() {
            public void onReceiveRefreshedAd(AdView arg0) {
            }

            public void onReceiveAd(AdView arg0) {
            }

            public void onFailedToReceiveRefreshedAd(AdView arg0) {
            }

            public void onFailedToReceiveAd(AdView arg0) {
            }
        });
        adView.setVisibility(0);
        adView.requestFreshAd();
        adView.setRequestInterval(13);
        adView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        LinearLayout linearlayout = new LinearLayout(this);
        linearlayout.addView(adView);
        linearlayout.setGravity(80);
        addContentView(linearlayout, new LinearLayout.LayoutParams(-1, -1));
        adView.requestFreshAd();
        AdManager.setTestDevices(new String[]{AdManager.TEST_EMULATOR, "android_id"});
    }
}
