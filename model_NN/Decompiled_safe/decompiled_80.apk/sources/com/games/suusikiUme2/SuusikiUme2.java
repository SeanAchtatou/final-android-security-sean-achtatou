package com.games.suusikiUme2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import com.admob.android.ads.AdListener;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;
import com.adwhirl.AdWhirlLayout;
import jp.co.microad.smartphone.sdk.MicroAdLayout;

public class SuusikiUme2 extends Activity implements View.OnClickListener, AdWhirlLayout.AdWhirlInterface {
    private static final String TAG = "SuusikiUme2";
    Intent intent;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ((MicroAdLayout) findViewById(R.id.adview)).init(this);
        MicroAdLayout ad = (MicroAdLayout) findViewById(R.id.adview);
        ad.init(null);
        ad.maxWidth -= 60;
        this.intent = new Intent(this, Game.class);
        findViewById(R.id.new_game_button).setOnClickListener(this);
        findViewById(R.id.survival_button).setOnClickListener(this);
        findViewById(R.id.records_button).setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.new_game_button:
                Log.d(TAG, "clicked on Play");
                this.intent.putExtra("com.games.suusikiUme2.gameMode", 0);
                openNewGameDialog();
                return;
            case R.id.survival_button:
                Log.d(TAG, "clicked on Survival");
                this.intent.putExtra("com.games.suusikiUme2.gameMode", 1);
                this.intent.putExtra("com.games.suusikiUme2.difficulty", 3);
                startActivity(this.intent);
                return;
            case R.id.records_button:
                Log.d(TAG, "clicked on Records");
                this.intent.putExtra("com.games.suusikiUme2.gameMode", 2);
                startActivity(this.intent);
                return;
            default:
                return;
        }
    }

    private void openNewGameDialog() {
        new AlertDialog.Builder(this).setTitle("difficulty").setItems((int) R.array.difficulty, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                SuusikiUme2.this.startGame(i);
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void startGame(int i) {
        Log.d(TAG, "startGame with " + i);
        this.intent.putExtra("com.games.suusikiUme2.difficulty", i);
        startActivity(this.intent);
    }

    private void setAdmob() {
        AdView adView = new AdView(this);
        adView.setAdListener(new AdListener() {
            public void onReceiveRefreshedAd(AdView arg0) {
            }

            public void onReceiveAd(AdView arg0) {
            }

            public void onFailedToReceiveRefreshedAd(AdView arg0) {
            }

            public void onFailedToReceiveAd(AdView arg0) {
            }
        });
        adView.setVisibility(0);
        adView.requestFreshAd();
        adView.setRequestInterval(13);
        adView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        LinearLayout linearlayout = new LinearLayout(this);
        linearlayout.addView(adView);
        linearlayout.setGravity(80);
        addContentView(linearlayout, new LinearLayout.LayoutParams(-1, -1));
        adView.requestFreshAd();
        AdManager.setTestDevices(new String[]{AdManager.TEST_EMULATOR, "android_id"});
    }

    public void adWhirlGeneric() {
    }
}
