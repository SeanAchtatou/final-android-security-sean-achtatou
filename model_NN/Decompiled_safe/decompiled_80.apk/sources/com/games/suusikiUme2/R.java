package com.games.suusikiUme2;

public final class R {

    public static final class array {
        public static final int difficulty = 2130968576;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int background = 2131034112;
        public static final int puzzle_background = 2131034113;
        public static final int puzzle_dark = 2131034116;
        public static final int puzzle_foreground = 2131034118;
        public static final int puzzle_hilite = 2131034115;
        public static final int puzzle_hilite_2 = 2131034117;
        public static final int puzzle_hint_0 = 2131034119;
        public static final int puzzle_hint_1 = 2131034120;
        public static final int puzzle_hint_2 = 2131034121;
        public static final int puzzle_light = 2131034114;
        public static final int puzzle_selected = 2131034122;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int add_Button = 2131165188;
        public static final int adview = 2131165192;
        public static final int answer = 2131165186;
        public static final int caunter = 2131165185;
        public static final int chronometer = 2131165184;
        public static final int clear_button = 2131165248;
        public static final int div_Button = 2131165191;
        public static final int easy_button = 2131165199;
        public static final int hard_button = 2131165201;
        public static final int layout_main = 2131165195;
        public static final int mul_Button = 2131165190;
        public static final int new_game_button = 2131165196;
        public static final int normal_button = 2131165200;
        public static final int problem = 2131165187;
        public static final int records_button = 2131165198;
        public static final int return_title_button = 2131165194;
        public static final int score1 = 2131165210;
        public static final int score10 = 2131165246;
        public static final int score2 = 2131165214;
        public static final int score3 = 2131165218;
        public static final int score4 = 2131165222;
        public static final int score5 = 2131165226;
        public static final int score6 = 2131165230;
        public static final int score7 = 2131165234;
        public static final int score8 = 2131165238;
        public static final int score9 = 2131165242;
        public static final int scrollView1 = 2131165202;
        public static final int start_Button = 2131165193;
        public static final int sub_Button = 2131165189;
        public static final int survival_button = 2131165197;
        public static final int tableLayout1 = 2131165203;
        public static final int tableRow1 = 2131165204;
        public static final int tableRow10 = 2131165240;
        public static final int tableRow11 = 2131165244;
        public static final int tableRow2 = 2131165208;
        public static final int tableRow3 = 2131165212;
        public static final int tableRow4 = 2131165216;
        public static final int tableRow5 = 2131165220;
        public static final int tableRow6 = 2131165224;
        public static final int tableRow7 = 2131165228;
        public static final int tableRow8 = 2131165232;
        public static final int tableRow9 = 2131165236;
        public static final int textView1 = 2131165205;
        public static final int textView10 = 2131165233;
        public static final int textView11 = 2131165237;
        public static final int textView12 = 2131165241;
        public static final int textView13 = 2131165245;
        public static final int textView2 = 2131165206;
        public static final int textView3 = 2131165207;
        public static final int textView4 = 2131165209;
        public static final int textView5 = 2131165213;
        public static final int textView6 = 2131165217;
        public static final int textView7 = 2131165221;
        public static final int textView8 = 2131165225;
        public static final int textView9 = 2131165229;
        public static final int time1 = 2131165211;
        public static final int time10 = 2131165247;
        public static final int time2 = 2131165215;
        public static final int time3 = 2131165219;
        public static final int time4 = 2131165223;
        public static final int time5 = 2131165227;
        public static final int time6 = 2131165231;
        public static final int time7 = 2131165235;
        public static final int time8 = 2131165239;
        public static final int time9 = 2131165243;
    }

    public static final class layout {
        public static final int game = 2130903040;
        public static final int main = 2130903041;
        public static final int records = 2130903042;
    }

    public static final class string {
        public static final int about_text = 2131099665;
        public static final int about_title = 2131099664;
        public static final int app_name = 2131099648;
        public static final int best_record_title = 2131099658;
        public static final int easy_label = 2131099654;
        public static final int end_massage_label = 2131099676;
        public static final int end_massage_text = 2131099677;
        public static final int exit_label = 2131099652;
        public static final int game_title = 2131099663;
        public static final int hard_label = 2131099656;
        public static final int hints_summary = 2131099672;
        public static final int hints_title = 2131099671;
        public static final int hoge = 2131099678;
        public static final int initial_data_label = 2131099662;
        public static final int keypad_title = 2131099674;
        public static final int main_title = 2131099649;
        public static final int medium_label = 2131099655;
        public static final int music_summary = 2131099670;
        public static final int music_title = 2131099669;
        public static final int new_game_label = 2131099650;
        public static final int new_game_title = 2131099653;
        public static final int no_moves_label = 2131099673;
        public static final int record_easy_label = 2131099659;
        public static final int record_hard_label = 2131099661;
        public static final int record_medium_label = 2131099660;
        public static final int records_label = 2131099651;
        public static final int records_title = 2131099657;
        public static final int result_title = 2131099675;
        public static final int settings_label = 2131099666;
        public static final int settings_shortcut = 2131099668;
        public static final int settings_title = 2131099667;
    }
}
