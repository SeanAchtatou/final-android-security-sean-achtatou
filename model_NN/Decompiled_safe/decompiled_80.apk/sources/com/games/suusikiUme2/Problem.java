package com.games.suusikiUme2;

import com.adwhirl.util.AdWhirlUtil;
import java.util.Random;
import jp.hishidama.eval.ExpRuleFactory;
import jp.hishidama.eval.Expression;
import jp.hishidama.eval.Rule;

public class Problem {
    private static final int DIFFICULTY_EASY = 0;
    private static final int DIFFICULTY_HARD = 2;
    private static final int DIFFICULTY_MEDIUM = 1;
    private static final int DIFFICULTY_VERY_EASY = 3;
    private static final int DIFFICULTY_VERY_HARD = 4;
    private static final int EASY_MAX_NUMBER = 3;
    private static final int EASY_MAX_OPERATERS = 2;
    private static final int EASY_MAX_VALUE = 9;
    private static final int HARD_MAX_NUMBER = 5;
    private static final int HARD_MAX_OPERATERS = 4;
    private static final int HARD_MAX_VALUE = 19;
    private static final int MEDIUM_MAX_NUMBER = 4;
    private static final int MEDIUM_MAX_OPERATERS = 3;
    private static final int MEDIUM_MAX_VALUE = 15;
    private static final int VERY_EASY_MAX_NUMBER = 3;
    private static final int VERY_EASY_MAX_OPERATERS = 2;
    private static final int VERY_EASY_MAX_VALUE = 9;
    private static final int VERY_HARD_MAX_NUMBER = 5;
    private static final int VERY_HARD_MAX_OPERATERS = 4;
    private static final int VERY_HARD_MAX_VALUE = 39;
    private int answer = 0;
    private int diffi;
    private int hideOperater = 0;
    private int maxNumbers = 0;
    private int maxOperaters = 0;
    private int maxValue = 0;
    public int[] n = new int[4];
    private int nbNumberL = 0;
    private int nbNumberR = 0;
    private int nbOperatorL = 0;
    private int nbOperatorR = 0;
    private int[] numberL = new int[5];
    private int[] numberR = new int[5];
    private int[] operatorL = new int[4];
    private int[] operatorR = new int[4];
    private int type = 1;

    public Problem(int diff) {
        setDifficulty(diff);
        genProblem(this.type);
        this.n[0] = this.numberR[0];
        this.n[1] = this.numberR[0];
        this.n[2] = this.numberR[0];
        this.n[3] = this.numberR[0];
    }

    private void setDifficulty(int diff) {
        this.diffi = diff;
        switch (diff) {
            case 0:
                this.maxNumbers = 3;
                this.maxValue = 9;
                this.maxOperaters = 2;
                return;
            case 1:
                this.maxNumbers = 4;
                this.maxValue = 15;
                this.maxOperaters = 3;
                return;
            case 2:
                this.maxNumbers = 5;
                this.maxValue = HARD_MAX_VALUE;
                this.maxOperaters = 4;
                return;
            case 3:
                this.maxNumbers = 3;
                this.maxValue = 9;
                this.maxOperaters = 2;
                return;
            case 4:
                this.maxNumbers = 5;
                this.maxValue = VERY_HARD_MAX_VALUE;
                this.maxOperaters = 4;
                return;
            default:
                return;
        }
    }

    private void genProblem(int problemType) {
        switch (problemType) {
            case 1:
                this.nbNumberL = this.maxNumbers - 1;
                this.nbOperatorL = this.nbNumberL - 1;
                this.nbNumberR = this.maxNumbers - this.nbNumberL;
                this.nbOperatorR = this.nbNumberR - 1;
                this.hideOperater = getRand(this.maxNumbers - 2);
                getEquation1();
                return;
            case 2:
                this.nbNumberL = getRand(this.maxNumbers - 1);
                this.nbOperatorL = this.nbNumberL - 1;
                this.nbNumberR = this.maxNumbers - this.nbNumberL;
                this.nbOperatorR = this.nbNumberR - 1;
                this.hideOperater = getRand(this.maxNumbers - 2);
                getEquation2();
                return;
            default:
                return;
        }
    }

    private void getEquation1() {
        boolean f1 = false;
        while (!f1) {
            this.numberL[0] = getRand(this.maxValue);
            for (int i = 1; i < this.maxNumbers - 1; i++) {
                this.numberL[i] = getRand(this.maxValue);
                this.operatorL[i - 1] = getRand(this.maxOperaters);
            }
            String equation = String.valueOf("") + this.numberL[0];
            for (int i2 = 1; i2 < this.nbNumberL; i2++) {
                equation = String.valueOf(String.valueOf(equation) + getOperater(this.operatorL[i2 - 1])) + this.numberL[i2];
            }
            Expression exp = ExpRuleFactory.getDefaultRule().parse(equation);
            if (0.0d >= exp.evalDouble() - ((double) exp.evalInt())) {
                f1 = true;
            }
            this.numberR[0] = exp.evalInt();
        }
    }

    private void getEquation2() {
        boolean f1 = false;
        while (!f1) {
            for (int i = 0; i < this.nbNumberR; i++) {
                this.numberR[i] = getRand(this.maxValue);
            }
            for (int i2 = 0; i2 < this.nbOperatorR; i2++) {
                this.operatorR[i2] = getRand(this.maxOperaters);
            }
            for (int i3 = 0; i3 < this.nbNumberL; i3++) {
                this.numberL[i3] = getRand(this.maxValue);
            }
            for (int i4 = 0; i4 < this.nbOperatorL; i4++) {
                this.operatorL[i4] = getRand(this.maxOperaters);
            }
            String equation = new StringBuilder().append(this.numberL[0]).toString();
            for (int i5 = 1; i5 < this.nbNumberL; i5++) {
                equation = String.valueOf(String.valueOf(equation) + getOperater(this.operatorL[i5 - 1])) + this.numberL[i5];
            }
            String equation2 = new StringBuilder().append(this.numberR[0]).toString();
            for (int i6 = 1; i6 < this.nbNumberR; i6++) {
                equation2 = String.valueOf(String.valueOf(equation2) + getOperater(this.operatorR[i6 - 1])) + this.numberR[i6];
            }
            Rule rule = ExpRuleFactory.getDefaultRule();
            if (rule.parse(equation).evalInt() != rule.parse(equation2).evalInt() || this.diffi == 2) {
                f1 = true;
            }
        }
        setAnswer2();
    }

    private void setAnswer2() {
        String equation = new StringBuilder().append(this.numberL[0]).toString();
        for (int i = 1; i < this.nbNumberL; i++) {
            equation = String.valueOf(String.valueOf(equation) + getOperater(this.operatorL[i - 1])) + this.numberL[i];
        }
        String equation2 = new StringBuilder().append(this.numberR[0]).toString();
        for (int i2 = 1; i2 < this.nbNumberR; i2++) {
            equation2 = String.valueOf(String.valueOf(equation2) + getOperater(this.operatorR[i2 - 1])) + this.numberR[i2];
        }
        Rule rule = ExpRuleFactory.getDefaultRule();
        int a = rule.parse(equation).evalInt();
        int b = rule.parse(equation2).evalInt();
        if (this.diffi == 1 || this.diffi == 0) {
            if (a > b) {
                this.answer = 1;
            } else if (a < b) {
                this.answer = 2;
            }
        } else if (this.diffi != 2) {
        } else {
            if (a > b) {
                this.answer = 1;
            } else if (a == b) {
                this.answer = 2;
            } else if (a < b) {
                this.answer = 3;
            }
        }
    }

    public int getAnswer2() {
        return this.answer;
    }

    public int getHideOperater() {
        return this.operatorL[this.hideOperater - 1];
    }

    public String getAnswer() {
        return new StringBuilder().append(this.numberR[0]).toString();
    }

    public String getCandidature(int c) {
        int a = this.numberR[0];
        int candi = a;
        while (candi == a) {
            switch (getRand(13)) {
                case 1:
                    candi = a * getRand(5);
                    break;
                case 2:
                    candi = a / getRand(5);
                    break;
                case 3:
                    candi = a + this.numberL[0];
                    break;
                case 4:
                    candi = a + this.numberL[this.nbNumberL - 1];
                    break;
                case 5:
                    candi = a - this.numberL[0];
                    break;
                case 6:
                    candi = a - this.numberL[this.nbNumberL - 1];
                    break;
                case 7:
                    candi = a * 10;
                    break;
                case AdWhirlUtil.NETWORK_TYPE_QUATTRO:
                    candi = a / 10;
                    break;
                case AdWhirlUtil.NETWORK_TYPE_CUSTOM:
                    candi = a + (getRand(10) * 10);
                    break;
                case AdWhirlUtil.NETWORK_TYPE_ADWHIRL:
                    candi = a + (getRand(10) * 100);
                    break;
                case AdWhirlUtil.NETWORK_TYPE_MOBCLIX:
                    candi = a + this.numberL[this.nbNumberL / 2];
                    break;
                case AdWhirlUtil.NETWORK_TYPE_MDOTM:
                    candi = a - this.numberL[this.nbNumberL / 2];
                    break;
                default:
                    candi = a * a;
                    break;
            }
            if (this.n[0] == candi || this.n[1] == candi || a == candi || this.n[2] == candi || this.n[3] == candi) {
                candi = a;
            } else {
                this.n[c] = candi;
            }
        }
        return new StringBuilder().append(candi).toString();
    }

    public String getStringFull() {
        String equation = new StringBuilder().append(this.numberL[0]).toString();
        for (int i = 1; i < this.nbNumberL; i++) {
            equation = String.valueOf(String.valueOf(equation) + getStringOperater(this.operatorL[i - 1])) + this.numberL[i];
        }
        String equation2 = String.valueOf(String.valueOf(equation) + getStringOperater(0)) + this.numberR[0];
        for (int i2 = 1; i2 < this.nbNumberR; i2++) {
            equation2 = String.valueOf(String.valueOf(equation2) + getStringOperater(this.operatorR[i2 - 1])) + this.numberR[i2];
        }
        return equation2;
    }

    public String getStringProblem() {
        String equation = new StringBuilder().append(this.numberL[0]).toString();
        for (int i = 1; i < this.nbNumberL; i++) {
            equation = String.valueOf(String.valueOf(equation) + getStringOperater(this.operatorL[i - 1])) + this.numberL[i];
        }
        if (this.type == 1) {
            equation = String.valueOf(String.valueOf(equation) + getStringOperater(0)) + " □ ";
        } else if (this.type == 2) {
            equation = String.valueOf(String.valueOf(equation) + "□") + this.numberR[0];
        }
        for (int i2 = 1; i2 < this.nbNumberR; i2++) {
            equation = String.valueOf(String.valueOf(equation) + getStringOperater(this.operatorR[i2 - 1])) + this.numberR[i2];
        }
        return equation;
    }

    private String getStringOperater(int operationCode) {
        switch (operationCode) {
            case 0:
                return "＝";
            case 1:
                return "＋";
            case 2:
                return "－";
            case 3:
                return "×";
            case 4:
                return "÷";
            default:
                return "error";
        }
    }

    private String getOperater(int operationCode) {
        switch (operationCode) {
            case 0:
                return "=";
            case 1:
                return "+";
            case 2:
                return "-";
            case 3:
                return "*";
            case 4:
                return "/";
            default:
                return "error";
        }
    }

    public int getRand(int maxNumber) {
        Random rand = new Random();
        for (int i = 0; i < 100; i++) {
            rand = new Random();
        }
        return rand.nextInt(maxNumber) + 1;
    }

    public int getZeroRand(int maxNumber) {
        Random rand = new Random();
        for (int i = 0; i < 100; i++) {
            rand = new Random();
        }
        return rand.nextInt(maxNumber + 1);
    }
}
