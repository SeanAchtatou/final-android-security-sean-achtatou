package com.adwhirl.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import java.util.concurrent.TimeUnit;

public class CustomAdapter extends AdWhirlAdapter {
    public CustomAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
    }

    public void handle() {
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.scheduler.schedule(new FetchCustomRunnable(this), 0, TimeUnit.SECONDS);
        }
    }

    public void displayCustom() {
        Activity activity;
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null && (activity = adWhirlLayout.activityReference.get()) != null) {
            switch (adWhirlLayout.custom.type) {
                case 1:
                    Log.d(AdWhirlUtil.ADWHIRL, "Serving custom type: banner");
                    RelativeLayout bannerView = new RelativeLayout(activity);
                    if (adWhirlLayout.custom.image != null) {
                        ImageView bannerImageView = new ImageView(activity);
                        bannerImageView.setImageDrawable(adWhirlLayout.custom.image);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                        layoutParams.addRule(13);
                        bannerView.addView(bannerImageView, layoutParams);
                        adWhirlLayout.pushSubView(bannerView);
                        break;
                    } else {
                        adWhirlLayout.rotateThreadedNow();
                        return;
                    }
                case 2:
                    Log.d(AdWhirlUtil.ADWHIRL, "Serving custom type: icon");
                    RelativeLayout relativeLayout = new RelativeLayout(activity);
                    if (adWhirlLayout.custom.image != null) {
                        double density = AdWhirlUtil.getDensity(activity);
                        double px320 = (double) AdWhirlUtil.convertToScreenPixels(320, density);
                        double px50 = (double) AdWhirlUtil.convertToScreenPixels(50, density);
                        double px4 = (double) AdWhirlUtil.convertToScreenPixels(4, density);
                        double px6 = (double) AdWhirlUtil.convertToScreenPixels(6, density);
                        relativeLayout.setLayoutParams(new FrameLayout.LayoutParams((int) px320, (int) px50));
                        ImageView blendView = new ImageView(activity);
                        int backgroundColor = Color.rgb(adWhirlLayout.extra.bgRed, adWhirlLayout.extra.bgGreen, adWhirlLayout.extra.bgBlue);
                        blendView.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, backgroundColor, backgroundColor, backgroundColor}));
                        relativeLayout.addView(blendView, new RelativeLayout.LayoutParams(-1, -1));
                        ImageView imageView = new ImageView(activity);
                        imageView.setImageDrawable(adWhirlLayout.custom.image);
                        imageView.setId(10);
                        imageView.setPadding((int) px4, 0, (int) px6, 0);
                        imageView.setScaleType(ImageView.ScaleType.CENTER);
                        relativeLayout.addView(imageView, new RelativeLayout.LayoutParams(-2, -1));
                        ImageView imageView2 = new ImageView(activity);
                        imageView2.setImageDrawable(new BitmapDrawable(getClass().getResourceAsStream("/com/adwhirl/assets/ad_frame.gif")));
                        imageView2.setPadding((int) px4, 0, (int) px6, 0);
                        imageView2.setScaleType(ImageView.ScaleType.CENTER);
                        relativeLayout.addView(imageView2, new RelativeLayout.LayoutParams(-2, -1));
                        TextView textView = new TextView(activity);
                        textView.setText(adWhirlLayout.custom.description);
                        textView.setTypeface(Typeface.DEFAULT_BOLD, 1);
                        textView.setTextColor(Color.rgb(adWhirlLayout.extra.fgRed, adWhirlLayout.extra.fgGreen, adWhirlLayout.extra.fgBlue));
                        RelativeLayout.LayoutParams textViewParams = new RelativeLayout.LayoutParams(-1, -1);
                        textViewParams.addRule(1, imageView.getId());
                        textViewParams.addRule(10);
                        textViewParams.addRule(12);
                        textViewParams.addRule(15);
                        textViewParams.addRule(13);
                        textView.setGravity(16);
                        relativeLayout.addView(textView, textViewParams);
                        adWhirlLayout.pushSubView(relativeLayout);
                        break;
                    } else {
                        adWhirlLayout.rotateThreadedNow();
                        return;
                    }
                default:
                    Log.w(AdWhirlUtil.ADWHIRL, "Unknown custom type!");
                    adWhirlLayout.rotateThreadedNow();
                    return;
            }
            adWhirlLayout.adWhirlManager.resetRollover();
            adWhirlLayout.rotateThreadedDelayed();
        }
    }

    private static class FetchCustomRunnable implements Runnable {
        private CustomAdapter customAdapter;

        public FetchCustomRunnable(CustomAdapter customAdapter2) {
            this.customAdapter = customAdapter2;
        }

        public void run() {
            AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.customAdapter.adWhirlLayoutReference.get();
            if (adWhirlLayout != null) {
                adWhirlLayout.custom = adWhirlLayout.adWhirlManager.getCustom(this.customAdapter.ration.nid);
                if (adWhirlLayout.custom == null) {
                    adWhirlLayout.rotateThreadedNow();
                } else {
                    adWhirlLayout.handler.post(new DisplayCustomRunnable(this.customAdapter));
                }
            }
        }
    }

    private static class DisplayCustomRunnable implements Runnable {
        private CustomAdapter customAdapter;

        public DisplayCustomRunnable(CustomAdapter customAdapter2) {
            this.customAdapter = customAdapter2;
        }

        public void run() {
            this.customAdapter.displayCustom();
        }
    }
}
