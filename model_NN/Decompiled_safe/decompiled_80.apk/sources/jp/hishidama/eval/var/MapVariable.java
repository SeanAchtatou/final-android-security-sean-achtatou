package jp.hishidama.eval.var;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MapVariable<K, V> extends DefaultVariable {
    protected Map<K, V> map;

    public MapVariable() {
        this(new HashMap());
    }

    public MapVariable(Class<K> keyType, Class<V> valueType) {
        this(Collections.checkedMap(new HashMap(), keyType, valueType));
    }

    public MapVariable(Map<K, V> varMap) {
        this.map = varMap;
    }

    public void setMap(Map<K, V> varMap) {
        this.map = varMap;
    }

    public Map<K, V> getMap() {
        return this.map;
    }

    public void put(K name, V value) {
        this.map.put(name, value);
    }

    public V get(K name) {
        return this.map.get(name);
    }

    public Object getValue(Object name) {
        return get(name);
    }

    public void setValue(Object name, Object value) {
        put(name, value);
    }
}
