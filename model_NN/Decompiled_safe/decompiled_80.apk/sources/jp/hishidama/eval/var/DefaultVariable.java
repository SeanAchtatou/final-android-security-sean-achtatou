package jp.hishidama.eval.var;

import java.lang.reflect.Array;
import java.util.List;
import java.util.Map;
import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.exp.AbstractExpression;

public class DefaultVariable implements Variable {
    public Object getValue(Object name) {
        return null;
    }

    public void setValue(Object name, Object value) {
    }

    public Object getArrayValue(Object array, String arrayName, Object index, AbstractExpression exp) {
        if (array == null) {
            throw new EvalException(EvalException.EXP_NOT_DEF_OBJ, arrayName, exp, null);
        } else if (array.getClass().isArray()) {
            return Array.get(array, getInt(index));
        } else {
            if (array instanceof Map) {
                return ((Map) array).get(index);
            }
            if (array instanceof List) {
                return ((List) array).get(getInt(index));
            }
            throw new UnsupportedOperationException(array.getClass().getName());
        }
    }

    public void setArrayValue(Object array, String arrayName, Object index, Object value, AbstractExpression exp) {
        if (array == null) {
            throw new EvalException(EvalException.EXP_NOT_DEF_OBJ, arrayName, exp, null);
        } else if (array.getClass().isArray()) {
            Array.set(array, getInt(index), value);
        } else if (array instanceof Map) {
            ((Map) array).put(index, value);
        } else if (array instanceof List) {
            ((List) array).set(getInt(index), value);
        } else {
            throw new UnsupportedOperationException(array.getClass().getName());
        }
    }

    public int getInt(Object value) {
        if (value == null) {
            throw new NumberFormatException("value=" + value);
        } else if (value instanceof Number) {
            return ((Number) value).intValue();
        } else {
            return Integer.parseInt(value.toString());
        }
    }

    public Object getFieldValue(Object obj, String objName, String field, AbstractExpression exp) {
        if (obj == null) {
            throw new EvalException(EvalException.EXP_NOT_DEF_OBJ, objName, exp, null);
        }
        try {
            return obj.getClass().getField(field).get(obj);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    public void setFieldValue(Object obj, String objName, String field, Object value, AbstractExpression exp) {
        if (obj == null) {
            throw new EvalException(EvalException.EXP_NOT_DEF_OBJ, objName, exp, null);
        }
        try {
            obj.getClass().getField(field).set(obj, value);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }
}
