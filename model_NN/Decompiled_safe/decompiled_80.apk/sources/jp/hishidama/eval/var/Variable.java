package jp.hishidama.eval.var;

import jp.hishidama.eval.exp.AbstractExpression;

public interface Variable {
    Object getArrayValue(Object obj, String str, Object obj2, AbstractExpression abstractExpression);

    Object getFieldValue(Object obj, String str, String str2, AbstractExpression abstractExpression);

    Object getValue(Object obj);

    void setArrayValue(Object obj, String str, Object obj2, Object obj3, AbstractExpression abstractExpression);

    void setFieldValue(Object obj, String str, String str2, Object obj2, AbstractExpression abstractExpression);

    void setValue(Object obj, Object obj2);
}
