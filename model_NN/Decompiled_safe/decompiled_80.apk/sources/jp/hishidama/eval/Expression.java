package jp.hishidama.eval;

import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.func.Function;
import jp.hishidama.eval.log.EvalLog;
import jp.hishidama.eval.oper.DoubleOperator;
import jp.hishidama.eval.oper.IntOperator;
import jp.hishidama.eval.oper.LongOperator;
import jp.hishidama.eval.oper.Operator;
import jp.hishidama.eval.ref.Refactor;
import jp.hishidama.eval.repl.Replace;
import jp.hishidama.eval.srch.Search;
import jp.hishidama.eval.var.Variable;

public abstract class Expression {
    protected AbstractExpression ae;
    public Function func;
    public EvalLog log;
    public Operator oper;
    public Replace repl;
    public Search srch;
    public Variable var;

    public abstract Expression dup();

    public abstract Object eval();

    public abstract void optimize(Variable variable, Operator operator);

    public abstract void refactorFunc(Refactor refactor, Rule rule);

    public abstract void refactorName(Refactor refactor);

    public abstract void search(Search search);

    public static Expression parse(String str) {
        return ExpRuleFactory.getDefaultRule().parse(str);
    }

    public void setVariable(Variable var2) {
        this.var = var2;
    }

    public void setFunction(Function func2) {
        this.func = func2;
    }

    public void setOperator(Operator oper2) {
        this.oper = oper2;
    }

    public void setEvalLog(EvalLog log2) {
        this.log = log2;
    }

    public int evalInt() {
        Operator bak = this.oper;
        try {
            if (!(this.oper instanceof IntOperator)) {
                setOperator(new IntOperator());
            }
            Number n = (Number) eval();
            if (n != null) {
                return n.intValue();
            }
            this.oper = bak;
            return 0;
        } finally {
            this.oper = bak;
        }
    }

    public long evalLong() {
        Operator bak = this.oper;
        try {
            if (!(this.oper instanceof LongOperator)) {
                setOperator(new LongOperator());
            }
            Number n = (Number) eval();
            if (n != null) {
                return n.longValue();
            }
            this.oper = bak;
            return 0;
        } finally {
            this.oper = bak;
        }
    }

    public double evalDouble() {
        Operator bak = this.oper;
        try {
            if (!(this.oper instanceof DoubleOperator)) {
                setOperator(new DoubleOperator());
            }
            Number n = (Number) eval();
            if (n != null) {
                return n.doubleValue();
            }
            this.oper = bak;
            return 0.0d;
        } finally {
            this.oper = bak;
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Expression)) {
            return super.equals(obj);
        }
        AbstractExpression e = ((Expression) obj).ae;
        if (this.ae == null && e == null) {
            return true;
        }
        if (this.ae == null || e == null) {
            return false;
        }
        return this.ae.equals(e);
    }

    public int hashCode() {
        if (this.ae == null) {
            return 0;
        }
        return this.ae.hashCode();
    }

    public boolean same(Expression obj) {
        AbstractExpression e = obj.ae;
        if (this.ae == null) {
            return e == null;
        }
        return this.ae.same(e);
    }

    public boolean isEmpty() {
        return this.ae == null;
    }

    public String toString() {
        if (this.ae == null) {
            return "";
        }
        return this.ae.toString();
    }
}
