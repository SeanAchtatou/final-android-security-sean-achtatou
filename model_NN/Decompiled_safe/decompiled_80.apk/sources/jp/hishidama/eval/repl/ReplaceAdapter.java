package jp.hishidama.eval.repl;

import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.Col1Expression;
import jp.hishidama.eval.exp.Col2Expression;
import jp.hishidama.eval.exp.Col2OpeExpression;
import jp.hishidama.eval.exp.Col3Expression;
import jp.hishidama.eval.exp.FunctionExpression;
import jp.hishidama.eval.exp.WordExpression;

public class ReplaceAdapter implements Replace {
    public AbstractExpression replace0(WordExpression exp) {
        return exp;
    }

    public AbstractExpression replace1(Col1Expression exp) {
        return exp;
    }

    public AbstractExpression replace2(Col2Expression exp) {
        return exp;
    }

    public AbstractExpression replace2(Col2OpeExpression exp) {
        return exp;
    }

    public AbstractExpression replace3(Col3Expression exp) {
        return exp;
    }

    public AbstractExpression replaceVar0(WordExpression exp) {
        return exp;
    }

    public AbstractExpression replaceVar1(Col1Expression exp) {
        return exp;
    }

    public AbstractExpression replaceVar2(Col2Expression exp) {
        return exp;
    }

    public AbstractExpression replaceVar2(Col2OpeExpression exp) {
        return exp;
    }

    public AbstractExpression replaceVar3(Col3Expression exp) {
        return exp;
    }

    public AbstractExpression replaceFunc(FunctionExpression exp) {
        return exp;
    }

    public AbstractExpression replaceLet(Col2Expression exp) {
        return exp;
    }
}
