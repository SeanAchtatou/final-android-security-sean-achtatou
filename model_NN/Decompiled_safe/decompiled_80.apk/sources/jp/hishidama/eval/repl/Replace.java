package jp.hishidama.eval.repl;

import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.Col1Expression;
import jp.hishidama.eval.exp.Col2Expression;
import jp.hishidama.eval.exp.Col2OpeExpression;
import jp.hishidama.eval.exp.Col3Expression;
import jp.hishidama.eval.exp.FunctionExpression;
import jp.hishidama.eval.exp.WordExpression;

public interface Replace {
    AbstractExpression replace0(WordExpression wordExpression);

    AbstractExpression replace1(Col1Expression col1Expression);

    AbstractExpression replace2(Col2Expression col2Expression);

    AbstractExpression replace2(Col2OpeExpression col2OpeExpression);

    AbstractExpression replace3(Col3Expression col3Expression);

    AbstractExpression replaceFunc(FunctionExpression functionExpression);

    AbstractExpression replaceLet(Col2Expression col2Expression);

    AbstractExpression replaceVar0(WordExpression wordExpression);

    AbstractExpression replaceVar1(Col1Expression col1Expression);

    AbstractExpression replaceVar2(Col2Expression col2Expression);

    AbstractExpression replaceVar2(Col2OpeExpression col2OpeExpression);

    AbstractExpression replaceVar3(Col3Expression col3Expression);
}
