package jp.hishidama.eval;

public class EvalExceptionFormatter {
    private static EvalExceptionFormatter me;

    public static EvalExceptionFormatter getDefault() {
        if (me == null) {
            me = new EvalExceptionFormatter();
        }
        return me;
    }

    public String toString(EvalException e) {
        return toString(e, getFormat(e, getErrCodeMessage(e.getErrorCode())));
    }

    public String getErrCodeMessage(int code) {
        switch (code) {
            case EvalException.PARSE_NOT_FOUND_END_OP /*1001*/:
                return "演算子「%0」が在りません。";
            case EvalException.PARSE_INVALID_OP /*1002*/:
                return "演算子の文法エラーです。";
            case EvalException.PARSE_INVALID_CHAR /*1003*/:
                return "未対応の識別子です。";
            case EvalException.PARSE_END_OF_STR /*1004*/:
                return "式の解釈の途中で文字列が終了しています。";
            case EvalException.PARSE_STILL_EXIST /*1005*/:
                return "式の解釈が終わりましたが文字列が残っています。";
            case EvalException.PARSE_NOT_FUNC /*1101*/:
                return "関数として使用できません。";
            case EvalException.EXP_FORBIDDEN_CALL /*2001*/:
                return "禁止されているメソッドを呼び出しました。";
            case EvalException.EXP_NOT_VARIABLE /*2002*/:
                return "変数として使用できません。";
            case EvalException.EXP_NOT_NUMBER /*2003*/:
                return "数値として使用できません。";
            case EvalException.EXP_NOT_LET /*2004*/:
                return "代入できません。";
            case EvalException.EXP_NOT_CHAR /*2005*/:
                return "文字として使用できません。";
            case EvalException.EXP_NOT_STRING /*2006*/:
                return "文字列として使用できません。";
            case EvalException.EXP_NOT_VAR_VALUE /*2101*/:
                return "変数の値が取得できません。";
            case EvalException.EXP_NOT_LET_VAR /*2102*/:
                return "変数に代入できません。";
            case EvalException.EXP_NOT_DEF_VAR /*2103*/:
                return "変数が未定義です。";
            case EvalException.EXP_NOT_DEF_OBJ /*2104*/:
                return "オブジェクトが未定義です。";
            case EvalException.EXP_NOT_ARR_VALUE /*2201*/:
                return "配列の値が取得できません。";
            case EvalException.EXP_NOT_LET_ARR /*2202*/:
                return "配列に代入できません。";
            case EvalException.EXP_NOT_FLD_VALUE /*2301*/:
                return "フィールドの値が取得できません。";
            case EvalException.EXP_NOT_LET_FIELD /*2302*/:
                return "フィールドに代入できません。";
            case EvalException.EXP_FUNC_CALL_ERROR /*2401*/:
                return "関数の呼び出しに失敗しました。";
            default:
                return "エラーが発生しました。";
        }
    }

    public String getFormat(EvalException e, String msgFmt) {
        StringBuilder fmt = new StringBuilder(128);
        fmt.append(msgFmt);
        String word = e.getWord();
        String string = e.getString();
        boolean bWord = false;
        if (word != null && word.length() > 0) {
            bWord = true;
            if (word.equals(string)) {
                bWord = false;
            }
        }
        if (bWord) {
            fmt.append(" %n=「%w」");
        }
        if (e.getPos() >= 0) {
            fmt.append(" pos=%p");
        }
        if (string != null) {
            fmt.append(" string=「%s」");
        }
        if (e.getCause() != null) {
            fmt.append(" cause by %e");
        }
        return fmt.toString();
    }

    public String toString(EvalException e, String format) {
        StringBuilder sb = new StringBuilder(256);
        int len = format.length();
        int i = 0;
        while (true) {
            if (i < len) {
                char c = format.charAt(i);
                if (c != '%') {
                    sb.append(c);
                } else if (i + 1 >= len) {
                    sb.append(c);
                } else {
                    i++;
                    char c2 = format.charAt(i);
                    switch (c2) {
                        case '%':
                            sb.append('%');
                            continue;
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                            int n = c2 - '0';
                            String[] msgOpt = e.getOption();
                            if (msgOpt == null || n >= msgOpt.length) {
                                sb.append('%');
                                sb.append(c2);
                                break;
                            } else {
                                sb.append(msgOpt[n]);
                                continue;
                            }
                            break;
                        case 'c':
                            sb.append(e.getErrorCode());
                            continue;
                        case 'e':
                            sb.append(e.getCause());
                            continue;
                        case 'n':
                            sb.append(e.getExpressionName());
                            continue;
                        case 'p':
                            sb.append(e.getPos());
                            continue;
                        case 's':
                            sb.append(e.getString());
                            continue;
                        case 'w':
                            sb.append(e.getWord());
                            continue;
                        default:
                            sb.append('%');
                            sb.append(c2);
                            continue;
                    }
                }
                i++;
            }
        }
        return sb.toString();
    }
}
