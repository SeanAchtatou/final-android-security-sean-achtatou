package jp.hishidama.eval.exp;

public class DivExpression extends Col2Expression {
    public static final String NAME = "div";

    public DivExpression() {
        setOperator("/");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected DivExpression(DivExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new DivExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.div(vl, vr);
    }
}
