package jp.hishidama.eval.exp;

public abstract class Col2OpeExpression extends Col2Expression {
    protected Col2OpeExpression() {
    }

    protected Col2OpeExpression(Col2Expression from, ShareExpValue s) {
        super(from, s);
    }

    /* access modifiers changed from: protected */
    public final Object operateObject(Object vl, Object vr) {
        throw new UnsupportedOperationException("このメソッドが呼ばれてはいけない");
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        this.expl = this.expl.replace();
        this.expr = this.expr.replace();
        return this.share.repl.replace2(this);
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replaceVar() {
        this.expl = this.expl.replaceVar();
        this.expr = this.expr.replaceVar();
        return this.share.repl.replaceVar2(this);
    }
}
