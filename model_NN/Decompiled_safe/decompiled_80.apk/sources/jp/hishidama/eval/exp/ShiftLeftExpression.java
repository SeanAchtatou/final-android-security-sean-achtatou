package jp.hishidama.eval.exp;

public class ShiftLeftExpression extends Col2Expression {
    public static final String NAME = "sla";

    public ShiftLeftExpression() {
        setOperator("<<");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected ShiftLeftExpression(ShiftLeftExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new ShiftLeftExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.shiftLeft(vl, vr);
    }
}
