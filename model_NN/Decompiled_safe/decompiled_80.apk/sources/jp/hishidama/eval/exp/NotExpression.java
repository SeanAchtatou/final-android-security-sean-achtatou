package jp.hishidama.eval.exp;

public class NotExpression extends Col1Expression {
    public static final String NAME = "not";

    public NotExpression() {
        setOperator("!");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected NotExpression(NotExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new NotExpression(this, s);
    }

    public Object eval() {
        Object x = this.exp.eval();
        Object r = this.share.oper.not(x);
        if (this.share.log != null) {
            this.share.log.logEval(getExpressionName(), x, r);
        }
        return r;
    }
}
