package jp.hishidama.eval.exp;

public abstract class Col1Expression extends AbstractExpression {
    protected AbstractExpression exp;

    public static AbstractExpression create(AbstractExpression exp2, String string, int pos, AbstractExpression x) {
        Col1Expression n = (Col1Expression) exp2;
        n.setExpression(x);
        n.setPos(string, pos);
        return n;
    }

    protected Col1Expression() {
    }

    protected Col1Expression(Col1Expression from, ShareExpValue s) {
        super(from, s);
        if (from.exp != null) {
            this.exp = from.exp.dup(s);
        }
    }

    public void setExpression(AbstractExpression x) {
        this.exp = x;
    }

    /* access modifiers changed from: protected */
    public final int getCols() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public final int getFirstPos() {
        return this.exp.getFirstPos();
    }

    /* access modifiers changed from: protected */
    public void search() {
        this.share.srch.search(this);
        if (!this.share.srch.end() && !this.share.srch.search1_begin(this) && !this.share.srch.end()) {
            this.exp.search();
            if (!this.share.srch.end()) {
                this.share.srch.search1_end(this);
            }
        }
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        this.exp = this.exp.replace();
        return this.share.repl.replace1(this);
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replaceVar() {
        this.exp = this.exp.replaceVar();
        return this.share.repl.replaceVar1(this);
    }

    public boolean equals(Object obj) {
        if (obj instanceof Col1Expression) {
            Col1Expression e = (Col1Expression) obj;
            if (getClass() == e.getClass()) {
                if (this.exp == null) {
                    if (e.exp == null) {
                        return true;
                    }
                    return false;
                } else if (e.exp == null) {
                    return false;
                } else {
                    return this.exp.equals(e.exp);
                }
            }
        }
        return false;
    }

    public int hashCode() {
        return getClass().hashCode() ^ this.exp.hashCode();
    }

    public void dump(int n) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append(' ');
        }
        sb.append(getOperator());
        System.out.println(sb.toString());
        if (this.exp != null) {
            this.exp.dump(n + 1);
        }
    }

    public String toString() {
        if (this.exp == null) {
            return getOperator();
        }
        StringBuilder sb = new StringBuilder();
        if (this.exp.getPriority() > this.prio) {
            sb.append(getOperator());
            sb.append(this.exp.toString());
        } else if (this.exp.getPriority() == this.prio) {
            sb.append(getOperator());
            sb.append(' ');
            sb.append(this.exp.toString());
        } else {
            sb.append(getOperator());
            sb.append(this.share.paren.getOperator());
            sb.append(this.exp.toString());
            sb.append(this.share.paren.getEndOperator());
        }
        return sb.toString();
    }
}
