package jp.hishidama.eval.exp;

public final class LetAndExpression extends BitAndExpression {
    public static final String NAME = "bitAndLet";

    public LetAndExpression() {
        setOperator("&=");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected LetAndExpression(LetAndExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new LetAndExpression(this, s);
    }

    public Object eval() {
        Object val = super.eval();
        this.expl.let(val, this.pos);
        return val;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        this.expl = this.expl.replaceVar();
        this.expr = this.expr.replace();
        return this.share.repl.replaceLet(this);
    }
}
