package jp.hishidama.eval.exp;

import java.util.List;

public class FuncArgExpression extends Col2OpeExpression {
    public static final String NAME = "funcArg";

    public FuncArgExpression() {
        setOperator(",");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected FuncArgExpression(FuncArgExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new FuncArgExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public void evalArgs(List<Object> args) {
        this.expl.evalArgs(args);
        this.expr.evalArgs(args);
    }

    /* access modifiers changed from: protected */
    public String toStringLeftSpace() {
        return "";
    }
}
