package jp.hishidama.eval.exp;

public class BitAndExpression extends Col2Expression {
    public static final String NAME = "bitAnd";

    public BitAndExpression() {
        setOperator("&");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected BitAndExpression(BitAndExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new BitAndExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.bitAnd(vl, vr);
    }
}
