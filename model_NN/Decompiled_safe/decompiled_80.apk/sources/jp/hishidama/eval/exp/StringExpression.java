package jp.hishidama.eval.exp;

import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.lex.Lex;
import jp.hishidama.util.CharUtil;

public class StringExpression extends WordExpression {
    public static final String NAME = "word";

    public String getExpressionName() {
        return NAME;
    }

    public static AbstractExpression create(Lex lex, int prio) {
        String str = lex.getWord();
        AbstractExpression exp = new StringExpression(CharUtil.escapeString(str, 1, str.length() - 2));
        exp.setPos(lex.getString(), lex.getPos());
        exp.setPriority(prio);
        exp.share = lex.getShare();
        return exp;
    }

    public StringExpression(String str) {
        super(str);
        setOperator("\"");
        setEndOperator("\"");
    }

    protected StringExpression(StringExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new StringExpression(this, s);
    }

    public static StringExpression create(AbstractExpression from, String word) {
        StringExpression n = new StringExpression(word);
        n.string = from.string;
        n.pos = from.pos;
        n.prio = from.prio;
        n.share = from.share;
        return n;
    }

    public Object eval() {
        try {
            return this.share.oper.string(this.word, this);
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException((int) EvalException.EXP_NOT_STRING, this, e2);
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof StringExpression) {
            return this.word.equals(((StringExpression) obj).word);
        }
        return false;
    }

    public int hashCode() {
        return this.word.hashCode();
    }

    public String toString() {
        return getOperator() + this.word + getEndOperator();
    }
}
