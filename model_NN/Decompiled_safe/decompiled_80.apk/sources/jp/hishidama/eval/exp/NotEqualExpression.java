package jp.hishidama.eval.exp;

public class NotEqualExpression extends Col2Expression {
    public static final String NAME = "ne";

    public NotEqualExpression() {
        setOperator("!=");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected NotEqualExpression(NotEqualExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new NotEqualExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.notEqual(vl, vr);
    }
}
