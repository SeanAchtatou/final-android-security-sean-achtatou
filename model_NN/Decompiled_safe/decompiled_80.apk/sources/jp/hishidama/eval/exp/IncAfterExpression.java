package jp.hishidama.eval.exp;

public class IncAfterExpression extends Col1AfterExpression {
    public static final String NAME = "incAfter";

    public IncAfterExpression() {
        setOperator("++");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected IncAfterExpression(IncAfterExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new IncAfterExpression(this, s);
    }

    public Object eval() {
        Object val = this.exp.eval();
        Object r = this.share.oper.inc(val, 1);
        this.exp.let(r, this.pos);
        if (this.share.log != null) {
            this.share.log.logEval(getExpressionName(), val, r);
        }
        return val;
    }
}
