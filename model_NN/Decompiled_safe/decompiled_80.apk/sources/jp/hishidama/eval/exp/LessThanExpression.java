package jp.hishidama.eval.exp;

public class LessThanExpression extends Col2Expression {
    public static final String NAME = "lt";

    public LessThanExpression() {
        setOperator("<");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected LessThanExpression(LessThanExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new LessThanExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.lessThan(vl, vr);
    }
}
