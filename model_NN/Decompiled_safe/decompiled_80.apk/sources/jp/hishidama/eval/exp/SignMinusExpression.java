package jp.hishidama.eval.exp;

public class SignMinusExpression extends Col1Expression {
    public static final String NAME = "signMinus";

    public SignMinusExpression() {
        setOperator("-");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected SignMinusExpression(SignMinusExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new SignMinusExpression(this, s);
    }

    public Object eval() {
        Object x = this.exp.eval();
        Object r = this.share.oper.signMinus(x);
        if (this.share.log != null) {
            this.share.log.logEval(getExpressionName(), x, r);
        }
        return r;
    }
}
