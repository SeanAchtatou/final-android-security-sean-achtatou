package jp.hishidama.eval.exp;

public class IfExpression extends Col3Expression {
    public static final String NAME = "if";

    public IfExpression() {
        setOperator("?");
        setEndOperator(":");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected IfExpression(IfExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new IfExpression(this, s);
    }

    public Object eval() {
        Object r;
        Object x = this.exp1.eval();
        if (this.share.oper.bool(x)) {
            r = this.exp2.eval();
        } else {
            r = this.exp3.eval();
        }
        if (this.share.log != null) {
            this.share.log.logEval(getExpressionName(), x, r);
        }
        return r;
    }
}
