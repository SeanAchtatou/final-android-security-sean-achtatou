package jp.hishidama.eval.exp;

import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.ref.Refactor;
import jp.hishidama.eval.srch.SearchAdapter;

public class Search4RefactorName extends SearchAdapter {
    protected Refactor ref;

    Search4RefactorName(Refactor ref2) {
        this.ref = ref2;
    }

    public void search0(WordExpression exp) {
        String name;
        if ((exp instanceof VariableExpression) && (name = this.ref.getNewName(null, exp.getWord())) != null) {
            exp.setWord(name);
        }
    }

    public boolean search2_2(Col2Expression exp) {
        if (!(exp instanceof FieldExpression)) {
            return false;
        }
        AbstractExpression exp1 = exp.expl;
        Object obj = exp1.getVariable();
        if (obj == null) {
            throw new EvalException(EvalException.EXP_NOT_DEF_OBJ, toString(), exp1, null);
        }
        AbstractExpression exp2 = exp.expr;
        String name = this.ref.getNewName(obj, exp2.getWord());
        if (name != null) {
            exp2.setWord(name);
        }
        return true;
    }

    public boolean searchFunc_2(FunctionExpression exp) {
        Object obj = null;
        if (exp.target != null) {
            obj = exp.target.getVariable();
        }
        String name = this.ref.getNewFuncName(obj, exp.name);
        if (name == null) {
            return false;
        }
        exp.name = name;
        return false;
    }
}
