package jp.hishidama.eval.exp;

public class PowerExpression extends Col2Expression {
    public static final String NAME = "power";

    public PowerExpression() {
        setOperator("**");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected PowerExpression(PowerExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new PowerExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.power(vl, vr);
    }
}
