package jp.hishidama.eval.exp;

public abstract class Col2Expression extends AbstractExpression {
    public AbstractExpression expl;
    public AbstractExpression expr;

    /* access modifiers changed from: protected */
    public abstract Object operateObject(Object obj, Object obj2);

    public static AbstractExpression create(AbstractExpression exp, String string, int pos, AbstractExpression x, AbstractExpression y) {
        Col2Expression n = (Col2Expression) exp;
        n.setExpression(x, y);
        n.setPos(string, pos);
        return n;
    }

    protected Col2Expression() {
    }

    protected Col2Expression(Col2Expression from, ShareExpValue s) {
        super(from, s);
        if (from.expl != null) {
            this.expl = from.expl.dup(s);
        }
        if (from.expr != null) {
            this.expr = from.expr.dup(s);
        }
    }

    public final void setExpression(AbstractExpression x, AbstractExpression y) {
        this.expl = x;
        this.expr = y;
    }

    /* access modifiers changed from: protected */
    public final int getCols() {
        return 2;
    }

    /* access modifiers changed from: protected */
    public final int getFirstPos() {
        return this.expl.getFirstPos();
    }

    public Object eval() {
        Object x = this.expl.eval();
        Object y = this.expr.eval();
        Object r = operateObject(x, y);
        if (this.share.log != null) {
            this.share.log.logEval(getExpressionName(), x, y, r);
        }
        return r;
    }

    /* access modifiers changed from: protected */
    public void search() {
        this.share.srch.search(this);
        if (!this.share.srch.end() && !this.share.srch.search2_begin(this) && !this.share.srch.end()) {
            this.expl.search();
            if (!this.share.srch.end() && !this.share.srch.search2_2(this) && !this.share.srch.end()) {
                this.expr.search();
                if (!this.share.srch.end()) {
                    this.share.srch.search2_end(this);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        this.expl = this.expl.replace();
        this.expr = this.expr.replace();
        return this.share.repl.replace2(this);
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replaceVar() {
        this.expl = this.expl.replaceVar();
        this.expr = this.expr.replaceVar();
        return this.share.repl.replaceVar2(this);
    }

    public boolean equals(Object obj) {
        if (obj instanceof Col2Expression) {
            Col2Expression e = (Col2Expression) obj;
            if (getClass() == e.getClass()) {
                if (!this.expl.equals(e.expl) || !this.expr.equals(e.expr)) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return (getClass().hashCode() ^ this.expl.hashCode()) ^ (this.expr.hashCode() * 2);
    }

    public void dump(int n) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append(' ');
        }
        sb.append(getOperator());
        System.out.println(sb.toString());
        this.expl.dump(n + 1);
        this.expr.dump(n + 1);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.expl.getPriority() < this.prio) {
            sb.append(this.share.paren.getOperator());
            sb.append(this.expl.toString());
            sb.append(this.share.paren.getEndOperator());
        } else {
            sb.append(this.expl.toString());
        }
        sb.append(toStringLeftSpace());
        sb.append(getOperator());
        sb.append(' ');
        if (this.expr.getPriority() < this.prio) {
            sb.append(this.share.paren.getOperator());
            sb.append(this.expr.toString());
            sb.append(this.share.paren.getEndOperator());
        } else {
            sb.append(this.expr.toString());
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public String toStringLeftSpace() {
        return " ";
    }
}
