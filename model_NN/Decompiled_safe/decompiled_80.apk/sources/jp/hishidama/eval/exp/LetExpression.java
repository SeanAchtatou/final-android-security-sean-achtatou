package jp.hishidama.eval.exp;

public class LetExpression extends Col2OpeExpression {
    public static final String NAME = "let";

    public LetExpression() {
        setOperator("=");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected LetExpression(LetExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new LetExpression(this, s);
    }

    public Object eval() {
        Object val = this.expr.eval();
        this.expl.let(val, this.pos);
        return val;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        this.expl = this.expl.replaceVar();
        this.expr = this.expr.replace();
        return this.share.repl.replaceLet(this);
    }
}
