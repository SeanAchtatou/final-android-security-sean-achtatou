package jp.hishidama.eval.exp;

public class EqualExpression extends Col2Expression {
    public static final String NAME = "eq";

    public EqualExpression() {
        setOperator("==");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected EqualExpression(EqualExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new EqualExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.equal(vl, vr);
    }
}
