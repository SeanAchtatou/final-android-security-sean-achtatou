package jp.hishidama.eval.exp;

public class ShiftRightLogicalExpression extends Col2Expression {
    public static final String NAME = "srl";

    public ShiftRightLogicalExpression() {
        setOperator(">>>");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected ShiftRightLogicalExpression(ShiftRightLogicalExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new ShiftRightLogicalExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.shiftRightLogical(vl, vr);
    }
}
