package jp.hishidama.eval.exp;

public class ShiftRightExpression extends Col2Expression {
    public static final String NAME = "sra";

    public ShiftRightExpression() {
        setOperator(">>");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected ShiftRightExpression(ShiftRightExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new ShiftRightExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.shiftRight(vl, vr);
    }
}
