package jp.hishidama.eval.exp;

public class CommaExpression extends Col2OpeExpression {
    public static final String NAME = "comma";

    public CommaExpression() {
        setOperator(",");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected CommaExpression(CommaExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new CommaExpression(this, s);
    }

    public Object eval() {
        this.expl.eval();
        return this.expr.eval();
    }

    /* access modifiers changed from: protected */
    public String toStringLeftSpace() {
        return "";
    }
}
