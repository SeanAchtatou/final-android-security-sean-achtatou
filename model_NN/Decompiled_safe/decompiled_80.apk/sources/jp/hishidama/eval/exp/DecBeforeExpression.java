package jp.hishidama.eval.exp;

public class DecBeforeExpression extends Col1Expression {
    public static final String NAME = "decBefore";

    public DecBeforeExpression() {
        setOperator("--");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected DecBeforeExpression(DecBeforeExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new DecBeforeExpression(this, s);
    }

    public Object eval() {
        Object val = this.exp.eval();
        Object r = this.share.oper.inc(val, -1);
        this.exp.let(r, this.pos);
        if (this.share.log != null) {
            this.share.log.logEval(getExpressionName(), val, r);
        }
        return r;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        this.exp = this.exp.replaceVar();
        return this.share.repl.replaceVar1(this);
    }
}
