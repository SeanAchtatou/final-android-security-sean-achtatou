package jp.hishidama.eval.exp;

import jp.hishidama.eval.EvalException;

public class FieldExpression extends Col2OpeExpression {
    public static final String NAME = "field";

    public FieldExpression() {
        setOperator(".");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected FieldExpression(FieldExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new FieldExpression(this, s);
    }

    public Object eval() {
        try {
            return this.share.oper.variable(getVariable(), this);
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException(EvalException.EXP_NOT_FLD_VALUE, toString(), this, e2);
        }
    }

    /* access modifiers changed from: protected */
    public Object getVariable() {
        try {
            return this.share.var.getFieldValue(this.expl.getVariable(), this.expl.toString(), this.expr.getWord(), this);
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException(EvalException.EXP_NOT_FLD_VALUE, toString(), this, e2);
        }
    }

    /* access modifiers changed from: protected */
    public void let(Object val, int pos) {
        try {
            this.share.var.setFieldValue(this.expl.getVariable(), this.expl.toString(), this.expr.getWord(), val, this);
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException(EvalException.EXP_NOT_LET_FIELD, toString(), this, e2);
        }
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        this.expl = this.expl.replaceVar();
        return this.share.repl.replace2((Col2OpeExpression) this);
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replaceVar() {
        this.expl = this.expl.replaceVar();
        return this.share.repl.replaceVar2((Col2OpeExpression) this);
    }

    public String toString() {
        return this.expl.toString() + '.' + this.expr.toString();
    }
}
