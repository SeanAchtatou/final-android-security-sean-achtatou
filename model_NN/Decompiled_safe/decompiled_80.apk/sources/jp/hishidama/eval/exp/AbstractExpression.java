package jp.hishidama.eval.exp;

import java.util.List;
import jp.hishidama.eval.EvalException;

public abstract class AbstractExpression {
    private String ope1;
    private String ope2;
    protected int pos = -1;
    protected int prio;
    public ShareExpValue share;
    protected String string = null;

    public abstract void dump(int i);

    public abstract AbstractExpression dup(ShareExpValue shareExpValue);

    public abstract boolean equals(Object obj);

    public abstract Object eval();

    /* access modifiers changed from: protected */
    public abstract int getCols();

    public abstract String getExpressionName();

    /* access modifiers changed from: protected */
    public abstract int getFirstPos();

    public abstract int hashCode();

    /* access modifiers changed from: protected */
    public abstract AbstractExpression replace();

    /* access modifiers changed from: protected */
    public abstract AbstractExpression replaceVar();

    /* access modifiers changed from: protected */
    public abstract void search();

    public abstract String toString();

    protected AbstractExpression() {
    }

    protected AbstractExpression(AbstractExpression from, ShareExpValue s) {
        this.string = from.string;
        this.pos = from.pos;
        this.prio = from.prio;
        if (s != null) {
            this.share = s;
        } else {
            this.share = from.share;
        }
        this.ope1 = from.ope1;
        this.ope2 = from.ope2;
    }

    public final String getOperator() {
        return this.ope1;
    }

    public final String getEndOperator() {
        return this.ope2;
    }

    public final void setOperator(String ope) {
        this.ope1 = ope;
    }

    public final void setEndOperator(String ope) {
        this.ope2 = ope;
    }

    public String getWord() {
        return getOperator();
    }

    /* access modifiers changed from: protected */
    public void setWord(String word) {
        throw new EvalException((int) EvalException.EXP_FORBIDDEN_CALL, this, (Throwable) null);
    }

    /* access modifiers changed from: protected */
    public final void setPos(String string2, int pos2) {
        this.string = string2;
        this.pos = pos2;
    }

    public final String getString() {
        return this.string;
    }

    public final int getPos() {
        return this.pos;
    }

    public final void setPriority(int prio2) {
        this.prio = prio2;
    }

    /* access modifiers changed from: protected */
    public final int getPriority() {
        return this.prio;
    }

    /* access modifiers changed from: protected */
    public void let(Object val, int pos2) {
        throw new EvalException(EvalException.EXP_NOT_LET, toString(), this, null);
    }

    /* access modifiers changed from: protected */
    public Object getVariable() {
        throw new EvalException(EvalException.EXP_NOT_VARIABLE, toString(), this, null);
    }

    /* access modifiers changed from: protected */
    public void evalArgs(List<Object> args) {
        args.add(eval());
    }

    public boolean same(AbstractExpression exp) {
        return same(getOperator(), exp.getOperator()) && same(getEndOperator(), exp.getEndOperator()) && equals(exp);
    }

    private static boolean same(String str1, String str2) {
        if (str1 == null) {
            return str2 == null;
        }
        return str1.equals(str2);
    }
}
