package jp.hishidama.eval.exp;

public class OrExpression extends Col2OpeExpression {
    public static final String NAME = "or";

    public OrExpression() {
        setOperator("||");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected OrExpression(OrExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new OrExpression(this, s);
    }

    public Object eval() {
        Object val = this.expl.eval();
        if (this.share.oper.bool(val)) {
            return val;
        }
        return this.expr.eval();
    }
}
