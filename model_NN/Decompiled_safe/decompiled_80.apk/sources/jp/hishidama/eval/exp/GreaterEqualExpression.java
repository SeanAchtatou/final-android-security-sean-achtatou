package jp.hishidama.eval.exp;

public class GreaterEqualExpression extends Col2Expression {
    public static final String NAME = "ge";

    public GreaterEqualExpression() {
        setOperator(">=");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected GreaterEqualExpression(GreaterEqualExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new GreaterEqualExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.greaterEqual(vl, vr);
    }
}
