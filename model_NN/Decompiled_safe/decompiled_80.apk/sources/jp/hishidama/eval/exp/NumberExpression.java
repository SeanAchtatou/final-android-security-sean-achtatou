package jp.hishidama.eval.exp;

import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.lex.Lex;

public class NumberExpression extends WordExpression {
    public static final String NAME = "number";

    public String getExpressionName() {
        return NAME;
    }

    public static AbstractExpression create(Lex lex, int prio) {
        AbstractExpression exp = new NumberExpression(lex.getWord());
        exp.setPos(lex.getString(), lex.getPos());
        exp.setPriority(prio);
        exp.share = lex.getShare();
        return exp;
    }

    public NumberExpression(String str) {
        super(str);
    }

    protected NumberExpression(NumberExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new NumberExpression(this, s);
    }

    public static NumberExpression create(AbstractExpression from, String word) {
        NumberExpression n = new NumberExpression(word);
        n.string = from.string;
        n.pos = from.pos;
        n.prio = from.prio;
        n.share = from.share;
        return n;
    }

    public Object eval() {
        try {
            return this.share.oper.number(this.word, this);
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException((int) EvalException.EXP_NOT_NUMBER, this, e2);
        }
    }
}
