package jp.hishidama.eval.exp;

import jp.hishidama.eval.Rule;
import jp.hishidama.eval.ref.Refactor;
import jp.hishidama.eval.repl.ReplaceAdapter;
import jp.hishidama.eval.rule.ShareRuleValue;

public class Replace4RefactorGetter extends ReplaceAdapter {
    protected Refactor ref;
    protected ShareRuleValue rule;

    Replace4RefactorGetter(Refactor ref2, Rule rule2) {
        this.ref = ref2;
        this.rule = (ShareRuleValue) rule2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression var(VariableExpression exp) {
        String name = this.ref.getNewName(null, exp.getWord());
        if (name == null) {
            return exp;
        }
        return this.rule.parse(name, exp.share);
    }

    /* access modifiers changed from: protected */
    public AbstractExpression field(FieldExpression exp) {
        Object obj = exp.expl.getVariable();
        if (obj != null) {
            AbstractExpression exp2 = exp.expr;
            String name = this.ref.getNewName(obj, exp2.getWord());
            if (name != null) {
                exp.expr = this.rule.parse(name, exp2.share);
            }
        }
        return exp;
    }

    public AbstractExpression replace0(WordExpression exp) {
        if (exp instanceof VariableExpression) {
            return var((VariableExpression) exp);
        }
        return exp;
    }

    public AbstractExpression replace2(Col2OpeExpression exp) {
        if (exp instanceof FieldExpression) {
            return field((FieldExpression) exp);
        }
        return exp;
    }
}
