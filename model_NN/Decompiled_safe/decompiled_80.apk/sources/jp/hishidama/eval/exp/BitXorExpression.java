package jp.hishidama.eval.exp;

public class BitXorExpression extends Col2Expression {
    public static final String NAME = "bitXor";

    public BitXorExpression() {
        setOperator("^");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected BitXorExpression(BitXorExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new BitXorExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.bitXor(vl, vr);
    }
}
