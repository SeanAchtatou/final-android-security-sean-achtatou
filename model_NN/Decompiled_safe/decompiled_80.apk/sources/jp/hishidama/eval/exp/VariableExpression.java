package jp.hishidama.eval.exp;

import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.lex.Lex;

public class VariableExpression extends WordExpression {
    public static final String NAME = "variable";

    public String getExpressionName() {
        return NAME;
    }

    public static AbstractExpression create(Lex lex, int prio) {
        AbstractExpression exp = new VariableExpression(lex.getWord());
        exp.setPos(lex.getString(), lex.getPos());
        exp.setPriority(prio);
        exp.share = lex.getShare();
        return exp;
    }

    public VariableExpression(String str) {
        super(str);
    }

    protected VariableExpression(VariableExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new VariableExpression(this, s);
    }

    public Object eval() {
        try {
            Object x = getVarValue();
            Object r = this.share.oper.variable(x, this);
            if (this.share.log != null) {
                this.share.log.logEval(getExpressionName(), x, r);
            }
            return r;
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException((int) EvalException.EXP_NOT_VAR_VALUE, this, e2);
        }
    }

    /* access modifiers changed from: protected */
    public void let(Object val, int pos) {
        String name = getWord();
        try {
            this.share.var.setValue(name, val);
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException(EvalException.EXP_NOT_LET_VAR, getExpressionName(), name, this.string, pos, e2);
        }
    }

    private Object getVarValue() {
        String word = getWord();
        try {
            Object val = this.share.var.getValue(word);
            if (val != null) {
                return val;
            }
            throw new EvalException(EvalException.EXP_NOT_DEF_VAR, word, this, null);
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException(EvalException.EXP_NOT_VAR_VALUE, word, this, e2);
        }
    }

    /* access modifiers changed from: protected */
    public Object getVariable() {
        try {
            return this.share.var.getValue(this.word);
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException(EvalException.EXP_NOT_VARIABLE, this.word, this, e2);
        }
    }
}
