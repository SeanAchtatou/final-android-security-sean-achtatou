package jp.hishidama.eval.exp;

public class GreaterThanExpression extends Col2Expression {
    public static final String NAME = "gt";

    public GreaterThanExpression() {
        setOperator(">");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected GreaterThanExpression(GreaterThanExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new GreaterThanExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.greaterThan(vl, vr);
    }
}
