package jp.hishidama.eval.exp;

public class BitOrExpression extends Col2Expression {
    public static final String NAME = "bitOr";

    public BitOrExpression() {
        setOperator("|");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected BitOrExpression(BitOrExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new BitOrExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.bitOr(vl, vr);
    }
}
