package jp.hishidama.eval.exp;

import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.lex.Lex;
import jp.hishidama.util.CharUtil;

public class CharExpression extends WordExpression {
    public static final String NAME = "char";

    public String getExpressionName() {
        return NAME;
    }

    public static AbstractExpression create(Lex lex, int prio) {
        String str = lex.getWord();
        AbstractExpression exp = new CharExpression(CharUtil.escapeString(str, 1, str.length() - 2));
        exp.setPos(lex.getString(), lex.getPos());
        exp.setPriority(prio);
        exp.share = lex.getShare();
        return exp;
    }

    public CharExpression(String str) {
        super(str);
        setOperator("'");
        setEndOperator("'");
    }

    protected CharExpression(CharExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new CharExpression(this, s);
    }

    public static CharExpression create(AbstractExpression from, String word) {
        CharExpression n = new CharExpression(word);
        n.string = from.string;
        n.pos = from.pos;
        n.prio = from.prio;
        n.share = from.share;
        return n;
    }

    public Object eval() {
        try {
            return this.share.oper.character(this.word, this);
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException((int) EvalException.EXP_NOT_CHAR, this, e2);
        }
    }

    public String toString() {
        return getOperator() + this.word + getEndOperator();
    }
}
