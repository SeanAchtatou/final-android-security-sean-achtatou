package jp.hishidama.eval.exp;

import jp.hishidama.eval.EvalThroughException;
import jp.hishidama.eval.Expression;
import jp.hishidama.eval.Rule;
import jp.hishidama.eval.func.InvokeFunction;
import jp.hishidama.eval.oper.JavaExOperator;
import jp.hishidama.eval.oper.Operator;
import jp.hishidama.eval.ref.Refactor;
import jp.hishidama.eval.repl.Replace;
import jp.hishidama.eval.srch.Search;
import jp.hishidama.eval.var.MapVariable;
import jp.hishidama.eval.var.Variable;

public class ShareExpValue extends Expression {
    public AbstractExpression paren;

    public void setAbstractExpression(AbstractExpression ae) {
        this.ae = ae;
    }

    public void initVar() {
        if (this.var == null) {
            this.var = new MapVariable(String.class, Object.class);
        }
    }

    public void initOper() {
        if (this.oper == null) {
            this.oper = new JavaExOperator();
        }
    }

    public void initFunc() {
        if (this.func == null) {
            this.func = new InvokeFunction();
        }
    }

    public Object eval() {
        initVar();
        initOper();
        initFunc();
        try {
            return this.ae.eval();
        } catch (EvalThroughException e) {
            throw ((RuntimeException) e.getCause());
        }
    }

    public void optimize(Variable var, Operator oper) {
        Operator bak = this.oper;
        this.oper = oper;
        try {
            optimize(var, new OptimizeReplacer());
        } finally {
            this.oper = bak;
        }
    }

    /* access modifiers changed from: protected */
    public void optimize(Variable var, Replace repl) {
        Variable bak = this.var;
        if (var == null) {
            var = new MapVariable(String.class, Object.class);
        }
        this.var = var;
        this.repl = repl;
        try {
            this.ae = this.ae.replace();
        } finally {
            this.var = bak;
        }
    }

    public void search(Search srch) {
        if (srch == null) {
            throw new NullPointerException();
        }
        this.srch = srch;
        this.ae.search();
    }

    public void refactorName(Refactor ref) {
        if (ref == null) {
            throw new NullPointerException();
        }
        this.srch = new Search4RefactorName(ref);
        this.ae.search();
    }

    public void refactorFunc(Refactor ref, Rule rule) {
        if (ref == null) {
            throw new NullPointerException();
        }
        this.repl = new Replace4RefactorGetter(ref, rule);
        this.ae.replace();
    }

    public boolean same(Expression obj) {
        if (!(obj instanceof ShareExpValue)) {
            return false;
        }
        if (!this.paren.same(((ShareExpValue) obj).paren) || !super.same(obj)) {
            return false;
        }
        return true;
    }

    public Expression dup() {
        ShareExpValue n = new ShareExpValue();
        n.ae = this.ae.dup(n);
        n.paren = this.paren.dup(n);
        return n;
    }
}
