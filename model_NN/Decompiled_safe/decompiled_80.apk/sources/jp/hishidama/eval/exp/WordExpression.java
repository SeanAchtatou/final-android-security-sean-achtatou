package jp.hishidama.eval.exp;

public abstract class WordExpression extends AbstractExpression {
    protected String word;

    protected WordExpression(String str) {
        this.word = str;
    }

    protected WordExpression(WordExpression from, ShareExpValue s) {
        super(from, s);
        this.word = from.word;
    }

    public String getWord() {
        return this.word;
    }

    /* access modifiers changed from: protected */
    public void setWord(String word2) {
        this.word = word2;
    }

    /* access modifiers changed from: protected */
    public int getCols() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getFirstPos() {
        return this.pos;
    }

    /* access modifiers changed from: protected */
    public void search() {
        this.share.srch.search(this);
        if (!this.share.srch.end()) {
            this.share.srch.search0(this);
        }
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        return this.share.repl.replace0(this);
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replaceVar() {
        return this.share.repl.replaceVar0(this);
    }

    public boolean equals(Object obj) {
        if (obj instanceof WordExpression) {
            WordExpression e = (WordExpression) obj;
            if (getClass() == e.getClass()) {
                return this.word.equals(e.word);
            }
        }
        return false;
    }

    public int hashCode() {
        return this.word.hashCode();
    }

    public void dump(int n) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            sb.append(' ');
        }
        sb.append(this.word);
        System.out.println(sb.toString());
    }

    public String toString() {
        return this.word;
    }
}
