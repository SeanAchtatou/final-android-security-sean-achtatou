package jp.hishidama.eval.exp;

import java.util.ArrayList;
import java.util.List;
import jp.hishidama.eval.EvalException;

public class FunctionExpression extends Col1Expression {
    public static final String NAME = "function";
    String name;
    protected AbstractExpression target;

    public String getExpressionName() {
        return NAME;
    }

    public static AbstractExpression create(AbstractExpression x, AbstractExpression args, int prio, ShareExpValue share) {
        AbstractExpression obj;
        if (x instanceof VariableExpression) {
            obj = null;
        } else if (x instanceof FieldExpression) {
            FieldExpression f = (FieldExpression) x;
            obj = f.expl;
            x = f.expr;
        } else {
            throw new EvalException(EvalException.PARSE_NOT_FUNC, x.toString(), x, null);
        }
        FunctionExpression f2 = new FunctionExpression(obj, x.getWord());
        f2.setExpression(args);
        f2.setPos(x.getString(), x.getPos());
        f2.setPriority(prio);
        f2.share = share;
        return f2;
    }

    public FunctionExpression() {
        setOperator("(");
        setEndOperator(")");
    }

    public FunctionExpression(AbstractExpression obj, String word) {
        this();
        this.target = obj;
        this.name = word;
    }

    protected FunctionExpression(FunctionExpression from, ShareExpValue s) {
        super(from, s);
        if (from.target != null) {
            this.target = from.target.dup(s);
        }
        this.name = from.name;
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new FunctionExpression(this, s);
    }

    public Object eval() {
        Object r;
        Object obj = null;
        if (this.target != null) {
            obj = this.target.getVariable();
        }
        List<Object> args = evalArgsObject();
        try {
            Object[] arr = args.toArray(new Object[args.size()]);
            if (this.target != null) {
                r = this.share.func.eval(obj, this.name, arr);
            } else {
                r = this.share.func.eval(this.name, arr);
            }
            if (this.share.log != null) {
                this.share.log.logEvalFunction(getExpressionName(), this.name, arr, r);
            }
            return r;
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException(EvalException.EXP_FUNC_CALL_ERROR, this.name, this, e2);
        }
    }

    private List<Object> evalArgsObject() {
        List<Object> args = new ArrayList<>();
        if (this.exp != null) {
            this.exp.evalArgs(args);
        }
        return args;
    }

    /* access modifiers changed from: protected */
    public Object getVariable() {
        return eval();
    }

    /* access modifiers changed from: protected */
    public void search() {
        this.share.srch.search(this);
        if (!this.share.srch.end() && !this.share.srch.searchFunc_begin(this) && !this.share.srch.end()) {
            if (this.target != null) {
                this.target.search();
                if (this.share.srch.end()) {
                    return;
                }
            }
            if (!this.share.srch.searchFunc_2(this) && !this.share.srch.end()) {
                if (this.exp != null) {
                    this.exp.search();
                    if (this.share.srch.end()) {
                        return;
                    }
                }
                this.share.srch.searchFunc_end(this);
            }
        }
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        if (this.target != null) {
            this.target = this.target.replace();
        }
        if (this.exp != null) {
            this.exp = this.exp.replace();
        }
        return this.share.repl.replaceFunc(this);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof FunctionExpression)) {
            return false;
        }
        FunctionExpression e = (FunctionExpression) obj;
        if (!this.name.equals(e.name) || !equals(this.target, e.target) || !equals(this.exp, e.exp)) {
            return false;
        }
        return true;
    }

    private static boolean equals(AbstractExpression e1, AbstractExpression e2) {
        if (e1 == null) {
            if (e2 == null) {
                return true;
            }
            return false;
        } else if (e2 != null) {
            return e1.equals(e2);
        } else {
            return false;
        }
    }

    public int hashCode() {
        int t;
        int a;
        if (this.target != null) {
            t = this.target.hashCode();
        } else {
            t = 0;
        }
        if (this.exp != null) {
            a = this.exp.hashCode();
        } else {
            a = 0;
        }
        return (this.name.hashCode() ^ t) ^ (a * 2);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.target != null) {
            sb.append(this.target.toString());
            sb.append('.');
        }
        sb.append(this.name);
        sb.append('(');
        if (this.exp != null) {
            sb.append(this.exp.toString());
        }
        sb.append(')');
        return sb.toString();
    }
}
