package jp.hishidama.eval.exp;

public final class LetShiftLeftExpression extends ShiftLeftExpression {
    public static final String NAME = "slaLet";

    public LetShiftLeftExpression() {
        setOperator("<<=");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected LetShiftLeftExpression(LetShiftLeftExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new LetShiftLeftExpression(this, s);
    }

    public Object eval() {
        Object val = super.eval();
        this.expl.let(val, this.pos);
        return val;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        this.expl = this.expl.replaceVar();
        this.expr = this.expr.replace();
        return this.share.repl.replaceLet(this);
    }
}
