package jp.hishidama.eval.exp;

public class MultExpression extends Col2Expression {
    public static final String NAME = "mult";

    public MultExpression() {
        setOperator("*");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected MultExpression(MultExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new MultExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.mult(vl, vr);
    }
}
