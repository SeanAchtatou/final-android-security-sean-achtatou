package jp.hishidama.eval.exp;

import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.ref.Refactor;
import jp.hishidama.eval.repl.ReplaceAdapter;

@Deprecated
public class Replace4RefactorName extends ReplaceAdapter {
    protected Refactor ref;

    Replace4RefactorName(Refactor ref2) {
        this.ref = ref2;
    }

    /* access modifiers changed from: protected */
    public void var(VariableExpression exp) {
        String name = this.ref.getNewName(null, exp.getWord());
        if (name != null) {
            exp.setWord(name);
        }
    }

    /* access modifiers changed from: protected */
    public void field(FieldExpression exp) {
        AbstractExpression exp1 = exp.expl;
        Object obj = exp1.getVariable();
        if (obj == null) {
            throw new EvalException(EvalException.EXP_NOT_DEF_OBJ, toString(), exp1, null);
        }
        AbstractExpression exp2 = exp.expr;
        String name = this.ref.getNewName(obj, exp2.getWord());
        if (name != null) {
            exp2.setWord(name);
        }
    }

    /* access modifiers changed from: protected */
    public void func(FunctionExpression exp) {
        Object obj = null;
        if (exp.target != null) {
            obj = exp.target.getVariable();
        }
        String name = this.ref.getNewFuncName(obj, exp.name);
        if (name != null) {
            exp.name = name;
        }
    }

    public AbstractExpression replace0(WordExpression exp) {
        if (exp instanceof VariableExpression) {
            var((VariableExpression) exp);
        }
        return exp;
    }

    public AbstractExpression replace2(Col2Expression exp) {
        if (exp instanceof FieldExpression) {
            field((FieldExpression) exp);
        }
        return exp;
    }

    public AbstractExpression replaceFunc(FunctionExpression exp) {
        func(exp);
        return exp;
    }

    public AbstractExpression replaceVar(AbstractExpression exp) {
        if (exp instanceof VariableExpression) {
            var((VariableExpression) exp);
        } else if (exp instanceof FieldExpression) {
            field((FieldExpression) exp);
        } else if (exp instanceof FunctionExpression) {
            func((FunctionExpression) exp);
        }
        return exp;
    }
}
