package jp.hishidama.eval.exp;

import jp.hishidama.eval.repl.ReplaceAdapter;

public class OptimizeReplacer extends ReplaceAdapter {
    /* access modifiers changed from: protected */
    public boolean isConst(AbstractExpression x) {
        return (x instanceof NumberExpression) || (x instanceof StringExpression) || (x instanceof CharExpression);
    }

    /* access modifiers changed from: protected */
    public boolean isTrue(AbstractExpression x) {
        return x.share.oper.bool(x.eval());
    }

    /* access modifiers changed from: protected */
    public AbstractExpression toConst(AbstractExpression exp) {
        try {
            Object val = exp.eval();
            if (val instanceof String) {
                return StringExpression.create(exp, (String) val);
            }
            if (val instanceof Character) {
                return CharExpression.create(exp, val.toString());
            }
            return val instanceof Number ? NumberExpression.create(exp, val.toString()) : exp;
        } catch (Exception e) {
            return exp;
        }
    }

    public AbstractExpression replace0(WordExpression exp) {
        if (exp instanceof VariableExpression) {
            return toConst(exp);
        }
        return exp;
    }

    public AbstractExpression replace1(Col1Expression exp) {
        if (exp instanceof ParenExpression) {
            return exp.exp;
        }
        if (exp instanceof SignPlusExpression) {
            return exp.exp;
        }
        return isConst(exp.exp) ? toConst(exp) : exp;
    }

    public AbstractExpression replace2(Col2Expression exp) {
        boolean const_l = isConst(exp.expl);
        boolean const_r = isConst(exp.expr);
        if (!const_l || !const_r) {
            return exp;
        }
        return toConst(exp);
    }

    public AbstractExpression replace2(Col2OpeExpression exp) {
        if (exp instanceof ArrayExpression) {
            if (isConst(exp.expr)) {
                return toConst(exp);
            }
            return exp;
        } else if (exp instanceof FieldExpression) {
            return toConst(exp);
        } else {
            boolean const_l = isConst(exp.expl);
            if (exp instanceof AndExpression) {
                if (!const_l) {
                    return exp;
                }
                if (isTrue(exp.expl)) {
                    return exp.expr;
                }
                return exp.expl;
            } else if (exp instanceof OrExpression) {
                if (!const_l) {
                    return exp;
                }
                if (isTrue(exp.expl)) {
                    return exp.expl;
                }
                return exp.expr;
            } else if (exp instanceof CommaExpression) {
                return const_l ? exp.expr : exp;
            } else {
                return exp;
            }
        }
    }

    public AbstractExpression replace3(Col3Expression exp) {
        if (!isConst(exp.exp1)) {
            return exp;
        }
        if (isTrue(exp.exp1)) {
            return exp.exp2;
        }
        return exp.exp3;
    }
}
