package jp.hishidama.eval.exp;

public class LetShiftRightExpression extends ShiftRightExpression {
    public static final String NAME = "sratLet";

    public LetShiftRightExpression() {
        setOperator(">>=");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected LetShiftRightExpression(LetShiftRightExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new LetShiftRightExpression(this, s);
    }

    public Object eval() {
        Object val = super.eval();
        this.expl.let(val, this.pos);
        return val;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        this.expl = this.expl.replaceVar();
        this.expr = this.expr.replace();
        return this.share.repl.replaceLet(this);
    }
}
