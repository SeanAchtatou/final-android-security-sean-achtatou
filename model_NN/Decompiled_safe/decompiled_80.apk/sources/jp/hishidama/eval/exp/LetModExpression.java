package jp.hishidama.eval.exp;

public class LetModExpression extends ModExpression {
    public static final String NAME = "modLet";

    public LetModExpression() {
        setOperator("%=");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected LetModExpression(LetModExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new LetModExpression(this, s);
    }

    public Object eval() {
        Object val = super.eval();
        this.expl.let(val, this.pos);
        return val;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        this.expl = this.expl.replaceVar();
        this.expr = this.expr.replace();
        return this.share.repl.replaceLet(this);
    }
}
