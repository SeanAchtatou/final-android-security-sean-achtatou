package jp.hishidama.eval.exp;

public class MinusExpression extends Col2Expression {
    public static final String NAME = "minus";

    public MinusExpression() {
        setOperator("-");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected MinusExpression(MinusExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new MinusExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.minus(vl, vr);
    }
}
