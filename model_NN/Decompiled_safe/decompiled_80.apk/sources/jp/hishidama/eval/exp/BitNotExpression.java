package jp.hishidama.eval.exp;

public class BitNotExpression extends Col1Expression {
    public static final String NAME = "bitNot";

    public BitNotExpression() {
        setOperator("~");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected BitNotExpression(BitNotExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new BitNotExpression(this, s);
    }

    public Object eval() {
        Object x = this.exp.eval();
        Object r = this.share.oper.bitNot(x);
        if (this.share.log != null) {
            this.share.log.logEval(getExpressionName(), x, r);
        }
        return r;
    }
}
