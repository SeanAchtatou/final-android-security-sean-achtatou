package jp.hishidama.eval.exp;

public class IncBeforeExpression extends Col1Expression {
    public static final String NAME = "incBefore";

    public IncBeforeExpression() {
        setOperator("++");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected IncBeforeExpression(IncBeforeExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new IncBeforeExpression(this, s);
    }

    public Object eval() {
        Object val = this.exp.eval();
        Object r = this.share.oper.inc(val, 1);
        this.exp.let(r, this.pos);
        if (this.share.log != null) {
            this.share.log.logEval(getExpressionName(), val, r);
        }
        return r;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        this.exp = this.exp.replaceVar();
        return this.share.repl.replaceVar1(this);
    }
}
