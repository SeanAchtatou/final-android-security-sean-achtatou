package jp.hishidama.eval.exp;

public class SignPlusExpression extends Col1Expression {
    public static final String NAME = "signPlus";

    public SignPlusExpression() {
        setOperator("+");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected SignPlusExpression(SignPlusExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new SignPlusExpression(this, s);
    }

    public Object eval() {
        Object x = this.exp.eval();
        Object r = this.share.oper.signPlus(x);
        if (this.share.log != null) {
            this.share.log.logEval(getExpressionName(), x, r);
        }
        return r;
    }
}
