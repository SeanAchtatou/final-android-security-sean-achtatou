package jp.hishidama.eval.exp;

public class ParenExpression extends Col1Expression {
    public static final String NAME = "paren";

    public ParenExpression() {
        setOperator("(");
        setEndOperator(")");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected ParenExpression(ParenExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new ParenExpression(this, s);
    }

    public Object eval() {
        Object r = this.exp.eval();
        if (this.share.log != null) {
            this.share.log.logEval(getExpressionName(), r);
        }
        return r;
    }

    public String toString() {
        if (this.exp == null) {
            return "";
        }
        return String.valueOf(getOperator()) + this.exp.toString() + getEndOperator();
    }
}
