package jp.hishidama.eval.exp;

public class AndExpression extends Col2OpeExpression {
    public static final String NAME = "and";

    public AndExpression() {
        setOperator("&&");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected AndExpression(AndExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new AndExpression(this, s);
    }

    public Object eval() {
        Object val = this.expl.eval();
        if (!this.share.oper.bool(val)) {
            return val;
        }
        return this.expr.eval();
    }
}
