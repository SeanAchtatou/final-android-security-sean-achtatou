package jp.hishidama.eval.exp;

public class ModExpression extends Col2Expression {
    public static final String NAME = "mod";

    public ModExpression() {
        setOperator("%");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected ModExpression(ModExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new ModExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.mod(vl, vr);
    }
}
