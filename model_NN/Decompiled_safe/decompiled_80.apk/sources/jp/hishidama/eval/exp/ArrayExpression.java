package jp.hishidama.eval.exp;

import jp.hishidama.eval.EvalException;

public class ArrayExpression extends Col2OpeExpression {
    public static final String NAME = "array";

    public ArrayExpression() {
        setOperator("[");
        setEndOperator("]");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected ArrayExpression(ArrayExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new ArrayExpression(this, s);
    }

    public Object eval() {
        try {
            return this.share.oper.variable(getVariable(), this);
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException((int) EvalException.EXP_NOT_VAR_VALUE, this, e2);
        }
    }

    /* access modifiers changed from: protected */
    public Object getVariable() {
        try {
            return this.share.var.getArrayValue(this.expl.getVariable(), this.expl.toString(), this.expr.eval(), this);
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException(EvalException.EXP_NOT_ARR_VALUE, toString(), this, e2);
        }
    }

    /* access modifiers changed from: protected */
    public void let(Object val, int pos) {
        try {
            this.share.var.setArrayValue(this.expl.getVariable(), this.expl.toString(), this.expr.eval(), val, this);
        } catch (EvalException e) {
            throw e;
        } catch (Exception e2) {
            throw new EvalException(EvalException.EXP_NOT_LET_ARR, toString(), this, e2);
        }
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replaceVar() {
        this.expl = this.expl.replaceVar();
        this.expr = this.expr.replace();
        return this.share.repl.replaceVar2((Col2OpeExpression) this);
    }

    public String toString() {
        return this.expl.toString() + '[' + this.expr.toString() + ']';
    }
}
