package jp.hishidama.eval.exp;

public class PlusExpression extends Col2Expression {
    public static final String NAME = "plus";

    public PlusExpression() {
        setOperator("+");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected PlusExpression(PlusExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new PlusExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.plus(vl, vr);
    }
}
