package jp.hishidama.eval.exp;

public class LetPlusExpression extends PlusExpression {
    public static final String NAME = "plusLet";

    public LetPlusExpression() {
        setOperator("+=");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected LetPlusExpression(LetPlusExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new LetPlusExpression(this, s);
    }

    public Object eval() {
        Object val = super.eval();
        this.expl.let(val, this.pos);
        return val;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        this.expl = this.expl.replaceVar();
        this.expr = this.expr.replace();
        return this.share.repl.replaceLet(this);
    }
}
