package jp.hishidama.eval.exp;

public class LetMultExpression extends MultExpression {
    public static final String NAME = "multLet";

    public LetMultExpression() {
        setOperator("*=");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected LetMultExpression(LetMultExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new LetMultExpression(this, s);
    }

    public Object eval() {
        Object val = super.eval();
        this.expl.let(val, this.pos);
        return val;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression replace() {
        this.expl = this.expl.replaceVar();
        this.expr = this.expr.replace();
        return this.share.repl.replaceLet(this);
    }
}
