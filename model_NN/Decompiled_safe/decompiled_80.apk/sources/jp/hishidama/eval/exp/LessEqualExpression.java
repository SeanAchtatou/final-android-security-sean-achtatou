package jp.hishidama.eval.exp;

public class LessEqualExpression extends Col2Expression {
    public static final String NAME = "le";

    public LessEqualExpression() {
        setOperator("<=");
    }

    public String getExpressionName() {
        return NAME;
    }

    protected LessEqualExpression(LessEqualExpression from, ShareExpValue s) {
        super(from, s);
    }

    public AbstractExpression dup(ShareExpValue s) {
        return new LessEqualExpression(this, s);
    }

    /* access modifiers changed from: protected */
    public Object operateObject(Object vl, Object vr) {
        return this.share.oper.lessEqual(vl, vr);
    }
}
