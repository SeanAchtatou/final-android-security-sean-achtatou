package jp.hishidama.eval.rule;

import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.Col2Expression;
import jp.hishidama.eval.lex.Lex;

public class Col2Rule extends AbstractRule {
    public Col2Rule(ShareRuleValue share) {
        super(share);
    }

    /* access modifiers changed from: protected */
    public AbstractExpression parse(Lex lex) {
        AbstractExpression x = this.nextRule.parse(lex);
        while (true) {
            switch (lex.getType()) {
                case Lex.TYPE_OPE:
                    String ope = lex.getOperator();
                    if (!isMyOperator(ope)) {
                        break;
                    } else {
                        x = Col2Expression.create(newExpression(ope, lex.getShare()), lex.getString(), lex.getPos(), x, this.nextRule.parse(lex.next()));
                    }
            }
        }
        return x;
    }
}
