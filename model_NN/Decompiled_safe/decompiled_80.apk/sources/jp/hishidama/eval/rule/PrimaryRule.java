package jp.hishidama.eval.rule;

import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.CharExpression;
import jp.hishidama.eval.exp.Col1Expression;
import jp.hishidama.eval.exp.NumberExpression;
import jp.hishidama.eval.exp.StringExpression;
import jp.hishidama.eval.exp.VariableExpression;
import jp.hishidama.eval.lex.Lex;

public class PrimaryRule extends AbstractRule {
    public PrimaryRule(ShareRuleValue share) {
        super(share);
    }

    public final AbstractExpression parse(Lex lex) {
        switch (lex.getType()) {
            case Lex.TYPE_WORD:
                AbstractExpression w = VariableExpression.create(lex, this.prio);
                lex.next();
                return w;
            case Lex.TYPE_NUM:
                AbstractExpression n = NumberExpression.create(lex, this.prio);
                lex.next();
                return n;
            case Lex.TYPE_OPE:
                String ope = lex.getOperator();
                int pos = lex.getPos();
                if (!isMyOperator(ope)) {
                    throw new EvalException(EvalException.PARSE_INVALID_OP, lex);
                } else if (ope.equals(this.share.paren.getOperator())) {
                    return parseParen(lex, ope, pos);
                } else {
                    return Col1Expression.create(newExpression(ope, lex.getShare()), lex.getString(), pos, parse(lex.next()));
                }
            case Lex.TYPE_STRING:
                AbstractExpression s = StringExpression.create(lex, this.prio);
                lex.next();
                return s;
            case Lex.TYPE_CHAR:
                AbstractExpression c = CharExpression.create(lex, this.prio);
                lex.next();
                return c;
            case Lex.TYPE_EOF:
                throw new EvalException(EvalException.PARSE_END_OF_STR, lex);
            default:
                throw new EvalException(EvalException.PARSE_INVALID_CHAR, lex);
        }
    }

    /* access modifiers changed from: protected */
    public AbstractExpression parseParen(Lex lex, String ope, int pos) {
        AbstractExpression s = this.share.topRule.parse(lex.next());
        if (!lex.isOperator(this.share.paren.getEndOperator())) {
            throw new EvalException((int) EvalException.PARSE_NOT_FOUND_END_OP, new String[]{this.share.paren.getEndOperator()}, lex);
        }
        lex.next();
        return Col1Expression.create(newExpression(ope, lex.getShare()), lex.getString(), pos, s);
    }
}
