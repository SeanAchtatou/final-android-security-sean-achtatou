package jp.hishidama.eval.rule;

import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.Col3Expression;
import jp.hishidama.eval.lex.Lex;

public class Col3Rule extends AbstractRule {
    public AbstractExpression cond;

    public Col3Rule(ShareRuleValue share) {
        super(share);
    }

    /* access modifiers changed from: protected */
    public AbstractExpression parse(Lex lex) {
        AbstractExpression x = this.nextRule.parse(lex);
        switch (lex.getType()) {
            case Lex.TYPE_OPE:
                String ope = lex.getOperator();
                int pos = lex.getPos();
                if (isMyOperator(ope) && lex.isOperator(this.cond.getOperator())) {
                    x = parseCond(lex, x, ope, pos);
                }
                return x;
            default:
                return x;
        }
    }

    /* access modifiers changed from: protected */
    public AbstractExpression parseCond(Lex lex, AbstractExpression x, String ope, int pos) {
        AbstractExpression y = parse(lex.next());
        if (!lex.isOperator(this.cond.getEndOperator())) {
            throw new EvalException((int) EvalException.PARSE_NOT_FOUND_END_OP, new String[]{this.cond.getEndOperator()}, lex);
        }
        return Col3Expression.create(newExpression(ope, lex.getShare()), lex.getString(), pos, x, y, parse(lex.next()));
    }
}
