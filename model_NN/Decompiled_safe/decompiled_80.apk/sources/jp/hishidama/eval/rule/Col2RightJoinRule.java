package jp.hishidama.eval.rule;

import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.Col2Expression;
import jp.hishidama.eval.lex.Lex;

public class Col2RightJoinRule extends AbstractRule {
    public Col2RightJoinRule(ShareRuleValue share) {
        super(share);
    }

    /* access modifiers changed from: protected */
    public AbstractExpression parse(Lex lex) {
        AbstractExpression x = this.nextRule.parse(lex);
        switch (lex.getType()) {
            case Lex.TYPE_OPE:
                String ope = lex.getOperator();
                if (isMyOperator(ope)) {
                    x = Col2Expression.create(newExpression(ope, lex.getShare()), lex.getString(), lex.getPos(), x, parse(lex.next()));
                }
                return x;
            default:
                return x;
        }
    }
}
