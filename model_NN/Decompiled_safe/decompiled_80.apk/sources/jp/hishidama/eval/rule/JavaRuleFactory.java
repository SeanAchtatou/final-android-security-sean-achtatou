package jp.hishidama.eval.rule;

import jp.hishidama.eval.ExpRuleFactory;
import jp.hishidama.eval.exp.AbstractExpression;

public class JavaRuleFactory extends ExpRuleFactory {
    private static JavaRuleFactory me;

    public static ExpRuleFactory getInstance() {
        if (me == null) {
            me = new JavaRuleFactory();
        }
        return me;
    }

    /* access modifiers changed from: protected */
    public AbstractRule createCommaRule(ShareRuleValue share) {
        return null;
    }

    /* access modifiers changed from: protected */
    public AbstractRule createPowerRule(ShareRuleValue share) {
        return null;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetPowerExpression() {
        return null;
    }
}
