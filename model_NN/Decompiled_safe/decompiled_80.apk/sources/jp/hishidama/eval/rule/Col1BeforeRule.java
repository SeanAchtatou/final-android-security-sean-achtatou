package jp.hishidama.eval.rule;

import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.Col1Expression;
import jp.hishidama.eval.lex.Lex;

public class Col1BeforeRule extends AbstractRule {
    public Col1BeforeRule(ShareRuleValue share) {
        super(share);
    }

    public AbstractExpression parse(Lex lex) {
        switch (lex.getType()) {
            case Lex.TYPE_OPE:
                String ope = lex.getOperator();
                if (!isMyOperator(ope)) {
                    return this.nextRule.parse(lex);
                }
                return Col1Expression.create(newExpression(ope, lex.getShare()), lex.getString(), lex.getPos(), parse(lex.next()));
            default:
                return this.nextRule.parse(lex);
        }
    }
}
