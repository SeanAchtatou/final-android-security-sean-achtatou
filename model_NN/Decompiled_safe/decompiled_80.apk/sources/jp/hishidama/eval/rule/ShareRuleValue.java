package jp.hishidama.eval.rule;

import java.util.List;
import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.Expression;
import jp.hishidama.eval.Rule;
import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.ShareExpValue;
import jp.hishidama.eval.lex.Lex;
import jp.hishidama.eval.lex.LexFactory;
import jp.hishidama.eval.oper.Operator;
import jp.hishidama.eval.ref.Refactor;
import jp.hishidama.eval.srch.Search;
import jp.hishidama.eval.var.Variable;

public class ShareRuleValue extends Rule {
    public AbstractRule funcArgRule;
    public LexFactory lexFactory;
    protected List<String>[] opeList = new List[4];
    public AbstractExpression paren;
    public AbstractRule topRule;

    public Expression parse(String str) {
        if (str == null) {
            return null;
        }
        if (str.trim().length() <= 0) {
            return new EmptyExpression();
        }
        ShareExpValue exp = new ShareExpValue();
        exp.setAbstractExpression(parse(str, exp));
        if (this.defaultVar != null) {
            exp.setVariable(this.defaultVar);
        }
        if (this.defaultFunc != null) {
            exp.setFunction(this.defaultFunc);
        }
        if (this.defaultOper != null) {
            exp.setOperator(this.defaultOper);
        }
        if (this.defaultLog != null) {
            exp.setEvalLog(this.defaultLog);
        }
        return exp;
    }

    public AbstractExpression parse(String str, ShareExpValue exp) {
        if (str == null) {
            return null;
        }
        Lex lex = this.lexFactory.create(str, this.opeList, this, exp);
        lex.check();
        AbstractExpression x = this.topRule.parse(lex);
        if (lex.getType() == Integer.MAX_VALUE) {
            return x;
        }
        throw new EvalException(EvalException.PARSE_STILL_EXIST, lex);
    }

    class EmptyExpression extends Expression {
        EmptyExpression() {
        }

        public Object eval() {
            return null;
        }

        public void optimize(Variable var, Operator oper) {
        }

        public void search(Search srch) {
        }

        public void refactorName(Refactor ref) {
        }

        public void refactorFunc(Refactor ref, Rule rule) {
        }

        public Expression dup() {
            return new EmptyExpression();
        }

        public boolean same(Expression obj) {
            if (obj instanceof EmptyExpression) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "";
        }
    }
}
