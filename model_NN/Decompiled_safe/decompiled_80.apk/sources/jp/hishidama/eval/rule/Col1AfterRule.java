package jp.hishidama.eval.rule;

import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.Col1Expression;
import jp.hishidama.eval.exp.Col2Expression;
import jp.hishidama.eval.exp.FunctionExpression;
import jp.hishidama.eval.lex.Lex;

public class Col1AfterRule extends AbstractRule {
    public AbstractExpression array;
    public AbstractExpression field;
    public AbstractExpression func;

    public Col1AfterRule(ShareRuleValue share) {
        super(share);
    }

    public AbstractExpression parse(Lex lex) {
        AbstractExpression x = this.nextRule.parse(lex);
        while (true) {
            switch (lex.getType()) {
                case Lex.TYPE_OPE:
                    String ope = lex.getOperator();
                    int pos = lex.getPos();
                    if (!isMyOperator(ope)) {
                        break;
                    } else if (lex.isOperator(this.func.getOperator())) {
                        x = parseFunc(lex, x);
                    } else if (lex.isOperator(this.array.getOperator())) {
                        x = parseArray(lex, x, ope, pos);
                    } else if (lex.isOperator(this.field.getOperator())) {
                        x = parseField(lex, x, ope, pos);
                    } else {
                        x = Col1Expression.create(newExpression(ope, lex.getShare()), lex.getString(), pos, x);
                        lex.next();
                    }
            }
        }
        return x;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression parseFunc(Lex lex, AbstractExpression x) {
        AbstractExpression a = null;
        lex.next();
        if (!lex.isOperator(this.func.getEndOperator())) {
            a = this.share.funcArgRule.parse(lex);
            if (!lex.isOperator(this.func.getEndOperator())) {
                throw new EvalException((int) EvalException.PARSE_NOT_FOUND_END_OP, new String[]{this.func.getEndOperator()}, lex);
            }
        }
        lex.next();
        return FunctionExpression.create(x, a, this.prio, lex.getShare());
    }

    /* access modifiers changed from: protected */
    public AbstractExpression parseArray(Lex lex, AbstractExpression x, String ope, int pos) {
        AbstractExpression y = this.share.topRule.parse(lex.next());
        if (!lex.isOperator(this.array.getEndOperator())) {
            throw new EvalException((int) EvalException.PARSE_NOT_FOUND_END_OP, new String[]{this.array.getEndOperator()}, lex);
        }
        lex.next();
        return Col2Expression.create(newExpression(ope, lex.getShare()), lex.getString(), pos, x, y);
    }

    /* access modifiers changed from: protected */
    public AbstractExpression parseField(Lex lex, AbstractExpression x, String ope, int pos) {
        return Col2Expression.create(newExpression(ope, lex.getShare()), lex.getString(), pos, x, this.nextRule.parse(lex.next()));
    }
}
