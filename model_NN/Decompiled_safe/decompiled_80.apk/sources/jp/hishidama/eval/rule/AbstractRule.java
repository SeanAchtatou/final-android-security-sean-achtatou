package jp.hishidama.eval.rule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.ParenExpression;
import jp.hishidama.eval.exp.ShareExpValue;
import jp.hishidama.eval.lex.Lex;

public abstract class AbstractRule {
    public AbstractRule nextRule;
    private final Map<String, AbstractExpression> opes = new HashMap();
    public int prio;
    protected ShareRuleValue share;

    /* access modifiers changed from: protected */
    public abstract AbstractExpression parse(Lex lex);

    public AbstractRule(ShareRuleValue share2) {
        this.share = share2;
    }

    public final void addExpression(AbstractExpression exp) {
        if (exp != null) {
            addOperator(exp.getOperator(), exp);
            addLexOperator(exp.getEndOperator());
            if (exp instanceof ParenExpression) {
                this.share.paren = exp;
            }
        }
    }

    public final void addOperator(String ope, AbstractExpression exp) {
        this.opes.put(ope, exp);
        addLexOperator(ope);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public final String[] getOperators() {
        List<String> list = new ArrayList<>();
        for (String s : this.opes.keySet()) {
            list.add(s);
        }
        return (String[]) list.toArray(new String[list.size()]);
    }

    public final void addLexOperator(String ope) {
        if (ope != null) {
            int n = ope.length() - 1;
            if (this.share.opeList[n] == null) {
                this.share.opeList[n] = new ArrayList();
            }
            this.share.opeList[n].add(ope);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean isMyOperator(String ope) {
        return this.opes.containsKey(ope);
    }

    /* access modifiers changed from: protected */
    public final AbstractExpression newExpression(String ope, ShareExpValue share2) {
        try {
            AbstractExpression n = this.opes.get(ope).dup(share2);
            n.setPriority(this.prio);
            n.share = share2;
            return n;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    public final void initPriority(int prio2) {
        this.prio = prio2;
        if (this.nextRule != null) {
            this.nextRule.initPriority(prio2 + 1);
        }
    }
}
