package jp.hishidama.eval;

public class EvalThroughException extends EvalException {
    public EvalThroughException(RuntimeException e) {
        super(e);
    }

    public String toString() {
        return getCause().toString();
    }
}
