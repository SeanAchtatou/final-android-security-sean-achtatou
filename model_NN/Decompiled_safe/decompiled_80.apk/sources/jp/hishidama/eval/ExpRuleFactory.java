package jp.hishidama.eval;

import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.AndExpression;
import jp.hishidama.eval.exp.ArrayExpression;
import jp.hishidama.eval.exp.BitAndExpression;
import jp.hishidama.eval.exp.BitNotExpression;
import jp.hishidama.eval.exp.BitOrExpression;
import jp.hishidama.eval.exp.BitXorExpression;
import jp.hishidama.eval.exp.CommaExpression;
import jp.hishidama.eval.exp.DecAfterExpression;
import jp.hishidama.eval.exp.DecBeforeExpression;
import jp.hishidama.eval.exp.DivExpression;
import jp.hishidama.eval.exp.EqualExpression;
import jp.hishidama.eval.exp.FieldExpression;
import jp.hishidama.eval.exp.FuncArgExpression;
import jp.hishidama.eval.exp.FunctionExpression;
import jp.hishidama.eval.exp.GreaterEqualExpression;
import jp.hishidama.eval.exp.GreaterThanExpression;
import jp.hishidama.eval.exp.IfExpression;
import jp.hishidama.eval.exp.IncAfterExpression;
import jp.hishidama.eval.exp.IncBeforeExpression;
import jp.hishidama.eval.exp.LessEqualExpression;
import jp.hishidama.eval.exp.LessThanExpression;
import jp.hishidama.eval.exp.LetAndExpression;
import jp.hishidama.eval.exp.LetDivExpression;
import jp.hishidama.eval.exp.LetExpression;
import jp.hishidama.eval.exp.LetMinusExpression;
import jp.hishidama.eval.exp.LetModExpression;
import jp.hishidama.eval.exp.LetMultExpression;
import jp.hishidama.eval.exp.LetOrExpression;
import jp.hishidama.eval.exp.LetPlusExpression;
import jp.hishidama.eval.exp.LetPowerExpression;
import jp.hishidama.eval.exp.LetShiftLeftExpression;
import jp.hishidama.eval.exp.LetShiftRightExpression;
import jp.hishidama.eval.exp.LetShiftRightLogicalExpression;
import jp.hishidama.eval.exp.LetXorExpression;
import jp.hishidama.eval.exp.MinusExpression;
import jp.hishidama.eval.exp.ModExpression;
import jp.hishidama.eval.exp.MultExpression;
import jp.hishidama.eval.exp.NotEqualExpression;
import jp.hishidama.eval.exp.NotExpression;
import jp.hishidama.eval.exp.OrExpression;
import jp.hishidama.eval.exp.ParenExpression;
import jp.hishidama.eval.exp.PlusExpression;
import jp.hishidama.eval.exp.PowerExpression;
import jp.hishidama.eval.exp.ShiftLeftExpression;
import jp.hishidama.eval.exp.ShiftRightExpression;
import jp.hishidama.eval.exp.ShiftRightLogicalExpression;
import jp.hishidama.eval.exp.SignMinusExpression;
import jp.hishidama.eval.exp.SignPlusExpression;
import jp.hishidama.eval.func.Function;
import jp.hishidama.eval.lex.LexFactory;
import jp.hishidama.eval.log.EvalLog;
import jp.hishidama.eval.oper.Operator;
import jp.hishidama.eval.rule.AbstractRule;
import jp.hishidama.eval.rule.Col1AfterRule;
import jp.hishidama.eval.rule.Col1BeforeRule;
import jp.hishidama.eval.rule.Col2RightJoinRule;
import jp.hishidama.eval.rule.Col2Rule;
import jp.hishidama.eval.rule.Col3Rule;
import jp.hishidama.eval.rule.JavaRuleFactory;
import jp.hishidama.eval.rule.PrimaryRule;
import jp.hishidama.eval.rule.ShareRuleValue;
import jp.hishidama.eval.var.Variable;

public class ExpRuleFactory {
    private static ExpRuleFactory me;
    private Rule cachedRule;
    protected LexFactory defaultLexFactory;
    protected AbstractRule topRule;

    public static ExpRuleFactory getInstance() {
        if (me == null) {
            me = new ExpRuleFactory();
        }
        return me;
    }

    public static Rule getDefaultRule() {
        return getInstance().getRule();
    }

    public static Rule getJavaRule() {
        return JavaRuleFactory.getInstance().getRule();
    }

    public Rule getRule() {
        if (this.cachedRule == null) {
            this.cachedRule = createRule();
        }
        return this.cachedRule;
    }

    /* access modifiers changed from: protected */
    public Rule createRule() {
        return createRule(null, null, null, null);
    }

    /* access modifiers changed from: protected */
    public Rule createRule(Variable var, Function func, Operator oper, EvalLog log) {
        ShareRuleValue share = new ShareRuleValue();
        share.lexFactory = getLexFactory();
        init(share);
        share.defaultVar = var;
        share.defaultFunc = func;
        share.defaultOper = oper;
        share.defaultLog = log;
        return share;
    }

    /* access modifiers changed from: protected */
    public void init(ShareRuleValue share) {
        this.topRule = null;
        initMainRule(null, share);
        this.topRule.initPriority(1);
        share.topRule = this.topRule;
        initFuncArgRule(share);
        this.topRule = null;
    }

    /* access modifiers changed from: protected */
    public AbstractRule initMainRule(AbstractRule rule, ShareRuleValue share) {
        return add(add(add(add(add(add(add(add(add(add(add(add(add(add(add(add(add(rule, createCommaRule(share)), createLetRule(share)), createIfRule(share)), createOrRule(share)), createAndRule(share)), createBitOrRule(share)), createBitXorRule(share)), createBitAndRule(share)), createEqualRule(share)), createGreaterRule(share)), createShiftRule(share)), createPlusRule(share)), createMultRule(share)), createSignRule(share)), createPowerRule(share)), createCol1AfterRule(share)), createPrimaryRule(share));
    }

    /* access modifiers changed from: protected */
    public void initFuncArgRule(ShareRuleValue share) {
        AbstractRule argRule = createFuncArgRule(share);
        share.funcArgRule = argRule;
        String[] a_opes = argRule.getOperators();
        String[] t_opes = this.topRule.getOperators();
        boolean match = false;
        int length = a_opes.length;
        int i = 0;
        loop0:
        while (true) {
            if (i >= length) {
                break;
            }
            String argOpe = a_opes[i];
            for (String topOpe : t_opes) {
                if (argOpe.equals(topOpe)) {
                    match = true;
                    break loop0;
                }
            }
            i++;
        }
        if (match) {
            argRule.nextRule = this.topRule.nextRule;
        } else {
            argRule.nextRule = this.topRule;
        }
        argRule.prio = this.topRule.prio;
    }

    /* access modifiers changed from: protected */
    public final AbstractRule add(AbstractRule rule, AbstractRule r) {
        if (r == null) {
            return rule;
        }
        if (this.topRule == null) {
            this.topRule = r;
        }
        if (rule != null) {
            rule.nextRule = r;
        }
        return r;
    }

    /* access modifiers changed from: protected */
    public AbstractRule createCommaRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2Rule(share);
        me2.addExpression(createCommaExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createCommaExpression() {
        return new CommaExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createLetRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2RightJoinRule(share);
        me2.addExpression(createLetExpression());
        me2.addExpression(createLetMultExpression());
        me2.addExpression(createLetDivExpression());
        me2.addExpression(createLetModExpression());
        me2.addExpression(createLetPlusExpression());
        me2.addExpression(createLetMinusExpression());
        me2.addExpression(createLetShiftLeftExpression());
        me2.addExpression(createLetShiftRightExpression());
        me2.addExpression(createLetShiftRightLogicalExpression());
        me2.addExpression(createLetAndExpression());
        me2.addExpression(createLetOrExpression());
        me2.addExpression(createLetXorExpression());
        me2.addExpression(createLetPowerExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetExpression() {
        return new LetExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetMultExpression() {
        return new LetMultExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetDivExpression() {
        return new LetDivExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetModExpression() {
        return new LetModExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetPlusExpression() {
        return new LetPlusExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetMinusExpression() {
        return new LetMinusExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetShiftLeftExpression() {
        return new LetShiftLeftExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetShiftRightExpression() {
        return new LetShiftRightExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetShiftRightLogicalExpression() {
        return new LetShiftRightLogicalExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetAndExpression() {
        return new LetAndExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetOrExpression() {
        return new LetOrExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetXorExpression() {
        return new LetXorExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLetPowerExpression() {
        return new LetPowerExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createIfRule(ShareRuleValue share) {
        Col3Rule me2 = new Col3Rule(share);
        AbstractExpression createIfExpression = createIfExpression();
        me2.cond = createIfExpression;
        me2.addExpression(createIfExpression);
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createIfExpression() {
        return new IfExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createOrRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2Rule(share);
        me2.addExpression(createOrExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createOrExpression() {
        return new OrExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createAndRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2Rule(share);
        me2.addExpression(createAndExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createAndExpression() {
        return new AndExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createBitOrRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2Rule(share);
        me2.addExpression(createBitOrExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createBitOrExpression() {
        return new BitOrExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createBitXorRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2Rule(share);
        me2.addExpression(createBitXorExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createBitXorExpression() {
        return new BitXorExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createBitAndRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2Rule(share);
        me2.addExpression(createBitAndExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createBitAndExpression() {
        return new BitAndExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createEqualRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2Rule(share);
        me2.addExpression(createEqualExpression());
        me2.addExpression(createNotEqualExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createEqualExpression() {
        return new EqualExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createNotEqualExpression() {
        return new NotEqualExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createGreaterRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2Rule(share);
        me2.addExpression(createLessThanExpression());
        me2.addExpression(createLessEqualExpression());
        me2.addExpression(createGreaterThanExpression());
        me2.addExpression(createGreaterEqualExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLessThanExpression() {
        return new LessThanExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createLessEqualExpression() {
        return new LessEqualExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createGreaterThanExpression() {
        return new GreaterThanExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createGreaterEqualExpression() {
        return new GreaterEqualExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createShiftRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2Rule(share);
        me2.addExpression(createShiftLeftExpression());
        me2.addExpression(createShiftRightExpression());
        me2.addExpression(createShiftRightLogicalExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createShiftLeftExpression() {
        return new ShiftLeftExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createShiftRightExpression() {
        return new ShiftRightExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createShiftRightLogicalExpression() {
        return new ShiftRightLogicalExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createPlusRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2Rule(share);
        me2.addExpression(createPlusExpression());
        me2.addExpression(createMinusExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createPlusExpression() {
        return new PlusExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createMinusExpression() {
        return new MinusExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createMultRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2Rule(share);
        me2.addExpression(createMultExpression());
        me2.addExpression(createDivExpression());
        me2.addExpression(createModExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createMultExpression() {
        return new MultExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createDivExpression() {
        return new DivExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createModExpression() {
        return new ModExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createSignRule(ShareRuleValue share) {
        AbstractRule me2 = new Col1BeforeRule(share);
        me2.addExpression(createSignPlusExpression());
        me2.addExpression(createSignMinusExpression());
        me2.addExpression(createBitNotExpression());
        me2.addExpression(createNotExpression());
        me2.addExpression(createIncBeforeExpression());
        me2.addExpression(createDecBeforeExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createSignPlusExpression() {
        return new SignPlusExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createSignMinusExpression() {
        return new SignMinusExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createBitNotExpression() {
        return new BitNotExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createNotExpression() {
        return new NotExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createIncBeforeExpression() {
        return new IncBeforeExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createDecBeforeExpression() {
        return new DecBeforeExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createPowerRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2Rule(share);
        me2.addExpression(createPowerExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createPowerExpression() {
        return new PowerExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createCol1AfterRule(ShareRuleValue share) {
        Col1AfterRule me2 = new Col1AfterRule(share);
        AbstractExpression createFunctionExpression = createFunctionExpression();
        me2.func = createFunctionExpression;
        me2.addExpression(createFunctionExpression);
        AbstractExpression createArrayExpression = createArrayExpression();
        me2.array = createArrayExpression;
        me2.addExpression(createArrayExpression);
        me2.addExpression(createIncAfterExpression());
        me2.addExpression(createDecAfterExpression());
        AbstractExpression createFieldExpression = createFieldExpression();
        me2.field = createFieldExpression;
        me2.addExpression(createFieldExpression);
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createFunctionExpression() {
        return new FunctionExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createArrayExpression() {
        return new ArrayExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createIncAfterExpression() {
        return new IncAfterExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createDecAfterExpression() {
        return new DecAfterExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createFieldExpression() {
        return new FieldExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createPrimaryRule(ShareRuleValue share) {
        AbstractRule me2 = new PrimaryRule(share);
        me2.addExpression(createParenExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createParenExpression() {
        return new ParenExpression();
    }

    /* access modifiers changed from: protected */
    public AbstractRule createFuncArgRule(ShareRuleValue share) {
        AbstractRule me2 = new Col2Rule(share);
        me2.addExpression(createFuncArgExpression());
        return me2;
    }

    /* access modifiers changed from: protected */
    public AbstractExpression createFuncArgExpression() {
        return new FuncArgExpression();
    }

    /* access modifiers changed from: protected */
    public LexFactory getLexFactory() {
        if (this.defaultLexFactory == null) {
            this.defaultLexFactory = new LexFactory();
        }
        return this.defaultLexFactory;
    }
}
