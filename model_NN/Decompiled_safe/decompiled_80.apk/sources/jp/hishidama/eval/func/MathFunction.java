package jp.hishidama.eval.func;

import jp.hishidama.lang.reflect.InvokeUtil;

public class MathFunction implements Function {
    protected InvokeUtil invoker;

    public Object eval(String name, Object[] args) throws Exception {
        if (this.invoker == null) {
            this.invoker = new InvokeUtil();
            this.invoker.addMethods(Math.class, "");
        }
        return this.invoker.invoke(name, null, args);
    }

    public Object eval(Object object, String name, Object[] args) throws Exception {
        return eval(name, args);
    }
}
