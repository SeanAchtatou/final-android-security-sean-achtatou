package jp.hishidama.eval.func;

public interface Function {
    Object eval(Object obj, String str, Object[] objArr) throws Exception;

    Object eval(String str, Object[] objArr) throws Exception;
}
