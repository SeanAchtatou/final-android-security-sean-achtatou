package jp.hishidama.eval.func;

import java.util.HashMap;
import java.util.Map;
import jp.hishidama.lang.reflect.InvokeUtil;

public class InvokeFunction implements Function {
    protected Map<Class<?>, InvokeUtil> map = new HashMap();

    public Object eval(String name, Object[] args) throws Exception {
        return null;
    }

    public Object eval(Object object, String name, Object[] args) throws Exception {
        if (object == null) {
            return null;
        }
        Class<?> clazz = object.getClass();
        if (!this.map.containsKey(clazz)) {
            InvokeUtil invoker = new InvokeUtil();
            invoker.addMethods(clazz, "");
            this.map.put(clazz, invoker);
        }
        return this.map.get(clazz).invoke(name, object, args);
    }
}
