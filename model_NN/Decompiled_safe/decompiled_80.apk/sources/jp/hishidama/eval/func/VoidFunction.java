package jp.hishidama.eval.func;

public class VoidFunction implements Function {
    protected boolean dump;

    public VoidFunction() {
        this(false);
    }

    public VoidFunction(boolean dump2) {
        this.dump = dump2;
    }

    public Object eval(String name, Object[] args) throws Exception {
        if (!this.dump) {
            return null;
        }
        System.out.println(String.valueOf(name) + "関数が呼ばれた");
        for (int i = 0; i < args.length; i++) {
            System.out.println("arg[" + i + "] " + args[i]);
        }
        return null;
    }

    public Object eval(Object object, String name, Object[] args) throws Exception {
        if (!this.dump) {
            return null;
        }
        System.out.println(object + "." + name + "関数が呼ばれた");
        for (int i = 0; i < args.length; i++) {
            System.out.println("arg[" + i + "] " + args[i]);
        }
        return null;
    }
}
