package jp.hishidama.eval.ref;

public interface Refactor {
    String getNewFuncName(Object obj, String str);

    String getNewName(Object obj, String str);
}
