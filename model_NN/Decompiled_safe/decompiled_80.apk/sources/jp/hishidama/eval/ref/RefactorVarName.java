package jp.hishidama.eval.ref;

public class RefactorVarName extends RefactorAdapter {
    protected String newName;
    protected String oldName;
    protected Class<?> targetClass;

    public RefactorVarName(Class<?> targetClass2, String oldName2, String newName2) {
        this.targetClass = targetClass2;
        this.oldName = oldName2;
        this.newName = newName2;
        if (oldName2 == null || newName2 == null) {
            throw new NullPointerException();
        }
    }

    public String getNewName(Object target, String name) {
        if (!name.equals(this.oldName)) {
            return null;
        }
        if (this.targetClass == null) {
            if (target == null) {
                return this.newName;
            }
        } else if (target != null && this.targetClass.isAssignableFrom(target.getClass())) {
            return this.newName;
        }
        return null;
    }
}
