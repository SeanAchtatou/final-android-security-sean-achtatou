package jp.hishidama.eval.oper;

import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.util.NumberUtil;

public class IntOperator implements Operator {
    public static final int FALSE = 0;
    public static final int TRUE = 1;

    /* access modifiers changed from: protected */
    public int n(Object obj) {
        if (obj == null) {
            return 0;
        }
        return ((Number) obj).intValue();
    }

    public Object power(Object x, Object y) {
        return Integer.valueOf(power(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int power(int x, int y) {
        return (int) Math.pow((double) x, (double) y);
    }

    public Object signPlus(Object x) {
        return Integer.valueOf(signPlus(n(x)));
    }

    /* access modifiers changed from: protected */
    public int signPlus(int x) {
        return x;
    }

    public Object signMinus(Object x) {
        return Integer.valueOf(signMinus(n(x)));
    }

    /* access modifiers changed from: protected */
    public int signMinus(int x) {
        return -x;
    }

    public Object plus(Object x, Object y) {
        return Integer.valueOf(plus(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int plus(int x, int y) {
        return x + y;
    }

    public Object minus(Object x, Object y) {
        return Integer.valueOf(minus(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int minus(int x, int y) {
        return x - y;
    }

    public Object mult(Object x, Object y) {
        return Integer.valueOf(mult(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int mult(int x, int y) {
        return x * y;
    }

    public Object div(Object x, Object y) {
        return Integer.valueOf(div(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int div(int x, int y) {
        return x / y;
    }

    public Object mod(Object x, Object y) {
        return Integer.valueOf(mod(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int mod(int x, int y) {
        return x % y;
    }

    public Object bitNot(Object x) {
        return Integer.valueOf(bitNot(n(x)));
    }

    /* access modifiers changed from: protected */
    public int bitNot(int x) {
        return x ^ -1;
    }

    public Object shiftLeft(Object x, Object y) {
        return Integer.valueOf(shiftLeft(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int shiftLeft(int x, int y) {
        return x << y;
    }

    public Object shiftRight(Object x, Object y) {
        return Integer.valueOf(shiftRight(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int shiftRight(int x, int y) {
        return x >> y;
    }

    public Object shiftRightLogical(Object x, Object y) {
        return Integer.valueOf(shiftRightLogical(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int shiftRightLogical(int x, int y) {
        return x >>> y;
    }

    public Object bitAnd(Object x, Object y) {
        return Integer.valueOf(bitAnd(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int bitAnd(int x, int y) {
        return x & y;
    }

    public Object bitOr(Object x, Object y) {
        return Integer.valueOf(bitOr(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int bitOr(int x, int y) {
        return x | y;
    }

    public Object bitXor(Object x, Object y) {
        return Integer.valueOf(bitXor(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int bitXor(int x, int y) {
        return x ^ y;
    }

    public Object not(Object x) {
        return Integer.valueOf(not(n(x)));
    }

    /* access modifiers changed from: protected */
    public int not(int x) {
        return x == 0 ? 1 : 0;
    }

    public Object equal(Object x, Object y) {
        return Integer.valueOf(equal(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int equal(int x, int y) {
        return x == y ? 1 : 0;
    }

    public Object notEqual(Object x, Object y) {
        return Integer.valueOf(notEqual(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int notEqual(int x, int y) {
        return x != y ? 1 : 0;
    }

    public Object lessThan(Object x, Object y) {
        return Integer.valueOf(lessThan(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int lessThan(int x, int y) {
        return x < y ? 1 : 0;
    }

    public Object lessEqual(Object x, Object y) {
        return Integer.valueOf(lessEqual(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int lessEqual(int x, int y) {
        return x <= y ? 1 : 0;
    }

    public Object greaterThan(Object x, Object y) {
        return Integer.valueOf(greaterThan(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int greaterThan(int x, int y) {
        return x > y ? 1 : 0;
    }

    public Object greaterEqual(Object x, Object y) {
        return Integer.valueOf(greaterEqual(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public int greaterEqual(int x, int y) {
        return x >= y ? 1 : 0;
    }

    public boolean bool(Object x) {
        return n(x) != 0;
    }

    public Object inc(Object x, int inc) {
        return Integer.valueOf(inc(n(x), inc));
    }

    /* access modifiers changed from: protected */
    public int inc(int x, int inc) {
        return x + inc;
    }

    public Object character(String word, AbstractExpression exp) {
        return Integer.valueOf(word.charAt(0));
    }

    public Object string(String word, AbstractExpression exp) {
        return Integer.valueOf(toNumber(word, exp));
    }

    public Object number(String word, AbstractExpression exp) {
        return Integer.valueOf(toNumber(word, exp));
    }

    public Object variable(Object value, AbstractExpression exp) {
        if (value == null) {
            return value;
        }
        if (value instanceof Number) {
            return value;
        }
        return Integer.valueOf(toNumber(value.toString(), exp));
    }

    /* access modifiers changed from: protected */
    public int toNumber(String word, AbstractExpression exp) {
        try {
            return Integer.valueOf((int) NumberUtil.parseLong(word)).intValue();
        } catch (Exception e) {
            try {
                return Integer.valueOf(word).intValue();
            } catch (Exception e2) {
                try {
                    return (int) Double.parseDouble(word);
                } catch (Exception e3) {
                    throw new EvalException((int) EvalException.EXP_NOT_NUMBER, exp, e3);
                }
            }
        }
    }
}
