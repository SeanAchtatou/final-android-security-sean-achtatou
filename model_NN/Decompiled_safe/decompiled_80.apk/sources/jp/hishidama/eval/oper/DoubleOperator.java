package jp.hishidama.eval.oper;

import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.util.NumberUtil;

public class DoubleOperator implements Operator {
    public static final int FALSE = 0;
    public static final int TRUE = 1;

    /* access modifiers changed from: protected */
    public double n(Object obj) {
        if (obj == null) {
            return 0.0d;
        }
        return ((Number) obj).doubleValue();
    }

    public Object power(Object x, Object y) {
        return Double.valueOf(power(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double power(double x, double y) {
        return Math.pow(x, y);
    }

    public Object signPlus(Object x) {
        return Double.valueOf(signPlus(n(x)));
    }

    /* access modifiers changed from: protected */
    public double signPlus(double x) {
        return x;
    }

    public Object signMinus(Object x) {
        return Double.valueOf(signMinus(n(x)));
    }

    /* access modifiers changed from: protected */
    public double signMinus(double x) {
        return -x;
    }

    public Object plus(Object x, Object y) {
        return Double.valueOf(plus(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double plus(double x, double y) {
        return x + y;
    }

    public Object minus(Object x, Object y) {
        return Double.valueOf(minus(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double minus(double x, double y) {
        return x - y;
    }

    public Object mult(Object x, Object y) {
        return Double.valueOf(mult(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double mult(double x, double y) {
        return x * y;
    }

    public Object div(Object x, Object y) {
        return Double.valueOf(div(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double div(double x, double y) {
        return x / y;
    }

    public Object mod(Object x, Object y) {
        return Double.valueOf(mod(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double mod(double x, double y) {
        return x % y;
    }

    public Object bitNot(Object x) {
        return Double.valueOf(bitNot(n(x)));
    }

    /* access modifiers changed from: protected */
    public double bitNot(double x) {
        return (double) (((long) x) ^ -1);
    }

    public Object shiftLeft(Object x, Object y) {
        return Double.valueOf(shiftLeft(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double shiftLeft(double x, double y) {
        return Math.pow(2.0d, y) * x;
    }

    public Object shiftRight(Object x, Object y) {
        return Double.valueOf(shiftRight(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double shiftRight(double x, double y) {
        return x / Math.pow(2.0d, y);
    }

    public Object shiftRightLogical(Object x, Object y) {
        return Double.valueOf(shiftRightLogical(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double shiftRightLogical(double x, double y) {
        if (x < 0.0d) {
            x = -x;
        }
        return x / Math.pow(2.0d, y);
    }

    public Object bitAnd(Object x, Object y) {
        return Double.valueOf(bitAnd(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double bitAnd(double x, double y) {
        return (double) (((long) x) & ((long) y));
    }

    public Object bitOr(Object x, Object y) {
        return Double.valueOf(bitOr(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double bitOr(double x, double y) {
        return (double) (((long) x) | ((long) y));
    }

    public Object bitXor(Object x, Object y) {
        return Double.valueOf(bitXor(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double bitXor(double x, double y) {
        return (double) (((long) x) ^ ((long) y));
    }

    public Object not(Object x) {
        return Double.valueOf(not(n(x)));
    }

    /* access modifiers changed from: protected */
    public double not(double x) {
        return (double) (x == 0.0d ? 1 : 0);
    }

    public Object equal(Object x, Object y) {
        return Double.valueOf(equal(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double equal(double x, double y) {
        return (double) (x == y ? 1 : 0);
    }

    public Object notEqual(Object x, Object y) {
        return Double.valueOf(notEqual(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double notEqual(double x, double y) {
        return (double) (x != y ? 1 : 0);
    }

    public Object lessThan(Object x, Object y) {
        return Double.valueOf(lessThan(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double lessThan(double x, double y) {
        return (double) (x < y ? 1 : 0);
    }

    public Object lessEqual(Object x, Object y) {
        return Double.valueOf(lessEqual(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double lessEqual(double x, double y) {
        return (double) (x <= y ? 1 : 0);
    }

    public Object greaterThan(Object x, Object y) {
        return Double.valueOf(greaterThan(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double greaterThan(double x, double y) {
        return (double) (x > y ? 1 : 0);
    }

    public Object greaterEqual(Object x, Object y) {
        return Double.valueOf(greaterEqual(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public double greaterEqual(double x, double y) {
        return (double) (x >= y ? 1 : 0);
    }

    public boolean bool(Object x) {
        return n(x) != 0.0d;
    }

    public Object inc(Object x, int inc) {
        return Double.valueOf(inc(n(x), inc));
    }

    /* access modifiers changed from: protected */
    public double inc(double x, int inc) {
        return ((double) inc) + x;
    }

    public Object character(String word, AbstractExpression exp) {
        return Double.valueOf((double) word.charAt(0));
    }

    public Object string(String word, AbstractExpression exp) {
        return toNumber(word, exp);
    }

    public Object number(String word, AbstractExpression exp) {
        return toNumber(word, exp);
    }

    public Object variable(Object value, AbstractExpression exp) {
        if (value == null) {
            return value;
        }
        if (value instanceof Number) {
            return value;
        }
        return toNumber(value.toString(), exp);
    }

    /* access modifiers changed from: protected */
    public Double toNumber(String word, AbstractExpression exp) {
        try {
            return Double.valueOf(word);
        } catch (Exception e) {
            try {
                return Double.valueOf((double) NumberUtil.parseLong(word));
            } catch (Exception e2) {
                throw new EvalException((int) EvalException.EXP_NOT_NUMBER, exp, e2);
            }
        }
    }
}
