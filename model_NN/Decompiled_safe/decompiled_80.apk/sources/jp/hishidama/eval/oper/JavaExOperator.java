package jp.hishidama.eval.oper;

import java.math.BigDecimal;
import java.math.BigInteger;
import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.BitAndExpression;
import jp.hishidama.eval.exp.BitNotExpression;
import jp.hishidama.eval.exp.BitOrExpression;
import jp.hishidama.eval.exp.BitXorExpression;
import jp.hishidama.eval.exp.DivExpression;
import jp.hishidama.eval.exp.MinusExpression;
import jp.hishidama.eval.exp.ModExpression;
import jp.hishidama.eval.exp.MultExpression;
import jp.hishidama.eval.exp.NotExpression;
import jp.hishidama.eval.exp.PlusExpression;
import jp.hishidama.eval.exp.ShiftLeftExpression;
import jp.hishidama.eval.exp.ShiftRightExpression;
import jp.hishidama.eval.exp.ShiftRightLogicalExpression;
import jp.hishidama.eval.exp.SignMinusExpression;
import jp.hishidama.util.NumberUtil;

public class JavaExOperator implements Operator {
    /* access modifiers changed from: package-private */
    public boolean inLong(Object x) {
        if (x instanceof Long) {
            return true;
        }
        if (x instanceof Integer) {
            return true;
        }
        if (x instanceof Short) {
            return true;
        }
        if (x instanceof Byte) {
            return true;
        }
        if (x instanceof BigInteger) {
            return true;
        }
        if (x instanceof BigDecimal) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public long l(Object x) {
        return ((Number) x).longValue();
    }

    /* access modifiers changed from: package-private */
    public boolean inDouble(Object x) {
        if (x instanceof Double) {
            return true;
        }
        if (x instanceof Float) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public double d(Object x) {
        return ((Number) x).doubleValue();
    }

    /* access modifiers changed from: package-private */
    public Object n(long n, Object x) {
        if (x instanceof Long) {
            return Long.valueOf(n);
        }
        if (x instanceof Double) {
            return Double.valueOf((double) n);
        }
        if (x instanceof Integer) {
            return Integer.valueOf((int) n);
        }
        if (x instanceof Short) {
            return Short.valueOf((short) ((int) n));
        }
        if (x instanceof Byte) {
            return Byte.valueOf((byte) ((int) n));
        }
        if (x instanceof Float) {
            return Float.valueOf((float) n);
        }
        if (x instanceof BigInteger) {
            return BigInteger.valueOf(n);
        }
        if (x instanceof BigDecimal) {
            return BigDecimal.valueOf(n);
        }
        if (x instanceof String) {
            return String.valueOf(n);
        }
        return Long.valueOf(n);
    }

    /* access modifiers changed from: package-private */
    public Object n(long n, Object x, Object y) {
        if ((x instanceof Byte) || (y instanceof Byte)) {
            return Byte.valueOf((byte) ((int) n));
        }
        if ((x instanceof Short) || (y instanceof Short)) {
            return Short.valueOf((short) ((int) n));
        }
        if ((x instanceof Integer) || (y instanceof Integer)) {
            return Integer.valueOf((int) n);
        }
        if ((x instanceof Long) || (y instanceof Long)) {
            return Long.valueOf(n);
        }
        if ((x instanceof BigInteger) || (y instanceof BigInteger)) {
            return BigInteger.valueOf(n);
        }
        if ((x instanceof BigDecimal) || (y instanceof BigDecimal)) {
            return BigDecimal.valueOf(n);
        }
        if ((x instanceof Float) || (y instanceof Float)) {
            return Float.valueOf((float) n);
        }
        if ((x instanceof Double) || (y instanceof Double)) {
            return Double.valueOf((double) n);
        }
        if ((x instanceof String) || (y instanceof String)) {
            return String.valueOf(n);
        }
        return Long.valueOf(n);
    }

    /* access modifiers changed from: package-private */
    public Object n(double n, Object x) {
        if (x instanceof Float) {
            return Float.valueOf((float) n);
        }
        if (x instanceof String) {
            return String.valueOf(n);
        }
        return Double.valueOf(n);
    }

    /* access modifiers changed from: package-private */
    public Object n(double n, Object x, Object y) {
        if ((x instanceof Float) || (y instanceof Float)) {
            return Float.valueOf((float) n);
        }
        if ((x instanceof Number) || (y instanceof Number)) {
            return Double.valueOf(n);
        }
        if ((x instanceof String) || (y instanceof String)) {
            return String.valueOf(n);
        }
        return Double.valueOf(n);
    }

    /* access modifiers changed from: package-private */
    public Object nn(long n, Object x) {
        if (x instanceof BigDecimal) {
            return BigDecimal.valueOf(n);
        }
        if (x instanceof BigInteger) {
            return BigInteger.valueOf(n);
        }
        return Long.valueOf(n);
    }

    /* access modifiers changed from: package-private */
    public Object nn(long n, Object x, Object y) {
        if ((x instanceof BigDecimal) || (y instanceof BigDecimal)) {
            return BigDecimal.valueOf(n);
        }
        if ((x instanceof BigInteger) || (y instanceof BigInteger)) {
            return BigInteger.valueOf(n);
        }
        return Long.valueOf(n);
    }

    /* access modifiers changed from: package-private */
    public Object nn(double n, Object x) {
        if (inLong(x)) {
            return Long.valueOf((long) n);
        }
        return Double.valueOf(n);
    }

    /* access modifiers changed from: package-private */
    public Object nn(double n, Object x, Object y) {
        if (!inLong(x) || !inLong(y)) {
            return Double.valueOf(n);
        }
        return Long.valueOf((long) n);
    }

    /* access modifiers changed from: package-private */
    public RuntimeException undefined(String name, Object x) {
        String c = null;
        if (x != null) {
            c = x.getClass().getName();
        }
        return new UnsupportedOperationException("未定義単項演算" + name + "：" + c);
    }

    /* access modifiers changed from: package-private */
    public RuntimeException undefined(String name, Object x, Object y) {
        String c1 = null;
        String c2 = null;
        if (x != null) {
            c1 = x.getClass().getName();
        }
        if (y != null) {
            c2 = y.getClass().getName();
        }
        return new UnsupportedOperationException("未定義二項演算" + name + "：" + c1 + " , " + c2);
    }

    public Object power(Object x, Object y) {
        if (x == null && y == null) {
            return null;
        }
        return nn(Math.pow(d(x), d(y)), x, y);
    }

    public Object signPlus(Object x) {
        return x;
    }

    public Object signMinus(Object x) {
        if (x == null) {
            return null;
        }
        if (inLong(x)) {
            return n(-l(x), x);
        }
        if (inDouble(x)) {
            return n(-d(x), x);
        }
        if (x instanceof Boolean) {
            return x;
        }
        throw undefined(SignMinusExpression.NAME, x);
    }

    public Object plus(Object x, Object y) {
        if (x == null && y == null) {
            return null;
        }
        if (inLong(x) && inLong(y)) {
            return nn(l(x) + l(y), x, y);
        }
        if (inDouble(x) && inDouble(y)) {
            return nn(d(x) + d(y), x, y);
        }
        if ((x instanceof String) || (y instanceof String)) {
            return String.valueOf(String.valueOf(x)) + String.valueOf(y);
        }
        if ((x instanceof Character) || (y instanceof Character)) {
            return String.valueOf(String.valueOf(x)) + String.valueOf(y);
        }
        throw undefined(PlusExpression.NAME, x, y);
    }

    public Object minus(Object x, Object y) {
        if (x == null && y == null) {
            return null;
        }
        if (inLong(x) && inLong(y)) {
            return nn(l(x) - l(y), x, y);
        }
        if (inDouble(x) && inDouble(y)) {
            return nn(d(x) - d(y), x, y);
        }
        throw undefined(MinusExpression.NAME, x, y);
    }

    public Object mult(Object x, Object y) {
        if (x == null && y == null) {
            return null;
        }
        if (inLong(x) && inLong(y)) {
            return nn(l(x) * l(y), x, y);
        }
        if (inDouble(x) && inDouble(y)) {
            return nn(d(x) * d(y), x, y);
        }
        String s = null;
        int ct = 0;
        boolean str = false;
        if ((x instanceof String) && (y instanceof Number)) {
            s = (String) x;
            ct = ((Number) y).intValue();
            str = true;
        } else if ((y instanceof String) && (x instanceof Number)) {
            s = (String) y;
            ct = ((Number) x).intValue();
            str = true;
        }
        if (str) {
            StringBuilder sb = new StringBuilder(s.length() * ct);
            for (int i = 0; i < ct; i++) {
                sb.append(s);
            }
            return sb.toString();
        }
        throw undefined(MultExpression.NAME, x, y);
    }

    public Object div(Object x, Object y) {
        if (x == null && y == null) {
            return null;
        }
        if (inLong(x) && inLong(y)) {
            return nn(l(x) / l(y), x);
        }
        if (inDouble(x) && inDouble(y)) {
            return nn(d(x) / d(y), x);
        }
        if ((x instanceof String) && (y instanceof String)) {
            return ((String) x).split((String) y);
        }
        throw undefined(DivExpression.NAME, x, y);
    }

    public Object mod(Object x, Object y) {
        if (x == null && y == null) {
            return null;
        }
        if (inLong(x) && inLong(y)) {
            return nn(l(x) % l(y), x);
        }
        if (inDouble(x) && inDouble(y)) {
            return nn(d(x) % d(y), x);
        }
        throw undefined(ModExpression.NAME, x, y);
    }

    public Object bitNot(Object x) {
        if (x == null) {
            return null;
        }
        if (x instanceof Number) {
            return n(l(x) ^ -1, x);
        }
        throw undefined(BitNotExpression.NAME, x);
    }

    public Object shiftLeft(Object x, Object y) {
        if (x == null && y == null) {
            return null;
        }
        if (inLong(x) && inLong(y)) {
            return n(l(x) << ((int) l(y)), x);
        }
        if (inDouble(x) && inDouble(y)) {
            return n(d(x) * Math.pow(2.0d, d(y)), x);
        }
        throw undefined(ShiftLeftExpression.NAME, x, y);
    }

    public Object shiftRight(Object x, Object y) {
        if (x == null && y == null) {
            return null;
        }
        if (inLong(x) && inLong(y)) {
            return n(l(x) >> ((int) l(y)), x);
        }
        if (inDouble(x) && inDouble(y)) {
            return n(d(x) / Math.pow(2.0d, d(y)), x);
        }
        throw undefined(ShiftRightExpression.NAME, x, y);
    }

    public Object shiftRightLogical(Object x, Object y) {
        if (x == null && y == null) {
            return null;
        }
        if ((x instanceof Byte) && (y instanceof Number)) {
            return n((l(x) & 255) >>> ((int) l(y)), x);
        }
        if ((x instanceof Short) && (y instanceof Number)) {
            return n((l(x) & 65535) >>> ((int) l(y)), x);
        }
        if ((x instanceof Integer) && (y instanceof Number)) {
            return n((l(x) & 4294967295L) >>> ((int) l(y)), x);
        }
        if (inLong(x) && (y instanceof Number)) {
            return n(l(x) >>> ((int) l(y)), x);
        }
        if (!inDouble(x) || !(y instanceof Number)) {
            throw undefined(ShiftRightLogicalExpression.NAME, x, y);
        }
        double t = d(x);
        if (t < 0.0d) {
            t = -t;
        }
        return n(t / Math.pow(2.0d, d(y)), x);
    }

    public Object bitAnd(Object x, Object y) {
        if (x == null && y == null) {
            return null;
        }
        if ((x instanceof Number) && (y instanceof Number)) {
            return n(l(x) & l(y), x);
        }
        throw undefined(BitAndExpression.NAME, x, y);
    }

    public Object bitOr(Object x, Object y) {
        if (x == null && y == null) {
            return null;
        }
        if ((x instanceof Number) && (y instanceof Number)) {
            return n(l(x) | l(y), x);
        }
        throw undefined(BitOrExpression.NAME, x, y);
    }

    public Object bitXor(Object x, Object y) {
        if (x == null && y == null) {
            return null;
        }
        if ((x instanceof Number) && (y instanceof Number)) {
            return n(l(x) ^ l(y), x);
        }
        throw undefined(BitXorExpression.NAME, x, y);
    }

    public Object not(Object x) {
        if (x == null) {
            return null;
        }
        if (x instanceof Boolean) {
            return ((Boolean) x).booleanValue() ? Boolean.FALSE : Boolean.TRUE;
        }
        if (!(x instanceof Number)) {
            throw undefined(NotExpression.NAME, x);
        } else if (l(x) == 0) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    /* access modifiers changed from: protected */
    public int compare(Object x, Object y) {
        if (x == null || y == null) {
            return compareNull(x, y);
        }
        if (inLong(x) && inLong(y)) {
            long c = l(x) - l(y);
            if (c == 0) {
                return 0;
            }
            if (c < 0) {
                return -1;
            }
            return 1;
        } else if ((x instanceof Number) && (y instanceof Number)) {
            double n = d(x) - d(y);
            if (n == 0.0d) {
                return 0;
            }
            return n < 0.0d ? -1 : 1;
        } else if ((x instanceof String) && !(y instanceof String)) {
            return ((String) x).compareTo(y.toString());
        } else {
            if (!(x instanceof String) && (y instanceof String)) {
                return x.toString().compareTo((String) y);
            }
            Class<?> xc = x.getClass();
            Class<?> yc = y.getClass();
            if (xc.isAssignableFrom(yc) && (x instanceof Comparable)) {
                return ((Comparable) x).compareTo(y);
            }
            if (yc.isAssignableFrom(xc) && (y instanceof Comparable)) {
                return -((Comparable) y).compareTo(x);
            }
            if (x.equals(y)) {
                return 0;
            }
            throw undefined("compare", x, y);
        }
    }

    /* access modifiers changed from: protected */
    public int compareNull(Object x, Object y) {
        if (x == null && y == null) {
            return 0;
        }
        if (x == null && y != null) {
            return -1;
        }
        if (x != null && y == null) {
            return 1;
        }
        throw new InternalError();
    }

    public Object equal(Object x, Object y) {
        return compare(x, y) == 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    public Object notEqual(Object x, Object y) {
        return compare(x, y) != 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    public Object lessThan(Object x, Object y) {
        return compare(x, y) < 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    public Object lessEqual(Object x, Object y) {
        return compare(x, y) <= 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    public Object greaterThan(Object x, Object y) {
        return compare(x, y) > 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    public Object greaterEqual(Object x, Object y) {
        return compare(x, y) >= 0 ? Boolean.TRUE : Boolean.FALSE;
    }

    public boolean bool(Object x) {
        if (x == null) {
            return false;
        }
        if (x instanceof Boolean) {
            return ((Boolean) x).booleanValue();
        }
        if (x instanceof Number) {
            return ((Number) x).longValue() != 0;
        }
        return Boolean.valueOf(x.toString()).booleanValue();
    }

    public Object inc(Object x, int inc) {
        if (x == null) {
            return null;
        }
        if (inLong(x)) {
            return n(l(x) + ((long) inc), x);
        }
        if (inDouble(x)) {
            return n(d(x) + ((double) inc), x);
        }
        throw undefined("inc" + inc, x);
    }

    public Object character(String word, AbstractExpression exp) {
        return Character.valueOf(word.charAt(0));
    }

    public Object string(String word, AbstractExpression exp) {
        return word;
    }

    public Object number(String word, AbstractExpression exp) {
        try {
            return Long.valueOf(NumberUtil.parseLong(word));
        } catch (Exception e) {
            try {
                return Long.valueOf(word);
            } catch (Exception e2) {
                return Double.valueOf(word);
            }
        }
    }

    public Object variable(Object value, AbstractExpression exp) {
        return value;
    }
}
