package jp.hishidama.eval.oper;

import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.exp.AbstractExpression;

public interface Operator {
    Object bitAnd(Object obj, Object obj2);

    Object bitNot(Object obj);

    Object bitOr(Object obj, Object obj2);

    Object bitXor(Object obj, Object obj2);

    boolean bool(Object obj);

    Object character(String str, AbstractExpression abstractExpression) throws EvalException;

    Object div(Object obj, Object obj2);

    Object equal(Object obj, Object obj2);

    Object greaterEqual(Object obj, Object obj2);

    Object greaterThan(Object obj, Object obj2);

    Object inc(Object obj, int i);

    Object lessEqual(Object obj, Object obj2);

    Object lessThan(Object obj, Object obj2);

    Object minus(Object obj, Object obj2);

    Object mod(Object obj, Object obj2);

    Object mult(Object obj, Object obj2);

    Object not(Object obj);

    Object notEqual(Object obj, Object obj2);

    Object number(String str, AbstractExpression abstractExpression) throws EvalException;

    Object plus(Object obj, Object obj2);

    Object power(Object obj, Object obj2);

    Object shiftLeft(Object obj, Object obj2);

    Object shiftRight(Object obj, Object obj2);

    Object shiftRightLogical(Object obj, Object obj2);

    Object signMinus(Object obj);

    Object signPlus(Object obj);

    Object string(String str, AbstractExpression abstractExpression) throws EvalException;

    Object variable(Object obj, AbstractExpression abstractExpression) throws EvalException;
}
