package jp.hishidama.eval.oper;

import jp.hishidama.eval.EvalException;
import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.util.NumberUtil;

public class LongOperator implements Operator {
    public static final int FALSE = 0;
    public static final int TRUE = 1;

    /* access modifiers changed from: protected */
    public long n(Object obj) {
        if (obj == null) {
            return 0;
        }
        return ((Number) obj).longValue();
    }

    public Object power(Object x, Object y) {
        return Long.valueOf(power(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long power(long x, long y) {
        return (long) Math.pow((double) x, (double) y);
    }

    public Object signPlus(Object x) {
        return Long.valueOf(signPlus(n(x)));
    }

    /* access modifiers changed from: protected */
    public long signPlus(long x) {
        return x;
    }

    public Object signMinus(Object x) {
        return Long.valueOf(signMinus(n(x)));
    }

    /* access modifiers changed from: protected */
    public long signMinus(long x) {
        return -x;
    }

    public Object plus(Object x, Object y) {
        return Long.valueOf(plus(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long plus(long x, long y) {
        return x + y;
    }

    public Object minus(Object x, Object y) {
        return Long.valueOf(minus(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long minus(long x, long y) {
        return x - y;
    }

    public Object mult(Object x, Object y) {
        return Long.valueOf(mult(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long mult(long x, long y) {
        return x * y;
    }

    public Object div(Object x, Object y) {
        return Long.valueOf(div(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long div(long x, long y) {
        return x / y;
    }

    public Object mod(Object x, Object y) {
        return Long.valueOf(mod(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long mod(long x, long y) {
        return x % y;
    }

    public Object bitNot(Object x) {
        return Long.valueOf(bitNot(n(x)));
    }

    /* access modifiers changed from: protected */
    public long bitNot(long x) {
        return -1 ^ x;
    }

    public Object shiftLeft(Object x, Object y) {
        return Long.valueOf(shiftLeft(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long shiftLeft(long x, long y) {
        return x << ((int) y);
    }

    public Object shiftRight(Object x, Object y) {
        return Long.valueOf(shiftRight(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long shiftRight(long x, long y) {
        return x >> ((int) y);
    }

    public Object shiftRightLogical(Object x, Object y) {
        return Long.valueOf(shiftRightLogical(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long shiftRightLogical(long x, long y) {
        return x >>> ((int) y);
    }

    public Object bitAnd(Object x, Object y) {
        return Long.valueOf(bitAnd(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long bitAnd(long x, long y) {
        return x & y;
    }

    public Object bitOr(Object x, Object y) {
        return Long.valueOf(bitOr(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long bitOr(long x, long y) {
        return x | y;
    }

    public Object bitXor(Object x, Object y) {
        return Long.valueOf(bitXor(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long bitXor(long x, long y) {
        return x ^ y;
    }

    public Object not(Object x) {
        return Long.valueOf(not(n(x)));
    }

    /* access modifiers changed from: protected */
    public long not(long x) {
        return (long) (x == 0 ? 1 : 0);
    }

    public Object equal(Object x, Object y) {
        return Long.valueOf(equal(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long equal(long x, long y) {
        return (long) (x == y ? 1 : 0);
    }

    public Object notEqual(Object x, Object y) {
        return Long.valueOf(notEqual(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long notEqual(long x, long y) {
        return (long) (x != y ? 1 : 0);
    }

    public Object lessThan(Object x, Object y) {
        return Long.valueOf(lessThan(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long lessThan(long x, long y) {
        return (long) (x < y ? 1 : 0);
    }

    public Object lessEqual(Object x, Object y) {
        return Long.valueOf(lessEqual(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long lessEqual(long x, long y) {
        return (long) (x <= y ? 1 : 0);
    }

    public Object greaterThan(Object x, Object y) {
        return Long.valueOf(greaterThan(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long greaterThan(long x, long y) {
        return (long) (x > y ? 1 : 0);
    }

    public Object greaterEqual(Object x, Object y) {
        return Long.valueOf(greaterEqual(n(x), n(y)));
    }

    /* access modifiers changed from: protected */
    public long greaterEqual(long x, long y) {
        return (long) (x >= y ? 1 : 0);
    }

    public boolean bool(Object x) {
        return n(x) != 0;
    }

    public Object inc(Object x, int inc) {
        return Long.valueOf(inc(n(x), inc));
    }

    /* access modifiers changed from: protected */
    public long inc(long x, int inc) {
        return ((long) inc) + x;
    }

    public Object character(String word, AbstractExpression exp) {
        return Long.valueOf((long) word.charAt(0));
    }

    public Object string(String word, AbstractExpression exp) {
        return toNumber(word, exp);
    }

    public Object number(String word, AbstractExpression exp) {
        return toNumber(word, exp);
    }

    public Object variable(Object value, AbstractExpression exp) {
        if (value == null) {
            return value;
        }
        if (value instanceof Number) {
            return value;
        }
        return toNumber(value.toString(), exp);
    }

    /* access modifiers changed from: protected */
    public Long toNumber(String word, AbstractExpression exp) {
        try {
            return Long.valueOf(NumberUtil.parseLong(word));
        } catch (Exception e) {
            try {
                return Long.valueOf(word);
            } catch (Exception e2) {
                try {
                    return Long.valueOf((long) Double.parseDouble(word));
                } catch (Exception e3) {
                    throw new EvalException((int) EvalException.EXP_NOT_NUMBER, exp, e3);
                }
            }
        }
    }
}
