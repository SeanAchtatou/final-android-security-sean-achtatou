package jp.hishidama.eval.lex;

import java.util.List;
import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.ShareExpValue;
import jp.hishidama.eval.lex.comment.CommentLex;
import jp.hishidama.util.CharUtil;

public class Lex {
    public static final int TYPE_CHAR = 2147483636;
    public static final int TYPE_EOF = Integer.MAX_VALUE;
    public static final int TYPE_ERR = -1;
    public static final int TYPE_NUM = 2147483633;
    public static final int TYPE_OPE = 2147483634;
    public static final int TYPE_STRING = 2147483635;
    public static final int TYPE_WORD = 2147483632;
    protected String NUMBER_CHAR = "._";
    protected String SPC_CHAR = " \t\r\n";
    protected List<CommentLex> commentList = null;
    protected ShareExpValue expShare;
    protected int len = 0;
    protected String ope;
    protected List<String>[] opeList;
    protected int pos = 0;
    protected String string;
    protected int type = -1;

    protected Lex(String str, List<String>[] lists, AbstractExpression paren, ShareExpValue exp) {
        this.string = str;
        this.opeList = lists;
        this.expShare = exp;
        if (this.expShare.paren == null) {
            this.expShare.paren = paren;
        }
    }

    /* access modifiers changed from: protected */
    public boolean isSpace(int pos2) {
        if (pos2 >= this.string.length()) {
            return true;
        }
        return this.SPC_CHAR.indexOf(this.string.charAt(pos2)) >= 0;
    }

    public void setCommentLexList(List<CommentLex> list) {
        this.commentList = list;
    }

    public List<CommentLex> getCommentLexList() {
        return this.commentList;
    }

    /* access modifiers changed from: protected */
    public CommentLex isCommentTop(int pos2) {
        if (this.commentList != null) {
            for (CommentLex c : this.commentList) {
                if (c.isTop(this.string, pos2) >= 0) {
                    return c;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean isNumberTop(int pos2) {
        if (pos2 >= this.string.length()) {
            return false;
        }
        char c = this.string.charAt(pos2);
        return '0' <= c && c <= '9';
    }

    /* access modifiers changed from: protected */
    public boolean isSpecialNumber(int pos2) {
        if (pos2 >= this.string.length()) {
            return false;
        }
        return this.NUMBER_CHAR.indexOf(this.string.charAt(pos2)) >= 0;
    }

    /* access modifiers changed from: protected */
    public String isOperator(int pos2) {
        List<String> list;
        for (int i = this.opeList.length - 1; i >= 0; i--) {
            if (pos2 + i < this.string.length() && (list = this.opeList[i]) != null) {
                int j = 0;
                while (j < list.size()) {
                    String ope2 = list.get(j);
                    int k = 0;
                    while (k <= i) {
                        if (this.string.charAt(pos2 + k) != ope2.charAt(k)) {
                            j++;
                        } else {
                            k++;
                        }
                    }
                    return ope2;
                }
                continue;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean isStringTop(int pos2) {
        if (pos2 >= this.string.length()) {
            return false;
        }
        return this.string.charAt(pos2) == '\"';
    }

    /* access modifiers changed from: protected */
    public boolean isStringEnd(int pos2) {
        return isStringTop(pos2);
    }

    /* access modifiers changed from: protected */
    public boolean isCharTop(int pos2) {
        if (pos2 >= this.string.length()) {
            return false;
        }
        return this.string.charAt(pos2) == '\'';
    }

    /* access modifiers changed from: protected */
    public boolean isCharEnd(int pos2) {
        return isCharTop(pos2);
    }

    public void check() {
        while (true) {
            if (!isSpace(this.pos)) {
                CommentLex comment = isCommentTop(this.pos);
                if (comment != null) {
                    this.pos += comment.topLength();
                    int end = comment.skip(this.string, this.pos);
                    if (end < 0) {
                        this.type = TYPE_EOF;
                        return;
                    }
                    this.pos = end;
                } else if (isStringTop(this.pos)) {
                    processString();
                    return;
                } else if (isCharTop(this.pos)) {
                    processChar();
                    return;
                } else {
                    String ope2 = isOperator(this.pos);
                    if (ope2 != null) {
                        this.type = TYPE_OPE;
                        this.ope = ope2;
                        this.len = ope2.length();
                        return;
                    }
                    boolean number = isNumberTop(this.pos);
                    this.type = number ? TYPE_NUM : TYPE_WORD;
                    this.len = 1;
                    while (!isSpace(this.pos + this.len) && isCommentTop(this.pos + this.len) == null) {
                        if ((number && isSpecialNumber(this.pos + this.len)) || isOperator(this.pos + this.len) == null) {
                            this.len++;
                        } else {
                            return;
                        }
                    }
                    return;
                }
            } else if (this.pos >= this.string.length()) {
                this.type = TYPE_EOF;
                this.len = 0;
                return;
            } else {
                this.pos++;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void processString() {
        int[] ret = new int[1];
        this.type = TYPE_STRING;
        this.len = 1;
        while (!isStringEnd(this.pos + this.len)) {
            this.len += getCharLen(this.pos + this.len, ret);
            if (this.pos + this.len >= this.string.length()) {
                this.type = TYPE_EOF;
                return;
            }
        }
        this.len++;
    }

    /* access modifiers changed from: protected */
    public void processChar() {
        int[] ret = new int[1];
        this.type = TYPE_CHAR;
        this.len = 1;
        while (!isCharEnd(this.pos + this.len)) {
            this.len += getCharLen(this.pos + this.len, ret);
            if (this.pos + this.len >= this.string.length()) {
                this.type = TYPE_EOF;
                return;
            }
        }
        this.len++;
    }

    /* access modifiers changed from: protected */
    public int getCharLen(int pos2, int[] ret) {
        CharUtil.escapeChar(this.string, pos2, this.string.length(), ret);
        return ret[0];
    }

    public Lex next() {
        this.pos += this.len;
        check();
        return this;
    }

    public int getType() {
        return this.type;
    }

    public String getOperator() {
        return this.ope;
    }

    public boolean isOperator(String ope2) {
        if (this.type == 2147483634) {
            return this.ope.equals(ope2);
        }
        return false;
    }

    public String getWord() {
        return this.string.substring(this.pos, this.pos + this.len);
    }

    public String getString() {
        return this.string;
    }

    public int getPos() {
        return this.pos;
    }

    public ShareExpValue getShare() {
        return this.expShare;
    }
}
