package jp.hishidama.eval.lex;

import java.util.ArrayList;
import java.util.List;
import jp.hishidama.eval.exp.ShareExpValue;
import jp.hishidama.eval.lex.comment.BlockComment;
import jp.hishidama.eval.lex.comment.CommentLex;
import jp.hishidama.eval.lex.comment.LineComment;
import jp.hishidama.eval.rule.ShareRuleValue;

public class LexFactory {
    protected List<CommentLex> defaultCommentList = new ArrayList();

    public LexFactory() {
        initCommentList();
    }

    /* access modifiers changed from: protected */
    public void initCommentList() {
        this.defaultCommentList.add(new BlockComment("/*", "*/"));
        this.defaultCommentList.add(new LineComment("//"));
    }

    public void setDefaultCommentLexList(List<CommentLex> list) {
        this.defaultCommentList = list;
    }

    public Lex create(String str, List<String>[] opeList, ShareRuleValue share, ShareExpValue exp) {
        Lex lex = new Lex(str, opeList, share.paren, exp);
        if (this.defaultCommentList != null) {
            lex.setCommentLexList(this.defaultCommentList);
        }
        return lex;
    }
}
