package jp.hishidama.eval.lex.comment;

import com.adwhirl.util.AdWhirlUtil;

public class LineComment extends CommentLex {
    public LineComment(String top) {
        super(top);
    }

    public int isEnd(String string, int pos) {
        if (pos >= string.length()) {
            return 0;
        }
        switch (string.charAt(pos)) {
            case AdWhirlUtil.NETWORK_TYPE_ADWHIRL:
                return 1;
            case AdWhirlUtil.NETWORK_TYPE_MOBCLIX:
            case AdWhirlUtil.NETWORK_TYPE_MDOTM:
            default:
                return -1;
            case AdWhirlUtil.NETWORK_TYPE_4THSCREEN:
                return (pos + 1 >= string.length() || string.charAt(pos + 1) != 10) ? 1 : 2;
        }
    }
}
