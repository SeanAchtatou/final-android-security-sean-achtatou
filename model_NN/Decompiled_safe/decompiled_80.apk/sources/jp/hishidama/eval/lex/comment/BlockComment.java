package jp.hishidama.eval.lex.comment;

public class BlockComment extends CommentLex {
    protected String end;

    public BlockComment(String top, String end2) {
        super(top);
        this.end = end2;
    }

    public String getEndString() {
        return this.end;
    }

    public int isEnd(String string, int pos) {
        return is(this.end, string, pos);
    }
}
