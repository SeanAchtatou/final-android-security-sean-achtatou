package jp.hishidama.eval.lex.comment;

public abstract class CommentLex {
    protected String top;

    public abstract int isEnd(String str, int i);

    protected CommentLex(String top2) {
        this.top = top2;
    }

    public int isTop(String string, int pos) {
        return is(this.top, string, pos);
    }

    /* access modifiers changed from: protected */
    public int is(String ope, String string, int pos) {
        int size = ope.length();
        int i = 0;
        while (i < size) {
            if (pos >= string.length()) {
                return -1;
            }
            if (string.charAt(pos) != ope.charAt(i)) {
                return -1;
            }
            i++;
            pos++;
        }
        return size;
    }

    public String getTopString() {
        return this.top;
    }

    public int topLength() {
        return this.top.length();
    }

    public int skip(String string, int pos) {
        while (pos <= string.length()) {
            int e = isEnd(string, pos);
            if (e >= 0) {
                return pos + e;
            }
            pos++;
        }
        return -1;
    }
}
