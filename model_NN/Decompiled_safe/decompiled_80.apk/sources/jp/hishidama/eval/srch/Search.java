package jp.hishidama.eval.srch;

import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.Col1Expression;
import jp.hishidama.eval.exp.Col2Expression;
import jp.hishidama.eval.exp.Col3Expression;
import jp.hishidama.eval.exp.FunctionExpression;
import jp.hishidama.eval.exp.WordExpression;

public interface Search {
    boolean end();

    void search(AbstractExpression abstractExpression);

    void search0(WordExpression wordExpression);

    boolean search1_begin(Col1Expression col1Expression);

    void search1_end(Col1Expression col1Expression);

    boolean search2_2(Col2Expression col2Expression);

    boolean search2_begin(Col2Expression col2Expression);

    void search2_end(Col2Expression col2Expression);

    boolean search3_2(Col3Expression col3Expression);

    boolean search3_3(Col3Expression col3Expression);

    boolean search3_begin(Col3Expression col3Expression);

    void search3_end(Col3Expression col3Expression);

    boolean searchFunc_2(FunctionExpression functionExpression);

    boolean searchFunc_begin(FunctionExpression functionExpression);

    void searchFunc_end(FunctionExpression functionExpression);
}
