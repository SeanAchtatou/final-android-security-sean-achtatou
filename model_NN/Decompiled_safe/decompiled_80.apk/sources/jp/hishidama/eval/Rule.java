package jp.hishidama.eval;

import jp.hishidama.eval.func.Function;
import jp.hishidama.eval.log.EvalLog;
import jp.hishidama.eval.oper.Operator;
import jp.hishidama.eval.var.Variable;

public abstract class Rule implements Cloneable {
    protected Function defaultFunc = null;
    protected EvalLog defaultLog = null;
    protected Operator defaultOper = null;
    protected Variable defaultVar = null;

    public abstract Expression parse(String str);

    public Variable getDefaultVariable() {
        return this.defaultVar;
    }

    public Rule defaultVariable(Variable var) {
        Rule rule = clone();
        rule.defaultVar = var;
        return rule;
    }

    public Function getDefaultFunction() {
        return this.defaultFunc;
    }

    public Rule defaultFunction(Function func) {
        Rule rule = clone();
        rule.defaultFunc = func;
        return rule;
    }

    public Operator getDefaultOperator() {
        return this.defaultOper;
    }

    public Rule defaultOperator(Operator oper) {
        Rule rule = clone();
        rule.defaultOper = oper;
        return rule;
    }

    public EvalLog getDefaultEvalLog() {
        return this.defaultLog;
    }

    public Rule defaultEvalLog(EvalLog log) {
        Rule rule = clone();
        rule.defaultLog = log;
        return rule;
    }

    /* access modifiers changed from: protected */
    public Rule clone() {
        try {
            return (Rule) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e.toString());
        }
    }
}
