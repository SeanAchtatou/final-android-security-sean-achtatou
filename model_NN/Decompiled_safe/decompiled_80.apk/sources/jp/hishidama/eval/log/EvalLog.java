package jp.hishidama.eval.log;

public interface EvalLog {
    void logEval(String str, Object obj);

    void logEval(String str, Object obj, Object obj2);

    void logEval(String str, Object obj, Object obj2, Object obj3);

    void logEvalFunction(String str, String str2, Object[] objArr, Object obj);
}
