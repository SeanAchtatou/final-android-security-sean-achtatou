package jp.hishidama.eval.log;

public class EvalLogAdapter implements EvalLog {
    public void logEval(String name, Object r) {
    }

    public void logEval(String name, Object x, Object r) {
    }

    public void logEval(String name, Object x, Object y, Object r) {
    }

    public void logEvalFunction(String name, String funcName, Object[] args, Object r) {
    }
}
