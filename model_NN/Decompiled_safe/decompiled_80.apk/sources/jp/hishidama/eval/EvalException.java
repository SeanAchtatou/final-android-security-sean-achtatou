package jp.hishidama.eval;

import jp.hishidama.eval.exp.AbstractExpression;
import jp.hishidama.eval.exp.StringExpression;
import jp.hishidama.eval.lex.Lex;

public class EvalException extends RuntimeException {
    public static final int EXP_FORBIDDEN_CALL = 2001;
    public static final int EXP_FUNC_CALL_ERROR = 2401;
    public static final int EXP_NOT_ARR_VALUE = 2201;
    public static final int EXP_NOT_CHAR = 2005;
    public static final int EXP_NOT_DEF_OBJ = 2104;
    public static final int EXP_NOT_DEF_VAR = 2103;
    public static final int EXP_NOT_FLD_VALUE = 2301;
    public static final int EXP_NOT_LET = 2004;
    public static final int EXP_NOT_LET_ARR = 2202;
    public static final int EXP_NOT_LET_FIELD = 2302;
    public static final int EXP_NOT_LET_VAR = 2102;
    public static final int EXP_NOT_NUMBER = 2003;
    public static final int EXP_NOT_STRING = 2006;
    public static final int EXP_NOT_VARIABLE = 2002;
    public static final int EXP_NOT_VAR_VALUE = 2101;
    public static final int PARSE_END_OF_STR = 1004;
    public static final int PARSE_INVALID_CHAR = 1003;
    public static final int PARSE_INVALID_OP = 1002;
    public static final int PARSE_NOT_FOUND_END_OP = 1001;
    public static final int PARSE_NOT_FUNC = 1101;
    public static final int PARSE_STILL_EXIST = 1005;
    protected String ename;
    protected int msgCode;
    protected String[] msgOpt;
    protected int pos;
    protected String string;
    protected String word;

    protected EvalException(RuntimeException e) {
        super(e);
        this.pos = -1;
        this.ename = StringExpression.NAME;
    }

    public EvalException(int msg, Lex lex) {
        this(msg, (String[]) null, lex);
    }

    public EvalException(int msg, String[] opt, Lex lex) {
        this.pos = -1;
        this.ename = StringExpression.NAME;
        this.msgCode = msg;
        this.msgOpt = opt;
        if (lex != null) {
            this.string = lex.getString();
            this.pos = lex.getPos();
            this.ename = StringExpression.NAME;
            this.word = lex.getWord();
        }
    }

    public EvalException(int msg, AbstractExpression exp, Throwable e) {
        this(msg, exp.getExpressionName(), exp.getWord(), exp.getString(), exp.getPos(), e);
    }

    public EvalException(int msg, String word2, AbstractExpression exp, Throwable e) {
        this(msg, exp.getExpressionName(), word2, exp.getString(), exp.getPos(), e);
    }

    public EvalException(int msg, String expName, String word2, String string2, int pos2, Throwable e) {
        this.pos = -1;
        this.ename = StringExpression.NAME;
        initException(e);
        this.msgCode = msg;
        this.string = string2;
        this.pos = pos2;
        this.ename = expName;
        this.word = word2;
    }

    /* access modifiers changed from: protected */
    public void initException(Throwable e) {
        while (e != null && e.getClass() == RuntimeException.class && e.getCause() != null) {
            e = e.getCause();
        }
        if (e != null) {
            super.initCause(e);
        }
    }

    public int getErrorCode() {
        return this.msgCode;
    }

    public String[] getOption() {
        return this.msgOpt;
    }

    public String getExpressionName() {
        return this.ename;
    }

    public String getWord() {
        return this.word;
    }

    public String getString() {
        return this.string;
    }

    public int getPos() {
        return this.pos;
    }

    public String toString() {
        return EvalExceptionFormatter.getDefault().toString(this);
    }
}
