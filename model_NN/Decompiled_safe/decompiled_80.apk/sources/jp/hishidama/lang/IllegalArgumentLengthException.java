package jp.hishidama.lang;

public class IllegalArgumentLengthException extends IllegalArgumentException {
    private static final long serialVersionUID = -1652752633469109078L;
    protected int len;
    protected int max;
    protected int min;

    public IllegalArgumentLengthException(int len2, int min2, int max2) {
        this.len = len2;
        this.min = min2;
        this.max = max2;
    }

    public IllegalArgumentLengthException(String message, int len2, int min2, int max2) {
        super(message);
        this.len = len2;
        this.min = min2;
        this.max = max2;
    }

    public int getLen() {
        return this.len;
    }

    public void setLen(int len2) {
        this.len = len2;
    }

    public int getMin() {
        return this.min;
    }

    public void setMin(int min2) {
        this.min = min2;
    }

    public int getMax() {
        return this.max;
    }

    public void setMax(int max2) {
        this.max = max2;
    }

    public String getMessage() {
        String msg = super.getMessage();
        if (msg == null) {
            msg = "";
        }
        return String.valueOf(msg) + " args.length=" + this.len + " (" + this.min + ".." + this.max + ")";
    }
}
