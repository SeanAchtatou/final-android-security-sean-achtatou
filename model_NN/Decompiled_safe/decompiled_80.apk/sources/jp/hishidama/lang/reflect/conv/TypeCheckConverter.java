package jp.hishidama.lang.reflect.conv;

public class TypeCheckConverter extends TypeConverter {
    protected Class<?> clazz;

    public TypeCheckConverter(Class<?> clazz2) {
        this.clazz = clazz2;
    }

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (this.clazz.isInstance(obj)) {
            return 32767;
        }
        return -1;
    }

    public Object convert(Object object) {
        if (object == null) {
            return null;
        }
        if (this.clazz.isInstance(object)) {
            return object;
        }
        throw new ClassCastException(String.valueOf(object.getClass().getName()) + " cannot be cast to " + this.clazz.getName());
    }
}
