package jp.hishidama.lang.reflect.conv;

public class DoubleConverter extends TypeConverter {
    public static final DoubleConverter INSTANCE = new DoubleConverter();

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj instanceof Double) {
            return 32767;
        }
        if (obj instanceof Float) {
            return 32766;
        }
        if (obj instanceof Long) {
            return 32765;
        }
        if (obj instanceof Integer) {
            return 32764;
        }
        if (obj instanceof Short) {
            return 32763;
        }
        if (obj instanceof Byte) {
            return 32762;
        }
        if (obj instanceof Number) {
            return 16383;
        }
        return 3;
    }

    public Double convert(Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof Double) {
            return (Double) object;
        }
        if (object instanceof Number) {
            return Double.valueOf(((Number) object).doubleValue());
        }
        return Double.valueOf(object.toString());
    }
}
