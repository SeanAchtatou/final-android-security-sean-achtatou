package jp.hishidama.lang.reflect.conv;

public class StringConverter extends TypeConverter {
    public static final StringConverter INSTANCE = new StringConverter();

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj instanceof String) {
            return 32767;
        }
        return 3;
    }

    public String convert(Object object) {
        if (object == null) {
            return null;
        }
        return object.toString();
    }
}
