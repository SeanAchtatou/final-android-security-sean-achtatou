package jp.hishidama.lang.reflect.conv;

public class ObjectConverter extends TypeConverter {
    public static final ObjectConverter INSTANCE = new ObjectConverter();

    public int match(Object obj) {
        return 2;
    }

    public Object convert(Object object) {
        return object;
    }
}
