package jp.hishidama.lang.reflect;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import jp.hishidama.lang.IllegalArgumentLengthException;
import jp.hishidama.lang.reflect.conv.TypeConverter;
import jp.hishidama.lang.reflect.conv.TypeConverterManager;

public class Invoker {
    protected TypeConverter[] convs;
    protected Method method;
    protected String name;
    protected TypeConverter objConv;

    public Invoker(String name2, Class<?> clazz, Method method2, TypeConverterManager manager) {
        this.name = name2;
        this.method = method2;
        initObjectConverter(clazz, manager);
        initArgsConverter(manager);
    }

    /* access modifiers changed from: protected */
    public void initObjectConverter(Class<?> clazz, TypeConverterManager manager) {
        this.objConv = manager.getConverter(clazz);
    }

    /* access modifiers changed from: protected */
    public void initArgsConverter(TypeConverterManager manager) {
        this.convs = getArgsConverter(this.method.getParameterTypes(), manager);
    }

    /* access modifiers changed from: protected */
    public TypeConverter[] getArgsConverter(Class<?>[] types, TypeConverterManager manager) {
        TypeConverter[] convs2 = new TypeConverter[types.length];
        for (int i = 0; i < convs2.length; i++) {
            TypeConverter conv = manager.getConverter(types[i]);
            if (conv == null) {
                StringBuilder sb = new StringBuilder(64);
                sb.append(this.name);
                sb.append(" class=");
                sb.append(types[i]);
                throw new UnsupportedOperationException(sb.toString());
            }
            convs2[i] = conv;
        }
        return convs2;
    }

    public String getName() {
        return this.name;
    }

    public TypeConverter[] getTypeConverter() {
        return this.convs;
    }

    public Object invoke(Object obj, Object... args) throws Exception {
        checkArgs(args);
        Object obj2 = objectConvert(obj);
        Object[] cargs = new Object[this.convs.length];
        for (int i = 0; i < this.convs.length; i++) {
            cargs[i] = this.convs[i].convert(args[i]);
        }
        return this.method.invoke(obj2, cargs);
    }

    /* access modifiers changed from: protected */
    public void checkArgs(Object... args) {
        if (args.length != this.convs.length) {
            throw new IllegalArgumentLengthException(this.name, args.length, this.convs.length, this.convs.length);
        }
    }

    /* access modifiers changed from: protected */
    public <T> T objectConvert(Object obj) {
        if (isStatic()) {
            return null;
        }
        if (this.objConv != null) {
            obj = this.objConv.convert(obj);
        }
        return obj;
    }

    /* access modifiers changed from: protected */
    public boolean isStatic() {
        if (this.method == null) {
            return false;
        }
        return Modifier.isStatic(this.method.getModifiers());
    }
}
