package jp.hishidama.lang.reflect.conv;

import java.util.Map;

public class MapConverter extends TypeConverter {
    public static final MapConverter INSTANCE = new MapConverter();

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj instanceof Map) {
            return 32767;
        }
        return -1;
    }

    public Map<?, ?> convert(Object object) {
        if (object == null) {
            return null;
        }
        return (Map) object;
    }
}
