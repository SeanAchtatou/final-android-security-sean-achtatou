package jp.hishidama.lang.reflect.conv;

import java.io.File;
import java.net.URI;

public class FileConverter extends TypeConverter {
    public static final FileConverter INSTANCE = new FileConverter();

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj instanceof File) {
            return 32767;
        }
        if (obj instanceof URI) {
            return 32765;
        }
        return 3;
    }

    public File convert(Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof File) {
            return (File) object;
        }
        if (object instanceof URI) {
            return new File((URI) object);
        }
        return new File(object.toString());
    }
}
