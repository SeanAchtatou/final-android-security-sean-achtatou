package jp.hishidama.lang.reflect.conv;

public class ByteConverter extends TypeConverter {
    public static final ByteConverter INSTANCE = new ByteConverter();

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj instanceof Byte) {
            return 32767;
        }
        if (obj instanceof Double) {
            return 16388;
        }
        if (obj instanceof Float) {
            return 16387;
        }
        if (obj instanceof Long) {
            return 16386;
        }
        if (obj instanceof Integer) {
            return 16385;
        }
        if (obj instanceof Short) {
            return 16384;
        }
        if (obj instanceof Number) {
            return 16383;
        }
        return 3;
    }

    public Byte convert(Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof Byte) {
            return (Byte) object;
        }
        if (object instanceof Number) {
            return Byte.valueOf(((Number) object).byteValue());
        }
        return Byte.valueOf(object.toString());
    }
}
