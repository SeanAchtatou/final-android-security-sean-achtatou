package jp.hishidama.lang.reflect.conv;

import java.lang.reflect.Array;

public class ArrayConverter extends TypeConverter {
    static final /* synthetic */ boolean $assertionsDisabled = (!ArrayConverter.class.desiredAssertionStatus());
    protected Class<?> clazz;
    protected TypeConverter conv;

    public ArrayConverter(Class<?> clazz2, TypeConverterManager manager) {
        if ($assertionsDisabled || clazz2.isArray()) {
            this.clazz = clazz2;
            this.conv = manager.getConverter(clazz2.getComponentType());
            return;
        }
        throw new AssertionError(clazz2);
    }

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (this.clazz.isInstance(obj)) {
            if (this.clazz.getComponentType() == obj.getClass().getComponentType()) {
                return 32767;
            }
            return 32766;
        } else if (!obj.getClass().isArray()) {
            return -1;
        } else {
            int r = 16383;
            int sz = Array.getLength(obj);
            for (int i = 0; i < sz; i++) {
                Object elem = Array.get(obj, i);
                if (elem != null) {
                    int t = this.conv.match(elem);
                    if (t <= -1) {
                        return -1;
                    }
                    r = Math.min(r, t);
                }
            }
            return r;
        }
    }

    public Object convert(Object object) {
        if (object == null) {
            return null;
        }
        if (this.clazz.isInstance(object)) {
            return object;
        }
        if (object.getClass().isArray()) {
            int sz = Array.getLength(object);
            Object arr = Array.newInstance(this.clazz.getComponentType(), sz);
            for (int i = 0; i < sz; i++) {
                Array.set(arr, i, this.conv.convert(Array.get(object, i)));
            }
            return arr;
        }
        Object arr2 = Array.newInstance(this.clazz.getComponentType(), 1);
        Array.set(arr2, 0, this.conv.convert(object));
        return arr2;
    }
}
