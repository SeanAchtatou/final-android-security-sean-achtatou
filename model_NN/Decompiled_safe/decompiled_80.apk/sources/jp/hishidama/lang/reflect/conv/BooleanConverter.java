package jp.hishidama.lang.reflect.conv;

public class BooleanConverter extends TypeConverter {
    public static final BooleanConverter INSTANCE = new BooleanConverter();

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj instanceof Boolean) {
            return 32767;
        }
        if (obj instanceof Number) {
            return 8191;
        }
        return 3;
    }

    public Boolean convert(Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof Boolean) {
            return (Boolean) object;
        }
        if (object instanceof Number) {
            return ((Number) object).intValue() != 0;
        }
        return Boolean.valueOf(object.toString());
    }
}
