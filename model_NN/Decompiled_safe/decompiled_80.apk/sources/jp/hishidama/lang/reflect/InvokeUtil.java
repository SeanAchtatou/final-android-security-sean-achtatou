package jp.hishidama.lang.reflect;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jp.hishidama.eval.lex.Lex;
import jp.hishidama.lang.IllegalArgumentLengthException;
import jp.hishidama.lang.reflect.conv.TypeConverter;
import jp.hishidama.lang.reflect.conv.TypeConverterManager;

public class InvokeUtil {
    protected static final int BOTH = 0;
    protected static final int DYNAMIC = 1;
    protected static final int STATIC = 2;
    protected Map<String, Methods> MAP;
    protected TypeConverterManager manager;

    public InvokeUtil() {
        this(new TypeConverterManager());
    }

    public InvokeUtil(TypeConverterManager manager2) {
        this.MAP = new HashMap();
        this.manager = manager2;
    }

    public TypeConverterManager getConverterManager() {
        return this.manager;
    }

    public void addMethods(Class<?> clazz, String prefix) {
        for (Method m : clazz.getMethods()) {
            addMethod(clazz, String.valueOf(prefix) + m.getName(), m);
        }
    }

    public void addMethod(Class<?> clazz, String name, Method method) {
        addInvoker(name, new Invoker(name, clazz, method, this.manager));
    }

    public void addInvoker(String name, Invoker invoker) {
        Methods mm = this.MAP.get(name);
        if (mm == null) {
            mm = createMethods(name);
            this.MAP.put(name, mm);
        }
        mm.add(invoker);
    }

    /* access modifiers changed from: protected */
    public Methods createMethods(String name) {
        return new Methods();
    }

    public Object invoke(String name, Object obj, Object... args) {
        Methods mm = this.MAP.get(name);
        if (mm == null) {
            throw new UnsupportedOperationException("name=" + name);
        }
        try {
            return mm.getInvoker(args, 0, false).invoke(obj, args);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    public Invoker getInstanceInvoker(String name, Object... args) {
        Methods mm = this.MAP.get(name);
        if (mm != null) {
            return mm.getInvoker(args, 1, true);
        }
        throw new UnsupportedOperationException("name=" + name);
    }

    public Invoker getStaticInvoker(String name, Object... args) {
        Methods mm = this.MAP.get(name);
        if (mm != null) {
            return mm.getInvoker(args, 2, true);
        }
        throw new UnsupportedOperationException("name=" + name);
    }

    protected static class Methods {
        protected static final Comparator<Invoker> COMP = new Comparator<Invoker>() {
            public int compare(Invoker o1, Invoker o2) {
                return o1.getTypeConverter().length - o2.getTypeConverter().length;
            }
        };
        protected List<Invoker> list = new ArrayList(4);

        protected Methods() {
        }

        public void add(Invoker invoker) {
            this.list.add(invoker);
            Collections.sort(this.list, COMP);
        }

        public Invoker getInvoker(Object[] args, int ds, boolean search) {
            return resolve(args, ds, search);
        }

        protected static class Index {
            public int max;
            public int min;

            public Index(int min2, int max2) {
                this.min = min2;
                this.max = max2;
            }
        }

        /* Debug info: failed to restart local var, previous not found, register: 7 */
        /* access modifiers changed from: protected */
        public Invoker resolve(Object[] args, int ds, boolean search) {
            Index index = getInvokerEqualsLength(args, ds);
            if (index.min == index.max) {
                return this.list.get(index.min);
            }
            if (index.min < index.max) {
                Invoker r = getInvokerMatchType(index, args, ds);
                if (r == null) {
                    r = this.list.get(index.min);
                }
                return r;
            } else if (search) {
                return null;
            } else {
                throw new IllegalArgumentLengthException(args.length, this.list.get(0).getTypeConverter().length, this.list.get(this.list.size() - 1).getTypeConverter().length);
            }
        }

        /* access modifiers changed from: protected */
        public Index getInvokerEqualsLength(Object[] args, int ds) {
            int min = Lex.TYPE_EOF;
            int max = Integer.MIN_VALUE;
            for (int i = 0; i < this.list.size(); i++) {
                Invoker v = this.list.get(i);
                switch (ds) {
                    case 1:
                        if (v.isStatic()) {
                            continue;
                        }
                        break;
                    case 2:
                        if (!v.isStatic()) {
                            continue;
                        }
                        break;
                }
                if (v.getTypeConverter().length == args.length) {
                    min = Math.min(min, i);
                    max = Math.max(max, i);
                }
            }
            return new Index(min, max);
        }

        /* access modifiers changed from: protected */
        public Invoker getInvokerMatchType(Index index, Object[] args, int ds) {
            int max = Integer.MIN_VALUE;
            Invoker ret = null;
            for (int i = index.min; i <= index.max; i++) {
                Invoker v = this.list.get(i);
                switch (ds) {
                    case 1:
                        if (v.isStatic()) {
                            continue;
                        }
                        break;
                    case 2:
                        if (!v.isStatic()) {
                            continue;
                        }
                        break;
                }
                TypeConverter[] convs = v.getTypeConverter();
                int cc = 0;
                int j = 0;
                while (true) {
                    if (j < args.length) {
                        int c = convs[j].match(args[j]);
                        if (c < 0) {
                            cc = -1;
                        } else {
                            cc += c;
                            j++;
                        }
                    }
                }
                if (cc >= 0 && max < cc) {
                    max = cc;
                    ret = v;
                }
            }
            return ret;
        }
    }
}
