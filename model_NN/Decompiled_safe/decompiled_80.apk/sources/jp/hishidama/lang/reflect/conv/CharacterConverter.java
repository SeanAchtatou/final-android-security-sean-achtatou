package jp.hishidama.lang.reflect.conv;

public class CharacterConverter extends TypeConverter {
    public static final CharacterConverter INSTANCE = new CharacterConverter();

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj instanceof Character) {
            return 32767;
        }
        if (obj instanceof Number) {
            return 16383;
        }
        return 3;
    }

    public Character convert(Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof Character) {
            return (Character) object;
        }
        if (object instanceof Number) {
            return Character.valueOf((char) ((Number) object).intValue());
        }
        return Character.valueOf(object.toString().charAt(0));
    }
}
