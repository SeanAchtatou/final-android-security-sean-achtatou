package jp.hishidama.lang.reflect.conv;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

public class URIConverter extends TypeConverter {
    public static final URIConverter INSTANCE = new URIConverter();

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj instanceof URI) {
            return 32767;
        }
        if (obj instanceof File) {
            return 32766;
        }
        return 3;
    }

    public URI convert(Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof URI) {
            return (URI) object;
        }
        if (object instanceof File) {
            return ((File) object).toURI();
        }
        try {
            return new URI(object.toString());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
