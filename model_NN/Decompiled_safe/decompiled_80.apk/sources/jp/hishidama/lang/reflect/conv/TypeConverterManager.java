package jp.hishidama.lang.reflect.conv;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class TypeConverterManager {
    protected Map<Class<?>, TypeConverter> MAP = null;

    public TypeConverter getConverter(Class<?> clazz) {
        if (clazz.isArray()) {
            return new ArrayConverter(clazz, this);
        }
        TypeConverter conv = findConverter(clazz);
        if (conv == null) {
            conv = getDefaultConverter(clazz);
        }
        return conv;
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    /* access modifiers changed from: protected */
    public TypeConverter findConverter(Class<?> clazz) {
        Map<Class<?>, TypeConverter> map = getConverterMap();
        if (map.containsKey(clazz)) {
            return map.get(clazz);
        }
        if (clazz == Object.class) {
            return ObjectConverter.INSTANCE;
        }
        for (Map.Entry<Class<?>, TypeConverter> entry : map.entrySet()) {
            if (entry.getKey().isAssignableFrom(clazz)) {
                TypeConverter conv = (TypeConverter) entry.getValue();
                map.put(clazz, conv);
                return conv;
            }
        }
        map.put(clazz, null);
        return null;
    }

    /* access modifiers changed from: protected */
    public TypeConverter getDefaultConverter(Class<?> clazz) {
        return new TypeCheckConverter(clazz);
    }

    public Map<Class<?>, TypeConverter> getConverterMap() {
        if (this.MAP == null) {
            Map<Class<?>, TypeConverter> map = new HashMap<>(32);
            initConverterMap(map);
            this.MAP = map;
        }
        return this.MAP;
    }

    /* access modifiers changed from: protected */
    public void initConverterMap(Map<Class<?>, TypeConverter> map) {
        map.put(Boolean.TYPE, BooleanConverter.INSTANCE);
        map.put(Boolean.class, BooleanConverter.INSTANCE);
        map.put(Byte.TYPE, ByteConverter.INSTANCE);
        map.put(Byte.class, ByteConverter.INSTANCE);
        map.put(Character.TYPE, CharacterConverter.INSTANCE);
        map.put(Character.class, CharacterConverter.INSTANCE);
        map.put(Double.TYPE, DoubleConverter.INSTANCE);
        map.put(Double.class, DoubleConverter.INSTANCE);
        map.put(File.class, FileConverter.INSTANCE);
        map.put(Float.TYPE, FloatConverter.INSTANCE);
        map.put(Float.class, FloatConverter.INSTANCE);
        map.put(Integer.TYPE, IntegerConverter.INSTANCE);
        map.put(Integer.class, IntegerConverter.INSTANCE);
        map.put(Long.TYPE, LongConverter.INSTANCE);
        map.put(Long.class, LongConverter.INSTANCE);
        map.put(Map.class, MapConverter.INSTANCE);
        map.put(Short.TYPE, ShortConverter.INSTANCE);
        map.put(Short.class, ShortConverter.INSTANCE);
        map.put(String.class, StringConverter.INSTANCE);
        map.put(URI.class, URIConverter.INSTANCE);
    }
}
