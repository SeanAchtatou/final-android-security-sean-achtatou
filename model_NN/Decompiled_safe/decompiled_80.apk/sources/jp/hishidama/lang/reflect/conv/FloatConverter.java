package jp.hishidama.lang.reflect.conv;

public class FloatConverter extends TypeConverter {
    public static final FloatConverter INSTANCE = new FloatConverter();

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj instanceof Float) {
            return 32767;
        }
        if (obj instanceof Double) {
            return 16384;
        }
        if (obj instanceof Long) {
            return 32766;
        }
        if (obj instanceof Integer) {
            return 32765;
        }
        if (obj instanceof Short) {
            return 32764;
        }
        if (obj instanceof Byte) {
            return 32763;
        }
        if (obj instanceof Number) {
            return 16383;
        }
        return 3;
    }

    public Float convert(Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof Float) {
            return (Float) object;
        }
        if (object instanceof Number) {
            return Float.valueOf(((Number) object).floatValue());
        }
        return Float.valueOf(object.toString());
    }
}
