package jp.hishidama.lang.reflect.conv;

public class IntegerConverter extends TypeConverter {
    public static final IntegerConverter INSTANCE = new IntegerConverter();

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj instanceof Integer) {
            return 32767;
        }
        if (obj instanceof Double) {
            return 16386;
        }
        if (obj instanceof Float) {
            return 16385;
        }
        if (obj instanceof Long) {
            return 16384;
        }
        if (obj instanceof Short) {
            return 32766;
        }
        if (obj instanceof Byte) {
            return 32765;
        }
        if (obj instanceof Number) {
            return 16383;
        }
        return 3;
    }

    public Integer convert(Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof Integer) {
            return (Integer) object;
        }
        if (object instanceof Number) {
            return Integer.valueOf(((Number) object).intValue());
        }
        return Integer.valueOf(object.toString());
    }
}
