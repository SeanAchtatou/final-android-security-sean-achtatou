package jp.hishidama.lang.reflect.conv;

public class ShortConverter extends TypeConverter {
    public static final ShortConverter INSTANCE = new ShortConverter();

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj instanceof Short) {
            return 32767;
        }
        if (obj instanceof Double) {
            return 16387;
        }
        if (obj instanceof Float) {
            return 16386;
        }
        if (obj instanceof Long) {
            return 16385;
        }
        if (obj instanceof Integer) {
            return 16384;
        }
        if (obj instanceof Byte) {
            return 32766;
        }
        if (obj instanceof Number) {
            return 16383;
        }
        return 3;
    }

    public Short convert(Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof Short) {
            return (Short) object;
        }
        if (object instanceof Number) {
            return Short.valueOf(((Number) object).shortValue());
        }
        return Short.valueOf(object.toString());
    }
}
