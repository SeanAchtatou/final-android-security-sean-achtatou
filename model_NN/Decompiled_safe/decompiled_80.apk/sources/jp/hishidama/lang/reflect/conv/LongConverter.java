package jp.hishidama.lang.reflect.conv;

public class LongConverter extends TypeConverter {
    public static final LongConverter INSTANCE = new LongConverter();

    public int match(Object obj) {
        if (obj == null) {
            return 1;
        }
        if (obj instanceof Long) {
            return 32767;
        }
        if (obj instanceof Double) {
            return 16385;
        }
        if (obj instanceof Float) {
            return 16384;
        }
        if (obj instanceof Integer) {
            return 32766;
        }
        if (obj instanceof Short) {
            return 32765;
        }
        if (obj instanceof Byte) {
            return 32764;
        }
        if (obj instanceof Number) {
            return 16383;
        }
        return 3;
    }

    public Long convert(Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof Long) {
            return (Long) object;
        }
        if (object instanceof Number) {
            return Long.valueOf(((Number) object).longValue());
        }
        return Long.valueOf(object.toString());
    }
}
