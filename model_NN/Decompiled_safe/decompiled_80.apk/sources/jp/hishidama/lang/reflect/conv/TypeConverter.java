package jp.hishidama.lang.reflect.conv;

public abstract class TypeConverter {
    protected static final int MATCH_EQUALS = 32767;
    protected static final int MATCH_NULL = 1;
    protected static final int MATCH_OBJECT = 2;
    protected static final int MATCH_STRING = 3;
    protected static final int UNMATCH = -1;

    public abstract Object convert(Object obj);

    public abstract int match(Object obj);
}
