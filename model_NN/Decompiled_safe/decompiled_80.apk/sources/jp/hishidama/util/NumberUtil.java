package jp.hishidama.util;

public class NumberUtil {
    public static long parseLong(String str) {
        String str2;
        int len;
        if (str == null || (len = (str2 = str.trim()).length()) <= 0) {
            return 0;
        }
        switch (str2.charAt(len - 1)) {
            case '.':
            case 'L':
            case 'l':
                len--;
                break;
        }
        if (len >= 3 && str2.charAt(0) == '0') {
            switch (str2.charAt(1)) {
                case 'B':
                case 'b':
                    return parseLongBin(str2, 2, len - 2);
                case 'O':
                case 'o':
                    return parseLongOct(str2, 2, len - 2);
                case 'X':
                case 'x':
                    return parseLongHex(str2, 2, len - 2);
            }
        }
        return parseLongDec(str2, 0, len);
    }

    public static long parseLongBin(String str) {
        if (str == null) {
            return 0;
        }
        return parseLongBin(str, 0, str.length());
    }

    public static long parseLongBin(String str, int pos, int len) {
        long ret = 0;
        int i = 0;
        int pos2 = pos;
        while (i < len) {
            ret *= 2;
            int pos3 = pos2 + 1;
            switch (str.charAt(pos2)) {
                case '0':
                    break;
                case '1':
                    ret++;
                    break;
                default:
                    throw new NumberFormatException(str.substring(pos3, len));
            }
            i++;
            pos2 = pos3;
        }
        return ret;
    }

    public static long parseLongOct(String str) {
        if (str == null) {
            return 0;
        }
        return parseLongOct(str, 0, str.length());
    }

    public static long parseLongOct(String str, int pos, int len) {
        long ret = 0;
        int i = 0;
        int pos2 = pos;
        while (i < len) {
            ret *= 8;
            int pos3 = pos2 + 1;
            char c = str.charAt(pos2);
            switch (c) {
                case '0':
                    break;
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                    ret += (long) (c - '0');
                    break;
                default:
                    throw new NumberFormatException(str.substring(pos3, len));
            }
            i++;
            pos2 = pos3;
        }
        return ret;
    }

    public static long parseLongDec(String str) {
        if (str == null) {
            return 0;
        }
        return parseLongDec(str, 0, str.length());
    }

    public static long parseLongDec(String str, int pos, int len) {
        long ret = 0;
        int i = 0;
        int pos2 = pos;
        while (i < len) {
            ret *= 10;
            int pos3 = pos2 + 1;
            char c = str.charAt(pos2);
            switch (c) {
                case '0':
                    break;
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    ret += (long) (c - '0');
                    break;
                default:
                    throw new NumberFormatException(str.substring(pos3, len));
            }
            i++;
            pos2 = pos3;
        }
        return ret;
    }

    public static long parseLongHex(String str) {
        if (str == null) {
            return 0;
        }
        return parseLongHex(str, 0, str.length());
    }

    public static long parseLongHex(String str, int pos, int len) {
        long ret = 0;
        int i = 0;
        int pos2 = pos;
        while (i < len) {
            ret *= 16;
            int pos3 = pos2 + 1;
            char c = str.charAt(pos2);
            switch (c) {
                case '0':
                    break;
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    ret += (long) (c - '0');
                    break;
                case 'A':
                case 'B':
                case 'C':
                case 'D':
                case 'E':
                case 'F':
                    ret += (long) ((c - 'A') + 10);
                    break;
                case 'a':
                case 'b':
                case 'c':
                case 'd':
                case 'e':
                case 'f':
                    ret += (long) ((c - 'a') + 10);
                    break;
                default:
                    throw new NumberFormatException(str.substring(pos3, len));
            }
            i++;
            pos2 = pos3;
        }
        return ret;
    }
}
