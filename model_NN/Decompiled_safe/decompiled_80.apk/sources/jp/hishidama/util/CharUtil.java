package jp.hishidama.util;

public class CharUtil {
    public static String escapeString(String str) {
        return escapeString(str, 0, str.length());
    }

    public static String escapeString(String str, int pos, int len) {
        StringBuilder sb = new StringBuilder(len);
        int end_pos = pos + len;
        int[] ret = new int[1];
        while (pos < end_pos) {
            char c = escapeChar(str, pos, end_pos, ret);
            if (ret[0] <= 0) {
                break;
            }
            sb.append(c);
            pos += ret[0];
        }
        return sb.toString();
    }

    public static char escapeChar(String str, int pos, int end_pos, int[] ret) {
        long code;
        int i;
        char c;
        if (pos >= end_pos) {
            ret[0] = 0;
            return 0;
        }
        char c2 = str.charAt(pos);
        if (c2 != '\\') {
            ret[0] = 1;
            return c2;
        }
        int pos2 = pos + 1;
        if (pos2 >= end_pos) {
            ret[0] = 1;
            return c2;
        }
        ret[0] = 2;
        char c3 = str.charAt(pos2);
        switch (c3) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
                long code2 = (long) (c3 - '0');
                for (int i2 = 1; i2 < 3 && (pos2 = pos2 + 1) < end_pos && (c = str.charAt(pos2)) >= '0' && c <= '7'; i2++) {
                    ret[0] = ret[0] + 1;
                    code2 = (code2 * 8) + ((long) (c - '0'));
                }
                return (char) ((int) code2);
            case 'b':
                return 8;
            case 'f':
                return 12;
            case 'n':
                return 10;
            case 'r':
                return 13;
            case 't':
                return 9;
            case 'u':
                long code3 = 0;
                for (int i3 = 0; i3 < 4 && (pos2 = pos2 + 1) < end_pos; i3++) {
                    char c4 = str.charAt(pos2);
                    if ('0' <= c4 && c4 <= '9') {
                        ret[0] = ret[0] + 1;
                        code = code3 * 16;
                        i = c4 - '0';
                    } else if ('a' <= c4 && c4 <= 'f') {
                        ret[0] = ret[0] + 1;
                        code = code3 * 16;
                        i = (c4 - 'a') + 10;
                    } else if ('A' <= c4 && c4 <= 'F') {
                        ret[0] = ret[0] + 1;
                        code = code3 * 16;
                        i = (c4 - 'A') + 10;
                    }
                    code3 = code + ((long) i);
                }
                return (char) ((int) code3);
            default:
                return c3;
        }
    }
}
