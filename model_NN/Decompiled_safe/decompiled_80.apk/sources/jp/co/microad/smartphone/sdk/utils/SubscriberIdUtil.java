package jp.co.microad.smartphone.sdk.utils;

import android.app.Activity;
import android.provider.Settings;
import jp.co.microad.smartphone.sdk.log.MLog;

public class SubscriberIdUtil {
    public static String KEY_SUBSCRIBER_ID = "MICROAD_SUBID";
    public static String KEY_VERSION = "MICROAD_VERSION";

    public static String get(Activity activity) {
        if (activity == null) {
            return null;
        }
        return new PreferencesLogic(activity, 0).getString(KEY_SUBSCRIBER_ID, "");
    }

    public static String put(Activity activity, String version, String subscriberId, boolean isEffective) {
        String newSubscriberId;
        if (activity == null) {
            return null;
        }
        PreferencesLogic pref = new PreferencesLogic(activity, 0);
        String prefId = pref.getString(KEY_SUBSCRIBER_ID, "");
        String prefVer = pref.getString(KEY_VERSION, "");
        if (prefVer == null || !prefVer.equals(version) || !StringUtil.isNotEmpty(prefId)) {
            if (isEffective) {
                newSubscriberId = CryptUtil.md5(Settings.Secure.getString(activity.getContentResolver(), "android_id"));
                MLog.d(" SubscriberID:" + newSubscriberId + " (ANDROID_ID)");
            } else {
                newSubscriberId = subscriberId;
                MLog.d(" SubscriberID:" + newSubscriberId + " (SettingsAPI)");
            }
            pref.edit().putString(KEY_VERSION, version).putString(KEY_SUBSCRIBER_ID, newSubscriberId).commit();
            return newSubscriberId;
        }
        MLog.d(" SubscriberID:" + prefId + " (SharedPreferences)");
        return prefId;
    }
}
