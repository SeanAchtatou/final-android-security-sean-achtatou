package jp.co.microad.smartphone.sdk.logic;

import android.location.Location;
import android.location.LocationManager;
import java.lang.ref.WeakReference;
import jp.co.microad.smartphone.sdk.MicroAdLayout;
import jp.co.microad.smartphone.sdk.log.MLog;

public class LocationLogic {
    public boolean beTimeout = false;
    WeakReference<MicroAdLayout> layoutRef;
    MLocationManager locManager;
    Location location;
    String provider;
    public Boolean requested = false;

    public LocationLogic(MicroAdLayout layout) {
        this.layoutRef = new WeakReference<>(layout);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001c, code lost:
        if (r3.locManager != null) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        r3.locManager = new jp.co.microad.smartphone.sdk.logic.LocationLogic.MLocationManager(r3, (android.location.LocationManager) r4.getSystemService("location"));
        r3.locManager.setUseLastKnownLocation(false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0033, code lost:
        r3.locManager.start();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0039, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003a, code lost:
        r0 = r1;
        jp.co.microad.smartphone.sdk.log.MLog.e("failed to requestUpdate. " + r0.getMessage());
        jp.co.microad.smartphone.sdk.log.MLog.d(" stacktrace", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void requestUpdate(android.content.Context r4) {
        /*
            r3 = this;
            java.lang.String r1 = "requestUpdate"
            jp.co.microad.smartphone.sdk.log.MLog.d(r1)
            java.lang.Boolean r1 = r3.requested
            monitor-enter(r1)
            java.lang.Boolean r2 = r3.requested     // Catch:{ all -> 0x005b }
            boolean r2 = r2.booleanValue()     // Catch:{ all -> 0x005b }
            if (r2 == 0) goto L_0x0012
            monitor-exit(r1)     // Catch:{ all -> 0x005b }
        L_0x0011:
            return
        L_0x0012:
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x005b }
            r3.requested = r2     // Catch:{ all -> 0x005b }
            monitor-exit(r1)     // Catch:{ all -> 0x005b }
            jp.co.microad.smartphone.sdk.logic.LocationLogic$MLocationManager r1 = r3.locManager     // Catch:{ Exception -> 0x0039 }
            if (r1 != 0) goto L_0x0033
            jp.co.microad.smartphone.sdk.logic.LocationLogic$MLocationManager r2 = new jp.co.microad.smartphone.sdk.logic.LocationLogic$MLocationManager     // Catch:{ Exception -> 0x0039 }
            java.lang.String r1 = "location"
            java.lang.Object r1 = r4.getSystemService(r1)     // Catch:{ Exception -> 0x0039 }
            android.location.LocationManager r1 = (android.location.LocationManager) r1     // Catch:{ Exception -> 0x0039 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0039 }
            r3.locManager = r2     // Catch:{ Exception -> 0x0039 }
            jp.co.microad.smartphone.sdk.logic.LocationLogic$MLocationManager r1 = r3.locManager     // Catch:{ Exception -> 0x0039 }
            r2 = 0
            r1.setUseLastKnownLocation(r2)     // Catch:{ Exception -> 0x0039 }
        L_0x0033:
            jp.co.microad.smartphone.sdk.logic.LocationLogic$MLocationManager r1 = r3.locManager     // Catch:{ Exception -> 0x0039 }
            r1.start()     // Catch:{ Exception -> 0x0039 }
            goto L_0x0011
        L_0x0039:
            r1 = move-exception
            r0 = r1
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "failed to requestUpdate. "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r0.getMessage()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            jp.co.microad.smartphone.sdk.log.MLog.e(r1)
            java.lang.String r1 = " stacktrace"
            jp.co.microad.smartphone.sdk.log.MLog.d(r1, r0)
            goto L_0x0011
        L_0x005b:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x005b }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.co.microad.smartphone.sdk.logic.LocationLogic.requestUpdate(android.content.Context):void");
    }

    public void removeUpdate() {
        MLog.d("removeUpdates");
        synchronized (this.requested) {
            this.requested = false;
        }
        try {
            if (this.locManager != null) {
                this.locManager.stop();
            }
        } catch (Exception e) {
            Exception e2 = e;
            MLog.e("failed to removeUpdate. " + e2.getMessage());
            MLog.d(" stacktrace", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void setLocation(Location location2) {
        if (location2 == null) {
            MLog.d("location == null");
            return;
        }
        MicroAdLayout layout = this.layoutRef.get();
        if (layout == null) {
            MLog.d("MicroAdLayoutが取得できませんでした");
            return;
        }
        layout.mad.latitude = location2.getLatitude();
        layout.mad.longitude = location2.getLongitude();
        MLog.d("latitude=" + layout.mad.latitude + " longitude=" + layout.mad.longitude);
        removeUpdate();
    }

    class MLocationManager extends BetterLocationManager {
        public MLocationManager(LocationManager locationManager) {
            super(locationManager);
        }

        /* access modifiers changed from: protected */
        public void onLocationProviderNotAvailable() {
        }

        /* access modifiers changed from: protected */
        public void onLocationProgress(long time) {
            MLog.d("location progress... " + time);
        }

        /* access modifiers changed from: protected */
        public void onLocationTimeout() {
            MLog.d("location timeout.");
            LocationLogic.this.removeUpdate();
            LocationLogic.this.beTimeout = true;
        }

        /* access modifiers changed from: protected */
        public void onUpdateLocation(Location location, int updateCount) {
            MLog.d("update location.");
            LocationLogic.this.setLocation(location);
        }
    }
}
