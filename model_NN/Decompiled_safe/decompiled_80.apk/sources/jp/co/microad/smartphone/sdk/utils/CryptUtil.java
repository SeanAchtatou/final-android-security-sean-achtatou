package jp.co.microad.smartphone.sdk.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import jp.co.microad.smartphone.sdk.log.MLog;

public class CryptUtil {
    private static final int ITER_COUNT = 128;
    private static final PBEParameterSpec PBE_PARAM_SPEC = new PBEParameterSpec(SALT, ITER_COUNT);
    private static final byte[] SALT = {-52, 115, 98, -64, 14, 40, 68, -106};

    public static String md5(String text) {
        if (StringUtil.isEmpty(text)) {
            return "";
        }
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(text.getBytes());
            return byteToString(md.digest());
        } catch (NoSuchAlgorithmException e) {
            MLog.e("暗号化に失敗しました", e);
            return "";
        }
    }

    public static String digest(String s, String algorithm) {
        try {
            return byteToString(MessageDigest.getInstance(algorithm).digest(s.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] stringToByte(String string) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            int n = string.length();
            for (int i = 0; i < n; i = i + 1 + 1) {
                os.write(Integer.parseInt(string.substring(i, i + 2), 16));
            }
            os.close();
            return os.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e.toString());
        } catch (Throwable th) {
            os.close();
            throw th;
        }
    }

    public static String byteToString(byte[] bytes) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            byte b = bytes[i] & 255;
            buffer.append(toHexString(bytes[i]));
        }
        return buffer.toString();
    }

    private static String toHexString(byte b) {
        return new String(new char[]{"0123456789abcdef".charAt((b >> 4) & 15), "0123456789abcdef".charAt(b & 15)});
    }

    public static byte[] objectToByte(Object object) {
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        try {
            ObjectOutputStream os = new ObjectOutputStream(byteArray);
            try {
                os.writeObject(object);
                os.flush();
                byteArray.flush();
                os.close();
                return byteArray.toByteArray();
            } catch (IOException e) {
                return null;
            } catch (Throwable th) {
                os.close();
                throw th;
            }
        } catch (IOException e2) {
            return null;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object byteToObject(byte[] r6) {
        /*
            r5 = 0
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0029 }
            r2.<init>()     // Catch:{ IOException -> 0x0029 }
            r2.write(r6)     // Catch:{ all -> 0x0024 }
            r2.close()     // Catch:{ IOException -> 0x0029 }
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x0029 }
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ IOException -> 0x0029 }
            byte[] r4 = r2.toByteArray()     // Catch:{ IOException -> 0x0029 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0029 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x0029 }
            java.lang.Object r3 = r1.readObject()     // Catch:{ IOException -> 0x0035, ClassNotFoundException -> 0x003d, all -> 0x0045 }
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x0029 }
        L_0x0023:
            return r3
        L_0x0024:
            r3 = move-exception
            r2.close()     // Catch:{ IOException -> 0x0029 }
            throw r3     // Catch:{ IOException -> 0x0029 }
        L_0x0029:
            r3 = move-exception
            r0 = r3
            java.lang.RuntimeException r3 = new java.lang.RuntimeException
            java.lang.String r4 = r0.toString()
            r3.<init>(r4)
            throw r3
        L_0x0035:
            r0 = move-exception
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x0029 }
        L_0x003b:
            r3 = r5
            goto L_0x0023
        L_0x003d:
            r0 = move-exception
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x0029 }
        L_0x0043:
            r3 = r5
            goto L_0x0023
        L_0x0045:
            r3 = move-exception
            if (r1 == 0) goto L_0x004b
            r1.close()     // Catch:{ IOException -> 0x0029 }
        L_0x004b:
            throw r3     // Catch:{ IOException -> 0x0029 }
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.co.microad.smartphone.sdk.utils.CryptUtil.byteToObject(byte[]):java.lang.Object");
    }

    public static String encrypt(String text, String key) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        byte[] btePassword = text.getBytes();
        SecretKey pbeKey = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(new PBEKeySpec(key.toCharArray()));
        Cipher descipher = Cipher.getInstance("PBEWithMD5AndDES");
        descipher.init(1, pbeKey, PBE_PARAM_SPEC);
        return byteToString(descipher.doFinal(btePassword));
    }

    public static String decrypt(String dec, String key) throws Exception {
        byte[] btePassword = stringToByte(dec);
        SecretKey pbeKey = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(new PBEKeySpec(key.toCharArray()));
        Cipher descipher = Cipher.getInstance("PBEWithMD5AndDES");
        descipher.init(2, pbeKey, PBE_PARAM_SPEC);
        return new String(descipher.doFinal(btePassword));
    }
}
