package jp.co.microad.smartphone.sdk;

import android.app.Activity;
import android.os.Build;
import android.view.Display;
import java.lang.ref.WeakReference;
import jp.co.microad.smartphone.sdk.entity.Settings;
import jp.co.microad.smartphone.sdk.log.MLog;
import jp.co.microad.smartphone.sdk.logic.HttpLogic;
import jp.co.microad.smartphone.sdk.utils.SettingsUtil;
import jp.co.microad.smartphone.sdk.utils.SpotIdUtil;

public class MicroAdJs {
    static final int HIGH = 409920;
    static final int LOW = 103680;
    static final int MEDIUM = 153600;
    WeakReference<Activity> activityRef;
    public String color = SettingsUtil.get(Settings.BACKGROUND_COLOR);
    public int height;
    HttpLogic httpLogic = new HttpLogic();
    public double latitude = 999.0d;
    public double longitude = 999.0d;
    public String model;
    public String os;
    String redirectUrl;
    public String release;
    public String spotId;
    public String subscriberId;
    public String version;
    public int width;

    public void setRedirectUrl(String redirectUrl2) {
        MLog.d("############ set RedirectURL:" + redirectUrl2 + "#################");
        this.redirectUrl = redirectUrl2;
    }

    public String getRedirectUrl() {
        return this.redirectUrl;
    }

    public static MicroAdJs getInstance(Activity activity, int maxHeight, int maxWidth) {
        Display disp = activity.getWindowManager().getDefaultDisplay();
        int height2 = maxHeight > 0 ? maxHeight : disp.getHeight();
        int width2 = maxWidth > 0 ? maxWidth : disp.getWidth();
        MicroAdJs mad = new MicroAdJs();
        mad.setActivityRef(activity);
        mad.height = height2;
        mad.width = width2;
        mad.os = "Android";
        mad.release = Build.VERSION.RELEASE;
        mad.version = Build.VERSION.SDK;
        mad.model = Build.MODEL;
        mad.spotId = SpotIdUtil.getSpotId(activity);
        return mad;
    }

    public boolean haveLocation() {
        return 0.0d <= this.latitude && this.latitude <= 180.0d && 0.0d <= this.longitude && this.longitude <= 360.0d;
    }

    public void setActivityRef(Activity activity) {
        this.activityRef = new WeakReference<>(activity);
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude2) {
        this.latitude = latitude2;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude2) {
        this.longitude = longitude2;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public String getSpotId() {
        return this.spotId;
    }

    public void setSpotId(String spotId2) {
        this.spotId = spotId2;
    }

    public String getSubscriberId() {
        return this.subscriberId;
    }

    public void setSubscriberId(String subscriberId2) {
        this.subscriberId = subscriberId2;
    }

    public String getOs() {
        return this.os;
    }

    public void setOs(String os2) {
        this.os = os2;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model2) {
        this.model = model2;
    }

    public void setWidth(int width2) {
        this.width = width2;
    }

    public void setHeight(int height2) {
        this.height = height2;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public String getRelease() {
        return this.release;
    }

    public void setRelease(String release2) {
        this.release = release2;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color2) {
        this.color = color2;
    }

    public String toString() {
        return "MicroAdJs [spotId=" + this.spotId + ", subscriberId=" + this.subscriberId + ", width=" + this.width + ", height=" + this.height + ", os=" + this.os + ", version=" + this.version + ", release=" + this.release + ", model=" + this.model + ", latitude=" + this.latitude + ", longitude=" + this.longitude + ", activityRef=" + this.activityRef + "]";
    }

    public boolean isLow() {
        return this.width * this.height <= LOW;
    }

    public boolean isMedium() {
        long area = (long) (this.width * this.height);
        return 103680 < area && area <= 153600;
    }

    public boolean isHigh() {
        long area = (long) (this.width * this.height);
        return 153600 < area && area <= 409920;
    }

    public boolean isExtraHigh() {
        return HIGH < this.width * this.height;
    }

    public void log(String msg) {
        MLog.d("[JS]" + msg);
    }

    public void log(String msg, Throwable t) {
        MLog.e("[JS]" + msg, t);
    }
}
