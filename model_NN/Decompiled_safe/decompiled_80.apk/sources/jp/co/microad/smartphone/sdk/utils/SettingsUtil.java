package jp.co.microad.smartphone.sdk.utils;

import android.app.Activity;
import java.io.IOException;
import java.util.Properties;
import jp.co.microad.smartphone.sdk.log.MLog;

public class SettingsUtil {
    public static String env = "";
    static Properties settings = new Properties();

    public static boolean isDebugg() {
        return StringUtil.isNotEmpty(env);
    }

    public static String get(String key) {
        if (settings.containsKey(key)) {
            return settings.getProperty(key);
        }
        MLog.w("key[" + key + "]が定義されていません。");
        return "";
    }

    public static void load(Activity activity) {
        String fileName;
        if (StringUtil.isEmpty(env)) {
            fileName = "settings.properties";
            MLog.d("load settings.properties...");
        } else {
            fileName = "settings_" + env + ".properties";
            MLog.d("load " + fileName + "...");
        }
        try {
            settings.load(ResourceUtil.getResourceAsStream(activity, fileName));
            if (isDebugg()) {
                for (Object key : settings.keySet()) {
                    MLog.d("  ->" + key + "=" + settings.getProperty(key.toString()));
                }
            }
        } catch (IOException e) {
            IOException e2 = e;
            MLog.e("定義ファイルの読み込みに失敗しました。", e2);
            throw new RuntimeException(e2);
        }
    }
}
