package jp.co.microad.smartphone.sdk.task;

import android.os.AsyncTask;
import android.webkit.WebView;
import java.lang.ref.WeakReference;
import jp.co.microad.smartphone.sdk.MicroAdJs;
import jp.co.microad.smartphone.sdk.logic.AdLogic;

public class AdTask extends AsyncTask<MicroAdJs, Integer, Void> {
    AdLogic adLogic = new AdLogic();
    String encryptKey;
    WeakReference<WebView> webViewRef;

    public AdTask(WebView webView) {
        this.webViewRef = new WeakReference<>(webView);
    }

    /* access modifiers changed from: protected */
    public Void doInBackground(MicroAdJs... params) {
        this.adLogic.loadAd(this.webViewRef.get(), params[0]);
        return null;
    }
}
