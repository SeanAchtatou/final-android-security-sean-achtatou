package jp.co.microad.smartphone.sdk.entity;

import jp.co.microad.smartphone.sdk.log.MLog;
import jp.co.microad.smartphone.sdk.utils.SettingsUtil;
import jp.co.microad.smartphone.sdk.utils.StringUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class Settings {
    public static String ANDROID_ID_FLG = "androidIdFlag";
    public static String BACKGROUND_COLOR = "backgroundColor";
    public static String GPS_FLG = "gpsFlag";
    public static String ROTATION_INTERVAL = "rotationInterval";
    public static String SUBSCRIBER_ID = "subscriberId";
    public String backgroundColor;
    public boolean gpsFlag;
    public boolean isEffectiveAndroidId;
    boolean loaded;
    public int rotationInterval;
    public String subscriberId;

    public void setLoaded(boolean loaded2) {
        this.loaded = loaded2;
    }

    public boolean isLoaded() {
        return this.loaded;
    }

    public Settings() {
        this.backgroundColor = "";
        this.gpsFlag = false;
        this.rotationInterval = 10;
        this.isEffectiveAndroidId = false;
        this.loaded = false;
        this.backgroundColor = SettingsUtil.get("backgroundColor");
        this.gpsFlag = Boolean.valueOf(SettingsUtil.get("gpsFlag")).booleanValue();
        this.rotationInterval = Integer.valueOf(SettingsUtil.get("rotationInterval")).intValue();
    }

    public void load(String json) {
        boolean z;
        boolean z2;
        MLog.d(json);
        try {
            JSONObject obj = new JSONObject(json);
            String backgroundColor2 = obj.getString(BACKGROUND_COLOR);
            String gpsFlag2 = obj.getString(GPS_FLG);
            String rotationInterval2 = obj.getString(ROTATION_INTERVAL);
            String flg = obj.getString(ANDROID_ID_FLG);
            this.subscriberId = obj.getString(SUBSCRIBER_ID);
            if (StringUtil.isNotEmpty(backgroundColor2) && !"null".equals(backgroundColor2)) {
                this.backgroundColor = backgroundColor2;
            }
            if ("1".equals(gpsFlag2)) {
                z = true;
            } else {
                z = false;
            }
            this.gpsFlag = z;
            try {
                int i = Integer.valueOf(rotationInterval2).intValue();
                if (i != 0) {
                    this.rotationInterval = i;
                }
            } catch (NumberFormatException e) {
                MLog.e("NumberFormartException on rotationInterval. rotationInterval=" + rotationInterval2, e);
            }
            if ("1".equals(flg)) {
                z2 = true;
            } else {
                z2 = false;
            }
            this.isEffectiveAndroidId = z2;
        } catch (JSONException e2) {
            MLog.e("failed to parse json.", e2);
        }
    }

    public String toString() {
        return "Settings [backgroundColor=" + this.backgroundColor + ", gpsFlag=" + this.gpsFlag + ", rotationInterval=" + this.rotationInterval + ", subscriberId=" + this.subscriberId + ", isEffectiveAndroidId=" + this.isEffectiveAndroidId + ", loaded=" + this.loaded + "]";
    }
}
