package jp.co.microad.smartphone.sdk.utils;

import android.os.Build;
import java.util.Locale;

public class UserAgentUtil {
    static final String SEPALATOR = " ";

    public static String getUserAgent() {
        StringBuilder ua = new StringBuilder();
        ua.append("Android").append(SEPALATOR);
        ua.append(Build.VERSION.RELEASE == null ? "" : Build.VERSION.RELEASE).append(SEPALATOR);
        ua.append(Build.BRAND == null ? "" : Build.BRAND).append(SEPALATOR);
        ua.append(Locale.getDefault().toString()).append(SEPALATOR);
        ua.append(Build.MODEL == null ? "" : Build.BRAND).append(SEPALATOR);
        return ua.toString();
    }
}
