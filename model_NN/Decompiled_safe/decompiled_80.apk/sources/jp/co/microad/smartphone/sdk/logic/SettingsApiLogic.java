package jp.co.microad.smartphone.sdk.logic;

import android.app.Activity;
import android.os.Build;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import jp.co.microad.smartphone.sdk.entity.Settings;
import jp.co.microad.smartphone.sdk.log.MLog;
import jp.co.microad.smartphone.sdk.utils.SettingsUtil;
import jp.co.microad.smartphone.sdk.utils.StringUtil;
import jp.co.microad.smartphone.sdk.utils.SubscriberIdUtil;

public class SettingsApiLogic {
    HttpLogic httpLogic = new HttpLogic();

    public Settings loadSettings(Activity activity, String spotId) {
        MLog.d("@@@ loading settings.");
        Settings settings = new Settings();
        String settingsurl = SettingsUtil.get("settingsurl");
        String version = Build.VERSION.RELEASE;
        try {
            String url = String.format(settingsurl, spotId, URLEncoder.encode(version, "UTF-8"), URLEncoder.encode(Build.MODEL, "UTF-8"));
            MLog.d(url);
            String response = this.httpLogic.doGet(url, "");
            if (StringUtil.isEmpty(response)) {
                MLog.e("設定情報が取得できませんでした");
            } else {
                settings.load(response);
                settings.subscriberId = SubscriberIdUtil.put(activity, version, settings.subscriberId, settings.isEffectiveAndroidId);
                if (StringUtil.isNotEmpty(settings.subscriberId)) {
                    settings.setLoaded(true);
                }
                MLog.d("@@@ loaded settings. " + settings.toString());
            }
            return settings;
        } catch (UnsupportedEncodingException e) {
            MLog.e("エンコードに失敗しました");
            throw new RuntimeException(e);
        }
    }
}
