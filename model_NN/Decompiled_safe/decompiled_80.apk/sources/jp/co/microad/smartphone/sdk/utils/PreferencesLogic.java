package jp.co.microad.smartphone.sdk.utils;

import android.app.Activity;
import android.content.SharedPreferences;

public class PreferencesLogic {
    public static final String PREF_KEY = "MICROAD";
    SharedPreferences.Editor editor;
    SharedPreferences pref;

    public PreferencesLogic(Activity activity, int mode) {
        this.pref = activity.getSharedPreferences("MICROAD", mode);
    }

    public String getString(String key, String defValue) {
        return this.pref.getString(key, defValue);
    }

    public SharedPreferences.Editor edit() {
        this.editor = this.pref.edit();
        return this.editor;
    }

    public SharedPreferences.Editor putString(String key, String value) {
        return this.editor.putString(key, value);
    }

    public SharedPreferences.Editor putInt(String key, int value) {
        return this.editor.putInt(key, value);
    }

    public SharedPreferences.Editor putLong(String key, long value) {
        return this.editor.putLong(key, value);
    }

    public SharedPreferences.Editor putFloat(String key, float value) {
        return this.editor.putFloat(key, value);
    }

    public SharedPreferences.Editor putBoolean(String key, boolean value) {
        return this.editor.putBoolean(key, value);
    }

    public SharedPreferences.Editor remove(String key) {
        return this.editor.remove(key);
    }

    public SharedPreferences.Editor clear() {
        return this.editor.clear();
    }

    public boolean commit() {
        return this.editor.commit();
    }
}
