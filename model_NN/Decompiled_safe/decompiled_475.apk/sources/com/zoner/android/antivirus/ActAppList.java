package com.zoner.android.antivirus;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.zoner.android.antivirus.AppInfoStorage;
import com.zoner.android.antivirus.DbPhoneFilter;
import java.util.Locale;
import java.util.Map;

public class ActAppList extends ListActivity {
    private static final int DIALOG_APPLIST = 1;
    private static final int DIALOG_LOAD_PROGRESS = 5;
    private static final int DIALOG_PERMLIST = 2;
    private static final int DIALOG_SORT_APPS = 3;
    private static final int DIALOG_SORT_PERMS = 4;
    /* access modifiers changed from: private */
    public AppListFields mFields;
    /* access modifiers changed from: private */
    public AsyncTask<Void, Integer, AppInfoStorage> mInitTask;
    /* access modifiers changed from: private */
    public boolean mInstalled = false;

    private class AppListFields {
        public AppInfoStorage appInfoStorage;
        public AppInfoStorage.CurrList currList;
        public Drawable icon;
        public String id;
        public String name;
        public ProgressDialog progressDlg;
        public InstallReceiver receiver;
        public AppInfoStorage.SortBy sortAppsBy;
        public AppInfoStorage.SortBy sortPermsBy;

        private AppListFields() {
            this.progressDlg = null;
            this.sortAppsBy = AppInfoStorage.SortBy.LEVEL;
            this.sortPermsBy = AppInfoStorage.SortBy.LEVEL;
        }

        /* synthetic */ AppListFields(ActAppList actAppList, AppListFields appListFields) {
            this();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mFields = (AppListFields) getLastNonConfigurationInstance();
        if (this.mFields == null) {
            this.mFields = new AppListFields(this, null);
        }
        registerInstallReceiver();
        if (this.mFields.appInfoStorage == null) {
            this.mInitTask = new AppListTask().execute(new Void[0]);
        } else {
            this.mFields.appInfoStorage.showList(this, this.mFields.currList);
            if (this.mFields.currList == AppInfoStorage.CurrList.APP_LIST) {
                setTitle(String.format(Locale.ENGLISH, getString(R.string.title_appinfo_apps), Integer.valueOf(this.mFields.appInfoStorage.mAppCount), Integer.valueOf(this.mFields.appInfoStorage.mPermCount)));
            } else {
                setTitle(String.format(Locale.ENGLISH, getString(R.string.title_appinfo_perms), Integer.valueOf(this.mFields.appInfoStorage.mPermCount), Integer.valueOf(this.mFields.appInfoStorage.mAppCount)));
            }
        }
        getListView().setTextFilterEnabled(true);
        registerForContextMenu(getListView());
    }

    public void onDestroy() {
        if (this.mFields.receiver != null) {
            unregisterReceiver(this.mFields.receiver);
        }
        this.mFields.receiver = null;
        super.onDestroy();
    }

    public void onBackPressed() {
        if (this.mFields.currList == AppInfoStorage.CurrList.PERM_LIST) {
            this.mFields.currList = AppInfoStorage.CurrList.APP_LIST;
            setTitle(String.format(Locale.ENGLISH, getString(R.string.title_appinfo_apps), Integer.valueOf(this.mFields.appInfoStorage.mAppCount), Integer.valueOf(this.mFields.appInfoStorage.mPermCount)));
            this.mFields.appInfoStorage.showList(this, this.mFields.currList);
            return;
        }
        super.onBackPressed();
    }

    public Object onRetainNonConfigurationInstance() {
        return this.mFields;
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Map<String, Object> map = (Map) l.getItemAtPosition(position);
        this.mFields.name = (String) map.get("name");
        this.mFields.id = (String) map.get(DbPhoneFilter.DbColumns.COLUMN_ID);
        Object icon = map.get("icon");
        if (icon instanceof Drawable) {
            this.mFields.icon = (Drawable) icon;
            showDialog(2);
            return;
        }
        this.mFields.icon = getResources().getDrawable(((Integer) icon).intValue());
        showDialog(1);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                final SimpleAdapter appsAdapter = this.mFields.appInfoStorage.getSubListAdapter(this.mFields.id, AppInfoStorage.CurrList.PERM_LIST);
                AlertDialog dlgApps = new AlertDialog.Builder(this).setIcon(resizeIcon(this.mFields.icon)).setTitle(String.valueOf(getString(R.string.appinfo_permdialog_title)) + "\n" + this.mFields.name).setAdapter(appsAdapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int pos) {
                        Map<String, Object> map = (Map) appsAdapter.getItem(pos);
                        ActAppList.this.mFields.name = (String) map.get("name");
                        ActAppList.this.mFields.id = (String) map.get(DbPhoneFilter.DbColumns.COLUMN_ID);
                        ActAppList.this.mFields.icon = (Drawable) map.get("icon");
                        ActAppList.this.dismissDialog(1);
                        ActAppList.this.showDialog(2);
                    }
                }).create();
                dlgApps.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        ActAppList.this.removeDialog(1);
                    }
                });
                return dlgApps;
            case 2:
                final SimpleAdapter permsAdapter = this.mFields.appInfoStorage.getSubListAdapter(this.mFields.id, AppInfoStorage.CurrList.APP_LIST);
                AlertDialog.Builder builder = new AlertDialog.Builder(this).setIcon(resizeIcon(this.mFields.icon)).setTitle(String.valueOf(getString(R.string.appinfo_appdialog_title)) + "\n" + this.mFields.name);
                if (permsAdapter.getCount() == 0) {
                    builder.setMessage((int) R.string.appinfo_no_perms);
                } else {
                    builder.setAdapter(permsAdapter, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int pos) {
                            Map<String, Object> map = (Map) permsAdapter.getItem(pos);
                            ActAppList.this.mFields.name = (String) map.get("name");
                            ActAppList.this.mFields.id = (String) map.get(DbPhoneFilter.DbColumns.COLUMN_ID);
                            ActAppList.this.mFields.icon = ActAppList.this.getResources().getDrawable(((Integer) map.get("icon")).intValue());
                            ActAppList.this.dismissDialog(2);
                            ActAppList.this.showDialog(1);
                        }
                    });
                }
                AlertDialog dlgPerms = builder.create();
                dlgPerms.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        ActAppList.this.removeDialog(2);
                    }
                });
                return dlgPerms;
            case 3:
                CharSequence[] appItems = new CharSequence[DIALOG_SORT_PERMS];
                appItems[0] = getString(R.string.appinfo_opts_name);
                appItems[1] = getString(R.string.appinfo_opts_level);
                appItems[2] = getString(R.string.appinfo_opts_permcount);
                appItems[3] = getString(R.string.appinfo_opts_package);
                return new AlertDialog.Builder(this).setTitle(getString(R.string.appinfo_opts_sort_title)).setIcon(17301661).setPositiveButton(getString(R.string.appinfo_opts_sort_asc), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ActAppList.this.mFields.appInfoStorage.sortList(ActAppList.this.mFields.sortAppsBy, true, ActAppList.this.mFields.currList);
                        ActAppList.this.mFields.appInfoStorage.showList(ActAppList.this, ActAppList.this.mFields.currList);
                    }
                }).setNegativeButton(getString(R.string.appinfo_opts_sort_desc), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ActAppList.this.mFields.appInfoStorage.sortList(ActAppList.this.mFields.sortAppsBy, false, ActAppList.this.mFields.currList);
                        ActAppList.this.mFields.appInfoStorage.showList(ActAppList.this, ActAppList.this.mFields.currList);
                    }
                }).setSingleChoiceItems(appItems, 1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch (item) {
                            case 1:
                                ActAppList.this.mFields.sortAppsBy = AppInfoStorage.SortBy.LEVEL;
                                return;
                            case 2:
                                ActAppList.this.mFields.sortAppsBy = AppInfoStorage.SortBy.COUNT;
                                return;
                            case 3:
                                ActAppList.this.mFields.sortAppsBy = AppInfoStorage.SortBy.ID;
                                return;
                            default:
                                ActAppList.this.mFields.sortAppsBy = AppInfoStorage.SortBy.NAME;
                                return;
                        }
                    }
                }).create();
            case DIALOG_SORT_PERMS /*4*/:
                CharSequence[] permItems = new CharSequence[DIALOG_SORT_PERMS];
                permItems[0] = getString(R.string.perminfo_opts_name);
                permItems[1] = getString(R.string.perminfo_opts_level);
                permItems[2] = getString(R.string.perminfo_opts_appcount);
                permItems[3] = getString(R.string.perminfo_opts_prefix);
                return new AlertDialog.Builder(this).setTitle(getString(R.string.appinfo_opts_sort_title)).setIcon(17301661).setPositiveButton(getString(R.string.appinfo_opts_sort_asc), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ActAppList.this.mFields.appInfoStorage.sortList(ActAppList.this.mFields.sortPermsBy, true, ActAppList.this.mFields.currList);
                        ActAppList.this.mFields.appInfoStorage.showList(ActAppList.this, ActAppList.this.mFields.currList);
                    }
                }).setNegativeButton(getString(R.string.appinfo_opts_sort_desc), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ActAppList.this.mFields.appInfoStorage.sortList(ActAppList.this.mFields.sortPermsBy, false, ActAppList.this.mFields.currList);
                        ActAppList.this.mFields.appInfoStorage.showList(ActAppList.this, ActAppList.this.mFields.currList);
                    }
                }).setSingleChoiceItems(permItems, 1, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch (item) {
                            case 1:
                                ActAppList.this.mFields.sortPermsBy = AppInfoStorage.SortBy.LEVEL;
                                return;
                            case 2:
                                ActAppList.this.mFields.sortPermsBy = AppInfoStorage.SortBy.COUNT;
                                return;
                            case 3:
                                ActAppList.this.mFields.sortPermsBy = AppInfoStorage.SortBy.ID;
                                return;
                            default:
                                ActAppList.this.mFields.sortPermsBy = AppInfoStorage.SortBy.NAME;
                                return;
                        }
                    }
                }).create();
            case DIALOG_LOAD_PROGRESS /*5*/:
                this.mFields.progressDlg = new LoadDialog(this);
                this.mFields.progressDlg.setMessage(getString(R.string.appinfo_progress_loading));
                return this.mFields.progressDlg;
            default:
                return null;
        }
    }

    private Drawable resizeIcon(Drawable icon) {
        int size = (int) (3.5d * ((double) getResources().getDisplayMetrics().density) * ((double) new TextView(this).getTextSize()));
        return new BitmapDrawable(Bitmap.createScaledBitmap(((BitmapDrawable) icon).getBitmap(), size, size, true));
    }

    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        if (this.mFields.currList != AppInfoStorage.CurrList.PERM_LIST) {
            Map<String, Object> map = (Map) getListView().getItemAtPosition(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
            this.mFields.name = (String) map.get("name");
            this.mFields.id = (String) map.get(DbPhoneFilter.DbColumns.COLUMN_ID);
            this.mFields.icon = (Drawable) map.get("icon");
            getMenuInflater().inflate(R.menu.appinfo_ctx, menu);
            menu.setHeaderIcon(this.mFields.icon);
            menu.setHeaderTitle(String.valueOf(getString(R.string.appinfo_ctx_title)) + "\n" + this.mFields.name);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.appinfo_ctx_start /*2131361965*/:
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage(this.mFields.id);
                if (launchIntent != null) {
                    try {
                        launchIntent.addCategory("android.intent.category.LAUNCHER");
                        launchIntent.setFlags(274726912);
                        startActivity(launchIntent);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(this, getString(R.string.appinfo_app_start_failed), 0).show();
                    }
                } else {
                    Toast.makeText(this, getString(R.string.appinfo_app_start_failed), 0).show();
                }
                return true;
            case R.id.appinfo_ctx_info /*2131361966*/:
                showDialog(2);
                return true;
            case R.id.appinfo_ctx_uninstall /*2131361967*/:
                startActivity(new Intent("android.intent.action.DELETE", Uri.parse("package:" + this.mFields.id)));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        MenuInflater inflater = getMenuInflater();
        if (this.mFields.currList == AppInfoStorage.CurrList.APP_LIST) {
            inflater.inflate(R.menu.appinfo, menu);
            return true;
        }
        inflater.inflate(R.menu.perminfo, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.appinfo_showperms /*2131361962*/:
                this.mFields.currList = AppInfoStorage.CurrList.PERM_LIST;
                setTitle(String.format(Locale.ENGLISH, getString(R.string.title_appinfo_perms), Integer.valueOf(this.mFields.appInfoStorage.mPermCount), Integer.valueOf(this.mFields.appInfoStorage.mAppCount)));
                this.mFields.appInfoStorage.showList(this, this.mFields.currList);
                return true;
            case R.id.appinfo_sort /*2131361963*/:
                showDialog(3);
                return true;
            case R.id.appinfo_filter /*2131361964*/:
            case R.id.perminfo_filter /*2131361982*/:
                ((InputMethodManager) getSystemService("input_method")).showSoftInput(getListView(), 1);
                return true;
            case R.id.perminfo_sort /*2131361981*/:
                showDialog(DIALOG_SORT_PERMS);
                return true;
            default:
                if (item.hasSubMenu()) {
                    return false;
                }
                Toast.makeText(this, item.getTitle(), 0).show();
                return true;
        }
    }

    class AppListTask extends AsyncTask<Void, Integer, AppInfoStorage> {
        AppListTask() {
        }

        /* access modifiers changed from: protected */
        public AppInfoStorage doInBackground(Void... params) {
            ActAppList.this.mFields.currList = AppInfoStorage.CurrList.APP_LIST;
            return new AppInfoStorage(this, ActAppList.this.getPackageManager(), ActAppList.this.getApplicationContext());
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            if (ActAppList.this.mFields.progressDlg == null) {
                ActAppList.this.showDialog(ActAppList.DIALOG_LOAD_PROGRESS);
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(AppInfoStorage storage) {
            ActAppList.this.mFields.appInfoStorage = storage;
            ActAppList.this.setTitle(String.format(Locale.ENGLISH, ActAppList.this.getString(R.string.title_appinfo_apps), Integer.valueOf(storage.mAppCount), Integer.valueOf(storage.mPermCount)));
            ActAppList.this.mFields.appInfoStorage.showList(ActAppList.this, ActAppList.this.mFields.currList);
            if (ActAppList.this.mFields.progressDlg != null) {
                ActAppList.this.mFields.progressDlg.dismiss();
                ActAppList.this.mFields.progressDlg = null;
            }
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            if (!ActAppList.this.mInstalled) {
                ActAppList.this.finish();
                return;
            }
            ActAppList.this.mInstalled = false;
            ActAppList.this.mInitTask = new AppListTask().execute(new Void[0]);
        }
    }

    /* access modifiers changed from: package-private */
    public void registerInstallReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addDataScheme("package");
        filter.addAction("android.intent.action.PACKAGE_ADDED");
        filter.addAction("android.intent.action.PACKAGE_REMOVED");
        this.mFields.receiver = new InstallReceiver(this, null);
        registerReceiver(this.mFields.receiver, new IntentFilter(filter));
    }

    private class InstallReceiver extends BroadcastReceiver {
        private InstallReceiver() {
        }

        /* synthetic */ InstallReceiver(ActAppList actAppList, InstallReceiver installReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            if (ActAppList.this.mInitTask != null || ActAppList.this.mFields.appInfoStorage != null) {
                if (ActAppList.this.mInitTask != null && ActAppList.this.mFields.appInfoStorage == null) {
                    ActAppList.this.mInstalled = true;
                    ActAppList.this.mInitTask.cancel(true);
                } else if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
                    if (ActAppList.this.mFields.appInfoStorage.addPackage(intent.getDataString().substring(8))) {
                        ActAppList.this.mFields.appInfoStorage.showList(ActAppList.this, ActAppList.this.mFields.currList);
                        if (ActAppList.this.mFields.currList == AppInfoStorage.CurrList.APP_LIST) {
                            ActAppList.this.setTitle(String.format(Locale.ENGLISH, ActAppList.this.getString(R.string.title_appinfo_apps), Integer.valueOf(ActAppList.this.mFields.appInfoStorage.mAppCount), Integer.valueOf(ActAppList.this.mFields.appInfoStorage.mPermCount)));
                        } else {
                            ActAppList.this.setTitle(String.format(Locale.ENGLISH, ActAppList.this.getString(R.string.title_appinfo_perms), Integer.valueOf(ActAppList.this.mFields.appInfoStorage.mPermCount), Integer.valueOf(ActAppList.this.mFields.appInfoStorage.mAppCount)));
                        }
                        try {
                            ActAppList.this.dismissDialog(1);
                        } catch (Exception e) {
                        }
                        try {
                            ActAppList.this.dismissDialog(2);
                        } catch (Exception e2) {
                        }
                    }
                } else if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
                    ActAppList.this.mFields.appInfoStorage.removePackage(intent.getDataString().substring(8));
                    ActAppList.this.mFields.appInfoStorage.showList(ActAppList.this, ActAppList.this.mFields.currList);
                    if (ActAppList.this.mFields.currList == AppInfoStorage.CurrList.APP_LIST) {
                        ActAppList.this.setTitle(String.format(Locale.ENGLISH, ActAppList.this.getString(R.string.title_appinfo_apps), Integer.valueOf(ActAppList.this.mFields.appInfoStorage.mAppCount), Integer.valueOf(ActAppList.this.mFields.appInfoStorage.mPermCount)));
                    } else {
                        ActAppList.this.setTitle(String.format(Locale.ENGLISH, ActAppList.this.getString(R.string.title_appinfo_perms), Integer.valueOf(ActAppList.this.mFields.appInfoStorage.mPermCount), Integer.valueOf(ActAppList.this.mFields.appInfoStorage.mAppCount)));
                    }
                    try {
                        ActAppList.this.dismissDialog(1);
                    } catch (Exception e3) {
                    }
                    try {
                        ActAppList.this.dismissDialog(2);
                    } catch (Exception e4) {
                    }
                }
            }
        }
    }

    private class LoadDialog extends ProgressDialog {
        public LoadDialog(Context context) {
            super(context);
        }

        public void onBackPressed() {
            super.onBackPressed();
            ActAppList.this.mFields.progressDlg = null;
            ActAppList.this.finish();
        }
    }
}
