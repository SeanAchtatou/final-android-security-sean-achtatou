package com.zoner.android.antivirus;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

public class DbScanner {
    public static final int ERRFORMAT = 3;
    public static final int ERRHASH = 2;
    public static final int ERROK = 0;
    public static final int ERRVER = 1;
    public static final int currentVersion = 1;
    public static int lastID = 1;
    private static HashMap<String, DbEntry> sDbSHA1 = new HashMap<>(1);

    static {
        sDbSHA1.put("e7hnvsL+aXKPeB65u5gCrVjqwEM=", new DbEntry("TESTFILE.Not.a.Virus", 4128));
    }

    private static void SwapDb(HashMap<String, DbEntry> db, int id) {
        lastID = id;
        sDbSHA1 = db;
    }

    private static final class DbEntry {
        public final long mFileSize;
        public final String mVirusName;

        DbEntry(String virusName, long fileSize) {
            this.mVirusName = virusName;
            this.mFileSize = fileSize;
        }
    }

    public static String getResult(String val, long fileSize) {
        if (sDbSHA1 == null) {
            return null;
        }
        String result = null;
        DbEntry entry = sDbSHA1.get(val);
        if (entry != null && entry.mFileSize == fileSize) {
            result = entry.mVirusName;
        }
        return result;
    }

    private static final class DbResult {
        public HashMap<String, DbEntry> dbSHA1 = null;
        public int errorCode;
        public int lastID = 1;

        DbResult(int code) {
            this.errorCode = code;
        }

        DbResult(HashMap<String, DbEntry> db, int id) {
            this.dbSHA1 = db;
            this.errorCode = 0;
            this.lastID = id;
        }
    }

    /* JADX INFO: Multiple debug info for r12v5 byte[]: [D('buffer' byte[]), D('in' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r0v4 java.util.HashMap: [D('version' short), D('dbSHA1' java.util.HashMap<java.lang.String, com.zoner.android.antivirus.DbScanner$DbEntry>)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
     arg types: [byte[], int, byte]
     candidates:
      ClspMth{java.lang.String.<init>(int[], int, int):void}
      ClspMth{java.lang.String.<init>(char[], int, int):void}
      ClspMth{java.lang.String.<init>(byte[], int, int):void} */
    private static DbResult CreateDb(InputStream in) {
        DbResult res;
        DataInputStream is = new DataInputStream(in);
        try {
            byte[] buffer = new byte[255];
            is.readFully(buffer, 0, 6);
            if (is.readShort() != 1) {
                try {
                    is.close();
                } catch (Exception e) {
                }
                return new DbResult(1);
            }
            int lastID2 = is.readInt();
            int strs = is.readInt();
            int sigs = is.readInt();
            String[] strTable = new String[strs];
            HashMap<String, DbEntry> dbSHA1 = new HashMap<>(sigs);
            for (int i = 0; i < strs; i++) {
                byte len = is.readByte();
                is.readFully(buffer, 0, len);
                strTable[i] = new String(buffer, 0, (int) len);
            }
            for (int i2 = 0; i2 < sigs; i2++) {
                is.readFully(buffer, 0, 28);
                dbSHA1.put(new String(buffer, 0, 28), new DbEntry(strTable[is.readShort()], (long) is.readInt()));
            }
            do {
            } while (is.read(buffer) > 0);
            res = new DbResult(dbSHA1, lastID2);
            try {
                is.close();
                return res;
            } catch (Exception e2) {
                return res;
            }
        } catch (Exception e3) {
            res = new DbResult(3);
        }
    }

    public static int LoadDb(InputStream in) {
        try {
            GZIPInputStream zis = new GZIPInputStream(new BufferedInputStream(in));
            DbResult res = CreateDb(zis);
            if (res.errorCode == 0) {
                SwapDb(res.dbSHA1, res.lastID);
            }
            try {
                zis.close();
            } catch (Exception e) {
            }
            return res.errorCode;
        } catch (Exception e2) {
            return 3;
        }
    }

    public static int LoadDb(InputStream in, String hash) {
        try {
            HashInputStream his = new HashInputStream(in, "SHA-1");
            try {
                GZIPInputStream zis = new GZIPInputStream(his);
                DbResult res = CreateDb(zis);
                if (res.errorCode == 0) {
                    if (new BigInteger(1, his.digest()).toString(16).equals(hash)) {
                        SwapDb(res.dbSHA1, res.lastID);
                    } else {
                        res.errorCode = 2;
                    }
                }
                try {
                    zis.close();
                } catch (Exception e) {
                }
                return res.errorCode;
            } catch (Exception e2) {
                try {
                    his.close();
                } catch (Exception e3) {
                }
                return 3;
            }
        } catch (Exception e4) {
            return 3;
        }
    }
}
