package com.zoner.android.antivirus;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Binder;
import android.os.Environment;
import android.os.FileObserver;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import com.zoner.android.antivirus.ResultStorage;
import java.io.FileNotFoundException;
import java.util.List;

public class ZapService extends Service implements IScanWorker, IResultWorker, SharedPreferences.OnSharedPreferenceChangeListener {
    private static final int ID_COUNT = 1;
    static final int ID_SCAN = 0;
    static final int SCAN_INOTIFY = 2;
    static final int SCAN_START = 1;
    static final int STATUS_SCAN_FINISHED = 2;
    static final int STATUS_SCAN_RESULT = 1;
    public ResultStorage mAccessResults = new ResultStorage();
    private final IBinder mBinder = new LocalBinder();
    public ResultStorage mDemandResults = new ResultStorage();
    private final Observer mDownloadObserver = new Observer(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/download");
    private final InstallReceiver mInstallReceiver = new InstallReceiver(this, null);
    public PendingIntent mLiveThreat = null;
    private final MountReceiver mMountReceiver = new MountReceiver(this, null);
    public ResultNotifier mNotifier;
    public ResultStorage mPackageResults = new ResultStorage();
    private PhoneFilter mPhoneFilter;
    public ScanQueue mQueue;
    private ScanHandler mScanHandler;
    public PendingIntent mUpdate = null;
    public Messenger[] mtReplyTo = new Messenger[1];

    public void onCreate() {
        this.mNotifier = new ResultNotifier(this);
        try {
            DbScanner.LoadDb(openFileInput("zavdb.and"));
        } catch (FileNotFoundException e) {
        }
        this.mQueue = new ScanQueue(this);
        this.mScanHandler = new ScanHandler(this);
        new Thread(this.mScanHandler).start();
        runUpdate(true);
        runLiveThreat(true);
        runFileObserver(true);
        runInstallReceiver(true);
        runMountReceiver(true);
        runPermanentIcon(true);
        if (getPackageManager().hasSystemFeature("android.hardware.telephony")) {
            try {
                this.mPhoneFilter = new PhoneFilter(this);
            } catch (Exception e2) {
                throw new RuntimeException(e2);
            }
        }
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
        startForeground(0, null);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null || !intent.hasExtra(PhoneFilter.PHONELOG)) {
            return 1;
        }
        this.mPhoneFilter.phoneLog(intent);
        return 1;
    }

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        /* access modifiers changed from: package-private */
        public ZapService getService() {
            return ZapService.this;
        }
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    public void sendReply(int id, Message msg) {
        Messenger replyTo = this.mtReplyTo[id];
        if (replyTo != null) {
            try {
                replyTo.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        if (key.equals(Globals.PREF_SCAN_PACKAGES)) {
            runInstallReceiver(false);
        } else if (key.equals(Globals.PREF_SCAN_ONACCESS)) {
            runFileObserver(false);
        } else if (key.equals(Globals.PREF_SCAN_MOUNT)) {
            runMountReceiver(false);
        } else if (key.equals(Globals.PREF_MISC_ICON)) {
            runPermanentIcon(false);
        } else if (key.equals(Globals.PREF_MISC_LIVETHREAT)) {
            runLiveThreat(false);
        } else if (key.equals(Globals.PREF_UPDATE_FREQ)) {
            runUpdate(false);
        }
    }

    private void runInstallReceiver(boolean starting) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Globals.PREF_SCAN_PACKAGES, true)) {
            IntentFilter filter = new IntentFilter();
            filter.addDataScheme("package");
            filter.addAction("android.intent.action.PACKAGE_ADDED");
            registerReceiver(this.mInstallReceiver, filter);
        } else if (!starting) {
            unregisterReceiver(this.mInstallReceiver);
        }
    }

    private void runMountReceiver(boolean starting) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Globals.PREF_SCAN_MOUNT, false)) {
            IntentFilter filter = new IntentFilter();
            filter.addDataScheme("file");
            filter.addAction("android.intent.action.MEDIA_MOUNTED");
            registerReceiver(this.mMountReceiver, filter);
        } else if (!starting) {
            unregisterReceiver(this.mMountReceiver);
        }
    }

    private void runFileObserver(boolean starting) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Globals.PREF_SCAN_ONACCESS, true)) {
            this.mDownloadObserver.startWatching();
        } else if (!starting) {
            this.mDownloadObserver.stopWatching();
        }
    }

    private void runPermanentIcon(boolean starting) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Globals.PREF_MISC_ICON, true)) {
            this.mNotifier.showIcon();
        } else if (!starting) {
            this.mNotifier.hideIcon();
        }
    }

    private void runUpdate(boolean starting) {
        int freq = Globals.toInt(PreferenceManager.getDefaultSharedPreferences(this).getString(Globals.PREF_UPDATE_FREQ, Globals.DEF_UPDATE_FREQ), 0);
        if (freq != 0) {
            if (this.mUpdate == null) {
                this.mUpdate = PendingIntent.getBroadcast(this, 0, new Intent(this, AlarmUpdate.class), 134217728);
            }
            ((AlarmManager) getSystemService("alarm")).setRepeating(3, SystemClock.elapsedRealtime(), 3600000 * ((long) freq), this.mUpdate);
        } else if (this.mUpdate != null) {
            ((AlarmManager) getSystemService("alarm")).cancel(this.mUpdate);
            this.mUpdate = null;
        }
    }

    private void runLiveThreat(boolean starting) {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Globals.PREF_MISC_LIVETHREAT, true)) {
            if (this.mLiveThreat == null) {
                this.mLiveThreat = PendingIntent.getBroadcast(this, 0, new Intent(this, AlarmLiveThreat.class), 134217728);
            }
            ((AlarmManager) getSystemService("alarm")).setInexactRepeating(3, SystemClock.elapsedRealtime() + 3600000, 3600000, this.mLiveThreat);
        } else if (this.mLiveThreat != null) {
            ((AlarmManager) getSystemService("alarm")).cancel(this.mLiveThreat);
            this.mLiveThreat = null;
        }
    }

    private class Observer extends FileObserver {
        private final String basePath;

        public Observer(String path) {
            super(path);
            this.basePath = path;
        }

        public void onEvent(int event, String path) {
            if ((1073741824 & event) == 0 && event == 8) {
                ZapService.this.mQueue.addAccess(String.valueOf(this.basePath) + "/" + path);
            }
        }
    }

    private class InstallReceiver extends BroadcastReceiver {
        private InstallReceiver() {
        }

        /* synthetic */ InstallReceiver(ZapService zapService, InstallReceiver installReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            ZapService.this.mQueue.addPackage(intent.getDataString().substring(8));
        }
    }

    private class MountReceiver extends BroadcastReceiver {
        private MountReceiver() {
        }

        /* synthetic */ MountReceiver(ZapService zapService, MountReceiver mountReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            ZapService.this.mQueue.addMount(intent.getDataString().substring(7));
        }
    }

    public void scanPath(String path, boolean isDir) {
        this.mQueue.addDemand(path, isDir);
    }

    public void scanPackages(List<PackageInfo> list) {
        this.mQueue.addDemand(list);
    }

    public ResultStorage.Storage demandResults() {
        return this.mDemandResults.get();
    }

    public ResultStorage.Storage accessResults() {
        return this.mAccessResults.get();
    }

    public ResultStorage.Storage installResults() {
        return this.mPackageResults.get();
    }

    public boolean scanFinished() {
        return !this.mQueue.mDemandScans;
    }

    public boolean scanPaused() {
        return this.mQueue.mDemandPaused;
    }

    public void scanStop() {
        this.mQueue.demandStop();
    }

    public void scanPause() {
        this.mQueue.demandPause();
    }

    public void scanResume() {
        this.mQueue.demandResume();
    }

    public void resolve(ScanResult result) {
        switch (result.queue) {
            case 1:
                this.mAccessResults.remove(result);
                return;
            case 2:
                this.mPackageResults.remove(result);
                return;
            default:
                this.mDemandResults.remove(result);
                return;
        }
    }

    public void allResolved() {
        this.mPackageResults.resolve();
        this.mAccessResults.resolve();
        this.mDemandResults.resolve();
        this.mNotifier.notifyResolved();
    }

    public void demandResolved() {
        this.mDemandResults.resolve();
        this.mNotifier.notifyResolved();
    }

    public boolean hasResults() {
        return this.mPackageResults.hasResults() || this.mAccessResults.hasResults() || this.mDemandResults.hasResults();
    }

    public int infectionCount() {
        return this.mPackageResults.cInfections() + this.mAccessResults.cInfections() + this.mDemandResults.cInfections();
    }

    public boolean hasDemandResults() {
        return this.mDemandResults.hasResults();
    }
}
