package com.zoner.android.antivirus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Starter extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            if (prefs != null && prefs.getBoolean(Globals.PREF_MISC_BOOT, true)) {
                context.startService(new Intent(context, ZapService.class));
            }
        } else if (intent.getAction().equals("android.intent.action.PACKAGE_REPLACED") && intent.getDataString().startsWith("package:com.zoner.android.antivirus")) {
            context.startService(new Intent(context, ZapService.class));
        }
    }
}
