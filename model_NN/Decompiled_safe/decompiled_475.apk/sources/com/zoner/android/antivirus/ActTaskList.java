package com.zoner.android.antivirus;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.zoner.android.antivirus.ProcInfo;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ActTaskList extends Activity {
    static final int DIALOG_SORT = 2;
    static final int DIALOG_TASK_DETAIL = 1;
    TaskListFields mFields;
    final Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public TextView mTotalMem;
    final Runnable mUpdateList = new Runnable() {
        public void run() {
            ActTaskList.this.mFields.procInfo.deleteInactive();
            ActTaskList.this.mFields.procInfo.addNew();
            ActTaskList.this.mFields.procInfo.sortList();
            ((SimpleAdapter) ActTaskList.this.getListView().getAdapter()).notifyDataSetChanged();
            ActTaskList.this.mTotalMem.setText(ActTaskList.this.mFields.procInfo.mMemUsed);
            ActTaskList.this.updateDialog();
            ActTaskList.this.mUpdating = false;
        }
    };
    boolean mUpdating = false;

    class TaskListFields {
        public ActivityManager am;
        public String appName;
        public TextView cpu;
        public Map<String, Object> currItem;
        public Dialog detailDlg;
        public Drawable icon;
        public TextView mem;
        public String pid;
        public ProcInfo procInfo;
        public String procName;
        public ProcInfo.SortBy sortBy = ProcInfo.SortBy.NAME;
        public TextView state;
        public TextView stime;
        public TextView threads;
        public int updInterval = 2000;
        public UpdateListTask updTask;
        public Timer updTimer;
        public TextView utime;
        public TextView vmem;

        TaskListFields() {
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        stopGlobalUpdate();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        startGlobalUpdate();
    }

    /* access modifiers changed from: package-private */
    public ListView getListView() {
        return (ListView) findViewById(R.id.tasklist_list);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        this.mFields = (TaskListFields) getLastNonConfigurationInstance();
        if (this.mFields == null) {
            this.mFields = new TaskListFields();
            this.mFields.am = (ActivityManager) getSystemService("activity");
            this.mFields.procInfo = new ProcInfo(this, this.mFields.am);
            this.mFields.updTimer = new Timer();
        }
        setContentView((int) R.layout.tasklist);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> l, View v, int position, long id) {
                Map<String, Object> map = (Map) l.getItemAtPosition(position);
                ActTaskList.this.mFields.procName = (String) map.get("procName");
                ActTaskList.this.mFields.appName = (String) map.get("appName");
                ActTaskList.this.mFields.icon = (Drawable) map.get("appIcon");
                ActTaskList.this.mFields.pid = ((Integer) map.get("pid")).toString();
                ActTaskList.this.showDialog(1);
            }
        });
        getListView().setAdapter((ListAdapter) new TaskAdapter(this, this.mFields.procInfo.mTaskList, R.layout.tasklist_row, new String[]{"appName", "mem"}, new int[]{R.id.tasklist_row_name, R.id.tasklist_row_mem}));
        getWindow().setFeatureInt(7, R.layout.tasklist_title);
        this.mTotalMem = (TextView) findViewById(R.id.tasklist_footer_mem);
        registerForContextMenu(getListView());
    }

    public Object onRetainNonConfigurationInstance() {
        return this.mFields;
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                View layout = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.tasklist_detail, (ViewGroup) findViewById(R.id.tasklist_detail_layout));
                ((TextView) layout.findViewById(R.id.tasklist_detail_pkgname)).setText(this.mFields.procName);
                ((TextView) layout.findViewById(R.id.tasklist_detail_pid)).setText(this.mFields.pid);
                this.mFields.state = (TextView) layout.findViewById(R.id.tasklist_detail_state);
                this.mFields.utime = (TextView) layout.findViewById(R.id.tasklist_detail_utime);
                this.mFields.stime = (TextView) layout.findViewById(R.id.tasklist_detail_stime);
                this.mFields.cpu = (TextView) layout.findViewById(R.id.tasklist_detail_cpu);
                this.mFields.threads = (TextView) layout.findViewById(R.id.tasklist_detail_threads);
                this.mFields.mem = (TextView) layout.findViewById(R.id.tasklist_detail_mem);
                this.mFields.vmem = (TextView) layout.findViewById(R.id.tasklist_detail_virtual);
                this.mFields.currItem = this.mFields.procInfo.getItem(this.mFields.procName);
                this.mFields.procInfo.updateAdvanced(this.mFields.pid, this.mFields.currItem);
                updateDialog();
                this.mFields.detailDlg = new Dialog(this);
                this.mFields.detailDlg.requestWindowFeature(3);
                this.mFields.detailDlg.setContentView(layout);
                this.mFields.detailDlg.setFeatureDrawable(3, resizeIcon(this.mFields.icon));
                this.mFields.detailDlg.setTitle(this.mFields.appName);
                ((Button) layout.findViewById(R.id.tasklist_detail_switchto)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ActTaskList.this.switchTo(ActTaskList.this.mFields.procName);
                        ActTaskList.this.dismissDialog(1);
                    }
                });
                ((Button) layout.findViewById(R.id.tasklist_detail_kill)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ActTaskList.this.kill(ActTaskList.this.mFields.procName);
                        ActTaskList.this.dismissDialog(1);
                    }
                });
                this.mFields.detailDlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        ActTaskList.this.removeDialog(1);
                        if (dialog == ActTaskList.this.mFields.detailDlg) {
                            ActTaskList.this.mFields.currItem = null;
                        }
                    }
                });
                return this.mFields.detailDlg;
            case 2:
                return new AlertDialog.Builder(this).setTitle(getString(R.string.appinfo_opts_sort_title)).setIcon(17301661).setPositiveButton(getString(R.string.appinfo_opts_sort_asc), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ActTaskList.this.mFields.procInfo.sortList(ActTaskList.this.mFields.sortBy, true);
                        ((SimpleAdapter) ActTaskList.this.getListView().getAdapter()).notifyDataSetChanged();
                    }
                }).setNegativeButton(getString(R.string.appinfo_opts_sort_desc), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ActTaskList.this.mFields.procInfo.sortList(ActTaskList.this.mFields.sortBy, false);
                        ((SimpleAdapter) ActTaskList.this.getListView().getAdapter()).notifyDataSetChanged();
                    }
                }).setSingleChoiceItems(new CharSequence[]{getString(R.string.tasklist_opts_name), getString(R.string.tasklist_opts_mem)}, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch (item) {
                            case 1:
                                ActTaskList.this.mFields.sortBy = ProcInfo.SortBy.MEM;
                                return;
                            default:
                                ActTaskList.this.mFields.sortBy = ProcInfo.SortBy.NAME;
                                return;
                        }
                    }
                }).create();
            default:
                return null;
        }
    }

    private Drawable resizeIcon(Drawable icon) {
        int size = (int) (3.5d * ((double) getResources().getDisplayMetrics().density) * ((double) new TextView(this).getTextSize()));
        return new BitmapDrawable(Bitmap.createScaledBitmap(((BitmapDrawable) icon).getBitmap(), size, size, true));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tasklist, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tasklist_sort /*2131361983*/:
                showDialog(2);
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void switchTo(String name) {
        Intent launchIntent = getPackageManager().getLaunchIntentForPackage(name);
        if (launchIntent != null) {
            try {
                launchIntent.addCategory("android.intent.category.LAUNCHER");
                launchIntent.setFlags(274726912);
                startActivity(launchIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(this, getString(R.string.appinfo_app_start_failed), 0).show();
            }
        } else {
            Toast.makeText(this, getString(R.string.appinfo_app_start_failed), 0).show();
        }
    }

    /* access modifiers changed from: package-private */
    public void kill(String name) {
        try {
            this.mFields.am.restartPackage(name);
        } catch (SecurityException e) {
            Toast.makeText(this, getString(R.string.tasklist_kill_failed), 0).show();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    /* access modifiers changed from: private */
    public void updateDialog() {
        if (this.mFields.currItem != null) {
            this.mFields.state.setText((String) this.mFields.currItem.get("state"));
            this.mFields.utime.setText((String) this.mFields.currItem.get("utime"));
            this.mFields.stime.setText((String) this.mFields.currItem.get("stime"));
            this.mFields.cpu.setText((String) this.mFields.currItem.get("cpu"));
            this.mFields.threads.setText((String) this.mFields.currItem.get("threads"));
            this.mFields.mem.setText((String) this.mFields.currItem.get("mem"));
            this.mFields.vmem.setText((String) this.mFields.currItem.get("vmem"));
        }
    }

    /* access modifiers changed from: package-private */
    public void startGlobalUpdate() {
        this.mFields.updTask = new UpdateListTask();
        this.mFields.updTimer.scheduleAtFixedRate(this.mFields.updTask, 500, (long) this.mFields.updInterval);
    }

    /* access modifiers changed from: package-private */
    public void stopGlobalUpdate() {
        this.mFields.updTask.cancel();
        this.mFields.updTimer.purge();
    }

    class UpdateListTask extends TimerTask {
        UpdateListTask() {
        }

        public void run() {
            if (!ActTaskList.this.mUpdating) {
                ActTaskList.this.mFields.procInfo.updateTaskList(ActTaskList.this.mFields.currItem);
                ActTaskList.this.mUpdating = true;
                ActTaskList.this.mHandler.post(ActTaskList.this.mUpdateList);
            }
        }
    }

    class TaskAdapter extends SimpleAdapter {
        TaskAdapter(Context context, List<Map<String, Object>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View row = super.getView(position, convertView, parent);
            ((ImageView) row.findViewById(R.id.tasklist_row_icon)).setImageDrawable((Drawable) ((Map) getItem(position)).get("appIcon"));
            return row;
        }
    }
}
