package com.zoner.android.antivirus;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.RelativeLayout;

public class CheckableRelativeLayout extends RelativeLayout implements Checkable {
    private CheckBox mCheckbox;

    public CheckableRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View v = getChildAt(i);
            if (v instanceof CheckBox) {
                this.mCheckbox = (CheckBox) v;
            }
        }
    }

    public boolean isChecked() {
        if (this.mCheckbox != null) {
            return this.mCheckbox.isChecked();
        }
        return false;
    }

    public void setChecked(boolean checked) {
        if (this.mCheckbox != null) {
            this.mCheckbox.setChecked(checked);
        }
    }

    public void toggle() {
        if (this.mCheckbox != null) {
            this.mCheckbox.toggle();
        }
    }
}
