package com.zoner.android.antivirus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.zoner.android.antivirus.DbPhoneFilter;

public class ActCallQuery extends Activity {
    /* access modifiers changed from: private */
    public boolean mFilter;
    /* access modifiers changed from: private */
    public String mPasswd;
    /* access modifiers changed from: private */
    public CheckBox mRemember = null;
    /* access modifiers changed from: private */
    public DbPhoneFilter.FilterResult mResult;
    /* access modifiers changed from: private */
    public boolean mUserAction = false;

    public void onCreate(Bundle savedInstanceState) {
        boolean showRemember;
        boolean lock;
        super.onCreate(savedInstanceState);
        requestWindowFeature(3);
        setContentView((int) R.layout.callquery);
        setFeatureDrawableResource(3, R.drawable.dialog_block);
        getWindow().setLayout(-1, -2);
        Intent intent = getIntent();
        this.mResult = (DbPhoneFilter.FilterResult) intent.getParcelableExtra(PhoneFilter.RESULT);
        this.mFilter = intent.getBooleanExtra("filtered", false);
        if (!this.mFilter || !(this.mResult.getModeOrig() == DbPhoneFilter.FilterList.Mode.Ask || this.mResult.getModeOrig() == DbPhoneFilter.FilterList.Mode.None)) {
            showRemember = false;
        } else {
            showRemember = true;
        }
        this.mPasswd = intent.getStringExtra("passwd");
        if (this.mPasswd != null) {
            lock = true;
        } else {
            lock = false;
        }
        if (!lock) {
            findViewById(R.id.callquery_parental_layout).setVisibility(8);
        } else if (this.mPasswd.length() == 0) {
            parentalLock(false);
        } else {
            parentalLock(true);
            ((EditText) findViewById(R.id.callquery_parental_password)).addTextChangedListener(new TextWatcher() {
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void afterTextChanged(Editable s) {
                    if (ActCallQuery.this.mPasswd.equals(s.toString())) {
                        ActCallQuery.this.parentalLock(false);
                    }
                }
            });
        }
        if (!showRemember) {
            findViewById(R.id.callquery_block_layout).setVisibility(8);
        }
        this.mRemember = (CheckBox) findViewById(R.id.callquery_remember);
        this.mRemember.setChecked(false);
        ((TextView) findViewById(R.id.callquery_phone_number)).setText(PhoneNumberUtils.formatNumber(this.mResult.phoneNumber));
        String name = Globals.getContactName(this, this.mResult.phoneNumber);
        if (name.length() == 0) {
            name = getString(R.string.callfilter_unknown_contact);
        }
        ((TextView) findViewById(R.id.callquery_contact)).setText("(" + name + ")");
        findViewById(R.id.callquery_btn_yes).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActCallQuery.this.mUserAction = true;
                if (ActCallQuery.this.mFilter) {
                    ActCallQuery.this.mResult.modeRes = DbPhoneFilter.FilterList.Mode.Allow;
                    if (ActCallQuery.this.mRemember.isChecked()) {
                        DbPhoneFilter db = new DbPhoneFilter(ActCallQuery.this);
                        ActCallQuery.this.mResult.phoneId = db.addCallOutFilter(ActCallQuery.this.mResult.phoneId, ActCallQuery.this.mResult.phoneNumber, ActCallQuery.this.mResult.modeRes);
                    }
                    ActCallQuery.this.phoneLog();
                }
                PhoneFilter.sBlock = false;
                try {
                    PhoneFilter.getITelephony(ActCallQuery.this).call(ActCallQuery.this.mResult.phoneNumber);
                } catch (Exception e) {
                    Log.d("ZonerAV", "Exception: getITelephony() e = " + e.getMessage());
                }
                ActCallQuery.this.finish();
            }
        });
        findViewById(R.id.callquery_btn_no).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActCallQuery.this.mUserAction = true;
                if (ActCallQuery.this.mFilter) {
                    ActCallQuery.this.mResult.modeRes = DbPhoneFilter.FilterList.Mode.Deny;
                    if (ActCallQuery.this.mRemember.isChecked()) {
                        DbPhoneFilter db = new DbPhoneFilter(ActCallQuery.this);
                        ActCallQuery.this.mResult.phoneId = db.addCallOutFilter(ActCallQuery.this.mResult.phoneId, ActCallQuery.this.mResult.phoneNumber, ActCallQuery.this.mResult.modeRes);
                    }
                    ActCallQuery.this.phoneLog();
                }
                ActCallQuery.this.finish();
            }
        });
    }

    /* access modifiers changed from: private */
    public void phoneLog() {
        if (this.mFilter) {
            Intent intent = new Intent(this, ZapService.class);
            PhoneFilter.phoneLogIntent(intent, this.mResult);
            startService(intent);
        }
    }

    public void onPause() {
        super.onPause();
        if (!this.mUserAction) {
            this.mResult.modeRes = DbPhoneFilter.FilterList.Mode.Deny;
            phoneLog();
            Toast.makeText(getApplicationContext(), getString(R.string.callquery_call_blocked), 0).show();
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void parentalLock(boolean lock) {
        boolean z;
        boolean z2;
        boolean z3;
        EditText password = (EditText) findViewById(R.id.callquery_parental_password);
        password.setEnabled(lock);
        password.setFocusable(lock);
        password.setFocusableInTouchMode(lock);
        if (!lock) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(password.getWindowToken(), 2);
            password.clearFocus();
        }
        ((ImageView) findViewById(R.id.callquery_parental_status)).setImageResource(lock ? R.drawable.no : R.drawable.yes);
        View findViewById = findViewById(R.id.callquery_remember);
        if (lock) {
            z = false;
        } else {
            z = true;
        }
        findViewById.setEnabled(z);
        View findViewById2 = findViewById(R.id.callquery_allow);
        if (lock) {
            z2 = false;
        } else {
            z2 = true;
        }
        findViewById2.setEnabled(z2);
        View findViewById3 = findViewById(R.id.callquery_btn_yes);
        if (lock) {
            z3 = false;
        } else {
            z3 = true;
        }
        findViewById3.setEnabled(z3);
    }
}
