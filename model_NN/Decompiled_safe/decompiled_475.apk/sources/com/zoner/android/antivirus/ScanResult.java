package com.zoner.android.antivirus;

import java.util.ArrayList;

public final class ScanResult {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$ScanResult$Result = null;
    private static final int MAX_POOL_SIZE = 10;
    public static final String sClean = "CLEAN";
    public static final String sInfected = "INFECTED";
    private static ScanResult sPool = null;
    private static int sPoolSize = 0;
    private static final Object sPoolSync = new Object();
    public static final String sUnknown = "UNKNOWN";
    private ScanResult next;
    String path;
    int queue;
    Result result;
    int type;
    private String virusName;
    private ArrayList<String> virusNames;

    enum Result {
        CLEAN,
        INFECTED
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$ScanResult$Result() {
        int[] iArr = $SWITCH_TABLE$com$zoner$android$antivirus$ScanResult$Result;
        if (iArr == null) {
            iArr = new int[Result.values().length];
            try {
                iArr[Result.CLEAN.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Result.INFECTED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$zoner$android$antivirus$ScanResult$Result = iArr;
        }
        return iArr;
    }

    ScanResult() {
        this.result = Result.CLEAN;
    }

    ScanResult(ScanResult old) {
        this.result = old.result;
        this.path = old.path;
        this.type = old.type;
        this.virusName = old.virusName;
    }

    public static ScanResult obtain() {
        synchronized (sPoolSync) {
            if (sPool == null) {
                return new ScanResult();
            }
            ScanResult res = sPool;
            sPool = res.next;
            res.next = null;
            sPoolSize--;
            return res;
        }
    }

    public static ScanResult obtain(String path2, int type2) {
        ScanResult res = obtain();
        res.path = path2;
        res.type = type2;
        return res;
    }

    public void recycle() {
        synchronized (sPoolSync) {
            if (sPoolSize < MAX_POOL_SIZE) {
                clearForRecycle();
                this.next = sPool;
                sPool = this;
                sPoolSize++;
            }
        }
    }

    private void clearForRecycle() {
        this.result = Result.CLEAN;
        this.path = null;
        this.virusName = null;
        this.virusNames = null;
    }

    public void setVirusName(String name) {
        if (this.virusNames != null) {
            this.virusNames.add(name);
        } else if (this.virusName != null) {
            this.virusNames = new ArrayList<>(1);
            this.virusNames.add(name);
        } else {
            this.virusName = name;
        }
        this.result = Result.INFECTED;
    }

    public int getVirusCount() {
        if (this.virusName == null) {
            return 0;
        }
        int names = 1;
        if (this.virusNames != null) {
            names = 1 + this.virusNames.size();
        }
        return names;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public String getVirusName(int i) {
        if (i < 0 || this.virusName == null || (i > 0 && this.virusNames == null)) {
            throw new IndexOutOfBoundsException();
        } else if (i == 0) {
            return this.virusName;
        } else {
            return this.virusNames.get(i - 1);
        }
    }

    public int getResultString() {
        return toResId(this.result);
    }

    public static int toResId(Result result2) {
        switch ($SWITCH_TABLE$com$zoner$android$antivirus$ScanResult$Result()[result2.ordinal()]) {
            case 1:
                return R.string.result_clean;
            case 2:
                return R.string.result_infected;
            default:
                return R.string.result_unknown;
        }
    }
}
