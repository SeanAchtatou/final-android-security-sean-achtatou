package com.zoner.android.antivirus;

import android.graphics.Color;
import com.google.android.mms.pdu.PduPart;
import java.util.HashMap;

public class PermissionLevels {
    public static final int[] colors = {-7829368, Color.rgb(0, (int) PduPart.P_CONTENT_TRANSFER_ENCODING, 0), Color.rgb((int) PduPart.P_CONTENT_TRANSFER_ENCODING, (int) PduPart.P_CONTENT_TRANSFER_ENCODING, 0), Color.rgb((int) PduPart.P_CONTENT_TRANSFER_ENCODING, 120, 0), -65536};
    public static final int safeColor = -16711936;
    private HashMap<String, Integer> mLevels = new HashMap<>();

    public int getLevel(String permission) {
        Integer level = this.mLevels.get(permission);
        if (level == null) {
            return 0;
        }
        return level.intValue();
    }

    public int getIcon(int level) {
        switch (level) {
            case 1:
                return R.drawable.level1;
            case 2:
                return R.drawable.level2;
            case DbScanner.ERRFORMAT:
                return R.drawable.level3;
            case 4:
                return R.drawable.level4;
            default:
                return R.drawable.level0;
        }
    }

    PermissionLevels() {
        this.mLevels.put("android.permission.ACCESS_CACHE_FILESYSTEM", 2);
        this.mLevels.put("android.permission.ACCESS_COARSE_LOCATION", 2);
        this.mLevels.put("android.permission.ACCESS_FINE_LOCATION", 2);
        this.mLevels.put("android.permission.ACCESS_LOCATION_EXTRA_COMMANDS", 3);
        this.mLevels.put("android.permission.ACCESS_MOCK_LOCATION", 1);
        this.mLevels.put("android.permission.ACCESS_NETWORK_STATE", 1);
        this.mLevels.put("android.permission.ACCESS_WIFI_STATE", 1);
        this.mLevels.put("android.permission.ACCOUNT_MANAGER", 3);
        this.mLevels.put("android.permission.ASEC_ACCESS", 2);
        this.mLevels.put("android.permission.ASEC_CREATE", 2);
        this.mLevels.put("android.permission.ASEC_DESTROY", 3);
        this.mLevels.put("android.permission.ASEC_MOUNT_UNMOUNT", 3);
        this.mLevels.put("android.permission.ASEC_RENAME", 3);
        this.mLevels.put("android.permission.AUTHENTICATE_ACCOUNTS", 4);
        this.mLevels.put("android.permission.BACKUP", 4);
        this.mLevels.put("android.permission.BATTERY_STATS", 1);
        this.mLevels.put("android.permission.BIND_APPWIDGET", 3);
        this.mLevels.put("android.permission.BIND_DEVICE_ADMIN", 3);
        this.mLevels.put("android.permission.BIND_INPUT_METHOD", 3);
        this.mLevels.put("android.permission.BIND_REMOTEVIEWS", 3);
        this.mLevels.put("android.permission.BIND_WALLPAPER", 3);
        this.mLevels.put("android.permission.BLUETOOTH", 2);
        this.mLevels.put("android.permission.BLUETOOTH_ADMIN", 3);
        this.mLevels.put("android.permission.BRICK", 4);
        this.mLevels.put("android.permission.BROADCAST_PACKAGE_REMOVED", 3);
        this.mLevels.put("android.permission.BROADCAST_SMS", 3);
        this.mLevels.put("android.permission.BROADCAST_STICKY", 1);
        this.mLevels.put("android.permission.BROADCAST_WAP_PUSH", 3);
        this.mLevels.put("android.permission.CALL_PHONE", 4);
        this.mLevels.put("android.permission.CALL_PRIVILEGED", 4);
        this.mLevels.put("android.permission.CAMERA", 2);
        this.mLevels.put("android.permission.CHANGE_BACKGROUND_DATA_SETTING", 3);
        this.mLevels.put("android.permission.CHANGE_COMPONENT_ENABLED_STATE", 3);
        this.mLevels.put("android.permission.CHANGE_CONFIGURATION", 2);
        this.mLevels.put("android.permission.CHANGE_NETWORK_STATE", 4);
        this.mLevels.put("android.permission.CHANGE_WIFI_MULTICAST_STATE", 3);
        this.mLevels.put("android.permission.CHANGE_WIFI_STATE", 3);
        this.mLevels.put("android.permission.CLEAR_APP_CACHE", 3);
        this.mLevels.put("android.permission.CLEAR_APP_USER_DATA", 3);
        this.mLevels.put("android.permission.CONTROL_LOCATION_UPDATES", 3);
        this.mLevels.put("android.permission.COPY_PROTECTED_DATA", 4);
        this.mLevels.put("android.permission.DELETE_CACHE_FILES", 3);
        this.mLevels.put("android.permission.DELETE_PACKAGES", 4);
        this.mLevels.put("android.permission.DEVICE_POWER", 4);
        this.mLevels.put("android.permission.DIAGNOSTIC", 4);
        this.mLevels.put("android.permission.DISABLE_KEYGUARD", 3);
        this.mLevels.put("android.permission.DUMP", 3);
        this.mLevels.put("android.permission.EXPAND_STATUS_BAR", 1);
        this.mLevels.put("android.permission.FACTORY_TEST", 3);
        this.mLevels.put("android.permission.FLASHLIGHT", 1);
        this.mLevels.put("android.permission.FORCE_BACK", 3);
        this.mLevels.put("android.permission.FORCE_STOP_PACKAGES", 4);
        this.mLevels.put("android.permission.GET_ACCOUNTS", 3);
        this.mLevels.put("android.permission.GET_PACKAGE_SIZE", 1);
        this.mLevels.put("android.permission.GET_TASKS", 2);
        this.mLevels.put("android.permission.GLOBAL_SEARCH", 1);
        this.mLevels.put("android.permission.GLOBAL_SEARCH_CONTROL", 4);
        this.mLevels.put("android.permission.HARDWARE_TEST", 4);
        this.mLevels.put("android.permission.INJECT_EVENTS", 4);
        this.mLevels.put("android.permission.INSTALL_LOCATION_PROVIDER", 2);
        this.mLevels.put("android.permission.INSTALL_PACKAGES", 4);
        this.mLevels.put("android.permission.INTERNAL_SYSTEM_WINDOW", 3);
        this.mLevels.put("android.permission.INTERNET", 2);
        this.mLevels.put("android.permission.KILL_BACKGROUND_PROCESSES", 3);
        this.mLevels.put("android.permission.MANAGE_ACCOUNTS", 3);
        this.mLevels.put("android.permission.MANAGE_APP_TOKENS", 3);
        this.mLevels.put("android.permission.MASTER_CLEAR", 4);
        this.mLevels.put("android.permission.MODIFY_AUDIO_SETTINGS", 2);
        this.mLevels.put("android.permission.MODIFY_PHONE_STATE", 3);
        this.mLevels.put("android.permission.MOUNT_FORMAT_FILESYSTEMS", 4);
        this.mLevels.put("android.permission.MOUNT_UNMOUNT_FILESYSTEMS", 3);
        this.mLevels.put("android.permission.MOVE_PACKAGE", 4);
        this.mLevels.put("android.permission.NFC", 2);
        this.mLevels.put("android.permission.PACKAGE_USAGE_STATS", 3);
        this.mLevels.put("android.permission.PERFORM_CDMA_PROVISIONING", 4);
        this.mLevels.put("android.permission.PERSISTENT_ACTIVITY", 3);
        this.mLevels.put("android.permission.PROCESS_OUTGOING_CALLS", 4);
        this.mLevels.put("android.permission.READ_CALENDAR", 2);
        this.mLevels.put("android.permission.READ_CONTACTS", 2);
        this.mLevels.put("android.permission.READ_FRAME_BUFFER", 3);
        this.mLevels.put("com.android.browser.permission.READ_HISTORY_BOOKMARKS", 2);
        this.mLevels.put("android.permission.READ_INPUT_STATE", 4);
        this.mLevels.put("android.permission.READ_LOGS", 3);
        this.mLevels.put("android.permission.READ_PHONE_STATE", 2);
        this.mLevels.put("android.permission.READ_SMS", 3);
        this.mLevels.put("android.permission.READ_SYNC_SETTINGS", 1);
        this.mLevels.put("android.permission.READ_SYNC_STATS", 2);
        this.mLevels.put("android.permission.READ_USER_DICTIONARY", 1);
        this.mLevels.put("android.permission.REBOOT", 3);
        this.mLevels.put("android.permission.RECEIVE_BOOT_COMPLETED", 2);
        this.mLevels.put("android.permission.RECEIVE_MMS", 3);
        this.mLevels.put("android.permission.RECEIVE_SMS", 3);
        this.mLevels.put("android.permission.RECEIVE_WAP_PUSH", 4);
        this.mLevels.put("android.permission.RECORD_AUDIO", 2);
        this.mLevels.put("android.permission.REORDER_TASKS", 3);
        this.mLevels.put("android.permission.RESTART_PACKAGES", 3);
        this.mLevels.put("android.permission.SEND_SMS", 4);
        this.mLevels.put("android.permission.SET_ACTIVITY_WATCHER", 4);
        this.mLevels.put("android.permission.SET_ALARM", 1);
        this.mLevels.put("android.permission.SET_ALWAYS_FINISH", 3);
        this.mLevels.put("android.permission.SET_ANIMATION_SCALE", 2);
        this.mLevels.put("android.permission.SET_DEBUG_APP", 4);
        this.mLevels.put("android.permission.SET_ORIENTATION", 2);
        this.mLevels.put("android.permission.SET_PREFERRED_APPLICATIONS", 4);
        this.mLevels.put("android.permission.SET_PROCESS_LIMIT", 3);
        this.mLevels.put("android.permission.SET_TIME", 2);
        this.mLevels.put("android.permission.SET_TIME_ZONE", 2);
        this.mLevels.put("android.permission.SET_WALLPAPER", 1);
        this.mLevels.put("android.permission.SET_WALLPAPER_COMPONENT", 3);
        this.mLevels.put("android.permission.SET_WALLPAPER_HINTS", 1);
        this.mLevels.put("android.permission.SHUTDOWN", 3);
        this.mLevels.put("android.permission.STATUS_BAR", 4);
        this.mLevels.put("android.permission.STATUS_BAR_SERVICE", 4);
        this.mLevels.put("android.permission.STOP_APP_SWITCHES", 3);
        this.mLevels.put("android.permission.SUBSCRIBED_FEEDS_READ", 1);
        this.mLevels.put("android.permission.SUBSCRIBED_FEEDS_WRITE", 3);
        this.mLevels.put("android.permission.SYSTEM_ALERT_WINDOW", 2);
        this.mLevels.put("android.permission.UPDATE_DEVICE_STATS", 3);
        this.mLevels.put("android.permission.USE_CREDENTIALS", 1);
        this.mLevels.put("android.permission.USE_SIP", 4);
        this.mLevels.put("android.permission.VIBRATE", 1);
        this.mLevels.put("android.permission.WAKE_LOCK", 1);
        this.mLevels.put("android.permission.WRITE_APN_SETTINGS", 4);
        this.mLevels.put("android.permission.WRITE_CALENDAR", 3);
        this.mLevels.put("android.permission.WRITE_CONTACTS", 3);
        this.mLevels.put("android.permission.WRITE_EXTERNAL_STORAGE", 2);
        this.mLevels.put("android.permission.WRITE_GSERVICES", 3);
        this.mLevels.put("com.android.browser.permission.WRITE_HISTORY_BOOKMARKS", 3);
        this.mLevels.put("android.permission.WRITE_SECURE_SETTINGS", 4);
        this.mLevels.put("android.permission.WRITE_SETTINGS", 3);
        this.mLevels.put("android.permission.WRITE_SMS", 3);
        this.mLevels.put("android.permission.WRITE_SYNC_SETTINGS", 3);
        this.mLevels.put("android.permission.WRITE_USER_DICTIONARY", 2);
        this.mLevels.put("com.android.vending.CHECK_LICENSE", 1);
        this.mLevels.put("com.android.vending.BILLING", 2);
        this.mLevels.put("android.permission.READ_EXTERNAL_STORAGE", 2);
        this.mLevels.put("android.permission.ACCESS_COARSE_UPDATES", 2);
        this.mLevels.put("android.permission.ACCESS_WIMAX_STATE", 1);
        this.mLevels.put("android.permission.CHANGE_WIMAX_STATE", 3);
        this.mLevels.put("android.permission.READ_OWNER_DATA", 2);
        this.mLevels.put("android.permission.WRITE_OWNER_DATA", 3);
        this.mLevels.put("android.permission.READ_SETTINGS", 2);
        this.mLevels.put("com.android.launcher.permission.INSTALL_SHORTCUT", 2);
        this.mLevels.put("com.android.launcher.permission.UNINSTALL_SHORTCUT", 3);
    }
}
