package com.zoner.android.antivirus;

import android.util.Log;
import com.zoner.android.antivirus.ScanResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Scanner {
    private static final int ENDHDR = 22;
    private static final long END_MAXLEN = 65557;
    private static final int ZIPBLOCKSZ = 512;
    private byte[] EICAR;
    private final byte[] EICAR_INIT = {88, 53, 79, 33, 80, 37, 64, 65, 80, 91, 52, 92, 80, 90, 88, 53, 52, 40, 80, 94, 41, 55, 67, 67, 41, 55, 125, 36, 69, 73, 67, 65, 82, 45, 83, 84, 65, 78, 68, 65, 82, 68, 45, 65, 78, 84, 73, 86, 73, 82, 85, 83, 45, 84, 69, 83, 84, 45, 70, 73, 76, 69, 33, 36, 72, 43, 72};
    private final int EICAR_MAX_SIZE = 128;
    private final int EICAR_MIN_SIZE = 68;
    private final String EICAR_NAME = "EICAR.Not.a.Virus";

    public Scanner() {
        int len = this.EICAR_INIT.length;
        this.EICAR = new byte[(len + 1)];
        System.arraycopy(this.EICAR_INIT, 0, this.EICAR, 0, len);
        this.EICAR[len] = 42;
    }

    private void isEicar(InputStream is, ScanResult result) throws IOException {
        byte[] buffer = new byte[68];
        int len = 0;
        while (true) {
            int read = is.read(buffer, len, 68 - len);
            if (read <= 0) {
                break;
            }
            len += read;
        }
        if (Arrays.equals(buffer, this.EICAR)) {
            result.setVirusName("EICAR.Not.a.Virus");
        }
        is.close();
    }

    private void scanApkEntry(String key, String val, long fileSize, ScanResult result) {
        String virusName;
        String key2 = key.toUpperCase(Locale.ENGLISH);
        if (key2.endsWith("-DIGEST") && key2.substring(0, key2.length() - 7).equals("SHA1") && (virusName = DbScanner.getResult(val, fileSize)) != null) {
            result.setVirusName(virusName);
        }
    }

    private boolean isZipFile(String path) throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile(path, "r");
        byte[] buf = new byte[ZIPBLOCKSZ];
        randomAccessFile.read(buf, 0, 4);
        if (buf[0] == 80 && buf[1] == 75 && buf[2] == 3 && buf[3] == 4) {
            long len = randomAccessFile.length();
            long minPos = (len - END_MAXLEN > 0 ? len - END_MAXLEN : 0) - 490;
            for (long pos = len - 512; pos >= minPos; pos -= 490) {
                int off = 0;
                if (pos < 0) {
                    off = (int) (-pos);
                }
                randomAccessFile.seek(((long) off) + pos);
                randomAccessFile.read(buf, 0, ZIPBLOCKSZ - off);
                for (int i = 490 - off; i >= 0; i--) {
                    if (buf[i + 0] == 80 && buf[i + 1] == 75 && buf[i + 2] == 5 && buf[i + 3] == 6) {
                        randomAccessFile.close();
                        return true;
                    }
                }
            }
            randomAccessFile.close();
            return false;
        }
        randomAccessFile.close();
        return false;
    }

    private void scanEicar(String path, ScanResult result) throws FileNotFoundException, IOException {
        File file = new File(path);
        long size = file.length();
        if (size >= 68 && size <= 128) {
            isEicar(new FileInputStream(file), result);
        }
    }

    private void scanEicar(ZipFile zipFile, ScanResult result) {
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) entries.nextElement();
            long size = entry.getSize();
            if (size >= 68 && size <= 128) {
                try {
                    isEicar(zipFile.getInputStream(entry), result);
                } catch (IOException e) {
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.jar.JarFile.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.util.jar.JarFile.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.util.jar.JarFile.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    public void scanFile(String path, ScanResult result) {
        try {
            if (!isZipFile(path)) {
                scanEicar(path, result);
                return;
            }
            JarFile jarFile = new JarFile(path, false);
            Manifest man = jarFile.getManifest();
            scanEicar(jarFile, result);
            if (man != null) {
                for (Map.Entry<String, Attributes> file : man.getEntries().entrySet()) {
                    ZipEntry zipFile = jarFile.getEntry((String) file.getKey());
                    if (zipFile != null) {
                        long fileSize = zipFile.getSize();
                        for (Map.Entry<Object, Object> se : ((Attributes) file.getValue()).entrySet()) {
                            scanApkEntry(se.getKey().toString(), se.getValue().toString(), fileSize, result);
                        }
                    }
                }
            }
        } catch (IOException e) {
        }
    }

    public ScanResult scan(String path, int type) {
        ScanResult result = ScanResult.obtain(path, type);
        scanFile(path, result);
        if (result.result != ScanResult.Result.CLEAN) {
            int count = result.getVirusCount();
            for (int i = 0; i < count; i++) {
                Log.e("zap", result.getVirusName(i));
            }
        }
        return result;
    }
}
