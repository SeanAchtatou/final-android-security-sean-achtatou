package com.zoner.android.antivirus;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.HeaderViewListAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.zoner.android.antivirus.DbPhoneFilter;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActCallsFilter extends Activity {
    private static final int DLG_SORT_FILTERS = 1;
    public static final int FILTER_TYPE_IN = 1;
    public static final int FILTER_TYPE_OUT = 0;
    public static final int FILTER_TYPE_SMS = 2;
    /* access modifiers changed from: private */
    public static final Comparator<Map<String, Object>> sNameComparator = new Comparator<Map<String, Object>>() {
        private final Collator collator = Collator.getInstance();

        public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            return compare((Map<String, Object>) ((Map) obj), (Map<String, Object>) ((Map) obj2));
        }

        public int compare(Map<String, Object> map1, Map<String, Object> map2) {
            String name1 = (String) map1.get("name");
            String name2 = (String) map2.get("name");
            if (name1.equals(name2)) {
                return this.collator.compare(map1.get("number"), map2.get("number"));
            }
            if (name1.length() == 0) {
                return 1;
            }
            if (name2.length() == 0) {
                return -1;
            }
            return this.collator.compare(map1.get("name"), map2.get("name"));
        }
    };
    /* access modifiers changed from: private */
    public static final Comparator<Map<String, Object>> sNumberComparator = new Comparator<Map<String, Object>>() {
        private final Collator collator = Collator.getInstance();

        public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            return compare((Map<String, Object>) ((Map) obj), (Map<String, Object>) ((Map) obj2));
        }

        public int compare(Map<String, Object> map1, Map<String, Object> map2) {
            return this.collator.compare(map1.get("number"), map2.get("number"));
        }
    };
    /* access modifiers changed from: private */
    public int HEADER_ADD;
    /* access modifiers changed from: private */
    public int HEADER_KNOWN;
    /* access modifiers changed from: private */
    public int HEADER_UNKNOWN;
    /* access modifiers changed from: private */
    public FilterFields mFields;

    private class FilterFields {
        public Comparator<Map<String, Object>> comp;
        public Map<String, Object> currItem;
        public View headerKnown;
        public View headerUnknown;
        public List<Map<String, Object>> list;
        public boolean showKnown;

        private FilterFields() {
            this.comp = ActCallsFilter.sNameComparator;
        }

        /* synthetic */ FilterFields(ActCallsFilter actCallsFilter, FilterFields filterFields) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        boolean z;
        super.onCreate(savedInstanceState);
        this.mFields = (FilterFields) getLastNonConfigurationInstance();
        if (this.mFields == null) {
            this.mFields = new FilterFields(this, null);
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        FilterFields filterFields = this.mFields;
        if (prefs.getBoolean(Globals.PREF_BLOCK_WHITELIST, true)) {
            z = false;
        } else {
            z = true;
        }
        filterFields.showKnown = z;
        loadLayout(this.mFields.showKnown);
    }

    private void loadLayout(boolean showKnown) {
        setContentView((int) R.layout.calls_filter);
        ListView listView = (ListView) findViewById(R.id.callfilter_list);
        LayoutInflater inflater = (LayoutInflater) getSystemService("layout_inflater");
        if (showKnown) {
            this.HEADER_KNOWN = 0;
            this.mFields.headerKnown = inflater.inflate((int) R.layout.callfilter_known, (ViewGroup) findViewById(R.id.callfilter_known_layout));
            listView.addHeaderView(this.mFields.headerKnown, null, true);
        } else {
            this.HEADER_KNOWN = -1;
            this.mFields.headerKnown = null;
        }
        this.mFields.headerUnknown = inflater.inflate((int) R.layout.callfilter_unknown, (ViewGroup) findViewById(R.id.callfilter_unknown_layout));
        listView.addHeaderView(this.mFields.headerUnknown, null, true);
        this.HEADER_UNKNOWN = this.HEADER_KNOWN + 1;
        listView.addHeaderView(inflater.inflate((int) R.layout.calls_filter_header, (ViewGroup) findViewById(R.id.callfilter_header)), null, true);
        this.HEADER_ADD = this.HEADER_UNKNOWN + 1;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onItemClick(AdapterView<?> l, View v, int position, long id) {
                if (position == ActCallsFilter.this.HEADER_ADD) {
                    ActCallsFilter.this.startActivityForResult(new Intent(ActCallsFilter.this.getApplicationContext(), ActCallsFilterAdd.class), 0);
                } else if (position == ActCallsFilter.this.HEADER_UNKNOWN) {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ActCallsFilter.this);
                    Intent unknownIntent = new Intent(ActCallsFilter.this.getApplicationContext(), ActCallsFilterAdd.class);
                    unknownIntent.putExtra("unknown", true);
                    unknownIntent.putExtra("number", ActCallsFilter.this.getString(R.string.callfilter_unknown_number));
                    unknownIntent.putExtra("actionCallsIn", prefs.getInt(Globals.PREF_BLOCK_UNKNOWN_IN, Globals.DEF_BLOCK_UNKNOWN_IN));
                    unknownIntent.putExtra("actionCallsOut", prefs.getInt(Globals.PREF_BLOCK_UNKNOWN_OUT, Globals.DEF_BLOCK_UNKNOWN_OUT));
                    unknownIntent.putExtra("actionSmsIn", prefs.getInt(Globals.PREF_BLOCK_UNKNOWN_SMS, Globals.DEF_BLOCK_UNKNOWN_SMS));
                    ActCallsFilter.this.startActivityForResult(unknownIntent, 0);
                } else if (position == ActCallsFilter.this.HEADER_KNOWN) {
                    SharedPreferences prefs2 = PreferenceManager.getDefaultSharedPreferences(ActCallsFilter.this);
                    Intent knownIntent = new Intent(ActCallsFilter.this.getApplicationContext(), ActCallsFilterAdd.class);
                    knownIntent.putExtra("known", true);
                    knownIntent.putExtra("number", ActCallsFilter.this.getString(R.string.callfilter_known_number));
                    knownIntent.putExtra("actionCallsIn", prefs2.getInt(Globals.PREF_BLOCK_KNOWN_IN, Globals.DEF_BLOCK_KNOWN_IN));
                    knownIntent.putExtra("actionCallsOut", prefs2.getInt(Globals.PREF_BLOCK_KNOWN_OUT, Globals.DEF_BLOCK_KNOWN_OUT));
                    knownIntent.putExtra("actionSmsIn", prefs2.getInt(Globals.PREF_BLOCK_KNOWN_SMS, Globals.DEF_BLOCK_KNOWN_SMS));
                    ActCallsFilter.this.startActivityForResult(knownIntent, 0);
                } else {
                    Map<String, Object> map = (Map) l.getItemAtPosition(position);
                    Intent editIntent = new Intent(ActCallsFilter.this.getApplicationContext(), ActCallsFilterAdd.class);
                    editIntent.putExtra(DbPhoneFilter.DbColumns.COLUMN_ID, (Long) map.get(DbPhoneFilter.DbColumns.COLUMN_ID));
                    editIntent.putExtra("number", (String) map.get("number"));
                    editIntent.putExtra("actionCallsIn", ((DbPhoneFilter.FilterList.Mode) map.get("actionCallsIn")).ordinal());
                    editIntent.putExtra("actionCallsOut", ((DbPhoneFilter.FilterList.Mode) map.get("actionCallsOut")).ordinal());
                    editIntent.putExtra("actionSmsIn", ((DbPhoneFilter.FilterList.Mode) map.get("actionSmsIn")).ordinal());
                    ActCallsFilter.this.startActivityForResult(editIntent, 0);
                }
            }
        });
        registerForContextMenu(listView);
    }

    public Object onRetainNonConfigurationInstance() {
        return this.mFields;
    }

    public void onResume() {
        boolean showKnown;
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (prefs.getBoolean(Globals.PREF_BLOCK_WHITELIST, true)) {
            showKnown = false;
        } else {
            showKnown = true;
        }
        if (this.mFields.showKnown != showKnown) {
            loadLayout(showKnown);
            this.mFields.showKnown = showKnown;
        }
        if (showKnown) {
            loadKnownFiltering();
        }
        loadUnknownFiltering();
        fillFilterList((ListView) findViewById(R.id.callfilter_list));
        if (!prefs.getBoolean(Globals.PREF_BLOCK_ENABLE, false)) {
            Toast.makeText(this, getString(R.string.callfilter_not_enabled), 0).show();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        if (info.position != this.HEADER_ADD && info.position != this.HEADER_UNKNOWN && info.position != this.HEADER_KNOWN) {
            this.mFields.currItem = (Map) ((ListView) findViewById(R.id.callfilter_list)).getItemAtPosition(info.position);
            getMenuInflater().inflate(R.menu.callfilter_ctx, menu);
            menu.setHeaderTitle((String) this.mFields.currItem.get("number"));
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.callfilter_ctx_edit /*2131361973*/:
                Intent editIntent = new Intent(getApplicationContext(), ActCallsFilterAdd.class);
                editIntent.putExtra(DbPhoneFilter.DbColumns.COLUMN_ID, (Long) this.mFields.currItem.get(DbPhoneFilter.DbColumns.COLUMN_ID));
                editIntent.putExtra("number", (String) this.mFields.currItem.get("number"));
                editIntent.putExtra("actionCallsIn", ((DbPhoneFilter.FilterList.Mode) this.mFields.currItem.get("actionCallsIn")).ordinal());
                editIntent.putExtra("actionCallsOut", ((DbPhoneFilter.FilterList.Mode) this.mFields.currItem.get("actionCallsOut")).ordinal());
                editIntent.putExtra("actionSmsIn", ((DbPhoneFilter.FilterList.Mode) this.mFields.currItem.get("actionSmsIn")).ordinal());
                startActivityForResult(editIntent, 0);
                return true;
            case R.id.callfilter_ctx_delete /*2131361974*/:
                new DbPhoneFilter(this).delFilter(((Long) this.mFields.currItem.get(DbPhoneFilter.DbColumns.COLUMN_ID)).longValue());
                this.mFields.list.remove(this.mFields.currItem);
                ((SimpleAdapter) ((HeaderViewListAdapter) ((ListView) findViewById(R.id.callfilter_list)).getAdapter()).getWrappedAdapter()).notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.callfilter, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.callfilter_sort /*2131361971*/:
                showDialog(1);
                return true;
            case R.id.callfilter_clear /*2131361972*/:
                clearFilters();
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new AlertDialog.Builder(this).setTitle(getString(R.string.appinfo_opts_sort_title)).setIcon(17301661).setPositiveButton(getString(R.string.appinfo_opts_sort_asc), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ActCallsFilter.this.sortList();
                    }
                }).setNegativeButton(getString(R.string.appinfo_opts_sort_desc), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ActCallsFilter.this.mFields.comp = Collections.reverseOrder(ActCallsFilter.this.mFields.comp);
                        ActCallsFilter.this.sortList();
                    }
                }).setSingleChoiceItems(new CharSequence[]{getString(R.string.callfilter_sort_byname), getString(R.string.callfilter_sort_bynumber)}, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        switch (item) {
                            case 1:
                                ActCallsFilter.this.mFields.comp = ActCallsFilter.sNumberComparator;
                                return;
                            default:
                                ActCallsFilter.this.mFields.comp = ActCallsFilter.sNameComparator;
                                return;
                        }
                    }
                }).create();
            default:
                return null;
        }
    }

    private void clearFilters() {
        PreferenceManager.getDefaultSharedPreferences(this).edit().putInt(Globals.PREF_BLOCK_UNKNOWN_IN, Globals.DEF_BLOCK_UNKNOWN_IN).putInt(Globals.PREF_BLOCK_UNKNOWN_OUT, Globals.DEF_BLOCK_UNKNOWN_OUT).putInt(Globals.PREF_BLOCK_UNKNOWN_SMS, Globals.DEF_BLOCK_UNKNOWN_SMS).putInt(Globals.PREF_BLOCK_KNOWN_IN, Globals.DEF_BLOCK_KNOWN_IN).putInt(Globals.PREF_BLOCK_KNOWN_OUT, Globals.DEF_BLOCK_KNOWN_OUT).putInt(Globals.PREF_BLOCK_KNOWN_SMS, Globals.DEF_BLOCK_KNOWN_SMS).commit();
        loadKnownFiltering();
        loadUnknownFiltering();
        DbPhoneFilter phoneFilter = new DbPhoneFilter(this);
        boolean failed = false;
        for (int i = this.mFields.list.size() - 1; i >= 0; i--) {
            if (phoneFilter.delFilter(((Long) this.mFields.list.get(i).get(DbPhoneFilter.DbColumns.COLUMN_ID)).longValue())) {
                this.mFields.list.remove(i);
            } else {
                failed = true;
            }
        }
        ((SimpleAdapter) ((HeaderViewListAdapter) ((ListView) findViewById(R.id.callfilter_list)).getAdapter()).getWrappedAdapter()).notifyDataSetChanged();
        if (failed) {
            Toast.makeText(this, getString(R.string.callfilter_clear_failed), 0).show();
        }
    }

    private void fillFilterList(ListView listView) {
        DbPhoneFilter.FilterIterator filter;
        this.mFields.list = new ArrayList();
        try {
            filter = new DbPhoneFilter(this).queryFilterList();
        } catch (Exception e) {
            filter = null;
        }
        if (filter != null) {
            while (filter.moveToNext()) {
                addItem(this.mFields.list, filter.getID(), filter.getPhoneNumber(true), filter.getCallInMode(), filter.getCallOutMode(), filter.getSmsInMode());
            }
            filter.close();
        }
        listView.setAdapter((ListAdapter) new SimpleAdapter(this, this.mFields.list, R.layout.callfilter_row, new String[]{"number", "name", "iconCallsIn", "iconCallsOut", "iconSmsIn"}, new int[]{R.id.callfilter_row_number, R.id.callfilter_row_name, R.id.callfilter_row_callsin, R.id.callfilter_row_callsout, R.id.callfilter_row_smsin}));
        sortList();
    }

    private void addItem(List<Map<String, Object>> list, long phoneID, String number, DbPhoneFilter.FilterList.Mode actionCallsIn, DbPhoneFilter.FilterList.Mode actionCallsOut, DbPhoneFilter.FilterList.Mode actionSmsIn) {
        Map<String, Object> item = new HashMap<>();
        item.put(DbPhoneFilter.DbColumns.COLUMN_ID, Long.valueOf(phoneID));
        item.put("number", number);
        item.put("name", Globals.getContactName(this, number));
        item.put("actionCallsIn", actionCallsIn);
        item.put("iconCallsIn", Integer.valueOf(getActionIcon(1, actionCallsIn)));
        item.put("actionCallsOut", actionCallsOut);
        item.put("iconCallsOut", Integer.valueOf(getActionIcon(0, actionCallsOut)));
        item.put("actionSmsIn", actionSmsIn);
        item.put("iconSmsIn", Integer.valueOf(getActionIcon(2, actionSmsIn)));
        list.add(item);
    }

    static int getActionIcon(int type, DbPhoneFilter.FilterList.Mode action) {
        switch (type) {
            case 0:
                if (action == DbPhoneFilter.FilterList.Mode.Allow) {
                    return R.drawable.filter_callout_allow;
                }
                if (action == DbPhoneFilter.FilterList.Mode.Deny) {
                    return R.drawable.filter_callout_block;
                }
                if (action == DbPhoneFilter.FilterList.Mode.Ask) {
                    return R.drawable.filter_callout_ask;
                }
                return R.drawable.filter_callout_notset;
            case 1:
                if (action == DbPhoneFilter.FilterList.Mode.Allow) {
                    return R.drawable.filter_callin_allow;
                }
                if (action == DbPhoneFilter.FilterList.Mode.Deny) {
                    return R.drawable.filter_callin_block;
                }
                return R.drawable.filter_callin_notset;
            case 2:
                if (action == DbPhoneFilter.FilterList.Mode.Allow) {
                    return R.drawable.filter_sms_allow;
                }
                if (action == DbPhoneFilter.FilterList.Mode.Deny) {
                    return R.drawable.filter_sms_block;
                }
                return R.drawable.filter_sms_notset;
            default:
                return R.drawable.level0;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    private void loadKnownFiltering() {
        if (this.mFields.headerKnown != null) {
            ((TextView) this.mFields.headerKnown.findViewById(R.id.callfilter_known_number)).setText((int) R.string.callfilter_known_number);
            ((TextView) this.mFields.headerKnown.findViewById(R.id.callfilter_known_name)).setText((int) R.string.callfilter_known_name);
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            int inActionOrd = prefs.getInt(Globals.PREF_BLOCK_KNOWN_IN, Globals.DEF_BLOCK_KNOWN_IN);
            int outActionOrd = prefs.getInt(Globals.PREF_BLOCK_KNOWN_OUT, Globals.DEF_BLOCK_KNOWN_OUT);
            int smsActionOrd = prefs.getInt(Globals.PREF_BLOCK_KNOWN_SMS, Globals.DEF_BLOCK_KNOWN_SMS);
            DbPhoneFilter.FilterList.Mode inAction = DbPhoneFilter.FilterList.Mode.getEnum(inActionOrd);
            DbPhoneFilter.FilterList.Mode outAction = DbPhoneFilter.FilterList.Mode.getEnum(outActionOrd);
            DbPhoneFilter.FilterList.Mode smsAction = DbPhoneFilter.FilterList.Mode.getEnum(smsActionOrd);
            ((ImageView) this.mFields.headerKnown.findViewById(R.id.callfilter_known_callsin)).setImageResource(getActionIcon(1, inAction));
            ((ImageView) this.mFields.headerKnown.findViewById(R.id.callfilter_known_callsout)).setImageResource(getActionIcon(0, outAction));
            ((ImageView) this.mFields.headerKnown.findViewById(R.id.callfilter_known_smsin)).setImageResource(getActionIcon(2, smsAction));
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    private void loadUnknownFiltering() {
        if (this.mFields.headerUnknown != null) {
            ((TextView) this.mFields.headerUnknown.findViewById(R.id.callfilter_unknown_number)).setText((int) R.string.callfilter_unknown_number);
            ((TextView) this.mFields.headerUnknown.findViewById(R.id.callfilter_unknown_name)).setText((int) R.string.callfilter_unknown_name);
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            int inActionOrd = prefs.getInt(Globals.PREF_BLOCK_UNKNOWN_IN, Globals.DEF_BLOCK_UNKNOWN_IN);
            int outActionOrd = prefs.getInt(Globals.PREF_BLOCK_UNKNOWN_OUT, Globals.DEF_BLOCK_UNKNOWN_OUT);
            int smsActionOrd = prefs.getInt(Globals.PREF_BLOCK_UNKNOWN_SMS, Globals.DEF_BLOCK_UNKNOWN_SMS);
            DbPhoneFilter.FilterList.Mode inAction = DbPhoneFilter.FilterList.Mode.getEnum(inActionOrd);
            DbPhoneFilter.FilterList.Mode outAction = DbPhoneFilter.FilterList.Mode.getEnum(outActionOrd);
            DbPhoneFilter.FilterList.Mode smsAction = DbPhoneFilter.FilterList.Mode.getEnum(smsActionOrd);
            ((ImageView) this.mFields.headerUnknown.findViewById(R.id.callfilter_unknown_callsin)).setImageResource(getActionIcon(1, inAction));
            ((ImageView) this.mFields.headerUnknown.findViewById(R.id.callfilter_unknown_callsout)).setImageResource(getActionIcon(0, outAction));
            ((ImageView) this.mFields.headerUnknown.findViewById(R.id.callfilter_unknown_smsin)).setImageResource(getActionIcon(2, smsAction));
        }
    }

    /* access modifiers changed from: private */
    public void sortList() {
        Collections.sort(this.mFields.list, this.mFields.comp);
        ((SimpleAdapter) ((HeaderViewListAdapter) ((ListView) findViewById(R.id.callfilter_list)).getAdapter()).getWrappedAdapter()).notifyDataSetChanged();
    }
}
