package com.zoner.android.antivirus;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;
import com.zoner.android.antivirus.ScanResult;

public class ResultNotifier {
    private static final int NOT_ZAV = 1;
    static final int STATE_CLEAN = 2;
    static final int STATE_CLEAN_SCANNING = 3;
    static final int STATE_INFECTED = 4;
    static final int STATE_INFECTED_SCANNING = 5;
    static final int STATE_NOICON = -1;
    static final int STATE_READY = 0;
    static final int STATE_SCANNING = 1;
    private static Intent appIntent;
    static final int[] icon = {R.drawable.zav_white, R.anim.scan_white, R.drawable.zav_green, R.anim.scan_green, R.drawable.zav_red, R.anim.scan_red};
    static final int[] nicon = {R.drawable.notif, R.drawable.notif, R.drawable.notif_clean, R.drawable.notif_clean, R.drawable.notif_threats, R.drawable.notif_threats};
    private static Intent resultIntent;
    static final int[] text = {R.string.notif_ready, R.string.notif_scanning, R.string.notif_clean, R.string.notif_clean, R.plurals.notif_threats, R.plurals.notif_threats};
    static final int[] ticon = {R.drawable.zav_white, R.drawable.zav_white, R.drawable.zav_green, R.drawable.zav_green, R.drawable.zav_red, R.drawable.zav_red};
    private int cResults = 0;
    private boolean hasResults = false;
    private boolean isPermanent = false;
    private boolean isScanning = false;
    private NotificationManager mNM;
    private ZapService mService;

    ResultNotifier(ZapService service) {
        this.mService = service;
        this.mNM = (NotificationManager) this.mService.getSystemService("notification");
        appIntent = this.mService.getPackageManager().getLaunchIntentForPackage(this.mService.getPackageName());
        if (appIntent == null) {
            Log.d("ZonerAV", "Cannot get launch intent for notification");
        } else {
            appIntent.addCategory("android.intent.category.LAUNCHER");
            appIntent.setFlags(274726912);
        }
        resultIntent = new Intent(this.mService, ActScanResults.class);
        resultIntent.setFlags(947912704);
    }

    public synchronized void scanStart() {
        this.isScanning = true;
        showNotification(false, null);
    }

    public synchronized void scanStop() {
        this.isScanning = false;
        showNotification(false, null);
    }

    public synchronized void notifyPackage(ScanResult result) {
        if (result.result != ScanResult.Result.CLEAN) {
            this.hasResults = true;
            this.cResults++;
        }
        boolean oldResults = this.hasResults;
        this.hasResults = true;
        showTicker(this.mService.getString(result.result != ScanResult.Result.CLEAN ? R.string.ticker_package_infected : R.string.ticker_package_clean));
        this.hasResults = oldResults;
        showNotification(result.result != ScanResult.Result.CLEAN, null);
    }

    public synchronized void notifyOnAccess(ScanResult result) {
        this.hasResults = true;
        if (result.result != ScanResult.Result.CLEAN) {
            this.cResults++;
        }
        showTicker(this.mService.getString(R.string.ticker_access));
        showNotification(result.result != ScanResult.Result.CLEAN, null);
    }

    public synchronized void notifyOnDemand(int cInfections) {
        String ticker;
        this.hasResults = true;
        this.cResults += cInfections;
        if (this.cResults > 0) {
            ticker = this.mService.getResources().getQuantityString(R.plurals.ticker_threats, this.cResults, Integer.valueOf(this.cResults));
        } else {
            ticker = this.mService.getString(R.string.ticker_clean);
        }
        showTicker(ticker);
        showNotification(false, null);
    }

    public synchronized void showIcon() {
        this.isPermanent = true;
        showNotification(false, null);
    }

    public synchronized void hideIcon() {
        this.isPermanent = false;
        showNotification(false, null);
    }

    public synchronized void notifyResolved() {
        this.hasResults = this.mService.hasResults();
        this.cResults = this.mService.infectionCount();
        showNotification(false, null);
    }

    private int getState() {
        if (!this.hasResults) {
            if (this.isScanning) {
                return 1;
            }
            if (this.isPermanent) {
                return 0;
            }
            return STATE_NOICON;
        } else if (this.cResults == 0) {
            if (this.isScanning) {
                return 3;
            }
            return 2;
        } else if (this.isScanning) {
            return STATE_INFECTED_SCANNING;
        } else {
            return STATE_INFECTED;
        }
    }

    private void showTicker(String ticker) {
        showNotification(false, ticker);
    }

    private void showNotification(boolean popup, String ticker) {
        String fullText;
        int i;
        int state = getState();
        if (state == STATE_NOICON) {
            this.mNM.cancel(1);
            return;
        }
        Intent[] intent = new Intent[6];
        intent[0] = appIntent;
        intent[1] = appIntent;
        intent[2] = resultIntent;
        intent[3] = resultIntent;
        intent[STATE_INFECTED] = resultIntent;
        intent[STATE_INFECTED_SCANNING] = resultIntent;
        if (state == STATE_INFECTED || state == STATE_INFECTED_SCANNING) {
            fullText = this.mService.getResources().getQuantityString(text[state], this.cResults, Integer.valueOf(this.cResults));
        } else {
            fullText = this.mService.getString(text[state]);
        }
        if (ticker != null) {
            i = ticon[state];
        } else {
            i = icon[state];
        }
        Notification n = new Notification(i, ticker, System.currentTimeMillis());
        if (this.isPermanent) {
            n.flags |= 2;
        }
        RemoteViews contentView = new RemoteViews(this.mService.getPackageName(), (int) R.layout.notif);
        contentView.setImageViewResource(R.id.notification_image, nicon[state]);
        contentView.setTextViewText(R.id.notification_text, fullText);
        if (Build.VERSION.SDK_INT >= 11) {
            contentView.setTextColor(R.id.notification_header, this.mService.getResources().getColor(17170433));
            contentView.setTextColor(R.id.notification_text, this.mService.getResources().getColor(17170433));
        } else {
            contentView.setTextColor(R.id.notification_header, this.mService.getResources().getColor(17170435));
            contentView.setTextColor(R.id.notification_text, this.mService.getResources().getColor(17170435));
        }
        n.contentView = contentView;
        n.contentIntent = PendingIntent.getActivity(this.mService, 0, intent[state], 0);
        this.mNM.notify(1, n);
        if (popup) {
            this.mService.startActivity(intent[state]);
        }
    }
}
