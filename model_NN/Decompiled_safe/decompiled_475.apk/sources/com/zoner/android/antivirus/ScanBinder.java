package com.zoner.android.antivirus;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;

public class ScanBinder extends ServiceBinder {
    /* access modifiers changed from: private */
    public IScanConnector mCaller;

    private class ServiceHandler extends Handler {
        private ServiceHandler() {
        }

        /* synthetic */ ServiceHandler(ScanBinder scanBinder, ServiceHandler serviceHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    ScanResult result = (ScanResult) msg.obj;
                    if (result != null) {
                        ScanBinder.this.mCaller.onScanResult(result);
                        return;
                    }
                    return;
                case 2:
                    ScanBinder.this.mCaller.onScanFinish();
                    return;
                default:
                    super.handleMessage(msg);
                    return;
            }
        }
    }

    ScanBinder(IScanConnector caller, Context context) {
        super(caller, context, 0);
        this.mCaller = caller;
        this.mMessenger = new Messenger(new ServiceHandler(this, null));
    }
}
