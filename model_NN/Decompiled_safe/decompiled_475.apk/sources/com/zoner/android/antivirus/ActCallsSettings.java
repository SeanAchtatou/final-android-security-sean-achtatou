package com.zoner.android.antivirus;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class ActCallsSettings extends Activity {
    CheckBox mChkEnable;
    CheckBox mChkLock;
    CheckBox mChkWhite;
    EditText mPass;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.calls_settings);
        View.OnClickListener listener = new View.OnClickListener() {
            public void onClick(View v) {
                boolean newValue = ActCallsSettings.this.mChkEnable.isChecked();
                if (v instanceof RelativeLayout) {
                    newValue = !newValue;
                    ActCallsSettings.this.mChkEnable.setChecked(newValue);
                }
                ActCallsSettings.this.writeSetting(Globals.PREF_BLOCK_ENABLE, newValue);
            }
        };
        ((RelativeLayout) findViewById(R.id.relativeLayout1)).setOnClickListener(listener);
        this.mChkEnable = (CheckBox) findViewById(R.id.callset_enable);
        this.mChkEnable.setOnClickListener(listener);
        View.OnClickListener listener2 = new View.OnClickListener() {
            public void onClick(View v) {
                boolean newValue = ActCallsSettings.this.mChkWhite.isChecked();
                if (v instanceof RelativeLayout) {
                    newValue = !newValue;
                    ActCallsSettings.this.mChkWhite.setChecked(newValue);
                }
                ActCallsSettings.this.writeSetting(Globals.PREF_BLOCK_WHITELIST, newValue);
            }
        };
        ((RelativeLayout) findViewById(R.id.relativeLayout2)).setOnClickListener(listener2);
        this.mChkWhite = (CheckBox) findViewById(R.id.callset_white);
        this.mChkWhite.setOnClickListener(listener2);
        View.OnClickListener listener3 = new View.OnClickListener() {
            public void onClick(View v) {
                boolean newValue = ActCallsSettings.this.mChkLock.isChecked();
                if (v instanceof RelativeLayout) {
                    newValue = !newValue;
                    ActCallsSettings.this.mChkLock.setChecked(newValue);
                }
                ActCallsSettings.this.mPass.setEnabled(newValue);
                ActCallsSettings.this.mPass.setFocusable(newValue);
                ActCallsSettings.this.mPass.setFocusableInTouchMode(newValue);
                ActCallsSettings.this.writeSetting(Globals.PREF_BLOCK_PARENTAL, newValue);
            }
        };
        ((RelativeLayout) findViewById(R.id.relativeLayout3)).setOnClickListener(listener3);
        this.mChkLock = (CheckBox) findViewById(R.id.callset_lock);
        this.mChkLock.setOnClickListener(listener3);
        this.mPass = (EditText) findViewById(R.id.callset_pass);
        readSettings();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        readSettingEnabled();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString(Globals.PREF_BLOCK_PASSWORD, this.mPass.getText().toString()).commit();
    }

    private void readSettings() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        this.mChkEnable.setChecked(prefs.getBoolean(Globals.PREF_BLOCK_ENABLE, false));
        this.mChkWhite.setChecked(prefs.getBoolean(Globals.PREF_BLOCK_WHITELIST, true));
        boolean lock = prefs.getBoolean(Globals.PREF_BLOCK_PARENTAL, false);
        this.mChkLock.setChecked(lock);
        this.mPass.setText(prefs.getString(Globals.PREF_BLOCK_PASSWORD, Globals.DEF_BLOCK_PASSWORD));
        this.mPass.setEnabled(lock);
        this.mPass.setFocusable(lock);
        this.mPass.setFocusableInTouchMode(lock);
    }

    private void readSettingEnabled() {
        this.mChkEnable.setChecked(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Globals.PREF_BLOCK_ENABLE, false));
    }

    /* access modifiers changed from: private */
    public void writeSetting(String name, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean(name, value).commit();
    }
}
