package com.zoner.android.antivirus;

import com.zoner.android.antivirus.ResultStorage;

public interface IResultWorker {
    ResultStorage.Storage accessResults();

    void allResolved();

    void demandResolved();

    ResultStorage.Storage demandResults();

    boolean hasDemandResults();

    boolean hasResults();

    ResultStorage.Storage installResults();

    void resolve(ScanResult scanResult);

    boolean scanFinished();
}
