package com.zoner.android.antivirus;

import android.app.ListActivity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.zoner.android.antivirus.DbPhoneFilter;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppInfoStorage {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$AppInfoStorage$SortBy;
    private static final Comparator<Map<String, Object>> sIDComparator = new Comparator<Map<String, Object>>() {
        private final Collator collator = Collator.getInstance();

        public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            return compare((Map<String, Object>) ((Map) obj), (Map<String, Object>) ((Map) obj2));
        }

        public int compare(Map<String, Object> map1, Map<String, Object> map2) {
            return this.collator.compare((String) map1.get(DbPhoneFilter.DbColumns.COLUMN_ID), (String) map2.get(DbPhoneFilter.DbColumns.COLUMN_ID));
        }
    };
    private static final Comparator<Map<String, Object>> sLevelComparator = new Comparator<Map<String, Object>>() {
        public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            return compare((Map<String, Object>) ((Map) obj), (Map<String, Object>) ((Map) obj2));
        }

        /* Debug info: failed to restart local var, previous not found, register: 7 */
        public int compare(Map<String, Object> map1, Map<String, Object> map2) {
            int lvlCompare = ((Integer) map1.get("level")).compareTo((Integer) map2.get("level"));
            if (lvlCompare != 0) {
                return lvlCompare;
            }
            return new Integer(Globals.toInt((String) map1.get("count"), 0)).compareTo(Integer.valueOf(Globals.toInt((String) map2.get("count"), 0)));
        }
    };
    private static final Comparator<Map<String, Object>> sNameComparator = new Comparator<Map<String, Object>>() {
        private final Collator collator = Collator.getInstance();

        public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            return compare((Map<String, Object>) ((Map) obj), (Map<String, Object>) ((Map) obj2));
        }

        public int compare(Map<String, Object> map1, Map<String, Object> map2) {
            return this.collator.compare(map1.get("name"), map2.get("name"));
        }
    };
    private static final Comparator<Map<String, Object>> sSubCountComparator = new Comparator<Map<String, Object>>() {
        public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            return compare((Map<String, Object>) ((Map) obj), (Map<String, Object>) ((Map) obj2));
        }

        public int compare(Map<String, Object> map1, Map<String, Object> map2) {
            return new Integer(Globals.toInt((String) map1.get("count"), 0)).compareTo(Integer.valueOf(Globals.toInt((String) map2.get("count"), 0)));
        }
    };
    public int mAppCount;
    private List<Map<String, Object>> mAppList;
    private Map<String, ItemInfo> mAppPermsMap;
    private Comparator<Map<String, Object>> mAppsComp = Collections.reverseOrder(sLevelComparator);
    private AsyncTask<Void, Integer, AppInfoStorage> mAsyncTask;
    private Context mCtx;
    private PackageManager mPM;
    private Map<String, ItemInfo> mPermAppsMap;
    public int mPermCount;
    private PermissionLevels mPermLevels = new PermissionLevels();
    private List<Map<String, Object>> mPermList;
    private Comparator<Map<String, Object>> mPermsComp = Collections.reverseOrder(sLevelComparator);

    public enum CurrList {
        APP_LIST,
        PERM_LIST
    }

    public enum SortBy {
        NAME,
        ID,
        COUNT,
        LEVEL
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$AppInfoStorage$SortBy() {
        int[] iArr = $SWITCH_TABLE$com$zoner$android$antivirus$AppInfoStorage$SortBy;
        if (iArr == null) {
            iArr = new int[SortBy.values().length];
            try {
                iArr[SortBy.COUNT.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[SortBy.ID.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[SortBy.LEVEL.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[SortBy.NAME.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$zoner$android$antivirus$AppInfoStorage$SortBy = iArr;
        }
        return iArr;
    }

    private class ItemInfo {
        public Map<String, Object> item;
        public List<Map<String, Object>> sublist;

        private ItemInfo() {
        }

        /* synthetic */ ItemInfo(AppInfoStorage appInfoStorage, ItemInfo itemInfo) {
            this();
        }
    }

    AppInfoStorage(AsyncTask<Void, Integer, AppInfoStorage> asyncTask, PackageManager pm, Context ctx) {
        this.mPM = pm;
        this.mCtx = ctx;
        this.mAsyncTask = asyncTask;
        this.mAppList = new ArrayList();
        this.mAppPermsMap = new HashMap();
        this.mPermList = new ArrayList();
        this.mPermAppsMap = new HashMap();
        List<PackageInfo> list = pm.getInstalledPackages(4096);
        if (list != null) {
            for (PackageInfo pi : list) {
                if (this.mAsyncTask.isCancelled()) {
                    break;
                } else if ((pi.applicationInfo.flags & 1) == 0) {
                    addApp(pi);
                }
            }
            Collections.sort(this.mAppList, this.mAppsComp);
            Collections.sort(this.mPermList, this.mPermsComp);
            this.mAppCount = this.mAppList.size();
            this.mPermCount = this.mPermList.size();
        }
    }

    /* access modifiers changed from: protected */
    public void addApp(PackageInfo info) {
        Map<String, Object> appItem = getAppItem(info);
        this.mAppList.add(appItem);
        int level = 0;
        List<Map<String, Object>> permList = new ArrayList<>();
        String[] perms = info.requestedPermissions;
        if (perms != null) {
            for (String perm : perms) {
                ItemInfo permInfo = this.mPermAppsMap.get(perm);
                if (permInfo == null) {
                    Map<String, Object> permItem = getPermItem(perm);
                    permInfo = new ItemInfo(this, null);
                    permInfo.sublist = new ArrayList();
                    permInfo.item = permItem;
                    this.mPermList.add(permItem);
                }
                int currLvl = ((Integer) permInfo.item.get("level")).intValue();
                if (currLvl > level) {
                    level = currLvl;
                }
                permList.add(permInfo.item);
                permInfo.sublist.add(appItem);
                permInfo.item.put("count", Integer.toString(Globals.toInt((String) permInfo.item.get("count"), 0) + 1));
                Collections.sort(permInfo.sublist, sNameComparator);
                this.mPermAppsMap.put(perm, permInfo);
            }
        }
        Collections.sort(permList, Collections.reverseOrder(sLevelComparator));
        appItem.put("level", Integer.valueOf(level));
        ItemInfo itemInfo = new ItemInfo(this, null);
        itemInfo.item = appItem;
        itemInfo.sublist = permList;
        this.mAppPermsMap.put(info.packageName, itemInfo);
    }

    private Map<String, Object> getAppItem(PackageInfo info) {
        Map<String, Object> tmp = new HashMap<>();
        tmp.put("name", this.mPM.getApplicationLabel(info.applicationInfo));
        tmp.put(DbPhoneFilter.DbColumns.COLUMN_ID, info.packageName);
        tmp.put("icon", this.mPM.getApplicationIcon(info.applicationInfo));
        tmp.put("count", Integer.toString(info.requestedPermissions != null ? info.requestedPermissions.length : 0));
        return tmp;
    }

    private Map<String, Object> getPermItem(String perm) {
        String desc;
        Map<String, Object> tmp = new HashMap<>();
        try {
            desc = (String) this.mPM.getPermissionInfo(perm, 0).loadDescription(this.mPM);
        } catch (PackageManager.NameNotFoundException e) {
            desc = this.mCtx.getString(R.string.appinfo_no_perm_desc);
        }
        if (desc == null || desc.length() == 0) {
            desc = this.mCtx.getString(R.string.appinfo_no_perm_desc);
        }
        int level = this.mPermLevels.getLevel(perm);
        int icon = this.mPermLevels.getIcon(level);
        tmp.put(DbPhoneFilter.DbColumns.COLUMN_ID, perm);
        tmp.put("name", perm.substring(perm.lastIndexOf(".") + 1));
        tmp.put("prefix", perm.substring(0, perm.lastIndexOf(".") + 1));
        tmp.put("desc", desc);
        tmp.put("count", "0");
        tmp.put("icon", Integer.valueOf(icon));
        tmp.put("level", Integer.valueOf(level));
        return tmp;
    }

    public boolean addPackage(String packageName) {
        try {
            addApp(this.mPM.getPackageInfo(packageName, 4096));
            this.mAppCount = this.mAppList.size();
            this.mPermCount = this.mPermList.size();
            Collections.sort(this.mAppList, this.mAppsComp);
            Collections.sort(this.mPermList, this.mPermsComp);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("ZonerAV", "No package info for " + packageName);
            return false;
        }
    }

    public void removePackage(String packageName) {
        ItemInfo appInfo = this.mAppPermsMap.get(packageName);
        if (appInfo != null) {
            this.mAppList.remove(appInfo.item);
            this.mAppCount--;
            for (Map<String, Object> permItem : appInfo.sublist) {
                int count = Globals.toInt((String) permItem.get("count"), 0) - 1;
                if (count <= 0) {
                    this.mPermAppsMap.remove(permItem.get(DbPhoneFilter.DbColumns.COLUMN_ID));
                    this.mPermList.remove(permItem);
                    this.mPermCount--;
                } else {
                    permItem.put("count", Integer.toString(count));
                    this.mPermAppsMap.get((String) permItem.get(DbPhoneFilter.DbColumns.COLUMN_ID)).sublist.remove(appInfo.item);
                }
            }
        }
    }

    public void showList(ListActivity act, CurrList list) {
        SimpleAdapter adapter;
        if (list == CurrList.APP_LIST) {
            adapter = getAppListAdapter(act);
        } else {
            adapter = getPermListAdapter(act);
        }
        act.setListAdapter(adapter);
    }

    public SimpleAdapter getSubListAdapter(String id, CurrList list) {
        if (list == CurrList.APP_LIST) {
            return getAppPermsListAdapter(this.mCtx, id);
        }
        return getPermAppsListAdapter(this.mCtx, id);
    }

    private SimpleAdapter getAppListAdapter(Context ctx) {
        return new AppAdapter(ctx, this.mAppList, R.layout.appinfo_app_row, new String[]{"name"}, new int[]{R.id.appinfo_appitem_name});
    }

    private SimpleAdapter getPermAppsListAdapter(Context ctx, String perm) {
        ItemInfo info = this.mPermAppsMap.get(perm);
        if (info == null) {
            info = new ItemInfo(this, null);
            info.sublist = new ArrayList();
        }
        return new AppAdapter(ctx, info.sublist, R.layout.appinfo_subapp_row, new String[]{"name"}, new int[]{R.id.appinfo_appitem_name});
    }

    private SimpleAdapter getPermListAdapter(Context ctx) {
        return new PermAdapter(ctx, this.mPermList, R.layout.appinfo_perm_row, new String[]{"name"}, new int[]{R.id.appinfo_permitem_name});
    }

    private SimpleAdapter getAppPermsListAdapter(Context ctx, String packageName) {
        ItemInfo info = this.mAppPermsMap.get(packageName);
        if (info == null) {
            info = new ItemInfo(this, null);
            info.sublist = new ArrayList();
        }
        return new PermAdapter(ctx, info.sublist, R.layout.appinfo_subperm_row, new String[]{"name"}, new int[]{R.id.appinfo_permitem_name});
    }

    public void sortList(SortBy by, Boolean ascending, CurrList list) {
        Comparator<Map<String, Object>> comp;
        switch ($SWITCH_TABLE$com$zoner$android$antivirus$AppInfoStorage$SortBy()[by.ordinal()]) {
            case 2:
                comp = sIDComparator;
                break;
            case DbScanner.ERRFORMAT:
                comp = sSubCountComparator;
                break;
            case 4:
                comp = sLevelComparator;
                break;
            default:
                comp = sNameComparator;
                break;
        }
        if (!ascending.booleanValue()) {
            comp = Collections.reverseOrder(comp);
        }
        if (list == CurrList.APP_LIST) {
            this.mAppsComp = comp;
            Collections.sort(this.mAppList, comp);
            return;
        }
        this.mPermsComp = comp;
        Collections.sort(this.mPermList, comp);
    }

    class AppAdapter extends SimpleAdapter {
        AppAdapter(Context context, List<Map<String, Object>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View row = super.getView(position, convertView, parent);
            Map<String, Object> rowMap = (Map) getItem(position);
            ((ImageView) row.findViewById(R.id.appinfo_appitem_icon)).setImageDrawable((Drawable) rowMap.get("icon"));
            TextView countView = (TextView) row.findViewById(R.id.appinfo_appitem_permcount);
            ((TextView) row.findViewById(R.id.appinfo_appitem_packagename)).setText((String) rowMap.get(DbPhoneFilter.DbColumns.COLUMN_ID));
            String color = (String) rowMap.get("count");
            countView.setText(color);
            if (Globals.toInt(color, 0) == 0) {
                countView.setTextColor((int) PermissionLevels.safeColor);
            } else {
                countView.setTextColor(PermissionLevels.colors[((Integer) rowMap.get("level")).intValue()]);
            }
            return row;
        }
    }

    class PermAdapter extends SimpleAdapter {
        PermAdapter(Context context, List<Map<String, Object>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View row = super.getView(position, convertView, parent);
            Map<String, Object> rowMap = (Map) getItem(position);
            ((ImageView) row.findViewById(R.id.appinfo_permitem_icon)).setImageResource(((Integer) rowMap.get("icon")).intValue());
            ((TextView) row.findViewById(R.id.appinfo_permitem_prefix)).setText((String) rowMap.get("prefix"));
            ((TextView) row.findViewById(R.id.appinfo_permitem_appcount)).setText((String) rowMap.get("count"));
            ((TextView) row.findViewById(R.id.appinfo_permitem_desc)).setText((String) rowMap.get("desc"));
            return row;
        }
    }
}
