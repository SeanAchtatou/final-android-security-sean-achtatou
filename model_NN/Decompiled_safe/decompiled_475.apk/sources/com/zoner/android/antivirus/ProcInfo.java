package com.zoner.android.antivirus;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Debug;
import android.util.Log;
import java.io.BufferedReader;
import java.io.FileReader;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ProcInfo {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$ProcInfo$SortBy;
    private static final Comparator<Map<String, Object>> sMemComparator = new Comparator<Map<String, Object>>() {
        public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            return compare((Map<String, Object>) ((Map) obj), (Map<String, Object>) ((Map) obj2));
        }

        public int compare(Map<String, Object> map1, Map<String, Object> map2) {
            return Double.valueOf(Globals.toDouble(((String) map1.get("mem")).split(" ")[0], 0.0d)).compareTo(Double.valueOf(Globals.toDouble(((String) map2.get("mem")).split(" ")[0], 0.0d)));
        }
    };
    private static final Comparator<Map<String, Object>> sNameComparator = new Comparator<Map<String, Object>>() {
        private final Collator collator = Collator.getInstance();

        public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            return compare((Map<String, Object>) ((Map) obj), (Map<String, Object>) ((Map) obj2));
        }

        public int compare(Map<String, Object> map1, Map<String, Object> map2) {
            return this.collator.compare(map1.get("appName"), map2.get("appName"));
        }
    };
    private ActivityManager mAM;
    private Comparator<Map<String, Object>> mComp = sNameComparator;
    private Context mCtx;
    public String mMemTotal;
    public String mMemUsed;
    public List<Map<String, Object>> mNewList;
    public List<Map<String, Object>> mTaskList;
    private Map<String, Object> mTaskMap;

    public enum SortBy {
        NAME,
        MEM
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$ProcInfo$SortBy() {
        int[] iArr = $SWITCH_TABLE$com$zoner$android$antivirus$ProcInfo$SortBy;
        if (iArr == null) {
            iArr = new int[SortBy.values().length];
            try {
                iArr[SortBy.MEM.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[SortBy.NAME.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$zoner$android$antivirus$ProcInfo$SortBy = iArr;
        }
        return iArr;
    }

    ProcInfo(Context ctx, ActivityManager am) {
        this.mCtx = ctx;
        this.mAM = am;
        this.mTaskMap = new HashMap();
        this.mTaskList = getTaskList();
        this.mNewList = new ArrayList();
    }

    private List<Map<String, Object>> getTaskList() {
        List<Map<String, Object>> tasks = new ArrayList<>();
        for (ActivityManager.RunningAppProcessInfo info : this.mAM.getRunningAppProcesses()) {
            addProcess(tasks, info);
        }
        Collections.sort(tasks, this.mComp);
        updateTotalStats();
        return tasks;
    }

    public Map<String, Object> getItem(String name) {
        return (Map) this.mTaskMap.get(name);
    }

    private synchronized Map<String, Object> addProcess(List<Map<String, Object>> tasks, ActivityManager.RunningAppProcessInfo info) {
        Map<String, Object> serviceItem;
        serviceItem = new HashMap<>();
        PackageManager pm = this.mCtx.getPackageManager();
        try {
            ApplicationInfo ai = pm.getApplicationInfo(info.processName, 0);
            serviceItem.put("appName", pm.getApplicationLabel(ai));
            serviceItem.put("appIcon", pm.getApplicationIcon(ai));
        } catch (Exception e) {
            serviceItem.put("appName", info.processName);
            serviceItem.put("appIcon", this.mCtx.getResources().getDrawable(17301651));
        }
        serviceItem.put("procName", info.processName);
        try {
            serviceItem.put("appName", pm.getApplicationLabel(pm.getApplicationInfo(info.processName, 0)));
        } catch (Exception e2) {
            serviceItem.put("appName", info.processName);
        }
        serviceItem.put("pid", Integer.valueOf(info.pid));
        updateItem(serviceItem, false);
        this.mTaskMap.put(info.processName, serviceItem);
        tasks.add(serviceItem);
        return serviceItem;
    }

    private void updateTotalStats() {
        try {
            BufferedReader in = new BufferedReader(new FileReader("/proc/meminfo"), 1024);
            long memTotal = Globals.toLong(in.readLine().split("  *")[1], 0);
            this.mMemTotal = String.format(Locale.ENGLISH, "%.2f M", Double.valueOf(((double) memTotal) / 1024.0d));
            try {
                String[] data = in.readLine().split("  *");
                this.mMemUsed = String.valueOf(String.format(Locale.ENGLISH, "%.2f M", Double.valueOf(((double) (memTotal - Globals.toLong(data[1], 0))) / 1024.0d))) + " / " + this.mMemTotal;
            } catch (Exception e) {
            }
        } catch (Exception e2) {
        }
    }

    public synchronized void updateTaskList(Map<String, Object> advancedItem) {
        for (ActivityManager.RunningAppProcessInfo info : this.mAM.getRunningAppProcesses()) {
            Map<String, Object> item = (Map) this.mTaskMap.get(info.processName);
            if (item == null) {
                item = addProcess(this.mNewList, info);
            }
            updateItem(item, item.equals(advancedItem));
            item.put("updated", true);
        }
        updateTotalStats();
    }

    public synchronized void deleteInactive() {
        Collection<Map<String, Object>> toDel = new ArrayList<>();
        for (Map<String, Object> curr : this.mTaskList) {
            Boolean updated = (Boolean) curr.get("updated");
            if (updated != null) {
                if (updated.booleanValue()) {
                    curr.put("updated", false);
                } else {
                    this.mTaskMap.remove(curr.get("procName"));
                    toDel.add(curr);
                }
            }
        }
        this.mTaskList.removeAll(toDel);
    }

    public void addNew() {
        this.mTaskList.addAll(this.mNewList);
        this.mNewList.clear();
    }

    private void updateItem(Map<String, Object> item, boolean updateAll) {
        Integer pid = (Integer) item.get("pid");
        Debug.MemoryInfo[] mem = this.mAM.getProcessMemoryInfo(new int[]{pid.intValue()});
        item.put("mem", String.format(Locale.ENGLISH, "%.2f M", Double.valueOf(((double) mem[0].getTotalPrivateDirty()) / 1024.0d)));
        if (updateAll) {
            updateAdvanced(pid.intValue(), item);
        }
    }

    private boolean updateAdvanced(int pid, Map<String, Object> item) {
        int cpuUsage;
        if (item == null) {
            return false;
        }
        Long oldSysTime = (Long) item.get("sysTime");
        if (oldSysTime == null) {
            oldSysTime = 0L;
        }
        String procData = readData("/proc/stat");
        if (procData == null) {
            Log.d("ZonerAV", "cannot read /proc/stat");
            return false;
        }
        String[] data = procData.split("  *");
        long sysTime = Globals.toLong(data[1], 0) + Globals.toLong(data[2], 0) + Globals.toLong(data[3], 0) + Globals.toLong(data[4], 0) + Globals.toLong(data[5], 0) + Globals.toLong(data[6], 0) + Globals.toLong(data[7], 0);
        item.put("sysTime", Long.valueOf(sysTime));
        Long oldCpuTime = (Long) item.get("cpuTime");
        if (oldCpuTime == null) {
            oldCpuTime = 0L;
        }
        String procData2 = readData("/proc/" + pid + "/stat");
        if (procData2 == null) {
            Log.d("ZonerAV", "cannot read /proc/" + pid + "/stat");
            return false;
        }
        String[] data2 = procData2.split(" ");
        long cpuTime = Globals.toLong(data2[12], 0) + Globals.toLong(data2[13], 0);
        item.put("cpuTime", Long.valueOf(cpuTime));
        if (oldCpuTime.longValue() == 0 || oldSysTime.longValue() == 0) {
            cpuUsage = 0;
        } else {
            cpuUsage = (int) ((((double) (cpuTime - oldCpuTime.longValue())) / ((double) (sysTime - oldSysTime.longValue()))) * 100.0d);
        }
        item.put("cpu", String.valueOf(cpuUsage) + "%");
        item.put("state", getProcessState(data2[2]));
        long utime = Globals.toLong(data2[12], 0) / 100;
        long stime = Globals.toLong(data2[13], 0) / 100;
        item.put("utime", String.format(Locale.ENGLISH, "%2dh%2dm%2ds", Long.valueOf(utime / 3600), Long.valueOf((utime / 60) % 60), Long.valueOf(utime % 60)));
        item.put("stime", String.format(Locale.ENGLISH, "%2dh%2dm%2ds", Long.valueOf(stime / 3600), Long.valueOf((stime / 60) % 60), Long.valueOf(stime % 60)));
        item.put("threads", data2[19]);
        item.put("vmem", String.format(Locale.ENGLISH, "%.2f M", Double.valueOf(((double) Globals.toLong(data2[22], 0)) / 1048576.0d)));
        return true;
    }

    public boolean updateAdvanced(String pid, Map<String, Object> item) {
        return updateAdvanced(Globals.toInt(pid, 1), item);
    }

    private String getProcessState(String stateChar) {
        if (stateChar.equals("R")) {
            return this.mCtx.getString(R.string.tasklist_state_running);
        }
        if (stateChar.equals("S")) {
            return this.mCtx.getString(R.string.tasklist_state_sleeping);
        }
        if (stateChar.equals("D")) {
            return this.mCtx.getString(R.string.tasklist_state_io);
        }
        if (stateChar.equals("Z")) {
            return this.mCtx.getString(R.string.tasklist_state_zombie);
        }
        if (stateChar.equals("T")) {
            return this.mCtx.getString(R.string.tasklist_state_traced);
        }
        if (stateChar.equals("W")) {
            return this.mCtx.getString(R.string.tasklist_state_paging);
        }
        return this.mCtx.getString(R.string.tasklist_state_unknown);
    }

    private String readData(String filename) {
        try {
            return new BufferedReader(new FileReader(filename), 1024).readLine();
        } catch (Exception e) {
            return null;
        }
    }

    public void sortList() {
        Collections.sort(this.mTaskList, this.mComp);
    }

    public void sortList(SortBy by, Boolean ascending) {
        switch ($SWITCH_TABLE$com$zoner$android$antivirus$ProcInfo$SortBy()[by.ordinal()]) {
            case 2:
                this.mComp = sMemComparator;
                break;
            default:
                this.mComp = sNameComparator;
                break;
        }
        if (!ascending.booleanValue()) {
            this.mComp = Collections.reverseOrder(this.mComp);
        }
        Collections.sort(this.mTaskList, this.mComp);
    }
}
