package com.zoner.android.antivirus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.telephony.PhoneStateListener;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.android.internal.telephony.ITelephony;
import com.google.android.mms.pdu.PduParser;
import com.zoner.android.antivirus.DbPhoneFilter;
import java.lang.reflect.Method;

public class PhoneFilter {
    public static String PHONELOG = "phonelog";
    public static String RESULT = "result";
    public static String TIMESTAMP = "time";
    public static boolean sBlock = false;
    private BroadcastReceiver mCallReceiver = new BroadcastReceiver() {
        private static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$DbPhoneFilter$FilterList$Mode;

        static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$DbPhoneFilter$FilterList$Mode() {
            int[] iArr = $SWITCH_TABLE$com$zoner$android$antivirus$DbPhoneFilter$FilterList$Mode;
            if (iArr == null) {
                iArr = new int[DbPhoneFilter.FilterList.Mode.values().length];
                try {
                    iArr[DbPhoneFilter.FilterList.Mode.Allow.ordinal()] = 2;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[DbPhoneFilter.FilterList.Mode.Ask.ordinal()] = 4;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[DbPhoneFilter.FilterList.Mode.Deny.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[DbPhoneFilter.FilterList.Mode.None.ordinal()] = 1;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[DbPhoneFilter.FilterList.Mode.Once.ordinal()] = 5;
                } catch (NoSuchFieldError e5) {
                }
                $SWITCH_TABLE$com$zoner$android$antivirus$DbPhoneFilter$FilterList$Mode = iArr;
            }
            return iArr;
        }

        public void onReceive(Context context, Intent intent) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(PhoneFilter.this.mContext);
            boolean filtered = prefs.getBoolean(Globals.PREF_BLOCK_ENABLE, false);
            if (filtered || prefs.getBoolean(Globals.PREF_BLOCK_PARENTAL, false)) {
                String original = intent.getStringExtra("android.intent.extra.PHONE_NUMBER");
                String modified = getResultData();
                DbPhoneFilter db = null;
                if (filtered) {
                    db = new DbPhoneFilter(context);
                }
                DbPhoneFilter.FilterResult result = PhoneFilter.this.getFilterResult(prefs, db, original, modified, DbPhoneFilter.Type.Call, DbPhoneFilter.Direction.Outgoing);
                switch ($SWITCH_TABLE$com$zoner$android$antivirus$DbPhoneFilter$FilterList$Mode()[result.modeRes.ordinal()]) {
                    case 2:
                        if (result.phoneNumber != null) {
                            if (filtered) {
                                PhoneFilter.this.phoneLog(result);
                            }
                            PhoneFilter.sBlock = false;
                            return;
                        }
                        return;
                    case DbScanner.ERRFORMAT:
                        if (filtered) {
                            PhoneFilter.this.phoneLog(result);
                        }
                        setResultData(null);
                        return;
                    case 4:
                        Intent i = new Intent(PhoneFilter.this.mContext, ActCallQuery.class);
                        i.setFlags(268697600);
                        i.putExtra(PhoneFilter.RESULT, result);
                        i.putExtra("filtered", db != null);
                        if (prefs.getBoolean(Globals.PREF_BLOCK_PARENTAL, false)) {
                            i.putExtra("passwd", prefs.getString(Globals.PREF_BLOCK_PASSWORD, Globals.DEF_BLOCK_PASSWORD));
                        }
                        PhoneFilter.this.mContext.startActivity(i);
                        setResultData(null);
                        return;
                    default:
                        return;
                }
            } else {
                PhoneFilter.sBlock = false;
            }
        }
    };
    /* access modifiers changed from: private */
    public Context mContext;
    private LogHandler mLogHandler;
    private Looper mLogLooper;
    private BroadcastReceiver mMmsReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(PhoneFilter.this.mContext);
            if (prefs.getBoolean(Globals.PREF_BLOCK_ENABLE, false)) {
                DbPhoneFilter db = new DbPhoneFilter(context);
                try {
                    String phone = new PduParser(intent.getByteArrayExtra("data")).parse().getFrom().getString();
                    DbPhoneFilter.FilterResult result = PhoneFilter.this.getFilterResult(prefs, db, phone, phone, DbPhoneFilter.Type.MMS, DbPhoneFilter.Direction.Incoming);
                    if (result.modeRes == DbPhoneFilter.FilterList.Mode.Deny) {
                        abortBroadcast();
                    } else {
                        result.modeRes = DbPhoneFilter.FilterList.Mode.Allow;
                    }
                    PhoneFilter.this.phoneLog(result);
                } catch (Exception e) {
                }
            }
        }
    };
    private PhoneStateListener mPhoneListener = new PhoneStateListener() {
        public void onCallStateChanged(int state, String phone) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(PhoneFilter.this.mContext);
            switch (state) {
                case 0:
                    PhoneFilter.sBlock = true;
                    return;
                case 1:
                    try {
                        PhoneFilter.sBlock = false;
                        if (prefs.getBoolean(Globals.PREF_BLOCK_ENABLE, false)) {
                            DbPhoneFilter.FilterResult result = PhoneFilter.this.getFilterResult(prefs, new DbPhoneFilter(PhoneFilter.this.mContext), phone, phone, DbPhoneFilter.Type.Call, DbPhoneFilter.Direction.Incoming);
                            if (result.modeRes == DbPhoneFilter.FilterList.Mode.Deny) {
                                PhoneFilter.this.mTelephony.endCall();
                            } else {
                                result.modeRes = DbPhoneFilter.FilterList.Mode.Allow;
                            }
                            PhoneFilter.this.phoneLog(result);
                            return;
                        }
                        return;
                    } catch (Exception e) {
                        Log.d("ZonerAV", "Exception: PhoneStateListener() e = " + e);
                        return;
                    }
                case 2:
                default:
                    return;
            }
        }
    };
    private BroadcastReceiver mSmsReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Object[] messages;
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(PhoneFilter.this.mContext);
            if (prefs.getBoolean(Globals.PREF_BLOCK_ENABLE, false) && (messages = (Object[]) intent.getExtras().get("pdus")) != null) {
                DbPhoneFilter db = new DbPhoneFilter(context);
                DbPhoneFilter.FilterResult result = null;
                String phoneLast = null;
                int n = 0;
                while (true) {
                    if (n >= messages.length) {
                        break;
                    }
                    String phone = SmsMessage.createFromPdu((byte[]) messages[n]).getOriginatingAddress();
                    if (phone != null) {
                        if (!phone.equals(phoneLast)) {
                            result = PhoneFilter.this.getFilterResult(prefs, db, phone, phone, DbPhoneFilter.Type.SMS, DbPhoneFilter.Direction.Incoming);
                            if (result.modeRes != DbPhoneFilter.FilterList.Mode.Deny) {
                                result.modeRes = DbPhoneFilter.FilterList.Mode.Allow;
                                break;
                            }
                        }
                        phoneLast = phone;
                        n++;
                    } else if (result != null) {
                        result.modeRes = DbPhoneFilter.FilterList.Mode.Allow;
                    }
                }
                if (result != null) {
                    if (result.modeRes == DbPhoneFilter.FilterList.Mode.Deny) {
                        abortBroadcast();
                    } else {
                        result.modeRes = DbPhoneFilter.FilterList.Mode.Allow;
                    }
                    PhoneFilter.this.phoneLog(result);
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public ITelephony mTelephony;
    private TelephonyManager tm;

    PhoneFilter(Context context) throws Exception {
        HandlerThread thread = new HandlerThread("PhoneLogger");
        thread.start();
        this.mLogLooper = thread.getLooper();
        this.mLogHandler = new LogHandler(this.mLogLooper);
        IntentFilter filter = new IntentFilter("android.intent.action.NEW_OUTGOING_CALL");
        filter.addCategory("android.intent.category.DEFAULT");
        filter.setPriority(Integer.MIN_VALUE);
        context.registerReceiver(this.mCallReceiver, filter);
        IntentFilter filter2 = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        filter2.setPriority(Integer.MAX_VALUE);
        context.registerReceiver(this.mSmsReceiver, filter2);
        IntentFilter filter3 = new IntentFilter("android.provider.Telephony.WAP_PUSH_RECEIVED");
        filter3.addDataType("application/vnd.wap.mms-message");
        filter3.setPriority(Integer.MAX_VALUE);
        context.registerReceiver(this.mMmsReceiver, filter3);
        this.tm = (TelephonyManager) context.getSystemService(DbPhoneFilter.DbColumns.COLUMN_PHONE);
        this.tm.listen(this.mPhoneListener, 32);
        this.mTelephony = getITelephony(context);
        this.mContext = context;
    }

    public static final ITelephony getITelephony(Context context) throws Exception {
        TelephonyManager tm2 = (TelephonyManager) context.getSystemService(DbPhoneFilter.DbColumns.COLUMN_PHONE);
        Method m = Class.forName(tm2.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
        m.setAccessible(true);
        return (ITelephony) m.invoke(tm2, new Object[0]);
    }

    private final class LogHandler extends Handler {
        public LogHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            DbPhoneFilter db = new DbPhoneFilter(PhoneFilter.this.mContext);
            Bundle data = msg.getData();
            Long timestamp = Long.valueOf(data.getLong(PhoneFilter.TIMESTAMP));
            DbPhoneFilter.FilterResult result = (DbPhoneFilter.FilterResult) data.getParcelable(PhoneFilter.RESULT);
            if (result != null) {
                db.phoneLog(result, timestamp);
            } else {
                db.phoneLog(null, DbPhoneFilter.Type.Call, DbPhoneFilter.Direction.Outgoing, DbPhoneFilter.Logs.Action.Hanged, timestamp);
            }
        }
    }

    public void phoneLog(DbPhoneFilter.FilterResult result, Long timestamp) {
        Message msg = this.mLogHandler.obtainMessage();
        Bundle data = new Bundle();
        data.putLong(TIMESTAMP, timestamp.longValue());
        if (result != null) {
            data.putParcelable(RESULT, result);
        }
        msg.setData(data);
        this.mLogHandler.sendMessage(msg);
    }

    public void phoneLog(DbPhoneFilter.FilterResult result) {
        phoneLog(result, Long.valueOf(System.currentTimeMillis()));
    }

    public void phoneLog(Intent intent) {
        phoneLog((DbPhoneFilter.FilterResult) intent.getParcelableExtra(PHONELOG), Long.valueOf(intent.getLongExtra(TIMESTAMP, System.currentTimeMillis())));
    }

    public static void phoneLogIntent(Intent intent, DbPhoneFilter.FilterResult result) {
        long now = System.currentTimeMillis();
        intent.putExtra(PHONELOG, result);
        intent.putExtra(TIMESTAMP, now);
    }

    private static DbPhoneFilter.FilterList.Mode getModeUnknown(SharedPreferences prefs, DbPhoneFilter.Type type, DbPhoneFilter.Direction direction) {
        String[][] key = {new String[]{Globals.PREF_BLOCK_UNKNOWN_IN, Globals.PREF_BLOCK_UNKNOWN_OUT}, new String[]{Globals.PREF_BLOCK_UNKNOWN_SMS, Globals.DEF_BLOCK_PASSWORD}, new String[]{Globals.PREF_BLOCK_UNKNOWN_SMS, Globals.DEF_BLOCK_PASSWORD}};
        int[][] defValue = {new int[]{Globals.DEF_BLOCK_UNKNOWN_IN, Globals.DEF_BLOCK_UNKNOWN_OUT}, new int[]{Globals.DEF_BLOCK_UNKNOWN_SMS, Globals.DEF_BLOCK_UNKNOWN_SMS}, new int[]{Globals.DEF_BLOCK_UNKNOWN_SMS, Globals.DEF_BLOCK_UNKNOWN_SMS}};
        int typeOrd = type.ordinal();
        int dirOrd = direction.ordinal();
        return DbPhoneFilter.FilterList.Mode.getEnum(prefs.getInt(key[typeOrd][dirOrd], defValue[typeOrd][dirOrd]));
    }

    private static DbPhoneFilter.FilterList.Mode getModeKnown(SharedPreferences prefs, DbPhoneFilter.Type type, DbPhoneFilter.Direction direction) {
        String[][] key = {new String[]{Globals.PREF_BLOCK_KNOWN_IN, Globals.PREF_BLOCK_KNOWN_OUT}, new String[]{Globals.PREF_BLOCK_KNOWN_SMS, Globals.DEF_BLOCK_PASSWORD}, new String[]{Globals.PREF_BLOCK_KNOWN_SMS, Globals.DEF_BLOCK_PASSWORD}};
        int[][] defValue = {new int[]{Globals.DEF_BLOCK_KNOWN_IN, Globals.DEF_BLOCK_KNOWN_OUT}, new int[]{Globals.DEF_BLOCK_KNOWN_SMS, Globals.DEF_BLOCK_KNOWN_SMS}, new int[]{Globals.DEF_BLOCK_KNOWN_SMS, Globals.DEF_BLOCK_KNOWN_SMS}};
        int typeOrd = type.ordinal();
        int dirOrd = direction.ordinal();
        return DbPhoneFilter.FilterList.Mode.getEnum(prefs.getInt(key[typeOrd][dirOrd], defValue[typeOrd][dirOrd]));
    }

    private DbPhoneFilter.FilterResult filterParentalLock(SharedPreferences prefs, DbPhoneFilter.Direction direction, DbPhoneFilter.FilterResult result) {
        if (direction == DbPhoneFilter.Direction.Outgoing && prefs.getBoolean(Globals.PREF_BLOCK_PARENTAL, false) && result.modeRes != DbPhoneFilter.FilterList.Mode.Deny) {
            result.modeRes = DbPhoneFilter.FilterList.Mode.Ask;
        }
        return result;
    }

    /* access modifiers changed from: private */
    public DbPhoneFilter.FilterResult getFilterResult(SharedPreferences prefs, DbPhoneFilter db, String original, String modified, DbPhoneFilter.Type type, DbPhoneFilter.Direction direction) {
        String phone = modified;
        if (phone == null || phone.length() == 0 || PhoneNumberUtils.isEmergencyNumber(phone)) {
            return new DbPhoneFilter.FilterResult(phone, type, direction, DbPhoneFilter.FilterList.Mode.Allow);
        }
        if (db == null) {
            return new DbPhoneFilter.FilterResult(phone, type, direction, DbPhoneFilter.FilterList.Mode.Ask);
        }
        DbPhoneFilter.FilterResult result = db.getFilterResult(phone, type, direction);
        if (result != null && result.modeRes != DbPhoneFilter.FilterList.Mode.None) {
            return filterParentalLock(prefs, direction, result);
        }
        boolean white = prefs.getBoolean(Globals.PREF_BLOCK_WHITELIST, true);
        DbPhoneFilter.FilterList.Mode modeRes = getModeUnknown(prefs, type, direction);
        if (!white || modeRes != DbPhoneFilter.FilterList.Mode.Allow) {
            Cursor phones = this.mContext.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone)), new String[]{"_id"}, null, null, null);
            if (phones != null) {
                if (phones.getCount() != 0) {
                    modeRes = white ? DbPhoneFilter.FilterList.Mode.Allow : getModeKnown(prefs, type, direction);
                }
                phones.close();
            }
            if (result != null) {
                result.modeRes = modeRes;
            } else {
                result = new DbPhoneFilter.FilterResult(phone, type, direction, modeRes);
            }
            return filterParentalLock(prefs, direction, result);
        }
        if (result != null) {
            result.modeRes = modeRes;
        } else {
            result = new DbPhoneFilter.FilterResult(phone, type, direction, modeRes);
        }
        return filterParentalLock(prefs, direction, result);
    }
}
