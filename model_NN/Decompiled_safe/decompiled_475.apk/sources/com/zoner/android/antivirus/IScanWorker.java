package com.zoner.android.antivirus;

import android.content.pm.PackageInfo;
import java.util.List;

public interface IScanWorker {
    boolean scanFinished();

    void scanPackages(List<PackageInfo> list);

    void scanPath(String str, boolean z);

    void scanPause();

    boolean scanPaused();

    void scanResume();

    void scanStop();
}
