package com.zoner.android.antivirus;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import java.io.File;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActFileBrowser extends Activity implements IBinderConnector {
    private static final Comparator<Map<String, Object>> sFileComparator = new Comparator<Map<String, Object>>() {
        private final Collator collator = Collator.getInstance();

        public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            return compare((Map<String, Object>) ((Map) obj), (Map<String, Object>) ((Map) obj2));
        }

        public int compare(Map<String, Object> map1, Map<String, Object> map2) {
            boolean isDir1 = ((Boolean) map1.get("dir")).booleanValue();
            if (isDir1 == ((Boolean) map2.get("dir")).booleanValue()) {
                return this.collator.compare(map1.get("name"), map2.get("name"));
            }
            if (isDir1) {
                return -1;
            }
            return 1;
        }
    };
    /* access modifiers changed from: private */
    public String currentPath;
    /* access modifiers changed from: private */
    public Map<String, Object> fileInfo;
    private ServiceBinder mServiceBinder;
    private IResultWorker mWorker;
    private final String sRoot = "/";

    public void onCreate(Bundle savedInstanceState) {
        String startPath;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.file_browser);
        findViewById(R.id.browser_btn_exit).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActFileBrowser.this.setResult(-1);
                ActFileBrowser.this.finish();
            }
        });
        findViewById(R.id.browser_btn_root).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActFileBrowser.this.showDir("/");
            }
        });
        findViewById(R.id.browser_btn_home).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String extDir = PreferenceManager.getDefaultSharedPreferences(ActFileBrowser.this.getApplicationContext()).getString(Globals.PREF_MISC_HOME, null);
                if (extDir == null) {
                    extDir = Environment.getExternalStorageDirectory().getAbsolutePath();
                }
                ActFileBrowser.this.showDir(extDir);
            }
        });
        findViewById(R.id.browser_btn_scan).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActFileBrowser.this.scanPath(ActFileBrowser.this.currentPath, true);
            }
        });
        ListView browserList = (ListView) findViewById(R.id.browser_list);
        browserList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Map<String, Object> map = (Map) parent.getItemAtPosition(position);
                boolean isDir = ((Boolean) map.get("dir")).booleanValue();
                boolean canRead = ((Boolean) map.get("read")).booleanValue();
                String path = (String) map.get("path");
                if (!canRead) {
                    Toast.makeText(ActFileBrowser.this.getApplicationContext(), ActFileBrowser.this.getString(isDir ? R.string.browser_dir_locked : R.string.browser_file_locked), 0).show();
                } else if (isDir) {
                    ActFileBrowser.this.showDir(path);
                } else {
                    ActFileBrowser.this.scanPath(path, false);
                }
            }
        });
        registerForContextMenu(browserList);
        if (savedInstanceState != null) {
            startPath = savedInstanceState.getString("path");
        } else {
            startPath = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(Globals.PREF_MISC_HOME, null);
            if (startPath == null) {
                startPath = Environment.getExternalStorageDirectory().getAbsolutePath();
            }
        }
        this.currentPath = startPath;
        this.mServiceBinder = new ServiceBinder(this, this, null);
        browserList.setEnabled(false);
    }

    public void onResume() {
        super.onResume();
        showDir(this.currentPath);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.mServiceBinder.doBind();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.mServiceBinder.doUnbind();
        this.mWorker = null;
        super.onStop();
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onBound(ZapService worker) {
        this.mWorker = worker;
        if (!this.mWorker.scanFinished()) {
            Intent intent = new Intent(getApplicationContext(), ActScanResults.class);
            intent.setFlags(131072);
            startActivityForResult(intent, 0);
        } else if (this.mWorker.demandResults().hasResults) {
            Intent intent2 = new Intent(getApplicationContext(), ActScanResults.class);
            intent2.putExtra("userScan", true);
            intent2.setFlags(131072);
            startActivityForResult(intent2, 0);
        }
        ((ListView) findViewById(R.id.browser_list)).setEnabled(true);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 0) {
            finish();
        }
        showDir(this.currentPath);
    }

    public void onBackPressed() {
        String path;
        if (this.currentPath.equals("/")) {
            setResult(-1);
            super.onBackPressed();
            return;
        }
        int idx = this.currentPath.lastIndexOf("/");
        if (idx == 0) {
            path = "/";
        } else {
            path = this.currentPath.substring(0, idx);
        }
        showDir(path);
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("path", this.currentPath);
        super.onSaveInstanceState(savedInstanceState);
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        this.fileInfo = (Map) ((ListView) findViewById(R.id.browser_list)).getItemAtPosition(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
        if (((Boolean) this.fileInfo.get("read")).booleanValue()) {
            getMenuInflater().inflate(R.menu.browse_ctx, menu);
            menu.setHeaderTitle((String) this.fileInfo.get("path"));
            if (!((Boolean) this.fileInfo.get("dir")).booleanValue()) {
                menu.removeItem(R.id.browse_ctx_open);
            }
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.browse_ctx_open /*2131361968*/:
                showDir((String) this.fileInfo.get("path"));
                return true;
            case R.id.browse_ctx_scan /*2131361969*/:
                scanPath((String) this.fileInfo.get("path"), ((Boolean) this.fileInfo.get("dir")).booleanValue());
                return true;
            case R.id.browse_ctx_delete /*2131361970*/:
                final boolean isDir = ((Boolean) this.fileInfo.get("dir")).booleanValue();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(isDir ? R.string.browser_dir_delete : R.string.browser_file_delete)).setPositiveButton(getString(R.string.button_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (!ActFileBrowser.this.deletePath((String) ActFileBrowser.this.fileInfo.get("path"))) {
                            Toast.makeText(ActFileBrowser.this.getApplicationContext(), ActFileBrowser.this.getString(isDir ? R.string.browser_dir_delete_fail : R.string.browser_file_delete_fail), 0).show();
                        } else {
                            Toast.makeText(ActFileBrowser.this.getApplicationContext(), ActFileBrowser.this.getString(isDir ? R.string.browser_dir_delete_success : R.string.browser_file_delete_success), 0).show();
                        }
                        ActFileBrowser.this.showDir(ActFileBrowser.this.currentPath);
                    }
                }).setNegativeButton(getString(R.string.button_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                builder.create().show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filebrowser, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString(Globals.PREF_MISC_HOME, this.currentPath).commit();
        Toast.makeText(this, String.valueOf(getString(R.string.browser_home_set)) + " " + this.currentPath, 0).show();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void scanPath(String path, boolean isDir) {
        File[] files;
        if (Globals.isSystemPath(path) || path.equals("/dev") || path.equals("/proc") || path.equals("/sys")) {
            Toast.makeText(this, (int) R.string.browse_scan_forbidden, 0).show();
        } else if (!isDir || !((files = new File(path).listFiles()) == null || files.length == 0)) {
            Intent intent = new Intent();
            intent.setClass(getApplicationContext(), ActScanResults.class);
            intent.putExtra("pathToScan", path);
            intent.putExtra("isDir", isDir);
            intent.putExtra("userScan", true);
            startActivityForResult(intent, 0);
        } else {
            Toast.makeText(this, getString(R.string.browser_dir_empty), 0).show();
        }
    }

    /* access modifiers changed from: private */
    public boolean deletePath(String path) {
        boolean delete;
        boolean success = true;
        File file = new File(path);
        File[] children = file.listFiles();
        if (children != null) {
            for (File child : children) {
                if (child.isDirectory()) {
                    delete = deletePath(child.getAbsolutePath());
                } else {
                    delete = child.delete();
                }
                success &= delete;
            }
        }
        return success & file.delete();
    }

    /* access modifiers changed from: private */
    public void showDir(String dirPath) {
        setTitle(dirPath);
        List<Map<String, Object>> fileList = new ArrayList<>();
        File[] files = new File(dirPath).listFiles();
        if (files != null) {
            for (File file : files) {
                addItem(fileList, file.getName(), file.getPath(), file.isDirectory(), file.canRead());
            }
            Collections.sort(fileList, sFileComparator);
        }
        ((ListView) findViewById(R.id.browser_list)).setAdapter((ListAdapter) new SimpleAdapter(this, fileList, R.layout.file_browser_row, new String[]{"icon", "name"}, new int[]{R.id.browse_list_icon, R.id.browse_list_name}));
        this.currentPath = dirPath;
    }

    /* access modifiers changed from: protected */
    public void addItem(List<Map<String, Object>> data, String name, String path, boolean isDir, boolean canRead) {
        Map<String, Object> temp = new HashMap<>();
        temp.put("name", name);
        temp.put("path", path);
        temp.put("dir", Boolean.valueOf(isDir));
        temp.put("read", Boolean.valueOf(canRead));
        temp.put("icon", Integer.valueOf(isDir ? canRead ? R.drawable.dir : R.drawable.dir_locked : canRead ? R.drawable.file : R.drawable.file_locked));
        data.add(temp);
    }
}
