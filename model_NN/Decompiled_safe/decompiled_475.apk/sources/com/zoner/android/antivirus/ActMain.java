package com.zoner.android.antivirus;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.zoner.android.antivirus.AlarmUpdate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ActMain extends Activity {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$AlarmUpdate$ReportType = null;
    private static final int DLG_ABOUT = 2;
    private static final int DLG_FIRST_RUN = 1;
    private static final int DLG_NETWORK = 4;
    private static final int DLG_OUTDATED = 3;
    private static final int LIST_ITEM_ANTIVIRUS = 0;
    private static final int LIST_ITEM_FILTER = 3;
    private static final int LIST_ITEM_PERMISSION = 1;
    private static final int LIST_ITEM_TASK = 2;
    private static final int LIST_ITEM_TEST = 5;
    private static final int LIST_ITEM_UPDATE = 4;
    private ArrayList<Map<String, Object>> mItems = new ArrayList<>();
    private boolean mManUpd = false;

    static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$AlarmUpdate$ReportType() {
        int[] iArr = $SWITCH_TABLE$com$zoner$android$antivirus$AlarmUpdate$ReportType;
        if (iArr == null) {
            iArr = new int[AlarmUpdate.ReportType.values().length];
            try {
                iArr[AlarmUpdate.ReportType.ERROR.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AlarmUpdate.ReportType.OUTDATED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AlarmUpdate.ReportType.SUCCESS.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[AlarmUpdate.ReportType.UP2DATE.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$zoner$android$antivirus$AlarmUpdate$ReportType = iArr;
        }
        return iArr;
    }

    private void addItem(int icon, int name, int desc, boolean tel) {
        Map<String, Object> item = new HashMap<>();
        item.put("icon", Integer.valueOf(icon));
        item.put("name", getString(name));
        item.put("desc", getString(desc));
        if (tel && !getPackageManager().hasSystemFeature("android.hardware.telephony")) {
            item.put("disabled", true);
        }
        this.mItems.add(item);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.listmenu);
        Object lastConfig = getLastNonConfigurationInstance();
        if (lastConfig != null) {
            this.mManUpd = ((Boolean) lastConfig).booleanValue();
        }
        ((ImageView) findViewById(R.id.listmenu_image)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent("android.intent.action.SEND");
                i.setType("text/plain");
                i.putExtra("android.intent.extra.SUBJECT", ActMain.this.getString(R.string.share_subject));
                i.putExtra("android.intent.extra.TEXT", ActMain.this.getString(R.string.share_text));
                try {
                    ActMain.this.startActivity(Intent.createChooser(i, ActMain.this.getString(R.string.share_title)));
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(ActMain.this, (int) R.string.share_error, 0).show();
                }
            }
        });
        addItem(R.drawable.menu_av, R.string.main_button_antivirus, R.string.main_desc_antivirus, false);
        addItem(R.drawable.menu_apps, R.string.main_button_rights, R.string.main_desc_rights, false);
        addItem(R.drawable.menu_tasks, R.string.main_button_tasks, R.string.main_desc_tasks, false);
        addItem(R.drawable.menu_filter, R.string.main_button_calls, R.string.main_desc_calls, true);
        addItem(R.drawable.menu_update, R.string.main_button_update, R.string.main_desc_update, false);
        ListView lv = (ListView) findViewById(R.id.listmenu_list);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                switch (position) {
                    case 0:
                        ActMain.this.startActivity(new Intent(ActMain.this.getApplicationContext(), ActMalware.class));
                        return;
                    case 1:
                        ActMain.this.startActivity(new Intent(ActMain.this.getApplicationContext(), ActAppList.class));
                        return;
                    case 2:
                        ActMain.this.startActivity(new Intent(ActMain.this.getApplicationContext(), ActTaskList.class));
                        return;
                    case DbScanner.ERRFORMAT /*3*/:
                        ActMain.this.startActivity(new Intent(ActMain.this.getApplicationContext(), ActCallsTabs.class));
                        return;
                    case 4:
                        ActMain.this.update();
                        return;
                    case ActMain.LIST_ITEM_TEST /*5*/:
                        ActMain.this.testFunc();
                        return;
                    default:
                        return;
                }
            }
        });
        lv.setAdapter((ListAdapter) new MenuAdapter(this, this.mItems, R.layout.listmenu_row, new String[]{"icon", "name", "desc"}, new int[]{R.id.listmenu_row_icon, R.id.listmenu_row_name, R.id.listmenu_row_desc}));
        startService(new Intent(this, ZapService.class));
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (!sharedPreferences.getBoolean("firstRun", false)) {
            sharedPreferences.edit().putBoolean("firstRun", true).commit();
            showDialog(1);
        }
    }

    public Object onRetainNonConfigurationInstance() {
        return Boolean.valueOf(this.mManUpd);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        boolean z;
        Globals.gGUIMain = true;
        if (Globals.gUpdating) {
            z = false;
        } else {
            z = true;
        }
        updateEnable(z);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Globals.gGUIMain = false;
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        AlarmUpdate.ReportType type = (AlarmUpdate.ReportType) intent.getSerializableExtra("update");
        if (type != null && this.mManUpd) {
            switch ($SWITCH_TABLE$com$zoner$android$antivirus$AlarmUpdate$ReportType()[type.ordinal()]) {
                case 1:
                    Toast.makeText(this, getString(R.string.log_update_error), 0).show();
                    break;
                case 2:
                    showDialog(3);
                    break;
                case DbScanner.ERRFORMAT /*3*/:
                    Toast.makeText(this, getString(R.string.log_update_up2date), 0).show();
                    break;
                case 4:
                    Toast.makeText(this, getString(R.string.log_update_success), 0).show();
                    break;
            }
            updateEnable(true);
        }
    }

    /* access modifiers changed from: private */
    public void update() {
        NetworkInfo ni = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (ni == null || !ni.isConnected() || ni.getState() != NetworkInfo.State.CONNECTED) {
            showDialog(4);
            return;
        }
        updateEnable(false);
        this.mManUpd = true;
        Toast.makeText(this, getString(R.string.main_update_start), 0).show();
        ((AlarmManager) getSystemService("alarm")).set(2, SystemClock.elapsedRealtime(), PendingIntent.getBroadcast(this, 0, new Intent(this, AlarmUpdate.class), 134217728));
    }

    private void updateEnable(boolean enable) {
        SimpleAdapter adapter = (SimpleAdapter) ((ListView) findViewById(R.id.listmenu_list)).getAdapter();
        Map<String, Object> item = (Map) adapter.getItem(4);
        if (!enable) {
            item.put("disabled", true);
            item.put("desc", getString(R.string.main_desc_update_ongoing));
        } else {
            this.mManUpd = false;
            item.remove("disabled");
            Long lastUpdate = Long.valueOf(PreferenceManager.getDefaultSharedPreferences(this).getLong(Globals.PREF_UPDATE_LAST, 0));
            Locale locale = Locale.ENGLISH;
            String string = getString(R.string.main_desc_update);
            Object[] objArr = new Object[2];
            objArr[0] = Integer.valueOf(DbScanner.lastID);
            objArr[1] = lastUpdate.longValue() != 0 ? DateFormat.getDateFormat(this).format(new Date(lastUpdate.longValue())) : getString(R.string.main_version_unknown);
            item.put("desc", String.format(locale, string, objArr));
        }
        adapter.notifyDataSetChanged();
    }

    class MenuAdapter extends SimpleAdapter {
        MenuAdapter(Context context, List<Map<String, Object>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = super.getView(position, convertView, parent);
            Boolean disabled = (Boolean) ((Map) getItem(position)).get("disabled");
            boolean enabled = disabled == null || !disabled.booleanValue();
            row.findViewById(R.id.listmenu_row_name).setEnabled(enabled);
            row.findViewById(R.id.listmenu_row_desc).setEnabled(enabled);
            if (position == 3 && !enabled) {
                ((TextView) row.findViewById(R.id.listmenu_row_desc)).setText((int) R.string.main_desc_calls_disabled);
            }
            return row;
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public boolean isEnabled(int position) {
            Boolean disabled = (Boolean) ((Map) getItem(position)).get("disabled");
            return disabled == null || !disabled.booleanValue();
        }
    }

    /* access modifiers changed from: private */
    public void testFunc() {
        ((AlarmManager) getSystemService("alarm")).set(2, SystemClock.elapsedRealtime() + 1000, PendingIntent.getBroadcast(this, 0, new Intent(this, AlarmLiveThreat.class), 134217728));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_about /*2131361978*/:
                showDialog(2);
                return true;
            case R.id.main_log /*2131361979*/:
                startActivity(new Intent(getBaseContext(), ActLog.class));
                return true;
            case R.id.main_settings /*2131361980*/:
                startActivity(new Intent(getBaseContext(), Preferences.class));
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        String version;
        LayoutInflater inflater = (LayoutInflater) getSystemService("layout_inflater");
        Dialog dlg = new Dialog(this);
        switch (id) {
            case 1:
                final View layout = inflater.inflate((int) R.layout.first_run, (ViewGroup) findViewById(R.id.first_layout));
                dlg.requestWindowFeature(3);
                dlg.setContentView(layout);
                dlg.setFeatureDrawableResource(3, R.drawable.launcher);
                dlg.setTitle((int) R.string.first_title);
                ((Button) layout.findViewById(R.id.first_btn_ok)).setOnClickListener(new View.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                     arg types: [java.lang.String, int]
                     candidates:
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                    public void onClick(View v) {
                        CheckBox chk_block = (CheckBox) layout.findViewById(R.id.first_chk_block);
                        CheckBox chk_lt = (CheckBox) layout.findViewById(R.id.first_chk_lt);
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(ActMain.this.getApplicationContext());
                        if (((CheckBox) layout.findViewById(R.id.first_chk_scan)).isChecked()) {
                            Intent startScan = new Intent(ActMain.this.getBaseContext(), ActScanResults.class);
                            startScan.putExtra("scanPackages", true);
                            ActMain.this.startActivity(startScan);
                        }
                        if (chk_block.isChecked()) {
                            sharedPreferences.edit().putBoolean(Globals.PREF_BLOCK_ENABLE, true).commit();
                        }
                        if (!chk_lt.isChecked()) {
                            sharedPreferences.edit().putBoolean(Globals.PREF_MISC_LIVETHREAT, false).commit();
                        }
                        ActMain.this.dismissDialog(1);
                    }
                });
                return dlg;
            case 2:
                View aboutLayout = inflater.inflate((int) R.layout.main_about, (ViewGroup) findViewById(R.id.main_about_layout));
                String version2 = String.valueOf(getString(R.string.main_about_version)) + " ";
                try {
                    version = String.valueOf(version2) + getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    version = String.valueOf(version2) + getString(R.string.main_about_version_unknown);
                }
                ((TextView) aboutLayout.findViewById(R.id.main_about_version)).setText(version);
                SpannableString s = new SpannableString(getText(R.string.main_about_www));
                Linkify.addLinks(s, 1);
                TextView www = (TextView) aboutLayout.findViewById(R.id.main_about_www);
                www.setText(s);
                www.setMovementMethod(LinkMovementMethod.getInstance());
                Dialog dlg2 = new Dialog(this);
                dlg2.requestWindowFeature(3);
                dlg2.setContentView(aboutLayout);
                dlg2.setFeatureDrawableResource(3, R.drawable.launcher);
                dlg2.setTitle(getString(R.string.main_opts_about));
                ((Button) aboutLayout.findViewById(R.id.main_about_ok)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ActMain.this.dismissDialog(2);
                    }
                });
                return dlg2;
            case DbScanner.ERRFORMAT /*3*/:
                return new AlertDialog.Builder(this).setMessage((int) R.string.main_update_outdated).setPositiveButton((int) R.string.button_ok, (DialogInterface.OnClickListener) null).setNegativeButton((int) R.string.main_market_open, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            ActMain.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + ActMain.this.getPackageName())));
                        } catch (Exception e) {
                            Toast.makeText(ActMain.this, ActMain.this.getString(R.string.main_market_open_failed), 0).show();
                        }
                    }
                }).create();
            case 4:
                return new AlertDialog.Builder(this).setMessage((int) R.string.main_update_network).setPositiveButton((int) R.string.button_ok, (DialogInterface.OnClickListener) null).setNegativeButton((int) R.string.main_network_settings, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            ActMain.this.startActivity(new Intent("android.settings.WIRELESS_SETTINGS", (Uri) null));
                        } catch (Exception e) {
                            Toast.makeText(ActMain.this, ActMain.this.getString(R.string.main_network_settings_failed), 0).show();
                        }
                    }
                }).create();
            default:
                return dlg;
        }
    }
}
