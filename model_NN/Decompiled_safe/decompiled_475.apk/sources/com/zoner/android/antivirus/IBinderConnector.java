package com.zoner.android.antivirus;

public interface IBinderConnector {
    void onBound(ZapService zapService);
}
