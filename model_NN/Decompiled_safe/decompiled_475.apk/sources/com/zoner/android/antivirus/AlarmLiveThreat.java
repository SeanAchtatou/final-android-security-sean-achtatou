package com.zoner.android.antivirus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.zoner.android.antivirus.DbPhoneFilter;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class AlarmLiveThreat extends BroadcastReceiver {
    static final HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    private Context mContext;
    private ConnectivityManager mNet;
    private TelephonyManager mPhone;

    public void onReceive(Context context, Intent intent) {
        this.mContext = context;
        this.mPhone = (TelephonyManager) this.mContext.getSystemService(DbPhoneFilter.DbColumns.COLUMN_PHONE);
        this.mNet = (ConnectivityManager) this.mContext.getSystemService("connectivity");
        NetworkInfo ni = this.mNet.getActiveNetworkInfo();
        if (ni != null && ni.isConnected() && ni.getState() == NetworkInfo.State.CONNECTED) {
            new Thread() {
                public void run() {
                    AlarmLiveThreat.this.sendData();
                }
            }.start();
        }
    }

    public void sendData() {
        DbPhoneFilter db = new DbPhoneFilter(this.mContext);
        String callCount = db.getSuspiciousCallCount();
        boolean infections = haveInfections();
        if (callCount != null || infections) {
            try {
                URL url = new URL("https://livethreat.zonerantivirus.com/livethreat.php");
                trustAllHosts();
                HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                https.setRequestMethod("POST");
                https.setDoInput(true);
                https.setDoOutput(true);
                https.connect();
                DataOutputStream out = new DataOutputStream(https.getOutputStream());
                writeImei(out);
                writeCalls(out, callCount);
                if (infections) {
                    writeInfections(out);
                }
                writeVersion(out);
                out.flush();
                out.close();
                https.getInputStream();
                db.resetSuspiciousCallCount();
                this.mContext.deleteFile("livethreat.log");
            } catch (Exception e) {
                Log.d("ZonerAV", "LT: communication exception " + e.getMessage());
            }
        }
    }

    private boolean haveInfections() {
        try {
            this.mContext.openFileInput("livethreat.log").close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private String getIMEI() {
        String imei = this.mPhone.getDeviceId();
        if (imei != null) {
            return imei;
        }
        String imei2 = Settings.Secure.getString(this.mContext.getContentResolver(), "android_id");
        if (imei2 == null) {
            return "FFFFFFFFFFFFFFF";
        }
        return imei2;
    }

    private void writeImei(DataOutputStream out) throws IOException {
        out.writeBytes("imei=" + getIMEI() + "\n");
    }

    private void writeVersion(DataOutputStream out) throws IOException {
        String version;
        try {
            version = this.mContext.getPackageManager().getPackageInfo(this.mContext.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            version = "unknown";
        }
        out.writeBytes("version=" + version + "\n");
        out.writeBytes("dbver=" + DbScanner.lastID + "\n");
    }

    private void writeCalls(DataOutputStream out, String calls) throws IOException {
        out.writeBytes("suspcalls=" + calls + "\n");
        if (calls != null) {
            out.writeBytes("callpkgs=" + getCallPackages() + "\n");
        }
    }

    private String getCallPackages() {
        String[] permissions;
        String strList = new String();
        List<PackageInfo> list = this.mContext.getPackageManager().getInstalledPackages(4096);
        if (list == null) {
            return strList;
        }
        for (PackageInfo info : list) {
            if ((info.applicationInfo.flags & 1) == 0 && (permissions = info.requestedPermissions) != null) {
                int length = permissions.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (permissions[i].equals("android.permission.CALL_PHONE")) {
                        strList = String.valueOf(strList) + info.packageName + ";";
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
        return strList;
    }

    private void writeInfections(DataOutputStream out) throws IOException {
        FileInputStream fis = this.mContext.openFileInput("livethreat.log");
        byte[] buffer = new byte[1024];
        while (true) {
            int size = fis.read(buffer);
            if (size == -1) {
                fis.close();
                return;
            }
            out.write(buffer, 0, size);
        }
    }

    private static void trustAllHosts() {
        TrustManager[] trustAllCerts = {new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }
        }};
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
