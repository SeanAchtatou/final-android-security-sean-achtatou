package com.zoner.android.antivirus;

import android.content.Context;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;
import com.zoner.android.antivirus.ScanResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class Logger {
    public static final char CLEAN = 'C';
    public static final char DEBUG = 'D';
    public static final char ERROR = 'E';
    public static final String FILE_CLEAN = "FC";
    public static final String FILE_INFECTED = "FI";
    public static final char INFO = 'I';
    public static final String PACKAGE_CLEAN = "PC";
    public static final String PACKAGE_INFECTED = "PI";
    public static final char THREAT = 'T';
    public static final String UPDATE_ERROR = "UE";
    public static final String UPDATE_OUTDATED = "UO";
    public static final String UPDATE_SUCCESS = "US";
    public static final String UPDATE_UP2DATE = "U2";
    public static final char WARNING = 'W';
    private final Context mContext;
    private final File mLogFile;

    public static String getPath(String packageName) {
        return Environment.getExternalStorageDirectory() + "/Android/data/" + packageName + "/log.txt";
    }

    public static void deleteFile(String packageName) {
        try {
            new File(getPath(packageName)).delete();
        } catch (Exception e) {
            Log.d("ZonerAV", "Log: cannot delete file");
        }
    }

    Logger(Context context) {
        this.mContext = context;
        this.mLogFile = new File(getPath(context.getPackageName()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    private void writeMsg(char type, String category, String message) throws IOException {
        Date d = new Date(System.currentTimeMillis());
        FileWriter writer = new FileWriter(this.mLogFile, true);
        writer.write(String.valueOf(type) + "\t" + DateFormat.getDateFormat(this.mContext).format(d) + " " + DateFormat.getTimeFormat(this.mContext).format(d) + "\t" + category + "\t" + message + "\n");
        writer.close();
    }

    public void msg(char type, String category, String message) {
        try {
            writeMsg(type, category, message);
        } catch (IOException e) {
            try {
                this.mLogFile.getParentFile().mkdirs();
                writeMsg(type, category, message);
            } catch (IOException e2) {
                Log.d("ZonerAV", "Log: IO write exception");
            }
        }
    }

    public void logInstall(ScanResult result) {
        if (result.result != ScanResult.Result.CLEAN) {
            msg(THREAT, PACKAGE_INFECTED, getInfections(result));
        } else {
            msg(CLEAN, PACKAGE_CLEAN, result.path);
        }
    }

    public void logAccess(ScanResult result) {
        if (result.result != ScanResult.Result.CLEAN) {
            msg(THREAT, FILE_INFECTED, getInfections(result));
        }
    }

    public void logDemand(ScanResult result) {
        if (result.result != ScanResult.Result.CLEAN) {
            msg(THREAT, result.type == 2 ? PACKAGE_INFECTED : FILE_INFECTED, getInfections(result));
        }
    }

    public static String getInfections(ScanResult result) {
        String message = new String();
        int count = result.getVirusCount();
        for (int i = 0; i < count; i++) {
            if (i != 0) {
                message = String.valueOf(message) + ", ";
            }
            message = String.valueOf(message) + result.getVirusName(i);
        }
        return String.valueOf(message) + " - " + result.path;
    }
}
