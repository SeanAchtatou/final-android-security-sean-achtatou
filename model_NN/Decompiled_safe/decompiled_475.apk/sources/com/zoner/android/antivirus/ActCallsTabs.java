package com.zoner.android.antivirus;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TabHost;

public class ActCallsTabs extends TabActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.calls_tabs);
        Resources res = getResources();
        TabHost tabHost = getTabHost();
        tabHost.addTab(tabHost.newTabSpec(getString(R.string.calls_filter)).setIndicator(getString(R.string.calls_filter), res.getDrawable(R.drawable.calls_filter)).setContent(new Intent(this, ActCallsFilter.class)));
        tabHost.addTab(tabHost.newTabSpec(getString(R.string.calls_log)).setIndicator(getString(R.string.calls_log), res.getDrawable(R.drawable.calls_log)).setContent(new Intent(this, ActCallsLog.class)));
        tabHost.addTab(tabHost.newTabSpec(getString(R.string.calls_settings)).setIndicator(getString(R.string.calls_settings), res.getDrawable(R.drawable.calls_settings)).setContent(new Intent(this, ActCallsSettings.class)));
        tabHost.setCurrentTab(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Globals.PREF_BLOCK_ENABLE, false) ? 0 : 2);
    }
}
