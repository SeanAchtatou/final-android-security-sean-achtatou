package com.zoner.android.antivirus;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.zoner.android.antivirus.ResultStorage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActScanResults extends Activity implements IScanConnector {
    /* access modifiers changed from: private */
    public AnimationDrawable mAnim;
    private TextView mCurrent = null;
    private String mCurrentPath;
    private IResultWorker mResultWorker;
    private List<Map<String, Object>> mResults;
    private ScanBinder mScanBinder;
    /* access modifiers changed from: private */
    public IScanWorker mScanWorker;
    private CheckBox mSelectAll;

    class AnimStarter implements Runnable {
        AnimStarter() {
        }

        public void run() {
            if (ActScanResults.this.mScanWorker == null || !ActScanResults.this.mScanWorker.scanPaused()) {
                ActScanResults.this.mAnim.start();
            } else {
                ActScanResults.this.mAnim.stop();
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(3);
        if (savedInstanceState != null) {
            this.mCurrentPath = (String) savedInstanceState.getCharSequence("currPath");
        }
        this.mScanBinder = new ScanBinder(this, this);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        if (this.mCurrent != null) {
            outState.putCharSequence("currPath", this.mCurrent.getText());
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.mScanBinder.doBind();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.mScanBinder.doUnbind();
        this.mResultWorker = null;
        super.onStop();
    }

    public void onBound(ZapService worker) {
        this.mScanWorker = worker;
        this.mResultWorker = worker;
        if (this.mScanWorker.scanPaused()) {
            guiProgress();
            findViewById(R.id.scanprogress_pause).setVisibility(8);
            findViewById(R.id.scanprogress_resume).setVisibility(0);
            setTitle((int) R.string.title_scan_paused);
        } else if (!this.mResultWorker.scanFinished()) {
            guiProgress();
            this.mCurrent.setText(this.mCurrentPath);
        } else {
            Intent intent = getIntent();
            String path = intent.getStringExtra("pathToScan");
            if (intent.getBooleanExtra("userScan", false) ? this.mResultWorker.hasDemandResults() : this.mResultWorker.hasResults()) {
                guiResults();
                showResults();
            } else if (intent.getBooleanExtra("scanPackages", false)) {
                guiProgress();
                intent.removeExtra("scanPackages");
                List<PackageInfo> list = getPackageManager().getInstalledPackages(0);
                if (list != null) {
                    this.mScanWorker.scanPackages(list);
                }
            } else if (path != null) {
                guiProgress();
                intent.removeExtra("pathToScan");
                this.mScanWorker.scanPath(path, Boolean.valueOf(intent.getBooleanExtra("isDir", false)).booleanValue());
            } else {
                setResult(-1);
                finish();
            }
        }
    }

    private void guiProgress() {
        setContentView((int) R.layout.scan_progress);
        this.mCurrent = (TextView) findViewById(R.id.scanprogress_current);
        setTitle((int) R.string.title_scan_scanning);
        this.mAnim = (AnimationDrawable) getResources().getDrawable(R.anim.scan);
        setFeatureDrawable(3, this.mAnim);
        this.mCurrent.post(new AnimStarter());
        findViewById(R.id.scanprogress_pause).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ActScanResults.this.mScanWorker != null) {
                    ActScanResults.this.pauseScan();
                }
            }
        });
        findViewById(R.id.scanprogress_resume).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ActScanResults.this.mScanWorker != null) {
                    ActScanResults.this.resumeScan();
                }
            }
        });
        findViewById(R.id.scanprogress_stop).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ActScanResults.this.mScanWorker != null) {
                    ActScanResults.this.stopScan();
                }
            }
        });
    }

    private void guiResults() {
    }

    /* access modifiers changed from: package-private */
    public void pauseScan() {
        this.mScanWorker.scanPause();
        findViewById(R.id.scanprogress_pause).setVisibility(8);
        findViewById(R.id.scanprogress_resume).setVisibility(0);
        setTitle((int) R.string.title_scan_paused);
        this.mAnim.stop();
    }

    /* access modifiers changed from: package-private */
    public void resumeScan() {
        this.mScanWorker.scanResume();
        findViewById(R.id.scanprogress_pause).setVisibility(0);
        findViewById(R.id.scanprogress_resume).setVisibility(8);
        setTitle((int) R.string.title_scan_scanning);
        this.mAnim.start();
    }

    /* access modifiers changed from: package-private */
    public void stopScan() {
        this.mScanWorker.scanStop();
    }

    private void showResults() {
        ResultStorage.Storage results = getScanResults();
        if (!results.hasResults) {
            resultsResolved();
            finish();
        } else if (results.results.isEmpty()) {
            resultsResolved();
            setTitle((int) R.string.app_name);
            getWindow().setFeatureDrawableResource(3, R.drawable.scan_clean);
            setContentView((int) R.layout.scan_clean);
            findViewById(R.id.scanclean_ok).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ActScanResults.this.finish();
                }
            });
        } else {
            setTitle((int) R.string.app_name);
            getWindow().setFeatureDrawableResource(3, R.drawable.scan_threats);
            setContentView((int) R.layout.scan_infected);
            ListView listView = (ListView) findViewById(R.id.scaninfected_list);
            listView.setItemsCanFocus(false);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                /* Debug info: failed to restart local var, previous not found, register: 10 */
                public void onItemClick(AdapterView<?> l, View v, int position, long id) {
                    boolean z;
                    ListView listView = (ListView) l;
                    int count = l.getCount();
                    if (count == 1) {
                        listView.setItemChecked(position, true);
                    }
                    boolean consistent = true;
                    boolean checked = listView.isItemChecked(0);
                    if (checked) {
                        int i = 0;
                        while (true) {
                            if (i >= count) {
                                break;
                            } else if (listView.isItemChecked(i) != checked) {
                                consistent = false;
                                break;
                            } else {
                                i++;
                            }
                        }
                    }
                    CheckBox checkBox = (CheckBox) ActScanResults.this.findViewById(R.id.scaninfected_selectall);
                    if (!consistent || !checked) {
                        z = false;
                    } else {
                        z = true;
                    }
                    checkBox.setChecked(z);
                }
            });
            showFormattedResults(results);
            ((TextView) findViewById(R.id.scaninfected_lbl)).setText(getResources().getQuantityString(R.plurals.scaninfected_results, this.mResults.size(), Integer.valueOf(this.mResults.size())));
            ((Button) findViewById(R.id.scaninfected_remove)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ActScanResults.this.removeSelected();
                }
            });
            ((Button) findViewById(R.id.scaninfected_ignore)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ActScanResults.this.ignoreSelected();
                }
            });
            this.mSelectAll = (CheckBox) findViewById(R.id.scaninfected_selectall);
            this.mSelectAll.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ActScanResults.this.selectAll();
                }
            });
        }
    }

    private ResultStorage.Storage getScanResults() {
        if (getIntent().getBooleanExtra("userScan", false)) {
            return this.mResultWorker.demandResults();
        }
        ResultStorage.Storage result = this.mResultWorker.installResults();
        addResults(result, this.mResultWorker.accessResults());
        addResults(result, this.mResultWorker.demandResults());
        return result;
    }

    private void resultsResolved() {
        if (getIntent().getBooleanExtra("userScan", false)) {
            this.mResultWorker.demandResolved();
        } else {
            this.mResultWorker.allResolved();
        }
        setResult(-1);
    }

    private void addResults(ResultStorage.Storage s1, ResultStorage.Storage s2) {
        for (ScanResult cur : s2.results) {
            if (s1.results.contains(cur)) {
                this.mResultWorker.resolve(cur);
            } else {
                s1.results.add(cur);
            }
        }
        s1.hasResults |= s2.hasResults;
    }

    private void showFormattedResults(ResultStorage.Storage results) {
        SimpleAdapter adapter;
        this.mResults = getFormattedResults(results);
        ListView listView = (ListView) findViewById(R.id.scaninfected_list);
        if (this.mResults.size() < 2) {
            adapter = new SimpleAdapter(this, this.mResults, R.layout.scaninfected_single_row, new String[]{"virus", "path"}, new int[]{R.id.scaninfected_single_row_virus, R.id.scaninfected_single_row_path});
        } else {
            adapter = new SimpleAdapter(this, this.mResults, R.layout.scaninfected_multi_row, new String[]{"virus", "path"}, new int[]{R.id.scaninfected_multi_row_virus, R.id.scaninfected_multi_row_path});
        }
        listView.setAdapter((ListAdapter) adapter);
        if (adapter.getCount() < 2) {
            findViewById(R.id.scaninfected_selectall).setVisibility(8);
            findViewById(R.id.scaninfected_selectall_lbl).setVisibility(8);
        }
        selectAll();
    }

    private List<Map<String, Object>> getFormattedResults(ResultStorage.Storage results) {
        List<Map<String, Object>> formattedResults = new ArrayList<>();
        for (ScanResult formatScanResult : results.results) {
            formattedResults.add(formatScanResult(formatScanResult));
        }
        return formattedResults;
    }

    /* access modifiers changed from: package-private */
    public Map<String, Object> formatScanResult(ScanResult result) {
        HashMap<String, Object> item = new HashMap<>();
        String virusName = Globals.DEF_BLOCK_PASSWORD;
        for (int i = 0; i < result.getVirusCount(); i++) {
            virusName = String.valueOf(virusName) + ", " + result.getVirusName(i);
        }
        if (virusName.length() < 3) {
            item.put("virus", virusName);
        } else {
            item.put("virus", virusName.substring(2));
        }
        item.put("path", result.path);
        item.put("obj_result", result);
        return item;
    }

    /* access modifiers changed from: package-private */
    public void removeSelected() {
        ListView listView = (ListView) findViewById(R.id.scaninfected_list);
        SimpleAdapter adapter = (SimpleAdapter) listView.getAdapter();
        SparseBooleanArray checkedItems = listView.getCheckedItemPositions();
        if (checkedItems != null) {
            boolean failed = false;
            boolean files = false;
            boolean checked = false;
            for (int i = checkedItems.size() - 1; i >= 0; i--) {
                int pos = checkedItems.keyAt(i);
                if (checkedItems.valueAt(i) && pos < this.mResults.size()) {
                    checked = true;
                    ScanResult result = getResult(adapter, pos);
                    if (result.type != 2) {
                        files = true;
                    }
                    if (!resolveItem(result, pos, true)) {
                        failed = true;
                    } else {
                        listView.setItemChecked(pos, false);
                    }
                }
            }
            if (!checked) {
                Toast.makeText(this, getString(R.string.scaninfected_nothing_selected), 0).show();
            } else if (failed) {
                Toast.makeText(this, getString(R.string.scaninfected_delete_failed), 0).show();
                updateList(adapter);
            } else {
                if (files) {
                    Toast.makeText(this, getString(R.string.scaninfected_delete_successful), 0).show();
                }
                if (this.mResults.size() == 0) {
                    resultsResolved();
                    finish();
                    return;
                }
                updateList(adapter);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ignoreSelected() {
        ListView listView = (ListView) findViewById(R.id.scaninfected_list);
        SimpleAdapter adapter = (SimpleAdapter) listView.getAdapter();
        SparseBooleanArray checkedItems = listView.getCheckedItemPositions();
        if (checkedItems != null) {
            boolean checked = false;
            for (int i = checkedItems.size() - 1; i >= 0; i--) {
                int pos = checkedItems.keyAt(i);
                if (checkedItems.valueAt(i) && pos < this.mResults.size()) {
                    checked = true;
                    if (resolveItem(getResult(adapter, pos), pos, false)) {
                        listView.setItemChecked(pos, false);
                    }
                }
            }
            if (!checked) {
                Toast.makeText(this, getString(R.string.scaninfected_nothing_selected), 0).show();
            } else if (this.mResults.size() == 0) {
                resultsResolved();
                finish();
            } else {
                updateList(adapter);
            }
        }
    }

    private ScanResult getResult(SimpleAdapter adapter, int pos) {
        return (ScanResult) ((Map) adapter.getItem(pos)).get("obj_result");
    }

    private boolean resolveItem(ScanResult result, int pos, boolean remove) {
        if (this.mResultWorker == null) {
            return false;
        }
        if (remove) {
            if (result.type == 2) {
                if (!packageUninstall(result.path)) {
                    return false;
                }
            } else if (!fileDelete(result.path)) {
                return false;
            }
        }
        this.mResultWorker.resolve(result);
        this.mResults.remove(pos);
        return true;
    }

    private boolean fileDelete(String path) {
        File f = new File(path);
        return f.delete() || !f.exists();
    }

    private boolean packageUninstall(String packageName) {
        startActivity(new Intent("android.intent.action.DELETE", Uri.parse("package:" + packageName)));
        return true;
    }

    /* access modifiers changed from: package-private */
    public void selectAll() {
        boolean check = ((CheckBox) findViewById(R.id.scaninfected_selectall)).isChecked();
        ListView listView = (ListView) findViewById(R.id.scaninfected_list);
        int count = listView.getCount();
        for (int i = 0; i < count; i++) {
            listView.setItemChecked(i, check);
        }
    }

    /* access modifiers changed from: package-private */
    public void updateList(SimpleAdapter adapter) {
        adapter.notifyDataSetChanged();
        ((TextView) findViewById(R.id.scaninfected_lbl)).setText(getResources().getQuantityString(R.plurals.scaninfected_results, this.mResults.size(), Integer.valueOf(this.mResults.size())));
    }

    public void onScanFinish() {
        guiResults();
        showResults();
    }

    public void onScanResult(ScanResult result) {
        if (this.mCurrent != null) {
            this.mCurrent.setText(result.path);
        }
    }
}
