package com.zoner.android.antivirus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.zoner.android.antivirus.DbPhoneFilter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class AlarmUpdate extends BroadcastReceiver {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$AlarmUpdate$ReportType;
    static final HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    private Context mContext;
    private Logger mLog;
    private ConnectivityManager mNet;
    private TelephonyManager mPhone;

    public enum ReportType {
        ERROR,
        OUTDATED,
        UP2DATE,
        SUCCESS
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$AlarmUpdate$ReportType() {
        int[] iArr = $SWITCH_TABLE$com$zoner$android$antivirus$AlarmUpdate$ReportType;
        if (iArr == null) {
            iArr = new int[ReportType.values().length];
            try {
                iArr[ReportType.ERROR.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ReportType.OUTDATED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ReportType.SUCCESS.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ReportType.UP2DATE.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$zoner$android$antivirus$AlarmUpdate$ReportType = iArr;
        }
        return iArr;
    }

    public void onReceive(Context context, Intent intent) {
        this.mContext = context;
        this.mPhone = (TelephonyManager) this.mContext.getSystemService(DbPhoneFilter.DbColumns.COLUMN_PHONE);
        this.mNet = (ConnectivityManager) this.mContext.getSystemService("connectivity");
        NetworkInfo ni = this.mNet.getActiveNetworkInfo();
        if (ni != null && ni.isConnected() && ni.getState() == NetworkInfo.State.CONNECTED) {
            Globals.gUpdating = true;
            new Thread() {
                public void run() {
                    AlarmUpdate.this.updateDB();
                }
            }.start();
        }
    }

    public void updateDB() {
        this.mLog = new Logger(this.mContext);
        try {
            URL url = new URL("https://jahudka.zoner.com/update/android.cgi");
            trustAllHosts();
            System.setProperty("http.keepAlive", "false");
            HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
            https.setHostnameVerifier(DO_NOT_VERIFY);
            https.setRequestMethod("GET");
            https.setDoInput(true);
            https.setDoOutput(false);
            https.setRequestProperty("User-Agent", "Zoner AntiVirus for Android");
            https.setRequestProperty("ZAV-DBVer", "1");
            https.setRequestProperty("ZAV-DBLast", new StringBuilder().append(DbScanner.lastID).toString());
            https.setRequestProperty("ZAV-IMEI", getIMEI());
            https.connect();
            if (!downloadDB(https)) {
                https.disconnect();
                this.mContext.deleteFile("zavdb.new");
                return;
            }
            https.disconnect();
            File dir = this.mContext.getFilesDir();
            new File(dir, "zavdb.new").renameTo(new File(dir, "zavdb.and"));
        } catch (Exception e) {
            Exception e2 = e;
            reportUpdate(this.mLog, ReportType.ERROR, this.mContext.getString(R.string.log_update_error_communication));
            Log.d("ZonerAV", "Update: communication exception " + e2.getMessage());
            e2.printStackTrace();
        }
    }

    private boolean downloadDB(HttpsURLConnection https) throws IOException {
        if (https.getResponseCode() != 200) {
            reportUpdate(this.mLog, ReportType.ERROR, this.mContext.getString(R.string.log_update_error_communication));
            Log.d("ZonerAV", "Update: communication exception - code " + https.getResponseCode());
            return false;
        }
        String error = https.getHeaderField("ZAV-Error");
        if (error != null) {
            reportUpdate(this.mLog, ReportType.ERROR, error);
            return false;
        } else if (https.getHeaderField("ZAV-Outdated") != null) {
            reportUpdate(this.mLog, ReportType.OUTDATED, this.mContext.getString(R.string.log_update_outdated_please));
            return false;
        } else if (https.getHeaderField("ZAV-OK") != null) {
            reportUpdate(this.mLog, ReportType.UP2DATE, String.valueOf(this.mContext.getString(R.string.log_update_dbver)) + ": " + DbScanner.lastID);
            return true;
        } else {
            String hash = https.getHeaderField("ZAV-Hash");
            String strLen = https.getHeaderField("Content-Length");
            if (hash == null || strLen == null) {
                reportUpdate(this.mLog, ReportType.ERROR, this.mContext.getString(R.string.log_update_error_communication));
                Log.d("ZonerAV", "Update: bad headers");
                return false;
            }
            int length = Globals.toInt(strLen, 0);
            if (length == 0) {
                reportUpdate(this.mLog, ReportType.ERROR, this.mContext.getString(R.string.log_update_error_communication));
                Log.d("ZonerAV", "Update: DB size is 0");
                return false;
            }
            InputStream reader = https.getInputStream();
            FileOutputStream openFileOutput = this.mContext.openFileOutput("zavdb.new", 0);
            int total = 0;
            byte[] buf = new byte[4096];
            while (true) {
                int read = reader.read(buf);
                if (read < 0) {
                    break;
                }
                total += read;
                openFileOutput.write(buf, 0, read);
            }
            openFileOutput.close();
            if (total != length) {
                reportUpdate(this.mLog, ReportType.ERROR, this.mContext.getString(R.string.log_update_error_communication));
                Log.d("ZonerAV", "Update: total != length (" + total + " vs. " + length + ")");
                return false;
            }
            int res = DbScanner.LoadDb(this.mContext.openFileInput("zavdb.new"), hash);
            if (res != 0) {
                reportUpdate(this.mLog, ReportType.ERROR, String.valueOf(this.mContext.getString(R.string.log_update_error_database)) + " (" + res + ")");
                return false;
            }
            reportUpdate(this.mLog, ReportType.SUCCESS, String.valueOf(this.mContext.getString(R.string.log_update_dbver)) + ": " + DbScanner.lastID);
            return true;
        }
    }

    private void reportUpdate(Logger log, ReportType type, String msg) {
        if (msg == null) {
            msg = " ";
        }
        switch ($SWITCH_TABLE$com$zoner$android$antivirus$AlarmUpdate$ReportType()[type.ordinal()]) {
            case 1:
                log.msg(Logger.ERROR, Logger.UPDATE_ERROR, msg);
                break;
            case DbScanner.ERRFORMAT /*3*/:
                PreferenceManager.getDefaultSharedPreferences(this.mContext).edit().putLong(Globals.PREF_UPDATE_LAST, System.currentTimeMillis()).commit();
                break;
            case 4:
                PreferenceManager.getDefaultSharedPreferences(this.mContext).edit().putLong(Globals.PREF_UPDATE_LAST, System.currentTimeMillis()).commit();
                log.msg(Logger.INFO, Logger.UPDATE_SUCCESS, msg);
                break;
        }
        Globals.gUpdating = false;
        if (Globals.gGUIMain) {
            Intent i = new Intent(this.mContext, ActMain.class);
            i.setFlags(805306368);
            i.putExtra("update", type);
            this.mContext.startActivity(i);
        }
    }

    private String getIMEI() {
        String imei = this.mPhone.getDeviceId();
        if (imei != null) {
            return imei;
        }
        String imei2 = Settings.Secure.getString(this.mContext.getContentResolver(), "android_id");
        if (imei2 == null) {
            return "FFFFFFFFFFFFFFF";
        }
        return imei2;
    }

    private static void trustAllHosts() {
        TrustManager[] trustAllCerts = {new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }
        }};
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
