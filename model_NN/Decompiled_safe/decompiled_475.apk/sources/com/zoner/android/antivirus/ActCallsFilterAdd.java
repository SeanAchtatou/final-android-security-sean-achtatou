package com.zoner.android.antivirus;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.mms.pdu.PduHeaders;
import com.zoner.android.antivirus.DbPhoneFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActCallsFilterAdd extends Activity {
    private static final int DLG_ALLOWDENY = 1;
    private static final int DLG_CHOOSE_NUMBER = 2;
    private static final String PHONE_NUMBER_SPLITTER = ",";
    private static final String TYPE_NUMBER_SPLITTER = ": ";
    /* access modifiers changed from: private */
    public DbPhoneFilter.FilterList.Mode[] mActions;
    private DbPhoneFilter.FilterList.Mode[] mActionsOther;
    private DbPhoneFilter.FilterList.Mode[] mActionsOut;
    private String[] mChoicesOther;
    private String[] mChoicesOut;
    /* access modifiers changed from: private */
    public FilterFields mFields;
    private boolean mKnown = false;
    private boolean mUnknown = false;

    private class FilterFields {
        public String[] choices;
        public long id;
        public int type;

        private FilterFields() {
        }

        /* synthetic */ FilterFields(ActCallsFilterAdd actCallsFilterAdd, FilterFields filterFields) {
            this();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        this.mUnknown = intent.getBooleanExtra("unknown", false);
        this.mKnown = intent.getBooleanExtra("known", false);
        if (this.mUnknown || this.mKnown) {
            this.mChoicesOut = new String[]{getString(R.string.callfilter_add_allow), getString(R.string.callfilter_add_block), getString(R.string.callfilter_add_ask)};
            this.mChoicesOther = new String[]{getString(R.string.callfilter_add_allow), getString(R.string.callfilter_add_block)};
            this.mActionsOut = new DbPhoneFilter.FilterList.Mode[]{DbPhoneFilter.FilterList.Mode.Allow, DbPhoneFilter.FilterList.Mode.Deny, DbPhoneFilter.FilterList.Mode.Ask};
            this.mActionsOther = new DbPhoneFilter.FilterList.Mode[]{DbPhoneFilter.FilterList.Mode.Allow, DbPhoneFilter.FilterList.Mode.Deny};
        } else {
            this.mChoicesOut = new String[]{getString(R.string.callfilter_add_none), getString(R.string.callfilter_add_allow), getString(R.string.callfilter_add_block), getString(R.string.callfilter_add_ask)};
            this.mChoicesOther = new String[]{getString(R.string.callfilter_add_none), getString(R.string.callfilter_add_allow), getString(R.string.callfilter_add_block)};
            this.mActionsOut = new DbPhoneFilter.FilterList.Mode[]{DbPhoneFilter.FilterList.Mode.None, DbPhoneFilter.FilterList.Mode.Allow, DbPhoneFilter.FilterList.Mode.Deny, DbPhoneFilter.FilterList.Mode.Ask};
            this.mActionsOther = new DbPhoneFilter.FilterList.Mode[]{DbPhoneFilter.FilterList.Mode.None, DbPhoneFilter.FilterList.Mode.Allow, DbPhoneFilter.FilterList.Mode.Deny};
        }
        this.mFields = (FilterFields) getLastNonConfigurationInstance();
        if (this.mFields == null) {
            this.mFields = new FilterFields(this, null);
            this.mFields.id = intent.getLongExtra(DbPhoneFilter.DbColumns.COLUMN_ID, -1);
        }
        setContentView((int) R.layout.callfilter_add);
        ListView listView = (ListView) findViewById(R.id.callfilter_add_rules);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                ActCallsFilterAdd.this.mFields.type = position;
                ActCallsFilterAdd.this.showDialog(1);
            }
        });
        populateList(listView);
        String number = (String) intent.getCharSequenceExtra("number");
        EditText editText = (EditText) findViewById(R.id.callfilter_add_number);
        if (number != null) {
            editText.setText(number);
        } else {
            setTitle((int) R.string.title_contact_blocking_add);
        }
        if (this.mUnknown) {
            editText.setEnabled(false);
            editText.setFocusable(false);
            editText.setFocusableInTouchMode(false);
            findViewById(R.id.callfilter_add_contact).setEnabled(false);
        } else if (this.mKnown) {
            editText.setEnabled(false);
            editText.setFocusable(false);
            editText.setFocusableInTouchMode(false);
            findViewById(R.id.callfilter_add_contact).setEnabled(false);
        }
        findViewById(R.id.callfilter_add_ok).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ActCallsFilterAdd.this.addToDatabase()) {
                    ActCallsFilterAdd.this.setResult(-1);
                    ActCallsFilterAdd.this.finish();
                }
            }
        });
        findViewById(R.id.callfilter_add_cancel).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActCallsFilterAdd.this.setResult(0);
                ActCallsFilterAdd.this.finish();
            }
        });
        findViewById(R.id.callfilter_add_contact).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActCallsFilterAdd.this.getContact();
            }
        });
    }

    public Object onRetainNonConfigurationInstance() {
        return this.mFields;
    }

    /* Debug info: failed to restart local var, previous not found, register: 13 */
    /* access modifiers changed from: package-private */
    public boolean addToDatabase() {
        boolean failed;
        ListAdapter adapter = ((ListView) findViewById(R.id.callfilter_add_rules)).getAdapter();
        Map<String, Object> callIn = (Map) adapter.getItem(1);
        Map<String, Object> callOut = (Map) adapter.getItem(0);
        Map<String, Object> smsIn = (Map) adapter.getItem(2);
        if (this.mUnknown) {
            PreferenceManager.getDefaultSharedPreferences(this).edit().putInt(Globals.PREF_BLOCK_UNKNOWN_IN, ((DbPhoneFilter.FilterList.Mode) callIn.get(DbPhoneFilter.Logs.COLUMN_ACTION)).ordinal()).putInt(Globals.PREF_BLOCK_UNKNOWN_OUT, ((DbPhoneFilter.FilterList.Mode) callOut.get(DbPhoneFilter.Logs.COLUMN_ACTION)).ordinal()).putInt(Globals.PREF_BLOCK_UNKNOWN_SMS, ((DbPhoneFilter.FilterList.Mode) smsIn.get(DbPhoneFilter.Logs.COLUMN_ACTION)).ordinal()).commit();
            return true;
        } else if (this.mKnown) {
            PreferenceManager.getDefaultSharedPreferences(this).edit().putInt(Globals.PREF_BLOCK_KNOWN_IN, ((DbPhoneFilter.FilterList.Mode) callIn.get(DbPhoneFilter.Logs.COLUMN_ACTION)).ordinal()).putInt(Globals.PREF_BLOCK_KNOWN_OUT, ((DbPhoneFilter.FilterList.Mode) callOut.get(DbPhoneFilter.Logs.COLUMN_ACTION)).ordinal()).putInt(Globals.PREF_BLOCK_KNOWN_SMS, ((DbPhoneFilter.FilterList.Mode) smsIn.get(DbPhoneFilter.Logs.COLUMN_ACTION)).ordinal()).commit();
            return true;
        } else {
            String number = ((EditText) findViewById(R.id.callfilter_add_number)).getText().toString();
            if (number.length() == 0) {
                Toast.makeText(this, getString(R.string.callfilter_add_empty), 0).show();
                return false;
            } else if (PhoneNumberUtils.isEmergencyNumber(number)) {
                Toast.makeText(this, getString(R.string.callfilter_add_emergency), 0).show();
                return false;
            } else {
                DbPhoneFilter phoneFilter = new DbPhoneFilter(this);
                DbPhoneFilter.FilterList.Mode callInMode = (DbPhoneFilter.FilterList.Mode) callIn.get(DbPhoneFilter.Logs.COLUMN_ACTION);
                DbPhoneFilter.FilterList.Mode callOutMode = (DbPhoneFilter.FilterList.Mode) callOut.get(DbPhoneFilter.Logs.COLUMN_ACTION);
                DbPhoneFilter.FilterList.Mode smsInMode = (DbPhoneFilter.FilterList.Mode) smsIn.get(DbPhoneFilter.Logs.COLUMN_ACTION);
                DbPhoneFilter.FilterList.Mode mmsInMode = smsInMode;
                if (this.mFields.id != -1) {
                    failed = !phoneFilter.updateFilter(this.mFields.id, number, callInMode, callOutMode, smsInMode, mmsInMode);
                } else {
                    this.mFields.id = phoneFilter.addFilter(number, callInMode, callOutMode, smsInMode, mmsInMode);
                    failed = this.mFields.id == -1;
                }
                if (failed) {
                    Toast.makeText(this, getString(R.string.callfilter_add_failed), 0).show();
                }
                if (failed) {
                    return false;
                }
                return true;
            }
        }
    }

    private void populateList(ListView listView) {
        Intent intent = getIntent();
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> item = new HashMap<>();
        item.put("title", getString(R.string.callfilter_add_out));
        item.put(DbPhoneFilter.FilterList.TABLE_NAME, 0);
        setItemAction(item, DbPhoneFilter.FilterList.Mode.getEnum(intent.getIntExtra("actionCallsOut", DbPhoneFilter.FilterList.Mode.Deny.ordinal())));
        list.add(item);
        Map<String, Object> item2 = new HashMap<>();
        item2.put("title", getString(R.string.callfilter_add_in));
        item2.put(DbPhoneFilter.FilterList.TABLE_NAME, 1);
        setItemAction(item2, DbPhoneFilter.FilterList.Mode.getEnum(intent.getIntExtra("actionCallsIn", DbPhoneFilter.FilterList.Mode.Deny.ordinal())));
        list.add(item2);
        Map<String, Object> item3 = new HashMap<>();
        item3.put("title", getString(R.string.callfilter_add_sms));
        item3.put(DbPhoneFilter.FilterList.TABLE_NAME, 2);
        setItemAction(item3, DbPhoneFilter.FilterList.Mode.getEnum(intent.getIntExtra("actionSmsIn", DbPhoneFilter.FilterList.Mode.Deny.ordinal())));
        list.add(item3);
        listView.setAdapter((ListAdapter) new ActionAdapter(this, list, R.layout.callfilter_add_row, new String[]{"title", "icon", "actionString"}, new int[]{R.id.callfilter_add_row_text, R.id.callfilter_add_row_icon, R.id.callfilter_add_row_action}));
    }

    /* access modifiers changed from: private */
    public void getContact() {
        startActivityForResult(new Intent("android.intent.action.PICK", ContactsContract.Contacts.CONTENT_URI), 1);
    }

    /* Debug info: failed to restart local var, previous not found, register: 13 */
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if (resultCode == -1) {
            Cursor cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id=?", new String[]{data.getData().getLastPathSegment()}, null);
            if (cursor.moveToFirst()) {
                String number = cursor.getString(cursor.getColumnIndexOrThrow("data1"));
                if (cursor.getCount() > 1) {
                    Resources res = getResources();
                    String numbers = ((Object) ContactsContract.CommonDataKinds.Phone.getTypeLabel(res, cursor.getInt(cursor.getColumnIndexOrThrow("data2")), getString(R.string.callfilter_add_type_unknown))) + TYPE_NUMBER_SPLITTER + number;
                    while (cursor.moveToNext()) {
                        int type = cursor.getInt(cursor.getColumnIndexOrThrow("data2"));
                        numbers = String.valueOf(numbers) + PHONE_NUMBER_SPLITTER + ((Object) ContactsContract.CommonDataKinds.Phone.getTypeLabel(res, type, getString(R.string.callfilter_add_type_unknown))) + TYPE_NUMBER_SPLITTER + cursor.getString(cursor.getColumnIndexOrThrow("data1"));
                    }
                    this.mFields.choices = numbers.split(PHONE_NUMBER_SPLITTER);
                    showDialog(2);
                    cursor.close();
                    return;
                }
                cursor.close();
                ((EditText) findViewById(R.id.callfilter_add_number)).setText(number);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                String title = getString(getTitleRes(this.mFields.type));
                String[] choices = this.mFields.type == 0 ? this.mChoicesOut : this.mChoicesOther;
                this.mActions = this.mFields.type == 0 ? this.mActionsOut : this.mActionsOther;
                Dialog filterDlg = new AlertDialog.Builder(this).setTitle(title).setItems(choices, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idx) {
                        DbPhoneFilter.FilterList.Mode action = ActCallsFilterAdd.this.mActions[idx];
                        SimpleAdapter adapter = (SimpleAdapter) ((ListView) ActCallsFilterAdd.this.findViewById(R.id.callfilter_add_rules)).getAdapter();
                        ActCallsFilterAdd.this.setItemAction((Map) adapter.getItem(ActCallsFilterAdd.this.mFields.type), action);
                        adapter.notifyDataSetChanged();
                    }
                }).create();
                filterDlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        ActCallsFilterAdd.this.removeDialog(1);
                    }
                });
                return filterDlg;
            case 2:
                Dialog chooseDlg = new AlertDialog.Builder(this).setTitle(getString(R.string.callfilter_add_choose)).setItems(this.mFields.choices, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int idx) {
                        ((EditText) ActCallsFilterAdd.this.findViewById(R.id.callfilter_add_number)).setText(ActCallsFilterAdd.this.mFields.choices[idx].split(ActCallsFilterAdd.TYPE_NUMBER_SPLITTER)[1]);
                    }
                }).create();
                chooseDlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        ActCallsFilterAdd.this.removeDialog(2);
                    }
                });
                return chooseDlg;
            default:
                return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void setItemAction(Map<String, Object> item, DbPhoneFilter.FilterList.Mode action) {
        item.put("icon", Integer.valueOf(ActCallsFilter.getActionIcon(((Integer) item.get(DbPhoneFilter.FilterList.TABLE_NAME)).intValue(), action)));
        item.put("actionString", getString(getActionRes(action)));
        item.put("actionColor", Integer.valueOf(getActionColor(action)));
        item.put(DbPhoneFilter.Logs.COLUMN_ACTION, action);
    }

    /* access modifiers changed from: package-private */
    public int getTitleRes(int type) {
        if (type == 1) {
            return R.string.callfilter_add_in;
        }
        if (type == 0) {
            return R.string.callfilter_add_out;
        }
        if (type == 2) {
            return R.string.callfilter_add_sms;
        }
        return R.string.callfilter_add_error;
    }

    /* access modifiers changed from: package-private */
    public int getActionRes(DbPhoneFilter.FilterList.Mode type) {
        if (type == DbPhoneFilter.FilterList.Mode.Allow) {
            return R.string.callfilter_add_allow;
        }
        if (type == DbPhoneFilter.FilterList.Mode.Deny) {
            return R.string.callfilter_add_block;
        }
        if (type == DbPhoneFilter.FilterList.Mode.Ask) {
            return R.string.callfilter_add_ask;
        }
        return R.string.callfilter_add_none;
    }

    /* access modifiers changed from: package-private */
    public int getActionColor(DbPhoneFilter.FilterList.Mode type) {
        if (type == DbPhoneFilter.FilterList.Mode.Allow) {
            return Color.rgb(70, (int) PduHeaders.MBOX_TOTALS, 26);
        }
        if (type == DbPhoneFilter.FilterList.Mode.Deny) {
            return Color.rgb(211, 25, 32);
        }
        if (type == DbPhoneFilter.FilterList.Mode.Ask) {
            return Color.rgb(210, (int) PduHeaders.CONTENT_CLASS, 24);
        }
        return -7829368;
    }

    class ActionAdapter extends SimpleAdapter {
        ActionAdapter(Context context, List<Map<String, Object>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View row = super.getView(position, convertView, parent);
            ((TextView) row.findViewById(R.id.callfilter_add_row_action)).setTextColor(((Integer) ((Map) getItem(position)).get("actionColor")).intValue());
            return row;
        }
    }
}
