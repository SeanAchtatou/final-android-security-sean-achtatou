package com.zoner.android.antivirus;

public interface IScanConnector extends IBinderConnector {
    void onScanFinish();

    void onScanResult(ScanResult scanResult);
}
