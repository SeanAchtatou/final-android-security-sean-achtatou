package com.zoner.android.antivirus;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Messenger;
import com.zoner.android.antivirus.ZapService;

public class ServiceBinder {
    /* access modifiers changed from: private */
    public IBinderConnector mBindTo;
    /* access modifiers changed from: private */
    public boolean mBound = false;
    /* access modifiers changed from: private */
    public boolean mConnected = false;
    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            if (ServiceBinder.this.mBound) {
                ServiceBinder.this.mService = ((ZapService.LocalBinder) service).getService();
                if (ServiceBinder.this.mReplyToID != null) {
                    ServiceBinder.this.mService.mtReplyTo[ServiceBinder.this.mReplyToID.intValue()] = ServiceBinder.this.mMessenger;
                }
                ServiceBinder.this.mBindTo.onBound(ServiceBinder.this.mService);
                ServiceBinder.this.mConnected = true;
            }
        }

        public void onServiceDisconnected(ComponentName arg0) {
            ServiceBinder.this.mConnected = false;
            if (ServiceBinder.this.mReplyToID != null) {
                ServiceBinder.this.mService.mtReplyTo[ServiceBinder.this.mReplyToID.intValue()] = null;
            }
            ServiceBinder.this.mBound = false;
        }
    };
    private Context mContext;
    protected Messenger mMessenger;
    /* access modifiers changed from: private */
    public Integer mReplyToID;
    /* access modifiers changed from: private */
    public ZapService mService;

    ServiceBinder(IBinderConnector caller, Context context, Integer id) {
        this.mBindTo = caller;
        this.mContext = context;
        this.mReplyToID = id;
    }

    public void doBind() {
        this.mContext.bindService(new Intent(this.mContext, ZapService.class), this.mConnection, 0);
        this.mBound = true;
    }

    public void doUnbind() {
        if (this.mBound) {
            if (this.mConnected && this.mReplyToID != null) {
                this.mService.mtReplyTo[this.mReplyToID.intValue()] = null;
            }
            this.mContext.unbindService(this.mConnection);
            this.mBound = false;
        }
    }
}
