package com.zoner.android.antivirus;

public final class R {

    public static final class anim {
        public static final int scan = 2130968576;
        public static final int scan_green = 2130968577;
        public static final int scan_red = 2130968578;
        public static final int scan_white = 2130968579;
    }

    public static final class array {
        public static final int updateArray = 2131099648;
        public static final int updateValues = 2131099649;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int add_contact = 2130837504;
        public static final int add_filter = 2130837505;
        public static final int call = 2130837506;
        public static final int calls_filter = 2130837507;
        public static final int calls_log = 2130837508;
        public static final int calls_settings = 2130837509;
        public static final int dialog_block = 2130837510;
        public static final int dir = 2130837511;
        public static final int dir_locked = 2130837512;
        public static final int file = 2130837513;
        public static final int file_locked = 2130837514;
        public static final int filter_callin_allow = 2130837515;
        public static final int filter_callin_block = 2130837516;
        public static final int filter_callin_notset = 2130837517;
        public static final int filter_callout_allow = 2130837518;
        public static final int filter_callout_ask = 2130837519;
        public static final int filter_callout_block = 2130837520;
        public static final int filter_callout_notset = 2130837521;
        public static final int filter_sms_allow = 2130837522;
        public static final int filter_sms_block = 2130837523;
        public static final int filter_sms_notset = 2130837524;
        public static final int hangup = 2130837525;
        public static final int home = 2130837526;
        public static final int launcher = 2130837527;
        public static final int left = 2130837528;
        public static final int level0 = 2130837529;
        public static final int level1 = 2130837530;
        public static final int level2 = 2130837531;
        public static final int level3 = 2130837532;
        public static final int level4 = 2130837533;
        public static final int log_clean = 2130837534;
        public static final int log_debug = 2130837535;
        public static final int log_error = 2130837536;
        public static final int log_info = 2130837537;
        public static final int log_threat = 2130837538;
        public static final int log_unknown = 2130837539;
        public static final int log_warning = 2130837540;
        public static final int logo = 2130837541;
        public static final int menu_apps = 2130837542;
        public static final int menu_av = 2130837543;
        public static final int menu_filter = 2130837544;
        public static final int menu_tasks = 2130837545;
        public static final int menu_test = 2130837546;
        public static final int menu_update = 2130837547;
        public static final int no = 2130837548;
        public static final int notif = 2130837549;
        public static final int notif_clean = 2130837550;
        public static final int notif_threats = 2130837551;
        public static final int root = 2130837552;
        public static final int scan_0 = 2130837553;
        public static final int scan_1 = 2130837554;
        public static final int scan_2 = 2130837555;
        public static final int scan_3 = 2130837556;
        public static final int scan_4 = 2130837557;
        public static final int scan_5 = 2130837558;
        public static final int scan_6 = 2130837559;
        public static final int scan_7 = 2130837560;
        public static final int scan_8 = 2130837561;
        public static final int scan_9 = 2130837562;
        public static final int scan_clean = 2130837563;
        public static final int scan_device = 2130837564;
        public static final int scan_sdcard = 2130837565;
        public static final int scan_threats = 2130837566;
        public static final int scan_user = 2130837567;
        public static final int tab_filters_off = 2130837568;
        public static final int tab_filters_on = 2130837569;
        public static final int tab_logs_off = 2130837570;
        public static final int tab_logs_on = 2130837571;
        public static final int tab_settings_off = 2130837572;
        public static final int tab_settings_on = 2130837573;
        public static final int yes = 2130837574;
        public static final int zav_green = 2130837575;
        public static final int zav_green_0 = 2130837576;
        public static final int zav_green_1 = 2130837577;
        public static final int zav_green_2 = 2130837578;
        public static final int zav_green_3 = 2130837579;
        public static final int zav_green_4 = 2130837580;
        public static final int zav_green_5 = 2130837581;
        public static final int zav_green_6 = 2130837582;
        public static final int zav_green_7 = 2130837583;
        public static final int zav_red = 2130837584;
        public static final int zav_red_0 = 2130837585;
        public static final int zav_red_1 = 2130837586;
        public static final int zav_red_2 = 2130837587;
        public static final int zav_red_3 = 2130837588;
        public static final int zav_red_4 = 2130837589;
        public static final int zav_red_5 = 2130837590;
        public static final int zav_red_6 = 2130837591;
        public static final int zav_red_7 = 2130837592;
        public static final int zav_white = 2130837593;
        public static final int zav_white_0 = 2130837594;
        public static final int zav_white_1 = 2130837595;
        public static final int zav_white_2 = 2130837596;
        public static final int zav_white_3 = 2130837597;
        public static final int zav_white_4 = 2130837598;
        public static final int zav_white_5 = 2130837599;
        public static final int zav_white_6 = 2130837600;
        public static final int zav_white_7 = 2130837601;
    }

    public static final class id {
        public static final int appinfo_appitem_icon = 2131361793;
        public static final int appinfo_appitem_name = 2131361796;
        public static final int appinfo_appitem_packagename = 2131361797;
        public static final int appinfo_appitem_permcount = 2131361795;
        public static final int appinfo_ctx_info = 2131361966;
        public static final int appinfo_ctx_start = 2131361965;
        public static final int appinfo_ctx_uninstall = 2131361967;
        public static final int appinfo_filter = 2131361964;
        public static final int appinfo_permitem_appcount = 2131361799;
        public static final int appinfo_permitem_desc = 2131361802;
        public static final int appinfo_permitem_icon = 2131361798;
        public static final int appinfo_permitem_name = 2131361801;
        public static final int appinfo_permitem_prefix = 2131361800;
        public static final int appinfo_showperms = 2131361962;
        public static final int appinfo_sort = 2131361963;
        public static final int browse_ctx_delete = 2131361970;
        public static final int browse_ctx_open = 2131361968;
        public static final int browse_ctx_scan = 2131361969;
        public static final int browse_list_icon = 2131361874;
        public static final int browse_list_name = 2131361875;
        public static final int browser_btn_exit = 2131361869;
        public static final int browser_btn_home = 2131361871;
        public static final int browser_btn_root = 2131361870;
        public static final int browser_btn_scan = 2131361873;
        public static final int browser_list = 2131361872;
        public static final int browser_sethome = 2131361975;
        public static final int callfilter_add_cancel = 2131361808;
        public static final int callfilter_add_contact = 2131361804;
        public static final int callfilter_add_number = 2131361803;
        public static final int callfilter_add_ok = 2131361807;
        public static final int callfilter_add_row_action = 2131361811;
        public static final int callfilter_add_row_icon = 2131361809;
        public static final int callfilter_add_row_more = 2131361812;
        public static final int callfilter_add_row_text = 2131361810;
        public static final int callfilter_add_rules = 2131361805;
        public static final int callfilter_clear = 2131361972;
        public static final int callfilter_ctx_delete = 2131361974;
        public static final int callfilter_ctx_edit = 2131361973;
        public static final int callfilter_header = 2131361844;
        public static final int callfilter_header_icon = 2131361845;
        public static final int callfilter_header_title = 2131361846;
        public static final int callfilter_known_callsin = 2131361817;
        public static final int callfilter_known_callsout = 2131361816;
        public static final int callfilter_known_layout = 2131361813;
        public static final int callfilter_known_name = 2131361815;
        public static final int callfilter_known_number = 2131361814;
        public static final int callfilter_known_smsin = 2131361818;
        public static final int callfilter_list = 2131361843;
        public static final int callfilter_row_callsin = 2131361823;
        public static final int callfilter_row_callsout = 2131361822;
        public static final int callfilter_row_layout = 2131361819;
        public static final int callfilter_row_name = 2131361821;
        public static final int callfilter_row_number = 2131361820;
        public static final int callfilter_row_smsin = 2131361824;
        public static final int callfilter_sort = 2131361971;
        public static final int callfilter_unknown_callsin = 2131361829;
        public static final int callfilter_unknown_callsout = 2131361828;
        public static final int callfilter_unknown_layout = 2131361825;
        public static final int callfilter_unknown_name = 2131361827;
        public static final int callfilter_unknown_number = 2131361826;
        public static final int callfilter_unknown_smsin = 2131361830;
        public static final int callquery_allow = 2131361839;
        public static final int callquery_block_layout = 2131361838;
        public static final int callquery_btn_no = 2131361842;
        public static final int callquery_btn_yes = 2131361841;
        public static final int callquery_contact = 2131361833;
        public static final int callquery_parental_layout = 2131361834;
        public static final int callquery_parental_password = 2131361836;
        public static final int callquery_parental_status = 2131361837;
        public static final int callquery_parental_text = 2131361835;
        public static final int callquery_phone_number = 2131361832;
        public static final int callquery_remember = 2131361840;
        public static final int callquery_title_divider = 2131361831;
        public static final int callset_enable = 2131361848;
        public static final int callset_enable_desc = 2131361850;
        public static final int callset_enable_text = 2131361849;
        public static final int callset_lock = 2131361859;
        public static final int callset_lock_desc = 2131361861;
        public static final int callset_lock_text = 2131361860;
        public static final int callset_pass = 2131361867;
        public static final int callset_pass_desc = 2131361866;
        public static final int callset_pass_text = 2131361865;
        public static final int callset_white = 2131361853;
        public static final int callset_white_desc = 2131361855;
        public static final int callset_white_text = 2131361854;
        public static final int first_btn_ok = 2131361885;
        public static final int first_chk_block = 2131361881;
        public static final int first_chk_lt = 2131361883;
        public static final int first_chk_scan = 2131361879;
        public static final int first_layout = 2131361876;
        public static final int first_title_divider = 2131361877;
        public static final int imageView1 = 2131361851;
        public static final int imageView2 = 2131361856;
        public static final int imageView4 = 2131361868;
        public static final int linearLayout1 = 2131361794;
        public static final int linearLayout2 = 2131361806;
        public static final int linearLayout3 = 2131361858;
        public static final int linearLayout4 = 2131361863;
        public static final int linearLayout5 = 2131361864;
        public static final int listmenu_image = 2131361888;
        public static final int listmenu_layout = 2131361886;
        public static final int listmenu_list = 2131361887;
        public static final int listmenu_row_desc = 2131361892;
        public static final int listmenu_row_icon = 2131361890;
        public static final int listmenu_row_layout = 2131361889;
        public static final int listmenu_row_name = 2131361891;
        public static final int log_delete = 2131361977;
        public static final int log_refresh = 2131361976;
        public static final int log_row_header = 2131361894;
        public static final int log_row_icon = 2131361893;
        public static final int log_row_message = 2131361895;
        public static final int main_about = 2131361978;
        public static final int main_about_copyright = 2131361901;
        public static final int main_about_layout = 2131361896;
        public static final int main_about_ok = 2131361902;
        public static final int main_about_title = 2131361898;
        public static final int main_about_title_divider = 2131361897;
        public static final int main_about_version = 2131361899;
        public static final int main_about_www = 2131361900;
        public static final int main_log = 2131361979;
        public static final int main_settings = 2131361980;
        public static final int notification_header = 2131361904;
        public static final int notification_image = 2131361903;
        public static final int notification_text = 2131361905;
        public static final int perminfo_filter = 2131361982;
        public static final int perminfo_sort = 2131361981;
        public static final int relativeLayout1 = 2131361792;
        public static final int relativeLayout2 = 2131361852;
        public static final int relativeLayout3 = 2131361857;
        public static final int relativeLayout4 = 2131361862;
        public static final int results_row_virus = 2131361929;
        public static final int scanclean_ok = 2131361908;
        public static final int scanclean_result = 2131361907;
        public static final int scanclean_title_divider_1 = 2131361906;
        public static final int scaninfected_buttons_layout = 2131361915;
        public static final int scaninfected_header_layout = 2131361910;
        public static final int scaninfected_ignore = 2131361918;
        public static final int scaninfected_lbl = 2131361911;
        public static final int scaninfected_list = 2131361914;
        public static final int scaninfected_multi_row_checkbox = 2131361926;
        public static final int scaninfected_multi_row_path = 2131361927;
        public static final int scaninfected_multi_row_virus = 2131361925;
        public static final int scaninfected_remove = 2131361917;
        public static final int scaninfected_selectall = 2131361912;
        public static final int scaninfected_selectall_layout = 2131361916;
        public static final int scaninfected_selectall_lbl = 2131361913;
        public static final int scaninfected_single_row_path = 2131361930;
        public static final int scaninfected_single_row_virus = 2131361928;
        public static final int scaninfected_title_divider = 2131361909;
        public static final int scanprogress_current = 2131361920;
        public static final int scanprogress_pause = 2131361922;
        public static final int scanprogress_resume = 2131361923;
        public static final int scanprogress_stop = 2131361924;
        public static final int scanprogress_title_divider_1 = 2131361919;
        public static final int scaprogress_buttons_layout = 2131361921;
        public static final int scrollView1 = 2131361847;
        public static final int tasklist_detail_cpu = 2131361944;
        public static final int tasklist_detail_cpu_label = 2131361943;
        public static final int tasklist_detail_kill = 2131361956;
        public static final int tasklist_detail_layout = 2131361936;
        public static final int tasklist_detail_mem = 2131361946;
        public static final int tasklist_detail_mem_label = 2131361945;
        public static final int tasklist_detail_pid = 2131361940;
        public static final int tasklist_detail_pid_label = 2131361939;
        public static final int tasklist_detail_pkgname = 2131361938;
        public static final int tasklist_detail_state = 2131361948;
        public static final int tasklist_detail_state_label = 2131361947;
        public static final int tasklist_detail_stime = 2131361950;
        public static final int tasklist_detail_stime_label = 2131361949;
        public static final int tasklist_detail_switchto = 2131361955;
        public static final int tasklist_detail_threads = 2131361952;
        public static final int tasklist_detail_threads_label = 2131361951;
        public static final int tasklist_detail_title_divider = 2131361937;
        public static final int tasklist_detail_utime = 2131361942;
        public static final int tasklist_detail_utime_label = 2131361941;
        public static final int tasklist_detail_virtual = 2131361954;
        public static final int tasklist_detail_virtual_label = 2131361953;
        public static final int tasklist_footer = 2131361933;
        public static final int tasklist_footer_label = 2131361934;
        public static final int tasklist_footer_layout = 2131361931;
        public static final int tasklist_footer_mem = 2131361935;
        public static final int tasklist_list = 2131361932;
        public static final int tasklist_row_icon = 2131361957;
        public static final int tasklist_row_mem = 2131361959;
        public static final int tasklist_row_name = 2131361958;
        public static final int tasklist_sort = 2131361983;
        public static final int tasklist_title_mem = 2131361961;
        public static final int tasklist_title_name = 2131361960;
        public static final int textView1 = 2131361878;
        public static final int textView2 = 2131361880;
        public static final int textView3 = 2131361882;
        public static final int textView4 = 2131361884;
    }

    public static final class layout {
        public static final int appinfo_app_row = 2130903040;
        public static final int appinfo_perm_row = 2130903041;
        public static final int appinfo_subapp_row = 2130903042;
        public static final int appinfo_subperm_row = 2130903043;
        public static final int callfilter_add = 2130903044;
        public static final int callfilter_add_row = 2130903045;
        public static final int callfilter_known = 2130903046;
        public static final int callfilter_row = 2130903047;
        public static final int callfilter_unknown = 2130903048;
        public static final int callquery = 2130903049;
        public static final int calls_filter = 2130903050;
        public static final int calls_filter_header = 2130903051;
        public static final int calls_settings = 2130903052;
        public static final int calls_tabs = 2130903053;
        public static final int file_browser = 2130903054;
        public static final int file_browser_row = 2130903055;
        public static final int first_run = 2130903056;
        public static final int listmenu = 2130903057;
        public static final int listmenu_row = 2130903058;
        public static final int log_row = 2130903059;
        public static final int main_about = 2130903060;
        public static final int notif = 2130903061;
        public static final int scan_clean = 2130903062;
        public static final int scan_infected = 2130903063;
        public static final int scan_progress = 2130903064;
        public static final int scaninfected_multi_row = 2130903065;
        public static final int scaninfected_single_row = 2130903066;
        public static final int tasklist = 2130903067;
        public static final int tasklist_detail = 2130903068;
        public static final int tasklist_row = 2130903069;
        public static final int tasklist_title = 2130903070;
    }

    public static final class menu {
        public static final int appinfo = 2131296256;
        public static final int appinfo_ctx = 2131296257;
        public static final int browse_ctx = 2131296258;
        public static final int callfilter = 2131296259;
        public static final int callfilter_ctx = 2131296260;
        public static final int filebrowser = 2131296261;
        public static final int log = 2131296262;
        public static final int main = 2131296263;
        public static final int perminfo = 2131296264;
        public static final int tasklist = 2131296265;
    }

    public static final class plurals {
        public static final int notif_threats = 2131230721;
        public static final int scaninfected_results = 2131230720;
        public static final int ticker_threats = 2131230722;
    }

    public static final class string {
        public static final int app_name = 2131165184;
        public static final int appinfo_app_start_failed = 2131165258;
        public static final int appinfo_appdialog_title = 2131165244;
        public static final int appinfo_ctx_info = 2131165255;
        public static final int appinfo_ctx_start = 2131165254;
        public static final int appinfo_ctx_title = 2131165257;
        public static final int appinfo_ctx_uninstall = 2131165256;
        public static final int appinfo_no_perm_desc = 2131165260;
        public static final int appinfo_no_perms = 2131165261;
        public static final int appinfo_opts_filter = 2131165253;
        public static final int appinfo_opts_level = 2131165246;
        public static final int appinfo_opts_name = 2131165245;
        public static final int appinfo_opts_package = 2131165247;
        public static final int appinfo_opts_permcount = 2131165248;
        public static final int appinfo_opts_showperms = 2131165259;
        public static final int appinfo_opts_sort = 2131165249;
        public static final int appinfo_opts_sort_asc = 2131165251;
        public static final int appinfo_opts_sort_desc = 2131165252;
        public static final int appinfo_opts_sort_title = 2131165250;
        public static final int appinfo_permdialog_title = 2131165262;
        public static final int appinfo_progress_loading = 2131165243;
        public static final int browse_scan_forbidden = 2131165280;
        public static final int browser_delete = 2131165271;
        public static final int browser_dir_delete = 2131165272;
        public static final int browser_dir_delete_fail = 2131165276;
        public static final int browser_dir_delete_success = 2131165274;
        public static final int browser_dir_empty = 2131165281;
        public static final int browser_dir_locked = 2131165278;
        public static final int browser_file_delete = 2131165273;
        public static final int browser_file_delete_fail = 2131165277;
        public static final int browser_file_delete_success = 2131165275;
        public static final int browser_file_locked = 2131165279;
        public static final int browser_home_set = 2131165268;
        public static final int browser_open = 2131165269;
        public static final int browser_opts_sethome = 2131165267;
        public static final int browser_scan = 2131165270;
        public static final int button_cancel = 2131165188;
        public static final int button_no = 2131165186;
        public static final int button_ok = 2131165187;
        public static final int button_yes = 2131165185;
        public static final int callfilter_add_allow = 2131165372;
        public static final int callfilter_add_ask = 2131165374;
        public static final int callfilter_add_block = 2131165373;
        public static final int callfilter_add_choose = 2131165376;
        public static final int callfilter_add_emergency = 2131165379;
        public static final int callfilter_add_empty = 2131165378;
        public static final int callfilter_add_error = 2131165371;
        public static final int callfilter_add_failed = 2131165377;
        public static final int callfilter_add_in = 2131165368;
        public static final int callfilter_add_none = 2131165375;
        public static final int callfilter_add_out = 2131165369;
        public static final int callfilter_add_sms = 2131165370;
        public static final int callfilter_add_type_unknown = 2131165380;
        public static final int callfilter_clear = 2131165364;
        public static final int callfilter_clear_failed = 2131165365;
        public static final int callfilter_delete = 2131165360;
        public static final int callfilter_edit = 2131165359;
        public static final int callfilter_header_title = 2131165354;
        public static final int callfilter_known_name = 2131165358;
        public static final int callfilter_known_number = 2131165357;
        public static final int callfilter_not_enabled = 2131165367;
        public static final int callfilter_sort = 2131165361;
        public static final int callfilter_sort_byname = 2131165362;
        public static final int callfilter_sort_bynumber = 2131165363;
        public static final int callfilter_unknown_contact = 2131165366;
        public static final int callfilter_unknown_name = 2131165356;
        public static final int callfilter_unknown_number = 2131165355;
        public static final int callquery_allow = 2131165383;
        public static final int callquery_call = 2131165381;
        public static final int callquery_call_blocked = 2131165386;
        public static final int callquery_hang = 2131165382;
        public static final int callquery_parental = 2131165385;
        public static final int callquery_remember = 2131165384;
        public static final int calls_filter = 2131165336;
        public static final int calls_log = 2131165337;
        public static final int calls_log_allowed = 2131165339;
        public static final int calls_log_callin = 2131165342;
        public static final int calls_log_callout = 2131165343;
        public static final int calls_log_denied = 2131165340;
        public static final int calls_log_hanged = 2131165341;
        public static final int calls_log_mms = 2131165345;
        public static final int calls_log_sms = 2131165344;
        public static final int calls_set_enable = 2131165346;
        public static final int calls_set_enable_desc = 2131165347;
        public static final int calls_set_lock = 2131165350;
        public static final int calls_set_lock_desc = 2131165351;
        public static final int calls_set_pass = 2131165352;
        public static final int calls_set_pass_desc = 2131165353;
        public static final int calls_set_white = 2131165348;
        public static final int calls_set_white_desc = 2131165349;
        public static final int calls_settings = 2131165338;
        public static final int first_block = 2131165234;
        public static final int first_check_block = 2131165233;
        public static final int first_check_lt = 2131165235;
        public static final int first_check_scan = 2131165231;
        public static final int first_lt = 2131165236;
        public static final int first_scan = 2131165232;
        public static final int first_text = 2131165230;
        public static final int first_title = 2131165229;
        public static final int log_empty = 2131165397;
        public static final int log_file_clean = 2131165402;
        public static final int log_file_infected = 2131165401;
        public static final int log_opts_delete = 2131165325;
        public static final int log_opts_refresh = 2131165324;
        public static final int log_pkg_clean = 2131165400;
        public static final int log_pkg_infected = 2131165399;
        public static final int log_unknown = 2131165398;
        public static final int log_update_dbver = 2131165410;
        public static final int log_update_error = 2131165406;
        public static final int log_update_error_communication = 2131165403;
        public static final int log_update_error_database = 2131165404;
        public static final int log_update_outdated = 2131165407;
        public static final int log_update_outdated_please = 2131165405;
        public static final int log_update_success = 2131165409;
        public static final int log_update_up2date = 2131165408;
        public static final int main_about_copyright = 2131165228;
        public static final int main_about_version = 2131165225;
        public static final int main_about_version_unknown = 2131165226;
        public static final int main_about_www = 2131165227;
        public static final int main_button_antivirus = 2131165200;
        public static final int main_button_calls = 2131165206;
        public static final int main_button_rights = 2131165202;
        public static final int main_button_tasks = 2131165204;
        public static final int main_button_test = 2131165212;
        public static final int main_button_update = 2131165209;
        public static final int main_desc_antivirus = 2131165201;
        public static final int main_desc_calls = 2131165207;
        public static final int main_desc_calls_disabled = 2131165208;
        public static final int main_desc_rights = 2131165203;
        public static final int main_desc_tasks = 2131165205;
        public static final int main_desc_test = 2131165213;
        public static final int main_desc_update = 2131165210;
        public static final int main_desc_update_ongoing = 2131165211;
        public static final int main_market_open = 2131165217;
        public static final int main_market_open_failed = 2131165218;
        public static final int main_network_settings = 2131165220;
        public static final int main_network_settings_failed = 2131165221;
        public static final int main_opts_about = 2131165223;
        public static final int main_opts_log = 2131165224;
        public static final int main_opts_settings = 2131165222;
        public static final int main_update_network = 2131165219;
        public static final int main_update_outdated = 2131165216;
        public static final int main_update_start = 2131165214;
        public static final int main_version_unknown = 2131165215;
        public static final int malware_button_device = 2131165237;
        public static final int malware_button_sdcard = 2131165239;
        public static final int malware_button_user = 2131165241;
        public static final int malware_desc_device = 2131165238;
        public static final int malware_desc_sdcard = 2131165240;
        public static final int malware_desc_user = 2131165242;
        public static final int notif_clean = 2131165392;
        public static final int notif_ready = 2131165390;
        public static final int notif_scanning = 2131165391;
        public static final int perminfo_opts_appcount = 2131165266;
        public static final int perminfo_opts_level = 2131165263;
        public static final int perminfo_opts_name = 2131165264;
        public static final int perminfo_opts_prefix = 2131165265;
        public static final int pref_block_enable = 2131165314;
        public static final int pref_block_enable_sum = 2131165315;
        public static final int pref_cat_blocking = 2131165313;
        public static final int pref_cat_misc = 2131165319;
        public static final int pref_cat_scanning = 2131165306;
        public static final int pref_cat_update = 2131165316;
        public static final int pref_misc_icon = 2131165320;
        public static final int pref_misc_icon_sum = 2131165321;
        public static final int pref_misc_livethreat = 2131165322;
        public static final int pref_misc_livethreat_sum = 2131165323;
        public static final int pref_scan_mount = 2131165311;
        public static final int pref_scan_mount_sum = 2131165312;
        public static final int pref_scan_onaccess = 2131165309;
        public static final int pref_scan_onaccess_sum = 2131165310;
        public static final int pref_scan_packages = 2131165307;
        public static final int pref_scan_packages_sum = 2131165308;
        public static final int pref_update_freq = 2131165317;
        public static final int pref_update_freq_sum = 2131165318;
        public static final int preferences_title = 2131165305;
        public static final int result_clean = 2131165387;
        public static final int result_infected = 2131165388;
        public static final int result_unknown = 2131165389;
        public static final int scanclean_result = 2131165329;
        public static final int scaninfected_delete_failed = 2131165334;
        public static final int scaninfected_delete_successful = 2131165335;
        public static final int scaninfected_ignore = 2131165332;
        public static final int scaninfected_nothing_selected = 2131165333;
        public static final int scaninfected_remove = 2131165331;
        public static final int scaninfected_selectall = 2131165330;
        public static final int scanprogress_pause = 2131165326;
        public static final int scanprogress_resume = 2131165327;
        public static final int scanprogress_stop = 2131165328;
        public static final int share_error = 2131165414;
        public static final int share_subject = 2131165412;
        public static final int share_text = 2131165413;
        public static final int share_title = 2131165411;
        public static final int tasklist_detail_cpu = 2131165294;
        public static final int tasklist_detail_kill = 2131165283;
        public static final int tasklist_detail_mem = 2131165295;
        public static final int tasklist_detail_pid = 2131165292;
        public static final int tasklist_detail_state = 2131165296;
        public static final int tasklist_detail_stime = 2131165297;
        public static final int tasklist_detail_switchto = 2131165282;
        public static final int tasklist_detail_threads = 2131165298;
        public static final int tasklist_detail_utime = 2131165293;
        public static final int tasklist_detail_virtual = 2131165299;
        public static final int tasklist_footer_label = 2131165302;
        public static final int tasklist_kill_failed = 2131165284;
        public static final int tasklist_opts_mem = 2131165304;
        public static final int tasklist_opts_name = 2131165303;
        public static final int tasklist_state_io = 2131165287;
        public static final int tasklist_state_paging = 2131165290;
        public static final int tasklist_state_running = 2131165285;
        public static final int tasklist_state_sleeping = 2131165286;
        public static final int tasklist_state_traced = 2131165289;
        public static final int tasklist_state_unknown = 2131165291;
        public static final int tasklist_state_zombie = 2131165288;
        public static final int tasklist_title_mem = 2131165301;
        public static final int tasklist_title_name = 2131165300;
        public static final int ticker_access = 2131165395;
        public static final int ticker_clean = 2131165396;
        public static final int ticker_package_clean = 2131165393;
        public static final int ticker_package_infected = 2131165394;
        public static final int title_antivirus = 2131165189;
        public static final int title_appinfo_apps = 2131165190;
        public static final int title_appinfo_loading = 2131165192;
        public static final int title_appinfo_perms = 2131165191;
        public static final int title_contact_blocking = 2131165197;
        public static final int title_contact_blocking_add = 2131165198;
        public static final int title_contact_blocking_edit = 2131165199;
        public static final int title_log = 2131165193;
        public static final int title_scan_paused = 2131165196;
        public static final int title_scan_results = 2131165194;
        public static final int title_scan_scanning = 2131165195;
    }

    public static final class xml {
        public static final int preferences = 2131034112;
    }
}
