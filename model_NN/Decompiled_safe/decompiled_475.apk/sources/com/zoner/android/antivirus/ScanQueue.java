package com.zoner.android.antivirus;

import android.content.pm.PackageInfo;
import android.os.Handler;
import android.os.Message;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ScanQueue {
    static final int QUEUE_ACCESS = 1;
    static final int QUEUE_DEMAND = 0;
    static final int QUEUE_INSTALL = 2;
    static final int TYPE_DIR = 1;
    static final int TYPE_FILE = 0;
    static final int TYPE_PACKAGE = 2;
    public boolean mDemandPaused = false;
    public boolean mDemandScans = false;
    private Queue<QueueItem> mQueueAccess = new LinkedList();
    private Queue<QueueItem> mQueueDemand = new LinkedList();
    private Queue<QueueItem> mQueueInstall = new LinkedList();
    ZapService mService;

    ScanQueue(ZapService service) {
        this.mService = service;
    }

    class QueueItem {
        int queue;
        String target;
        int type;

        QueueItem(String target2, int type2) {
            this.target = target2;
            this.type = type2;
        }
    }

    public synchronized void addDirContents(String path, boolean isDir) {
        if (this.mDemandScans) {
            addDemand(path, isDir);
        }
    }

    public synchronized void addDemand(List<PackageInfo> list) {
        this.mDemandScans = true;
        boolean start = this.mQueueDemand.isEmpty();
        for (PackageInfo cur : list) {
            this.mQueueDemand.add(new QueueItem(cur.packageName, 2));
        }
        if (start) {
            this.mService.mNotifier.scanStart();
            notifyAll();
        }
    }

    public synchronized void addDemand(String path, boolean isDir) {
        int i = 1;
        synchronized (this) {
            this.mDemandScans = true;
            Queue<QueueItem> queue = this.mQueueDemand;
            if (!isDir) {
                i = 0;
            }
            addNew(queue, i, path);
        }
    }

    public synchronized void addAccess(String path) {
        addNew(this.mQueueAccess, 0, path);
    }

    public synchronized void addMount(String path) {
        this.mDemandScans = true;
        addNew(this.mQueueAccess, 1, path);
    }

    public synchronized void addPackage(String name) {
        addNew(this.mQueueInstall, 2, name);
    }

    public synchronized void addNew(Queue<QueueItem> queue, int type, String target) {
        boolean start = queue.isEmpty();
        queue.add(new QueueItem(target, type));
        if (start) {
            this.mService.mNotifier.scanStart();
            notifyAll();
        }
    }

    public synchronized QueueItem getNext() {
        QueueItem item;
        while (this.mQueueInstall.isEmpty() && this.mQueueAccess.isEmpty() && (this.mDemandPaused || this.mQueueDemand.isEmpty())) {
            this.mService.mNotifier.scanStop();
            if (this.mDemandScans && this.mQueueDemand.isEmpty()) {
                demandDone();
            }
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
        if (!this.mQueueInstall.isEmpty()) {
            item = this.mQueueInstall.remove();
            item.queue = 2;
        } else if (!this.mQueueAccess.isEmpty()) {
            item = this.mQueueAccess.remove();
            item.queue = 1;
        } else {
            item = this.mQueueDemand.remove();
            item.queue = 0;
        }
        return item;
    }

    private void demandDone() {
        this.mDemandScans = false;
        this.mService.mNotifier.notifyOnDemand(this.mService.mDemandResults.cInfections());
        if (this.mService.mtReplyTo[0] != null) {
            this.mService.sendReply(0, Message.obtain((Handler) null, 2));
        }
    }

    public synchronized void demandStop() {
        this.mDemandPaused = false;
        this.mService.mNotifier.scanStop();
        this.mQueueDemand.clear();
        demandDone();
    }

    public synchronized void demandPause() {
        this.mDemandPaused = true;
    }

    public synchronized void demandResume() {
        this.mDemandPaused = false;
        this.mService.mNotifier.scanStart();
        notifyAll();
    }
}
