package com.zoner.android.antivirus;

import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.zoner.android.antivirus.ScanQueue;
import com.zoner.android.antivirus.ScanResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ScanHandler implements Runnable {
    private Logger mLogger = new Logger(this.mService);
    private ScanQueue mQueue = this.mService.mQueue;
    private Scanner mScanner = new Scanner();
    private ZapService mService;

    ScanHandler(ZapService parent) {
        this.mService = parent;
    }

    public void run() {
        ScanResult result;
        while (true) {
            ScanQueue.QueueItem qi = this.mQueue.getNext();
            if (!Globals.isSystemPath(qi.target) && (result = doScan(qi)) != null) {
                result.queue = qi.queue;
                if (!(this.mService.mLiveThreat == null || result.result == ScanResult.Result.CLEAN)) {
                    saveInfection(result);
                }
                if (qi.queue == 2) {
                    this.mLogger.logInstall(result);
                    this.mService.mNotifier.notifyPackage(result);
                    if (result.result != ScanResult.Result.CLEAN) {
                        this.mService.mPackageResults.add(result);
                    }
                } else if (qi.queue == 1) {
                    this.mLogger.logAccess(result);
                    if (result.result != ScanResult.Result.CLEAN) {
                        this.mService.mNotifier.notifyOnAccess(result);
                        this.mService.mAccessResults.add(result);
                    }
                } else {
                    this.mLogger.logDemand(result);
                    if (this.mService.mtReplyTo[0] != null) {
                        Message reply = Message.obtain((Handler) null, 1);
                        reply.obj = new ScanResult(result);
                        this.mService.sendReply(0, reply);
                    }
                    if (this.mService.mQueue.mDemandScans) {
                        this.mService.mDemandResults.add(result);
                    } else {
                        result.recycle();
                    }
                }
            }
        }
    }

    private ScanResult doScan(ScanQueue.QueueItem item) {
        if (item.type == 2) {
            try {
                ScanResult result = this.mScanner.scan(this.mService.getPackageManager().getApplicationInfo(item.target, 0).sourceDir, item.type);
                result.path = item.target;
                return result;
            } catch (PackageManager.NameNotFoundException e) {
                Log.d("ZonerAV", "cannot get package source for " + item.target);
                return null;
            }
        } else if (item.type != 1) {
            return this.mScanner.scan(item.target, item.type);
        } else {
            openDir(item.target);
            return null;
        }
    }

    private void openDir(String path) {
        File[] files = new File(path).listFiles();
        if (files != null) {
            for (File file : files) {
                try {
                    this.mQueue.addDirContents(file.getCanonicalPath(), file.isDirectory());
                } catch (Exception e) {
                }
            }
        }
    }

    private void saveInfection(ScanResult result) {
        try {
            FileOutputStream fos = this.mService.openFileOutput("livethreat.log", 32768);
            try {
                fos.write(("infection=" + Logger.getInfections(result) + "\n").getBytes());
            } catch (IOException e) {
                Log.d("ZonerAV", "LT: cannot write to storage");
            }
            try {
                fos.close();
            } catch (IOException e2) {
                Log.d("ZonerAV", "LT: cannot close storage after writing");
            }
        } catch (FileNotFoundException e3) {
            Log.d("ZonerAV", "LT: cannot open storage for writing");
        }
    }
}
