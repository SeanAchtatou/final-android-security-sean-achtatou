package com.zoner.android.antivirus;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: DbScanner */
final class HashInputStream extends InputStream {
    private MessageDigest mHash;
    private BufferedInputStream mInput;

    HashInputStream(InputStream in, String algorithm) throws NoSuchAlgorithmException {
        this.mInput = new BufferedInputStream(in);
        this.mHash = MessageDigest.getInstance(algorithm);
        this.mHash.reset();
    }

    /* access modifiers changed from: package-private */
    public byte[] digest() {
        return this.mHash.digest();
    }

    public void close() throws IOException {
        this.mInput.close();
        super.close();
    }

    public int read() throws IOException {
        int val = this.mInput.read();
        if (val != -1) {
            this.mHash.update((byte) val);
        }
        return val;
    }

    public int read(byte[] buffer, int offset, int length) throws IOException {
        int len = this.mInput.read(buffer, offset, length);
        if (len > 0) {
            this.mHash.update(buffer, offset, len);
        }
        return len;
    }

    public int read(byte[] b) throws IOException {
        int len = this.mInput.read(b);
        if (len > 0) {
            this.mHash.update(b, 0, len);
        }
        return len;
    }
}
