package com.zoner.android.antivirus;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import com.zoner.android.antivirus.DbPhoneFilter;
import java.io.File;

public class Globals {
    public static final boolean DEF_BLOCK_ENABLE = false;
    public static final int DEF_BLOCK_KNOWN_IN = DbPhoneFilter.FilterList.Mode.Allow.ordinal();
    public static final int DEF_BLOCK_KNOWN_OUT = DbPhoneFilter.FilterList.Mode.Ask.ordinal();
    public static final int DEF_BLOCK_KNOWN_SMS = DbPhoneFilter.FilterList.Mode.Allow.ordinal();
    public static final boolean DEF_BLOCK_PARENTAL = false;
    public static final String DEF_BLOCK_PASSWORD = "";
    public static final int DEF_BLOCK_UNKNOWN_IN = DbPhoneFilter.FilterList.Mode.Allow.ordinal();
    public static final int DEF_BLOCK_UNKNOWN_OUT = DbPhoneFilter.FilterList.Mode.Ask.ordinal();
    public static final int DEF_BLOCK_UNKNOWN_SMS = DbPhoneFilter.FilterList.Mode.Allow.ordinal();
    public static final boolean DEF_BLOCK_WHITELIST = true;
    public static final boolean DEF_MISC_BOOT = true;
    public static final boolean DEF_MISC_ICON = true;
    public static final boolean DEF_MISC_LIVETHREAT = true;
    public static final boolean DEF_SCAN_MOUNT = false;
    public static final boolean DEF_SCAN_ONACCESS = true;
    public static final boolean DEF_SCAN_PACKAGES = true;
    public static final String DEF_UPDATE_FREQ = "24";
    public static final long DEF_UPDATE_LAST = 0;
    public static final String PREF_BLOCK_ENABLE = "prefBlock";
    public static final String PREF_BLOCK_KNOWN_IN = "prefBlockKnownIn";
    public static final String PREF_BLOCK_KNOWN_OUT = "prefBlockKnownOut";
    public static final String PREF_BLOCK_KNOWN_SMS = "prefBlockKnownSms";
    public static final String PREF_BLOCK_PARENTAL = "prefBlockLock";
    public static final String PREF_BLOCK_PASSWORD = "prefBlockPass";
    public static final String PREF_BLOCK_UNKNOWN_IN = "prefBlockUnknownIn";
    public static final String PREF_BLOCK_UNKNOWN_OUT = "prefBlockUnknownOut";
    public static final String PREF_BLOCK_UNKNOWN_SMS = "prefBlockUnknownSms";
    public static final String PREF_BLOCK_WHITELIST = "prefBlockWhite";
    public static final String PREF_MISC_BOOT = "prefMiscBoot";
    public static final String PREF_MISC_HOME = "prefMiscHomePath";
    public static final String PREF_MISC_ICON = "prefMiscIcon";
    public static final String PREF_MISC_LIVETHREAT = "prefMiscLT";
    public static final String PREF_SCAN_MOUNT = "prefScanMount";
    public static final String PREF_SCAN_ONACCESS = "prefScanOA";
    public static final String PREF_SCAN_PACKAGES = "prefScanPkg";
    public static final String PREF_UPDATE_FREQ = "prefUpdateFreq";
    public static final String PREF_UPDATE_LAST = "prefUpdateLast";
    public static boolean gGUIMain = false;
    public static boolean gUpdating = false;

    public static String getContactName(Context context, String number) {
        Cursor phones = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number)), new String[]{"display_name"}, null, null, null);
        if (phones == null || !phones.moveToFirst()) {
            return DEF_BLOCK_PASSWORD;
        }
        return phones.getString(0);
    }

    public static boolean isSystemPath(String path) {
        try {
            String canonicalPath = new File(path).getCanonicalPath();
            if (canonicalPath.startsWith("/dev/") || canonicalPath.startsWith("/proc/") || canonicalPath.startsWith("/sys/")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public static int toInt(String number, int defValue) {
        try {
            return Integer.parseInt(number);
        } catch (Exception e) {
            return defValue;
        }
    }

    public static long toLong(String number, long defValue) {
        try {
            return Long.parseLong(number);
        } catch (Exception e) {
            return defValue;
        }
    }

    public static double toDouble(String number, double defValue) {
        try {
            return Double.parseDouble(number);
        } catch (Exception e) {
            return defValue;
        }
    }
}
