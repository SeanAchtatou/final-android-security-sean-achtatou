package com.zoner.android.antivirus;

import android.app.ListActivity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SimpleAdapter;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ActLog extends ListActivity {
    private static String mLogPath;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getListView().setFastScrollEnabled(true);
        mLogPath = Logger.getPath(getPackageName());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        refreshList();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.log, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.log_refresh /*2131361976*/:
                refreshList();
                return true;
            case R.id.log_delete /*2131361977*/:
                Logger.deleteFile(getPackageName());
                refreshList();
                return true;
            default:
                return false;
        }
    }

    private void refreshList() {
        new ReadLogTask(this, null).execute(this);
    }

    private class ReadLogTask extends AsyncTask<Context, Void, List<Map<String, Object>>> {
        private Context ctx;

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((List<Map<String, Object>>) ((List) obj));
        }

        private ReadLogTask() {
        }

        /* synthetic */ ReadLogTask(ActLog actLog, ReadLogTask readLogTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public List<Map<String, Object>> doInBackground(Context... args) {
            this.ctx = args[0];
            return ActLog.this.getLog();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<Map<String, Object>> result) {
            List<Map<String, Object>> list = result;
            ActLog.this.setListAdapter(new SimpleAdapter(this.ctx, list, R.layout.log_row, new String[]{"icon", "header", "message"}, new int[]{R.id.log_row_icon, R.id.log_row_header, R.id.log_row_message}));
        }
    }

    /* access modifiers changed from: private */
    public List<Map<String, Object>> getLog() {
        LinkedList<Map<String, Object>> list = new LinkedList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(mLogPath), 1024);
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    reader.close();
                    return list;
                }
                String[] items = line.split("\t");
                if (items.length >= 4) {
                    int icon = R.drawable.log_unknown;
                    switch (items[0].charAt(0)) {
                        case 'C':
                            icon = R.drawable.log_clean;
                            break;
                        case 'D':
                            icon = R.drawable.log_debug;
                            break;
                        case 'E':
                            icon = R.drawable.log_error;
                            break;
                        case 'I':
                            icon = R.drawable.log_info;
                            break;
                        case 'T':
                            icon = R.drawable.log_threat;
                            break;
                        case 'W':
                            icon = R.drawable.log_warning;
                            break;
                    }
                    int hdr = R.string.log_unknown;
                    if (items[2].equals(Logger.PACKAGE_INFECTED)) {
                        hdr = R.string.log_pkg_infected;
                    } else if (items[2].equals(Logger.PACKAGE_CLEAN)) {
                        hdr = R.string.log_pkg_clean;
                    } else if (items[2].equals(Logger.FILE_INFECTED)) {
                        hdr = R.string.log_file_infected;
                    } else if (items[2].equals(Logger.FILE_CLEAN)) {
                        hdr = R.string.log_file_clean;
                    } else if (items[2].equals(Logger.UPDATE_ERROR)) {
                        hdr = R.string.log_update_error;
                    } else if (items[2].equals(Logger.UPDATE_OUTDATED)) {
                        hdr = R.string.log_update_outdated;
                    } else if (items[2].equals(Logger.UPDATE_UP2DATE)) {
                        hdr = R.string.log_update_up2date;
                    } else if (items[2].equals(Logger.UPDATE_SUCCESS)) {
                        hdr = R.string.log_update_success;
                    }
                    Map<String, Object> item = new HashMap<>();
                    item.put("icon", Integer.valueOf(icon));
                    item.put("header", String.valueOf(items[1]) + " - " + getString(hdr));
                    item.put("message", items[3]);
                    list.addFirst(item);
                }
            }
        } catch (FileNotFoundException e) {
            Log.d("ZonerAV", "Log: No log file");
            Map<String, Object> item2 = new HashMap<>();
            item2.put("icon", Integer.valueOf((int) R.drawable.log_info));
            item2.put("header", getString(R.string.log_empty));
            item2.put("message", Globals.DEF_BLOCK_PASSWORD);
            list.add(item2);
            return list;
        } catch (IOException e2) {
            Log.d("ZonerAV", "Log: IO read exception");
            Map<String, Object> item22 = new HashMap<>();
            item22.put("icon", Integer.valueOf((int) R.drawable.log_info));
            item22.put("header", getString(R.string.log_empty));
            item22.put("message", Globals.DEF_BLOCK_PASSWORD);
            list.add(item22);
            return list;
        }
    }
}
