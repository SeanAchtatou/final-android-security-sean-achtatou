package com.zoner.android.antivirus;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.zoner.android.antivirus.DbPhoneFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActCallsLog extends Activity {
    ListView mLogView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mLogView = new ListView(this);
        setContentView(this.mLogView);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        refreshLog();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.log, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.log_refresh /*2131361976*/:
                refreshLog();
                return true;
            case R.id.log_delete /*2131361977*/:
                new DbPhoneFilter(this).clearAllLogs();
                refreshLog();
                return true;
            default:
                return false;
        }
    }

    private void refreshLog() {
        new ReadLogTask(this, null).execute(this);
    }

    private class ReadLogTask extends AsyncTask<Context, Void, List<Map<String, Object>>> {
        private Context ctx;

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((List<Map<String, Object>>) ((List) obj));
        }

        private ReadLogTask() {
        }

        /* synthetic */ ReadLogTask(ActCallsLog actCallsLog, ReadLogTask readLogTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public List<Map<String, Object>> doInBackground(Context... args) {
            this.ctx = args[0];
            return ActCallsLog.this.getLog();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<Map<String, Object>> result) {
            List<Map<String, Object>> list = result;
            ActCallsLog.this.mLogView.setAdapter((ListAdapter) new SimpleAdapter(this.ctx, list, R.layout.log_row, new String[]{"icon", "header", "message"}, new int[]{R.id.log_row_icon, R.id.log_row_header, R.id.log_row_message}));
        }
    }

    /* access modifiers changed from: private */
    public List<Map<String, Object>> getLog() {
        DbPhoneFilter.LogsIterator logs;
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        try {
            logs = new DbPhoneFilter(this).queryLogs();
        } catch (Exception e) {
            logs = null;
        }
        if (logs == null || logs.isEmpty()) {
            HashMap hashMap = new HashMap();
            hashMap.put("icon", Integer.valueOf((int) R.drawable.log_info));
            hashMap.put("header", getString(R.string.log_empty));
            hashMap.put("message", Globals.DEF_BLOCK_PASSWORD);
            list.add(hashMap);
            if (logs != null) {
                logs.close();
            }
        } else {
            while (logs.moveToNext()) {
                HashMap hashMap2 = new HashMap();
                int type = logs.getType().ordinal();
                int direction = logs.getDirection().ordinal();
                int action = logs.getAction().ordinal();
                int[][][] icons = {new int[][]{new int[]{R.drawable.filter_callin_allow, R.drawable.filter_callin_block, R.drawable.filter_callin_block}, new int[]{R.drawable.filter_callout_allow, R.drawable.filter_callout_block, R.drawable.filter_callout_block}}, new int[][]{new int[]{R.drawable.filter_sms_allow, R.drawable.filter_sms_block, R.drawable.filter_sms_block}, new int[]{R.drawable.filter_sms_allow, R.drawable.filter_sms_block, R.drawable.filter_sms_block}}, new int[][]{new int[]{R.drawable.filter_sms_allow, R.drawable.filter_sms_block, R.drawable.filter_sms_block}, new int[]{R.drawable.filter_sms_allow, R.drawable.filter_sms_block, R.drawable.filter_sms_block}}};
                int[] actions = {R.string.calls_log_allowed, R.string.calls_log_denied, R.string.calls_log_hanged};
                int[][] types = {new int[]{R.string.calls_log_callin, R.string.calls_log_sms, R.string.calls_log_mms}, new int[]{R.string.calls_log_callout, R.string.calls_log_sms, R.string.calls_log_mms}};
                String number = logs.getPhoneNumber(false);
                String name = Globals.getContactName(this, number);
                if (name.length() == 0) {
                    name = getString(R.string.callfilter_unknown_contact);
                }
                hashMap2.put("icon", Integer.valueOf(icons[type][direction][action]));
                hashMap2.put("header", String.valueOf(logs.getTimestampStr()) + " - " + getString(types[direction][type]) + " " + getString(actions[action]));
                hashMap2.put("message", String.valueOf(number) + " (" + name + ")");
                list.add(hashMap2);
            }
            logs.close();
        }
        return list;
    }
}
