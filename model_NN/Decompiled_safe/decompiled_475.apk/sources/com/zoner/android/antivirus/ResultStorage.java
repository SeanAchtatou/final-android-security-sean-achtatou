package com.zoner.android.antivirus;

import com.zoner.android.antivirus.ScanResult;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

public class ResultStorage {
    private TreeSet<ScanResult> mData = new TreeSet<>(new Comparator<ScanResult>() {
        public int compare(ScanResult r1, ScanResult r2) {
            return r1.path.compareTo(r2.path);
        }
    });
    private boolean mResolving = false;
    private boolean mResults = false;

    public class Storage {
        public boolean hasResults;
        public SortedSet<ScanResult> results;

        Storage(TreeSet<ScanResult> results2, boolean hasResults2) {
            this.results = new TreeSet((SortedSet) results2);
            this.hasResults = hasResults2;
        }
    }

    public synchronized void add(ScanResult result) {
        this.mResults = true;
        if (result.result != ScanResult.Result.CLEAN) {
            this.mData.add(result);
        } else {
            result.recycle();
        }
    }

    public synchronized void remove(ScanResult result) {
        this.mData.remove(result);
    }

    public synchronized Storage get() {
        boolean hasResults;
        hasResults = this.mResults || this.mResolving;
        this.mResolving = hasResults;
        this.mResults = false;
        return new Storage(this.mData, hasResults);
    }

    public synchronized boolean hasResults() {
        return this.mResults || this.mResolving;
    }

    public synchronized int cInfections() {
        return this.mData.size();
    }

    public synchronized void resolve() {
        this.mResolving = false;
    }
}
