package com.zoner.android.antivirus;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.PhoneNumberUtils;
import android.text.format.DateFormat;
import android.util.Log;
import java.util.Calendar;
import java.util.Date;

public class DbPhoneFilter {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$DbPhoneFilter$FilterList$Mode = null;
    private static final String DATABASE_NAME = "phonefltr.db";
    private static final int DATABASE_VERSION = 1;
    private final Context mContext;
    /* access modifiers changed from: private */
    public final DbOpenHelper mDbOpenHelper;

    static /* synthetic */ int[] $SWITCH_TABLE$com$zoner$android$antivirus$DbPhoneFilter$FilterList$Mode() {
        int[] iArr = $SWITCH_TABLE$com$zoner$android$antivirus$DbPhoneFilter$FilterList$Mode;
        if (iArr == null) {
            iArr = new int[FilterList.Mode.values().length];
            try {
                iArr[FilterList.Mode.Allow.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[FilterList.Mode.Ask.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[FilterList.Mode.Deny.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[FilterList.Mode.None.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[FilterList.Mode.Once.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            $SWITCH_TABLE$com$zoner$android$antivirus$DbPhoneFilter$FilterList$Mode = iArr;
        }
        return iArr;
    }

    public DbPhoneFilter(Context context) {
        this.mDbOpenHelper = new DbOpenHelper(context);
        this.mContext = context;
    }

    private String getDateTimeStr(long time) {
        Date d = new Date(time);
        return String.valueOf(DateFormat.getDateFormat(this.mContext).format(d)) + " " + DateFormat.getTimeFormat(this.mContext).format(d);
    }

    public enum Type {
        Call,
        SMS,
        MMS;

        public static final Type getEnum(int ordinal) {
            return ((Type[]) Type.class.getEnumConstants())[ordinal];
        }
    }

    public enum Direction {
        Incoming,
        Outgoing;

        public static final Direction getEnum(int ordinal) {
            return ((Direction[]) Direction.class.getEnumConstants())[ordinal];
        }
    }

    private static class DbColumns {
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_PHONE = "phone";

        private DbColumns() {
        }

        /* synthetic */ DbColumns(DbColumns dbColumns) {
            this();
        }
    }

    public static final class FilterList extends DbColumns {
        public static final String COLUMN_BLOCKS = "blocks";
        public static final String COLUMN_CALL_IN = "call_in";
        public static final String COLUMN_CALL_OUT = "call_out";
        public static final String COLUMN_CREATED = "created";
        public static final String COLUMN_CREATED_STR = "created_str";
        public static final String COLUMN_MMS_IN = "mms_in";
        public static final String COLUMN_NORMAL_NUMBER = "norm_number";
        public static final String COLUMN_SMS_IN = "sms_in";
        public static final String TABLE_NAME = "filter";

        public FilterList() {
            super(null);
        }

        public enum Mode {
            None,
            Allow,
            Deny,
            Ask,
            Once;

            public static final Mode getEnum(int ordinal) {
                return ((Mode[]) Mode.class.getEnumConstants())[ordinal];
            }
        }
    }

    public static final class Logs extends DbColumns {
        public static final String COLUMN_ACTION = "action";
        public static final String COLUMN_DIRECTION = "direction";
        public static final String COLUMN_LOGED = "loged";
        public static final String COLUMN_LOGED_STR = "loged_str";
        public static final String COLUMN_TYPE = "type";
        public static final String TABLE_NAME = "logs";

        public Logs() {
            super(null);
        }

        public enum Action {
            Allowed,
            Denied,
            Hanged;

            public static final Action getEnum(int ordinal) {
                return ((Action[]) Action.class.getEnumConstants())[ordinal];
            }
        }
    }

    private static final class DbOpenHelper extends SQLiteOpenHelper {
        public DbOpenHelper(Context context) {
            super(context, DbPhoneFilter.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE filter (id INTEGER PRIMARY KEY,phone TEXT,norm_number TEXT UNIQUE,created INTEGER,created_str STRING,call_in INTEGER,call_out INTEGER,sms_in INTEGER,mms_in INTEGER,blocks INTEGER);");
            db.execSQL("CREATE TABLE logs (id INTEGER PRIMARY KEY,phone TEXT,type INTEGER,direction INTEGER,action INTEGER,loged INTEGER,loged_str STRING);");
            db.execSQL("CREATE INDEX filter_index ON filter (phone);");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS filter");
            db.execSQL("DROP TABLE IF EXISTS logs");
            onCreate(db);
        }
    }

    private abstract class AbstractIterator {
        protected Cursor mCursor;
        private SQLiteDatabase mDb;
        protected int mIdIdx = this.mCursor.getColumnIndex(DbColumns.COLUMN_ID);
        protected int mPhoneIdx = this.mCursor.getColumnIndex(DbColumns.COLUMN_PHONE);
        protected int mTimestamp;
        protected int mTimestampStr;

        public AbstractIterator(String table, String[] columns, String selection, String[] selectionArgs, String orderBy) throws Exception {
            this.mDb = DbPhoneFilter.this.mDbOpenHelper.getReadableDatabase();
            this.mCursor = this.mDb.query(table, columns, selection, selectionArgs, null, null, orderBy);
        }

        public int getCount() {
            return this.mCursor.getCount();
        }

        public boolean isEmpty() {
            return this.mCursor.getCount() == 0;
        }

        public boolean moveToNext() {
            return this.mCursor.moveToNext();
        }

        public long getID() {
            if (this.mIdIdx < 0) {
                return -1;
            }
            return this.mCursor.getLong(this.mIdIdx);
        }

        public String getPhoneNumber(boolean format) {
            if (this.mPhoneIdx < 0) {
                return null;
            }
            String phone = this.mCursor.getString(this.mPhoneIdx);
            if (phone == null) {
                return "Unknown";
            }
            return format ? PhoneNumberUtils.formatNumber(phone) : phone;
        }

        public Date getTimestamp() {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(this.mCursor.getLong(this.mTimestamp));
            return calendar.getTime();
        }

        public String getTimestampStr() {
            return this.mCursor.getString(this.mTimestampStr);
        }

        /* access modifiers changed from: protected */
        public void close() {
            this.mCursor.close();
            this.mDb.close();
            this.mIdIdx = -1;
            this.mPhoneIdx = -1;
            this.mTimestamp = -1;
            this.mTimestampStr = -1;
        }
    }

    public final class FilterIterator extends AbstractIterator {
        private int mBlocksIdx;
        private int mCallInIdx;
        private int mCallOutIdx;
        private int mMmsInIdx;
        private int mNormalNumber = this.mCursor.getColumnIndex(FilterList.COLUMN_NORMAL_NUMBER);
        private int mSmsInIdx;

        public FilterIterator(String[] columns, String selection, String[] selectionArgs, String orderBy) throws Exception {
            super(FilterList.TABLE_NAME, columns, selection, selectionArgs, orderBy);
            this.mTimestamp = this.mCursor.getColumnIndex(FilterList.COLUMN_CREATED);
            this.mTimestampStr = this.mCursor.getColumnIndex(FilterList.COLUMN_CREATED_STR);
            this.mCallInIdx = this.mCursor.getColumnIndex(FilterList.COLUMN_CALL_IN);
            this.mCallOutIdx = this.mCursor.getColumnIndex(FilterList.COLUMN_CALL_OUT);
            this.mSmsInIdx = this.mCursor.getColumnIndex(FilterList.COLUMN_SMS_IN);
            this.mMmsInIdx = this.mCursor.getColumnIndex(FilterList.COLUMN_MMS_IN);
            this.mBlocksIdx = this.mCursor.getColumnIndex(FilterList.COLUMN_BLOCKS);
        }

        public String getNormalizedPhoneNumber() {
            if (this.mNormalNumber < 0) {
                return null;
            }
            return this.mCursor.getString(this.mNormalNumber);
        }

        /* access modifiers changed from: private */
        public FilterList.Mode getMode(int idx) {
            if (idx < 0) {
                return null;
            }
            return FilterList.Mode.getEnum(this.mCursor.getInt(idx));
        }

        public FilterList.Mode getCallInMode() {
            return getMode(this.mCallInIdx);
        }

        public FilterList.Mode getCallOutMode() {
            return getMode(this.mCallOutIdx);
        }

        public FilterList.Mode getSmsInMode() {
            return getMode(this.mSmsInIdx);
        }

        public FilterList.Mode getMmsInMode() {
            return getMode(this.mMmsInIdx);
        }

        public long getBlocks() {
            return this.mCursor.getLong(this.mBlocksIdx);
        }

        public void close() {
            this.mNormalNumber = -1;
            this.mCallInIdx = -1;
            this.mCallOutIdx = -1;
            this.mSmsInIdx = -1;
            this.mMmsInIdx = -1;
            this.mBlocksIdx = -1;
            super.close();
        }
    }

    public final class LogsIterator extends AbstractIterator {
        private int mActionIdx = this.mCursor.getColumnIndex(Logs.COLUMN_ACTION);
        private int mDirectionIdx = this.mCursor.getColumnIndex(Logs.COLUMN_DIRECTION);
        private int mTypeIdx = this.mCursor.getColumnIndex(Logs.COLUMN_TYPE);

        public LogsIterator(String[] columns, String selection, String[] selectionArgs, String orderBy) throws Exception {
            super(Logs.TABLE_NAME, columns, selection, selectionArgs, orderBy);
            this.mTimestamp = this.mCursor.getColumnIndex(Logs.COLUMN_LOGED);
            this.mTimestampStr = this.mCursor.getColumnIndex(Logs.COLUMN_LOGED_STR);
        }

        public Type getType() {
            if (this.mTypeIdx < 0) {
                return null;
            }
            return Type.getEnum(this.mCursor.getInt(this.mTypeIdx));
        }

        public Direction getDirection() {
            if (this.mDirectionIdx < 0) {
                return null;
            }
            return Direction.getEnum(this.mCursor.getInt(this.mDirectionIdx));
        }

        public Logs.Action getAction() {
            if (this.mActionIdx < 0) {
                return null;
            }
            return Logs.Action.getEnum(this.mCursor.getInt(this.mActionIdx));
        }

        public void close() {
            this.mTypeIdx = -1;
            this.mDirectionIdx = -1;
            this.mActionIdx = -1;
            super.close();
        }
    }

    public static String normalizeNumber(String phone) {
        return PhoneNumberUtils.getStrippedReversed(phone);
    }

    public static String unformatNumber(String phone) {
        return PhoneNumberUtils.stripSeparators(phone);
    }

    private long addFilter(ContentValues values) {
        long now = System.currentTimeMillis();
        String nowStr = getDateTimeStr(now);
        try {
            SQLiteDatabase db = this.mDbOpenHelper.getWritableDatabase();
            String phone = (String) values.get(DbColumns.COLUMN_PHONE);
            if (phone != null) {
                values.put(FilterList.COLUMN_NORMAL_NUMBER, normalizeNumber(phone));
            }
            values.put(FilterList.COLUMN_CREATED, Long.valueOf(now));
            values.put(FilterList.COLUMN_CREATED_STR, nowStr);
            long id = db.insert(FilterList.TABLE_NAME, null, values);
            db.close();
            return id;
        } catch (Exception e) {
            return -1;
        }
    }

    public long addFilter(String phone, FilterList.Mode callIn, FilterList.Mode callOut, FilterList.Mode smsIn, FilterList.Mode mmsIn) {
        ContentValues values = new ContentValues();
        if (phone != null) {
            values.put(DbColumns.COLUMN_PHONE, unformatNumber(phone));
        }
        if (callIn != null) {
            values.put(FilterList.COLUMN_CALL_IN, Integer.valueOf(callIn.ordinal()));
        }
        if (callOut != null) {
            values.put(FilterList.COLUMN_CALL_OUT, Integer.valueOf(callOut.ordinal()));
        }
        if (smsIn != null) {
            values.put(FilterList.COLUMN_SMS_IN, Integer.valueOf(smsIn.ordinal()));
        }
        if (mmsIn != null) {
            values.put(FilterList.COLUMN_MMS_IN, Integer.valueOf(mmsIn.ordinal()));
        }
        return addFilter(values);
    }

    private long addFilter(String modeColumn, String phone, FilterList.Mode mode) {
        try {
            FilterIterator filter = queryFilterList(new String[]{DbColumns.COLUMN_ID, modeColumn}, "phone=?", new String[]{phone}, null);
            long id = -1;
            FilterList.Mode modeOld = FilterList.Mode.None;
            if (filter.moveToNext()) {
                id = filter.getID();
                modeOld = filter.getMode(1);
            }
            filter.close();
            if (id == -1) {
                int modeNone = FilterList.Mode.None.ordinal();
                ContentValues values = new ContentValues();
                values.put(DbColumns.COLUMN_PHONE, unformatNumber(phone));
                values.put(FilterList.COLUMN_CALL_IN, Integer.valueOf(modeNone));
                values.put(FilterList.COLUMN_CALL_OUT, Integer.valueOf(modeNone));
                values.put(FilterList.COLUMN_SMS_IN, Integer.valueOf(modeNone));
                values.put(FilterList.COLUMN_MMS_IN, Integer.valueOf(modeNone));
                values.put(modeColumn, Integer.valueOf(mode.ordinal()));
                return addFilter(values);
            } else if (modeOld != FilterList.Mode.None || !updateFilter(modeColumn, id, null, mode)) {
                return -1;
            } else {
                return id;
            }
        } catch (Exception e) {
            return -1;
        }
    }

    public long addCallInFilter(String phone, FilterList.Mode mode) {
        return addFilter(FilterList.COLUMN_CALL_IN, phone, mode);
    }

    public long addCallOutFilter(String phone, FilterList.Mode mode) {
        return addFilter(FilterList.COLUMN_CALL_OUT, phone, mode);
    }

    public long addSmsInFilter(String phone, FilterList.Mode mode) {
        return addFilter(FilterList.COLUMN_SMS_IN, phone, mode);
    }

    public long addMmsInFilter(String phone, FilterList.Mode mode) {
        return addFilter(FilterList.COLUMN_MMS_IN, phone, mode);
    }

    public long addCallInFilter(long id, String phone, FilterList.Mode mode) {
        if (id == -1) {
            return addFilter(FilterList.COLUMN_CALL_IN, phone, mode);
        }
        if (updateCallInFilter(id, null, mode)) {
            return id;
        }
        return -1;
    }

    public long addCallOutFilter(long id, String phone, FilterList.Mode mode) {
        if (id == -1) {
            return addFilter(FilterList.COLUMN_CALL_OUT, phone, mode);
        }
        if (updateCallOutFilter(id, null, mode)) {
            return id;
        }
        return -1;
    }

    public long addSmsInFilter(long id, String phone, FilterList.Mode mode) {
        if (id == -1) {
            return addFilter(FilterList.COLUMN_SMS_IN, phone, mode);
        }
        if (updateSmsInFilter(id, null, mode)) {
            return id;
        }
        return -1;
    }

    public long addMmsInFilter(long id, String phone, FilterList.Mode mode) {
        if (id == -1) {
            return addFilter(FilterList.COLUMN_MMS_IN, phone, mode);
        }
        if (updateSmsInFilter(id, null, mode)) {
            return id;
        }
        return -1;
    }

    public boolean delFilter(long id) {
        boolean result;
        try {
            SQLiteDatabase db = this.mDbOpenHelper.getWritableDatabase();
            if (db.delete(FilterList.TABLE_NAME, "id=" + id, null) > 0) {
                result = true;
            } else {
                result = false;
            }
            db.close();
            return result;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean clearAllFilters() {
        try {
            SQLiteDatabase db = this.mDbOpenHelper.getWritableDatabase();
            db.delete(FilterList.TABLE_NAME, null, null);
            db.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean updateFilter(long id, ContentValues values) {
        boolean result;
        long now = System.currentTimeMillis();
        String nowStr = getDateTimeStr(now);
        try {
            SQLiteDatabase db = this.mDbOpenHelper.getWritableDatabase();
            String phone = (String) values.get(DbColumns.COLUMN_PHONE);
            if (phone != null) {
                values.put(FilterList.COLUMN_NORMAL_NUMBER, normalizeNumber(phone));
            }
            values.put(FilterList.COLUMN_CREATED, Long.valueOf(now));
            values.put(FilterList.COLUMN_CREATED_STR, nowStr);
            if (db.update(FilterList.TABLE_NAME, values, "id=" + id, null) > 0) {
                result = true;
            } else {
                result = false;
            }
            db.close();
            return result;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean updateFilter(long id, String phone, FilterList.Mode callIn, FilterList.Mode callOut, FilterList.Mode smsIn, FilterList.Mode mmsIn) {
        ContentValues values = new ContentValues();
        if (phone != null) {
            values.put(DbColumns.COLUMN_PHONE, unformatNumber(phone));
        }
        if (callIn != null) {
            values.put(FilterList.COLUMN_CALL_IN, Integer.valueOf(callIn.ordinal()));
        }
        if (callOut != null) {
            values.put(FilterList.COLUMN_CALL_OUT, Integer.valueOf(callOut.ordinal()));
        }
        if (smsIn != null) {
            values.put(FilterList.COLUMN_SMS_IN, Integer.valueOf(smsIn.ordinal()));
        }
        if (mmsIn != null) {
            values.put(FilterList.COLUMN_MMS_IN, Integer.valueOf(mmsIn.ordinal()));
        }
        return updateFilter(id, values);
    }

    private boolean updateFilter(String modeColumn, long id, String phone, FilterList.Mode mode) {
        ContentValues values = new ContentValues();
        if (phone != null) {
            values.put(DbColumns.COLUMN_PHONE, unformatNumber(phone));
        }
        values.put(modeColumn, Integer.valueOf(mode.ordinal()));
        return updateFilter(id, values);
    }

    public boolean updateCallInFilter(long id, String phone, FilterList.Mode mode) {
        return updateFilter(FilterList.COLUMN_CALL_IN, id, phone, mode);
    }

    public boolean updateCallOutFilter(long id, String phone, FilterList.Mode mode) {
        return updateFilter(FilterList.COLUMN_CALL_OUT, id, phone, mode);
    }

    public boolean updateSmsInFilter(long id, String phone, FilterList.Mode mode) {
        return updateFilter(FilterList.COLUMN_SMS_IN, id, phone, mode);
    }

    public boolean updateMmsInFilter(long id, String phone, FilterList.Mode mode) {
        return updateFilter(FilterList.COLUMN_MMS_IN, id, phone, mode);
    }

    public static final class FilterResult implements Parcelable {
        public static final Parcelable.Creator<FilterResult> CREATOR = new Parcelable.Creator<FilterResult>() {
            public FilterResult createFromParcel(Parcel in) {
                return new FilterResult(in, null);
            }

            public FilterResult[] newArray(int size) {
                return new FilterResult[size];
            }
        };
        public Direction direction;
        private FilterList.Mode modeOrig;
        public FilterList.Mode modeRes;
        public long phoneId;
        public String phoneNumber;
        public Type type;

        public FilterResult(String phone, Type type2, Direction direction2, FilterList.Mode mode) {
            this(phone, type2, direction2, mode, -1);
        }

        public FilterResult(String phone, Type type2, Direction direction2, FilterList.Mode mode, long phoneId2) {
            if (phone != null && phone.length() == 0) {
                phone = null;
            }
            this.phoneId = phoneId2;
            this.phoneNumber = phone;
            this.type = type2;
            this.direction = direction2;
            this.modeOrig = mode;
            this.modeRes = mode;
        }

        public FilterList.Mode getModeOrig() {
            return this.modeOrig;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel out, int flags) {
            out.writeLong(this.phoneId);
            out.writeString(this.phoneNumber);
            out.writeInt(this.type.ordinal());
            out.writeInt(this.direction.ordinal());
            out.writeInt(this.modeOrig.ordinal());
            out.writeInt(this.modeRes.ordinal());
        }

        private FilterResult(Parcel in) {
            this.phoneId = in.readLong();
            this.phoneNumber = in.readString();
            this.type = Type.getEnum(in.readInt());
            this.direction = Direction.getEnum(in.readInt());
            this.modeOrig = FilterList.Mode.getEnum(in.readInt());
            this.modeRes = FilterList.Mode.getEnum(in.readInt());
        }

        /* synthetic */ FilterResult(Parcel parcel, FilterResult filterResult) {
            this(parcel);
        }
    }

    public FilterResult getFilterResult(String phone, Type type, Direction direction) {
        String[] columns = {DbColumns.COLUMN_ID, DbColumns.COLUMN_PHONE, new String[][]{new String[]{FilterList.COLUMN_CALL_IN, FilterList.COLUMN_CALL_OUT}, new String[]{FilterList.COLUMN_SMS_IN, Globals.DEF_BLOCK_PASSWORD}, new String[]{FilterList.COLUMN_MMS_IN, Globals.DEF_BLOCK_PASSWORD}}[type.ordinal()][direction.ordinal()]};
        String phone2 = unformatNumber(phone);
        try {
            FilterIterator filter = queryFilterList(columns, "norm_number LIKE '" + PhoneNumberUtils.toCallerIDMinMatch(phone2) + "%'", null, null);
            if (filter.isEmpty()) {
                filter.close();
                return null;
            }
            long phoneId = -1;
            FilterList.Mode mode = null;
            while (filter.moveToNext()) {
                String filterPhone = filter.getPhoneNumber(false);
                if (PhoneNumberUtils.compare(filterPhone, phone2)) {
                    phoneId = filter.getID();
                    mode = filter.getMode(2);
                }
                if (filterPhone.equals(phone2)) {
                    break;
                }
            }
            filter.close();
            if (phoneId != -1) {
                return new FilterResult(phone2, type, direction, mode, phoneId);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public FilterResult tryCallIn(String phone) {
        return getFilterResult(phone, Type.Call, Direction.Incoming);
    }

    public FilterResult tryCallOut(String phone) {
        return getFilterResult(phone, Type.Call, Direction.Outgoing);
    }

    public FilterResult trySmsIn(String phone) {
        return getFilterResult(phone, Type.SMS, Direction.Incoming);
    }

    public FilterResult tryMmsIn(String phone) {
        return getFilterResult(phone, Type.MMS, Direction.Incoming);
    }

    public FilterIterator queryFilterList(String[] columns, String selection, String[] selectionArgs, String orderBy) throws Exception {
        return new FilterIterator(columns, selection, selectionArgs, orderBy);
    }

    public FilterIterator queryFilterList() throws Exception {
        return new FilterIterator(null, "phone is NOT NULL", null, null);
    }

    public LogsIterator queryLogs(String[] columns, String selection, String[] selectionArgs, String orderBy) throws Exception {
        return new LogsIterator(columns, selection, selectionArgs, orderBy);
    }

    public LogsIterator queryLogs() throws Exception {
        return new LogsIterator(null, "action=" + Logs.Action.Denied.ordinal(), null, "id DESC");
    }

    private boolean incrementBlocks(String where) {
        try {
            FilterIterator phone = queryFilterList(new String[]{FilterList.COLUMN_BLOCKS}, where, null, null);
            if (!phone.moveToNext()) {
                phone.close();
                return false;
            }
            long blocks = phone.getBlocks();
            phone.close();
            try {
                SQLiteDatabase db = this.mDbOpenHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(FilterList.COLUMN_BLOCKS, Long.valueOf(1 + blocks));
                db.update(FilterList.TABLE_NAME, values, where, null);
                db.close();
                return true;
            } catch (Exception e) {
                return false;
            }
        } catch (Exception e2) {
            return false;
        }
    }

    private Long getTimeStamp(Long timestamp) {
        return Long.valueOf(timestamp != null ? timestamp.longValue() : System.currentTimeMillis());
    }

    public void phoneLog(FilterResult result, Long timestamp) {
        Long timestamp2 = getTimeStamp(timestamp);
        switch ($SWITCH_TABLE$com$zoner$android$antivirus$DbPhoneFilter$FilterList$Mode()[result.modeRes.ordinal()]) {
            case 2:
            default:
                return;
            case DbScanner.ERRFORMAT /*3*/:
                Logs.Action action = Logs.Action.Denied;
                if (result.phoneId != -1) {
                    incrementBlocks("id=" + result.phoneId);
                }
                phoneLog(result.phoneNumber, result.type, result.direction, action, timestamp2);
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void phoneLog(String phone, Type type, Direction direction, Logs.Action action, Long timestamp) {
        if (action != Logs.Action.Hanged) {
            Long timestamp2 = getTimeStamp(timestamp);
            String timeStr = getDateTimeStr(timestamp2.longValue());
            try {
                SQLiteDatabase db = this.mDbOpenHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(DbColumns.COLUMN_PHONE, phone);
                values.put(Logs.COLUMN_TYPE, Integer.valueOf(type.ordinal()));
                values.put(Logs.COLUMN_DIRECTION, Integer.valueOf(direction.ordinal()));
                values.put(Logs.COLUMN_ACTION, Integer.valueOf(action.ordinal()));
                values.put(Logs.COLUMN_LOGED, timestamp2);
                values.put(Logs.COLUMN_LOGED_STR, timeStr);
                db.insert(Logs.TABLE_NAME, null, values);
                db.close();
            } catch (Exception e) {
            }
        } else if (!incrementBlocks("phone is NULL")) {
            try {
                SQLiteDatabase db2 = this.mDbOpenHelper.getWritableDatabase();
                ContentValues values2 = new ContentValues();
                values2.putNull(DbColumns.COLUMN_PHONE);
                values2.put(FilterList.COLUMN_BLOCKS, (Integer) 1);
                db2.insert(FilterList.TABLE_NAME, null, values2);
                db2.close();
            } catch (Exception e2) {
            }
        }
    }

    public void clearAllLogs() {
        try {
            SQLiteDatabase db = this.mDbOpenHelper.getWritableDatabase();
            db.delete(Logs.TABLE_NAME, null, null);
            db.close();
        } catch (Exception e) {
        }
    }

    public String getSuspiciousCallCount() {
        try {
            FilterIterator unknown = queryFilterList(new String[]{FilterList.COLUMN_BLOCKS}, "phone is NULL", null, null);
            long count = !unknown.moveToNext() ? 0 : unknown.getBlocks();
            unknown.close();
            if (count > 0) {
                return new StringBuilder().append(count).toString();
            }
            return null;
        } catch (Exception e) {
            Log.d("ZonerAV", "Exception: getSuspiciousCallCount() e = " + e.getMessage());
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void resetSuspiciousCallCount() {
        try {
            SQLiteDatabase db = this.mDbOpenHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(FilterList.COLUMN_BLOCKS, (Integer) 0);
            db.update(FilterList.TABLE_NAME, values, "phone is NULL", null);
            db.close();
        } catch (Exception e) {
        }
    }

    public void dump() {
        Log.e("zap", "Filter list:");
        try {
            FilterIterator filter = queryFilterList(null, null, null, null);
            while (filter.moveToNext()) {
                Log.e("zap", filter.getID() + ", " + filter.getPhoneNumber(false) + ", " + filter.getNormalizedPhoneNumber() + ", " + filter.getCallInMode().toString() + ", " + filter.getCallOutMode().toString() + ", " + filter.getSmsInMode().toString() + ", " + filter.getMmsInMode().toString() + ", " + filter.getTimestampStr() + ", " + filter.getBlocks());
            }
            filter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            LogsIterator logs = queryLogs();
            Log.e("zap", "Logs:");
            while (logs.moveToNext()) {
                Log.e("zap", logs.getID() + ", " + logs.getPhoneNumber(false) + ", " + logs.getType().toString() + ", " + logs.getDirection().toString() + ", " + logs.getAction().toString() + ", " + logs.getTimestampStr());
            }
            logs.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
