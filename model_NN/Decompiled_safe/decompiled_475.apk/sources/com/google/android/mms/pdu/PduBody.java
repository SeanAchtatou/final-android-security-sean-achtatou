package com.google.android.mms.pdu;

public class PduBody {
    public PduBody() {
        throw new RuntimeException("Stub!");
    }

    public boolean addPart(PduPart pduPart) {
        throw new RuntimeException("Stub!");
    }

    public void addPart(int i, PduPart pduPart) {
        throw new RuntimeException("Stub!");
    }

    public PduPart removePart(int i) {
        throw new RuntimeException("Stub!");
    }

    public void removeAll() {
        throw new RuntimeException("Stub!");
    }

    public PduPart getPart(int i) {
        throw new RuntimeException("Stub!");
    }

    public int getPartIndex(PduPart pduPart) {
        throw new RuntimeException("Stub!");
    }

    public int getPartsNum() {
        throw new RuntimeException("Stub!");
    }

    public PduPart getPartByContentId(String str) {
        throw new RuntimeException("Stub!");
    }

    public PduPart getPartByContentLocation(String str) {
        throw new RuntimeException("Stub!");
    }

    public PduPart getPartByName(String str) {
        throw new RuntimeException("Stub!");
    }

    public PduPart getPartByFileName(String str) {
        throw new RuntimeException("Stub!");
    }
}
