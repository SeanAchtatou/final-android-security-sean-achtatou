package com.google.android.mms.pdu;

import android.net.Uri;

public class PduPart {
    public static final String CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding";
    public static final String P_7BIT = "7bit";
    public static final String P_8BIT = "8bit";
    public static final String P_BASE64 = "base64";
    public static final String P_BINARY = "binary";
    public static final int P_CHARSET = 129;
    public static final int P_COMMENT = 155;
    public static final int P_CONTENT_DISPOSITION = 197;
    public static final int P_CONTENT_ID = 192;
    public static final int P_CONTENT_LOCATION = 142;
    public static final int P_CONTENT_TRANSFER_ENCODING = 200;
    public static final int P_CONTENT_TYPE = 145;
    public static final int P_CREATION_DATE = 147;
    public static final int P_CT_MR_TYPE = 137;
    public static final int P_DEP_COMMENT = 140;
    public static final int P_DEP_CONTENT_DISPOSITION = 174;
    public static final int P_DEP_DOMAIN = 141;
    public static final int P_DEP_FILENAME = 134;
    public static final int P_DEP_NAME = 133;
    public static final int P_DEP_PATH = 143;
    public static final int P_DEP_START = 138;
    public static final int P_DEP_START_INFO = 139;
    public static final int P_DIFFERENCES = 135;
    public static final int P_DISPOSITION_ATTACHMENT = 129;
    public static final int P_DISPOSITION_FROM_DATA = 128;
    public static final int P_DISPOSITION_INLINE = 130;
    public static final int P_DOMAIN = 156;
    public static final int P_FILENAME = 152;
    public static final int P_LEVEL = 130;
    public static final int P_MAC = 146;
    public static final int P_MAX_AGE = 142;
    public static final int P_MODIFICATION_DATE = 148;
    public static final int P_NAME = 151;
    public static final int P_PADDING = 136;
    public static final int P_PATH = 157;
    public static final int P_Q = 128;
    public static final String P_QUOTED_PRINTABLE = "quoted-printable";
    public static final int P_READ_DATE = 149;
    public static final int P_SEC = 145;
    public static final int P_SECURE = 144;
    public static final int P_SIZE = 150;
    public static final int P_START = 153;
    public static final int P_START_INFO = 154;
    public static final int P_TYPE = 131;

    public PduPart() {
        throw new RuntimeException("Stub!");
    }

    public void setData(byte[] bArr) {
        throw new RuntimeException("Stub!");
    }

    public byte[] getData() {
        throw new RuntimeException("Stub!");
    }

    public void setDataUri(Uri uri) {
        throw new RuntimeException("Stub!");
    }

    public Uri getDataUri() {
        throw new RuntimeException("Stub!");
    }

    public void setContentId(byte[] bArr) {
        throw new RuntimeException("Stub!");
    }

    public byte[] getContentId() {
        throw new RuntimeException("Stub!");
    }

    public void setCharset(int i) {
        throw new RuntimeException("Stub!");
    }

    public int getCharset() {
        throw new RuntimeException("Stub!");
    }

    public void setContentLocation(byte[] bArr) {
        throw new RuntimeException("Stub!");
    }

    public byte[] getContentLocation() {
        throw new RuntimeException("Stub!");
    }

    public void setContentDisposition(byte[] bArr) {
        throw new RuntimeException("Stub!");
    }

    public byte[] getContentDisposition() {
        throw new RuntimeException("Stub!");
    }

    public void setContentType(byte[] bArr) {
        throw new RuntimeException("Stub!");
    }

    public byte[] getContentType() {
        throw new RuntimeException("Stub!");
    }

    public void setContentTransferEncoding(byte[] bArr) {
        throw new RuntimeException("Stub!");
    }

    public byte[] getContentTransferEncoding() {
        throw new RuntimeException("Stub!");
    }

    public void setName(byte[] bArr) {
        throw new RuntimeException("Stub!");
    }

    public byte[] getName() {
        throw new RuntimeException("Stub!");
    }

    public void setFilename(byte[] bArr) {
        throw new RuntimeException("Stub!");
    }

    public byte[] getFilename() {
        throw new RuntimeException("Stub!");
    }

    public String generateLocation() {
        throw new RuntimeException("Stub!");
    }
}
