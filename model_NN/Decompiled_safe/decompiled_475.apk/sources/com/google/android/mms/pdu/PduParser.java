package com.google.android.mms.pdu;

import java.io.ByteArrayInputStream;
import java.util.HashMap;

public class PduParser {
    public PduParser(byte[] bArr) {
        throw new RuntimeException("Stub!");
    }

    public GenericPdu parse() {
        throw new RuntimeException("Stub!");
    }

    /* access modifiers changed from: protected */
    public PduHeaders parseHeaders(ByteArrayInputStream byteArrayInputStream) {
        throw new RuntimeException("Stub!");
    }

    protected static PduBody parseParts(ByteArrayInputStream byteArrayInputStream) {
        throw new RuntimeException("Stub!");
    }

    protected static int parseUnsignedInt(ByteArrayInputStream byteArrayInputStream) {
        throw new RuntimeException("Stub!");
    }

    protected static int parseValueLength(ByteArrayInputStream byteArrayInputStream) {
        throw new RuntimeException("Stub!");
    }

    protected static EncodedStringValue parseEncodedStringValue(ByteArrayInputStream byteArrayInputStream) {
        throw new RuntimeException("Stub!");
    }

    protected static byte[] parseWapString(ByteArrayInputStream byteArrayInputStream, int i) {
        throw new RuntimeException("Stub!");
    }

    protected static boolean isTokenCharacter(int i) {
        throw new RuntimeException("Stub!");
    }

    protected static boolean isText(int i) {
        throw new RuntimeException("Stub!");
    }

    protected static byte[] getWapString(ByteArrayInputStream byteArrayInputStream, int i) {
        throw new RuntimeException("Stub!");
    }

    protected static int extractByteValue(ByteArrayInputStream byteArrayInputStream) {
        throw new RuntimeException("Stub!");
    }

    protected static int parseShortInteger(ByteArrayInputStream byteArrayInputStream) {
        throw new RuntimeException("Stub!");
    }

    protected static long parseLongInteger(ByteArrayInputStream byteArrayInputStream) {
        throw new RuntimeException("Stub!");
    }

    protected static long parseIntegerValue(ByteArrayInputStream byteArrayInputStream) {
        throw new RuntimeException("Stub!");
    }

    protected static int skipWapValue(ByteArrayInputStream byteArrayInputStream, int i) {
        throw new RuntimeException("Stub!");
    }

    protected static void parseContentTypeParams(ByteArrayInputStream byteArrayInputStream, HashMap<Integer, Object> hashMap, Integer num) {
        throw new RuntimeException("Stub!");
    }

    protected static byte[] parseContentType(ByteArrayInputStream byteArrayInputStream, HashMap<Integer, Object> hashMap) {
        throw new RuntimeException("Stub!");
    }

    protected static boolean parsePartHeaders(ByteArrayInputStream byteArrayInputStream, PduPart pduPart, int i) {
        throw new RuntimeException("Stub!");
    }

    protected static boolean checkMandatoryHeader(PduHeaders pduHeaders) {
        throw new RuntimeException("Stub!");
    }
}
