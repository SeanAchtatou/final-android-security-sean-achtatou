package com.google.android.mms.pdu;

import com.google.android.mms.InvalidHeaderValueException;

public class GenericPdu {
    public GenericPdu() {
        throw new RuntimeException("Stub!");
    }

    public int getMessageType() {
        throw new RuntimeException("Stub!");
    }

    public void setMessageType(int i) throws InvalidHeaderValueException {
        throw new RuntimeException("Stub!");
    }

    public int getMmsVersion() {
        throw new RuntimeException("Stub!");
    }

    public void setMmsVersion(int i) throws InvalidHeaderValueException {
        throw new RuntimeException("Stub!");
    }

    public EncodedStringValue getFrom() {
        throw new RuntimeException("Stub!");
    }

    public void setFrom(EncodedStringValue encodedStringValue) {
        throw new RuntimeException("Stub!");
    }
}
