package com.rd.Magic_Card_Game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class CardNums extends Activity {
    private ImageButton okbtn;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.cards);
        this.okbtn = (ImageButton) findViewById(R.id.okbtn);
        this.okbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CardNums.this.startActivity(new Intent(CardNums.this, GrpCards1.class));
            }
        });
    }
}
