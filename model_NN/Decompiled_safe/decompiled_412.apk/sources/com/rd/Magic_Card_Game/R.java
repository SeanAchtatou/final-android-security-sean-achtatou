package com.rd.Magic_Card_Game;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int card1 = 2130837504;
        public static final int card2 = 2130837505;
        public static final int card3 = 2130837506;
        public static final int card4 = 2130837507;
        public static final int card5 = 2130837508;
        public static final int cardnums = 2130837509;
        public static final int circlebtn = 2130837510;
        public static final int circlebtn2 = 2130837511;
        public static final int icon = 2130837512;
        public static final int mainbg = 2130837513;
        public static final int nextbtn = 2130837514;
        public static final int nobg = 2130837515;
        public static final int nobtn = 2130837516;
        public static final int num1 = 2130837517;
        public static final int num10 = 2130837518;
        public static final int num11 = 2130837519;
        public static final int num12 = 2130837520;
        public static final int num13 = 2130837521;
        public static final int num14 = 2130837522;
        public static final int num15 = 2130837523;
        public static final int num16 = 2130837524;
        public static final int num17 = 2130837525;
        public static final int num18 = 2130837526;
        public static final int num19 = 2130837527;
        public static final int num2 = 2130837528;
        public static final int num20 = 2130837529;
        public static final int num21 = 2130837530;
        public static final int num22 = 2130837531;
        public static final int num23 = 2130837532;
        public static final int num24 = 2130837533;
        public static final int num25 = 2130837534;
        public static final int num26 = 2130837535;
        public static final int num27 = 2130837536;
        public static final int num28 = 2130837537;
        public static final int num29 = 2130837538;
        public static final int num3 = 2130837539;
        public static final int num30 = 2130837540;
        public static final int num4 = 2130837541;
        public static final int num5 = 2130837542;
        public static final int num6 = 2130837543;
        public static final int num7 = 2130837544;
        public static final int num8 = 2130837545;
        public static final int num9 = 2130837546;
        public static final int okbtn = 2130837547;
        public static final int playbtn = 2130837548;
        public static final int resultbg = 2130837549;
        public static final int secondbg = 2130837550;
        public static final int thumbbg = 2130837551;
        public static final int yesbg = 2130837552;
        public static final int yesbtn = 2130837553;
    }

    public static final class id {
        public static final int btn = 2131034135;
        public static final int btnno1 = 2131034114;
        public static final int btnno2 = 2131034117;
        public static final int btnno3 = 2131034120;
        public static final int btnno4 = 2131034123;
        public static final int btnno5 = 2131034126;
        public static final int btnyes1 = 2131034115;
        public static final int btnyes2 = 2131034118;
        public static final int btnyes3 = 2131034121;
        public static final int btnyes4 = 2131034124;
        public static final int btnyes5 = 2131034127;
        public static final int grpcrads = 2131034113;
        public static final int message = 2131034133;
        public static final int next1 = 2131034116;
        public static final int next2 = 2131034119;
        public static final int next3 = 2131034122;
        public static final int next4 = 2131034125;
        public static final int next5 = 2131034128;
        public static final int nextthumb = 2131034139;
        public static final int nxtbtn = 2131034130;
        public static final int okbtn = 2131034112;
        public static final int playbtn = 2131034129;
        public static final int playbtn2 = 2131034140;
        public static final int playbtn3 = 2131034131;
        public static final int result = 2131034132;
        public static final int resultnobtn = 2131034137;
        public static final int resultyesbtn = 2131034136;
        public static final int score = 2131034134;
        public static final int thumb = 2131034138;
    }

    public static final class layout {
        public static final int cards = 2130903040;
        public static final int grpcards1 = 2130903041;
        public static final int grpcards2 = 2130903042;
        public static final int grpcards3 = 2130903043;
        public static final int grpcards4 = 2130903044;
        public static final int grpcards5 = 2130903045;
        public static final int main = 2130903046;
        public static final int main2 = 2130903047;
        public static final int nocard = 2130903048;
        public static final int result = 2130903049;
        public static final int thumb = 2130903050;
        public static final int yescard = 2130903051;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }
}
