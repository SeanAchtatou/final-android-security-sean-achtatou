package com.rd.Magic_Card_Game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Secondbg extends Activity {
    private ImageButton nxt;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main2);
        this.nxt = (ImageButton) findViewById(R.id.nxtbtn);
        this.nxt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Secondbg.this.startActivity(new Intent(Secondbg.this, CardNums.class));
            }
        });
    }
}
