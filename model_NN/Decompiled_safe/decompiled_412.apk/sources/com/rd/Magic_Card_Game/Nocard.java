package com.rd.Magic_Card_Game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Nocard extends Activity {
    private ImageButton nocard;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.nocard);
        this.nocard = (ImageButton) findViewById(R.id.playbtn3);
        this.nocard.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Nocard.this.startActivity(new Intent(Nocard.this, CardNums.class));
            }
        });
    }
}
