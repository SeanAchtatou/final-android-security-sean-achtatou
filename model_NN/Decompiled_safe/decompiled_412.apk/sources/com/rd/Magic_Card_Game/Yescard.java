package com.rd.Magic_Card_Game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Yescard extends Activity {
    private ImageButton yescard;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.yescard);
        this.yescard = (ImageButton) findViewById(R.id.playbtn2);
        this.yescard.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Yescard.this.startActivity(new Intent(Yescard.this, CardNums.class));
            }
        });
    }
}
