package com.rd.Magic_Card_Game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class GrpCards1 extends Activity {
    public TextView text;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.grpcards1);
        ((ImageButton) findViewById(R.id.next1)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int num1 = 0;
                RadioButton btnyes1 = (RadioButton) GrpCards1.this.findViewById(R.id.btnyes1);
                if (((RadioButton) GrpCards1.this.findViewById(R.id.btnno1)).isChecked()) {
                    num1 = 0;
                } else if (btnyes1.isChecked()) {
                    num1 = 1;
                }
                String Snum1 = String.valueOf(num1);
                Intent intent = new Intent(GrpCards1.this, GrpCards2.class);
                intent.putExtra("score", Snum1);
                GrpCards1.this.startActivity(intent);
            }
        });
    }
}
