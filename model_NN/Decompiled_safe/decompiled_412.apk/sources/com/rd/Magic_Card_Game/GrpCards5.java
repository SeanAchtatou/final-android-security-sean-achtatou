package com.rd.Magic_Card_Game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

public class GrpCards5 extends Activity {
    int num5;
    public TextView text;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.grpcards5);
        ((ImageButton) findViewById(R.id.next5)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RadioButton q52 = (RadioButton) GrpCards5.this.findViewById(R.id.btnyes5);
                if (((RadioButton) GrpCards5.this.findViewById(R.id.btnno5)).isChecked()) {
                    GrpCards5.this.num5 = 0;
                } else if (q52.isChecked()) {
                    GrpCards5.this.num5 = 16;
                }
                String Snum1 = String.valueOf(Integer.parseInt(GrpCards5.this.getIntent().getStringExtra("score")) + GrpCards5.this.num5);
                Intent intent = new Intent(GrpCards5.this, Thumb.class);
                intent.putExtra("score", Snum1);
                GrpCards5.this.startActivity(intent);
            }
        });
    }
}
