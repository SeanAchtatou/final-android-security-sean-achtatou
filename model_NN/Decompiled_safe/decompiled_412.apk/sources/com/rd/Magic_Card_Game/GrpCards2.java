package com.rd.Magic_Card_Game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

public class GrpCards2 extends Activity {
    int num2;
    public TextView text;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.grpcards2);
        ((ImageButton) findViewById(R.id.next2)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RadioButton btnyes2 = (RadioButton) GrpCards2.this.findViewById(R.id.btnyes2);
                if (((RadioButton) GrpCards2.this.findViewById(R.id.btnno2)).isChecked()) {
                    GrpCards2.this.num2 = 0;
                } else if (btnyes2.isChecked()) {
                    GrpCards2.this.num2 = 2;
                }
                String Snum1 = String.valueOf(Integer.parseInt(GrpCards2.this.getIntent().getStringExtra("score")) + GrpCards2.this.num2);
                Intent intent = new Intent(GrpCards2.this, GrpCards3.class);
                intent.putExtra("score", Snum1);
                GrpCards2.this.startActivity(intent);
            }
        });
    }
}
