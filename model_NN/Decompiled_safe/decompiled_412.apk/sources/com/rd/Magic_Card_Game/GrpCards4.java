package com.rd.Magic_Card_Game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

public class GrpCards4 extends Activity {
    int num4;
    public TextView text;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.grpcards4);
        ((ImageButton) findViewById(R.id.next4)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RadioButton q42 = (RadioButton) GrpCards4.this.findViewById(R.id.btnyes4);
                if (((RadioButton) GrpCards4.this.findViewById(R.id.btnno4)).isChecked()) {
                    GrpCards4.this.num4 = 0;
                } else if (q42.isChecked()) {
                    GrpCards4.this.num4 = 8;
                }
                String Snum1 = String.valueOf(Integer.parseInt(GrpCards4.this.getIntent().getStringExtra("score")) + GrpCards4.this.num4);
                Intent intent = new Intent(GrpCards4.this, GrpCards5.class);
                intent.putExtra("score", Snum1);
                GrpCards4.this.startActivity(intent);
            }
        });
    }
}
