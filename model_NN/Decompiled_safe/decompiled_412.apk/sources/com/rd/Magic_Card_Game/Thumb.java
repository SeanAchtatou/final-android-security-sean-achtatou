package com.rd.Magic_Card_Game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class Thumb extends Activity {
    int num6;
    public TextView text;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.thumb);
        ((ImageButton) findViewById(R.id.nextthumb)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (((ImageButton) Thumb.this.findViewById(R.id.thumb)).isClickable()) {
                    Thumb.this.num6 = 0;
                    String Snum1 = String.valueOf(Integer.parseInt(Thumb.this.getIntent().getStringExtra("score")) + Thumb.this.num6);
                    Intent intent = new Intent(Thumb.this, Result.class);
                    intent.putExtra("score", Snum1);
                    Thumb.this.startActivity(intent);
                }
            }
        });
    }
}
