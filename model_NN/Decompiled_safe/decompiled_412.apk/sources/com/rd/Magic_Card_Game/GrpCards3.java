package com.rd.Magic_Card_Game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;

public class GrpCards3 extends Activity {
    int num3;
    public TextView text;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.grpcards3);
        ((ImageButton) findViewById(R.id.next3)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                RadioButton btnyes3 = (RadioButton) GrpCards3.this.findViewById(R.id.btnyes3);
                if (((RadioButton) GrpCards3.this.findViewById(R.id.btnno3)).isChecked()) {
                    GrpCards3.this.num3 = 0;
                } else if (btnyes3.isChecked()) {
                    GrpCards3.this.num3 = 4;
                }
                String Snum1 = String.valueOf(Integer.parseInt(GrpCards3.this.getIntent().getStringExtra("score")) + GrpCards3.this.num3);
                Intent intent = new Intent(GrpCards3.this, GrpCards4.class);
                intent.putExtra("score", Snum1);
                GrpCards3.this.startActivity(intent);
            }
        });
    }
}
