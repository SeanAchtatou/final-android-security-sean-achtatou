package com.rd.Magic_Card_Game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class Result extends Activity {
    String message;
    int num20;
    TextView score;
    int sum;
    public TextView text;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.result);
        int IntScore = Integer.parseInt(getIntent().getStringExtra("score"));
        this.score = (TextView) findViewById(R.id.score);
        if (IntScore == 30) {
            this.score.setBackgroundResource(R.drawable.num30);
        } else if (IntScore == 29) {
            this.score.setBackgroundResource(R.drawable.num29);
        } else if (IntScore == 28) {
            this.score.setBackgroundResource(R.drawable.num28);
        } else if (IntScore == 27) {
            this.score.setBackgroundResource(R.drawable.num27);
        } else if (IntScore == 26) {
            this.score.setBackgroundResource(R.drawable.num26);
        } else if (IntScore == 25) {
            this.score.setBackgroundResource(R.drawable.num25);
        } else if (IntScore == 24) {
            this.score.setBackgroundResource(R.drawable.num24);
        } else if (IntScore == 23) {
            this.message = " Text ";
            this.score.setBackgroundResource(R.drawable.num23);
        } else if (IntScore == 22) {
            this.score.setBackgroundResource(R.drawable.num22);
        } else if (IntScore == 21) {
            this.score.setBackgroundResource(R.drawable.num21);
        } else if (IntScore == 20) {
            this.score.setBackgroundResource(R.drawable.num20);
        } else if (IntScore == 19) {
            this.score.setBackgroundResource(R.drawable.num19);
        } else if (IntScore == 18) {
            this.score.setBackgroundResource(R.drawable.num18);
        } else if (IntScore == 17) {
            this.score.setBackgroundResource(R.drawable.num17);
        } else if (IntScore == 16) {
            this.score.setBackgroundResource(R.drawable.num16);
        } else if (IntScore == 15) {
            this.score.setBackgroundResource(R.drawable.num15);
        } else if (IntScore == 14) {
            this.score.setBackgroundResource(R.drawable.num14);
        } else if (IntScore == 13) {
            this.score.setBackgroundResource(R.drawable.num13);
        } else if (IntScore == 12) {
            this.score.setBackgroundResource(R.drawable.num12);
        } else if (IntScore == 11) {
            this.score.setBackgroundResource(R.drawable.num11);
        } else if (IntScore == 10) {
            this.score.setBackgroundResource(R.drawable.num10);
        } else if (IntScore == 9) {
            this.score.setBackgroundResource(R.drawable.num9);
        } else if (IntScore == 8) {
            this.score.setBackgroundResource(R.drawable.num8);
        } else if (IntScore == 7) {
            this.score.setBackgroundResource(R.drawable.num7);
        } else if (IntScore == 6) {
            this.score.setBackgroundResource(R.drawable.num6);
        } else if (IntScore == 5) {
            this.score.setBackgroundResource(R.drawable.num5);
        } else if (IntScore == 4) {
            this.score.setBackgroundResource(R.drawable.num4);
        } else if (IntScore == 3) {
            this.score.setBackgroundResource(R.drawable.num3);
        } else if (IntScore == 2) {
            this.score.setBackgroundResource(R.drawable.num2);
        } else if (IntScore == 1) {
            this.score.setBackgroundResource(R.drawable.num1);
        }
        this.text = (TextView) findViewById(R.id.message);
        this.text.setText(this.message);
        ((ImageButton) findViewById(R.id.resultyesbtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Result.this.startActivity(new Intent(Result.this, Yescard.class));
            }
        });
        ((ImageButton) findViewById(R.id.resultnobtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Result.this.startActivity(new Intent(Result.this, Nocard.class));
            }
        });
    }
}
