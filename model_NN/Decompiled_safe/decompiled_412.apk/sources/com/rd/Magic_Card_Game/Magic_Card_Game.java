package com.rd.Magic_Card_Game;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Magic_Card_Game extends Activity {
    private ImageButton play;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.play = (ImageButton) findViewById(R.id.playbtn);
        this.play.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Magic_Card_Game.this.startActivity(new Intent(Magic_Card_Game.this, Secondbg.class));
            }
        });
    }
}
