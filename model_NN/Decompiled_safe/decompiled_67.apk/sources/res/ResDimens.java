package res;

import android.util.DisplayMetrics;
import android.util.TypedValue;

public final class ResDimens {
    public static final int AD_HEIGHT = 52;
    public static final int BUTTON_HEIGHT = 48;
    public static final int BUTTON_ICON_SIZE = 35;
    public static final int COLOR_PICKER_CIRCLE_BORDER_SIZE = 2;
    public static final int COLOR_PICKER_INNER_CIRCLE_RADIUS = 32;
    public static final int COLOR_PICKER_OUTER_CIRCLE_RADIUS = 120;
    public static final int COLOR_PICKER_OUTER_CIRCLE_STROKE = 48;
    public static final int COLOR_PICKER_VIEW_SIZE = 250;
    public static final int HINT_LINE_SIZE = 1;
    public static final int ICON_SIZE = 48;
    public static final int SCREEN_TITLE_HEIGHT = 64;
    public static final int THUMPNAIL_BORDER_SIZE = 100;
    public static final int THUMPNAIL_SIZE = 96;

    public static synchronized int getDip(DisplayMetrics hDisplayMetrics, int iSize) {
        int i;
        synchronized (ResDimens.class) {
            float iValue = TypedValue.applyDimension(1, (float) iSize, hDisplayMetrics);
            if (iValue < 1.0f) {
                i = 1;
            } else {
                i = (int) iValue;
            }
        }
        return i;
    }
}
