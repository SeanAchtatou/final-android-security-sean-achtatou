package data;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import data.MorePuzzleList;
import helper.Global;
import helper.PuzzleConfig;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class Puzzle {
    private static final String PREF_FILE_PROGRESS_DATA_JIGSAW = "PREF_FILE_PROGRESS_DATA_JIGSAW";
    private static final String PREF_FILE_PUZZLE_DATA = "PREF_FILE_PUZZLE_DATA";
    private static final String PREF_KEY_BEST_MOVES = "PREF_KEY_BEST_MOVES";
    private static final String PREF_KEY_BEST_MOVES_USER = "PREF_KEY_BEST_MOVES_USER";
    private static final String PREF_KEY_BEST_TIME_IN_MS = "PREF_KEY_BEST_TIME_IN_MS";
    private static final String PREF_KEY_BEST_TIME_IN_MS_USER = "PREF_KEY_BEST_TIME_IN_MS_USER";
    private static final String PREF_KEY_IMAGE_COUNT = "PREF_KEY_IMAGE_COUNT";
    private static final String PREF_KEY_INDEX_UNSOLVED = "PREF_KEY_INDEX_UNSOLVED";
    private static final String PREF_KEY_PUZZLE_VERSION_CODE = "PREF_KEY_PUZZLE_VERSION_CODE";
    private List<PuzzleImage> m_arrPuzzleImage;
    private PuzzleImage m_hPuzzleImageSelected;
    private int m_iGridSizeMax;
    private int m_iGridSizeMin;
    private int m_iImageCount = 0;
    private int m_iImageIndexNextUnsolved = 0;
    private final int m_iVersionCode;
    private String m_sAdWhirlKey;
    private final String m_sDeveloperName;
    private final String m_sId;
    private String m_sInfo;
    private final String m_sPuzzleName;

    public static synchronized Puzzle create(Context hContext) {
        Puzzle puzzle;
        synchronized (Puzzle.class) {
            String sPuzzleId = hContext.getPackageName();
            if (TextUtils.isEmpty(sPuzzleId)) {
                puzzle = null;
            } else {
                String sDevName = PuzzleConfig.getDeveloperName(hContext);
                if (TextUtils.isEmpty(sDevName)) {
                    puzzle = null;
                } else {
                    String sPuzzleName = PuzzleConfig.getPuzzleName(hContext);
                    if (TextUtils.isEmpty(sPuzzleName)) {
                        puzzle = null;
                    } else {
                        int iPuzzleVersionCode = PuzzleConfig.getVersionCode(hContext);
                        if (iPuzzleVersionCode == -1) {
                            puzzle = null;
                        } else {
                            puzzle = new Puzzle(sPuzzleId, sDevName, sPuzzleName, iPuzzleVersionCode);
                        }
                    }
                }
            }
        }
        return puzzle;
    }

    private Puzzle(String sId, String sDeveloperName, String sPuzzleName, int iVersionCode) {
        this.m_sId = sId;
        this.m_sDeveloperName = sDeveloperName;
        this.m_sPuzzleName = sPuzzleName;
        this.m_iVersionCode = iVersionCode;
    }

    public String getDeveloperName() {
        return this.m_sDeveloperName;
    }

    public String getId() {
        return this.m_sId;
    }

    public String getName() {
        return this.m_sPuzzleName;
    }

    public int getVersionCode() {
        return this.m_iVersionCode;
    }

    public void setGridSizeMin(Integer iDimensionMin) {
        this.m_iGridSizeMin = iDimensionMin.intValue();
    }

    public int getGridSizeMin() {
        return this.m_iGridSizeMin;
    }

    public void setGridSizeMax(Integer iDimensionMax) {
        this.m_iGridSizeMax = iDimensionMax.intValue();
    }

    public int getGridSizeMax() {
        return this.m_iGridSizeMax;
    }

    public void setAdWhirlKey(String sAdWhirlKey) {
        this.m_sAdWhirlKey = sAdWhirlKey;
    }

    public String getAdWhirlKey() {
        return this.m_sAdWhirlKey;
    }

    public void setImageCount(int iImageCount) {
        this.m_iImageCount = iImageCount;
    }

    public int getImageCount() {
        return this.m_iImageCount;
    }

    public void setInfo(String sInfo) {
        this.m_sInfo = sInfo;
    }

    public String getInfo() {
        return this.m_sInfo;
    }

    public void loadImageCount(Context hContext) {
        SharedPreferences hPref = getPrefFilePuzzleDataSharedPref(hContext);
        if (this.m_iVersionCode == hPref.getInt(PREF_KEY_PUZZLE_VERSION_CODE, -1)) {
            this.m_iImageCount = hPref.getInt(PREF_KEY_IMAGE_COUNT, 0);
        } else {
            int iImageCount = getImageCount(hContext, this.m_sId);
            SharedPreferences.Editor hEditor = hPref.edit();
            if (hEditor.putInt(PREF_KEY_IMAGE_COUNT, iImageCount).commit()) {
                hEditor.putInt(PREF_KEY_PUZZLE_VERSION_CODE, this.m_iVersionCode).commit();
            }
            this.m_iImageCount = iImageCount;
        }
        if (Global.isSDCardAvailable()) {
            MorePuzzleList.MorePuzzleProgress.setImageCount(hContext, this.m_sId, this.m_iImageCount);
            MorePuzzleList.MorePuzzleProgress.saveProgressData(hContext);
        }
    }

    public static synchronized int getImageCount(Context hContext, String sPuzzleId) {
        int iImageCounter;
        synchronized (Puzzle.class) {
            iImageCounter = 0;
            do {
                InputStream hInputStream = null;
                try {
                    InputStream hInputStream2 = hContext.getContentResolver().openInputStream(PuzzleHelper.getImageUri(sPuzzleId, iImageCounter));
                    if (hInputStream2 != null) {
                        try {
                            hInputStream2.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    iImageCounter++;
                } catch (Exception e2) {
                    if (hInputStream != null) {
                        hInputStream.close();
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                } catch (Throwable th) {
                    if (hInputStream != null) {
                        try {
                            hInputStream.close();
                        } catch (Exception e4) {
                            e4.printStackTrace();
                        }
                    }
                    throw th;
                }
            } while (iImageCounter != 200);
        }
        return iImageCounter;
    }

    public void loadImageIndexNextUnsolved(Context hContext, int iImageCount) {
        this.m_iImageIndexNextUnsolved = getPrefFileProgressDataJigsawSharedPref(hContext).getInt(PREF_KEY_INDEX_UNSOLVED, 0);
        if (this.m_iImageIndexNextUnsolved > iImageCount) {
            this.m_iImageIndexNextUnsolved = iImageCount;
        }
        if (Global.isSDCardAvailable()) {
            MorePuzzleList.MorePuzzleProgress.setImageIndexNextUnsolved(hContext, this.m_sId, this.m_iImageIndexNextUnsolved);
            MorePuzzleList.MorePuzzleProgress.saveProgressData(hContext);
        }
    }

    private SharedPreferences getPrefFilePuzzleDataSharedPref(Context hContext) {
        return hContext.getSharedPreferences(PREF_FILE_PUZZLE_DATA, 0);
    }

    private SharedPreferences getPrefFileProgressDataJigsawSharedPref(Context hContext) {
        return hContext.getSharedPreferences(PREF_FILE_PROGRESS_DATA_JIGSAW, 0);
    }

    public void setPuzzleSolved(Context hContext, int iImageIndexSolved, String sUser, int iMoves, long lTimeInMS) {
        SharedPreferences hPref = getPrefFileProgressDataJigsawSharedPref(hContext);
        SharedPreferences.Editor hEditor = hPref.edit();
        this.m_iImageIndexNextUnsolved = hPref.getInt(PREF_KEY_INDEX_UNSOLVED, 0);
        if (this.m_iImageIndexNextUnsolved < iImageIndexSolved + 1) {
            this.m_iImageIndexNextUnsolved = iImageIndexSolved + 1;
            if (this.m_iImageIndexNextUnsolved > this.m_iImageCount) {
                this.m_iImageIndexNextUnsolved = this.m_iImageCount;
            }
            hEditor.putInt(PREF_KEY_INDEX_UNSOLVED, this.m_iImageIndexNextUnsolved);
            if (Global.isSDCardAvailable()) {
                MorePuzzleList.MorePuzzleProgress.setImageIndexNextUnsolved(hContext, this.m_sId, this.m_iImageIndexNextUnsolved);
                MorePuzzleList.MorePuzzleProgress.saveProgressData(hContext);
            }
        }
        long lBestTimeInMs = hPref.getLong(PREF_KEY_BEST_TIME_IN_MS + iImageIndexSolved, 2147483646);
        if (lTimeInMS < lBestTimeInMs || lBestTimeInMs <= 0) {
            hEditor.putLong(PREF_KEY_BEST_TIME_IN_MS + iImageIndexSolved, lTimeInMS);
            hEditor.putString(PREF_KEY_BEST_TIME_IN_MS_USER + iImageIndexSolved, sUser);
        }
        int iBestMoves = hPref.getInt(PREF_KEY_BEST_MOVES + iImageIndexSolved, 2147483646);
        if (iMoves < iBestMoves || iBestMoves <= 0) {
            hEditor.putInt(PREF_KEY_BEST_MOVES + iImageIndexSolved, iMoves);
            hEditor.putString(PREF_KEY_BEST_MOVES_USER + iImageIndexSolved, sUser);
        }
        if (!hEditor.commit()) {
            throw new RuntimeException("Unable to save progress for solved puzzle");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x007b, code lost:
        if (helper.Constants.debug() == false) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x009d, code lost:
        throw new java.lang.RuntimeException("Reset ImageIndexNextUnsolved " + r14.m_iImageIndexNextUnsolved + " to " + r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x009e, code lost:
        r14.m_iImageIndexNextUnsolved = r5;
        r4.edit().putInt(data.Puzzle.PREF_KEY_INDEX_UNSOLVED, r14.m_iImageIndexNextUnsolved).commit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00b3, code lost:
        if (helper.Global.isSDCardAvailable() == false) goto L_0x0019;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00b5, code lost:
        data.MorePuzzleList.MorePuzzleProgress.setImageIndexNextUnsolved(r15, r14.m_sId, r14.m_iImageIndexNextUnsolved);
        data.MorePuzzleList.MorePuzzleProgress.saveProgressData(r15);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadPuzzleImageData(android.content.Context r15) {
        /*
            r14 = this;
            int r11 = r14.m_iImageIndexNextUnsolved
            long[] r2 = new long[r11]
            int r11 = r14.m_iImageIndexNextUnsolved
            java.lang.String[] r3 = new java.lang.String[r11]
            int r11 = r14.m_iImageIndexNextUnsolved
            int[] r0 = new int[r11]
            int r11 = r14.m_iImageIndexNextUnsolved
            java.lang.String[] r1 = new java.lang.String[r11]
            android.content.SharedPreferences r4 = r14.getPrefFileProgressDataJigsawSharedPref(r15)
            r5 = 0
        L_0x0015:
            int r11 = r14.m_iImageIndexNextUnsolved
            if (r5 < r11) goto L_0x001d
        L_0x0019:
            r14.setPuzzleImageData(r2, r3, r0, r1)
            return
        L_0x001d:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            java.lang.String r12 = "PREF_KEY_BEST_TIME_IN_MS"
            r11.<init>(r12)
            java.lang.StringBuilder r11 = r11.append(r5)
            java.lang.String r11 = r11.toString()
            r12 = -1
            long r7 = r4.getLong(r11, r12)
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            java.lang.String r12 = "PREF_KEY_BEST_TIME_IN_MS_USER"
            r11.<init>(r12)
            java.lang.StringBuilder r11 = r11.append(r5)
            java.lang.String r11 = r11.toString()
            r12 = 0
            java.lang.String r10 = r4.getString(r11, r12)
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            java.lang.String r12 = "PREF_KEY_BEST_MOVES"
            r11.<init>(r12)
            java.lang.StringBuilder r11 = r11.append(r5)
            java.lang.String r11 = r11.toString()
            r12 = -1
            int r6 = r4.getInt(r11, r12)
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            java.lang.String r12 = "PREF_KEY_BEST_MOVES_USER"
            r11.<init>(r12)
            java.lang.StringBuilder r11 = r11.append(r5)
            java.lang.String r11 = r11.toString()
            r12 = 0
            java.lang.String r9 = r4.getString(r11, r12)
            r11 = -1
            int r11 = (r7 > r11 ? 1 : (r7 == r11 ? 0 : -1))
            if (r11 == 0) goto L_0x0077
            r11 = -1
            if (r6 != r11) goto L_0x00c1
        L_0x0077:
            boolean r11 = helper.Constants.debug()
            if (r11 == 0) goto L_0x009e
            java.lang.RuntimeException r11 = new java.lang.RuntimeException
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            java.lang.String r13 = "Reset ImageIndexNextUnsolved "
            r12.<init>(r13)
            int r13 = r14.m_iImageIndexNextUnsolved
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r13 = " to "
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.StringBuilder r12 = r12.append(r5)
            java.lang.String r12 = r12.toString()
            r11.<init>(r12)
            throw r11
        L_0x009e:
            r14.m_iImageIndexNextUnsolved = r5
            android.content.SharedPreferences$Editor r11 = r4.edit()
            java.lang.String r12 = "PREF_KEY_INDEX_UNSOLVED"
            int r13 = r14.m_iImageIndexNextUnsolved
            android.content.SharedPreferences$Editor r11 = r11.putInt(r12, r13)
            r11.commit()
            boolean r11 = helper.Global.isSDCardAvailable()
            if (r11 == 0) goto L_0x0019
            java.lang.String r11 = r14.m_sId
            int r12 = r14.m_iImageIndexNextUnsolved
            data.MorePuzzleList.MorePuzzleProgress.setImageIndexNextUnsolved(r15, r11, r12)
            data.MorePuzzleList.MorePuzzleProgress.saveProgressData(r15)
            goto L_0x0019
        L_0x00c1:
            r2[r5] = r7
            r3[r5] = r10
            r0[r5] = r6
            r1[r5] = r9
            int r5 = r5 + 1
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: data.Puzzle.loadPuzzleImageData(android.content.Context):void");
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    private void setPuzzleImageData(long[] arrBestTimeInMs, String[] arrBestTimeInMsPlayer, int[] arrBestMoves, String[] arrBestMovesPlayer) {
        int iIndexPuzzleImageSelected;
        if (this.m_arrPuzzleImage == null || this.m_arrPuzzleImage.size() != this.m_iImageCount) {
            this.m_arrPuzzleImage = new ArrayList();
            for (int i = 0; i < this.m_iImageCount; i++) {
                this.m_arrPuzzleImage.add(new PuzzleImage(this.m_sId, i));
            }
            if (this.m_hPuzzleImageSelected != null && (iIndexPuzzleImageSelected = this.m_hPuzzleImageSelected.getIndex()) < this.m_iImageCount) {
                this.m_arrPuzzleImage.set(iIndexPuzzleImageSelected, this.m_hPuzzleImageSelected);
            }
        }
        if (this.m_iImageIndexNextUnsolved > this.m_iImageCount) {
            this.m_iImageIndexNextUnsolved = this.m_iImageCount;
        }
        for (int i2 = 0; i2 < this.m_iImageIndexNextUnsolved; i2++) {
            PuzzleImage hPuzzleImage = this.m_arrPuzzleImage.get(i2);
            hPuzzleImage.setSolved(true);
            hPuzzleImage.setIsNextUnsolved(false);
            hPuzzleImage.setBestTimeInMs(arrBestTimeInMs[i2]);
            hPuzzleImage.setBestTimeInMsName(arrBestTimeInMsPlayer[i2]);
            hPuzzleImage.setBestMoves(arrBestMoves[i2]);
            hPuzzleImage.setBestMovesName(arrBestMovesPlayer[i2]);
        }
        if (this.m_iImageIndexNextUnsolved == this.m_iImageCount) {
            if (!this.m_arrPuzzleImage.isEmpty()) {
                this.m_arrPuzzleImage.get(0).setIsNextUnsolved(true);
            }
        } else if (this.m_iImageIndexNextUnsolved < this.m_iImageCount) {
            this.m_arrPuzzleImage.get(this.m_iImageIndexNextUnsolved).setIsNextUnsolved(true);
        }
    }

    public List<PuzzleImage> getPuzzleImage() {
        return this.m_arrPuzzleImage;
    }

    public void setImageIndexNextUnsolved(int iImageIndexNextUnsolved) {
        this.m_iImageIndexNextUnsolved = iImageIndexNextUnsolved;
    }

    public int getImageIndexNextUnsolved() {
        return this.m_iImageIndexNextUnsolved;
    }

    public void setPuzzleImageSelected(PuzzleImage hPuzzleImageSelected) {
        if (this.m_hPuzzleImageSelected != null && !this.m_hPuzzleImageSelected.equals(hPuzzleImageSelected)) {
            this.m_hPuzzleImageSelected.setImage(null, null);
        }
        this.m_hPuzzleImageSelected = hPuzzleImageSelected;
    }

    public PuzzleImage getPuzzleImageSelected() {
        return this.m_hPuzzleImageSelected;
    }

    public void setPuzzleImageRandom() {
        setPuzzleImageSelected(new PuzzleImage(this.m_sId, new Random().nextInt(this.m_iImageCount)));
    }

    public int setPuzzleImageNext() {
        int iIndexCurrent = this.m_hPuzzleImageSelected.getIndex();
        int iIndexNext = iIndexCurrent < this.m_arrPuzzleImage.size() - 1 ? iIndexCurrent + 1 : 0;
        setPuzzleImageSelected(this.m_arrPuzzleImage.get(iIndexNext));
        if (this.m_hPuzzleImageSelected != null) {
            return iIndexNext;
        }
        throw new RuntimeException("Invalid puzzle image");
    }

    public boolean isAllSolved() {
        for (PuzzleImage hPuzzleImageLoop : this.m_arrPuzzleImage) {
            if (!hPuzzleImageLoop.isSolved()) {
                return false;
            }
        }
        return true;
    }

    public void clearPuzzleImageTempData() {
        for (PuzzleImage hPuzzleImageLoop : this.m_arrPuzzleImage) {
            hPuzzleImageLoop.setImage(null, null);
            hPuzzleImageLoop.setThumbnail(null);
        }
    }
}
