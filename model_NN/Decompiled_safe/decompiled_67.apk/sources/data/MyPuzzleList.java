package data;

import helper.Global;
import helper.PuzzleConfig;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

public final class MyPuzzleList {
    private static final int BUFFER_SIZE = 8192;
    private static final String MY_PUZZLE_FOLDER_NAME = "MyPuzzle";
    private static final String MY_PUZZLE_LIST_FILE_NAME = "my_puzzle_list.dat";
    private final LinkedList<MyPuzzle> m_arrMyPuzzleExtern = new LinkedList<>();
    private final LinkedList<MyPuzzle> m_arrMyPuzzleIntern = new LinkedList<>();

    /* JADX WARNING: Removed duplicated region for block: B:42:0x0083 A[Catch:{ all -> 0x0095 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:25:0x0063=Splitter:B:25:0x0063, B:39:0x007d=Splitter:B:39:0x007d, B:48:0x008f=Splitter:B:48:0x008f} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:52:0x0096=Splitter:B:52:0x0096, B:16:0x0049=Splitter:B:16:0x0049} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void loadFromSD() {
        /*
            r10 = this;
            monitor-enter(r10)
            java.util.LinkedList<data.MyPuzzle> r7 = r10.m_arrMyPuzzleIntern     // Catch:{ all -> 0x0073 }
            r7.clear()     // Catch:{ all -> 0x0073 }
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x0073 }
            java.io.File r7 = getMyPuzzleRootDir()     // Catch:{ all -> 0x0073 }
            java.lang.String r8 = "my_puzzle_list.dat"
            r3.<init>(r7, r8)     // Catch:{ all -> 0x0073 }
            boolean r7 = r3.exists()     // Catch:{ all -> 0x0073 }
            if (r7 != 0) goto L_0x0019
        L_0x0017:
            monitor-exit(r10)
            return
        L_0x0019:
            r4 = 0
            java.io.ObjectInputStream r5 = new java.io.ObjectInputStream     // Catch:{ ClassCastException -> 0x00a4, ClassNotFoundException -> 0x00a1, Exception -> 0x008d }
            java.io.BufferedInputStream r7 = new java.io.BufferedInputStream     // Catch:{ ClassCastException -> 0x00a4, ClassNotFoundException -> 0x00a1, Exception -> 0x008d }
            java.io.FileInputStream r8 = new java.io.FileInputStream     // Catch:{ ClassCastException -> 0x00a4, ClassNotFoundException -> 0x00a1, Exception -> 0x008d }
            r8.<init>(r3)     // Catch:{ ClassCastException -> 0x00a4, ClassNotFoundException -> 0x00a1, Exception -> 0x008d }
            r9 = 8192(0x2000, float:1.14794E-41)
            r7.<init>(r8, r9)     // Catch:{ ClassCastException -> 0x00a4, ClassNotFoundException -> 0x00a1, Exception -> 0x008d }
            r5.<init>(r7)     // Catch:{ ClassCastException -> 0x00a4, ClassNotFoundException -> 0x00a1, Exception -> 0x008d }
            java.lang.Object r0 = r5.readObject()     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
            java.util.LinkedList r0 = (java.util.LinkedList) r0     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
            r1.<init>()     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
        L_0x003a:
            boolean r8 = r7.hasNext()     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
            if (r8 != 0) goto L_0x004e
            int r7 = r1.size()     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
            if (r7 <= 0) goto L_0x0049
            r10.saveToSD()     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
        L_0x0049:
            helper.Global.closeInputStream(r5)     // Catch:{ all -> 0x0073 }
            r4 = r5
            goto L_0x0017
        L_0x004e:
            java.lang.Object r6 = r7.next()     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
            data.MyPuzzle r6 = (data.MyPuzzle) r6     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
            boolean r8 = r6.loadData()     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
            if (r8 == 0) goto L_0x0076
            java.util.LinkedList<data.MyPuzzle> r8 = r10.m_arrMyPuzzleIntern     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
            r8.add(r6)     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
            goto L_0x003a
        L_0x0060:
            r7 = move-exception
            r2 = r7
            r4 = r5
        L_0x0063:
            boolean r7 = helper.Constants.debug()     // Catch:{ all -> 0x0095 }
            if (r7 == 0) goto L_0x006c
            r2.printStackTrace()     // Catch:{ all -> 0x0095 }
        L_0x006c:
            r3.delete()     // Catch:{ all -> 0x0095 }
            helper.Global.closeInputStream(r4)     // Catch:{ all -> 0x0073 }
            goto L_0x0017
        L_0x0073:
            r7 = move-exception
            monitor-exit(r10)
            throw r7
        L_0x0076:
            r1.add(r6)     // Catch:{ ClassCastException -> 0x0060, ClassNotFoundException -> 0x007a, Exception -> 0x009d, all -> 0x009a }
            goto L_0x003a
        L_0x007a:
            r7 = move-exception
            r2 = r7
            r4 = r5
        L_0x007d:
            boolean r7 = helper.Constants.debug()     // Catch:{ all -> 0x0095 }
            if (r7 == 0) goto L_0x0086
            r2.printStackTrace()     // Catch:{ all -> 0x0095 }
        L_0x0086:
            r3.delete()     // Catch:{ all -> 0x0095 }
            helper.Global.closeInputStream(r4)     // Catch:{ all -> 0x0073 }
            goto L_0x0017
        L_0x008d:
            r7 = move-exception
            r2 = r7
        L_0x008f:
            java.lang.RuntimeException r7 = new java.lang.RuntimeException     // Catch:{ all -> 0x0095 }
            r7.<init>(r2)     // Catch:{ all -> 0x0095 }
            throw r7     // Catch:{ all -> 0x0095 }
        L_0x0095:
            r7 = move-exception
        L_0x0096:
            helper.Global.closeInputStream(r4)     // Catch:{ all -> 0x0073 }
            throw r7     // Catch:{ all -> 0x0073 }
        L_0x009a:
            r7 = move-exception
            r4 = r5
            goto L_0x0096
        L_0x009d:
            r7 = move-exception
            r2 = r7
            r4 = r5
            goto L_0x008f
        L_0x00a1:
            r7 = move-exception
            r2 = r7
            goto L_0x007d
        L_0x00a4:
            r7 = move-exception
            r2 = r7
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: data.MyPuzzleList.loadFromSD():void");
    }

    public synchronized void synchWithSD() {
        boolean bSave = false;
        HashSet<String> arrInstalledMyPuzzleId = new HashSet<>();
        ArrayList<MyPuzzle> arrToDelete = new ArrayList<>();
        Iterator<MyPuzzle> it = this.m_arrMyPuzzleIntern.iterator();
        while (it.hasNext()) {
            MyPuzzle hMyPuzzleLoop = it.next();
            String sMyPuzzleId = hMyPuzzleLoop.getId();
            if (!MyPuzzle.isOnSD(sMyPuzzleId)) {
                arrToDelete.add(hMyPuzzleLoop);
            } else {
                arrInstalledMyPuzzleId.add(sMyPuzzleId);
            }
        }
        Iterator it2 = arrToDelete.iterator();
        while (it2.hasNext()) {
            MyPuzzle hMyPuzzleLoop2 = (MyPuzzle) it2.next();
            hMyPuzzleLoop2.deleteFromSD();
            this.m_arrMyPuzzleIntern.remove(hMyPuzzleLoop2);
            bSave = true;
        }
        File[] arrMyPuzzleRootDir = getMyPuzzleRootDir().listFiles();
        if (arrMyPuzzleRootDir != null) {
            for (File hMyPuzzleDir : arrMyPuzzleRootDir) {
                if (hMyPuzzleDir.isDirectory()) {
                    String sMyPuzzleId2 = hMyPuzzleDir.getName();
                    if (!arrInstalledMyPuzzleId.contains(sMyPuzzleId2)) {
                        if (!MyPuzzle.isOnSD(sMyPuzzleId2)) {
                            File[] arrFile = hMyPuzzleDir.listFiles();
                            if (arrFile != null) {
                                for (File hFileLoop : arrFile) {
                                    hFileLoop.delete();
                                }
                            }
                        } else {
                            this.m_arrMyPuzzleIntern.add(new MyPuzzle(sMyPuzzleId2));
                            bSave = true;
                        }
                    }
                }
            }
            if (bSave) {
                saveToSD();
            }
        }
    }

    public synchronized void saveToSD() {
        Exception e;
        File hDir = getMyPuzzleRootDir();
        File hFileTemp = new File(hDir, "my_puzzle_list.dat.tmp");
        ObjectOutputStream hObjetOut = null;
        try {
            hFileTemp.getParentFile().mkdirs();
            hFileTemp.createNewFile();
            ObjectOutputStream hObjetOut2 = new ObjectOutputStream(new FileOutputStream(hFileTemp));
            try {
                hObjetOut2.writeObject(this.m_arrMyPuzzleIntern);
                hFileTemp.renameTo(new File(hDir, MY_PUZZLE_LIST_FILE_NAME));
                Global.closeOutputStream(hObjetOut2);
            } catch (Exception e2) {
                e = e2;
                hObjetOut = hObjetOut2;
                try {
                    throw new RuntimeException(e);
                } catch (Throwable th) {
                    th = th;
                    Global.closeOutputStream(hObjetOut);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                hObjetOut = hObjetOut2;
                Global.closeOutputStream(hObjetOut);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            throw new RuntimeException(e);
        }
    }

    public void add(MyPuzzle hMyPuzzle) {
        this.m_arrMyPuzzleIntern.addFirst(hMyPuzzle);
        saveToSD();
    }

    public synchronized void remove(MyPuzzle hMyPuzzle) {
        String sId = hMyPuzzle.getId();
        Iterator<MyPuzzle> it = this.m_arrMyPuzzleIntern.iterator();
        while (true) {
            if (it.hasNext()) {
                MyPuzzle hMyPuzzleLoop = it.next();
                if (hMyPuzzleLoop.getId().equals(sId)) {
                    this.m_arrMyPuzzleIntern.remove(hMyPuzzleLoop);
                    saveToSD();
                    break;
                }
            } else {
                break;
            }
        }
        hMyPuzzle.deleteFromSD();
    }

    public synchronized int moveUp(MyPuzzle hMyPuzzle) {
        int i;
        String sId = hMyPuzzle.getId();
        int iSize = this.m_arrMyPuzzleIntern.size();
        int i2 = 0;
        while (true) {
            if (i2 < iSize) {
                MyPuzzle hMyPuzzleLoop = this.m_arrMyPuzzleIntern.get(i2);
                if (hMyPuzzleLoop.getId().equals(sId) && i2 > 0) {
                    this.m_arrMyPuzzleIntern.remove(i2);
                    int iNewPos = i2 - 1;
                    this.m_arrMyPuzzleIntern.add(iNewPos, hMyPuzzleLoop);
                    saveToSD();
                    i = iNewPos;
                    break;
                }
                i2++;
            } else {
                i = -1;
                break;
            }
        }
        return i;
    }

    public synchronized int moveDown(MyPuzzle hMyPuzzle) {
        int i;
        String sId = hMyPuzzle.getId();
        int iSize = this.m_arrMyPuzzleIntern.size() - 1;
        int i2 = 0;
        while (true) {
            if (i2 >= iSize) {
                i = -1;
                break;
            }
            MyPuzzle hMyPuzzleLoop = this.m_arrMyPuzzleIntern.get(i2);
            if (hMyPuzzleLoop.getId().equals(sId)) {
                this.m_arrMyPuzzleIntern.remove(i2);
                int iNewPos = i2 + 1;
                this.m_arrMyPuzzleIntern.add(iNewPos, hMyPuzzleLoop);
                saveToSD();
                i = iNewPos;
                break;
            }
            i2++;
        }
        return i;
    }

    public static synchronized File getMyPuzzleRootDir() {
        File hFileDir;
        synchronized (MyPuzzleList.class) {
            hFileDir = new File(String.valueOf(PuzzleConfig.ROOT_DIR_HIDDEN) + MY_PUZZLE_FOLDER_NAME);
            hFileDir.mkdirs();
        }
        return hFileDir;
    }

    public MyPuzzle get(int iIndex) {
        return this.m_arrMyPuzzleExtern.get(iIndex);
    }

    public int size() {
        return this.m_arrMyPuzzleExtern.size();
    }

    public void update() {
        this.m_arrMyPuzzleExtern.clear();
        Iterator<MyPuzzle> it = this.m_arrMyPuzzleIntern.iterator();
        while (it.hasNext()) {
            this.m_arrMyPuzzleExtern.add(it.next());
        }
    }
}
