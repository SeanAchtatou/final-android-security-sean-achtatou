package data;

import android.graphics.Bitmap;
import helper.BitmapHelper;
import helper.Global;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.UUID;

public final class MyPuzzle implements Serializable {
    private static final transient int ICON_SIZE = 72;
    private static final transient String MY_PUZZLE_DATA = "my_puzzle.dat";
    private static final long serialVersionUID = -5054248460404509340L;
    private transient LinkedList<MyPuzzleImage> m_arrMyPuzzleImage;
    private transient MyPuzzleData m_hMyPuzzleData;
    private final String m_sId;

    public MyPuzzle(String sName, Bitmap hIcon) {
        this.m_sId = UUID.randomUUID().toString();
        this.m_hMyPuzzleData = new MyPuzzleData(this.m_sId, sName, hIcon);
        this.m_arrMyPuzzleImage = this.m_hMyPuzzleData.getMyPuzzleImageList();
        setName(sName);
        setIcon(hIcon);
        saveToSD();
    }

    public MyPuzzle(String sMyPuzzleId) {
        this.m_sId = sMyPuzzleId;
        this.m_hMyPuzzleData = loadFromSD(sMyPuzzleId);
        this.m_arrMyPuzzleImage = this.m_hMyPuzzleData.getMyPuzzleImageList();
    }

    public boolean loadData() {
        if (this.m_hMyPuzzleData == null) {
            this.m_hMyPuzzleData = loadFromSD(this.m_sId);
        }
        if (this.m_hMyPuzzleData == null) {
            return false;
        }
        this.m_arrMyPuzzleImage = this.m_hMyPuzzleData.getMyPuzzleImageList();
        return true;
    }

    public String getId() {
        return this.m_sId;
    }

    public void setName(String sMyPuzzleName) {
        this.m_hMyPuzzleData.setName(sMyPuzzleName);
    }

    public String getName() {
        return this.m_hMyPuzzleData.getName();
    }

    public void setIcon(Bitmap hMyPuzzleIcon) {
        this.m_hMyPuzzleData.setIcon(hMyPuzzleIcon);
    }

    public void clearIcon() {
        this.m_hMyPuzzleData.clearIconTemp();
    }

    public Bitmap getIcon() {
        return this.m_hMyPuzzleData.getIcon();
    }

    public int size() {
        return this.m_arrMyPuzzleImage.size();
    }

    public MyPuzzleImage get(int iIndex) {
        return this.m_arrMyPuzzleImage.get(iIndex);
    }

    public void add(MyPuzzleImage hMyPuzzleImage) {
        this.m_arrMyPuzzleImage.add(hMyPuzzleImage);
    }

    public int moveUp(MyPuzzleImage hMyPuzzleImage) {
        String sId = hMyPuzzleImage.getId();
        int iSize = this.m_arrMyPuzzleImage.size();
        int i = 0;
        while (i < iSize) {
            MyPuzzleImage hMyPuzzleImageLoop = this.m_arrMyPuzzleImage.get(i);
            if (!hMyPuzzleImageLoop.getId().equals(sId) || i <= 0) {
                i++;
            } else {
                this.m_arrMyPuzzleImage.remove(i);
                int iNewPos = i - 1;
                this.m_arrMyPuzzleImage.add(iNewPos, hMyPuzzleImageLoop);
                saveToSD();
                return iNewPos;
            }
        }
        return -1;
    }

    public int moveDown(MyPuzzleImage hMyPuzzleImage) {
        String sId = hMyPuzzleImage.getId();
        int iSize = this.m_arrMyPuzzleImage.size() - 1;
        for (int i = 0; i < iSize; i++) {
            MyPuzzleImage hMyPuzzleImageLoop = this.m_arrMyPuzzleImage.get(i);
            if (hMyPuzzleImageLoop.getId().equals(sId)) {
                this.m_arrMyPuzzleImage.remove(i);
                int iNewPos = i + 1;
                this.m_arrMyPuzzleImage.add(iNewPos, hMyPuzzleImageLoop);
                saveToSD();
                return iNewPos;
            }
        }
        return -1;
    }

    public void remove(MyPuzzleImage hMyPuzzleImage, boolean bDeleteData) {
        String sId = hMyPuzzleImage.getId();
        Iterator<MyPuzzleImage> it = this.m_arrMyPuzzleImage.iterator();
        while (true) {
            if (it.hasNext()) {
                MyPuzzleImage hMyPuzzleImageLoop = it.next();
                if (hMyPuzzleImageLoop.getId().equals(sId)) {
                    this.m_arrMyPuzzleImage.remove(hMyPuzzleImageLoop);
                    saveToSD();
                    break;
                }
            } else {
                break;
            }
        }
        if (bDeleteData) {
            hMyPuzzleImage.deleteFromSD();
        }
    }

    public void clearThumbnail() {
        Iterator<MyPuzzleImage> it = this.m_arrMyPuzzleImage.iterator();
        while (it.hasNext()) {
            it.next().clearThumbnail();
        }
    }

    public static synchronized boolean isOnSD(String sMyPuzzleId) {
        boolean z;
        synchronized (MyPuzzle.class) {
            MyPuzzleData hMyPuzzleData = loadFromSD(sMyPuzzleId);
            if (hMyPuzzleData == null) {
                z = false;
            } else if (!hMyPuzzleData.getId().equals(sMyPuzzleId)) {
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }

    public void syncWithSD() {
        boolean bSave = false;
        HashSet<String> arrInstalledMyPuzzleImage = new HashSet<>();
        arrInstalledMyPuzzleImage.add(MY_PUZZLE_DATA);
        ArrayList<MyPuzzleImage> arrToDelete = new ArrayList<>();
        Iterator<MyPuzzleImage> it = this.m_arrMyPuzzleImage.iterator();
        while (it.hasNext()) {
            MyPuzzleImage hMyPuzzleImageLoop = it.next();
            String sMyPuzzleId = hMyPuzzleImageLoop.getId();
            if (hMyPuzzleImageLoop.isImageOnSD()) {
                arrInstalledMyPuzzleImage.add(MyPuzzleImage.getImageFilename(sMyPuzzleId));
                arrInstalledMyPuzzleImage.add(MyPuzzleImage.getThumbnailFilename(sMyPuzzleId));
            } else {
                arrToDelete.add(hMyPuzzleImageLoop);
            }
        }
        Iterator it2 = arrToDelete.iterator();
        while (it2.hasNext()) {
            MyPuzzleImage hMyPuzzleImageLoop2 = (MyPuzzleImage) it2.next();
            hMyPuzzleImageLoop2.deleteFromSD();
            this.m_arrMyPuzzleImage.remove(hMyPuzzleImageLoop2);
            bSave = true;
        }
        if (bSave) {
            saveToSD();
        }
        File[] arrMyPuzzleDir = getMyPuzzleDir(this.m_sId).listFiles();
        if (arrMyPuzzleDir != null) {
            for (File hFileImageLoop : arrMyPuzzleDir) {
                if (!arrInstalledMyPuzzleImage.contains(hFileImageLoop.getName())) {
                    hFileImageLoop.delete();
                }
            }
        }
    }

    public void saveToSD() {
        Exception e;
        File hFileDir = getMyPuzzleDir(this.m_sId);
        File hFileTemp = new File(hFileDir, "my_puzzle.dat.tmp");
        ObjectOutputStream hObjectOut = null;
        try {
            hFileTemp.getParentFile().mkdirs();
            hFileTemp.createNewFile();
            ObjectOutputStream hObjectOut2 = new ObjectOutputStream(new FileOutputStream(hFileTemp));
            try {
                hObjectOut2.writeObject(this.m_hMyPuzzleData);
                hFileTemp.renameTo(new File(hFileDir, MY_PUZZLE_DATA));
                Global.closeOutputStream(hObjectOut2);
            } catch (Exception e2) {
                e = e2;
                hObjectOut = hObjectOut2;
                try {
                    throw new RuntimeException(e);
                } catch (Throwable th) {
                    th = th;
                    Global.closeOutputStream(hObjectOut);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                hObjectOut = hObjectOut2;
                Global.closeOutputStream(hObjectOut);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            throw new RuntimeException(e);
        }
    }

    private static synchronized MyPuzzleData loadFromSD(String sMyPuzzleId) {
        Exception e;
        MyPuzzleData myPuzzleData;
        synchronized (MyPuzzle.class) {
            File hFile = new File(getMyPuzzleDir(sMyPuzzleId), MY_PUZZLE_DATA);
            if (!hFile.exists()) {
                myPuzzleData = null;
            } else {
                ObjectInputStream hIn = null;
                try {
                    ObjectInputStream hIn2 = new ObjectInputStream(new BufferedInputStream(new FileInputStream(hFile), 4096));
                    try {
                        MyPuzzleData hMyPuzzleData = (MyPuzzleData) hIn2.readObject();
                        if (!hMyPuzzleData.getId().equals(sMyPuzzleId)) {
                            hFile.delete();
                            Global.closeInputStream(hIn2);
                            myPuzzleData = null;
                        } else {
                            Global.closeInputStream(hIn2);
                            myPuzzleData = hMyPuzzleData;
                        }
                    } catch (Exception e2) {
                        e = e2;
                        hIn = hIn2;
                        try {
                            hFile.delete();
                            throw new RuntimeException(e);
                        } catch (Throwable th) {
                            th = th;
                            Global.closeInputStream(hIn);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        hIn = hIn2;
                        Global.closeInputStream(hIn);
                        throw th;
                    }
                } catch (Exception e3) {
                    e = e3;
                    hFile.delete();
                    throw new RuntimeException(e);
                }
            }
        }
        return myPuzzleData;
    }

    public void deleteFromSD() {
        File hFileDir = getMyPuzzleDir(this.m_sId);
        File[] arrFile = hFileDir.listFiles();
        if (arrFile != null) {
            for (File hFileLoop : arrFile) {
                hFileLoop.delete();
            }
        }
        hFileDir.delete();
    }

    public static synchronized File getMyPuzzleDir(String sMyPuzzleId) {
        File hFileDir;
        synchronized (MyPuzzle.class) {
            hFileDir = new File(MyPuzzleList.getMyPuzzleRootDir(), sMyPuzzleId);
            hFileDir.mkdirs();
        }
        return hFileDir;
    }

    public static synchronized Bitmap createIcon(String sImageFilePath) {
        Bitmap createBitmapFromImageFile;
        synchronized (MyPuzzle.class) {
            createBitmapFromImageFile = BitmapHelper.createBitmapFromImageFile(sImageFilePath, ICON_SIZE);
        }
        return createBitmapFromImageFile;
    }
}
