package dialog;

import android.app.Activity;
import android.os.Handler;
import android.widget.LinearLayout;

public abstract class DialogScreenBase extends LinearLayout {
    protected final DialogManager m_hDialogManager;
    protected final Handler m_hHandler = new Handler();

    public DialogScreenBase(Activity hActivity, DialogManager hDialogManager) {
        super(hActivity);
        this.m_hDialogManager = hDialogManager;
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        setOrientation(1);
        setGravity(17);
    }

    public void cleanUp() {
    }

    public boolean unlockScreenOnDismiss() {
        return true;
    }
}
