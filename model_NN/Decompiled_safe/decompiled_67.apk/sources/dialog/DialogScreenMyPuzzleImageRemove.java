package dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cfg.Option;
import data.MyPuzzleImage;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class DialogScreenMyPuzzleImageRemove extends DialogScreenBase {
    private static final int BUTTON_ID_CANCEL = 0;
    private static final int BUTTON_ID_OK = 1;
    private final View.OnClickListener m_hOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case 0:
                    DialogScreenMyPuzzleImageRemove.this.m_hDialogManager.dismiss();
                    return;
                case 1:
                    DialogScreenMyPuzzleImageRemove.this.m_hDialogManager.dismiss();
                    DialogScreenMyPuzzleImageRemove.this.m_hOnDialogClick.onClick(DialogScreenMyPuzzleImageRemove.this.m_hDialogManager, -1);
                    return;
                default:
                    throw new RuntimeException("Invalid view id");
            }
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener m_hOnDialogClick;
    private final ImageView m_ivMyPuzzleImageThumbnail;

    public DialogScreenMyPuzzleImageRemove(Activity hActivity, DialogManager hDialogManager) {
        super(hActivity, hDialogManager);
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        TitleView titleView = new TitleView(hActivity, hMetrics);
        titleView.setTitle(ResString.dialog_screen_my_puzzle_image_remove_title);
        addView(titleView);
        LinearLayout linearLayout = new LinearLayout(hActivity);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout.setOrientation(1);
        linearLayout.setGravity(17);
        addView(linearLayout);
        TextView textView = new TextView(hActivity);
        textView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        textView.setGravity(17);
        textView.setTextColor(-1);
        textView.setTextSize(1, 20.0f);
        textView.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        textView.setTypeface(Typeface.DEFAULT, 1);
        int iDip20 = ResDimens.getDip(hMetrics, 20);
        textView.setPadding(iDip20, 0, iDip20, iDip20);
        textView.setText(ResString.dialog_screen_my_puzzle_image_remove_text);
        linearLayout.addView(textView);
        RelativeLayout relativeLayout = new RelativeLayout(hActivity);
        int iDipThumbnailBorderSize = ResDimens.getDip(hMetrics, 100);
        relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(iDipThumbnailBorderSize, iDipThumbnailBorderSize));
        relativeLayout.setBackgroundColor(-1);
        linearLayout.addView(relativeLayout);
        View view2 = new View(hActivity);
        int iDipThumbnailSize = ResDimens.getDip(hMetrics, 96);
        RelativeLayout.LayoutParams hImageBackgroundParam = new RelativeLayout.LayoutParams(iDipThumbnailSize, iDipThumbnailSize);
        hImageBackgroundParam.addRule(13);
        view2.setLayoutParams(hImageBackgroundParam);
        view2.setBackgroundColor(Option.HINT_COLOR_DEFAULT);
        relativeLayout.addView(view2);
        this.m_ivMyPuzzleImageThumbnail = new ImageView(hActivity);
        RelativeLayout.LayoutParams hImageParam = new RelativeLayout.LayoutParams(iDipThumbnailSize, iDipThumbnailSize);
        hImageParam.addRule(13);
        this.m_ivMyPuzzleImageThumbnail.setLayoutParams(hImageParam);
        relativeLayout.addView(this.m_ivMyPuzzleImageThumbnail);
        ButtonContainer buttonContainer = new ButtonContainer(hActivity);
        buttonContainer.createButton(0, "btn_img_cancel", this.m_hOnClick);
        buttonContainer.createButton(1, "btn_img_ok", this.m_hOnClick);
        addView(buttonContainer);
    }

    public void onShow(MyPuzzleImage hMyPuzzleImage, DialogInterface.OnClickListener hOnDialogClick) {
        this.m_hOnDialogClick = hOnDialogClick;
        this.m_ivMyPuzzleImageThumbnail.setImageBitmap(hMyPuzzleImage.getThumbnail(ResDimens.getDip(getResources().getDisplayMetrics(), 96)));
    }

    public void cleanUp() {
        super.cleanUp();
        this.m_ivMyPuzzleImageThumbnail.setImageBitmap(null);
        this.m_hOnDialogClick = null;
    }
}
