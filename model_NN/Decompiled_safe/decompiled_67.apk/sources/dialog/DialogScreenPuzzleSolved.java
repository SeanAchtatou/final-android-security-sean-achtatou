package dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import cfg.Option;
import res.ResDimens;
import screen.ScreenPlayBase;
import view.ButtonContainer;

public final class DialogScreenPuzzleSolved extends DialogScreenBase {
    private static final int BUTTON_ID_INFO = 1;
    private static final int BUTTON_ID_NEXT = 2;
    private static final int BUTTON_ID_SAVE = 0;
    private final View m_btnInfo;
    private final View m_btnSave;
    private final View.OnClickListener m_hOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case 0:
                    DialogScreenPuzzleSolved.this.m_hDialogManager.dismiss();
                    DialogScreenPuzzleSolved.this.m_hHandler.post(new Runnable() {
                        public void run() {
                            DialogScreenPuzzleSolved.this.m_hDialogManager.showSaveImage(DialogScreenPuzzleSolved.this.m_hResultData.sPuzzleId, DialogScreenPuzzleSolved.this.m_hResultData.iImageIndex, DialogScreenPuzzleSolved.this.m_hResultData, DialogScreenPuzzleSolved.this.m_hOnDismiss);
                        }
                    });
                    return;
                case 1:
                    DialogScreenPuzzleSolved.this.m_hDialogManager.dismiss();
                    DialogScreenPuzzleSolved.this.m_hHandler.post(new Runnable() {
                        public void run() {
                            DialogScreenPuzzleSolved.this.m_hDialogManager.showPuzzleImageInfo(DialogScreenPuzzleSolved.this.m_hResultData.sPuzzleImageInfo, DialogScreenPuzzleSolved.this.m_hResultData, DialogScreenPuzzleSolved.this.m_hOnDismiss);
                        }
                    });
                    return;
                case 2:
                    DialogScreenPuzzleSolved.this.m_hDialogManager.dismiss();
                    DialogScreenPuzzleSolved.this.m_hHandler.post(new Runnable() {
                        public void run() {
                            DialogScreenPuzzleSolved.this.m_hDialogManager.showResult(DialogScreenPuzzleSolved.this.m_hResultData, DialogScreenPuzzleSolved.this.m_hOnDismiss);
                        }
                    });
                    return;
                default:
                    throw new RuntimeException("Invalid view id");
            }
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnDismissListener m_hOnDismiss;
    /* access modifiers changed from: private */
    public ScreenPlayBase.ResultData m_hResultData;
    private final ImageView m_ivImage;

    public DialogScreenPuzzleSolved(Activity hActivity, DialogManager hDialogManager) {
        super(hActivity, hDialogManager);
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        RelativeLayout vgContentContainer = new RelativeLayout(hActivity);
        vgContentContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        addView(vgContentContainer);
        RelativeLayout vgImageContainer = new RelativeLayout(hActivity);
        RelativeLayout.LayoutParams hImageContainerParam = new RelativeLayout.LayoutParams(hMetrics.widthPixels, hMetrics.widthPixels);
        hImageContainerParam.addRule(13);
        vgImageContainer.setLayoutParams(hImageContainerParam);
        vgImageContainer.setBackgroundColor(-1);
        vgContentContainer.addView(vgImageContainer);
        this.m_ivImage = new ImageView(hActivity);
        RelativeLayout.LayoutParams hImageParam = new RelativeLayout.LayoutParams(-1, -1);
        hImageParam.addRule(13);
        int iDip2 = ResDimens.getDip(hMetrics, 2);
        hImageParam.setMargins(iDip2, iDip2, iDip2, iDip2);
        this.m_ivImage.setLayoutParams(hImageParam);
        this.m_ivImage.setBackgroundColor(Option.HINT_COLOR_DEFAULT);
        vgImageContainer.addView(this.m_ivImage);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        this.m_btnSave = vgButtonContainer.createButton(0, "btn_img_save", this.m_hOnClick);
        this.m_btnInfo = vgButtonContainer.createButton(1, "btn_img_info", this.m_hOnClick);
        vgButtonContainer.createButton(2, "btn_img_next", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    public void onShow(ScreenPlayBase.ResultData hResultData, DialogInterface.OnDismissListener hOnDismiss) {
        this.m_hOnDismiss = hOnDismiss;
        this.m_ivImage.setImageBitmap(hResultData.hImage);
        this.m_hResultData = hResultData;
        if (this.m_hResultData.sPuzzleId == null) {
            this.m_btnSave.setVisibility(8);
            this.m_btnInfo.setVisibility(8);
            return;
        }
        switch (hResultData.iGameMode) {
            case 0:
                this.m_btnSave.setVisibility(0);
                this.m_btnInfo.setVisibility(0);
                this.m_btnInfo.setEnabled(!TextUtils.isEmpty(this.m_hResultData.sPuzzleImageInfo));
                this.m_btnInfo.invalidate();
                return;
            case 1:
                this.m_btnSave.setVisibility(8);
                this.m_btnInfo.setVisibility(8);
                return;
            case 2:
                this.m_btnSave.setVisibility(8);
                this.m_btnInfo.setVisibility(8);
                return;
            default:
                throw new RuntimeException("Invalid game mode");
        }
    }

    public boolean unlockScreenOnDismiss() {
        return false;
    }

    public void cleanUp() {
        super.cleanUp();
        this.m_ivImage.setImageBitmap(null);
        this.m_btnInfo.setEnabled(true);
        this.m_btnSave.setEnabled(true);
    }
}
