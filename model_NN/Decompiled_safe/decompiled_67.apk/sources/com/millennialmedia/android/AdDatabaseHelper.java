package com.millennialmedia.android;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.millennialmedia.android.MMAdViewSDK;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class AdDatabaseHelper extends SQLiteOpenHelper {
    private static final String ACTIVITY = "activity";
    private static final String ADS_TABLE_NAME = "ads";
    private static final String AD_ID = "adid";
    private static final String ANDROID_ANCHOR = "anchor";
    private static final String ANDROID_ANCHOR_2 = "anchor2";
    private static final String ANDROID_POSITION = "position";
    private static final String ANDROID_POSITION_2 = "position2";
    private static final String APPEARANCE_DELAY = "appearancedelay";
    private static final String BUTTONS_TABLE_NAME = "buttons";
    private static final String CONTENT_URL = "contenturl";
    private static final String DB_NAME = "millennialmedia.db";
    private static final int DB_VERSION = 31;
    private static final String DEFERRED_VIEW_START = "deferredviewstart";
    private static final String DURATION = "duration";
    private static final String END_ACTIVITY = "endactivity";
    private static final String END_OPACITY = "endopacity";
    private static final String EXPIRATION = "expiration";
    private static final String FADE_DURATION = "fadeduration";
    private static final String IMAGE_URL = "imageurl";
    private static final String INACTIVITY_TIMEOUT = "inactivitytimeout";
    private static final String LINK_URL = "linkurl";
    private static final String LOG = "log";
    private static final String NAME = "name";
    private static final String ON_COMPLETION = "oncompletion";
    private static final String PADDING_BOTTOM = "paddingbottom";
    private static final String PADDING_LEFT = "paddingleft";
    private static final String PADDING_RIGHT = "paddingright";
    private static final String PADDING_TOP = "paddingtop";
    private static final String SD_CARD = "sdcard";
    private static final String SHOW_CONTROLS = "showcontrols";
    private static final String SHOW_COUNTDOWN = "showcountdown";
    private static final String START_ACTIVITY = "startactivity";
    private static final String START_OPACITY = "startopacity";
    private static final String STAY_IN_PLAYER = "stayInPlayer";
    private static final String TYPE = "type";
    private static final String _ID = "id";
    private SQLiteDatabase db = getReadableDatabase();

    AdDatabaseHelper(Context context) {
        super(context, DB_NAME, (SQLiteDatabase.CursorFactory) null, (int) DB_VERSION);
    }

    public void onOpen(SQLiteDatabase db2) {
    }

    public void onCreate(SQLiteDatabase db2) {
        MMAdViewSDK.Log.d("Creating cached ad database");
        db2.execSQL("CREATE TABLE ads (id INTEGER NOT NULL PRIMARY KEY,name TEXT,type INTEGER,startactivity BLOB,endactivity BLOB,showcontrols INTEGER,contenturl TEXT,expiration TEXT,deferredviewstart BIGINT,oncompletion TEXT,duration BIGINT,stayInPlayer INTEGER,log BLOB,sdcard INTEGER,showcountdown INTEGER);");
        db2.execSQL("CREATE TABLE buttons (id INTEGER NOT NULL PRIMARY KEY,imageurl TEXT,linkurl TEXT,activity BLOB,position INTEGER,anchor INTEGER,position2 INTEGER,anchor2 INTEGER,paddingtop INTEGER,paddingleft INTEGER,paddingbottom INTEGER,paddingright INTEGER,appearancedelay BIGINT,inactivitytimeout BIGINT,startopacity FLOAT,endopacity FLOAT,fadeduration BIGINT,adid INTEGER CONSTRAINT fk_buttons_ads_id REFERENCES ads(id) ON DELETE CASCADE);");
        db2.execSQL("CREATE TRIGGER fk_buttons_ads_id BEFORE DELETE ON ads FOR EACH ROW BEGIN DELETE from buttons WHERE buttons.adid=OLD.id; END;");
    }

    public void onUpgrade(SQLiteDatabase db2, int oldVersion, int newVersion) {
        MMAdViewSDK.Log.v("Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        db2.execSQL("DROP TABLE IF EXISTS ads");
        db2.execSQL("DROP TABLE IF EXISTS buttons");
        onCreate(db2);
    }

    public synchronized void close() {
        if (this.db != null) {
            this.db.close();
        }
        super.close();
    }

    /* access modifiers changed from: package-private */
    public void storeAd(VideoAd ad2) {
        ContentValues adValues = new ContentValues();
        adValues.put(NAME, ad2.id);
        adValues.put(TYPE, Integer.valueOf(ad2.type));
        putArray(adValues, START_ACTIVITY, ad2.startActivity);
        putArray(adValues, END_ACTIVITY, ad2.endActivity);
        adValues.put(SHOW_CONTROLS, Boolean.valueOf(ad2.showControls));
        adValues.put(CONTENT_URL, ad2.contentUrl);
        try {
            if (ad2.expiration != null) {
                adValues.put(EXPIRATION, new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy").format(ad2.expiration).toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        adValues.put(DEFERRED_VIEW_START, Long.valueOf(ad2.deferredViewStart));
        adValues.put(ON_COMPLETION, ad2.onCompletionUrl);
        adValues.put(DURATION, Long.valueOf(ad2.duration));
        adValues.put(STAY_IN_PLAYER, Boolean.valueOf(ad2.stayInPlayer));
        adValues.put(SD_CARD, Boolean.valueOf(ad2.storedOnSdCard));
        adValues.put(SHOW_COUNTDOWN, Boolean.valueOf(ad2.showCountdown));
        putArray(adValues, LOG, ad2.activities.toArray());
        long adid = this.db.insert(ADS_TABLE_NAME, null, adValues);
        for (int i = 0; i < ad2.buttons.size(); i++) {
            VideoImage button = ad2.buttons.get(i);
            ContentValues buttonValues = new ContentValues();
            buttonValues.put(IMAGE_URL, button.imageUrl);
            buttonValues.put(LINK_URL, button.linkUrl);
            putArray(buttonValues, ACTIVITY, button.activity);
            buttonValues.put(ANDROID_POSITION, Integer.valueOf(button.position));
            buttonValues.put(ANDROID_ANCHOR, Integer.valueOf(button.anchor));
            buttonValues.put(ANDROID_POSITION_2, Integer.valueOf(button.position2));
            buttonValues.put(ANDROID_ANCHOR_2, Integer.valueOf(button.anchor2));
            buttonValues.put(PADDING_TOP, Integer.valueOf(button.paddingTop));
            buttonValues.put(PADDING_LEFT, Integer.valueOf(button.paddingLeft));
            buttonValues.put(PADDING_RIGHT, Integer.valueOf(button.paddingRight));
            buttonValues.put(PADDING_BOTTOM, Integer.valueOf(button.paddingBottom));
            buttonValues.put(APPEARANCE_DELAY, Long.valueOf(button.appearanceDelay));
            buttonValues.put(INACTIVITY_TIMEOUT, Long.valueOf(button.inactivityTimeout));
            buttonValues.put(START_OPACITY, Float.valueOf(button.fromAlpha));
            buttonValues.put(END_OPACITY, Float.valueOf(button.toAlpha));
            buttonValues.put(FADE_DURATION, Long.valueOf(button.fadeDuration));
            buttonValues.put(AD_ID, Long.valueOf(adid));
            this.db.insert(BUTTONS_TABLE_NAME, null, buttonValues);
        }
    }

    /* access modifiers changed from: package-private */
    public VideoAd getVideoAd(String adName) {
        Cursor cursor = this.db.rawQuery("SELECT DISTINCT ads.name,ads.contenturl,ads.expiration,ads.deferredviewstart,ads.oncompletion,ads.showcontrols,ads.startactivity,ads.endactivity,ads.duration,ads.stayInPlayer,ads.log,ads.id,ads.sdcard,ads.showcountdown FROM ads WHERE ads.name='" + adName + "'", null);
        int row = cursor.getCount();
        VideoAd ad2 = null;
        if (row > 0) {
            ad2 = new VideoAd();
            cursor.moveToFirst();
            ad2.id = cursor.getString(0);
            ad2.contentUrl = cursor.getString(1);
            String exp = cursor.getString(2);
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
                if (exp != null) {
                    ad2.expiration = simpleDateFormat.parse(exp);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            ad2.deferredViewStart = cursor.getLong(3);
            ad2.onCompletionUrl = cursor.getString(4);
            ad2.showControls = cursor.getInt(5) == 1;
            ad2.startActivity = (String[]) getArray(cursor, 6, new String[0]);
            if (ad2.startActivity == null) {
                ad2.startActivity = new String[0];
            }
            ad2.endActivity = (String[]) getArray(cursor, 7, new String[0]);
            if (ad2.endActivity == null) {
                ad2.endActivity = new String[0];
            }
            ad2.duration = cursor.getLong(8);
            ad2.stayInPlayer = cursor.getInt(9) == 1;
            VideoLogEvent[] logEvents = (VideoLogEvent[]) getArray(cursor, 10, new VideoLogEvent[0]);
            ad2.activities = new ArrayList<>();
            if (logEvents != null) {
                for (int i = 0; i < logEvents.length; i++) {
                    ad2.activities.add(logEvents[i]);
                }
            }
            int id = cursor.getInt(11);
            ad2.storedOnSdCard = cursor.getInt(12) == 1;
            ad2.showCountdown = cursor.getInt(13) == 1;
            Cursor buttonsCursor = this.db.rawQuery("SELECT DISTINCT buttons.imageurl,buttons.linkurl,buttons.activity,buttons.position,buttons.anchor,buttons.position2,buttons.anchor2,buttons.paddingleft,buttons.paddingtop,buttons.paddingright,buttons.paddingbottom,buttons.appearancedelay,buttons.inactivitytimeout,buttons.startopacity,buttons.endopacity,buttons.fadeduration,buttons.id FROM ads,buttons WHERE buttons.adid=" + id + " ORDER BY " + BUTTONS_TABLE_NAME + "." + _ID, null);
            int buttonsCount = buttonsCursor.getCount();
            if (buttonsCount > 0) {
                buttonsCursor.moveToFirst();
                ad2.buttons = new ArrayList<>(row);
                for (int i2 = 0; i2 < buttonsCount; i2++) {
                    VideoImage button = new VideoImage();
                    button.imageUrl = buttonsCursor.getString(0);
                    button.linkUrl = buttonsCursor.getString(1);
                    button.activity = (String[]) getArray(buttonsCursor, 2, new String[0]);
                    if (button.activity == null) {
                        button.activity = new String[0];
                    }
                    button.position = buttonsCursor.getInt(3);
                    button.anchor = buttonsCursor.getInt(4);
                    button.position2 = buttonsCursor.getInt(5);
                    button.anchor2 = buttonsCursor.getInt(6);
                    button.paddingLeft = buttonsCursor.getInt(7);
                    button.paddingTop = buttonsCursor.getInt(8);
                    button.paddingRight = buttonsCursor.getInt(9);
                    button.paddingBottom = buttonsCursor.getInt(10);
                    button.appearanceDelay = buttonsCursor.getLong(11);
                    button.inactivityTimeout = buttonsCursor.getLong(12);
                    button.fromAlpha = buttonsCursor.getFloat(13);
                    button.toAlpha = buttonsCursor.getFloat(14);
                    button.fadeDuration = buttonsCursor.getLong(15);
                    ad2.buttons.add(button);
                    if (!buttonsCursor.isLast()) {
                        buttonsCursor.moveToNext();
                    }
                }
            }
            buttonsCursor.close();
        }
        cursor.close();
        return ad2;
    }

    /* access modifiers changed from: package-private */
    public boolean checkIfAdExists(String adName) {
        Cursor cursor = this.db.rawQuery("SELECT * FROM ads WHERE ads.name='" + adName + "'", null);
        int row = cursor.getCount();
        cursor.close();
        if (row > 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean shouldShowBottomBar(String adName) {
        Cursor cursor = this.db.rawQuery("SELECT DISTINCT showcontrols FROM ads WHERE ads.name='" + adName + "'", null);
        int shouldShowControls = 1;
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            shouldShowControls = cursor.getInt(0);
        }
        cursor.close();
        return shouldShowControls == 1;
    }

    /* access modifiers changed from: package-private */
    public boolean purgeAdFromDb(String adName) {
        if (this.db.delete(ADS_TABLE_NAME, "ads.name=?", new String[]{adName}) > 0) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public List<VideoImage> getButtonsForAd(String name) {
        Cursor cursor = this.db.rawQuery("SELECT DISTINCT buttons.imageurl,buttons.linkurl,buttons.activity,buttons.position,buttons.anchor,buttons.position2,buttons.anchor2,buttons.paddingleft,buttons.paddingtop,buttons.paddingright,buttons.paddingbottom,buttons.appearancedelay,buttons.inactivitytimeout,buttons.startopacity,buttons.endopacity,buttons.fadeduration,buttons.id FROM ads,buttons WHERE ads.name='" + name + "'" + " AND " + BUTTONS_TABLE_NAME + "." + AD_ID + "=" + ADS_TABLE_NAME + "." + _ID + " ORDER BY " + BUTTONS_TABLE_NAME + "." + _ID, null);
        int row = cursor.getCount();
        List<VideoImage> buttons = null;
        cursor.moveToFirst();
        if (row > 0) {
            buttons = new ArrayList<>(row);
            for (int i = 0; i < row; i++) {
                VideoImage button = new VideoImage();
                button.imageUrl = cursor.getString(0);
                button.linkUrl = cursor.getString(1);
                button.activity = (String[]) getArray(cursor, 2, new String[0]);
                if (button.activity == null) {
                    button.activity = new String[0];
                }
                button.position = cursor.getInt(3);
                button.anchor = cursor.getInt(4);
                button.position2 = cursor.getInt(5);
                button.anchor2 = cursor.getInt(6);
                button.paddingLeft = cursor.getInt(7);
                button.paddingTop = cursor.getInt(8);
                button.paddingRight = cursor.getInt(9);
                button.paddingBottom = cursor.getInt(10);
                button.appearanceDelay = cursor.getLong(11);
                button.inactivityTimeout = cursor.getLong(12);
                button.fromAlpha = cursor.getFloat(13);
                button.toAlpha = cursor.getFloat(14);
                button.fadeDuration = cursor.getLong(15);
                buttons.add(button);
                if (!cursor.isLast()) {
                    cursor.moveToNext();
                }
            }
        }
        cursor.close();
        return buttons;
    }

    /* access modifiers changed from: package-private */
    public int getButtonCountForAd(String name) {
        Cursor cursor = this.db.rawQuery("SELECT COUNT(*)  FROM ads,buttons WHERE ads.name='" + name + "'" + " AND " + BUTTONS_TABLE_NAME + "." + AD_ID + "=" + ADS_TABLE_NAME + "." + _ID, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    /* access modifiers changed from: package-private */
    public boolean isAdOnSDCard(String name) {
        int result = 0;
        Cursor cursor = this.db.rawQuery("SELECT sdcard FROM ads WHERE ads.name='" + name + "'", null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            result = cursor.getInt(0);
        }
        cursor.close();
        return result == 1;
    }

    /* access modifiers changed from: package-private */
    public void updateAdOnSDCard(String name, int isOnSdCard) {
        this.db.rawQuery("UPDATE ads SET sdcard = " + isOnSdCard + " WHERE " + ADS_TABLE_NAME + "." + NAME + "='" + name + "'", null).close();
    }

    /* access modifiers changed from: package-private */
    public List<String> getAllExpiredAds() {
        Cursor cursor = this.db.rawQuery("SELECT ads.expiration,ads.name FROM ads", null);
        int row = cursor.getCount();
        if (row > 0) {
            List<String> expiredAds = new ArrayList<>();
            cursor.moveToFirst();
            for (int i = 0; i < row; i++) {
                String exp = cursor.getString(0);
                String name = cursor.getString(1);
                if (exp != null) {
                    try {
                        Date expiration = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy").parse(exp);
                        if (expiration != null && expiration.getTime() <= System.currentTimeMillis()) {
                            MMAdViewSDK.Log.v(name + " is expired");
                            if (name != null) {
                                expiredAds.add(name);
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (name != null) {
                    expiredAds.add(name);
                }
                if (!cursor.isLast()) {
                    cursor.moveToNext();
                }
            }
            cursor.close();
            return expiredAds;
        }
        cursor.close();
        return null;
    }

    /* access modifiers changed from: package-private */
    public Date getExpiration(String adName) {
        Cursor cursor = this.db.rawQuery("SELECT ads.expiration FROM ads WHERE ads.name='" + adName + "'", null);
        Date expiration = null;
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            String exp = cursor.getString(0);
            if (exp != null) {
                try {
                    expiration = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy").parse(exp);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            cursor.close();
            return expiration;
        }
        cursor.close();
        return null;
    }

    /* access modifiers changed from: package-private */
    public long getDeferredViewStart(String adName) {
        Cursor cursor = this.db.rawQuery("SELECT ads.deferredviewstart FROM ads WHERE ads.name='" + adName + "'", null);
        int row = cursor.getCount();
        long deferredViewStart = System.currentTimeMillis();
        if (row > 0) {
            cursor.moveToFirst();
            deferredViewStart = cursor.getLong(0);
        }
        cursor.close();
        return deferredViewStart;
    }

    /* access modifiers changed from: package-private */
    public void updateDeferredViewStart(String adName) {
        this.db.rawQuery("UPDATE ads SET deferredviewstart = " + Long.toString(System.currentTimeMillis()) + " WHERE " + ADS_TABLE_NAME + "." + NAME + "='" + adName + "'", null).close();
    }

    /* access modifiers changed from: package-private */
    public void updateAdData(VideoAd ad2) {
        if (ad2 != null) {
            ContentValues adValues = new ContentValues();
            putArray(adValues, START_ACTIVITY, ad2.startActivity);
            putArray(adValues, END_ACTIVITY, ad2.endActivity);
            putArray(adValues, LOG, ad2.activities.toArray());
            this.db.update(ADS_TABLE_NAME, adValues, "ads.name=?", new String[]{ad2.id});
            int adIdNum = getIdForAdName(ad2.id);
            if (adIdNum > 0) {
                for (int i = 0; i < ad2.buttons.size(); i++) {
                    VideoImage button = ad2.buttons.get(i);
                    ContentValues buttonValues = new ContentValues();
                    putArray(buttonValues, ACTIVITY, button.activity);
                    this.db.update(BUTTONS_TABLE_NAME, buttonValues, "buttons.adid=? AND buttons.imageurl=? ", new String[]{String.valueOf(adIdNum), button.imageUrl});
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int getIdForAdName(String name) {
        Cursor cursor = this.db.query(ADS_TABLE_NAME, new String[]{_ID}, "ads.name= ?", new String[]{name}, null, null, null);
        int id = 0;
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            id = cursor.getInt(0);
        }
        cursor.close();
        return id;
    }

    private <T> T[] getArray(Cursor cursor, int index, T[] emptyArray) {
        try {
            ByteArrayInputStream byteIn = new ByteArrayInputStream(cursor.getBlob(index));
            ObjectInputStream in = new ObjectInputStream(byteIn);
            ArrayList<T> list = (ArrayList) in.readObject();
            in.close();
            byteIn.close();
            if (list != null) {
                return list.toArray(emptyArray);
            }
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (ClassNotFoundException e3) {
            e3.printStackTrace();
        }
        return null;
    }

    private <T> void putArray(ContentValues values, String col, T[] array) {
        if (array != null) {
            try {
                List<T> list = new ArrayList<>();
                for (T add : array) {
                    list.add(add);
                }
                ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(byteOut);
                out.writeObject(list);
                byte[] byteArray = byteOut.toByteArray();
                out.close();
                byteOut.close();
                values.put(col, byteArray);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
