package com.millennialmedia.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.MediaController;
import com.millennialmedia.android.MMAdViewSDK;
import java.io.IOException;
import java.util.Map;

public class MillennialMediaView extends SurfaceView implements MediaController.MediaPlayerControl {
    private static final int STATE_ERROR = -1;
    private static final int STATE_IDLE = 0;
    private static final int STATE_PAUSED = 4;
    private static final int STATE_PLAYBACK_COMPLETED = 5;
    private static final int STATE_PLAYING = 3;
    private static final int STATE_PREPARED = 2;
    private static final int STATE_PREPARING = 1;
    private static final int STATE_RESUME = 7;
    private static final int STATE_SUSPEND = 6;
    private static final int STATE_SUSPEND_UNSUPPORTED = 8;
    private String TAG;
    private MediaPlayer.OnBufferingUpdateListener mBufferingUpdateListener;
    /* access modifiers changed from: private */
    public boolean mCanPause;
    /* access modifiers changed from: private */
    public boolean mCanSeekBack;
    /* access modifiers changed from: private */
    public boolean mCanSeekForward;
    private MediaPlayer.OnCompletionListener mCompletionListener;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public int mCurrentBufferPercentage;
    /* access modifiers changed from: private */
    public int mCurrentState;
    private int mDuration;
    private MediaPlayer.OnErrorListener mErrorListener;
    private Map<String, String> mHeaders;
    private MediaPlayer.OnInfoListener mInfoListener;
    /* access modifiers changed from: private */
    public MediaController mMediaController;
    /* access modifiers changed from: private */
    public MediaPlayer mMediaPlayer;
    /* access modifiers changed from: private */
    public MediaPlayer.OnCompletionListener mOnCompletionListener;
    /* access modifiers changed from: private */
    public MediaPlayer.OnErrorListener mOnErrorListener;
    /* access modifiers changed from: private */
    public MediaPlayer.OnPreparedListener mOnPreparedListener;
    MediaPlayer.OnPreparedListener mPreparedListener;
    SurfaceHolder.Callback mSHCallback;
    /* access modifiers changed from: private */
    public int mSeekWhenPrepared;
    MediaPlayer.OnVideoSizeChangedListener mSizeChangedListener;
    private int mStateWhenSuspended;
    /* access modifiers changed from: private */
    public int mSurfaceHeight;
    /* access modifiers changed from: private */
    public SurfaceHolder mSurfaceHolder;
    /* access modifiers changed from: private */
    public int mSurfaceWidth;
    /* access modifiers changed from: private */
    public int mTargetState;
    private Uri mUri;
    /* access modifiers changed from: private */
    public int mVideoHeight;
    /* access modifiers changed from: private */
    public int mVideoWidth;

    public MillennialMediaView(Context context) {
        super(context);
        this.TAG = "VideoView";
        this.mCurrentState = 0;
        this.mTargetState = 0;
        this.mSurfaceHolder = null;
        this.mMediaPlayer = null;
        this.mSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
            public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                int unused = MillennialMediaView.this.mVideoWidth = mp.getVideoWidth();
                int unused2 = MillennialMediaView.this.mVideoHeight = mp.getVideoHeight();
                if (MillennialMediaView.this.mVideoWidth != 0 && MillennialMediaView.this.mVideoHeight != 0) {
                    MillennialMediaView.this.getHolder().setFixedSize(MillennialMediaView.this.mVideoWidth, MillennialMediaView.this.mVideoHeight);
                }
            }
        };
        this.mPreparedListener = new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                int unused = MillennialMediaView.this.mCurrentState = 2;
                boolean unused2 = MillennialMediaView.this.mCanPause = MillennialMediaView.this.mCanSeekBack = MillennialMediaView.this.mCanSeekForward = true;
                if (MillennialMediaView.this.mOnPreparedListener != null) {
                    MillennialMediaView.this.mOnPreparedListener.onPrepared(MillennialMediaView.this.mMediaPlayer);
                }
                if (MillennialMediaView.this.mMediaController != null) {
                    MillennialMediaView.this.mMediaController.setEnabled(true);
                }
                int unused3 = MillennialMediaView.this.mVideoWidth = mp.getVideoWidth();
                int unused4 = MillennialMediaView.this.mVideoHeight = mp.getVideoHeight();
                int seekToPosition = MillennialMediaView.this.mSeekWhenPrepared;
                if (seekToPosition != 0) {
                    MillennialMediaView.this.seekTo(seekToPosition);
                }
                if (MillennialMediaView.this.mVideoWidth != 0 && MillennialMediaView.this.mVideoHeight != 0) {
                    MillennialMediaView.this.getHolder().setFixedSize(MillennialMediaView.this.mVideoWidth, MillennialMediaView.this.mVideoHeight);
                    if (MillennialMediaView.this.mSurfaceWidth != MillennialMediaView.this.mVideoWidth || MillennialMediaView.this.mSurfaceHeight != MillennialMediaView.this.mVideoHeight) {
                        return;
                    }
                    if (MillennialMediaView.this.mTargetState == 3) {
                        MillennialMediaView.this.start();
                        if (MillennialMediaView.this.mMediaController != null) {
                            MillennialMediaView.this.mMediaController.show();
                        }
                    } else if (MillennialMediaView.this.isPlaying()) {
                    } else {
                        if ((seekToPosition != 0 || MillennialMediaView.this.getCurrentPosition() > 0) && MillennialMediaView.this.mMediaController != null) {
                            MillennialMediaView.this.mMediaController.show(0);
                        }
                    }
                } else if (MillennialMediaView.this.mTargetState == 3) {
                    MillennialMediaView.this.start();
                }
            }
        };
        this.mInfoListener = new MediaPlayer.OnInfoListener() {
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                if (what == 801) {
                    boolean unused = MillennialMediaView.this.mCanSeekBack = MillennialMediaView.this.mCanSeekForward = false;
                }
                return false;
            }
        };
        this.mCompletionListener = new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                int unused = MillennialMediaView.this.mCurrentState = 5;
                int unused2 = MillennialMediaView.this.mTargetState = 5;
                if (MillennialMediaView.this.mMediaController != null) {
                    MillennialMediaView.this.mMediaController.hide();
                }
                if (MillennialMediaView.this.mOnCompletionListener != null) {
                    MillennialMediaView.this.mOnCompletionListener.onCompletion(MillennialMediaView.this.mMediaPlayer);
                }
            }
        };
        this.mErrorListener = new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int framework_err, int impl_err) {
                int messageId;
                MMAdViewSDK.Log.d("Error: " + framework_err + "," + impl_err);
                int unused = MillennialMediaView.this.mCurrentState = -1;
                int unused2 = MillennialMediaView.this.mTargetState = -1;
                if (MillennialMediaView.this.mMediaController != null) {
                    MillennialMediaView.this.mMediaController.hide();
                }
                if (MillennialMediaView.this.mOnErrorListener != null && MillennialMediaView.this.mOnErrorListener.onError(MillennialMediaView.this.mMediaPlayer, framework_err, impl_err)) {
                    return true;
                }
                if (MillennialMediaView.this.getWindowToken() != null) {
                    Resources resources = MillennialMediaView.this.mContext.getResources();
                    if (framework_err == 200) {
                        messageId = 17039381;
                    } else {
                        messageId = 17039377;
                    }
                    new AlertDialog.Builder(MillennialMediaView.this.mContext).setTitle("Sorry").setMessage(messageId).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog2, int whichButton) {
                            if (MillennialMediaView.this.mOnCompletionListener != null) {
                                MillennialMediaView.this.mOnCompletionListener.onCompletion(MillennialMediaView.this.mMediaPlayer);
                            }
                        }
                    }).setCancelable(false).show();
                }
                return true;
            }
        };
        this.mBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                int unused = MillennialMediaView.this.mCurrentBufferPercentage = percent;
            }
        };
        this.mSHCallback = new SurfaceHolder.Callback() {
            public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
                boolean isValidState;
                boolean hasValidSize;
                int unused = MillennialMediaView.this.mSurfaceWidth = w;
                int unused2 = MillennialMediaView.this.mSurfaceHeight = h;
                if (MillennialMediaView.this.mTargetState == 3) {
                    isValidState = true;
                } else {
                    isValidState = false;
                }
                if (MillennialMediaView.this.mVideoWidth == w && MillennialMediaView.this.mVideoHeight == h) {
                    hasValidSize = true;
                } else {
                    hasValidSize = false;
                }
                if (MillennialMediaView.this.mMediaPlayer != null && isValidState && hasValidSize) {
                    if (MillennialMediaView.this.mSeekWhenPrepared != 0) {
                        MillennialMediaView.this.seekTo(MillennialMediaView.this.mSeekWhenPrepared);
                    }
                    MillennialMediaView.this.start();
                    if (MillennialMediaView.this.mMediaController != null) {
                        if (MillennialMediaView.this.mMediaController.isShowing()) {
                            MillennialMediaView.this.mMediaController.hide();
                        }
                        MillennialMediaView.this.mMediaController.show();
                    }
                }
            }

            public void surfaceCreated(SurfaceHolder holder) {
                SurfaceHolder unused = MillennialMediaView.this.mSurfaceHolder = holder;
                if (MillennialMediaView.this.mMediaPlayer != null && MillennialMediaView.this.mCurrentState == 6 && MillennialMediaView.this.mTargetState == 7) {
                    MillennialMediaView.this.mMediaPlayer.setDisplay(MillennialMediaView.this.mSurfaceHolder);
                } else {
                    MillennialMediaView.this.openVideo();
                }
            }

            public void surfaceDestroyed(SurfaceHolder holder) {
                SurfaceHolder unused = MillennialMediaView.this.mSurfaceHolder = null;
                if (MillennialMediaView.this.mMediaController != null) {
                    MillennialMediaView.this.mMediaController.hide();
                }
                if (MillennialMediaView.this.mCurrentState != 6) {
                    MillennialMediaView.this.release(true);
                }
            }
        };
        this.mContext = context;
        initVideoView();
    }

    public MillennialMediaView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        this.mContext = context;
        initVideoView();
    }

    public MillennialMediaView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.TAG = "VideoView";
        this.mCurrentState = 0;
        this.mTargetState = 0;
        this.mSurfaceHolder = null;
        this.mMediaPlayer = null;
        this.mSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
            public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                int unused = MillennialMediaView.this.mVideoWidth = mp.getVideoWidth();
                int unused2 = MillennialMediaView.this.mVideoHeight = mp.getVideoHeight();
                if (MillennialMediaView.this.mVideoWidth != 0 && MillennialMediaView.this.mVideoHeight != 0) {
                    MillennialMediaView.this.getHolder().setFixedSize(MillennialMediaView.this.mVideoWidth, MillennialMediaView.this.mVideoHeight);
                }
            }
        };
        this.mPreparedListener = new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                int unused = MillennialMediaView.this.mCurrentState = 2;
                boolean unused2 = MillennialMediaView.this.mCanPause = MillennialMediaView.this.mCanSeekBack = MillennialMediaView.this.mCanSeekForward = true;
                if (MillennialMediaView.this.mOnPreparedListener != null) {
                    MillennialMediaView.this.mOnPreparedListener.onPrepared(MillennialMediaView.this.mMediaPlayer);
                }
                if (MillennialMediaView.this.mMediaController != null) {
                    MillennialMediaView.this.mMediaController.setEnabled(true);
                }
                int unused3 = MillennialMediaView.this.mVideoWidth = mp.getVideoWidth();
                int unused4 = MillennialMediaView.this.mVideoHeight = mp.getVideoHeight();
                int seekToPosition = MillennialMediaView.this.mSeekWhenPrepared;
                if (seekToPosition != 0) {
                    MillennialMediaView.this.seekTo(seekToPosition);
                }
                if (MillennialMediaView.this.mVideoWidth != 0 && MillennialMediaView.this.mVideoHeight != 0) {
                    MillennialMediaView.this.getHolder().setFixedSize(MillennialMediaView.this.mVideoWidth, MillennialMediaView.this.mVideoHeight);
                    if (MillennialMediaView.this.mSurfaceWidth != MillennialMediaView.this.mVideoWidth || MillennialMediaView.this.mSurfaceHeight != MillennialMediaView.this.mVideoHeight) {
                        return;
                    }
                    if (MillennialMediaView.this.mTargetState == 3) {
                        MillennialMediaView.this.start();
                        if (MillennialMediaView.this.mMediaController != null) {
                            MillennialMediaView.this.mMediaController.show();
                        }
                    } else if (MillennialMediaView.this.isPlaying()) {
                    } else {
                        if ((seekToPosition != 0 || MillennialMediaView.this.getCurrentPosition() > 0) && MillennialMediaView.this.mMediaController != null) {
                            MillennialMediaView.this.mMediaController.show(0);
                        }
                    }
                } else if (MillennialMediaView.this.mTargetState == 3) {
                    MillennialMediaView.this.start();
                }
            }
        };
        this.mInfoListener = new MediaPlayer.OnInfoListener() {
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                if (what == 801) {
                    boolean unused = MillennialMediaView.this.mCanSeekBack = MillennialMediaView.this.mCanSeekForward = false;
                }
                return false;
            }
        };
        this.mCompletionListener = new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                int unused = MillennialMediaView.this.mCurrentState = 5;
                int unused2 = MillennialMediaView.this.mTargetState = 5;
                if (MillennialMediaView.this.mMediaController != null) {
                    MillennialMediaView.this.mMediaController.hide();
                }
                if (MillennialMediaView.this.mOnCompletionListener != null) {
                    MillennialMediaView.this.mOnCompletionListener.onCompletion(MillennialMediaView.this.mMediaPlayer);
                }
            }
        };
        this.mErrorListener = new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int framework_err, int impl_err) {
                int messageId;
                MMAdViewSDK.Log.d("Error: " + framework_err + "," + impl_err);
                int unused = MillennialMediaView.this.mCurrentState = -1;
                int unused2 = MillennialMediaView.this.mTargetState = -1;
                if (MillennialMediaView.this.mMediaController != null) {
                    MillennialMediaView.this.mMediaController.hide();
                }
                if (MillennialMediaView.this.mOnErrorListener != null && MillennialMediaView.this.mOnErrorListener.onError(MillennialMediaView.this.mMediaPlayer, framework_err, impl_err)) {
                    return true;
                }
                if (MillennialMediaView.this.getWindowToken() != null) {
                    Resources resources = MillennialMediaView.this.mContext.getResources();
                    if (framework_err == 200) {
                        messageId = 17039381;
                    } else {
                        messageId = 17039377;
                    }
                    new AlertDialog.Builder(MillennialMediaView.this.mContext).setTitle("Sorry").setMessage(messageId).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog2, int whichButton) {
                            if (MillennialMediaView.this.mOnCompletionListener != null) {
                                MillennialMediaView.this.mOnCompletionListener.onCompletion(MillennialMediaView.this.mMediaPlayer);
                            }
                        }
                    }).setCancelable(false).show();
                }
                return true;
            }
        };
        this.mBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                int unused = MillennialMediaView.this.mCurrentBufferPercentage = percent;
            }
        };
        this.mSHCallback = new SurfaceHolder.Callback() {
            public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
                boolean isValidState;
                boolean hasValidSize;
                int unused = MillennialMediaView.this.mSurfaceWidth = w;
                int unused2 = MillennialMediaView.this.mSurfaceHeight = h;
                if (MillennialMediaView.this.mTargetState == 3) {
                    isValidState = true;
                } else {
                    isValidState = false;
                }
                if (MillennialMediaView.this.mVideoWidth == w && MillennialMediaView.this.mVideoHeight == h) {
                    hasValidSize = true;
                } else {
                    hasValidSize = false;
                }
                if (MillennialMediaView.this.mMediaPlayer != null && isValidState && hasValidSize) {
                    if (MillennialMediaView.this.mSeekWhenPrepared != 0) {
                        MillennialMediaView.this.seekTo(MillennialMediaView.this.mSeekWhenPrepared);
                    }
                    MillennialMediaView.this.start();
                    if (MillennialMediaView.this.mMediaController != null) {
                        if (MillennialMediaView.this.mMediaController.isShowing()) {
                            MillennialMediaView.this.mMediaController.hide();
                        }
                        MillennialMediaView.this.mMediaController.show();
                    }
                }
            }

            public void surfaceCreated(SurfaceHolder holder) {
                SurfaceHolder unused = MillennialMediaView.this.mSurfaceHolder = holder;
                if (MillennialMediaView.this.mMediaPlayer != null && MillennialMediaView.this.mCurrentState == 6 && MillennialMediaView.this.mTargetState == 7) {
                    MillennialMediaView.this.mMediaPlayer.setDisplay(MillennialMediaView.this.mSurfaceHolder);
                } else {
                    MillennialMediaView.this.openVideo();
                }
            }

            public void surfaceDestroyed(SurfaceHolder holder) {
                SurfaceHolder unused = MillennialMediaView.this.mSurfaceHolder = null;
                if (MillennialMediaView.this.mMediaController != null) {
                    MillennialMediaView.this.mMediaController.hide();
                }
                if (MillennialMediaView.this.mCurrentState != 6) {
                    MillennialMediaView.this.release(true);
                }
            }
        };
        this.mContext = context;
        initVideoView();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = getDefaultSize(this.mVideoWidth, widthMeasureSpec);
        int height = getDefaultSize(this.mVideoHeight, heightMeasureSpec);
        if (this.mVideoWidth > 0 && this.mVideoHeight > 0) {
            if (this.mVideoWidth * height > this.mVideoHeight * width) {
                height = (this.mVideoHeight * width) / this.mVideoWidth;
            } else if (this.mVideoWidth * height < this.mVideoHeight * width) {
                width = (this.mVideoWidth * height) / this.mVideoHeight;
            }
        }
        setMeasuredDimension(width, height);
    }

    public int resolveAdjustedSize(int desiredSize, int measureSpec) {
        int result = desiredSize;
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        switch (specMode) {
            case Integer.MIN_VALUE:
                return Math.min(desiredSize, specSize);
            case 0:
                return desiredSize;
            case 1073741824:
                return specSize;
            default:
                return result;
        }
    }

    private void initVideoView() {
        this.mVideoWidth = 0;
        this.mVideoHeight = 0;
        getHolder().addCallback(this.mSHCallback);
        getHolder().setType(3);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.mCurrentState = 0;
        this.mTargetState = 0;
    }

    public void setVideoPath(String path) {
        setVideoURI(Uri.parse(path));
    }

    public void setVideoURI(Uri uri) {
        setVideoURI(uri, null);
    }

    public void setVideoURI(Uri uri, Map<String, String> headers) {
        this.mUri = uri;
        this.mHeaders = headers;
        this.mSeekWhenPrepared = 0;
        openVideo();
        requestLayout();
        invalidate();
    }

    public void stopPlayback() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.stop();
            this.mMediaPlayer.release();
            this.mUri = null;
            this.mMediaPlayer = null;
            this.mCurrentState = 0;
            this.mTargetState = 0;
        }
    }

    public void openVideo() {
        if (this.mUri != null && this.mSurfaceHolder != null) {
            Intent i = new Intent("com.android.music.musicservicecommand");
            i.putExtra("command", "pause");
            this.mContext.sendBroadcast(i);
            release(false);
            try {
                this.mMediaPlayer = new MediaPlayer();
                this.mMediaPlayer.setOnPreparedListener(this.mPreparedListener);
                this.mMediaPlayer.setOnVideoSizeChangedListener(this.mSizeChangedListener);
                this.mDuration = -1;
                this.mMediaPlayer.setOnCompletionListener(this.mCompletionListener);
                this.mMediaPlayer.setOnErrorListener(this.mErrorListener);
                this.mMediaPlayer.setOnBufferingUpdateListener(this.mBufferingUpdateListener);
                this.mCurrentBufferPercentage = 0;
                this.mMediaPlayer.setDataSource(this.mContext, this.mUri);
                this.mMediaPlayer.setDisplay(this.mSurfaceHolder);
                this.mMediaPlayer.setAudioStreamType(3);
                this.mMediaPlayer.setScreenOnWhilePlaying(true);
                this.mMediaPlayer.prepareAsync();
                this.mCurrentState = 1;
                attachMediaController();
            } catch (IOException e) {
                Log.w(this.TAG, "Unable to open content: " + this.mUri, e);
                this.mCurrentState = -1;
                this.mTargetState = -1;
                this.mErrorListener.onError(this.mMediaPlayer, 1, 0);
            } catch (IllegalArgumentException e2) {
                Log.w(this.TAG, "Unable to open content: " + this.mUri, e2);
                this.mCurrentState = -1;
                this.mTargetState = -1;
                this.mErrorListener.onError(this.mMediaPlayer, 1, 0);
            }
        }
    }

    public void setMediaController(MediaController controller) {
        if (this.mMediaController != null) {
            this.mMediaController.hide();
        }
        this.mMediaController = controller;
        attachMediaController();
    }

    private void attachMediaController() {
        if (this.mMediaPlayer != null && this.mMediaController != null) {
            this.mMediaController.setMediaPlayer(this);
            this.mMediaController.setAnchorView(getParent() instanceof View ? (View) getParent() : this);
            this.mMediaController.setEnabled(isInPlaybackState());
        }
    }

    public void setOnPreparedListener(MediaPlayer.OnPreparedListener l) {
        this.mOnPreparedListener = l;
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener l) {
        this.mOnCompletionListener = l;
    }

    public void setOnErrorListener(MediaPlayer.OnErrorListener l) {
        this.mOnErrorListener = l;
    }

    /* access modifiers changed from: private */
    public void release(boolean cleartargetstate) {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.reset();
            this.mMediaPlayer.release();
            this.mMediaPlayer = null;
            this.mCurrentState = 0;
            if (cleartargetstate) {
                this.mTargetState = 0;
            }
        }
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (!isInPlaybackState() || this.mMediaController == null) {
            return false;
        }
        toggleMediaControlsVisiblity();
        return false;
    }

    public boolean onTrackballEvent(MotionEvent ev) {
        if (!isInPlaybackState() || this.mMediaController == null) {
            return false;
        }
        toggleMediaControlsVisiblity();
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean isKeyCodeSupported;
        if (keyCode == 4 || keyCode == 24 || keyCode == 25 || keyCode == 82 || keyCode == 5 || keyCode == 6) {
            isKeyCodeSupported = false;
        } else {
            isKeyCodeSupported = true;
        }
        if (isInPlaybackState() && isKeyCodeSupported && this.mMediaController != null) {
            if (keyCode == 79 || keyCode == 85) {
                if (this.mMediaPlayer.isPlaying()) {
                    pause();
                    this.mMediaController.show();
                } else {
                    start();
                    this.mMediaController.hide();
                }
                return true;
            } else if (keyCode != 86 || !this.mMediaPlayer.isPlaying()) {
                toggleMediaControlsVisiblity();
            } else {
                pause();
                this.mMediaController.show();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void toggleMediaControlsVisiblity() {
        if (this.mMediaController.isShowing()) {
            this.mMediaController.hide();
        } else {
            this.mMediaController.show();
        }
    }

    public void start() {
        if (isInPlaybackState()) {
            this.mMediaPlayer.start();
            this.mCurrentState = 3;
        }
        this.mTargetState = 3;
    }

    public void pause() {
        if (isInPlaybackState() && this.mMediaPlayer.isPlaying()) {
            this.mMediaPlayer.pause();
            this.mCurrentState = 4;
        }
        this.mTargetState = 4;
    }

    public int getDuration() {
        if (!isInPlaybackState()) {
            this.mDuration = -1;
            return this.mDuration;
        } else if (this.mDuration > 0) {
            return this.mDuration;
        } else {
            this.mDuration = this.mMediaPlayer.getDuration();
            return this.mDuration;
        }
    }

    public int getCurrentPosition() {
        if (isInPlaybackState()) {
            return this.mMediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    public void seekTo(int msec) {
        if (isInPlaybackState()) {
            this.mMediaPlayer.seekTo(msec);
            this.mSeekWhenPrepared = 0;
            return;
        }
        this.mSeekWhenPrepared = msec;
    }

    public boolean isPlaying() {
        return isInPlaybackState() && this.mMediaPlayer.isPlaying();
    }

    public int getBufferPercentage() {
        if (this.mMediaPlayer != null) {
            return this.mCurrentBufferPercentage;
        }
        return 0;
    }

    private boolean isInPlaybackState() {
        return (this.mMediaPlayer == null || this.mCurrentState == -1 || this.mCurrentState == 0 || this.mCurrentState == 1) ? false : true;
    }

    public boolean canPause() {
        return this.mCanPause;
    }

    public boolean canSeekBackward() {
        return this.mCanSeekBack;
    }

    public boolean canSeekForward() {
        return this.mCanSeekForward;
    }
}
