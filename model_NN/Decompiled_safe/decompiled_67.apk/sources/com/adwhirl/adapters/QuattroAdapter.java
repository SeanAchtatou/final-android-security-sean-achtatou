package com.adwhirl.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.adwhirl.obj.Extra;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import com.qwapi.adclient.android.data.Ad;
import com.qwapi.adclient.android.data.Status;
import com.qwapi.adclient.android.requestparams.AdRequestParams;
import com.qwapi.adclient.android.requestparams.AnimationType;
import com.qwapi.adclient.android.requestparams.DisplayMode;
import com.qwapi.adclient.android.requestparams.Gender;
import com.qwapi.adclient.android.requestparams.MediaType;
import com.qwapi.adclient.android.requestparams.Placement;
import com.qwapi.adclient.android.view.AdEventsListener;
import com.qwapi.adclient.android.view.QWAdView;
import java.util.GregorianCalendar;
import org.json.JSONException;
import org.json.JSONObject;

public class QuattroAdapter extends AdWhirlAdapter implements AdEventsListener {
    private String publisherId = null;
    private QWAdView quattroView;
    private String siteId = null;

    public QuattroAdapter(AdWhirlLayout adWhirlLayout, Ration ration) throws JSONException {
        super(adWhirlLayout, ration);
        JSONObject jsonObject = new JSONObject(this.ration.key);
        this.siteId = jsonObject.getString("siteID");
        this.publisherId = jsonObject.getString("publisherID");
    }

    public void handle() {
        Activity activity;
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null && (activity = adWhirlLayout.activityReference.get()) != null) {
            this.quattroView = new QWAdView(activity, this.siteId, this.publisherId, MediaType.banner, Placement.top, DisplayMode.normal, 0, AnimationType.slide, this, true);
            Extra extra = adWhirlLayout.extra;
            int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
            int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
            this.quattroView.setBackgroundColor(bgColor);
            this.quattroView.setTextColor(fgColor);
        }
    }

    public void onAdClick(Context arg0, Ad arg1) {
    }

    public void onAdRequest(Context context, AdRequestParams params) {
        if (params != null) {
            params.setTestMode(AdWhirlTargeting.getTestMode());
            AdWhirlTargeting.Gender gender = AdWhirlTargeting.getGender();
            if (gender == AdWhirlTargeting.Gender.FEMALE) {
                params.setGender(Gender.female);
            } else if (gender == AdWhirlTargeting.Gender.MALE) {
                params.setGender(Gender.male);
            }
            GregorianCalendar birthDate = AdWhirlTargeting.getBirthDate();
            if (birthDate != null) {
                params.setBirthDate(birthDate.getTime());
            }
            String postalCode = AdWhirlTargeting.getPostalCode();
            if (!TextUtils.isEmpty(postalCode)) {
                params.setZipCode(postalCode);
            }
        }
    }

    public void onAdRequestFailed(Context arg0, AdRequestParams arg1, Status arg2) {
        Log.d(AdWhirlUtil.ADWHIRL, "Quattro failure");
        this.quattroView.setAdEventsListener((AdEventsListener) null, false);
        this.quattroView = null;
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.rollover();
        }
    }

    public void onAdRequestSuccessful(Context arg0, AdRequestParams arg1, Ad arg2) {
        Log.d(AdWhirlUtil.ADWHIRL, "Quattro success");
        this.quattroView.setAdEventsListener((AdEventsListener) null, false);
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.adWhirlManager.resetRollover();
            adWhirlLayout.handler.post(new AdWhirlLayout.ViewAdRunnable(adWhirlLayout, this.quattroView));
            adWhirlLayout.rotateThreadedDelayed();
        }
    }

    public void onDisplayAd(Context arg0, Ad arg1) {
    }
}
