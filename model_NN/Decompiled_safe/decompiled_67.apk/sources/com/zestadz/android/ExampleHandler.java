package com.zestadz.android;

import android.util.Log;
import org.xml.sax.Attributes;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class ExampleHandler extends DefaultHandler implements ErrorHandler {
    private boolean error = false;
    private ParsedExampleDataSet myParsedExampleDataSet = new ParsedExampleDataSet();
    private boolean picture = false;
    private boolean text = false;
    private boolean url = false;

    public ParsedExampleDataSet getParsedData() {
        return this.myParsedExampleDataSet;
    }

    public void startDocument() throws SAXException {
        this.myParsedExampleDataSet = new ParsedExampleDataSet();
    }

    public void endDocument() throws SAXException {
    }

    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        if (localName.equals("ads")) {
            return;
        }
        if (localName.equals("ad")) {
            this.myParsedExampleDataSet.setExtractedAdType(atts.getValue("type"));
        } else if (localName.equals("text")) {
            this.text = true;
        } else if (localName.equals("picture")) {
            this.picture = true;
        } else if (localName.equals("url")) {
            this.url = true;
        } else if (localName.equals("error")) {
            this.error = true;
        }
    }

    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        if (!localName.equals("ads") && !localName.equals("ad")) {
            if (localName.equals("text")) {
                this.text = false;
            } else if (localName.equals("picture")) {
                this.picture = false;
            } else if (localName.equals("url")) {
                this.url = false;
            } else if (localName.equals("error")) {
                this.error = false;
            }
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (this.text) {
            this.myParsedExampleDataSet.setExtractedText(new String(ch, start, length));
        } else if (this.picture) {
            this.myParsedExampleDataSet.setExtractedPicture(new String(ch, start, length));
        } else if (this.url) {
            this.myParsedExampleDataSet.setExtractedURL(new String(ch, start, length));
        } else if (this.error) {
            this.myParsedExampleDataSet.setExtractedError(new String(ch, start, length));
        }
    }

    public void error(SAXParseException e) {
        Log.v("ERROR WHILE PARSING", e.toString());
    }
}
