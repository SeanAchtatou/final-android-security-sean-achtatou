package com.simboly.puzzlebox;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import helper.CrashHandler;
import main.PuzzleTypeConfig;

public final class ActivityMain extends Activity implements CrashHandler.ILogCollectListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new CrashHandler(this, this);
    }

    public void onLogCollectFinished() {
        Intent hIntent = new Intent();
        hIntent.setClassName(getPackageName(), ActivityGame.class.getName());
        hIntent.setAction(PuzzleTypeConfig.START_PUZZLE_ACTION);
        startActivity(hIntent);
        finish();
    }
}
