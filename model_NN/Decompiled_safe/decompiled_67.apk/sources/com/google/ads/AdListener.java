package com.google.ads;

import com.google.ads.AdRequest;

public interface AdListener {
    void onDismissScreen(Ad ad2);

    void onFailedToReceiveAd(Ad ad2, AdRequest.ErrorCode errorCode);

    void onLeaveApplication(Ad ad2);

    void onPresentScreen(Ad ad2);

    void onReceiveAd(Ad ad2);
}
