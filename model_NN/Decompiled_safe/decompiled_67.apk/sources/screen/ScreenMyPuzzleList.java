package screen;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cfg.Option;
import data.MyPuzzle;
import data.MyPuzzleList;
import dialog.DialogManager;
import game.IGame;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class ScreenMyPuzzleList extends ScreenBase {
    private static final int BUTTON_ID_ADD = 1;
    private static final int BUTTON_ID_BACK = 0;
    private static final int BUTTON_ID_EDIT_CANCEL = 2;
    private static final int BUTTON_ID_EDIT_DOWN = 4;
    private static final int BUTTON_ID_EDIT_REMOVE = 5;
    private static final int BUTTON_ID_EDIT_UP = 3;
    /* access modifiers changed from: private */
    public final ViewAdapter m_hAdapter;
    /* access modifiers changed from: private */
    public final DialogManager m_hDialogManager;
    /* access modifiers changed from: private */
    public final MyPuzzleList m_hMyPuzzleList = new MyPuzzleList();
    /* access modifiers changed from: private */
    public MyPuzzle m_hMyPuzzleSelected;
    private final AdapterView.OnItemClickListener m_hOnItemClick = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View v, int iPos, long lId) {
            MyPuzzle hMyPuzzle;
            if (iPos < 0 || !ScreenMyPuzzleList.this.m_bIsCurrentScreen || ScreenMyPuzzleList.this.m_hGame.isScreenLocked() || (hMyPuzzle = ScreenMyPuzzleList.this.m_hAdapter.getItem(iPos)) == null) {
                return;
            }
            if (ScreenMyPuzzleList.this.m_hMyPuzzleSelected == null) {
                ScreenMyPuzzleList.this.m_hGame.switchScreen(11, true, hMyPuzzle);
            } else if (ScreenMyPuzzleList.this.m_hMyPuzzleSelected.equals(hMyPuzzle)) {
                ScreenMyPuzzleList.this.m_hDialogManager.showMyPuzzleAdd(ScreenMyPuzzleList.this.m_iScreenId, ScreenMyPuzzleList.this.m_hMyPuzzleList, ScreenMyPuzzleList.this.m_hMyPuzzleSelected, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        ScreenMyPuzzleList.this.m_hAdapter.notifyDataSetChanged();
                    }
                });
            }
        }
    };
    private final AdapterView.OnItemLongClickListener m_hOnItemLongClick = new AdapterView.OnItemLongClickListener() {
        public boolean onItemLongClick(AdapterView<?> adapterView, View hView, int iPos, long lId) {
            if (iPos < 0) {
                return true;
            }
            MyPuzzle hMyPuzzle = ScreenMyPuzzleList.this.m_hAdapter.getItem(iPos);
            if (hMyPuzzle == null) {
                return true;
            }
            if (ScreenMyPuzzleList.this.m_hMyPuzzleSelected != null && ScreenMyPuzzleList.this.m_hMyPuzzleSelected.equals(hMyPuzzle)) {
                return true;
            }
            ScreenMyPuzzleList.this.m_hMyPuzzleSelected = hMyPuzzle;
            ScreenMyPuzzleList.this.switchEditMode();
            return true;
        }
    };
    private final TitleView m_hTitle;
    private final ListView m_lvList;
    private final ButtonContainer m_vgButtonContainer;
    private final ButtonContainer m_vgButtonContainerEdit;

    public ScreenMyPuzzleList(Activity hActivity, IGame hGame) {
        super(10, hActivity, hGame);
        this.m_hDialogManager = hGame.getDialogManager();
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        this.m_hTitle = new TitleView(hActivity, hMetrics);
        addView(this.m_hTitle);
        FrameLayout vgContent = new FrameLayout(hActivity);
        vgContent.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        addView(vgContent);
        this.m_lvList = new ListView(hActivity);
        this.m_lvList.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.m_lvList.setCacheColorHint(0);
        this.m_lvList.setOnItemClickListener(this.m_hOnItemClick);
        this.m_lvList.setOnItemLongClickListener(this.m_hOnItemLongClick);
        this.m_lvList.setFocusable(false);
        this.m_hAdapter = new ViewAdapter(this, null);
        this.m_lvList.setAdapter((ListAdapter) this.m_hAdapter);
        vgContent.addView(this.m_lvList);
        int iDipButtonHeight = ResDimens.getDip(hMetrics, 48);
        FrameLayout vgButtonContainerRoot = new FrameLayout(hActivity);
        vgButtonContainerRoot.setLayoutParams(new LinearLayout.LayoutParams(-1, iDipButtonHeight));
        addView(vgButtonContainerRoot);
        this.m_vgButtonContainer = new ButtonContainer(hActivity, iDipButtonHeight);
        this.m_vgButtonContainer.createButton(0, "btn_img_back", this.m_hOnClick);
        this.m_vgButtonContainer.createButton(1, "btn_img_my_puzzle_add", this.m_hOnClick);
        vgButtonContainerRoot.addView(this.m_vgButtonContainer);
        this.m_vgButtonContainerEdit = new ButtonContainer(hActivity, iDipButtonHeight);
        this.m_vgButtonContainerEdit.createButton(2, "btn_img_cancel", this.m_hOnClick);
        this.m_vgButtonContainerEdit.createButton(3, "btn_img_arrow_up", this.m_hOnClick);
        this.m_vgButtonContainerEdit.createButton(4, "btn_img_arrow_down", this.m_hOnClick);
        this.m_vgButtonContainerEdit.createButton(5, "btn_img_delete", this.m_hOnClick);
        vgButtonContainerRoot.addView(this.m_vgButtonContainerEdit);
    }

    public boolean beforeShow(int iComeFromScreenId, boolean bReset, Object hData) {
        super.beforeShow(iComeFromScreenId, bReset, hData);
        switchDefaultMode();
        loadMyPuzzleList();
        if (!bReset) {
            return true;
        }
        this.m_lvList.setSelectionAfterHeaderView();
        return true;
    }

    public void onShow() {
        super.onShow();
        this.m_lvList.startAnimation(this.m_hAnimIn);
    }

    public void onActivityPause() {
        super.onActivityPause();
        this.m_hDialogManager.dismiss();
        switchDefaultMode();
    }

    public void onActivityResume() {
        super.onActivityResume();
        loadMyPuzzleList();
    }

    public void onAfterHide() {
        super.onAfterHide();
        int iSize = this.m_hMyPuzzleList.size();
        for (int i = 0; i < iSize; i++) {
            this.m_hMyPuzzleList.get(i).clearIcon();
        }
    }

    /* access modifiers changed from: private */
    public void loadMyPuzzleList() {
        this.m_hGame.lockScreen();
        new Thread(new Runnable() {
            public void run() {
                ScreenMyPuzzleList.this.m_hMyPuzzleList.loadFromSD();
                ScreenMyPuzzleList.this.m_hMyPuzzleList.synchWithSD();
                ScreenMyPuzzleList.this.m_hHandler.post(new Runnable() {
                    public void run() {
                        ScreenMyPuzzleList.this.m_hAdapter.notifyDataSetChanged();
                        ScreenMyPuzzleList.this.m_hGame.unlockScreen();
                    }
                });
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public void onClick(int iViewId) {
        super.onClick(iViewId);
        switch (iViewId) {
            case 0:
                this.m_hGame.switchScreen(9);
                return;
            case 1:
                this.m_hDialogManager.showMyPuzzleAdd(this.m_iScreenId, this.m_hMyPuzzleList, null, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        ScreenMyPuzzleList.this.loadMyPuzzleList();
                    }
                });
                return;
            case 2:
                switchDefaultMode();
                this.m_hGame.unlockScreen();
                return;
            case 3:
                int iPosUp = this.m_hMyPuzzleList.moveUp(this.m_hMyPuzzleSelected);
                if (iPosUp != -1) {
                    this.m_hAdapter.notifyDataSetChanged();
                    this.m_lvList.setSelection(iPosUp - 1);
                }
                this.m_hGame.unlockScreen();
                return;
            case 4:
                int iPosDown = this.m_hMyPuzzleList.moveDown(this.m_hMyPuzzleSelected);
                if (iPosDown != -1) {
                    this.m_hAdapter.notifyDataSetChanged();
                    this.m_lvList.setSelection(iPosDown - 1);
                }
                this.m_hGame.unlockScreen();
                return;
            case 5:
                this.m_hGame.lockScreen();
                this.m_hDialogManager.showMyPuzzleRemove(this.m_hMyPuzzleSelected, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog2, int which) {
                        ScreenMyPuzzleList.this.m_hGame.unlockScreen();
                        if (which == -1) {
                            ScreenMyPuzzleList.this.m_hMyPuzzleList.remove(ScreenMyPuzzleList.this.m_hMyPuzzleSelected);
                            ScreenMyPuzzleList.this.switchDefaultMode();
                        }
                    }
                });
                return;
            default:
                throw new RuntimeException("Invalid view id");
        }
    }

    /* access modifiers changed from: private */
    public void switchDefaultMode() {
        this.m_hMyPuzzleSelected = null;
        this.m_hTitle.setTitle(ResString.screen_my_puzzle_list_title);
        this.m_vgButtonContainer.setVisibility(0);
        this.m_vgButtonContainerEdit.setVisibility(8);
        this.m_hAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void switchEditMode() {
        if (this.m_hMyPuzzleSelected == null) {
            throw new RuntimeException("No data selected");
        }
        this.m_hTitle.setTitle("Edit List");
        this.m_vgButtonContainer.setVisibility(8);
        this.m_vgButtonContainerEdit.setVisibility(0);
        this.m_hAdapter.notifyDataSetChanged();
    }

    public void onActivityResult(int iRequestCode, int iResultCode, Intent hData) {
        Uri hUri;
        Cursor hCursor;
        super.onActivityResult(iRequestCode, iResultCode, hData);
        if (this.m_bIsCurrentScreen && hData != null && (hUri = hData.getData()) != null && (hCursor = this.m_hActivity.getContentResolver().query(hUri, new String[]{"_data"}, null, null, null)) != null && hCursor.moveToFirst()) {
            String sImageFilePath = hCursor.getString(0);
            hCursor.close();
            Bitmap hMyPuzzleIcon = MyPuzzle.createIcon(sImageFilePath);
            if (hMyPuzzleIcon != null) {
                this.m_hDialogManager.setMyPuzzleAddIcon(hMyPuzzleIcon);
            }
        }
    }

    private final class ViewAdapter extends BaseAdapter {
        private ViewAdapter() {
        }

        /* synthetic */ ViewAdapter(ScreenMyPuzzleList screenMyPuzzleList, ViewAdapter viewAdapter) {
            this();
        }

        public void notifyDataSetChanged() {
            ScreenMyPuzzleList.this.m_hMyPuzzleList.update();
            super.notifyDataSetChanged();
        }

        public int getCount() {
            return ScreenMyPuzzleList.this.m_hMyPuzzleList.size();
        }

        public MyPuzzle getItem(int iPos) {
            return ScreenMyPuzzleList.this.m_hMyPuzzleList.get(iPos);
        }

        public long getItemId(int iId) {
            return (long) iId;
        }

        public View getView(int iPos, View vConvert, ViewGroup vgParent) {
            ListItem hListItem;
            if (vConvert == null) {
                hListItem = new ListItem(ScreenMyPuzzleList.this.m_hActivity);
            } else {
                hListItem = (ListItem) vConvert;
            }
            MyPuzzle hMyPuzzle = getItem(iPos);
            hListItem.ivPuzzleIcon.setImageBitmap(hMyPuzzle.getIcon());
            hListItem.tvPuzzleName.setText(hMyPuzzle.getName());
            if (ScreenMyPuzzleList.this.m_hMyPuzzleSelected == null || !hMyPuzzle.equals(ScreenMyPuzzleList.this.m_hMyPuzzleSelected)) {
                hListItem.tvPuzzleName.setTextColor(-1);
            } else {
                hListItem.tvPuzzleName.setTextColor(-65536);
            }
            hListItem.tvPuzzleImageCount.setText(String.valueOf(hMyPuzzle.size()));
            return hListItem;
        }

        private final class ListItem extends LinearLayout {
            public final ImageView ivPuzzleIcon;
            public final TextView tvPuzzleImageCount;
            public final TextView tvPuzzleName;

            public ListItem(Context hContext) {
                super(hContext);
                DisplayMetrics hMetrics = getResources().getDisplayMetrics();
                setLayoutParams(new AbsListView.LayoutParams(-1, -1));
                setOrientation(0);
                setGravity(19);
                int iDip10 = ResDimens.getDip(hMetrics, 10);
                setPadding(iDip10, iDip10, iDip10, iDip10);
                int iDipIconSize = ResDimens.getDip(hMetrics, 48);
                this.ivPuzzleIcon = new ImageView(hContext);
                this.ivPuzzleIcon.setLayoutParams(new LinearLayout.LayoutParams(iDipIconSize, iDipIconSize));
                addView(this.ivPuzzleIcon);
                LinearLayout vgTextContainer = new LinearLayout(hContext);
                vgTextContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                vgTextContainer.setOrientation(1);
                vgTextContainer.setGravity(19);
                vgTextContainer.setPadding(iDip10, 0, 0, 0);
                addView(vgTextContainer);
                this.tvPuzzleName = getTextView(hContext);
                this.tvPuzzleName.setTypeface(Typeface.DEFAULT, 1);
                vgTextContainer.addView(this.tvPuzzleName);
                this.tvPuzzleImageCount = getTextView(hContext);
                vgTextContainer.addView(this.tvPuzzleImageCount);
            }

            private TextView getTextView(Context hContext) {
                TextView tvText = new TextView(hContext);
                tvText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                tvText.setTextColor(-1);
                tvText.setTextSize(1, 16.0f);
                tvText.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
                return tvText;
            }
        }
    }
}
