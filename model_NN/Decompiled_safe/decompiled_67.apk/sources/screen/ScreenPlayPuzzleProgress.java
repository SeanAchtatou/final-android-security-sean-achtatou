package screen;

import android.app.Activity;
import android.view.KeyEvent;
import android.widget.Toast;
import com.adwhirl.util.AdWhirlUtil;
import data.Puzzle;
import data.PuzzleImage;
import game.IGame;
import helper.Constants;
import helper.ImageSaver;
import res.ResString;
import screen.ScreenPlayBase;

public final class ScreenPlayPuzzleProgress extends ScreenPlayBase {
    private static final int GRID_SIZE_MAX = 8;
    private static final int GRID_SIZE_MIN = 3;
    private static final int IMAGE_COUNT_PER_DIMENSION = 20;
    /* access modifiers changed from: private */
    public final Puzzle m_hPuzzle;
    private int m_iGridSizeMax;
    private int m_iGridSizeMin;
    private final String m_sLevel = ResString.screen_puzzle_label_level;

    public ScreenPlayPuzzleProgress(Activity hActivity, IGame hGame) {
        super(4, hActivity, hGame);
        this.m_hPuzzle = hGame.getPuzzle();
    }

    public boolean beforeShow(int iComeFromScreenId, boolean bReset, Object hData) {
        super.beforeShow(iComeFromScreenId, bReset, hData);
        int iGridSizeMin = this.m_hPuzzle.getGridSizeMin();
        if (iGridSizeMin < 3) {
            this.m_iGridSizeMin = 3;
        } else if (iGridSizeMin > 8) {
            this.m_iGridSizeMin = 8;
        } else {
            this.m_iGridSizeMin = iGridSizeMin;
        }
        int iGridSizeMax = this.m_hPuzzle.getGridSizeMax();
        if (iGridSizeMax < this.m_iGridSizeMin) {
            this.m_iGridSizeMax = this.m_iGridSizeMin;
            return true;
        } else if (iGridSizeMax > 8) {
            this.m_iGridSizeMax = 8;
            return true;
        } else {
            this.m_iGridSizeMax = iGridSizeMax;
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public int calculateNewGridSize() {
        int iGridSize = (this.m_hPuzzle.getPuzzleImageSelected().getIndex() / 20) + this.m_iGridSizeMin;
        return iGridSize <= this.m_iGridSizeMax ? iGridSize : this.m_iGridSizeMax;
    }

    /* access modifiers changed from: protected */
    public String getDisplayLevel() {
        int iImageIndex = this.m_hPuzzle.getPuzzleImageSelected().getIndex();
        return String.format(this.m_sLevel, Integer.valueOf(iImageIndex + 1), Integer.valueOf(this.m_hPuzzle.getImageCount()));
    }

    /* access modifiers changed from: protected */
    public void loadImage() {
        super.loadImage();
        this.m_hGame.lockScreen();
        setImage(this.m_hPuzzle.getPuzzleImageSelected().getImage(this.m_hActivity));
    }

    /* access modifiers changed from: protected */
    public ScreenPlayBase.ResultData getResultData() {
        PuzzleImage hPuzzleImage = this.m_hPuzzle.getPuzzleImageSelected();
        ScreenPlayBase.ResultData hResultData = new ScreenPlayBase.ResultData(0);
        hResultData.sPuzzleId = this.m_hPuzzle.getId();
        hResultData.iImageIndex = hPuzzleImage.getIndex();
        hResultData.sPuzzleImageInfo = hPuzzleImage.getInfo(this.m_hActivity);
        hResultData.sMovesNameRecord = hPuzzleImage.getBestMovesName();
        hResultData.iMovesRecord = hPuzzleImage.getBestMoves();
        hResultData.sTimeInMsNameRecord = hPuzzleImage.getBestTimeInMsName();
        hResultData.lTimeInMsRecord = hPuzzleImage.getBestTimeInMs();
        return hResultData;
    }

    /* access modifiers changed from: protected */
    public void dialogResultDismiss(ScreenPlayBase.ResultData hResultData) {
        this.m_hPuzzle.setPuzzleSolved(this.m_hActivity, hResultData.iImageIndex, getPlayerName(), hResultData.iMovesNew, hResultData.lTimeInMsNew);
        this.m_hPuzzle.loadPuzzleImageData(this.m_hActivity);
        if (this.m_hPuzzle.isAllSolved()) {
            this.m_hGame.switchScreen(8);
        } else {
            this.m_hPuzzle.setPuzzleImageNext();
            startLevel();
            this.m_hGame.unlockScreen();
        }
        this.m_hHandler.post(new Runnable() {
            public void run() {
                ScreenPlayPuzzleProgress.this.m_hDialogManager.dismiss();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onBackPress() {
        if (this.m_hPuzzle == null) {
            throw new RuntimeException();
        }
        this.m_hDialogManager.showLeavePuzzle(3);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (!Constants.debug()) {
            return super.dispatchKeyEvent(event);
        }
        switch (event.getKeyCode()) {
            case AdWhirlUtil.NETWORK_TYPE_ONERIOT:
                if (1 == event.getAction()) {
                    solved(10000, 1000000);
                }
                return true;
            case 84:
                if (1 == event.getAction()) {
                    if (this.m_hPuzzle == null) {
                        return true;
                    }
                    new Object() {
                        /* access modifiers changed from: private */
                        public final Runnable m_hRunnable = new Runnable() {
                            public void run() {
                                int iIndex = ScreenPlayPuzzleProgress.this.m_hPuzzle.setPuzzleImageNext();
                                if (ImageSaver.save(ScreenPlayPuzzleProgress.this.m_hActivity, ScreenPlayPuzzleProgress.this.m_hPuzzle.getId(), iIndex) != null) {
                                    AnonymousClass1TestPuzzleImage r2 = AnonymousClass1TestPuzzleImage.this;
                                    r2.m_iSaveImageSuccedCount = r2.m_iSaveImageSuccedCount + 1;
                                    if (ScreenPlayPuzzleProgress.this.m_hPuzzle.getPuzzleImageSelected().getImage(ScreenPlayPuzzleProgress.this.m_hActivity) == null) {
                                        throw new RuntimeException("Couldn't load image with index: " + iIndex);
                                    }
                                    ScreenPlayPuzzleProgress.this.startLevel();
                                    if (iIndex != 0) {
                                        ScreenPlayPuzzleProgress.this.m_hHandler.post(AnonymousClass1TestPuzzleImage.this.m_hRunnable);
                                        return;
                                    }
                                    Toast.makeText(ScreenPlayPuzzleProgress.this.m_hActivity, "Save image succed: " + AnonymousClass1TestPuzzleImage.this.m_iSaveImageSuccedCount, 1).show();
                                    ScreenPlayPuzzleProgress.this.m_hGame.unlockScreen();
                                    return;
                                }
                                throw new RuntimeException("Unable to save image");
                            }
                        };
                        /* access modifiers changed from: private */
                        public int m_iSaveImageSuccedCount = 0;

                        public void startTest() {
                            ScreenPlayPuzzleProgress.this.m_hGame.lockScreen();
                            ScreenPlayPuzzleProgress.this.m_hHandler.post(this.m_hRunnable);
                        }
                    }.startTest();
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }
}
