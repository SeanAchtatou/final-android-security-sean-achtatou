package screen;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cfg.Option;
import data.PuzzleImage;
import dialog.DialogManager;
import game.IGame;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class ScreenPuzzleImage extends ScreenPuzzleBase {
    private static final int BUTTON_ID_BACK = 0;
    private static final int BUTTON_ID_INFO = 2;
    private static final int BUTTON_ID_NEXT = 3;
    private static final int BUTTON_ID_SAVE = 1;
    private final View m_btnInfo;
    private final View m_btnSave;
    private final DialogManager m_hDialogManager;
    private final TitleView m_hTitle;
    private final ImageView m_ivImage;
    private final TextView m_tvImageState;
    private final RelativeLayout m_vgImageContainer;

    public ScreenPuzzleImage(Activity hActivity, IGame hGame) {
        super(3, hActivity, hGame);
        this.m_hDialogManager = hGame.getDialogManager();
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        this.m_hTitle = new TitleView(hActivity, hMetrics);
        addView(this.m_hTitle);
        LinearLayout linearLayout = new LinearLayout(hActivity);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout.setOrientation(1);
        linearLayout.setGravity(17);
        addView(linearLayout);
        this.m_vgImageContainer = new RelativeLayout(hActivity);
        int iDipAvailableHeight = hMetrics.heightPixels - ((ResDimens.getDip(hMetrics, 64) + ResDimens.getDip(hMetrics, 48)) + ResDimens.getDip(hMetrics, 52));
        int iDipImageContainerSize = iDipAvailableHeight < hMetrics.widthPixels ? iDipAvailableHeight : hMetrics.widthPixels;
        this.m_vgImageContainer.setLayoutParams(new LinearLayout.LayoutParams(iDipImageContainerSize, iDipImageContainerSize));
        this.m_vgImageContainer.setBackgroundColor(-1);
        this.m_vgImageContainer.setId(3);
        this.m_vgImageContainer.setOnClickListener(this.m_hOnClick);
        linearLayout.addView(this.m_vgImageContainer);
        int iDip2 = ResDimens.getDip(hMetrics, 2);
        this.m_tvImageState = new TextView(hActivity);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        layoutParams.setMargins(iDip2, iDip2, iDip2, iDip2);
        this.m_tvImageState.setLayoutParams(layoutParams);
        this.m_tvImageState.setGravity(17);
        this.m_tvImageState.setTextColor(-1);
        this.m_tvImageState.setBackgroundColor(Option.HINT_COLOR_DEFAULT);
        this.m_tvImageState.setTextSize(1, 25.0f);
        this.m_tvImageState.setTypeface(Typeface.DEFAULT, 1);
        this.m_tvImageState.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        this.m_vgImageContainer.addView(this.m_tvImageState);
        int iDip5 = ResDimens.getDip(hMetrics, 5);
        this.m_tvImageState.setPadding(iDip5, iDip5, iDip5, iDip5);
        this.m_ivImage = new ImageView(hActivity);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams2.addRule(13);
        layoutParams2.setMargins(iDip2, iDip2, iDip2, iDip2);
        this.m_ivImage.setLayoutParams(layoutParams2);
        this.m_ivImage.setBackgroundColor(Option.HINT_COLOR_DEFAULT);
        this.m_vgImageContainer.addView(this.m_ivImage);
        ButtonContainer buttonContainer = new ButtonContainer(hActivity);
        buttonContainer.createButton(0, "btn_img_back", this.m_hOnClick);
        this.m_btnSave = buttonContainer.createButton(1, "btn_img_save", this.m_hOnClick);
        this.m_btnInfo = buttonContainer.createButton(2, "btn_img_info", this.m_hOnClick);
        buttonContainer.createButton(3, "btn_img_next", this.m_hOnClick);
        addView(buttonContainer);
    }

    public boolean beforeShow(int iSwitchBackScreenId, boolean bReset, Object hData) {
        boolean z;
        super.beforeShow(iSwitchBackScreenId, bReset, hData);
        PuzzleImage hPuzzleImage = this.m_hPuzzle.getPuzzleImageSelected();
        this.m_hTitle.setTitle("Level " + (hPuzzleImage.getIndex() + 1));
        if (hPuzzleImage.isSolved()) {
            Bitmap hImage = hPuzzleImage.getImage(this.m_hActivity);
            this.m_tvImageState.setText((CharSequence) null);
            this.m_ivImage.setVisibility(0);
            this.m_ivImage.setImageBitmap(hImage);
            View view2 = this.m_btnInfo;
            if (TextUtils.isEmpty(hPuzzleImage.getInfo(this.m_hActivity))) {
                z = false;
            } else {
                z = true;
            }
            view2.setEnabled(z);
            this.m_btnSave.setEnabled(true);
        } else {
            this.m_tvImageState.setText(ResString.image_unsolved);
            this.m_ivImage.setVisibility(4);
            this.m_ivImage.setImageBitmap(null);
            this.m_btnInfo.setEnabled(false);
            this.m_btnSave.setEnabled(false);
        }
        return true;
    }

    public void onShow() {
        super.onShow();
        this.m_hDialogManager.screenSwitchDismiss();
        this.m_vgImageContainer.startAnimation(this.m_hAnimIn);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: game.IGame.switchScreen(int, boolean):void
     arg types: [int, int]
     candidates:
      game.IGame.switchScreen(int, java.lang.Object):void
      game.IGame.switchScreen(int, boolean):void */
    /* access modifiers changed from: protected */
    public void onClick(int iViewId) {
        super.onClick(iViewId);
        switch (iViewId) {
            case 0:
                this.m_hGame.switchScreen(2);
                return;
            case 1:
                PuzzleImage hPuzzleImage = this.m_hPuzzle.getPuzzleImageSelected();
                if (hPuzzleImage.isSolved()) {
                    this.m_hDialogManager.showSaveImage(this.m_hPuzzle.getId(), hPuzzleImage.getIndex(), null, null);
                    return;
                } else {
                    this.m_hGame.unlockScreen();
                    return;
                }
            case 2:
                this.m_hDialogManager.showPuzzleImageInfo(this.m_hPuzzle.getPuzzleImageSelected().getInfo(this.m_hActivity), null, null);
                return;
            case 3:
                this.m_hPuzzle.clearPuzzleImageTempData();
                this.m_hGame.switchScreen(4, true);
                return;
            default:
                throw new RuntimeException("Invalid view id");
        }
    }
}
