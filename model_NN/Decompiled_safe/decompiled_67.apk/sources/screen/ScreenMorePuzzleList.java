package screen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Process;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cfg.Option;
import com.simboly.puzzlebox.ActivityGame;
import data.MorePuzzle;
import data.MorePuzzleList;
import game.IGame;
import helper.Constants;
import helper.Global;
import helper.L;
import helper.PuzzleConfig;
import java.util.List;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class ScreenMorePuzzleList extends ScreenBase {
    private static final int BUTTON_ID_EDIT_CANCEL = 3;
    private static final int BUTTON_ID_EDIT_DOWN = 5;
    private static final int BUTTON_ID_EDIT_UP = 4;
    private static final int BUTTON_ID_MY_PUZZLE_LIST = 0;
    private static final int BUTTON_ID_NEXT = 2;
    private static final int BUTTON_ID_REFRESH = 1;
    /* access modifiers changed from: private */
    public boolean m_bInitMorePuzzleInstalledListDone = false;
    /* access modifiers changed from: private */
    public boolean m_bInitMorePuzzleInstalledListInProgress = false;
    /* access modifiers changed from: private */
    public boolean m_bInitMorePuzzleMarketListInProgress = false;
    /* access modifiers changed from: private */
    public final PuzzleListAdapter m_hAdapter;
    /* access modifiers changed from: private */
    public final MorePuzzleList m_hMorePuzzleList;
    /* access modifiers changed from: private */
    public MorePuzzle m_hMorePuzzleSelected;
    private final AdapterView.OnItemClickListener m_hOnItemClick = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View v, int iPos, long lId) {
            MorePuzzle hMorePuzzle;
            if (iPos >= 0 && ScreenMorePuzzleList.this.m_bIsCurrentScreen && !ScreenMorePuzzleList.this.m_hGame.isScreenLocked() && (hMorePuzzle = ScreenMorePuzzleList.this.m_hAdapter.getItem(iPos)) != null && ScreenMorePuzzleList.this.m_hMorePuzzleSelected == null) {
                switch (hMorePuzzle.getType()) {
                    case 0:
                        ScreenMorePuzzleList.this.play(hMorePuzzle);
                        return;
                    case 1:
                        String sMarketUri = hMorePuzzle.getMarketUri();
                        if (Constants.debug()) {
                            L.v("Market Uri: " + sMarketUri);
                        }
                        if (!TextUtils.isEmpty(sMarketUri)) {
                            Intent hMarketIntent = new Intent("android.intent.action.VIEW", Uri.parse(sMarketUri));
                            if (ScreenBase.isIntentAvailable(ScreenMorePuzzleList.this.m_hActivity, hMarketIntent)) {
                                ScreenMorePuzzleList.this.m_hActivity.startActivity(hMarketIntent);
                                return;
                            } else {
                                Toast.makeText(ScreenMorePuzzleList.this.m_hActivity, String.format(ResString.toast_market_not_installed, PuzzleConfig.getMarketDisplayName(PuzzleConfig.getMarketId(ScreenMorePuzzleList.this.m_hActivity).intValue())), 0).show();
                                return;
                            }
                        } else {
                            return;
                        }
                    default:
                        throw new RuntimeException("Invalid addon type");
                }
            }
        }
    };
    private final AdapterView.OnItemLongClickListener m_hOnItemLongClick = new AdapterView.OnItemLongClickListener() {
        public boolean onItemLongClick(AdapterView<?> adapterView, View hView, int iPos, long lId) {
            if (iPos < 0) {
                return true;
            }
            MorePuzzle hMorePuzzle = ScreenMorePuzzleList.this.m_hAdapter.getItem(iPos);
            if (hMorePuzzle == null) {
                return true;
            }
            if (hMorePuzzle.getType() != 0) {
                return true;
            }
            if (ScreenMorePuzzleList.this.m_hMorePuzzleSelected != null && ScreenMorePuzzleList.this.m_hMorePuzzleSelected.equals(hMorePuzzle)) {
                return true;
            }
            ScreenMorePuzzleList.this.m_hMorePuzzleSelected = hMorePuzzle;
            ScreenMorePuzzleList.this.switchEditMode();
            return true;
        }
    };
    /* access modifiers changed from: private */
    public final ListView m_lvList;
    private final View m_pbProgress;
    private final ButtonContainer m_vgButtonContainer;
    private final ButtonContainer m_vgButtonContainerEdit;

    public ScreenMorePuzzleList(Activity hActivity, IGame hGame) {
        super(9, hActivity, hGame);
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        this.m_hMorePuzzleList = new MorePuzzleList(hActivity);
        TitleView hTitle = new TitleView(hActivity, hMetrics);
        hTitle.setTitle(ResString.screen_more_puzzle_list_title);
        addView(hTitle);
        RelativeLayout vgContent = new RelativeLayout(hActivity);
        vgContent.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        addView(vgContent);
        this.m_pbProgress = new ProgressBar(hActivity, null, 16842874);
        RelativeLayout.LayoutParams hProgressBarParams = new RelativeLayout.LayoutParams(-2, -2);
        hProgressBarParams.addRule(13);
        this.m_pbProgress.setLayoutParams(hProgressBarParams);
        this.m_pbProgress.setVisibility(8);
        vgContent.addView(this.m_pbProgress);
        this.m_lvList = new ListView(hActivity);
        RelativeLayout.LayoutParams hListParams = new RelativeLayout.LayoutParams(-1, -1);
        hListParams.addRule(13);
        this.m_lvList.setLayoutParams(hListParams);
        this.m_lvList.setVisibility(8);
        this.m_lvList.setCacheColorHint(0);
        this.m_lvList.setOnItemClickListener(this.m_hOnItemClick);
        this.m_lvList.setOnItemLongClickListener(this.m_hOnItemLongClick);
        this.m_lvList.setFocusable(false);
        this.m_hAdapter = new PuzzleListAdapter(this, null);
        this.m_lvList.setAdapter((ListAdapter) this.m_hAdapter);
        vgContent.addView(this.m_lvList);
        int iDipButtonHeight = ResDimens.getDip(hMetrics, 48);
        FrameLayout vgButtonContainerRoot = new FrameLayout(hActivity);
        vgButtonContainerRoot.setLayoutParams(new LinearLayout.LayoutParams(-1, iDipButtonHeight));
        addView(vgButtonContainerRoot);
        this.m_vgButtonContainer = new ButtonContainer(hActivity, iDipButtonHeight);
        this.m_vgButtonContainer.createButton(0, "btn_img_my_puzzle_list", this.m_hOnClick);
        this.m_vgButtonContainer.createButton(1, "btn_img_refresh", this.m_hOnClick);
        this.m_vgButtonContainer.createButton(2, "btn_img_next", this.m_hOnClick);
        vgButtonContainerRoot.addView(this.m_vgButtonContainer);
        this.m_vgButtonContainerEdit = new ButtonContainer(hActivity, iDipButtonHeight);
        this.m_vgButtonContainerEdit.createButton(3, "btn_img_cancel", this.m_hOnClick);
        this.m_vgButtonContainerEdit.createButton(4, "btn_img_arrow_up", this.m_hOnClick);
        this.m_vgButtonContainerEdit.createButton(5, "btn_img_arrow_down", this.m_hOnClick);
        vgButtonContainerRoot.addView(this.m_vgButtonContainerEdit);
        switchDefaultMode();
    }

    public boolean beforeShow(int iComeFromScreenId, boolean bReset, Object hData) {
        super.beforeShow(iComeFromScreenId, bReset, hData);
        switchDefaultMode();
        if (bReset || !this.m_bInitMorePuzzleInstalledListDone) {
            initMorePuzzleInstalledList();
            return true;
        }
        setRefreshProgressState(false);
        this.m_hAdapter.notifyDataSetChanged();
        this.m_lvList.startAnimation(this.m_hAnimIn);
        return true;
    }

    /* access modifiers changed from: private */
    public void setRefreshProgressState(boolean bRefreshState) {
        if (bRefreshState) {
            this.m_lvList.setVisibility(8);
            this.m_pbProgress.setVisibility(0);
            this.m_hGame.lockScreen();
            return;
        }
        this.m_lvList.setVisibility(0);
        this.m_pbProgress.setVisibility(8);
        this.m_hGame.unlockScreen();
    }

    private void initMorePuzzleInstalledList() {
        this.m_bInitMorePuzzleInstalledListInProgress = true;
        setRefreshProgressState(true);
        this.m_lvList.setSelectionAfterHeaderView();
        new Thread(new Runnable() {
            public void run() {
                Process.setThreadPriority(10);
                ScreenMorePuzzleList.this.m_hMorePuzzleList.initMorePuzzleInstalled(ScreenMorePuzzleList.this.m_hActivity);
                ScreenMorePuzzleList.this.m_hMorePuzzleList.initMorePuzzleMarketFromSD(ScreenMorePuzzleList.this.m_hActivity);
                ScreenMorePuzzleList.this.m_hHandler.post(new Runnable() {
                    public void run() {
                        ScreenMorePuzzleList.this.m_hAdapter.notifyDataSetChanged();
                        ScreenMorePuzzleList.this.m_bInitMorePuzzleInstalledListInProgress = false;
                        ScreenMorePuzzleList.this.m_bInitMorePuzzleInstalledListDone = true;
                        ScreenMorePuzzleList.this.setRefreshProgressState(false);
                        ScreenMorePuzzleList.this.initMorePuzzleMarketList();
                    }
                });
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void initMorePuzzleMarketList() {
        if (this.m_hMorePuzzleList.isTimeToSynchWithWeb(this.m_hActivity) && !this.m_bInitMorePuzzleMarketListInProgress) {
            this.m_bInitMorePuzzleMarketListInProgress = true;
            new Thread(new Runnable() {
                public void run() {
                    Process.setThreadPriority(10);
                    List<MorePuzzle> arrMorePuzzleMarketWeb = ScreenMorePuzzleList.this.m_hMorePuzzleList.loadMorePuzzleMarketFromWeb(ScreenMorePuzzleList.this.m_hActivity);
                    if (arrMorePuzzleMarketWeb != null) {
                        Runnable hRunnableUpdate = new Runnable() {
                            public void run() {
                                if (ScreenMorePuzzleList.this.m_bIsCurrentScreen && ScreenMorePuzzleList.this.m_lvList.getVisibility() == 0) {
                                    ScreenMorePuzzleList.this.m_hAdapter.notifyDataSetChanged();
                                }
                            }
                        };
                        ScreenMorePuzzleList.this.m_hMorePuzzleList.syncMorePuzzleMarketWithWebData(ScreenMorePuzzleList.this.m_hActivity, arrMorePuzzleMarketWeb);
                        boolean bSavedSucceed = ScreenMorePuzzleList.this.m_hMorePuzzleList.saveMorePuzzleMarketToSD(ScreenMorePuzzleList.this.m_hActivity, arrMorePuzzleMarketWeb);
                        ScreenMorePuzzleList.this.m_hHandler.post(hRunnableUpdate);
                        for (MorePuzzle hMorePuzzleMarketWebLoop : arrMorePuzzleMarketWeb) {
                            if (ScreenMorePuzzleList.this.m_hMorePuzzleList.downloadIcon(ScreenMorePuzzleList.this.m_hActivity, hMorePuzzleMarketWebLoop) && ScreenMorePuzzleList.this.m_hMorePuzzleList.addMorePuzzleMarketFromWeb(ScreenMorePuzzleList.this.m_hActivity, hMorePuzzleMarketWebLoop)) {
                                ScreenMorePuzzleList.this.m_hHandler.post(hRunnableUpdate);
                            }
                        }
                        if (bSavedSucceed && ScreenMorePuzzleList.this.m_hMorePuzzleList.isEveryIconDownloaded(ScreenMorePuzzleList.this.m_hActivity, arrMorePuzzleMarketWeb)) {
                            ScreenMorePuzzleList.this.m_hHandler.post(new Runnable() {
                                public void run() {
                                    ScreenMorePuzzleList.this.m_hMorePuzzleList.synchWithWebSucceeded(ScreenMorePuzzleList.this.m_hActivity);
                                }
                            });
                        }
                    }
                    ScreenMorePuzzleList.this.m_bInitMorePuzzleMarketListInProgress = false;
                    if (Constants.debug()) {
                        L.v("Init MorePuzzleMarket done");
                    }
                }
            }).start();
        }
    }

    public void onActivityPause() {
        super.onActivityPause();
        switchDefaultMode();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: game.IGame.switchScreen(int, boolean):void
     arg types: [int, int]
     candidates:
      game.IGame.switchScreen(int, java.lang.Object):void
      game.IGame.switchScreen(int, boolean):void */
    /* access modifiers changed from: protected */
    public void onClick(int iViewId) {
        super.onClick(iViewId);
        switch (iViewId) {
            case 0:
                if (!Global.isSDCardAvailable()) {
                    Toast.makeText(this.m_hActivity, ResString.sd_card_unmounted, 0).show();
                    this.m_hGame.unlockScreen();
                    return;
                }
                this.m_hGame.switchScreen(10, true);
                return;
            case 1:
                if (this.m_bInitMorePuzzleInstalledListInProgress) {
                    if (Constants.debug()) {
                        L.v("RefreshInPorgress");
                    }
                    this.m_hGame.unlockScreen();
                    return;
                } else if (this.m_bInitMorePuzzleMarketListInProgress) {
                    if (Constants.debug()) {
                        L.v("MorePuzzeMarketInProgress");
                    }
                    this.m_hGame.unlockScreen();
                    return;
                } else {
                    this.m_hMorePuzzleList.resetSyncWithWebDate(this.m_hActivity);
                    initMorePuzzleInstalledList();
                    return;
                }
            case 2:
                this.m_hGame.switchScreen(2, true);
                return;
            case 3:
                switchDefaultMode();
                this.m_hGame.unlockScreen();
                return;
            case 4:
                int iPosUp = this.m_hMorePuzzleList.moveInstalledUp(this.m_hActivity, this.m_hMorePuzzleSelected);
                if (iPosUp != -1) {
                    this.m_hAdapter.notifyDataSetChanged();
                    this.m_lvList.setSelection(iPosUp - 1);
                }
                this.m_hGame.unlockScreen();
                return;
            case 5:
                int iPosDown = this.m_hMorePuzzleList.moveInstalledDown(this.m_hActivity, this.m_hMorePuzzleSelected);
                if (iPosDown != -1) {
                    this.m_hAdapter.notifyDataSetChanged();
                    this.m_lvList.setSelection(iPosDown - 1);
                }
                this.m_hGame.unlockScreen();
                return;
            default:
                throw new RuntimeException("Invalid view id");
        }
    }

    private void switchDefaultMode() {
        this.m_hMorePuzzleSelected = null;
        this.m_vgButtonContainer.setVisibility(0);
        this.m_vgButtonContainerEdit.setVisibility(8);
        this.m_hAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void switchEditMode() {
        if (this.m_hMorePuzzleSelected == null) {
            throw new RuntimeException("No data selected");
        }
        this.m_vgButtonContainer.setVisibility(8);
        this.m_vgButtonContainerEdit.setVisibility(0);
        this.m_hAdapter.notifyDataSetChanged();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: game.IGame.switchScreen(int, boolean):void
     arg types: [int, int]
     candidates:
      game.IGame.switchScreen(int, java.lang.Object):void
      game.IGame.switchScreen(int, boolean):void */
    /* access modifiers changed from: private */
    public void play(MorePuzzle hMorePuzzle) {
        String sMorePuzzleId = hMorePuzzle.getId();
        if (this.m_hGame.getPuzzle().getId().equals(sMorePuzzleId)) {
            this.m_hGame.switchScreen(2, true);
            return;
        }
        Intent hIntent = new Intent();
        hIntent.setClassName(sMorePuzzleId, ActivityGame.class.getName());
        this.m_hActivity.startActivity(hIntent);
    }

    private final class PuzzleListAdapter extends BaseAdapter {
        private PuzzleListAdapter() {
        }

        /* synthetic */ PuzzleListAdapter(ScreenMorePuzzleList screenMorePuzzleList, PuzzleListAdapter puzzleListAdapter) {
            this();
        }

        public void notifyDataSetChanged() {
            ScreenMorePuzzleList.this.m_hMorePuzzleList.update();
            super.notifyDataSetChanged();
        }

        public int getViewTypeCount() {
            return 2;
        }

        public int getItemViewType(int position) {
            return ScreenMorePuzzleList.this.m_hMorePuzzleList.getItemViewType(position);
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public boolean isEnabled(int position) {
            return ScreenMorePuzzleList.this.m_hMorePuzzleList.isEnabled(position);
        }

        public int getCount() {
            return ScreenMorePuzzleList.this.m_hMorePuzzleList.size();
        }

        public MorePuzzle getItem(int iPos) {
            return ScreenMorePuzzleList.this.m_hMorePuzzleList.get(iPos);
        }

        public long getItemId(int iId) {
            return (long) iId;
        }

        public View getView(int iPos, View vConvert, ViewGroup vgParent) {
            switch (getItemViewType(iPos)) {
                case 0:
                    return getViewSeperator(iPos, vConvert);
                case 1:
                    return getViewPuzzle(iPos, vConvert);
                default:
                    throw new RuntimeException("Invalid type id");
            }
        }

        private View getViewSeperator(int iPos, View vConvert) {
            ListItemSeparator hListItem;
            if (vConvert == null) {
                hListItem = new ListItemSeparator(ScreenMorePuzzleList.this.m_hActivity);
            } else {
                hListItem = (ListItemSeparator) vConvert;
            }
            hListItem.tvTitle.setText(ScreenMorePuzzleList.this.m_hMorePuzzleList.getSeparatorTitle(iPos));
            return hListItem;
        }

        private final class ListItemSeparator extends RelativeLayout {
            public final TextView tvTitle;

            public ListItemSeparator(Context hContext) {
                super(hContext);
                DisplayMetrics hMetrics = getResources().getDisplayMetrics();
                setLayoutParams(new AbsListView.LayoutParams(-1, -1));
                setBackgroundColor(-2013265920);
                int iDip10 = ResDimens.getDip(hMetrics, 10);
                int iDip5 = ResDimens.getDip(hMetrics, 5);
                setPadding(iDip10, iDip5, iDip10, iDip5);
                this.tvTitle = new TextView(hContext);
                RelativeLayout.LayoutParams hParams = new RelativeLayout.LayoutParams(-1, -2);
                hParams.addRule(13);
                this.tvTitle.setLayoutParams(hParams);
                this.tvTitle.setTextColor(-1);
                this.tvTitle.setTextSize(1, 16.0f);
                this.tvTitle.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
                this.tvTitle.setTypeface(Typeface.DEFAULT, 1);
                addView(this.tvTitle);
            }
        }

        private View getViewPuzzle(int iPos, View vConvert) {
            ListItem hListItem;
            if (vConvert == null) {
                hListItem = new ListItem(ScreenMorePuzzleList.this.m_hActivity);
            } else {
                hListItem = (ListItem) vConvert;
            }
            MorePuzzle hMorePuzzle = getItem(iPos);
            hListItem.ivPuzzleIcon.setImageBitmap(hMorePuzzle.getIcon(ScreenMorePuzzleList.this.m_hActivity));
            hListItem.tvPuzzleName.setText(hMorePuzzle.getPuzzleName());
            if (ScreenMorePuzzleList.this.m_hMorePuzzleSelected == null || !hMorePuzzle.equals(ScreenMorePuzzleList.this.m_hMorePuzzleSelected)) {
                hListItem.tvPuzzleName.setTextColor(-1);
            } else {
                hListItem.tvPuzzleName.setTextColor(-65536);
            }
            String sDeveloperName = hMorePuzzle.getDeveloperName();
            if (sDeveloperName == null || TextUtils.isEmpty(sDeveloperName)) {
                hListItem.tvPuzzleDeveloperName.setText((CharSequence) null);
                hListItem.tvPuzzleDeveloperName.setVisibility(8);
            } else {
                hListItem.tvPuzzleDeveloperName.setText(sDeveloperName);
                hListItem.tvPuzzleDeveloperName.setVisibility(0);
            }
            switch (hMorePuzzle.getType()) {
                case 0:
                    if (!Global.isSDCardAvailable()) {
                        hListItem.tvPuzzleProgressData.setVisibility(8);
                        break;
                    } else {
                        String sPuzzleId = hMorePuzzle.getId();
                        int iImageIndexNextUnsolved = MorePuzzleList.MorePuzzleProgress.getImageIndexNextUnsolved(ScreenMorePuzzleList.this.m_hActivity, sPuzzleId);
                        hListItem.tvPuzzleProgressData.setText(String.valueOf(iImageIndexNextUnsolved) + "/" + MorePuzzleList.MorePuzzleProgress.getImageCount(ScreenMorePuzzleList.this.m_hActivity, sPuzzleId));
                        hListItem.tvPuzzleProgressData.setVisibility(0);
                        break;
                    }
                case 1:
                    hListItem.tvPuzzleProgressData.setText((CharSequence) null);
                    hListItem.tvPuzzleProgressData.setVisibility(8);
                    break;
                default:
                    throw new RuntimeException("Invalid addon type");
            }
            return hListItem;
        }

        private final class ListItem extends LinearLayout {
            public final ImageView ivPuzzleIcon;
            public final TextView tvPuzzleDeveloperName;
            public final TextView tvPuzzleName;
            public final TextView tvPuzzleProgressData;

            public ListItem(Context hContext) {
                super(hContext);
                DisplayMetrics hMetrics = getResources().getDisplayMetrics();
                setLayoutParams(new AbsListView.LayoutParams(-1, -1));
                setOrientation(0);
                setGravity(19);
                int iDip10 = ResDimens.getDip(hMetrics, 10);
                setPadding(iDip10, iDip10, iDip10, iDip10);
                int iDipIconSize = ResDimens.getDip(hMetrics, 48);
                this.ivPuzzleIcon = new ImageView(hContext);
                this.ivPuzzleIcon.setLayoutParams(new LinearLayout.LayoutParams(iDipIconSize, iDipIconSize));
                addView(this.ivPuzzleIcon);
                LinearLayout vgTextContainer = new LinearLayout(hContext);
                vgTextContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                vgTextContainer.setOrientation(1);
                vgTextContainer.setGravity(19);
                vgTextContainer.setPadding(iDip10, 0, 0, 0);
                addView(vgTextContainer);
                this.tvPuzzleName = getTextView(hContext);
                this.tvPuzzleName.setTypeface(Typeface.DEFAULT, 1);
                vgTextContainer.addView(this.tvPuzzleName);
                this.tvPuzzleDeveloperName = getTextView(hContext);
                vgTextContainer.addView(this.tvPuzzleDeveloperName);
                this.tvPuzzleProgressData = getTextView(hContext);
                vgTextContainer.addView(this.tvPuzzleProgressData);
            }

            private TextView getTextView(Context hContext) {
                TextView tvText = new TextView(hContext);
                tvText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                tvText.setTextColor(-1);
                tvText.setTextSize(1, 16.0f);
                tvText.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
                return tvText;
            }
        }
    }
}
