package com.mobclick.android;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

public class f {
    private LocationManager a;
    private Context b;

    public f(Context context) {
        this.b = context;
    }

    public Location a() {
        Location lastKnownLocation;
        Location lastKnownLocation2;
        try {
            this.a = (LocationManager) this.b.getSystemService("location");
            if (m.a(this.b, "android.permission.ACCESS_FINE_LOCATION") && (lastKnownLocation2 = this.a.getLastKnownLocation("gps")) != null) {
                Log.i("MobclickAgent", "get location from gps:" + lastKnownLocation2.getLatitude() + "," + lastKnownLocation2.getLongitude());
                return lastKnownLocation2;
            } else if (!m.a(this.b, "android.permission.ACCESS_COARSE_LOCATION") || (lastKnownLocation = this.a.getLastKnownLocation("network")) == null) {
                Log.i("MobclickAgent", "Could not get location from GPS or Cell-id, lack ACCESS_COARSE_LOCATION or ACCESS_COARSE_LOCATION permission?");
                return null;
            } else {
                Log.i("MobclickAgent", "get location from network:" + lastKnownLocation.getLatitude() + "," + lastKnownLocation.getLongitude());
                return lastKnownLocation;
            }
        } catch (Exception e) {
            Log.e("MobclickAgent", e.getMessage());
            return null;
        }
    }
}
