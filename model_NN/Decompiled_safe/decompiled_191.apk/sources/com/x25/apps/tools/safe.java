package com.x25.apps.tools;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;
import com.mobclick.android.MobclickAgent;

public class safe extends Activity {
    private Ads ads;
    public Context context;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        this.context = this;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.safe);
        ((Button) findViewById(R.id.safe_go)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DatePicker mdate = (DatePicker) safe.this.findViewById(R.id.mdate);
                int year = mdate.getYear();
                int month = mdate.getMonth() + 1;
                int day = mdate.getDayOfMonth();
                String mday_str = ((EditText) safe.this.findViewById(R.id.safe_mday)).getText().toString();
                String cday_str = ((EditText) safe.this.findViewById(R.id.safe_cday)).getText().toString();
                if (mday_str.length() < 1 || cday_str.length() < 1) {
                    Toast.makeText(safe.this.context, (int) R.string.safe_input_tips, 5000).show();
                    return;
                }
                int mday = Integer.valueOf(mday_str).intValue();
                int cday = Integer.valueOf(cday_str).intValue();
                if (mday > 36 || mday < 26) {
                    Toast.makeText(safe.this.context, (int) R.string.safe_mday_tips, 5000).show();
                }
                if (cday > 8 || cday < 3) {
                    Toast.makeText(safe.this.context, (int) R.string.safe_cday_tips, 5000).show();
                }
                Intent intent = new Intent();
                intent.setClass(safe.this, safeResult.class);
                intent.putExtra("year", year);
                intent.putExtra("month", month);
                intent.putExtra("day", day);
                intent.putExtra("mday", mday);
                intent.putExtra("cday", cday);
                safe.this.context.startActivity(intent);
            }
        });
        this.ads = new Ads(this);
        this.ads.getAd().getWb();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }
}
