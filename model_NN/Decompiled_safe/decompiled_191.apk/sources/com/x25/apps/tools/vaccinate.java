package com.x25.apps.tools;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import com.mobclick.android.MobclickAgent;

public class vaccinate extends Activity {
    private Ads ads;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.vaccinate);
        ((WebView) findViewById(R.id.vaccinate_wv)).loadUrl("file:///android_asset/vaccinate.html");
        this.ads = new Ads(this);
        this.ads.getAd().getWb();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }
}
