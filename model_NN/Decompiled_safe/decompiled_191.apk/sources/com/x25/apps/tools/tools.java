package com.x25.apps.tools;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import com.mobclick.android.MobclickAgent;
import com.waps.AppConnect;
import com.waps.UpdatePointsNotifier;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class tools extends Activity implements UpdatePointsNotifier {
    protected static final int MENU_ABOUT = 2;
    protected static final int MENU_QUIT = 3;
    protected static final int MENU_UPDATE = 1;
    private Ads ads;
    public Class<?>[] clses;
    public Context context = this;
    public Integer[] images;
    /* access modifiers changed from: private */
    public int needPoint = 80;
    /* access modifiers changed from: private */
    public int point = 0;
    public String[] texts;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        Global.disableWaps("2011-08-19 18:00:00");
        MobclickAgent.update(this);
        AppConnect.getInstance(this);
        GridView grid = (GridView) findViewById(R.id.tools);
        grid.setAdapter((ListAdapter) new SimpleAdapter(this.context, loadApps(), R.layout.grid_item, new String[]{"imageView", "imageTitle"}, new int[]{R.id.imageView, R.id.imageTitle}));
        grid.setOnItemClickListener(new ItemClickListener());
        this.ads = new Ads(this);
        this.ads.getAd().getWb();
    }

    private List<Map<String, Object>> loadApps() {
        List<Map<String, Object>> list = new ArrayList<>();
        this.images = new Integer[]{Integer.valueOf((int) R.drawable.sex), Integer.valueOf((int) R.drawable.pregnancy), Integer.valueOf((int) R.drawable.blood), Integer.valueOf((int) R.drawable.edd), Integer.valueOf((int) R.drawable.height), Integer.valueOf((int) R.drawable.safe), Integer.valueOf((int) R.drawable.vaccinate), Integer.valueOf((int) R.drawable.zhidao), Integer.valueOf((int) R.drawable.check)};
        this.texts = new String[]{getResources().getString(R.string.sex_app), getResources().getString(R.string.pregnancy_app), getResources().getString(R.string.blood_app), getResources().getString(R.string.edd_app), getResources().getString(R.string.height_app), getResources().getString(R.string.safe_app), getResources().getString(R.string.vaccinate_app), getResources().getString(R.string.zhidao_app), getResources().getString(R.string.check_app)};
        this.clses = new Class[]{sex.class, pregnancy.class, blood.class, edd.class, height.class, safe.class, vaccinate.class, zhidao.class, check.class};
        int j = this.texts.length;
        for (int i = 0; i < j; i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("imageView", this.images[i]);
            map.put("imageTitle", this.texts[i]);
            map.put("intent", browseIntent(this.clses[i]));
            list.add(map);
        }
        return list;
    }

    /* access modifiers changed from: protected */
    public Intent browseIntent(Class<?> cls) {
        Intent intent = new Intent();
        intent.setClass(this, cls);
        return intent;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (Global.isWaps(this)) {
            menu.add(0, 1, 0, (int) R.string.waps).setIcon((int) R.drawable.update);
        } else {
            menu.add(0, 1, 0, (int) R.string.update).setIcon((int) R.drawable.update);
        }
        menu.add(0, 2, 0, (int) R.string.about).setIcon((int) R.drawable.about);
        menu.add(0, 3, 0, (int) R.string.quit).setIcon((int) R.drawable.quit);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 1:
                if (Global.isWaps(this)) {
                    AppConnect.getInstance(this).showOffers(this);
                    return true;
                }
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:\"AppTeam Lab\"")));
                return true;
            case 2:
                String about = this.context.getResources().getString(R.string.app_about);
                try {
                    InputStream inputStream = this.context.getResources().openRawResource(R.raw.about);
                    byte[] reader = new byte[inputStream.available()];
                    do {
                    } while (inputStream.read(reader) != -1);
                    inputStream.close();
                    about = String.valueOf(about) + new String(reader, "GBK");
                } catch (IOException e) {
                }
                new AlertDialog.Builder(this).setTitle((int) R.string.app_name).setMessage(Html.fromHtml(about)).setPositiveButton((int) R.string.back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
                return true;
            case 3:
                quit();
                return true;
            default:
                return true;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        quit();
        return false;
    }

    private void quit() {
        new AlertDialog.Builder(this).setTitle((int) R.string.quit).setMessage((int) R.string.quit_confirm).setPositiveButton((int) R.string.confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ((ActivityManager) tools.this.getSystemService("activity")).restartPackage(tools.this.getPackageName());
                tools.this.finish();
            }
        }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    class ItemClickListener implements AdapterView.OnItemClickListener {
        ItemClickListener() {
        }

        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
            if (Global.isWaps(tools.this)) {
                Store store = new Store(tools.this);
                if (store.load("isBuy") == 0) {
                    if (tools.this.point < tools.this.needPoint) {
                        new AlertDialog.Builder(tools.this).setTitle("激活软件功能").setMessage("请先激活，永久开启所有软件功能只需要 " + tools.this.needPoint + " 积分，你当前积分余额为：" + tools.this.point + " ，您可以通过下载安装精品应用 ‘免费赚积分’。为确保积分到账，安装后请启动一次！").setPositiveButton("免费赚取 " + tools.this.needPoint + " 积分", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                                AppConnect.getInstance(tools.this).showOffers(tools.this);
                            }
                        }).show();
                        return;
                    }
                    tools tools = tools.this;
                    tools.point = tools.point - tools.this.needPoint;
                    AppConnect.getInstance(tools.this).spendPoints(tools.this.needPoint, tools.this);
                    store.save("isBuy", 1);
                    new AlertDialog.Builder(tools.this).setTitle("开启软件功能成功").setMessage("恭喜您，你已经成功永久开启所有软件功能，现在你可以自由免费使用所有功能啦！").setPositiveButton((int) R.string.confirm, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                        }
                    }).show();
                    return;
                }
            }
            tools.this.context.startActivity((Intent) ((HashMap) parent.getItemAtPosition(position)).get("intent"));
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        AppConnect.getInstance(this).getPoints(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AppConnect.getInstance(this).finalize();
        this.ads.finalize();
    }

    public void getUpdatePoints(String currencyName, int pointTotal) {
        this.point = pointTotal;
    }

    public void getUpdatePointsFailed(String error) {
        this.point = 0;
    }
}
