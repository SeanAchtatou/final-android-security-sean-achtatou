package com.x25.apps.tools;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.mobclick.android.MobclickAgent;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class edd extends Activity {
    private static final String[] mWeek = {"3周", "4周", "5周"};
    private Ads ads;
    public Context context;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        this.context = this;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.edd);
        ArrayAdapter<String> weekAdapter = new ArrayAdapter<>(this, 17367048, mWeek);
        weekAdapter.setDropDownViewResource(17367049);
        ((Spinner) findViewById(R.id.edd_week)).setAdapter((SpinnerAdapter) weekAdapter);
        ((Button) findViewById(R.id.edd_go)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String sWeek;
                DatePicker mdate = (DatePicker) edd.this.findViewById(R.id.mdate);
                int year = mdate.getYear();
                int month = mdate.getMonth() + 1;
                int day = mdate.getDayOfMonth();
                int mweek = (int) ((Spinner) edd.this.findViewById(R.id.edd_week)).getSelectedItemId();
                long uTime = 0;
                try {
                    uTime = new SimpleDateFormat("yyyy-MM-dd").parse(String.valueOf(year) + "-" + month + "-" + day).getTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                long nTime = System.currentTimeMillis();
                String date = new SimpleDateFormat("yyyy-MM-dd").format(Long.valueOf(uTime + (((long) ((mweek + 39) * 604800)) * 1000)));
                int nWeek = Math.round((float) ((nTime - uTime) / 604800000));
                if (nWeek < 0) {
                    sWeek = edd.this.context.getResources().getString(R.string.edd_result_week_no);
                } else if (nWeek > 41) {
                    sWeek = edd.this.context.getResources().getString(R.string.edd_result_week_exp);
                } else {
                    sWeek = String.valueOf(edd.this.context.getResources().getString(R.string.edd_result_week)) + nWeek + edd.this.context.getResources().getString(R.string.edd_result_week_sub);
                }
                TextView tResult = (TextView) edd.this.findViewById(R.id.edd_result);
                tResult.setText(String.valueOf(edd.this.context.getResources().getString(R.string.edd_result)) + date);
                TextView wResult = (TextView) edd.this.findViewById(R.id.edd_result_week);
                wResult.setText(sWeek);
                tResult.setVisibility(0);
                wResult.setVisibility(0);
            }
        });
        this.ads = new Ads(this);
        this.ads.getAd().getWb();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }
}
