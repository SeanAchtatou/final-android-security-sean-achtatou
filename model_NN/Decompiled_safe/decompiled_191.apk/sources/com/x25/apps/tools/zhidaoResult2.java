package com.x25.apps.tools;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.mobclick.android.MobclickAgent;

public class zhidaoResult2 extends Activity {
    private Ads ads;
    /* access modifiers changed from: private */
    public Context context;
    public String[] data_1 = new String[42];
    public String[] data_2 = new String[42];
    public String[] data_3 = new String[42];
    public String[] data_4 = new String[42];
    /* access modifiers changed from: private */
    public int index;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        setContentView((int) R.layout.zhidao_result2);
        initData();
        this.index = getIntent().getExtras().getInt("index");
        ((TextView) findViewById(R.id.zhidao_tv)).setText("第 " + this.index + " 孕周");
        ((TextView) findViewById(R.id.zhidao_tv_1)).setText(this.data_1[this.index]);
        ((TextView) findViewById(R.id.zhidao_tv_2)).setText(this.data_2[this.index]);
        ((TextView) findViewById(R.id.zhidao_tv_3)).setText(this.data_3[this.index]);
        ((TextView) findViewById(R.id.zhidao_tv_4)).setText(this.data_4[this.index]);
        ((ImageButton) findViewById(R.id.zhidao_pre)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(zhidaoResult2.this, zhidao.class);
                intent.putExtra("index", zhidaoResult2.this.index - 1);
                intent.putExtra("cur", 1);
                zhidaoResult2.this.context.startActivity(intent);
                zhidaoResult2.this.finish();
            }
        });
        ((ImageButton) findViewById(R.id.zhidao_next)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(zhidaoResult2.this, zhidao.class);
                intent.putExtra("index", zhidaoResult2.this.index + 1);
                intent.putExtra("cur", 1);
                zhidaoResult2.this.context.startActivity(intent);
                zhidaoResult2.this.finish();
            }
        });
        this.ads = new Ads(this);
        this.ads.getAd().getWb();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }

    private void initData() {
        this.data_1[1] = "月经过期10天或10天以上";
        this.data_1[2] = "推算排卵期和月经周期";
        this.data_1[3] = "体重增加，身体伸展";
        this.data_1[4] = "身体有点像感冒的症状";
        this.data_1[5] = "早孕反应出现了";
        this.data_1[6] = "胸部胀痛、乳房增大变软、容易犯困";
        this.data_1[7] = "经常感到饥饿，情绪易波动";
        this.data_1[8] = "子宫在长大 ";
        this.data_1[9] = "晨昏乏力、身体不适、恶心呕吐 ";
        this.data_1[10] = "受孕激素影响情绪波动";
        this.data_1[11] = "腹部出现了一条妊娠纹";
        this.data_1[12] = "面上出现褐色的斑块";
        this.data_1[13] = "腹部隆起，该穿孕妇装了";
        this.data_1[14] = "体重增加，乳房改变 ";
        this.data_1[15] = "能感觉到胎动了";
        this.data_1[16] = "胎动更明显，甚至有触痛感";
        this.data_1[17] = "身体重心变化，行动有些不方便";
        this.data_1[18] = "作B超看宝宝模样";
        this.data_1[19] = "子宫底每周会升高１厘米 ";
        this.data_1[20] = "腹部越来越大，易疲劳，有腰痛";
        this.data_1[21] = "食欲大增，偏食减少";
        this.data_1[22] = "上楼到吃力呼吸相对困难";
        this.data_1[23] = "子宫扩展到肚脐上方";
        this.data_1[24] = "妊娠斑妊娠纹悄悄出现";
        this.data_1[25] = "眼睛易发干和遇光流泪";
        this.data_1[26] = "子宫顶部在肚脐上方6.25厘米处";
        this.data_1[27] = "体重增长幅度加大";
        this.data_1[28] = "偶尔肚子发硬发紧，这是假宫缩";
        this.data_1[29] = "秘便、水肿状况增多 ";
        this.data_1[30] = "呼吸困难，胃部受挤";
        this.data_1[31] = "体重增长较快";
        this.data_1[32] = "尿意频繁，易感疲惫";
        this.data_1[33] = "手脚、腿等会出现水肿";
        this.data_1[34] = "胎头下降，妈妈呼吸顺畅";
        this.data_1[35] = "身体会越来越感到沉重";
        this.data_1[36] = "下腹部坠胀，尿频";
        this.data_1[37] = "子宫颈为分娩作准备而扩大";
        this.data_1[38] = "水肿可能再次出现";
        this.data_1[39] = "心情紧张烦躁很正常";
        this.data_1[40] = "到预产期了";
        this.data_1[41] = "生育完毕";
        this.data_2[1] = "为卵子受精作最后准备";
        this.data_2[2] = "精子受精能力可维持2-3天";
        this.data_2[3] = "反射：摩罗、踏足、觅食";
        this.data_2[4] = "B超看到它像一颗小松子";
        this.data_2[5] = "胚胎长约0.6厘米，象小苹果籽";
        this.data_2[6] = "胚胎形状象颗蚕豆了";
        this.data_2[7] = "宝宝约有一颗桑葚那么大";
        this.data_2[8] = "胎宝贝在迅速生长 ";
        this.data_2[9] = "生殖器官已经在生长了";
        this.data_2[10] = "宝宝像一个扁豆荚";
        this.data_2[11] = "宝宝开始爱做伸展运动";
        this.data_2[12] = "宝宝已经初具人形";
        this.data_2[13] = "胎儿的脸看上去更像成人了";
        this.data_2[14] = "胎儿会做鬼脸吃手指了";
        this.data_2[15] = "在子宫中打嗝了";
        this.data_2[16] = "胎儿看上去象一个梨子 ";
        this.data_2[17] = "胎儿心跳强劲有力 ";
        this.data_2[18] = "宝宝的生殖器已经各就各位 ";
        this.data_2[19] = "能吞咽羊水，肾脏已能产尿液";
        this.data_2[20] = "胎儿生长趋于平稳 ";
        this.data_2[21] = "眉毛和眼睑清晰可见";
        this.data_2[22] = "恒牙的牙胚在发育";
        this.data_2[23] = "胎儿听力基本形成";
        this.data_2[24] = "宝宝占了子宫大部分空间";
        this.data_2[25] = "胎儿舌头上的味蕾正在形成";
        this.data_2[26] = "宝宝很瘦，全身覆盖细细的绒毛";
        this.data_2[27] = "眼睛可以睁开和闭合";
        this.data_2[28] = "占满了整个子宫";
        this.data_2[29] = "听觉系统也发育完成";
        this.data_2[30] = "胎儿的眼睛开闭自由 ";
        this.data_2[31] = "肺和胃肠接近成熟";
        this.data_2[32] = "生殖器发育接近成熟，胎头";
        this.data_2[33] = "呼吸系统和消化系统发育已经接近成熟";
        this.data_2[34] = "圆圆的开始变胖";
        this.data_2[35] = "指甲长长了";
        this.data_2[36] = "已经是个足月儿了";
        this.data_2[37] = "头发已经长的又长又密了";
        this.data_2[38] = "皮肤开始变得光滑";
        this.data_2[39] = "身体各器官都发育完成";
        this.data_2[40] = "......";
        this.data_2[41] = "......";
        this.data_3[1] = "谨记减少食盐量";
        this.data_3[2] = "做X线检查4周后再考虑怀孕";
        this.data_3[3] = "第一次微笑";
        this.data_3[4] = "不要随便吃药，剧烈运动";
        this.data_3[5] = "禁烟戒酒、营养适度、适当活动、谨慎服药";
        this.data_3[6] = "警惕宫外孕";
        this.data_3[7] = "预防早期流产 ";
        this.data_3[8] = "洗浴时间要适度";
        this.data_3[9] = "刺激性饮料不要喝";
        this.data_3[10] = "要去医院做产前检查了";
        this.data_3[11] = "注意感冒侵袭，增强抵抗力";
        this.data_3[12] = "如果经常头晕眼花要去医院检查";
        this.data_3[13] = "可以性生活，但要节制";
        this.data_3[14] = "不宜抬重物，不要往高处够 ";
        this.data_3[15] = "开始做运动，但动动量要由小增至适合自已的量 ";
        this.data_3[16] = "防止静脉曲张 ";
        this.data_3[17] = "在胎儿觉醒时做胎教";
        this.data_3[18] = "吃得过多有害无益，营养均衡最重要";
        this.data_3[19] = "乳房护理很重要 ";
        this.data_3[20] = "注意头发护理";
        this.data_3[21] = "不宜盲目大量补充各种维生素类药物";
        this.data_3[22] = "防止贫血 ";
        this.data_3[23] = "用药要谨慎 ";
        this.data_3[24] = "当心妊娠期糖尿病";
        this.data_3[25] = "当心孕期水肿";
        this.data_3[26] = "皮肤很痒要防胆汁郁积";
        this.data_3[27] = "羊水过少过多都要重视";
        this.data_3[28] = "监护宝宝胎心、体重";
        this.data_3[29] = "合理休息保睡眠质量";
        this.data_3[30] = "白带增多注意外阴清洁";
        this.data_3[31] = "准备待产包";
        this.data_3[32] = "适当运动，预防早产";
        this.data_3[33] = "学习一些分娩技巧";
        this.data_3[34] = "防胎位不正，提前请好产假";
        this.data_3[35] = "膳食纤维，防止便秘";
        this.data_3[36] = "留意孕晚期阴道出血 ";
        this.data_3[37] = "慎防宫内感染 ";
        this.data_3[38] = "了解自然分娩与剖腹产";
        this.data_3[39] = "随时做好临产的准备";
        this.data_3[40] = "......";
        this.data_3[41] = "......";
        this.data_4[1] = "补钙、维生素、叶酸，少食多餐";
        this.data_4[2] = "补钙、维生素、叶酸，少食多餐";
        this.data_4[3] = "状态：睡眠、警觉、裤子";
        this.data_4[4] = "每天叶酸摄取量应达约1毫克";
        this.data_4[5] = "不要因呕吐而拒食";
        this.data_4[6] = "克服早孕反应，尽量吃东西";
        this.data_4[7] = "饿了就得吃，补充营养";
        this.data_4[8] = "......";
        this.data_4[9] = "各种营养每天摄入量有标准";
        this.data_4[10] = "孕期营养及胎教知识指导";
        this.data_4[11] = "烟、酒和咖啡对胎儿有害";
        this.data_4[12] = "......";
        this.data_4[13] = "早孕反应消失，食欲开始增加";
        this.data_4[14] = "不要饮酒、吸烟";
        this.data_4[15] = "给胎儿更多的营养 ";
        this.data_4[16] = "多吃补铁补血食物";
        this.data_4[17] = "水果、核桃每天都吃一些";
        this.data_4[18] = "多吃富含钙、锌、铁的食物";
        this.data_4[19] = "吃得杂一些，不偏食";
        this.data_4[20] = "增强营养，饮食均衡 ";
        this.data_4[21] = "多种营养不可缺";
        this.data_4[22] = "黄豆、牛肉、肝、肾 ";
        this.data_4[23] = "过胖或过瘦咨询医生作饮食调整 ";
        this.data_4[24] = "少食多餐，增加膳食纤维";
        this.data_4[25] = "控制盐份摄入，吃冬瓜消水肿";
        this.data_4[26] = "营养全面、饮食均衡";
        this.data_4[27] = "西瓜、红豆、洋葱、大蒜、茄子、芹菜";
        this.data_4[28] = "多吃些豆类和谷类的食品";
        this.data_4[29] = "睡前2小时内不要大量吃喝";
        this.data_4[30] = "营养适中防止巨大儿 ";
        this.data_4[31] = "含糖高的食品不要吃得太多";
        this.data_4[32] = "少吃多餐更合适";
        this.data_4[33] = "各种食物少量丰富";
        this.data_4[34] = "饮食无需刻意";
        this.data_4[35] = "全麦面包、芹菜、胡萝卜、白薯... ";
        this.data_4[36] = "饮食要清淡不要吃太咸";
        this.data_4[37] = "......";
        this.data_4[38] = "......";
        this.data_4[39] = "......";
        this.data_4[40] = "......";
        this.data_4[41] = "......";
    }
}
