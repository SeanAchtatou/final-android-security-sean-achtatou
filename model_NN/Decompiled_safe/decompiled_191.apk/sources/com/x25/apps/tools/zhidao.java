package com.x25.apps.tools;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import com.mobclick.android.MobclickAgent;

public class zhidao extends TabActivity {
    protected static final int MENU_BACK = 1;
    public Context context;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        int index;
        int cur;
        this.context = this;
        super.onCreate(savedInstanceState);
        try {
            Bundle extras = getIntent().getExtras();
            index = extras.getInt("index");
            cur = extras.getInt("cur");
            if (index < 1 || index > 41) {
                index = 1;
            }
        } catch (Exception e) {
            index = 1;
            cur = 0;
        }
        setTitle("孕 " + (((int) Math.ceil((double) ((index - 1) / 4))) + 1) + " 月 第 " + index + " 孕周");
        TabHost tabHost = getTabHost();
        TabHost.TabSpec tabSpec = tabHost.newTabSpec("pic");
        tabSpec.setIndicator(getResources().getString(R.string.zhidao_pic), getResources().getDrawable(R.drawable.zhidao_tab_1));
        Intent intent = new Intent();
        intent.setClass(this, zhidaoResult.class);
        intent.putExtra("index", index);
        tabSpec.setContent(intent);
        tabHost.addTab(tabSpec);
        TabHost.TabSpec tabSpec2 = tabHost.newTabSpec("tips");
        tabSpec2.setIndicator(getResources().getString(R.string.zhidao_tips), getResources().getDrawable(R.drawable.zhidao_tab_2));
        Intent intent2 = new Intent();
        intent2.setClass(this, zhidaoResult2.class);
        intent2.putExtra("index", index);
        tabSpec2.setContent(intent2);
        tabHost.addTab(tabSpec2);
        tabHost.setCurrentTab(cur);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.back).setIcon((int) R.drawable.quit);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 1:
                finish();
                Intent intent = new Intent();
                intent.setClass(this, tools.class);
                this.context.startActivity(intent);
                return true;
            default:
                return true;
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
