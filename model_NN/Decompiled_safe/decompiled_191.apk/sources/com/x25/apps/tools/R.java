package com.x25.apps.tools;

public final class R {

    public static final class anim {
        public static final int layout_wave_scale = 2130968576;
        public static final int wave_scale = 2130968577;
        public static final int zoom_enter = 2130968578;
        public static final int zoom_exit = 2130968579;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int blood = 2130837505;
        public static final int check = 2130837506;
        public static final int edd = 2130837507;
        public static final int height = 2130837508;
        public static final int icon = 2130837509;
        public static final int left = 2130837510;
        public static final int main_bg = 2130837511;
        public static final int pregnancy = 2130837512;
        public static final int quit = 2130837513;
        public static final int right = 2130837514;
        public static final int safe = 2130837515;
        public static final int sex = 2130837516;
        public static final int update = 2130837517;
        public static final int vaccinate = 2130837518;
        public static final int zhidao = 2130837519;
        public static final int zhidao_1 = 2130837520;
        public static final int zhidao_10 = 2130837521;
        public static final int zhidao_11 = 2130837522;
        public static final int zhidao_12 = 2130837523;
        public static final int zhidao_13 = 2130837524;
        public static final int zhidao_14 = 2130837525;
        public static final int zhidao_15 = 2130837526;
        public static final int zhidao_16 = 2130837527;
        public static final int zhidao_17 = 2130837528;
        public static final int zhidao_18 = 2130837529;
        public static final int zhidao_19 = 2130837530;
        public static final int zhidao_2 = 2130837531;
        public static final int zhidao_20 = 2130837532;
        public static final int zhidao_21 = 2130837533;
        public static final int zhidao_22 = 2130837534;
        public static final int zhidao_23 = 2130837535;
        public static final int zhidao_24 = 2130837536;
        public static final int zhidao_25 = 2130837537;
        public static final int zhidao_26 = 2130837538;
        public static final int zhidao_27 = 2130837539;
        public static final int zhidao_28 = 2130837540;
        public static final int zhidao_29 = 2130837541;
        public static final int zhidao_3 = 2130837542;
        public static final int zhidao_30 = 2130837543;
        public static final int zhidao_31 = 2130837544;
        public static final int zhidao_32 = 2130837545;
        public static final int zhidao_33 = 2130837546;
        public static final int zhidao_34 = 2130837547;
        public static final int zhidao_35 = 2130837548;
        public static final int zhidao_36 = 2130837549;
        public static final int zhidao_37 = 2130837550;
        public static final int zhidao_38 = 2130837551;
        public static final int zhidao_39 = 2130837552;
        public static final int zhidao_4 = 2130837553;
        public static final int zhidao_40 = 2130837554;
        public static final int zhidao_41 = 2130837555;
        public static final int zhidao_5 = 2130837556;
        public static final int zhidao_6 = 2130837557;
        public static final int zhidao_7 = 2130837558;
        public static final int zhidao_8 = 2130837559;
        public static final int zhidao_9 = 2130837560;
        public static final int zhidao_icon_1 = 2130837561;
        public static final int zhidao_icon_2 = 2130837562;
        public static final int zhidao_icon_3 = 2130837563;
        public static final int zhidao_icon_4 = 2130837564;
        public static final int zhidao_tab_1 = 2130837565;
        public static final int zhidao_tab_2 = 2130837566;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131165208;
        public static final int OfferProgressBar = 2131165211;
        public static final int RelativeLayout01 = 2131165209;
        public static final int age = 2131165222;
        public static final int app = 2131165226;
        public static final int appIcon = 2131165227;
        public static final int baby_sex = 2131165201;
        public static final int blood_go = 2131165186;
        public static final int blood_intro = 2131165188;
        public static final int blood_tips = 2131165187;
        public static final int blood_type = 2131165184;
        public static final int blood_type1 = 2131165185;
        public static final int check_wv = 2131165189;
        public static final int description = 2131165231;
        public static final int edd_go = 2131165192;
        public static final int edd_intro = 2131165196;
        public static final int edd_result = 2131165193;
        public static final int edd_result_week = 2131165194;
        public static final int edd_tips = 2131165195;
        public static final int edd_week = 2131165190;
        public static final int height_father = 2131165199;
        public static final int height_go = 2131165204;
        public static final int height_intro = 2131165206;
        public static final int height_mother = 2131165200;
        public static final int height_tips = 2131165205;
        public static final int imageTitle = 2131165198;
        public static final int imageView = 2131165197;
        public static final int mdate = 2131165191;
        public static final int month = 2131165223;
        public static final int notification = 2131165229;
        public static final int offersWebView = 2131165210;
        public static final int pregnancy_age = 2131165212;
        public static final int pregnancy_go = 2131165214;
        public static final int pregnancy_sex = 2131165213;
        public static final int pregnancy_tips = 2131165215;
        public static final int progress_bar = 2131165232;
        public static final int progress_text = 2131165228;
        public static final int safe_cday = 2131165218;
        public static final int safe_go = 2131165219;
        public static final int safe_intro = 2131165220;
        public static final int safe_mday = 2131165217;
        public static final int sex_f = 2131165203;
        public static final int sex_go = 2131165224;
        public static final int sex_intro = 2131165216;
        public static final int sex_m = 2131165202;
        public static final int sex_tips = 2131165225;
        public static final int title = 2131165230;
        public static final int tools = 2131165207;
        public static final int vaccinate_wv = 2131165233;
        public static final int wv = 2131165221;
        public static final int zhidao_iv = 2131165237;
        public static final int zhidao_next = 2131165236;
        public static final int zhidao_pre = 2131165234;
        public static final int zhidao_tv = 2131165235;
        public static final int zhidao_tv_1 = 2131165238;
        public static final int zhidao_tv_2 = 2131165239;
        public static final int zhidao_tv_3 = 2131165240;
        public static final int zhidao_tv_4 = 2131165241;
    }

    public static final class layout {
        public static final int blood = 2130903040;
        public static final int check = 2130903041;
        public static final int edd = 2130903042;
        public static final int grid_item = 2130903043;
        public static final int height = 2130903044;
        public static final int main = 2130903045;
        public static final int offers_web_view = 2130903046;
        public static final int pregnancy = 2130903047;
        public static final int safe = 2130903048;
        public static final int safe_result = 2130903049;
        public static final int sex = 2130903050;
        public static final int umeng_download_notification = 2130903051;
        public static final int vaccinate = 2130903052;
        public static final int zhidao_result = 2130903053;
        public static final int zhidao_result2 = 2130903054;
    }

    public static final class raw {
        public static final int about = 2131034112;
    }

    public static final class string {
        public static final int about = 2131099653;
        public static final int app_about = 2131099649;
        public static final int app_name = 2131099648;
        public static final int back = 2131099650;
        public static final int blood_app = 2131099674;
        public static final int blood_app_intro = 2131099675;
        public static final int blood_app_tips = 2131099680;
        public static final int blood_go = 2131099678;
        public static final int blood_result = 2131099679;
        public static final int blood_str = 2131099676;
        public static final int blood_str1 = 2131099677;
        public static final int cancel = 2131099657;
        public static final int check_app = 2131099724;
        public static final int confirm = 2131099656;
        public static final int edd_app = 2131099681;
        public static final int edd_app_intro = 2131099682;
        public static final int edd_app_tips = 2131099691;
        public static final int edd_go = 2131099685;
        public static final int edd_mdate = 2131099684;
        public static final int edd_mweek = 2131099683;
        public static final int edd_result = 2131099686;
        public static final int edd_result_week = 2131099689;
        public static final int edd_result_week_exp = 2131099688;
        public static final int edd_result_week_no = 2131099687;
        public static final int edd_result_week_sub = 2131099690;
        public static final int height_app = 2131099692;
        public static final int height_app_intro = 2131099693;
        public static final int height_app_tips = 2131099703;
        public static final int height_father_str = 2131099694;
        public static final int height_go = 2131099700;
        public static final int height_input_tips = 2131099701;
        public static final int height_mother_str = 2131099695;
        public static final int height_result = 2131099702;
        public static final int height_sex = 2131099696;
        public static final int height_sex_f = 2131099698;
        public static final int height_sex_m = 2131099697;
        public static final int height_unit = 2131099699;
        public static final int pregnancy_age = 2131099668;
        public static final int pregnancy_app = 2131099666;
        public static final int pregnancy_app_intro = 2131099667;
        public static final int pregnancy_app_tips = 2131099671;
        public static final int pregnancy_go = 2131099669;
        public static final int pregnancy_result_f = 2131099673;
        public static final int pregnancy_result_m = 2131099672;
        public static final int pregnancy_sex = 2131099670;
        public static final int quit = 2131099654;
        public static final int quit_confirm = 2131099655;
        public static final int safe_app = 2131099704;
        public static final int safe_app_intro = 2131099705;
        public static final int safe_cday = 2131099707;
        public static final int safe_cday_tips = 2131099713;
        public static final int safe_go = 2131099710;
        public static final int safe_input_tips = 2131099711;
        public static final int safe_mdate = 2131099709;
        public static final int safe_mday = 2131099706;
        public static final int safe_mday_tips = 2131099712;
        public static final int safe_unit = 2131099708;
        public static final int sex_age = 2131099661;
        public static final int sex_app = 2131099658;
        public static final int sex_app_intro = 2131099659;
        public static final int sex_app_tips = 2131099660;
        public static final int sex_go = 2131099663;
        public static final int sex_month = 2131099662;
        public static final int sex_result_f = 2131099665;
        public static final int sex_result_m = 2131099664;
        public static final int update = 2131099651;
        public static final int vaccinate_app = 2131099714;
        public static final int waps = 2131099652;
        public static final int zhidao_app = 2131099715;
        public static final int zhidao_next = 2131099719;
        public static final int zhidao_pic = 2131099716;
        public static final int zhidao_pre = 2131099718;
        public static final int zhidao_tips = 2131099717;
        public static final int zhidao_tips_1 = 2131099720;
        public static final int zhidao_tips_2 = 2131099721;
        public static final int zhidao_tips_3 = 2131099722;
        public static final int zhidao_tips_4 = 2131099723;
    }
}
