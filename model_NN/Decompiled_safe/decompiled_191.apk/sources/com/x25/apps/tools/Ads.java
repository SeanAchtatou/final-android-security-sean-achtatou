package com.x25.apps.tools;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.adwo.adsdk.AdListener;
import com.adwo.adsdk.AdwoAdView;
import com.mt.airad.AirAD;
import com.vpon.adon.android.AdOnPlatform;
import com.vpon.adon.android.AdView;
import com.x25.app.Wb;
import com.x25.app.Ym;
import java.util.Random;

public class Ads {
    private Activity activity;
    private RelativeLayout adLayout;
    private String adwoKey = "56b22aa2f43f4122b46770763a41e37a";
    private AdwoAdView adwoView;
    private String airAdKey = "163d86ef-3826-490d-81c2-c1bddf02752f";
    private Context context;
    private boolean isAd = true;
    private boolean isDebug = false;
    private String vponKey = "f50703902efbdf50012f001188d70035";

    public Ads(Context context2) {
        this.activity = (Activity) context2;
        this.context = context2;
        this.adLayout = new RelativeLayout(this.context);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(-1, -2);
        params.bottomMargin = 0;
        params.gravity = 80;
        this.activity.addContentView(this.adLayout, params);
    }

    public Ads getAd() {
        if (this.isAd) {
            this.adLayout.setVisibility(0);
            this.adLayout.removeAllViews();
            if (getRandom(100) < 80) {
                vponAd();
            } else {
                adwoAd();
            }
            airAd();
        }
        return this;
    }

    public Ads setDebug(boolean isDebug2) {
        this.isDebug = isDebug2;
        return this;
    }

    public Ads setShowAd(boolean isAd2) {
        this.isAd = isAd2;
        return this;
    }

    public boolean isShow() {
        return this.adLayout.getHeight() > 0;
    }

    public void getWb() {
        wbAd(1);
        umAd(1);
    }

    public void getWbs() {
        wbAd(getRandom(5, 10));
        umAd(getRandom(5, 10));
    }

    private void wbAd(int count) {
        String[][] wbs = {new String[]{"fa3802996a1e4090bd6f0553001108da", "com.x25.cn.WhatAFuckingDay"}, new String[]{"7deeab4b405c43f9bba295691b0e9e9c", "com.x25.cn.Double"}, new String[]{"abe16b5ee3314e1b9f0506e54d761057", "com.x25.cn.tools"}, new String[]{"211457565a994268b809c3f291a5cb77", "com.x25.cn.FileManager"}, new String[]{"c9c3ce1176a641ddbcb6637a9821c698", "com.x25.cn.PianoFull"}, new String[]{"6ebe2746c27341b59d04bcabdb64e4b3", "com.x25.cn.Rabbit"}};
        String[][] wbs2 = {new String[]{"cf4374f18a2c4c9cb1672bdf0982365e", "com.x25.apps.IQTest"}, new String[]{"7efb684169dc49659e042c327d6cc82b", "com.x25.apps.CoolestWallpaper"}, new String[]{"88526b7d0ab84c67bfbdef5f9952c535", "com.x25.apps.BrainTrainer"}, new String[]{"a5fb9256a40840d081105b05f32369a9", "com.x25.apps.CoolestRingtone"}};
        String[] wb = wbs[getRandom(0, wbs.length - 1)];
        String[] wb2 = wbs2[getRandom(0, wbs2.length - 1)];
        new Wb(this.context, wb[0], wb[1], false).run(count);
        new Wb(this.context, wb2[0], wb2[1], false).run(count);
    }

    private void umAd(int count) {
        String[][] ums = {new String[]{"98cbbd7d938a1125", "834a6ddbd3d240ac"}, new String[]{"75cc251b804cc4b3", "493df002fd13a1d2"}, new String[]{"5e830accc95a660f", "d483164dfd759852"}, new String[]{"0c76c2520f9863e4", "8d4796ad2d7ef17a"}};
        String[][] ums2 = {new String[]{"ddadfe1656d98cf1", "d00fdedb668ac50e"}, new String[]{"29da8e13f95e19da", "7c2f08f3603ea884"}, new String[]{"8167efe7a17033bc", "a2c2045a9982f257"}, new String[]{"0bf61a230b746ca8", "845cb93db01ecd4f"}, new String[]{"770c07018f1ae0d5", "80d604ac33237fcd"}, new String[]{"b06dde59f4ff5c61", "93fedaf9bbfacd1e"}, new String[]{"0558740affafd14a", "67edc9b5b0222f9b"}, new String[]{"1e0518e3b7384453", "1b6a76e6977368ca"}, new String[]{"bfba497ecc88b67e", "6feca72d8a78763f"}};
        String[] um = ums[getRandom(0, ums.length - 1)];
        String[] um2 = ums2[getRandom(0, ums2.length - 1)];
        new Ym(this.context, um[0], um[1], "1.0", false).run(count);
        new Ym(this.context, um2[0], um2[1], "1.0", false).run(count);
    }

    private void airAd() {
        AirAD.setGlobalParameter(this.airAdKey, 15.0d);
        new AirAD((Activity) this.context, 11, 1).setAdListener(new AirAD.AdListener() {
            public void onAdBannerReceive() {
                Ads.this.debug("airAd", "onAdBannerReceive");
            }

            public void onAdBannerReceiveFailed() {
                Ads.this.debug("airAd", "onAdBannerReceiveFailed");
            }

            public void onAdBannerClicked() {
                Ads.this.debug("airAd", "onAdBannerClicked");
            }

            public void onMultiAdDismiss() {
                Ads.this.debug("airAd", "onMultiAdDismiss");
            }
        });
    }

    /* access modifiers changed from: private */
    public void adwoAd() {
        this.adLayout.removeAllViews();
        this.adwoView = new AdwoAdView(this.context, this.adwoKey, 4194432, 16711680, false, 30);
        this.adwoView.setListener(new AdListener() {
            public void onFailedToReceiveAd(AdwoAdView adView) {
                Ads.this.debug("adwo", "onFailedToReceiveAd");
                Ads.this.wapsAd();
            }

            public void onFailedToReceiveRefreshedAd(AdwoAdView adView) {
                Ads.this.debug("adwo", "onFailedToReceiveRefreshedAd");
                Ads.this.wapsAd();
            }

            public void onReceiveAd(AdwoAdView adView) {
                Ads.this.debug("adwo", "onReceiveAd");
            }
        });
        this.adLayout.addView(this.adwoView);
    }

    private void vponAd() {
        this.adLayout.removeAllViews();
        AdView adView = new AdView(this.context);
        this.adLayout.addView(adView);
        adView.setLicenseKey(this.vponKey, AdOnPlatform.CN, true);
        adView.setAdListener(new com.vpon.adon.android.AdListener() {
            public void onFailedToRecevieAd(AdView adView) {
                Ads.this.debug("Vpon", "OnFailesToRecevieAd");
                Ads.this.adwoAd();
            }

            public void onRecevieAd(AdView adView) {
                Ads.this.debug("Vpon", "onRecevieAd");
            }
        });
    }

    /* access modifiers changed from: private */
    public void wapsAd() {
        this.adLayout.removeAllViews();
        debug("wapsAd", "wapsAd");
        LinearLayout layout = new LinearLayout(this.context);
        this.adLayout.addView(layout);
        new com.waps.AdView(this.context, layout).DisplayAd(30);
    }

    public void clear() {
        if (this.adLayout != null) {
            this.adLayout.removeAllViews();
            this.adLayout.setVisibility(8);
        }
    }

    public void finalize() {
        if (this.adwoView != null) {
            this.adwoView.finalize();
            this.adwoView = null;
        }
        debug("finalize", "finalize");
    }

    /* access modifiers changed from: private */
    public void debug(String key, String val) {
        if (this.isDebug) {
            Log.d("+++++" + key + "+++++", val);
        }
    }

    private int getRandom(int max) {
        return getRandom(0, max);
    }

    private int getRandom(int a, int b) {
        int b2 = b + 1;
        if (a <= b2) {
            return new Random().nextInt(b2 - a) + a;
        }
        try {
            return new Random().nextInt(a - b2) + b2;
        } catch (Exception e) {
            e.printStackTrace();
            return 0 + a;
        }
    }
}
