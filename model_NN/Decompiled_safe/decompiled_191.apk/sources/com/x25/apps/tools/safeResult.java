package com.x25.apps.tools;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import com.mobclick.android.MobclickAgent;

public class safeResult extends Activity {
    private Ads ads;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.safe_result);
        Bundle extras = getIntent().getExtras();
        int year = extras.getInt("year");
        WebView wv = (WebView) findViewById(R.id.wv);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("file:///android_asset/safe.html?" + (String.valueOf(year) + "_" + extras.getInt("month") + "_" + extras.getInt("day") + "_" + extras.getInt("mday") + "_" + extras.getInt("cday")));
        this.ads = new Ads(this);
        this.ads.getAd().getWb();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }
}
