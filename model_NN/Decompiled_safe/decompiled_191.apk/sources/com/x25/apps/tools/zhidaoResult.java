package com.x25.apps.tools;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobclick.android.MobclickAgent;

public class zhidaoResult extends Activity {
    private Ads ads;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public int index;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        setContentView((int) R.layout.zhidao_result);
        this.index = getIntent().getExtras().getInt("index");
        ((ImageView) findViewById(R.id.zhidao_iv)).setImageResource(getResources().getIdentifier("zhidao_" + this.index, "drawable", getPackageName()));
        ((TextView) findViewById(R.id.zhidao_tv)).setText("第 " + this.index + " 孕周");
        ((ImageButton) findViewById(R.id.zhidao_pre)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(zhidaoResult.this, zhidao.class);
                intent.putExtra("index", zhidaoResult.this.index - 1);
                intent.putExtra("cur", 0);
                zhidaoResult.this.context.startActivity(intent);
                zhidaoResult.this.finish();
            }
        });
        ((ImageButton) findViewById(R.id.zhidao_next)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(zhidaoResult.this, zhidao.class);
                intent.putExtra("index", zhidaoResult.this.index + 1);
                intent.putExtra("cur", 0);
                zhidaoResult.this.context.startActivity(intent);
                zhidaoResult.this.finish();
            }
        });
        this.ads = new Ads(this);
        this.ads.getAd().getWb();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }
}
