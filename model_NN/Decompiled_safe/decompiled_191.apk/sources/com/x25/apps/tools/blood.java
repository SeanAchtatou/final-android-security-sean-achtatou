package com.x25.apps.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.mobclick.android.MobclickAgent;

public class blood extends Activity {
    private static final String[] mBlood = {"A型", "B型", "O型", "AB型"};
    /* access modifiers changed from: private */
    public static final String[] mBloodType = {"A", "B", "O", "AB"};
    private Ads ads;
    public Context context;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        this.context = this;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.blood);
        ArrayAdapter<String> ageAdapter = new ArrayAdapter<>(this, 17367048, mBlood);
        ageAdapter.setDropDownViewResource(17367049);
        ((Spinner) findViewById(R.id.blood_type)).setAdapter((SpinnerAdapter) ageAdapter);
        ((Spinner) findViewById(R.id.blood_type1)).setAdapter((SpinnerAdapter) ageAdapter);
        ((Button) findViewById(R.id.blood_go)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String b1 = blood.mBloodType[(int) ((Spinner) blood.this.findViewById(R.id.blood_type)).getSelectedItemId()];
                String b2 = blood.mBloodType[(int) ((Spinner) blood.this.findViewById(R.id.blood_type1)).getSelectedItemId()];
                String result = "";
                if (b1 == "A" && b2 == "B") {
                    result = " A 型、B 型、AB 型、O 型";
                }
                if (b1 == "B" && b2 == "A") {
                    result = "A 型、B 型、AB 型、O 型";
                }
                if (b1 == "A" && b2 == "A") {
                    result = "A 型或 O 型，不可能为 B 型 和 AB 型";
                }
                if (b1 == "A" && b2 == "O") {
                    result = "A 型或 O 型，不可能为 B 型 和 AB 型";
                }
                if (b1 == "O" && b2 == "A") {
                    result = "A 型或 O 型，不可能为 B 型 和 AB 型";
                }
                if (b1 == "A" && b2 == "AB") {
                    result = "A 型 、B型 及 AB型之一，不可能为 O 型";
                }
                if (b1 == "AB" && b2 == "A") {
                    result = "A 型 、B型 及 AB型之一，不可能为 O 型";
                }
                if (b1 == "B" && b2 == "B") {
                    result = "B 型或 O 型，不可能为 A 型 和 AB 型";
                }
                if (b1 == "B" && b2 == "O") {
                    result = "B 型或 O 型，不可能为 A 型 和 AB 型";
                }
                if (b1 == "O" && b2 == "B") {
                    result = "B 型或 O 型，不可能为 A 型 和 AB 型";
                }
                if (b1 == "B" && b2 == "AB") {
                    result = "A 型 、B型 及 AB型之一，不可能为 O 型";
                }
                if (b1 == "AB" && b2 == "B") {
                    result = "A 型 、B型 及 AB型之一，不可能为 O 型";
                }
                if (b1 == "O" && b2 == "O") {
                    result = "O 型，不可能为 A 型、B 型和 AB 型";
                }
                if (b1 == "O" && b2 == "AB") {
                    result = "A 型或 B 型，不可能为 O 型 和 AB 型";
                }
                if (b1 == "AB" && b2 == "O") {
                    result = "A 型或 B 型，不可能为 O 型 和 AB 型";
                }
                if (b1 == "AB" && b2 == "AB") {
                    result = "A 型 、B型 及 AB型之一，不可能为 O 型";
                }
                new AlertDialog.Builder(blood.this).setTitle((int) R.string.blood_app).setMessage(String.valueOf(blood.this.context.getResources().getString(R.string.blood_result)) + result).setPositiveButton((int) R.string.back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
            }
        });
        this.ads = new Ads(this);
        this.ads.getAd().getWb();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }
}
