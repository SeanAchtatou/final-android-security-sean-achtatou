package com.x25.apps.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.mobclick.android.MobclickAgent;

public class height extends Activity {
    private Ads ads;
    public int baby_sex = 0;
    public Context context;
    public RadioGroup sex;
    public RadioButton sex_f;
    public RadioButton sex_m;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        this.context = this;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.height);
        this.sex = (RadioGroup) findViewById(R.id.baby_sex);
        this.sex_m = (RadioButton) findViewById(R.id.sex_m);
        this.sex_f = (RadioButton) findViewById(R.id.sex_f);
        this.sex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == height.this.sex_m.getId()) {
                    height.this.baby_sex = 0;
                } else if (checkedId == height.this.sex_f.getId()) {
                    height.this.baby_sex = 1;
                }
            }
        });
        ((Button) findViewById(R.id.height_go)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int baby_height;
                String father_height = ((EditText) height.this.findViewById(R.id.height_father)).getText().toString();
                String mother_height = ((EditText) height.this.findViewById(R.id.height_mother)).getText().toString();
                if (father_height.length() < 1 || mother_height.length() < 1) {
                    Toast.makeText(height.this.context, (int) R.string.height_input_tips, 5000).show();
                    return;
                }
                int fh = Integer.valueOf(father_height).intValue();
                int mh = Integer.valueOf(mother_height).intValue();
                if (height.this.baby_sex == 0) {
                    baby_height = ((fh + mh) + 13) / 2;
                } else {
                    baby_height = ((fh + mh) - 13) / 2;
                }
                new AlertDialog.Builder(height.this).setTitle((int) R.string.height_app).setMessage(String.valueOf(height.this.context.getResources().getString(R.string.height_result)) + baby_height + height.this.context.getResources().getString(R.string.height_unit)).setPositiveButton((int) R.string.back, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                    }
                }).show();
            }
        });
        this.ads = new Ads(this);
        this.ads.getAd().getWb();
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.ads.finalize();
    }
}
