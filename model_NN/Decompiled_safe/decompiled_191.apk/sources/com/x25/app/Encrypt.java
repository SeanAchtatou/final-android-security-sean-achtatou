package com.x25.app;

import java.math.BigInteger;
import java.security.MessageDigest;

public class Encrypt {
    static String md5(String paramString) {
        if (paramString == null || paramString.length() <= 0) {
            return null;
        }
        try {
            MessageDigest localMessageDigest = MessageDigest.getInstance("MD5");
            localMessageDigest.update(paramString.getBytes(), 0, paramString.length());
            return String.format("%032x", new BigInteger(1, localMessageDigest.digest()));
        } catch (Exception e) {
            return null;
        }
    }

    /* JADX INFO: Multiple debug info for r9v1 java.lang.String: [D('paramString2' java.lang.String), D('str1' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r9v12 int: [D('str3' java.lang.String), D('n' int)] */
    /* JADX INFO: Multiple debug info for r9v13 char: [D('n' int), D('i1' int)] */
    /* JADX INFO: Multiple debug info for r9v14 int: [D('i1' int), D('i2' int)] */
    /* JADX INFO: Multiple debug info for r9v15 char: [D('i2' int), D('c1' char)] */
    /* JADX INFO: Multiple debug info for r9v16 int: [D('c1' char), D('i' int)] */
    static String hash(String paramString1, String paramString2) {
        StringBuffer localStringBuffer1 = new StringBuffer();
        StringBuffer localStringBuffer2 = new StringBuffer();
        try {
            String str1 = md5(paramString2);
            String str2 = String.valueOf(md5(str1.substring(12))) + md5(str1.substring(0, 20));
            int j = str2.length();
            int k = paramString1.length();
            int m = 0;
            int m2 = 0;
            while (m < k) {
                localStringBuffer2.delete(0, localStringBuffer2.length());
                localStringBuffer2.append(paramString1.charAt(m));
                localStringBuffer2.append(paramString1.charAt(m + 1));
                localStringBuffer1.append((char) (((char) Integer.valueOf(localStringBuffer2.toString(), 16).intValue()) ^ str2.charAt(m2)));
                m += 2;
                m2 = (m2 + 1) % j;
            }
        } catch (Exception e) {
        }
        return localStringBuffer1.toString();
    }
}
