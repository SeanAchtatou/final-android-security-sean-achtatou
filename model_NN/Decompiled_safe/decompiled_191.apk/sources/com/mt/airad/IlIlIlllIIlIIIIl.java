package com.mt.airad;

import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import com.mobclick.android.ReportPolicy;
import com.waps.AnimationType;

class IlIlIlllIIlIIIIl {
    protected static final long _$1 = 1000;
    private Animation _$2;
    private Animation _$3;

    protected IlIlIlllIIlIIIIl(int i, int i2) {
        _$1(i, i2);
    }

    private void _$1(int i, int i2) {
        switch (i2) {
            case 1:
            case AirAD.ANIMATION_FIXED:
                if (i == 11) {
                    this._$3 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                    this._$2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
                    this._$2.setInterpolator(new AccelerateInterpolator(4.0f));
                    this._$3.setInterpolator(new AccelerateDecelerateInterpolator());
                    return;
                }
                this._$3 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                this._$2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
                this._$2.setInterpolator(new AccelerateInterpolator(4.0f));
                this._$3.setInterpolator(new AccelerateDecelerateInterpolator());
                return;
            case ReportPolicy.BATCH_AT_TERMINATE:
                this._$3 = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                this._$2 = new TranslateAnimation(1, 0.0f, 1, 1.0f, 1, 0.0f, 1, 0.0f);
                this._$2.setInterpolator(new AccelerateInterpolator(4.0f));
                this._$3.setInterpolator(new AccelerateDecelerateInterpolator());
                return;
            case ReportPolicy.PUSH:
                this._$3 = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                this._$2 = new TranslateAnimation(1, 0.0f, 1, -1.0f, 1, 0.0f, 1, 0.0f);
                this._$2.setInterpolator(new AccelerateInterpolator(4.0f));
                this._$3.setInterpolator(new AccelerateDecelerateInterpolator());
                return;
            case 4:
                if (i == 11) {
                    this._$3 = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 1.0f);
                    this._$2 = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 1.0f);
                } else {
                    this._$3 = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.0f);
                    this._$2 = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.0f);
                }
                this._$2.setInterpolator(new AccelerateInterpolator(4.0f));
                this._$3.setInterpolator(new AccelerateDecelerateInterpolator());
                return;
            case AnimationType.ALPHA /*5*/:
            case AnimationType.TRANSLATE_FROM_RIGHT /*6*/:
            default:
                this._$3 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                this._$2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
                this._$2.setInterpolator(new AccelerateInterpolator(4.0f));
                this._$3.setInterpolator(new AccelerateDecelerateInterpolator());
                return;
            case AnimationType.TRANSLATE_FROM_LEFT /*7*/:
                this._$3 = new AlphaAnimation(0.0f, 1.0f);
                this._$2 = new AlphaAnimation(1.0f, 0.0f);
                this._$2.setInterpolator(new LinearInterpolator());
                this._$3.setInterpolator(new LinearInterpolator());
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void hideAction(View view) {
        if (view.getVisibility() == 0) {
            this._$2.setDuration(_$1);
            view.startAnimation(this._$2);
            view.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void showAction(View view) {
        if (view.getVisibility() == 8) {
            view.setVisibility(0);
            this._$3.setDuration(_$1);
            view.startAnimation(this._$3);
        }
    }
}
