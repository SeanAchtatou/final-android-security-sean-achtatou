package com.mt.airad;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.HttpAuthHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class IlllIllIlllIIIII extends WebViewClient {
    final /* synthetic */ MultiAD _$1;

    IlllIllIlllIIIII(MultiAD multiAD) {
        this._$1 = multiAD;
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this._$1._$4();
        boolean unused = this._$1._$7 = false;
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        boolean unused = this._$1._$7 = true;
    }

    public void onReceivedHttpAuthRequest(WebView webView, HttpAuthHandler httpAuthHandler, String str, String str2) {
        super.onReceivedHttpAuthRequest(webView, httpAuthHandler, str, str2);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.startsWith("mailto:") || str.startsWith("geo:") || str.startsWith("tel:")) {
            this._$1.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return super.shouldOverrideUrlLoading(webView, str);
        }
        webView.loadUrl(str);
        return true;
    }
}
