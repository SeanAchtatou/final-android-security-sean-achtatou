package com.vpon.adon.android.webClientHandler;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.vpon.adon.android.entity.Ad;
import java.net.URLDecoder;

public class SmsHandler extends WebClientHandler {
    public SmsHandler(WebClientHandler next) {
        super(next);
    }

    public boolean handle(Context context, Ad ad, String url) {
        if (!url.startsWith("sms://")) {
            return doNext(context, ad, url);
        }
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse(url));
        String[] smsInfo = url.split("body=");
        if (smsInfo.length >= 2) {
            Uri uri = Uri.parse(smsInfo[0]);
            String smsRecevicer = smsInfo[0].split("//")[1];
            intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + smsRecevicer.substring(0, smsRecevicer.length() - 1)));
            intent.putExtra("sms_body", URLDecoder.decode(smsInfo[1]));
        }
        context.startActivity(intent);
        return true;
    }
}
