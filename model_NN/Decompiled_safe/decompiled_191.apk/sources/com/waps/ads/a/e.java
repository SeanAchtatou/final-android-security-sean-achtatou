package com.waps.ads.a;

import android.util.Log;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.b.c;

public class e extends a {
    public e(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    public void handle() {
        Log.d("AdGroup_SDK", "Event notification request initiated");
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            if (adGroupLayout.i != null) {
                String str = this.d.e;
                if (str == null) {
                    Log.w("AdGroup_SDK", "Event key is null");
                    adGroupLayout.rollover();
                    return;
                }
                int indexOf = str.indexOf("|;|");
                if (indexOf < 0) {
                    Log.w("AdGroup_SDK", "Event key separator not found");
                    adGroupLayout.rollover();
                    return;
                }
                try {
                    adGroupLayout.i.getClass().getMethod(str.substring(indexOf + 3), null).invoke(adGroupLayout.i, null);
                } catch (Exception e) {
                    Log.e("AdGroup_SDK", "Caught exception in handle()", e);
                    adGroupLayout.rollover();
                }
            } else {
                Log.w("AdGroup_SDK", "Event notification would be sent, but no interface is listening");
                adGroupLayout.rollover();
            }
        }
    }
}
