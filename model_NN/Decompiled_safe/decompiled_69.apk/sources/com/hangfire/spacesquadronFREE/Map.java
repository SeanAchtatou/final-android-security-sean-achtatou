package com.hangfire.spacesquadronFREE;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceHolder;

public final class Map {
    public static final int CLOSE_FIELD_SIZE = 5;
    public static final int FAR_FIELD_SIZE = 30;
    public static final int MAP_BORDER_WIDTH = 150;
    public static final int MEDIUM_FIELD_SIZE = 12;
    private Paint borderPaint;
    private float fltOldScreenX;
    private float fltOldScreenY;
    private Bitmap mapImage;
    private float maxMapImageX;
    private float maxMapImageY;
    private float maxScreenX;
    private float maxScreenY;
    private int maxStarFieldX;
    private int maxStarFieldY;
    private Bitmap rawMapImage;
    private int screenHeight;
    private int screenWidth;
    private float[] starField;
    private int starFieldSize;
    private Paint starPaint;
    public int worldSizeX;
    public int worldSizeY;

    public Map(Resources res, int imageID, int sizeX, int sizeY) throws Exception {
        try {
            this.rawMapImage = BitmapFactory.decodeResource(res, imageID);
            this.worldSizeX = sizeX;
            this.worldSizeY = sizeY;
            setScreenSize(1, 1);
            this.starFieldSize = 47;
            this.starField = new float[(this.starFieldSize * 3)];
            this.starPaint = new Paint();
            this.starPaint.setARGB(255, 200, 200, 200);
            this.borderPaint = new Paint();
            this.borderPaint.setARGB(40, 255, 0, 0);
            generateStarFields();
        } catch (Exception e) {
            throw e;
        }
    }

    public void setScreenSize(int width, int height) throws Exception {
        try {
            this.screenWidth = width;
            this.screenHeight = height;
            this.maxStarFieldX = this.screenWidth + 100;
            this.maxStarFieldY = this.screenHeight + 100;
            if (this.rawMapImage != null) {
                float imgRatio = ((float) this.rawMapImage.getWidth()) / ((float) this.rawMapImage.getHeight());
                int imageWidth = (int) (((double) width) * 1.5d);
                int imageHeight = (int) (((double) height) * 1.5d);
                float calcRatio = ((float) imageWidth) / ((float) imageHeight);
                if (imgRatio != calcRatio) {
                    if (calcRatio > imgRatio) {
                        imageHeight = (int) (((float) (imageWidth * 1)) / imgRatio);
                    } else {
                        imageWidth = (int) (((float) imageHeight) * imgRatio);
                    }
                }
                this.mapImage = Bitmap.createScaledBitmap(this.rawMapImage, imageWidth, imageHeight, true);
                this.maxScreenX = (float) (this.worldSizeX - width);
                this.maxScreenY = (float) (this.worldSizeY - height);
                this.maxMapImageX = (float) (width - imageWidth);
                this.maxMapImageY = (float) (height - imageHeight);
                if (this.maxMapImageX < this.maxMapImageY) {
                    this.maxMapImageX = this.maxMapImageY;
                } else {
                    this.maxMapImageY = this.maxMapImageX;
                }
                this.maxMapImageY = -this.maxMapImageY;
                generateStarFields();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void generateStarFields() throws Exception {
        float multiplier;
        int i = 0;
        while (i < this.starFieldSize * 3) {
            try {
                if (i < 90) {
                    multiplier = (float) ((Math.random() * 0.2d) + 0.1d);
                } else if (i < 36) {
                    multiplier = (float) (0.3d + (Math.random() * 0.2d));
                } else {
                    multiplier = (float) (0.5d + (Math.random() * 0.1d));
                }
                this.starField[i] = (float) ((Math.random() * ((double) (this.screenWidth + 200))) - 0.044921875d);
                this.starField[i + 1] = (float) ((Math.random() * ((double) (this.screenHeight + 200))) - 0.044921875d);
                this.starField[i + 2] = multiplier;
                i += 3;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    private void drawStarField(Canvas canvas, Screen screen) throws Exception {
        try {
            float moveX = ((float) screen.fltScreenX) - this.fltOldScreenX;
            float moveY = this.fltOldScreenY - ((float) screen.fltScreenY);
            if (!(moveX == 0.0f && moveY == 0.0f)) {
                for (int i = 0; i < this.starFieldSize * 3; i += 3) {
                    float x = this.starField[i];
                    float y = this.starField[i + 1];
                    float dist = this.starField[i + 2];
                    float x2 = x - (moveX * dist);
                    float y2 = y - (moveY * dist);
                    if (x2 < -100.0f) {
                        x2 = 200.0f + ((float) this.screenWidth) + x2;
                        y2 = ((float) (Math.random() * ((double) (this.screenHeight + 200)))) - 0.044921875f;
                    } else if (x2 > ((float) this.maxStarFieldX)) {
                        x2 = (x2 - ((float) this.screenWidth)) - 200.0f;
                        y2 = ((float) (Math.random() * ((double) (this.screenHeight + 200)))) - 0.044921875f;
                    } else if (y2 < -100.0f) {
                        y2 = ((float) this.screenHeight) + y2 + 200.0f;
                        x2 = ((float) (Math.random() * ((double) (this.screenWidth + 200)))) - 0.044921875f;
                    } else if (y2 > ((float) this.maxStarFieldY)) {
                        y2 = (y2 - ((float) this.screenHeight)) - 200.0f;
                        x2 = ((float) (Math.random() * ((double) (this.screenWidth + 200)))) - 0.044921875f;
                    }
                    this.starField[i] = x2;
                    this.starField[i + 1] = y2;
                }
            }
            int bigBoys = (this.starFieldSize * 3) - 15;
            for (int i2 = 0; i2 < this.starFieldSize * 3; i2 += 3) {
                float x3 = this.starField[i2];
                float y3 = this.starField[i2 + 1];
                if (x3 >= 0.0f && x3 < ((float) this.screenWidth) && y3 >= 0.0f && y3 < ((float) this.screenHeight)) {
                    if (i2 < bigBoys) {
                        canvas.drawPoint(x3, y3, this.starPaint);
                    } else {
                        canvas.drawRect(x3, y3, x3 + 2.0f, y3 + 2.0f, this.starPaint);
                    }
                }
            }
            this.fltOldScreenX = (float) screen.fltScreenX;
            this.fltOldScreenY = (float) screen.fltScreenY;
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawBorders(Canvas canvas, Screen screen) throws Exception {
        int minX = 0;
        try {
            int maxX = screen.intScreenWidth;
            int zoomModifier = 2;
            if (screen.zoomedIn) {
                zoomModifier = 1;
            }
            if (screen.intScreenX < 150) {
                minX = (150 - screen.intScreenX) / zoomModifier;
                canvas.drawRect(0.0f, 0.0f, (float) minX, (float) screen.intScreenHeight, this.borderPaint);
            } else if (screen.intScreenX + (screen.intScreenWidth * zoomModifier) > this.worldSizeX - 150) {
                maxX = ((this.worldSizeX - screen.intScreenX) - 150) / zoomModifier;
                canvas.drawRect((float) maxX, 0.0f, (float) screen.intScreenWidth, (float) screen.intScreenHeight, this.borderPaint);
            }
            if (screen.intScreenY < 150) {
                canvas.drawRect((float) minX, (float) (screen.intScreenHeight - ((150 - screen.intScreenY) / zoomModifier)), (float) maxX, (float) screen.intScreenHeight, this.borderPaint);
            } else if (screen.intScreenY + (screen.intScreenHeight * zoomModifier) > this.worldSizeY - 150) {
                canvas.drawRect((float) minX, 0.0f, (float) maxX, (float) ((((150 - this.worldSizeY) + screen.intScreenY) + (screen.intScreenHeight * zoomModifier)) / zoomModifier), this.borderPaint);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void drawMap(Canvas canvas, SurfaceHolder surfaceHolder, Screen screen) throws Exception {
        try {
            synchronized (surfaceHolder) {
                canvas.drawBitmap(this.mapImage, (float) ((int) ((((float) screen.intScreenX) / this.maxScreenX) * this.maxMapImageX)), (float) ((int) ((-this.maxMapImageY) + ((((float) screen.intScreenY) / this.maxScreenY) * this.maxMapImageY))), (Paint) null);
                drawStarField(canvas, screen);
                drawBorders(canvas, screen);
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
