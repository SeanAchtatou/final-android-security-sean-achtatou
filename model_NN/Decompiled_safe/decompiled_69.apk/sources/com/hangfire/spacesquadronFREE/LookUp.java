package com.hangfire.spacesquadronFREE;

public final class LookUp {
    private static final float[] cosValues = getCos();
    private static final float[] sinValues = getSin();
    private static final float[] sqrtValues = getSqrt();

    private static final float[] getSin() {
        float[] sineValues = new float[3600];
        for (int i = 0; i < 3600; i++) {
            sineValues[i] = (float) Math.sin((double) Functions.toRad(((float) i) / 10.0f));
        }
        return sineValues;
    }

    private static final float[] getCos() {
        float[] cosineValues = new float[3600];
        for (int i = 0; i < 3600; i++) {
            cosineValues[i] = (float) Math.cos((double) Functions.toRad(((float) i) / 10.0f));
        }
        return cosineValues;
    }

    private static final float[] getSqrt() {
        float[] squareRootValues = new float[1000];
        for (int i = 0; i < 1000; i++) {
            squareRootValues[i] = (float) Math.sqrt((double) (((float) i) / 5.0f));
        }
        return squareRootValues;
    }

    public static final float sin(float ang) throws Exception {
        try {
            return sinValues[(int) Math.floor((double) (10.0f * Functions.wrapAngle(ang)))];
        } catch (Exception e) {
            throw e;
        }
    }

    public static final float cos(float ang) throws Exception {
        try {
            return cosValues[(int) Math.floor((double) (10.0f * Functions.wrapAngle(ang)))];
        } catch (Exception e) {
            throw e;
        }
    }

    public static final float sqrt(float val) throws Exception {
        try {
            int i = (int) Math.floor((double) (5.0f * val));
            if (i < 0) {
                return 0.0f;
            }
            if (i > 999) {
                return (float) Math.sqrt((double) val);
            }
            return sqrtValues[i];
        } catch (Exception e) {
            throw e;
        }
    }
}
