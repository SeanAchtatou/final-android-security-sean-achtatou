package com.hangfire.spacesquadronFREE;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public final class Emitter {
    private static final int EMITTER_CLIP_RANGE = 100;
    public static final int FLAME_EMITTER = 0;
    private float alphaDec = 0.0f;
    private float alphaRnd = 0.0f;
    private float alphaStart = 0.0f;
    private float ang;
    private float emitAngRnd = 0.0f;
    private int emitCount = 0;
    public int emitDelay = 0;
    private Paint emitPaint;
    private float emitSpeed = 0.0f;
    public boolean emitterOn = false;
    private int halfRadius;
    private int maxParticles = 0;
    private int nextIndex = 0;
    private EmitterParticle[] particle;
    private Bitmap particleImage;
    private int radius;
    private double x;
    private float xspd;
    private double y;
    private float yspd;

    private class EmitterParticle {
        public float alpha;
        public double x;
        public float xspd;
        public double y;
        public float yspd;

        private EmitterParticle() {
            this.x = 0.0d;
            this.y = 0.0d;
            this.xspd = 0.0f;
            this.yspd = 0.0f;
            this.alpha = 0.0f;
        }

        /* synthetic */ EmitterParticle(Emitter emitter, EmitterParticle emitterParticle) {
            this();
        }
    }

    public Emitter(int type) throws Exception {
        try {
            this.emitPaint = new Paint();
            switch (type) {
                case 0:
                    this.maxParticles = 10;
                    this.emitSpeed = 2.0f;
                    this.emitAngRnd = 5.0f;
                    this.alphaStart = 200.0f;
                    this.alphaDec = 10.0f;
                    this.alphaRnd = 50.0f;
                    this.emitDelay = 0;
                    this.radius = 16;
                    this.halfRadius = 8;
                    break;
            }
            this.particle = new EmitterParticle[this.maxParticles];
            for (int i = 0; i < this.maxParticles; i++) {
                this.particle[i] = new EmitterParticle(this, null);
            }
            this.nextIndex = 1;
        } catch (Exception e) {
            throw e;
        }
    }

    public void process(double px, double py, float pang, float pxspd, float pyspd) throws Exception {
        try {
            this.x = px;
            this.y = py;
            this.ang = pang;
            this.xspd = pxspd;
            this.yspd = pyspd;
            if (this.emitterOn) {
                this.emitCount--;
                if (this.emitCount <= 0) {
                    this.emitCount = this.emitDelay;
                    float partAng = Functions.wrapAngle((float) (((double) (this.ang - this.emitAngRnd)) + (Math.random() * ((double) this.emitAngRnd) * 2.0d)));
                    EmitterParticle part = this.particle[this.nextIndex];
                    part.xspd = this.xspd + (LookUp.sin(partAng) * this.emitSpeed);
                    part.yspd = this.yspd + (LookUp.cos(partAng) * this.emitSpeed);
                    part.x = this.x;
                    part.y = this.y;
                    part.alpha = this.alphaStart + ((float) ((int) (Math.random() * ((double) this.alphaRnd))));
                    this.nextIndex++;
                    if (this.nextIndex >= this.maxParticles) {
                        this.nextIndex = 0;
                    }
                }
            }
            for (int i = 0; i < this.maxParticles; i++) {
                EmitterParticle part2 = this.particle[i];
                if (part2.alpha > 0.0f) {
                    part2.alpha -= this.alphaDec;
                    if (part2.alpha < 0.0f) {
                        part2.alpha = 0.0f;
                    }
                    part2.x += (double) part2.xspd;
                    part2.y += (double) part2.yspd;
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void draw(Canvas canvas, Screen screen) throws Exception {
        int rad;
        try {
            if (screen.inScreenWorldValues(this.x, this.y, EMITTER_CLIP_RANGE, EMITTER_CLIP_RANGE)) {
                for (int i = 0; i < this.maxParticles; i++) {
                    EmitterParticle part = this.particle[i];
                    if (part.alpha > 0.0f) {
                        if (screen.zoomedIn) {
                            rad = this.radius;
                        } else {
                            rad = this.halfRadius;
                        }
                        if (screen.inScreenWorldValues(part.x, part.y, rad, rad)) {
                            float screenX = screen.canvasX(part.x);
                            float screenY = screen.canvasY(part.y);
                            this.emitPaint.setAlpha((int) part.alpha);
                            canvas.drawBitmap(this.particleImage, screenX - ((float) rad), screenY - ((float) rad), this.emitPaint);
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
