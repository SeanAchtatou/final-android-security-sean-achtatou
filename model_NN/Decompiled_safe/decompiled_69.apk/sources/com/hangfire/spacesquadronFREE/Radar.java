package com.hangfire.spacesquadronFREE;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;

public final class Radar {
    private static final int BUTTON_CLOSE = 0;
    private int bottom;
    public float dragX = -1.0f;
    public float dragY = -1.0f;
    private int drawHeight;
    private float drawRatio;
    private int drawWidth;
    private int left;
    private boolean mEndTurnPressed = false;
    private int maxWorldX;
    private int maxWorldY;
    public MenuButton mbClose;
    private Paint radPaint;
    public boolean radarMode = false;
    private int right;
    private Paint screenPaint;
    private double startScreenX;
    private double startScreenY;
    private int top;
    private Paint unitPaint;

    public Radar(Resources res, int mX, int mY) throws Exception {
        try {
            this.maxWorldX = mX;
            this.maxWorldY = mY;
            this.radPaint = new Paint();
            this.radPaint.setARGB(200, 50, 50, 100);
            this.unitPaint = new Paint();
            this.screenPaint = new Paint();
            this.screenPaint.setStyle(Paint.Style.STROKE);
            this.screenPaint.setARGB(255, 255, 255, 200);
            this.mbClose = new MenuButton(0, 0, false, false);
            this.mbClose.setData(0, 0, 100, 100, "X", 10, 0);
            setScreenSize(100, 100);
        } catch (Exception e) {
            throw e;
        }
    }

    public void setScreenSize(int width, int height) throws Exception {
        int smallButtonWidth;
        try {
            this.drawWidth = width;
            this.drawRatio = ((float) width) / ((float) this.maxWorldX);
            this.drawHeight = (int) (((float) this.maxWorldY) * this.drawRatio);
            if (this.drawHeight > height) {
                this.drawHeight = height;
                this.drawRatio = ((float) height) / ((float) this.maxWorldY);
                this.drawWidth = (int) (((float) this.maxWorldX) * this.drawRatio);
            }
            this.left = (width / 2) - (this.drawWidth / 2);
            this.right = this.left + this.drawWidth;
            this.bottom = (height / 2) + (this.drawHeight / 2);
            this.top = (height / 2) - (this.drawHeight / 2);
            if (height > width) {
                smallButtonWidth = width / 6;
            } else {
                smallButtonWidth = height / 6;
            }
            this.mbClose.setDimensions(width - smallButtonWidth, 0, width, smallButtonWidth, smallButtonWidth / 3);
        } catch (Exception e) {
            throw e;
        }
    }

    public void touchDown(float x, float y, Screen screen, Timer timer) throws Exception {
        try {
            this.mEndTurnPressed = false;
            if (this.mbClose.press(x, y, true)) {
                return;
            }
            if (onEndTurnButton(x, y, screen)) {
                this.mEndTurnPressed = true;
            } else {
                startRadarDrag(x, y, screen, timer);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private boolean onEndTurnButton(float x, float y, Screen screen) throws Exception {
        try {
            if (x <= ((float) (screen.intScreenWidth - 60)) || y <= ((float) (screen.intScreenHeight - 60))) {
                return false;
            }
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public void startRadarDrag(float x, float y, Screen screen, Timer timer) throws Exception {
        try {
            this.dragX = x;
            this.dragY = y;
            this.startScreenX = screen.fltScreenX;
            this.startScreenY = screen.fltScreenY;
        } catch (Exception e) {
            throw e;
        }
    }

    public void touchUp(float x, float y, Screen screen, Timer timer) throws Exception {
        try {
            if (this.radarMode) {
                if (this.mbClose.pressed && this.mbClose.press(x, y, false)) {
                    this.radarMode = false;
                }
                if (onEndTurnButton(x, y, screen) && this.mEndTurnPressed) {
                    timer.endTurn();
                }
            }
            this.dragX = -1.0f;
            this.dragY = -1.0f;
            this.mEndTurnPressed = false;
        } catch (Exception e) {
            throw e;
        }
    }

    public void dragRadar(float x, float y, Screen screen) throws Exception {
        try {
            if (this.dragX != -1.0f || this.dragY != -1.0f) {
                screen.positionCamera(this.startScreenX - ((double) ((this.dragX - x) / this.drawRatio)), this.startScreenY - ((double) ((y - this.dragY) / this.drawRatio)));
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void draw(Canvas canvas, Screen screen, Units units, Asteroids asteroids, EndTurnButton endTurnButton, Timer timer, BitmapFontDrawer textDrawer) throws Exception {
        try {
            canvas.drawRect((float) this.left, (float) this.top, (float) this.right, (float) this.bottom, this.radPaint);
            int x = (int) (((float) this.left) + (((float) screen.intScreenX) * this.drawRatio));
            int y = (int) (((float) this.bottom) - (((float) screen.intScreenY) * this.drawRatio));
            Canvas canvas2 = canvas;
            canvas2.drawRect((float) x, (float) ((int) (((float) this.bottom) - (((float) screen.intScreenTop) * this.drawRatio))), (float) ((int) (((float) this.left) + (((float) screen.intScreenRight) * this.drawRatio))), (float) y, this.screenPaint);
            for (int i = 0; i < units.unit.size(); i++) {
                Unit u = units.unit.get(i);
                if (u.inGameZone() && u.getVisibleToPlayer()) {
                    int x2 = (int) (((double) this.left) + (u.fltWorldX * ((double) this.drawRatio)));
                    int y2 = (int) (((double) this.bottom) - (u.fltWorldY * ((double) this.drawRatio)));
                    if (u.team == 0) {
                        if (u == units.unitPlayer) {
                            this.unitPaint.setARGB(255, 100, 255, 100);
                            canvas.drawRect((float) (x2 - 2), (float) (y2 - 2), (float) (x2 + 2), (float) (y2 + 2), this.unitPaint);
                        } else {
                            this.unitPaint.setARGB(255, 0, 255, 0);
                            canvas.drawRect((float) (x2 - 1), (float) (y2 - 1), (float) (x2 + 2), (float) (y2 + 2), this.unitPaint);
                        }
                        if (u.warning) {
                            this.unitPaint.setARGB(Math.abs(units.hazardPulse), 255, 255, 0);
                            canvas.drawRect((float) (x2 - 3), (float) (y2 - 3), (float) (x2 + 4), (float) (y2 + 4), this.unitPaint);
                        }
                    } else {
                        this.unitPaint.setARGB(255, 255, 0, 0);
                        canvas.drawRect((float) (x2 - 1), (float) (y2 - 1), (float) (x2 + 2), (float) (y2 + 2), this.unitPaint);
                    }
                }
            }
            this.unitPaint.setARGB(255, 0, 0, 0);
            for (int i2 = 0; i2 < asteroids.asteroid.size(); i2++) {
                Asteroid a = asteroids.asteroid.get(i2);
                int x3 = (int) (((double) this.left) + (a.dblWorldX * ((double) this.drawRatio)));
                int y3 = (int) (((double) this.bottom) - (a.dblWorldY * ((double) this.drawRatio)));
                canvas.drawRect((float) (x3 - 2), (float) (y3 - 2), (float) (x3 + 2), (float) (y3 + 2), this.unitPaint);
            }
            this.mbClose.setVisible(true);
            this.mbClose.draw(canvas, textDrawer);
            endTurnButton.drawEndTurn(canvas, screen, timer);
        } catch (Exception e) {
            throw e;
        }
    }
}
