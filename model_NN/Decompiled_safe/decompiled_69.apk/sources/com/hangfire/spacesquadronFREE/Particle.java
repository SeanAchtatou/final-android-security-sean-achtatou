package com.hangfire.spacesquadronFREE;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public final class Particle {
    private int alpha;
    private int alphadec;
    private float frame = 0.0f;
    private float frameinc;
    private int frames;
    private float friction;
    private int halfHeight;
    private int halfWidth;
    private int quarterHeight;
    private int quarterWidth;
    public int type;
    private double x;
    private float xspd;
    private double y;
    private float yspd;

    public Particle(int t, double xpos, double ypos, float xs, float ys, int hWidth, int hHeight, float fric, int al, int aldec, int fr, float frinc) throws Exception {
        try {
            this.x = xpos;
            this.y = ypos;
            this.xspd = xs;
            this.yspd = ys;
            this.halfHeight = hHeight;
            this.halfWidth = hWidth;
            this.quarterWidth = this.halfWidth / 2;
            this.quarterHeight = this.halfHeight / 2;
            this.friction = fric;
            this.alpha = al;
            this.alphadec = aldec;
            this.type = t;
            this.frames = fr;
            this.frameinc = frinc;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean processAlive() throws Exception {
        try {
            if (this.alphadec > 0) {
                this.alpha -= this.alphadec;
                if (this.alpha <= 0) {
                    return false;
                }
            }
            if (this.frameinc > 0.0f) {
                this.frame += this.frameinc;
                if (this.frame > ((float) this.frames)) {
                    return false;
                }
            }
            if (this.friction > 0.0f) {
                this.xspd *= this.friction;
                this.yspd *= this.friction;
                this.x += (double) this.xspd;
                this.y += (double) this.yspd;
            }
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public int getFrame() {
        return (int) Math.floor((double) this.frame);
    }

    public void draw(Canvas canvas, Screen screen, Bitmap img, boolean zoomed, Paint paint) throws Exception {
        try {
            int intScreenX = screen.canvasX((int) this.x);
            int intScreenY = screen.canvasY((int) this.y);
            if (screen.inScreenWorldValues(this.x, this.y, this.halfWidth, this.halfHeight)) {
                int a = this.alpha;
                if (a > 255) {
                    a = 255;
                }
                if (this.type == 4) {
                    paint.setARGB(a, 255, 255, a);
                    canvas.drawCircle((float) intScreenX, (float) intScreenY, ((float) this.halfHeight) + (((255.0f - ((float) a)) / 255.0f) * ((float) this.halfHeight)), paint);
                    return;
                }
                paint.setAlpha(a);
                if (zoomed) {
                    canvas.drawBitmap(img, (float) (intScreenX - this.halfWidth), (float) (intScreenY - this.halfHeight), paint);
                } else {
                    canvas.drawBitmap(img, (float) (intScreenX - this.quarterWidth), (float) (intScreenY - this.quarterHeight), paint);
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public String getSaveString() throws Exception {
        try {
            return String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf((int) this.x)) + "B" + String.valueOf((int) this.y)) + "B" + String.valueOf((int) (this.xspd * 1000.0f))) + "B" + String.valueOf((int) (this.yspd * 1000.0f))) + "B" + String.valueOf((int) (this.friction * 1000.0f))) + "B" + String.valueOf(this.halfWidth)) + "B" + String.valueOf(this.halfHeight)) + "B" + String.valueOf(this.quarterWidth)) + "B" + String.valueOf(this.quarterHeight)) + "B" + String.valueOf(this.alpha)) + "B" + String.valueOf(this.alphadec)) + "B" + String.valueOf(this.frames)) + "B" + String.valueOf((int) (this.frame * 1000.0f))) + "B" + String.valueOf((int) (this.frameinc * 1000.0f))) + "B" + String.valueOf(this.type);
        } catch (Exception e) {
            throw e;
        }
    }

    public void restoreFromSaveString(String saveString) throws Exception {
        try {
            String[] data = saveString.split("B");
            int pos = 0 + 1;
            this.x = (double) Integer.parseInt(data[0]);
            int pos2 = pos + 1;
            this.y = (double) Integer.parseInt(data[pos]);
            int pos3 = pos2 + 1;
            this.xspd = ((float) Integer.parseInt(data[pos2])) / 1000.0f;
            int pos4 = pos3 + 1;
            this.yspd = ((float) Integer.parseInt(data[pos3])) / 1000.0f;
            int pos5 = pos4 + 1;
            this.friction = ((float) Integer.parseInt(data[pos4])) / 1000.0f;
            int pos6 = pos5 + 1;
            this.halfWidth = Integer.parseInt(data[pos5]);
            int pos7 = pos6 + 1;
            this.halfHeight = Integer.parseInt(data[pos6]);
            int pos8 = pos7 + 1;
            this.quarterWidth = Integer.parseInt(data[pos7]);
            int pos9 = pos8 + 1;
            this.quarterHeight = Integer.parseInt(data[pos8]);
            int pos10 = pos9 + 1;
            this.alpha = Integer.parseInt(data[pos9]);
            int pos11 = pos10 + 1;
            this.alphadec = Integer.parseInt(data[pos10]);
            int pos12 = pos11 + 1;
            this.frames = Integer.parseInt(data[pos11]);
            int pos13 = pos12 + 1;
            this.frame = ((float) Integer.parseInt(data[pos12])) / 1000.0f;
            int pos14 = pos13 + 1;
            this.frameinc = ((float) Integer.parseInt(data[pos13])) / 1000.0f;
            int i = pos14 + 1;
            this.type = Integer.parseInt(data[pos14]);
        } catch (Exception e) {
            throw e;
        }
    }
}
