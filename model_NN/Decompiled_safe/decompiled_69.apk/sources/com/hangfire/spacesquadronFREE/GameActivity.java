package com.hangfire.spacesquadronFREE;

import android.app.Activity;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

public final class GameActivity extends Activity {
    private boolean mMenuSetup = false;
    private PowerManager mPowerManager;
    private GameThread mThread;
    private GameView mView;
    private PowerManager.WakeLock mWakeLock;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.game_layout);
        this.mPowerManager = (PowerManager) getSystemService("power");
        this.mWakeLock = this.mPowerManager.newWakeLock(26, "Space Squadron");
        this.mWakeLock.acquire();
        this.mView = (GameView) findViewById(R.id.view_game);
        this.mThread = this.mView.getThread();
        this.mThread.initialise(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mWakeLock.acquire();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        prepareToClose();
        super.onPause();
    }

    public void prepareToClose() {
        try {
            if (this.mWakeLock != null && this.mWakeLock.isHeld()) {
                this.mWakeLock.release();
            }
            if (this.mThread != null) {
                this.mThread.stopMusic();
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        prepareToClose();
        super.onDestroy();
    }

    public void close() {
        prepareToClose();
        System.runFinalizersOnExit(true);
        System.exit(0);
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        if (!this.mThread.menuAllowed()) {
            return false;
        }
        if (this.mMenuSetup) {
            return true;
        }
        getMenuInflater().inflate(R.menu.space_mdpi, menu);
        this.mMenuSetup = true;
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        this.mThread.menuItemPressed(item.getItemId());
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        this.mThread.backButtonPressed(true);
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        this.mThread.backButtonPressed(false);
        return true;
    }
}
