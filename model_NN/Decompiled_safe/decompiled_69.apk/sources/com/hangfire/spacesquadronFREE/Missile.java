package com.hangfire.spacesquadronFREE;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

public final class Missile {
    public int AICount = 0;
    public float ang;
    public int damage;
    public int fuel;
    public int halfheight;
    public int halfwidth;
    public int height;
    public int loadOwnerID;
    public int loadTargetID;
    public float maxTurn;
    public Unit owner;
    public int searchArc;
    public int smokeCounter = 2;
    public float speed;
    public Unit target;
    public float targetAng;
    public int type;
    public int width;
    public double x;
    public float xspd;
    public double y;
    public float yspd;

    public Missile(double xs, double ys, float yang, Unit own, float s, float t, int f, int d, int sa, int ty, int lwidth, int lheight) throws Exception {
        try {
            this.x = xs;
            this.y = ys;
            this.ang = yang;
            this.targetAng = this.ang;
            this.owner = own;
            this.speed = s;
            this.maxTurn = t;
            this.fuel = f;
            this.damage = d;
            this.searchArc = sa;
            this.type = ty;
            this.width = lwidth / 2;
            this.height = lheight / 2;
            this.halfwidth = this.width / 2;
            this.halfheight = this.height / 2;
            this.xspd = LookUp.sin(this.ang) * this.speed;
            this.yspd = LookUp.cos(this.ang) * this.speed;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean isGuided() {
        if (this.type == 11 || this.type == 9) {
            return true;
        }
        return false;
    }

    public void move(Particles part, Units units, Asteroids asteroids) throws Exception {
        try {
            if (this.AICount == 0) {
                this.AICount = 5;
                if (this.searchArc <= 0 || this.fuel <= 0) {
                    this.target = null;
                } else {
                    setTargetAng(units, asteroids);
                }
            } else {
                this.AICount--;
            }
            if (this.targetAng != this.ang) {
                float turn = Functions.angDiff(this.targetAng, this.ang);
                if (turn > this.maxTurn) {
                    turn = this.maxTurn;
                } else if (turn < (-this.maxTurn)) {
                    turn = -this.maxTurn;
                }
                this.ang = Functions.wrapAngle(this.ang + turn);
                this.xspd = LookUp.sin(this.ang) * this.speed;
                this.yspd = LookUp.cos(this.ang) * this.speed;
            }
            if (this.fuel > 0) {
                this.fuel--;
            }
            this.x += (double) this.xspd;
            this.y += (double) this.yspd;
            this.smokeCounter--;
            if (this.smokeCounter == 0 && this.fuel > 0) {
                part.makeRocketSmoke(this.x, this.y, this.ang, -5.0f);
                this.smokeCounter = 2;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void draw(Canvas canvas, Screen screen, boolean zoomed, Drawable img) throws Exception {
        try {
            int intScreenX = screen.canvasX((int) this.x);
            int intScreenY = screen.canvasY((int) this.y);
            if (screen.inScreenWorldValues(this.x, this.y, 1, 1)) {
                canvas.save();
                canvas.rotate(this.ang, (float) intScreenX, (float) intScreenY);
                if (zoomed) {
                    img.setBounds(intScreenX - this.width, intScreenY - this.height, this.width + intScreenX, this.height + intScreenY);
                } else {
                    img.setBounds(intScreenX - this.halfwidth, intScreenY - this.halfheight, this.halfwidth + intScreenX, this.halfheight + intScreenY);
                }
                img.draw(canvas);
                canvas.restore();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void setTargetAng(Units units, Asteroids asteroids) throws Exception {
        float closestDist = 1000000.0f;
        try {
            this.targetAng = this.ang;
            this.target = null;
            for (int i = 0; i < units.unit.size(); i++) {
                Unit u = units.unit.get(i);
                if (u.inGameZone() && Functions.inCircle(this.x, this.y, u.fltWorldX, u.fltWorldY, 400)) {
                    float xdist = (float) (u.fltWorldX - this.x);
                    float ydist = (float) (u.fltWorldY - this.y);
                    float currentDist = (xdist * xdist) + (ydist * ydist);
                    if (currentDist < closestDist) {
                        float currentAng = Functions.aTanFull(xdist, ydist);
                        if (Math.abs(Functions.angDiff(this.ang, currentAng)) < ((float) this.searchArc)) {
                            closestDist = currentDist;
                            this.targetAng = currentAng;
                            this.target = u;
                        }
                    }
                }
            }
            for (int i2 = 0; i2 < asteroids.asteroid.size(); i2++) {
                Asteroid a = asteroids.asteroid.get(i2);
                if (Functions.inCircle(this.x, this.y, a.dblWorldX, a.dblWorldY, 400)) {
                    float xdist2 = (float) (a.dblWorldX - this.x);
                    float ydist2 = (float) (a.dblWorldY - this.y);
                    float currentDist2 = (xdist2 * xdist2) + (ydist2 * ydist2);
                    if (currentDist2 < closestDist) {
                        float currentAng2 = Functions.aTanFull(xdist2, ydist2);
                        if (Math.abs(Functions.angDiff(this.ang, currentAng2)) < ((float) this.searchArc)) {
                            closestDist = currentDist2;
                            this.targetAng = currentAng2;
                            this.target = null;
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public String getSaveString(Units units) throws Exception {
        try {
            return String.valueOf(String.valueOf((int) this.x)) + "B" + String.valueOf((int) this.y) + "B" + String.valueOf((int) (this.xspd * 1000.0f)) + "B" + String.valueOf((int) (this.yspd * 1000.0f)) + "B" + String.valueOf((int) this.ang) + "B" + String.valueOf((int) this.targetAng) + "B" + String.valueOf(units.getUnitIndex(this.owner)) + "B" + String.valueOf(this.width) + "B" + String.valueOf(this.height) + "B" + String.valueOf(this.halfwidth) + "B" + String.valueOf(this.halfheight) + "B" + String.valueOf((int) (this.speed * 1000.0f)) + "B" + String.valueOf((int) (this.maxTurn * 1000.0f)) + "B" + String.valueOf(this.fuel) + "B" + String.valueOf(this.damage) + "B" + String.valueOf(this.searchArc) + "B" + String.valueOf(this.AICount) + "B" + String.valueOf(this.type) + "B" + String.valueOf(this.smokeCounter) + "B" + String.valueOf(units.getUnitIndex(this.target));
        } catch (Exception e) {
            throw e;
        }
    }

    public void restoreFromSaveString(String saveString, Units units) throws Exception {
        try {
            String[] data = saveString.split("B");
            int pos = 0 + 1;
            this.x = (double) Integer.parseInt(data[0]);
            int pos2 = pos + 1;
            this.y = (double) Integer.parseInt(data[pos]);
            int pos3 = pos2 + 1;
            this.xspd = ((float) Integer.parseInt(data[pos2])) / 1000.0f;
            int pos4 = pos3 + 1;
            this.yspd = ((float) Integer.parseInt(data[pos3])) / 1000.0f;
            int pos5 = pos4 + 1;
            this.ang = (float) Integer.parseInt(data[pos4]);
            int pos6 = pos5 + 1;
            this.targetAng = (float) Integer.parseInt(data[pos5]);
            int pos7 = pos6 + 1;
            this.loadOwnerID = Integer.parseInt(data[pos6]);
            int pos8 = pos7 + 1;
            this.width = Integer.parseInt(data[pos7]);
            int pos9 = pos8 + 1;
            this.height = Integer.parseInt(data[pos8]);
            int pos10 = pos9 + 1;
            this.halfwidth = Integer.parseInt(data[pos9]);
            int pos11 = pos10 + 1;
            this.halfheight = Integer.parseInt(data[pos10]);
            int pos12 = pos11 + 1;
            this.speed = ((float) Integer.parseInt(data[pos11])) / 1000.0f;
            int pos13 = pos12 + 1;
            this.maxTurn = ((float) Integer.parseInt(data[pos12])) / 1000.0f;
            int pos14 = pos13 + 1;
            this.fuel = Integer.parseInt(data[pos13]);
            int pos15 = pos14 + 1;
            this.damage = Integer.parseInt(data[pos14]);
            int pos16 = pos15 + 1;
            this.searchArc = Integer.parseInt(data[pos15]);
            int pos17 = pos16 + 1;
            this.AICount = Integer.parseInt(data[pos16]);
            int pos18 = pos17 + 1;
            this.type = Integer.parseInt(data[pos17]);
            int pos19 = pos18 + 1;
            this.smokeCounter = Integer.parseInt(data[pos18]);
            int i = pos19 + 1;
            this.loadTargetID = Integer.parseInt(data[pos19]);
        } catch (Exception e) {
            throw e;
        }
    }
}
