package com.hangfire.spacesquadronFREE;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.ArrayList;

public final class Particles {
    private static final float DEBRIS_SPEED_MIN = 2.0f;
    private static final float DEBRIS_SPEED_RANDOM = 4.0f;
    public static final int PARTICLE_DEBRIS = 5;
    public static final int PARTICLE_FLAME = 3;
    public static final int PARTICLE_FLAME_EMIT_SIZE = 16;
    public static final int PARTICLE_LARGEEXPLOSION = 2;
    public static final int PARTICLE_SHOCKWAVE = 4;
    public static final int PARTICLE_SMALLEXPLOSION = 1;
    public static final int PARTICLE_SMOKE = 0;
    private Bitmap imgDebris;
    private Bitmap imgDebris_small;
    private Bitmap imgFlame;
    private Bitmap imgFlame_small;
    private Bitmap imgSmoke;
    private Bitmap imgSmoke_small;
    private Bitmap[] largeExplosion;
    private Bitmap[] largeExplosion_small;
    private int mDebrisSize = 1;
    private int mJetFlameSize = 1;
    private int mLargeExplosionSize = 1;
    private int mSmallExplosionSize = 1;
    private int mSmokeSize = 1;
    private Paint paint;
    public ArrayList<Particle> particle = new ArrayList<>();
    private Bitmap[] smallExplosion;
    private Bitmap[] smallExplosion_small;

    public Particles(Resources res) throws Exception {
        try {
            this.imgSmoke = BitmapFactory.decodeResource(res, R.drawable.smoke);
            this.imgSmoke_small = Functions.getZoomedOutBitmap(res, R.drawable.smoke, 2);
            this.imgFlame = BitmapFactory.decodeResource(res, R.drawable.flame);
            this.imgFlame_small = Functions.getZoomedOutBitmap(res, R.drawable.flame, 2);
            this.imgDebris = BitmapFactory.decodeResource(res, R.drawable.debrislarge);
            this.imgDebris_small = BitmapFactory.decodeResource(res, R.drawable.debrissmall);
            this.smallExplosion = new Bitmap[15];
            this.smallExplosion_small = new Bitmap[15];
            for (int e = 0; e < 15; e++) {
                this.smallExplosion[e] = Functions.getZoomedOutBitmap(res, (e * 2) + R.drawable.explarge01, 2);
                this.smallExplosion_small[e] = Functions.getZoomedOutBitmap(res, (e * 2) + R.drawable.explarge01, 4);
            }
            this.largeExplosion = new Bitmap[30];
            this.largeExplosion_small = new Bitmap[30];
            for (int e2 = 0; e2 < 30; e2++) {
                this.largeExplosion[e2] = BitmapFactory.decodeResource(res, R.drawable.explarge01 + e2);
                this.largeExplosion_small[e2] = Functions.getZoomedOutBitmap(res, R.drawable.explarge01 + e2, 2);
            }
            this.paint = new Paint();
            this.mSmokeSize = this.imgSmoke.getWidth() / 2;
            this.mJetFlameSize = this.imgFlame.getWidth() / 2;
            this.mDebrisSize = this.imgDebris.getWidth() / 2;
            this.mSmallExplosionSize = this.smallExplosion[0].getWidth() / 2;
            this.mLargeExplosionSize = this.largeExplosion[0].getWidth() / 2;
        } catch (Exception e3) {
            throw e3;
        }
    }

    public void makeDebris(double x, double y, int radius) throws Exception {
        try {
            float ang = Functions.wrapAngle(((float) Math.random()) * 360.0f);
            this.particle.add(new Particle(5, x + ((double) (LookUp.sin(ang) * ((float) radius))), y + ((double) (LookUp.cos(ang) * ((float) radius))), (float) (((double) LookUp.sin(ang)) * (2.0d + (Math.random() * 4.0d))), (float) (((double) LookUp.cos(ang)) * (2.0d + (Math.random() * 4.0d))), this.mDebrisSize, this.mDebrisSize, 0.99f, 255, 4, 0, 0.0f));
        } catch (Exception e) {
            throw e;
        }
    }

    public void makeShockWave(double x, double y, int radius) throws Exception {
        try {
            int life = 30 - (radius / 6);
            if (life < 10) {
                life = 10;
            }
            this.particle.add(new Particle(4, x, y, 0.0f, 0.0f, radius, radius, 0.0f, 255, life, 0, 0.0f));
        } catch (Exception e) {
            throw e;
        }
    }

    public void makeRocketSmoke(double x, double y, float ang, float offset) throws Exception {
        try {
            double x2 = x + ((double) (LookUp.sin(Functions.wrapAngle(ang)) * offset));
            this.particle.add(new Particle(0, x2, y + ((double) (LookUp.cos(Functions.wrapAngle(ang)) * offset)), 0.0f, 0.0f, this.mSmokeSize, this.mSmokeSize, 0.0f, 255, 7, 0, 0.0f));
        } catch (Exception e) {
            throw e;
        }
    }

    public void makeJetFlame(double x, double y, float shipSpdX, float shipSpdY, float shipAng, float offset, float emitSpeed, float power) throws Exception {
        try {
            double x2 = x + ((double) (LookUp.sin(Functions.wrapAngle(180.0f + shipAng)) * offset));
            double y2 = y + ((double) (LookUp.cos(Functions.wrapAngle(180.0f + shipAng)) * offset));
            float shipSpdX2 = shipSpdX + (LookUp.sin(Functions.wrapAngle(180.0f + shipAng)) * emitSpeed);
            this.particle.add(new Particle(3, x2, y2, shipSpdX2, shipSpdY + (LookUp.cos(Functions.wrapAngle(180.0f + shipAng)) * emitSpeed), this.mJetFlameSize, this.mJetFlameSize, 1.0f, (int) (60.0f + (195.0f * power)), 15, 0, 0.0f));
        } catch (Exception e) {
            throw e;
        }
    }

    public void makeBulletHitSmoke(double x, double y) throws Exception {
        try {
            this.particle.add(new Particle(0, x, y, 0.0f, 0.0f, this.mSmokeSize, this.mSmokeSize, 0.0f, Units.AI_ZONE_WAIT, 5, 0, 0.0f));
        } catch (Exception e) {
            throw e;
        }
    }

    public void makeSmallExplosion(double x, double y) throws Exception {
        try {
            this.particle.add(new Particle(1, x, y, 0.0f, 0.0f, this.mSmallExplosionSize, this.mSmallExplosionSize, 0.0f, 255, 0, 14, 0.5f));
        } catch (Exception e) {
            throw e;
        }
    }

    public void makeLargeExplosion(double x, double y) throws Exception {
        try {
            this.particle.add(new Particle(2, x, y, 0.0f, 0.0f, this.mLargeExplosionSize, this.mLargeExplosionSize, 0.0f, 255, 0, 29, 0.5f));
        } catch (Exception e) {
            throw e;
        }
    }

    public void processParticles() throws Exception {
        int p = 0;
        while (p < this.particle.size()) {
            try {
                if (!this.particle.get(p).processAlive()) {
                    this.particle.remove(p);
                }
                p++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public void drawParticles(Canvas canvas, Screen screen) throws Exception {
        int p = 0;
        while (p < this.particle.size()) {
            try {
                Particle part = this.particle.get(p);
                switch (part.type) {
                    case 0:
                        if (!screen.zoomedIn) {
                            part.draw(canvas, screen, this.imgSmoke_small, screen.zoomedIn, this.paint);
                            break;
                        } else {
                            part.draw(canvas, screen, this.imgSmoke, screen.zoomedIn, this.paint);
                            break;
                        }
                    case 1:
                        if (!screen.zoomedIn) {
                            part.draw(canvas, screen, this.smallExplosion_small[part.getFrame()], screen.zoomedIn, this.paint);
                            break;
                        } else {
                            part.draw(canvas, screen, this.smallExplosion[part.getFrame()], screen.zoomedIn, this.paint);
                            break;
                        }
                    case 2:
                        if (!screen.zoomedIn) {
                            part.draw(canvas, screen, this.largeExplosion_small[part.getFrame()], screen.zoomedIn, this.paint);
                            break;
                        } else {
                            part.draw(canvas, screen, this.largeExplosion[part.getFrame()], screen.zoomedIn, this.paint);
                            break;
                        }
                    case 3:
                        if (!screen.zoomedIn) {
                            part.draw(canvas, screen, this.imgFlame_small, screen.zoomedIn, this.paint);
                            break;
                        } else {
                            part.draw(canvas, screen, this.imgFlame, screen.zoomedIn, this.paint);
                            break;
                        }
                    case 4:
                        part.draw(canvas, screen, null, screen.zoomedIn, this.paint);
                        break;
                    case 5:
                        if (!screen.zoomedIn) {
                            part.draw(canvas, screen, this.imgDebris_small, screen.zoomedIn, this.paint);
                            break;
                        } else {
                            part.draw(canvas, screen, this.imgDebris, screen.zoomedIn, this.paint);
                            break;
                        }
                }
                p++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public String getSaveString() throws Exception {
        try {
            String saveString = String.valueOf(this.particle.size());
            for (int i = 0; i < this.particle.size(); i++) {
                saveString = String.valueOf(saveString) + "A" + this.particle.get(i).getSaveString();
            }
            return saveString;
        } catch (Exception e) {
            throw e;
        }
    }

    public void restoreFromSaveString(String saveString) throws Exception {
        try {
            this.particle = new ArrayList<>();
            String[] particleData = saveString.split("A");
            int numParticles = Integer.parseInt(particleData[0]);
            for (int i = 0; i < numParticles; i++) {
                this.particle.add(new Particle(0, 0.0d, 0.0d, 0.0f, 0.0f, 0, 0, 0.0f, 0, 0, 0, 0.0f));
                this.particle.get(i).restoreFromSaveString(particleData[i + 1]);
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
