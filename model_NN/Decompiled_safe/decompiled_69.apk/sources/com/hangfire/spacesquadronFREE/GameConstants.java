package com.hangfire.spacesquadronFREE;

public final class GameConstants {
    public static final int ACTIVE_VIEW_NONE = 0;
    public static final int ACTIVE_VIEW_SPACE = 1;
    public static final int COLTYPE_BOX = 1;
    public static final int COLTYPE_CIRCLE = 0;
    public static final float EJECTION_SPEED = 3.0f;
    public static final int SELECTED_NOTHING = 0;
    public static final int SELECTED_UNIT = 1;
    public static final float fltBuildingFriction = 0.5f;
    public static final float fltFriction = 0.95f;
    public static final float fltTurretFriction = 0.9f;
    public static final int intArmourValue = 10;
    public static final int intArrowRadius = 30;
    public static final int intGunButtonWidth = 35;
    public static final int intGunRefire = 10;
    public static final int intInventoryValue = 5;
    public static final int intMissileAICount = 5;
    public static final int intMissileSearchRange = 400;
    public static final int intMissileSmokeCount = 2;
    public static final int intPodSmokeCount = 3;
    public static final int intPressHoldTime = 500;
    public static final int intRocketRefire = 15;
    public static final int intSelectorTime = 500;
    public static final int intUnitRadius = 30;
}
