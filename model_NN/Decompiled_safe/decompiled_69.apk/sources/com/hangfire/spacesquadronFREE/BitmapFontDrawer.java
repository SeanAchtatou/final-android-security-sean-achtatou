package com.hangfire.spacesquadronFREE;

import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public final class BitmapFontDrawer {
    public static final int FONT_FUTURE_BLACK = 0;
    public static final int FONT_FUTURE_WHITE = 1;
    public static final String NEWLINE = "<NEWLINE>";
    private static final int NUM_CACHED_LINES = 50;
    public static final int TEXT_ALIGN_CENTER = 0;
    public static final int TEXT_ALIGN_FITTOSCREEN = 3;
    public static final int TEXT_ALIGN_LEFT = 1;
    public static final int TEXT_ALIGN_RIGHT = 2;
    private String[] fitToScreenCache = new String[50];
    private int fitToScreenCacheLines = 0;
    private String fitToScreenCacheString = "";
    private BitmapFont futureFont;
    private BitmapFont futureFontWhite;
    private int mFinalY = 0;

    public String[] getCacheData() throws Exception {
        try {
            String[] newCache = new String[this.fitToScreenCacheLines];
            for (int i = 0; i < this.fitToScreenCacheLines; i++) {
                newCache[i] = this.fitToScreenCache[i];
            }
            return newCache;
        } catch (Exception e) {
            throw e;
        }
    }

    public BitmapFontDrawer(Resources res) throws Exception {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            this.futureFont = new BitmapFont(BitmapFactory.decodeResource(res, R.drawable.charactermap, options), Color.argb(255, 0, 0, 0));
            this.futureFontWhite = new BitmapFont(BitmapFactory.decodeResource(res, R.drawable.charactermap, options), -1);
            for (int i = 0; i < 50; i++) {
                this.fitToScreenCache[i] = new String();
                this.fitToScreenCache[i] = "";
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockSplitter
        jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x000e in list []
        	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:47)
        	at jadx.core.dex.instructions.SwitchNode.initBlocks(SwitchNode.java:71)
        	at jadx.core.dex.visitors.blocksmaker.BlockSplitter.lambda$initBlocksInTargetNodes$0(BlockSplitter.java:71)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at jadx.core.dex.visitors.blocksmaker.BlockSplitter.initBlocksInTargetNodes(BlockSplitter.java:68)
        	at jadx.core.dex.visitors.blocksmaker.BlockSplitter.visit(BlockSplitter.java:53)
        */
    public void draw(android.graphics.Canvas r17, java.lang.String r18, int r19, int r20, int r21, int r22, int r23, int r24, android.graphics.Paint r25) throws java.lang.Exception {
        /*
            r16 = this;
            if (r18 == 0) goto L_0x0009
            java.lang.String r2 = ""
            r0 = r18
            r1 = r2
            if (r0 != r1) goto L_0x000a
        L_0x0009:
            return
        L_0x000a:
            switch(r21) {
                case 0: goto L_0x000e;
                case 1: goto L_0x0030;
                default: goto L_0x000d;
            }     // Catch:{ Exception -> 0x002d }
        L_0x000d:
            goto L_0x0009     // Catch:{ Exception -> 0x002d }
        L_0x000e:
            r0 = r16     // Catch:{ Exception -> 0x002d }
            com.hangfire.spacesquadronFREE.BitmapFont r0 = r0.futureFont     // Catch:{ Exception -> 0x002d }
            r7 = r0     // Catch:{ Exception -> 0x002d }
        L_0x0013:
            r2 = 3     // Catch:{ Exception -> 0x002d }
            r0 = r23     // Catch:{ Exception -> 0x002d }
            r1 = r2     // Catch:{ Exception -> 0x002d }
            if (r0 != r1) goto L_0x0036     // Catch:{ Exception -> 0x002d }
            r2 = r16     // Catch:{ Exception -> 0x002d }
            r3 = r17     // Catch:{ Exception -> 0x002d }
            r4 = r18     // Catch:{ Exception -> 0x002d }
            r5 = r19     // Catch:{ Exception -> 0x002d }
            r6 = r20     // Catch:{ Exception -> 0x002d }
            r8 = r22     // Catch:{ Exception -> 0x002d }
            r9 = r24     // Catch:{ Exception -> 0x002d }
            r10 = r25     // Catch:{ Exception -> 0x002d }
            r2.fitToScreen(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x002d }
            goto L_0x0009
            r2 = move-exception
            r15 = r2
            throw r15
        L_0x0030:
            r0 = r16
            com.hangfire.spacesquadronFREE.BitmapFont r0 = r0.futureFontWhite     // Catch:{ Exception -> 0x002d }
            r7 = r0     // Catch:{ Exception -> 0x002d }
            goto L_0x0013     // Catch:{ Exception -> 0x002d }
        L_0x0036:
            r8 = r17     // Catch:{ Exception -> 0x002d }
            r9 = r18     // Catch:{ Exception -> 0x002d }
            r10 = r19     // Catch:{ Exception -> 0x002d }
            r11 = r20     // Catch:{ Exception -> 0x002d }
            r12 = r22     // Catch:{ Exception -> 0x002d }
            r13 = r23     // Catch:{ Exception -> 0x002d }
            r14 = r25     // Catch:{ Exception -> 0x002d }
            r7.drawString(r8, r9, r10, r11, r12, r13, r14)     // Catch:{ Exception -> 0x002d }
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: com.hangfire.spacesquadronFREE.BitmapFontDrawer.draw(android.graphics.Canvas, java.lang.String, int, int, int, int, int, int, android.graphics.Paint):void");
    }

    public void draw(Canvas canvas, String text, int x, int y, int font, int size, int align, Paint paint) throws Exception {
        try {
            draw(canvas, text, x, y, font, size, align, canvas.getWidth(), paint);
        } catch (Exception e) {
            throw e;
        }
    }

    public void drawLines(Canvas canvas, String[] text, int x, int y, int font, int size, int width, Paint paint) throws Exception {
        BitmapFont bitmapFont;
        if (text != null) {
            try {
                if (text.length > 0) {
                    switch (font) {
                        case 0:
                            bitmapFont = this.futureFont;
                            break;
                        case 1:
                            bitmapFont = this.futureFontWhite;
                            break;
                        default:
                            return;
                    }
                    for (String drawString : text) {
                        bitmapFont.drawString(canvas, drawString, x, y, size, 0, paint);
                        y += size + 5;
                    }
                    this.mFinalY = y;
                }
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public static final String newLine(int count) {
        String returnString = "";
        for (int i = 1; i < count + 1; i++) {
            returnString = String.valueOf(returnString) + " " + NEWLINE + " ";
        }
        return returnString;
    }

    private void fitToScreen(Canvas canvas, String text, int x, int y, BitmapFont bitmapFont, int size, int width, Paint paint) throws Exception {
        int cachePosition;
        if (text != null && text != "") {
            try {
                if (text == this.fitToScreenCacheString) {
                    for (int i = 0; i < this.fitToScreenCacheLines; i++) {
                        bitmapFont.drawString(canvas, this.fitToScreenCache[i], x, y, size, 0, paint);
                        y += size + 5;
                    }
                    this.mFinalY = y;
                    return;
                }
                this.fitToScreenCacheString = text;
                String[] word = text.split(" ");
                String line = "";
                int i2 = 0;
                int cachePosition2 = 0;
                while (i2 < word.length) {
                    String oldLine = line;
                    line = String.valueOf(line) + " " + word[i2];
                    if (word[i2].trim().equals(NEWLINE)) {
                        bitmapFont.drawString(canvas, oldLine.trim(), x, y, size, 0, paint);
                        cachePosition = cachePosition2 + 1;
                        this.fitToScreenCache[cachePosition2] = oldLine.trim();
                        line = "";
                        y += size + 5;
                    } else if (bitmapFont.getStringWidth(line, size) > width) {
                        bitmapFont.drawString(canvas, oldLine.trim(), x, y, size, 0, paint);
                        cachePosition = cachePosition2 + 1;
                        this.fitToScreenCache[cachePosition2] = oldLine.trim();
                        line = "";
                        y += size + 5;
                        i2--;
                    } else if (i2 == word.length - 1) {
                        bitmapFont.drawString(canvas, line.trim(), x, y, size, 0, paint);
                        this.fitToScreenCache[cachePosition2] = line.trim();
                        this.fitToScreenCacheLines = cachePosition2 + 1;
                        this.mFinalY = y;
                        return;
                    } else {
                        cachePosition = cachePosition2;
                    }
                    i2++;
                    cachePosition2 = cachePosition;
                }
                this.fitToScreenCacheLines = cachePosition2;
                this.mFinalY = y;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public int generateCacheAndHeight(String text, int font, int size, int width) throws Exception {
        BitmapFont bitmapFont;
        int cachePosition;
        switch (font) {
            case 0:
                try {
                    bitmapFont = this.futureFont;
                    break;
                } catch (Exception e) {
                    throw e;
                }
            case 1:
                bitmapFont = this.futureFontWhite;
                break;
            default:
                return 0;
        }
        if (text == null) {
            return 0;
        }
        if (text == "") {
            return 0;
        }
        int height = size;
        this.fitToScreenCacheString = text;
        String[] word = text.split(" ");
        String line = "";
        int i = 0;
        int cachePosition2 = 0;
        while (i < word.length) {
            String oldLine = line;
            line = String.valueOf(line) + " " + word[i];
            if (word[i].trim().equals(NEWLINE)) {
                cachePosition = cachePosition2 + 1;
                this.fitToScreenCache[cachePosition2] = oldLine.trim();
                line = "";
                height += size + 5;
            } else if (bitmapFont.getStringWidth(line, size) > width) {
                cachePosition = cachePosition2 + 1;
                this.fitToScreenCache[cachePosition2] = oldLine.trim();
                line = "";
                height += size + 5;
                i--;
            } else if (i == word.length - 1) {
                this.fitToScreenCache[cachePosition2] = line.trim();
                this.fitToScreenCacheLines = cachePosition2 + 1;
                return height;
            } else {
                cachePosition = cachePosition2;
            }
            i++;
            cachePosition2 = cachePosition;
        }
        this.fitToScreenCacheLines = cachePosition2;
        return height;
    }

    public int getFinalY() {
        return this.mFinalY;
    }
}
