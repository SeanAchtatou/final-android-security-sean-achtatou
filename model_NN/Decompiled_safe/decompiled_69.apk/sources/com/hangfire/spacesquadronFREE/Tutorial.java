package com.hangfire.spacesquadronFREE;

import android.graphics.Canvas;
import android.graphics.Paint;

public final class Tutorial {
    private int mBoxX = 10;
    private int mBoxX2 = 200;
    private int mBoxY = 10;
    private int mBoxY2 = 40;
    private int mLineX = 100;
    private int mLineY = 200;
    private boolean mMapDragged = false;
    private String mMessage1 = "";
    private String mMessage2 = "";
    private String mMessage3 = "";
    private boolean mMoveCircleSelected = false;
    private Paint mPaint = new Paint();
    private boolean mScreenDragged = false;
    private int mScreenHeight = 1;
    private int mScreenWidth = 1;
    private int mStage = 0;
    private int mTextSize = 1;

    public void setScreenSize(int width, int height) throws Exception {
        try {
            this.mScreenWidth = width;
            this.mScreenHeight = height;
            this.mTextSize = width / 22;
            this.mPaint.setTextSize((float) this.mTextSize);
            this.mPaint.setStrokeWidth((float) (width / 50));
        } catch (Exception e) {
            throw e;
        }
    }

    public void draw(Canvas canvas) throws Exception {
        try {
            this.mPaint.setARGB(200, 80, 80, 255);
            if (this.mLineX != -1) {
                canvas.drawLine((float) ((this.mBoxX + this.mBoxX2) / 4), (float) ((this.mBoxY + this.mBoxY2) / 2), (float) this.mLineX, (float) this.mLineY, this.mPaint);
            }
            canvas.drawRect((float) this.mBoxX, (float) this.mBoxY, (float) this.mBoxX2, (float) this.mBoxY2, this.mPaint);
            this.mPaint.setARGB(255, 255, 255, 255);
            canvas.drawText(this.mMessage1, (float) (this.mBoxX + 5), (float) (this.mBoxY + 5 + this.mTextSize), this.mPaint);
            canvas.drawText(this.mMessage2, (float) (this.mBoxX + 5), (float) (this.mBoxY + 10 + (this.mTextSize * 2)), this.mPaint);
            canvas.drawText(this.mMessage3, (float) (this.mBoxX + 5), (float) (this.mBoxY + 15 + (this.mTextSize * 3)), this.mPaint);
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean complete(Units units, UnitPanel unitPanel, Timer timer, Screen screen, Radar radar, GameThread thread) throws Exception {
        try {
            if (!thread.getTutorialOn()) {
                return true;
            }
            switch (this.mStage) {
                case 0:
                    if (units.unitPlayer == units.unitSelected) {
                        this.mStage = 1;
                        break;
                    }
                    break;
                case 1:
                    if (units.unitPlayer == units.unitSelected) {
                        if (!unitPanel.isVisible()) {
                            if (!radar.radarMode) {
                                if (units.moveCircleSelected) {
                                    this.mMoveCircleSelected = true;
                                }
                                if (!units.moveCircleSelected && this.mMoveCircleSelected) {
                                    this.mStage = 2;
                                    break;
                                }
                            } else {
                                return true;
                            }
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
                case 2:
                    if (units.unitPlayer == units.unitSelected) {
                        if (unitPanel.isVisible()) {
                            this.mStage = 3;
                            break;
                        }
                    } else {
                        return true;
                    }
                    break;
                case 3:
                    if (unitPanel.isVisible()) {
                        if (unitPanel.mbEject.on) {
                            this.mStage = 4;
                            break;
                        }
                    } else {
                        return true;
                    }
                    break;
                case 4:
                    if (unitPanel.isVisible()) {
                        if (!unitPanel.mbEject.on) {
                            this.mStage = 5;
                            break;
                        }
                    } else {
                        return true;
                    }
                    break;
                case 5:
                    if (unitPanel.isVisible()) {
                        if (unitPanel.mbGun1.on) {
                            this.mStage = 6;
                            break;
                        }
                    } else {
                        return true;
                    }
                    break;
                case 6:
                    if (!unitPanel.isVisible()) {
                        this.mStage = 7;
                        break;
                    }
                    break;
                case 7:
                    if (timer.startTurn) {
                        this.mStage = 8;
                        break;
                    }
                    break;
                case 8:
                    if (screen.dragging) {
                        this.mScreenDragged = true;
                    }
                    if (!screen.dragging && this.mScreenDragged) {
                        this.mStage = 9;
                        break;
                    }
                case 9:
                    if (screen.zoomedIn) {
                        this.mStage = 10;
                        break;
                    }
                    break;
                case 10:
                    if (radar.radarMode) {
                        this.mStage = 11;
                        break;
                    }
                    break;
                case 11:
                    if (radar.radarMode) {
                        if (radar.dragX != -1.0f) {
                            this.mMapDragged = true;
                        }
                        if (radar.dragX == -1.0f && this.mMapDragged) {
                            this.mStage = 12;
                            break;
                        }
                    } else {
                        return true;
                    }
                case 12:
                    if (!radar.radarMode) {
                        this.mStage = 13;
                        break;
                    }
                    break;
                case 13:
                    if (units.moveCircleSelected || unitPanel.isVisible()) {
                        return true;
                    }
            }
            switch (this.mStage) {
                case 0:
                    this.mLineX = units.unitPlayer.intScreenX;
                    this.mLineY = units.unitPlayer.intScreenY;
                    this.mMessage1 = "In mission 1, you only have 1 ship.";
                    this.mMessage2 = "The grey circle denotes your commander.";
                    this.mMessage3 = "Tap it now to select it.";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.9d);
                    break;
                case 1:
                    this.mLineX = units.moveCircleX;
                    this.mLineY = units.moveCircleY;
                    this.mMessage1 = "To move this ship, drag this circle.";
                    this.mMessage2 = "The ship will accelerate more each turn.";
                    this.mMessage3 = "Movement will happen when we end the turn.";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.95d);
                    break;
                case 2:
                    this.mLineX = units.unitPlayer.intScreenX;
                    this.mLineY = units.unitPlayer.intScreenY;
                    this.mMessage1 = "We can also order pilots to fire and even";
                    this.mMessage2 = "eject if things go wrong. To do this, tap";
                    this.mMessage3 = "the ship again to access the inventory.";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.9d);
                    break;
                case 3:
                    this.mLineX = unitPanel.mbEject.getX(0);
                    this.mLineY = unitPanel.mbEject.getY(0);
                    this.mMessage1 = "This screen shows the status of your ship.";
                    this.mMessage2 = "You should eject your commander if his ship";
                    this.mMessage3 = "is likely to be destroyed. Test this now.";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.95d);
                    break;
                case 4:
                    this.mLineX = unitPanel.mbEject.getX(0);
                    this.mLineY = unitPanel.mbEject.getY(0);
                    this.mMessage1 = "The commander will eject at the end of the";
                    this.mMessage2 = "turn. We don't want to do this, so press";
                    this.mMessage3 = "eject again to toggle this order off.";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.95d);
                    break;
                case 5:
                    this.mLineX = unitPanel.mbGun1.getX(3);
                    this.mLineY = unitPanel.mbGun1.getY(3);
                    this.mMessage1 = "You can toggle firing of weapons on";
                    this.mMessage2 = "and off in exactly the same way.";
                    this.mMessage3 = "Set this weapon to fire.";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.85d);
                    break;
                case 6:
                    this.mLineX = unitPanel.mbClose.getX(2);
                    this.mLineY = unitPanel.mbClose.getY(2);
                    this.mMessage1 = "The rest of the screen shows armour and the";
                    this.mMessage2 = "status of ship components. Keep an eye on";
                    this.mMessage3 = "your ships! Now close the window.";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.95d);
                    break;
                case 7:
                    this.mLineX = (int) (((double) this.mScreenWidth) * 0.95d);
                    this.mLineY = this.mScreenHeight - ((int) (((double) this.mScreenWidth) * 0.05d));
                    this.mMessage1 = "Now we've chosen our moves,";
                    this.mMessage2 = "we can end the turn.";
                    this.mMessage3 = "Press the end turn button.";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.8d);
                    break;
                case 8:
                    this.mLineX = -1;
                    this.mLineY = -1;
                    this.mMessage1 = "Repeat the previous steps each turn to";
                    this.mMessage2 = "control your ships. To scroll around,";
                    this.mMessage3 = "just drag the screen. Try this now.";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.95d);
                    break;
                case 9:
                    this.mLineX = -1;
                    this.mLineY = -1;
                    this.mMessage1 = "If you want to get closer to the action,";
                    this.mMessage2 = "use the toggle zoom function. Press the";
                    this.mMessage3 = "menu key on your phone and zoom in.";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.95d);
                    break;
                case 10:
                    this.mLineX = -1;
                    this.mLineY = -1;
                    this.mMessage1 = "To get a high level view of the area,";
                    this.mMessage2 = "use the map feature. Press the menu key";
                    this.mMessage3 = "on your phone and select the map.";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.95d);
                    break;
                case 11:
                    this.mLineX = this.mScreenWidth / 2;
                    this.mLineY = (this.mScreenHeight / 2) + 5;
                    this.mMessage1 = "Green dots are friendly units, red dots are";
                    this.mMessage2 = "hostile and black is space debris. Drag the";
                    this.mMessage3 = "screen to move the map. Try this now.";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.95d);
                    break;
                case 12:
                    this.mLineX = radar.mbClose.getX(2);
                    this.mLineY = radar.mbClose.getY(2);
                    this.mMessage1 = "If a beep sounds when the turn begins, a";
                    this.mMessage2 = "hostile has been detected. Make sure you";
                    this.mMessage3 = "check the map! Now close this window.";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.95d);
                    break;
                case 13:
                    this.mLineX = -1;
                    this.mLineY = -1;
                    this.mMessage1 = "That's the basics! Refer to the help page";
                    this.mMessage2 = "on the menu for full info. Now head north";
                    this.mMessage3 = "and eliminate those enemy scouts!";
                    this.mBoxX2 = (int) (((double) this.mScreenWidth) * 0.95d);
                    break;
            }
            if (this.mLineY > this.mScreenHeight / 2) {
                this.mBoxY = this.mScreenHeight / 6;
            } else {
                this.mBoxY = (this.mScreenHeight / 6) * 4;
            }
            this.mBoxY2 = this.mBoxY + (this.mTextSize * 3) + 20;
            this.mBoxX = 5;
            return false;
        } catch (Exception e) {
            throw e;
        }
    }
}
