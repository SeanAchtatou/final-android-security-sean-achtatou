package com.hangfire.spacesquadronFREE;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

public final class EndTurnButton {
    private Bitmap endTurn;
    private Bitmap endTurnDisabled;
    private boolean endTurnPressed = false;
    private Bitmap endTurnSelect;

    public EndTurnButton(Resources res) throws Exception {
        try {
            this.endTurn = BitmapFactory.decodeResource(res, R.drawable.nextturn);
            this.endTurnSelect = BitmapFactory.decodeResource(res, R.drawable.nextturnselect);
            this.endTurnDisabled = BitmapFactory.decodeResource(res, R.drawable.nextturndis);
        } catch (Exception e) {
            throw e;
        }
    }

    public void draw(Canvas canvas, Screen screen, Timer timer, GameThread thread) throws Exception {
        try {
            if (thread.getOnScreenTurnButtonOn()) {
                drawEndTurn(canvas, screen, timer);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void drawEndTurn(Canvas canvas, Screen screen, Timer timer) throws Exception {
        try {
            int x = screen.intScreenWidth - this.endTurn.getWidth();
            int y = screen.intScreenHeight - this.endTurn.getHeight();
            if (timer.realTimeMode) {
                canvas.drawBitmap(this.endTurnDisabled, (float) x, (float) y, (Paint) null);
            } else if (this.endTurnPressed) {
                canvas.drawBitmap(this.endTurnSelect, (float) x, (float) y, (Paint) null);
            } else {
                canvas.drawBitmap(this.endTurn, (float) x, (float) y, (Paint) null);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean touchDown(Screen screen, float x, float y, GameThread thread) throws Exception {
        try {
            if (!inZone(screen, x, y) || !thread.getOnScreenTurnButtonOn()) {
                return false;
            }
            this.endTurnPressed = true;
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public void touchUp(Screen screen, float x, float y, Timer timer, GameThread thread) throws Exception {
        try {
            if (inZone(screen, x, y) && thread.getOnScreenTurnButtonOn() && this.endTurnPressed) {
                timer.endTurn();
            }
            this.endTurnPressed = false;
        } catch (Exception e) {
            throw e;
        }
    }

    private boolean inZone(Screen screen, float x, float y) throws Exception {
        try {
            if (y <= ((float) (screen.intScreenHeight - this.endTurn.getWidth())) || x <= ((float) (screen.intScreenWidth - this.endTurn.getWidth()))) {
                return false;
            }
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
}
