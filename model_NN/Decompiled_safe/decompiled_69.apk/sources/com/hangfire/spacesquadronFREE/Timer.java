package com.hangfire.spacesquadronFREE;

public final class Timer {
    public static final int FRAMES_PER_SECOND = 30;
    public static final int FRAMES_PER_TURN = 60;
    public static final int INTERVAL_DONE = 0;
    public static final int INTERVAL_WAIT = 1;
    private long lastStamp;
    public boolean realTimeMode = false;
    public boolean startRealTime = false;
    public boolean startTurn = false;
    private int turnCounter = 0;
    private long waitNanos;

    public Timer() throws Exception {
        try {
            long startTime = System.currentTimeMillis();
            long startNano = System.nanoTime();
            do {
            } while (2000 + startTime > System.currentTimeMillis());
            this.waitNanos = (System.nanoTime() - startNano) / 60;
        } catch (Exception e) {
            throw e;
        }
    }

    public void reset() throws Exception {
        try {
            this.lastStamp = System.nanoTime();
            this.realTimeMode = false;
            this.startTurn = false;
            this.startRealTime = false;
        } catch (Exception e) {
            throw e;
        }
    }

    public void processTimer() throws Exception {
        try {
            if (this.realTimeMode) {
                if (this.turnCounter == 60) {
                    this.startRealTime = true;
                } else {
                    this.startRealTime = false;
                }
                this.turnCounter--;
                if (this.turnCounter <= 0) {
                    this.realTimeMode = false;
                    this.startTurn = true;
                }
            } else {
                this.startTurn = false;
            }
            do {
            } while (System.nanoTime() < this.lastStamp + this.waitNanos);
            this.lastStamp = System.nanoTime();
        } catch (Exception e) {
            throw e;
        }
    }

    public void endTurn() {
        this.realTimeMode = true;
        this.turnCounter = 60;
    }
}
