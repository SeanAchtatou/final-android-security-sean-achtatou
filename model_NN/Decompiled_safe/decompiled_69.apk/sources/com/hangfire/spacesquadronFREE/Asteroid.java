package com.hangfire.spacesquadronFREE;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

public final class Asteroid {
    private float ang = 0.0f;
    private float angSpeed = 0.0f;
    public double dblWorldX = 0.0d;
    public double dblWorldY = 0.0d;
    private float fltScreenDestX = 0.0f;
    private float fltScreenDestY = 0.0f;
    private float fltScreenX = 0.0f;
    private float fltScreenY = 0.0f;
    private boolean inScreen = false;
    public int intColRadius = 0;
    private int intHalfVisRadius = 0;
    private int intScreenX = 0;
    private int intScreenY = 0;
    private int intVisualRadius = 0;
    public float speed = 0.0f;
    public float speedX = 0.0f;
    public float speedY = 0.0f;
    public float travelAng = 0.0f;
    public int type = 0;

    public Asteroid(int t, double x, double y, float tang, float s, int visRadius, int colRadius) throws Exception {
        try {
            this.dblWorldX = x;
            this.dblWorldY = y;
            this.type = t;
            this.ang = (float) (Math.random() * 360.0d);
            this.angSpeed = -1.0f + ((float) (Math.random() * 2.0d));
            this.intVisualRadius = visRadius;
            this.intHalfVisRadius = visRadius / 2;
            this.intColRadius = colRadius;
            this.travelAng = tang;
            this.speed = s;
            this.speedX = LookUp.sin(this.travelAng) * this.speed;
            this.speedY = LookUp.cos(this.travelAng) * this.speed;
        } catch (Exception e) {
            throw e;
        }
    }

    public void setSpeed(float s) throws Exception {
        try {
            this.speed = s;
            this.speedX = LookUp.sin(this.travelAng) * s;
            this.speedY = LookUp.cos(this.travelAng) * s;
        } catch (Exception e) {
            throw e;
        }
    }

    public void addSpeed(float xspd, float yspd) throws Exception {
        try {
            this.speedX += xspd;
            this.speedY += yspd;
            this.speed = LookUp.sqrt((this.speedX * this.speedX) + (this.speedY * this.speedY));
            this.travelAng = Functions.aTanFull(this.speedX, this.speedY);
        } catch (Exception e) {
            throw e;
        }
    }

    public void projectAway(double midX, double midY, float projDist) throws Exception {
        try {
            float xdiff = (float) (midX - this.dblWorldX);
            float ydiff = (float) (midY - this.dblWorldY);
            float ratio = projDist / LookUp.sqrt((xdiff * xdiff) + (ydiff * ydiff));
            this.dblWorldX = midX - ((double) (xdiff * ratio));
            this.dblWorldY = midY - ((double) (ydiff * ratio));
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean moveStillAlive(Map map) throws Exception {
        try {
            this.dblWorldX += (double) this.speedX;
            this.dblWorldY += (double) this.speedY;
            if (this.dblWorldX < 0.0d) {
                return false;
            }
            if (this.dblWorldX > ((double) map.worldSizeX)) {
                return false;
            }
            if (this.dblWorldY < 0.0d) {
                return false;
            }
            if (this.dblWorldY > ((double) map.worldSizeY)) {
                return false;
            }
            this.ang = Functions.wrapAngle(this.ang + this.angSpeed);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public void calculateScreenPos(Screen screen) throws Exception {
        try {
            this.fltScreenX = screen.canvasX(this.dblWorldX);
            this.fltScreenY = screen.canvasY(this.dblWorldY);
            this.intScreenX = (int) this.fltScreenX;
            this.intScreenY = (int) this.fltScreenY;
            this.inScreen = screen.inScreenWorldValues(this.dblWorldX, this.dblWorldY, this.intVisualRadius, this.intVisualRadius);
        } catch (Exception e) {
            throw e;
        }
    }

    public void draw(Canvas canvas, Boolean zoomed, Drawable img) throws Exception {
        try {
            if (this.inScreen) {
                canvas.save();
                canvas.rotate(this.ang, this.fltScreenX, this.fltScreenY);
                if (zoomed.booleanValue()) {
                    img.setBounds(this.intScreenX - this.intVisualRadius, this.intScreenY - this.intVisualRadius, this.intScreenX + this.intVisualRadius, this.intScreenY + this.intVisualRadius);
                } else {
                    img.setBounds(this.intScreenX - this.intHalfVisRadius, this.intScreenY - this.intHalfVisRadius, this.intScreenX + this.intHalfVisRadius, this.intScreenY + this.intHalfVisRadius);
                }
                img.draw(canvas);
                canvas.restore();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void drawArrowLine(Canvas canvas, NavArrow navArrow, Screen screen) throws Exception {
        try {
            if (this.inScreen) {
                navArrow.drawLine(canvas, this.intScreenX, this.intScreenY, this.travelAng, this.speed, 0.0f, screen, true, true, 0);
                this.fltScreenDestX = navArrow.fltX;
                this.fltScreenDestY = navArrow.fltY;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void drawArrowHead(Canvas canvas, NavArrow navArrow) throws Exception {
        try {
            if (this.inScreen) {
                navArrow.drawHead(canvas, this.fltScreenDestX, this.fltScreenDestY, this.travelAng, 0, false);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public String getSaveString() throws Exception {
        try {
            return String.valueOf(String.valueOf(this.type)) + "B" + String.valueOf((int) this.dblWorldX) + "B" + String.valueOf((int) this.dblWorldY) + "B" + String.valueOf((int) (this.speedX * 1000.0f)) + "B" + String.valueOf((int) (this.speedY * 1000.0f)) + "B" + String.valueOf((int) this.travelAng) + "B" + String.valueOf((int) (this.speed * 1000.0f)) + "B" + String.valueOf((int) (this.angSpeed * 1000.0f)) + "B" + String.valueOf((int) this.ang) + "B" + String.valueOf((int) this.fltScreenX) + "B" + String.valueOf((int) this.fltScreenY) + "B" + String.valueOf(this.intScreenX) + "B" + String.valueOf(this.intScreenY) + "B" + Functions.toStr(this.inScreen) + "B" + String.valueOf(this.intVisualRadius) + "B" + String.valueOf(this.intHalfVisRadius) + "B" + String.valueOf(this.intColRadius) + "B" + String.valueOf((int) this.fltScreenDestX) + "B" + String.valueOf((int) this.fltScreenDestY);
        } catch (Exception e) {
            throw e;
        }
    }

    public void restoreFromSaveString(String saveString) throws Exception {
        try {
            String[] data = saveString.split("B");
            int pos = 0 + 1;
            this.type = Integer.parseInt(data[0]);
            int pos2 = pos + 1;
            this.dblWorldX = (double) Integer.parseInt(data[pos]);
            int pos3 = pos2 + 1;
            this.dblWorldY = (double) Integer.parseInt(data[pos2]);
            int pos4 = pos3 + 1;
            this.speedX = ((float) Integer.parseInt(data[pos3])) / 1000.0f;
            int pos5 = pos4 + 1;
            this.speedY = ((float) Integer.parseInt(data[pos4])) / 1000.0f;
            int pos6 = pos5 + 1;
            this.travelAng = (float) Integer.parseInt(data[pos5]);
            int pos7 = pos6 + 1;
            this.speed = ((float) Integer.parseInt(data[pos6])) / 1000.0f;
            int pos8 = pos7 + 1;
            this.angSpeed = ((float) Integer.parseInt(data[pos7])) / 1000.0f;
            int pos9 = pos8 + 1;
            this.ang = (float) Integer.parseInt(data[pos8]);
            int pos10 = pos9 + 1;
            this.fltScreenX = (float) Integer.parseInt(data[pos9]);
            int pos11 = pos10 + 1;
            this.fltScreenY = (float) Integer.parseInt(data[pos10]);
            int pos12 = pos11 + 1;
            this.intScreenX = Integer.parseInt(data[pos11]);
            int pos13 = pos12 + 1;
            this.intScreenY = Integer.parseInt(data[pos12]);
            int pos14 = pos13 + 1;
            this.inScreen = Functions.toBoolean(data[pos13]);
            int pos15 = pos14 + 1;
            this.intVisualRadius = Integer.parseInt(data[pos14]);
            int pos16 = pos15 + 1;
            this.intHalfVisRadius = Integer.parseInt(data[pos15]);
            int pos17 = pos16 + 1;
            this.intColRadius = Integer.parseInt(data[pos16]);
            int pos18 = pos17 + 1;
            this.fltScreenDestX = (float) Integer.parseInt(data[pos17]);
            int i = pos18 + 1;
            this.fltScreenDestY = (float) Integer.parseInt(data[pos18]);
        } catch (Exception e) {
            throw e;
        }
    }
}
