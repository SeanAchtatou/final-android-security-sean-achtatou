package com.hangfire.spacesquadronFREE;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.ArrayList;

public final class Units {
    public static final int AI_ASTEROID_AVOID_DISTANCE = 10;
    public static final int AI_ASTEROID_COVER_RANGE = 800;
    public static final int AI_AT_LOCATION_THRESHHOLD = 400;
    public static final int AI_AVOIDANCE_CHECK_DISTANCE = 400;
    public static final int AI_AVOID_TURNLOCK_RANGE = 200;
    public static final int AI_EVADE_MISSILE_THRESHHOLD = 400;
    public static final int AI_EVADE_PURSUIT_THRESHHOLD = 400;
    public static final int AI_MAP_BORDER_AVOID_BUFFER = 50;
    public static final int AI_MAXIMUM_AVOIDANCE_CHECKS = 4;
    public static final int AI_MISSION_ATTACKUNIT = 2;
    public static final int AI_MISSION_DEFENDUNIT = 1;
    public static final int AI_MISSION_MOVETOLOCATION = 3;
    public static final int AI_MISSION_SEEKANDDESTROY = 0;
    public static final int AI_SEARCH_DISTANCE = 1400;
    public static final float AI_TARGETANGDIFF_WEIGHTING = 3.0f;
    public static final int AI_ZONE_SEARCHED_PROXIMITY = 300;
    public static final int AI_ZONE_WAIT = 120;
    public static final int HAZARD_PULSE_SPEED = 20;
    public static final int LINE_OF_SIGHT_RANGE = 1400;
    public static final int MOVEMENT_HEAVY = 2;
    public static final int MOVEMENT_LIGHT = 1;
    public static final int MOVEMENT_STATIC = 0;
    public static final int NUMBER_OF_TEAMS = 2;
    public static final int PLAYER_PULSE_BRIGHTNESS = 60;
    public static final int PLAYER_PULSE_SPEED = 1;
    public static final int TEAM_ENEMY = 1;
    public static final int TEAM_PLAYER = 0;
    public static final int UNIT_ASSAULT = 10;
    public static final int UNIT_CARGO = 5;
    public static final int UNIT_DEFENDER = 14;
    public static final int UNIT_ELITE = 12;
    public static final int UNIT_ENEMYASSAULT = 7;
    public static final int UNIT_ENEMYCARGO = 17;
    public static final int UNIT_ENEMYCRUISER = 18;
    public static final int UNIT_ENEMYCRUISERTURRET = 19;
    public static final int UNIT_ENEMYDEFENDER = 13;
    public static final int UNIT_ENEMYFACTORY = 16;
    public static final int UNIT_ENEMYFAST = 11;
    public static final int UNIT_ENEMYFIGHTER = 4;
    public static final int UNIT_ENEMYHQ = 9;
    public static final int UNIT_ENEMYHQ2 = 21;
    public static final int UNIT_ENEMYSCOUT = 3;
    public static final int UNIT_ENEMYTURRET = 8;
    public static final int UNIT_FAST = 6;
    public static final int UNIT_FIGHTER = 1;
    public static final int UNIT_HEAVY = 15;
    public static final int UNIT_HQ = 0;
    public static final int UNIT_HQ2 = 20;
    public static final int UNIT_TURRET = 2;
    public static final int VISIBILITY_BLEND_IN_SPEED = 10;
    public int[] AI_SearchZoneWait;
    public double[] AI_ZoneSearchX;
    public double[] AI_ZoneSearchY;
    public boolean[] AI_ZoneSearched;
    private float collisionAng;
    private double cornerHitX;
    private double cornerHitY;
    private Bitmap hazardImage;
    private Bitmap hazardImage_small;
    private Paint hazardPaint;
    public int hazardPulse = 0;
    private Bitmap img_assault;
    private Bitmap img_assault_ZO;
    private Bitmap img_assault_ejected;
    private Bitmap img_assault_ejected_ZO;
    private Bitmap img_cargo;
    private Bitmap img_cargo_ZO;
    private Bitmap img_cargo_ejected;
    private Bitmap img_cargo_ejected_ZO;
    private Bitmap img_defender;
    private Bitmap img_defender_ZO;
    private Bitmap img_defender_ejected;
    private Bitmap img_defender_ejected_ZO;
    private Bitmap img_elite;
    private Bitmap img_elite_ZO;
    private Bitmap img_elite_ejected;
    private Bitmap img_elite_ejected_ZO;
    private Bitmap img_enemyassault;
    private Bitmap img_enemyassault_ZO;
    private Bitmap img_enemyassault_ejected;
    private Bitmap img_enemyassault_ejected_ZO;
    private Bitmap img_enemycargo;
    private Bitmap img_enemycargo_ZO;
    private Bitmap img_enemycargo_ejected;
    private Bitmap img_enemycargo_ejected_ZO;
    private Bitmap img_enemycruiser;
    private Bitmap img_enemycruiser_ZO;
    private Bitmap img_enemycruiser_ejected;
    private Bitmap img_enemycruiser_ejected_ZO;
    private Bitmap img_enemydefender;
    private Bitmap img_enemydefender_ZO;
    private Bitmap img_enemydefender_ejected;
    private Bitmap img_enemydefender_ejected_ZO;
    private Bitmap img_enemyfactory;
    private Bitmap img_enemyfactory_ZO;
    private Bitmap img_enemyfast;
    private Bitmap img_enemyfast_ZO;
    private Bitmap img_enemyfast_ejected;
    private Bitmap img_enemyfast_ejected_ZO;
    private Bitmap img_enemyfighter;
    private Bitmap img_enemyfighter_ZO;
    private Bitmap img_enemyfighter_ejected;
    private Bitmap img_enemyfighter_ejected_ZO;
    private Bitmap img_enemyhq;
    private Bitmap img_enemyhq2;
    private Bitmap img_enemyhq2_ZO;
    private Bitmap img_enemyhq_ZO;
    private Bitmap img_enemyscout;
    private Bitmap img_enemyscout_ZO;
    private Bitmap img_enemyscout_ejected;
    private Bitmap img_enemyscout_ejected_ZO;
    private Bitmap img_enemyturret;
    private Bitmap img_enemyturret_ZO;
    private Bitmap img_enemyturret_ejected;
    private Bitmap img_enemyturret_ejected_ZO;
    private Bitmap img_fast;
    private Bitmap img_fast_ZO;
    private Bitmap img_fast_ejected;
    private Bitmap img_fast_ejected_ZO;
    private Bitmap img_fighter;
    private Bitmap img_fighter_ZO;
    private Bitmap img_fighter_ejected;
    private Bitmap img_fighter_ejected_ZO;
    private Bitmap img_heavy;
    private Bitmap img_heavy_ZO;
    private Bitmap img_heavy_ejected;
    private Bitmap img_heavy_ejected_ZO;
    private Bitmap img_hq;
    private Bitmap img_hq2;
    private Bitmap img_hq2_ZO;
    private Bitmap img_hq_ZO;
    private Bitmap img_turret;
    private Bitmap img_turret_ZO;
    private Bitmap img_turret_ejected;
    private Bitmap img_turret_ejected_ZO;
    public int loadUnitPlayerID;
    public boolean mPlayWarning;
    private float moveCircleDistance = 0.0f;
    private float moveCircleGrabOffsetX = 0.0f;
    private float moveCircleGrabOffsetY = 0.0f;
    private Paint moveCirclePaint;
    private int moveCircleRadius = 0;
    public boolean moveCircleSelected = false;
    private int moveCircleTieLineX = 0;
    private int moveCircleTieLineY = 0;
    public int moveCircleX = 0;
    public int moveCircleY = 0;
    private int numUnits = 0;
    private int playerUnitRingAlpha = 100;
    private Paint playerUnitRingPaint;
    private Bitmap podImage;
    private Bitmap podImage_ZO;
    private float projectDistance;
    private int selectionRingRadius = 30;
    private Paint shipAlpha;
    public ArrayList<Unit> unit = new ArrayList<>();
    public boolean unitChanged = false;
    public Unit unitPlayer = null;
    public Unit unitSelected = null;

    public Units(Resources res) throws Exception {
        try {
            System.gc();
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inScaled = false;
            this.img_hq_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayhqsmall, opt);
            this.img_fighter_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayfightersmall, opt);
            this.img_fighter_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayfightereject, opt);
            this.img_turret_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayturretsmall, opt);
            this.img_turret_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayturreteject, opt);
            this.img_enemyscout_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemyscoutsmall, opt);
            this.img_enemyscout_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemyscouteject, opt);
            this.img_enemyfighter_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemyfightersmall, opt);
            this.img_enemyfighter_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemyfightereject, opt);
            this.img_cargo_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplaycargosmall, opt);
            this.img_cargo_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplaycargoeject, opt);
            this.img_fast_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayfastsmall, opt);
            this.img_fast_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayfasteject, opt);
            this.img_enemyassault_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemyassaultsmall, opt);
            this.img_enemyassault_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemyassaulteject, opt);
            this.img_enemyturret_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemyturretsmall, opt);
            this.img_enemyturret_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemyturreteject, opt);
            this.img_enemyhq_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemyhqsmall, opt);
            this.img_assault_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayassaultsmall, opt);
            this.img_assault_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayassaulteject, opt);
            this.img_enemyfast_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemyfastsmall, opt);
            this.img_enemyfast_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemyfasteject, opt);
            this.img_elite_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayelitesmall, opt);
            this.img_elite_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayeliteeject, opt);
            this.img_enemydefender_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemydefendersmall, opt);
            this.img_enemydefender_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemydefendereject, opt);
            this.img_defender_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplaydefendersmall, opt);
            this.img_defender_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplaydefendereject, opt);
            this.img_heavy_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayheavysmall, opt);
            this.img_heavy_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayheavyeject, opt);
            this.img_enemyfactory_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemyfactorysmall, opt);
            this.img_enemycargo_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemycargosmall, opt);
            this.img_enemycargo_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemycargoeject, opt);
            this.img_enemycruiser_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemycruisersmall, opt);
            this.img_enemycruiser_ejected_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemycruisereject, opt);
            this.img_hq2_ZO = BitmapFactory.decodeResource(res, R.drawable.shipplayhq2small, opt);
            this.img_enemyhq2_ZO = BitmapFactory.decodeResource(res, R.drawable.shipenemyhq2small, opt);
            this.podImage_ZO = BitmapFactory.decodeResource(res, R.drawable.escapepod, opt);
            this.img_hq = Bitmap.createScaledBitmap(this.img_hq_ZO, this.img_hq_ZO.getWidth() / 2, this.img_hq_ZO.getHeight() / 2, true);
            this.img_fighter = Bitmap.createScaledBitmap(this.img_fighter_ZO, this.img_fighter_ZO.getWidth() / 2, this.img_fighter_ZO.getHeight() / 2, true);
            this.img_fighter_ejected = Bitmap.createScaledBitmap(this.img_fighter_ejected_ZO, this.img_fighter_ejected_ZO.getWidth() / 2, this.img_fighter_ejected_ZO.getHeight() / 2, true);
            this.img_turret = Bitmap.createScaledBitmap(this.img_turret_ZO, this.img_turret_ZO.getWidth() / 2, this.img_turret_ZO.getHeight() / 2, true);
            this.img_turret_ejected = Bitmap.createScaledBitmap(this.img_turret_ejected_ZO, this.img_turret_ejected_ZO.getWidth() / 2, this.img_turret_ejected_ZO.getHeight() / 2, true);
            this.img_enemyscout = Bitmap.createScaledBitmap(this.img_enemyscout_ZO, this.img_enemyscout_ZO.getWidth() / 2, this.img_enemyscout_ZO.getHeight() / 2, true);
            this.img_enemyscout_ejected = Bitmap.createScaledBitmap(this.img_enemyscout_ejected_ZO, this.img_enemyscout_ejected_ZO.getWidth() / 2, this.img_enemyscout_ejected_ZO.getHeight() / 2, true);
            this.img_enemyfighter = Bitmap.createScaledBitmap(this.img_enemyfighter_ZO, this.img_enemyfighter_ZO.getWidth() / 2, this.img_enemyfighter_ZO.getHeight() / 2, true);
            this.img_enemyfighter_ejected = Bitmap.createScaledBitmap(this.img_enemyfighter_ejected_ZO, this.img_enemyfighter_ejected_ZO.getWidth() / 2, this.img_enemyfighter_ejected_ZO.getHeight() / 2, true);
            this.img_cargo = Bitmap.createScaledBitmap(this.img_cargo_ZO, this.img_cargo_ZO.getWidth() / 2, this.img_cargo_ZO.getHeight() / 2, true);
            this.img_cargo_ejected = Bitmap.createScaledBitmap(this.img_cargo_ejected_ZO, this.img_cargo_ejected_ZO.getWidth() / 2, this.img_cargo_ejected_ZO.getHeight() / 2, true);
            this.img_fast = Bitmap.createScaledBitmap(this.img_fast_ZO, this.img_fast_ZO.getWidth() / 2, this.img_fast_ZO.getHeight() / 2, true);
            this.img_fast_ejected = Bitmap.createScaledBitmap(this.img_fast_ejected_ZO, this.img_fast_ejected_ZO.getWidth() / 2, this.img_fast_ejected_ZO.getHeight() / 2, true);
            this.img_enemyassault = Bitmap.createScaledBitmap(this.img_enemyassault_ZO, this.img_enemyassault_ZO.getWidth() / 2, this.img_enemyassault_ZO.getHeight() / 2, true);
            this.img_enemyassault_ejected = Bitmap.createScaledBitmap(this.img_enemyassault_ejected_ZO, this.img_enemyassault_ejected_ZO.getWidth() / 2, this.img_enemyassault_ejected_ZO.getHeight() / 2, true);
            this.img_enemyturret = Bitmap.createScaledBitmap(this.img_enemyturret_ZO, this.img_enemyturret_ZO.getWidth() / 2, this.img_enemyturret_ZO.getHeight() / 2, true);
            this.img_enemyturret_ejected = Bitmap.createScaledBitmap(this.img_enemyturret_ejected_ZO, this.img_enemyturret_ejected_ZO.getWidth() / 2, this.img_enemyturret_ejected_ZO.getHeight() / 2, true);
            this.img_enemyhq = Bitmap.createScaledBitmap(this.img_enemyhq_ZO, this.img_enemyhq_ZO.getWidth() / 2, this.img_enemyhq_ZO.getHeight() / 2, true);
            this.img_assault = Bitmap.createScaledBitmap(this.img_assault_ZO, this.img_assault_ZO.getWidth() / 2, this.img_assault_ZO.getHeight() / 2, true);
            this.img_assault_ejected = Bitmap.createScaledBitmap(this.img_assault_ejected_ZO, this.img_assault_ejected_ZO.getWidth() / 2, this.img_assault_ejected_ZO.getHeight() / 2, true);
            this.img_enemyfast = Bitmap.createScaledBitmap(this.img_enemyfast_ZO, this.img_enemyfast_ZO.getWidth() / 2, this.img_enemyfast_ZO.getHeight() / 2, true);
            this.img_enemyfast_ejected = Bitmap.createScaledBitmap(this.img_enemyfast_ejected_ZO, this.img_enemyfast_ejected_ZO.getWidth() / 2, this.img_enemyfast_ejected_ZO.getHeight() / 2, true);
            this.img_elite = Bitmap.createScaledBitmap(this.img_elite_ZO, this.img_elite_ZO.getWidth() / 2, this.img_elite_ZO.getHeight() / 2, true);
            this.img_elite_ejected = Bitmap.createScaledBitmap(this.img_elite_ejected_ZO, this.img_elite_ejected_ZO.getWidth() / 2, this.img_elite_ejected_ZO.getHeight() / 2, true);
            this.img_enemydefender = Bitmap.createScaledBitmap(this.img_enemydefender_ZO, this.img_enemydefender_ZO.getWidth() / 2, this.img_enemydefender_ZO.getHeight() / 2, true);
            this.img_enemydefender_ejected = Bitmap.createScaledBitmap(this.img_enemydefender_ejected_ZO, this.img_enemydefender_ejected_ZO.getWidth() / 2, this.img_enemydefender_ejected_ZO.getHeight() / 2, true);
            this.img_defender = Bitmap.createScaledBitmap(this.img_defender_ZO, this.img_defender_ZO.getWidth() / 2, this.img_defender_ZO.getHeight() / 2, true);
            this.img_defender_ejected = Bitmap.createScaledBitmap(this.img_defender_ejected_ZO, this.img_defender_ejected_ZO.getWidth() / 2, this.img_defender_ejected_ZO.getHeight() / 2, true);
            this.img_heavy = Bitmap.createScaledBitmap(this.img_heavy_ZO, this.img_heavy_ZO.getWidth() / 2, this.img_heavy_ZO.getHeight() / 2, true);
            this.img_heavy_ejected = Bitmap.createScaledBitmap(this.img_heavy_ejected_ZO, this.img_heavy_ejected_ZO.getWidth() / 2, this.img_heavy_ejected_ZO.getHeight() / 2, true);
            this.img_enemyfactory = Bitmap.createScaledBitmap(this.img_enemyfactory_ZO, this.img_enemyfactory_ZO.getWidth() / 2, this.img_enemyfactory_ZO.getHeight() / 2, true);
            this.img_enemycargo = Bitmap.createScaledBitmap(this.img_enemycargo_ZO, this.img_enemycargo_ZO.getWidth() / 2, this.img_enemycargo_ZO.getHeight() / 2, true);
            this.img_enemycargo_ejected = Bitmap.createScaledBitmap(this.img_enemycargo_ejected_ZO, this.img_enemycargo_ejected_ZO.getWidth() / 2, this.img_enemycargo_ejected_ZO.getHeight() / 2, true);
            this.img_enemycruiser = Bitmap.createScaledBitmap(this.img_enemycruiser_ZO, this.img_enemycruiser_ZO.getWidth() / 2, this.img_enemycruiser_ZO.getHeight() / 2, true);
            this.img_enemycruiser_ejected = Bitmap.createScaledBitmap(this.img_enemycruiser_ejected_ZO, this.img_enemycruiser_ejected_ZO.getWidth() / 2, this.img_enemycruiser_ejected_ZO.getHeight() / 2, true);
            this.img_hq2 = Bitmap.createScaledBitmap(this.img_hq2_ZO, this.img_hq2_ZO.getWidth() / 2, this.img_hq2_ZO.getHeight() / 2, true);
            this.img_enemyhq2 = Bitmap.createScaledBitmap(this.img_enemyhq2_ZO, this.img_enemyhq2_ZO.getWidth() / 2, this.img_enemyhq2_ZO.getHeight() / 2, true);
            this.podImage = Bitmap.createScaledBitmap(this.podImage_ZO, this.podImage_ZO.getWidth() / 2, this.podImage_ZO.getHeight() / 2, true);
            this.shipAlpha = new Paint();
            this.hazardImage = BitmapFactory.decodeResource(res, R.drawable.hazard);
            this.hazardImage_small = Functions.getZoomedOutBitmap(res, R.drawable.hazard, 2);
            this.hazardPaint = new Paint();
            this.hazardPaint.setAlpha(255);
            this.moveCirclePaint = new Paint();
            this.moveCirclePaint.setARGB(100, 255, 0, 0);
            this.moveCirclePaint.setStrokeWidth(3.0f);
            this.playerUnitRingPaint = new Paint();
            this.playerUnitRingPaint.setARGB(60, 255, 255, 255);
            this.playerUnitRingPaint.setStrokeWidth(2.0f);
            this.playerUnitRingPaint.setStyle(Paint.Style.STROKE);
            this.AI_ZoneSearchX = new double[2];
            this.AI_ZoneSearchY = new double[2];
            this.AI_ZoneSearched = new boolean[2];
            this.AI_SearchZoneWait = new int[2];
            for (int i = 0; i < 2; i++) {
                this.AI_ZoneSearchX[i] = 0.0d;
                this.AI_ZoneSearchY[i] = 0.0d;
                this.AI_ZoneSearched[i] = true;
                this.AI_SearchZoneWait[i] = 0;
            }
            System.gc();
        } catch (Exception e) {
            throw e;
        }
    }

    public int getUnitIndex(Unit u) {
        if (u == null) {
            return -1;
        }
        for (int i = 0; i < this.unit.size(); i++) {
            if (this.unit.get(i) == u) {
                return i;
            }
        }
        return -1;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Unit getUnitFromIndex(int i) {
        if (i == -1) {
            return null;
        }
        return this.unit.get(i);
    }

    public void move(Map map, Particles particles) throws Exception {
        int u = 0;
        while (u < this.unit.size()) {
            try {
                Unit un = this.unit.get(u);
                if (un.inGameZone()) {
                    un.move(particles, map, this);
                }
                if (un.pod.podAlive && un.pod.podLaunched) {
                    un.pod.move(map, particles);
                }
                u++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public void collide(Asteroids asteroids, Map map) throws Exception {
        int u1 = 0;
        while (u1 < this.unit.size()) {
            try {
                Unit unit1 = this.unit.get(u1);
                if (unit1.inGameZone()) {
                    for (int u2 = u1 + 1; u2 < this.unit.size(); u2++) {
                        Unit unit2 = this.unit.get(u2);
                        if (unit2.inGameZone() && !unitsAttached(unit1, unit2) && unitsCollide(unit1, unit2, map)) {
                            postUnit2UnitCollisionProcess(unit1, unit2);
                        }
                    }
                    for (int a = 0; a < asteroids.asteroid.size(); a++) {
                        Asteroid ast = asteroids.asteroid.get(a);
                        if (unitHitsAsteroid(unit1, ast)) {
                            postUnit2AsteroidCollisionProcess(unit1, ast, map);
                        }
                    }
                    for (int u22 = 0; u22 < this.unit.size(); u22++) {
                        Pod pod = this.unit.get(u22).pod;
                        if (pod.podAlive && pod.podLaunched && !pod.podSafe) {
                            unitHitsPod(unit1, pod);
                        }
                    }
                }
                u1++;
            } catch (Exception e) {
                throw e;
            }
        }
        for (int i = 0; i < this.unit.size(); i++) {
            Pod pod2 = this.unit.get(i).pod;
            if (pod2.podAlive && pod2.podLaunched && !pod2.podSafe) {
                for (int a2 = 0; a2 < asteroids.asteroid.size(); a2++) {
                    podHitsAsteroid(pod2, asteroids.asteroid.get(a2));
                }
            }
        }
    }

    private boolean unitsAttached(Unit u1, Unit u2) throws Exception {
        try {
            if (u1.attachedTo == u2 || u2.attachedTo == u1) {
                return true;
            }
            return false;
        } catch (Exception e) {
            throw e;
        }
    }

    public void setScreenSize(int x, int y) throws Exception {
        if (x > y) {
            try {
                this.selectionRingRadius = y / 10;
                this.moveCircleRadius = y / 12;
            } catch (Exception e) {
                throw e;
            }
        } else {
            this.selectionRingRadius = x / 10;
            this.moveCircleRadius = x / 12;
        }
        if (this.selectionRingRadius > 50) {
            this.selectionRingRadius = 50;
        }
        if (this.moveCircleRadius > 40) {
            this.moveCircleRadius = 40;
        }
        this.moveCircleDistance = (float) (y / 8);
        if (y < x) {
            this.moveCircleDistance = (float) (x / 8);
        }
    }

    public boolean selectUnitMoveCircle(float x, float y) throws Exception {
        try {
            this.moveCircleSelected = false;
            if (this.unitSelected != null && this.unitSelected.navigationPossible() && x >= ((float) (this.moveCircleX - this.moveCircleRadius)) && x <= ((float) (this.moveCircleX + this.moveCircleRadius)) && y >= ((float) (this.moveCircleY - this.moveCircleRadius)) && y <= ((float) (this.moveCircleY + this.moveCircleRadius))) {
                this.moveCircleGrabOffsetX = x - ((float) this.moveCircleX);
                this.moveCircleGrabOffsetY = y - ((float) this.moveCircleY);
                this.moveCircleTieLineX = (int) x;
                this.moveCircleTieLineY = (int) y;
                this.moveCircleSelected = true;
            }
            return this.moveCircleSelected;
        } catch (Exception e) {
            throw e;
        }
    }

    public void touchUp(float x, float y) {
        this.moveCircleSelected = false;
    }

    public boolean moveUnitArrow(float x, float y, Screen screen, NavArrow navArrow) throws Exception {
        try {
            if (this.unitSelected == null || !this.moveCircleSelected) {
                return false;
            }
            calculateMoveCircleXY(screen, navArrow);
            this.moveCircleTieLineX = (int) x;
            this.moveCircleTieLineY = (int) y;
            this.unitSelected.calcSpeedTurn(x - this.moveCircleGrabOffsetX, y - this.moveCircleGrabOffsetY, screen, this.moveCircleDistance);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    private void calculateMoveCircleXY(Screen screen, NavArrow navArrow) throws Exception {
        try {
            this.unitSelected.workOutDestination(navArrow, screen);
            this.unitSelected.setScreenPosition(screen);
            float dist = this.moveCircleDistance;
            if (this.unitSelected.fltSpeed < 0.0f) {
                dist = -dist;
            }
            this.moveCircleX = (int) (this.unitSelected.fltScreenFinishX + (LookUp.sin(this.unitSelected.fltScreenFinishAng) * dist));
            this.moveCircleY = (int) (this.unitSelected.fltScreenFinishY - (LookUp.cos(this.unitSelected.fltScreenFinishAng) * dist));
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean selectUnit(float x, float y, UnitPanel unitPanel) throws Exception {
        try {
            Unit u = getUnit(x, y, false);
            if (u == null) {
                return false;
            }
            if (this.unitSelected == u) {
                unitPanel.setVisible(true, this.unitSelected);
            }
            this.unitSelected = u;
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    private Unit getUnit(float x, float y, boolean playableOnly) throws Exception {
        Unit closest = null;
        float closestDist = 1.0E7f;
        int i = 0;
        while (i < this.unit.size()) {
            try {
                Unit u = this.unit.get(i);
                if (u.onScreen() && ((u.isPlayable && playableOnly) || !playableOnly)) {
                    float xDist = Math.abs(x - ((float) u.intScreenX));
                    if (xDist < ((float) this.selectionRingRadius)) {
                        float yDist = Math.abs(y - ((float) u.intScreenY));
                        if (yDist < ((float) this.selectionRingRadius)) {
                            float dist = (xDist * xDist) + (yDist * yDist);
                            if (dist < closestDist) {
                                closestDist = dist;
                                closest = u;
                            }
                        }
                    }
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
        return closest;
    }

    public Unit makeUnit(int type, int x, int y, int angle, boolean player, boolean playable, int team, Map map) throws Exception {
        Unit returnUnit = null;
        switch (type) {
            case 0:
                returnUnit = makeUnitHQ((float) x, (float) y, (float) angle, team);
                break;
            case 1:
                returnUnit = makeUnitFighter((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 2:
                returnUnit = makeUnitTurret((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 3:
                returnUnit = makeUnitEnemyScout((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 4:
                returnUnit = makeUnitEnemyFighter((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 5:
                returnUnit = makeUnitCargo((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 6:
                returnUnit = makeUnitFast((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 7:
                returnUnit = makeUnitEnemyAssault((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 8:
                returnUnit = makeUnitEnemyTurret((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 9:
                returnUnit = makeUnitEnemyHQ((float) x, (float) y, (float) angle, team);
                break;
            case 10:
                returnUnit = makeUnitAssault((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 11:
                returnUnit = makeUnitEnemyFast((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 12:
                returnUnit = makeUnitElite((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 13:
                returnUnit = makeUnitEnemyDefender((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 14:
                returnUnit = makeUnitDefender((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 15:
                returnUnit = makeUnitHeavy((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case 16:
                returnUnit = makeUnitEnemyFactory((float) x, (float) y, (float) angle, team);
                break;
            case UNIT_ENEMYCARGO /*17*/:
                returnUnit = makeUnitEnemyCargo((float) x, (float) y, (float) angle, player, playable, team);
                break;
            case UNIT_ENEMYCRUISER /*18*/:
                returnUnit = makeUnitEnemyCruiser((float) x, (float) y, (float) angle, player, playable, team, map);
                break;
            case 20:
                returnUnit = makeUnitHQ2((float) x, (float) y, (float) angle, team);
                break;
            case UNIT_ENEMYHQ2 /*21*/:
                returnUnit = makeUnitEnemyHQ2((float) x, (float) y, (float) angle, team);
                break;
        }
        if (player) {
            try {
                this.unitPlayer = returnUnit;
            } catch (Exception e) {
                throw e;
            }
        }
        return returnUnit;
    }

    public boolean lineIntersectsUnitCircle(double x1, double y1, double x2, double y2, int radius, Unit[] ignoreList) throws Exception {
        int i = 0;
        while (i < this.unit.size()) {
            try {
                Unit u = this.unit.get(i);
                if (!inIgnoreList(u, ignoreList)) {
                    if (Functions.lineIntersectsCircle(x1, y1, x2, y2, u.fltWorldX, u.fltWorldY, (float) u.intCollisionHalfWidth)) {
                        return true;
                    }
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
        return false;
    }

    public boolean inIgnoreList(Unit unit2, Unit[] list) throws Exception {
        if (list != null) {
            try {
                if (list.length > 0) {
                    for (Unit unit3 : list) {
                        if (unit3 == unit2) {
                            return true;
                        }
                    }
                    return false;
                }
            } catch (Exception e) {
                throw e;
            }
        }
        return false;
    }

    private boolean podHitsAsteroid(Pod p, Asteroid a) throws Exception {
        try {
            int radius = p.intColRadius + a.intColRadius;
            float xdist = (float) (p.dblWorldX - a.dblWorldX);
            float ydist = (float) (p.dblWorldY - a.dblWorldY);
            if (Math.abs(xdist) < ((float) radius) && Math.abs(ydist) < ((float) radius)) {
                float dist = (xdist * xdist) + (ydist * ydist);
                if (dist < ((float) (radius * radius))) {
                    float ratio = ((float) radius) / LookUp.sqrt(dist);
                    p.dblWorldX = a.dblWorldX + ((double) (xdist * ratio));
                    p.dblWorldY = a.dblWorldY + ((double) (ydist * ratio));
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            throw e;
        }
    }

    private boolean unitHitsPod(Unit u, Pod p) throws Exception {
        try {
            if (Functions.inCircle(p.dblWorldX, p.dblWorldY, u.fltWorldX, u.fltWorldY, u.intCollisionRadius + p.intColRadius)) {
                float projAng = Functions.aTanFull((float) (p.dblWorldX - u.fltWorldX), (float) (p.dblWorldY - u.fltWorldY));
                float dist = Functions.inRotatedBoxProjectAway(p.dblWorldX, p.dblWorldY, u.fltWorldX, u.fltWorldY, u.intCollisionHalfWidth, u.intCollisionHalfHeight, u.fltAngle, projAng);
                if (dist > 0.0f) {
                    p.dblWorldX += (double) (LookUp.sin(projAng) * dist);
                    p.dblWorldY += (double) (LookUp.cos(projAng) * dist);
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            throw e;
        }
    }

    private boolean unitHitsAsteroid(Unit u, Asteroid a) throws Exception {
        boolean pointIn = false;
        try {
            float radius = (float) (a.intColRadius * a.intColRadius);
            u.calcCornerCoords();
            for (int c = 0; c < 8; c += 2) {
                float xdist = (float) (u.cornerCoords[c] - a.dblWorldX);
                float ydist = (float) (u.cornerCoords[c + 1] - a.dblWorldY);
                float dist = (xdist * xdist) + (ydist * ydist);
                if (dist < 100000.0f && dist < radius) {
                    this.cornerHitX = u.cornerCoords[c];
                    this.cornerHitY = u.cornerCoords[c + 1];
                    pointIn = true;
                }
            }
            return pointIn;
        } catch (Exception e) {
            throw e;
        }
    }

    private void postUnit2AsteroidCollisionProcess(Unit u, Asteroid a, Map map) throws Exception {
        try {
            float xdist = (float) (a.dblWorldX - this.cornerHitX);
            float ydist = (float) (a.dblWorldY - this.cornerHitY);
            float ratio = ((float) a.intColRadius) / LookUp.sqrt((xdist * xdist) + (ydist * ydist));
            u.position(u.fltWorldX - ((double) ((float) (this.cornerHitX - (a.dblWorldX - ((double) (xdist * ratio)))))), u.fltWorldY - ((double) ((float) (this.cornerHitY - (a.dblWorldY - ((double) (ydist * ratio)))))), u.fltAngle, map, this);
            float colAng = Functions.aTanFull(xdist, ydist);
            float shipAng = u.fltAngle;
            float shipSpd = Math.abs(u.fltSpeed);
            if (u.fltSpeed < 0.0f) {
                shipAng = Functions.wrapAngle(180.0f + shipAng);
            }
            float angDiff = Math.abs(Functions.angDiff(shipAng, colAng));
            if (angDiff < 90.0f) {
                float transRatio = (90.0f + angDiff) / 180.0f;
                float remSpeed = (1.0f - transRatio) * shipSpd;
                u.setSpeed(u.fltSpeed * transRatio);
                float colAng2 = Functions.wrapAngle(180.0f + colAng);
                u.fltXSpd += LookUp.sin(colAng2) * remSpeed;
                u.fltYSpd += LookUp.cos(colAng2) * remSpeed;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void postUnit2UnitCollisionProcess(Unit unit1, Unit unit2) throws Exception {
        try {
            float colAng = Functions.aTanFull((float) (unit2.fltWorldX - unit1.fltWorldX), (float) (unit2.fltWorldY - unit1.fltWorldY));
            float shipAng = unit1.fltAngle;
            float shipSpd = Math.abs(unit1.fltSpeed);
            if (unit1.fltSpeed < 0.0f) {
                shipAng = Functions.wrapAngle(180.0f + shipAng);
            }
            float angDiff = Math.abs(Functions.angDiff(shipAng, colAng));
            if (angDiff < 90.0f) {
                float transRatio = angDiff / 90.0f;
                float remSpeed = (1.0f - transRatio) * shipSpd;
                unit1.setSpeed(unit1.fltSpeed * transRatio);
                unit2.fltXSpd += LookUp.sin(colAng) * remSpeed;
                unit2.fltYSpd += LookUp.cos(colAng) * remSpeed;
            }
            float colAng2 = Functions.wrapAngle(180.0f + colAng);
            float shipAng2 = unit2.fltAngle;
            float shipSpd2 = Math.abs(unit2.fltSpeed);
            if (unit2.fltSpeed < 0.0f) {
                shipAng2 = Functions.wrapAngle(180.0f + shipAng2);
            }
            float angDiff2 = Math.abs(Functions.angDiff(shipAng2, colAng2));
            if (angDiff2 < 90.0f) {
                float transRatio2 = angDiff2 / 90.0f;
                float remSpeed2 = (1.0f - transRatio2) * shipSpd2;
                unit2.setSpeed(unit2.fltSpeed * transRatio2);
                unit1.fltXSpd += LookUp.sin(colAng2) * remSpeed2;
                unit1.fltYSpd += LookUp.cos(colAng2) * remSpeed2;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private boolean unitsCollide(Unit unit1, Unit unit2, Map map) throws Exception {
        try {
            if (Functions.inCircle(unit1.fltWorldX, unit1.fltWorldY, unit2.fltWorldX, unit2.fltWorldY, unit1.intCollisionRadius + unit2.intCollisionRadius)) {
                unit1.calcCornerCoords();
                unit2.calcCornerCoords();
                double unit1centerX = unit1.fltWorldX;
                double unit1centerY = unit1.fltWorldY;
                int unit1Width = unit1.intCollisionHalfWidth;
                int unit1Height = unit1.intCollisionHalfHeight;
                int unit1radius = unit1.intCollisionRadius;
                float unit1Ang = unit1.fltAngle;
                double unit2centerX = unit2.fltWorldX;
                double unit2centerY = unit2.fltWorldY;
                int unit2Width = unit2.intCollisionHalfWidth;
                int unit2Height = unit2.intCollisionHalfHeight;
                int unit2radius = unit2.intCollisionRadius;
                float unit2Ang = unit2.fltAngle;
                this.collisionAng = Functions.aTanFull((float) (unit1centerX - unit2centerX), (float) (unit1centerY - unit2centerY));
                for (int c = 0; c < 8; c += 2) {
                    double unit1x = unit1.cornerCoords[c];
                    double unit1y = unit1.cornerCoords[c + 1];
                    if (Functions.inCircle(unit1x, unit1y, unit2centerX, unit2centerY, unit2radius)) {
                        this.projectDistance = Functions.inRotatedBoxProjectAway(unit1x, unit1y, unit2centerX, unit2centerY, unit2Width, unit2Height, unit2Ang, this.collisionAng);
                        if (this.projectDistance > 0.0f) {
                            this.projectDistance = this.projectDistance / 2.0f;
                            unit1.projectAway(unit2centerX, unit2centerY, this.projectDistance, map, this);
                            unit2.projectAway(unit1centerX, unit1centerY, this.projectDistance, map, this);
                            return true;
                        }
                    }
                    double unit2x = unit2.cornerCoords[c];
                    double unit2y = unit2.cornerCoords[c + 1];
                    if (Functions.inCircle(unit2x, unit2y, unit1centerX, unit1centerY, unit1radius)) {
                        this.projectDistance = Functions.inRotatedBoxProjectAway(unit2x, unit2y, unit1centerX, unit1centerY, unit1Width, unit1Height, unit1Ang, Functions.wrapAngle(this.collisionAng + 180.0f));
                        if (this.projectDistance > 0.0f) {
                            this.projectDistance = this.projectDistance / 2.0f;
                            unit1.projectAway(unit2centerX, unit2centerY, this.projectDistance, map, this);
                            unit2.projectAway(unit1centerX, unit1centerY, this.projectDistance, map, this);
                            return true;
                        }
                    }
                }
            }
            return false;
        } catch (Exception e) {
            throw e;
        }
    }

    public void draw(Canvas canvas, Timer timer, Screen screen, NavArrow navArrow) throws Exception {
        try {
            drawPods(canvas, timer, screen, navArrow);
            drawUnits(canvas, timer, screen, navArrow);
            drawUnitUI(canvas, timer);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawPods(Canvas canvas, Timer timer, Screen screen, NavArrow navArrow) throws Exception {
        int u = 0;
        while (u < this.unit.size()) {
            try {
                Pod p = this.unit.get(u).pod;
                if (p.podAlive && p.podLaunched) {
                    p.calculateScreenPos(screen);
                    if (!timer.realTimeMode) {
                        p.drawArrowLine(canvas, navArrow, screen);
                    }
                    if (screen.zoomedIn) {
                        p.draw(canvas, this.podImage_ZO);
                    } else {
                        p.draw(canvas, this.podImage);
                    }
                    if (!timer.realTimeMode) {
                        p.drawArrowHead(canvas, navArrow);
                    }
                }
                u++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    private void drawUnits(Canvas canvas, Timer timer, Screen screen, NavArrow navArrow) throws Exception {
        int u = 0;
        while (u < this.unit.size()) {
            try {
                Unit un = this.unit.get(u);
                if (un.inGameZone()) {
                    un.setScreenPosition(screen);
                    if (!timer.realTimeMode && ((un.inScreen || un.arrowInScreen) && un.getVisibleToPlayer())) {
                        un.drawArrowLine(canvas, navArrow, screen);
                    }
                }
                u++;
            } catch (Exception e) {
                throw e;
            }
        }
        if (this.unitSelected != null && this.unitSelected.onScreen() && !timer.realTimeMode && this.unitSelected.getVisibleToPlayer()) {
            canvas.drawCircle((float) this.unitSelected.intScreenX, (float) this.unitSelected.intScreenY, (float) this.selectionRingRadius, this.moveCirclePaint);
        }
        if (((this.unitPlayer != null) && this.unitPlayer.onScreen()) && !timer.realTimeMode) {
            canvas.drawCircle((float) this.unitPlayer.intScreenX, (float) this.unitPlayer.intScreenY, (float) this.selectionRingRadius, this.playerUnitRingPaint);
        }
        for (int u2 = 0; u2 < this.unit.size(); u2++) {
            Unit un2 = this.unit.get(u2);
            if (un2.onScreen() && un2.getVisibleToPlayer()) {
                drawUnit(un2, canvas, screen);
                if (!timer.realTimeMode && un2.warning && un2.team == 0) {
                    if (screen.zoomedIn) {
                        canvas.drawBitmap(this.hazardImage, (float) (un2.intScreenX - 20), (float) (un2.intScreenY - 20), this.hazardPaint);
                    } else {
                        canvas.drawBitmap(this.hazardImage_small, (float) (un2.intScreenX - 10), (float) (un2.intScreenY - 10), this.hazardPaint);
                    }
                }
            }
        }
        if (this.unitSelected != null && !this.moveCircleSelected && !timer.realTimeMode && this.unitSelected.navigationPossible()) {
            canvas.drawLine((float) this.moveCircleX, (float) this.moveCircleY, this.unitSelected.fltScreenArrowHeadX, this.unitSelected.fltScreenArrowHeadY, this.moveCirclePaint);
        }
        if (!timer.realTimeMode) {
            for (int u3 = 0; u3 < this.unit.size(); u3++) {
                Unit un3 = this.unit.get(u3);
                if (un3.inGameZone() && un3.arrowInScreen && un3.getVisibleToPlayer()) {
                    un3.drawArrowHead(canvas, navArrow);
                }
            }
        }
    }

    private void drawUnit(Unit un, Canvas canvas, Screen screen) throws Exception {
        Bitmap unitImg;
        try {
            if (!screen.zoomedIn) {
                switch (un.type) {
                    case 0:
                        unitImg = this.img_hq;
                        break;
                    case 1:
                        if (!un.hasEjected) {
                            unitImg = this.img_fighter;
                            break;
                        } else {
                            unitImg = this.img_fighter_ejected;
                            break;
                        }
                    case 2:
                        if (!un.hasEjected) {
                            unitImg = this.img_turret;
                            break;
                        } else {
                            unitImg = this.img_turret_ejected;
                            break;
                        }
                    case 3:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemyscout;
                            break;
                        } else {
                            unitImg = this.img_enemyscout_ejected;
                            break;
                        }
                    case 4:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemyfighter;
                            break;
                        } else {
                            unitImg = this.img_enemyfighter_ejected;
                            break;
                        }
                    case 5:
                        if (!un.hasEjected) {
                            unitImg = this.img_cargo;
                            break;
                        } else {
                            unitImg = this.img_cargo_ejected;
                            break;
                        }
                    case 6:
                        if (!un.hasEjected) {
                            unitImg = this.img_fast;
                            break;
                        } else {
                            unitImg = this.img_fast_ejected;
                            break;
                        }
                    case 7:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemyassault;
                            break;
                        } else {
                            unitImg = this.img_enemyassault_ejected;
                            break;
                        }
                    case 8:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemyturret;
                            break;
                        } else {
                            unitImg = this.img_enemyturret_ejected;
                            break;
                        }
                    case 9:
                        unitImg = this.img_enemyhq;
                        break;
                    case 10:
                        if (!un.hasEjected) {
                            unitImg = this.img_assault;
                            break;
                        } else {
                            unitImg = this.img_assault_ejected;
                            break;
                        }
                    case 11:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemyfast;
                            break;
                        } else {
                            unitImg = this.img_enemyfast_ejected;
                            break;
                        }
                    case 12:
                        if (!un.hasEjected) {
                            unitImg = this.img_elite;
                            break;
                        } else {
                            unitImg = this.img_elite_ejected;
                            break;
                        }
                    case 13:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemydefender;
                            break;
                        } else {
                            unitImg = this.img_enemydefender_ejected;
                            break;
                        }
                    case 14:
                        if (!un.hasEjected) {
                            unitImg = this.img_defender;
                            break;
                        } else {
                            unitImg = this.img_defender_ejected;
                            break;
                        }
                    case 15:
                        if (!un.hasEjected) {
                            unitImg = this.img_heavy;
                            break;
                        } else {
                            unitImg = this.img_heavy_ejected;
                            break;
                        }
                    case 16:
                        unitImg = this.img_enemyfactory;
                        break;
                    case UNIT_ENEMYCARGO /*17*/:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemycargo;
                            break;
                        } else {
                            unitImg = this.img_enemycargo_ejected;
                            break;
                        }
                    case UNIT_ENEMYCRUISER /*18*/:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemycruiser;
                            break;
                        } else {
                            unitImg = this.img_enemycruiser_ejected;
                            break;
                        }
                    case UNIT_ENEMYCRUISERTURRET /*19*/:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemyturret;
                            break;
                        } else {
                            unitImg = this.img_enemyturret_ejected;
                            break;
                        }
                    case 20:
                        unitImg = this.img_hq2;
                        break;
                    case UNIT_ENEMYHQ2 /*21*/:
                        unitImg = this.img_enemyhq2;
                        break;
                    default:
                        unitImg = this.img_fighter;
                        break;
                }
            } else {
                switch (un.type) {
                    case 0:
                        unitImg = this.img_hq_ZO;
                        break;
                    case 1:
                        if (!un.hasEjected) {
                            unitImg = this.img_fighter_ZO;
                            break;
                        } else {
                            unitImg = this.img_fighter_ejected_ZO;
                            break;
                        }
                    case 2:
                        if (!un.hasEjected) {
                            unitImg = this.img_turret_ZO;
                            break;
                        } else {
                            unitImg = this.img_turret_ejected_ZO;
                            break;
                        }
                    case 3:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemyscout_ZO;
                            break;
                        } else {
                            unitImg = this.img_enemyscout_ejected_ZO;
                            break;
                        }
                    case 4:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemyfighter_ZO;
                            break;
                        } else {
                            unitImg = this.img_enemyfighter_ejected_ZO;
                            break;
                        }
                    case 5:
                        if (!un.hasEjected) {
                            unitImg = this.img_cargo_ZO;
                            break;
                        } else {
                            unitImg = this.img_cargo_ejected_ZO;
                            break;
                        }
                    case 6:
                        if (!un.hasEjected) {
                            unitImg = this.img_fast_ZO;
                            break;
                        } else {
                            unitImg = this.img_fast_ejected_ZO;
                            break;
                        }
                    case 7:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemyassault_ZO;
                            break;
                        } else {
                            unitImg = this.img_enemyassault_ejected_ZO;
                            break;
                        }
                    case 8:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemyturret_ZO;
                            break;
                        } else {
                            unitImg = this.img_enemyturret_ejected_ZO;
                            break;
                        }
                    case 9:
                        unitImg = this.img_enemyhq_ZO;
                        break;
                    case 10:
                        if (!un.hasEjected) {
                            unitImg = this.img_assault_ZO;
                            break;
                        } else {
                            unitImg = this.img_assault_ejected_ZO;
                            break;
                        }
                    case 11:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemyfast_ZO;
                            break;
                        } else {
                            unitImg = this.img_enemyfast_ejected_ZO;
                            break;
                        }
                    case 12:
                        if (!un.hasEjected) {
                            unitImg = this.img_elite_ZO;
                            break;
                        } else {
                            unitImg = this.img_elite_ejected_ZO;
                            break;
                        }
                    case 13:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemydefender_ZO;
                            break;
                        } else {
                            unitImg = this.img_enemydefender_ejected_ZO;
                            break;
                        }
                    case 14:
                        if (!un.hasEjected) {
                            unitImg = this.img_defender_ZO;
                            break;
                        } else {
                            unitImg = this.img_defender_ejected_ZO;
                            break;
                        }
                    case 15:
                        if (!un.hasEjected) {
                            unitImg = this.img_heavy_ZO;
                            break;
                        } else {
                            unitImg = this.img_heavy_ejected_ZO;
                            break;
                        }
                    case 16:
                        unitImg = this.img_enemyfactory_ZO;
                        break;
                    case UNIT_ENEMYCARGO /*17*/:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemycargo_ZO;
                            break;
                        } else {
                            unitImg = this.img_enemycargo_ejected_ZO;
                            break;
                        }
                    case UNIT_ENEMYCRUISER /*18*/:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemycruiser_ZO;
                            break;
                        } else {
                            unitImg = this.img_enemycruiser_ejected_ZO;
                            break;
                        }
                    case UNIT_ENEMYCRUISERTURRET /*19*/:
                        if (!un.hasEjected) {
                            unitImg = this.img_enemyturret_ZO;
                            break;
                        } else {
                            unitImg = this.img_enemyturret_ejected_ZO;
                            break;
                        }
                    case 20:
                        unitImg = this.img_hq2_ZO;
                        break;
                    case UNIT_ENEMYHQ2 /*21*/:
                        unitImg = this.img_enemyhq2_ZO;
                        break;
                    default:
                        unitImg = this.img_fighter_ZO;
                        break;
                }
            }
            un.draw(canvas, screen, unitImg, this.shipAlpha);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawUnitUI(Canvas canvas, Timer timer) throws Exception {
        try {
            if (this.unitSelected != null && this.unitSelected.navigationPossible() && !timer.realTimeMode) {
                if (!this.moveCircleSelected) {
                    canvas.drawCircle((float) this.moveCircleX, (float) this.moveCircleY, (float) this.moveCircleRadius, this.moveCirclePaint);
                    return;
                }
                canvas.drawLine(this.unitSelected.fltScreenArrowHeadX, this.unitSelected.fltScreenArrowHeadY, (float) this.moveCircleTieLineX, (float) this.moveCircleTieLineY, this.moveCirclePaint);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void startTurn(Projectiles projectiles, Map map, Units units, Asteroids asteroids, Sounds sounds, Screen screen, GameThread thread) throws Exception {
        try {
            this.mPlayWarning = false;
            for (int u = 0; u < this.unit.size(); u++) {
                Unit un = this.unit.get(u);
                if (un.inGameZone()) {
                    un.calcNoChangeValues();
                }
            }
            for (int u2 = 0; u2 < this.unit.size(); u2++) {
                Unit un2 = this.unit.get(u2);
                if (un2.inGameZone()) {
                    un2.startTurn(projectiles, map, units, asteroids, sounds, screen, thread);
                }
            }
            if (this.mPlayWarning) {
                sounds.playSound(7, (double) screen.intMiddleX, (double) screen.intMiddleY, screen, thread);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void startRealTime() throws Exception {
        int u = 0;
        while (u < this.unit.size()) {
            try {
                Unit un = this.unit.get(u);
                if (un.inGameZone()) {
                    un.warning = false;
                }
                u++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public void processNonRealTime(Screen screen, NavArrow navArrow) throws Exception {
        try {
            this.hazardPulse -= 20;
            if (this.hazardPulse < -255) {
                this.hazardPulse = 255;
            }
            this.hazardPaint.setAlpha(Math.abs(this.hazardPulse));
            this.playerUnitRingAlpha--;
            if (this.playerUnitRingAlpha < -60) {
                this.playerUnitRingAlpha = 60;
            }
            if (this.playerUnitRingAlpha < 20 && this.playerUnitRingAlpha > -20) {
                this.playerUnitRingAlpha = -20;
            }
            this.playerUnitRingPaint.setAlpha(Math.abs(this.playerUnitRingAlpha));
            if (this.unitSelected != null && this.unitSelected.navigationPossible()) {
                calculateMoveCircleXY(screen, navArrow);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void processRealTime(Map map, Sounds sounds, Projectiles projectiles, Screen screen, GameThread thread) throws Exception {
        for (int i = 0; i < 2; i++) {
            checkAISearchZone(i, map);
        }
        int i2 = 0;
        while (i2 < this.unit.size()) {
            try {
                Unit u = this.unit.get(i2);
                if (u.inGameZone()) {
                    fireUnit(u, projectiles, sounds, screen, thread);
                }
                if (u.ejectOrder) {
                    u.eject(map, sounds, screen, thread);
                }
                i2++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    private void fireUnit(Unit u, Projectiles projectiles, Sounds sounds, Screen screen, GameThread thread) throws Exception {
        int rocketCount = 0;
        int w = 0;
        while (w < u.inventory.size()) {
            try {
                InventorySlot inv = u.inventory.get(w);
                if (inv.weapon && inv.firing) {
                    if (inv.type == 7 && u.gunRefire == 0 && inv.value > 0) {
                        inv.value--;
                        u.gunRefire = 10;
                        projectiles.makeBullet(u.fltWorldX, u.fltWorldY, u.fltAngle, u.intCollisionHalfHeight, u, inv.side, sounds, screen, thread);
                    }
                    if (inv.type != 7) {
                        if (u.rocketRefire == 0 && inv.value > 0 && rocketCount == u.rocketFireCount) {
                            inv.value--;
                            u.rocketRefire = 15;
                            u.rocketFireCount++;
                            projectiles.makeMissile(u.fltWorldX, u.fltWorldY, u.fltAngle, u.intCollisionHalfHeight, inv.type, u, inv.side, sounds, screen, thread);
                        }
                        rocketCount++;
                    }
                }
                w++;
            } catch (Exception e) {
                throw e;
            }
        }
        if (u.gunRefire > 0) {
            u.gunRefire--;
        }
        if (u.rocketRefire > 0) {
            u.rocketRefire--;
        }
    }

    public void setAISearchZone(double x, double y, int aiTeam, Map map) throws Exception {
        try {
            double maxX = (double) ((map.worldSizeX - 150) - 600);
            double maxY = (double) ((map.worldSizeY - 150) - 600);
            if (x < 750.0d) {
                x = 750.0d;
            }
            if (x > maxX) {
                x = maxX;
            }
            if (y < 750.0d) {
                y = 750.0d;
            }
            if (y > maxY) {
                y = maxY;
            }
            this.AI_ZoneSearchX[aiTeam] = x;
            this.AI_ZoneSearchY[aiTeam] = y;
            this.AI_ZoneSearched[aiTeam] = false;
            this.AI_SearchZoneWait[aiTeam] = 120;
            for (int i = 0; i < this.unit.size(); i++) {
                Unit u = this.unit.get(i);
                if (u.inGameZone() && u.team == aiTeam) {
                    u.AI_SetSearchZone(this.AI_ZoneSearchX[aiTeam], this.AI_ZoneSearchY[aiTeam], false);
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void checkAISearchZone(int aiTeam, Map map) throws Exception {
        try {
            if (this.AI_ZoneSearched[aiTeam]) {
                double x = 750.0d + (Math.random() * (((double) ((map.worldSizeX - 150) - 600)) - 750.0d));
                double y = 750.0d + (Math.random() * (((double) ((map.worldSizeY - 150) - 600)) - 750.0d));
                if (Math.abs(x - this.AI_ZoneSearchX[aiTeam]) < 300.0d && Math.abs(y - this.AI_ZoneSearchY[aiTeam]) < 300.0d) {
                    x = ((double) map.worldSizeX) - x;
                    y = ((double) map.worldSizeY) - y;
                }
                setAISearchZone(x, y, aiTeam, map);
            }
            for (int i = 0; i < this.unit.size(); i++) {
                if (this.AI_SearchZoneWait[aiTeam] > 0) {
                    Unit u = this.unit.get(i);
                    if (u.inGameZone() && u.team == aiTeam && Math.abs(u.fltWorldX - this.AI_ZoneSearchX[aiTeam]) < 300.0d && Math.abs(u.fltWorldY - this.AI_ZoneSearchY[aiTeam]) < 300.0d) {
                        int[] iArr = this.AI_SearchZoneWait;
                        iArr[aiTeam] = iArr[aiTeam] - 1;
                        if (this.AI_SearchZoneWait[aiTeam] <= 0) {
                            this.AI_ZoneSearched[aiTeam] = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public String getSaveString() throws Exception {
        String saveString = "";
        int i = 0;
        while (i < this.unit.size()) {
            try {
                saveString = String.valueOf(saveString) + this.unit.get(i).getSaveString(this) + "A";
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
        for (int i2 = 0; i2 < 2; i2++) {
            saveString = String.valueOf(String.valueOf(String.valueOf(String.valueOf(saveString) + String.valueOf((int) this.AI_ZoneSearchX[i2]) + "A") + String.valueOf((int) this.AI_ZoneSearchY[i2]) + "A") + Functions.toStr(this.AI_ZoneSearched[i2]) + "A") + String.valueOf(this.AI_SearchZoneWait[i2]) + "A";
        }
        return saveString;
    }

    public void restoreFromSaveString(String saveString) throws Exception {
        Exception e;
        int dataPos;
        int dataPos2 = 0;
        try {
            String[] unitData = saveString.split("A");
            int i = 0;
            while (true) {
                try {
                    dataPos = dataPos2;
                    if (i >= this.unit.size()) {
                        break;
                    }
                    dataPos2 = dataPos + 1;
                    this.unit.get(i).restoreFromSaveString(unitData[dataPos]);
                    this.numUnits = i + 1;
                    i++;
                } catch (Exception e2) {
                    e = e2;
                    throw e;
                }
            }
            for (int i2 = 0; i2 < 2; i2++) {
                int dataPos3 = dataPos + 1;
                this.AI_ZoneSearchX[i2] = (double) Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos3 + 1;
                this.AI_ZoneSearchY[i2] = (double) Integer.parseInt(unitData[dataPos3]);
                int dataPos4 = dataPos + 1;
                this.AI_ZoneSearched[i2] = Functions.toBoolean(unitData[dataPos]);
                dataPos = dataPos4 + 1;
                this.AI_SearchZoneWait[i2] = Integer.parseInt(unitData[dataPos4]);
            }
        } catch (Exception e3) {
            e = e3;
            throw e;
        }
    }

    public void convertIDsToReferences() {
        for (int i = 0; i < this.unit.size(); i++) {
            Unit u = this.unit.get(i);
            u.attachedTo = getUnitFromIndex(u.loadAttachedToID);
            u.AI_MissionUnit = getUnitFromIndex(u.loadAI_MissionUnitID);
        }
    }

    public boolean allEnemiesGone() {
        for (int i = 0; i < this.unit.size(); i++) {
            Unit u = this.unit.get(i);
            if (u.team == 1) {
                if (u.shipAlive && !u.isSafe) {
                    return false;
                }
                if (u.pod.podLaunched && u.pod.podAlive && !u.pod.podSafe) {
                    return false;
                }
            }
        }
        return true;
    }

    public Unit makeUnitHQ(float x, float y, float angle, int team) throws Exception {
        try {
            this.unit.add(new Unit(0, x, y, angle, 0.5f, 0.0f, 0.0f, 0.0f, this.img_hq_ZO.getWidth(), this.img_hq_ZO.getHeight(), (int) (((double) this.img_hq_ZO.getWidth()) * 0.95d), (int) (((double) this.img_hq_ZO.getHeight()) * 0.95d), false, false, true, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(3, 0, 2);
            returnUnit.addInventory(-1, 0, 2);
            returnUnit.addInventory(6, 0, 3);
            returnUnit.addInventory(-1, 0, 3);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addArmour(0, 10);
            returnUnit.addArmour(1, 10);
            returnUnit.addArmour(2, 10);
            returnUnit.addArmour(3, 10);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitFighter(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(1, x, y, angle, 0.95f, 0.82f, 4.25f, 1.0f, this.img_fighter_ZO.getWidth(), this.img_fighter_ZO.getHeight(), (int) (((double) this.img_fighter_ZO.getWidth()) * 0.95d), (int) (((double) this.img_fighter_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(2, 0, 2);
            returnUnit.addInventory(3, 0, 3);
            returnUnit.addInventory(6, 0, 3);
            returnUnit.addInventory(4, 0, 0);
            returnUnit.addInventory(7, 100, 0);
            returnUnit.addInventory(5, 0, 1);
            returnUnit.addInventory(9, 10, 1);
            returnUnit.addArmour(0, 2);
            returnUnit.addArmour(1, 2);
            returnUnit.addArmour(2, 3);
            returnUnit.addArmour(3, 1);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitTurret(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(2, x, y, angle, 0.9f, 0.0f, 0.0f, 1.0f, this.img_turret_ZO.getWidth(), this.img_turret_ZO.getHeight(), (int) (((double) this.img_turret_ZO.getWidth()) * 0.95d), (int) (((double) this.img_turret_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(2, 0, 3);
            returnUnit.addInventory(3, 0, 3);
            returnUnit.addInventory(6, 0, 2);
            returnUnit.addInventory(7, 100, 0);
            returnUnit.addInventory(4, 0, 1);
            returnUnit.addArmour(0, 2);
            returnUnit.addArmour(1, 2);
            returnUnit.addArmour(2, 4);
            returnUnit.addArmour(3, 1);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitEnemyScout(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(3, x, y, angle, 0.95f, 0.65f, 3.5f, 0.8f, this.img_enemyscout_ZO.getWidth(), this.img_enemyscout_ZO.getHeight(), (int) (((double) this.img_enemyscout_ZO.getWidth()) * 0.95d), (int) (((double) this.img_enemyscout_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(3, 0, 2);
            returnUnit.addInventory(2, 0, 2);
            returnUnit.addInventory(6, 0, 3);
            returnUnit.addInventory(5, 0, 3);
            returnUnit.addInventory(7, 50, 0);
            returnUnit.addInventory(4, 0, 1);
            returnUnit.addArmour(0, 1);
            returnUnit.addArmour(1, 1);
            returnUnit.addArmour(2, 1);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitEnemyFighter(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(4, x, y, angle, 0.95f, 0.8f, 4.2f, 0.9f, this.img_enemyfighter_ZO.getWidth(), this.img_enemyfighter_ZO.getHeight(), (int) (((double) this.img_enemyfighter_ZO.getWidth()) * 0.95d), (int) (((double) this.img_enemyfighter_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(2, 0, 2);
            returnUnit.addInventory(3, 0, 3);
            returnUnit.addInventory(6, 0, 3);
            returnUnit.addInventory(4, 0, 0);
            returnUnit.addInventory(7, 80, 0);
            returnUnit.addInventory(5, 0, 1);
            returnUnit.addInventory(9, 8, 1);
            returnUnit.addArmour(0, 2);
            returnUnit.addArmour(1, 2);
            returnUnit.addArmour(2, 2);
            returnUnit.addArmour(3, 1);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitCargo(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(5, x, y, angle, 0.95f, 0.25f, 2.4f, 0.3f, this.img_cargo_ZO.getWidth(), this.img_cargo_ZO.getHeight(), (int) (((double) this.img_cargo_ZO.getWidth()) * 0.95d), (int) (((double) this.img_cargo_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(2, 0, 2);
            returnUnit.addInventory(-1, 0, 2);
            returnUnit.addInventory(3, 0, 3);
            returnUnit.addInventory(6, 0, 3);
            returnUnit.addInventory(5, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(4, 0, 0);
            returnUnit.addInventory(5, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(4, 0, 1);
            returnUnit.addArmour(0, 6);
            returnUnit.addArmour(1, 6);
            returnUnit.addArmour(2, 4);
            returnUnit.addArmour(3, 2);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitFast(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(6, x, y, angle, 0.95f, 0.95f, 5.25f, 1.1f, this.img_fast_ZO.getWidth(), this.img_fast_ZO.getHeight(), (int) (((double) this.img_fast_ZO.getWidth()) * 0.95d), (int) (((double) this.img_fast_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(6, 0, 2);
            returnUnit.addInventory(2, 0, 3);
            returnUnit.addInventory(3, 0, 3);
            returnUnit.addInventory(4, 0, 0);
            returnUnit.addInventory(9, 12, 0);
            returnUnit.addInventory(5, 0, 1);
            returnUnit.addInventory(7, 100, 1);
            returnUnit.addArmour(0, 3);
            returnUnit.addArmour(1, 3);
            returnUnit.addArmour(2, 3);
            returnUnit.addArmour(3, 2);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitEnemyAssault(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(7, x, y, angle, 0.95f, 0.75f, 4.0f, 0.8f, this.img_enemyassault_ZO.getWidth(), this.img_enemyassault_ZO.getHeight(), (int) (((double) this.img_enemyassault_ZO.getWidth()) * 0.95d), (int) (((double) this.img_enemyassault_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(2, 0, 2);
            returnUnit.addInventory(6, 0, 2);
            returnUnit.addInventory(3, 0, 3);
            returnUnit.addInventory(5, 0, 0);
            returnUnit.addInventory(4, 0, 0);
            returnUnit.addInventory(7, 80, 0);
            returnUnit.addInventory(5, 0, 1);
            returnUnit.addInventory(8, 6, 1);
            returnUnit.addInventory(8, 6, 1);
            returnUnit.addArmour(0, 4);
            returnUnit.addArmour(1, 4);
            returnUnit.addArmour(2, 5);
            returnUnit.addArmour(3, 2);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitEnemyTurret(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(8, x, y, angle, 0.9f, 0.0f, 0.0f, 0.9f, this.img_enemyturret_ZO.getWidth(), this.img_enemyturret_ZO.getHeight(), (int) (((double) this.img_enemyturret_ZO.getWidth()) * 0.95d), (int) (((double) this.img_enemyturret_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(2, 0, 3);
            returnUnit.addInventory(3, 0, 3);
            returnUnit.addInventory(6, 0, 2);
            returnUnit.addInventory(7, 100, 0);
            returnUnit.addInventory(4, 0, 1);
            returnUnit.addArmour(0, 2);
            returnUnit.addArmour(1, 2);
            returnUnit.addArmour(2, 4);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitEnemyHQ(float x, float y, float angle, int team) throws Exception {
        try {
            this.unit.add(new Unit(9, x, y, angle, 0.5f, 0.0f, 0.0f, 0.0f, this.img_enemyhq_ZO.getWidth(), this.img_enemyhq_ZO.getHeight(), (int) (((double) this.img_enemyhq_ZO.getWidth()) * 0.95d), (int) (((double) this.img_enemyhq_ZO.getHeight()) * 0.95d), false, false, true, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(3, 0, 2);
            returnUnit.addInventory(6, 0, 2);
            returnUnit.addInventory(-1, 0, 3);
            returnUnit.addInventory(-1, 0, 3);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addArmour(0, 10);
            returnUnit.addArmour(1, 10);
            returnUnit.addArmour(2, 10);
            returnUnit.addArmour(3, 10);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitAssault(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(10, x, y, angle, 0.95f, 0.8f, 4.1f, 0.9f, this.img_assault_ZO.getWidth(), this.img_assault_ZO.getHeight(), (int) (((double) this.img_assault_ZO.getWidth()) * 0.95d), (int) (((double) this.img_assault_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(2, 0, 2);
            returnUnit.addInventory(6, 0, 3);
            returnUnit.addInventory(3, 0, 3);
            returnUnit.addInventory(5, 0, 0);
            returnUnit.addInventory(4, 0, 0);
            returnUnit.addInventory(7, 80, 0);
            returnUnit.addInventory(5, 0, 1);
            returnUnit.addInventory(8, 8, 1);
            returnUnit.addInventory(8, 8, 1);
            returnUnit.addArmour(0, 5);
            returnUnit.addArmour(1, 5);
            returnUnit.addArmour(2, 6);
            returnUnit.addArmour(3, 3);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitEnemyFast(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(11, x, y, angle, 0.95f, 0.85f, 4.5f, 1.05f, this.img_enemyfast_ZO.getWidth(), this.img_enemyfast_ZO.getHeight(), (int) (((double) this.img_enemyfast_ZO.getWidth()) * 0.95d), (int) (((double) this.img_enemyfast_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(3, 0, 2);
            returnUnit.addInventory(2, 0, 3);
            returnUnit.addInventory(6, 0, 3);
            returnUnit.addInventory(4, 0, 0);
            returnUnit.addInventory(9, 10, 0);
            returnUnit.addInventory(5, 0, 1);
            returnUnit.addInventory(7, 80, 1);
            returnUnit.addArmour(0, 3);
            returnUnit.addArmour(1, 3);
            returnUnit.addArmour(2, 2);
            returnUnit.addArmour(3, 1);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitElite(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(12, x, y, angle, 0.95f, 0.9f, 5.0f, 1.1f, this.img_elite_ZO.getWidth(), this.img_elite_ZO.getHeight(), (int) (((double) this.img_elite_ZO.getWidth()) * 0.95d), (int) (((double) this.img_elite_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(2, 0, 2);
            returnUnit.addInventory(3, 0, 2);
            returnUnit.addInventory(6, 0, 3);
            returnUnit.addInventory(5, 0, 0);
            returnUnit.addInventory(4, 0, 0);
            returnUnit.addInventory(7, 200, 0);
            returnUnit.addInventory(11, 2, 0);
            returnUnit.addInventory(5, 0, 1);
            returnUnit.addInventory(4, 0, 1);
            returnUnit.addInventory(9, 20, 1);
            returnUnit.addInventory(8, 6, 1);
            returnUnit.addArmour(0, 6);
            returnUnit.addArmour(1, 6);
            returnUnit.addArmour(2, 6);
            returnUnit.addArmour(3, 4);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitEnemyDefender(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(13, x, y, angle, 0.95f, 0.7f, 3.2f, 0.85f, this.img_enemydefender_ZO.getWidth(), this.img_enemydefender_ZO.getHeight(), (int) (((double) this.img_enemydefender_ZO.getWidth()) * 0.95d), (int) (((double) this.img_enemydefender_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(2, 0, 2);
            returnUnit.addInventory(3, 0, 3);
            returnUnit.addInventory(6, 0, 3);
            returnUnit.addInventory(4, 0, 0);
            returnUnit.addInventory(7, 200, 0);
            returnUnit.addInventory(5, 0, 1);
            returnUnit.addInventory(11, 1, 1);
            returnUnit.addInventory(9, 8, 1);
            returnUnit.addArmour(0, 6);
            returnUnit.addArmour(1, 6);
            returnUnit.addArmour(2, 6);
            returnUnit.addArmour(3, 4);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitDefender(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(14, x, y, angle, 0.95f, 0.65f, 3.2f, 0.85f, this.img_defender_ZO.getWidth(), this.img_defender_ZO.getHeight(), (int) (((double) this.img_defender_ZO.getWidth()) * 0.95d), (int) (((double) this.img_defender_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(2, 0, 2);
            returnUnit.addInventory(3, 0, 2);
            returnUnit.addInventory(6, 0, 3);
            returnUnit.addInventory(4, 0, 0);
            returnUnit.addInventory(10, 1, 0);
            returnUnit.addInventory(7, 100, 0);
            returnUnit.addInventory(5, 0, 1);
            returnUnit.addInventory(11, 2, 1);
            returnUnit.addInventory(9, 12, 1);
            returnUnit.addArmour(0, 6);
            returnUnit.addArmour(1, 6);
            returnUnit.addArmour(2, 6);
            returnUnit.addArmour(3, 4);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitHeavy(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(15, x, y, angle, 0.95f, 0.7f, 3.6f, 0.9f, this.img_heavy_ZO.getWidth(), this.img_heavy_ZO.getHeight(), (int) (((double) this.img_heavy_ZO.getWidth()) * 0.95d), (int) (((double) this.img_heavy_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(2, 0, 2);
            returnUnit.addInventory(6, 0, 3);
            returnUnit.addInventory(3, 0, 3);
            returnUnit.addInventory(5, 0, 0);
            returnUnit.addInventory(4, 0, 0);
            returnUnit.addInventory(7, 80, 0);
            returnUnit.addInventory(10, 6, 0);
            returnUnit.addInventory(5, 0, 1);
            returnUnit.addInventory(4, 0, 1);
            returnUnit.addInventory(8, 14, 1);
            returnUnit.addInventory(8, 14, 1);
            returnUnit.addArmour(0, 6);
            returnUnit.addArmour(1, 6);
            returnUnit.addArmour(2, 8);
            returnUnit.addArmour(3, 4);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitEnemyFactory(float x, float y, float angle, int team) throws Exception {
        try {
            this.unit.add(new Unit(16, x, y, angle, 0.5f, 0.0f, 0.0f, 0.0f, this.img_enemyfactory_ZO.getWidth(), this.img_enemyfactory_ZO.getHeight(), (int) (((double) this.img_enemyfactory_ZO.getWidth()) * 0.95d), (int) (((double) this.img_enemyfactory_ZO.getHeight()) * 0.95d), false, false, true, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(3, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 3);
            returnUnit.addInventory(-1, 0, 3);
            returnUnit.addInventory(-1, 0, 2);
            returnUnit.addInventory(-1, 0, 2);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(6, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addArmour(0, 12);
            returnUnit.addArmour(1, 12);
            returnUnit.addArmour(2, 8);
            returnUnit.addArmour(3, 8);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitEnemyCargo(float x, float y, float angle, boolean player, boolean playable, int team) throws Exception {
        try {
            this.unit.add(new Unit(17, x, y, angle, 0.95f, 0.25f, 2.4f, 0.3f, this.img_enemycargo_ZO.getWidth(), this.img_enemycargo_ZO.getHeight(), (int) (((double) this.img_enemycargo_ZO.getWidth()) * 0.95d), (int) (((double) this.img_enemycargo_ZO.getHeight()) * 0.95d), player, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(2, 0, 2);
            returnUnit.addInventory(3, 0, 2);
            returnUnit.addInventory(6, 0, 3);
            returnUnit.addInventory(-1, 0, 3);
            returnUnit.addInventory(5, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(4, 0, 0);
            returnUnit.addInventory(5, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(4, 0, 1);
            returnUnit.addArmour(0, 4);
            returnUnit.addArmour(1, 4);
            returnUnit.addArmour(2, 4);
            returnUnit.addArmour(3, 2);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitEnemyCruiser(float x, float y, float angle, boolean player, boolean playable, int team, Map map) throws Exception {
        Exception e;
        try {
            this.unit.add(new Unit(18, x, y, angle, 0.95f, 0.5f, 2.0f, 0.3f, this.img_enemycruiser_ZO.getWidth(), this.img_enemycruiser_ZO.getHeight(), (int) (((double) this.img_enemycruiser_ZO.getWidth()) * 0.95d), (int) (((double) this.img_enemycruiser_ZO.getHeight()) * 0.95d), player, playable, false, team, true));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            try {
                returnUnit.addInventory(2, 0, 2);
                returnUnit.addInventory(3, 0, 2);
                returnUnit.addInventory(6, 0, 3);
                returnUnit.addInventory(5, 0, 3);
                returnUnit.addInventory(5, 0, 0);
                returnUnit.addInventory(4, 0, 0);
                returnUnit.addInventory(4, 0, 0);
                returnUnit.addInventory(10, 6, 0);
                returnUnit.addInventory(5, 0, 1);
                returnUnit.addInventory(4, 0, 1);
                returnUnit.addInventory(4, 0, 1);
                returnUnit.addInventory(10, 6, 1);
                returnUnit.addArmour(0, 9);
                returnUnit.addArmour(1, 9);
                returnUnit.addArmour(2, 13);
                returnUnit.addArmour(3, 7);
                float offset = ((float) this.img_enemycruiser_ZO.getWidth()) * 0.8f;
                makeUnitEnemyCruiserTurret(returnUnit, 32.236f, offset, 45.0f, 60.0f, playable, team, map);
                makeUnitEnemyCruiserTurret(returnUnit, 327.763f, offset, 315.0f, 60.0f, playable, team, map);
                makeUnitEnemyCruiserTurret(returnUnit, 147.763f, offset, 135.0f, 60.0f, playable, team, map);
                makeUnitEnemyCruiserTurret(returnUnit, 212.236f, offset, 225.0f, 60.0f, playable, team, map);
                return returnUnit;
            } catch (Exception e2) {
                e = e2;
                throw e;
            }
        } catch (Exception e3) {
            e = e3;
            throw e;
        }
    }

    public Unit makeUnitEnemyCruiserTurret(Unit u, float ang, float dist, float centerAng, float pivotLimit, boolean playable, int team, Map map) throws Exception {
        Exception e;
        try {
            this.unit.add(new Unit(19, 500.0f, 500.0f, 0.0f, 0.9f, 0.0f, 0.0f, 0.9f, this.img_enemyturret_ZO.getWidth(), this.img_enemyturret_ZO.getHeight(), (int) (((double) this.img_enemyturret_ZO.getWidth()) * 0.95d), (int) (((double) this.img_enemyturret_ZO.getHeight()) * 0.95d), false, playable, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            try {
                returnUnit.addInventory(2, 0, 3);
                returnUnit.addInventory(3, 0, 3);
                returnUnit.addInventory(6, 0, 2);
                returnUnit.addInventory(7, 100, 0);
                returnUnit.addInventory(9, 20, 0);
                returnUnit.addInventory(4, 0, 1);
                returnUnit.addInventory(11, 4, 1);
                returnUnit.addArmour(0, 6);
                returnUnit.addArmour(1, 6);
                returnUnit.addArmour(2, 8);
                returnUnit.attachToUnit(u, ang, dist, centerAng, pivotLimit, map, this);
                return returnUnit;
            } catch (Exception e2) {
                e = e2;
                throw e;
            }
        } catch (Exception e3) {
            e = e3;
            throw e;
        }
    }

    public Unit makeUnitHQ2(float x, float y, float angle, int team) throws Exception {
        try {
            this.unit.add(new Unit(20, x, y, angle, 0.5f, 0.0f, 0.0f, 0.0f, this.img_hq_ZO.getWidth(), this.img_hq_ZO.getHeight(), (int) (((double) this.img_hq_ZO.getWidth()) * 0.95d), (int) (((double) this.img_hq_ZO.getHeight()) * 0.95d), false, false, true, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(3, 0, 3);
            returnUnit.addInventory(-1, 0, 3);
            returnUnit.addInventory(6, 0, 2);
            returnUnit.addInventory(-1, 0, 2);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addArmour(0, 13);
            returnUnit.addArmour(1, 13);
            returnUnit.addArmour(2, 13);
            returnUnit.addArmour(3, 13);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit makeUnitEnemyHQ2(float x, float y, float angle, int team) throws Exception {
        try {
            this.unit.add(new Unit(21, x, y, angle, 0.5f, 0.0f, 0.0f, 1.0f, this.img_enemyhq2_ZO.getWidth(), this.img_enemyhq2_ZO.getHeight(), (int) (((double) this.img_enemyhq2_ZO.getWidth()) * 0.95d), (int) (((double) this.img_enemyhq2_ZO.getHeight()) * 0.95d), false, false, false, team, false));
            Unit returnUnit = this.unit.get(this.unit.size() - 1);
            returnUnit.addInventory(3, 0, 3);
            returnUnit.addInventory(6, 0, 3);
            returnUnit.addInventory(4, 0, 2);
            returnUnit.addInventory(2, 0, 2);
            returnUnit.addInventory(11, 8, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(-1, 0, 0);
            returnUnit.addInventory(11, 8, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addInventory(-1, 0, 1);
            returnUnit.addArmour(0, 13);
            returnUnit.addArmour(1, 13);
            returnUnit.addArmour(2, 13);
            returnUnit.addArmour(3, 13);
            return returnUnit;
        } catch (Exception e) {
            throw e;
        }
    }
}
