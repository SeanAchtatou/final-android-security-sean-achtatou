package com.hangfire.spacesquadronFREE;

import android.content.Context;

public final class MissionData {
    public static final int MISSIONSTATUS_FAILURE = 2;
    public static final int MISSIONSTATUS_NONE = 0;
    public static final int MISSIONSTATUS_SUCCESS = 1;
    public static final int NUMBER_OF_MISSIONS = 2;
    private int mBackgroundImgID = 0;
    private String mDescription = "";
    private String mName = "";
    private int mNumber = 0;
    private String mReason = "";
    private int mSoundID = 0;
    private int mStartX = 0;
    private int mStartY = 0;
    private int mStatus = 0;
    private int mWorldSizeX = 0;
    private int mWorldSizeY = 0;
    private Unit[] unitRef;

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public MissionData(Context context, int missionNumber) throws Exception {
        try {
            this.mNumber = missionNumber;
            switch (this.mNumber) {
                case 1:
                    Mission1_SetDetails(context);
                    return;
                case 2:
                    Mission2_SetDetails(context);
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            throw e;
        }
        throw e;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void setup(Units units, Asteroids asteroids, Map map) throws Exception {
        try {
            this.unitRef = null;
            switch (this.mNumber) {
                case 1:
                    Mission1_Load(units, asteroids, map);
                    return;
                case 2:
                    Mission2_Load(units, asteroids, map);
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            throw e;
        }
        throw e;
    }

    public int getUpdatedStatus(Units units) throws Exception {
        try {
            switch (this.mNumber) {
                case 1:
                    Mission1_SetStatus(units);
                    break;
                case 2:
                    Mission2_SetStatus(units);
                    break;
            }
            return this.mStatus;
        } catch (Exception e) {
            throw e;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void calculateAwards(Units units, CareerRecords cRecs) throws Exception {
        try {
            switch (this.mNumber) {
                case 1:
                    Mission1_SetAwards(units, cRecs);
                    return;
                case 2:
                    Mission2_SetAwards(units, cRecs);
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            throw e;
        }
        throw e;
    }

    public String getName() {
        return this.mName;
    }

    public String getDescription() {
        return this.mDescription;
    }

    public int getSoundID() {
        return this.mSoundID;
    }

    public int getMissionNumber() {
        return this.mNumber;
    }

    public int getWorldSizeX() {
        return this.mWorldSizeX;
    }

    public int getWorldSizeY() {
        return this.mWorldSizeY;
    }

    public int getBackgroundImageID() {
        return this.mBackgroundImgID;
    }

    public int getStartX() {
        return this.mStartX;
    }

    public int getStartY() {
        return this.mStartY;
    }

    public int getStatus() {
        return this.mStatus;
    }

    public String getReason() {
        return this.mReason;
    }

    public void setReason(String reason) {
        this.mReason = reason;
    }

    private void Mission1_SetDetails(Context context) throws Exception {
        try {
            this.mName = context.getString(R.string.mission1_name);
            this.mDescription = context.getString(R.string.mission1_description);
            this.mSoundID = R.raw.briefmission1;
            this.mWorldSizeX = 3000;
            this.mWorldSizeY = 3000;
            this.mStartX = 1500;
            this.mStartY = 600;
            this.mBackgroundImgID = R.drawable.backdrop1;
        } catch (Exception e) {
            throw e;
        }
    }

    private void Mission1_Load(Units units, Asteroids asteroids, Map map) throws Exception {
        try {
            this.unitRef = new Unit[3];
            this.unitRef[0] = units.makeUnit(3, 1200, 2400, 180, false, false, 1, map);
            this.unitRef[1] = units.makeUnit(3, 1600, 2400, 180, false, false, 1, map);
            this.unitRef[2] = units.makeUnit(1, 1500, 600, 0, true, true, 0, null);
            asteroids.makeAsteroid(2, 1600.0f, 1200.0f, 120.0f, 0.2f);
            asteroids.makeAsteroid(1, 1000.0f, 1000.0f, 70.0f, 0.3f);
            asteroids.makeAsteroid(1, 500.0f, 2000.0f, 140.0f, 0.35f);
            asteroids.makeAsteroid(0, 2200.0f, 600.0f, 320.0f, 0.5f);
            asteroids.makeAsteroid(0, 1700.0f, 700.0f, 30.0f, 0.45f);
            this.unitRef[0].AI_SetMission_AttackUnit(this.unitRef[2]);
            this.unitRef[1].AI_SetMission_AttackUnit(this.unitRef[2]);
        } catch (Exception e) {
            throw e;
        }
    }

    private void Mission1_SetStatus(Units units) throws Exception {
        try {
            this.mStatus = 0;
            if (this.unitRef[0].shipAlive && this.unitRef[0].hasPilot && !this.unitRef[0].isSafe) {
                return;
            }
            if (!this.unitRef[1].shipAlive || !this.unitRef[1].hasPilot || this.unitRef[1].isSafe) {
                this.mStatus = 1;
                this.mReason = "";
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void Mission1_SetAwards(Units units, CareerRecords cRecs) throws Exception {
        try {
            cRecs.getStats().addBraveryInTheAir();
            if (cRecs.getCurrent().getRank() <= 1 && !this.unitRef[0].shipAlive && !this.unitRef[1].shipAlive) {
                cRecs.getStats().addPromotion();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void Mission2_SetDetails(Context context) throws Exception {
        try {
            this.mName = context.getString(R.string.mission2_name);
            this.mDescription = context.getString(R.string.mission2_description);
            this.mSoundID = R.raw.briefmission2;
            this.mWorldSizeX = 3000;
            this.mWorldSizeY = 3000;
            this.mStartX = 1500;
            this.mStartY = 1200;
            this.mBackgroundImgID = R.drawable.backdrop1;
        } catch (Exception e) {
            throw e;
        }
    }

    private void Mission2_Load(Units units, Asteroids asteroids, Map map) throws Exception {
        try {
            this.unitRef = new Unit[7];
            this.unitRef[0] = units.makeUnit(3, 400, 500, 90, false, false, 1, map);
            this.unitRef[1] = units.makeUnit(4, 350, 600, 90, false, false, 1, map);
            int randomY = 500;
            if (Math.random() > 0.5d) {
                randomY = 2400;
            }
            this.unitRef[2] = units.makeUnit(4, 2650, randomY + 100, 270, false, false, 1, map);
            this.unitRef[6] = units.makeUnit(0, 1500, 1200, 90, false, false, 0, null);
            this.unitRef[4] = units.makeUnit(2, 1500, 1400, 0, false, true, 0, null);
            this.unitRef[5] = units.makeUnit(2, 1500, 1000, 180, false, true, 0, null);
            this.unitRef[3] = units.makeUnit(1, 1700, 1200, 45, true, true, 0, null);
            asteroids.makeAsteroid(2, 900.0f, 1300.0f, 120.0f, 0.0f);
            asteroids.makeAsteroid(1, 1500.0f, 800.0f, 70.0f, 0.3f);
            asteroids.makeAsteroid(1, 100.0f, 1200.0f, 140.0f, 0.35f);
            asteroids.makeAsteroid(0, 200.0f, 400.0f, 60.0f, 0.5f);
            asteroids.makeAsteroid(0, 700.0f, 2700.0f, 110.0f, 0.45f);
            this.unitRef[0].AI_SetMission_AttackUnit(this.unitRef[6]);
            this.unitRef[1].AI_SetMission_DefendUnit(this.unitRef[0], 1000, 90, 100);
            this.unitRef[2].AI_SetMission_AttackUnit(this.unitRef[3]);
        } catch (Exception e) {
            throw e;
        }
    }

    private void Mission2_SetStatus(Units units) throws Exception {
        try {
            this.mStatus = 0;
            if ((!this.unitRef[0].shipAlive || !this.unitRef[0].hasPilot || this.unitRef[0].isSafe) && ((!this.unitRef[1].shipAlive || !this.unitRef[1].hasPilot || this.unitRef[1].isSafe) && (!this.unitRef[2].shipAlive || !this.unitRef[2].hasPilot || this.unitRef[2].isSafe))) {
                this.mStatus = 1;
                this.mReason = "";
            }
            if (!this.unitRef[6].shipAlive) {
                this.mStatus = 2;
                this.mReason = "Our base HQ was destroyed.";
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void Mission2_SetAwards(Units units, CareerRecords cRecs) throws Exception {
        try {
            if (cRecs.getCurrent().getRank() <= 2 && !this.unitRef[0].shipAlive && !this.unitRef[1].shipAlive && !this.unitRef[2].shipAlive) {
                cRecs.getStats().addPromotion();
            }
            if (this.unitRef[4].shipAlive && this.unitRef[4].hasPilot && this.unitRef[5].shipAlive && this.unitRef[5].hasPilot && this.unitRef[3].shipAlive && this.unitRef[3].hasPilot) {
                cRecs.getStats().addMeritoriousServiceMedal();
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
