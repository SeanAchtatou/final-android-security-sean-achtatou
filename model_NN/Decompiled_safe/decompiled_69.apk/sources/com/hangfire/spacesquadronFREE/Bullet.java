package com.hangfire.spacesquadronFREE;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

public final class Bullet {
    public float ang;
    public int life;
    public int loadOwnerID;
    public Unit owner;
    public double x;
    public float xspd;
    public double y;
    public float yspd;

    public Bullet(double xs, double ys, float yang, Unit own, float xspeed, float yspeed) throws Exception {
        try {
            this.x = xs;
            this.y = ys;
            this.ang = yang;
            this.owner = own;
            this.xspd = xspeed;
            this.yspd = yspeed;
            this.life = 75;
        } catch (Exception e) {
            throw e;
        }
    }

    public int move() throws Exception {
        try {
            this.x += (double) this.xspd;
            this.y += (double) this.yspd;
            this.life--;
            return this.life;
        } catch (Exception e) {
            throw e;
        }
    }

    public void draw(Canvas canvas, Screen screen, boolean zoomed, Drawable img) throws Exception {
        try {
            int intScreenX = screen.canvasX((int) this.x);
            int intScreenY = screen.canvasY((int) this.y);
            if (screen.inScreenWorldValues(this.x, this.y, 1, 1)) {
                canvas.save();
                canvas.rotate(this.ang, (float) intScreenX, (float) intScreenY);
                if (zoomed) {
                    img.setBounds(intScreenX - 1, intScreenY - 8, intScreenX + 1, intScreenY + 8);
                } else {
                    img.setBounds(intScreenX - 1, intScreenY - 4, intScreenX, intScreenY + 4);
                }
                if (this.life > 10) {
                    img.setAlpha(255);
                } else {
                    img.setAlpha(this.life * 25);
                }
                img.draw(canvas);
                canvas.restore();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public String getSaveString(Units units) throws Exception {
        try {
            return String.valueOf(String.valueOf((int) this.x)) + "B" + String.valueOf((int) this.y) + "B" + String.valueOf((int) (this.xspd * 1000.0f)) + "B" + String.valueOf((int) (this.yspd * 1000.0f)) + "B" + String.valueOf((int) this.ang) + "B" + String.valueOf(units.getUnitIndex(this.owner)) + "B" + String.valueOf(this.life);
        } catch (Exception e) {
            throw e;
        }
    }

    public void restoreFromSaveString(String saveString, Units units) throws Exception {
        try {
            String[] data = saveString.split("B");
            int pos = 0 + 1;
            this.x = (double) Integer.parseInt(data[0]);
            int pos2 = pos + 1;
            this.y = (double) Integer.parseInt(data[pos]);
            int pos3 = pos2 + 1;
            this.xspd = ((float) Integer.parseInt(data[pos2])) / 1000.0f;
            int pos4 = pos3 + 1;
            this.yspd = ((float) Integer.parseInt(data[pos3])) / 1000.0f;
            int pos5 = pos4 + 1;
            this.ang = (float) Integer.parseInt(data[pos4]);
            int pos6 = pos5 + 1;
            this.loadOwnerID = Integer.parseInt(data[pos5]);
            int i = pos6 + 1;
            this.life = Integer.parseInt(data[pos6]);
        } catch (Exception e) {
            throw e;
        }
    }
}
