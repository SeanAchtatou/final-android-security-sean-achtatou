package com.hangfire.spacesquadronFREE;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public final class SpaceController {
    private Asteroids asteroids;
    private boolean dragAllowed = false;
    public boolean gameOver = false;
    public boolean invalidWorld = true;
    private EndTurnButton mEndTurnButton;
    private boolean mFirstStateSaved = false;
    private int mReturnMode;
    private Sounds mSounds;
    private MotionEvent mTouchEventDown;
    private MotionEvent mTouchEventMove;
    private MotionEvent mTouchEventUp;
    private Tutorial mTutorial;
    private Map map;
    private NavArrow navArrow;
    private Particles particles;
    private Projectiles projectiles;
    public boolean quitAfterGameOver = false;
    private Radar radar;
    private Screen screen;
    private boolean statusMessageDisplayed = false;
    private UnitPanel unitPanel;
    private Units units;

    public SpaceController(Context context) throws Exception {
        try {
            this.navArrow = new NavArrow(context.getResources());
            this.mSounds = new Sounds(context);
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean setup(Context context, MissionData md, GameThread thread) throws Exception {
        try {
            this.map = null;
            this.screen = null;
            this.radar = null;
            this.particles = null;
            this.projectiles = null;
            this.asteroids = null;
            this.units = null;
            this.mEndTurnButton = null;
            this.unitPanel = null;
            System.gc();
            Resources res = context.getResources();
            int maxWorldX = md.getWorldSizeX();
            int maxWorldY = md.getWorldSizeY();
            this.map = new Map(res, md.getBackgroundImageID(), maxWorldX, maxWorldY);
            this.screen = new Screen(maxWorldX, maxWorldY);
            this.radar = new Radar(res, maxWorldX, maxWorldY);
            this.particles = new Particles(res);
            this.projectiles = new Projectiles(res);
            this.asteroids = new Asteroids(res);
            this.units = new Units(res);
            this.mEndTurnButton = new EndTurnButton(res);
            this.unitPanel = new UnitPanel(res);
            md.setup(this.units, this.asteroids, this.map);
            boolean loadingGame = loadSpace(thread);
            if (md.getMissionNumber() == 1 && !loadingGame && thread.getTutorialOn()) {
                this.mTutorial = new Tutorial();
            }
            this.invalidWorld = false;
            this.gameOver = false;
            this.quitAfterGameOver = false;
            this.statusMessageDisplayed = false;
            return loadingGame;
        } catch (Exception e) {
            throw e;
        }
    }

    public int run(Context context, SurfaceHolder surfaceHolder, BitmapFontDrawer mTextDrawer, Timer timer, GameThread thread, MissionData md) throws Exception {
        Canvas canvas;
        try {
            if (!this.mFirstStateSaved) {
                saveGameState(thread);
                this.mFirstStateSaved = true;
            }
            this.mReturnMode = 0;
            canvas = null;
            canvas = surfaceHolder.lockCanvas(null);
            handleInput(timer, thread);
            handleMusic(thread);
            if (timer.realTimeMode) {
                if (timer.startRealTime) {
                    this.units.startRealTime();
                }
                this.asteroids.move(this.map);
                this.units.move(this.map, this.particles);
                this.asteroids.collide();
                this.units.collide(this.asteroids, this.map);
                this.units.processRealTime(this.map, this.mSounds, this.projectiles, this.screen, thread);
                this.projectiles.move(this.particles, this.units, this.asteroids);
                this.projectiles.collide(this.particles, this.mSounds, this.units, this.asteroids, this.screen, thread);
                this.particles.processParticles();
            } else {
                if (timer.startTurn) {
                    this.units.startTurn(this.projectiles, this.map, this.units, this.asteroids, this.mSounds, this.screen, thread);
                    handleMissionStatus(context, md);
                    saveGameState(thread);
                }
                this.units.processNonRealTime(this.screen, this.navArrow);
                if (this.mTutorial != null) {
                    if (this.mTutorial.complete(this.units, this.unitPanel, timer, this.screen, this.radar, thread)) {
                        this.mTutorial = null;
                    }
                }
                if (this.gameOver) {
                    timer.endTurn();
                }
            }
            this.mSounds.processSounds();
            doDraw(surfaceHolder, canvas, mTextDrawer, timer, thread);
            if (canvas != null) {
                surfaceHolder.unlockCanvasAndPost(canvas);
                timer.processTimer();
            }
            if (this.quitAfterGameOver) {
                this.mReturnMode = 12;
            }
            return this.mReturnMode;
        } catch (Exception e) {
            throw e;
        } catch (Throwable th) {
            if (canvas != null) {
                surfaceHolder.unlockCanvasAndPost(canvas);
                timer.processTimer();
            }
            throw th;
        }
    }

    public void destroy(GameThread thread) throws Exception {
        try {
            this.screen = null;
            this.map = null;
            this.navArrow = null;
            this.mEndTurnButton = null;
            this.units = null;
            this.projectiles = null;
            this.particles = null;
            this.radar = null;
            this.asteroids = null;
            this.unitPanel = null;
            this.mSounds.destroy(thread);
            this.mSounds = null;
        } catch (Exception e) {
            throw e;
        }
    }

    public Screen getScreen() {
        return this.screen;
    }

    private void doDraw(SurfaceHolder surfaceHolder, Canvas canvas, BitmapFontDrawer mTextDrawer, Timer timer, GameThread thread) throws Exception {
        try {
            this.map.drawMap(canvas, surfaceHolder, this.screen);
            this.asteroids.draw(canvas, this.screen, timer, this.navArrow);
            this.units.draw(canvas, timer, this.screen, this.navArrow);
            this.projectiles.drawProjectiles(canvas, this.screen);
            this.particles.drawParticles(canvas, this.screen);
            if (this.radar.radarMode) {
                this.radar.draw(canvas, this.screen, this.units, this.asteroids, this.mEndTurnButton, timer, mTextDrawer);
            }
            if (!timer.realTimeMode) {
                if (this.unitPanel.isVisible()) {
                    this.unitPanel.draw(canvas, mTextDrawer, this.units, this.screen);
                } else {
                    this.mEndTurnButton.draw(canvas, this.screen, timer, thread);
                }
                if (this.mTutorial != null) {
                    this.mTutorial.draw(canvas);
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void setSurfaceSize(int width, int height) throws Exception {
        try {
            this.screen.setScreenSize(width, height);
            this.map.setScreenSize(width, height);
            this.radar.setScreenSize(width, height);
            this.units.setScreenSize(width, height);
            this.unitPanel.setScreenSize(width, height);
            if (this.mTutorial != null) {
                this.mTutorial.setScreenSize(width, height);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean onTouch(MotionEvent event) throws Exception {
        try {
            if (this.mTouchEventUp == null && event.getAction() == 1) {
                this.mTouchEventUp = event;
                return true;
            } else if (this.mTouchEventDown == null && event.getAction() == 0) {
                this.mTouchEventDown = event;
                return true;
            } else if (this.mTouchEventMove != null || event.getAction() != 2) {
                return false;
            } else {
                this.mTouchEventMove = event;
                return true;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void handleInput(Timer timer, GameThread thread) throws Exception {
        try {
            handleTouchEvents(timer, thread);
        } catch (Exception e) {
            throw e;
        }
    }

    private void handleTouchEvents(Timer timer, GameThread thread) throws Exception {
        try {
            if (this.mTouchEventDown != null) {
                touchSelect(this.mTouchEventDown.getX(), this.mTouchEventDown.getY(), timer, thread);
                this.mTouchEventDown = null;
            }
            if (this.mTouchEventMove != null) {
                touchMove(this.mTouchEventMove.getX(), this.mTouchEventMove.getY(), timer);
                this.mTouchEventMove = null;
            }
            if (this.mTouchEventUp != null) {
                touchDeSelect(this.mTouchEventUp.getX(), this.mTouchEventUp.getY(), timer, thread);
                this.mTouchEventUp = null;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void touchSelect(float x, float y, Timer timer, GameThread thread) throws Exception {
        try {
            if (!timer.realTimeMode) {
                this.dragAllowed = false;
                if (this.radar.radarMode) {
                    this.radar.touchDown(x, y, this.screen, timer);
                    return;
                } else if (this.unitPanel.isVisible()) {
                    this.unitPanel.touchDown(x, y);
                    return;
                } else if (this.mEndTurnButton.touchDown(this.screen, x, y, thread) || this.units.selectUnitMoveCircle(x, y) || this.units.selectUnit(x, y, this.unitPanel)) {
                    return;
                }
            }
            this.dragAllowed = true;
            this.screen.startCameraDrag(x, y);
        } catch (Exception e) {
            throw e;
        }
    }

    private void touchDeSelect(float x, float y, Timer timer, GameThread thread) throws Exception {
        try {
            if (!timer.realTimeMode) {
                this.radar.touchUp(x, y, this.screen, timer);
                this.unitPanel.touchUp(x, y, this.units, this.mSounds, this.screen, thread);
                this.mEndTurnButton.touchUp(this.screen, x, y, timer, thread);
                this.units.touchUp(x, y);
                if (this.screen.wasTapped()) {
                    this.units.unitSelected = null;
                }
            }
            this.screen.stopCameraDrag();
        } catch (Exception e) {
            throw e;
        }
    }

    private void touchMove(float x, float y, Timer timer) throws Exception {
        try {
            if (!timer.realTimeMode) {
                if (this.radar.radarMode) {
                    this.radar.dragRadar(x, y, this.screen);
                    return;
                } else if (this.unitPanel.isVisible() || this.units.moveUnitArrow(x, y, this.screen, this.navArrow)) {
                    return;
                }
            }
            if (this.dragAllowed) {
                this.screen.dragCamera(x, y);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void menuItemPressed(int item, GameThread thread) throws Exception {
        try {
            if (this.gameOver) {
                this.quitAfterGameOver = true;
                return;
            }
            switch (item) {
                case R.id.menu_button_zoom:
                    this.screen.toggleZoom();
                    return;
                case R.id.menu_button_map:
                    if (this.radar != null) {
                        this.radar.radarMode = true;
                    }
                    if (this.unitPanel != null) {
                        this.unitPanel.setVisible(false, null);
                        return;
                    }
                    return;
                case R.id.menu_button_menu:
                    this.mReturnMode = 1;
                    return;
                default:
                    return;
            }
            throw e;
        } catch (Exception e) {
            throw e;
        }
    }

    public void backButtonPressed(boolean depressed, Timer timer, GameThread thread) throws Exception {
        if (!depressed) {
            try {
                if (this.gameOver) {
                    this.quitAfterGameOver = true;
                } else if (this.radar != null && this.radar.radarMode) {
                    this.radar.radarMode = false;
                } else if (this.unitPanel != null && this.unitPanel.isVisible()) {
                    this.unitPanel.setVisible(false, null);
                } else if (thread.getOnScreenTurnButtonOn()) {
                    this.mReturnMode = 1;
                } else {
                    timer.endTurn();
                }
            } catch (Exception e) {
                throw e;
            }
        }
    }

    private void handleMusic(GameThread thread) throws Exception {
        try {
            if (thread.getMusicID() != R.raw.musicspace) {
                thread.setMusic(R.raw.musicspace);
                thread.quietMusic(false);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void handleMissionStatus(Context context, MissionData md) throws Exception {
        try {
            if (this.gameOver) {
                this.mReturnMode = 0;
                return;
            }
            Unit u = this.units.unitPlayer;
            int status = md.getUpdatedStatus(this.units);
            if (!this.statusMessageDisplayed) {
                if (status == 1) {
                    this.mReturnMode = 7;
                    this.statusMessageDisplayed = true;
                } else if (status == 2) {
                    this.mReturnMode = 8;
                    this.statusMessageDisplayed = true;
                }
            }
            int pilotDead = u.getPilotDead();
            if (pilotDead != 0) {
                this.gameOver = true;
                this.mReturnMode = 9;
                if (pilotDead == 2) {
                    md.setReason(context.getString(R.string.deadreason_ship));
                } else if (pilotDead == 1) {
                    this.mReturnMode = 9;
                    md.setReason(context.getString(R.string.deadreason_pilot));
                } else if (pilotDead == 3) {
                    md.setReason(context.getString(R.string.deadreason_pod));
                }
            } else if (status == 1) {
                if (this.units.allEnemiesGone()) {
                    this.mReturnMode = 10;
                } else if (u.shipAlive && !u.hasEjected && u.isSafe) {
                    this.mReturnMode = 10;
                } else if (u.hasEjected && u.pod.podSafe) {
                    this.mReturnMode = 10;
                }
            } else if (status == 2) {
                if (this.units.allEnemiesGone()) {
                    this.mReturnMode = 11;
                } else if (u.shipAlive && !u.hasEjected && u.isSafe) {
                    this.mReturnMode = 11;
                } else if (u.hasEjected && u.pod.podSafe) {
                    this.mReturnMode = 11;
                }
            } else if (u.shipAlive && !u.hasEjected && u.isSafe) {
                this.mReturnMode = 11;
                md.setReason(context.getString(R.string.failreason_fled));
            } else if (u.hasEjected && u.pod.podSafe) {
                this.mReturnMode = 11;
                md.setReason(context.getString(R.string.failreason_ejected));
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public Units getUnits() {
        return this.units;
    }

    public void saveGameState(GameThread thread) throws Exception {
        thread.saveCurrentCareer();
        SharedPreferences.Editor ed = thread.getSP().edit();
        ed.putString(GameThread.ASTEROIDS_PREF, this.asteroids.getSaveString());
        ed.putString(GameThread.UNITS_PREF, this.units.getSaveString());
        ed.putString(GameThread.PROJECTILES_PREF, this.projectiles.getSaveString(this.units));
        ed.putString(GameThread.PARTICLES_PREF, this.particles.getSaveString());
        ed.putString(GameThread.SCREEN_PREF, this.screen.getSaveString());
        ed.putString(GameThread.STATISTICS_PREF, thread.getCareerRecords().encodeCareerRecord(thread.getCareerRecords().getStats()));
        ed.commit();
    }

    public boolean loadSpace(GameThread thread) throws Exception {
        try {
            SharedPreferences sp = thread.getSP();
            String loadData = sp.getString(GameThread.ASTEROIDS_PREF, null);
            if (loadData == null) {
                return false;
            }
            this.asteroids.restoreFromSaveString(loadData);
            this.units.restoreFromSaveString(sp.getString(GameThread.UNITS_PREF, null));
            this.projectiles.restoreFromSaveString(sp.getString(GameThread.PROJECTILES_PREF, null), this.units);
            this.particles.restoreFromSaveString(sp.getString(GameThread.PARTICLES_PREF, null));
            String loadData2 = sp.getString(GameThread.SCREEN_PREF, null);
            if (loadData2 != null) {
                this.screen.restoreFromSaveString(loadData2);
            }
            String loadData3 = sp.getString(GameThread.STATISTICS_PREF, null);
            thread.getCareerRecords().decodeCareerRecord(loadData3.split("~"), 0, thread.getCareerRecords().getStats());
            this.projectiles.convertIDsToReferences(this.units);
            this.units.convertIDsToReferences();
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
}
