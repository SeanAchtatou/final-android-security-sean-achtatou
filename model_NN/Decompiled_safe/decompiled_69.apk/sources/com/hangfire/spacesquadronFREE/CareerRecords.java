package com.hangfire.spacesquadronFREE;

import android.content.SharedPreferences;
import java.util.ArrayList;

public final class CareerRecords {
    private static final String CAREER_CURRENT_PREF = "CURRENTCAREER";
    private static final String CAREER_RECORDS_PREF = "HALLOFFAME";
    public static final int CAREER_STATE_DEAD = 2;
    public static final int CAREER_STATE_INPROGRESS = 3;
    public static final int CAREER_STATE_QUIT = 0;
    public static final int CAREER_STATE_WON = 1;
    public static final int MAX_PILOTNAME_LENGTH = 12;
    public static final int MIN_PILOTNAME_LENGTH = 3;
    public static final int NUMBER_OF_RANKS = 10;
    private static final String[] RANKS = {"Pilot Officer", "Flying Officer", "Flight Lieutenant", "Squadron Leader", "Wing Commander", "Group Captain", "Air Commodore", "Air Vice Marshal", "Air Marshal", "Air Chief Marshal"};
    public static final int RANK_AIR_CHIEF_MARSHAL = 9;
    public static final int RANK_AIR_COMMODORE = 6;
    public static final int RANK_AIR_MARSHAL = 8;
    public static final int RANK_AIR_VICE_MARSHAL = 7;
    public static final int RANK_FLIGHT_LIEUTENANT = 2;
    public static final int RANK_FLYING_OFFICER = 1;
    public static final int RANK_GROUP_CAPTAIN = 5;
    public static final int RANK_PILOT_OFFICER = 0;
    public static final int RANK_SQUADRON_LEADER = 3;
    public static final int RANK_WING_COMMANDER = 4;
    private ArrayList<CareerRecord> allRecords = new ArrayList<>();
    private CareerRecord mCurrent;
    private ArrayList<CareerRecord> mHallOfFame = new ArrayList<>();
    private CareerRecord mStats;

    public CareerRecords(SharedPreferences sp, GameThread thread) throws Exception {
        try {
            loadCareerRecords(sp);
            loadCurrentCareer(sp, thread);
            resetStats();
            createAllRecords();
        } catch (Exception e) {
            throw e;
        }
    }

    private void createAllRecords() throws Exception {
        try {
            Integer pos = 0;
            this.allRecords.clear();
            boolean currentAdded = false;
            if (this.mHallOfFame.size() > 0) {
                for (int i = 0; i < this.mHallOfFame.size(); i++) {
                    pos = Integer.valueOf(i + 1);
                    if (this.mCurrent != null && !currentAdded && this.mCurrent.getScore() >= this.mHallOfFame.get(i).getScore()) {
                        this.allRecords.add(this.mCurrent);
                        this.mCurrent.setRankPosition(pos.intValue());
                        currentAdded = true;
                    }
                    this.allRecords.add(this.mHallOfFame.get(i));
                    if (currentAdded) {
                        this.mHallOfFame.get(i).setRankPosition(pos.intValue() + 1);
                    } else {
                        this.mHallOfFame.get(i).setRankPosition(pos.intValue());
                    }
                }
            }
            if (this.mCurrent != null && !currentAdded) {
                this.allRecords.add(this.mCurrent);
                this.mCurrent.setRankPosition(pos.intValue() + 1);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public ArrayList<CareerRecord> getHallOfFame() {
        return this.allRecords;
    }

    public void resetStats() throws Exception {
        try {
            this.mStats = new CareerRecord();
        } catch (Exception e) {
            throw e;
        }
    }

    public CareerRecord getStats() {
        return this.mStats;
    }

    public void assignStats(boolean scoringAllowed) throws Exception {
        try {
            this.mCurrent.setEjections(this.mCurrent.getEjections() + this.mStats.getEjections());
            this.mCurrent.setShipsLost(this.mCurrent.getShipsLost() + this.mStats.getShipsLost());
            this.mCurrent.setPilotsLost(this.mCurrent.getPilotsLost() + this.mStats.getPilotsLost());
            this.mCurrent.setEnemyShipsKilled(this.mCurrent.getEnemyShipsKilled() + this.mStats.getEnemyShipsKilled());
            this.mCurrent.setEnemyPodsKilled(this.mCurrent.getEnemyPodsKilled() + this.mStats.getEnemyPodsKilled());
            this.mCurrent.setFriendlyShipsKilled(this.mCurrent.getFriendlyShipsKilled() + this.mStats.getFriendlyShipsKilled());
            this.mCurrent.setFriendlyPodsKilled(this.mCurrent.getFriendlyPodsKilled() + this.mStats.getFriendlyPodsKilled());
            this.mCurrent.setDamageReceived(this.mCurrent.getDamageReceived() + this.mStats.getDamageReceived());
            this.mCurrent.setBulletsFired(this.mCurrent.getBulletsFired() + this.mStats.getBulletsFired());
            this.mCurrent.setBulletsHitEnemy(this.mCurrent.getBulletsHitEnemy() + this.mStats.getBulletsHitEnemy());
            this.mCurrent.setBulletsHitFriendly(this.mCurrent.getBulletsHitFriendly() + this.mStats.getBulletsHitFriendly());
            this.mCurrent.setRocketsFired(this.mCurrent.getRocketsFired() + this.mStats.getRocketsFired());
            this.mCurrent.setRocketsHitEnemy(this.mCurrent.getRocketsHitEnemy() + this.mStats.getRocketsHitEnemy());
            this.mCurrent.setRocketsHitFriendly(this.mCurrent.getRocketsHitFriendly() + this.mStats.getRocketsHitFriendly());
            this.mCurrent.setMissilesFired(this.mCurrent.getMissilesFired() + this.mStats.getMissilesFired());
            this.mCurrent.setMissilesHitEnemy(this.mCurrent.getMissilesHitEnemy() + this.mStats.getMissilesHitEnemy());
            this.mCurrent.setMissilesHitFriendly(this.mCurrent.getMissilesHitFriendly() + this.mStats.getMissilesHitFriendly());
            if (scoringAllowed) {
                this.mCurrent.setRank(this.mCurrent.getRank() + this.mStats.getRank());
                this.mCurrent.setBraveryInTheAir(this.mCurrent.getBraveryInTheAir() + this.mStats.getBraveryInTheAir());
                this.mCurrent.setMeritoriousServiceMedal(this.mCurrent.getMeritoriousServiceMedal() + this.mStats.getMeritoriousServiceMedal());
                this.mCurrent.setAirForceCross(this.mCurrent.getAirForceCross() + this.mStats.getAirForceCross());
                this.mCurrent.setDistinguishedFlyingCross(this.mCurrent.getDistinguishedFlyingCross() + this.mStats.getDistinguishedFlyingCross());
                this.mCurrent.setConspicuousGallantryCross(this.mCurrent.getConspicuousGallantryCross() + this.mStats.getConspicuousGallantryCross());
                if (this.mStats.getScore() > 0) {
                    this.mCurrent.setScore(this.mCurrent.getScore() + this.mStats.getScore());
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void newCurrent(String name) throws Exception {
        try {
            this.mCurrent = new CareerRecord();
            this.mCurrent.setPilotName(name);
            createAllRecords();
        } catch (Exception e) {
            throw e;
        }
    }

    public CareerRecord getCurrent() {
        return this.mCurrent;
    }

    public void deleteCurrent(SharedPreferences sp, int state) throws Exception {
        try {
            if (this.mCurrent.getScore() > 0) {
                this.mCurrent.setState(state);
                this.mHallOfFame.add(this.mCurrent);
                sortCareerRecords();
                saveCareerRecords(sp);
            }
            this.mCurrent = null;
            SharedPreferences.Editor ed = sp.edit();
            ed.remove(CAREER_CURRENT_PREF);
            ed.commit();
            createAllRecords();
        } catch (Exception e) {
            throw e;
        }
    }

    private void loadCurrentCareer(SharedPreferences sp, GameThread thread) throws Exception {
        try {
            String currentData = sp.getString(CAREER_CURRENT_PREF, null);
            if (currentData != null) {
                String[] dataValues = currentData.split("~");
                this.mCurrent = new CareerRecord();
                decodeCareerRecord(dataValues, 0, this.mCurrent);
                thread.loadMissionData(this.mCurrent.getCurrentMission());
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void saveCurrentCareer(SharedPreferences sp) throws Exception {
        try {
            if (this.mCurrent != null) {
                String currentEncoded = encodeCareerRecord(this.mCurrent);
                SharedPreferences.Editor ed = sp.edit();
                ed.putString(CAREER_CURRENT_PREF, currentEncoded);
                ed.commit();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void saveCareerRecords(SharedPreferences sp) throws Exception {
        try {
            sortCareerRecords();
            String hallOfFame = "HOF~" + Integer.toString(this.mHallOfFame.size()) + "~";
            for (int i = 0; i < this.mHallOfFame.size(); i++) {
                hallOfFame = String.valueOf(hallOfFame) + encodeCareerRecord(this.mHallOfFame.get(i));
            }
            SharedPreferences.Editor ed = sp.edit();
            ed.putString(CAREER_RECORDS_PREF, hallOfFame);
            ed.commit();
        } catch (Exception e) {
            throw e;
        }
    }

    private void loadCareerRecords(SharedPreferences sp) throws Exception {
        try {
            this.mHallOfFame.clear();
            String hallOfFame = sp.getString(CAREER_RECORDS_PREF, null);
            if (hallOfFame != null) {
                String[] hofValues = hallOfFame.split("~");
                int records = Integer.parseInt(hofValues[1]);
                int pos = 2;
                for (int i = 0; i < records; i++) {
                    this.mHallOfFame.add(new CareerRecord());
                    pos = decodeCareerRecord(hofValues, pos, this.mHallOfFame.get(i));
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void deleteCareerRecords(SharedPreferences sp) throws Exception {
        try {
            SharedPreferences.Editor ed = sp.edit();
            ed.remove(CAREER_RECORDS_PREF);
            ed.commit();
            this.mHallOfFame.clear();
            createAllRecords();
        } catch (Exception e) {
            throw e;
        }
    }

    public void deleteCareerRecord(SharedPreferences sp, int id) throws Exception {
        try {
            this.mHallOfFame.remove(this.allRecords.get(id));
            saveCareerRecords(sp);
            createAllRecords();
        } catch (Exception e) {
            throw e;
        }
    }

    public int decodeCareerRecord(String[] recData, int pos, CareerRecord rec) throws Exception {
        Exception e;
        int pos2 = pos + 1;
        try {
            rec.setPilotName(recData[pos]);
            int pos3 = pos2 + 1;
            try {
                rec.setCurrentMission(Integer.parseInt(recData[pos2]));
                pos2 = pos3 + 1;
                rec.setState(Integer.parseInt(recData[pos3]));
                int pos4 = pos2 + 1;
                rec.setMissionSuccesses(Integer.parseInt(recData[pos2]));
                pos2 = pos4 + 1;
                rec.setMissionFailures(Integer.parseInt(recData[pos4]));
                int pos5 = pos2 + 1;
                rec.setRank(Integer.parseInt(recData[pos2]));
                pos2 = pos5 + 1;
                rec.setEjections(Integer.parseInt(recData[pos5]));
                int pos6 = pos2 + 1;
                rec.setShipsLost(Integer.parseInt(recData[pos2]));
                pos2 = pos6 + 1;
                rec.setPilotsLost(Integer.parseInt(recData[pos6]));
                int pos7 = pos2 + 1;
                rec.setEnemyShipsKilled(Integer.parseInt(recData[pos2]));
                pos2 = pos7 + 1;
                rec.setEnemyPodsKilled(Integer.parseInt(recData[pos7]));
                int pos8 = pos2 + 1;
                rec.setFriendlyShipsKilled(Integer.parseInt(recData[pos2]));
                pos2 = pos8 + 1;
                rec.setFriendlyPodsKilled(Integer.parseInt(recData[pos8]));
                int pos9 = pos2 + 1;
                rec.setDamageReceived(Integer.parseInt(recData[pos2]));
                pos2 = pos9 + 1;
                rec.setBulletsFired(Integer.parseInt(recData[pos9]));
                int pos10 = pos2 + 1;
                rec.setBulletsHitEnemy(Integer.parseInt(recData[pos2]));
                pos2 = pos10 + 1;
                rec.setBulletsHitFriendly(Integer.parseInt(recData[pos10]));
                int pos11 = pos2 + 1;
                rec.setRocketsFired(Integer.parseInt(recData[pos2]));
                pos2 = pos11 + 1;
                rec.setRocketsHitEnemy(Integer.parseInt(recData[pos11]));
                int pos12 = pos2 + 1;
                rec.setRocketsHitFriendly(Integer.parseInt(recData[pos2]));
                pos2 = pos12 + 1;
                rec.setMissilesFired(Integer.parseInt(recData[pos12]));
                int pos13 = pos2 + 1;
                rec.setMissilesHitEnemy(Integer.parseInt(recData[pos2]));
                pos2 = pos13 + 1;
                rec.setMissilesHitFriendly(Integer.parseInt(recData[pos13]));
                int pos14 = pos2 + 1;
                rec.setBraveryInTheAir(Integer.parseInt(recData[pos2]));
                pos2 = pos14 + 1;
                rec.setMeritoriousServiceMedal(Integer.parseInt(recData[pos14]));
                int pos15 = pos2 + 1;
                rec.setAirForceCross(Integer.parseInt(recData[pos2]));
                pos2 = pos15 + 1;
                rec.setDistinguishedFlyingCross(Integer.parseInt(recData[pos15]));
                int pos16 = pos2 + 1;
                rec.setConspicuousGallantryCross(Integer.parseInt(recData[pos2]));
                pos2 = pos16 + 1;
                rec.setScore(Integer.parseInt(recData[pos16]));
                return pos2;
            } catch (Exception e2) {
                e = e2;
                throw e;
            }
        } catch (Exception e3) {
            e = e3;
            throw e;
        }
    }

    public String encodeCareerRecord(CareerRecord rec) throws Exception {
        try {
            return String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + rec.getPilotName().trim() + "~") + Integer.toString(rec.getCurrentMission()) + "~") + Integer.toString(rec.getState()) + "~") + Integer.toString(rec.getMissionSuccesses()) + "~") + Integer.toString(rec.getMissionFailures()) + "~") + Integer.toString(rec.getRank()) + "~") + Integer.toString(rec.getEjections()) + "~") + Integer.toString(rec.getShipsLost()) + "~") + Integer.toString(rec.getPilotsLost()) + "~") + Integer.toString(rec.getEnemyShipsKilled()) + "~") + Integer.toString(rec.getEnemyPodsKilled()) + "~") + Integer.toString(rec.getFriendlyShipsKilled()) + "~") + Integer.toString(rec.getFriendlyPodsKilled()) + "~") + Integer.toString(rec.getDamageReceived()) + "~") + Integer.toString(rec.getBulletsFired()) + "~") + Integer.toString(rec.getBulletsHitEnemy()) + "~") + Integer.toString(rec.getBulletsHitFriendly()) + "~") + Integer.toString(rec.getRocketsFired()) + "~") + Integer.toString(rec.getRocketsHitEnemy()) + "~") + Integer.toString(rec.getRocketsHitFriendly()) + "~") + Integer.toString(rec.getMissilesFired()) + "~") + Integer.toString(rec.getMissilesHitEnemy()) + "~") + Integer.toString(rec.getMissilesHitFriendly()) + "~") + Integer.toString(rec.getBraveryInTheAir()) + "~") + Integer.toString(rec.getMeritoriousServiceMedal()) + "~") + Integer.toString(rec.getAirForceCross()) + "~") + Integer.toString(rec.getDistinguishedFlyingCross()) + "~") + Integer.toString(rec.getConspicuousGallantryCross()) + "~") + Integer.toString(rec.getScore()) + "~";
        } catch (Exception e) {
            throw e;
        }
    }

    private void sortCareerRecords() throws Exception {
        int a = 0;
        while (a < this.mHallOfFame.size()) {
            try {
                boolean sorted = true;
                for (int b = 0; b < (this.mHallOfFame.size() - a) - 1; b++) {
                    if (this.mHallOfFame.get(b).getScore() < this.mHallOfFame.get(b + 1).getScore()) {
                        this.mHallOfFame.set(b, this.mHallOfFame.get(b + 1));
                        this.mHallOfFame.set(b + 1, this.mHallOfFame.get(b));
                        sorted = false;
                    }
                }
                if (!sorted) {
                    a++;
                } else {
                    return;
                }
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public static final String getRank(int index) throws Exception {
        if (index >= 10 || index < 0) {
            return "";
        }
        try {
            return RANKS[index];
        } catch (Exception e) {
            throw e;
        }
    }

    public void addStats(Missile missile, Bullet bullet, Unit unit, Pod pod) throws Exception {
        if (missile != null) {
            try {
                if (!missile.owner.isPlayable) {
                    if (unit != null) {
                        if (unit.isPlayable) {
                            if (!unit.shipAlive) {
                                this.mStats.addShipsLost(1);
                            }
                            if (unit.pilotJustKilled()) {
                                this.mStats.addPilotsLost(1);
                            }
                        }
                    } else if (pod != null && pod.source.isPlayable) {
                        this.mStats.addPilotsLost(1);
                    }
                } else if (unit != null) {
                    if (unit.team == 0) {
                        if (missile.isGuided()) {
                            this.mStats.addMissilesHitFriendly(1);
                        } else {
                            this.mStats.addRocketsHitFriendly(1);
                        }
                        if (!unit.shipAlive) {
                            this.mStats.addFriendlyShipsKilled(1);
                        }
                        if (unit.pilotJustKilled()) {
                            this.mStats.addPilotsLost(1);
                            return;
                        }
                        return;
                    }
                    if (missile.isGuided()) {
                        this.mStats.addMissilesHitEnemy(1);
                    } else {
                        this.mStats.addRocketsHitEnemy(1);
                    }
                    if (!unit.shipAlive) {
                        this.mStats.addEnemyShipsKilled(1);
                    }
                } else if (pod == null) {
                } else {
                    if (pod.source.team == 0) {
                        if (missile.isGuided()) {
                            this.mStats.addMissilesHitFriendly(1);
                        } else {
                            this.mStats.addRocketsHitFriendly(1);
                        }
                        this.mStats.addFriendlyPodsKilled(1);
                        this.mStats.addPilotsLost(1);
                        return;
                    }
                    if (missile.isGuided()) {
                        this.mStats.addMissilesHitEnemy(1);
                    } else {
                        this.mStats.addRocketsHitEnemy(1);
                    }
                    this.mStats.addEnemyPodsKilled(1);
                }
            } catch (Exception e) {
                throw e;
            }
        } else if (bullet == null) {
        } else {
            if (!bullet.owner.isPlayable) {
                if (unit != null) {
                    if (unit.isPlayable) {
                        if (!unit.shipAlive) {
                            this.mStats.addShipsLost(1);
                        }
                        if (unit.pilotJustKilled()) {
                            this.mStats.addPilotsLost(1);
                        }
                    }
                } else if (pod != null && pod.source.isPlayable) {
                    this.mStats.addPilotsLost(1);
                }
            } else if (unit != null) {
                if (unit.team == 0) {
                    this.mStats.addBulletsHitFriendly(1);
                    if (!unit.shipAlive) {
                        this.mStats.addFriendlyShipsKilled(1);
                    }
                    if (unit.pilotJustKilled()) {
                        this.mStats.addPilotsLost(1);
                        return;
                    }
                    return;
                }
                this.mStats.addBulletsHitEnemy(1);
                if (!unit.shipAlive) {
                    this.mStats.addEnemyShipsKilled(1);
                }
            } else if (pod == null) {
            } else {
                if (pod.source.team == 0) {
                    this.mStats.addBulletsHitFriendly(1);
                    this.mStats.addFriendlyPodsKilled(1);
                    this.mStats.addPilotsLost(1);
                    return;
                }
                this.mStats.addBulletsHitEnemy(1);
                this.mStats.addEnemyPodsKilled(1);
            }
        }
    }
}
