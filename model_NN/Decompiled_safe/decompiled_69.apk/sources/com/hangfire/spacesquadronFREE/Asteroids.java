package com.hangfire.spacesquadronFREE;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import java.util.ArrayList;

public final class Asteroids {
    public static final int ASTEROID_LARGEROUND = 2;
    public static final int ASTEROID_MEDIUMROUND = 1;
    public static final int ASTEROID_SMALLROUND = 0;
    public ArrayList<Asteroid> asteroid = new ArrayList<>();
    private Drawable asteroidLarge;
    private Drawable asteroidMedium;
    private Drawable asteroidSmall;
    public Asteroid resultAsteroid;

    public Asteroids(Resources res) throws Exception {
        try {
            this.asteroidSmall = res.getDrawable(R.drawable.asteroidsmall);
            this.asteroidMedium = res.getDrawable(R.drawable.asteroidmedium);
            this.asteroidLarge = res.getDrawable(R.drawable.asteroidlarge);
        } catch (Exception e) {
            throw e;
        }
    }

    public void makeAsteroid(int type, float x, float y, float sx, float sy) throws Exception {
        switch (type) {
            case 0:
                try {
                    this.asteroid.add(new Asteroid(type, (double) x, (double) y, sx, sy, 25, 23));
                    return;
                } catch (Exception e) {
                    throw e;
                }
            case 1:
                this.asteroid.add(new Asteroid(type, (double) x, (double) y, sx, sy, 40, 38));
                return;
            case 2:
                this.asteroid.add(new Asteroid(type, (double) x, (double) y, sx, sy, 50, 47));
                return;
            default:
                return;
        }
        throw e;
    }

    public void move(Map map) throws Exception {
        int i = 0;
        while (i < this.asteroid.size()) {
            try {
                if (!this.asteroid.get(i).moveStillAlive(map)) {
                    this.asteroid.remove(i);
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public void collide() throws Exception {
        int i = 0;
        while (i < this.asteroid.size()) {
            try {
                Asteroid a1 = this.asteroid.get(i);
                for (int i2 = i + 1; i2 < this.asteroid.size(); i2++) {
                    Asteroid a2 = this.asteroid.get(i2);
                    int radius = a1.intColRadius + a2.intColRadius;
                    if (Functions.inCircle(a1.dblWorldX, a1.dblWorldY, a2.dblWorldX, a2.dblWorldY, radius)) {
                        double midX = (a1.dblWorldX + a2.dblWorldX) / 2.0d;
                        double midY = (a1.dblWorldY + a2.dblWorldY) / 2.0d;
                        float halfRadius = ((float) radius) / 2.0f;
                        a1.projectAway(midX, midY, halfRadius);
                        a2.projectAway(midX, midY, halfRadius);
                        float colAng = Functions.aTanFull((float) (a2.dblWorldX - a1.dblWorldX), (float) (a2.dblWorldY - a1.dblWorldY));
                        float angDiff = Math.abs(Functions.angDiff(a1.travelAng, colAng));
                        float a1toa2transfer = 0.0f;
                        float a2toa1transfer = 0.0f;
                        if (angDiff < 90.0f) {
                            float transRatio = angDiff / 90.0f;
                            a1toa2transfer = transRatio * a1.speed;
                            a1.setSpeed((1.0f - transRatio) * a1.speed);
                        }
                        float returnAng = Functions.wrapAngle(180.0f + colAng);
                        float angDiff2 = Math.abs(Functions.angDiff(a2.travelAng, returnAng));
                        if (angDiff2 < 90.0f) {
                            float transRatio2 = angDiff2 / 90.0f;
                            a2toa1transfer = transRatio2 * a2.speed;
                            a2.setSpeed((1.0f - transRatio2) * a2.speed);
                        }
                        if (a1toa2transfer > 0.0f) {
                            a2.addSpeed(LookUp.sin(colAng) * a1toa2transfer, LookUp.cos(colAng) * a1toa2transfer);
                        }
                        if (a2toa1transfer > 0.0f) {
                            a1.addSpeed(LookUp.sin(returnAng) * a2toa1transfer, LookUp.cos(returnAng) * a2toa1transfer);
                        }
                    }
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public void draw(Canvas canvas, Screen screen, Timer timer, NavArrow navArrow) throws Exception {
        int i = 0;
        while (i < this.asteroid.size()) {
            try {
                Asteroid a = this.asteroid.get(i);
                a.calculateScreenPos(screen);
                switch (a.type) {
                    case 0:
                        a.draw(canvas, Boolean.valueOf(screen.zoomedIn), this.asteroidSmall);
                        break;
                    case 1:
                        a.draw(canvas, Boolean.valueOf(screen.zoomedIn), this.asteroidMedium);
                        break;
                    case 2:
                        a.draw(canvas, Boolean.valueOf(screen.zoomedIn), this.asteroidLarge);
                        break;
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
        if (!timer.realTimeMode) {
            for (int i2 = 0; i2 < this.asteroid.size(); i2++) {
                this.asteroid.get(i2).drawArrowLine(canvas, navArrow, screen);
            }
            for (int i3 = 0; i3 < this.asteroid.size(); i3++) {
                this.asteroid.get(i3).drawArrowHead(canvas, navArrow);
            }
        }
    }

    public boolean lineIntersectAsteroid(double x, double y, double x2, double y2, int radius) throws Exception {
        double minX = x;
        if (x2 < x) {
            minX = x2;
        }
        double maxX = x;
        if (x2 > x) {
            maxX = x2;
        }
        double minY = y;
        if (y2 < y) {
            minY = y2;
        }
        double maxY = y;
        if (y2 > y) {
            maxY = y2;
        }
        try {
            this.resultAsteroid = null;
            double intersectDist = 1.0E9d;
            for (int i = 0; i < this.asteroid.size(); i++) {
                Asteroid a = this.asteroid.get(i);
                if (a.dblWorldX - ((double) a.intColRadius) < maxX && a.dblWorldX + ((double) a.intColRadius) > minX && a.dblWorldY - ((double) a.intColRadius) < maxY && a.dblWorldY + ((double) a.intColRadius) > minY) {
                    if (Functions.lineIntersectsCircle(x, y, x2, y2, a.dblWorldX, a.dblWorldY, (float) (a.intColRadius + radius))) {
                        double xdist = a.dblWorldX - x;
                        double ydist = a.dblWorldY - y;
                        double dist = (xdist * xdist) + (ydist * ydist);
                        if (dist < intersectDist) {
                            this.resultAsteroid = a;
                            intersectDist = dist;
                        }
                    }
                }
            }
            if (this.resultAsteroid != null) {
                return true;
            }
            return false;
        } catch (Exception e) {
            throw e;
        }
    }

    public String getSaveString() throws Exception {
        try {
            String saveString = String.valueOf(this.asteroid.size());
            for (int i = 0; i < this.asteroid.size(); i++) {
                saveString = String.valueOf(saveString) + "A" + this.asteroid.get(i).getSaveString();
            }
            return saveString;
        } catch (Exception e) {
            throw e;
        }
    }

    public void restoreFromSaveString(String saveString) throws Exception {
        try {
            this.asteroid = new ArrayList<>();
            String[] asteroidData = saveString.split("A");
            int numAsteroids = Integer.parseInt(asteroidData[0]);
            for (int i = 0; i < numAsteroids; i++) {
                this.asteroid.add(new Asteroid(0, 0.0d, 0.0d, 0.0f, 0.0f, 0, 0));
                this.asteroid.get(i).restoreFromSaveString(asteroidData[i + 1]);
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
