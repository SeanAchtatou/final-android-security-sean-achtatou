package com.hangfire.spacesquadronFREE;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.util.ArrayList;

public final class Unit {
    public static final int DEAD_IN_SHIP = 1;
    public static final int DEAD_POD_DESTROYED = 3;
    public static final int DEAD_SHIP_DESTROYED = 2;
    private static final boolean DEBUG_VISIBLITY = false;
    private static final int JET_PARTICLE_EMIT_DELAY = 4;
    private static final float JET_PARTICLE_SPEED = 0.5f;
    public static final int NOT_DEAD = 0;
    public static final int SIDE_FRONT = 2;
    public static final int SIDE_LEFT = 0;
    public static final int SIDE_REAR = 3;
    public static final int SIDE_RIGHT = 1;
    private static final int TURN_LEFT = 1;
    private static final int TURN_NONE = 0;
    private static final int TURN_RIGHT = 2;
    private int AI_AvoidanceCircleRadius = 0;
    private double AI_AvoidanceCircleX = 0.0d;
    private double AI_AvoidanceCircleY = 0.0d;
    private boolean AI_DefendMode = DEBUG_VISIBLITY;
    private float AI_MinSpeed = 0.0f;
    private int AI_Mission = 0;
    private int AI_MissionAng = 0;
    private int AI_MissionDist = 0;
    private int AI_MissionRadius = 0;
    public Unit AI_MissionUnit = null;
    private double AI_MissionX = 0.0d;
    private double AI_MissionY = 0.0d;
    private boolean AI_RunAway = DEBUG_VISIBLITY;
    private double AI_SearchZoneX;
    private double AI_SearchZoneY;
    private double AI_X;
    private double AI_Y;
    public ArrayList<ArmourSlot> armour = new ArrayList<>();
    public boolean arrowInScreen = DEBUG_VISIBLITY;
    private float attachAngle = 0.0f;
    private float attachDistance = 0.0f;
    private float attachPivotCenter = 0.0f;
    private float attachPivotLimit = 0.0f;
    private float attachRelativeAng = 0.0f;
    public Unit attachedTo = null;
    public boolean childrenAttached = DEBUG_VISIBLITY;
    public float cornerAngDiff;
    public float cornerAngDist;
    public double[] cornerCoords;
    public boolean cornerCoordsCalced = DEBUG_VISIBLITY;
    private double dblNoChangeWorldX;
    private double dblNoChangeWorldY;
    public boolean ejectOrder = DEBUG_VISIBLITY;
    public float fltAngle;
    private float fltFriction;
    private float fltMaxAccel;
    private float fltMaxSpeed;
    private float fltMaxTurn;
    private float fltOldSpeed;
    public float fltScreenArrowHeadAng;
    public float fltScreenArrowHeadX;
    public float fltScreenArrowHeadY;
    public float fltScreenFinishAng;
    public float fltScreenFinishX;
    public float fltScreenFinishY;
    public float fltSpeed;
    private float fltTurn;
    public double fltWorldX;
    public double fltWorldY;
    public float fltXSpd;
    public float fltYSpd;
    public int gunRefire = 0;
    public boolean hasEjected = DEBUG_VISIBLITY;
    public boolean hasPilot = true;
    public boolean inScreen = DEBUG_VISIBLITY;
    public int intCollisionHalfHeight = 0;
    public int intCollisionHalfWidth = 0;
    public int intCollisionRadius = 0;
    public int intHalfHeight = 0;
    public int intHalfWidth = 0;
    public int intHeight = 0;
    public int intQuarterHeight = 0;
    public int intQuarterWidth = 0;
    public int intScreenX;
    public int intScreenY;
    public int intVisualRadius = 0;
    public int intWidth = 0;
    public ArrayList<InventorySlot> inventory = new ArrayList<>();
    public boolean isBuilding;
    public boolean isLeavingMap = DEBUG_VISIBLITY;
    public boolean isPlayable;
    public boolean isPlayer = DEBUG_VISIBLITY;
    public boolean isSafe = DEBUG_VISIBLITY;
    private ArrayList<Integer> itemsToDestroy = new ArrayList<>();
    private int jetEmitCount = 4;
    private int jetEmitDelay = 4;
    private boolean jetEmitterOn = true;
    private double lastEmitX = 0.0d;
    private double lastEmitY = 0.0d;
    private float leavingAng;
    public int loadAI_MissionUnitID;
    public int loadAttachedToID;
    private int numPowers;
    private int numTurns;
    private boolean pilotKilledReturned = DEBUG_VISIBLITY;
    public Pod pod;
    private float powerReduce;
    public int rocketFireCount = 0;
    public int rocketRefire = 0;
    public boolean shipAlive = true;
    private int shipAlpha = 255;
    public int[] sideArmourCount;
    public int[] sideInvCount;
    public int team;
    private float turnReduce;
    public int type;
    private boolean visibleToPlayer = true;
    public boolean warning;

    public Unit(int t, float fltX, float fltY, float fltAng, float fltFric, float maxAccel, float maxSpeed, float maxTurn, int imgWidth, int imgHeight, int colWidth, int colHeight, boolean player, boolean playable, boolean building, int side, boolean hasChildren) throws Exception {
        try {
            this.childrenAttached = hasChildren;
            this.isPlayable = playable;
            this.isPlayer = player;
            this.isBuilding = building;
            if (this.isBuilding) {
                this.hasPilot = DEBUG_VISIBLITY;
            }
            this.team = side;
            if (this.team != 0) {
                this.isPlayable = DEBUG_VISIBLITY;
                this.visibleToPlayer = DEBUG_VISIBLITY;
                this.shipAlpha = 0;
            }
            this.fltWorldX = (double) fltX;
            this.fltWorldY = (double) fltY;
            this.dblNoChangeWorldX = (double) fltX;
            this.dblNoChangeWorldY = (double) fltY;
            this.fltScreenArrowHeadX = fltX;
            this.fltScreenArrowHeadY = fltY;
            this.fltAngle = fltAng;
            this.intWidth = imgWidth;
            this.intHeight = imgHeight;
            this.intHalfWidth = this.intWidth / 2;
            this.intHalfHeight = this.intHeight / 2;
            this.intQuarterWidth = this.intWidth / 4;
            this.intQuarterHeight = this.intHeight / 4;
            this.intVisualRadius = this.intHalfWidth;
            if (this.intHalfHeight > this.intVisualRadius) {
                this.intVisualRadius = this.intHalfHeight;
            }
            this.intCollisionHalfWidth = colWidth / 2;
            this.intCollisionHalfHeight = colHeight / 2;
            this.intCollisionRadius = this.intCollisionHalfWidth;
            if (this.intCollisionHalfHeight > this.intCollisionRadius) {
                this.intCollisionRadius = this.intCollisionHalfHeight;
            }
            this.cornerAngDiff = Functions.aTanFull((float) this.intCollisionHalfWidth, (float) this.intCollisionHalfHeight);
            this.cornerAngDist = LookUp.sqrt((float) ((this.intCollisionHalfWidth * this.intCollisionHalfWidth) + (this.intCollisionHalfHeight * this.intCollisionHalfHeight)));
            this.fltTurn = 0.0f;
            this.fltSpeed = 0.0f;
            this.fltOldSpeed = this.fltSpeed;
            this.fltMaxAccel = maxAccel;
            this.fltFriction = fltFric;
            this.fltMaxSpeed = maxSpeed;
            this.fltMaxTurn = maxTurn;
            this.type = t;
            this.cornerCoords = new double[8];
            this.sideInvCount = new int[4];
            this.sideArmourCount = new int[4];
            this.pod = new Pod(this);
        } catch (Exception e) {
            throw e;
        }
    }

    public void AI_SetMission_SeekAndDestroy() {
        this.AI_Mission = 0;
    }

    public void AI_SetMission_DefendUnit(Unit unit, int protectionRadius, int formationAng, int formationDistance) {
        this.AI_Mission = 1;
        this.AI_MissionUnit = unit;
        this.AI_MissionRadius = protectionRadius;
        this.AI_MissionAng = formationAng;
        this.AI_MissionDist = formationDistance;
    }

    public void AI_SetMission_AttackUnit(Unit unit) {
        this.AI_Mission = 2;
        this.AI_MissionUnit = unit;
    }

    public void AI_SetMission_MoveToLocation(double x, double y, int zoneRadius) {
        this.AI_Mission = 3;
        this.AI_MissionX = x;
        this.AI_MissionY = y;
        this.AI_MissionRadius = zoneRadius;
    }

    public void attachToUnit(Unit u, float ang, float dist, float centerAng, float pivotLimit, Map map, Units units) throws Exception {
        try {
            this.attachedTo = u;
            this.attachAngle = ang;
            this.attachDistance = dist;
            this.attachPivotCenter = centerAng;
            this.attachPivotLimit = pivotLimit;
            this.attachRelativeAng = centerAng;
            positionRotateAttached(map, units);
        } catch (Exception e) {
            throw e;
        }
    }

    private void positionRotateAttached(Map map, Units units) throws Exception {
        try {
            float ang = Functions.wrapAngle(this.attachAngle + this.attachedTo.fltAngle);
            this.fltWorldX = this.attachedTo.fltWorldX + ((double) (LookUp.sin(ang) * this.attachDistance));
            this.fltWorldY = this.attachedTo.fltWorldY + ((double) (LookUp.cos(ang) * this.attachDistance));
            this.fltAngle = Functions.wrapAngle(this.attachRelativeAng + this.attachedTo.fltAngle);
            postMoveProcesses(map, units);
        } catch (Exception e) {
            throw e;
        }
    }

    public void addInventory(int type2, int value, int side) throws Exception {
        try {
            this.inventory.add(new InventorySlot(type2, value, side));
            int[] iArr = this.sideInvCount;
            iArr[side] = iArr[side] + 1;
            if (type2 == 5) {
                this.numPowers++;
                this.powerReduce = this.fltMaxAccel / ((float) this.numPowers);
            }
            if (type2 == 4) {
                this.numTurns++;
                this.turnReduce = this.fltMaxTurn / ((float) this.numTurns);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean available() {
        if (this.isLeavingMap || this.isSafe || !this.inScreen || !this.shipAlive || !this.hasPilot) {
            return DEBUG_VISIBLITY;
        }
        return true;
    }

    public boolean navigationPossible() {
        if (this.isLeavingMap || this.isSafe || !this.isPlayable || !this.arrowInScreen || !this.shipAlive || !this.hasPilot) {
            return DEBUG_VISIBLITY;
        }
        return true;
    }

    public boolean firingPossible() {
        if (this.isLeavingMap || this.isSafe || !this.isPlayable || !this.shipAlive || !this.hasPilot) {
            return DEBUG_VISIBLITY;
        }
        return true;
    }

    public boolean inGameZone() {
        if (this.isSafe || !this.shipAlive) {
            return DEBUG_VISIBLITY;
        }
        return true;
    }

    public boolean eliminated() {
        if (!this.shipAlive || this.isSafe || !this.hasPilot) {
            return true;
        }
        return DEBUG_VISIBLITY;
    }

    public boolean onScreen() {
        if (this.isSafe || !this.shipAlive || !this.inScreen) {
            return DEBUG_VISIBLITY;
        }
        return true;
    }

    public void destroyInventory(int i) throws Exception {
        try {
            this.inventory.get(i).type = 1;
            this.inventory.get(i).weapon = DEBUG_VISIBLITY;
        } catch (Exception e) {
            throw e;
        }
    }

    public void addArmour(int side, int count) throws Exception {
        for (int i = 0; i < count; i++) {
            this.armour.add(new ArmourSlot(side));
        }
        try {
            this.sideArmourCount[side] = this.sideArmourCount[side] + count;
        } catch (Exception e) {
            throw e;
        }
    }

    public void damage(int side, int damage, Units units, Particles particles, Sounds sounds, Screen screen, GameThread thread) throws Exception {
        try {
            if (this.isPlayable) {
                thread.getCareerRecords().getStats().addDamageReceived(damage / 5);
            }
            for (int i = 0; i < this.armour.size(); i++) {
                ArmourSlot a = this.armour.get(i);
                if (a.side == side) {
                    if (a.value >= damage) {
                        a.value -= damage;
                        return;
                    } else {
                        damage -= a.value;
                        a.value = 0;
                    }
                }
            }
            int numItemsToDestroy = (int) Math.floor((double) (((float) damage) / 5.0f));
            this.itemsToDestroy.clear();
            for (int i2 = 0; i2 < this.inventory.size(); i2++) {
                if (this.inventory.get(i2).type != 1) {
                    this.itemsToDestroy.add(Integer.valueOf(i2));
                }
            }
            if (numItemsToDestroy >= this.itemsToDestroy.size()) {
                destroy(units, particles, sounds, screen, thread);
                return;
            }
            for (int i3 = 0; i3 < numItemsToDestroy; i3++) {
                InventorySlot is = this.inventory.get(this.itemsToDestroy.get((int) Math.floor(Math.random() * ((double) this.itemsToDestroy.size()))).intValue());
                switch (is.type) {
                    case 2:
                        pilotKill(sounds, screen, thread);
                        break;
                    case 3:
                        engineKill();
                        break;
                    case 4:
                        reduceTurn();
                        break;
                    case 5:
                        reducePower();
                        break;
                    case 6:
                        destroy(units, particles, sounds, screen, thread);
                        break;
                }
                is.type = 1;
                is.weapon = DEBUG_VISIBLITY;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void engineKill() throws Exception {
        try {
            this.fltTurn = 0.0f;
            this.fltMaxTurn = 0.0f;
            this.fltMaxAccel = 0.0f;
            this.jetEmitterOn = DEBUG_VISIBLITY;
            if (!this.isPlayable) {
                attemptEject();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean pilotAlive() {
        if (!this.hasEjected && this.shipAlive && this.hasPilot) {
            return true;
        }
        if (!this.hasEjected || !this.pod.podAlive) {
            return DEBUG_VISIBLITY;
        }
        return true;
    }

    private void attemptEject() {
        if (this.shipAlive && this.hasPilot && !this.hasEjected && !this.isSafe) {
            this.ejectOrder = true;
        }
    }

    private void pilotKill(Sounds sounds, Screen screen, GameThread thread) throws Exception {
        try {
            if (!this.hasEjected) {
                sounds.playSound(9, this.fltWorldX, this.fltWorldY, screen, thread);
            }
            this.hasPilot = DEBUG_VISIBLITY;
        } catch (Exception e) {
            throw e;
        }
    }

    private void reducePower() throws Exception {
        try {
            if (this.powerReduce > this.fltMaxAccel) {
                this.fltMaxAccel = 0.0f;
                this.jetEmitterOn = DEBUG_VISIBLITY;
            } else {
                this.fltMaxAccel -= this.powerReduce;
                this.jetEmitDelay *= 2;
            }
            if (!this.isPlayable) {
                this.AI_RunAway = true;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void reduceTurn() throws Exception {
        try {
            if (this.turnReduce > this.fltMaxTurn) {
                this.fltMaxTurn = 0.0f;
            } else {
                this.fltMaxTurn -= this.turnReduce;
            }
            if (!this.isPlayable) {
                this.AI_RunAway = true;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void destroy(Units u, Particles part, Sounds sounds, Screen screen, GameThread thread) throws Exception {
        try {
            this.shipAlive = DEBUG_VISIBLITY;
            this.jetEmitterOn = DEBUG_VISIBLITY;
            if (u.unitSelected == this) {
                u.unitSelected = null;
            }
            part.makeLargeExplosion(this.fltWorldX, this.fltWorldY);
            int numExplosions = Math.round(((float) this.intWidth) / 30.0f);
            int numExplosions2 = numExplosions * numExplosions;
            if (numExplosions2 > 16) {
                numExplosions2 = 16;
            }
            for (int i = 0; i < numExplosions2; i++) {
                part.makeLargeExplosion((this.fltWorldX - ((double) this.intHalfWidth)) + (Math.random() * ((double) this.intWidth)), (this.fltWorldY - ((double) this.intHalfWidth)) + (Math.random() * ((double) this.intWidth)));
                part.makeDebris(this.fltWorldX, this.fltWorldY, this.intWidth);
                part.makeDebris(this.fltWorldX, this.fltWorldY, this.intWidth);
            }
            part.makeShockWave(this.fltWorldX, this.fltWorldY, this.intHalfWidth);
            if (this.childrenAttached) {
                destroySubUnits(u, part, sounds, screen, thread);
            }
            if (((double) sounds.playSound(5, this.fltWorldX, this.fltWorldY, screen, thread)) <= 0.25d) {
                return;
            }
            if (this.type == 5 || this.type == 17 || this.type == 18 || this.type == 0 || this.type == 9 || this.type == 16) {
                thread.vibrate(400);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void destroySubUnits(Units u, Particles part, Sounds sounds, Screen screen, GameThread thread) throws Exception {
        int i = 0;
        while (i < u.unit.size()) {
            try {
                Unit killIt = u.unit.get(i);
                if (killIt.shipAlive && killIt.attachedTo == this) {
                    killIt.destroy(u, part, sounds, screen, thread);
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    private void setSubUnitsToSafe(Units u) throws Exception {
        int i = 0;
        while (i < u.unit.size()) {
            try {
                Unit safeIt = u.unit.get(i);
                if (safeIt.shipAlive && safeIt.attachedTo == this) {
                    safeIt.isSafe = true;
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public void calcCornerCoords() throws Exception {
        try {
            if (!this.cornerCoordsCalced) {
                float ang = Functions.wrapAngle(this.fltAngle - this.cornerAngDiff);
                this.cornerCoords[0] = this.fltWorldX + ((double) (LookUp.sin(ang) * this.cornerAngDist));
                this.cornerCoords[1] = this.fltWorldY + ((double) (LookUp.cos(ang) * this.cornerAngDist));
                float ang2 = Functions.wrapAngle(this.fltAngle + this.cornerAngDiff);
                this.cornerCoords[2] = this.fltWorldX + ((double) (LookUp.sin(ang2) * this.cornerAngDist));
                this.cornerCoords[3] = this.fltWorldY + ((double) (LookUp.cos(ang2) * this.cornerAngDist));
                float ang3 = Functions.wrapAngle(this.fltAngle + 180.0f + this.cornerAngDiff);
                this.cornerCoords[4] = this.fltWorldX + ((double) (LookUp.sin(ang3) * this.cornerAngDist));
                this.cornerCoords[5] = this.fltWorldY + ((double) (LookUp.cos(ang3) * this.cornerAngDist));
                float ang4 = Functions.wrapAngle((this.fltAngle + 180.0f) - this.cornerAngDiff);
                this.cornerCoords[6] = this.fltWorldX + ((double) (LookUp.sin(ang4) * this.cornerAngDist));
                this.cornerCoords[7] = this.fltWorldY + ((double) (LookUp.cos(ang4) * this.cornerAngDist));
                this.cornerCoordsCalced = true;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void eject(Map map, Sounds sounds, Screen screen, GameThread thread) throws Exception {
        try {
            this.ejectOrder = DEBUG_VISIBLITY;
            if (this.hasPilot) {
                this.hasPilot = DEBUG_VISIBLITY;
                this.hasEjected = true;
                this.pod.launch(map, sounds, screen, thread);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void move(Particles part, Map map, Units units) throws Exception {
        try {
            if (this.attachedTo != null) {
                this.attachRelativeAng = Functions.wrapAngle(this.attachRelativeAng + this.fltTurn);
                float angDiff = Functions.angDiff(this.attachRelativeAng, this.attachPivotCenter);
                if (angDiff > this.attachPivotLimit) {
                    this.attachRelativeAng = Functions.wrapAngle(this.attachPivotCenter + this.attachPivotLimit);
                } else if (angDiff < (-this.attachPivotLimit)) {
                    this.attachRelativeAng = Functions.wrapAngle(this.attachPivotCenter - this.attachPivotLimit);
                }
                positionRotateAttached(map, units);
                return;
            }
            this.fltWorldX = this.fltWorldX + ((double) this.fltXSpd);
            this.fltWorldY = this.fltWorldY + ((double) this.fltYSpd);
            this.fltXSpd = this.fltXSpd * this.fltFriction;
            this.fltYSpd = this.fltYSpd * this.fltFriction;
            this.fltWorldX = this.fltWorldX + ((double) (LookUp.sin(this.fltAngle) * this.fltSpeed));
            this.fltWorldY = this.fltWorldY + ((double) (LookUp.cos(this.fltAngle) * this.fltSpeed));
            this.fltAngle = Functions.wrapAngle(this.fltAngle + this.fltTurn);
            this.fltOldSpeed = this.fltSpeed;
            if (this.jetEmitterOn) {
                this.jetEmitCount = this.jetEmitCount - 1;
                float emitDistX = (float) (this.fltWorldX - this.lastEmitX);
                float emitDistY = (float) (this.fltWorldY - this.lastEmitY);
                if ((emitDistX * emitDistX) + (emitDistY * emitDistY) > 16.0f || this.jetEmitCount <= 0) {
                    if (this.visibleToPlayer) {
                        part.makeJetFlame(this.fltWorldX, this.fltWorldY, this.fltXSpd, this.fltYSpd, this.fltAngle, (float) this.intHalfHeight, 0.5f, Math.abs(this.fltSpeed / this.fltMaxSpeed));
                    }
                    this.lastEmitX = this.fltWorldX;
                    this.lastEmitY = this.fltWorldY;
                    this.jetEmitCount = this.jetEmitDelay;
                }
            }
            postMoveProcesses(map, units);
        } catch (Exception e) {
            throw e;
        }
    }

    public double[] getPositionAngleAfterTurn() throws Exception {
        try {
            double[] returnCoords = new double[3];
            double x = this.fltWorldX;
            double y = this.fltWorldY;
            float ang = this.fltAngle;
            float xspd = this.fltXSpd;
            float yspd = this.fltYSpd;
            for (int i = 0; i < 60; i++) {
                double x2 = x + ((double) xspd);
                double y2 = y + ((double) yspd);
                xspd *= this.fltFriction;
                yspd *= this.fltFriction;
                x = x2 + ((double) (LookUp.sin(ang) * this.fltSpeed));
                y = y2 + ((double) (LookUp.cos(ang) * this.fltSpeed));
                ang = Functions.wrapAngle(this.fltTurn + ang);
            }
            returnCoords[0] = x;
            returnCoords[1] = y;
            returnCoords[2] = (double) ang;
            return returnCoords;
        } catch (Exception e) {
            throw e;
        }
    }

    public void position(double x, double y, float ang, Map map, Units units) throws Exception {
        try {
            this.fltWorldX = x;
            this.fltWorldY = y;
            this.fltAngle = ang;
            postMoveProcesses(map, units);
        } catch (Exception e) {
            throw e;
        }
    }

    public void setSpeed(float speed) throws Exception {
        try {
            this.fltSpeed = speed;
            if (this.fltSpeed > this.fltMaxSpeed) {
                this.fltSpeed = this.fltMaxSpeed;
            }
            if (this.fltSpeed < (-this.fltMaxSpeed)) {
                this.fltSpeed = -this.fltMaxSpeed;
            }
            this.fltOldSpeed = speed;
        } catch (Exception e) {
            throw e;
        }
    }

    public void setTurn(float turn) throws Exception {
        try {
            if (turn > this.fltMaxTurn) {
                turn = this.fltMaxTurn;
            }
            if (turn < (-this.fltMaxTurn)) {
                turn = -this.fltMaxTurn;
            }
            this.fltTurn = turn;
        } catch (Exception e) {
            throw e;
        }
    }

    public void projectAway(double x, double y, float distance, Map map, Units units) throws Exception {
        try {
            float ang = Functions.aTanFull((float) (this.fltWorldX - x), (float) (this.fltWorldY - y));
            position(this.fltWorldX + ((double) (LookUp.sin(ang) * distance)), this.fltWorldY + ((double) (LookUp.cos(ang) * distance)), this.fltAngle, map, units);
        } catch (Exception e) {
            throw e;
        }
    }

    private void postMoveProcesses(Map map, Units units) throws Exception {
        try {
            this.cornerCoordsCalced = DEBUG_VISIBLITY;
            if (!this.isLeavingMap) {
                if (this.fltWorldX < 150.0d) {
                    leaveZone(270.0f);
                }
                if (this.fltWorldX > ((double) (map.worldSizeX - 150))) {
                    leaveZone(90.0f);
                }
                if (this.fltWorldY < 150.0d) {
                    leaveZone(180.0f);
                }
                if (this.fltWorldY > ((double) (map.worldSizeY - 150))) {
                    leaveZone(0.0f);
                }
            } else if (this.fltWorldX < 0.0d || this.fltWorldX > ((double) map.worldSizeX) || this.fltWorldY < 0.0d || this.fltWorldY > ((double) map.worldSizeY)) {
                this.isSafe = true;
                this.jetEmitterOn = DEBUG_VISIBLITY;
                if (this.childrenAttached) {
                    setSubUnitsToSafe(units);
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void leaveZone(float targetAng) {
        this.isLeavingMap = true;
        this.leavingAng = targetAng;
    }

    public void calcSpeedTurn(float x, float y, Screen screen, float distance) throws Exception {
        float xdiff;
        float ydiff;
        boolean flipSpeed = DEBUG_VISIBLITY;
        try {
            float xdiff2 = x - ((float) this.intScreenX);
            float ydiff2 = ((float) this.intScreenY) - y;
            float ang = Functions.aTanFull(xdiff2, ydiff2);
            if ((xdiff2 * xdiff2) + (ydiff2 * ydiff2) > distance * distance) {
                xdiff = (x - ((float) this.intScreenX)) - (LookUp.sin(ang) * distance);
                ydiff = (((float) this.intScreenY) - y) - (LookUp.cos(ang) * distance);
            } else {
                xdiff = 0.0f;
                ydiff = 0.0f;
            }
            if (!screen.zoomedIn) {
                xdiff *= 2.0f;
                ydiff *= 2.0f;
            }
            float diff = Functions.angDiff(Functions.wrapAngle(ang), this.fltAngle);
            if (diff > 90.0f || diff < -90.0f) {
                flipSpeed = true;
            }
            float newSpeed = LookUp.sqrt((xdiff * xdiff) + (ydiff * ydiff)) / 60.0f;
            if (flipSpeed) {
                newSpeed *= -1.0f;
            }
            if (newSpeed - this.fltOldSpeed > this.fltMaxAccel) {
                newSpeed = this.fltOldSpeed + this.fltMaxAccel;
            }
            if (newSpeed - this.fltOldSpeed < this.fltMaxAccel * -1.0f) {
                newSpeed = this.fltOldSpeed - this.fltMaxAccel;
            }
            if (newSpeed > this.fltMaxSpeed) {
                newSpeed = this.fltMaxSpeed;
            }
            if (newSpeed < this.fltMaxSpeed * -1.0f) {
                newSpeed = this.fltMaxSpeed * -1.0f;
            }
            this.fltSpeed = newSpeed;
            if (this.fltSpeed < 0.0f) {
                if (diff > 90.0f) {
                    diff = (180.0f - diff) * -1.0f;
                } else if (diff < -90.0f) {
                    diff += 180.0f;
                }
            }
            this.fltTurn = diff / 60.0f;
            if (this.fltTurn > this.fltMaxTurn) {
                this.fltTurn = this.fltMaxTurn;
            }
            if (this.fltTurn < this.fltMaxTurn * -1.0f) {
                this.fltTurn = this.fltMaxTurn * -1.0f;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void setScreenPosition(Screen screen) throws Exception {
        try {
            this.intScreenX = screen.canvasX((int) this.fltWorldX);
            this.intScreenY = screen.canvasY((int) this.fltWorldY);
            this.inScreen = screen.inScreenWorldValues(this.fltWorldX, this.fltWorldY, this.intVisualRadius, this.intVisualRadius);
            this.arrowInScreen = screen.inScreenWorldValues(this.dblNoChangeWorldX, this.dblNoChangeWorldY, 10, 10);
        } catch (Exception e) {
            throw e;
        }
    }

    public void setVisibleToPlayer(boolean vis) {
        this.visibleToPlayer = vis;
        if (!vis) {
            this.shipAlpha = 0;
        }
    }

    public boolean getVisibleToPlayer() {
        return this.visibleToPlayer;
    }

    public void draw(Canvas canvas, Screen screen, Bitmap img, Paint alphaPaint) throws Exception {
        try {
            if (this.shipAlpha < 255) {
                this.shipAlpha += 10;
                if (this.shipAlpha > 255) {
                    this.shipAlpha = 255;
                }
            }
            alphaPaint.setAlpha(this.shipAlpha);
            canvas.save();
            canvas.rotate(this.fltAngle, (float) this.intScreenX, (float) this.intScreenY);
            canvas.drawBitmap(img, (float) (this.intScreenX - (img.getWidth() / 2)), (float) (this.intScreenY - (img.getHeight() / 2)), alphaPaint);
            canvas.restore();
        } catch (Exception e) {
            throw e;
        }
    }

    public void workOutDestination(NavArrow navArrow, Screen screen) throws Exception {
        try {
            navArrow.drawLine(null, this.intScreenX, this.intScreenY, this.fltAngle, this.fltSpeed, this.fltTurn, screen, DEBUG_VISIBLITY, DEBUG_VISIBLITY, this.team);
            this.fltScreenFinishX = navArrow.fltX;
            this.fltScreenFinishY = navArrow.fltY;
            this.fltScreenFinishAng = navArrow.fltAng;
            this.arrowInScreen = screen.inScreenScreenValues((int) this.fltScreenFinishX, (int) this.fltScreenFinishY, 10, 10);
        } catch (Exception e) {
            throw e;
        }
    }

    public void drawArrowLine(Canvas canvas, NavArrow navArrow, Screen screen) throws Exception {
        try {
            navArrow.drawLine(canvas, this.intScreenX, this.intScreenY, this.fltAngle, this.fltSpeed, this.fltTurn, screen, !this.isPlayable, this.team != 0, this.team);
            this.fltScreenArrowHeadX = navArrow.fltX;
            this.fltScreenArrowHeadY = navArrow.fltY;
            this.fltScreenArrowHeadAng = navArrow.fltAng;
            this.arrowInScreen = screen.inScreenScreenValues((int) this.fltScreenArrowHeadX, (int) this.fltScreenArrowHeadY, 10, 10);
        } catch (Exception e) {
            throw e;
        }
    }

    public void drawArrowHead(Canvas canvas, NavArrow navArrow) throws Exception {
        try {
            navArrow.drawHead(canvas, this.fltScreenArrowHeadX, this.fltScreenArrowHeadY, this.fltScreenArrowHeadAng, this.team, this.isPlayable);
        } catch (Exception e) {
            throw e;
        }
    }

    public void calcNoChangeValues() throws Exception {
        this.dblNoChangeWorldX = this.fltWorldX + ((double) (LookUp.sin(this.fltAngle) * this.fltSpeed * 60.0f));
        this.dblNoChangeWorldY = this.fltWorldY + ((double) (LookUp.cos(this.fltAngle) * this.fltSpeed * 60.0f));
    }

    public void startTurn(Projectiles projectiles, Map map, Units units, Asteroids asteroids, Sounds sounds, Screen screen, GameThread thread) throws Exception {
        try {
            if (this.isLeavingMap) {
                setTurn(Functions.angDiff(this.leavingAng, this.fltAngle) / 60.0f);
                setSpeed(this.fltSpeed + this.fltMaxAccel);
            } else {
                this.fltTurn = 0.0f;
            }
            for (int i = 0; i < this.inventory.size(); i++) {
                InventorySlot inv = this.inventory.get(i);
                if (inv.weapon) {
                    if (inv.loading) {
                        inv.loading = DEBUG_VISIBLITY;
                    }
                    if (inv.firing) {
                        inv.loading = true;
                        inv.firing = DEBUG_VISIBLITY;
                    }
                }
            }
            this.rocketRefire = 0;
            this.rocketFireCount = 0;
            this.gunRefire = 0;
            this.warning = projectiles.missileTrackingUnit(this) != null ? true : DEBUG_VISIBLITY;
            if (this.warning && this.team == 0) {
                units.mPlayWarning = true;
            }
            if (this.team != 0) {
                calcVisibleToPlayer(units, asteroids, sounds, screen, thread);
            }
            if (!this.isPlayable && !this.isLeavingMap && this.hasPilot) {
                this.AI_MinSpeed = 0.0f;
                AI(map, units, asteroids, projectiles);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void calcVisibleToPlayer(Units units, Asteroids asteroids, Sounds sounds, Screen screen, GameThread thread) throws Exception {
        double pointx;
        double pointy;
        try {
            if (this.attachedTo != null) {
                setVisibleToPlayer(this.attachedTo.visibleToPlayer);
                return;
            }
            for (int i = 0; i < units.unit.size(); i++) {
                Unit friendlyUnit = units.unit.get(i);
                if (friendlyUnit.team == 0 && friendlyUnit.inGameZone()) {
                    for (int point = 0; point < 5; point++) {
                        if (point == 0) {
                            pointx = this.fltWorldX;
                            pointy = this.fltWorldY;
                        } else {
                            pointx = this.cornerCoords[(point - 1) * 2];
                            pointy = this.cornerCoords[((point - 1) * 2) + 1];
                        }
                        if (Functions.inCircle(pointx, pointy, friendlyUnit.fltWorldX, friendlyUnit.fltWorldY, 1400)) {
                            if (!asteroids.lineIntersectAsteroid(pointx, pointy, friendlyUnit.fltWorldX, friendlyUnit.fltWorldY, this.intCollisionRadius)) {
                                if (!units.lineIntersectsUnitCircle(pointx, pointy, friendlyUnit.fltWorldX, friendlyUnit.fltWorldY, 0, getAttachedUnits(units, true, friendlyUnit))) {
                                    if (!this.visibleToPlayer) {
                                        sounds.playSound(10, (double) screen.intMiddleX, (double) screen.intMiddleY, screen, thread);
                                    }
                                    setVisibleToPlayer(true);
                                    return;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                    continue;
                }
            }
            setVisibleToPlayer(DEBUG_VISIBLITY);
        } catch (Exception e) {
            throw e;
        }
    }

    public Unit[] getAttachedUnits(Units units, boolean addSelf, Unit extraToAdd) throws Exception {
        Exception e;
        int elementPos;
        int elementPos2;
        int elementPos3;
        int numElements = 0;
        if (addSelf) {
            numElements = 0 + 1;
        }
        if (extraToAdd != null) {
            numElements++;
        }
        try {
            if (this.childrenAttached) {
                for (int i = 0; i < units.unit.size(); i++) {
                    Unit attachUnit = units.unit.get(i);
                    if (attachUnit.attachedTo == this && attachUnit.inGameZone()) {
                        numElements++;
                    }
                }
            }
            Unit[] returnArray = new Unit[numElements];
            if (addSelf) {
                elementPos = 0 + 1;
                try {
                    returnArray[0] = this;
                } catch (Exception e2) {
                    e = e2;
                }
            } else {
                elementPos = 0;
            }
            if (extraToAdd != null) {
                elementPos2 = elementPos + 1;
                returnArray[elementPos] = extraToAdd;
            } else {
                elementPos2 = elementPos;
            }
            if (this.childrenAttached) {
                int i2 = 0;
                while (true) {
                    elementPos = elementPos3;
                    if (i2 >= units.unit.size()) {
                        break;
                    }
                    Unit attachUnit2 = units.unit.get(i2);
                    if (attachUnit2.attachedTo != this || !attachUnit2.inGameZone()) {
                        elementPos3 = elementPos;
                    } else {
                        elementPos3 = elementPos + 1;
                        returnArray[elementPos] = attachUnit2;
                    }
                    i2++;
                }
            }
            return returnArray;
        } catch (Exception e3) {
            e = e3;
        }
        throw e;
    }

    public InventorySlot getGun(int index, int side) throws Exception {
        int count = 0;
        int i = 0;
        while (i < this.inventory.size()) {
            try {
                InventorySlot is = this.inventory.get(i);
                if (is.side == side && is.weapon) {
                    if (count == index) {
                        return is;
                    }
                    count++;
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
        return null;
    }

    public int getPilotDead() {
        if (!this.shipAlive && !this.hasEjected) {
            return 2;
        }
        if (this.shipAlive && !this.hasEjected && !this.hasPilot) {
            return 1;
        }
        if (!this.hasEjected || this.pod.podAlive) {
            return 0;
        }
        return 3;
    }

    public boolean pilotJustKilled() {
        if (this.pilotKilledReturned) {
            return DEBUG_VISIBLITY;
        }
        if (getPilotDead() == 0) {
            return DEBUG_VISIBLITY;
        }
        this.pilotKilledReturned = true;
        return true;
    }

    private void AI(Map map, Units units, Asteroids asteroids, Projectiles projectiles) throws Exception {
        try {
            if (!AI_CheckEvadeProjectiles(projectiles, map) && !AI_CheckRunAway(map, asteroids, units) && !AI_CheckAvoidPursuit(units, map)) {
                switch (this.AI_Mission) {
                    case 0:
                        if (AI_TargetEnemy(null, units, map, asteroids) == null) {
                            AI_MoveToSeekLocation(asteroids, units, map);
                            break;
                        }
                        break;
                    case 1:
                        if (this.AI_MissionUnit != null && this.AI_MissionUnit.inGameZone()) {
                            int xDist = (int) (this.fltWorldX - this.AI_MissionUnit.fltWorldX);
                            int yDist = (int) (this.fltWorldY - this.AI_MissionUnit.fltWorldY);
                            if ((xDist * xDist) + (yDist * yDist) < this.AI_MissionRadius * this.AI_MissionRadius) {
                                if (AI_TargetEnemy(null, units, map, asteroids) == null) {
                                    AI_MoveToFormationPosition(this.AI_MissionUnit, (float) this.AI_MissionAng, this.AI_MissionDist, asteroids, units, map);
                                    break;
                                }
                            } else {
                                AI_MoveToFormationPosition(this.AI_MissionUnit, (float) this.AI_MissionAng, this.AI_MissionDist, asteroids, units, map);
                                break;
                            }
                        } else {
                            this.AI_Mission = 0;
                            break;
                        }
                        break;
                    case 2:
                        if (this.AI_MissionUnit != null && this.AI_MissionUnit.inGameZone()) {
                            int xDist2 = (int) (this.fltWorldX - this.AI_MissionUnit.fltWorldX);
                            int yDist2 = (int) (this.fltWorldY - this.AI_MissionUnit.fltWorldY);
                            if ((xDist2 * xDist2) + (yDist2 * yDist2) < 1950000) {
                                if (AI_TargetEnemy(this.AI_MissionUnit, units, map, asteroids) != this.AI_MissionUnit) {
                                    AI_CheckMoveToCoordinates(this.AI_MissionUnit.dblNoChangeWorldX, this.AI_MissionUnit.dblNoChangeWorldY, 100, asteroids, units, getAttachedUnits(units, DEBUG_VISIBLITY, this.AI_MissionUnit), DEBUG_VISIBLITY, map);
                                    break;
                                }
                            } else {
                                AI_CheckMoveToCoordinates(this.AI_MissionUnit.dblNoChangeWorldX, this.AI_MissionUnit.dblNoChangeWorldY, 100, asteroids, units, getAttachedUnits(units, DEBUG_VISIBLITY, this.AI_MissionUnit), DEBUG_VISIBLITY, map);
                                break;
                            }
                        } else {
                            this.AI_Mission = 0;
                            break;
                        }
                        break;
                    case 3:
                        if (!AI_CheckMoveToCoordinates(this.AI_MissionX, this.AI_MissionY, this.AI_MissionRadius, asteroids, units, getAttachedUnits(units, DEBUG_VISIBLITY, null), true, map) && AI_TargetEnemy(null, units, map, asteroids) == null) {
                            this.AI_X = this.fltWorldX;
                            this.AI_Y = this.fltWorldY;
                            break;
                        }
                }
            }
            AI_SetTurnAndThrust();
            AI_CheckFireWeapons(units, asteroids);
        } catch (Exception e) {
            throw e;
        }
    }

    private boolean AI_MoveToFormationPosition(Unit unit, float angle, int dist, Asteroids asteroids, Units units, Map map) throws Exception {
        try {
            float angle2 = Functions.wrapAngle(unit.fltAngle + angle);
            double[] shipCoords = unit.getPositionAngleAfterTurn();
            this.AI_X = shipCoords[0] + ((double) (LookUp.sin(angle2) * ((float) dist)));
            this.AI_Y = shipCoords[1] + ((double) (LookUp.cos(angle2) * ((float) dist)));
            return AI_CheckMoveToCoordinates(this.AI_X, this.AI_Y, this.intCollisionRadius, asteroids, units, getAttachedUnits(units, DEBUG_VISIBLITY, null), DEBUG_VISIBLITY, map);
        } catch (Exception e) {
            throw e;
        }
    }

    private boolean AI_CheckEvadeProjectiles(Projectiles projectiles, Map map) throws Exception {
        try {
            if (movementClass() == 2 || movementClass() == 0) {
                return DEBUG_VISIBLITY;
            }
            Missile missile = projectiles.missileTrackingUnit(this);
            if (missile != null) {
                AI_EvadeHomingMissile(missile.x, missile.y, map);
                return true;
            }
            for (int i = 0; i < projectiles.missile.size(); i++) {
                Missile missile2 = projectiles.missile.get(i);
                float xDist = (float) (missile2.x - this.fltWorldX);
                float yDist = (float) (missile2.y - this.fltWorldY);
                if ((xDist * xDist) + (yDist * yDist) < ((float) 160000) && AI_EvadeProjectile(missile2.x, missile2.y, missile2.ang, missile2.speed, map)) {
                    return true;
                }
            }
            for (int i2 = 0; i2 < projectiles.bullet.size(); i2++) {
                Bullet bullet = projectiles.bullet.get(i2);
                float xDist2 = (float) (bullet.x - this.fltWorldX);
                float yDist2 = (float) (bullet.y - this.fltWorldY);
                if ((xDist2 * xDist2) + (yDist2 * yDist2) < ((float) 160000) && AI_EvadeProjectile(bullet.x, bullet.y, bullet.ang, 12.0f, map)) {
                    return true;
                }
            }
            return DEBUG_VISIBLITY;
        } catch (Exception e) {
            throw e;
        }
    }

    private boolean AI_EvadeProjectile(double x, double y, float ang, float spd, Map map) throws Exception {
        int turn;
        try {
            if (Functions.linesIntersect(x, y, x + ((double) (LookUp.sin(ang) * spd * 60.0f)), y + ((double) (LookUp.cos(ang) * spd * 60.0f)), this.fltWorldX, this.fltWorldY, this.dblNoChangeWorldX, this.dblNoChangeWorldY)[0] == 0.0d) {
                return DEBUG_VISIBLITY;
            }
            float angDiff = Functions.angDiff(ang, this.fltAngle);
            if (angDiff < 0.0f) {
                if (angDiff > -90.0f) {
                    turn = 1;
                } else {
                    turn = 2;
                }
            } else if (angDiff < 90.0f) {
                turn = 2;
            } else {
                turn = 1;
            }
            AI_CalcEvadeCoordsFromTurn(turn, map);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    private void AI_EvadeHomingMissile(double x, double y, Map map) throws Exception {
        int turn;
        try {
            float xDist = (float) (x - this.fltWorldX);
            float yDist = (float) (y - this.fltWorldY);
            boolean inThreshhold = (xDist * xDist) + (yDist * yDist) < 160000.0f;
            float angDiff = Functions.angDiff(Functions.aTanFull(xDist, yDist), this.fltAngle);
            float absAngDiff = Math.abs(angDiff);
            if (absAngDiff < 30.0f) {
                turn = 1;
            } else if (absAngDiff < 60.0f) {
                if (inThreshhold) {
                    turn = 1;
                } else {
                    turn = 0;
                }
            } else if (absAngDiff < 90.0f) {
                turn = 0;
            } else if (absAngDiff >= 120.0f) {
                turn = 2;
            } else if (inThreshhold) {
                turn = 2;
            } else {
                turn = 0;
            }
            if (angDiff < 0.0f) {
                if (turn == 2) {
                    turn = 1;
                } else if (turn == 1) {
                    turn = 2;
                }
            }
            AI_CalcEvadeCoordsFromTurn(turn, map);
        } catch (Exception e) {
            throw e;
        }
    }

    private void AI_CalcEvadeCoordsFromTurn(int turn, Map map) throws Exception {
        if (turn == 0) {
            try {
                this.AI_X = this.fltWorldX + ((double) (LookUp.sin(this.fltAngle) * 500.0f));
                this.AI_Y = this.fltWorldY + ((double) (LookUp.cos(this.fltAngle) * 500.0f));
            } catch (Exception e) {
                throw e;
            }
        } else if (turn == 1) {
            this.AI_X = this.fltWorldX + ((double) (LookUp.sin(Functions.wrapAngle(this.fltAngle - 60.0f)) * 500.0f));
            this.AI_Y = this.fltWorldY + ((double) (LookUp.cos(Functions.wrapAngle(this.fltAngle - 60.0f)) * 500.0f));
        } else {
            this.AI_X = this.fltWorldX + ((double) (LookUp.sin(Functions.wrapAngle(this.fltAngle + 60.0f)) * 500.0f));
            this.AI_Y = this.fltWorldY + ((double) (LookUp.cos(Functions.wrapAngle(this.fltAngle + 60.0f)) * 500.0f));
        }
        ConstrainAI_X_AI_YToBorders(map);
    }

    private boolean AI_CheckRunAway(Map map, Asteroids asteroids, Units units) throws Exception {
        try {
            if (movementClass() == 0) {
                return DEBUG_VISIBLITY;
            }
            if (!(this.AI_RunAway || this.type == 17 || this.type == 5)) {
                int ammo = 0;
                for (int i = 0; i < this.inventory.size(); i++) {
                    InventorySlot inv = this.inventory.get(i);
                    if (inv.weapon) {
                        ammo += inv.value;
                    }
                }
                if (ammo <= 0) {
                    this.AI_RunAway = true;
                }
            }
            if (!this.AI_RunAway) {
                return DEBUG_VISIBLITY;
            }
            int xDiff = (int) (this.fltWorldX - ((double) (map.worldSizeX / 2)));
            int yDiff = (int) (this.fltWorldY - ((double) (map.worldSizeY / 2)));
            if (Math.abs(xDiff) > Math.abs(yDiff)) {
                this.AI_Y = this.fltWorldY;
                if (xDiff < 0) {
                    this.AI_X = -100.0d;
                } else {
                    this.AI_X = (double) (map.worldSizeX + 100);
                }
            } else {
                this.AI_X = this.fltWorldX;
                if (yDiff < 0) {
                    this.AI_Y = -100.0d;
                } else {
                    this.AI_Y = (double) (map.worldSizeY + 100);
                }
            }
            return AI_CheckMoveToCoordinates(this.AI_X, this.AI_Y, 10, asteroids, units, getAttachedUnits(units, DEBUG_VISIBLITY, null), true, map);
        } catch (Exception e) {
            throw e;
        }
    }

    private boolean AI_CheckAvoidPursuit(Units units, Map map) throws Exception {
        try {
            if (movementClass() == 0) {
                return DEBUG_VISIBLITY;
            }
            for (int i = 0; i < units.unit.size(); i++) {
                Unit u = units.unit.get(i);
                if (!(u == this || u.team == this.team || !u.inGameZone())) {
                    float xDiff = (float) (u.fltWorldX - this.fltWorldX);
                    float yDiff = (float) (u.fltWorldY - this.fltWorldY);
                    if ((xDiff * xDiff) + (yDiff * yDiff) < ((float) 160000)) {
                        float faceAngDiff = Functions.angDiff(u.fltAngle, this.fltAngle);
                        if (Math.abs(faceAngDiff) < 30.0f && Math.abs(Functions.angDiff(Functions.aTanFull(xDiff, yDiff), this.fltAngle)) > 150.0f) {
                            if (faceAngDiff > 0.0f) {
                                AI_CalcEvadeCoordsFromTurn(1, map);
                            } else {
                                AI_CalcEvadeCoordsFromTurn(2, map);
                            }
                            return true;
                        }
                    } else {
                        continue;
                    }
                }
            }
            return DEBUG_VISIBLITY;
        } catch (Exception e) {
            throw e;
        }
    }

    private boolean AI_CheckMoveToCoordinates(double x, double y, int boundary, Asteroids asteroids, Units units, Unit[] ignoreUnit, boolean allowMapEdge, Map map) throws Exception {
        try {
            int xDist = (int) (x - this.fltWorldX);
            int yDist = (int) (y - this.fltWorldY);
            if ((xDist * xDist) + (yDist * yDist) < boundary * boundary) {
                return DEBUG_VISIBLITY;
            }
            AI_SetPathWithAvoidance(this.fltWorldX, this.fltWorldY, x, y, asteroids, units, ignoreUnit);
            if (!allowMapEdge) {
                ConstrainAI_X_AI_YToBorders(map);
            }
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    private void ConstrainAI_X_AI_YToBorders(Map map) throws Exception {
        try {
            int buffer = (int) (200.0f + (this.fltSpeed * 60.0f));
            if (this.AI_X < ((double) buffer)) {
                this.AI_X = (double) buffer;
            } else if (this.AI_X > ((double) (map.worldSizeX - buffer))) {
                this.AI_X = (double) (map.worldSizeX - buffer);
            }
            if (this.AI_Y < ((double) buffer)) {
                this.AI_Y = (double) buffer;
            } else if (this.AI_Y > ((double) (map.worldSizeY - buffer))) {
                this.AI_Y = (double) (map.worldSizeY - buffer);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x005d A[Catch:{ Exception -> 0x005f }, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0034 A[Catch:{ Exception -> 0x005f }, ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean AI_SetPathWithAvoidance(double r21, double r23, double r25, double r27, com.hangfire.spacesquadronFREE.Asteroids r29, com.hangfire.spacesquadronFREE.Units r30, com.hangfire.spacesquadronFREE.Unit[] r31) throws java.lang.Exception {
        /*
            r20 = this;
            r18 = 0
            r0 = r25
            r2 = r20
            r2.AI_X = r0     // Catch:{ Exception -> 0x005f }
            r0 = r27
            r2 = r20
            r2.AI_Y = r0     // Catch:{ Exception -> 0x005f }
            r17 = 0
        L_0x0010:
            r0 = r20
            double r0 = r0.AI_X     // Catch:{ Exception -> 0x005f }
            r8 = r0
            r0 = r20
            double r0 = r0.AI_Y     // Catch:{ Exception -> 0x005f }
            r10 = r0
            r3 = r20
            r4 = r21
            r6 = r23
            r12 = r29
            r13 = r30
            r14 = r31
            boolean r3 = r3.AI_GetAvoidanceCircle(r4, r6, r8, r10, r12, r13, r14)     // Catch:{ Exception -> 0x005f }
            if (r3 == 0) goto L_0x0032
            r3 = 4
            r0 = r18
            r1 = r3
            if (r0 < r1) goto L_0x0036
        L_0x0032:
            if (r18 <= 0) goto L_0x005d
            r3 = 1
        L_0x0035:
            return r3
        L_0x0036:
            r0 = r20
            double r0 = r0.AI_X     // Catch:{ Exception -> 0x005f }
            r8 = r0
            r0 = r20
            double r0 = r0.AI_Y     // Catch:{ Exception -> 0x005f }
            r10 = r0
            r0 = r20
            double r0 = r0.AI_AvoidanceCircleX     // Catch:{ Exception -> 0x005f }
            r12 = r0
            r0 = r20
            double r0 = r0.AI_AvoidanceCircleY     // Catch:{ Exception -> 0x005f }
            r14 = r0
            r0 = r20
            int r0 = r0.AI_AvoidanceCircleRadius     // Catch:{ Exception -> 0x005f }
            r16 = r0
            r3 = r20
            r4 = r21
            r6 = r23
            int r17 = r3.AI_SetPathToCircleAvoidance(r4, r6, r8, r10, r12, r14, r16, r17)     // Catch:{ Exception -> 0x005f }
            int r18 = r18 + 1
            goto L_0x0010
        L_0x005d:
            r3 = 0
            goto L_0x0035
        L_0x005f:
            r3 = move-exception
            r19 = r3
            throw r19
        */
        throw new UnsupportedOperationException("Method not decompiled: com.hangfire.spacesquadronFREE.Unit.AI_SetPathWithAvoidance(double, double, double, double, com.hangfire.spacesquadronFREE.Asteroids, com.hangfire.spacesquadronFREE.Units, com.hangfire.spacesquadronFREE.Unit[]):boolean");
    }

    private int AI_SetPathToCircleAvoidance(double startX, double startY, double endX, double endY, double cX, double cY, int cRad, int turn) throws Exception {
        int turn2;
        float newAng;
        try {
            float avoidRadius = (float) (this.intCollisionRadius + cRad + 10 + 10);
            int xDist = (int) (cX - startX);
            int yDist = (int) (cY - startY);
            float dist = LookUp.sqrt((float) ((xDist * xDist) + (yDist * yDist)));
            if (dist < avoidRadius) {
                dist = avoidRadius;
            }
            float turnAng = Functions.toDeg((float) Math.asin((double) ((avoidRadius / 2.0f) / dist))) * 2.0f;
            float angToCircle = Functions.aTanFull((float) xDist, (float) yDist);
            float angDiff = Functions.angDiff(angToCircle, this.fltAngle);
            if (turn == 1 || (angDiff > 0.0f && turn != 2)) {
                newAng = Functions.wrapAngle(angToCircle - turnAng);
                turn2 = 1;
            } else {
                newAng = Functions.wrapAngle(angToCircle + turnAng);
                turn2 = 2;
            }
            this.AI_X = this.fltWorldX + ((double) (LookUp.sin(newAng) * dist * 2.0f));
            this.AI_Y = this.fltWorldY + ((double) (LookUp.cos(newAng) * dist * 2.0f));
            return turn2;
        } catch (Exception e) {
            throw e;
        }
    }

    private boolean AI_GetAvoidanceCircle(double startX, double startY, double endX, double endY, Asteroids asteroids, Units units, Unit[] ignoreUnit) throws Exception {
        try {
            if (movementClass() == 0) {
                return DEBUG_VISIBLITY;
            }
            int closestDistance = 160000;
            boolean avoid = DEBUG_VISIBLITY;
            float lineAng = Functions.aTanFull((float) (endX - startX), (float) (endY - startY));
            double endX2 = endX + ((double) (LookUp.sin(lineAng) * this.fltSpeed * 60.0f));
            double endY2 = endY + ((double) (LookUp.cos(lineAng) * this.fltSpeed * 60.0f));
            for (int i = 0; i < asteroids.asteroid.size(); i++) {
                Asteroid a = asteroids.asteroid.get(i);
                double astX = a.dblWorldX + ((double) (a.speedX * 60.0f));
                double astY = a.dblWorldY + ((double) (a.speedY * 60.0f));
                int xDist = (int) (startX - astX);
                int yDist = (int) (startY - astY);
                int dist = (xDist * xDist) + (yDist * yDist);
                if (dist < closestDistance) {
                    if (Functions.lineIntersectsCircle(startX, startY, endX2, endY2, astX, astY, (float) (a.intColRadius + this.intCollisionRadius + 10))) {
                        closestDistance = dist;
                        this.AI_AvoidanceCircleX = astX;
                        this.AI_AvoidanceCircleY = astY;
                        this.AI_AvoidanceCircleRadius = a.intColRadius;
                        avoid = true;
                    }
                }
            }
            for (int i2 = 0; i2 < units.unit.size(); i2++) {
                Unit u = units.unit.get(i2);
                if (u != this && !units.inIgnoreList(u, ignoreUnit) && u.inGameZone() && !(movementClass() == 2 && u.movementClass() == 1)) {
                    int xDist2 = (int) (startX - u.dblNoChangeWorldX);
                    int yDist2 = (int) (startY - u.dblNoChangeWorldY);
                    int dist2 = (xDist2 * xDist2) + (yDist2 * yDist2);
                    if (dist2 < closestDistance) {
                        if (Functions.lineIntersectsCircle(startX, startY, endX2, endY2, u.dblNoChangeWorldX, u.dblNoChangeWorldY, (float) (u.intCollisionRadius + this.intCollisionRadius + 10))) {
                            closestDistance = dist2;
                            this.AI_AvoidanceCircleX = u.dblNoChangeWorldX;
                            this.AI_AvoidanceCircleY = u.dblNoChangeWorldY;
                            this.AI_AvoidanceCircleRadius = u.intCollisionRadius;
                            avoid = true;
                        }
                    }
                }
            }
            return avoid;
        } catch (Exception e) {
            throw e;
        }
    }

    public int movementClass() {
        switch (this.type) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 0;
            case 3:
                return 1;
            case 4:
                return 1;
            case 5:
                return 2;
            case 6:
                return 1;
            case 7:
                return 1;
            case 8:
                return 0;
            case 9:
                return 0;
            case 10:
                return 1;
            case 11:
                return 1;
            case 12:
                return 1;
            case 13:
                return 1;
            case 14:
                return 1;
            case 15:
                return 1;
            case 16:
                return 0;
            case Units.UNIT_ENEMYCARGO /*17*/:
                return 2;
            case Units.UNIT_ENEMYCRUISER /*18*/:
                return 2;
            case Units.UNIT_ENEMYCRUISERTURRET /*19*/:
                return 1;
            case 20:
                return 0;
            case Units.UNIT_ENEMYHQ2 /*21*/:
                return 0;
            default:
                return 0;
        }
    }

    private boolean AI_MoveToSeekLocation(Asteroids asteroids, Units units, Map map) throws Exception {
        try {
            return AI_CheckMoveToCoordinates(this.AI_SearchZoneX, this.AI_SearchZoneY, 50, asteroids, units, getAttachedUnits(units, DEBUG_VISIBLITY, null), DEBUG_VISIBLITY, map);
        } catch (Exception e) {
            throw e;
        }
    }

    private Unit AI_TargetEnemy(Unit preferedTarget, Units units, Map map, Asteroids asteroids) throws Exception {
        Unit target = null;
        int targetValue = 10000000;
        float targetDistance = 0.0f;
        float targetAngDiff = 0.0f;
        boolean preferedFound = DEBUG_VISIBLITY;
        int i = 0;
        while (i < units.unit.size() && !preferedFound) {
            try {
                Unit un = units.unit.get(i);
                if (un.inGameZone() && this.team != un.team && Functions.inCircle(un.fltWorldX, un.fltWorldY, this.fltWorldX, this.fltWorldY, 1400)) {
                    float xdist = (float) (un.fltWorldX - this.fltWorldX);
                    float ydist = (float) (un.fltWorldY - this.fltWorldY);
                    float dist = LookUp.sqrt((xdist * xdist) + (ydist * ydist));
                    float angDiff = Functions.angDiff(Functions.aTanFull(xdist, ydist), this.fltAngle);
                    int value = (int) ((Math.abs(angDiff) * 3.0f) + dist);
                    if (value < targetValue && !preferedFound) {
                        targetValue = value;
                        target = un;
                        targetDistance = dist;
                        targetAngDiff = angDiff;
                        if (un == preferedTarget) {
                            preferedFound = true;
                        }
                    }
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
        if (target != null) {
            units.setAISearchZone(target.dblNoChangeWorldX, target.dblNoChangeWorldY, this.team, map);
            this.AI_X = target.dblNoChangeWorldX;
            this.AI_Y = target.dblNoChangeWorldY;
            if (targetDistance < 200.0f && Math.abs(targetAngDiff) > 90.0f) {
                AI_PlotAvoidTurnLock(asteroids);
            }
            AI_CheckMoveToCoordinates(this.AI_X, this.AI_Y, 0, asteroids, units, getAttachedUnits(units, DEBUG_VISIBLITY, target), DEBUG_VISIBLITY, map);
        }
        return target;
    }

    private void AI_PlotAvoidTurnLock(Asteroids asteroids) throws Exception {
        if (movementClass() != 0) {
            Asteroid target = null;
            int targetValue = 10000000;
            for (int i = 0; i < asteroids.asteroid.size(); i++) {
                Asteroid a = asteroids.asteroid.get(i);
                if (Functions.inCircle(this.fltWorldX, this.fltWorldY, a.dblWorldX, a.dblWorldY, Units.AI_ASTEROID_COVER_RANGE)) {
                    float xdist = (float) (a.dblWorldX - this.fltWorldX);
                    float ydist = (float) (a.dblWorldY - this.fltWorldY);
                    int value = (int) ((Math.abs(Functions.angDiff(Functions.aTanFull(xdist, ydist), this.fltAngle)) * 3.0f) + LookUp.sqrt((xdist * xdist) + (ydist * ydist)));
                    if (value < targetValue) {
                        targetValue = value;
                        target = a;
                    }
                }
            }
            if (target != null) {
                this.AI_X = target.dblWorldX + (target.dblWorldX - this.fltWorldX);
                this.AI_Y = target.dblWorldY + (target.dblWorldY - this.fltWorldY);
                return;
            }
            this.AI_X = this.fltWorldX + ((double) (LookUp.sin(this.fltAngle) * 600.0f));
            this.AI_Y = this.fltWorldY + ((double) (LookUp.cos(this.fltAngle) * 600.0f));
        }
    }

    private void AI_CheckFireWeapons(Units units, Asteroids asteroids) throws Exception {
        try {
            double[] shipX = new double[60];
            double[] shipY = new double[60];
            shipX[0] = this.fltWorldX;
            shipY[0] = this.fltWorldY;
            float shipAng = this.fltAngle;
            for (int turn = 1; turn < 60; turn++) {
                shipX[turn] = shipX[turn - 1] + ((double) (LookUp.sin(shipAng) * this.fltSpeed));
                shipY[turn] = shipY[turn - 1] + ((double) (LookUp.cos(shipAng) * this.fltSpeed));
                shipAng = Functions.wrapAngle(this.fltTurn + shipAng);
            }
            int rocketDelay = 0;
            for (int i = 0; i < this.inventory.size(); i++) {
                InventorySlot inv = this.inventory.get(i);
                if (inv.weapon && !inv.loading && inv.value > 0) {
                    switch (inv.type) {
                        case 7:
                            inv.firing = AI_CanFireWeapon(Projectiles.PROJ_BULLET_AI_FIRERANGE, units, asteroids, shipX, shipY, 0, 50, 12.0f, 1);
                            continue;
                        case 8:
                            inv.firing = AI_CanFireWeapon(1000, units, asteroids, shipX, shipY, rocketDelay, rocketDelay + 1, 14.0f, 1);
                            if (inv.firing) {
                                rocketDelay += 15;
                                break;
                            } else {
                                continue;
                            }
                        case 9:
                            inv.firing = AI_CanFireWeapon(1400, units, asteroids, shipX, shipY, rocketDelay, rocketDelay + 1, 13.0f, 26);
                            if (inv.firing) {
                                rocketDelay += 15;
                                break;
                            } else {
                                continue;
                            }
                        case 10:
                            inv.firing = AI_CanFireWeapon(1000, units, asteroids, shipX, shipY, rocketDelay, rocketDelay + 1, 12.0f, 1);
                            if (inv.firing) {
                                rocketDelay += 15;
                                break;
                            } else {
                                continue;
                            }
                        case 11:
                            inv.firing = AI_CanFireWeapon(1400, units, asteroids, shipX, shipY, rocketDelay, rocketDelay + 1, 14.0f, 30);
                            if (inv.firing) {
                                rocketDelay += 15;
                                break;
                            } else {
                                continue;
                            }
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void AI_SetTurnAndThrust() throws Exception {
        try {
            double xdist = this.AI_X - this.fltWorldX;
            double ydist = this.AI_Y - this.fltWorldY;
            float dist = LookUp.sqrt((float) ((xdist * xdist) + (ydist * ydist)));
            float angDiff = Functions.angDiff(Functions.aTanFull((float) xdist, (float) ydist), this.fltAngle);
            setTurn(angDiff / 60.0f);
            float maxSpeed = dist / (60.0f * (1.0f + ((float) Math.random())));
            float targetSpeed = ((180.0f - Math.abs(angDiff)) / 180.0f) * this.fltMaxSpeed;
            if (targetSpeed > maxSpeed) {
                targetSpeed = maxSpeed;
            }
            if (targetSpeed < this.AI_MinSpeed) {
                targetSpeed = this.AI_MinSpeed;
            }
            if (targetSpeed - this.fltOldSpeed > this.fltMaxAccel) {
                targetSpeed = this.fltOldSpeed + this.fltMaxAccel;
            }
            if (targetSpeed - this.fltOldSpeed < this.fltMaxAccel * -1.0f) {
                targetSpeed = this.fltOldSpeed - this.fltMaxAccel;
            }
            setSpeed(targetSpeed);
        } catch (Exception e) {
            throw e;
        }
    }

    public void AI_SetSearchZone(double x, double y, boolean defendMode) throws Exception {
        if (!defendMode) {
            try {
                if (this.AI_DefendMode) {
                    return;
                }
            } catch (Exception e) {
                throw e;
            }
        }
        if (defendMode) {
            this.AI_DefendMode = true;
        }
        this.AI_SearchZoneX = (x - 300.0d) + (Math.random() * 300.0d * 2.0d);
        this.AI_SearchZoneY = (y - 300.0d) + (Math.random() * 300.0d * 2.0d);
    }

    private boolean AI_CanFireWeapon(int range, Units units, Asteroids asteroids, double[] shipX, double[] shipY, int startTurn, int endTurn, float projSpeed, int searchArc) throws Exception {
        double shipTempX;
        double shipTempY;
        int fireWeighting = 0;
        float closestUnit = 1000000.0f;
        try {
            int closestUnitTeam = this.team;
            int fireRangeSqr = range * range;
            for (int u = 0; u < units.unit.size(); u++) {
                Unit unit = units.unit.get(u);
                if (unit != this && unit.inGameZone()) {
                    float xDist = (float) (unit.fltWorldX - this.fltWorldX);
                    float yDist = (float) (unit.fltWorldY - this.fltWorldY);
                    float distSqr = (xDist * xDist) + (yDist * yDist);
                    if (distSqr < ((float) fireRangeSqr)) {
                        if (Math.abs(Functions.angDiff(Functions.aTanFull(xDist, yDist), this.fltAngle)) < 90.0f) {
                            float shipAng = this.fltAngle;
                            double targetXSpd = (unit.dblNoChangeWorldX - unit.fltWorldX) / 60.0d;
                            double targetYSpd = (unit.dblNoChangeWorldY - unit.fltWorldY) / 60.0d;
                            double targetX = unit.fltWorldX + (((double) startTurn) * targetXSpd);
                            double targetY = unit.fltWorldY + (((double) startTurn) * targetYSpd);
                            boolean canHitUnit = DEBUG_VISIBLITY;
                            for (int turn = startTurn; turn < endTurn && !canHitUnit; turn++) {
                                if (turn < 0) {
                                    shipTempX = this.fltWorldX;
                                    shipTempY = this.fltWorldY;
                                } else if (turn > 59) {
                                    shipTempX = shipX[59];
                                    shipTempY = shipY[59];
                                } else {
                                    shipTempX = shipX[turn];
                                    shipTempY = shipY[turn];
                                }
                                for (int arc = 0; arc < searchArc * 2 && !canHitUnit; arc += 2) {
                                    float arcAngle = Functions.wrapAngle((shipAng - ((float) searchArc)) + ((float) arc));
                                    if (Functions.lineIntersectsCircle(shipTempX, shipTempY, shipTempX + ((double) (LookUp.sin(arcAngle) * projSpeed * 60.0f)), shipTempY + ((double) (LookUp.cos(arcAngle) * projSpeed * 60.0f)), targetX, targetY, (float) unit.intCollisionRadius)) {
                                        if (unit.team == this.team) {
                                            fireWeighting--;
                                        } else {
                                            fireWeighting++;
                                        }
                                        if (distSqr < closestUnit) {
                                            closestUnit = distSqr;
                                            closestUnitTeam = unit.team;
                                        }
                                        canHitUnit = true;
                                    }
                                }
                                targetX += targetXSpd;
                                targetY += targetYSpd;
                                shipAng = Functions.wrapAngle(this.fltAngle + (this.fltTurn * ((float) turn)));
                            }
                        }
                    }
                }
            }
            if (closestUnitTeam != this.team) {
                fireWeighting++;
            }
            if (fireWeighting > 0) {
                return true;
            }
            return DEBUG_VISIBLITY;
        } catch (Exception e) {
            throw e;
        }
    }

    public String getSaveString(Units units) throws Exception {
        try {
            String saveString = String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(this.type)) + "B" + this.pod.getSaveString()) + "B" + String.valueOf(this.team)) + "B" + String.valueOf((int) (this.fltFriction * 1000.0f))) + "B" + Functions.toStr(this.shipAlive)) + "B" + Functions.toStr(this.isPlayer)) + "B" + Functions.toStr(this.hasPilot)) + "B" + Functions.toStr(this.isPlayable)) + "B" + Functions.toStr(this.isBuilding)) + "B" + Functions.toStr(this.warning)) + "B" + Functions.toStr(this.ejectOrder)) + "B" + Functions.toStr(this.hasEjected)) + "B" + Functions.toStr(this.isSafe)) + "B" + Functions.toStr(this.isLeavingMap)) + "B" + Functions.toStr(this.pilotKilledReturned)) + "B" + Functions.toStr(this.visibleToPlayer)) + "B" + String.valueOf(this.shipAlpha)) + "B" + units.getUnitIndex(this.attachedTo)) + "B" + String.valueOf((int) (this.attachAngle * 1000.0f))) + "B" + String.valueOf((int) (this.attachDistance * 1000.0f))) + "B" + String.valueOf((int) (this.attachPivotCenter * 1000.0f))) + "B" + String.valueOf((int) (this.attachPivotLimit * 1000.0f))) + "B" + String.valueOf((int) (this.attachRelativeAng * 1000.0f))) + "B" + Functions.toStr(this.childrenAttached)) + "B" + Functions.toStr(this.inScreen)) + "B" + Functions.toStr(this.arrowInScreen)) + "B" + Functions.toStr(this.cornerCoordsCalced)) + "B" + String.valueOf((int) this.fltWorldX)) + "B" + String.valueOf((int) this.fltWorldY)) + "B" + String.valueOf((int) this.dblNoChangeWorldX)) + "B" + String.valueOf((int) this.dblNoChangeWorldY)) + "B" + String.valueOf(this.intScreenX)) + "B" + String.valueOf(this.intScreenY)) + "B" + String.valueOf((int) this.fltAngle)) + "B" + String.valueOf((int) (this.fltXSpd * 1000.0f))) + "B" + String.valueOf((int) (this.fltYSpd * 1000.0f))) + "B" + String.valueOf((int) (this.fltOldSpeed * 1000.0f))) + "B" + String.valueOf((int) (this.fltSpeed * 1000.0f))) + "B" + String.valueOf((int) (this.fltMaxSpeed * 1000.0f))) + "B" + String.valueOf((int) (this.fltTurn * 1000.0f))) + "B" + String.valueOf((int) (this.fltMaxTurn * 1000.0f))) + "B" + String.valueOf((int) this.fltScreenArrowHeadX)) + "B" + String.valueOf((int) this.fltScreenArrowHeadY)) + "B" + String.valueOf((int) this.fltScreenArrowHeadAng)) + "B" + String.valueOf((int) this.fltScreenFinishX)) + "B" + String.valueOf((int) this.fltScreenFinishY)) + "B" + String.valueOf((int) this.fltScreenFinishAng)) + "B" + String.valueOf((int) this.leavingAng);
            for (int i = 0; i < 8; i++) {
                saveString = String.valueOf(saveString) + "B" + String.valueOf((int) this.cornerCoords[i]);
            }
            String saveString2 = String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(saveString) + "B" + String.valueOf((int) (this.cornerAngDiff * 1000.0f))) + "B" + String.valueOf((int) (this.cornerAngDist * 1000.0f))) + "B" + String.valueOf(this.intWidth)) + "B" + String.valueOf(this.intHeight)) + "B" + String.valueOf(this.intHalfWidth)) + "B" + String.valueOf(this.intHalfHeight)) + "B" + String.valueOf(this.intQuarterWidth)) + "B" + String.valueOf(this.intQuarterHeight)) + "B" + String.valueOf(this.intVisualRadius)) + "B" + String.valueOf(this.intCollisionHalfWidth)) + "B" + String.valueOf(this.intCollisionHalfHeight)) + "B" + String.valueOf(this.intCollisionRadius)) + "B" + String.valueOf(this.rocketRefire)) + "B" + String.valueOf(this.rocketFireCount)) + "B" + String.valueOf(this.gunRefire)) + "B" + String.valueOf(this.numPowers)) + "B" + String.valueOf((int) (this.powerReduce * 1000.0f))) + "B" + String.valueOf(this.numTurns)) + "B" + String.valueOf((int) (this.turnReduce * 1000.0f))) + "B" + Functions.toStr(this.jetEmitterOn)) + "B" + String.valueOf((int) this.lastEmitX)) + "B" + String.valueOf((int) this.lastEmitY)) + "B" + String.valueOf((int) this.AI_X)) + "B" + String.valueOf((int) this.AI_Y)) + "B" + String.valueOf((int) this.AI_SearchZoneX)) + "B" + String.valueOf((int) this.AI_SearchZoneY)) + "B" + Functions.toStr(this.AI_DefendMode)) + "B" + Functions.toStr(this.AI_RunAway)) + "B" + String.valueOf((int) (this.AI_MinSpeed * 1000.0f))) + "B" + String.valueOf(this.AI_Mission)) + "B" + units.getUnitIndex(this.AI_MissionUnit)) + "B" + String.valueOf((int) this.AI_MissionX)) + "B" + String.valueOf((int) this.AI_MissionY)) + "B" + String.valueOf(this.AI_MissionRadius)) + "B" + String.valueOf(this.AI_MissionAng)) + "B" + String.valueOf(this.AI_MissionDist)) + "B" + String.valueOf(this.inventory.size());
            for (int i2 = 0; i2 < this.inventory.size(); i2++) {
                saveString2 = String.valueOf(saveString2) + "B" + this.inventory.get(i2).getSaveString();
            }
            for (int i3 = 0; i3 < 4; i3++) {
                saveString2 = String.valueOf(saveString2) + "B" + String.valueOf(this.sideInvCount[i3]);
            }
            String saveString3 = String.valueOf(saveString2) + "B" + String.valueOf(this.armour.size());
            for (int i4 = 0; i4 < this.armour.size(); i4++) {
                saveString3 = String.valueOf(saveString3) + "B" + this.armour.get(i4).getSaveString();
            }
            for (int i5 = 0; i5 < 4; i5++) {
                saveString3 = String.valueOf(saveString3) + "B" + String.valueOf(this.sideArmourCount[i5]);
            }
            return saveString3;
        } catch (Exception e) {
            throw e;
        }
    }

    public void restoreFromSaveString(String saveString) throws Exception {
        Exception e;
        try {
            String[] unitData = saveString.split("B");
            int dataPos = 0 + 1;
            try {
                this.type = Integer.parseInt(unitData[0]);
                int dataPos2 = dataPos + 1;
                this.pod.restoreFromSaveString(unitData[dataPos]);
                dataPos = dataPos2 + 1;
                this.team = Integer.parseInt(unitData[dataPos2]);
                int dataPos3 = dataPos + 1;
                this.fltFriction = ((float) Integer.parseInt(unitData[dataPos])) / 1000.0f;
                dataPos = dataPos3 + 1;
                this.shipAlive = Functions.toBoolean(unitData[dataPos3]);
                int dataPos4 = dataPos + 1;
                this.isPlayer = Functions.toBoolean(unitData[dataPos]);
                dataPos = dataPos4 + 1;
                this.hasPilot = Functions.toBoolean(unitData[dataPos4]);
                int dataPos5 = dataPos + 1;
                this.isPlayable = Functions.toBoolean(unitData[dataPos]);
                dataPos = dataPos5 + 1;
                this.isBuilding = Functions.toBoolean(unitData[dataPos5]);
                int dataPos6 = dataPos + 1;
                this.warning = Functions.toBoolean(unitData[dataPos]);
                dataPos = dataPos6 + 1;
                this.ejectOrder = Functions.toBoolean(unitData[dataPos6]);
                int dataPos7 = dataPos + 1;
                this.hasEjected = Functions.toBoolean(unitData[dataPos]);
                dataPos = dataPos7 + 1;
                this.isSafe = Functions.toBoolean(unitData[dataPos7]);
                int dataPos8 = dataPos + 1;
                this.isLeavingMap = Functions.toBoolean(unitData[dataPos]);
                dataPos = dataPos8 + 1;
                this.pilotKilledReturned = Functions.toBoolean(unitData[dataPos8]);
                int dataPos9 = dataPos + 1;
                this.visibleToPlayer = Functions.toBoolean(unitData[dataPos]);
                dataPos = dataPos9 + 1;
                this.shipAlpha = Integer.parseInt(unitData[dataPos9]);
                int dataPos10 = dataPos + 1;
                this.loadAttachedToID = Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos10 + 1;
                this.attachAngle = ((float) Integer.parseInt(unitData[dataPos10])) / 1000.0f;
                int dataPos11 = dataPos + 1;
                this.attachDistance = ((float) Integer.parseInt(unitData[dataPos])) / 1000.0f;
                dataPos = dataPos11 + 1;
                this.attachPivotCenter = ((float) Integer.parseInt(unitData[dataPos11])) / 1000.0f;
                int dataPos12 = dataPos + 1;
                this.attachPivotLimit = ((float) Integer.parseInt(unitData[dataPos])) / 1000.0f;
                dataPos = dataPos12 + 1;
                this.attachRelativeAng = ((float) Integer.parseInt(unitData[dataPos12])) / 1000.0f;
                int dataPos13 = dataPos + 1;
                this.childrenAttached = Functions.toBoolean(unitData[dataPos]);
                dataPos = dataPos13 + 1;
                this.inScreen = Functions.toBoolean(unitData[dataPos13]);
                int dataPos14 = dataPos + 1;
                this.arrowInScreen = Functions.toBoolean(unitData[dataPos]);
                dataPos = dataPos14 + 1;
                this.cornerCoordsCalced = Functions.toBoolean(unitData[dataPos14]);
                int dataPos15 = dataPos + 1;
                this.fltWorldX = (double) Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos15 + 1;
                this.fltWorldY = (double) Integer.parseInt(unitData[dataPos15]);
                int dataPos16 = dataPos + 1;
                this.dblNoChangeWorldX = (double) Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos16 + 1;
                this.dblNoChangeWorldY = (double) Integer.parseInt(unitData[dataPos16]);
                int dataPos17 = dataPos + 1;
                this.intScreenX = Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos17 + 1;
                this.intScreenY = Integer.parseInt(unitData[dataPos17]);
                int dataPos18 = dataPos + 1;
                this.fltAngle = (float) Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos18 + 1;
                this.fltXSpd = ((float) Integer.parseInt(unitData[dataPos18])) / 1000.0f;
                int dataPos19 = dataPos + 1;
                this.fltYSpd = ((float) Integer.parseInt(unitData[dataPos])) / 1000.0f;
                dataPos = dataPos19 + 1;
                this.fltOldSpeed = ((float) Integer.parseInt(unitData[dataPos19])) / 1000.0f;
                int dataPos20 = dataPos + 1;
                this.fltSpeed = ((float) Integer.parseInt(unitData[dataPos])) / 1000.0f;
                dataPos = dataPos20 + 1;
                this.fltMaxSpeed = ((float) Integer.parseInt(unitData[dataPos20])) / 1000.0f;
                int dataPos21 = dataPos + 1;
                this.fltTurn = ((float) Integer.parseInt(unitData[dataPos])) / 1000.0f;
                dataPos = dataPos21 + 1;
                this.fltMaxTurn = ((float) Integer.parseInt(unitData[dataPos21])) / 1000.0f;
                int dataPos22 = dataPos + 1;
                this.fltScreenArrowHeadX = (float) Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos22 + 1;
                this.fltScreenArrowHeadY = (float) Integer.parseInt(unitData[dataPos22]);
                int dataPos23 = dataPos + 1;
                this.fltScreenArrowHeadAng = (float) Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos23 + 1;
                this.fltScreenFinishX = (float) Integer.parseInt(unitData[dataPos23]);
                int dataPos24 = dataPos + 1;
                this.fltScreenFinishY = (float) Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos24 + 1;
                this.fltScreenFinishAng = (float) Integer.parseInt(unitData[dataPos24]);
                int dataPos25 = dataPos + 1;
                this.leavingAng = (float) Integer.parseInt(unitData[dataPos]);
                int i = 0;
                while (true) {
                    dataPos = dataPos25;
                    if (i >= 8) {
                        break;
                    }
                    dataPos25 = dataPos + 1;
                    this.cornerCoords[i] = (double) Integer.parseInt(unitData[dataPos]);
                    i++;
                }
                int dataPos26 = dataPos + 1;
                this.cornerAngDiff = ((float) Integer.parseInt(unitData[dataPos])) / 1000.0f;
                dataPos = dataPos26 + 1;
                this.cornerAngDist = ((float) Integer.parseInt(unitData[dataPos26])) / 1000.0f;
                int dataPos27 = dataPos + 1;
                this.intWidth = Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos27 + 1;
                this.intHeight = Integer.parseInt(unitData[dataPos27]);
                int dataPos28 = dataPos + 1;
                this.intHalfWidth = Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos28 + 1;
                this.intHalfHeight = Integer.parseInt(unitData[dataPos28]);
                int dataPos29 = dataPos + 1;
                this.intQuarterWidth = Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos29 + 1;
                this.intQuarterHeight = Integer.parseInt(unitData[dataPos29]);
                int dataPos30 = dataPos + 1;
                this.intVisualRadius = Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos30 + 1;
                this.intCollisionHalfWidth = Integer.parseInt(unitData[dataPos30]);
                int dataPos31 = dataPos + 1;
                this.intCollisionHalfHeight = Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos31 + 1;
                this.intCollisionRadius = Integer.parseInt(unitData[dataPos31]);
                int dataPos32 = dataPos + 1;
                this.rocketRefire = Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos32 + 1;
                this.rocketFireCount = Integer.parseInt(unitData[dataPos32]);
                int dataPos33 = dataPos + 1;
                this.gunRefire = Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos33 + 1;
                this.numPowers = Integer.parseInt(unitData[dataPos33]);
                int dataPos34 = dataPos + 1;
                this.powerReduce = ((float) Integer.parseInt(unitData[dataPos])) / 1000.0f;
                dataPos = dataPos34 + 1;
                this.numTurns = Integer.parseInt(unitData[dataPos34]);
                int dataPos35 = dataPos + 1;
                this.turnReduce = ((float) Integer.parseInt(unitData[dataPos])) / 1000.0f;
                dataPos = dataPos35 + 1;
                this.jetEmitterOn = Functions.toBoolean(unitData[dataPos35]);
                int dataPos36 = dataPos + 1;
                this.lastEmitX = (double) Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos36 + 1;
                this.lastEmitY = (double) Integer.parseInt(unitData[dataPos36]);
                int dataPos37 = dataPos + 1;
                this.AI_X = (double) Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos37 + 1;
                this.AI_Y = (double) Integer.parseInt(unitData[dataPos37]);
                int dataPos38 = dataPos + 1;
                this.AI_SearchZoneX = (double) Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos38 + 1;
                this.AI_SearchZoneY = (double) Integer.parseInt(unitData[dataPos38]);
                int dataPos39 = dataPos + 1;
                this.AI_DefendMode = Functions.toBoolean(unitData[dataPos]);
                dataPos = dataPos39 + 1;
                this.AI_RunAway = Functions.toBoolean(unitData[dataPos39]);
                int dataPos40 = dataPos + 1;
                this.AI_MinSpeed = ((float) Integer.parseInt(unitData[dataPos])) / 1000.0f;
                dataPos = dataPos40 + 1;
                this.AI_Mission = Integer.parseInt(unitData[dataPos40]);
                int dataPos41 = dataPos + 1;
                this.loadAI_MissionUnitID = Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos41 + 1;
                this.AI_MissionX = (double) Integer.parseInt(unitData[dataPos41]);
                int dataPos42 = dataPos + 1;
                this.AI_MissionY = (double) Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos42 + 1;
                this.AI_MissionRadius = Integer.parseInt(unitData[dataPos42]);
                int dataPos43 = dataPos + 1;
                this.AI_MissionAng = Integer.parseInt(unitData[dataPos]);
                dataPos = dataPos43 + 1;
                this.AI_MissionDist = Integer.parseInt(unitData[dataPos43]);
                this.inventory = new ArrayList<>();
                int dataPos44 = dataPos + 1;
                int inventorySize = Integer.parseInt(unitData[dataPos]);
                int i2 = 0;
                int dataPos45 = dataPos44;
                while (i2 < inventorySize) {
                    this.inventory.add(new InventorySlot(0, 0, 0));
                    int dataPos46 = dataPos + 1;
                    this.inventory.get(i2).restoreFromSaveString(unitData[dataPos]);
                    i2++;
                    dataPos45 = dataPos46;
                }
                int i3 = 0;
                while (i3 < 4) {
                    int dataPos47 = dataPos + 1;
                    this.sideInvCount[i3] = Integer.parseInt(unitData[dataPos]);
                    i3++;
                    dataPos = dataPos47;
                }
                this.armour = new ArrayList<>();
                int dataPos48 = dataPos + 1;
                int armourSize = Integer.parseInt(unitData[dataPos]);
                int i4 = 0;
                dataPos = dataPos48;
                while (i4 < armourSize) {
                    this.armour.add(new ArmourSlot(0));
                    int dataPos49 = dataPos + 1;
                    this.armour.get(i4).restoreFromSaveString(unitData[dataPos]);
                    i4++;
                    dataPos = dataPos49;
                }
                int i5 = 0;
                while (i5 < 4) {
                    int dataPos50 = dataPos + 1;
                    this.sideArmourCount[i5] = Integer.parseInt(unitData[dataPos]);
                    i5++;
                    dataPos = dataPos50;
                }
            } catch (Exception e2) {
                e = e2;
                throw e;
            }
        } catch (Exception e3) {
            e = e3;
            throw e;
        }
    }
}
