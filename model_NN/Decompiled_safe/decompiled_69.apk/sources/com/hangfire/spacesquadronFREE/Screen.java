package com.hangfire.spacesquadronFREE;

public final class Screen {
    public static final int DRAG_DESELECT_THRESHHOLD = 20;
    public boolean dragging = false;
    private float firstDragX = 0.0f;
    private float firstDragY = 0.0f;
    public double fltScreenX = 0.0d;
    public double fltScreenY = 0.0d;
    private int intMaxScreenX = 0;
    private int intMaxScreenY = 0;
    private int intMaxWorldX = 0;
    private int intMaxWorldY = 0;
    public int intMiddleX = 0;
    public int intMiddleY = 0;
    public int intScreenHeight = 0;
    public int intScreenRight = 0;
    public int intScreenTop = 0;
    public int intScreenWidth = 0;
    public int intScreenX = 0;
    public int intScreenY = 0;
    public int lastCameraDrag = 0;
    private float preDragX = 0.0f;
    private float preDragY = 0.0f;
    public boolean zoomedIn = false;

    public Screen(int maxX, int maxY) throws Exception {
        try {
            this.intMaxWorldX = maxX;
            this.intMaxWorldY = maxY;
            calcMaxScreenSize();
        } catch (Exception e) {
            throw e;
        }
    }

    public void toggleZoom() throws Exception {
        try {
            if (this.zoomedIn) {
                this.zoomedIn = false;
                calcMaxScreenSize();
                positionCamera(this.fltScreenX - ((double) (this.intScreenWidth / 2)), this.fltScreenY - ((double) (this.intScreenHeight / 2)));
                return;
            }
            this.zoomedIn = true;
            calcMaxScreenSize();
            positionCamera(this.fltScreenX + ((double) (this.intScreenWidth / 2)), this.fltScreenY + ((double) (this.intScreenHeight / 2)));
        } catch (Exception e) {
            throw e;
        }
    }

    public void centerOn(double x, double y) throws Exception {
        try {
            if (this.zoomedIn) {
                positionCamera(x - ((double) (((float) this.intScreenWidth) / 2.0f)), y - ((double) (((float) this.intScreenHeight) / 2.0f)));
            } else {
                positionCamera(x - ((double) this.intScreenWidth), y - ((double) this.intScreenHeight));
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void setScreenSize(int width, int height) throws Exception {
        try {
            this.intScreenWidth = width;
            this.intScreenHeight = height;
            calcMaxScreenSize();
            checkScreenPosition();
            calculateBorders();
        } catch (Exception e) {
            throw e;
        }
    }

    private void calcMaxScreenSize() throws Exception {
        try {
            if (this.zoomedIn) {
                this.intMaxScreenX = this.intMaxWorldX - this.intScreenWidth;
                this.intMaxScreenY = this.intMaxWorldY - this.intScreenHeight;
                return;
            }
            this.intMaxScreenX = this.intMaxWorldX - (this.intScreenWidth * 2);
            this.intMaxScreenY = this.intMaxWorldY - (this.intScreenHeight * 2);
        } catch (Exception e) {
            throw e;
        }
    }

    public void positionCamera(int x, int y) throws Exception {
        try {
            this.intScreenX = x;
            this.intScreenY = y;
            this.fltScreenX = (double) x;
            this.fltScreenY = (double) y;
            checkScreenPosition();
            calculateBorders();
        } catch (Exception e) {
            throw e;
        }
    }

    public void positionCamera(double x, double y) throws Exception {
        try {
            this.intScreenX = (int) Math.round(x);
            this.intScreenY = (int) Math.round(y);
            this.fltScreenX = x;
            this.fltScreenY = y;
            checkScreenPosition();
            calculateBorders();
        } catch (Exception e) {
            throw e;
        }
    }

    public void moveCamera(float xDist, float yDist) throws Exception {
        try {
            if (this.zoomedIn) {
                this.fltScreenX += (double) xDist;
                this.fltScreenY += (double) yDist;
            } else {
                this.fltScreenX += (double) (xDist * 2.0f);
                this.fltScreenY += (double) (yDist * 2.0f);
            }
            this.intScreenX = (int) Math.round(this.fltScreenX);
            this.intScreenY = (int) Math.round(this.fltScreenY);
            checkScreenPosition();
            calculateBorders();
        } catch (Exception e) {
            throw e;
        }
    }

    private void calculateBorders() throws Exception {
        try {
            if (this.zoomedIn) {
                this.intScreenRight = this.intScreenX + this.intScreenWidth;
                this.intScreenTop = this.intScreenY + this.intScreenHeight;
                this.intMiddleX = this.intScreenX + (this.intScreenWidth / 2);
                this.intMiddleY = this.intScreenY + (this.intScreenHeight / 2);
                return;
            }
            this.intScreenRight = this.intScreenX + (this.intScreenWidth * 2);
            this.intScreenTop = this.intScreenY + (this.intScreenHeight * 2);
            this.intMiddleX = this.intScreenX + this.intScreenWidth;
            this.intMiddleY = this.intScreenY + this.intScreenHeight;
        } catch (Exception e) {
            throw e;
        }
    }

    private void checkScreenPosition() throws Exception {
        try {
            if (this.intScreenX < 0) {
                this.intScreenX = 0;
                this.fltScreenX = 0.0d;
            }
            if (this.intScreenY < 0) {
                this.intScreenY = 0;
                this.fltScreenY = 0.0d;
            }
            if (this.intScreenX > this.intMaxScreenX) {
                this.intScreenX = this.intMaxScreenX;
                this.fltScreenX = (double) this.intMaxScreenX;
            }
            if (this.intScreenY > this.intMaxScreenY) {
                this.intScreenY = this.intMaxScreenY;
                this.fltScreenY = (double) this.intMaxScreenY;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean inScreenScreenValues(int screenx, int screeny, int halfWidth, int halfHeight) throws Exception {
        if (screenx + halfWidth < 0) {
            return false;
        }
        if (screeny + halfHeight < 0) {
            return false;
        }
        try {
            if (this.zoomedIn) {
                if (screenx - halfWidth > this.intScreenWidth) {
                    return false;
                }
                if (screeny - halfHeight > this.intScreenHeight) {
                    return false;
                }
            } else if (screenx - halfWidth > this.intScreenWidth * 2) {
                return false;
            } else {
                if (screeny - halfHeight > this.intScreenHeight * 2) {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean inScreenWorldValues(double x, double y, int halfWidth, int halfHeight) throws Exception {
        try {
            if (x - ((double) halfWidth) > ((double) this.intScreenRight)) {
                return false;
            }
            if (((double) halfWidth) + x < ((double) this.intScreenX)) {
                return false;
            }
            if (y - ((double) halfHeight) > ((double) this.intScreenTop)) {
                return false;
            }
            if (((double) halfHeight) + y < ((double) this.intScreenY)) {
                return false;
            }
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public int canvasX(int x) throws Exception {
        try {
            if (this.zoomedIn) {
                return x - this.intScreenX;
            }
            return (x - this.intScreenX) / 2;
        } catch (Exception e) {
            throw e;
        }
    }

    public int canvasY(int y) throws Exception {
        try {
            if (this.zoomedIn) {
                return this.intScreenHeight - (y - this.intScreenY);
            }
            return this.intScreenHeight - ((y - this.intScreenY) / 2);
        } catch (Exception e) {
            throw e;
        }
    }

    public float canvasX(double x) throws Exception {
        try {
            if (this.zoomedIn) {
                return ((float) x) - ((float) this.intScreenX);
            }
            return (((float) x) - ((float) this.intScreenX)) / 2.0f;
        } catch (Exception e) {
            throw e;
        }
    }

    public float canvasY(double y) throws Exception {
        try {
            if (this.zoomedIn) {
                return ((float) this.intScreenHeight) - (((float) y) - ((float) this.intScreenY));
            }
            return ((float) this.intScreenHeight) - ((((float) y) - ((float) this.intScreenY)) / 2.0f);
        } catch (Exception e) {
            throw e;
        }
    }

    public void startCameraDrag(float x, float y) throws Exception {
        try {
            this.firstDragX = x;
            this.firstDragY = y;
            this.preDragX = (float) this.fltScreenX;
            this.preDragY = (float) this.fltScreenY;
            this.lastCameraDrag = 0;
            this.dragging = true;
        } catch (Exception e) {
            throw e;
        }
    }

    public void dragCamera(float x, float y) throws Exception {
        try {
            positionCamera((double) this.preDragX, (double) this.preDragY);
            moveCamera(this.firstDragX - x, y - this.firstDragY);
            int dragDist = (int) (Math.abs(this.firstDragX - x) + Math.abs(this.firstDragY - y));
            if (dragDist > this.lastCameraDrag) {
                this.lastCameraDrag = dragDist;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean wasTapped() throws Exception {
        try {
            if (!this.dragging || this.lastCameraDrag >= 20) {
                return false;
            }
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public void stopCameraDrag() {
        this.dragging = false;
    }

    public String getSaveString() throws Exception {
        try {
            return String.valueOf(String.valueOf(String.valueOf(String.valueOf(Functions.toStr(this.zoomedIn)) + "A" + String.valueOf(this.intScreenX)) + "A" + String.valueOf(this.intScreenY)) + "A" + String.valueOf(this.intScreenWidth)) + "A" + String.valueOf(this.intScreenHeight);
        } catch (Exception e) {
            throw e;
        }
    }

    public void restoreFromSaveString(String saveString) throws Exception {
        try {
            String[] data = saveString.split("A");
            int pos = 0 + 1;
            this.zoomedIn = Functions.toBoolean(data[0]);
            int pos2 = pos + 1;
            this.intScreenX = Integer.parseInt(data[pos]);
            int pos3 = pos2 + 1;
            this.intScreenY = Integer.parseInt(data[pos2]);
            int pos4 = pos3 + 1;
            this.intScreenWidth = Integer.parseInt(data[pos3]);
            int i = pos4 + 1;
            this.intScreenHeight = Integer.parseInt(data[pos4]);
            this.fltScreenX = (double) this.intScreenX;
            this.fltScreenY = (double) this.intScreenY;
            calculateBorders();
        } catch (Exception e) {
            throw e;
        }
    }
}
