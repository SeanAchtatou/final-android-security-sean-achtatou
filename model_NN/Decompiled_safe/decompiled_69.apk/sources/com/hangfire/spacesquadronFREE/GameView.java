package com.hangfire.spacesquadronFREE;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public final class GameView extends SurfaceView implements SurfaceHolder.Callback {
    private GameThread thread;

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        setFocusable(true);
        requestFocus();
        this.thread = new GameThread(holder, context);
    }

    public boolean onTouchEvent(MotionEvent event) {
        return this.thread.onTouch(event);
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        this.thread.setSurfaceSize(width, height);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (!this.thread.getRunning()) {
            this.thread.setRunning();
            this.thread.start();
        }
        this.thread.surfaceReady = true;
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.thread.surfaceReady = false;
    }

    public GameThread getThread() {
        return this.thread;
    }
}
