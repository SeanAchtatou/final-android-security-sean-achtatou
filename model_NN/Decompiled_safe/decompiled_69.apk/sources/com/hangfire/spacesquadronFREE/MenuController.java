package com.hangfire.spacesquadronFREE;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import java.util.ArrayList;

public final class MenuController {
    private static final int BUTTON_CAREER_DELETE = 51;
    private static final int BUTTON_CAREER_OK = 50;
    private static final int BUTTON_CHOOSEMISSION_DONE = 59;
    private static final int BUTTON_CHOOSEMISSION_DOWN = 57;
    private static final int BUTTON_CHOOSEMISSION_DUMMY = 58;
    private static final int BUTTON_CHOOSEMISSION_UP = 56;
    private static final int BUTTON_EXIT = 5;
    private static final int BUTTON_GAMECOMPLETE_OK = 55;
    private static final int BUTTON_HALL = 2;
    private static final int BUTTON_HALL_CLEAR = 49;
    private static final int BUTTON_HALL_OK = 48;
    private static final int BUTTON_HELP = 3;
    private static final int BUTTON_HELP_DONE = 6;
    private static final int BUTTON_LOADING_DUMMY = 45;
    private static final int BUTTON_MESSAGE_DUMMY = 44;
    private static final int BUTTON_MESSAGE_OK = 43;
    private static final int BUTTON_MISSIONBRIEF_OK = 46;
    private static final int BUTTON_MISSIONRESULT_OK = 47;
    private static final int BUTTON_NEW = 0;
    private static final int BUTTON_NEWPILOT_A = 12;
    private static final int BUTTON_NEWPILOT_BACKSPACE = 40;
    private static final int BUTTON_NEWPILOT_DUMMY = 42;
    private static final int BUTTON_NEWPILOT_OK = 41;
    private static final int BUTTON_NEWPILOT_SHIFT = 38;
    private static final int BUTTON_NEWPILOT_SPACE = 39;
    private static final int BUTTON_NEWPILOT_Z = 37;
    private static final int BUTTON_PLAYMUSIC = 8;
    private static final int BUTTON_PLAYSOUND = 7;
    private static final int BUTTON_RESUME = 1;
    private static final int BUTTON_SETTINGS = 4;
    private static final int BUTTON_SETTINGS_DONE = 11;
    private static final int BUTTON_TUTORIAL = 10;
    private static final int BUTTON_VIBRATION = 9;
    private static final int BUTTON_YNMESSAGE_DUMMY = 54;
    private static final int BUTTON_YNMESSAGE_NO = 53;
    private static final int BUTTON_YNMESSAGE_YES = 52;
    private static final int CHARACTER_LOWERCASE_START = 97;
    private static final int CHARACTER_UPPERCASE_START = 65;
    private static final int DELETE_RECORD_ALL = -1;
    private static final int DELETE_RECORD_NONE = -2;
    private static final String GAME_COMPLETE_MESSAGE = "You have completed the free version of Space Squadron! Easy, huh? To experience serious combat in the full campaign, including new weapons, elite fighters, cargo ships, base assaults, battle cruisers, and more, treat yourself to the full game from the app store! :-)";
    private static final int INTRO_SCREEN_TIME = 2000;
    private static final float INTRO_TRANSITION_PROPORTION = 4.0f;
    private static final int SCREEN_CAREER = 14;
    private static final int SCREEN_CHOOSEMISSION = 17;
    private static final int SCREEN_GAMECOMPLETE = 16;
    private static final int SCREEN_HALL = 5;
    public static final int SCREEN_HELP = 4;
    private static final int SCREEN_LOADING = 8;
    public static final int SCREEN_MENU = 2;
    private static final int SCREEN_MESSAGE = 7;
    public static final int SCREEN_MISSIONBRIEF = 9;
    public static final int SCREEN_MISSIONDEAD_MESSAGE = 13;
    public static final int SCREEN_MISSIONFAILURE_MESSAGE = 12;
    public static final int SCREEN_MISSIONRESULT = 10;
    public static final int SCREEN_MISSIONSUCCESS_MESSAGE = 11;
    private static final int SCREEN_NEWPILOT = 6;
    public static final int SCREEN_SETTINGS = 3;
    private static final int SCREEN_SPLASH = 0;
    public static final int SCREEN_SUBMODE_NONE = 0;
    public static final int SCREEN_SUBMODE_RESULT_DEAD = 3;
    public static final int SCREEN_SUBMODE_RESULT_FAILED = 2;
    public static final int SCREEN_SUBMODE_RESULT_SUCCESS = 1;
    private static final int SCREEN_TITLE = 1;
    private static final int SCREEN_YNMESSAGE = 15;
    private Bitmap mBMPBackDrop;
    private Bitmap mBMPHangfireLogo;
    private Bitmap mBMPIconDead;
    private Bitmap mBMPIconQuit;
    private Bitmap mBMPIconWon;
    private Bitmap mBMPMedalAFC;
    private Bitmap mBMPMedalBIA;
    private Bitmap mBMPMedalCGC;
    private Bitmap mBMPMedalDFC;
    private Bitmap mBMPMedalMSM;
    private Bitmap[] mBMPRank;
    private Bitmap mBMPTitle;
    private int mBackDropLeft = 0;
    private int mBackDropTop = 0;
    private int mCareerDragY = 0;
    private int mCareerRecordID = 0;
    private int mCareerScreenHeight = 0;
    private int mCareerY = 0;
    public int mCenterX = 1;
    private int mDragY = 0;
    private boolean mDragged = false;
    private boolean mFirstTimeIn = true;
    private int mHallDragY = 0;
    private int mHallIconWidth = 0;
    private int mHallY = 0;
    public int mHeight = 1;
    private HelpTextItem[] mHelpItem;
    private boolean mHelpReady = false;
    private int mHelpScreenHeight = 0;
    private int mHelpTop = 0;
    private int mHelpTopDrag = 0;
    private MediaPlayer mInputSound;
    private Paint mMenuPaint;
    private String mMessage = "";
    private MediaPlayer mMissionSound;
    private String mNewPlayerName = "";
    private int mNoScreen = 2;
    private boolean mPopUpDisplayed = false;
    private int mRecordToDelete = DELETE_RECORD_NONE;
    private boolean mResized = false;
    private int mResultDragY = 0;
    private int mResultScreenHeight = 0;
    private int mResultY = 0;
    private int mReturnMode;
    private int mReturnScreen = 2;
    private int mScreen = 0;
    private int mStartMission = 1;
    private String mStartMissionString = ("1/" + Integer.toString(2));
    private int mSubMode = 0;
    private long mTimeStamp = 0;
    private int mTitleLeft = 0;
    private float mTitleRatio = 1.0f;
    private MotionEvent mTouchEventDown;
    private MotionEvent mTouchEventMove;
    private MotionEvent mTouchEventUp;
    private boolean mTransitioning = false;
    public int mWidth = 1;
    private String mYNMessage = "";
    private int mYesScreen = 2;
    private ArrayList<MenuButton> menuButton = new ArrayList<>();

    private final class HelpTextItem {
        public int height;
        public Bitmap img;
        public String[] lines;

        private HelpTextItem() {
        }

        /* synthetic */ HelpTextItem(MenuController menuController, HelpTextItem helpTextItem) {
            this();
        }
    }

    public MenuController(Context context) throws Exception {
        try {
            this.mInputSound = MediaPlayer.create(context, (int) R.raw.fxinput);
            this.mInputSound.setVolume(3.0f, 3.0f);
            this.mMenuPaint = new Paint();
            this.menuButton.add(new MenuButton(0, 2, false, true));
            this.menuButton.add(new MenuButton(1, 2, false, true));
            this.menuButton.add(new MenuButton(2, 2, false, true));
            this.menuButton.add(new MenuButton(3, 2, false, true));
            this.menuButton.add(new MenuButton(4, 2, false, true));
            this.menuButton.add(new MenuButton(5, 2, false, true));
            this.menuButton.add(new MenuButton(6, 4, false, false));
            this.menuButton.add(new MenuButton(7, 3, true, false));
            this.menuButton.add(new MenuButton(8, 3, true, false));
            this.menuButton.add(new MenuButton(9, 3, true, false));
            this.menuButton.add(new MenuButton(10, 3, true, false));
            this.menuButton.add(new MenuButton(11, 3, false, false));
            for (int i = 12; i < BUTTON_NEWPILOT_SHIFT; i++) {
                this.menuButton.add(new MenuButton(i, 6, false, false));
            }
            this.menuButton.add(new MenuButton(BUTTON_NEWPILOT_SHIFT, 6, true, false));
            this.menuButton.add(new MenuButton(BUTTON_NEWPILOT_SPACE, 6, false, false));
            this.menuButton.add(new MenuButton(40, 6, false, false));
            this.menuButton.add(new MenuButton(BUTTON_NEWPILOT_OK, 6, false, false));
            this.menuButton.add(new MenuButton(BUTTON_NEWPILOT_DUMMY, 6, false, false));
            this.menuButton.get(BUTTON_NEWPILOT_DUMMY).setEnabled(false);
            this.menuButton.add(new MenuButton(BUTTON_MESSAGE_OK, 7, false, false));
            this.menuButton.add(new MenuButton(BUTTON_MESSAGE_DUMMY, 7, false, false));
            this.menuButton.get(BUTTON_MESSAGE_DUMMY).setEnabled(false);
            this.menuButton.add(new MenuButton(BUTTON_LOADING_DUMMY, 8, false, false));
            this.menuButton.add(new MenuButton(BUTTON_MISSIONBRIEF_OK, 9, false, false));
            this.menuButton.add(new MenuButton(BUTTON_MISSIONRESULT_OK, 10, false, false));
            this.menuButton.add(new MenuButton(BUTTON_HALL_OK, 5, false, false));
            this.menuButton.add(new MenuButton(BUTTON_HALL_CLEAR, 5, false, false));
            this.menuButton.add(new MenuButton(50, 14, false, false));
            this.menuButton.add(new MenuButton(BUTTON_CAREER_DELETE, 14, false, false));
            this.mBMPRank = new Bitmap[10];
            this.menuButton.add(new MenuButton(BUTTON_YNMESSAGE_YES, 15, false, false));
            this.menuButton.add(new MenuButton(BUTTON_YNMESSAGE_NO, 15, false, false));
            this.menuButton.add(new MenuButton(BUTTON_YNMESSAGE_DUMMY, 15, false, false));
            this.menuButton.get(BUTTON_YNMESSAGE_DUMMY).setEnabled(false);
            this.menuButton.add(new MenuButton(BUTTON_GAMECOMPLETE_OK, 16, false, false));
            this.menuButton.add(new MenuButton(BUTTON_CHOOSEMISSION_UP, 17, false, false));
            this.menuButton.add(new MenuButton(BUTTON_CHOOSEMISSION_DOWN, 17, false, false));
            this.menuButton.add(new MenuButton(BUTTON_CHOOSEMISSION_DUMMY, 17, false, false));
            this.menuButton.add(new MenuButton(BUTTON_CHOOSEMISSION_DONE, 17, false, false));
            changeScreen(0, 0);
        } catch (Exception e) {
            throw e;
        }
    }

    public void destroy() throws Exception {
        try {
            stopMissionSound();
            this.mInputSound = null;
        } catch (Exception e) {
            throw e;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int run(android.view.SurfaceHolder r4, android.content.Context r5, com.hangfire.spacesquadronFREE.GameThread r6, com.hangfire.spacesquadronFREE.BitmapFontDrawer r7, com.hangfire.spacesquadronFREE.Timer r8) throws java.lang.Exception {
        /*
            r3 = this;
            boolean r2 = r3.mResized     // Catch:{ Exception -> 0x0034 }
            if (r2 != 0) goto L_0x0007
            r2 = 13
        L_0x0006:
            return r2
        L_0x0007:
            r2 = 0
            r3.mReturnMode = r2     // Catch:{ Exception -> 0x0034 }
            r0 = 0
            r2 = 0
            android.graphics.Canvas r0 = r4.lockCanvas(r2)     // Catch:{ all -> 0x002a }
            r3.handleInput(r6, r5)     // Catch:{ all -> 0x002a }
            r3.handleMusic(r6)     // Catch:{ all -> 0x002a }
            r3.processSplashScreens()     // Catch:{ all -> 0x002a }
            r3.processMissionMessageScreens(r5, r6)     // Catch:{ all -> 0x002a }
            r3.doDraw(r5, r0, r6, r7)     // Catch:{ all -> 0x002a }
            if (r0 == 0) goto L_0x0027
            r4.unlockCanvasAndPost(r0)     // Catch:{ Exception -> 0x0034 }
            r8.processTimer()     // Catch:{ Exception -> 0x0034 }
        L_0x0027:
            int r2 = r3.mReturnMode     // Catch:{ Exception -> 0x0034 }
            goto L_0x0006
        L_0x002a:
            r2 = move-exception
            if (r0 == 0) goto L_0x0033
            r4.unlockCanvasAndPost(r0)     // Catch:{ Exception -> 0x0034 }
            r8.processTimer()     // Catch:{ Exception -> 0x0034 }
        L_0x0033:
            throw r2     // Catch:{ Exception -> 0x0034 }
        L_0x0034:
            r2 = move-exception
            r1 = r2
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.hangfire.spacesquadronFREE.MenuController.run(android.view.SurfaceHolder, android.content.Context, com.hangfire.spacesquadronFREE.GameThread, com.hangfire.spacesquadronFREE.BitmapFontDrawer, com.hangfire.spacesquadronFREE.Timer):int");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private void doDraw(Context context, Canvas canvas, GameThread thread, BitmapFontDrawer mTextDrawer) throws Exception {
        try {
            switch (this.mScreen) {
                case 0:
                    drawSplashScreen(canvas);
                    return;
                case 1:
                    drawTitleScreen(canvas);
                    return;
                case 2:
                    drawMainMenu(canvas, thread, mTextDrawer);
                    return;
                case 3:
                    drawSettingsMenu(context, canvas, mTextDrawer, thread);
                    return;
                case 4:
                    drawHelpMenu(context, canvas, mTextDrawer);
                    return;
                case 5:
                    drawHallOfFame(canvas, thread, mTextDrawer);
                    return;
                case 6:
                    drawNewPilotScreen(context, canvas, thread, mTextDrawer);
                    return;
                case 7:
                    drawMessageScreen(canvas, mTextDrawer);
                    return;
                case 8:
                    drawLoadingScreen(canvas, mTextDrawer);
                    return;
                case 9:
                    drawMissionBriefing(canvas, mTextDrawer, thread);
                    return;
                case 10:
                    drawMissionResult(canvas, mTextDrawer, thread);
                    return;
                case 11:
                case 12:
                case 13:
                default:
                    return;
                case 14:
                    drawCareerRecord(canvas, thread, mTextDrawer);
                    return;
                case 15:
                    drawYNMessageScreen(canvas, mTextDrawer);
                    return;
                case 16:
                    drawGameComplete(canvas, mTextDrawer, thread);
                    return;
                case 17:
                    drawChooseMission(context, canvas, thread, mTextDrawer);
                    return;
            }
        } catch (Exception e) {
            throw e;
        }
        throw e;
    }

    private void drawBackDrop(Canvas canvas) throws Exception {
        try {
            canvas.drawBitmap(this.mBMPBackDrop, (float) this.mBackDropLeft, (float) this.mBackDropTop, (Paint) null);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawTitle(Canvas canvas, float y) throws Exception {
        try {
            canvas.drawBitmap(this.mBMPTitle, (float) this.mTitleLeft, y, (Paint) null);
        } catch (Exception e) {
            throw e;
        }
    }

    public void setSurfaceSize(Context context, int width, int height) throws Exception {
        try {
            this.mResized = true;
            this.mWidth = width;
            this.mHeight = height;
            this.mCenterX = width / 2;
            Resources res = context.getResources();
            Bitmap rawBackDrop = BitmapFactory.decodeResource(res, R.drawable.menubackdrop);
            float imgWidth = (float) rawBackDrop.getWidth();
            float imgHeight = (float) rawBackDrop.getHeight();
            float screenRatio = ((float) width) / ((float) height);
            float imgRatio = imgWidth / imgHeight;
            if (screenRatio != imgRatio) {
                if (imgRatio > screenRatio) {
                    imgHeight = (float) height;
                    imgWidth = imgHeight * imgRatio;
                } else {
                    imgWidth = (float) width;
                    imgHeight = (1.0f * imgWidth) / imgRatio;
                }
            }
            this.mBMPBackDrop = Bitmap.createScaledBitmap(rawBackDrop, (int) imgWidth, (int) imgHeight, true);
            this.mBackDropLeft = (width / 2) - (this.mBMPBackDrop.getWidth() / 2);
            this.mBackDropTop = (height / 2) - (this.mBMPBackDrop.getHeight() / 2);
            this.mBMPTitle = fitToWidth(res, R.drawable.menutitle, this.mWidth);
            this.mTitleLeft = (width / 2) - (this.mBMPTitle.getWidth() / 2);
            this.mBMPHangfireLogo = fitToWidth(res, R.drawable.hangfirelogo, this.mWidth);
            this.mHelpTop = 0;
            this.mHallIconWidth = width / 8;
            if (this.mHallIconWidth > 100) {
                this.mHallIconWidth = 100;
                this.mBMPIconWon = BitmapFactory.decodeResource(res, R.drawable.fameiconwon);
                this.mBMPIconQuit = BitmapFactory.decodeResource(res, R.drawable.fameiconquit);
                this.mBMPIconDead = BitmapFactory.decodeResource(res, R.drawable.fameicondead);
            } else {
                this.mBMPIconWon = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.fameiconwon), this.mHallIconWidth, this.mHallIconWidth, true);
                this.mBMPIconQuit = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.fameiconquit), this.mHallIconWidth, this.mHallIconWidth, true);
                this.mBMPIconDead = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, R.drawable.fameicondead), this.mHallIconWidth, this.mHallIconWidth, true);
            }
            int maxAwardWidth = (int) (((double) this.mWidth) / 6.5d);
            this.mBMPMedalAFC = fitToWidth(res, R.drawable.medalafc, maxAwardWidth);
            this.mBMPMedalBIA = fitToWidth(res, R.drawable.medalbia, maxAwardWidth * 2);
            this.mBMPMedalCGC = fitToWidth(res, R.drawable.medalcgc, maxAwardWidth);
            this.mBMPMedalDFC = fitToWidth(res, R.drawable.medaldfc, maxAwardWidth);
            this.mBMPMedalMSM = fitToWidth(res, R.drawable.medalmsm, maxAwardWidth);
            int maxRankWidth = this.mWidth / 8;
            for (int i = 0; i < 10; i++) {
                this.mBMPRank[i] = fitToWidth(res, R.drawable.rank01 + i, maxRankWidth);
            }
            positionSizeItems(context);
            this.mTimeStamp = System.currentTimeMillis();
        } catch (Exception e) {
            throw e;
        }
    }

    private Bitmap fitToWidth(Resources res, int lid, int width) throws Exception {
        try {
            Bitmap mBMPSrc = BitmapFactory.decodeResource(res, lid);
            if (width >= mBMPSrc.getWidth()) {
                return mBMPSrc;
            }
            return Bitmap.createScaledBitmap(mBMPSrc, width, (int) (((float) (width * 1)) / (((float) mBMPSrc.getWidth()) / ((float) mBMPSrc.getHeight()))), true);
        } catch (Exception e) {
            throw e;
        }
    }

    private void positionSizeItems(Context context) throws Exception {
        try {
            int width = this.mWidth / 2;
            int verticalSpace = this.mHeight - this.mBMPTitle.getHeight();
            int height = verticalSpace / 8;
            int verticalSpacing = verticalSpace / 7;
            int x = this.mCenterX - (width / 2);
            int y = this.mBMPTitle.getHeight() + 20;
            this.menuButton.get(0).setData(x, y, x + width, y + height, context.getString(R.string.mainmenu_new), width / 9, 0);
            int y2 = y + verticalSpacing;
            this.menuButton.get(1).setData(x, y2, x + width, y2 + height, context.getString(R.string.mainmenu_resume), width / 9, 0);
            int y3 = y2 + verticalSpacing;
            this.menuButton.get(2).setData(x, y3, x + width, y3 + height, context.getString(R.string.mainmenu_hall), width / 9, 0);
            int y4 = y3 + verticalSpacing;
            this.menuButton.get(3).setData(x, y4, x + width, y4 + height, context.getString(R.string.mainmenu_help), width / 9, 0);
            int y5 = y4 + verticalSpacing;
            this.menuButton.get(4).setData(x, y5, x + width, y5 + height, context.getString(R.string.mainmenu_settings), width / 9, 0);
            int y6 = y5 + verticalSpacing;
            this.menuButton.get(5).setData(x, y6, x + width, y6 + height, context.getString(R.string.mainmenu_exit), width / 9, 0);
            this.menuButton.get(6).setData(x, y6, x + width, y6 + height, context.getString(R.string.helpmenu_done), width / 9, 0);
            int leftX = (this.mCenterX - width) + 10;
            this.menuButton.get(7).setData(leftX, 10, height + leftX, 10 + height, "X", width / 8, 0);
            int y7 = 10 + verticalSpacing;
            this.menuButton.get(8).setData(leftX, y7, height + leftX, y7 + height, "X", width / 8, 0);
            int y8 = y7 + verticalSpacing;
            this.menuButton.get(9).setData(leftX, y8, height + leftX, y8 + height, "X", width / 8, 0);
            int y9 = y8 + verticalSpacing;
            this.menuButton.get(10).setData(leftX, y9, height + leftX, y9 + height, "X", width / 8, 0);
            this.menuButton.get(11).setData(x, (this.mHeight - height) - 10, x + width, this.mHeight - 10, context.getString(R.string.settingsmenu_done), width / 9, 0);
            int buttonWidth = this.mWidth / 7;
            int buttonStep = this.mWidth / 6;
            int buttonGap = buttonStep - buttonWidth;
            for (int i = 12; i < BUTTON_NEWPILOT_SHIFT; i++) {
                x = (((i - 12) % 6) * buttonStep) + (buttonGap / 2);
                y9 = this.mHeight - ((buttonGap / 2) + ((6 - ((int) Math.floor((double) ((i - 12) / 6)))) * buttonStep));
                this.menuButton.get(i).setData(x, y9, x + buttonWidth, y9 + buttonWidth, "", (int) (((double) buttonWidth) / 1.5d), 0);
            }
            setKeyboardCharacters();
            int x2 = x + buttonStep;
            this.menuButton.get(BUTTON_NEWPILOT_SPACE).setData(x2, y9, (buttonWidth * 4) + x2 + (buttonGap * 3), y9 + buttonWidth, "", 1, 0);
            int x3 = buttonGap / 2;
            int y10 = y9 + buttonStep;
            this.menuButton.get(BUTTON_NEWPILOT_SHIFT).setData(x3, y10, (buttonWidth * 2) + x3 + buttonGap, y10 + buttonWidth, "SHIFT", buttonWidth / 2, 0);
            int x4 = x3 + (buttonStep * 2);
            this.menuButton.get(40).setData(x4, y10, (buttonWidth * 2) + x4 + buttonGap, y10 + buttonWidth, "DEL", buttonWidth / 2, 0);
            int x5 = x4 + (buttonStep * 2);
            this.menuButton.get(BUTTON_NEWPILOT_OK).setData(x5, y10, (buttonWidth * 2) + x5 + buttonGap, y10 + buttonWidth, "OK", buttonWidth / 2, 0);
            int y11 = (this.mHeight - (buttonGap / 2)) - (buttonStep * 7);
            this.menuButton.get(BUTTON_NEWPILOT_DUMMY).setData(buttonGap / 2, y11, this.mWidth - (buttonGap / 2), y11 + buttonWidth, "", (int) (((double) buttonWidth) / 1.5d), 0);
            int msgWidth = (this.mWidth / 8) * 7;
            int msgOffsetWidth = (this.mWidth - msgWidth) / 2;
            int msgHeight = this.mHeight / 2;
            int msgOffsetHeight = (this.mHeight - msgHeight) / 2;
            this.menuButton.get(BUTTON_MESSAGE_DUMMY).setData(msgOffsetWidth, msgOffsetHeight, msgOffsetWidth + msgWidth, msgOffsetHeight + msgHeight, "", 0, 0);
            int msgOKX = (this.mWidth / 2) - buttonWidth;
            int msgOKY = ((msgOffsetHeight + msgHeight) - buttonWidth) - 5;
            this.menuButton.get(BUTTON_MESSAGE_OK).setData(msgOKX, msgOKY, msgOKX + (buttonWidth * 2), msgOKY + buttonWidth, "OK", buttonWidth / 2, 0);
            int halfLoadingWidth = ((int) (((double) this.mWidth) / 1.5d)) / 2;
            int loadingHeight = this.mWidth / 6;
            int halfLoadingHeight = loadingHeight / 2;
            this.menuButton.get(BUTTON_LOADING_DUMMY).setData((this.mWidth / 2) - halfLoadingWidth, (this.mHeight / 2) - halfLoadingHeight, (this.mWidth / 2) + halfLoadingWidth, (this.mHeight / 2) + halfLoadingHeight, "LOADING...", (int) (((double) loadingHeight) / 1.5d), 1);
            int briefingX = (this.mWidth / 2) - buttonWidth;
            int briefingY = (this.mHeight - buttonWidth) - 5;
            this.menuButton.get(BUTTON_MISSIONBRIEF_OK).setData(briefingX, briefingY, briefingX + (buttonWidth * 2), briefingY + buttonWidth, "START", buttonWidth / 2, 0);
            this.menuButton.get(BUTTON_MISSIONRESULT_OK).setData(briefingX, briefingY, briefingX + (buttonWidth * 2), briefingY + buttonWidth, "OK", buttonWidth / 2, 0);
            this.menuButton.get(BUTTON_GAMECOMPLETE_OK).setData(briefingX, briefingY, briefingX + (buttonWidth * 2), briefingY + buttonWidth, "OK", buttonWidth / 2, 0);
            int hallX = (this.mWidth / 2) - buttonWidth;
            int hallY = (this.mHeight - buttonWidth) - 5;
            this.menuButton.get(BUTTON_HALL_OK).setData(hallX, hallY, hallX + (buttonWidth * 2), hallY + buttonWidth, "DONE", buttonWidth / 2, 0);
            this.menuButton.get(BUTTON_HALL_CLEAR).setData(this.mWidth - buttonWidth, (this.mHeight - (buttonWidth / 2)) - 5, this.mWidth, this.mHeight - 5, "CLEAR", buttonWidth / 4, 0);
            this.menuButton.get(50).setData(hallX, hallY, hallX + (buttonWidth * 2), hallY + buttonWidth, "DONE", buttonWidth / 2, 0);
            this.menuButton.get(BUTTON_CAREER_DELETE).setData(this.mWidth - buttonWidth, (this.mHeight - (buttonWidth / 2)) - 5, this.mWidth, this.mHeight - 5, "CLEAR", buttonWidth / 4, 0);
            int msgWidth2 = (this.mWidth / 8) * 7;
            int msgOffsetWidth2 = (this.mWidth - msgWidth2) / 2;
            int msgHeight2 = this.mHeight / 2;
            int msgOffsetHeight2 = (this.mHeight - msgHeight2) / 2;
            this.menuButton.get(BUTTON_YNMESSAGE_DUMMY).setData(msgOffsetWidth2, msgOffsetHeight2, msgOffsetWidth2 + msgWidth2, msgOffsetHeight2 + msgHeight2, "", 0, 0);
            int msgYesX = (this.mWidth / 2) - (buttonWidth * 3);
            int msgNoX = (this.mWidth / 2) + buttonWidth;
            int msgY = ((msgOffsetHeight2 + msgHeight2) - buttonWidth) - 5;
            this.menuButton.get(BUTTON_YNMESSAGE_YES).setData(msgYesX, msgY, msgYesX + (buttonWidth * 2), msgY + buttonWidth, "YES", buttonWidth / 2, 0);
            this.menuButton.get(BUTTON_YNMESSAGE_NO).setData(msgNoX, msgY, msgNoX + (buttonWidth * 2), msgY + buttonWidth, "NO", buttonWidth / 2, 0);
            int buttonWidth2 = this.mWidth / 7;
            int middlex = this.mWidth / 2;
            int middley = this.mHeight / 2;
            this.menuButton.get(BUTTON_CHOOSEMISSION_DUMMY).setData(middlex - buttonWidth2, middley - (buttonWidth2 / 2), middlex + buttonWidth2, middley + (buttonWidth2 / 2), "", (int) (((double) buttonWidth2) / 1.5d), 0);
            this.menuButton.get(BUTTON_CHOOSEMISSION_DUMMY).setEnabled(false);
            this.menuButton.get(BUTTON_CHOOSEMISSION_DOWN).setData((middlex - (buttonWidth2 * 2)) - 5, middley - (buttonWidth2 / 2), (middlex - buttonWidth2) - 5, middley + (buttonWidth2 / 2), "-", buttonWidth2 / 2, 0);
            this.menuButton.get(BUTTON_CHOOSEMISSION_UP).setData(middlex + buttonWidth2 + 5, middley - (buttonWidth2 / 2), (buttonWidth2 * 2) + middlex + 5, middley + (buttonWidth2 / 2), "+", buttonWidth2 / 2, 0);
            this.menuButton.get(BUTTON_CHOOSEMISSION_DONE).setData(middlex - buttonWidth2, (this.mHeight - buttonWidth2) - 10, middlex + buttonWidth2, this.mHeight - 10, "OK", buttonWidth2 / 2, 0);
        } catch (Exception e) {
            throw e;
        }
    }

    private void setKeyboardCharacters() throws Exception {
        int i = 12;
        while (i < BUTTON_NEWPILOT_SHIFT) {
            try {
                this.menuButton.get(i).setText(getCharForKey(i));
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    private String getCharForKey(int buttonID) throws Exception {
        try {
            if (this.menuButton.get(BUTTON_NEWPILOT_SHIFT).on) {
                return Character.toString((char) ((buttonID - 12) + CHARACTER_UPPERCASE_START));
            }
            return Character.toString((char) ((buttonID - 12) + CHARACTER_LOWERCASE_START));
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean onTouch(MotionEvent event) throws Exception {
        try {
            if (this.mTouchEventUp == null && event.getAction() == 1) {
                this.mTouchEventUp = event;
                return true;
            } else if (this.mTouchEventDown == null && event.getAction() == 0) {
                this.mTouchEventDown = event;
                return true;
            } else if (this.mTouchEventMove != null || event.getAction() != 2) {
                return false;
            } else {
                this.mTouchEventMove = event;
                return true;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void handleInput(GameThread thread, Context context) throws Exception {
        try {
            handleTouchEvents(thread, context);
        } catch (Exception e) {
            throw e;
        }
    }

    private void handleTouchEvents(GameThread thread, Context context) throws Exception {
        try {
            if (this.mTouchEventDown != null) {
                touchDown(this.mTouchEventDown.getX(), this.mTouchEventDown.getY());
                this.mTouchEventDown = null;
            }
            if (this.mTouchEventMove != null) {
                touchDrag(this.mTouchEventMove.getX(), this.mTouchEventMove.getY(), thread);
                this.mTouchEventMove = null;
            }
            if (this.mTouchEventUp != null) {
                touchUp(thread, this.mTouchEventUp.getX(), this.mTouchEventUp.getY(), context);
                this.mTouchEventUp = null;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void touchUp(GameThread thread, float x, float y, Context context) throws Exception {
        try {
            MenuButton b = touchButtons(x, y, false);
            if (b != null) {
                switch (b.id) {
                    case 0:
                        newGamePressed(context, thread);
                        break;
                    case 1:
                        resumeGame();
                        break;
                    case 2:
                        changeScreen(5, 0);
                        break;
                    case 3:
                        changeScreen(4, 0);
                        break;
                    case 4:
                        changeScreen(3, 0);
                        break;
                    case 5:
                        exitApp();
                        break;
                    case 6:
                        exitHelp();
                        break;
                    case 7:
                        thread.setSoundOn(b.on);
                        break;
                    case 8:
                        thread.setMusicOn(b.on);
                        break;
                    case 9:
                        thread.setVibrationOn(b.on);
                        break;
                    case 10:
                        thread.setTutorialOn(b.on);
                        break;
                    case 11:
                        exitSettings(thread);
                        break;
                    case BUTTON_NEWPILOT_SHIFT /*38*/:
                        setKeyboardCharacters();
                        break;
                    case BUTTON_NEWPILOT_OK /*41*/:
                        newPilotOkPressed(thread, context);
                        break;
                    case BUTTON_MESSAGE_OK /*43*/:
                        changeScreen(this.mReturnScreen, 0);
                        break;
                    case BUTTON_MISSIONBRIEF_OK /*46*/:
                        startGame();
                        break;
                    case BUTTON_MISSIONRESULT_OK /*47*/:
                        exitMissionResult(context, thread);
                        break;
                    case BUTTON_HALL_OK /*48*/:
                        exitHall();
                        break;
                    case BUTTON_HALL_CLEAR /*49*/:
                        clearHallPressed(context);
                        break;
                    case 50:
                        exitCareer();
                        break;
                    case BUTTON_CAREER_DELETE /*51*/:
                        clearCareerPressed(context, thread);
                        break;
                    case BUTTON_YNMESSAGE_YES /*52*/:
                        messageYNYesPressed(thread);
                        break;
                    case BUTTON_YNMESSAGE_NO /*53*/:
                        changeScreen(this.mNoScreen, 0);
                        break;
                    case BUTTON_GAMECOMPLETE_OK /*55*/:
                        exitGameComplete(thread);
                        break;
                    case BUTTON_CHOOSEMISSION_UP /*56*/:
                        setStartMission(this.mStartMission + 1, thread);
                        break;
                    case BUTTON_CHOOSEMISSION_DOWN /*57*/:
                        setStartMission(this.mStartMission - 1, thread);
                        break;
                    case BUTTON_CHOOSEMISSION_DONE /*59*/:
                        chooseMissionOKPressed(context, thread);
                        break;
                }
                if (b.id >= 12 && b.id <= 40) {
                    nameEntry(thread, b.id);
                }
                playInputSound(thread);
            } else if (this.mScreen == 5 && !this.mDragged) {
                selectHighScore(x, y, thread);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void chooseMissionOKPressed(Context context, GameThread thread) throws Exception {
        try {
            thread.getCareerRecords().newCurrent(this.mNewPlayerName);
            thread.loadMissionData(this.mStartMission);
            thread.getCareerRecords().getCurrent().setCurrentMission(this.mStartMission);
            switchToBriefingScreen(context, thread);
        } catch (Exception e) {
            throw e;
        }
    }

    private void setStartMission(int mission, GameThread thread) throws Exception {
        if (mission >= 1 && mission <= 2) {
            try {
                if (mission <= thread.getUnlockedMission()) {
                    this.mStartMission = mission;
                    this.mStartMissionString = String.valueOf(Integer.toString(mission)) + "/" + Integer.toString(2);
                }
            } catch (Exception e) {
                throw e;
            }
        }
    }

    private void newGamePressed(Context context, GameThread thread) throws Exception {
        try {
            if (thread.getCareerRecords().getCurrent() == null) {
                changeScreen(6, 0);
            } else {
                displayYNMessage(context.getString(R.string.yesno_endcurrentgame), 6, 2);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void clearHallPressed(Context context) throws Exception {
        try {
            this.mRecordToDelete = -1;
            displayYNMessage(context.getString(R.string.yesno_clearhall), 5, 5);
        } catch (Exception e) {
            throw e;
        }
    }

    private void clearCareerPressed(Context context, GameThread thread) throws Exception {
        try {
            if (thread.getCareerRecords().getHallOfFame().get(this.mCareerRecordID) == thread.getCareerRecords().getCurrent()) {
                displayMessage(context.getString(R.string.popup_cantdeletecareer), 14);
                return;
            }
            this.mRecordToDelete = this.mCareerRecordID;
            displayYNMessage(context.getString(R.string.yesno_clearcareer), 5, 14);
        } catch (Exception e) {
            throw e;
        }
    }

    private void messageYNYesPressed(GameThread thread) throws Exception {
        try {
            changeScreen(this.mYesScreen, 0);
            if (this.mYesScreen == 6) {
                thread.getCareerRecords().deleteCurrent(thread.getSP(), 0);
                thread.deleteGameState();
            }
            if (this.mYesScreen == 5 && this.mRecordToDelete != DELETE_RECORD_NONE) {
                if (this.mRecordToDelete == -1) {
                    this.mHallY = 0;
                    thread.getCareerRecords().deleteCareerRecords(thread.getSP());
                } else {
                    this.mCareerY = 0;
                    thread.getCareerRecords().deleteCareerRecord(thread.getSP(), this.mRecordToDelete);
                }
                this.mRecordToDelete = DELETE_RECORD_NONE;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void selectHighScore(float x, float y, GameThread thread) throws Exception {
        try {
            ArrayList<CareerRecord> careerRecords = thread.getCareerRecords().getHallOfFame();
            int index = ((((int) y) - ((this.mWidth / 12) + 20)) - this.mHallY) / (this.mHallIconWidth + 5);
            if (index >= 0 && index < careerRecords.size()) {
                this.mCareerRecordID = index;
                changeScreen(14, 0);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void nameEntry(GameThread thread, int code) throws Exception {
        try {
            String name = this.mNewPlayerName;
            if (code >= 12 && code <= BUTTON_NEWPILOT_Z && name.length() < 12) {
                name = String.valueOf(name) + getCharForKey(code);
            }
            if (code == 40 && name.length() > 0) {
                name = name.substring(0, name.length() - 1);
            }
            if (code == BUTTON_NEWPILOT_SPACE && name.length() > 0) {
                name = String.valueOf(name) + " ";
            }
            this.mNewPlayerName = name;
        } catch (Exception e) {
            throw e;
        }
    }

    private void displayMessage(String message, int returnScreen) throws Exception {
        try {
            this.mReturnScreen = returnScreen;
            this.mMessage = message;
            changeScreen(7, 0);
        } catch (Exception e) {
            throw e;
        }
    }

    private void displayYNMessage(String message, int yesScreen, int noScreen) throws Exception {
        try {
            this.mYesScreen = yesScreen;
            this.mNoScreen = noScreen;
            this.mYNMessage = message;
            changeScreen(15, 0);
        } catch (Exception e) {
            throw e;
        }
    }

    private void newPilotOkPressed(GameThread thread, Context context) throws Exception {
        try {
            this.mNewPlayerName = this.mNewPlayerName.trim();
            if (this.mNewPlayerName.length() < 3) {
                displayMessage(context.getString(R.string.popup_invalidname).replace("<NUM>", Integer.toString(3)), 6);
            } else {
                changeScreen(17, 0);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void switchToBriefingScreen(Context context, GameThread thread) throws Exception {
        try {
            playMissionSound(context, thread, thread.getMissionData().getSoundID());
            changeScreen(9, 0);
        } catch (Exception e) {
            throw e;
        }
    }

    private void exitMissionResult(Context context, GameThread thread) throws Exception {
        try {
            switch (this.mSubMode) {
                case 1:
                    int nextMission = thread.getMissionData().getMissionNumber() + 1;
                    if (nextMission <= 2) {
                        thread.loadMissionData(nextMission);
                        thread.getCareerRecords().getCurrent().setCurrentMission(nextMission);
                        switchToBriefingScreen(context, thread);
                        break;
                    } else {
                        thread.getCareerRecords().deleteCurrent(thread.getSP(), 1);
                        changeScreen(16, 0);
                        playMissionSound(context, thread, R.raw.speechvictory);
                        break;
                    }
                case 2:
                    switchToBriefingScreen(context, thread);
                    break;
                case 3:
                    thread.getCareerRecords().deleteCurrent(thread.getSP(), 2);
                    changeScreen(2, 0);
                    break;
            }
            this.mResultY = 0;
        } catch (Exception e) {
            throw e;
        }
    }

    private void startGame() throws Exception {
        try {
            stopMissionSound();
            changeScreen(8, 0);
            this.mReturnMode = 4;
        } catch (Exception e) {
            throw e;
        }
    }

    private void resumeGame() throws Exception {
        try {
            this.mReturnMode = 5;
        } catch (Exception e) {
            throw e;
        }
    }

    private void exitApp() throws Exception {
        try {
            this.mReturnMode = 6;
        } catch (Exception e) {
            throw e;
        }
    }

    private void touchDrag(float x, float y, GameThread thread) throws Exception {
        try {
            if (Math.abs(this.mDragY - ((int) y)) > 5) {
                this.mDragged = true;
            }
            if (this.mScreen == 4) {
                dragHelpScreen(y);
            }
            if (this.mScreen == 5) {
                dragHallScreen(y, thread);
            }
            if (this.mScreen == 14) {
                dragCareerScreen(y, thread);
            }
            if (this.mScreen == 10) {
                dragResultScreen(y);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void touchDown(float x, float y) throws Exception {
        try {
            touchButtons(x, y, true);
            this.mDragY = (int) y;
            this.mHelpTopDrag = this.mHelpTop;
            this.mHallDragY = this.mHallY;
            this.mCareerDragY = this.mCareerY;
            this.mResultDragY = this.mResultY;
            this.mDragged = false;
        } catch (Exception e) {
            throw e;
        }
    }

    private void dragHelpScreen(float y) throws Exception {
        try {
            this.mHelpTop = this.mHelpTopDrag - (this.mDragY - ((int) y));
            if (this.mHelpTop > 0) {
                this.mHelpTop = 0;
            }
            int min = this.mHeight - this.mHelpScreenHeight;
            if (min > 0) {
                min = 0;
            }
            if (this.mHelpTop < min) {
                this.mHelpTop = min;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void dragHallScreen(float y, GameThread thread) throws Exception {
        try {
            this.mHallY = this.mHallDragY - (this.mDragY - ((int) y));
            if (this.mHallY > 0) {
                this.mHallY = 0;
            }
            ArrayList<CareerRecord> careerRecords = thread.getCareerRecords().getHallOfFame();
            int min = this.mHeight - (((this.menuButton.get(BUTTON_HALL_OK).height + ((this.mHallIconWidth + 5) * careerRecords.size())) + (this.mWidth / 12)) + 30);
            if (min > 0) {
                min = 0;
            }
            if (this.mHallY < min) {
                this.mHallY = min;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void dragCareerScreen(float y, GameThread thread) throws Exception {
        try {
            this.mCareerY = this.mCareerDragY - (this.mDragY - ((int) y));
            if (this.mCareerY > 0) {
                this.mCareerY = 0;
            }
            int min = this.mHeight - this.mCareerScreenHeight;
            if (min > 0) {
                min = 0;
            }
            if (this.mCareerY < min) {
                this.mCareerY = min;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void dragResultScreen(float y) throws Exception {
        try {
            this.mResultY = this.mResultDragY - (this.mDragY - ((int) y));
            if (this.mResultY > 0) {
                this.mResultY = 0;
            }
            int min = this.mHeight - this.mResultScreenHeight;
            if (min > 0) {
                min = 0;
            }
            if (this.mResultY < min) {
                this.mResultY = min;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void changeScreen(int newScreen, int subMode) throws Exception {
        try {
            System.gc();
            for (int i = 0; i < this.menuButton.size(); i++) {
                MenuButton b = this.menuButton.get(i);
                b.setVisible(b.screen == newScreen);
            }
            this.mScreen = newScreen;
            this.mSubMode = subMode;
        } catch (Exception e) {
            throw e;
        }
    }

    private MenuButton touchButtons(float x, float y, boolean push) throws Exception {
        MenuButton b = null;
        int i = 0;
        while (i < this.menuButton.size()) {
            try {
                if (this.menuButton.get(i).press(x, y, push)) {
                    b = this.menuButton.get(i);
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
        return b;
    }

    private void exitHelp() throws Exception {
        try {
            this.mHelpTop = 0;
            changeScreen(2, 0);
        } catch (Exception e) {
            throw e;
        }
    }

    private void exitSettings(GameThread thread) throws Exception {
        try {
            thread.saveSettings();
            changeScreen(2, 0);
        } catch (Exception e) {
            throw e;
        }
    }

    private void exitNewPilot() throws Exception {
        try {
            changeScreen(2, 0);
        } catch (Exception e) {
            throw e;
        }
    }

    private void exitChooseMission() throws Exception {
        try {
            changeScreen(6, 0);
        } catch (Exception e) {
            throw e;
        }
    }

    private void exitMissionBriefing() throws Exception {
        try {
            stopMissionSound();
            changeScreen(2, 0);
        } catch (Exception e) {
            throw e;
        }
    }

    private void exitHall() throws Exception {
        try {
            this.mHallY = 0;
            changeScreen(2, 0);
        } catch (Exception e) {
            throw e;
        }
    }

    private void exitCareer() throws Exception {
        try {
            this.mCareerY = 0;
            changeScreen(5, 0);
        } catch (Exception e) {
            throw e;
        }
    }

    private void exitGameComplete(GameThread thread) throws Exception {
        try {
            changeScreen(5, 0);
        } catch (Exception e) {
            throw e;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void backButtonPressed(boolean depressed, GameThread thread, Context context) throws Exception {
        if (!depressed) {
            try {
                switch (this.mScreen) {
                    case 2:
                        exitApp();
                        return;
                    case 3:
                        exitSettings(thread);
                        return;
                    case 4:
                        exitHelp();
                        return;
                    case 5:
                        exitHall();
                        return;
                    case 6:
                        exitNewPilot();
                        return;
                    case 7:
                    case 8:
                    case 11:
                    case 12:
                    case 13:
                    case 15:
                    default:
                        return;
                    case 9:
                        exitMissionBriefing();
                        return;
                    case 10:
                        exitMissionResult(context, thread);
                        return;
                    case 14:
                        exitCareer();
                        return;
                    case 16:
                        exitGameComplete(thread);
                        return;
                    case 17:
                        exitChooseMission();
                        return;
                }
            } catch (Exception e) {
                throw e;
            }
            throw e;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    private void drawMainMenu(Canvas canvas, GameThread thread, BitmapFontDrawer mTextDrawer) throws Exception {
        boolean z;
        try {
            drawBackDrop(canvas);
            drawTitle(canvas, 5.0f);
            this.menuButton.get(0).draw(canvas, mTextDrawer);
            MenuButton menuButton2 = this.menuButton.get(1);
            if (thread.getCareerRecords().getCurrent() != null) {
                z = true;
            } else {
                z = false;
            }
            menuButton2.setEnabled(z);
            this.menuButton.get(1).draw(canvas, mTextDrawer);
            this.menuButton.get(2).draw(canvas, mTextDrawer);
            this.menuButton.get(3).draw(canvas, mTextDrawer);
            this.menuButton.get(4).draw(canvas, mTextDrawer);
            this.menuButton.get(5).draw(canvas, mTextDrawer);
        } catch (Exception e) {
            throw e;
        }
    }

    private void addHelpTextItem(int index, int height, String[] lines, Bitmap image) throws Exception {
        try {
            this.mHelpItem[index] = new HelpTextItem(this, null);
            this.mHelpItem[index].height = height;
            this.mHelpItem[index].lines = lines;
            this.mHelpItem[index].img = image;
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawHelpMenu(Context context, Canvas canvas, BitmapFontDrawer mTextDrawer) throws Exception {
        try {
            int fontHeight = this.mWidth / 18;
            int i = fontHeight;
            if (!this.mHelpReady) {
                this.mHelpItem = new HelpTextItem[8];
                for (int i2 = 0; i2 < 7; i2++) {
                    addHelpTextItem(i2, mTextDrawer.generateCacheAndHeight(context.getString(R.string.helptext1 + i2), 1, fontHeight, this.mWidth - 10), mTextDrawer.getCacheData(), BitmapFactory.decodeResource(context.getResources(), R.drawable.helpimage1 + i2));
                }
                addHelpTextItem(7, mTextDrawer.generateCacheAndHeight(context.getString(R.string.helptext8), 1, fontHeight, this.mWidth - 10), mTextDrawer.getCacheData(), null);
                this.mHelpReady = true;
            }
            int y = this.mHelpTop + fontHeight;
            drawBackDrop(canvas);
            canvas.drawARGB(200, 0, 0, 0);
            for (int i3 = 0; i3 < 8; i3++) {
                y = drawHelpItem(canvas, this.mHelpItem[i3].img, this.mHelpItem[i3].lines, mTextDrawer, 1, y, this.mHelpItem[i3].height);
            }
            MenuButton b = this.menuButton.get(6);
            b.setPosition(b.x, y);
            b.draw(canvas, mTextDrawer);
            this.mHelpScreenHeight = (y + (b.height + 5)) - this.mHelpTop;
        } catch (Exception e) {
            throw e;
        }
    }

    private int drawHelpItem(Canvas canvas, Bitmap img, String[] text, BitmapFontDrawer mTextDrawer, int font, int y, int height) throws Exception {
        if (text != null) {
            if (y + height > 0) {
                try {
                    if (y < this.mHeight) {
                        mTextDrawer.drawLines(canvas, text, this.mWidth / 2, y, font, this.mWidth / 18, this.mWidth - 10, null);
                    }
                } catch (Exception e) {
                    throw e;
                }
            }
            y += height + 5;
        }
        if (img == null) {
            return y;
        }
        if (y + height > 0 && y < this.mHeight) {
            canvas.drawBitmap(img, (float) ((this.mWidth / 2) - (img.getWidth() / 2)), (float) y, (Paint) null);
        }
        return y + img.getHeight() + 5 + (this.mWidth / 18);
    }

    private void drawSettingsMenu(Context context, Canvas canvas, BitmapFontDrawer mTextDrawer, GameThread thread) throws Exception {
        try {
            drawBackDrop(canvas);
            drawSettingsInfoBox(canvas, 7, thread.getSoundOn(), context.getString(R.string.settingsmenu_sound_description), mTextDrawer);
            drawSettingsInfoBox(canvas, 8, thread.getMusicOn(), context.getString(R.string.settingsmenu_music_description), mTextDrawer);
            drawSettingsInfoBox(canvas, 9, thread.getVibrationOn(), context.getString(R.string.settingsmenu_vibration_description), mTextDrawer);
            drawSettingsInfoBox(canvas, 10, thread.getTutorialOn(), context.getString(R.string.settingsmenu_tutorial_description), mTextDrawer);
            this.menuButton.get(11).draw(canvas, mTextDrawer);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawSettingsInfoBox(Canvas canvas, int bID, boolean toggleOn, String text, BitmapFontDrawer mTextDrawer) throws Exception {
        try {
            MenuButton b = this.menuButton.get(bID);
            b.setToggleOn(toggleOn);
            this.mMenuPaint.setARGB(150, 0, 0, 0);
            canvas.drawRect((float) (b.x - 1), (float) (b.y - 1), (float) ((this.mWidth - b.x) + 1), (float) (b.y2 + 1), this.mMenuPaint);
            b.draw(canvas, mTextDrawer);
            int fontHeight = this.mWidth / 16;
            mTextDrawer.draw(canvas, text, b.x2 + 10, b.getY(8) + (fontHeight / 2), 1, fontHeight, 1, null);
        } catch (Exception e) {
            throw e;
        }
    }

    private void processSplashScreens() throws Exception {
        try {
            if (this.mScreen == 0 || this.mScreen == 1) {
                if (this.mFirstTimeIn) {
                    this.mFirstTimeIn = false;
                    this.mTimeStamp = System.currentTimeMillis();
                }
                if (this.mTransitioning) {
                    this.mTitleRatio = 1.0f - (((float) (System.currentTimeMillis() - this.mTimeStamp)) / 500.0f);
                    this.mMenuPaint.setAlpha((int) (255.0f * this.mTitleRatio));
                    if (this.mTimeStamp + 500 < System.currentTimeMillis()) {
                        this.mTransitioning = false;
                        this.mFirstTimeIn = true;
                        this.mTitleRatio = 1.0f;
                        if (this.mScreen == 0) {
                            changeScreen(1, 0);
                            this.mBMPHangfireLogo.recycle();
                            this.mBMPHangfireLogo = null;
                        } else if (this.mScreen == 1) {
                            changeScreen(2, 0);
                        }
                    }
                } else if (this.mTimeStamp + 2000 < System.currentTimeMillis()) {
                    this.mTransitioning = true;
                    this.mTimeStamp = System.currentTimeMillis();
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawSplashScreen(Canvas canvas) throws Exception {
        try {
            drawBackDrop(canvas);
            drawTitle(canvas, (float) (((this.mHeight / 2) - (this.mBMPTitle.getHeight() / 2)) + 5));
            canvas.drawARGB(this.mMenuPaint.getAlpha(), 0, 0, 0);
            canvas.drawBitmap(this.mBMPHangfireLogo, (float) ((this.mWidth / 2) - (this.mBMPHangfireLogo.getWidth() / 2)), (float) ((this.mHeight / 2) - (this.mBMPHangfireLogo.getHeight() / 2)), this.mMenuPaint);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawTitleScreen(Canvas canvas) throws Exception {
        try {
            drawBackDrop(canvas);
            drawTitle(canvas, (float) (((int) (((float) ((this.mHeight / 2) - (this.mBMPTitle.getHeight() / 2))) * this.mTitleRatio)) + 5));
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawNewPilotScreen(Context context, Canvas canvas, GameThread thread, BitmapFontDrawer mTextDrawer) throws Exception {
        try {
            drawBackDrop(canvas);
            canvas.drawARGB(150, 0, 0, 0);
            for (int i = 12; i < BUTTON_NEWPILOT_SHIFT; i++) {
                this.menuButton.get(i).draw(canvas, mTextDrawer);
            }
            this.menuButton.get(BUTTON_NEWPILOT_SPACE).draw(canvas, mTextDrawer);
            this.menuButton.get(BUTTON_NEWPILOT_SHIFT).draw(canvas, mTextDrawer);
            this.menuButton.get(40).draw(canvas, mTextDrawer);
            this.menuButton.get(BUTTON_NEWPILOT_OK).draw(canvas, mTextDrawer);
            MenuButton mb = this.menuButton.get(BUTTON_NEWPILOT_DUMMY);
            mb.setText(this.mNewPlayerName);
            this.mMenuPaint.setARGB(150, 155, 255, 155);
            canvas.drawRect((float) (mb.x + 3), (float) (mb.y + 3), (float) (mb.x2 - 3), (float) (mb.y2 - 3), this.mMenuPaint);
            mb.draw(canvas, mTextDrawer);
            mTextDrawer.draw(canvas, context.getString(R.string.newpilot_entername), this.mCenterX, this.mWidth / 10, 1, this.mWidth / 10, 0, null);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawChooseMission(Context context, Canvas canvas, GameThread thread, BitmapFontDrawer mTextDrawer) throws Exception {
        try {
            drawBackDrop(canvas);
            canvas.drawARGB(150, 0, 0, 0);
            mTextDrawer.draw(canvas, context.getString(R.string.choosemission), this.mCenterX, this.mWidth / 11, 1, this.mWidth / 11, 0, null);
            this.menuButton.get(BUTTON_CHOOSEMISSION_DOWN).setEnabled(this.mStartMission > 1);
            this.menuButton.get(BUTTON_CHOOSEMISSION_UP).setEnabled(this.mStartMission < thread.getUnlockedMission());
            this.menuButton.get(BUTTON_CHOOSEMISSION_DOWN).draw(canvas, mTextDrawer);
            this.menuButton.get(BUTTON_CHOOSEMISSION_UP).draw(canvas, mTextDrawer);
            this.menuButton.get(BUTTON_CHOOSEMISSION_DONE).draw(canvas, mTextDrawer);
            MenuButton mb = this.menuButton.get(BUTTON_CHOOSEMISSION_DUMMY);
            mb.setText(this.mStartMissionString);
            this.mMenuPaint.setARGB(150, 155, 255, 155);
            canvas.drawRect((float) (mb.x + 3), (float) (mb.y + 3), (float) (mb.x2 - 3), (float) (mb.y2 - 3), this.mMenuPaint);
            mb.draw(canvas, mTextDrawer);
            mTextDrawer.draw(canvas, thread.getUnlockedMissionString(), this.mCenterX, mb.y - (this.mWidth / 14), 1, this.mWidth / 13, 0, null);
            mTextDrawer.draw(canvas, context.getString((R.string.mission1_name + this.mStartMission) - 1), this.mCenterX, mb.y2 + (this.mWidth / 13), 1, this.mWidth / 12, 0, null);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawMessageScreen(Canvas canvas, BitmapFontDrawer textDrawer) throws Exception {
        try {
            drawBackDrop(canvas);
            MenuButton mb = this.menuButton.get(BUTTON_MESSAGE_DUMMY);
            this.mMenuPaint.setARGB(150, 255, 0, 0);
            canvas.drawRect((float) (mb.x + 3), (float) (mb.y + 3), (float) (mb.x2 - 3), (float) (mb.y2 - 3), this.mMenuPaint);
            mb.draw(canvas, textDrawer);
            MenuButton mbOK = this.menuButton.get(BUTTON_MESSAGE_OK);
            mbOK.draw(canvas, textDrawer);
            int fontSize = mbOK.height / 2;
            textDrawer.draw(canvas, this.mMessage, mb.getX(8), mb.y + (fontSize * 2), 0, fontSize, 3, mb.width - 10, null);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawYNMessageScreen(Canvas canvas, BitmapFontDrawer textDrawer) throws Exception {
        try {
            drawBackDrop(canvas);
            MenuButton mb = this.menuButton.get(BUTTON_YNMESSAGE_DUMMY);
            this.mMenuPaint.setARGB(150, 255, 0, 0);
            canvas.drawRect((float) (mb.x + 3), (float) (mb.y + 3), (float) (mb.x2 - 3), (float) (mb.y2 - 3), this.mMenuPaint);
            mb.draw(canvas, textDrawer);
            MenuButton mbYes = this.menuButton.get(BUTTON_YNMESSAGE_YES);
            mbYes.draw(canvas, textDrawer);
            this.menuButton.get(BUTTON_YNMESSAGE_NO).draw(canvas, textDrawer);
            int fontSize = mbYes.height / 2;
            BitmapFontDrawer bitmapFontDrawer = textDrawer;
            Canvas canvas2 = canvas;
            bitmapFontDrawer.draw(canvas2, this.mYNMessage, mb.getX(8), mb.y + (fontSize * 2), 0, fontSize, 3, mb.width - 10, null);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawLoadingScreen(Canvas canvas, BitmapFontDrawer mTextDrawer) throws Exception {
        try {
            drawBackDrop(canvas);
            canvas.drawARGB(150, 0, 0, 0);
            this.menuButton.get(BUTTON_LOADING_DUMMY).setVisible(true);
            this.menuButton.get(BUTTON_LOADING_DUMMY).draw(canvas, mTextDrawer);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawMissionBriefing(Canvas canvas, BitmapFontDrawer textDrawer, GameThread thread) throws Exception {
        try {
            drawBackDrop(canvas);
            canvas.drawARGB(150, 0, 0, 0);
            MissionData md = thread.getMissionData();
            int fontSize = this.mWidth / 13;
            this.mMenuPaint.setARGB(150, 255, 255, 255);
            canvas.drawRect(0.0f, 0.0f, (float) this.mWidth, (float) (fontSize + 20), this.mMenuPaint);
            textDrawer.draw(canvas, String.valueOf(Integer.toString(md.getMissionNumber())) + "/" + 2 + ". " + md.getName(), this.mCenterX, fontSize + 10, 0, fontSize, 0, null);
            textDrawer.draw(canvas, md.getDescription(), this.mCenterX, (fontSize * 3) + 10, 1, fontSize, 3, null);
            this.menuButton.get(BUTTON_MISSIONBRIEF_OK).draw(canvas, textDrawer);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawMissionResult(Canvas canvas, BitmapFontDrawer textDrawer, GameThread thread) throws Exception {
        int y;
        CareerRecord stats;
        try {
            drawBackDrop(canvas);
            canvas.drawARGB(150, 0, 0, 0);
            MissionData md = thread.getMissionData();
            String headerText = "";
            String furtherInfo = "";
            boolean achievements = false;
            switch (this.mSubMode) {
                case 1:
                    headerText = "MISSION " + Integer.toString(md.getMissionNumber()) + " SUCCESSFUL!";
                    furtherInfo = "You completed the mission.";
                    achievements = true;
                    break;
                case 2:
                    headerText = "MISSION " + Integer.toString(md.getMissionNumber()) + " FAILED";
                    furtherInfo = "You failed the mission." + BitmapFontDrawer.newLine(2) + md.getReason() + BitmapFontDrawer.newLine(2) + "You must retry the mission.";
                    achievements = false;
                    break;
                case 3:
                    headerText = "GAME OVER";
                    furtherInfo = "Death in combat has ended your career." + BitmapFontDrawer.newLine(2) + md.getReason() + BitmapFontDrawer.newLine(2) + "GAME OVER";
                    achievements = true;
                    break;
            }
            int fontSize = this.mWidth / 12;
            int y2 = this.mResultY;
            this.mMenuPaint.setARGB(150, 255, 255, 255);
            canvas.drawRect(0.0f, (float) y2, (float) this.mWidth, (float) (fontSize + 20 + y2), this.mMenuPaint);
            textDrawer.draw(canvas, headerText, this.mCenterX, fontSize + 10 + y2, 0, fontSize, 0, null);
            textDrawer.draw(canvas, furtherInfo, this.mCenterX, (fontSize * 3) + 10 + y2, 1, fontSize, 3, null);
            int y3 = textDrawer.getFinalY() + fontSize;
            if (!achievements || (stats = thread.getCareerRecords().getStats()) == null || (stats.getAirForceCross() <= 0 && stats.getBraveryInTheAir() <= 0 && stats.getConspicuousGallantryCross() <= 0 && stats.getDistinguishedFlyingCross() <= 0 && stats.getMeritoriousServiceMedal() <= 0 && !stats.getPromoted())) {
                y = y3;
            } else {
                this.mMenuPaint.setARGB(150, 255, 255, 255);
                canvas.drawRect(0.0f, (float) y3, (float) this.mWidth, (float) (fontSize + 20 + y3), this.mMenuPaint);
                textDrawer.draw(canvas, "Congratulations!", this.mCenterX, fontSize + 10 + y3, 0, fontSize, 0, null);
                int y4 = y3 + fontSize + 20 + fontSize;
                if (stats.getPromoted()) {
                    textDrawer.draw(canvas, "You have been promoted!", this.mCenterX, y4, 1, fontSize, 0, null);
                    int y5 = y4 + fontSize;
                    CareerRecord current = thread.getCareerRecords().getCurrent();
                    if (current != null) {
                        int rankID = current.getRank();
                        String rankName = CareerRecords.getRank(rankID);
                        Bitmap rankImg = this.mBMPRank[rankID];
                        textDrawer.draw(canvas, rankName, this.mCenterX, (rankImg.getHeight() / 2) + y5 + (fontSize / 2), 1, fontSize, 0, null);
                        canvas.drawBitmap(rankImg, 0.0f, (float) (y5 + 2), (Paint) null);
                        canvas.drawBitmap(rankImg, (float) (this.mWidth - rankImg.getWidth()), (float) (y5 + 2), (Paint) null);
                        y4 = y5 + rankImg.getHeight() + 4 + fontSize;
                    } else {
                        return;
                    }
                }
                if (stats.getAirForceCross() > 0 || stats.getBraveryInTheAir() > 0 || stats.getConspicuousGallantryCross() > 0 || stats.getDistinguishedFlyingCross() > 0 || stats.getMeritoriousServiceMedal() > 0) {
                    textDrawer.draw(canvas, "You have been awarded:", this.mCenterX, y, 1, fontSize, 0, null);
                    y += fontSize * 2;
                    this.mMenuPaint.setARGB(150, 0, 150, 0);
                    if (stats.getConspicuousGallantryCross() > 0) {
                        textDrawer.draw(canvas, "Conspicuous Gallantry", this.mCenterX, y, 1, fontSize, 0, null);
                        int y6 = y + (fontSize / 2);
                        canvas.drawRect(0.0f, (float) (y6 - 2), (float) this.mWidth, (float) (this.mBMPMedalCGC.getHeight() + y6 + 2), this.mMenuPaint);
                        canvas.drawBitmap(this.mBMPMedalCGC, (float) (this.mCenterX - (this.mBMPMedalCGC.getWidth() / 2)), (float) y6, (Paint) null);
                        y = y6 + this.mBMPMedalCGC.getHeight() + (fontSize * 2);
                    }
                    if (stats.getDistinguishedFlyingCross() > 0) {
                        textDrawer.draw(canvas, "Distinguished Flying", this.mCenterX, y, 1, fontSize, 0, null);
                        int y7 = y + (fontSize / 2);
                        canvas.drawRect(0.0f, (float) (y7 - 2), (float) this.mWidth, (float) (this.mBMPMedalDFC.getHeight() + y7 + 2), this.mMenuPaint);
                        canvas.drawBitmap(this.mBMPMedalDFC, (float) (this.mCenterX - (this.mBMPMedalDFC.getWidth() / 2)), (float) y7, (Paint) null);
                        y = y7 + this.mBMPMedalDFC.getHeight() + (fontSize * 2);
                    }
                    if (stats.getAirForceCross() > 0) {
                        textDrawer.draw(canvas, "Air Force Cross", this.mCenterX, y, 1, fontSize, 0, null);
                        int y8 = y + (fontSize / 2);
                        canvas.drawRect(0.0f, (float) (y8 - 2), (float) this.mWidth, (float) (this.mBMPMedalAFC.getHeight() + y8 + 2), this.mMenuPaint);
                        canvas.drawBitmap(this.mBMPMedalAFC, (float) (this.mCenterX - (this.mBMPMedalAFC.getWidth() / 2)), (float) y8, (Paint) null);
                        y = y8 + this.mBMPMedalAFC.getHeight() + (fontSize * 2);
                    }
                    if (stats.getMeritoriousServiceMedal() > 0) {
                        textDrawer.draw(canvas, "Meritorious Service", this.mCenterX, y, 1, fontSize, 0, null);
                        int y9 = y + (fontSize / 2);
                        canvas.drawRect(0.0f, (float) (y9 - 2), (float) this.mWidth, (float) (this.mBMPMedalMSM.getHeight() + y9 + 2), this.mMenuPaint);
                        canvas.drawBitmap(this.mBMPMedalMSM, (float) (this.mCenterX - (this.mBMPMedalMSM.getWidth() / 2)), (float) y9, (Paint) null);
                        y = y9 + this.mBMPMedalMSM.getHeight() + (fontSize * 2);
                    }
                    if (stats.getBraveryInTheAir() > 0) {
                        textDrawer.draw(canvas, "Bravery In The Air", this.mCenterX, y, 1, fontSize, 0, null);
                        int y10 = y + (fontSize / 2);
                        canvas.drawRect(0.0f, (float) (y10 - 2), (float) this.mWidth, (float) (this.mBMPMedalBIA.getHeight() + y10 + 2), this.mMenuPaint);
                        canvas.drawBitmap(this.mBMPMedalBIA, (float) (this.mCenterX - (this.mBMPMedalBIA.getWidth() / 2)), (float) y10, (Paint) null);
                        y = y10 + this.mBMPMedalBIA.getHeight() + (fontSize * 2);
                    }
                }
            }
            MenuButton mbOK = this.menuButton.get(BUTTON_MISSIONRESULT_OK);
            mbOK.setPosition(mbOK.x, y);
            mbOK.draw(canvas, textDrawer);
            this.mResultScreenHeight = (y + (mbOK.height + 5)) - this.mResultY;
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawGameComplete(Canvas canvas, BitmapFontDrawer textDrawer, GameThread thread) throws Exception {
        try {
            drawBackDrop(canvas);
            canvas.drawARGB(150, 0, 0, 0);
            int fontSize = this.mWidth / 12;
            this.mMenuPaint.setARGB(150, 255, 255, 255);
            canvas.drawRect(0.0f, (float) 0, (float) this.mWidth, (float) (fontSize + 20 + 0), this.mMenuPaint);
            BitmapFontDrawer bitmapFontDrawer = textDrawer;
            Canvas canvas2 = canvas;
            bitmapFontDrawer.draw(canvas2, "CAMPAIGN COMPLETE!", this.mCenterX, fontSize + 10 + 0, 0, fontSize, 0, null);
            int fontSize2 = this.mWidth / 14;
            BitmapFontDrawer bitmapFontDrawer2 = textDrawer;
            Canvas canvas3 = canvas;
            bitmapFontDrawer2.draw(canvas3, GAME_COMPLETE_MESSAGE, this.mCenterX, (fontSize2 * 3) + 10 + 0, 1, fontSize2, 3, null);
            int y = textDrawer.getFinalY() + fontSize2;
            this.menuButton.get(BUTTON_GAMECOMPLETE_OK).draw(canvas, textDrawer);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawHallOfFame(Canvas canvas, GameThread thread, BitmapFontDrawer textDrawer) throws Exception {
        try {
            drawBackDrop(canvas);
            ArrayList<CareerRecord> careerRecords = thread.getCareerRecords().getHallOfFame();
            int y = this.mHallY;
            int fontSize = this.mWidth / 14;
            int headerBottom = fontSize + 20 + y;
            if (headerBottom > 0) {
                this.mMenuPaint.setARGB(150, 255, 255, 255);
                canvas.drawRect(0.0f, (float) y, (float) this.mWidth, (float) headerBottom, this.mMenuPaint);
                BitmapFontDrawer bitmapFontDrawer = textDrawer;
                Canvas canvas2 = canvas;
                bitmapFontDrawer.draw(canvas2, "CAREER RECORDS", this.mCenterX, fontSize + 10 + y, 0, fontSize, 0, null);
            }
            int y2 = y + fontSize + 20 + 5;
            int fontOffset = (this.mHallIconWidth / 2) + (fontSize / 3);
            for (int i = 0; i < careerRecords.size(); i++) {
                if (this.mHallIconWidth + y2 + 5 > 0 && y2 < this.mHeight) {
                    CareerRecord rec = careerRecords.get(i);
                    int x = (int) (((double) fontSize) * 1.5d);
                    Bitmap icon = getIconForCareerState(rec.getState());
                    this.mMenuPaint.setColor(getColourForCareerState(rec.getState()));
                    canvas.drawRect(0.0f, (float) y2, (float) this.mWidth, (float) (this.mHallIconWidth + y2 + 2), this.mMenuPaint);
                    textDrawer.draw(canvas, rec.getRankPositionString(), x, y2 + fontOffset, 1, fontSize, 2, null);
                    int x2 = x + 5;
                    if (icon != null) {
                        canvas.drawBitmap(icon, (float) x2, (float) (y2 + 1), (Paint) null);
                    }
                    BitmapFontDrawer bitmapFontDrawer2 = textDrawer;
                    Canvas canvas3 = canvas;
                    bitmapFontDrawer2.draw(canvas3, rec.getPilotName(), x2 + this.mHallIconWidth + 5, y2 + fontOffset, 1, fontSize, 1, null);
                    textDrawer.draw(canvas, rec.scoreString, this.mWidth - 5, y2 + fontOffset, 1, fontSize, 2, null);
                }
                y2 += this.mHallIconWidth + 5;
            }
            MenuButton mbOK = this.menuButton.get(BUTTON_HALL_OK);
            mbOK.setPosition(mbOK.x, y2);
            mbOK.draw(canvas, textDrawer);
            MenuButton mbClear = this.menuButton.get(BUTTON_HALL_CLEAR);
            mbClear.setPosition(mbClear.x, mbClear.height + y2);
            mbClear.draw(canvas, textDrawer);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawCareerRecord(Canvas canvas, GameThread thread, BitmapFontDrawer textDrawer) throws Exception {
        try {
            drawBackDrop(canvas);
            CareerRecord careerRecord = thread.getCareerRecords().getHallOfFame().get(this.mCareerRecordID);
            int y = this.mCareerY;
            int fontSize = this.mWidth / 12;
            int y2 = drawCareerHeader(canvas, textDrawer, y, careerRecord.getPilotName(), fontSize);
            if (y2 + fontSize + 10 > 0) {
                this.mMenuPaint.setARGB(150, 0, 0, 0);
                canvas.drawRect(0.0f, (float) y2, (float) this.mWidth, (float) (y2 + fontSize + 10), this.mMenuPaint);
                textDrawer.draw(canvas, CareerRecords.getRank(careerRecord.getRank()), this.mWidth / 2, y2 + 5 + (fontSize / 2) + (fontSize / 3), 1, fontSize, 0, null);
                Bitmap rank = this.mBMPRank[careerRecord.getRank()];
                canvas.drawBitmap(rank, 0.0f, (float) ((y2 - 2) - (rank.getHeight() / 2)), (Paint) null);
                canvas.drawBitmap(rank, (float) (this.mWidth - rank.getWidth()), (float) ((y2 - 2) - (rank.getHeight() / 2)), (Paint) null);
            }
            int y3 = y2 + fontSize + 15;
            if (this.mBMPIconWon.getHeight() + y3 > 0) {
                this.mMenuPaint.setColor(getColourForCareerState(careerRecord.getState()));
                canvas.drawRect(0.0f, (float) y3, (float) this.mWidth, (float) (this.mBMPIconWon.getHeight() + y3), this.mMenuPaint);
                Bitmap icon = getIconForCareerState(careerRecord.getState());
                if (icon != null) {
                    canvas.drawBitmap(icon, 5.0f, (float) y3, (Paint) null);
                }
                textDrawer.draw(canvas, getTextForCareerState(careerRecord.getState()), this.mBMPIconDead.getWidth() + 10, (this.mBMPIconDead.getHeight() / 2) + y3 + (fontSize / 3), 1, fontSize, 1, null);
            }
            int y4 = drawCareerLine(canvas, textDrawer, y3 + this.mBMPIconWon.getHeight() + 5, "Score:", careerRecord.scoreString, fontSize, Color.argb(150, 0, 0, 0));
            int y5 = drawCareerLine(canvas, textDrawer, y4, "Missions Completed:", careerRecord.missionSuccessesString, fontSize, Color.argb(150, 0, 0, 0));
            Canvas canvas2 = canvas;
            BitmapFontDrawer bitmapFontDrawer = textDrawer;
            int y6 = drawCareerHeader(canvas2, bitmapFontDrawer, drawCareerLine(canvas, textDrawer, y5, "Missions Failed:", careerRecord.missionFailuresString, fontSize, Color.argb(150, 0, 0, 0)), "Medals", fontSize);
            if (careerRecord.getConspicuousGallantryCross() > 0) {
                y6 = drawCareerMedals(canvas, textDrawer, "Conspicuous Gallantry", this.mBMPMedalCGC, careerRecord.getConspicuousGallantryCross(), y6, fontSize);
            }
            if (careerRecord.getDistinguishedFlyingCross() > 0) {
                y6 = drawCareerMedals(canvas, textDrawer, "Distinguished Flying", this.mBMPMedalDFC, careerRecord.getDistinguishedFlyingCross(), y6, fontSize);
            }
            if (careerRecord.getAirForceCross() > 0) {
                y6 = drawCareerMedals(canvas, textDrawer, "Air Force Cross", this.mBMPMedalAFC, careerRecord.getAirForceCross(), y6, fontSize);
            }
            if (careerRecord.getMeritoriousServiceMedal() > 0) {
                y6 = drawCareerMedals(canvas, textDrawer, "Meritorious Service", this.mBMPMedalMSM, careerRecord.getMeritoriousServiceMedal(), y6, fontSize);
            }
            if (careerRecord.getBraveryInTheAir() > 0) {
                y6 = drawCareerMedals(canvas, textDrawer, "Bravery In The Air", this.mBMPMedalBIA, careerRecord.getBraveryInTheAir(), y6, fontSize);
            }
            int y7 = drawCareerLine(canvas, textDrawer, drawCareerHeader(canvas, textDrawer, y6, "Ship Statistics", fontSize), "Enemy Kills:", careerRecord.enemyShipsKilledString, fontSize, Color.argb(150, 0, 150, 0));
            int y8 = drawCareerLine(canvas, textDrawer, y7, "Enemy Pod Kills:", careerRecord.enemyPodsKilledString, fontSize, Color.argb(150, 0, 150, 0));
            int y9 = drawCareerLine(canvas, textDrawer, y8, "Friendly Kills:", careerRecord.friendlyShipsKilledString, fontSize, Color.argb(150, 150, 0, 0));
            int y10 = drawCareerLine(canvas, textDrawer, y9, "Friendly Pod Kills:", careerRecord.friendlyPodsKilledString, fontSize, Color.argb(150, 150, 0, 0));
            int y11 = drawCareerLine(canvas, textDrawer, y10, "Ships Lost:", careerRecord.shipsLostString, fontSize, Color.argb(150, 150, 0, 0));
            int y12 = drawCareerLine(canvas, textDrawer, y11, "Pilots Lost:", careerRecord.pilotsLostString, fontSize, Color.argb(150, 150, 0, 0));
            int y13 = drawCareerLine(canvas, textDrawer, y12, "Damage Taken:", careerRecord.damageReceivedString, fontSize, Color.argb(150, 150, 0, 0));
            Canvas canvas3 = canvas;
            BitmapFontDrawer bitmapFontDrawer2 = textDrawer;
            int y14 = drawCareerHeader(canvas3, bitmapFontDrawer2, drawCareerLine(canvas, textDrawer, y13, "Ejections:", careerRecord.ejectionsString, fontSize, Color.argb(150, 150, 0, 0)), "Weapon Statistics", fontSize);
            int y15 = drawCareerLine(canvas, textDrawer, y14, "Bullets Fired:", careerRecord.bulletsFiredString, fontSize, Color.argb(150, 0, 0, 0));
            int y16 = drawCareerLine(canvas, textDrawer, y15, "Enemy Hits:", careerRecord.bulletsHitEnemyString, fontSize, Color.argb(150, 0, 150, 0));
            int y17 = drawCareerLine(canvas, textDrawer, y16, "Friendly Hits:", careerRecord.bulletsHitFriendlyString, fontSize, Color.argb(150, 150, 0, 0));
            int y18 = drawCareerLine(canvas, textDrawer, drawCareerLine(canvas, textDrawer, y17, "Accuracy:", careerRecord.bulletsAccuracyString, fontSize, Color.argb(150, 0, 0, 0)) + fontSize, "Rockets Fired:", careerRecord.rocketsFiredString, fontSize, Color.argb(150, 0, 0, 0));
            int y19 = drawCareerLine(canvas, textDrawer, y18, "Enemy Hits:", careerRecord.rocketsHitEnemyString, fontSize, Color.argb(150, 0, 150, 0));
            int y20 = drawCareerLine(canvas, textDrawer, y19, "Friendly Hits:", careerRecord.rocketsHitFriendlyString, fontSize, Color.argb(150, 150, 0, 0));
            int y21 = drawCareerLine(canvas, textDrawer, drawCareerLine(canvas, textDrawer, y20, "Accuracy:", careerRecord.rocketsAccuracyString, fontSize, Color.argb(150, 0, 0, 0)) + fontSize, "Missiles Fired:", careerRecord.missilesFiredString, fontSize, Color.argb(150, 0, 0, 0));
            int y22 = drawCareerLine(canvas, textDrawer, y21, "Enemy Hits:", careerRecord.missilesHitEnemyString, fontSize, Color.argb(150, 0, 150, 0));
            int y23 = drawCareerLine(canvas, textDrawer, y22, "Friendly Hits:", careerRecord.missilesHitFriendlyString, fontSize, Color.argb(150, 150, 0, 0));
            int y24 = drawCareerLine(canvas, textDrawer, y23, "Accuracy:", careerRecord.missilesAccuracyString, fontSize, Color.argb(150, 0, 0, 0));
            MenuButton mbOK = this.menuButton.get(50);
            mbOK.setPosition(mbOK.x, y24);
            mbOK.draw(canvas, textDrawer);
            MenuButton mbClear = this.menuButton.get(BUTTON_CAREER_DELETE);
            mbClear.setPosition(mbClear.x, mbClear.height + y24);
            mbClear.draw(canvas, textDrawer);
            this.mCareerScreenHeight = (mbOK.y2 + 5) - this.mCareerY;
        } catch (Exception e) {
            throw e;
        }
    }

    private int drawCareerHeader(Canvas canvas, BitmapFontDrawer textDrawer, int y, String heading, int fontSize) throws Exception {
        int headerBottom = fontSize + 20 + y;
        if (headerBottom > 0) {
            try {
                if (y < this.mHeight) {
                    this.mMenuPaint.setARGB(150, 255, 255, 255);
                    canvas.drawRect(0.0f, (float) y, (float) this.mWidth, (float) headerBottom, this.mMenuPaint);
                    BitmapFontDrawer bitmapFontDrawer = textDrawer;
                    Canvas canvas2 = canvas;
                    String str = heading;
                    bitmapFontDrawer.draw(canvas2, str, this.mCenterX, fontSize + 10 + y, 0, fontSize, 0, null);
                }
            } catch (Exception e) {
                throw e;
            }
        }
        return y + fontSize + 20 + 5;
    }

    private int drawCareerLine(Canvas canvas, BitmapFontDrawer textDrawer, int y, String title, String data, int fontSize, int color) throws Exception {
        if (y + fontSize + 10 > 0) {
            try {
                if (y < this.mHeight) {
                    this.mMenuPaint.setColor(color);
                    canvas.drawRect(0.0f, (float) y, (float) this.mWidth, (float) (y + fontSize + 10), this.mMenuPaint);
                    textDrawer.draw(canvas, title, 5, y + 5 + (fontSize / 2) + (fontSize / 3), 1, fontSize, 1, null);
                    textDrawer.draw(canvas, data, this.mWidth - 5, y + 5 + (fontSize / 2) + (fontSize / 3), 1, fontSize, 2, null);
                }
            } catch (Exception e) {
                throw e;
            }
        }
        return y + fontSize + 15;
    }

    private int drawCareerMedals(Canvas canvas, BitmapFontDrawer textDrawer, String header, Bitmap medal, int number, int y, int fontSize) throws Exception {
        try {
            if (y > this.mHeight) {
                return y;
            }
            int headerBottom = fontSize + 20 + y;
            if (headerBottom > 0) {
                this.mMenuPaint.setARGB(150, 0, 0, 0);
                canvas.drawRect(0.0f, (float) y, (float) this.mWidth, (float) headerBottom, this.mMenuPaint);
                BitmapFontDrawer bitmapFontDrawer = textDrawer;
                Canvas canvas2 = canvas;
                String str = header;
                bitmapFontDrawer.draw(canvas2, str, this.mCenterX, fontSize + 10 + y, 1, fontSize, 0, null);
            }
            int y2 = y + fontSize + 20 + 5;
            if (y2 > this.mHeight) {
                return y2;
            }
            int x = 5;
            this.mMenuPaint.setARGB(150, 0, 150, 0);
            canvas.drawRect(0.0f, (float) y2, (float) this.mWidth, (float) (medal.getHeight() + y2), this.mMenuPaint);
            for (int i = 0; i < number; i++) {
                canvas.drawBitmap(medal, (float) x, (float) y2, (Paint) null);
                x += medal.getWidth() + 5;
                if (medal.getWidth() + x + 5 > this.mWidth && i != number - 1) {
                    x = 5;
                    y2 += medal.getHeight() + 5;
                    canvas.drawRect(0.0f, (float) (y2 - 2), (float) this.mWidth, (float) (medal.getHeight() + y2 + 2), this.mMenuPaint);
                }
            }
            return y2 + medal.getHeight() + 5;
        } catch (Exception e) {
            throw e;
        }
    }

    private Bitmap getIconForCareerState(int state) throws Exception {
        switch (state) {
            case 0:
                return this.mBMPIconQuit;
            case 1:
                return this.mBMPIconWon;
            case 2:
                try {
                    return this.mBMPIconDead;
                } catch (Exception e) {
                    throw e;
                }
            default:
                return null;
        }
    }

    private int getColourForCareerState(int state) throws Exception {
        switch (state) {
            case 0:
                return Color.argb(150, 155, 0, 0);
            case 1:
                return Color.argb(150, 155, 155, 0);
            case 2:
                return Color.argb(150, 0, 0, 0);
            default:
                try {
                    return Color.argb(150, 0, 155, 0);
                } catch (Exception e) {
                    throw e;
                }
        }
    }

    private String getTextForCareerState(int state) throws Exception {
        switch (state) {
            case 0:
                return "Retired";
            case 1:
                return "Campaign Completed";
            case 2:
                return "Pilot Deceased";
            default:
                return "Campaign in Progress";
        }
    }

    private void handleMusic(GameThread thread) throws Exception {
        try {
            if (!(thread.getMusicID() == R.raw.musicmenu || this.mScreen == 7 || this.mScreen == 11 || this.mScreen == 12 || this.mScreen == 13)) {
                thread.setMusic(R.raw.musicmenu);
            }
            try {
                if (this.mMissionSound == null || !this.mMissionSound.isPlaying()) {
                    thread.quietMusic(false);
                } else {
                    thread.quietMusic(true);
                }
            } catch (IllegalStateException e) {
            }
        } catch (Exception e2) {
            throw e2;
        }
    }

    private void playMissionSound(Context context, GameThread thread, int soundID) throws Exception {
        try {
            stopMissionSound();
            if (thread.getSoundOn()) {
                this.mMissionSound = MediaPlayer.create(context, soundID);
                this.mMissionSound.setVolume(2.0f, 2.0f);
                this.mMissionSound.start();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void stopMissionSound() throws Exception {
        try {
            if (this.mMissionSound != null) {
                if (this.mMissionSound.isPlaying()) {
                    this.mMissionSound.stop();
                }
                this.mMissionSound.reset();
                this.mMissionSound.release();
                this.mMissionSound = null;
            }
        } catch (IllegalStateException e) {
        } catch (Exception e2) {
            throw e2;
        }
    }

    private void playInputSound(GameThread thread) throws Exception {
        try {
            if (thread.getSoundOn()) {
                this.mInputSound.start();
            }
            thread.vibrate(100);
        } catch (Exception e) {
            throw e;
        }
    }

    private void processMissionMessageScreens(Context context, GameThread thread) throws Exception {
        try {
            if (this.mScreen != 11 && this.mScreen != 12 && this.mScreen != 13) {
                return;
            }
            if (this.mPopUpDisplayed) {
                this.mPopUpDisplayed = false;
                this.mReturnMode = 5;
                return;
            }
            if (this.mScreen == 11) {
                playMissionSound(context, thread, R.raw.summarysuccessful);
                displayMessage(context.getString(R.string.popup_missionsuccess), 11);
            } else if (this.mScreen == 12) {
                playMissionSound(context, thread, R.raw.summaryfailed);
                displayMessage(context.getString(R.string.popup_missionfailed), 12);
            } else {
                playMissionSound(context, thread, R.raw.summarydead);
                displayMessage(context.getString(R.string.popup_missiondead), 12);
            }
            this.mPopUpDisplayed = true;
        } catch (Exception e) {
            throw e;
        }
    }

    public void processMissionOver(int overType, GameThread thread, SpaceController sc) throws Exception {
        try {
            CareerRecord rec = thread.getCareerRecords().getCurrent();
            MissionData md = thread.getMissionData();
            Units units = sc.getUnits();
            sc.invalidWorld = true;
            switch (overType) {
                case 1:
                    md.calculateAwards(units, thread.getCareerRecords());
                    thread.getCareerRecords().assignStats(true);
                    rec.addMissionSuccess();
                    if (md.getMissionNumber() != 2) {
                        rec.setCurrentMission(md.getMissionNumber() + 1);
                        thread.setUnlockedMission(md.getMissionNumber() + 1);
                        break;
                    } else {
                        rec.setScore(rec.getScore() + 1000);
                        rec.setState(1);
                        break;
                    }
                case 2:
                    thread.getCareerRecords().assignStats(false);
                    rec.addMissionFailure();
                    break;
                case 3:
                    md.calculateAwards(units, thread.getCareerRecords());
                    thread.getCareerRecords().assignStats(true);
                    if (md.getStatus() == 1) {
                        rec.addMissionSuccess();
                    } else {
                        rec.addMissionFailure();
                    }
                    rec.setState(2);
                    break;
            }
            thread.saveCurrentCareer();
            thread.deleteGameState();
        } catch (Exception e) {
            throw e;
        }
    }
}
