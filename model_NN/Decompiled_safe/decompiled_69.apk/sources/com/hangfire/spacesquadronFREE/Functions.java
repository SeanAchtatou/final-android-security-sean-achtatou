package com.hangfire.spacesquadronFREE;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public final class Functions {
    public static final float wrapAngle(float fltAngle) throws Exception {
        if (fltAngle >= 0.0f && fltAngle < 360.0f) {
            return fltAngle;
        }
        float wrapped = fltAngle % 360.0f;
        if (wrapped < 0.0f) {
            wrapped += 360.0f;
        }
        return wrapped;
    }

    public static final float toRad(float fltDegrees) {
        return (float) (((double) fltDegrees) * 0.0174532925d);
    }

    public static final float toDeg(float fltRad) throws Exception {
        return (float) (((double) fltRad) / 0.0174532925d);
    }

    public static final float aTanFull(float xdiff, float ydiff) throws Exception {
        if (xdiff == 0.0f) {
            xdiff = 0.001f;
        }
        if (ydiff == 0.0f) {
            ydiff = 0.001f;
        }
        try {
            float angDeg = toDeg((float) Math.atan((double) (xdiff / ydiff)));
            if (xdiff > 0.0f) {
                if (ydiff < 0.0f) {
                    return angDeg + 180.0f;
                }
                return angDeg;
            } else if (ydiff > 0.0f) {
                return angDeg + 360.0f;
            } else {
                return angDeg + 180.0f;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public static final float angDiff(float ang1, float ang2) throws Exception {
        float diff = ang1 - ang2;
        if (diff < -180.0f) {
            diff += 360.0f;
        }
        if (diff > 180.0f) {
            return diff - 360.0f;
        }
        return diff;
    }

    /* JADX INFO: Multiple debug info for r2v1 double: [D('xDiff' double), D('x' double)] */
    /* JADX INFO: Multiple debug info for r4v1 double: [D('y' double), D('yDiff' double)] */
    public static final boolean inCircle(double x, double y, double x2, double y2, int radius) throws Exception {
        double x3 = x - x2;
        try {
            if (Math.abs(x3) < ((double) radius)) {
                double yDiff = y - y2;
                if (Math.abs(yDiff) >= ((double) radius) || (x3 * x3) + (yDiff * yDiff) >= ((double) (radius * radius))) {
                    return false;
                }
                return true;
            }
            return false;
        } catch (Exception e) {
            throw e;
        }
    }

    /* JADX INFO: Multiple debug info for r4v2 float: [D('y' double), D('ydist' float)] */
    /* JADX INFO: Multiple debug info for r4v4 float: [D('ang' float), D('ydist' float)] */
    public static final boolean inRotatedBox(double x, double y, double bx, double by, int bwidth, int bheight, float bang) throws Exception {
        float xdist = (float) (x - bx);
        float ydist = (float) (y - by);
        try {
            float dist = LookUp.sqrt((xdist * xdist) + (ydist * ydist));
            float ang = wrapAngle(aTanFull(xdist, ydist) - bang);
            double x2 = ((double) (LookUp.sin(ang) * dist)) + bx;
            double y2 = ((double) (LookUp.cos(ang) * dist)) + by;
            if (y2 >= ((double) bheight) + by || y2 <= by - ((double) bheight) || x2 >= ((double) bwidth) + bx || x2 <= bx - ((double) bwidth)) {
                return false;
            }
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    /* JADX INFO: Multiple debug info for r2v2 float: [D('ang' float), D('xdist' float)] */
    public static final float inRotatedBoxProjectAway(double x, double y, double bx, double by, int bwidth, int bheight, float bang, float pAng) throws Exception {
        float xdist = (float) (x - bx);
        float ydist = (float) (y - by);
        try {
            float dist = LookUp.sqrt((xdist * xdist) + (ydist * ydist));
            float ang = wrapAngle(aTanFull(xdist, ydist) - bang);
            double x2 = ((double) (LookUp.sin(ang) * dist)) + bx;
            double y2 = ((double) (LookUp.cos(ang) * dist)) + by;
            double top = by + ((double) bheight);
            if (y2 < top) {
                double bottom = by - ((double) bheight);
                if (y2 > bottom) {
                    double right = bx + ((double) bwidth);
                    if (x2 < right) {
                        double left = bx - ((double) bwidth);
                        if (x2 > left) {
                            float pAng2 = wrapAngle(pAng - bang);
                            if (pAng2 < 90.0f) {
                                return closestEdgeDistance(x2, y2, pAng2, right, top);
                            }
                            if (pAng2 < 180.0f) {
                                return closestEdgeDistance(x2, y2, pAng2, right, bottom);
                            }
                            if (pAng2 < 270.0f) {
                                return closestEdgeDistance(x2, y2, pAng2, left, bottom);
                            }
                            return closestEdgeDistance(x2, y2, pAng2, left, top);
                        }
                    }
                }
            }
            return 0.0f;
        } catch (Exception e) {
            throw e;
        }
    }

    private static final float closestEdgeDistance(double x, double y, float ang, double edgeX, double edgeY) throws Exception {
        try {
            float dist1 = Math.abs(((float) (edgeY - y)) / LookUp.cos(ang));
            float dist2 = Math.abs(((float) (edgeX - x)) / LookUp.sin(ang));
            if (dist1 < dist2) {
                return dist1;
            }
            return dist2;
        } catch (Exception e) {
            throw e;
        }
    }

    /* JADX INFO: Multiple debug info for r2v2 float: [D('y' double), D('ydist' float)] */
    /* JADX INFO: Multiple debug info for r2v4 float: [D('ydist' float), D('distY' float)] */
    /* JADX INFO: Multiple debug info for r0v3 float: [D('xdist' float), D('x' double)] */
    /* JADX INFO: Multiple debug info for r0v5 float: [D('xdist' float), D('distX' float)] */
    private static final int closestEdge(double x, double y, float ang, int sideX, double edgeX, int sideY, double edgeY) throws Exception {
        try {
            if (Math.abs(((float) (edgeY - y)) / LookUp.cos(ang)) < Math.abs(((float) (edgeX - x)) / LookUp.sin(ang))) {
                return sideY;
            }
            return sideX;
        } catch (Exception e) {
            throw e;
        }
    }

    public static final int boxSide(double x, double y, float ang, double bx, double by, int bwidth, int bheight, float bang) throws Exception {
        float xdist = (float) (x - bx);
        float ydist = (float) (y - by);
        try {
            float dist = LookUp.sqrt((xdist * xdist) + (ydist * ydist));
            float ang2 = wrapAngle(aTanFull(xdist, ydist) - bang);
            double x2 = ((double) (LookUp.sin(ang2) * dist)) + bx;
            double y2 = ((double) (LookUp.cos(ang2) * dist)) + by;
            double top = by + ((double) bheight);
            double bottom = by - ((double) bheight);
            double right = bx + ((double) bwidth);
            double left = bx - ((double) bwidth);
            if (ang2 < 90.0f) {
                return closestEdge(x2, y2, ang2, 1, right, 2, top);
            }
            if (ang2 < 180.0f) {
                return closestEdge(x2, y2, ang2, 1, right, 3, bottom);
            }
            if (ang2 < 270.0f) {
                return closestEdge(x2, y2, ang2, 0, left, 3, bottom);
            }
            return closestEdge(x2, y2, ang2, 0, left, 2, top);
        } catch (Exception e) {
            throw e;
        }
    }

    /* JADX INFO: Multiple debug info for r10v2 double: [D('x1' double), D('x' double)] */
    /* JADX INFO: Multiple debug info for r12v1 double: [D('y' double), D('y1' double)] */
    /* JADX INFO: Multiple debug info for r10v3 double: [D('x' double), D('xdist' double)] */
    /* JADX INFO: Multiple debug info for r12v2 double: [D('y' double), D('ydist' double)] */
    /* JADX INFO: Multiple debug info for r10v5 double: [D('dist' double), D('xdist' double)] */
    public static final boolean lineIntersectsCircle(double x1, double y1, double x2, double y2, double cx, double cy, float radius) throws Exception {
        if (x1 > x2) {
            if (cx - ((double) radius) > x1 || ((double) radius) + cx < x2) {
                return false;
            }
        } else if (cx - ((double) radius) > x2 || ((double) radius) + cx < x1) {
            return false;
        }
        if (y1 > y2) {
            if (cy - ((double) radius) > y1 || ((double) radius) + cy < y2) {
                return false;
            }
        } else if (cy - ((double) radius) > y2 || ((double) radius) + cy < y1) {
            return false;
        }
        double u = (((cx - x1) * (x2 - x1)) + ((cy - y1) * (y2 - y1))) / (((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
        if (u < 0.0d || u > 1.0d) {
            return false;
        }
        double xdist = (x1 + ((x2 - x1) * u)) - cx;
        double ydist = (y1 + ((y2 - y1) * u)) - cy;
        if ((xdist * xdist) + (ydist * ydist) < ((double) (radius * radius))) {
            return true;
        }
        return false;
    }

    /* JADX INFO: Multiple debug info for r11v1 double: [D('line1_yEnd' double), D('line1B' double)] */
    /* JADX INFO: Multiple debug info for r19v1 double: [D('line2A' double), D('line2_yEnd' double)] */
    /* JADX INFO: Multiple debug info for r17v1 double: [D('line2B' double), D('line2_xEnd' double)] */
    /* JADX INFO: Multiple debug info for r13v2 double: [D('line2_xStart' double), D('line2C' double)] */
    /* JADX INFO: Multiple debug info for r7v4 double: [D('det' double), D('line1_yStart' double)] */
    /* JADX INFO: Multiple debug info for r11v4 double: [D('intersectX' double), D('line1B' double)] */
    /* JADX INFO: Multiple debug info for r7v5 double: [D('det' double), D('intersectY' double)] */
    public static final double[] linesIntersect(double line1_xStart, double line1_yStart, double line1_xEnd, double line1_yEnd, double line2_xStart, double line2_yStart, double line2_xEnd, double line2_yEnd) throws Exception {
        try {
            double[] returnCoords = new double[3];
            double line1A = line1_yEnd - line1_yStart;
            double line1B = line1_xStart - line1_xEnd;
            double line1C = (line1A * line1_xStart) + (line1_yStart * line1B);
            double line2_yEnd2 = line2_yEnd - line2_yStart;
            double line2_xEnd2 = line2_xStart - line2_xEnd;
            double line2C = (line2_xEnd2 * line2_yStart) + (line2_yEnd2 * line2_xStart);
            double line1_yStart2 = (line1A * line2_xEnd2) - (line2_yEnd2 * line1B);
            if (line1_yStart2 != 0.0d) {
                double line1B2 = ((line2_xEnd2 * line1C) - (line1B * line2C)) / line1_yStart2;
                double intersectY = ((line2C * line1A) - (line2_yEnd2 * line1C)) / line1_yStart2;
                if ((line1_xEnd > line1_xStart && line1B2 >= line1_xStart && line1B2 <= line1_xEnd) || (line1_xEnd < line1_xStart && line1B2 >= line1_xEnd && line1B2 <= line1_xStart)) {
                    returnCoords[0] = 1.0d;
                    returnCoords[1] = line1B2;
                    returnCoords[2] = intersectY;
                    return returnCoords;
                }
            }
            returnCoords[0] = 0.0d;
            returnCoords[1] = 0.0d;
            returnCoords[2] = 0.0d;
            return returnCoords;
        } catch (Exception e) {
            throw e;
        }
    }

    public static final Drawable getZoomedOutDrawable(Resources res, int id) throws Exception {
        try {
            Bitmap full = BitmapFactory.decodeResource(res, id);
            return new BitmapDrawable(Bitmap.createScaledBitmap(full, full.getWidth() / 2, full.getHeight() / 2, true));
        } catch (Exception e) {
            throw e;
        }
    }

    public static final Bitmap getZoomedOutBitmap(Resources res, int id, int factor) throws Exception {
        try {
            Bitmap full = BitmapFactory.decodeResource(res, id);
            return Bitmap.createScaledBitmap(full, full.getWidth() / factor, full.getHeight() / factor, true);
        } catch (Exception e) {
            throw e;
        }
    }

    public static final String toStr(boolean value) {
        if (value) {
            return "1";
        }
        return "0";
    }

    public static final boolean toBoolean(String value) {
        if (value.equals("1")) {
            return true;
        }
        return false;
    }
}
