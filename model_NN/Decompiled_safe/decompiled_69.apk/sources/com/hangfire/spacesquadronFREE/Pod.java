package com.hangfire.spacesquadronFREE;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public final class Pod {
    public double dblWorldX;
    public double dblWorldY;
    private float fltScreenDestX;
    private float fltScreenDestY;
    private float fltScreenX;
    private float fltScreenY;
    private boolean inScreen;
    public int intColRadius;
    private int intHalfVisRadius;
    private int intScreenX;
    private int intScreenY;
    private int intVisualRadius;
    public boolean podAlive = true;
    public boolean podLaunched = false;
    public boolean podSafe = false;
    private int smokeCounter = 3;
    public Unit source;
    public float speed;
    private float speedX;
    private float speedY;
    public float travelAng;

    public Pod(Unit u) throws Exception {
        try {
            this.intVisualRadius = 6;
            this.intHalfVisRadius = 3;
            this.intColRadius = 5;
            this.speed = 3.0f;
            this.source = u;
        } catch (Exception e) {
            throw e;
        }
    }

    public void launch(Map map, Sounds sounds, Screen screen, GameThread thread) throws Exception {
        try {
            this.dblWorldX = this.source.fltWorldX;
            this.dblWorldY = this.source.fltWorldY;
            float xdiff = (float) (this.dblWorldX - ((double) (map.worldSizeX / 2)));
            float ydiff = (float) (this.dblWorldY - ((double) (map.worldSizeY / 2)));
            if (Math.abs(xdiff) > Math.abs(ydiff)) {
                if (xdiff > 0.0f) {
                    this.travelAng = (float) (45.0d + (Math.random() * 90.0d));
                } else {
                    this.travelAng = (float) (225.0d + (Math.random() * 90.0d));
                }
            } else if (ydiff > 0.0f) {
                this.travelAng = Functions.wrapAngle((float) (315.0d + (Math.random() * 90.0d)));
            } else {
                this.travelAng = (float) (135.0d + (Math.random() * 90.0d));
            }
            this.speedX = LookUp.sin(this.travelAng) * this.speed;
            this.speedY = LookUp.cos(this.travelAng) * this.speed;
            this.podLaunched = true;
            if (this.source.isPlayable) {
                thread.getCareerRecords().getStats().addEjections(1);
            }
            sounds.playSound(3, this.dblWorldX, this.dblWorldY, screen, thread);
        } catch (Exception e) {
            throw e;
        }
    }

    public void setSpeed(float s) throws Exception {
        try {
            this.speed = s;
            this.speedX = LookUp.sin(this.travelAng) * s;
            this.speedY = LookUp.cos(this.travelAng) * s;
        } catch (Exception e) {
            throw e;
        }
    }

    public void addSpeed(float xspd, float yspd) throws Exception {
        try {
            this.speedX += xspd;
            this.speedY += yspd;
            this.speed = LookUp.sqrt((this.speedX * this.speedX) + (this.speedY * this.speedY));
            this.travelAng = Functions.aTanFull(this.speedX, this.speedY);
        } catch (Exception e) {
            throw e;
        }
    }

    public void projectAway(double midX, double midY, float projDist) throws Exception {
        try {
            float xdiff = (float) (midX - this.dblWorldX);
            float ydiff = (float) (midY - this.dblWorldY);
            float ratio = projDist / LookUp.sqrt((xdiff * xdiff) + (ydiff * ydiff));
            this.dblWorldX = midX - ((double) (xdiff * ratio));
            this.dblWorldY = midY - ((double) (ydiff * ratio));
        } catch (Exception e) {
            throw e;
        }
    }

    public void move(Map map, Particles part) throws Exception {
        try {
            this.dblWorldX += (double) this.speedX;
            this.dblWorldY += (double) this.speedY;
            if (this.dblWorldX < 0.0d) {
                this.podSafe = true;
            }
            if (this.dblWorldX > ((double) map.worldSizeX)) {
                this.podSafe = true;
            }
            if (this.dblWorldY < 0.0d) {
                this.podSafe = true;
            }
            if (this.dblWorldY > ((double) map.worldSizeY)) {
                this.podSafe = true;
            }
            this.smokeCounter--;
            if (this.smokeCounter == 0) {
                part.makeRocketSmoke(this.dblWorldX, this.dblWorldY, this.travelAng, -5.0f);
                this.smokeCounter = 3;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void calculateScreenPos(Screen screen) throws Exception {
        try {
            this.fltScreenX = screen.canvasX((double) ((float) this.dblWorldX));
            this.fltScreenY = screen.canvasY((double) ((float) this.dblWorldY));
            this.intScreenX = (int) this.fltScreenX;
            this.intScreenY = (int) this.fltScreenY;
            this.inScreen = screen.inScreenWorldValues(this.dblWorldX, this.dblWorldY, this.intVisualRadius, this.intVisualRadius);
        } catch (Exception e) {
            throw e;
        }
    }

    public void draw(Canvas canvas, Bitmap img) throws Exception {
        try {
            if (this.inScreen) {
                canvas.save();
                canvas.rotate(this.travelAng, this.fltScreenX, this.fltScreenY);
                canvas.drawBitmap(img, (float) (this.intScreenX - (img.getWidth() / 2)), (float) (this.intScreenY - (img.getHeight() / 2)), (Paint) null);
                canvas.restore();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void drawArrowLine(Canvas canvas, NavArrow navArrow, Screen screen) throws Exception {
        boolean z;
        try {
            if (this.inScreen) {
                int i = this.intScreenX;
                int i2 = this.intScreenY;
                float f = this.travelAng;
                float f2 = this.speed;
                if (this.source.team != 0) {
                    z = true;
                } else {
                    z = false;
                }
                navArrow.drawLine(canvas, i, i2, f, f2, 0.0f, screen, z, false, this.source.team);
                this.fltScreenDestX = navArrow.fltX;
                this.fltScreenDestY = navArrow.fltY;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void drawArrowHead(Canvas canvas, NavArrow navArrow) throws Exception {
        try {
            if (this.inScreen) {
                navArrow.drawHead(canvas, this.fltScreenDestX, this.fltScreenDestY, this.travelAng, this.source.team, false);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public String getSaveString() throws Exception {
        try {
            return String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf((int) this.dblWorldX)) + "C" + String.valueOf((int) this.dblWorldY)) + "C" + String.valueOf((int) (this.speedX * 1000.0f))) + "C" + String.valueOf((int) (this.speedY * 1000.0f))) + "C" + String.valueOf((int) this.travelAng)) + "C" + String.valueOf((int) (this.speed * 1000.0f))) + "C" + String.valueOf((int) this.fltScreenX)) + "C" + String.valueOf((int) this.fltScreenY)) + "C" + String.valueOf(this.intScreenX)) + "C" + String.valueOf(this.intScreenY)) + "C" + Functions.toStr(this.inScreen)) + "C" + String.valueOf(this.intVisualRadius)) + "C" + String.valueOf(this.intHalfVisRadius)) + "C" + String.valueOf(this.intColRadius)) + "C" + String.valueOf((int) this.fltScreenDestX)) + "C" + String.valueOf((int) this.fltScreenDestY)) + "C" + Functions.toStr(this.podAlive)) + "C" + Functions.toStr(this.podSafe)) + "C" + Functions.toStr(this.podLaunched);
        } catch (Exception e) {
            throw e;
        }
    }

    public void restoreFromSaveString(String saveString) throws Exception {
        try {
            String[] data = saveString.split("C");
            int pos = 0 + 1;
            this.dblWorldX = (double) Integer.parseInt(data[0]);
            int pos2 = pos + 1;
            this.dblWorldY = (double) Integer.parseInt(data[pos]);
            int pos3 = pos2 + 1;
            this.speedX = ((float) Integer.parseInt(data[pos2])) / 1000.0f;
            int pos4 = pos3 + 1;
            this.speedY = ((float) Integer.parseInt(data[pos3])) / 1000.0f;
            int pos5 = pos4 + 1;
            this.travelAng = (float) Integer.parseInt(data[pos4]);
            int pos6 = pos5 + 1;
            this.speed = ((float) Integer.parseInt(data[pos5])) / 1000.0f;
            int pos7 = pos6 + 1;
            this.fltScreenX = (float) Integer.parseInt(data[pos6]);
            int pos8 = pos7 + 1;
            this.fltScreenY = (float) Integer.parseInt(data[pos7]);
            int pos9 = pos8 + 1;
            this.intScreenX = Integer.parseInt(data[pos8]);
            int pos10 = pos9 + 1;
            this.intScreenY = Integer.parseInt(data[pos9]);
            int pos11 = pos10 + 1;
            this.inScreen = Functions.toBoolean(data[pos10]);
            int pos12 = pos11 + 1;
            this.intVisualRadius = Integer.parseInt(data[pos11]);
            int pos13 = pos12 + 1;
            this.intHalfVisRadius = Integer.parseInt(data[pos12]);
            int pos14 = pos13 + 1;
            this.intColRadius = Integer.parseInt(data[pos13]);
            int pos15 = pos14 + 1;
            this.fltScreenDestX = (float) Integer.parseInt(data[pos14]);
            int pos16 = pos15 + 1;
            this.fltScreenDestY = (float) Integer.parseInt(data[pos15]);
            int pos17 = pos16 + 1;
            this.podAlive = Functions.toBoolean(data[pos16]);
            int pos18 = pos17 + 1;
            this.podSafe = Functions.toBoolean(data[pos17]);
            int i = pos18 + 1;
            this.podLaunched = Functions.toBoolean(data[pos18]);
        } catch (Exception e) {
            throw e;
        }
    }
}
