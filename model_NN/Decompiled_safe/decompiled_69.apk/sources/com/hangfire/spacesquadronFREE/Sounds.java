package com.hangfire.spacesquadronFREE;

import android.content.Context;
import android.media.SoundPool;

public final class Sounds {
    public static final int NUM_SIMULTANEOUS_SOUNDS = 6;
    public static final int NUM_SOUNDS = 11;
    public static final int SND_BULLETHITMETAL = 6;
    public static final int SND_CONTACTWARNING = 10;
    public static final int SND_EJECTWARNING = 8;
    public static final int SND_ESCAPEPOD = 3;
    public static final int SND_EXPLOSION = 4;
    public static final int SND_EXPLOSIONBIG = 5;
    public static final int SND_GUN = 0;
    public static final int SND_MISSILE = 2;
    public static final int SND_MISSILELOCK = 7;
    public static final int SND_PILOTDEAD = 9;
    public static final int SND_ROCKET = 1;
    private Sound[] mSound;
    private SoundPool mSoundPool;

    public class Sound {
        public int delay;
        public int id;
        public int maxDelay;
        public float random = 0.1f;
        public float volume = 1.0f;

        public Sound() {
        }
    }

    public Sounds(Context context) throws Exception {
        try {
            this.mSound = new Sound[11];
            this.mSoundPool = new SoundPool(6, 3, 0);
            addSound(context, 0, R.raw.fxgun, 9, 1.0f, 0.1f);
            addSound(context, 1, R.raw.fxrocket, 14, 1.0f, 0.1f);
            addSound(context, 2, R.raw.fxmissile, 14, 1.0f, 0.1f);
            addSound(context, 3, R.raw.fxpodlaunch, 10, 1.0f, 0.1f);
            addSound(context, 4, R.raw.fxexplosion, 5, 1.0f, 0.2f);
            addSound(context, 5, R.raw.fxexplosionbig, 5, 1.0f, 0.1f);
            addSound(context, 6, R.raw.fxbullethit, 5, 1.0f, 0.2f);
            addSound(context, 7, R.raw.fxmissilelock, 5, 1.0f, 0.0f);
            addSound(context, 8, R.raw.fxejectwarning, 5, 1.0f, 0.0f);
            addSound(context, 9, R.raw.fxpilotkilled, 5, 1.0f, 0.0f);
            addSound(context, 10, R.raw.fxcontact, 5, 1.0f, 0.0f);
        } catch (Exception e) {
            throw e;
        }
    }

    public void destroy(GameThread thread) throws Exception {
        try {
            if (thread.getSoundSafe()) {
                for (int i = 0; i < 11; i++) {
                    this.mSoundPool.unload(this.mSound[i].id);
                }
                this.mSoundPool.release();
            }
            this.mSoundPool = null;
        } catch (Exception e) {
            throw e;
        }
    }

    private void addSound(Context context, int index, int res, int delay, float volume, float random) throws Exception {
        try {
            this.mSound[index] = new Sound();
            this.mSound[index].id = this.mSoundPool.load(context, res, 1);
            this.mSound[index].maxDelay = delay;
            this.mSound[index].delay = 0;
            this.mSound[index].volume = volume;
            this.mSound[index].random = random;
        } catch (Exception e) {
            throw e;
        }
    }

    public float playSound(int snd, double x, double y, Screen screen, GameThread thread) throws Exception {
        float volume;
        try {
            if (!thread.getSoundOn()) {
                return 0.0f;
            }
            if (this.mSound[snd].delay != 0) {
                return 0.0f;
            }
            float absXDiff = Math.abs((float) (x - ((double) screen.intMiddleX)));
            if (absXDiff > ((float) (screen.intScreenWidth * 2))) {
                return 0.0f;
            }
            float absYDiff = Math.abs((float) (y - ((double) screen.intMiddleY)));
            if (absYDiff > ((float) (screen.intScreenHeight * 2))) {
                return 0.0f;
            }
            if (absXDiff > absYDiff) {
                volume = 1.0f - ((absXDiff / ((float) (screen.intScreenWidth * 2))) * this.mSound[snd].volume);
            } else {
                volume = 1.0f - ((absYDiff / ((float) (screen.intScreenHeight * 2))) * this.mSound[snd].volume);
            }
            float rate = (float) (((double) (1.0f - this.mSound[snd].random)) + (Math.random() * ((double) this.mSound[snd].random) * 2.0d));
            this.mSound[snd].delay = this.mSound[snd].maxDelay;
            this.mSoundPool.play(this.mSound[snd].id, volume, volume, 1, 0, rate);
            return volume;
        } catch (Exception e) {
            throw e;
        }
    }

    public void processSounds() throws Exception {
        int i = 0;
        while (i < 11) {
            try {
                if (this.mSound[i].delay > 0) {
                    this.mSound[i].delay--;
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
    }
}
