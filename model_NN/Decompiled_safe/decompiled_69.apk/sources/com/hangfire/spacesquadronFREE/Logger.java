package com.hangfire.spacesquadronFREE;

import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Paint;

public final class Logger {
    private static final String LOG_PREF = "LOG_PREF";
    public static final int NUM_ENTRIES = 28;
    private static final String SPLIT_SEQUENCE = "-SPLIT-";
    private String[] entries = new String[28];
    public boolean logging = true;
    private SharedPreferences mSP;

    public Logger(SharedPreferences sp) {
        this.mSP = sp;
        for (int i = 0; i < 28; i++) {
            this.entries[i] = get(i);
        }
    }

    public void log(String data) {
        if (this.logging) {
            for (int i = 27; i > 0; i--) {
                this.entries[i] = this.entries[i - 1];
            }
            this.entries[0] = data;
            String encoded = "";
            for (int i2 = 0; i2 < 28; i2++) {
                encoded = String.valueOf(encoded) + this.entries[i2] + SPLIT_SEQUENCE;
            }
            SharedPreferences.Editor ed = this.mSP.edit();
            ed.putString(LOG_PREF, encoded);
            ed.commit();
        }
    }

    public void clear() {
        this.entries = new String[28];
        SharedPreferences.Editor ed = this.mSP.edit();
        ed.remove(LOG_PREF);
        ed.commit();
    }

    private String get(int entry) {
        String[] split = this.mSP.getString(LOG_PREF, "").split(SPLIT_SEQUENCE);
        if (entry >= split.length) {
            return "";
        }
        return split[entry];
    }

    public void print(Canvas canvas) {
        canvas.drawARGB(255, 0, 0, 0);
        Paint logPaint = new Paint();
        logPaint.setTextSize(10.0f);
        logPaint.setARGB(255, 255, 255, 255);
        for (int i = 0; i < 28; i++) {
            canvas.drawText(this.entries[i], 0.0f, (float) ((i * 15) + 15), logPaint);
        }
    }
}
