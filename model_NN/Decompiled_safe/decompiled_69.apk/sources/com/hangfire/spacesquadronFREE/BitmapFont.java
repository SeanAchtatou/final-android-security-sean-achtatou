package com.hangfire.spacesquadronFREE;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public final class BitmapFont {
    private static final int ARRAY_AT = 72;
    private static final int ARRAY_BACKSLASH = 73;
    private static final int ARRAY_CLOSEBRACKET = 66;
    private static final int ARRAY_COLON = 71;
    private static final int ARRAY_COMMA = 75;
    private static final int ARRAY_EQUALS = 70;
    private static final int ARRAY_EXCLAIMATION = 62;
    private static final int ARRAY_FORWARDSLASH = 78;
    private static final int ARRAY_FULLSTOP = 77;
    private static final int ARRAY_LARGERTHAN = 76;
    private static final int ARRAY_LOWERCASE_START = 26;
    private static final int ARRAY_MINUS = 67;
    private static final int ARRAY_NUMBERS_START = 52;
    private static final int ARRAY_OPENBRACKET = 65;
    private static final int ARRAY_PERCENT = 64;
    private static final int ARRAY_PLUS = 69;
    private static final int ARRAY_QUESTIONMARK = 79;
    private static final int ARRAY_QUOTES = 63;
    private static final int ARRAY_SMALLERTHAN = 74;
    private static final int ARRAY_UNDERSCORE = 68;
    private static final int ARRAY_UPPERCASE_START = 0;
    private static final int CHARCODE_AT = 64;
    private static final int CHARCODE_BACKSLASH = 92;
    private static final int CHARCODE_CLOSEBRACKET = 41;
    private static final int CHARCODE_COLON = 58;
    private static final int CHARCODE_COMMA = 44;
    private static final int CHARCODE_EQUALS = 61;
    private static final int CHARCODE_EXCLAIMATION = 33;
    private static final int CHARCODE_FORWARDSLASH = 47;
    private static final int CHARCODE_FULLSTOP = 46;
    private static final int CHARCODE_LARGERTHAN = 62;
    private static final int CHARCODE_LOWERCASE_START = 97;
    private static final int CHARCODE_MINUS = 45;
    private static final int CHARCODE_NUMBERS_START = 48;
    private static final int CHARCODE_OPENBRACKET = 40;
    private static final int CHARCODE_PERCENT = 37;
    private static final int CHARCODE_PLUS = 43;
    private static final int CHARCODE_QUESTIONMARK = 63;
    private static final int CHARCODE_QUOTES = 168;
    private static final int CHARCODE_SMALLERTHAN = 60;
    private static final int CHARCODE_UNDERSCORE = 95;
    private static final int CHARCODE_UPPERCASE_START = 65;
    private static final int NUM_CACHED_CHARACTERS = 100;
    private static final int NUM_CHARACTERS_PER_FONT = 80;
    private BitmapFontCharacter[] mCharacter = new BitmapFontCharacter[NUM_CACHED_CHARACTERS];
    private int mCharacterPointer = 0;
    private int mFullSpaceWidth = 0;
    private float mFullsizeHeight = 0.0f;
    private Bitmap[] mImage = new Bitmap[NUM_CHARACTERS_PER_FONT];
    private int mOffsetLowercase = 0;
    private int mOffsetNumbersAndSpecial = 0;
    private int mOffsetUppercase = 0;

    private final class BitmapFontCharacter {
        public char charCode;
        public Bitmap image;
        public float size;

        private BitmapFontCharacter() {
        }

        /* synthetic */ BitmapFontCharacter(BitmapFont bitmapFont, BitmapFontCharacter bitmapFontCharacter) {
            this();
        }
    }

    public BitmapFont(Bitmap characterMap, int colour) throws Exception {
        try {
            prepareCharacters(characterMap, colour);
            this.mOffsetUppercase = getYForPixelColour(characterMap, Color.argb(255, 0, 0, 0), 0, 0);
            this.mOffsetLowercase = getYForPixelColour(characterMap, Color.argb(255, 0, 0, 0), 0, 1) - getYForPixelColour(characterMap, Color.argb(255, 255, 0, 255), 1, 1);
            this.mOffsetNumbersAndSpecial = getYForPixelColour(characterMap, Color.argb(255, 0, 0, 0), 0, 2) - getYForPixelColour(characterMap, Color.argb(255, 255, 0, 255), 1, 2);
            this.mFullsizeHeight = (float) this.mImage[0].getHeight();
            if (((float) this.mImage[26].getHeight()) > this.mFullsizeHeight) {
                this.mFullsizeHeight = (float) this.mImage[26].getHeight();
            }
            if (((float) this.mImage[ARRAY_NUMBERS_START].getHeight()) > this.mFullsizeHeight) {
                this.mFullsizeHeight = (float) this.mImage[ARRAY_NUMBERS_START].getHeight();
            }
            this.mFullSpaceWidth = this.mImage[0].getWidth() / 2;
            for (int i = 0; i < NUM_CACHED_CHARACTERS; i++) {
                this.mCharacter[i] = new BitmapFontCharacter(this, null);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void drawString(Canvas canvas, String text, int x, int y, int size, int align, Paint paint) throws Exception {
        try {
            float resizeRatio = ((float) size) / this.mFullsizeHeight;
            int padding = (int) (4.0f * resizeRatio);
            if (padding < 1) {
                padding = 1;
            }
            int spaceWidth = (int) (((float) this.mFullSpaceWidth) * resizeRatio);
            int totalWidth = getStringWidth(text, size);
            switch (align) {
                case 0:
                    x -= totalWidth / 2;
                    break;
                case 2:
                    x -= totalWidth;
                    break;
            }
            for (int i = 0; i < text.length(); i++) {
                char c = text.charAt(i);
                Bitmap image = getCachedBitmap(c, (float) size, resizeRatio);
                if (image != null) {
                    float f = (float) x;
                    canvas.drawBitmap(image, f, (float) (y - ((int) (((float) getFullOffsetForChar(c)) * resizeRatio))), paint);
                    x += image.getWidth() + padding;
                } else {
                    x += spaceWidth;
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public int getStringWidth(String text, int size) throws Exception {
        try {
            float resizeRatio = ((float) size) / this.mFullsizeHeight;
            int padding = (int) (4.0f * resizeRatio);
            if (padding < 1) {
                padding = 1;
            }
            int spaceWidth = (int) (((float) this.mFullSpaceWidth) * resizeRatio);
            int totalWidth = 0;
            for (int i = 0; i < text.length(); i++) {
                Bitmap image = getBitmapForChar(text.charAt(i));
                if (image != null) {
                    totalWidth += ((int) (((float) image.getWidth()) * resizeRatio)) + padding;
                } else {
                    totalWidth += spaceWidth;
                }
            }
            return totalWidth;
        } catch (Exception e) {
            throw e;
        }
    }

    private Bitmap getCachedBitmap(char c, float size, float resizeRatio) throws Exception {
        for (int i = 0; i < NUM_CACHED_CHARACTERS; i++) {
            if (this.mCharacter[i].image != null && this.mCharacter[i].size == size && this.mCharacter[i].charCode == c) {
                return this.mCharacter[i].image;
            }
        }
        try {
            Bitmap image = getBitmapForChar(c);
            if (image == null) {
                return null;
            }
            Bitmap imageScaled = Bitmap.createScaledBitmap(image, (int) (((float) image.getWidth()) * resizeRatio), (int) (((float) image.getHeight()) * resizeRatio), true);
            if (this.mCharacter[this.mCharacterPointer].image != null) {
                this.mCharacter[this.mCharacterPointer].image.recycle();
            }
            this.mCharacter[this.mCharacterPointer].image = imageScaled;
            this.mCharacter[this.mCharacterPointer].charCode = c;
            this.mCharacter[this.mCharacterPointer].size = size;
            this.mCharacterPointer++;
            if (this.mCharacterPointer >= NUM_CACHED_CHARACTERS) {
                this.mCharacterPointer = 0;
            }
            return imageScaled;
        } catch (Exception e) {
            throw e;
        }
    }

    private Bitmap getBitmapForChar(char c) throws Exception {
        int val = c;
        if (val >= 65 && val <= 90) {
            try {
                return this.mImage[(val + 0) - 65];
            } catch (Exception e) {
                throw e;
            }
        } else if (val >= CHARCODE_LOWERCASE_START && val <= 122) {
            return this.mImage[(val + 26) - CHARCODE_LOWERCASE_START];
        } else {
            if (val >= CHARCODE_NUMBERS_START && val <= 57) {
                return this.mImage[(val + ARRAY_NUMBERS_START) - CHARCODE_NUMBERS_START];
            }
            switch (val) {
                case CHARCODE_EXCLAIMATION /*33*/:
                    return this.mImage[62];
                case CHARCODE_PERCENT /*37*/:
                    return this.mImage[64];
                case 40:
                    return this.mImage[65];
                case CHARCODE_CLOSEBRACKET /*41*/:
                    return this.mImage[ARRAY_CLOSEBRACKET];
                case CHARCODE_PLUS /*43*/:
                    return this.mImage[ARRAY_PLUS];
                case CHARCODE_COMMA /*44*/:
                    return this.mImage[75];
                case CHARCODE_MINUS /*45*/:
                    return this.mImage[ARRAY_MINUS];
                case CHARCODE_FULLSTOP /*46*/:
                    return this.mImage[ARRAY_FULLSTOP];
                case CHARCODE_FORWARDSLASH /*47*/:
                    return this.mImage[ARRAY_FORWARDSLASH];
                case CHARCODE_COLON /*58*/:
                    return this.mImage[ARRAY_COLON];
                case 60:
                    return this.mImage[ARRAY_SMALLERTHAN];
                case CHARCODE_EQUALS /*61*/:
                    return this.mImage[ARRAY_EQUALS];
                case 62:
                    return this.mImage[ARRAY_LARGERTHAN];
                case 63:
                    return this.mImage[ARRAY_QUESTIONMARK];
                case 64:
                    return this.mImage[ARRAY_AT];
                case CHARCODE_BACKSLASH /*92*/:
                    return this.mImage[ARRAY_BACKSLASH];
                case CHARCODE_UNDERSCORE /*95*/:
                    return this.mImage[ARRAY_UNDERSCORE];
                case CHARCODE_QUOTES /*168*/:
                    return this.mImage[63];
                default:
                    return null;
            }
        }
    }

    private int getFullOffsetForChar(char c) throws Exception {
        int val = c;
        if (val >= 65 && val <= 90) {
            try {
                return this.mOffsetUppercase;
            } catch (Exception e) {
                throw e;
            }
        } else if (val < CHARCODE_LOWERCASE_START || val > 122) {
            return this.mOffsetNumbersAndSpecial;
        } else {
            return this.mOffsetLowercase;
        }
    }

    private void prepareCharacters(Bitmap characterMap, int colour) throws Exception {
        int startX;
        int endX;
        int row = -1;
        int findCharXOffset = 0;
        int startY = 0;
        int endY = 0;
        for (int i = 0; i < NUM_CHARACTERS_PER_FONT; i++) {
            if (i == 0 || i == 26 || i == ARRAY_NUMBERS_START) {
                row++;
                try {
                    startY = getYForPixelColour(characterMap, Color.argb(255, 255, 0, 255), 1, row) + 1;
                    endY = getYForPixelColour(characterMap, Color.argb(255, 255, 0, 255), 1, row + 1) - 1;
                    findCharXOffset = 0;
                } catch (Exception e) {
                    throw e;
                }
            }
            if (findCharXOffset == 0) {
                startX = 1;
                endX = getVerticalBlankLineX(characterMap, startY, endY, 0, true, 2);
            } else {
                startX = getVerticalBlankLineX(characterMap, startY, endY, 0, false, findCharXOffset);
                endX = getVerticalBlankLineX(characterMap, startY, endY, 0, true, startX + 1);
            }
            findCharXOffset = endX;
            Bitmap newBitmap = Bitmap.createBitmap(characterMap, startX, startY, endX - startX, endY - startY);
            if (colour != -1) {
                int red = Color.red(colour);
                int green = Color.green(colour);
                int blue = Color.blue(colour);
                for (int x = 0; x < newBitmap.getWidth(); x++) {
                    for (int y = 0; y < newBitmap.getHeight(); y++) {
                        newBitmap.setPixel(x, y, Color.argb(Color.alpha(newBitmap.getPixel(x, y)), red, green, blue));
                    }
                }
            }
            this.mImage[i] = newBitmap;
        }
    }

    private int getYForPixelColour(Bitmap characterMap, int findColour, int x, int index) throws Exception {
        int instance = 0;
        int y = 0;
        while (y < characterMap.getHeight()) {
            try {
                if (characterMap.getPixel(x, y) == findColour) {
                    if (instance == index) {
                        return y;
                    }
                    instance++;
                }
                y++;
            } catch (Exception e) {
                throw e;
            }
        }
        return 0;
    }

    private int getVerticalBlankLineX(Bitmap characterMap, int startY, int endY, int index, boolean leftSide, int startX) throws Exception {
        int instance = 0;
        int lineBlockStart = -1;
        int x = startX;
        while (x < characterMap.getWidth()) {
            try {
                boolean lineFound = true;
                int y = startY + 1;
                while (true) {
                    if (y >= endY - 1) {
                        break;
                    } else if (Color.alpha(characterMap.getPixel(x, y)) != 0) {
                        lineFound = false;
                        break;
                    } else {
                        y++;
                    }
                }
                if (lineFound) {
                    if (lineBlockStart == -1) {
                        lineBlockStart = x;
                    }
                } else if (lineBlockStart == -1) {
                    continue;
                } else {
                    if (x - lineBlockStart > 1) {
                        if (instance != index) {
                            instance++;
                        } else if (leftSide) {
                            return lineBlockStart;
                        } else {
                            return x;
                        }
                    }
                    lineBlockStart = -1;
                }
                x++;
            } catch (Exception e) {
                throw e;
            }
        }
        return characterMap.getWidth() - 1;
    }
}
