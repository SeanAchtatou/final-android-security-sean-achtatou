package com.hangfire.spacesquadronFREE;

public class CareerRecord {
    private int airForceCross = 0;
    private int braveryInTheAir = 0;
    public String bulletsAccuracyString = "0%";
    private int bulletsFired = 0;
    public String bulletsFiredString = "0";
    private int bulletsHitEnemy = 0;
    public String bulletsHitEnemyString = "0";
    private int bulletsHitFriendly = 0;
    public String bulletsHitFriendlyString = "0";
    private int conspicuousGallantryCross = 0;
    private int currentMission = 1;
    private int damageReceived = 0;
    public String damageReceivedString = "0";
    private int distinguishedFlyingCross = 0;
    private int ejections = 0;
    public String ejectionsString = "0";
    private int enemyPodsKilled = 0;
    public String enemyPodsKilledString = "0";
    private int enemyShipsKilled = 0;
    public String enemyShipsKilledString = "0";
    private int friendlyPodsKilled = 0;
    public String friendlyPodsKilledString = "0";
    private int friendlyShipsKilled = 0;
    public String friendlyShipsKilledString = "0";
    private int meritoriousServiceMedal = 0;
    public String missilesAccuracyString = "0%";
    private int missilesFired = 0;
    public String missilesFiredString = "0";
    private int missilesHitEnemy = 0;
    public String missilesHitEnemyString = "0";
    private int missilesHitFriendly = 0;
    public String missilesHitFriendlyString = "0";
    private int missionFailures = 0;
    public String missionFailuresString = "0";
    private int missionSuccesses = 0;
    public String missionSuccessesString = "0";
    private String pilotName = "";
    private int pilotsLost = 0;
    public String pilotsLostString = "0";
    private boolean promoted = false;
    private int rank = 0;
    public String rankPositionString = "0.";
    public String rocketsAccuracyString = "0%";
    private int rocketsFired = 0;
    public String rocketsFiredString = "0";
    private int rocketsHitEnemy = 0;
    public String rocketsHitEnemyString = "0";
    private int rocketsHitFriendly = 0;
    public String rocketsHitFriendlyString = "0";
    private int score = 0;
    public String scoreString = "0";
    private int shipsLost = 0;
    public String shipsLostString = "0";
    private int state = 3;

    public int getState() {
        return this.state;
    }

    public void setState(int s) {
        this.state = s;
    }

    public String getPilotName() {
        return this.pilotName;
    }

    public void setPilotName(String name) {
        this.pilotName = name;
    }

    public int getCurrentMission() {
        return this.currentMission;
    }

    public void setCurrentMission(int mission) {
        this.currentMission = mission;
    }

    public void setMissionSuccesses(int m) throws Exception {
        try {
            this.missionSuccesses = m;
            this.missionSuccessesString = Integer.toString(m);
        } catch (Exception e) {
            throw e;
        }
    }

    public int getMissionSuccesses() {
        return this.missionSuccesses;
    }

    public void setMissionFailures(int m) throws Exception {
        try {
            this.missionFailures = m;
            this.missionFailuresString = Integer.toString(m);
        } catch (Exception e) {
            throw e;
        }
    }

    public int getMissionFailures() {
        return this.missionFailures;
    }

    public int getScore() {
        return this.score;
    }

    public void setScore(int s) throws Exception {
        try {
            this.score = s;
            this.scoreString = Integer.toString(s);
        } catch (Exception e) {
            throw e;
        }
    }

    public void setRankPosition(int pos) throws Exception {
        try {
            this.rankPositionString = String.valueOf(Integer.toString(pos)) + ".";
        } catch (Exception e) {
            throw e;
        }
    }

    public String getRankPositionString() {
        return this.rankPositionString;
    }

    public void setRank(int r) {
        this.rank = r;
    }

    public int getRank() {
        return this.rank;
    }

    public boolean getPromoted() {
        return this.promoted;
    }

    public void setEjections(int val) throws Exception {
        try {
            this.ejections = val;
            this.ejectionsString = Integer.toString(val);
        } catch (Exception e) {
            throw e;
        }
    }

    public int getEjections() {
        return this.ejections;
    }

    public void setShipsLost(int val) throws Exception {
        try {
            this.shipsLost = val;
            this.shipsLostString = Integer.toString(val);
        } catch (Exception e) {
            throw e;
        }
    }

    public int getShipsLost() {
        return this.shipsLost;
    }

    public void setPilotsLost(int val) throws Exception {
        try {
            this.pilotsLost = val;
            this.pilotsLostString = Integer.toString(val);
        } catch (Exception e) {
            throw e;
        }
    }

    public int getPilotsLost() {
        return this.pilotsLost;
    }

    public void setEnemyShipsKilled(int val) throws Exception {
        try {
            this.enemyShipsKilled = val;
            this.enemyShipsKilledString = Integer.toString(val);
        } catch (Exception e) {
            throw e;
        }
    }

    public int getEnemyShipsKilled() {
        return this.enemyShipsKilled;
    }

    public void setEnemyPodsKilled(int val) throws Exception {
        try {
            this.enemyPodsKilled = val;
            this.enemyPodsKilledString = Integer.toString(val);
        } catch (Exception e) {
            throw e;
        }
    }

    public int getEnemyPodsKilled() {
        return this.enemyPodsKilled;
    }

    public void setFriendlyShipsKilled(int val) throws Exception {
        try {
            this.friendlyShipsKilled = val;
            this.friendlyShipsKilledString = Integer.toString(val);
        } catch (Exception e) {
            throw e;
        }
    }

    public int getFriendlyShipsKilled() {
        return this.friendlyShipsKilled;
    }

    public void setFriendlyPodsKilled(int val) throws Exception {
        try {
            this.friendlyPodsKilled = val;
            this.friendlyPodsKilledString = Integer.toString(val);
        } catch (Exception e) {
            throw e;
        }
    }

    public int getFriendlyPodsKilled() {
        return this.friendlyPodsKilled;
    }

    public void setDamageReceived(int val) throws Exception {
        try {
            this.damageReceived = val;
            this.damageReceivedString = Integer.toString(val);
        } catch (Exception e) {
            throw e;
        }
    }

    public int getDamageReceived() {
        return this.damageReceived;
    }

    public void setBulletsFired(int val) throws Exception {
        try {
            this.bulletsFired = val;
            this.bulletsFiredString = Integer.toString(val);
            calcBulletAccuracy();
        } catch (Exception e) {
            throw e;
        }
    }

    public int getBulletsFired() {
        return this.bulletsFired;
    }

    public void setBulletsHitEnemy(int val) throws Exception {
        try {
            this.bulletsHitEnemy = val;
            this.bulletsHitEnemyString = Integer.toString(val);
            calcBulletAccuracy();
        } catch (Exception e) {
            throw e;
        }
    }

    public int getBulletsHitEnemy() {
        return this.bulletsHitEnemy;
    }

    public void setBulletsHitFriendly(int val) throws Exception {
        try {
            this.bulletsHitFriendly = val;
            this.bulletsHitFriendlyString = Integer.toString(val);
        } catch (Exception e) {
            throw e;
        }
    }

    public int getBulletsHitFriendly() {
        return this.bulletsHitFriendly;
    }

    private void calcBulletAccuracy() throws Exception {
        try {
            this.bulletsAccuracyString = String.valueOf(Integer.toString((int) ((((float) this.bulletsHitEnemy) / ((float) this.bulletsFired)) * 100.0f))) + "%";
        } catch (Exception e) {
            throw e;
        }
    }

    public void setRocketsFired(int val) throws Exception {
        try {
            this.rocketsFired = val;
            this.rocketsFiredString = Integer.toString(val);
            calcRocketAccuracy();
        } catch (Exception e) {
            throw e;
        }
    }

    public int getRocketsFired() {
        return this.rocketsFired;
    }

    public void setRocketsHitEnemy(int val) throws Exception {
        try {
            this.rocketsHitEnemy = val;
            this.rocketsHitEnemyString = Integer.toString(val);
            calcRocketAccuracy();
        } catch (Exception e) {
            throw e;
        }
    }

    public int getRocketsHitEnemy() {
        return this.rocketsHitEnemy;
    }

    public void setRocketsHitFriendly(int val) throws Exception {
        try {
            this.rocketsHitFriendly = val;
            this.rocketsHitFriendlyString = Integer.toString(val);
        } catch (Exception e) {
            throw e;
        }
    }

    public int getRocketsHitFriendly() {
        return this.rocketsHitFriendly;
    }

    private void calcRocketAccuracy() throws Exception {
        try {
            this.rocketsAccuracyString = String.valueOf(Integer.toString((int) ((((float) this.rocketsHitEnemy) / ((float) this.rocketsFired)) * 100.0f))) + "%";
        } catch (Exception e) {
            throw e;
        }
    }

    public void setMissilesFired(int val) throws Exception {
        try {
            this.missilesFired = val;
            this.missilesFiredString = Integer.toString(val);
            calcMissileAccuracy();
        } catch (Exception e) {
            throw e;
        }
    }

    public int getMissilesFired() {
        return this.missilesFired;
    }

    public void setMissilesHitEnemy(int val) throws Exception {
        try {
            this.missilesHitEnemy = val;
            this.missilesHitEnemyString = Integer.toString(val);
            calcMissileAccuracy();
        } catch (Exception e) {
            throw e;
        }
    }

    public int getMissilesHitEnemy() {
        return this.missilesHitEnemy;
    }

    public void setMissilesHitFriendly(int val) throws Exception {
        try {
            this.missilesHitFriendly = val;
            this.missilesHitFriendlyString = Integer.toString(val);
        } catch (Exception e) {
            throw e;
        }
    }

    public int getMissilesHitFriendly() {
        return this.missilesHitFriendly;
    }

    private void calcMissileAccuracy() throws Exception {
        try {
            this.missilesAccuracyString = String.valueOf(Integer.toString((int) ((((float) this.missilesHitEnemy) / ((float) this.missilesFired)) * 100.0f))) + "%";
        } catch (Exception e) {
            throw e;
        }
    }

    public void setBraveryInTheAir(int val) {
        this.braveryInTheAir = val;
    }

    public int getBraveryInTheAir() {
        return this.braveryInTheAir;
    }

    public void setMeritoriousServiceMedal(int val) {
        this.meritoriousServiceMedal = val;
    }

    public int getMeritoriousServiceMedal() {
        return this.meritoriousServiceMedal;
    }

    public void setAirForceCross(int val) {
        this.airForceCross = val;
    }

    public int getAirForceCross() {
        return this.airForceCross;
    }

    public void setDistinguishedFlyingCross(int val) {
        this.distinguishedFlyingCross = val;
    }

    public int getDistinguishedFlyingCross() {
        return this.distinguishedFlyingCross;
    }

    public void setConspicuousGallantryCross(int val) {
        this.conspicuousGallantryCross = val;
    }

    public int getConspicuousGallantryCross() {
        return this.conspicuousGallantryCross;
    }

    public void addMissionSuccess() throws Exception {
        try {
            setMissionSuccesses(this.missionSuccesses + 1);
            this.score += 200;
            setScore(this.score);
        } catch (Exception e) {
            throw e;
        }
    }

    public void addMissionFailure() throws Exception {
        try {
            setMissionFailures(this.missionFailures + 1);
            this.score += 0;
        } catch (Exception e) {
            throw e;
        }
    }

    public void addPromotion() {
        this.rank++;
        this.promoted = true;
        this.score += 500;
    }

    public void addEjections(int num) {
        this.ejections += num;
        this.score += num * -50;
    }

    public void addShipsLost(int num) {
        this.shipsLost += num;
        this.score += num * -100;
    }

    public void addPilotsLost(int num) {
        this.pilotsLost += num;
        this.score += num * -50;
    }

    public void addEnemyShipsKilled(int num) {
        this.enemyShipsKilled += num;
        this.score += num * 100;
    }

    public void addEnemyPodsKilled(int num) {
        this.enemyPodsKilled += num;
        this.score += num * 200;
    }

    public void addFriendlyShipsKilled(int num) {
        this.friendlyShipsKilled += num;
        this.score += num * -100;
    }

    public void addFriendlyPodsKilled(int num) {
        this.friendlyPodsKilled += num;
        this.score += num * -100;
    }

    public void addDamageReceived(int num) {
        this.damageReceived += num;
        this.score += num * -10;
    }

    public void addBulletsFired(int num) {
        this.bulletsFired += num;
        this.score += num * 0;
    }

    public void addBulletsHitEnemy(int num) {
        this.bulletsHitEnemy += num;
        this.score += num * 4;
    }

    public void addBulletsHitFriendly(int num) {
        this.bulletsHitFriendly += num;
        this.score += num * -2;
    }

    public void addRocketsFired(int num) {
        this.rocketsFired += num;
        this.score += num * 0;
    }

    public void addRocketsHitEnemy(int num) {
        this.rocketsHitEnemy += num;
        this.score += num * 40;
    }

    public void addRocketsHitFriendly(int num) {
        this.rocketsHitFriendly += num;
        this.score += num * -20;
    }

    public void addMissilesFired(int num) {
        this.missilesFired += num;
        this.score += num * 0;
    }

    public void addMissilesHitEnemy(int num) {
        this.missilesHitEnemy += num;
        this.score += num * 20;
    }

    public void addMissilesHitFriendly(int num) {
        this.missilesHitFriendly += num;
        this.score += num * -10;
    }

    public void addBraveryInTheAir() {
        this.braveryInTheAir++;
        this.score += 100;
    }

    public void addMeritoriousServiceMedal() {
        this.meritoriousServiceMedal++;
        this.score += 200;
    }

    public void addAirForceCross() {
        this.airForceCross++;
        this.score += 500;
    }

    public void addDistinguishedFlyingCross() {
        this.distinguishedFlyingCross++;
        this.score += 1000;
    }

    public void addConspicuousGallantryCross() {
        this.conspicuousGallantryCross++;
        this.score += 3000;
    }
}
