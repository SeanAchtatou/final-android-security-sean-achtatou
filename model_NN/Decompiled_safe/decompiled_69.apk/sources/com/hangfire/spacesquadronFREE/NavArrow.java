package com.hangfire.spacesquadronFREE;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

public final class NavArrow {
    private Drawable arrowHead = null;
    private Drawable arrowHeadBlue;
    private Drawable arrowHeadRed;
    private Paint arrowLine;
    public float fltAng;
    public float fltX;
    public float fltY;
    private int normalRadius;

    public NavArrow(Resources res) throws Exception {
        try {
            this.arrowHeadBlue = res.getDrawable(R.drawable.arrowheadblue);
            this.arrowHeadRed = res.getDrawable(R.drawable.arrowheadred);
            this.arrowHeadBlue.setAlpha(Units.AI_ZONE_WAIT);
            this.arrowHeadRed.setAlpha(60);
            this.normalRadius = this.arrowHeadBlue.getIntrinsicWidth() / 2;
            this.arrowLine = new Paint();
            this.arrowLine.setARGB(100, 255, 255, 255);
            this.arrowLine.setStrokeWidth(2.0f);
        } catch (Exception e) {
            throw e;
        }
    }

    public void drawLine(Canvas canvas, int x, int y, float fltAngle, float fltSpeed, float fltTurn, Screen screen, boolean greyed, boolean routeHidden, int side) throws Exception {
        try {
            if (!screen.zoomedIn) {
                fltSpeed /= 2.0f;
            }
            this.fltAng = fltAngle;
            this.fltX = (float) x;
            this.fltY = (float) y;
            if (side == 0) {
                if (greyed) {
                    this.arrowLine.setARGB(60, 75, 75, 200);
                } else {
                    this.arrowLine.setARGB(Units.AI_ZONE_WAIT, 100, 100, 255);
                }
            } else if (greyed) {
                this.arrowLine.setARGB(70, 200, 125, 125);
            } else {
                this.arrowLine.setARGB(Units.AI_ZONE_WAIT, 250, 150, 150);
            }
            if (routeHidden) {
                this.fltX = ((float) x) + (LookUp.sin(this.fltAng) * fltSpeed * 60.0f);
                this.fltY = ((float) y) - ((LookUp.cos(this.fltAng) * fltSpeed) * 60.0f);
                this.fltAng = fltAngle;
                if (canvas != null) {
                    canvas.drawLine((float) x, (float) y, this.fltX, this.fltY, this.arrowLine);
                    return;
                }
                return;
            }
            for (int i = 0; i < 60; i++) {
                float oldX = this.fltX;
                float oldY = this.fltY;
                this.fltX += LookUp.sin(this.fltAng) * fltSpeed;
                this.fltY -= LookUp.cos(this.fltAng) * fltSpeed;
                this.fltAng = Functions.wrapAngle(this.fltAng + fltTurn);
                if (canvas != null && !routeHidden) {
                    canvas.drawLine(oldX, oldY, this.fltX, this.fltY, this.arrowLine);
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void drawHead(Canvas canvas, float x, float y, float a, int side, boolean playable) throws Exception {
        try {
            canvas.save();
            canvas.rotate(a, x, y);
            if (side == 0) {
                this.arrowHead = this.arrowHeadBlue;
                if (playable) {
                    this.arrowHead.setAlpha(Units.AI_ZONE_WAIT);
                } else {
                    this.arrowHead.setAlpha(60);
                }
            } else {
                this.arrowHead = this.arrowHeadRed;
            }
            this.arrowHead.setBounds((int) (x - ((float) this.normalRadius)), (int) (y - ((float) this.normalRadius)), (int) (((float) this.normalRadius) + x), (int) (((float) this.normalRadius) + y));
            this.arrowHead.draw(canvas);
            canvas.restore();
        } catch (Exception e) {
            throw e;
        }
    }
}
