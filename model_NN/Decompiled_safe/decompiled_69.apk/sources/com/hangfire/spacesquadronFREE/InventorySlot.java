package com.hangfire.spacesquadronFREE;

public final class InventorySlot {
    public static final int INV_CPU = 3;
    public static final int INV_DESTROYED = 1;
    public static final int INV_FIRSTWEAPON = 7;
    public static final int INV_GENERIC = -1;
    public static final int INV_GUN = 7;
    public static final int INV_HEAVYMISSILE = 11;
    public static final int INV_HEAVYROCKET = 10;
    public static final int INV_MISSILE = 9;
    public static final int INV_PILOT = 2;
    public static final int INV_POWERJET = 5;
    public static final int INV_REACTOR = 6;
    public static final int INV_ROCKET = 8;
    public static final int INV_TURNJET = 4;
    public boolean firing = false;
    public boolean loading = false;
    public int side;
    public int type;
    public int value;
    public boolean weapon = false;

    public InventorySlot(int t, int v, int s) throws Exception {
        try {
            this.type = t;
            this.value = v;
            this.side = s;
            if (this.type >= 7) {
                this.weapon = true;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public String getSaveString() throws Exception {
        try {
            return String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(this.type)) + "C" + String.valueOf(this.value)) + "C" + String.valueOf(this.side)) + "C" + Functions.toStr(this.weapon)) + "C" + Functions.toStr(this.loading)) + "C" + Functions.toStr(this.firing);
        } catch (Exception e) {
            throw e;
        }
    }

    public void restoreFromSaveString(String saveString) throws Exception {
        try {
            String[] data = saveString.split("C");
            this.type = Integer.parseInt(data[0]);
            this.value = Integer.parseInt(data[1]);
            this.side = Integer.parseInt(data[2]);
            this.weapon = Functions.toBoolean(data[3]);
            this.loading = Functions.toBoolean(data[4]);
            this.firing = Functions.toBoolean(data[5]);
        } catch (Exception e) {
            throw e;
        }
    }
}
