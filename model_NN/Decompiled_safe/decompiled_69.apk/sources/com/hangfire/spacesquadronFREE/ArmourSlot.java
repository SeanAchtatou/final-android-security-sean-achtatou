package com.hangfire.spacesquadronFREE;

public final class ArmourSlot {
    public int side;
    public int value = 10;

    public ArmourSlot(int s) {
        this.side = s;
    }

    public String getSaveString() throws Exception {
        try {
            return String.valueOf(String.valueOf(this.side)) + "C" + String.valueOf(this.value);
        } catch (Exception e) {
            throw e;
        }
    }

    public void restoreFromSaveString(String saveString) throws Exception {
        try {
            String[] data = saveString.split("C");
            this.side = Integer.parseInt(data[0]);
            this.value = Integer.parseInt(data[1]);
        } catch (Exception e) {
            throw e;
        }
    }
}
