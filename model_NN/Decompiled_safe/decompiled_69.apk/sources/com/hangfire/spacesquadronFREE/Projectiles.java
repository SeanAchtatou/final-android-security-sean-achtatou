package com.hangfire.spacesquadronFREE;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import java.util.ArrayList;

public final class Projectiles {
    public static final int PROJ_BULLET = 7;
    public static final int PROJ_BULLET_AI_FIRERANGE = 700;
    public static final int PROJ_BULLET_DAMAGE = 5;
    public static final int PROJ_BULLET_LIFE = 75;
    public static final float PROJ_BULLET_SPEED = 12.0f;
    public static final int PROJ_HEAVYMISSILE = 11;
    public static final int PROJ_HEAVYMISSILE_AI_FIRERANGE = 1400;
    public static final int PROJ_HEAVYMISSILE_DAMAGE = 30;
    public static final int PROJ_HEAVYMISSILE_FUEL = 200;
    public static final int PROJ_HEAVYMISSILE_HEIGHT = 20;
    public static final int PROJ_HEAVYMISSILE_SEARCHARC = 30;
    public static final float PROJ_HEAVYMISSILE_SPEED = 7.0f;
    public static final float PROJ_HEAVYMISSILE_TURN = 1.5f;
    public static final int PROJ_HEAVYMISSILE_WIDTH = 8;
    public static final int PROJ_HEAVYROCKET = 10;
    public static final int PROJ_HEAVYROCKET_AI_FIRERANGE = 1000;
    public static final int PROJ_HEAVYROCKET_DAMAGE = 40;
    public static final int PROJ_HEAVYROCKET_FUEL = 0;
    public static final int PROJ_HEAVYROCKET_HEIGHT = 24;
    public static final int PROJ_HEAVYROCKET_SEARCHARC = 0;
    public static final float PROJ_HEAVYROCKET_SPEED = 6.0f;
    public static final float PROJ_HEAVYROCKET_TURN = 0.0f;
    public static final int PROJ_HEAVYROCKET_WIDTH = 8;
    public static final int PROJ_MISSILE = 9;
    public static final int PROJ_MISSILE_AI_FIRERANGE = 1400;
    public static final int PROJ_MISSILE_DAMAGE = 20;
    public static final int PROJ_MISSILE_FUEL = 150;
    public static final int PROJ_MISSILE_HEIGHT = 20;
    public static final int PROJ_MISSILE_SEARCHARC = 26;
    public static final float PROJ_MISSILE_SPEED = 6.5f;
    public static final float PROJ_MISSILE_TURN = 1.3f;
    public static final int PROJ_MISSILE_WIDTH = 8;
    public static final int PROJ_ROCKET = 8;
    public static final int PROJ_ROCKET_AI_FIRERANGE = 1000;
    public static final int PROJ_ROCKET_DAMAGE = 25;
    public static final int PROJ_ROCKET_FUEL = 0;
    public static final int PROJ_ROCKET_HEIGHT = 20;
    public static final int PROJ_ROCKET_SEARCHARC = 0;
    public static final float PROJ_ROCKET_SPEED = 7.0f;
    public static final float PROJ_ROCKET_TURN = 0.0f;
    public static final int PROJ_ROCKET_WIDTH = 8;
    public ArrayList<Bullet> bullet = new ArrayList<>();
    private Drawable imgBullet;
    private Drawable imgHeavyMissile;
    private Drawable imgHeavyRocket;
    private Drawable imgMissile;
    private Drawable imgRocket;
    public ArrayList<Missile> missile = new ArrayList<>();

    public Projectiles(Resources res) throws Exception {
        try {
            this.imgBullet = res.getDrawable(R.drawable.bullet);
            this.imgRocket = res.getDrawable(R.drawable.rocket);
            this.imgMissile = res.getDrawable(R.drawable.missile);
            this.imgHeavyRocket = res.getDrawable(R.drawable.heavyrocket);
            this.imgHeavyMissile = res.getDrawable(R.drawable.heavymissile);
        } catch (Exception e) {
            throw e;
        }
    }

    public void move(Particles particles, Units units, Asteroids asteroids) throws Exception {
        int b = 0;
        while (b < this.bullet.size()) {
            try {
                Bullet bul = this.bullet.get(b);
                bul.move();
                if (bul.life <= 0) {
                    this.bullet.remove(b);
                }
                b++;
            } catch (Exception e) {
                throw e;
            }
        }
        for (int b2 = 0; b2 < this.missile.size(); b2++) {
            this.missile.get(b2).move(particles, units, asteroids);
        }
    }

    public void collide(Particles particles, Sounds sounds, Units units, Asteroids asteroids, Screen screen, GameThread thread) throws Exception {
        try {
            collideBullets(particles, sounds, units, asteroids, screen, thread);
            collideMissiles(particles, sounds, units, asteroids, screen, thread);
        } catch (Exception e) {
            throw e;
        }
    }

    private void collideBullets(Particles particles, Sounds sounds, Units units, Asteroids asteroids, Screen screen, GameThread thread) throws Exception {
        int b = 0;
        while (b < this.bullet.size()) {
            try {
                Bullet bul = this.bullet.get(b);
                Unit u = projectileInUnit(bul.x, bul.y, units);
                if (u != null) {
                    u.damage(Functions.boxSide(bul.x, bul.y, Functions.wrapAngle(bul.ang + 180.0f), u.fltWorldX, u.fltWorldY, u.intCollisionHalfWidth, u.intCollisionHalfHeight, u.fltAngle), 5, units, particles, sounds, screen, thread);
                    sounds.playSound(6, bul.x, bul.y, screen, thread);
                    particles.makeBulletHitSmoke(bul.x, bul.y);
                    thread.getCareerRecords().addStats(null, bul, u, null);
                    this.bullet.remove(b);
                } else if (projectileInAsteroid(bul.x, bul.y, asteroids)) {
                    this.bullet.remove(b);
                    particles.makeBulletHitSmoke(bul.x, bul.y);
                } else {
                    Pod p = projectileInPod(bul.x, bul.y, units);
                    if (p != null) {
                        p.podAlive = false;
                        particles.makeSmallExplosion(p.dblWorldX, p.dblWorldY);
                        sounds.playSound(4, bul.x, bul.y, screen, thread);
                        thread.getCareerRecords().addStats(null, bul, null, p);
                        this.bullet.remove(b);
                    }
                }
                b++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    private void collideMissiles(Particles particles, Sounds sounds, Units units, Asteroids asteroids, Screen screen, GameThread thread) throws Exception {
        int b = 0;
        while (b < this.missile.size()) {
            try {
                Missile mis = this.missile.get(b);
                Unit u = projectileInUnit(mis.x, mis.y, units);
                if (u != null) {
                    u.damage(Functions.boxSide(mis.x, mis.y, Functions.wrapAngle(mis.ang + 180.0f), u.fltWorldX, u.fltWorldY, u.intCollisionHalfWidth, u.intCollisionHalfHeight, u.fltAngle), mis.damage, units, particles, sounds, screen, thread);
                    particles.makeSmallExplosion(mis.x, mis.y);
                    sounds.playSound(4, mis.x, mis.y, screen, thread);
                    thread.getCareerRecords().addStats(mis, null, u, null);
                    this.missile.remove(b);
                } else if (projectileInAsteroid(mis.x, mis.y, asteroids)) {
                    this.missile.remove(b);
                    particles.makeSmallExplosion(mis.x, mis.y);
                    sounds.playSound(4, mis.x, mis.y, screen, thread);
                } else {
                    Pod p = projectileInPod(mis.x, mis.y, units);
                    if (p != null) {
                        p.podAlive = false;
                        particles.makeSmallExplosion(p.dblWorldX, p.dblWorldY);
                        sounds.playSound(4, mis.x, mis.y, screen, thread);
                        thread.getCareerRecords().addStats(mis, null, null, p);
                        this.missile.remove(b);
                    }
                }
                b++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    private Pod projectileInPod(double x, double y, Units units) throws Exception {
        int i = 0;
        while (i < units.unit.size()) {
            try {
                Pod p = units.unit.get(i).pod;
                if (p.podAlive && p.podLaunched && !p.podSafe) {
                    if (Functions.inCircle(x, y, p.dblWorldX, p.dblWorldY, p.intColRadius)) {
                        return p;
                    }
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
        return null;
    }

    private Unit projectileInUnit(double x, double y, Units units) throws Exception {
        int un = 0;
        while (un < units.unit.size()) {
            try {
                Unit u = units.unit.get(un);
                if (u.inGameZone()) {
                    if (Functions.inCircle(x, y, u.fltWorldX, u.fltWorldY, u.intCollisionRadius)) {
                        if (Functions.inRotatedBox(x, y, u.fltWorldX, u.fltWorldY, u.intCollisionHalfWidth, u.intCollisionHalfHeight, u.fltAngle)) {
                            return u;
                        }
                    } else {
                        continue;
                    }
                }
                un++;
            } catch (Exception e) {
                throw e;
            }
        }
        return null;
    }

    private boolean projectileInAsteroid(double x, double y, Asteroids asteroids) throws Exception {
        int i = 0;
        while (i < asteroids.asteroid.size()) {
            try {
                Asteroid a = asteroids.asteroid.get(i);
                if (Functions.inCircle(x, y, a.dblWorldX, a.dblWorldY, a.intColRadius)) {
                    return true;
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
        return false;
    }

    public void drawProjectiles(Canvas canvas, Screen screen) throws Exception {
        int b = 0;
        while (b < this.bullet.size()) {
            try {
                this.bullet.get(b).draw(canvas, screen, screen.zoomedIn, this.imgBullet);
                b++;
            } catch (Exception e) {
                throw e;
            }
        }
        for (int m = 0; m < this.missile.size(); m++) {
            Missile mis = this.missile.get(m);
            switch (mis.type) {
                case 8:
                    mis.draw(canvas, screen, screen.zoomedIn, this.imgRocket);
                    break;
                case 9:
                    mis.draw(canvas, screen, screen.zoomedIn, this.imgMissile);
                    break;
                case 10:
                    mis.draw(canvas, screen, screen.zoomedIn, this.imgHeavyRocket);
                    break;
                case 11:
                    mis.draw(canvas, screen, screen.zoomedIn, this.imgHeavyMissile);
                    break;
            }
        }
    }

    public void makeBullet(double x, double y, float ang, int offset, Unit u, int side, Sounds sounds, Screen screen, GameThread thread) throws Exception {
        try {
            float xspd = xSpd(ang, 12.0f);
            float yspd = ySpd(ang, 12.0f);
            double x2 = getProjectileStartX(x, ang, (float) offset);
            double y2 = getProjectileStartY(y, ang, (float) offset);
            this.bullet.add(new Bullet(x2, y2, ang, u, xspd, yspd));
            if (u.isPlayable) {
                thread.getCareerRecords().getStats().addBulletsFired(1);
            }
            sounds.playSound(0, x2, y2, screen, thread);
        } catch (Exception e) {
            throw e;
        }
    }

    public void makeMissile(double x, double y, float ang, int offset, int type, Unit u, int side, Sounds sounds, Screen screen, GameThread thread) throws Exception {
        try {
            double x2 = getProjectileStartX(x, ang, (float) offset);
            double y2 = getProjectileStartY(y, ang, (float) offset);
            float speed = 0.0f;
            float turn = 0.0f;
            int fuel = 0;
            int damage = 0;
            int searchArc = 0;
            int width = 0;
            int height = 0;
            switch (type) {
                case 8:
                    turn = 0.0f;
                    fuel = 0;
                    damage = 25;
                    searchArc = 0;
                    width = 8;
                    height = 20;
                    sounds.playSound(1, x2, y2, screen, thread);
                    speed = 7.0f;
                    break;
                case 9:
                    turn = 1.3f;
                    fuel = 150;
                    damage = 20;
                    searchArc = 26;
                    width = 8;
                    height = 20;
                    sounds.playSound(2, x2, y2, screen, thread);
                    speed = 6.5f;
                    break;
                case 10:
                    turn = 0.0f;
                    fuel = 0;
                    damage = 40;
                    searchArc = 0;
                    width = 8;
                    height = 24;
                    sounds.playSound(1, x2, y2, screen, thread);
                    speed = 6.0f;
                    break;
                case 11:
                    turn = 1.5f;
                    fuel = 200;
                    damage = 30;
                    searchArc = 30;
                    width = 8;
                    height = 20;
                    sounds.playSound(2, x2, y2, screen, thread);
                    speed = 7.0f;
                    break;
            }
            this.missile.add(new Missile(x2, y2, ang, u, speed, turn, fuel, damage, searchArc, type, width, height));
            if (!u.isPlayable) {
                return;
            }
            if (type == 9 || type == 11) {
                thread.getCareerRecords().getStats().addMissilesFired(1);
            } else {
                thread.getCareerRecords().getStats().addRocketsFired(1);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private double getProjectileStartX(double x, float ang, float dist) throws Exception {
        try {
            return ((double) (LookUp.sin(ang) * dist)) + x;
        } catch (Exception e) {
            throw e;
        }
    }

    private double getProjectileStartY(double y, float ang, float dist) throws Exception {
        try {
            return ((double) (LookUp.cos(ang) * dist)) + y;
        } catch (Exception e) {
            throw e;
        }
    }

    private float xSpd(float ang, float speed) throws Exception {
        try {
            return LookUp.sin(ang) * speed;
        } catch (Exception e) {
            throw e;
        }
    }

    private float ySpd(float ang, float speed) throws Exception {
        try {
            return LookUp.cos(ang) * speed;
        } catch (Exception e) {
            throw e;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Missile missileTrackingUnit(Unit u) throws Exception {
        int i = 0;
        while (i < this.missile.size()) {
            try {
                if (this.missile.get(i).target == u) {
                    return this.missile.get(i);
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
        return null;
    }

    public String getSaveString(Units units) throws Exception {
        try {
            String saveString = String.valueOf(this.bullet.size());
            for (int i = 0; i < this.bullet.size(); i++) {
                saveString = String.valueOf(saveString) + "A" + this.bullet.get(i).getSaveString(units);
            }
            String saveString2 = String.valueOf(saveString) + "A" + String.valueOf(this.missile.size());
            for (int i2 = 0; i2 < this.missile.size(); i2++) {
                saveString2 = String.valueOf(saveString2) + "A" + this.missile.get(i2).getSaveString(units);
            }
            return saveString2;
        } catch (Exception e) {
            throw e;
        }
    }

    public void restoreFromSaveString(String saveString, Units units) throws Exception {
        try {
            this.bullet = new ArrayList<>();
            this.missile = new ArrayList<>();
            String[] projectileData = saveString.split("A");
            int dataPos = 0 + 1;
            int numBullets = Integer.parseInt(projectileData[0]);
            int i = 0;
            while (i < numBullets) {
                this.bullet.add(new Bullet(0.0d, 0.0d, 0.0f, null, 0.0f, 0.0f));
                this.bullet.get(i).restoreFromSaveString(projectileData[dataPos], units);
                i++;
                dataPos++;
            }
            int dataPos2 = dataPos + 1;
            int numMissiles = Integer.parseInt(projectileData[dataPos]);
            int i2 = 0;
            while (true) {
                int dataPos3 = dataPos2;
                if (i2 < numMissiles) {
                    this.missile.add(new Missile(0.0d, 0.0d, 0.0f, null, 0.0f, 0.0f, 0, 0, 0, 0, 0, 0));
                    dataPos2 = dataPos3 + 1;
                    this.missile.get(i2).restoreFromSaveString(projectileData[dataPos3], units);
                    i2++;
                } else {
                    return;
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void convertIDsToReferences(Units units) {
        for (int i = 0; i < this.bullet.size(); i++) {
            Bullet b = this.bullet.get(i);
            b.owner = units.getUnitFromIndex(b.loadOwnerID);
        }
        for (int i2 = 0; i2 < this.missile.size(); i2++) {
            Missile m = this.missile.get(i2);
            m.owner = units.getUnitFromIndex(m.loadOwnerID);
            m.target = units.getUnitFromIndex(m.loadTargetID);
        }
    }
}
