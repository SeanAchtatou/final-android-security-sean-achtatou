package com.hangfire.spacesquadronFREE;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public final class MenuButton {
    public static final int CENTER = 8;
    public static final int CORNER_BOTTOMLEFT = 2;
    public static final int CORNER_BOTTOMRIGHT = 3;
    public static final int CORNER_TOPLEFT = 0;
    public static final int CORNER_TOPRIGHT = 1;
    public static final int EDGE_BOTTOM = 5;
    public static final int EDGE_LEFT = 6;
    public static final int EDGE_RIGHT = 7;
    public static final int EDGE_TOP = 4;
    private static final int STROKE_DOWN = 1;
    private static final int STROKE_LEFT = 2;
    private static final int STROKE_RIGHT = 3;
    private static final int STROKE_UP = 0;
    private Bitmap disabledImage;
    private boolean enabled = true;
    public int height = 0;
    public int id = 0;
    private Bitmap image;
    private boolean isToggle = false;
    private Paint mButtonPaint;
    public boolean on = false;
    public boolean pressed = false;
    public int screen;
    private String text = "";
    private int textFont = 0;
    private int textSize = 1;
    private int textX = 1;
    private int textY = 1;
    private boolean visible = true;
    public int width = 0;
    public int x = 0;
    public int x2 = 0;
    public int y = 0;
    public int y2 = 0;

    public MenuButton(int lid, int screenid, boolean toggle, boolean vis) throws Exception {
        try {
            this.id = lid;
            this.screen = screenid;
            this.isToggle = toggle;
            this.visible = vis;
            this.mButtonPaint = new Paint();
        } catch (Exception e) {
            throw e;
        }
    }

    public void setData(int sx, int sy, int sx2, int sy2, String stext, int ltextSize, int ltextFont) throws Exception {
        try {
            this.x = sx;
            this.y = sy;
            this.x2 = sx2;
            this.y2 = sy2;
            this.width = sx2 - sx;
            this.height = sy2 - sy;
            this.text = stext;
            this.textSize = ltextSize;
            this.textFont = ltextFont;
            this.textX = (this.x2 + this.x) / 2;
            this.textY = ((this.y2 + this.y) / 2) + ((int) (((double) ltextSize) / 2.5d));
        } catch (Exception e) {
            throw e;
        }
    }

    public void setImage(Bitmap img) {
        this.image = img;
    }

    public void setDisabledImage(Bitmap img) {
        this.disabledImage = img;
    }

    public void setPosition(int px, int py) throws Exception {
        try {
            this.x = px;
            this.y = py;
            this.x2 = this.width + px;
            this.y2 = this.height + py;
            this.textX = (this.x2 + this.x) / 2;
            this.textY = ((this.y2 + this.y) / 2) + (this.textSize / 2);
        } catch (Exception e) {
            throw e;
        }
    }

    public void setDimensions(int px, int py, int px2, int py2, int ts) throws Exception {
        try {
            this.x = px;
            this.y = py;
            this.x2 = px2;
            this.y2 = py2;
            this.width = this.x2 - this.x;
            this.height = this.y2 - this.y;
            this.textX = (this.x2 + this.x) / 2;
            this.textY = ((this.y2 + this.y) / 2) + (this.textSize / 2);
            this.textSize = ts;
        } catch (Exception e) {
            throw e;
        }
    }

    public void setEnabled(boolean en) {
        this.enabled = en;
    }

    public void setIsToggle(boolean tog) {
        this.isToggle = tog;
    }

    public void setToggleOn(boolean tog) {
        this.on = tog;
    }

    public void setVisible(boolean vis) {
        this.visible = vis;
    }

    public void setText(String t) {
        this.text = t;
    }

    public void setTextSize(int s) {
        this.textSize = s;
    }

    public void setTextFont(int f) {
        this.textFont = f;
    }

    public boolean press(float px, float py, boolean push) throws Exception {
        try {
            if (!this.enabled || !this.visible || px < ((float) this.x) || px > ((float) this.x2) || py < ((float) this.y) || py > ((float) this.y2)) {
                this.pressed = false;
                return false;
            }
            if (push) {
                this.pressed = true;
            } else {
                if (this.pressed && this.isToggle) {
                    if (this.on) {
                        this.on = false;
                    } else {
                        this.on = true;
                    }
                }
                this.pressed = false;
            }
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public void draw(Canvas canvas, BitmapFontDrawer textDrawer) throws Exception {
        int buttonColor;
        int topEdgeColor;
        int sideEdgeColor;
        int bottomEdgeColor;
        int fontAlpha;
        int imgAlpha;
        try {
            if (this.visible) {
                if (!this.enabled) {
                    buttonColor = Color.argb(100, 180, 180, 180);
                    topEdgeColor = Color.argb(100, 220, 220, 220);
                    sideEdgeColor = Color.argb(100, 160, 160, 160);
                    bottomEdgeColor = Color.argb(100, 100, 100, 100);
                    fontAlpha = 100;
                    imgAlpha = 100;
                } else if (this.on) {
                    buttonColor = Color.argb(150, 255, 50, 50);
                    topEdgeColor = Color.argb(200, 255, 100, 100);
                    sideEdgeColor = Color.argb(200, 255, 0, 0);
                    bottomEdgeColor = Color.argb(200, 200, 0, 0);
                    fontAlpha = 255;
                    imgAlpha = 255;
                } else if (this.pressed) {
                    buttonColor = Color.argb(255, 150, 150, 150);
                    topEdgeColor = Color.argb(200, 255, 255, 0);
                    sideEdgeColor = Color.argb(200, 255, 255, 0);
                    bottomEdgeColor = Color.argb(200, 255, 255, 0);
                    fontAlpha = 255;
                    imgAlpha = 255;
                } else {
                    buttonColor = Color.argb(150, 180, 180, 180);
                    topEdgeColor = Color.argb(200, 220, 220, 220);
                    sideEdgeColor = Color.argb(200, 160, 160, 160);
                    bottomEdgeColor = Color.argb(200, 100, 100, 100);
                    fontAlpha = 255;
                    imgAlpha = 200;
                }
                this.mButtonPaint.setColor(buttonColor);
                canvas.drawRect((float) this.x, (float) this.y, (float) this.x2, (float) this.y2, this.mButtonPaint);
                this.mButtonPaint.setColor(topEdgeColor);
                drawSteppedLine(canvas, this.x, this.y, this.x2, this.y, 1, 3, this.mButtonPaint);
                this.mButtonPaint.setColor(bottomEdgeColor);
                drawSteppedLine(canvas, this.x, this.y2, this.x2, this.y2, 0, 3, this.mButtonPaint);
                this.mButtonPaint.setColor(sideEdgeColor);
                drawSteppedLine(canvas, this.x, this.y, this.x, this.y2, 3, 3, this.mButtonPaint);
                drawSteppedLine(canvas, this.x2, this.y, this.x2, this.y2, 2, 3, this.mButtonPaint);
                if (this.image != null) {
                    this.mButtonPaint.setAlpha(imgAlpha);
                    if (this.disabledImage == null || this.enabled) {
                        canvas.drawBitmap(this.image, (float) ((this.x + (this.width / 2)) - (this.image.getWidth() / 2)), (float) ((this.y + (this.height / 2)) - (this.image.getHeight() / 2)), this.mButtonPaint);
                        return;
                    }
                    canvas.drawBitmap(this.disabledImage, (float) ((this.x + (this.width / 2)) - (this.disabledImage.getWidth() / 2)), (float) ((this.y + (this.height / 2)) - (this.disabledImage.getHeight() / 2)), this.mButtonPaint);
                } else if (this.text != null) {
                    this.mButtonPaint.setAlpha(fontAlpha);
                    textDrawer.draw(canvas, this.text, this.textX, this.textY, this.textFont, this.textSize, 0, this.mButtonPaint);
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawSteppedLine(Canvas canvas, int lx, int ly, int lx2, int ly2, int strokeDirection, int strokeWidth, Paint paint) throws Exception {
        int i = 0;
        while (i < strokeWidth) {
            try {
                canvas.drawLine((float) lx, (float) ly, (float) lx2, (float) ly2, paint);
                switch (strokeDirection) {
                    case 0:
                        lx++;
                        lx2--;
                        ly--;
                        ly2--;
                        break;
                    case 1:
                        lx++;
                        lx2--;
                        ly++;
                        ly2++;
                        break;
                    case 2:
                        lx--;
                        lx2--;
                        ly++;
                        ly2--;
                        break;
                    case 3:
                        lx++;
                        lx2++;
                        ly++;
                        ly2--;
                        break;
                }
                i++;
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public int getX(int location) throws Exception {
        switch (location) {
            case 0:
                try {
                    return this.x;
                } catch (Exception e) {
                    throw e;
                }
            case 1:
                return this.x2;
            case 2:
                return this.x;
            case 3:
                return this.x2;
            case 4:
                return this.x + (this.width / 2);
            case 5:
                return this.x + (this.width / 2);
            case 6:
                return this.x;
            case 7:
                return this.x2;
            case 8:
                return this.x + (this.width / 2);
            default:
                return 0;
        }
    }

    public int getY(int location) throws Exception {
        switch (location) {
            case 0:
                try {
                    return this.y;
                } catch (Exception e) {
                    throw e;
                }
            case 1:
                return this.y;
            case 2:
                return this.y2;
            case 3:
                return this.y2;
            case 4:
                return this.y;
            case 5:
                return this.y2;
            case 6:
                return this.y + (this.height / 2);
            case 7:
                return this.y + (this.height / 2);
            case 8:
                return this.y + (this.height / 2);
            default:
                return 0;
        }
    }
}
