package com.hangfire.spacesquadronFREE;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public final class GameThread extends Thread {
    public static final String ASTEROIDS_PREF = "ASTEROIDS_SAVE";
    private static final String ERROR_PREF = "ERROR_LOG";
    public static final int MODE_ERROR = 3;
    public static final int MODE_MENU = 2;
    public static final int MODE_SPACE = 1;
    private static final String MUSIC_ON_PREF = "MUSIC_ON";
    public static final String PARTICLES_PREF = "PARTICLES_SAVE";
    public static final int PREFERENCES_VERSION = 1;
    public static final String PREFERENCES_VERSION_PREF = "PREFERENCES_VERSION";
    public static final String PROJECTILES_PREF = "PROJECTILES_SAVE";
    public static final String RESUMESCREEN_PREF = "RESUME_SCREEN";
    public static final int RETURNMODE_MENU_EXITAPP = 6;
    public static final int RETURNMODE_MENU_MAIN = 1;
    public static final int RETURNMODE_MENU_MISSION_DEAD = 12;
    public static final int RETURNMODE_MENU_MISSION_DEAD_MESSAGE = 9;
    public static final int RETURNMODE_MENU_MISSION_FAILURE = 11;
    public static final int RETURNMODE_MENU_MISSION_FAILURE_MESSAGE = 8;
    public static final int RETURNMODE_MENU_MISSION_SUCCESS = 10;
    public static final int RETURNMODE_MENU_MISSION_SUCCESS_MESSAGE = 7;
    public static final int RETURNMODE_MENU_NOTREADY = 13;
    public static final int RETURNMODE_NONE = 0;
    public static final int RETURNMODE_SPACE_NEW = 4;
    public static final int RETURNMODE_SPACE_RESUME = 5;
    public static final int RETURNMODE_SPACE_RESUME_DIRECT = 14;
    public static final float SAVE_FLOAT_MODIFIER = 1000.0f;
    public static final String SCREEN_PREF = "SCREEN_SAVE";
    private static final String SOUND_ON_PREF = "SOUND_ON";
    public static final String STATISTICS_PREF = "STATISTICS_SAVE";
    private static final String TUTORIAL_ON_PREF = "TUTORIAL_ON";
    public static final String UNITS_PREF = "UNITS_SAVE";
    public static final String UNLOCKED_PREF = "UNLOCKED_MISSION";
    private static final String VIBRATION_ON_PREF = "VIBRATION_ON";
    private GameActivity mActivity;
    private CareerRecords mCareerRecords;
    private Context mContext;
    private boolean mDisplayBug = true;
    private int mHeight = 0;
    private boolean mLoadedCore = false;
    private int mMainMode = 2;
    private MenuController mMenuController;
    private MissionData mMissionData = null;
    private String mMissionsUnlockedString = "1 mission(s) unlocked";
    private boolean mModeChanging = false;
    private MediaPlayer mMusic;
    private int mMusicID = 0;
    private boolean mMusicOn = true;
    private boolean mOnScreenTurnButton = true;
    private boolean mRunning = false;
    private boolean mSetSurfaceSizeCall = false;
    private boolean mSoundOn = true;
    private boolean mSoundSafe = false;
    private SpaceController mSpaceController;
    private SurfaceHolder mSurfaceHolder;
    private BitmapFontDrawer mTextDrawer;
    private Timer mTimer;
    private boolean mTutorialOn = true;
    private int mUnlockedMission = 1;
    private boolean mVibrationOn = true;
    private Vibrator mVibrator;
    private int mWidth = 0;
    public boolean surfaceReady = false;

    public GameThread(SurfaceHolder surfaceHolder, Context context) {
        this.mSurfaceHolder = surfaceHolder;
        this.mContext = context;
    }

    private void loadCore() throws Exception {
        String special;
        displayExceptionReport();
        try {
            synchronized (this) {
                try {
                    this.mLoadedCore = true;
                    int preLoaderPosition = 0 + 1;
                    try {
                        drawPreMenuLoader(0, "Checking");
                        this.mTimer = new Timer();
                        int preLoaderPosition2 = preLoaderPosition + 1;
                        drawPreMenuLoader(preLoaderPosition, "Checking");
                        if (checkPreferencesVersion()) {
                            special = "Saved Data OK";
                        } else {
                            special = "NEW VERSION DETECTED. Old saves lost!";
                        }
                        preLoaderPosition = preLoaderPosition2 + 1;
                        drawPreMenuLoader(preLoaderPosition2, special);
                        int preLoaderPosition3 = preLoaderPosition + 1;
                        drawPreMenuLoader(preLoaderPosition, special);
                        this.mMenuController = new MenuController(this.mContext);
                        preLoaderPosition = preLoaderPosition3 + 1;
                        drawPreMenuLoader(preLoaderPosition3, special);
                        this.mTextDrawer = new BitmapFontDrawer(this.mContext.getResources());
                        int preLoaderPosition4 = preLoaderPosition + 1;
                        drawPreMenuLoader(preLoaderPosition, special);
                        if (this.mSetSurfaceSizeCall) {
                            setSurfaceSize(this.mWidth, this.mHeight);
                        }
                        preLoaderPosition = preLoaderPosition4 + 1;
                        drawPreMenuLoader(preLoaderPosition4, special);
                        this.mCareerRecords = new CareerRecords(getSP(), this);
                        this.mTimer.reset();
                        this.mVibrator = (Vibrator) this.mActivity.getSystemService("vibrator");
                        loadUnlockedMission();
                        if (Integer.parseInt(Build.VERSION.SDK.substring(0, 1)) >= 5) {
                            this.mSoundSafe = true;
                        } else {
                            this.mSoundSafe = false;
                        }
                    } catch (Exception e) {
                        this.mSoundSafe = false;
                    } catch (Throwable th) {
                        th = th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        } catch (Exception e2) {
            throw e2;
        }
    }

    public boolean getRunning() {
        return this.mRunning;
    }

    private boolean checkPreferencesVersion() {
        SharedPreferences sp = getSP();
        if (sp.getInt(PREFERENCES_VERSION_PREF, 0) == 1) {
            return true;
        }
        SharedPreferences.Editor ed = sp.edit();
        ed.clear();
        ed.putInt(PREFERENCES_VERSION_PREF, 1);
        ed.commit();
        return false;
    }

    private void displayExceptionReport() {
        SharedPreferences sp = getSP();
        if (sp.contains(ERROR_PREF)) {
            this.mMainMode = 3;
            String[] error = sp.getString(ERROR_PREF, "No error content").split(BitmapFontDrawer.NEWLINE);
            Canvas canvas = null;
            try {
                canvas = this.mSurfaceHolder.lockCanvas(null);
                synchronized (this.mSurfaceHolder) {
                    canvas.drawARGB(255, 0, 0, 0);
                    Paint errorPaint = new Paint();
                    errorPaint.setTextSize(12.0f);
                    errorPaint.setARGB(255, 255, 255, 255);
                    errorPaint.setTextSize(13.0f);
                    canvas.drawText("Crash on last execution!", 0.0f, (float) 15, errorPaint);
                    int y = 15 + 15;
                    canvas.drawText("Please report this bug.", 0.0f, (float) y, errorPaint);
                    int y2 = y + 15;
                    canvas.drawText("Enter the bug report below at:", 0.0f, (float) y2, errorPaint);
                    int y3 = y2 + 15;
                    errorPaint.setTextSize(14.0f);
                    errorPaint.setARGB(255, 255, 255, 0);
                    canvas.drawText("www.hangfiresoftware.com/forum", 0.0f, (float) y3, errorPaint);
                    int y4 = y3 + 22;
                    errorPaint.setARGB(255, 255, 255, 255);
                    errorPaint.setTextSize(10.0f);
                    for (String drawText : error) {
                        canvas.drawText(drawText, 0.0f, (float) y4, errorPaint);
                        y4 += 13;
                    }
                    errorPaint.setTextSize(12.0f);
                    canvas.drawText("Press Back button then restart", 0.0f, (float) (y4 + 10), errorPaint);
                    SharedPreferences.Editor ed = sp.edit();
                    ed.remove(ERROR_PREF);
                    ed.commit();
                }
                do {
                } while (this.mDisplayBug);
                this.mActivity.close();
            } finally {
                if (canvas != null) {
                    this.mSurfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }

    public void saveSettings() {
        SharedPreferences.Editor ed = getSP().edit();
        ed.putBoolean(MUSIC_ON_PREF, this.mMusicOn);
        ed.putBoolean(SOUND_ON_PREF, this.mSoundOn);
        ed.putBoolean(VIBRATION_ON_PREF, this.mVibrationOn);
        ed.putBoolean(TUTORIAL_ON_PREF, this.mTutorialOn);
        ed.commit();
    }

    private void loadSettings() {
        SharedPreferences sp = getSP();
        this.mMusicOn = sp.getBoolean(MUSIC_ON_PREF, true);
        this.mSoundOn = sp.getBoolean(SOUND_ON_PREF, true);
        this.mVibrationOn = sp.getBoolean(VIBRATION_ON_PREF, true);
        this.mTutorialOn = sp.getBoolean(TUTORIAL_ON_PREF, true);
        this.mOnScreenTurnButton = true;
    }

    public SharedPreferences getSP() {
        return this.mActivity.getPreferences(0);
    }

    public MissionData getMissionData() {
        return this.mMissionData;
    }

    public void loadMissionData(int missionID) throws Exception {
        try {
            this.mMissionData = new MissionData(this.mContext, missionID);
        } catch (Exception e) {
            throw e;
        }
    }

    public CareerRecords getCareerRecords() {
        return this.mCareerRecords;
    }

    public void saveCareerRecords() throws Exception {
        try {
            this.mCareerRecords.saveCareerRecords(getSP());
        } catch (Exception e) {
            throw e;
        }
    }

    public void saveCurrentCareer() throws Exception {
        try {
            this.mCareerRecords.saveCurrentCareer(getSP());
        } catch (Exception e) {
            throw e;
        }
    }

    public void setMusic(int id) throws Exception {
        try {
            stopMusic();
            if (id == -1) {
                id = this.mMusicID;
            }
            if (this.mMusic == null) {
                this.mMusic = MediaPlayer.create(this.mContext, id);
                this.mMusic.setLooping(true);
            }
            if (this.mMusicOn) {
                this.mMusic.start();
            }
            this.mMusicID = id;
        } catch (Exception e) {
            throw e;
        }
    }

    public int getMusicID() {
        return this.mMusicID;
    }

    public void stopMusic() throws Exception {
        try {
            if (this.mMusic != null) {
                this.mMusic.release();
                this.mMusic = null;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void quietMusic(boolean quiet) throws Exception {
        try {
            if (this.mMusic != null) {
                if (quiet) {
                    this.mMusic.setVolume(0.5f, 0.5f);
                } else {
                    this.mMusic.setVolume(2.0f, 2.0f);
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void setMusicOn(boolean musicOn) throws Exception {
        try {
            this.mMusicOn = musicOn;
            if (!musicOn) {
                stopMusic();
            } else {
                setMusic(this.mMusicID);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean getMusicOn() {
        return this.mMusicOn;
    }

    public void setVibrationOn(boolean vibrationOn) throws Exception {
        try {
            this.mVibrationOn = vibrationOn;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean getVibrationOn() {
        return this.mVibrationOn;
    }

    public void setTutorialOn(boolean tutOn) throws Exception {
        try {
            this.mTutorialOn = tutOn;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean getTutorialOn() {
        return this.mTutorialOn;
    }

    private void loadUnlockedMission() throws Exception {
        try {
            this.mUnlockedMission = getSP().getInt(UNLOCKED_PREF, 1);
            setUnlockedMissionString();
        } catch (Exception e) {
            throw e;
        }
    }

    public int getUnlockedMission() {
        return this.mUnlockedMission;
    }

    public void setUnlockedMission(int mission) throws Exception {
        try {
            if (mission > this.mUnlockedMission) {
                SharedPreferences.Editor ed = getSP().edit();
                ed.putInt(UNLOCKED_PREF, mission);
                ed.commit();
                this.mUnlockedMission = mission;
                setUnlockedMissionString();
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private void setUnlockedMissionString() throws Exception {
        try {
            this.mMissionsUnlockedString = String.valueOf(Integer.toString(this.mUnlockedMission)) + " mission(s) unlocked";
        } catch (Exception e) {
            throw e;
        }
    }

    public String getUnlockedMissionString() {
        return this.mMissionsUnlockedString;
    }

    public void setSoundOn(boolean soundOn) {
        this.mSoundOn = soundOn;
    }

    public boolean getSoundOn() {
        return this.mSoundOn;
    }

    public boolean getSoundSafe() {
        return this.mSoundSafe;
    }

    public void vibrate(int time) throws Exception {
        try {
            if (this.mVibrationOn) {
                this.mVibrator.vibrate((long) time);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void setOnScreenTurnButtonOn(boolean buttonOn) {
        this.mOnScreenTurnButton = buttonOn;
    }

    public boolean getOnScreenTurnButtonOn() {
        return this.mOnScreenTurnButton;
    }

    public void initialise(GameActivity act) {
        this.mActivity = act;
        loadSettings();
    }

    private void setMode(int returnMode) throws Exception {
        try {
            this.mModeChanging = true;
            switch (returnMode) {
                case 1:
                    this.mMenuController.changeScreen(2, 0);
                    this.mMainMode = 2;
                    break;
                case 4:
                    if (this.mSpaceController == null) {
                        this.mSpaceController = new SpaceController(this.mContext);
                    }
                    this.mCareerRecords.resetStats();
                    boolean loadedGame = this.mSpaceController.setup(this.mContext, this.mMissionData, this);
                    this.mSpaceController.setSurfaceSize(this.mWidth, this.mHeight);
                    if (!loadedGame) {
                        this.mSpaceController.getScreen().centerOn((double) this.mMissionData.getStartX(), (double) this.mMissionData.getStartY());
                    }
                    this.mTimer.reset();
                    this.mMainMode = 1;
                    break;
                case 5:
                    if (this.mSpaceController != null && !this.mSpaceController.invalidWorld) {
                        this.mTimer.reset();
                        this.mMainMode = 1;
                        break;
                    } else {
                        this.mMenuController.switchToBriefingScreen(this.mContext, this);
                        this.mMainMode = 2;
                        break;
                    }
                    break;
                case 6:
                    this.mActivity.close();
                    break;
                case 7:
                    this.mMenuController.changeScreen(11, 0);
                    this.mMainMode = 2;
                    break;
                case 8:
                    this.mMenuController.changeScreen(12, 0);
                    this.mMainMode = 2;
                    break;
                case 9:
                    this.mMenuController.changeScreen(13, 0);
                    this.mMainMode = 2;
                    break;
                case 10:
                    this.mMenuController.processMissionOver(1, this, this.mSpaceController);
                    this.mMenuController.changeScreen(10, 1);
                    this.mMainMode = 2;
                    break;
                case 11:
                    this.mMenuController.processMissionOver(2, this, this.mSpaceController);
                    this.mMenuController.changeScreen(10, 2);
                    this.mMainMode = 2;
                    break;
                case 12:
                    this.mMenuController.processMissionOver(3, this, this.mSpaceController);
                    this.mMenuController.changeScreen(10, 3);
                    this.mMainMode = 2;
                    break;
            }
            this.mModeChanging = false;
        } catch (Exception e) {
            throw e;
        }
    }

    public void menuItemPressed(int item) {
        try {
            switch (this.mMainMode) {
                case 1:
                    if (this.mSpaceController != null) {
                        this.mSpaceController.menuItemPressed(item, this);
                        return;
                    }
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            handleException(e);
        }
    }

    public void backButtonPressed(boolean depressed) {
        try {
            synchronized (this) {
                switch (this.mMainMode) {
                    case 1:
                        if (this.mSpaceController != null) {
                            this.mSpaceController.backButtonPressed(depressed, this.mTimer, this);
                            break;
                        }
                        break;
                    case 2:
                        if (this.mMenuController != null) {
                            this.mMenuController.backButtonPressed(depressed, this, this.mContext);
                            break;
                        }
                        break;
                    case 3:
                        this.mDisplayBug = false;
                        break;
                }
            }
        } catch (Exception e) {
            handleException(e);
        }
    }

    public boolean onTouch(MotionEvent event) {
        try {
            switch (this.mMainMode) {
                case 1:
                    if (this.mSpaceController != null) {
                        return this.mSpaceController.onTouch(event);
                    }
                    break;
                case 2:
                    if (this.mMenuController != null) {
                        return this.mMenuController.onTouch(event);
                    }
                    break;
            }
            return false;
        } catch (Exception e) {
            handleException(e);
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setSurfaceSize(int r4, int r5) {
        /*
            r3 = this;
            monitor-enter(r3)     // Catch:{ Exception -> 0x001e }
            r3.mWidth = r4     // Catch:{ all -> 0x001b }
            r3.mHeight = r5     // Catch:{ all -> 0x001b }
            boolean r1 = r3.mLoadedCore     // Catch:{ all -> 0x001b }
            if (r1 != 0) goto L_0x000e
            r1 = 1
            r3.mSetSurfaceSizeCall = r1     // Catch:{ all -> 0x001b }
            monitor-exit(r3)     // Catch:{ all -> 0x001b }
        L_0x000d:
            return
        L_0x000e:
            com.hangfire.spacesquadronFREE.MenuController r1 = r3.mMenuController     // Catch:{ all -> 0x001b }
            if (r1 == 0) goto L_0x0019
            com.hangfire.spacesquadronFREE.MenuController r1 = r3.mMenuController     // Catch:{ all -> 0x001b }
            android.content.Context r2 = r3.mContext     // Catch:{ all -> 0x001b }
            r1.setSurfaceSize(r2, r4, r5)     // Catch:{ all -> 0x001b }
        L_0x0019:
            monitor-exit(r3)     // Catch:{ all -> 0x001b }
            goto L_0x000d
        L_0x001b:
            r1 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x001b }
            throw r1     // Catch:{ Exception -> 0x001e }
        L_0x001e:
            r1 = move-exception
            r0 = r1
            r3.handleException(r0)
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.hangfire.spacesquadronFREE.GameThread.setSurfaceSize(int, int):void");
    }

    public void setRunning() {
        this.mRunning = true;
    }

    public boolean menuAllowed() {
        if (this.mMainMode == 1) {
            if (!this.mSpaceController.gameOver) {
                return true;
            }
            this.mSpaceController.quitAfterGameOver = true;
        }
        return false;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 130 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r9 = this;
            boolean r0 = r9.mLoadedCore     // Catch:{ Exception -> 0x0028 }
            if (r0 != 0) goto L_0x0007
            r9.loadCore()     // Catch:{ Exception -> 0x0028 }
        L_0x0007:
            boolean r0 = r9.mRunning     // Catch:{ Exception -> 0x0028 }
            if (r0 != 0) goto L_0x0014
        L_0x000b:
            r9.cleanUp()
            return
        L_0x000f:
            r0 = 50
            sleep(r0)     // Catch:{ InterruptedException -> 0x0050 }
        L_0x0014:
            boolean r0 = r9.surfaceReady     // Catch:{ Exception -> 0x0028 }
            if (r0 == 0) goto L_0x000f
            boolean r0 = r9.mModeChanging     // Catch:{ Exception -> 0x0028 }
            if (r0 != 0) goto L_0x0007
            r7 = 0
            int r0 = r9.mMainMode     // Catch:{ Exception -> 0x0028 }
            switch(r0) {
                case 1: goto L_0x002e;
                case 2: goto L_0x0040;
                default: goto L_0x0022;
            }     // Catch:{ Exception -> 0x0028 }
        L_0x0022:
            if (r7 == 0) goto L_0x0007
            r9.setMode(r7)     // Catch:{ Exception -> 0x0028 }
            goto L_0x0007
        L_0x0028:
            r0 = move-exception
            r8 = r0
            r9.handleException(r8)
            goto L_0x000b
        L_0x002e:
            com.hangfire.spacesquadronFREE.SpaceController r0 = r9.mSpaceController     // Catch:{ Exception -> 0x0028 }
            android.content.Context r1 = r9.mContext     // Catch:{ Exception -> 0x0028 }
            android.view.SurfaceHolder r2 = r9.mSurfaceHolder     // Catch:{ Exception -> 0x0028 }
            com.hangfire.spacesquadronFREE.BitmapFontDrawer r3 = r9.mTextDrawer     // Catch:{ Exception -> 0x0028 }
            com.hangfire.spacesquadronFREE.Timer r4 = r9.mTimer     // Catch:{ Exception -> 0x0028 }
            com.hangfire.spacesquadronFREE.MissionData r6 = r9.mMissionData     // Catch:{ Exception -> 0x0028 }
            r5 = r9
            int r7 = r0.run(r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0028 }
            goto L_0x0022
        L_0x0040:
            com.hangfire.spacesquadronFREE.MenuController r0 = r9.mMenuController     // Catch:{ Exception -> 0x0028 }
            android.view.SurfaceHolder r1 = r9.mSurfaceHolder     // Catch:{ Exception -> 0x0028 }
            android.content.Context r2 = r9.mContext     // Catch:{ Exception -> 0x0028 }
            com.hangfire.spacesquadronFREE.BitmapFontDrawer r4 = r9.mTextDrawer     // Catch:{ Exception -> 0x0028 }
            com.hangfire.spacesquadronFREE.Timer r5 = r9.mTimer     // Catch:{ Exception -> 0x0028 }
            r3 = r9
            int r7 = r0.run(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0028 }
            goto L_0x0022
        L_0x0050:
            r0 = move-exception
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.hangfire.spacesquadronFREE.GameThread.run():void");
    }

    private void handleException(Exception e) {
        SharedPreferences.Editor ed = getSP().edit();
        String errorString = String.valueOf(e.getLocalizedMessage()) + BitmapFontDrawer.NEWLINE;
        for (StackTraceElement stackTraceElement : e.getStackTrace()) {
            String newElement = stackTraceElement.toString();
            int index = newElement.indexOf("com.hangfire.spacesquadron");
            if (index != -1) {
                newElement = newElement.substring(index + 40);
            }
            errorString = String.valueOf(errorString) + newElement + BitmapFontDrawer.NEWLINE;
        }
        ed.putString(ERROR_PREF, errorString);
        ed.commit();
        this.mActivity.finish();
    }

    public void shutDown() {
        System.runFinalizersOnExit(true);
        System.exit(0);
        this.mActivity.finish();
    }

    private void cleanUp() {
        try {
            stopMusic();
            if (this.mSpaceController != null) {
                this.mSpaceController.destroy(this);
            }
            this.mSpaceController = null;
            if (this.mMenuController != null) {
                this.mMenuController.destroy();
            }
            this.mMenuController = null;
            this.mTextDrawer = null;
            this.mTimer = null;
            this.mSurfaceHolder = null;
            this.mContext = null;
        } catch (Exception e) {
            handleException(e);
        }
    }

    private void drawPreMenuLoader(int stage, String special) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        Canvas canvas = null;
        try {
            canvas = this.mSurfaceHolder.lockCanvas(null);
            synchronized (this.mSurfaceHolder) {
                canvas.drawARGB(255, 0, 0, 0);
                Paint loadPaint = new Paint();
                loadPaint.setTextSize(10.0f);
                loadPaint.setARGB(255, 255, 255, 255);
                drawPreloaderItem(canvas, "Init NanoTimer", stage > 0, 15, loadPaint);
                if (stage > 1) {
                    z = true;
                } else {
                    z = false;
                }
                drawPreloaderItem(canvas, "Checking Saved Data", z, 30, loadPaint);
                if (stage > 2) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                drawPreloaderItem(canvas, special, z2, 45, loadPaint);
                if (stage > 3) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                drawPreloaderItem(canvas, "Loading MenuController", z3, 60, loadPaint);
                if (stage > 4) {
                    z4 = true;
                } else {
                    z4 = false;
                }
                drawPreloaderItem(canvas, "Loading BitmapFontDrawer", z4, 75, loadPaint);
                if (stage > 5) {
                    z5 = true;
                } else {
                    z5 = false;
                }
                drawPreloaderItem(canvas, "Resize Objects", z5, 90, loadPaint);
                if (stage > 6) {
                    z6 = true;
                } else {
                    z6 = false;
                }
                drawPreloaderItem(canvas, "Load Career History", z6, 105, loadPaint);
            }
        } finally {
            if (canvas != null) {
                this.mSurfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    private void drawPreloaderItem(Canvas canvas, String item, boolean done, int y, Paint loadPaint) {
        String item2;
        if (done) {
            item2 = String.valueOf(item) + " (Done)";
        } else {
            item2 = String.valueOf(item) + " (...)";
        }
        canvas.drawText(item2, 0.0f, (float) y, loadPaint);
    }

    public void deleteGameState() {
        SharedPreferences.Editor ed = getSP().edit();
        ed.remove(ASTEROIDS_PREF);
        ed.remove(PROJECTILES_PREF);
        ed.remove(STATISTICS_PREF);
        ed.remove(UNITS_PREF);
        ed.remove(PARTICLES_PREF);
        ed.commit();
    }
}
