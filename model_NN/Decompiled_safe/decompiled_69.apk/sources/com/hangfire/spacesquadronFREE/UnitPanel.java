package com.hangfire.spacesquadronFREE;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

public final class UnitPanel {
    private static final int BUTTON_CLOSE = 1;
    private static final int BUTTON_EJECT = 2;
    private static final int BUTTON_GUN1 = 3;
    private static final int BUTTON_GUN2 = 4;
    private static final int BUTTON_GUN3 = 5;
    private static final int BUTTON_GUN4 = 6;
    private Paint buttonLinkPaint;
    private Paint mAmmoPaint;
    private Bitmap mBMPArmourDestTall;
    private Bitmap mBMPArmourDestWide;
    private Bitmap mBMPArmourTall;
    private Bitmap mBMPArmourWide;
    private Bitmap mBMPCPU;
    private Bitmap mBMPGeneric;
    private Bitmap mBMPGun;
    private Bitmap mBMPHeavyMissile;
    private Bitmap mBMPHeavyRocket;
    private Bitmap mBMPItemDestroyed;
    private Bitmap mBMPMissile;
    private Bitmap mBMPPilot;
    private Bitmap mBMPPilotEjected;
    private Bitmap mBMPPowerJet;
    private Bitmap mBMPReactor;
    private Bitmap mBMPRocket;
    private Bitmap mBMPTurnJet;
    private Bitmap mBMPUnitAssault;
    private Bitmap mBMPUnitCargo;
    private Bitmap mBMPUnitDefender;
    private Bitmap mBMPUnitElite;
    private Bitmap mBMPUnitEnemyAssault;
    private Bitmap mBMPUnitEnemyCargo;
    private Bitmap mBMPUnitEnemyCruiser;
    private Bitmap mBMPUnitEnemyDefender;
    private Bitmap mBMPUnitEnemyFactory;
    private Bitmap mBMPUnitEnemyFast;
    private Bitmap mBMPUnitEnemyFighter;
    private Bitmap mBMPUnitEnemyHQ;
    private Bitmap mBMPUnitEnemyHQ2;
    private Bitmap mBMPUnitEnemyScout;
    private Bitmap mBMPUnitEnemyTurret;
    private Bitmap mBMPUnitFast;
    private Bitmap mBMPUnitFighter;
    private Bitmap mBMPUnitHQ;
    private Bitmap mBMPUnitHQ2;
    private Bitmap mBMPUnitHeavy;
    private Bitmap mBMPUnitTurret;
    private Bitmap mBMPWindow;
    private boolean mEjectWarningPlayed = false;
    private boolean mUnitPanelMode = false;
    public MenuButton mbClose;
    public MenuButton mbEject;
    public MenuButton mbGun1;
    private MenuButton mbGun2;
    private MenuButton mbGun3;
    private MenuButton mbGun4;

    public UnitPanel(Resources res) throws Exception {
        try {
            this.mBMPWindow = BitmapFactory.decodeResource(res, R.drawable.invbackground);
            this.mBMPArmourTall = BitmapFactory.decodeResource(res, R.drawable.invarmourtall);
            this.mBMPArmourWide = BitmapFactory.decodeResource(res, R.drawable.invarmourwide);
            this.mBMPArmourDestTall = BitmapFactory.decodeResource(res, R.drawable.invarmourdesttall);
            this.mBMPArmourDestWide = BitmapFactory.decodeResource(res, R.drawable.invarmourdestwide);
            this.mBMPItemDestroyed = BitmapFactory.decodeResource(res, R.drawable.invitemdestroyed);
            this.mBMPCPU = BitmapFactory.decodeResource(res, R.drawable.invcpu);
            this.mBMPReactor = BitmapFactory.decodeResource(res, R.drawable.invreactor);
            this.mBMPPilot = BitmapFactory.decodeResource(res, R.drawable.invpilot);
            this.mBMPPilotEjected = BitmapFactory.decodeResource(res, R.drawable.invpilotejected);
            this.mBMPPowerJet = BitmapFactory.decodeResource(res, R.drawable.invpowerjet);
            this.mBMPTurnJet = BitmapFactory.decodeResource(res, R.drawable.invturnjet);
            this.mBMPGeneric = BitmapFactory.decodeResource(res, R.drawable.inventorygeneric);
            this.mBMPGun = BitmapFactory.decodeResource(res, R.drawable.invmachinegun);
            this.mBMPRocket = BitmapFactory.decodeResource(res, R.drawable.invrocketlauncher);
            this.mBMPMissile = BitmapFactory.decodeResource(res, R.drawable.invmissilelauncher);
            this.mBMPHeavyRocket = BitmapFactory.decodeResource(res, R.drawable.invheavyrocketlauncher);
            this.mBMPHeavyMissile = BitmapFactory.decodeResource(res, R.drawable.invheavymissilelauncher);
            this.mBMPUnitHQ = BitmapFactory.decodeResource(res, R.drawable.shipplayhq);
            this.mBMPUnitFighter = BitmapFactory.decodeResource(res, R.drawable.shipplayfighter);
            this.mBMPUnitTurret = BitmapFactory.decodeResource(res, R.drawable.shipplayturret);
            this.mBMPUnitEnemyScout = BitmapFactory.decodeResource(res, R.drawable.shipenemyscout);
            this.mBMPUnitEnemyFighter = BitmapFactory.decodeResource(res, R.drawable.shipenemyfighter);
            this.mBMPUnitCargo = BitmapFactory.decodeResource(res, R.drawable.shipplaycargo);
            this.mBMPUnitFast = BitmapFactory.decodeResource(res, R.drawable.shipplayfast);
            this.mBMPUnitEnemyAssault = BitmapFactory.decodeResource(res, R.drawable.shipenemyassault);
            this.mBMPUnitEnemyTurret = BitmapFactory.decodeResource(res, R.drawable.shipenemyturret);
            this.mBMPUnitEnemyHQ = BitmapFactory.decodeResource(res, R.drawable.shipenemyhq);
            this.mBMPUnitAssault = BitmapFactory.decodeResource(res, R.drawable.shipplayassault);
            this.mBMPUnitEnemyFast = BitmapFactory.decodeResource(res, R.drawable.shipenemyfast);
            this.mBMPUnitElite = BitmapFactory.decodeResource(res, R.drawable.shipplayelite);
            this.mBMPUnitEnemyDefender = BitmapFactory.decodeResource(res, R.drawable.shipenemydefender);
            this.mBMPUnitDefender = BitmapFactory.decodeResource(res, R.drawable.shipplaydefender);
            this.mBMPUnitHeavy = BitmapFactory.decodeResource(res, R.drawable.shipplayheavy);
            this.mBMPUnitEnemyFactory = BitmapFactory.decodeResource(res, R.drawable.shipenemyfactory);
            this.mBMPUnitEnemyCargo = BitmapFactory.decodeResource(res, R.drawable.shipenemycargo);
            this.mBMPUnitEnemyCruiser = BitmapFactory.decodeResource(res, R.drawable.shipenemycruiser);
            this.mBMPUnitHQ2 = BitmapFactory.decodeResource(res, R.drawable.shipplayhq2);
            this.mBMPUnitEnemyHQ2 = BitmapFactory.decodeResource(res, R.drawable.shipenemyhq2);
            this.mbClose = new MenuButton(1, 0, false, false);
            this.mbClose.setData(0, 0, 100, 100, "X", 10, 0);
            this.mbEject = new MenuButton(2, 0, true, false);
            this.mbEject.setData(0, 0, 100, 100, "EJECT", 10, 0);
            this.mbEject.setImage(BitmapFactory.decodeResource(res, R.drawable.iconeject));
            this.mbGun1 = new MenuButton(3, 0, true, false);
            this.mbGun1.setData(0, 0, 100, 100, "FIRE", 10, 0);
            this.mbGun1.setImage(BitmapFactory.decodeResource(res, R.drawable.iconfire));
            this.mbGun1.setDisabledImage(BitmapFactory.decodeResource(res, R.drawable.iconloading));
            this.mbGun2 = new MenuButton(4, 0, true, false);
            this.mbGun2.setData(0, 0, 100, 100, "FIRE", 10, 0);
            this.mbGun2.setImage(BitmapFactory.decodeResource(res, R.drawable.iconfire));
            this.mbGun2.setDisabledImage(BitmapFactory.decodeResource(res, R.drawable.iconloading));
            this.mbGun3 = new MenuButton(5, 0, true, false);
            this.mbGun3.setData(0, 0, 100, 100, "FIRE", 10, 0);
            this.mbGun3.setImage(BitmapFactory.decodeResource(res, R.drawable.iconfire));
            this.mbGun3.setDisabledImage(BitmapFactory.decodeResource(res, R.drawable.iconloading));
            this.mbGun4 = new MenuButton(6, 0, true, false);
            this.mbGun4.setData(0, 0, 100, 100, "FIRE", 10, 0);
            this.mbGun4.setImage(BitmapFactory.decodeResource(res, R.drawable.iconfire));
            this.mbGun4.setDisabledImage(BitmapFactory.decodeResource(res, R.drawable.iconloading));
            this.mAmmoPaint = new Paint();
            this.mAmmoPaint.setARGB(255, 255, 255, 255);
            this.mAmmoPaint.setTextSize(9.0f);
            this.mAmmoPaint.setTextAlign(Paint.Align.CENTER);
            this.mAmmoPaint.setTextScaleX(2.0f);
            this.buttonLinkPaint = new Paint();
            this.buttonLinkPaint.setARGB(100, 255, 0, 0);
            this.buttonLinkPaint.setStrokeWidth(3.0f);
        } catch (Exception e) {
            throw e;
        }
    }

    public void setVisible(boolean visible, Unit selectedUnit) throws Exception {
        try {
            this.mUnitPanelMode = visible;
            this.mbClose.setVisible(false);
            this.mbGun1.setVisible(false);
            this.mbGun2.setVisible(false);
            this.mbGun3.setVisible(false);
            this.mbGun4.setVisible(false);
            this.mEjectWarningPlayed = false;
            this.mbEject.setVisible(false);
            if (selectedUnit != null) {
                this.mbEject.on = selectedUnit.ejectOrder;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean isVisible() {
        return this.mUnitPanelMode;
    }

    private void drawArmour(Canvas canvas, int x, int y, int stepX, int stepY, int side, Units units) throws Exception {
        try {
            Unit unit = units.unitSelected;
            int total = unit.sideArmourCount[side];
            if (stepX > 0) {
                x = (int) (((float) x) - ((((float) (total - 1)) / 2.0f) * ((float) stepX)));
            }
            if (stepY > 0) {
                y = (int) (((float) y) - ((((float) (total - 1)) / 2.0f) * ((float) stepY)));
            }
            for (int i = 0; i < unit.armour.size(); i++) {
                ArmourSlot as = unit.armour.get(i);
                if (as.side == side) {
                    if (as.value > 0) {
                        if (side == 0 || side == 1) {
                            canvas.drawBitmap(this.mBMPArmourTall, (float) x, (float) y, (Paint) null);
                        } else {
                            canvas.drawBitmap(this.mBMPArmourWide, (float) x, (float) y, (Paint) null);
                        }
                    } else if (side == 0 || side == 1) {
                        canvas.drawBitmap(this.mBMPArmourDestTall, (float) x, (float) y, (Paint) null);
                    } else {
                        canvas.drawBitmap(this.mBMPArmourDestWide, (float) x, (float) y, (Paint) null);
                    }
                    x += stepX;
                    y += stepY;
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private Bitmap getUnitBitmap(int type) throws Exception {
        switch (type) {
            case 0:
                try {
                    return this.mBMPUnitHQ;
                } catch (Exception e) {
                    throw e;
                }
            case 1:
                return this.mBMPUnitFighter;
            case 2:
                return this.mBMPUnitTurret;
            case 3:
                return this.mBMPUnitEnemyScout;
            case 4:
                return this.mBMPUnitEnemyFighter;
            case 5:
                return this.mBMPUnitCargo;
            case 6:
                return this.mBMPUnitFast;
            case 7:
                return this.mBMPUnitEnemyAssault;
            case 8:
                return this.mBMPUnitEnemyTurret;
            case 9:
                return this.mBMPUnitEnemyHQ;
            case 10:
                return this.mBMPUnitAssault;
            case 11:
                return this.mBMPUnitEnemyFast;
            case 12:
                return this.mBMPUnitElite;
            case 13:
                return this.mBMPUnitEnemyDefender;
            case 14:
                return this.mBMPUnitDefender;
            case 15:
                return this.mBMPUnitHeavy;
            case 16:
                return this.mBMPUnitEnemyFactory;
            case Units.UNIT_ENEMYCARGO /*17*/:
                return this.mBMPUnitEnemyCargo;
            case Units.UNIT_ENEMYCRUISER /*18*/:
                return this.mBMPUnitEnemyCruiser;
            case Units.UNIT_ENEMYCRUISERTURRET /*19*/:
                return this.mBMPUnitEnemyTurret;
            case 20:
                return this.mBMPUnitHQ2;
            case Units.UNIT_ENEMYHQ2 /*21*/:
                return this.mBMPUnitEnemyHQ2;
            default:
                return null;
        }
    }

    private void drawItems(Canvas canvas, int x, int y, int stepX, int stepY, int side, Units units, Screen screen) throws Exception {
        MenuButton mb;
        int linkLocation;
        try {
            Unit unit = units.unitSelected;
            int total = unit.sideInvCount[side];
            boolean firstWeapon = true;
            int itemHalf = this.mBMPCPU.getWidth() / 2;
            if (stepX > 0) {
                x = (int) (((float) x) - ((((float) (total - 1)) / 2.0f) * ((float) stepX)));
            }
            if (stepY > 0) {
                y = (int) (((float) y) - ((((float) (total - 1)) / 2.0f) * ((float) stepY)));
            }
            for (int i = 0; i < unit.inventory.size(); i++) {
                if (unit.inventory.get(i).side == side) {
                    InventorySlot is = unit.inventory.get(i);
                    if (unit.isPlayable && unit.hasPilot) {
                        if (is.type == 2) {
                            canvas.drawLine((float) (x + itemHalf), (float) (y + itemHalf), (float) (this.mbEject.x + (this.mbEject.width / 2)), (float) this.mbEject.y, this.buttonLinkPaint);
                        } else if (is.weapon) {
                            if (side == 0) {
                                if (firstWeapon) {
                                    mb = this.mbGun1;
                                    firstWeapon = false;
                                } else {
                                    mb = this.mbGun2;
                                }
                            } else if (firstWeapon) {
                                mb = this.mbGun3;
                                firstWeapon = false;
                            } else {
                                mb = this.mbGun4;
                            }
                            if (screen.intScreenWidth > screen.intScreenHeight) {
                                linkLocation = 7;
                            } else if (mb.y > y) {
                                linkLocation = 4;
                            } else {
                                linkLocation = 5;
                            }
                            canvas.drawLine((float) (x + itemHalf), (float) (y + itemHalf), (float) mb.getX(linkLocation), (float) mb.getY(linkLocation), this.buttonLinkPaint);
                        }
                    }
                    canvas.drawBitmap(getInventoryImage(is.type, unit), (float) x, (float) y, (Paint) null);
                    if (is.weapon) {
                        canvas.drawText(new Integer(is.value).toString(), (float) (x + itemHalf), (float) ((itemHalf / 2) + y), this.mAmmoPaint);
                    }
                    x += stepX;
                    y += stepY;
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private Bitmap getInventoryImage(int type, Unit u) throws Exception {
        switch (type) {
            case InventorySlot.INV_GENERIC:
                return this.mBMPGeneric;
            case 0:
            default:
                return null;
            case 1:
                try {
                    return this.mBMPItemDestroyed;
                } catch (Exception e) {
                    throw e;
                }
            case 2:
                if (u.hasPilot) {
                    return this.mBMPPilot;
                }
                return this.mBMPPilotEjected;
            case 3:
                return this.mBMPCPU;
            case 4:
                return this.mBMPTurnJet;
            case 5:
                return this.mBMPPowerJet;
            case 6:
                return this.mBMPReactor;
            case 7:
                return this.mBMPGun;
            case 8:
                return this.mBMPRocket;
            case 9:
                return this.mBMPMissile;
            case 10:
                return this.mBMPHeavyRocket;
            case 11:
                return this.mBMPHeavyMissile;
        }
    }

    public void draw(Canvas canvas, BitmapFontDrawer mTextDrawer, Units units, Screen screen) throws Exception {
        try {
            Unit unit = units.unitSelected;
            if (unit == null || !unit.inGameZone()) {
                units.unitSelected = null;
                this.mUnitPanelMode = false;
                return;
            }
            int winWidth = this.mBMPWindow.getWidth();
            int centerX = screen.intScreenWidth / 2;
            int centerY = screen.intScreenHeight / 2;
            int leftX = centerX - (this.mBMPWindow.getWidth() / 2);
            int topY = centerY - (this.mBMPWindow.getHeight() / 2);
            int armourStep = this.mBMPArmourTall.getWidth() + 1;
            int armourHalf = this.mBMPArmourTall.getWidth() / 2;
            int itemStep = this.mBMPCPU.getWidth();
            int itemHalf = this.mBMPCPU.getWidth() / 2;
            canvas.drawBitmap(this.mBMPWindow, (float) leftX, (float) topY, (Paint) null);
            drawArmour(canvas, leftX + ((int) (((double) winWidth) * 0.015d)), centerY - armourHalf, 0, armourStep, 3, units);
            drawArmour(canvas, leftX + ((int) (((double) winWidth) * 0.91d)), centerY - armourHalf, 0, armourStep, 2, units);
            drawArmour(canvas, centerX - armourHalf, topY + ((int) (((double) winWidth) * 0.015d)), armourStep, 0, 0, units);
            drawArmour(canvas, centerX - armourHalf, topY + ((int) (((double) winWidth) * 0.84d)), armourStep, 0, 1, units);
            drawItems(canvas, leftX + ((int) (((double) winWidth) * 0.11d)), centerY - itemHalf, 0, itemStep, 3, units, screen);
            drawItems(canvas, leftX + ((int) (((double) winWidth) * 0.725d)), centerY - itemHalf, 0, itemStep, 2, units, screen);
            drawItems(canvas, centerX - itemHalf, topY + ((int) (((double) winWidth) * 0.11d)), itemStep, 0, 0, units, screen);
            drawItems(canvas, centerX - itemHalf, topY + ((int) (((double) winWidth) * 0.655d)), itemStep, 0, 1, units, screen);
            Bitmap unitBMP = getUnitBitmap(unit.type);
            canvas.drawBitmap(unitBMP, (float) (centerX - (unitBMP.getWidth() / 2)), (float) (centerY - (unitBMP.getHeight() / 2)), (Paint) null);
            if (unit.isPlayable) {
                this.mbEject.setVisible(true);
                if (unit.hasPilot) {
                    this.mbEject.setEnabled(true);
                    this.mbEject.draw(canvas, mTextDrawer);
                    drawGunButton(canvas, this.mbGun1, mTextDrawer, units);
                    drawGunButton(canvas, this.mbGun2, mTextDrawer, units);
                    drawGunButton(canvas, this.mbGun3, mTextDrawer, units);
                    drawGunButton(canvas, this.mbGun4, mTextDrawer, units);
                } else {
                    this.mbEject.setEnabled(false);
                }
            }
            this.mbClose.setVisible(true);
            this.mbClose.draw(canvas, mTextDrawer);
        } catch (Exception e) {
            throw e;
        }
    }

    private void drawGunButton(Canvas canvas, MenuButton mbGun, BitmapFontDrawer mTextDrawer, Units units) throws Exception {
        try {
            InventorySlot is = getGunForButton(mbGun.id, units);
            if (is == null) {
                mbGun.setVisible(false);
                return;
            }
            mbGun.setVisible(true);
            if (is.firing) {
                mbGun.setEnabled(true);
                mbGun.setToggleOn(true);
            } else {
                mbGun.setToggleOn(false);
                if (is.loading || is.value <= 0) {
                    mbGun.setEnabled(false);
                } else {
                    mbGun.setEnabled(true);
                }
            }
            mbGun.draw(canvas, mTextDrawer);
        } catch (Exception e) {
            throw e;
        }
    }

    private InventorySlot getGunForButton(int buttonID, Units units) throws Exception {
        try {
            Unit unit = units.unitSelected;
            if (unit == null || !unit.inGameZone()) {
                units.unitSelected = null;
                this.mUnitPanelMode = false;
                return null;
            }
            switch (buttonID) {
                case 3:
                    return unit.getGun(0, 0);
                case 4:
                    return unit.getGun(1, 0);
                case 5:
                    return unit.getGun(0, 1);
                case 6:
                    return unit.getGun(1, 1);
                default:
                    return null;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void touchDown(float sx, float sy) throws Exception {
        try {
            this.mbClose.press(sx, sy, true);
            this.mbEject.press(sx, sy, true);
            this.mbGun1.press(sx, sy, true);
            this.mbGun2.press(sx, sy, true);
            this.mbGun3.press(sx, sy, true);
            this.mbGun4.press(sx, sy, true);
        } catch (Exception e) {
            throw e;
        }
    }

    public void touchUp(float sx, float sy, Units units, Sounds sounds, Screen screen, GameThread thread) throws Exception {
        try {
            if (this.mbClose.press(sx, sy, false)) {
                setVisible(false, null);
            }
            if (this.mbEject.press(sx, sy, false)) {
                units.unitSelected.ejectOrder = this.mbEject.on;
                if (!this.mEjectWarningPlayed && this.mbEject.on) {
                    sounds.playSound(8, (double) screen.intMiddleX, (double) screen.intMiddleY, screen, thread);
                    this.mEjectWarningPlayed = true;
                }
            }
            if (this.mbGun1.press(sx, sy, false)) {
                getGunForButton(3, units).firing = this.mbGun1.on;
            }
            if (this.mbGun2.press(sx, sy, false)) {
                getGunForButton(4, units).firing = this.mbGun2.on;
            }
            if (this.mbGun3.press(sx, sy, false)) {
                getGunForButton(5, units).firing = this.mbGun3.on;
            }
            if (this.mbGun4.press(sx, sy, false)) {
                getGunForButton(6, units).firing = this.mbGun4.on;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void setScreenSize(int x, int y) throws Exception {
        int buttonWidth;
        int smallButtonWidth;
        if (y > x) {
            try {
                buttonWidth = x / 5;
                smallButtonWidth = x / 6;
            } catch (Exception e) {
                throw e;
            }
        } else {
            buttonWidth = y / 5;
            smallButtonWidth = y / 6;
        }
        this.mbClose.setDimensions(x - smallButtonWidth, 0, x, smallButtonWidth, smallButtonWidth / 3);
        this.mbEject.setDimensions((x - buttonWidth) - 10, (y - buttonWidth) - 10, x - 10, y - 10, buttonWidth / 4);
        if (y > x) {
            this.mbGun1.setDimensions(10, 10, buttonWidth + 10, buttonWidth + 10, buttonWidth / 4);
            this.mbGun2.setDimensions(buttonWidth + 20, 10, (buttonWidth * 2) + 20, buttonWidth + 10, buttonWidth / 4);
            this.mbGun3.setDimensions(10, (y - 10) - buttonWidth, buttonWidth + 10, y - 10, buttonWidth / 4);
            this.mbGun4.setDimensions(buttonWidth + 20, (y - 10) - buttonWidth, (buttonWidth * 2) + 20, y - 10, buttonWidth / 4);
            return;
        }
        this.mbGun1.setDimensions(10, 10, buttonWidth + 10, buttonWidth + 10, buttonWidth / 4);
        this.mbGun2.setDimensions(10, buttonWidth + 20, buttonWidth + 10, (buttonWidth * 2) + 20, buttonWidth / 4);
        this.mbGun3.setDimensions(10, (y - 20) - (buttonWidth * 2), buttonWidth + 10, (y - 20) - buttonWidth, buttonWidth / 4);
        this.mbGun4.setDimensions(10, (y - 10) - buttonWidth, buttonWidth + 10, y - 10, buttonWidth / 4);
    }
}
