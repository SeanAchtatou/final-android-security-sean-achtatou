package com.m41m41.sniperrifle2;

import android.app.Activity;
import android.content.Intent;
import android.database.CursorJoiner;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EncodingUtils;

public class LoadHotSearchListTask extends AsyncTask<Activity, Integer, CursorJoiner.Result> {
    public static final String app_url = "market://search?q=pname:com.m41m41.sniperrifles2";
    public static final String english_name = "Sniper Rifles";
    private static final String refresh_err = "Refresh error,please retry";
    private final String RAN = "ran";
    private final String SEQ = "seq";
    private final String URL = "url";
    /* access modifiers changed from: private */
    public final activity activity;
    private ByteArrayBuffer baf;
    InputStream bis;
    /* access modifiers changed from: private */
    public String[] hotStrings = new String[(this.metaData.length / 3)];
    private String last_adress = "";
    private final String[] metaData = {"ran", "Istiglal Anti-Material", "Istiglal Anti-Material", "ran", "FD-200", "FD-200", "ran", "7.62 Tkiv 85", "7.62 Tkiv 85", "ran", "Accuracy International Arctic Warfare", "Accuracy International Arctic Warfare", "ran", "JNG-90", "JNG-90", "ran", "T-12", "T-12", "ran", "Kalekalip 12.7mm AMR", "Kalekalip 12.7mm AMR", "ran", "KNT-308", "KNT-308", "ran", "AMP Technical Services DSR-1", "AMP Technical Services DSR-1", "ran", "AMR-2", "AMR-2", "ran", "APR308", "APR308", "ran", "Armalite AR-50", "Armalite AR-50", "ran", "AWC G2", "AWC G2", "ran", "Barrett M90", "Barrett M90", "ran", "Blaser 93 Tactical", "Blaser 93 Tactical", "ran", "Bor", "Bor", "ran", "CheyTac Intervention", "CheyTac Intervention", "ran", "C3A1", "C3A1", "ran", "C14 Timberwolf", "C14 Timberwolf", "ran", "ZW-127", "ZW-127", "ran", "Denel NTW-20", "Denel NTW-20", "ran", "Desert Tactical Arms Stealth Recon Scout", "Desert Tactical Arms Stealth Recon Scout", "ran", "Dragunov SVD", "Dragunov SVD", "ran", "EDM Arms Windrunner", "EDM Arms Windrunner", "ran", "EXACTO", "EXACTO", "ran", "FN Special Police", "FN Special Police", "ran", "FR F2 sniper", "FR F2 sniper", "ran", "Gepard anti-materiel", "Gepard anti-materiel", "ran", "Harris Gun Works M-96", "Harris Gun Works M-96", "ran", "PSG1", "PSG1", "ran", "Heckler & Koch HK417", "Heckler & Koch HK417", "ran", "Howa M1500 heavy barrel", "Howa M1500 heavy barrel", "ran", "JS 7.62", "JS 7.62", "ran", "Kefefs", "Kefefs", "ran", "KSVK 12.7", "KSVK 12.7", "ran", "L42A1", "L42A1", "ran", "Longbow T-76", "Longbow T-76", "ran", "M21 Sniper Weapon System", "M21 Sniper Weapon System", "ran", "M40", "M40", "ran", "M89SR", "M89SR", "ran", "M1903A4", "M1903A4", "ran", "Mauser M67", "Mauser M67", "ran", "McMillan Tac-50", "McMillan Tac-50", "ran", "Mosin-Nagant Model 1891/30", "Mosin-Nagant Model 1891/30", "ran", "MSSR", "MSSR", "ran", "OSV-96", "OSV-96", "ran", "PGM 338", "PGM 338", "ran", "QBU-88", "QBU-88", "ran", "Robar RC-50", "Robar RC-50", "ran", "Sako TRG", "Sako TRG", "ran", "Savage 10FP", "Savage 10FP", "ran", "SSG 2000", "SSG 2000", "ran", "Ssg 82", "Ssg 82", "ran", "Steyr HS .50", "Steyr HS .50", "ran", "SV-98", "SV-98", "ran", "T93 sniper", "T93 sniper", "ran", "Tabuk Sniper", "Tabuk Sniper", "ran", "U.S. Marine Corps Designated Marksman", "U.S. Marine Corps Designated Marksman", "ran", "VB Berapi LP05", "VB Berapi LP05", "ran", "Vidhwansak", "Vidhwansak", "ran", "VSK-94", "VSK-94", "ran", "VSS Vintorez", "VSS Vintorez", "ran", "VSSK Vykhlop", "VSSK Vykhlop", "ran", "Walther WA 2000", "Walther WA 2000", "ran", "WKW Wilk", "WKW Wilk", "ran", "Zastava M91", "Zastava M91", "ran", "Zastava M93 Black Arrow", "Zastava M93 Black Arrow"};
    /* access modifiers changed from: private */
    public String[] searchOption = new String[(this.metaData.length / 3)];
    /* access modifiers changed from: private */
    public String[] stringToLink = new String[(this.metaData.length / 3)];
    URLConnection uconnection;
    URL uri;

    public String[] getHotStrings() {
        return this.hotStrings;
    }

    LoadHotSearchListTask(activity activity2) {
        this.activity = activity2;
        for (int i = 0; i < this.metaData.length; i += 3) {
            this.searchOption[i / 3] = this.metaData[i];
            this.hotStrings[i / 3] = this.metaData[i + 1];
            this.stringToLink[i / 3] = this.metaData[i + 2];
        }
    }

    /* access modifiers changed from: protected */
    public CursorJoiner.Result doInBackground(Activity... arg0) {
        this.activity.list_hot_search = getHotStrings();
        Intent intent = this.activity.getIntent();
        this.activity.getClass();
        this.activity.getClass();
        intent.putExtra("KEY_NET_status", "sucess");
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(CursorJoiner.Result result) {
        activity activity2 = this.activity;
        this.activity.getClass();
        activity2.removeDialog(5);
        Intent intent = this.activity.getIntent();
        this.activity.getClass();
        String stringExtra = intent.getStringExtra("KEY_NET_status");
        this.activity.getClass();
        if (stringExtra.compareTo("fail") == 0) {
            Toast.makeText(this.activity, refresh_err, 1).show();
        } else if (!(this.activity.list_hot_search == null || this.activity.list_hot_search.length == 0)) {
            ListView list1 = (ListView) this.activity.findViewById(R.id.ListView01);
            list1.setAdapter((ListAdapter) new ArrayAdapter(this.activity, 17367043, this.activity.list_hot_search));
            list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                    if (LoadHotSearchListTask.this.searchOption[position].compareTo("url") == 0) {
                        Intent i = new Intent(LoadHotSearchListTask.this.activity, BrowserActivity.class);
                        i.putExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD, LoadHotSearchListTask.this.hotStrings[position]);
                        i.setData(Uri.parse(LoadHotSearchListTask.this.stringToLink[position]));
                        LoadHotSearchListTask.this.activity.startActivity(i);
                    } else if (LoadHotSearchListTask.this.searchOption[position].compareTo("ran") == 0) {
                        LoadHotSearchListTask.this.activity.startSearch(LoadHotSearchListTask.this.stringToLink[position], false);
                    } else if (LoadHotSearchListTask.this.searchOption[position].compareTo("seq") == 0) {
                        LoadHotSearchListTask.this.activity.startSearch(LoadHotSearchListTask.this.stringToLink[position], true);
                    }
                }
            });
        }
        super.onPostExecute((Object) result);
    }

    public ByteArrayBuffer read(String address, int size) throws IOException {
        if (address.equalsIgnoreCase(this.last_adress)) {
            return this.baf;
        }
        this.uri = new URL(address);
        this.uconnection = this.uri.openConnection();
        this.uconnection.setUseCaches(true);
        this.bis = this.uconnection.getInputStream();
        this.baf = new ByteArrayBuffer(size);
        this.baf.clear();
        while (true) {
            int current = this.bis.read();
            if (current == -1) {
                this.last_adress = address;
                return this.baf;
            }
            this.baf.append((byte) current);
        }
    }

    public String decodeURL(ByteArrayBuffer buffer) {
        return EncodingUtils.getString(buffer.toByteArray(), "UTF-8");
    }
}
