package com.m41m41.sniperrifle2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.net.URLDecoder;

public class BrowserActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        String keyword = getIntent().getStringExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD);
        String url = getIntent().getData().toString();
        if (keyword == null || keyword.length() == 0) {
            setTitle(url);
        } else {
            setTitle(URLDecoder.decode(keyword));
        }
        requestWindowFeature(2);
        setContentView((int) R.layout.browser);
        setProgressBarVisibility(true);
        WebView webVIew = (WebView) findViewById(R.id.browser_WebView);
        webVIew.getSettings().setJavaScriptEnabled(true);
        webVIew.getSettings().setBuiltInZoomControls(true);
        webVIew.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                BrowserActivity.this.setProgress(0);
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                BrowserActivity.this.setProgress(10000);
            }
        });
        webVIew.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                BrowserActivity.this.setProgress(newProgress * 100);
                super.onProgressChanged(view, newProgress);
            }
        });
        webVIew.loadUrl(url);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.browser_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        String url = ((WebView) findViewById(R.id.browser_WebView)).getUrl();
        if (url == null) {
            return false;
        }
        String keyword = URLDecoder.decode(getIntent().getStringExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD));
        switch (item.getItemId()) {
            case R.id.openSystemBrowser /*2131099663*/:
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.share_link /*2131099664*/:
                Intent it = new Intent("android.intent.action.SEND");
                String subject = "Page of " + keyword;
                it.putExtra("android.intent.extra.SUBJECT", subject);
                it.putExtra("android.intent.extra.TEXT", String.valueOf(subject) + ":" + url + "\n");
                it.setType("text/plain");
                startActivity(Intent.createChooser(it, "Choose share format"));
                break;
        }
        return false;
    }
}
