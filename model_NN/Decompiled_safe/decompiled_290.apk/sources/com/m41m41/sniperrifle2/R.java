package com.m41m41.sniperrifle2;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771969;
        public static final int isGoneWithoutAd = 2130771973;
        public static final int keywords = 2130771971;
        public static final int refreshInterval = 2130771972;
        public static final int testing = 2130771968;
        public static final int textColor = 2130771970;
    }

    public static final class drawable {
        public static final int arrow = 2130837504;
        public static final int bg = 2130837505;
        public static final int icon = 2130837506;
        public static final int save = 2130837507;
    }

    public static final class id {
        public static final int ImageButtonNext = 2131099659;
        public static final int ImageButtonSave = 2131099660;
        public static final int LinearLayout01 = 2131099652;
        public static final int LinearLayout02 = 2131099658;
        public static final int ListView01 = 2131099651;
        public static final int ProgressBar01 = 2131099656;
        public static final int RelativeLayout01 = 2131099655;
        public static final int TextView01 = 2131099657;
        public static final int TextView02 = 2131099654;
        public static final int about = 2131099667;
        public static final int ad = 2131099650;
        public static final int browser_WebView = 2131099649;
        public static final int img_LinearLayout = 2131099648;
        public static final int img_View = 2131099653;
        public static final int keyword_edit = 2131099662;
        public static final int openSystemBrowser = 2131099663;
        public static final int openbrowser = 2131099669;
        public static final int save = 2131099668;
        public static final int search = 2131099665;
        public static final int share = 2131099666;
        public static final int share_link = 2131099664;
        public static final int username_view = 2131099661;
    }

    public static final class layout {
        public static final int browser = 2130903040;
        public static final int hot_picture_keyword = 2130903041;
        public static final int img_preview = 2130903042;
        public static final int search_dialog = 2130903043;
    }

    public static final class menu {
        public static final int browser_menu = 2131034112;
        public static final int main_menu = 2131034113;
        public static final int ohter_menu = 2131034114;
        public static final int picture_save_menu = 2131034115;
    }

    public static final class string {
        public static final int about = 2130968581;
        public static final int about_changelog = 2130968579;
        public static final int about_close = 2130968580;
        public static final int app_intro = 2130968577;
        public static final int app_name = 2130968576;
        public static final int changelog = 2130968578;
        public static final int custom_search = 2130968583;
        public static final int custom_search_start = 2130968584;
        public static final int keyword = 2130968585;
        public static final int openSystemBrowser = 2130968589;
        public static final int openbrowser = 2130968588;
        public static final int save_SD = 2130968586;
        public static final int share = 2130968582;
        public static final int share_link = 2130968587;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.testing, R.attr.backgroundColor, R.attr.textColor, R.attr.keywords, R.attr.refreshInterval, R.attr.isGoneWithoutAd};
        public static final int com_admob_android_ads_AdView_backgroundColor = 1;
        public static final int com_admob_android_ads_AdView_isGoneWithoutAd = 5;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_testing = 0;
        public static final int com_admob_android_ads_AdView_textColor = 2;
    }
}
