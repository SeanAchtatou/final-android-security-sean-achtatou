package com.m41m41.sniperrifle2;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLDecoder;

public class ImageBrowserActivity extends Activity {
    public static final int DIALOG_WAIT_DOWNLOAD = 5;
    public static GoogleImageSearchParser parser;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.img_preview);
        parser = new GoogleImageSearchParser();
        parser.startPreview(this);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 5:
                ProgressDialog dialog = new ProgressDialog(this);
                dialog.setMessage(String.valueOf(URLDecoder.decode(getIntent().getStringExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD))) + " ...");
                dialog.setIndeterminate(true);
                dialog.setCancelable(true);
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        ImageBrowserActivity.parser.cancel();
                    }
                });
                return dialog;
            default:
                return null;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            RelativeLayout progress = (RelativeLayout) findViewById(R.id.RelativeLayout01);
            if (progress.isShown()) {
                parser.cancel();
                progress.setVisibility(4);
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        String souuceUrl = getIntent().getStringExtra(GoogleImageSearchParser.KEY_SOURCE_URL);
        String keyword = URLDecoder.decode(getIntent().getStringExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD));
        String start_number = getIntent().getStringExtra(GoogleImageSearchParser.KEY_STARTNUMBER);
        String sourcePage = getIntent().getStringExtra(GoogleImageSearchParser.KEY_SOURCE_PAGE);
        if (souuceUrl == null || sourcePage == null) {
            Toast.makeText(this, "No picture!\nclick next", 1).show();
            return false;
        }
        switch (item.getItemId()) {
            case R.id.share_link /*2131099664*/:
                Intent it = new Intent("android.intent.action.SEND");
                String subject = "picture of " + keyword;
                it.putExtra("android.intent.extra.SUBJECT", subject);
                it.putExtra("android.intent.extra.TEXT", String.valueOf(subject) + "\nURL:\n" + souuceUrl + "\n" + "source page:\n" + sourcePage);
                it.setType("text/plain");
                startActivity(Intent.createChooser(it, "Choose share format"));
                break;
            case R.id.save /*2131099668*/:
                savePicture(keyword, start_number);
                break;
            case R.id.openbrowser /*2131099669*/:
                Intent i = new Intent(this, BrowserActivity.class);
                i.putExtra(GoogleImageSearchParser.INTENT_STR_KEYWORD, keyword);
                i.setData(Uri.parse(sourcePage));
                startActivity(i);
                break;
        }
        return false;
    }

    public void savePicture(String keyword, String start_number) {
        Bitmap bp;
        ImageView imgVIew = (ImageView) findViewById(R.id.img_View);
        if (imgVIew != null && (bp = (Bitmap) imgVIew.getTag()) != null) {
            File dirFile = new File(Environment.getExternalStorageDirectory() + "/" + LoadHotSearchListTask.english_name);
            if (!dirFile.exists() && !dirFile.mkdir()) {
                Toast.makeText(this, "can not create dir:\n" + dirFile.getAbsolutePath(), 0).show();
            }
            File f = new File(Environment.getExternalStorageDirectory() + "/" + LoadHotSearchListTask.english_name + "/" + keyword + "_" + start_number + "_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
            try {
                FileOutputStream fileOS = new FileOutputStream(f);
                bp.compress(Bitmap.CompressFormat.JPEG, 100, fileOS);
                fileOS.close();
                Toast.makeText(this, "save ok\n" + f.getAbsolutePath(), 1).show();
            } catch (FileNotFoundException | IOException e) {
                Toast.makeText(this, "can not save to:\n" + f.getAbsolutePath(), 1).show();
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.picture_save_menu, menu);
        return true;
    }
}
