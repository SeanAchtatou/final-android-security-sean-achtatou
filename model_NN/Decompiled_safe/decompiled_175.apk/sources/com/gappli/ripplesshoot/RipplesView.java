package com.gappli.ripplesshoot;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.lang.reflect.Array;
import java.util.Random;

public class RipplesView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    private static final int S_GOAL1 = 5;
    private static final int S_GOAL2 = 6;
    private static final int S_MENU = 1;
    private static final int S_OVER = 7;
    private static final int S_OVER3 = 8;
    private static final int S_PLAY = 4;
    private static final int S_START = 2;
    private static final int S_START1 = 3;
    private static final int S_TITLE = 0;
    private static int chcP;
    private static int init;
    private static int scoreG;
    private static int[] scoreH = new int[S_START];
    private int backC;
    private double ballA;
    private int ballG;
    private int ballH;
    private int ballH2;
    private int ballL;
    private int ballM;
    private int ballR;
    private int ballS;
    private int ballX;
    private int ballX2;
    private int ballX3;
    private int ballXB;
    private int ballY;
    private int ballY2;
    private int ballY3;
    private int ballYB;
    private Bitmap[] bmp = new Bitmap[56];
    private int chcX;
    private int gkG;
    private int gkG2;
    private int gkL;
    private int gkL2;
    private int gkM;
    private int gkS;
    private int gkT;
    private int gkX;
    private int gkX2;
    private int gkY;
    private int gkY2;
    private int goalG;
    private int hitX;
    private int hitY;
    private SurfaceHolder holder;
    private int i;
    private int j;
    private int k;
    private int keyX;
    private int keyY;
    private int menuY;
    private int missC;
    private int possP;
    private int possY;
    private Random rand;
    private int ripC;
    private int ripM;
    private int[] ripT;
    private int ripX;
    private int ripY;
    private double rollA;
    private int rollG;
    private int rollM;
    private int scene;
    private int[] scoreGG;
    private int[][] scoreHG;
    private int scoreI;
    private int screenX;
    private int screenY;
    private Thread thread;
    private boolean touchDown;

    public RipplesView(Context context) {
        super(context);
        int[] iArr = new int[S_OVER];
        iArr[S_MENU] = S_GOAL1;
        iArr[S_START] = 20;
        iArr[S_START1] = 30;
        iArr[S_PLAY] = 40;
        iArr[S_GOAL1] = 40;
        iArr[S_GOAL2] = 40;
        this.ripT = iArr;
        this.scoreGG = new int[S_START1];
        this.scoreHG = (int[][]) Array.newInstance(Integer.TYPE, S_START, S_START1);
        init = 0;
        this.rand = new Random();
        SharedPreferences pref = context.getSharedPreferences("PreferencesEx", 0);
        scoreH[0] = pref.getInt("hiscore1", 0);
        scoreH[S_MENU] = pref.getInt("hiscore2", 0);
        for (int i2 = 0; i2 < 56; i2 += S_MENU) {
            this.bmp[i2] = readBitmap(context, "r" + i2);
        }
        this.holder = getHolder();
        this.holder.addCallback(this);
        this.holder.setFixedSize(getWidth(), getHeight());
    }

    public void surfaceCreated(SurfaceHolder holder2) {
        this.thread = new Thread(this);
        this.thread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder2) {
        this.thread = null;
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int w, int h) {
    }

    /* JADX WARN: Type inference failed for: r14v0, types: [com.gappli.ripplesshoot.RipplesView] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void run() {
        /*
            r14 = this;
            android.graphics.Paint r5 = new android.graphics.Paint
            r5.<init>()
        L_0x0005:
            java.lang.Thread r1 = r14.thread
            if (r1 != 0) goto L_0x000a
            return
        L_0x000a:
            int r1 = r14.getWidth()
            r2 = 480(0x1e0, float:6.73E-43)
            int r1 = r1 - r2
            int r6 = r1 / 2
            int r1 = r14.getHeight()
            r2 = 800(0x320, float:1.121E-42)
            int r1 = r1 - r2
            int r7 = r1 / 2
            int r1 = com.gappli.ripplesshoot.RipplesView.init
            if (r1 < 0) goto L_0x0046
            int r1 = com.gappli.ripplesshoot.RipplesView.init
            r14.scene = r1
            r1 = -1
            com.gappli.ripplesshoot.RipplesView.init = r1
            int r1 = r14.scene
            if (r1 != 0) goto L_0x078e
            r1 = 100
            r14.screenX = r1
            r1 = 100
            r14.screenY = r1
            r1 = 70
            int r1 = r6 - r1
            r14.ballX = r1
            int r1 = r7 + 300
            r2 = 8
            int r1 = r1 - r2
            r14.ballY = r1
            r1 = 0
            r14.ballS = r1
            r1 = 0
            r14.ballG = r1
        L_0x0046:
            int[] r1 = r14.scoreGG
            r2 = 0
            int r3 = com.gappli.ripplesshoot.RipplesView.scoreG
            int r3 = r3 % 10
            r1[r2] = r3
            int r1 = com.gappli.ripplesshoot.RipplesView.scoreG
            r2 = 10
            if (r1 < r2) goto L_0x0984
            int[] r1 = r14.scoreGG
            r2 = 1
            int r3 = com.gappli.ripplesshoot.RipplesView.scoreG
            int r3 = r3 / 10
            int r3 = r3 % 10
            r1[r2] = r3
        L_0x0060:
            int r1 = com.gappli.ripplesshoot.RipplesView.scoreG
            r2 = 100
            if (r1 < r2) goto L_0x098c
            int[] r1 = r14.scoreGG
            r2 = 2
            int r3 = com.gappli.ripplesshoot.RipplesView.scoreG
            int r3 = r3 / 100
            int r3 = r3 % 10
            r1[r2] = r3
        L_0x0071:
            int r1 = r14.scene
            if (r1 != 0) goto L_0x09a9
            int r1 = r14.screenY
            if (r1 <= 0) goto L_0x0994
            int r1 = r14.screenY
            r2 = 10
            int r1 = r1 - r2
            r14.screenY = r1
        L_0x0080:
            int r1 = r14.ballS
            r2 = 10
            if (r1 <= r2) goto L_0x009a
            int r1 = r14.ballG
            r2 = 2
            if (r1 <= r2) goto L_0x099c
            r1 = 0
            r14.ballG = r1
        L_0x008e:
            int r1 = r14.ballX
            r2 = 560(0x230, float:7.85E-43)
            if (r1 >= r2) goto L_0x09a4
            int r1 = r14.ballX
            int r1 = r1 + 10
            r14.ballX = r1
        L_0x009a:
            int r1 = r14.scene
            r2 = 4
            if (r1 != r2) goto L_0x0279
            boolean r1 = r14.touchDown
            if (r1 == 0) goto L_0x00c2
            int r1 = r14.ripM
            r2 = -1
            if (r1 != r2) goto L_0x00c2
            int r1 = r14.keyX
            int r2 = r6 + 75
            if (r1 <= r2) goto L_0x00c2
            int r1 = r14.keyX
            int r2 = r6 + 480
            if (r1 >= r2) goto L_0x00c2
            r1 = 1
            r14.ripM = r1
            int r1 = r14.keyX
            r14.ripX = r1
            int r1 = r14.keyY
            r14.ripY = r1
            r1 = 0
            r14.touchDown = r1
        L_0x00c2:
            int r1 = r14.ballH
            int r2 = r14.ballH2
            if (r1 >= r2) goto L_0x0dc8
            int r1 = r14.ballH
            int r1 = r1 + 1
            r14.ballH = r1
        L_0x00ce:
            int r1 = r14.ballX
            int r2 = r6 + 77
            int r1 = r1 - r2
            r14.hitX = r1
            int r1 = r14.ballY
            int r2 = r7 + 361
            int r1 = r1 - r2
            r14.hitY = r1
            int r1 = r14.hitX
            int r2 = r14.hitX
            int r1 = r1 * r2
            int r2 = r14.hitY
            int r3 = r14.hitY
            int r2 = r2 * r3
            int r1 = r1 + r2
            r2 = 1024(0x400, float:1.435E-42)
            if (r1 >= r2) goto L_0x011a
            r1 = -1
            r14.ripC = r1
            int r1 = r14.hitY
            double r1 = (double) r1
            int r3 = r14.hitX
            double r3 = (double) r3
            double r1 = java.lang.Math.atan2(r1, r3)
            r14.ballA = r1
            int r1 = r6 + 77
            double r1 = (double) r1
            r3 = 4629137466983448576(0x403e000000000000, double:30.0)
            double r8 = r14.ballA
            double r8 = java.lang.Math.cos(r8)
            double r3 = r3 * r8
            double r1 = r1 + r3
            int r1 = (int) r1
            r14.ballX = r1
            int r1 = r7 + 361
            double r1 = (double) r1
            r3 = 4629137466983448576(0x403e000000000000, double:30.0)
            double r8 = r14.ballA
            double r8 = java.lang.Math.sin(r8)
            double r3 = r3 * r8
            double r1 = r1 + r3
            int r1 = (int) r1
            r14.ballY = r1
        L_0x011a:
            int r1 = r14.ballX
            int r2 = r6 + 77
            int r1 = r1 - r2
            r14.hitX = r1
            int r1 = r14.ballY
            int r2 = r7 + 361
            int r2 = r2 + 307
            int r1 = r1 - r2
            r14.hitY = r1
            int r1 = r14.hitX
            int r2 = r14.hitX
            int r1 = r1 * r2
            int r2 = r14.hitY
            int r3 = r14.hitY
            int r2 = r2 * r3
            int r1 = r1 + r2
            r2 = 900(0x384, float:1.261E-42)
            if (r1 > r2) goto L_0x016a
            r1 = -1
            r14.ripC = r1
            int r1 = r14.hitY
            double r1 = (double) r1
            int r3 = r14.hitX
            double r3 = (double) r3
            double r1 = java.lang.Math.atan2(r1, r3)
            r14.ballA = r1
            int r1 = r6 + 77
            double r1 = (double) r1
            r3 = 4629137466983448576(0x403e000000000000, double:30.0)
            double r8 = r14.ballA
            double r8 = java.lang.Math.cos(r8)
            double r3 = r3 * r8
            double r1 = r1 + r3
            int r1 = (int) r1
            r14.ballX = r1
            int r1 = r7 + 361
            int r1 = r1 + 307
            double r1 = (double) r1
            r3 = 4629137466983448576(0x403e000000000000, double:30.0)
            double r8 = r14.ballA
            double r8 = java.lang.Math.sin(r8)
            double r3 = r3 * r8
            double r1 = r1 + r3
            int r1 = (int) r1
            r14.ballY = r1
        L_0x016a:
            int r1 = r14.ballX
            int r2 = r6 + 77
            if (r1 >= r2) goto L_0x0186
            int r1 = r14.ballY
            int r2 = r7 + 361
            int r2 = r2 + 30
            if (r1 < r2) goto L_0x0dfc
            int r1 = r14.ballY
            int r2 = r7 + 361
            int r2 = r2 + 307
            r3 = 30
            int r2 = r2 - r3
            if (r1 > r2) goto L_0x0dfc
            r1 = 5
            com.gappli.ripplesshoot.RipplesView.init = r1
        L_0x0186:
            int r1 = r14.ballX
            int r2 = r6 + 520
            if (r1 > r2) goto L_0x019d
            int r1 = r14.ballY
            int r2 = r7 + 76
            r3 = 30
            int r2 = r2 - r3
            if (r1 <= r2) goto L_0x019d
            int r1 = r14.ballY
            int r2 = r7 + 950
            int r2 = r2 + 30
            if (r1 < r2) goto L_0x01a3
        L_0x019d:
            r1 = 0
            r14.ballS = r1
            r1 = 7
            com.gappli.ripplesshoot.RipplesView.init = r1
        L_0x01a3:
            int r1 = r14.ripC
            r2 = 1
            if (r1 != r2) goto L_0x01f9
            r1 = 0
            r14.ripC = r1
            int r1 = r14.gkX
            int r1 = r1 + 43
            r14.ballX3 = r1
            double r1 = r14.ballA
            double r1 = java.lang.Math.tan(r1)
            int r3 = r14.ballX3
            int r4 = r14.ballXB
            int r3 = r3 - r4
            double r3 = (double) r3
            double r1 = r1 * r3
            int r3 = r14.ballYB
            double r3 = (double) r3
            double r1 = r1 + r3
            int r1 = (int) r1
            r14.ballY3 = r1
            int r1 = r14.ballX3
            int r2 = r14.ballX
            int r1 = r1 - r2
            int r2 = r14.ballX3
            int r3 = r14.ballX
            int r2 = r2 - r3
            int r1 = r1 * r2
            int r2 = r14.ballY3
            int r3 = r14.ballY
            int r2 = r2 - r3
            int r3 = r14.ballY3
            int r4 = r14.ballY
            int r3 = r3 - r4
            int r2 = r2 * r3
            int r1 = r1 + r2
            double r1 = (double) r1
            double r1 = java.lang.Math.sqrt(r1)
            int r1 = (int) r1
            r14.gkL = r1
            int r1 = r14.ballS
            if (r1 <= 0) goto L_0x01f1
            int r1 = r14.gkL
            int r2 = r14.ballS
            int r2 = r2 * 2
            int r1 = r1 / r2
            r14.gkT = r1
        L_0x01f1:
            int r1 = r14.ballS
            r2 = 4
            if (r1 <= r2) goto L_0x0e0d
            r1 = 2
            r14.gkS = r1
        L_0x01f9:
            int r1 = r14.gkM
            if (r1 != 0) goto L_0x020b
            int r1 = r14.ripC
            r2 = -1
            if (r1 != r2) goto L_0x0e1e
            int r1 = r14.gkY
            int r2 = r14.ballY
            if (r1 >= r2) goto L_0x0e13
            r1 = 1
            r14.gkM = r1
        L_0x020b:
            int r1 = r14.gkG2
            r2 = 3
            if (r1 == r2) goto L_0x0215
            int r1 = r14.gkG2
            r2 = 4
            if (r1 != r2) goto L_0x0eb8
        L_0x0215:
            int r1 = r14.ballX
            int r2 = r14.gkX
            int r2 = r2 + 41
            int r1 = r1 - r2
            r14.gkX2 = r1
            int r1 = r14.ballY
            int r2 = r14.gkY
            int r1 = r1 - r2
            r14.gkY2 = r1
            int r1 = r14.gkX2
            int r2 = r14.gkX2
            int r1 = r1 * r2
            int r2 = r14.gkY2
            int r3 = r14.gkY2
            int r2 = r2 * r3
            int r1 = r1 + r2
            r14.gkL2 = r1
            int r1 = r14.gkL2
            r2 = 1600(0x640, float:2.242E-42)
            if (r1 > r2) goto L_0x0247
            int r1 = r14.gkY2
            double r1 = (double) r1
            int r3 = r14.gkX2
            double r3 = (double) r3
            double r1 = java.lang.Math.atan2(r1, r3)
            r14.ballA = r1
            r1 = -1
            r14.ripC = r1
        L_0x0247:
            int r1 = r14.ballX
            int r2 = r14.gkX
            int r2 = r2 + 25
            int r1 = r1 - r2
            r14.gkX2 = r1
            int r1 = r14.ballY
            int r2 = r14.gkY
            int r1 = r1 - r2
            r14.gkY2 = r1
            int r1 = r14.gkX2
            int r2 = r14.gkX2
            int r1 = r1 * r2
            int r2 = r14.gkY2
            int r3 = r14.gkY2
            int r2 = r2 * r3
            int r1 = r1 + r2
            r14.gkL2 = r1
            int r1 = r14.gkL2
            r2 = 2500(0x9c4, float:3.503E-42)
            if (r1 > r2) goto L_0x0279
            int r1 = r14.gkY2
            double r1 = (double) r1
            int r3 = r14.gkX2
            double r3 = (double) r3
            double r1 = java.lang.Math.atan2(r1, r3)
            r14.ballA = r1
            r1 = -1
            r14.ripC = r1
        L_0x0279:
            int r1 = r14.scene
            r2 = 4
            if (r1 < r2) goto L_0x02b8
            int r1 = r14.scene
            r2 = 8
            if (r1 > r2) goto L_0x02b8
            int r1 = r14.gkM
            if (r1 != 0) goto L_0x0f3a
            int r1 = r14.gkG
            if (r1 == 0) goto L_0x029e
            int r1 = r14.gkG
            int r1 = java.lang.Math.abs(r1)
            r14.gkG = r1
            int r1 = r14.gkG
            if (r1 <= 0) goto L_0x029e
            int r1 = r14.gkG
            r2 = 1
            int r1 = r1 - r2
            r14.gkG = r1
        L_0x029e:
            int r1 = r14.gkG
            r14.gkG2 = r1
        L_0x02a2:
            int r1 = r14.gkM
            r2 = -1
            if (r1 < r2) goto L_0x02b8
            int r1 = r14.gkM
            r2 = 1
            if (r1 > r2) goto L_0x02b8
            int r1 = r14.gkX
            int r2 = r6 + 90
            if (r1 <= r2) goto L_0x107b
            int r1 = r14.gkX
            r2 = 2
            int r1 = r1 - r2
            r14.gkX = r1
        L_0x02b8:
            int r1 = r14.scene
            r2 = 5
            if (r1 != r2) goto L_0x1097
            int r1 = r14.ballY
            int r2 = r7 + 361
            int r2 = r2 + 30
            if (r1 > r2) goto L_0x1081
            int r1 = r7 + 361
            int r1 = r1 + 30
            r14.ballY = r1
        L_0x02cb:
            int r1 = r14.ballX
            int r2 = r6 + 40
            if (r1 >= r2) goto L_0x02db
            int r1 = r6 + 40
            r14.ballX = r1
            r1 = 0
            r14.ballS = r1
            r1 = 6
            com.gappli.ripplesshoot.RipplesView.init = r1
        L_0x02db:
            android.view.SurfaceHolder r1 = r14.holder
            android.graphics.Canvas r0 = r1.lockCanvas()
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r5.setStyle(r1)
            r1 = 255(0xff, float:3.57E-43)
            r2 = 0
            r3 = 0
            r4 = 0
            int r1 = android.graphics.Color.argb(r1, r2, r3, r4)
            r5.setColor(r1)
            float r1 = (float) r6
            float r2 = (float) r7
            int r3 = r6 + 480
            float r3 = (float) r3
            int r4 = r7 + 800
            float r4 = (float) r4
            r0.drawRect(r1, r2, r3, r4, r5)
            int r1 = r14.scene
            if (r1 != 0) goto L_0x1147
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r5.setStyle(r1)
            r1 = 255(0xff, float:3.57E-43)
            r2 = 255(0xff, float:3.57E-43)
            r3 = 255(0xff, float:3.57E-43)
            r4 = 255(0xff, float:3.57E-43)
            int r1 = android.graphics.Color.argb(r1, r2, r3, r4)
            r5.setColor(r1)
            float r1 = (float) r6
            float r2 = (float) r7
            int r3 = r6 + 480
            float r3 = (float) r3
            int r4 = r7 + 800
            float r4 = (float) r4
            r0.drawRect(r1, r2, r3, r4, r5)
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 22
            r1 = r1[r2]
            int r2 = r6 + 80
            float r2 = (float) r2
            int r3 = r7 + 300
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r5.setStyle(r1)
            r1 = 255(0xff, float:3.57E-43)
            r2 = 255(0xff, float:3.57E-43)
            r3 = 255(0xff, float:3.57E-43)
            r4 = 255(0xff, float:3.57E-43)
            int r1 = android.graphics.Color.argb(r1, r2, r3, r4)
            r5.setColor(r1)
            int r1 = r6 + 80
            float r1 = (float) r1
            int r2 = r7 + 400
            float r2 = (float) r2
            int r3 = r6 + 80
            int r3 = r3 + 320
            float r3 = (float) r3
            int r4 = r7 + 400
            int r4 = r4 + 100
            float r4 = (float) r4
            r0.drawRect(r1, r2, r3, r4, r5)
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r5.setStyle(r1)
            r1 = 255(0xff, float:3.57E-43)
            r2 = 255(0xff, float:3.57E-43)
            r3 = 255(0xff, float:3.57E-43)
            r4 = 255(0xff, float:3.57E-43)
            int r1 = android.graphics.Color.argb(r1, r2, r3, r4)
            r5.setColor(r1)
            float r1 = (float) r6
            int r2 = r7 + 300
            float r2 = (float) r2
            int r3 = r14.ballX
            int r3 = r3 + 30
            float r3 = (float) r3
            int r4 = r7 + 300
            int r4 = r4 + 50
            float r4 = (float) r4
            r0.drawRect(r1, r2, r3, r4, r5)
            r1 = 430(0x1ae, float:6.03E-43)
            int r2 = r14.ballX
            int r1 = r1 - r2
            int r1 = r1 + 30
            float r1 = (float) r1
            int r2 = r7 + 300
            int r2 = r2 + 50
            float r2 = (float) r2
            int r3 = r6 + 480
            float r3 = (float) r3
            int r4 = r7 + 300
            int r4 = r4 + 51
            int r4 = r4 + 59
            float r4 = (float) r4
            r0.drawRect(r1, r2, r3, r4, r5)
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r5.setStyle(r1)
            r1 = 100
            r2 = 0
            r3 = 0
            r4 = 0
            int r1 = android.graphics.Color.argb(r1, r2, r3, r4)
            r5.setColor(r1)
            int r1 = r14.ballX
            int r1 = r1 + 30
            r2 = 4
            int r1 = r1 - r2
            float r1 = (float) r1
            int r2 = r14.ballY
            int r2 = r2 + 30
            int r2 = r2 + 4
            float r2 = (float) r2
            r3 = 1106247680(0x41f00000, float:30.0)
            r0.drawCircle(r1, r2, r3, r5)
            r1 = 430(0x1ae, float:6.03E-43)
            int r2 = r14.ballX
            int r1 = r1 - r2
            int r1 = r1 + 30
            r2 = 4
            int r1 = r1 - r2
            float r1 = (float) r1
            int r2 = r14.ballY
            int r2 = r2 + 30
            int r2 = r2 + 60
            int r2 = r2 + 4
            float r2 = (float) r2
            r3 = 1106247680(0x41f00000, float:30.0)
            r0.drawCircle(r1, r2, r3, r5)
            android.graphics.Bitmap[] r1 = r14.bmp
            int r2 = r14.ballG
            int r2 = r2 + 18
            r1 = r1[r2]
            int r2 = r14.ballX
            float r2 = (float) r2
            int r3 = r14.ballY
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 21
            int r3 = r14.ballG
            int r2 = r2 - r3
            r1 = r1[r2]
            r2 = 430(0x1ae, float:6.03E-43)
            int r3 = r14.ballX
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r14.ballY
            int r3 = r3 + 60
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
        L_0x03ff:
            int r1 = r14.scene
            r2 = 1
            if (r1 < r2) goto L_0x071e
            android.graphics.Bitmap[] r1 = r14.bmp
            int r2 = r14.gkG2
            r1 = r1[r2]
            int r2 = r14.gkX
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r14.gkY
            r4 = 53
            int r3 = r3 - r4
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 1
            if (r1 != r2) goto L_0x0450
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 23
            r1 = r1[r2]
            int r2 = r6 + 16
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r7 + 16
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 23
            r1 = r1[r2]
            int r2 = r6 + 16
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r7 + 16
            int r3 = r3 + 874
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
        L_0x0450:
            int r1 = r14.ballL
            if (r1 >= 0) goto L_0x0488
            int r1 = r14.ballH
            if (r1 < 0) goto L_0x0488
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r5.setStyle(r1)
            r1 = 100
            r2 = 0
            r3 = 0
            r4 = 0
            int r1 = android.graphics.Color.argb(r1, r2, r3, r4)
            r5.setColor(r1)
            int r1 = r14.ballX
            int r2 = r14.ballH
            int r2 = r2 * 2
            int r2 = r2 + 4
            int r1 = r1 - r2
            int r2 = r14.screenX
            int r1 = r1 - r2
            float r1 = (float) r1
            int r2 = r14.ballY
            int r3 = r14.ballH
            int r3 = r3 * 2
            int r3 = r3 + 4
            int r2 = r2 + r3
            int r3 = r14.screenY
            int r2 = r2 + r3
            float r2 = (float) r2
            r3 = 1106247680(0x41f00000, float:30.0)
            r0.drawCircle(r1, r2, r3, r5)
        L_0x0488:
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 1
            if (r1 != r2) goto L_0x05c9
            android.graphics.Bitmap[] r1 = r14.bmp
            int r2 = r14.rollG
            int r2 = r2 + 24
            r1 = r1[r2]
            r2 = 12
            int r2 = r6 - r2
            int r2 = r2 + 16
            int r2 = r2 + 61
            r3 = 45
            int r4 = r14.rollM
            int r3 = r3 - r4
            double r3 = (double) r3
            double r8 = r14.rollA
            r10 = 4609749470587618591(0x3ff91eb851eb851f, double:1.57)
            double r8 = r8 + r10
            double r8 = java.lang.Math.cos(r8)
            double r3 = r3 * r8
            int r3 = (int) r3
            int r2 = r2 - r3
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 12
            int r3 = r7 - r3
            int r3 = r3 + 16
            int r3 = r3 + 61
            r4 = 45
            int r8 = r14.rollM
            int r4 = r4 - r8
            double r8 = (double) r4
            double r10 = r14.rollA
            r12 = 4609749470587618591(0x3ff91eb851eb851f, double:1.57)
            double r10 = r10 + r12
            double r10 = java.lang.Math.sin(r10)
            double r8 = r8 * r10
            int r4 = (int) r8
            int r3 = r3 - r4
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            android.graphics.Bitmap[] r1 = r14.bmp
            int r2 = r14.rollG
            int r2 = r2 + 24
            r1 = r1[r2]
            r2 = 12
            int r2 = r6 - r2
            int r2 = r2 + 16
            int r2 = r2 + 61
            r3 = 45
            int r4 = r14.rollM
            int r3 = r3 - r4
            double r3 = (double) r3
            double r8 = r14.rollA
            r10 = 4609749470587618591(0x3ff91eb851eb851f, double:1.57)
            double r8 = r8 + r10
            double r8 = java.lang.Math.cos(r8)
            double r3 = r3 * r8
            int r3 = (int) r3
            int r2 = r2 + r3
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 12
            int r3 = r7 - r3
            int r3 = r3 + 16
            int r3 = r3 + 61
            r4 = 45
            int r8 = r14.rollM
            int r4 = r4 - r8
            double r8 = (double) r4
            double r10 = r14.rollA
            r12 = 4609749470587618591(0x3ff91eb851eb851f, double:1.57)
            double r10 = r10 + r12
            double r10 = java.lang.Math.sin(r10)
            double r8 = r8 * r10
            int r4 = (int) r8
            int r3 = r3 + r4
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            android.graphics.Bitmap[] r1 = r14.bmp
            int r2 = r14.rollG
            int r2 = r2 + 24
            r1 = r1[r2]
            r2 = 12
            int r2 = r6 - r2
            int r2 = r2 + 16
            int r2 = r2 + 61
            r3 = 45
            int r4 = r14.rollM
            int r3 = r3 - r4
            double r3 = (double) r3
            double r8 = r14.rollA
            r10 = 4609749470587618591(0x3ff91eb851eb851f, double:1.57)
            double r8 = r8 + r10
            double r8 = java.lang.Math.cos(r8)
            double r3 = r3 * r8
            int r3 = (int) r3
            int r2 = r2 - r3
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 12
            int r3 = r7 - r3
            int r3 = r3 + 16
            int r3 = r3 + 61
            int r3 = r3 + 874
            r4 = 45
            int r8 = r14.rollM
            int r4 = r4 - r8
            double r8 = (double) r4
            double r10 = r14.rollA
            r12 = 4609749470587618591(0x3ff91eb851eb851f, double:1.57)
            double r10 = r10 + r12
            double r10 = java.lang.Math.sin(r10)
            double r8 = r8 * r10
            int r4 = (int) r8
            int r3 = r3 - r4
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            android.graphics.Bitmap[] r1 = r14.bmp
            int r2 = r14.rollG
            int r2 = r2 + 24
            r1 = r1[r2]
            r2 = 12
            int r2 = r6 - r2
            int r2 = r2 + 16
            int r2 = r2 + 61
            r3 = 45
            int r4 = r14.rollM
            int r3 = r3 - r4
            double r3 = (double) r3
            double r8 = r14.rollA
            r10 = 4609749470587618591(0x3ff91eb851eb851f, double:1.57)
            double r8 = r8 + r10
            double r8 = java.lang.Math.cos(r8)
            double r3 = r3 * r8
            int r3 = (int) r3
            int r2 = r2 + r3
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            r3 = 12
            int r3 = r7 - r3
            int r3 = r3 + 16
            int r3 = r3 + 61
            int r3 = r3 + 874
            r4 = 45
            int r8 = r14.rollM
            int r4 = r4 - r8
            double r8 = (double) r4
            double r10 = r14.rollA
            r12 = 4609749470587618591(0x3ff91eb851eb851f, double:1.57)
            double r10 = r10 + r12
            double r10 = java.lang.Math.sin(r10)
            double r8 = r8 * r10
            int r4 = (int) r8
            int r3 = r3 + r4
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
        L_0x05c9:
            int r1 = r14.ripM
            if (r1 <= 0) goto L_0x05e5
            android.graphics.Bitmap[] r1 = r14.bmp
            int r2 = r14.ripM
            int r2 = r2 + 10
            r1 = r1[r2]
            int r2 = r14.ripX
            r3 = 41
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r14.ripY
            r4 = 41
            int r3 = r3 - r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
        L_0x05e5:
            int r1 = r14.ballL
            if (r1 < 0) goto L_0x0635
            android.graphics.Rect r9 = new android.graphics.Rect
            r1 = 0
            r2 = 0
            r3 = 59
            r4 = 59
            r9.<init>(r1, r2, r3, r4)
            android.graphics.Rect r8 = new android.graphics.Rect
            int r1 = r14.ballX2
            r2 = 6
            int r3 = r14.ballL
            int r2 = r2 - r3
            int r2 = r2 * 5
            int r2 = r2 / 2
            int r1 = r1 - r2
            int r2 = r14.ballY2
            r3 = 6
            int r4 = r14.ballL
            int r3 = r3 - r4
            int r3 = r3 * 5
            int r3 = r3 / 2
            int r2 = r2 - r3
            int r3 = r14.screenY
            int r2 = r2 + r3
            int r3 = r14.ballX2
            r4 = 6
            int r10 = r14.ballL
            int r4 = r4 - r10
            int r4 = r4 * 5
            int r3 = r3 + r4
            r4 = 1
            int r3 = r3 - r4
            int r4 = r14.ballY2
            r10 = 6
            int r11 = r14.ballL
            int r10 = r10 - r11
            int r10 = r10 * 5
            int r4 = r4 + r10
            r10 = 1
            int r4 = r4 - r10
            int r10 = r14.screenY
            int r4 = r4 + r10
            r8.<init>(r1, r2, r3, r4)
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 18
            r1 = r1[r2]
            r2 = 0
            r0.drawBitmap(r1, r9, r8, r2)
        L_0x0635:
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 2
            if (r1 != r2) goto L_0x12b6
            int r1 = r14.scene
            r2 = 2
            if (r1 != r2) goto L_0x12b6
            int r1 = r14.ballL
            if (r1 < 0) goto L_0x12b6
            android.graphics.Rect r9 = new android.graphics.Rect
            r1 = 0
            r2 = 0
            r3 = 59
            r4 = 59
            r9.<init>(r1, r2, r3, r4)
            android.graphics.Rect r8 = new android.graphics.Rect
            int r1 = r14.ballX
            int r2 = r14.ballL
            int r2 = r2 * 5
            int r1 = r1 - r2
            int r2 = r14.ballY
            int r3 = r14.ballL
            int r3 = r3 * 5
            int r2 = r2 - r3
            int r3 = r14.screenY
            int r2 = r2 + r3
            int r3 = r14.ballX
            int r4 = r14.ballL
            int r4 = r4 * 5
            int r3 = r3 + r4
            r4 = 1
            int r3 = r3 - r4
            int r4 = r14.ballY
            int r10 = r14.ballL
            int r10 = r10 * 5
            int r4 = r4 + r10
            r10 = 1
            int r4 = r4 - r10
            int r10 = r14.screenY
            int r4 = r4 + r10
            r8.<init>(r1, r2, r3, r4)
            android.graphics.Bitmap[] r1 = r14.bmp
            int r2 = r14.ballG
            int r2 = r2 + 18
            r1 = r1[r2]
            r2 = 0
            r0.drawBitmap(r1, r9, r8, r2)
        L_0x0685:
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 10
            r1 = r1[r2]
            int r2 = r6 + 8
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r7 + 361
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            r1 = 1
            r14.j = r1
            r1 = 0
            r14.i = r1
        L_0x06a1:
            int r1 = r14.i
            r2 = 3
            if (r1 < r2) goto L_0x12d6
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 1
            if (r1 == r2) goto L_0x06b0
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 2
            if (r1 != r2) goto L_0x06ee
        L_0x06b0:
            int r1 = com.gappli.ripplesshoot.RipplesView.scoreG
            int[] r2 = com.gappli.ripplesshoot.RipplesView.scoreH
            int r3 = com.gappli.ripplesshoot.RipplesView.chcP
            r4 = 1
            int r3 = r3 - r4
            r2 = r2[r3]
            if (r1 <= r2) goto L_0x06ee
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 51
            r1 = r1[r2]
            int r2 = r6 + 137
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r7 + 41
            r4 = 30
            int r3 = r3 - r4
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 51
            r1 = r1[r2]
            int r2 = r6 + 137
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r7 + 983
            r4 = 30
            int r3 = r3 - r4
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
        L_0x06ee:
            android.graphics.Bitmap[] r1 = r14.bmp
            int r2 = r14.missC
            int r2 = r2 + 41
            r1 = r1[r2]
            int r2 = r6 + 420
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r7 + 23
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            android.graphics.Bitmap[] r1 = r14.bmp
            int r2 = r14.missC
            int r2 = r2 + 41
            r1 = r1[r2]
            int r2 = r6 + 420
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r7 + 965
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
        L_0x071e:
            int r1 = r14.scene
            r2 = 2
            if (r1 != r2) goto L_0x1330
            int r1 = r14.gkG2
            r2 = 35
            if (r1 != r2) goto L_0x073f
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 29
            r1 = r1[r2]
            int r2 = r6 + 200
            float r2 = (float) r2
            int r3 = r14.gkY
            r4 = 41
            int r3 = r3 - r4
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
        L_0x073f:
            r1 = 255(0xff, float:3.57E-43)
            r2 = 0
            r3 = 0
            r4 = 0
            int r1 = android.graphics.Color.argb(r1, r2, r3, r4)
            r5.setColor(r1)
            r1 = 0
            r2 = 0
            int r3 = r14.getWidth()
            float r3 = (float) r3
            float r4 = (float) r7
            r0.drawRect(r1, r2, r3, r4, r5)
            r1 = 0
            int r2 = r7 + 800
            float r2 = (float) r2
            int r3 = r14.getWidth()
            float r3 = (float) r3
            int r4 = r14.getHeight()
            float r4 = (float) r4
            r0.drawRect(r1, r2, r3, r4, r5)
            r1 = 0
            float r2 = (float) r7
            float r3 = (float) r6
            int r4 = r7 + 800
            float r4 = (float) r4
            r0.drawRect(r1, r2, r3, r4, r5)
            int r1 = r6 + 480
            float r1 = (float) r1
            float r2 = (float) r7
            int r3 = r14.getWidth()
            float r3 = (float) r3
            int r4 = r7 + 800
            float r4 = (float) r4
            r0.drawRect(r1, r2, r3, r4, r5)
            android.view.SurfaceHolder r1 = r14.holder
            r1.unlockCanvasAndPost(r0)
            r1 = 50
            java.lang.Thread.sleep(r1)     // Catch:{ Exception -> 0x078b }
            goto L_0x0005
        L_0x078b:
            r1 = move-exception
            goto L_0x0005
        L_0x078e:
            int r1 = r14.scene
            r2 = 1
            if (r1 != r2) goto L_0x083b
            r1 = 255(0xff, float:3.57E-43)
            r14.backC = r1
            r1 = 420(0x1a4, float:5.89E-43)
            r14.menuY = r1
            r1 = 100
            r14.screenX = r1
            r1 = -127(0xffffffffffffff81, float:NaN)
            r14.screenY = r1
            int r1 = r6 + 76
            r14.ballX = r1
            int r1 = r7 + 76
            r14.ballY = r1
            r1 = -100
            r14.ballX2 = r1
            r1 = -100
            r14.ballY2 = r1
            r1 = 0
            r14.ballL = r1
            int r1 = r6 + 25
            r14.gkX = r1
            int r1 = r7 + 512
            r2 = 10
            int r1 = r1 - r2
            r14.gkY = r1
            r1 = 0
            r14.rollA = r1
            r1 = 0
            com.gappli.ripplesshoot.RipplesView.chcP = r1
            r1 = 0
            r14.chcX = r1
            r1 = 0
            r14.i = r1
        L_0x07ce:
            int r1 = r14.i
            r2 = 2
            if (r1 >= r2) goto L_0x0046
            int[][] r1 = r14.scoreHG
            int r2 = r14.i
            r1 = r1[r2]
            r2 = 0
            int[] r3 = com.gappli.ripplesshoot.RipplesView.scoreH
            int r4 = r14.i
            r3 = r3[r4]
            int r3 = r3 % 10
            r1[r2] = r3
            int[] r1 = com.gappli.ripplesshoot.RipplesView.scoreH
            int r2 = r14.i
            r1 = r1[r2]
            r2 = 10
            if (r1 < r2) goto L_0x0825
            int[][] r1 = r14.scoreHG
            int r2 = r14.i
            r1 = r1[r2]
            r2 = 1
            int[] r3 = com.gappli.ripplesshoot.RipplesView.scoreH
            int r4 = r14.i
            r3 = r3[r4]
            int r3 = r3 / 10
            int r3 = r3 % 10
            r1[r2] = r3
        L_0x0801:
            int[] r1 = com.gappli.ripplesshoot.RipplesView.scoreH
            int r2 = r14.i
            r1 = r1[r2]
            r2 = 100
            if (r1 < r2) goto L_0x0830
            int[][] r1 = r14.scoreHG
            int r2 = r14.i
            r1 = r1[r2]
            r2 = 2
            int[] r3 = com.gappli.ripplesshoot.RipplesView.scoreH
            int r4 = r14.i
            r3 = r3[r4]
            int r3 = r3 / 100
            int r3 = r3 % 10
            r1[r2] = r3
        L_0x081e:
            int r1 = r14.i
            int r1 = r1 + 1
            r14.i = r1
            goto L_0x07ce
        L_0x0825:
            int[][] r1 = r14.scoreHG
            int r2 = r14.i
            r1 = r1[r2]
            r2 = 1
            r3 = 0
            r1[r2] = r3
            goto L_0x0801
        L_0x0830:
            int[][] r1 = r14.scoreHG
            int r2 = r14.i
            r1 = r1[r2]
            r2 = 2
            r3 = 0
            r1[r2] = r3
            goto L_0x081e
        L_0x083b:
            int r1 = r14.scene
            r2 = 2
            if (r1 != r2) goto L_0x0919
            r1 = 0
            r14.touchDown = r1
            int r1 = r14.scoreI
            if (r1 == 0) goto L_0x0852
            int r1 = r14.ballX
            r14.ballX2 = r1
            int r1 = r14.ballY
            r14.ballY2 = r1
            r1 = 0
            r14.ballL = r1
        L_0x0852:
            int r1 = r14.scoreI
            int r1 = r1 % 2
            if (r1 != 0) goto L_0x08b8
            r1 = -1
            r14.possP = r1
            r1 = 0
            r14.possY = r1
        L_0x085e:
            r1 = 0
            r14.ballM = r1
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 1
            if (r1 != r2) goto L_0x08d5
            r1 = 0
            r14.ballG = r1
            r1 = -1
            r14.ballH = r1
            int r1 = r6 + 77
            r14.ballX = r1
            int r1 = r14.possP
            r2 = -1
            if (r1 != r2) goto L_0x08c0
            int r1 = r7 + 78
            r14.ballY = r1
            r1 = 4578695649957023231(0x3f8acb6f46508dff, double:0.013083333333333334)
            r3 = 5
            int r3 = r14.rand(r3)
            int r3 = r3 + 60
            double r3 = (double) r3
            double r1 = r1 * r3
            r14.ballA = r1
        L_0x0889:
            r1 = 3
            int r1 = r14.rand(r1)
            int r1 = r1 + 2
            int r1 = r1 * 4
            r14.ballH2 = r1
            r1 = 5
            int r1 = r14.rand(r1)
            int r1 = r1 + 10
            r14.ballS = r1
            r1 = 4
            int r1 = r14.rand(r1)
            r14.ballR = r1
        L_0x08a4:
            r1 = 0
            r14.gkM = r1
            r1 = 0
            r14.gkG = r1
            r1 = 1
            r14.gkG2 = r1
            r1 = -1
            r14.ripM = r1
            r1 = -1
            r14.ripC = r1
            r1 = -2
            r14.rollM = r1
            goto L_0x0046
        L_0x08b8:
            r1 = 1
            r14.possP = r1
            r1 = -250(0xffffffffffffff06, float:NaN)
            r14.possY = r1
            goto L_0x085e
        L_0x08c0:
            int r1 = r7 + 952
            r14.ballY = r1
            r1 = -4644676386897752577(0xbf8acb6f46508dff, double:-0.013083333333333334)
            r3 = 5
            int r3 = r14.rand(r3)
            int r3 = r3 + 60
            double r3 = (double) r3
            double r1 = r1 * r3
            r14.ballA = r1
            goto L_0x0889
        L_0x08d5:
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 2
            if (r1 != r2) goto L_0x08a4
            int r1 = r6 + 300
            r2 = 11
            int r2 = r14.rand(r2)
            int r2 = r2 * 10
            int r1 = r1 + r2
            r14.ballX = r1
            int r1 = r14.scoreI
            int r1 = r1 % 2
            if (r1 != 0) goto L_0x090b
            int r1 = r7 + 120
            r2 = 30
            int r2 = r14.rand(r2)
            int r2 = r2 * 10
            int r1 = r1 + r2
            r14.ballY = r1
        L_0x08fa:
            r1 = 37
            r14.ballG = r1
            r1 = 0
            r14.ballS = r1
            r1 = 0
            r14.ballR = r1
            r1 = 0
            r14.ballH2 = r1
            r1 = -2
            r14.ballH = r1
            goto L_0x08a4
        L_0x090b:
            int r1 = r7 + 920
            r2 = 30
            int r2 = r14.rand(r2)
            int r2 = r2 * 10
            int r1 = r1 - r2
            r14.ballY = r1
            goto L_0x08fa
        L_0x0919:
            int r1 = r14.scene
            r2 = 6
            if (r1 != r2) goto L_0x0942
            int r1 = com.gappli.ripplesshoot.RipplesView.scoreG
            int r1 = r1 + 1
            com.gappli.ripplesshoot.RipplesView.scoreG = r1
            int r1 = r14.missC
            r2 = 3
            if (r1 >= r2) goto L_0x092f
            int r1 = r14.missC
            int r1 = r1 + 1
            r14.missC = r1
        L_0x092f:
            int[] r1 = com.gappli.ripplesshoot.RipplesView.scoreH
            int r2 = com.gappli.ripplesshoot.RipplesView.chcP
            r3 = 1
            int r2 = r2 - r3
            r1 = r1[r2]
            int r2 = com.gappli.ripplesshoot.RipplesView.scoreG
            if (r1 >= r2) goto L_0x0046
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r14.writehiscore(r1)
            goto L_0x0046
        L_0x0942:
            int r1 = r14.scene
            r2 = 7
            if (r1 != r2) goto L_0x0046
            int r1 = r14.ballY
            int r2 = r7 + 361
            r3 = 30
            int r2 = r2 - r3
            if (r1 <= r2) goto L_0x0969
            int r1 = r14.ballY
            int r2 = r7 + 361
            if (r1 >= r2) goto L_0x0969
            int r1 = r7 + 361
            r2 = 30
            int r1 = r1 - r2
            r14.ballY = r1
        L_0x095d:
            int r1 = r14.missC
            if (r1 <= 0) goto L_0x0046
            int r1 = r14.missC
            r2 = 1
            int r1 = r1 - r2
            r14.missC = r1
            goto L_0x0046
        L_0x0969:
            int r1 = r14.ballY
            int r2 = r7 + 361
            int r2 = r2 + 307
            if (r1 <= r2) goto L_0x095d
            int r1 = r14.ballY
            int r2 = r7 + 361
            int r2 = r2 + 307
            int r2 = r2 + 30
            if (r1 >= r2) goto L_0x095d
            int r1 = r7 + 361
            int r1 = r1 + 307
            int r1 = r1 + 30
            r14.ballY = r1
            goto L_0x095d
        L_0x0984:
            int[] r1 = r14.scoreGG
            r2 = 1
            r3 = 0
            r1[r2] = r3
            goto L_0x0060
        L_0x098c:
            int[] r1 = r14.scoreGG
            r2 = 2
            r3 = 0
            r1[r2] = r3
            goto L_0x0071
        L_0x0994:
            int r1 = r14.ballS
            int r1 = r1 + 1
            r14.ballS = r1
            goto L_0x0080
        L_0x099c:
            int r1 = r14.ballG
            int r1 = r1 + 1
            r14.ballG = r1
            goto L_0x008e
        L_0x09a4:
            r1 = 1
            com.gappli.ripplesshoot.RipplesView.init = r1
            goto L_0x009a
        L_0x09a9:
            int r1 = r14.scene
            r2 = 1
            if (r1 != r2) goto L_0x0ac4
            int r1 = r14.backC
            r2 = 10
            if (r1 <= r2) goto L_0x0a17
            int r1 = r14.backC
            r2 = 10
            int r1 = r1 - r2
            r14.backC = r1
        L_0x09bb:
            int r1 = r14.screenX
            r2 = 100
            if (r1 >= r2) goto L_0x0a4f
            int r1 = r14.screenX
            r2 = 5
            if (r1 <= r2) goto L_0x0a4f
            int r1 = r14.screenX
            r2 = 5
            int r1 = r1 - r2
            r14.screenX = r1
            int r1 = r14.gkG
            r2 = 3
            if (r1 >= r2) goto L_0x0a2c
            int r1 = r14.gkG
            int r1 = r1 + 1
            r14.gkG = r1
        L_0x09d7:
            int r1 = r14.gkG
            if (r1 == 0) goto L_0x09e0
            int r1 = r14.gkG
            r2 = 2
            if (r1 != r2) goto L_0x0a30
        L_0x09e0:
            r1 = 1
            r14.gkG2 = r1
        L_0x09e3:
            int r1 = r14.gkX
            int r2 = r6 + 90
            if (r1 >= r2) goto L_0x0a44
            int r1 = r14.gkX
            int r1 = r1 + 5
            r14.gkX = r1
        L_0x09ef:
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            if (r1 <= 0) goto L_0x09ff
            int r1 = r14.chcX
            r2 = 480(0x1e0, float:6.73E-43)
            if (r1 >= r2) goto L_0x0a5b
            int r1 = r14.chcX
            int r1 = r1 + 40
            r14.chcX = r1
        L_0x09ff:
            boolean r1 = r14.touchDown
            if (r1 == 0) goto L_0x009a
            int r1 = r14.menuY
            if (r1 != 0) goto L_0x009a
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 4
            if (r1 != r2) goto L_0x0a83
            r1 = 0
            com.gappli.ripplesshoot.RipplesView.chcP = r1
            r1 = 0
            r14.chcX = r1
        L_0x0a12:
            r1 = 0
            r14.touchDown = r1
            goto L_0x009a
        L_0x0a17:
            r1 = 0
            r14.backC = r1
            int r1 = r14.menuY
            r2 = 20
            if (r1 <= r2) goto L_0x0a28
            int r1 = r14.menuY
            r2 = 20
            int r1 = r1 - r2
            r14.menuY = r1
            goto L_0x09bb
        L_0x0a28:
            r1 = 0
            r14.menuY = r1
            goto L_0x09bb
        L_0x0a2c:
            r1 = 0
            r14.gkG = r1
            goto L_0x09d7
        L_0x0a30:
            int r1 = r14.gkG
            r2 = 1
            if (r1 != r2) goto L_0x0a3a
            r1 = 32
            r14.gkG2 = r1
            goto L_0x09e3
        L_0x0a3a:
            int r1 = r14.gkG
            r2 = 3
            if (r1 != r2) goto L_0x09e3
            r1 = 33
            r14.gkG2 = r1
            goto L_0x09e3
        L_0x0a44:
            int r1 = r6 + 90
            r14.gkX = r1
            r1 = 0
            r14.gkG = r1
            r1 = 0
            r14.gkG2 = r1
            goto L_0x09ef
        L_0x0a4f:
            int r1 = r14.screenX
            r2 = 5
            if (r1 > r2) goto L_0x09ef
            r1 = 0
            r14.screenX = r1
            r1 = 2
            com.gappli.ripplesshoot.RipplesView.init = r1
            goto L_0x09ef
        L_0x0a5b:
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 1
            if (r1 == r2) goto L_0x0a65
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 2
            if (r1 != r2) goto L_0x0a79
        L_0x0a65:
            r1 = 0
            r14.scoreI = r1
            r1 = 3
            r14.missC = r1
            r1 = 0
            com.gappli.ripplesshoot.RipplesView.scoreG = r1
            int r1 = r14.screenX
            r2 = 100
            if (r1 != r2) goto L_0x09ff
            r1 = 90
            r14.screenX = r1
            goto L_0x09ff
        L_0x0a79:
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 3
            if (r1 != r2) goto L_0x09ff
            r1 = 4
            com.gappli.ripplesshoot.RipplesView.chcP = r1
            goto L_0x09ff
        L_0x0a83:
            int r1 = r14.keyY
            int r2 = r7 + 250
            if (r1 < r2) goto L_0x0a96
            int r1 = r14.keyY
            int r2 = r7 + 250
            int r2 = r2 + 100
            if (r1 > r2) goto L_0x0a96
            r1 = 1
            com.gappli.ripplesshoot.RipplesView.chcP = r1
            goto L_0x0a12
        L_0x0a96:
            int r1 = r14.keyY
            int r2 = r7 + 250
            int r2 = r2 + 160
            if (r1 < r2) goto L_0x0aad
            int r1 = r14.keyY
            int r2 = r7 + 250
            int r2 = r2 + 160
            int r2 = r2 + 100
            if (r1 > r2) goto L_0x0aad
            r1 = 2
            com.gappli.ripplesshoot.RipplesView.chcP = r1
            goto L_0x0a12
        L_0x0aad:
            int r1 = r14.keyY
            int r2 = r7 + 250
            int r2 = r2 + 320
            if (r1 < r2) goto L_0x0a12
            int r1 = r14.keyY
            int r2 = r7 + 250
            int r2 = r2 + 320
            int r2 = r2 + 100
            if (r1 > r2) goto L_0x0a12
            r1 = 3
            com.gappli.ripplesshoot.RipplesView.chcP = r1
            goto L_0x0a12
        L_0x0ac4:
            int r1 = r14.scene
            r2 = 2
            if (r1 != r2) goto L_0x0c0d
            int r1 = r14.rollM
            r2 = -2
            if (r1 != r2) goto L_0x0b0b
            int r1 = r14.possP
            r2 = 1
            if (r1 != r2) goto L_0x0b67
            int r1 = r14.screenY
            int r2 = r14.possY
            if (r1 <= r2) goto L_0x0b5e
            int r1 = r14.screenY
            r2 = 5
            int r1 = r1 - r2
            r14.screenY = r1
        L_0x0adf:
            double r1 = r14.rollA
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x0b83
            double r1 = r14.rollA
            r3 = 4592206448839134719(0x3fbacb6f46508dff, double:0.10466666666666667)
            double r1 = r1 - r3
            r14.rollA = r1
        L_0x0af1:
            r1 = -4631165588015641089(0xbfbacb6f46508dff, double:-0.10466666666666667)
            double r3 = r14.rollA
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x0b0b
            double r1 = r14.rollA
            r3 = 4592206448839134719(0x3fbacb6f46508dff, double:0.10466666666666667)
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x0b0b
            r1 = 0
            r14.rollA = r1
        L_0x0b0b:
            int r1 = r14.gkY
            int r2 = r7 + 512
            r3 = 10
            int r2 = r2 - r3
            if (r1 != r2) goto L_0x0b9b
            int r1 = r14.rollM
            r2 = -1
            if (r1 != r2) goto L_0x0b4d
            int r1 = r14.gkG
            r2 = 2
            if (r1 >= r2) goto L_0x0b24
            int r1 = r14.gkG
            int r1 = r1 + 1
            r14.gkG = r1
        L_0x0b24:
            int r1 = r14.gkG
            if (r1 <= 0) goto L_0x0b2e
            int r1 = r14.gkG
            int r1 = r1 + 33
            r14.gkG2 = r1
        L_0x0b2e:
            int r1 = r14.gkG2
            r2 = 35
            if (r1 != r2) goto L_0x0b4d
            int r1 = r14.rollG
            if (r1 != 0) goto L_0x0b97
            r1 = 1
            r14.rollG = r1
        L_0x0b3b:
            boolean r1 = r14.touchDown
            if (r1 == 0) goto L_0x0b4d
            r1 = 3
            com.gappli.ripplesshoot.RipplesView.init = r1
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 2
            if (r1 != r2) goto L_0x0b4a
            r1 = 0
            r14.ballG = r1
        L_0x0b4a:
            r1 = 0
            r14.touchDown = r1
        L_0x0b4d:
            int r1 = r14.ballL
            if (r1 < 0) goto L_0x009a
            int r1 = r14.ballL
            r2 = 6
            if (r1 >= r2) goto L_0x0c08
            int r1 = r14.ballL
            int r1 = r1 + 1
            r14.ballL = r1
            goto L_0x009a
        L_0x0b5e:
            int r1 = r14.possY
            r14.screenY = r1
            r1 = -1
            r14.rollM = r1
            goto L_0x0adf
        L_0x0b67:
            int r1 = r14.possP
            r2 = -1
            if (r1 != r2) goto L_0x0adf
            int r1 = r14.screenY
            int r2 = r14.possY
            if (r1 >= r2) goto L_0x0b7a
            int r1 = r14.screenY
            int r1 = r1 + 5
            r14.screenY = r1
            goto L_0x0adf
        L_0x0b7a:
            int r1 = r14.possY
            r14.screenY = r1
            r1 = -1
            r14.rollM = r1
            goto L_0x0adf
        L_0x0b83:
            double r1 = r14.rollA
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x0af1
            double r1 = r14.rollA
            r3 = 4592206448839134719(0x3fbacb6f46508dff, double:0.10466666666666667)
            double r1 = r1 + r3
            r14.rollA = r1
            goto L_0x0af1
        L_0x0b97:
            r1 = 0
            r14.rollG = r1
            goto L_0x0b3b
        L_0x0b9b:
            int r1 = r14.gkY
            int r2 = r7 + 512
            r3 = 10
            int r2 = r2 - r3
            r3 = 5
            int r2 = r2 - r3
            if (r1 <= r2) goto L_0x0bbf
            int r1 = r14.gkY
            int r2 = r7 + 512
            r3 = 10
            int r2 = r2 - r3
            int r2 = r2 + 5
            if (r1 >= r2) goto L_0x0bbf
            r1 = 0
            r14.gkG = r1
            r1 = 0
            r14.gkG2 = r1
            int r1 = r7 + 512
            r2 = 10
            int r1 = r1 - r2
            r14.gkY = r1
            goto L_0x0b4d
        L_0x0bbf:
            int r1 = r14.gkG
            r2 = 2
            if (r1 >= r2) goto L_0x0bf1
            int r1 = r14.gkG
            int r1 = r1 + 1
            r14.gkG = r1
        L_0x0bca:
            int r1 = r14.gkG
            int r1 = java.lang.Math.abs(r1)
            r14.gkG2 = r1
            int r1 = r14.gkX
            int r2 = r6 + 90
            if (r1 >= r2) goto L_0x0bde
            int r1 = r14.gkX
            int r1 = r1 + 5
            r14.gkX = r1
        L_0x0bde:
            int r1 = r14.gkY
            int r2 = r7 + 512
            r3 = 10
            int r2 = r2 - r3
            r3 = 2
            int r2 = r2 - r3
            if (r1 > r2) goto L_0x0bf5
            int r1 = r14.gkY
            int r1 = r1 + 2
            r14.gkY = r1
            goto L_0x0b4d
        L_0x0bf1:
            r1 = -1
            r14.gkG = r1
            goto L_0x0bca
        L_0x0bf5:
            int r1 = r14.gkY
            int r2 = r7 + 512
            r3 = 10
            int r2 = r2 - r3
            int r2 = r2 + 2
            if (r1 < r2) goto L_0x0b4d
            int r1 = r14.gkY
            r2 = 2
            int r1 = r1 - r2
            r14.gkY = r1
            goto L_0x0b4d
        L_0x0c08:
            r1 = -1
            r14.ballL = r1
            goto L_0x009a
        L_0x0c0d:
            int r1 = r14.scene
            r2 = 3
            if (r1 != r2) goto L_0x0ca3
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 1
            if (r1 != r2) goto L_0x0c99
            int r1 = r14.rollM
            r2 = -1
            if (r1 != r2) goto L_0x0c6d
            double r1 = r14.rollA
            double r3 = r14.ballA
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x0c5a
            double r1 = r14.rollA
            r3 = 4592206448839134719(0x3fbacb6f46508dff, double:0.10466666666666667)
            double r1 = r1 - r3
            r14.rollA = r1
        L_0x0c2e:
            double r1 = r14.ballA
            r3 = 4592206448839134719(0x3fbacb6f46508dff, double:0.10466666666666667)
            double r1 = r1 - r3
            double r3 = r14.rollA
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x0c51
            double r1 = r14.rollA
            double r3 = r14.ballA
            r8 = 4592206448839134719(0x3fbacb6f46508dff, double:0.10466666666666667)
            double r3 = r3 + r8
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x0c51
            double r1 = r14.ballA
            r14.rollA = r1
            r1 = 0
            r14.rollM = r1
        L_0x0c51:
            int r1 = r14.rollG
            if (r1 != 0) goto L_0x0c94
            r1 = 1
            r14.rollG = r1
            goto L_0x009a
        L_0x0c5a:
            double r1 = r14.rollA
            double r3 = r14.ballA
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x0c2e
            double r1 = r14.rollA
            r3 = 4592206448839134719(0x3fbacb6f46508dff, double:0.10466666666666667)
            double r1 = r1 + r3
            r14.rollA = r1
            goto L_0x0c2e
        L_0x0c6d:
            int r1 = r14.rollM
            if (r1 < 0) goto L_0x0c51
            int r1 = r14.gkG2
            r2 = 34
            if (r1 <= r2) goto L_0x0c89
            int r1 = r14.gkG2
            r2 = 1
            int r1 = r1 - r2
            r14.gkG2 = r1
        L_0x0c7d:
            int r1 = r14.rollM
            r2 = 5
            if (r1 >= r2) goto L_0x0c90
            int r1 = r14.rollM
            int r1 = r1 + 1
            r14.rollM = r1
            goto L_0x0c51
        L_0x0c89:
            r1 = 0
            r14.gkG = r1
            r1 = 0
            r14.gkG2 = r1
            goto L_0x0c7d
        L_0x0c90:
            r1 = 4
            com.gappli.ripplesshoot.RipplesView.init = r1
            goto L_0x0c51
        L_0x0c94:
            r1 = 0
            r14.rollG = r1
            goto L_0x009a
        L_0x0c99:
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 2
            if (r1 != r2) goto L_0x009a
            r1 = 4
            com.gappli.ripplesshoot.RipplesView.init = r1
            goto L_0x009a
        L_0x0ca3:
            int r1 = r14.scene
            r2 = 4
            if (r1 < r2) goto L_0x009a
            int r1 = r14.ballM
            if (r1 < 0) goto L_0x0cb9
            int r1 = r14.possP
            r2 = -1
            if (r1 != r2) goto L_0x0d98
            int r1 = r14.ballG
            r2 = 2
            if (r1 <= r2) goto L_0x0d90
            r1 = 0
            r14.ballG = r1
        L_0x0cb9:
            double r1 = r14.ballA
            r3 = 4578695649957023231(0x3f8acb6f46508dff, double:0.013083333333333334)
            int r8 = r14.ballR
            double r8 = (double) r8
            double r3 = r3 * r8
            r8 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            double r3 = r3 * r8
            int r8 = r14.possP
            double r8 = (double) r8
            double r3 = r3 * r8
            double r1 = r1 + r3
            r14.ballA = r1
            int r1 = r14.ballH
            r2 = -2
            if (r1 != r2) goto L_0x0cdd
            int r1 = r14.ballS
            if (r1 <= 0) goto L_0x0cdd
            int r1 = r14.ballS
            r2 = 1
            int r1 = r1 - r2
            r14.ballS = r1
        L_0x0cdd:
            int r1 = r14.ballX
            double r1 = (double) r1
            int r3 = r14.ballS
            int r3 = r3 * 2
            double r3 = (double) r3
            double r8 = r14.ballA
            double r8 = java.lang.Math.cos(r8)
            double r3 = r3 * r8
            double r1 = r1 + r3
            int r1 = (int) r1
            r14.ballX = r1
            int r1 = r14.ballY
            double r1 = (double) r1
            int r3 = r14.ballS
            int r3 = r3 * 2
            double r3 = (double) r3
            double r8 = r14.ballA
            double r8 = java.lang.Math.sin(r8)
            double r3 = r3 * r8
            double r1 = r1 + r3
            int r1 = (int) r1
            r14.ballY = r1
            int r1 = r14.ripM
            if (r1 <= 0) goto L_0x009a
            int r1 = r14.ripM
            r2 = 7
            if (r1 >= r2) goto L_0x0daa
            int r1 = r14.ripM
            int r1 = r1 + 1
            r14.ripM = r1
        L_0x0d12:
            int r1 = r14.ripM
            if (r1 <= 0) goto L_0x009a
            int r1 = r14.ripM
            r2 = 6
            if (r1 > r2) goto L_0x009a
            int r1 = r14.ballX
            int r2 = r14.ripX
            int r1 = r1 - r2
            r14.hitX = r1
            int r1 = r14.ballY
            int r2 = r14.possY
            int r1 = r1 + r2
            int r2 = r14.ripY
            int r1 = r1 - r2
            r14.hitY = r1
            int r1 = r14.hitX
            int r2 = r14.hitX
            int r1 = r1 * r2
            int r2 = r14.hitY
            int r3 = r14.hitY
            int r2 = r2 * r3
            int r1 = r1 + r2
            int[] r2 = r14.ripT
            int r3 = r14.ripM
            r2 = r2[r3]
            int r2 = r2 + 40
            int[] r3 = r14.ripT
            int r4 = r14.ripM
            r3 = r3[r4]
            int r3 = r3 + 40
            int r2 = r2 * r3
            if (r1 > r2) goto L_0x009a
            int r1 = r14.hitY
            double r1 = (double) r1
            int r3 = r14.hitX
            double r3 = (double) r3
            double r1 = java.lang.Math.atan2(r1, r3)
            r14.ballA = r1
            int r1 = r14.ripM
            int r1 = r1 * 2
            int r1 = r1 + 6
            r14.ballS = r1
            r1 = 0
            r14.ballR = r1
            r1 = -1
            r14.ballM = r1
            double r1 = r14.ballA
            double r1 = java.lang.Math.cos(r1)
            r3 = 0
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 >= 0) goto L_0x0d73
            r1 = 1
            r14.ripC = r1
        L_0x0d73:
            int r1 = r14.ballX
            r14.ballXB = r1
            int r1 = r14.ballY
            r14.ballYB = r1
            int r1 = r14.ballH
            r2 = 15
            if (r1 < r2) goto L_0x0daf
            int r1 = r14.ballS
            r2 = 6
            int r1 = r1 - r2
            r14.ballS = r1
        L_0x0d87:
            int r1 = r14.ballS
            if (r1 > 0) goto L_0x009a
            r1 = 2
            r14.ballS = r1
            goto L_0x009a
        L_0x0d90:
            int r1 = r14.ballG
            int r1 = r1 + 1
            r14.ballG = r1
            goto L_0x0cb9
        L_0x0d98:
            int r1 = r14.ballG
            r2 = 1
            if (r1 >= r2) goto L_0x0da2
            r1 = 3
            r14.ballG = r1
            goto L_0x0cb9
        L_0x0da2:
            int r1 = r14.ballG
            r2 = 1
            int r1 = r1 - r2
            r14.ballG = r1
            goto L_0x0cb9
        L_0x0daa:
            r1 = -1
            r14.ripM = r1
            goto L_0x0d12
        L_0x0daf:
            int r1 = r14.ballH
            r2 = 10
            if (r1 < r2) goto L_0x0dbc
            int r1 = r14.ballS
            r2 = 4
            int r1 = r1 - r2
            r14.ballS = r1
            goto L_0x0d87
        L_0x0dbc:
            int r1 = r14.ballH
            r2 = 5
            if (r1 < r2) goto L_0x0d87
            int r1 = r14.ballS
            r2 = 2
            int r1 = r1 - r2
            r14.ballS = r1
            goto L_0x0d87
        L_0x0dc8:
            int r1 = r14.ballH
            int r2 = r14.ballH2
            if (r1 != r2) goto L_0x0dd6
            int r1 = r14.ballH2
            int r1 = r1 * -1
            r14.ballH2 = r1
            goto L_0x00ce
        L_0x0dd6:
            int r1 = r14.ballH
            int r2 = r14.ballH2
            if (r1 <= r2) goto L_0x00ce
            int r1 = r14.ballH
            if (r1 != 0) goto L_0x0df4
            int r1 = r14.ballS
            r2 = 8
            if (r1 <= r2) goto L_0x0dec
            int r1 = r14.ballS
            r2 = 1
            int r1 = r1 - r2
            r14.ballS = r1
        L_0x0dec:
            int r1 = r14.ballH2
            int r1 = r1 * 0
            r14.ballH2 = r1
            goto L_0x00ce
        L_0x0df4:
            int r1 = r14.ballH
            r2 = 1
            int r1 = r1 - r2
            r14.ballH = r1
            goto L_0x00ce
        L_0x0dfc:
            int r1 = r14.ballX
            int r2 = r6 + 77
            r3 = 30
            int r2 = r2 - r3
            if (r1 >= r2) goto L_0x0186
            r1 = 0
            r14.ballS = r1
            r1 = 7
            com.gappli.ripplesshoot.RipplesView.init = r1
            goto L_0x0186
        L_0x0e0d:
            r1 = 8
            r14.gkS = r1
            goto L_0x01f9
        L_0x0e13:
            int r1 = r14.gkY
            int r2 = r14.ballY
            if (r1 <= r2) goto L_0x020b
            r1 = -1
            r14.gkM = r1
            goto L_0x020b
        L_0x0e1e:
            int r1 = r14.gkT
            int r2 = r14.gkS
            if (r1 <= r2) goto L_0x0e43
            int r1 = r14.gkY
            int r2 = r14.ballY3
            if (r1 >= r2) goto L_0x0e39
            r1 = 1
            r14.gkM = r1
        L_0x0e2d:
            int r1 = r14.gkT
            if (r1 <= 0) goto L_0x020b
            int r1 = r14.gkT
            r2 = 1
            int r1 = r1 - r2
            r14.gkT = r1
            goto L_0x020b
        L_0x0e39:
            int r1 = r14.gkY
            int r2 = r14.ballY3
            if (r1 <= r2) goto L_0x0e2d
            r1 = -1
            r14.gkM = r1
            goto L_0x0e2d
        L_0x0e43:
            int r1 = r14.gkT
            int r2 = r14.gkS
            if (r1 > r2) goto L_0x0e2d
            int r1 = r14.gkY
            r2 = 30
            int r1 = r1 - r2
            r2 = 30
            int r1 = r1 - r2
            int r2 = r14.ballY3
            if (r1 > r2) goto L_0x0e90
            r1 = 1
            r14.gkM = r1
        L_0x0e58:
            int r1 = r14.gkX
            r2 = 30
            int r1 = r1 - r2
            int r2 = r14.ballX
            if (r1 >= r2) goto L_0x0e2d
            int r1 = r14.gkY
            r2 = 30
            int r1 = r1 - r2
            r2 = 15
            int r1 = r1 - r2
            int r2 = r14.ballY3
            if (r1 > r2) goto L_0x0e7a
            int r1 = r14.ballY3
            int r2 = r14.gkY
            int r2 = r2 + 30
            int r2 = r2 + 15
            if (r1 > r2) goto L_0x0e7a
            r1 = 3
            r14.gkM = r1
        L_0x0e7a:
            int r1 = r14.ballY3
            int r2 = r7 + 514
            int r2 = r2 + 250
            if (r1 >= r2) goto L_0x0e9e
            int r1 = r14.ballY3
            int r2 = r14.gkY
            int r2 = r2 + 30
            int r2 = r2 + 30
            if (r1 <= r2) goto L_0x0e9e
            r1 = 2
            r14.gkM = r1
            goto L_0x0e2d
        L_0x0e90:
            int r1 = r14.ballY3
            int r2 = r14.gkY
            int r2 = r2 + 30
            int r2 = r2 + 30
            if (r1 > r2) goto L_0x0e58
            r1 = -1
            r14.gkM = r1
            goto L_0x0e58
        L_0x0e9e:
            int r1 = r14.ballY3
            int r2 = r7 + 514
            r3 = 250(0xfa, float:3.5E-43)
            int r2 = r2 - r3
            if (r1 <= r2) goto L_0x0e2d
            int r1 = r14.gkY
            r2 = 30
            int r1 = r1 - r2
            r2 = 30
            int r1 = r1 - r2
            int r2 = r14.ballY3
            if (r1 <= r2) goto L_0x0e2d
            r1 = -2
            r14.gkM = r1
            goto L_0x0e2d
        L_0x0eb8:
            int r1 = r14.gkG2
            r2 = 7
            if (r1 == r2) goto L_0x0ec3
            int r1 = r14.gkG2
            r2 = 8
            if (r1 != r2) goto L_0x0ef9
        L_0x0ec3:
            int r1 = r14.ballX
            int r2 = r14.gkX
            int r2 = r2 + 30
            int r1 = r1 - r2
            r14.gkX2 = r1
            int r1 = r14.ballY
            int r2 = r14.gkY
            int r2 = r2 + 50
            int r1 = r1 - r2
            r14.gkY2 = r1
            int r1 = r14.gkX2
            int r2 = r14.gkX2
            int r1 = r1 * r2
            int r2 = r14.gkY2
            int r3 = r14.gkY2
            int r2 = r2 * r3
            int r1 = r1 + r2
            r14.gkL2 = r1
            int r1 = r14.gkL2
            r2 = 900(0x384, float:1.261E-42)
            if (r1 > r2) goto L_0x0247
            int r1 = r14.gkY2
            double r1 = (double) r1
            int r3 = r14.gkX2
            double r3 = (double) r3
            double r1 = java.lang.Math.atan2(r1, r3)
            r14.ballA = r1
            r1 = -1
            r14.ripC = r1
            goto L_0x0247
        L_0x0ef9:
            int r1 = r14.gkG2
            r2 = 5
            if (r1 == r2) goto L_0x0f03
            int r1 = r14.gkG2
            r2 = 6
            if (r1 != r2) goto L_0x0247
        L_0x0f03:
            int r1 = r14.ballX
            int r2 = r14.gkX
            int r2 = r2 + 30
            int r1 = r1 - r2
            r14.gkX2 = r1
            int r1 = r14.ballY
            int r2 = r14.gkY
            r3 = 50
            int r2 = r2 - r3
            int r1 = r1 - r2
            r14.gkY2 = r1
            int r1 = r14.gkX2
            int r2 = r14.gkX2
            int r1 = r1 * r2
            int r2 = r14.gkY2
            int r3 = r14.gkY2
            int r2 = r2 * r3
            int r1 = r1 + r2
            r14.gkL2 = r1
            int r1 = r14.gkL2
            r2 = 900(0x384, float:1.261E-42)
            if (r1 > r2) goto L_0x0247
            int r1 = r14.gkY2
            double r1 = (double) r1
            int r3 = r14.gkX2
            double r3 = (double) r3
            double r1 = java.lang.Math.atan2(r1, r3)
            r14.ballA = r1
            r1 = -1
            r14.ripC = r1
            goto L_0x0247
        L_0x0f3a:
            int r1 = r14.gkM
            r2 = 3
            if (r1 != r2) goto L_0x0f5c
            int r1 = r14.gkG
            r2 = 2
            if (r1 >= r2) goto L_0x0f58
            int r1 = r14.gkG
            int r1 = r1 + 1
            r14.gkG = r1
            int r1 = r14.gkX
            int r1 = r1 + 4
            r14.gkX = r1
        L_0x0f50:
            int r1 = r14.gkG
            int r1 = r1 + 2
            r14.gkG2 = r1
            goto L_0x02a2
        L_0x0f58:
            r1 = 5
            r14.gkM = r1
            goto L_0x0f50
        L_0x0f5c:
            int r1 = r14.gkM
            r2 = 4
            if (r1 != r2) goto L_0x0f93
            int r1 = r14.gkY
            int r2 = r14.ballY
            if (r1 <= r2) goto L_0x0f7d
            int r1 = r14.gkY
            r2 = 1
            int r1 = r1 - r2
            r14.gkY = r1
        L_0x0f6d:
            int r1 = r14.gkG
            if (r1 != 0) goto L_0x0f8f
            r1 = 1
            r14.gkG = r1
        L_0x0f74:
            int r1 = r14.gkG
            r14.gkG2 = r1
            r1 = 0
            r14.gkM = r1
            goto L_0x02a2
        L_0x0f7d:
            int r1 = r14.gkY
            int r2 = r14.ballY
            if (r1 >= r2) goto L_0x0f8a
            int r1 = r14.gkY
            int r1 = r1 + 1
            r14.gkY = r1
            goto L_0x0f6d
        L_0x0f8a:
            int r1 = r14.ballY
            r14.gkY = r1
            goto L_0x0f6d
        L_0x0f8f:
            r1 = 0
            r14.gkG = r1
            goto L_0x0f74
        L_0x0f93:
            int r1 = r14.gkM
            r2 = 1
            if (r1 == r2) goto L_0x0f9d
            int r1 = r14.gkM
            r2 = -1
            if (r1 != r2) goto L_0x0fe4
        L_0x0f9d:
            int r1 = r14.gkM
            r2 = -1
            if (r1 != r2) goto L_0x0fca
            int r1 = r14.gkY
            int r2 = r7 + 383
            if (r1 >= r2) goto L_0x0fc3
            r1 = 0
            r14.gkM = r1
        L_0x0fab:
            int r1 = r14.gkG
            r2 = 2
            if (r1 >= r2) goto L_0x0fe0
            int r1 = r14.gkG
            int r1 = r1 + 1
            r14.gkG = r1
        L_0x0fb6:
            int r1 = r14.gkG
            int r1 = java.lang.Math.abs(r1)
            r14.gkG2 = r1
            r1 = 0
            r14.gkM = r1
            goto L_0x02a2
        L_0x0fc3:
            int r1 = r14.gkY
            r2 = 2
            int r1 = r1 - r2
            r14.gkY = r1
            goto L_0x0fab
        L_0x0fca:
            int r1 = r14.gkM
            r2 = 1
            if (r1 != r2) goto L_0x0fab
            int r1 = r14.gkY
            int r2 = r7 + 647
            if (r1 <= r2) goto L_0x0fd9
            r1 = 0
            r14.gkM = r1
            goto L_0x0fab
        L_0x0fd9:
            int r1 = r14.gkY
            int r1 = r1 + 2
            r14.gkY = r1
            goto L_0x0fab
        L_0x0fe0:
            r1 = -1
            r14.gkG = r1
            goto L_0x0fb6
        L_0x0fe4:
            int r1 = r14.gkM
            r2 = 2
            if (r1 == r2) goto L_0x0fee
            int r1 = r14.gkM
            r2 = -2
            if (r1 != r2) goto L_0x1024
        L_0x0fee:
            int r1 = r14.gkG
            r2 = 2
            if (r1 >= r2) goto L_0x100d
            int r1 = r14.gkG
            int r1 = r1 + 1
            r14.gkG = r1
        L_0x0ff9:
            int r1 = r14.gkM
            r2 = -2
            if (r1 != r2) goto L_0x1011
            int r1 = r14.gkG
            int r1 = r1 + 4
            r14.gkG2 = r1
            int r1 = r14.gkY
            r2 = 8
            int r1 = r1 - r2
            r14.gkY = r1
            goto L_0x02a2
        L_0x100d:
            r1 = 5
            r14.gkM = r1
            goto L_0x0ff9
        L_0x1011:
            int r1 = r14.gkM
            r2 = 2
            if (r1 != r2) goto L_0x02a2
            int r1 = r14.gkG
            int r1 = r1 + 6
            r14.gkG2 = r1
            int r1 = r14.gkY
            int r1 = r1 + 8
            r14.gkY = r1
            goto L_0x02a2
        L_0x1024:
            int r1 = r14.gkM
            r2 = 5
            if (r1 < r2) goto L_0x02a2
            int r1 = r14.gkM
            r2 = 8
            if (r1 >= r2) goto L_0x1046
            int r1 = r14.gkM
            int r1 = r1 + 1
            r14.gkM = r1
        L_0x1035:
            int r1 = r14.gkG2
            r2 = 7
            if (r1 < r2) goto L_0x1059
            int r1 = r14.gkY
            r2 = 10
            int r3 = r14.gkM
            int r2 = r2 - r3
            int r1 = r1 + r2
            r14.gkY = r1
            goto L_0x02a2
        L_0x1046:
            int r1 = r14.gkG
            if (r1 <= 0) goto L_0x1051
            int r1 = r14.gkG
            r2 = 1
            int r1 = r1 - r2
            r14.gkG = r1
            goto L_0x1035
        L_0x1051:
            int r1 = r14.gkG
            if (r1 != 0) goto L_0x1035
            r1 = 0
            r14.gkM = r1
            goto L_0x1035
        L_0x1059:
            int r1 = r14.gkG2
            r2 = 5
            if (r1 < r2) goto L_0x106a
            int r1 = r14.gkY
            r2 = 10
            int r3 = r14.gkM
            int r2 = r2 - r3
            int r1 = r1 - r2
            r14.gkY = r1
            goto L_0x02a2
        L_0x106a:
            int r1 = r14.gkG2
            r2 = 3
            if (r1 < r2) goto L_0x02a2
            int r1 = r14.gkX
            r2 = 8
            int r3 = r14.gkM
            int r2 = r2 - r3
            int r1 = r1 + r2
            r14.gkX = r1
            goto L_0x02a2
        L_0x107b:
            int r1 = r6 + 90
            r14.gkX = r1
            goto L_0x02b8
        L_0x1081:
            int r1 = r14.ballY
            int r2 = r7 + 361
            int r2 = r2 + 307
            r3 = 30
            int r2 = r2 - r3
            if (r1 < r2) goto L_0x02cb
            int r1 = r7 + 361
            int r1 = r1 + 307
            r2 = 30
            int r1 = r1 - r2
            r14.ballY = r1
            goto L_0x02cb
        L_0x1097:
            int r1 = r14.scene
            r2 = 6
            if (r1 < r2) goto L_0x02db
            int r1 = r14.scene
            r2 = 8
            if (r1 > r2) goto L_0x02db
            int r1 = r14.ballH
            r2 = -2
            if (r1 <= r2) goto L_0x10ad
            int r1 = r14.ballH
            r2 = 1
            int r1 = r1 - r2
            r14.ballH = r1
        L_0x10ad:
            int r1 = r14.scene
            r2 = 7
            if (r1 < r2) goto L_0x1118
            int r1 = r14.scene
            r2 = 8
            if (r1 > r2) goto L_0x1118
            int r1 = r14.gkG2
            if (r1 != 0) goto L_0x10ec
            r1 = 34
            r14.gkG2 = r1
            r1 = -999(0xfffffffffffffc19, float:NaN)
            r14.gkM = r1
        L_0x10c4:
            boolean r1 = r14.touchDown
            if (r1 == 0) goto L_0x02db
            int r1 = r14.scene
            r2 = 8
            if (r1 != r2) goto L_0x112a
            int r1 = com.gappli.ripplesshoot.RipplesView.scoreG
            int[] r2 = com.gappli.ripplesshoot.RipplesView.scoreH
            int r3 = com.gappli.ripplesshoot.RipplesView.chcP
            r4 = 1
            int r3 = r3 - r4
            r2 = r2[r3]
            if (r1 <= r2) goto L_0x10e4
            int[] r1 = com.gappli.ripplesshoot.RipplesView.scoreH
            int r2 = com.gappli.ripplesshoot.RipplesView.chcP
            r3 = 1
            int r2 = r2 - r3
            int r3 = com.gappli.ripplesshoot.RipplesView.scoreG
            r1[r2] = r3
        L_0x10e4:
            r1 = 1
            com.gappli.ripplesshoot.RipplesView.init = r1
        L_0x10e7:
            r1 = 0
            r14.touchDown = r1
            goto L_0x02db
        L_0x10ec:
            int r1 = r14.gkG2
            r2 = 34
            if (r1 != r2) goto L_0x10f7
            r1 = 36
            r14.gkG2 = r1
            goto L_0x10c4
        L_0x10f7:
            int r1 = r14.gkG2
            r2 = 36
            if (r1 != r2) goto L_0x1102
            r1 = 37
            r14.gkG2 = r1
            goto L_0x10c4
        L_0x1102:
            int r1 = r14.gkG2
            r2 = 37
            if (r1 != r2) goto L_0x110d
            r1 = 38
            r14.gkG2 = r1
            goto L_0x10c4
        L_0x110d:
            int r1 = r14.gkG2
            r2 = 38
            if (r1 != r2) goto L_0x10c4
            r1 = 37
            r14.gkG2 = r1
            goto L_0x10c4
        L_0x1118:
            int r1 = r14.scene
            r2 = 6
            if (r1 != r2) goto L_0x10c4
            int r1 = r14.goalG
            r2 = 1
            if (r1 != r2) goto L_0x1126
            r1 = 0
            r14.goalG = r1
            goto L_0x10c4
        L_0x1126:
            r1 = 1
            r14.goalG = r1
            goto L_0x10c4
        L_0x112a:
            int r1 = r14.missC
            if (r1 <= 0) goto L_0x1142
            int r1 = r14.scoreI
            int r1 = r1 + 1
            r14.scoreI = r1
            int r1 = r14.gkG2
            r2 = 37
            if (r1 < r2) goto L_0x113e
            r1 = 36
            r14.gkG2 = r1
        L_0x113e:
            r1 = 2
            com.gappli.ripplesshoot.RipplesView.init = r1
            goto L_0x10e7
        L_0x1142:
            r1 = 8
            com.gappli.ripplesshoot.RipplesView.init = r1
            goto L_0x10e7
        L_0x1147:
            int r1 = r14.scene
            r2 = 1
            if (r1 != r2) goto L_0x12a5
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 9
            r1 = r1[r2]
            int r2 = r14.screenX
            int r2 = r6 - r2
            float r2 = (float) r2
            int r3 = r14.screenY
            int r3 = r3 + r7
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            int r1 = r14.screenX
            r2 = 100
            if (r1 != r2) goto L_0x03ff
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            if (r1 <= 0) goto L_0x11a0
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 4
            if (r1 >= r2) goto L_0x11a0
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r5.setStyle(r1)
            r1 = 100
            r2 = 171(0xab, float:2.4E-43)
            r3 = 171(0xab, float:2.4E-43)
            r4 = 171(0xab, float:2.4E-43)
            int r1 = android.graphics.Color.argb(r1, r2, r3, r4)
            r5.setColor(r1)
            float r1 = (float) r6
            int r2 = r7 + 250
            int r3 = com.gappli.ripplesshoot.RipplesView.chcP
            r4 = 1
            int r3 = r3 - r4
            int r3 = r3 * 160
            int r2 = r2 + r3
            float r2 = (float) r2
            int r3 = r14.chcX
            int r3 = r3 + r6
            float r3 = (float) r3
            int r4 = r7 + 250
            int r8 = com.gappli.ripplesshoot.RipplesView.chcP
            r9 = 1
            int r8 = r8 - r9
            int r8 = r8 * 160
            int r4 = r4 + r8
            int r4 = r4 + 100
            float r4 = (float) r4
            r0.drawRect(r1, r2, r3, r4, r5)
        L_0x11a0:
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 27
            r1 = r1[r2]
            r2 = 5
            int r2 = r6 - r2
            float r2 = (float) r2
            int r3 = r7 + 50
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            android.graphics.Rect r9 = new android.graphics.Rect
            r1 = 0
            r2 = 0
            r3 = 348(0x15c, float:4.88E-43)
            r4 = 420(0x1a4, float:5.89E-43)
            int r8 = r14.menuY
            int r4 = r4 - r8
            r9.<init>(r1, r2, r3, r4)
            android.graphics.Rect r8 = new android.graphics.Rect
            int r1 = r6 + 55
            int r2 = r7 + 250
            int r3 = r6 + 55
            int r3 = r3 + 348
            int r4 = r7 + 250
            int r4 = r4 + 420
            int r10 = r14.menuY
            int r4 = r4 - r10
            r8.<init>(r1, r2, r3, r4)
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 26
            r1 = r1[r2]
            r2 = 0
            r0.drawBitmap(r1, r9, r8, r2)
            int r1 = r14.menuY
            if (r1 != 0) goto L_0x11f9
            r1 = 0
            r14.k = r1
        L_0x11e4:
            int r1 = r14.k
            r2 = 2
            if (r1 < r2) goto L_0x122f
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 28
            r1 = r1[r2]
            int r2 = r6 + 275
            float r2 = (float) r2
            int r3 = r7 + 705
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
        L_0x11f9:
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL
            r5.setStyle(r1)
            int r1 = r14.backC
            r2 = 255(0xff, float:3.57E-43)
            r3 = 255(0xff, float:3.57E-43)
            r4 = 255(0xff, float:3.57E-43)
            int r1 = android.graphics.Color.argb(r1, r2, r3, r4)
            r5.setColor(r1)
            float r1 = (float) r6
            float r2 = (float) r7
            int r3 = r6 + 480
            float r3 = (float) r3
            int r4 = r7 + 800
            float r4 = (float) r4
            r0.drawRect(r1, r2, r3, r4, r5)
            int r1 = com.gappli.ripplesshoot.RipplesView.chcP
            r2 = 4
            if (r1 != r2) goto L_0x03ff
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 53
            r1 = r1[r2]
            int r2 = r6 + 55
            float r2 = (float) r2
            int r3 = r7 + 260
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x03ff
        L_0x122f:
            int[] r1 = com.gappli.ripplesshoot.RipplesView.scoreH
            int r2 = r14.k
            r1 = r1[r2]
            if (r1 <= 0) goto L_0x125b
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 54
            r1 = r1[r2]
            int r2 = r6 + 55
            int r2 = r2 + 140
            float r2 = (float) r2
            int r3 = r7 + 250
            int r3 = r3 + 106
            int r4 = r14.k
            int r4 = r4 * 157
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            r1 = 1
            r14.j = r1
            r1 = 0
            r14.i = r1
        L_0x1256:
            int r1 = r14.i
            r2 = 3
            if (r1 < r2) goto L_0x1262
        L_0x125b:
            int r1 = r14.k
            int r1 = r1 + 1
            r14.k = r1
            goto L_0x11e4
        L_0x1262:
            int r1 = r14.i
            if (r1 == 0) goto L_0x1270
            int[] r1 = com.gappli.ripplesshoot.RipplesView.scoreH
            int r2 = r14.k
            r1 = r1[r2]
            int r2 = r14.j
            if (r1 < r2) goto L_0x1298
        L_0x1270:
            android.graphics.Bitmap[] r1 = r14.bmp
            int[][] r2 = r14.scoreHG
            int r3 = r14.k
            r2 = r2[r3]
            int r3 = r14.i
            r2 = r2[r3]
            int r2 = r2 + 41
            r1 = r1[r2]
            int r2 = r6 + 55
            int r2 = r2 + 280
            int r3 = r14.i
            int r3 = r3 * 37
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r7 + 250
            int r3 = r3 + 100
            int r4 = r14.k
            int r4 = r4 * 157
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
        L_0x1298:
            int r1 = r14.j
            int r1 = r1 * 10
            r14.j = r1
            int r1 = r14.i
            int r1 = r1 + 1
            r14.i = r1
            goto L_0x1256
        L_0x12a5:
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 9
            r1 = r1[r2]
            float r2 = (float) r6
            int r3 = r14.screenY
            int r3 = r3 + r7
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x03ff
        L_0x12b6:
            android.graphics.Bitmap[] r1 = r14.bmp
            int r2 = r14.ballG
            int r2 = r2 + 18
            r1 = r1[r2]
            int r2 = r14.ballX
            r3 = 31
            int r2 = r2 - r3
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r14.ballY
            r4 = 31
            int r3 = r3 - r4
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x0685
        L_0x12d6:
            int r1 = r14.i
            if (r1 == 0) goto L_0x12e0
            int r1 = com.gappli.ripplesshoot.RipplesView.scoreG
            int r2 = r14.j
            if (r1 < r2) goto L_0x1322
        L_0x12e0:
            android.graphics.Bitmap[] r1 = r14.bmp
            int[] r2 = r14.scoreGG
            int r3 = r14.i
            r2 = r2[r3]
            int r2 = r2 + 41
            r1 = r1[r2]
            int r2 = r6 + 285
            int r3 = r14.i
            int r3 = r3 * 37
            int r2 = r2 - r3
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r7 + 23
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            android.graphics.Bitmap[] r1 = r14.bmp
            int[] r2 = r14.scoreGG
            int r3 = r14.i
            r2 = r2[r3]
            int r2 = r2 + 41
            r1 = r1[r2]
            int r2 = r6 + 285
            int r3 = r14.i
            int r3 = r3 * 37
            int r2 = r2 - r3
            int r3 = r14.screenX
            int r2 = r2 - r3
            float r2 = (float) r2
            int r3 = r7 + 965
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
        L_0x1322:
            int r1 = r14.j
            int r1 = r1 * 10
            r14.j = r1
            int r1 = r14.i
            int r1 = r1 + 1
            r14.i = r1
            goto L_0x06a1
        L_0x1330:
            int r1 = r14.scene
            r2 = 7
            if (r1 != r2) goto L_0x134a
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 31
            r1 = r1[r2]
            int r2 = r6 + 180
            float r2 = (float) r2
            int r3 = r7 + 440
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x073f
        L_0x134a:
            int r1 = r14.scene
            r2 = 6
            if (r1 != r2) goto L_0x1366
            android.graphics.Bitmap[] r1 = r14.bmp
            int r2 = r14.goalG
            int r2 = r2 + 39
            r1 = r1[r2]
            int r2 = r6 + 180
            float r2 = (float) r2
            int r3 = r7 + 350
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x073f
        L_0x1366:
            int r1 = r14.scene
            r2 = 8
            if (r1 != r2) goto L_0x073f
            int r1 = com.gappli.ripplesshoot.RipplesView.scoreG
            int[] r2 = com.gappli.ripplesshoot.RipplesView.scoreH
            int r3 = com.gappli.ripplesshoot.RipplesView.chcP
            r4 = 1
            int r3 = r3 - r4
            r2 = r2[r3]
            if (r1 > r2) goto L_0x138d
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 30
            r1 = r1[r2]
            int r2 = r6 + 180
            float r2 = (float) r2
            int r3 = r7 + 430
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x073f
        L_0x138d:
            android.graphics.Bitmap[] r1 = r14.bmp
            r2 = 52
            r1 = r1[r2]
            int r2 = r6 + 180
            float r2 = (float) r2
            int r3 = r7 + 430
            int r4 = r14.screenY
            int r3 = r3 + r4
            float r3 = (float) r3
            r4 = 0
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x073f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.gappli.ripplesshoot.RipplesView.run():void");
    }

    private void writehiscore(int stage) {
        SharedPreferences.Editor editor = getContext().getSharedPreferences("PreferencesEx", 0).edit();
        editor.putInt("hiscore" + stage, scoreG);
        editor.commit();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 23) {
            this.touchDown = true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == 23) {
            this.touchDown = false;
        }
        return super.onKeyUp(keyCode, event);
    }

    public boolean onTouchEvent(MotionEvent event) {
        int touchAction = event.getAction();
        if (touchAction == 0) {
            this.touchDown = true;
            this.keyX = (int) event.getX();
            this.keyY = (int) event.getY();
        } else if (touchAction == S_MENU || touchAction == S_START1) {
            this.touchDown = false;
        }
        return true;
    }

    private int rand(int num) {
        return (this.rand.nextInt() >>> S_MENU) % num;
    }

    private static Bitmap readBitmap(Context context, String name) {
        return BitmapFactory.decodeResource(context.getResources(), context.getResources().getIdentifier(name, "drawable", context.getPackageName()));
    }

    public static void goreset() {
        if (scoreG > scoreH[chcP - S_MENU]) {
            scoreH[chcP - S_MENU] = scoreG;
        }
        init = S_MENU;
    }
}
