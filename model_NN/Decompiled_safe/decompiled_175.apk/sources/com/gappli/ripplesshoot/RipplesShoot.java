package com.gappli.ripplesshoot;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class RipplesShoot extends Activity {
    private static final int MENU_ITEM0 = 0;
    private static final int MENU_ITEM1 = 1;
    public static EditText edtInput;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getWindow().clearFlags(2048);
        getWindow().addFlags(1024);
        requestWindowFeature(MENU_ITEM1);
        setContentView(new RipplesView(this));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add((int) MENU_ITEM0, (int) MENU_ITEM0, (int) MENU_ITEM0, (int) R.string.menu_reset).setIcon(17301561);
        menu.add((int) MENU_ITEM0, (int) MENU_ITEM1, (int) MENU_ITEM0, (int) R.string.menu_web).setIcon(17301561);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_ITEM0 /*0*/:
                RipplesView.goreset();
                return MENU_ITEM1;
            case MENU_ITEM1 /*1*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://gappli.com")));
                return MENU_ITEM1;
            default:
                return MENU_ITEM1;
        }
    }
}
