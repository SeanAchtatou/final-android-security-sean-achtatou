package com.gappli.ripplesshoot;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int r0 = 2130837505;
        public static final int r1 = 2130837506;
        public static final int r10 = 2130837507;
        public static final int r11 = 2130837508;
        public static final int r12 = 2130837509;
        public static final int r13 = 2130837510;
        public static final int r14 = 2130837511;
        public static final int r15 = 2130837512;
        public static final int r16 = 2130837513;
        public static final int r17 = 2130837514;
        public static final int r18 = 2130837515;
        public static final int r19 = 2130837516;
        public static final int r2 = 2130837517;
        public static final int r20 = 2130837518;
        public static final int r21 = 2130837519;
        public static final int r22 = 2130837520;
        public static final int r23 = 2130837521;
        public static final int r24 = 2130837522;
        public static final int r25 = 2130837523;
        public static final int r26 = 2130837524;
        public static final int r27 = 2130837525;
        public static final int r28 = 2130837526;
        public static final int r29 = 2130837527;
        public static final int r3 = 2130837528;
        public static final int r30 = 2130837529;
        public static final int r31 = 2130837530;
        public static final int r32 = 2130837531;
        public static final int r33 = 2130837532;
        public static final int r34 = 2130837533;
        public static final int r35 = 2130837534;
        public static final int r36 = 2130837535;
        public static final int r37 = 2130837536;
        public static final int r38 = 2130837537;
        public static final int r39 = 2130837538;
        public static final int r4 = 2130837539;
        public static final int r40 = 2130837540;
        public static final int r41 = 2130837541;
        public static final int r42 = 2130837542;
        public static final int r43 = 2130837543;
        public static final int r44 = 2130837544;
        public static final int r45 = 2130837545;
        public static final int r46 = 2130837546;
        public static final int r47 = 2130837547;
        public static final int r48 = 2130837548;
        public static final int r49 = 2130837549;
        public static final int r5 = 2130837550;
        public static final int r50 = 2130837551;
        public static final int r51 = 2130837552;
        public static final int r52 = 2130837553;
        public static final int r53 = 2130837554;
        public static final int r54 = 2130837555;
        public static final int r55 = 2130837556;
        public static final int r6 = 2130837557;
        public static final int r7 = 2130837558;
        public static final int r8 = 2130837559;
        public static final int r9 = 2130837560;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
        public static final int menu_reset = 2130968579;
        public static final int menu_web = 2130968578;
    }
}
