package com.androidillusion.cameraillusion.media;

import android.content.ContentValues;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import com.androidillusion.cameraillusion.CameraActivity;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Vector;

public class MediaManager {
    private static MediaManager instance;

    public static MediaManager getInstance() {
        if (instance == null) {
            instance = new MediaManager();
        }
        return instance;
    }

    public Uri saveMediaEntry(String imagePath, String title, String displayName, String description, long dateTaken, int orientation, Location loc) {
        ContentValues v = new ContentValues();
        v.put("title", title);
        v.put("_display_name", displayName);
        v.put("description", description);
        v.put("date_added", Long.valueOf(dateTaken));
        v.put("datetaken", Long.valueOf(dateTaken));
        v.put("date_modified", Long.valueOf(dateTaken));
        v.put("mime_type", "image/jpeg");
        v.put("orientation", Integer.valueOf(orientation));
        File f = new File(imagePath);
        File parent = f.getParentFile();
        String path = parent.toString().toLowerCase();
        String name = parent.getName().toLowerCase();
        v.put("bucket_id", Integer.valueOf(path.hashCode()));
        v.put("bucket_display_name", name);
        v.put("_size", Long.valueOf(f.length()));
        if (loc != null) {
            v.put("latitude", Double.valueOf(loc.getLatitude()));
            v.put("longitude", Double.valueOf(loc.getLongitude()));
        }
        v.put("_data", imagePath);
        return CameraActivity.instance.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, v);
    }

    public boolean checkSD() {
        File sdcard = new File("/sdcard/");
        if (!sdcard.exists() || !sdcard.canWrite()) {
            return false;
        }
        return true;
    }

    public String getDateFileName() {
        Date actual = new Date(System.currentTimeMillis());
        NumberFormat number = new DecimalFormat();
        number.setMinimumIntegerDigits(2);
        String year = new StringBuilder().append(actual.getYear() + 1900).toString().toString();
        String fileName = String.valueOf(year) + "-" + number.format((long) (actual.getMonth() + 1)) + "-" + number.format((long) actual.getDate()) + " " + number.format((long) actual.getHours()) + "_" + number.format((long) actual.getMinutes()) + "_" + number.format((long) actual.getSeconds()) + ".jpg";
        Log.i("Info", "name " + fileName);
        return fileName;
    }

    public boolean checkDirectory(String uri) {
        if (new File(uri).exists()) {
            return true;
        }
        return false;
    }

    public void createDirectory(String uri) {
        new File(uri).mkdir();
    }

    public void saveFile(String filename, String directory, Bitmap imageBitmap) {
        FileOutputStream stream = null;
        File file = new File(directory, filename);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            if (file.canWrite()) {
                stream = new FileOutputStream(file);
            }
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 95, stream);
            stream.flush();
            stream.close();
        } catch (Exception e) {
            Log.i("Info", "saveFile exception " + e);
        }
    }

    public Vector<String> checkMaskFiles(String uri) {
        Vector<String> files = new Vector<>();
        Log.i("Info", "Searching files in " + uri);
        File directory = new File(uri);
        if (directory.exists()) {
            File[] file = directory.listFiles();
            for (int i = 0; i < file.length; i++) {
                try {
                    Log.i("Info", "File: " + file[i].getName());
                    if (file[i].getName().toLowerCase().endsWith(".png")) {
                        files.add(file[i].getName().substring(0, file[i].getName().length() - 4));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return files;
    }

    public void saveAscii(String fileName, String directory, char[][] asciiBuffer, boolean portraitMode) {
        FileOutputStream stream = null;
        File file = new File(directory, String.valueOf(fileName.substring(0, fileName.length() - 3)) + "html");
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            if (file.canWrite()) {
                stream = new FileOutputStream(file);
            }
            StringBuffer buffer = new StringBuffer();
            buffer.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            buffer.append("<html lang=en>");
            buffer.append("<body style=\"background: Black;\" lang=en leftmargin=0 topmargin=0>");
            buffer.append("<head>");
            buffer.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
            buffer.append("<style type=\"text/css\">");
            buffer.append("pre {");
            buffer.append("font: 7pt monospace;");
            buffer.append("line-height: 88%;");
            buffer.append("color: #FFFFFF;");
            if (portraitMode) {
                buffer.append("-moz-transform: rotate(90deg);");
                buffer.append("-moz-transform-origin: 0% 0%;");
                buffer.append("-webkit-transform: rotate(90deg);");
                buffer.append("-webkit-transform-origin: 0% 0%;");
                buffer.append("position:absolute;");
                buffer.append("margin: 0% 100%;");
            }
            buffer.append("}");
            buffer.append("code {");
            buffer.append("height: 100%;");
            buffer.append("width: 100%;");
            buffer.append("}");
            buffer.append("</style>");
            buffer.append("</head>");
            buffer.append("<pre><code>");
            for (int i = 0; i < asciiBuffer.length; i++) {
                buffer.append("\n" + new String(asciiBuffer[i]));
            }
            buffer.append("\n</code></pre></body></html>");
            stream.write(buffer.toString().getBytes());
            stream.flush();
            stream.close();
        } catch (Exception e) {
            Log.i("Info", "saveFile exception " + e);
        }
    }
}
