package com.androidillusion.cameraillusion.media;

import android.media.MediaPlayer;
import com.androidillusion.cameraillusion.CameraActivity;
import com.androidillusion.cameraillusion.R;

public class SoundManager {
    public static final int SOUND_1 = 1;
    private static SoundManager instance;
    MediaPlayer mp = MediaPlayer.create(CameraActivity.instance, (int) R.raw.camera);

    public static SoundManager getInstance() {
        if (instance == null) {
            instance = new SoundManager();
        }
        return instance;
    }

    private SoundManager() {
    }

    public void shotSound() {
        this.mp.start();
    }
}
