package com.androidillusion.cameraillusion;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.util.Log;
import com.androidillusion.cameraillusion.camera.CameraSurfaceView;
import com.androidillusion.cameraillusion.config.Settings;

public class SettingsActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String SETTINGS = "settings";
    CheckBoxPreference stretch;
    public String[] supportedRes;

    public void onCreate(Bundle savedInstanceState) {
        String versionString;
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.layout.settings);
        ListPreference resolution = (ListPreference) getPreferenceScreen().getPreferenceManager().findPreference("resolution");
        this.supportedRes = new String[CameraSurfaceView.availableResVector.size()];
        String[] index = new String[this.supportedRes.length];
        for (int i = 0; i < this.supportedRes.length; i++) {
            Point point = CameraSurfaceView.availableResVector.elementAt(i);
            this.supportedRes[i] = String.valueOf(point.x) + " x " + point.y;
            if (i == CameraSurfaceView.defaultRes) {
                this.supportedRes[i] = String.valueOf(this.supportedRes[i]) + " " + CameraActivity.instance.getString(R.string.defaultText);
            }
            index[i] = new StringBuilder().append(i).toString();
        }
        resolution.setEntries(this.supportedRes);
        resolution.setEntryValues(index);
        resolution.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Log.i("Info", "selected " + Integer.parseInt((String) newValue));
                CameraSurfaceView.selectedRes = Integer.parseInt((String) newValue);
                String text = SettingsActivity.this.supportedRes[CameraSurfaceView.selectedRes];
                if (CameraSurfaceView.selectedRes != CameraSurfaceView.defaultRes) {
                    text = String.valueOf(text) + " " + CameraActivity.instance.getString(R.string.noMasks);
                }
                preference.setSummary(text);
                if (CameraSurfaceView.selectedRes == 0) {
                    SettingsActivity.this.stretch.setChecked(false);
                    SettingsActivity.this.stretch.setEnabled(false);
                } else {
                    SettingsActivity.this.stretch.setEnabled(true);
                }
                CameraSurfaceView.restartCamera = true;
                return true;
            }
        });
        String text = this.supportedRes[CameraSurfaceView.selectedRes];
        if (CameraSurfaceView.selectedRes != CameraSurfaceView.defaultRes) {
            text = String.valueOf(text) + " " + CameraActivity.instance.getString(R.string.noMasks);
        }
        resolution.setSummary(text);
        this.stretch = (CheckBoxPreference) getPreferenceScreen().getPreferenceManager().findPreference("stretch");
        if (CameraSurfaceView.selectedRes == 0) {
            this.stretch.setChecked(false);
            this.stretch.setEnabled(false);
        } else {
            this.stretch.setEnabled(true);
        }
        this.stretch.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                CameraSurfaceView.restartCamera = true;
                return true;
            }
        });
        if (Settings.PRO_VERSION) {
            ((PreferenceCategory) findPreference("cam")).removePreference(findPreference("pro"));
        }
        Preference torch = getPreferenceScreen().getPreferenceManager().findPreference("torch");
        if (CameraSurfaceView.isTorchAvailable) {
            torch.setSummary(CameraActivity.instance.getString(R.string.available));
        } else {
            torch.setSummary(CameraActivity.instance.getString(R.string.notAvailable));
        }
        Preference res = getPreferenceScreen().getPreferenceManager().findPreference("availableRes");
        String chain = this.supportedRes[0];
        for (int i2 = 1; i2 < this.supportedRes.length; i2++) {
            chain = String.valueOf(chain) + ", " + this.supportedRes[i2];
        }
        res.setSummary(chain);
        Preference androidVersion = getPreferenceScreen().getPreferenceManager().findPreference("androidVersion");
        try {
            int version = Integer.parseInt(Build.VERSION.SDK);
            switch (version) {
                case 3:
                    versionString = "1.5 (Cupcake)";
                    break;
                case 4:
                    versionString = "1.6 (Donut)";
                    break;
                case 5:
                    versionString = "2.0 (Eclair)";
                    break;
                case 6:
                    versionString = "2.0.1 (Eclair)";
                    break;
                case 7:
                    versionString = "2.1 (Eclair)";
                    break;
                case 8:
                    versionString = "2.2 (Froyo)";
                    break;
                default:
                    versionString = new StringBuilder().append(version).toString();
                    break;
            }
            androidVersion.setSummary(versionString);
        } catch (Exception e) {
            androidVersion.setSummary(CameraActivity.instance.getString(R.string.notAvailable));
        }
        getPreferenceScreen().getPreferenceManager().findPreference("contact").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SettingsActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("mailto:camera.illusion@gmail.com")));
                return false;
            }
        });
        getPreferenceScreen().getPreferenceManager().findPreference("donate").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SettingsActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=RH8U99VWJ5TUS")));
                return false;
            }
        });
        getPreferenceScreen().getPreferenceManager().findPreference("about").setSummary(Settings.APP_VERSION);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.i("Info", "key " + key);
        if (key.equals("autofocus")) {
            Settings.AUTOFUCUS_BEFORE_SHOT = sharedPreferences.getBoolean("autofocus", true);
        } else if (key.equals("shutter")) {
            Settings.CAMERA_SOUND = sharedPreferences.getBoolean("shutter", true);
        } else if (key.equals("remember")) {
            Settings.REMEMBER_SELECTED_OPTIONS = sharedPreferences.getBoolean("remember", false);
        } else if (key.equals("statusbar")) {
            Settings.HIDE_STATUS_BAR = sharedPreferences.getBoolean("statusbar", false);
        } else if (key.equals("ascii")) {
            Settings.SAVE_HTML_ASCII_ART = sharedPreferences.getBoolean("ascii", false);
        } else if (key.equals("volume")) {
            Settings.VOLUME_KEY_SHUTTER = sharedPreferences.getBoolean("volume", true);
        } else if (key.equals("fps")) {
            Settings.DEBUG_MODE = sharedPreferences.getBoolean("fps", false);
        } else if (key.equals("stretch")) {
            Settings.STRETCH_PICTURE = sharedPreferences.getBoolean("stretch", false);
        } else if (key.equals("pro")) {
            Settings.PRO_BUTTON_INVISIBLE = sharedPreferences.getBoolean("pro", false);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }
}
