package com.androidillusion.cameraillusion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.admob.android.ads.AdView;
import com.androidillusion.cameraillusion.camera.CameraRefreshThread;
import com.androidillusion.cameraillusion.camera.CameraSurfaceView;
import com.androidillusion.cameraillusion.camera.CustomCameraView;
import com.androidillusion.cameraillusion.config.ChangeOrientationSave;
import com.androidillusion.cameraillusion.config.Settings;
import com.androidillusion.cameraillusion.media.MediaManager;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;
import java.util.Vector;

public class CameraActivity extends Activity {
    private static final String BRIGHTNESS = "BRIGHTNESS";
    private static final String CONTRAST = "CONTRAST";
    private static final String LAST_VERSION = "LAST_VERSION";
    private static final int MENU_CAMERA_ILLUSION = 16;
    private static final int MENU_EXIT = 14;
    private static final int MENU_GALLERY = 11;
    private static final int MENU_HELP = 13;
    private static final int MENU_ILLUSION = 10;
    private static final int MENU_PHOTO_ILLUSION = 15;
    private static final int MENU_SETTINGS = 12;
    public static final int MODE_CAMERA = 0;
    public static final int MODE_ILLUSION = 1;
    public static final int MODE_ILLUSION_OPTIONS = 2;
    public static final String PREFS_NAME = "MyPrefsFilePro";
    private static final String SATURATION = "SATURATION";
    private static final String SELECTED_EFFECT = "SELECTED_EFFECT";
    private static final String SELECTED_FILTER = "SELECTED_FILTER";
    private static final String SELECTED_MASK = "SELECTED_MASK";
    public static CameraActivity instance;
    public static boolean isDiceVisible = true;
    public static int orientation;
    public static boolean restartCamera = false;
    public View adMobLayout;
    public AdView adMobView;
    public SeekBar brightnessBar;
    public float brightnessValue = 1.0f;
    public View cameraLayout;
    public CameraSurfaceView cameraView;
    public SeekBar contrastBar;
    public float contrastValue = 1.0f;
    public CustomCameraView customCameraView;
    public Button diceButton;
    public Hashtable<Integer, Integer> effectHash = new Hashtable<>();
    String[] effectStrings;
    public Hashtable<Integer, Integer> filterHash = new Hashtable<>();
    String[] filterStrings;
    public Button flashButton;
    public View illusionLayout;
    LayoutInflater inflater;
    Toast infoMsg;
    public boolean isFlashOn = false;
    public boolean isOnPause = false;
    public long lastAd = 0;
    public int lastVersion;
    public Hashtable<Integer, Integer> maskHash = new Hashtable<>();
    String[] maskStrings;
    public Button plusButton;
    public Button proButton;
    public Spinner s1;
    public Spinner s2;
    public Spinner s3;
    public SeekBar saturationBar;
    public float saturationValue = 1.0f;
    public View seekBarLayout;
    public View selectEffectLayout;
    public View selectFilterLayout;
    public View selectMaskLayout;
    int selectedEffectID;
    public RadioGroup selectedEffectRG;
    int selectedFilterID;
    public RadioGroup selectedFilterRG;
    int selectedMaskID;
    public RadioGroup selectedMaskRG;
    public int selectedMode = 0;
    public TextView t3;
    public ImageButton thumbButton;

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.isOnPause = false;
        if (Settings.PRO_VERSION || Settings.PRO_BUTTON_INVISIBLE) {
            this.proButton.setVisibility(4);
        } else {
            this.proButton.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    public void onCreate(Bundle savedInstanceState) {
        CameraRefreshThread.init = true;
        CameraRefreshThread.endThread = false;
        super.onCreate(savedInstanceState);
        instance = this;
        requestWindowFeature(1);
        orientation = getResources().getConfiguration().orientation;
        this.infoMsg = Toast.makeText(this, "", 0);
        this.infoMsg.setGravity(17, 0, 0);
        PreferenceManager.setDefaultValues(this, R.layout.settings, false);
        SharedPreferences settingsPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Settings.AUTOFUCUS_BEFORE_SHOT = settingsPreferences.getBoolean("autofocus", true);
        Settings.CAMERA_SOUND = settingsPreferences.getBoolean("shutter", true);
        Settings.REMEMBER_SELECTED_OPTIONS = settingsPreferences.getBoolean("remember", false);
        Settings.HIDE_STATUS_BAR = settingsPreferences.getBoolean("statusbar", false);
        Settings.SAVE_HTML_ASCII_ART = settingsPreferences.getBoolean("ascii", false);
        Settings.VOLUME_KEY_SHUTTER = settingsPreferences.getBoolean("volume", true);
        Settings.STRETCH_PICTURE = settingsPreferences.getBoolean("stretch", false);
        Settings.PRO_BUTTON_INVISIBLE = settingsPreferences.getBoolean("pro", false);
        Settings.DEBUG_MODE = settingsPreferences.getBoolean("fps", false);
        CameraSurfaceView.selectedRes = Integer.parseInt(settingsPreferences.getString("resolution", "-1"));
        if (Settings.HIDE_STATUS_BAR) {
            getWindow().setFlags(1024, 1024);
        }
        SharedPreferences rememberSettings = getSharedPreferences(PREFS_NAME, 0);
        if (Settings.REMEMBER_SELECTED_OPTIONS) {
            this.selectedFilterID = rememberSettings.getInt(SELECTED_FILTER, R.id.filter1);
            this.selectedEffectID = rememberSettings.getInt(SELECTED_EFFECT, R.id.effect1);
            this.selectedMaskID = rememberSettings.getInt(SELECTED_MASK, R.id.mask1);
        } else {
            this.selectedFilterID = R.id.filter1;
            this.selectedEffectID = R.id.effect1;
            this.selectedMaskID = R.id.mask1;
        }
        this.lastVersion = rememberSettings.getInt(LAST_VERSION, 0);
        this.customCameraView = new CustomCameraView(this);
        this.inflater = (LayoutInflater) getSystemService("layout_inflater");
        Object data = getLastNonConfigurationInstance();
        if (data != null) {
            ChangeOrientationSave save = (ChangeOrientationSave) data;
            this.lastAd = save.time;
            this.selectedFilterID = save.selectedFilterID;
            this.selectedEffectID = save.selectedEffectID;
            this.selectedMaskID = save.selectedMaskID;
            if (save.cadena.length() > 0) {
                CameraRefreshThread.isNewPhoto = true;
            }
        } else {
            CameraRefreshThread.isNewPhoto = false;
            CameraRefreshThread.lastPhotoPath = "";
        }
        long currentTimeMillis = System.currentTimeMillis();
        this.adMobLayout = this.inflater.inflate((int) R.layout.admob, (ViewGroup) null);
        this.adMobView = (AdView) this.adMobLayout.findViewById(R.id.ad);
        this.adMobView.setVisibility(0);
        this.cameraView = new CameraSurfaceView(this);
        if (Settings.PRO_VERSION) {
            this.selectFilterLayout = this.inflater.inflate((int) R.layout.filters_pro, (ViewGroup) null);
        } else {
            this.selectFilterLayout = this.inflater.inflate((int) R.layout.filters, (ViewGroup) null);
        }
        configSelectedFilter();
        this.selectEffectLayout = this.inflater.inflate((int) R.layout.effects, (ViewGroup) null);
        configSelectedEffect();
        this.selectMaskLayout = this.inflater.inflate((int) R.layout.masks, (ViewGroup) null);
        configSelectedMask();
        this.cameraLayout = this.inflater.inflate((int) R.layout.camera, (ViewGroup) null);
        configCamera();
        this.illusionLayout = this.inflater.inflate((int) R.layout.illusion, (ViewGroup) null);
        this.seekBarLayout = this.inflater.inflate((int) R.layout.sliders, (ViewGroup) null);
        this.saturationBar = (SeekBar) this.seekBarLayout.findViewById(R.id.SeekBar01);
        this.saturationBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                CameraActivity.this.saturationValue = (((float) progress) / 100.0f) * 2.0f;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        this.brightnessBar = (SeekBar) this.seekBarLayout.findViewById(R.id.SeekBar02);
        this.brightnessBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                CameraActivity.this.brightnessValue = (((float) progress) / 100.0f) * 2.0f;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        this.contrastBar = (SeekBar) this.seekBarLayout.findViewById(R.id.SeekBar03);
        this.contrastBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                CameraActivity.this.contrastValue = (((float) progress) / 100.0f) * 2.0f;
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        if (Settings.REMEMBER_SELECTED_OPTIONS) {
            this.contrastBar.setProgress(rememberSettings.getInt(CONTRAST, 50));
            this.saturationBar.setProgress(rememberSettings.getInt(SATURATION, 50));
            this.brightnessBar.setProgress(rememberSettings.getInt(BRIGHTNESS, 50));
        }
        ((Button) this.seekBarLayout.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CameraActivity.this.setIllusionMode();
            }
        });
        ((Button) this.seekBarLayout.findViewById(R.id.reseteo)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CameraActivity.this.saturationBar.setProgress(50);
                CameraActivity.this.contrastBar.setProgress(50);
                CameraActivity.this.brightnessBar.setProgress(50);
            }
        });
        ((Button) this.illusionLayout.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CameraActivity.this.setCameraMode();
            }
        });
        this.plusButton = (Button) this.illusionLayout.findViewById(R.id.plus);
        this.plusButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CameraActivity.this.setIllusionOptionsMode();
            }
        });
        this.plusButton.setVisibility(4);
        ((Button) this.cameraLayout.findViewById(R.id.illusion_id)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CameraActivity.this.setIllusionMode();
            }
        });
        this.diceButton = (Button) this.cameraLayout.findViewById(R.id.dices);
        this.diceButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Random ram = new Random();
                ram.nextInt();
                CameraActivity.this.setSelectedFilter(CameraActivity.this.filterHash.get(Integer.valueOf(Math.abs(ram.nextInt()) % CameraActivity.this.filterHash.size())).intValue());
                CameraActivity.this.setSelectedEffect(CameraActivity.this.effectHash.get(Integer.valueOf(Math.abs(ram.nextInt()) % CameraActivity.this.effectHash.size())).intValue());
            }
        });
        this.proButton = (Button) this.cameraLayout.findViewById(R.id.pro);
        this.proButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CameraActivity.this.launchCameraIllusionProDialog();
            }
        });
        if (Settings.PRO_VERSION || Settings.PRO_BUTTON_INVISIBLE) {
            this.proButton.setVisibility(4);
        }
        this.thumbButton = (ImageButton) this.cameraLayout.findViewById(R.id.thumb);
        this.thumbButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(CameraActivity.instance, ImageViewerActivity.class);
                intent.putExtra("uri", CameraRefreshThread.lastPhotoUri);
                intent.putExtra("path", CameraRefreshThread.lastPhotoPath);
                CameraActivity.this.startActivity(intent);
            }
        });
        this.thumbButton.setVisibility(4);
        this.flashButton = (Button) this.cameraLayout.findViewById(R.id.flash);
        this.flashButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!CameraActivity.this.cameraView.isFocusing) {
                    if (CameraActivity.this.isFlashOn) {
                        CameraActivity.this.flashButton.setBackgroundResource(R.drawable.unselected_flash);
                    } else {
                        CameraActivity.this.flashButton.setBackgroundResource(R.drawable.selected_flash);
                    }
                    CameraActivity.this.isFlashOn = !CameraActivity.this.isFlashOn;
                    CameraActivity.this.cameraView.setTorchMode(CameraActivity.this.isFlashOn);
                }
            }
        });
        this.flashButton.setVisibility(4);
        setContentView(R.layout.empty);
        addContentView(this.cameraView, new ViewGroup.LayoutParams(-1, -1));
        addContentView(this.customCameraView, new ViewGroup.LayoutParams(-1, -1));
        addContentView(this.cameraLayout, new ViewGroup.LayoutParams(-1, -1));
        addContentView(this.adMobLayout, new ViewGroup.LayoutParams(-1, -1));
        addContentView(this.illusionLayout, new ViewGroup.LayoutParams(-1, -1));
        addContentView(this.seekBarLayout, new ViewGroup.LayoutParams(-1, -1));
        this.s1 = (Spinner) findViewById(R.id.spinner1);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367048, this.filterStrings);
        arrayAdapter.setDropDownViewResource(17367049);
        this.s1.setAdapter((SpinnerAdapter) arrayAdapter);
        this.s1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long selected) {
                int id = R.id.filter1;
                if (CameraActivity.this.filterHash.get(Integer.valueOf((int) selected)) != null) {
                    id = CameraActivity.this.filterHash.get(Integer.valueOf((int) selected)).intValue();
                }
                CameraActivity.this.selectedFilterID = id;
                if (id == R.id.filter20) {
                    CameraActivity.this.plusButton.setVisibility(0);
                } else {
                    CameraActivity.this.plusButton.setVisibility(4);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        setSelectedFilter(this.selectedFilterID);
        this.s2 = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter arrayAdapter2 = new ArrayAdapter(this, 17367048, this.effectStrings);
        arrayAdapter2.setDropDownViewResource(17367049);
        this.s2.setAdapter((SpinnerAdapter) arrayAdapter2);
        this.s2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long selected) {
                int id = R.id.effect1;
                if (CameraActivity.this.effectHash.get(Integer.valueOf((int) selected)) != null) {
                    id = CameraActivity.this.effectHash.get(Integer.valueOf((int) selected)).intValue();
                }
                CameraActivity.this.selectedEffectID = id;
                CameraRefreshThread.effectChanged = true;
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        setSelectedEffect(this.selectedEffectID);
        this.s3 = (Spinner) findViewById(R.id.spinner3);
        this.t3 = (TextView) findViewById(R.id.photo_mask_text);
        ArrayAdapter arrayAdapter3 = new ArrayAdapter(this, 17367048, this.maskStrings);
        arrayAdapter3.setDropDownViewResource(17367049);
        this.s3.setAdapter((SpinnerAdapter) arrayAdapter3);
        this.s3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long selected) {
                int id = R.id.mask1;
                if (CameraActivity.this.maskHash.get(Integer.valueOf((int) selected)) != null) {
                    id = CameraActivity.this.maskHash.get(Integer.valueOf((int) selected)).intValue();
                }
                CameraActivity.this.selectedMaskID = id;
                CameraRefreshThread.isMaskChanged = true;
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        setSelectedMask(this.selectedMaskID);
        setCameraMode();
        int actualVersion = 0;
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(Settings.APP, 0);
            actualVersion = pi.versionCode;
            Settings.APP_VERSION = pi.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (actualVersion > this.lastVersion) {
            new AlertDialog.Builder(this).setIcon((int) R.drawable.icon_camera_illusion).setTitle((int) R.string.add_new_title).setView(this.inflater.inflate(R.layout.initial_dialog, (ViewGroup) findViewById(R.id.dialog))).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if (CameraActivity.this.isIntentAvailable(Settings.PNAME_PHOTO_ILLUSION) || CameraActivity.this.isIntentAvailable(Settings.PNAME_PHOTO_ILLUSION_PRO)) {
                        Log.i("Info", "You have Photo Illusion/Pro installed");
                        return;
                    }
                    Log.i("Info", "You don't have Photo Illusion/Pro installed");
                    CameraActivity.this.launchPhotoIllusionDialog();
                }
            }).show();
        }
        this.lastVersion = actualVersion;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        CameraRefreshThread.endThread = true;
        if (this.cameraView.paintTh != null) {
            while (this.cameraView.paintTh.isAlive()) {
                Log.i("Info", "start join");
                try {
                    this.cameraView.paintTh.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Log.i("Info", "end join");
        }
        CustomCameraView.instance.mBitmap = null;
        SharedPreferences.Editor editor = getSharedPreferences(PREFS_NAME, 0).edit();
        editor.putInt(SELECTED_FILTER, getSelectedFilterID());
        editor.putInt(SELECTED_EFFECT, getSelectedEffectID());
        editor.putInt(SELECTED_MASK, getSelectedMaskID());
        editor.putInt(CONTRAST, this.contrastBar.getProgress());
        editor.putInt(SATURATION, this.saturationBar.getProgress());
        editor.putInt(BRIGHTNESS, this.brightnessBar.getProgress());
        editor.putInt(LAST_VERSION, this.lastVersion);
        editor.commit();
    }

    private void configCamera() {
        ((Button) this.cameraLayout.findViewById(R.id.shot)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CameraActivity.this.cameraView.shotPhoto();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.isOnPause = true;
        super.onPause();
    }

    private void configSelectedEffect() {
        this.selectedEffectRG = (RadioGroup) this.selectEffectLayout.findViewById(R.id.group);
        int count = this.selectedEffectRG.getChildCount();
        this.effectStrings = new String[count];
        for (int i = 0; i < count; i++) {
            this.effectStrings[i] = ((RadioButton) this.selectedEffectRG.getChildAt(i)).getText().toString();
            this.effectHash.put(Integer.valueOf(i), Integer.valueOf(this.selectedEffectRG.getChildAt(i).getId()));
        }
    }

    private void configSelectedFilter() {
        this.selectedFilterRG = (RadioGroup) this.selectFilterLayout.findViewById(R.id.group);
        int count = this.selectedFilterRG.getChildCount();
        this.filterStrings = new String[count];
        for (int i = 0; i < count; i++) {
            this.filterStrings[i] = ((RadioButton) this.selectedFilterRG.getChildAt(i)).getText().toString();
            this.filterHash.put(Integer.valueOf(i), Integer.valueOf(this.selectedFilterRG.getChildAt(i).getId()));
        }
    }

    private void configSelectedMask() {
        this.selectedMaskRG = (RadioGroup) this.selectMaskLayout.findViewById(R.id.group);
        int count = this.selectedMaskRG.getChildCount();
        Vector<String> customMasks = MediaManager.getInstance().checkMaskFiles(Settings.MASK_DIRECTORY);
        int count2 = customMasks.size();
        this.maskStrings = new String[(count + count2)];
        for (int i = 0; i < count; i++) {
            this.maskStrings[i] = ((RadioButton) this.selectedMaskRG.getChildAt(i)).getText().toString();
            this.maskHash.put(Integer.valueOf(i), Integer.valueOf(this.selectedMaskRG.getChildAt(i).getId()));
        }
        for (int i2 = 0; i2 < count2; i2++) {
            this.maskStrings[count + i2] = customMasks.elementAt(i2);
            this.maskHash.put(Integer.valueOf(count + i2), Integer.valueOf(this.maskStrings[count + i2].hashCode()));
            this.maskStrings[count + i2] = "*" + this.maskStrings[count + i2];
        }
    }

    /* access modifiers changed from: package-private */
    public void setCameraMode() {
        this.selectedMode = 0;
        this.cameraLayout.setVisibility(0);
        this.illusionLayout.setVisibility(4);
        this.seekBarLayout.setVisibility(4);
    }

    /* access modifiers changed from: package-private */
    public void setMenuMode() {
        this.selectedMode = 0;
        this.cameraLayout.setVisibility(4);
        this.illusionLayout.setVisibility(4);
        this.seekBarLayout.setVisibility(4);
    }

    /* access modifiers changed from: package-private */
    public void setIllusionMode() {
        this.selectedMode = 1;
        this.cameraLayout.setVisibility(4);
        this.illusionLayout.setVisibility(0);
        this.seekBarLayout.setVisibility(4);
    }

    /* access modifiers changed from: package-private */
    public void setIllusionOptionsMode() {
        this.selectedMode = 2;
        this.cameraLayout.setVisibility(4);
        this.illusionLayout.setVisibility(4);
        this.seekBarLayout.setVisibility(0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, (int) MENU_ILLUSION, 0, getString(R.string.menuIllusion)).setIcon((int) R.drawable.ic_menu_illusion);
        menu.add(0, (int) MENU_GALLERY, 0, getString(R.string.menuGallery)).setIcon(17301567);
        menu.add(0, (int) MENU_SETTINGS, 0, getString(R.string.menuSettings)).setIcon(17301577);
        if (isIntentAvailable(Settings.PNAME_PHOTO_ILLUSION_PRO)) {
            Log.i("Info", "Photo Illusion Pro available");
            menu.add(0, (int) MENU_PHOTO_ILLUSION, 0, getString(R.string.menuPhotoIllusion)).setIcon((int) R.drawable.ic_menu_photo_illusion_pro);
        } else {
            Log.i("Info", "Photo Illusion Pro not available");
            menu.add(0, (int) MENU_PHOTO_ILLUSION, 0, getString(R.string.menuPhotoIllusion)).setIcon((int) R.drawable.ic_menu_photo_illusion);
        }
        menu.add(0, (int) MENU_HELP, 0, getString(R.string.menuHelp)).setIcon(17301568);
        menu.add(0, (int) MENU_EXIT, 0, getString(R.string.menuExit)).setIcon(17301560);
        return true;
    }

    public boolean onMenuOpened(int featureId, Menu menu) {
        setMenuMode();
        return super.onMenuOpened(featureId, menu);
    }

    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
        if (this.selectedMode == 0) {
            setCameraMode();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_ILLUSION /*10*/:
                setIllusionMode();
                return true;
            case MENU_GALLERY /*11*/:
                Vector<Intent> intentVector = new Vector<>();
                intentVector.add(new Intent().setClassName("com.sonyericsson.android.mediascape", "com.sonyericsson.android.mediascape.activity.MceActivity"));
                intentVector.add(new Intent().setClassName("com.htc.album", "com.htc.album.AlbumTabSwitchActivity"));
                intentVector.add(new Intent().setClassName("com.google.android.gallery3d", "com.cooliris.media.Gallery"));
                intentVector.add(new Intent().setClassName("com.cooliris.media", "com.cooliris.media.Gallery"));
                intentVector.add(new Intent().setClassName("com.android.gallery", "com.android.camera.GalleryPicker"));
                intentVector.add(new Intent().setClassName("com.android.camera", "com.android.camera.GalleryPicker"));
                int i = 0;
                while (i < intentVector.size()) {
                    try {
                        ((Intent) intentVector.get(i)).setFlags(268435456);
                        instance.startActivity((Intent) intentVector.get(i));
                        return true;
                    } catch (Exception e) {
                        Log.i("Info", "intent not found: " + e);
                        i++;
                    }
                }
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("content:/" + Settings.PHOTOS_DIRECTORY));
                intent.addFlags(67108864);
                intent.putExtra("mediaTypes", "image/*");
                try {
                    instance.startActivity(intent);
                } catch (ActivityNotFoundException e2) {
                    Log.i("Info", "intent not found: " + e2);
                }
                return true;
            case MENU_SETTINGS /*12*/:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case MENU_HELP /*13*/:
                startActivity(new Intent(this, HelpActivity.class));
                return true;
            case MENU_EXIT /*14*/:
                finish();
                return true;
            case MENU_PHOTO_ILLUSION /*15*/:
                Intent launchIntent = new Intent();
                try {
                    if (isIntentAvailable(Settings.PNAME_PHOTO_ILLUSION_PRO)) {
                        Log.i("Info", "Launching Photo Illusion Pro...");
                        launchIntent.setClassName(Settings.PNAME_PHOTO_ILLUSION_PRO, Settings.CNAME_PHOTO_ILLUSION_PRO);
                        startActivity(launchIntent);
                        return true;
                    } else if (isIntentAvailable(Settings.PNAME_PHOTO_ILLUSION)) {
                        Log.i("Info", "Launching Photo Illusion...");
                        launchIntent.setClassName(Settings.PNAME_PHOTO_ILLUSION, Settings.CNAME_PHOTO_ILLUSION);
                        startActivity(launchIntent);
                        return true;
                    } else {
                        Log.i("Info", "Launching Photo Illusion dialog...");
                        launchPhotoIllusionDialog();
                        return true;
                    }
                } catch (Exception e3) {
                    Log.i("Info", "intent not found: " + e3);
                }
            default:
                return false;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            switch (this.selectedMode) {
                case 1:
                    setCameraMode();
                    return true;
                case 2:
                    setIllusionMode();
                    return true;
            }
        }
        if (keyCode != 27 && (!Settings.VOLUME_KEY_SHUTTER || (keyCode != 25 && keyCode != 24))) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.selectedMode == 0) {
            Log.i("Info", "Camera button");
            this.cameraView.shotPhoto();
        }
        return true;
    }

    public void showToast(String text) {
        this.infoMsg.setDuration(0);
        this.infoMsg.setText(text);
        this.infoMsg.show();
    }

    public void showLongToast(String text) {
        this.infoMsg.setDuration(1);
        this.infoMsg.setText(text);
        this.infoMsg.show();
    }

    public Object onRetainNonConfigurationInstance() {
        return new ChangeOrientationSave(this.lastAd, getSelectedFilterID(), getSelectedEffectID(), getSelectedMaskID(), CameraRefreshThread.lastPhotoPath);
    }

    /* access modifiers changed from: package-private */
    public void setSelectedFilter(int value) {
        if (this.filterHash.contains(Integer.valueOf(value))) {
            for (int i = 0; i < this.filterHash.size(); i++) {
                if (this.filterHash.get(Integer.valueOf(i)).intValue() == value) {
                    this.s1.setSelection(i);
                    return;
                }
            }
        }
    }

    public int getSelectedFilterID() {
        return this.selectedFilterID;
    }

    public int getSelectedEffectID() {
        return this.selectedEffectID;
    }

    /* access modifiers changed from: package-private */
    public void setSelectedEffect(int value) {
        if (this.effectHash.contains(Integer.valueOf(value))) {
            for (int i = 0; i < this.effectHash.size(); i++) {
                if (this.effectHash.get(Integer.valueOf(i)).intValue() == value) {
                    this.s2.setSelection(i);
                    return;
                }
            }
        }
    }

    public int getSelectedMaskID() {
        return this.selectedMaskID;
    }

    public void setSelectedMask(int value) {
        if (this.maskHash.contains(Integer.valueOf(value))) {
            for (int i = 0; i < this.maskHash.size(); i++) {
                if (this.maskHash.get(Integer.valueOf(i)).intValue() == value) {
                    this.s3.setSelection(i);
                    return;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void updateIcon(String imageFile) {
        Log.i("Info", "Loading icon " + imageFile);
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        opt.inSampleSize = 4;
        Bitmap bitmap = BitmapFactory.decodeFile(String.valueOf(Settings.PHOTOS_DIRECTORY) + imageFile, opt);
        Matrix matrix = new Matrix();
        matrix.preScale(0.4f, 0.4f);
        this.thumbButton.setImageBitmap(Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true));
        this.thumbButton.setVisibility(0);
        bitmap.recycle();
    }

    public boolean isIntentAvailable(String name) {
        Intent mainApps = new Intent("android.intent.action.MAIN");
        mainApps.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> activities = getPackageManager().queryIntentActivities(mainApps, 0);
        for (int i = 0; i < activities.size(); i++) {
            if (name.compareTo(activities.get(i).activityInfo.packageName) == 0) {
                return true;
            }
        }
        return false;
    }

    public boolean isIntentAvailable2(String action) {
        return getPackageManager().queryIntentActivities(new Intent(action), 65536).size() > 0;
    }

    public void launchPhotoIllusionDialog() {
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon_photo_illusion).setTitle("Photo Illusion").setView(this.inflater.inflate((int) R.layout.photo_illusion_dialog, (ViewGroup) findViewById(R.id.dialog))).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                CameraActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Settings.MARKET_PHOTO_ILLUSION)));
            }
        }).setNegativeButton(17039369, (DialogInterface.OnClickListener) null).show();
    }

    public void launchCameraIllusionProDialog() {
        new AlertDialog.Builder(this).setIcon((int) R.drawable.icon_camera_illusion_pro).setTitle("Camera Illusion PRO").setView(this.inflater.inflate((int) R.layout.camera_illusion_pro_dialog, (ViewGroup) findViewById(R.id.dialog))).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                CameraActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Settings.MARKET_CAMERA_ILLUSION_PRO)));
            }
        }).setNegativeButton(17039369, (DialogInterface.OnClickListener) null).show();
    }
}
