package com.androidillusion.cameraillusion.camera;

import android.graphics.Point;
import android.hardware.Camera;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;

public class CameraManager {
    private static final Pattern COMMA_PATTERN = Pattern.compile(",");
    private static Method Parameters_getSupportedFlashModes;
    private static Method Parameters_getSupportedPreviewSizes;
    private static int defaultSizePos;
    private static Vector<Point> supportedPreviewSize;

    static {
        initCompatibility();
    }

    private static void initCompatibility() {
        try {
            Parameters_getSupportedPreviewSizes = Camera.Parameters.class.getMethod("getSupportedPreviewSizes", new Class[0]);
            Parameters_getSupportedFlashModes = Camera.Parameters.class.getMethod("getSupportedFlashModes", new Class[0]);
        } catch (NoSuchMethodException e) {
        }
    }

    public static Vector<Point> getSupportedPreviewSizes(Camera.Parameters p) {
        if (supportedPreviewSize == null) {
            supportedPreviewSize = new Vector<>();
            try {
                if (Parameters_getSupportedPreviewSizes != null) {
                    Log.i("Info", "Version 2.0+");
                    List<Camera.Size> sizeList = (List) Parameters_getSupportedPreviewSizes.invoke(p, new Object[0]);
                    for (int i = 0; i < sizeList.size(); i++) {
                        Point point = new Point();
                        point.x = ((Camera.Size) sizeList.get(i)).width;
                        point.y = ((Camera.Size) sizeList.get(i)).height;
                        if (point.x > CameraSurfaceView.screenMaxSize || point.y > CameraSurfaceView.screenMinSize) {
                            Log.i("Info", "NOT supported " + point.x + " - " + point.y);
                        } else {
                            Log.i("Info", "supported " + point.x + " - " + point.y);
                            supportedPreviewSize.add(point);
                        }
                    }
                } else {
                    Log.i("Info", "<2.0");
                    String previewSizeValueString = p.get("preview-size-values");
                    if (previewSizeValueString == null) {
                        previewSizeValueString = p.get("preview-size-value");
                    }
                    if (previewSizeValueString == null) {
                        previewSizeValueString = p.get("preview-size");
                    }
                    if (previewSizeValueString != null) {
                        Log.i("Info", "preview-size string: " + previewSizeValueString);
                        addPreviewSizeValuesString(previewSizeValueString);
                    } else {
                        Log.i("Info", "preview-size string: " + previewSizeValueString);
                    }
                }
            } catch (InvocationTargetException e) {
                InvocationTargetException ite = e;
                Throwable cause = ite.getCause();
                if (cause instanceof RuntimeException) {
                    throw ((RuntimeException) cause);
                } else if (cause instanceof Error) {
                    throw ((Error) cause);
                } else {
                    throw new RuntimeException(ite);
                }
            } catch (IllegalAccessException e2) {
                IllegalAccessException illegalAccessException = e2;
                return null;
            }
        }
        if (supportedPreviewSize.size() == 0) {
            Point point2 = new Point();
            point2.x = (CameraSurfaceView.screenMaxSize >> 3) << 3;
            point2.y = (CameraSurfaceView.screenMinSize >> 3) << 3;
            supportedPreviewSize.add(point2);
        }
        return supportedPreviewSize;
    }

    private static void addPreviewSizeValuesString(String previewSizeValueString) {
        for (String previewSize : COMMA_PATTERN.split(previewSizeValueString)) {
            String previewSize2 = previewSize.trim();
            int dimPosition = previewSize2.indexOf(120);
            if (dimPosition < 0) {
                Log.w("Info", "Bad preview-size: " + previewSize2);
            } else {
                try {
                    int newX = Integer.parseInt(previewSize2.substring(0, dimPosition));
                    int newY = Integer.parseInt(previewSize2.substring(dimPosition + 1));
                    if (newX > CameraSurfaceView.screenMaxSize || newY > CameraSurfaceView.screenMinSize) {
                        Log.i("Info", "NOT supported " + newX + " - " + newY);
                    } else {
                        Log.i("Info", "supported " + newX + " - " + newY);
                        supportedPreviewSize.add(new Point(newX, newY));
                    }
                } catch (NumberFormatException e) {
                    Log.w("Info", "Bad preview-size: " + previewSize2);
                }
            }
        }
    }

    public static int getDefaultPreviewPos(Camera.Parameters p) {
        int selected = 0;
        if (supportedPreviewSize == null) {
            getSupportedPreviewSizes(p);
        }
        int dist = CameraSurfaceView.screenMaxSize * 2;
        for (int i = 0; i < supportedPreviewSize.size(); i++) {
            Point getPoint = supportedPreviewSize.elementAt(i);
            int pointDist = (CameraSurfaceView.screenMaxSize - getPoint.x) + (CameraSurfaceView.screenMinSize - getPoint.y);
            if (pointDist < dist) {
                selected = i;
                dist = pointDist;
            }
        }
        if (selected != 0) {
            supportedPreviewSize.removeElementAt(selected);
            supportedPreviewSize.insertElementAt(supportedPreviewSize.elementAt(selected), 0);
            selected = 0;
        }
        defaultSizePos = selected;
        return defaultSizePos;
    }

    public static boolean isTorchSupported(Camera.Parameters p) {
        boolean isSupported = false;
        if (Parameters_getSupportedPreviewSizes != null) {
            Log.i("Info", "Version 2.0+");
            try {
                List<String> modeList = (List) Parameters_getSupportedFlashModes.invoke(p, new Object[0]);
                for (int i = 0; i < modeList.size(); i++) {
                    if (((String) modeList.get(i)).equalsIgnoreCase("torch")) {
                        isSupported = true;
                        Log.i("Info", "TorchMode Supported");
                    }
                }
            } catch (Exception e) {
            }
        }
        return isSupported;
    }

    public static void setTorchMode(Camera.Parameters p, Camera camera, boolean enable) {
        Method methodSetFlashMode = null;
        try {
            try {
                methodSetFlashMode = p.getClass().getMethod("setFlashMode", String.class);
            } catch (NoSuchMethodException e) {
                NoSuchMethodException noSuchMethodException = e;
                Log.i("Info", "No method");
            }
            if (methodSetFlashMode != null) {
                Object[] oArgs = new Object[1];
                if (!enable) {
                    oArgs[0] = "off";
                } else {
                    oArgs[0] = "torch";
                }
                methodSetFlashMode.invoke(p, oArgs);
                Log.i("Info", "invoke");
                camera.setParameters(p);
            }
        } catch (Exception e2) {
            Log.i("Info", "Exception");
        }
    }
}
