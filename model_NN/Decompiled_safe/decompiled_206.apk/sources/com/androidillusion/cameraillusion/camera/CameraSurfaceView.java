package com.androidillusion.cameraillusion.camera;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.androidillusion.cameraillusion.CameraActivity;
import com.androidillusion.cameraillusion.R;
import com.androidillusion.cameraillusion.algorithm.ImageProccesing;
import com.androidillusion.cameraillusion.config.Settings;
import com.androidillusion.cameraillusion.media.SoundManager;
import java.io.IOException;
import java.util.Vector;

public class CameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    public static Vector<Point> availableResVector;
    public static int cameraMaxSize;
    public static int cameraMinSize;
    public static int defaultRes;
    public static boolean isTorchAvailable = false;
    public static boolean restartCamera = false;
    public static int screenMaxSize;
    public static int screenMinSize;
    public static int selectedRes = -1;
    CameraActivity activity;
    CameraSurfaceView instance;
    public boolean isFocusing = false;
    Camera mCamera;
    SurfaceHolder mHolder;
    public Camera.PreviewCallback pC;
    public CameraRefreshThread paintTh;

    public CameraSurfaceView(Context context) {
        super(context);
        this.activity = (CameraActivity) context;
        this.instance = this;
        SoundManager.getInstance();
        this.mHolder = getHolder();
        this.mHolder.addCallback(this);
        this.mHolder.setType(3);
        this.pC = new Camera.PreviewCallback() {
            public void onPreviewFrame(byte[] data, Camera camera) {
                if (!CameraRefreshThread.isProccesingFrame) {
                    CameraRefreshThread.isProccesingFrame = true;
                    CameraSurfaceView.this.paintTh = new CameraRefreshThread(CameraSurfaceView.this.mHolder, CameraSurfaceView.this.mCamera, CameraSurfaceView.this.instance);
                    CameraRefreshThread.previewYUVImage = data;
                    if (CameraRefreshThread.isNewPhoto) {
                        CameraActivity.instance.updateIcon(CameraRefreshThread.lastPhotoPath);
                        CameraRefreshThread.isNewPhoto = false;
                    }
                    CameraSurfaceView.this.paintTh.start();
                    return;
                }
                Log.i("Info", "Frame out");
            }
        };
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        int max = Math.max(CameraActivity.instance.getWindow().getWindowManager().getDefaultDisplay().getHeight(), CameraActivity.instance.getWindow().getWindowManager().getDefaultDisplay().getWidth());
        cameraMaxSize = max;
        screenMaxSize = max;
        int min = Math.min(CameraActivity.instance.getWindow().getWindowManager().getDefaultDisplay().getHeight(), CameraActivity.instance.getWindow().getWindowManager().getDefaultDisplay().getWidth());
        cameraMinSize = min;
        screenMinSize = min;
        if (ImageProccesing.temp == null) {
            ImageProccesing.temp = new int[(cameraMaxSize * cameraMinSize)];
        }
        try {
            configCamera(holder);
        } catch (Exception e) {
            CameraActivity.instance.finish();
        }
    }

    private void configCamera(SurfaceHolder holder) {
        this.mCamera = Camera.open();
        try {
            this.mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            this.mCamera.release();
            this.mCamera = null;
        }
        Camera.Parameters parameters = this.mCamera.getParameters();
        isTorchAvailable = CameraManager.isTorchSupported(parameters);
        if (isTorchAvailable) {
            this.activity.flashButton.setVisibility(0);
        }
        availableResVector = CameraManager.getSupportedPreviewSizes(parameters);
        defaultRes = CameraManager.getDefaultPreviewPos(parameters);
        if (selectedRes == -1) {
            selectedRes = defaultRes;
        }
        if (selectedRes != defaultRes) {
            this.activity.s3.setEnabled(false);
        } else {
            this.activity.s3.setEnabled(true);
        }
        cameraMaxSize = availableResVector.elementAt(selectedRes).x;
        cameraMinSize = availableResVector.elementAt(selectedRes).y;
        if (CameraActivity.orientation == 1) {
            CustomCameraView.posY = (screenMaxSize - cameraMaxSize) >> 1;
            CustomCameraView.posX = (screenMinSize - cameraMinSize) >> 1;
        } else {
            CustomCameraView.posX = (screenMaxSize - cameraMaxSize) >> 1;
            CustomCameraView.posY = (screenMinSize - cameraMinSize) >> 1;
        }
        if (Settings.STRETCH_PICTURE) {
            CustomCameraView.posX = 0;
            CustomCameraView.posY = 0;
        }
        parameters.setPreviewSize(cameraMaxSize, cameraMinSize);
        this.activity.flashButton.setBackgroundResource(R.drawable.unselected_flash);
        try {
            this.mCamera.setParameters(parameters);
            this.mCamera.startPreview();
            startListen();
        } catch (Exception e2) {
            selectedRes = 0;
        }
    }

    public void startListen() {
        if (this.mCamera != null) {
            if (restartCamera) {
                restartCamera();
                restartCamera = false;
            }
            this.mCamera.setOneShotPreviewCallback(this.pC);
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (this.mCamera != null) {
            this.mCamera.stopPreview();
            this.mCamera.release();
            this.mCamera = null;
        }
    }

    public void shotPhoto() {
        if (CameraRefreshThread.isSavingFrame) {
            return;
        }
        if (Settings.AUTOFUCUS_BEFORE_SHOT) {
            this.isFocusing = true;
            this.mCamera.autoFocus(new Camera.AutoFocusCallback() {
                public void onAutoFocus(boolean success, Camera camera) {
                    CameraSurfaceView.this.isFocusing = false;
                    CameraRefreshThread.isSavingFrame = true;
                }
            });
            return;
        }
        CameraRefreshThread.isSavingFrame = true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (Settings.TOUCH_SCREEN_AUTOFOCUS && !this.isFocusing) {
            this.isFocusing = true;
            this.mCamera.autoFocus(new Camera.AutoFocusCallback() {
                public void onAutoFocus(boolean success, Camera camera) {
                    CameraSurfaceView.this.isFocusing = false;
                }
            });
        }
        return true;
    }

    public void restartCamera() {
        CustomCameraView.instance.mBitmap = null;
        this.mCamera.stopPreview();
        Log.i("Info", "1");
        Camera.Parameters parameters = this.mCamera.getParameters();
        availableResVector = CameraManager.getSupportedPreviewSizes(parameters);
        defaultRes = CameraManager.getDefaultPreviewPos(parameters);
        if (selectedRes == -1) {
            selectedRes = defaultRes;
        }
        if (selectedRes != defaultRes) {
            this.activity.s3.setEnabled(false);
        } else {
            this.activity.s3.setEnabled(true);
        }
        cameraMaxSize = availableResVector.elementAt(selectedRes).x;
        cameraMinSize = availableResVector.elementAt(selectedRes).y;
        if (CameraActivity.orientation == 1) {
            CustomCameraView.posY = (screenMaxSize - cameraMaxSize) >> 1;
            CustomCameraView.posX = (screenMinSize - cameraMinSize) >> 1;
        } else {
            CustomCameraView.posX = (screenMaxSize - cameraMaxSize) >> 1;
            CustomCameraView.posY = (screenMinSize - cameraMinSize) >> 1;
        }
        if (Settings.STRETCH_PICTURE) {
            CustomCameraView.posX = 0;
            CustomCameraView.posY = 0;
        }
        parameters.setPreviewSize(cameraMaxSize, cameraMinSize);
        this.activity.flashButton.setBackgroundResource(R.drawable.unselected_flash);
        this.mCamera.setParameters(parameters);
        this.mCamera.startPreview();
        CameraRefreshThread.endThread = false;
        CameraRefreshThread.isProccesingFrame = false;
        ImageProccesing.custom = null;
        ImageProccesing.temp = null;
        this.activity.setSelectedMask(R.id.mask1);
        CameraRefreshThread.init = true;
        CustomCameraView.instance.mBitmap = null;
        CustomCameraView.instance.postInvalidate();
        this.mCamera.startPreview();
    }

    public void setTorchMode(boolean enable) {
        CameraManager.setTorchMode(this.mCamera.getParameters(), this.mCamera, enable);
    }
}
