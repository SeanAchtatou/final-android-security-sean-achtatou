package com.androidillusion.cameraillusion.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import com.androidillusion.cameraillusion.CameraActivity;
import com.androidillusion.cameraillusion.config.Settings;
import java.text.DecimalFormat;

public class CustomCameraView extends View {
    public static CustomCameraView instance;
    public static int posX;
    public static int posY;
    DecimalFormat dec = new DecimalFormat("###.##");
    int gap;
    public Bitmap mBitmap;
    int max;
    Paint paint;
    Paint textPaint;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public CustomCameraView(Context context) {
        super(context);
        instance = this;
        this.max = Math.max(CameraActivity.instance.getWindow().getWindowManager().getDefaultDisplay().getWidth(), CameraActivity.instance.getWindow().getWindowManager().getDefaultDisplay().getHeight());
        this.paint = new Paint();
        this.paint.setColor(-16777216);
        this.paint.setStyle(Paint.Style.FILL);
        this.gap = this.max / 28;
        this.textPaint = new Paint();
        this.textPaint.setColor(-1);
        this.textPaint.setTextSize((float) this.gap);
        this.textPaint.setShadowLayer(3.0f, 3.0f, 3.0f, -16777216);
        this.textPaint.setTextAlign(Paint.Align.RIGHT);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.drawRect(0.0f, 0.0f, (float) this.max, (float) this.max, this.paint);
        if (this.mBitmap != null) {
            canvas.drawBitmap(this.mBitmap, (float) posX, (float) posY, (Paint) null);
        }
        if (Settings.DEBUG_MODE) {
            canvas.drawText("res: " + CameraSurfaceView.cameraMaxSize + " x " + CameraSurfaceView.cameraMinSize, (float) (canvas.getWidth() - 10), (float) ((canvas.getHeight() / 3) - this.gap), this.textPaint);
            canvas.drawText("fps: " + this.dec.format(CameraRefreshThread.fps), (float) (canvas.getWidth() - 10), (float) ((canvas.getHeight() / 3) + this.gap), this.textPaint);
        }
    }
}
