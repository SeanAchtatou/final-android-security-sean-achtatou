package com.androidillusion.cameraillusion.camera;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.view.SurfaceHolder;
import com.androidillusion.cameraillusion.CameraActivity;
import com.androidillusion.cameraillusion.algorithm.ImageProccesing;
import com.androidillusion.cameraillusion.masks.Mask;

public class CameraRefreshThread extends Thread {
    public static char[][] asciiBuffer;
    public static Bitmap canvasBitmap;
    public static boolean effectChanged = false;
    public static boolean endThread = false;
    public static double fps;
    public static Bitmap imageBitmap;
    public static boolean init = true;
    static long initTime;
    public static boolean isMaskChanged = false;
    public static boolean isNewPhoto = false;
    public static boolean isProccesingFrame = false;
    public static boolean isSavingFrame = false;
    public static String lastPhotoPath = "";
    public static Uri lastPhotoUri = null;
    public static Matrix matrix;
    public static boolean portraitMode;
    public static byte[] previewYUVImage;
    public static int[] rgbImage;
    public static Bitmap saveMaskBitMap;
    public static Mask selectedMask;
    static int times;
    public static int wait = 0;
    Canvas canvas = new Canvas();
    Camera mCamera;
    SurfaceHolder mHolder;
    CameraSurfaceView mSurface;
    long time1;
    long time2;
    long time3;
    int u;
    int v;

    public CameraRefreshThread(SurfaceHolder mHolder2, Camera mCamera2, CameraSurfaceView mSurface2) {
        this.mHolder = mHolder2;
        this.mCamera = mCamera2;
        this.mSurface = mSurface2;
        if (effectChanged) {
            ImageProccesing.custom = null;
            effectChanged = false;
        }
        matrix = new Matrix();
        if (CameraActivity.orientation == 1) {
            matrix.preRotate(90.0f);
            matrix.postTranslate((float) CameraSurfaceView.cameraMinSize, 0.0f);
        }
        if (init) {
            System.gc();
            if (CameraActivity.orientation == 1) {
                portraitMode = true;
            } else {
                portraitMode = false;
            }
            rgbImage = new int[(CameraSurfaceView.cameraMaxSize * CameraSurfaceView.cameraMinSize)];
            if (CameraActivity.orientation == 1) {
                canvasBitmap = Bitmap.createBitmap(CameraSurfaceView.cameraMinSize, CameraSurfaceView.cameraMaxSize, Bitmap.Config.RGB_565);
            } else if (CameraActivity.orientation == 2) {
                canvasBitmap = Bitmap.createBitmap(CameraSurfaceView.cameraMaxSize, CameraSurfaceView.cameraMinSize, Bitmap.Config.RGB_565);
            }
            imageBitmap = Bitmap.createBitmap(CameraSurfaceView.cameraMaxSize, CameraSurfaceView.cameraMinSize, Bitmap.Config.RGB_565);
            Mask.loadMaskBitmap();
            init = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0439 A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0447 A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0455 A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0463 A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0471 A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x047f A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x048d A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0498 A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x04a3 A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x04ae A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x04b9 A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x04c4 A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x04cf A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0066 A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0076 A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0089 A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00cb A[Catch:{ Exception -> 0x02a0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x013d A[Catch:{ Exception -> 0x02a0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r24 = this;
            com.androidillusion.cameraillusion.CameraActivity r5 = com.androidillusion.cameraillusion.CameraActivity.instance     // Catch:{ Exception -> 0x02e0 }
            int r20 = r5.getSelectedFilterID()     // Catch:{ Exception -> 0x02e0 }
            com.androidillusion.cameraillusion.CameraActivity r5 = com.androidillusion.cameraillusion.CameraActivity.instance     // Catch:{ Exception -> 0x02e0 }
            int r19 = r5.getSelectedEffectID()     // Catch:{ Exception -> 0x02e0 }
            com.androidillusion.cameraillusion.CameraActivity r5 = com.androidillusion.cameraillusion.CameraActivity.instance     // Catch:{ Exception -> 0x02e0 }
            int r21 = r5.getSelectedMaskID()     // Catch:{ Exception -> 0x02e0 }
            r0 = r24
            android.graphics.Canvas r0 = r0.canvas     // Catch:{ Exception -> 0x02a0 }
            r5 = r0
            android.graphics.Bitmap r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.canvasBitmap     // Catch:{ Exception -> 0x02a0 }
            r5.setBitmap(r6)     // Catch:{ Exception -> 0x02a0 }
            r0 = r24
            android.view.SurfaceHolder r0 = r0.mHolder     // Catch:{ Exception -> 0x02a0 }
            r13 = r0
            monitor-enter(r13)     // Catch:{ Exception -> 0x02a0 }
            android.graphics.Paint r18 = new android.graphics.Paint     // Catch:{ all -> 0x029d }
            r18.<init>()     // Catch:{ all -> 0x029d }
            android.graphics.Paint$Style r5 = android.graphics.Paint.Style.FILL     // Catch:{ all -> 0x029d }
            r0 = r18
            r1 = r5
            r0.setStyle(r1)     // Catch:{ all -> 0x029d }
            r5 = -1
            r0 = r18
            r1 = r5
            r0.setColor(r1)     // Catch:{ all -> 0x029d }
            r0 = r24
            android.graphics.Canvas r0 = r0.canvas     // Catch:{ all -> 0x029d }
            r5 = r0
            r0 = r5
            r1 = r18
            r0.drawPaint(r1)     // Catch:{ all -> 0x029d }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x029d }
            r0 = r5
            r2 = r24
            r2.time1 = r0     // Catch:{ all -> 0x029d }
            switch(r20) {
                case 2131099672: goto L_0x0290;
                case 2131099673: goto L_0x02fd;
                case 2131099674: goto L_0x030a;
                case 2131099675: goto L_0x0317;
                case 2131099676: goto L_0x033b;
                case 2131099677: goto L_0x0360;
                case 2131099678: goto L_0x03b6;
                case 2131099679: goto L_0x03c4;
                case 2131099680: goto L_0x03d2;
                case 2131099681: goto L_0x03ec;
                case 2131099682: goto L_0x03f9;
                case 2131099683: goto L_0x03df;
                case 2131099684: goto L_0x036d;
                case 2131099685: goto L_0x037a;
                case 2131099686: goto L_0x0387;
                case 2131099687: goto L_0x0396;
                case 2131099688: goto L_0x03a6;
                case 2131099689: goto L_0x0420;
                case 2131099690: goto L_0x0406;
                case 2131099691: goto L_0x0413;
                default: goto L_0x004d;
            }     // Catch:{ all -> 0x029d }
        L_0x004d:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterNone(r5, r6, r7, r8)     // Catch:{ all -> 0x029d }
        L_0x0058:
            switch(r19) {
                case 2131099658: goto L_0x005b;
                case 2131099659: goto L_0x0439;
                case 2131099660: goto L_0x0447;
                case 2131099661: goto L_0x0455;
                case 2131099662: goto L_0x0463;
                case 2131099663: goto L_0x0471;
                case 2131099664: goto L_0x047f;
                case 2131099665: goto L_0x048d;
                case 2131099666: goto L_0x0498;
                case 2131099667: goto L_0x04a3;
                case 2131099668: goto L_0x04ae;
                case 2131099669: goto L_0x04b9;
                case 2131099670: goto L_0x04c4;
                case 2131099671: goto L_0x04cf;
                default: goto L_0x005b;
            }     // Catch:{ all -> 0x029d }
        L_0x005b:
            com.androidillusion.cameraillusion.CameraActivity r5 = com.androidillusion.cameraillusion.CameraActivity.instance     // Catch:{ all -> 0x029d }
            int r5 = r5.getSelectedFilterID()     // Catch:{ all -> 0x029d }
            r6 = 2131099681(0x7f060021, float:1.7811722E38)
            if (r5 != r6) goto L_0x0072
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            char[][] r5 = com.androidillusion.cameraillusion.algorithm.ImageProccesing.filterAsciiArt(r5, r6, r7)     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.asciiBuffer = r5     // Catch:{ all -> 0x029d }
        L_0x0072:
            boolean r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.isMaskChanged     // Catch:{ all -> 0x029d }
            if (r5 == 0) goto L_0x007c
            com.androidillusion.cameraillusion.masks.Mask.loadMaskBitmap()     // Catch:{ all -> 0x029d }
            r5 = 0
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.isMaskChanged = r5     // Catch:{ all -> 0x029d }
        L_0x007c:
            r5 = 8
            r0 = r5
            float[] r0 = new float[r0]     // Catch:{ all -> 0x029d }
            r17 = r0
            r17 = {0, 0, 1139802112, 0, 1139802112, 1134559232, 0, 1134559232} // fill-array     // Catch:{ all -> 0x029d }
            switch(r21) {
                case 2131099709: goto L_0x0091;
                default: goto L_0x0089;
            }     // Catch:{ all -> 0x029d }
        L_0x0089:
            com.androidillusion.cameraillusion.masks.Mask r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.selectedMask     // Catch:{ all -> 0x029d }
            android.graphics.Matrix r5 = r5.getOriginalMatrix()     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.matrix = r5     // Catch:{ all -> 0x029d }
        L_0x0091:
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x029d }
            r0 = r5
            r2 = r24
            r2.time2 = r0     // Catch:{ all -> 0x029d }
            android.graphics.Bitmap r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.imageBitmap     // Catch:{ all -> 0x029d }
            int[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            r7 = 0
            android.graphics.Bitmap r8 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.imageBitmap     // Catch:{ all -> 0x029d }
            int r8 = r8.getWidth()     // Catch:{ all -> 0x029d }
            r9 = 0
            r10 = 0
            android.graphics.Bitmap r11 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.imageBitmap     // Catch:{ all -> 0x029d }
            int r11 = r11.getWidth()     // Catch:{ all -> 0x029d }
            android.graphics.Bitmap r12 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.imageBitmap     // Catch:{ all -> 0x029d }
            int r12 = r12.getHeight()     // Catch:{ all -> 0x029d }
            r5.setPixels(r6, r7, r8, r9, r10, r11, r12)     // Catch:{ all -> 0x029d }
            r0 = r24
            android.graphics.Canvas r0 = r0.canvas     // Catch:{ all -> 0x029d }
            r5 = r0
            android.graphics.Bitmap r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.imageBitmap     // Catch:{ all -> 0x029d }
            android.graphics.Matrix r7 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.matrix     // Catch:{ all -> 0x029d }
            r8 = 0
            r5.drawBitmap(r6, r7, r8)     // Catch:{ all -> 0x029d }
            r5 = 2131099709(0x7f06003d, float:1.7811779E38)
            r0 = r21
            r1 = r5
            if (r0 == r1) goto L_0x0130
            r22 = 0
            r23 = 0
            boolean r5 = com.androidillusion.cameraillusion.masks.Mask.isCustomMask     // Catch:{ all -> 0x029d }
            if (r5 == 0) goto L_0x0114
            int r5 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.masks.Mask r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.selectedMask     // Catch:{ all -> 0x029d }
            android.graphics.Bitmap r6 = r6.getOriginalBitmap()     // Catch:{ all -> 0x029d }
            int r6 = r6.getWidth()     // Catch:{ all -> 0x029d }
            int r5 = r5 - r6
            int r22 = r5 / 2
            int r5 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.masks.Mask r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.selectedMask     // Catch:{ all -> 0x029d }
            android.graphics.Bitmap r6 = r6.getOriginalBitmap()     // Catch:{ all -> 0x029d }
            int r6 = r6.getHeight()     // Catch:{ all -> 0x029d }
            int r5 = r5 - r6
            int r23 = r5 / 2
            int r5 = com.androidillusion.cameraillusion.CameraActivity.orientation     // Catch:{ all -> 0x029d }
            r6 = 2
            if (r5 != r6) goto L_0x0114
            int r5 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.masks.Mask r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.selectedMask     // Catch:{ all -> 0x029d }
            android.graphics.Bitmap r6 = r6.getOriginalBitmap()     // Catch:{ all -> 0x029d }
            int r6 = r6.getWidth()     // Catch:{ all -> 0x029d }
            int r5 = r5 - r6
            int r22 = r5 / 2
            int r5 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.masks.Mask r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.selectedMask     // Catch:{ all -> 0x029d }
            android.graphics.Bitmap r6 = r6.getOriginalBitmap()     // Catch:{ all -> 0x029d }
            int r6 = r6.getHeight()     // Catch:{ all -> 0x029d }
            int r5 = r5 - r6
            int r23 = r5 / 2
        L_0x0114:
            r0 = r24
            android.graphics.Canvas r0 = r0.canvas     // Catch:{ all -> 0x029d }
            r5 = r0
            com.androidillusion.cameraillusion.masks.Mask r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.selectedMask     // Catch:{ all -> 0x029d }
            android.graphics.Bitmap r6 = r6.getOriginalBitmap()     // Catch:{ all -> 0x029d }
            r0 = r22
            float r0 = (float) r0     // Catch:{ all -> 0x029d }
            r7 = r0
            r0 = r23
            float r0 = (float) r0     // Catch:{ all -> 0x029d }
            r8 = r0
            r0 = r5
            r1 = r6
            r2 = r7
            r3 = r8
            r4 = r18
            r0.drawBitmap(r1, r2, r3, r4)     // Catch:{ all -> 0x029d }
        L_0x0130:
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x029d }
            r0 = r5
            r2 = r24
            r2.time3 = r0     // Catch:{ all -> 0x029d }
            boolean r5 = com.androidillusion.cameraillusion.config.Settings.DEBUG_MODE     // Catch:{ all -> 0x029d }
            if (r5 == 0) goto L_0x0162
            int r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.times     // Catch:{ all -> 0x029d }
            int r5 = r5 % 10
            if (r5 != 0) goto L_0x015c
            r5 = 4666723172467343360(0x40c3880000000000, double:10000.0)
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x029d }
            long r9 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.initTime     // Catch:{ all -> 0x029d }
            long r7 = r7 - r9
            double r7 = (double) r7     // Catch:{ all -> 0x029d }
            double r5 = r5 / r7
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.fps = r5     // Catch:{ all -> 0x029d }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.initTime = r5     // Catch:{ all -> 0x029d }
            r5 = 0
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.times = r5     // Catch:{ all -> 0x029d }
        L_0x015c:
            int r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.times     // Catch:{ all -> 0x029d }
            int r5 = r5 + 1
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.times = r5     // Catch:{ all -> 0x029d }
        L_0x0162:
            monitor-exit(r13)     // Catch:{ all -> 0x029d }
            r0 = r24
            com.androidillusion.cameraillusion.camera.CameraSurfaceView r0 = r0.mSurface     // Catch:{ Exception -> 0x02e0 }
            r5 = r0
            if (r5 == 0) goto L_0x018b
            boolean r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.endThread     // Catch:{ Exception -> 0x02e0 }
            if (r5 != 0) goto L_0x018b
            boolean r5 = com.androidillusion.cameraillusion.config.Settings.STRETCH_PICTURE     // Catch:{ Exception -> 0x02e0 }
            if (r5 == 0) goto L_0x054d
            int r5 = com.androidillusion.cameraillusion.CameraActivity.orientation     // Catch:{ Exception -> 0x02e0 }
            r6 = 1
            if (r5 != r6) goto L_0x053c
            com.androidillusion.cameraillusion.camera.CustomCameraView r5 = com.androidillusion.cameraillusion.camera.CustomCameraView.instance     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.canvasBitmap     // Catch:{ Exception -> 0x02e0 }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.screenMinSize     // Catch:{ Exception -> 0x02e0 }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.screenMaxSize     // Catch:{ Exception -> 0x02e0 }
            r9 = 0
            android.graphics.Bitmap r6 = android.graphics.Bitmap.createScaledBitmap(r6, r7, r8, r9)     // Catch:{ Exception -> 0x02e0 }
            r5.mBitmap = r6     // Catch:{ Exception -> 0x02e0 }
        L_0x0186:
            com.androidillusion.cameraillusion.camera.CustomCameraView r5 = com.androidillusion.cameraillusion.camera.CustomCameraView.instance     // Catch:{ Exception -> 0x02e0 }
            r5.postInvalidate()     // Catch:{ Exception -> 0x02e0 }
        L_0x018b:
            boolean r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.isSavingFrame     // Catch:{ Exception -> 0x02e0 }
            if (r5 == 0) goto L_0x0279
            int r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.wait     // Catch:{ Exception -> 0x02e0 }
            int r6 = r5 + 1
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.wait = r6     // Catch:{ Exception -> 0x02e0 }
            r6 = 1
            if (r5 <= r6) goto L_0x0279
            com.androidillusion.cameraillusion.media.MediaManager r16 = com.androidillusion.cameraillusion.media.MediaManager.getInstance()     // Catch:{ Exception -> 0x02e0 }
            boolean r5 = r16.checkSD()     // Catch:{ Exception -> 0x02e0 }
            if (r5 == 0) goto L_0x0559
            java.lang.String r5 = "Info"
            java.lang.String r6 = "SD available"
            android.util.Log.i(r5, r6)     // Catch:{ Exception -> 0x02e0 }
            java.lang.String r5 = com.androidillusion.cameraillusion.config.Settings.PHOTOS_DIRECTORY     // Catch:{ Exception -> 0x02e0 }
            r0 = r16
            r1 = r5
            boolean r5 = r0.checkDirectory(r1)     // Catch:{ Exception -> 0x02e0 }
            if (r5 != 0) goto L_0x01d2
            java.lang.String r5 = com.androidillusion.cameraillusion.config.Settings.PHOTOS_DIRECTORY     // Catch:{ Exception -> 0x02e0 }
            r0 = r16
            r1 = r5
            r0.createDirectory(r1)     // Catch:{ Exception -> 0x02e0 }
            java.lang.String r5 = "Info"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02e0 }
            java.lang.String r7 = "Creating dir: "
            r6.<init>(r7)     // Catch:{ Exception -> 0x02e0 }
            java.lang.String r7 = com.androidillusion.cameraillusion.config.Settings.PHOTOS_DIRECTORY     // Catch:{ Exception -> 0x02e0 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x02e0 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x02e0 }
            android.util.Log.i(r5, r6)     // Catch:{ Exception -> 0x02e0 }
        L_0x01d2:
            boolean r5 = com.androidillusion.cameraillusion.config.Settings.CAMERA_SOUND     // Catch:{ Exception -> 0x02e0 }
            if (r5 == 0) goto L_0x01dd
            com.androidillusion.cameraillusion.media.SoundManager r5 = com.androidillusion.cameraillusion.media.SoundManager.getInstance()     // Catch:{ Exception -> 0x02e0 }
            r5.shotSound()     // Catch:{ Exception -> 0x02e0 }
        L_0x01dd:
            java.lang.String r15 = r16.getDateFileName()     // Catch:{ Exception -> 0x02e0 }
            java.lang.String r12 = com.androidillusion.cameraillusion.config.Settings.PHOTOS_DIRECTORY     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.canvasBitmap     // Catch:{ Exception -> 0x02e0 }
            r6 = 0
            r7 = 0
            android.graphics.Bitmap r8 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.canvasBitmap     // Catch:{ Exception -> 0x02e0 }
            int r8 = r8.getWidth()     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r9 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.canvasBitmap     // Catch:{ Exception -> 0x02e0 }
            int r9 = r9.getHeight()     // Catch:{ Exception -> 0x02e0 }
            r10 = 0
            r11 = 1
            android.graphics.Bitmap r5 = android.graphics.Bitmap.createBitmap(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ Exception -> 0x02e0 }
            r0 = r16
            r1 = r15
            r2 = r12
            r3 = r5
            r0.saveFile(r1, r2, r3)     // Catch:{ Exception -> 0x02e0 }
            com.androidillusion.cameraillusion.CameraActivity r5 = com.androidillusion.cameraillusion.CameraActivity.instance     // Catch:{ Exception -> 0x02e0 }
            int r5 = r5.getSelectedFilterID()     // Catch:{ Exception -> 0x02e0 }
            r6 = 2131099681(0x7f060021, float:1.7811722E38)
            if (r5 != r6) goto L_0x021f
            boolean r5 = com.androidillusion.cameraillusion.config.Settings.SAVE_HTML_ASCII_ART     // Catch:{ Exception -> 0x02e0 }
            if (r5 == 0) goto L_0x021f
            java.lang.String r5 = com.androidillusion.cameraillusion.config.Settings.PHOTOS_DIRECTORY     // Catch:{ Exception -> 0x02e0 }
            char[][] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.asciiBuffer     // Catch:{ Exception -> 0x02e0 }
            boolean r7 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.portraitMode     // Catch:{ Exception -> 0x02e0 }
            r0 = r16
            r1 = r15
            r2 = r5
            r3 = r6
            r4 = r7
            r0.saveAscii(r1, r2, r3, r4)     // Catch:{ Exception -> 0x02e0 }
        L_0x021f:
            java.lang.String r5 = "Info"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02e0 }
            java.lang.String r7 = "Saving: "
            r6.<init>(r7)     // Catch:{ Exception -> 0x02e0 }
            java.lang.String r7 = com.androidillusion.cameraillusion.config.Settings.PHOTOS_DIRECTORY     // Catch:{ Exception -> 0x02e0 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x02e0 }
            java.lang.StringBuilder r6 = r6.append(r15)     // Catch:{ Exception -> 0x02e0 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x02e0 }
            android.util.Log.i(r5, r6)     // Catch:{ Exception -> 0x02e0 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02e0 }
            java.lang.String r6 = com.androidillusion.cameraillusion.config.Settings.PHOTOS_DIRECTORY     // Catch:{ Exception -> 0x02e0 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x02e0 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x02e0 }
            java.lang.StringBuilder r5 = r5.append(r15)     // Catch:{ Exception -> 0x02e0 }
            java.lang.String r6 = r5.toString()     // Catch:{ Exception -> 0x02e0 }
            java.lang.String r7 = ""
            java.lang.String r9 = ""
            long r10 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x02e0 }
            r12 = 0
            r13 = 0
            r5 = r16
            r8 = r15
            android.net.Uri r5 = r5.saveMediaEntry(r6, r7, r8, r9, r10, r12, r13)     // Catch:{ Exception -> 0x02e0 }
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.lastPhotoUri = r5     // Catch:{ Exception -> 0x02e0 }
            com.androidillusion.cameraillusion.CameraActivity r5 = com.androidillusion.cameraillusion.CameraActivity.instance     // Catch:{ Exception -> 0x02e0 }
            r6 = 2131034182(0x7f050046, float:1.7678874E38)
            java.lang.String r5 = r5.getString(r6)     // Catch:{ Exception -> 0x02e0 }
            r0 = r24
            r1 = r5
            r0.showToast(r1)     // Catch:{ Exception -> 0x02e0 }
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.lastPhotoPath = r15     // Catch:{ Exception -> 0x02e0 }
            r5 = 1
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.isNewPhoto = r5     // Catch:{ Exception -> 0x02e0 }
        L_0x0273:
            r5 = 0
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.isSavingFrame = r5     // Catch:{ Exception -> 0x02e0 }
            r5 = 0
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.wait = r5     // Catch:{ Exception -> 0x02e0 }
        L_0x0279:
            r5 = 0
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.isProccesingFrame = r5     // Catch:{ Exception -> 0x02e0 }
            r0 = r24
            com.androidillusion.cameraillusion.camera.CameraSurfaceView r0 = r0.mSurface     // Catch:{ Exception -> 0x02e0 }
            r5 = r0
            if (r5 == 0) goto L_0x0571
            boolean r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.endThread     // Catch:{ Exception -> 0x02e0 }
            if (r5 != 0) goto L_0x0571
            r0 = r24
            com.androidillusion.cameraillusion.camera.CameraSurfaceView r0 = r0.mSurface     // Catch:{ Exception -> 0x02e0 }
            r5 = r0
            r5.startListen()     // Catch:{ Exception -> 0x02e0 }
        L_0x028f:
            return
        L_0x0290:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterNone(r5, r6, r7, r8)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x029d:
            r5 = move-exception
            monitor-exit(r13)     // Catch:{ all -> 0x029d }
            throw r5     // Catch:{ Exception -> 0x02a0 }
        L_0x02a0:
            r5 = move-exception
            r14 = r5
            java.lang.String r5 = "Info"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x04f7 }
            java.lang.String r7 = "Exception painting "
            r6.<init>(r7)     // Catch:{ all -> 0x04f7 }
            java.lang.StringBuilder r6 = r6.append(r14)     // Catch:{ all -> 0x04f7 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x04f7 }
            android.util.Log.i(r5, r6)     // Catch:{ all -> 0x04f7 }
            r0 = r24
            com.androidillusion.cameraillusion.camera.CameraSurfaceView r0 = r0.mSurface     // Catch:{ Exception -> 0x02e0 }
            r5 = r0
            if (r5 == 0) goto L_0x018b
            boolean r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.endThread     // Catch:{ Exception -> 0x02e0 }
            if (r5 != 0) goto L_0x018b
            boolean r5 = com.androidillusion.cameraillusion.config.Settings.STRETCH_PICTURE     // Catch:{ Exception -> 0x02e0 }
            if (r5 == 0) goto L_0x04eb
            int r5 = com.androidillusion.cameraillusion.CameraActivity.orientation     // Catch:{ Exception -> 0x02e0 }
            r6 = 1
            if (r5 != r6) goto L_0x04da
            com.androidillusion.cameraillusion.camera.CustomCameraView r5 = com.androidillusion.cameraillusion.camera.CustomCameraView.instance     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.canvasBitmap     // Catch:{ Exception -> 0x02e0 }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.screenMinSize     // Catch:{ Exception -> 0x02e0 }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.screenMaxSize     // Catch:{ Exception -> 0x02e0 }
            r9 = 0
            android.graphics.Bitmap r6 = android.graphics.Bitmap.createScaledBitmap(r6, r7, r8, r9)     // Catch:{ Exception -> 0x02e0 }
            r5.mBitmap = r6     // Catch:{ Exception -> 0x02e0 }
        L_0x02d9:
            com.androidillusion.cameraillusion.camera.CustomCameraView r5 = com.androidillusion.cameraillusion.camera.CustomCameraView.instance     // Catch:{ Exception -> 0x02e0 }
            r5.postInvalidate()     // Catch:{ Exception -> 0x02e0 }
            goto L_0x018b
        L_0x02e0:
            r5 = move-exception
            r14 = r5
            java.lang.String r5 = "Info"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "Exception thread "
            r6.<init>(r7)
            java.lang.StringBuilder r6 = r6.append(r14)
            java.lang.String r6 = r6.toString()
            android.util.Log.i(r5, r6)
            r5 = 0
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.isProccesingFrame = r5
            r5 = 0
            com.androidillusion.cameraillusion.camera.CameraRefreshThread.isSavingFrame = r5
            goto L_0x028f
        L_0x02fd:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterMono(r5, r6, r7, r8)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x030a:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterNegative(r5, r6, r7, r8)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x0317:
            r5 = -8
            r0 = r5
            r1 = r24
            r1.u = r0     // Catch:{ all -> 0x029d }
            r5 = 21
            r0 = r5
            r1 = r24
            r1.v = r0     // Catch:{ all -> 0x029d }
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            r0 = r24
            int r0 = r0.u     // Catch:{ all -> 0x029d }
            r9 = r0
            r0 = r24
            int r0 = r0.v     // Catch:{ all -> 0x029d }
            r10 = r0
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterColorUV(r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x033b:
            r5 = 19
            r0 = r5
            r1 = r24
            r1.u = r0     // Catch:{ all -> 0x029d }
            r5 = -73
            r0 = r5
            r1 = r24
            r1.v = r0     // Catch:{ all -> 0x029d }
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            r0 = r24
            int r0 = r0.u     // Catch:{ all -> 0x029d }
            r9 = r0
            r0 = r24
            int r0 = r0.v     // Catch:{ all -> 0x029d }
            r10 = r0
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterColorUV(r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x0360:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterOldPhoto(r5, r6, r7, r8)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x036d:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterOil(r5, r6, r7, r8)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x037a:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterBW(r5, r6, r7, r8)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x0387:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            r9 = -65536(0xffffffffffff0000, float:NaN)
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterRGB(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x0396:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            r9 = -16711936(0xffffffffff00ff00, float:-1.7146522E38)
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterRGB(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x03a6:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            r9 = -16776961(0xffffffffff0000ff, float:-1.7014636E38)
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterRGB(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x03b6:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            r9 = 1
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterPencil(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x03c4:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            r9 = 0
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterPencil(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x03d2:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterThermal(r5, r6, r7, r8)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x03df:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterXray(r5, r6, r7, r8)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x03ec:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterAscii(r5, r6, r7, r8)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x03f9:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterComic(r5, r6, r7, r8)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x0406:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterEmboss(r5, r6, r7, r8)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x0413:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterLomo(r5, r6, r7, r8)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x0420:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            byte[] r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.previewYUVImage     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.CameraActivity r9 = com.androidillusion.cameraillusion.CameraActivity.instance     // Catch:{ all -> 0x029d }
            float r9 = r9.brightnessValue     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.CameraActivity r10 = com.androidillusion.cameraillusion.CameraActivity.instance     // Catch:{ all -> 0x029d }
            float r10 = r10.contrastValue     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.CameraActivity r11 = com.androidillusion.cameraillusion.CameraActivity.instance     // Catch:{ all -> 0x029d }
            float r11 = r11.saturationValue     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKFilterCustomized(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ all -> 0x029d }
            goto L_0x0058
        L_0x0439:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.CameraActivity.orientation     // Catch:{ all -> 0x029d }
            r9 = 1
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.effectMagic(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x029d }
            goto L_0x005b
        L_0x0447:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.CameraActivity.orientation     // Catch:{ all -> 0x029d }
            r9 = 2
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.effectMagic(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x029d }
            goto L_0x005b
        L_0x0455:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.CameraActivity.orientation     // Catch:{ all -> 0x029d }
            r9 = 3
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.effectMagic(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x029d }
            goto L_0x005b
        L_0x0463:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.CameraActivity.orientation     // Catch:{ all -> 0x029d }
            r9 = 4
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.effectMagic(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x029d }
            goto L_0x005b
        L_0x0471:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.CameraActivity.orientation     // Catch:{ all -> 0x029d }
            r9 = 1
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKEffectMirror(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x029d }
            goto L_0x005b
        L_0x047f:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            int r8 = com.androidillusion.cameraillusion.CameraActivity.orientation     // Catch:{ all -> 0x029d }
            r9 = 2
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKEffectMirror(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x029d }
            goto L_0x005b
        L_0x048d:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.NDKEffectPixelization(r5, r6, r7)     // Catch:{ all -> 0x029d }
            goto L_0x005b
        L_0x0498:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.effectBubble(r5, r6, r7)     // Catch:{ all -> 0x029d }
            goto L_0x005b
        L_0x04a3:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.effectMosaic(r5, r6, r7)     // Catch:{ all -> 0x029d }
            goto L_0x005b
        L_0x04ae:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.effectTunnel(r5, r6, r7)     // Catch:{ all -> 0x029d }
            goto L_0x005b
        L_0x04b9:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.effectPinch(r5, r6, r7)     // Catch:{ all -> 0x029d }
            goto L_0x005b
        L_0x04c4:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.effectTwirl(r5, r6, r7)     // Catch:{ all -> 0x029d }
            goto L_0x005b
        L_0x04cf:
            int[] r5 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.rgbImage     // Catch:{ all -> 0x029d }
            int r6 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMaxSize     // Catch:{ all -> 0x029d }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.cameraMinSize     // Catch:{ all -> 0x029d }
            com.androidillusion.cameraillusion.algorithm.ImageProccesing.effectGlass(r5, r6, r7)     // Catch:{ all -> 0x029d }
            goto L_0x005b
        L_0x04da:
            com.androidillusion.cameraillusion.camera.CustomCameraView r5 = com.androidillusion.cameraillusion.camera.CustomCameraView.instance     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.canvasBitmap     // Catch:{ Exception -> 0x02e0 }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.screenMaxSize     // Catch:{ Exception -> 0x02e0 }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.screenMinSize     // Catch:{ Exception -> 0x02e0 }
            r9 = 0
            android.graphics.Bitmap r6 = android.graphics.Bitmap.createScaledBitmap(r6, r7, r8, r9)     // Catch:{ Exception -> 0x02e0 }
            r5.mBitmap = r6     // Catch:{ Exception -> 0x02e0 }
            goto L_0x02d9
        L_0x04eb:
            com.androidillusion.cameraillusion.camera.CustomCameraView r5 = com.androidillusion.cameraillusion.camera.CustomCameraView.instance     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.canvasBitmap     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r6 = android.graphics.Bitmap.createBitmap(r6)     // Catch:{ Exception -> 0x02e0 }
            r5.mBitmap = r6     // Catch:{ Exception -> 0x02e0 }
            goto L_0x02d9
        L_0x04f7:
            r5 = move-exception
            r0 = r24
            com.androidillusion.cameraillusion.camera.CameraSurfaceView r0 = r0.mSurface     // Catch:{ Exception -> 0x02e0 }
            r6 = r0
            if (r6 == 0) goto L_0x0520
            boolean r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.endThread     // Catch:{ Exception -> 0x02e0 }
            if (r6 != 0) goto L_0x0520
            boolean r6 = com.androidillusion.cameraillusion.config.Settings.STRETCH_PICTURE     // Catch:{ Exception -> 0x02e0 }
            if (r6 == 0) goto L_0x0531
            int r6 = com.androidillusion.cameraillusion.CameraActivity.orientation     // Catch:{ Exception -> 0x02e0 }
            r7 = 1
            if (r6 != r7) goto L_0x0521
            com.androidillusion.cameraillusion.camera.CustomCameraView r6 = com.androidillusion.cameraillusion.camera.CustomCameraView.instance     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r7 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.canvasBitmap     // Catch:{ Exception -> 0x02e0 }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.screenMinSize     // Catch:{ Exception -> 0x02e0 }
            int r9 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.screenMaxSize     // Catch:{ Exception -> 0x02e0 }
            r10 = 0
            android.graphics.Bitmap r7 = android.graphics.Bitmap.createScaledBitmap(r7, r8, r9, r10)     // Catch:{ Exception -> 0x02e0 }
            r6.mBitmap = r7     // Catch:{ Exception -> 0x02e0 }
        L_0x051b:
            com.androidillusion.cameraillusion.camera.CustomCameraView r6 = com.androidillusion.cameraillusion.camera.CustomCameraView.instance     // Catch:{ Exception -> 0x02e0 }
            r6.postInvalidate()     // Catch:{ Exception -> 0x02e0 }
        L_0x0520:
            throw r5     // Catch:{ Exception -> 0x02e0 }
        L_0x0521:
            com.androidillusion.cameraillusion.camera.CustomCameraView r6 = com.androidillusion.cameraillusion.camera.CustomCameraView.instance     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r7 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.canvasBitmap     // Catch:{ Exception -> 0x02e0 }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.screenMaxSize     // Catch:{ Exception -> 0x02e0 }
            int r9 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.screenMinSize     // Catch:{ Exception -> 0x02e0 }
            r10 = 0
            android.graphics.Bitmap r7 = android.graphics.Bitmap.createScaledBitmap(r7, r8, r9, r10)     // Catch:{ Exception -> 0x02e0 }
            r6.mBitmap = r7     // Catch:{ Exception -> 0x02e0 }
            goto L_0x051b
        L_0x0531:
            com.androidillusion.cameraillusion.camera.CustomCameraView r6 = com.androidillusion.cameraillusion.camera.CustomCameraView.instance     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r7 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.canvasBitmap     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r7 = android.graphics.Bitmap.createBitmap(r7)     // Catch:{ Exception -> 0x02e0 }
            r6.mBitmap = r7     // Catch:{ Exception -> 0x02e0 }
            goto L_0x051b
        L_0x053c:
            com.androidillusion.cameraillusion.camera.CustomCameraView r5 = com.androidillusion.cameraillusion.camera.CustomCameraView.instance     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.canvasBitmap     // Catch:{ Exception -> 0x02e0 }
            int r7 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.screenMaxSize     // Catch:{ Exception -> 0x02e0 }
            int r8 = com.androidillusion.cameraillusion.camera.CameraSurfaceView.screenMinSize     // Catch:{ Exception -> 0x02e0 }
            r9 = 0
            android.graphics.Bitmap r6 = android.graphics.Bitmap.createScaledBitmap(r6, r7, r8, r9)     // Catch:{ Exception -> 0x02e0 }
            r5.mBitmap = r6     // Catch:{ Exception -> 0x02e0 }
            goto L_0x0186
        L_0x054d:
            com.androidillusion.cameraillusion.camera.CustomCameraView r5 = com.androidillusion.cameraillusion.camera.CustomCameraView.instance     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r6 = com.androidillusion.cameraillusion.camera.CameraRefreshThread.canvasBitmap     // Catch:{ Exception -> 0x02e0 }
            android.graphics.Bitmap r6 = android.graphics.Bitmap.createBitmap(r6)     // Catch:{ Exception -> 0x02e0 }
            r5.mBitmap = r6     // Catch:{ Exception -> 0x02e0 }
            goto L_0x0186
        L_0x0559:
            java.lang.String r5 = "Info"
            java.lang.String r6 = "SD not found"
            android.util.Log.i(r5, r6)     // Catch:{ Exception -> 0x02e0 }
            com.androidillusion.cameraillusion.CameraActivity r5 = com.androidillusion.cameraillusion.CameraActivity.instance     // Catch:{ Exception -> 0x02e0 }
            r6 = 2131034183(0x7f050047, float:1.7678876E38)
            java.lang.String r5 = r5.getString(r6)     // Catch:{ Exception -> 0x02e0 }
            r0 = r24
            r1 = r5
            r0.showToast(r1)     // Catch:{ Exception -> 0x02e0 }
            goto L_0x0273
        L_0x0571:
            java.lang.String r5 = "Info"
            java.lang.String r6 = "end thread"
            android.util.Log.i(r5, r6)     // Catch:{ Exception -> 0x02e0 }
            goto L_0x028f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.androidillusion.cameraillusion.camera.CameraRefreshThread.run():void");
    }

    public void showToast(String text) {
        if (CameraActivity.instance != null) {
            CameraActivity.instance.showToast(text);
        }
    }
}
