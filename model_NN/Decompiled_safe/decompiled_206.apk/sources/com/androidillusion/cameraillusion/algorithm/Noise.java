package com.androidillusion.cameraillusion.algorithm;

import java.lang.reflect.Array;
import java.util.Random;

public class Noise {
    private static final int B = 256;
    private static final int BM = 255;
    private static final int N = 4096;
    static float[] g1 = new float[514];
    static float[][] g2 = ((float[][]) Array.newInstance(Float.TYPE, 514, 2));
    static float[][] g3 = ((float[][]) Array.newInstance(Float.TYPE, 514, 3));
    static int[] p = new int[514];
    private static Random randomGenerator = new Random();
    static boolean start = true;

    public float evaluate(float x) {
        return noise1(x);
    }

    public float evaluate(float x, float y) {
        return noise2(x, y);
    }

    public float evaluate(float x, float y, float z) {
        return noise3(x, y, z);
    }

    public static float turbulence2(float x, float y, float octaves) {
        float t = 0.0f;
        for (float f = 1.0f; f <= octaves; f *= 2.0f) {
            t += Math.abs(noise2(f * x, f * y)) / f;
        }
        return t;
    }

    public static float turbulence3(float x, float y, float z, float octaves) {
        float t = 0.0f;
        for (float f = 1.0f; f <= octaves; f *= 2.0f) {
            t += Math.abs(noise3(f * x, f * y, f * z)) / f;
        }
        return t;
    }

    private static float sCurve(float t) {
        return t * t * (3.0f - (2.0f * t));
    }

    public static float noise1(float x) {
        if (start) {
            start = false;
            init();
        }
        float t = x + 4096.0f;
        int bx0 = ((int) t) & BM;
        float rx0 = t - ((float) ((int) t));
        return 2.3f * lerp(sCurve(rx0), rx0 * g1[p[bx0]], (rx0 - 1.0f) * g1[p[(bx0 + 1) & BM]]);
    }

    /* JADX INFO: Multiple debug info for r10v2 int: [D('x' float), D('bx0' int)] */
    /* JADX INFO: Multiple debug info for r11v2 int: [D('by0' int), D('y' float)] */
    /* JADX INFO: Multiple debug info for r2v5 int: [D('i' int), D('t' float)] */
    /* JADX INFO: Multiple debug info for r1v6 int: [D('by1' int), D('b11' int)] */
    /* JADX INFO: Multiple debug info for r3v3 float: [D('j' int), D('sy' float)] */
    /* JADX INFO: Multiple debug info for r10v6 float[]: [D('b00' int), D('q' float[])] */
    /* JADX INFO: Multiple debug info for r10v14 float: [D('v' float), D('a' float)] */
    /* JADX INFO: Multiple debug info for r11v6 float[]: [D('b01' int), D('q' float[])] */
    /* JADX INFO: Multiple debug info for r11v14 float: [D('v' float), D('b' float)] */
    public static float noise2(float x, float y) {
        if (start) {
            start = false;
            init();
        }
        float t = x + 4096.0f;
        int bx0 = ((int) t) & BM;
        int bx1 = (bx0 + 1) & BM;
        float rx0 = t - ((float) ((int) t));
        float rx1 = rx0 - 1.0f;
        float t2 = y + 4096.0f;
        int by0 = ((int) t2) & BM;
        int by1 = (by0 + 1) & BM;
        float ry0 = t2 - ((float) ((int) t2));
        float ry1 = ry0 - 1.0f;
        int i = p[bx0];
        int j = p[bx1];
        int b00 = p[i + by0];
        int b10 = p[by0 + j];
        int b01 = p[i + by1];
        int b11 = p[by1 + j];
        float sx = sCurve(rx0);
        float sy = sCurve(ry0);
        float[] q = g2[b00];
        float u = (q[0] * rx0) + (q[1] * ry0);
        float[] q2 = g2[b10];
        float a = lerp(sx, u, (q2[1] * ry0) + (q2[0] * rx1));
        float[] q3 = g2[b01];
        float u2 = (q3[0] * rx0) + (q3[1] * ry1);
        float[] q4 = g2[b11];
        return lerp(sy, a, lerp(sx, u2, (q4[1] * ry1) + (q4[0] * rx1))) * 1.5f;
    }

    /* JADX INFO: Multiple debug info for r19v2 int: [D('x' float), D('bx0' int)] */
    /* JADX INFO: Multiple debug info for r20v2 int: [D('by0' int), D('y' float)] */
    /* JADX INFO: Multiple debug info for r21v1 float: [D('z' float), D('t' float)] */
    /* JADX INFO: Multiple debug info for r19v3 int: [D('i' int), D('bx0' int)] */
    /* JADX INFO: Multiple debug info for r5v4 int: [D('by1' int), D('b11' int)] */
    /* JADX INFO: Multiple debug info for r8v1 float: [D('j' int), D('sy' float)] */
    /* JADX INFO: Multiple debug info for r19v15 float: [D('v' float), D('a' float)] */
    /* JADX INFO: Multiple debug info for r20v21 float: [D('v' float), D('b' float)] */
    /* JADX INFO: Multiple debug info for r19v25 float: [D('v' float), D('a' float)] */
    /* JADX INFO: Multiple debug info for r20v37 float: [D('v' float), D('b' float)] */
    /* JADX INFO: Multiple debug info for r19v26 float: [D('d' float), D('a' float)] */
    public static float noise3(float x, float y, float z) {
        if (start) {
            start = false;
            init();
        }
        float t = x + 4096.0f;
        int bx0 = ((int) t) & BM;
        int bx1 = (bx0 + 1) & BM;
        float rx0 = t - ((float) ((int) t));
        float rx1 = rx0 - 1.0f;
        float t2 = 4096.0f + y;
        int by0 = ((int) t2) & BM;
        int by1 = (by0 + 1) & BM;
        float ry0 = t2 - ((float) ((int) t2));
        float ry1 = ry0 - 1.0f;
        float t3 = z + 4096.0f;
        int bz0 = ((int) t3) & BM;
        int bz1 = (bz0 + 1) & BM;
        float rz0 = t3 - ((float) ((int) t3));
        float rz1 = rz0 - 1.0f;
        int bx02 = p[bx0];
        int j = p[bx1];
        int b00 = p[bx02 + by0];
        int b10 = p[by0 + j];
        int b01 = p[bx02 + by1];
        int b11 = p[j + by1];
        float t4 = sCurve(rx0);
        float sy = sCurve(ry0);
        float sz = sCurve(rz0);
        float[] q = g3[b00 + bz0];
        float u = (q[0] * rx0) + (q[1] * ry0) + (q[2] * rz0);
        float[] q2 = g3[b10 + bz0];
        float a = lerp(t4, u, (q2[2] * rz0) + (q2[0] * rx1) + (q2[1] * ry0));
        float[] q3 = g3[b01 + bz0];
        float u2 = (q3[0] * rx0) + (q3[1] * ry1) + (q3[2] * rz0);
        float[] q4 = g3[bz0 + b11];
        float c = lerp(sy, a, lerp(t4, u2, (q4[2] * rz0) + (q4[0] * rx1) + (q4[1] * ry1)));
        float[] q5 = g3[b00 + bz1];
        float u3 = (q5[0] * rx0) + (q5[1] * ry0) + (q5[2] * rz1);
        float[] q6 = g3[b10 + bz1];
        float a2 = lerp(t4, u3, (q6[2] * rz1) + (q6[0] * rx1) + (q6[1] * ry0));
        float[] q7 = g3[b01 + bz1];
        float u4 = (q7[0] * rx0) + (q7[1] * ry1) + (q7[2] * rz1);
        float[] q8 = g3[b11 + bz1];
        return lerp(sz, c, lerp(sy, a2, lerp(t4, u4, (q8[2] * rz1) + (q8[0] * rx1) + (q8[1] * ry1)))) * 1.5f;
    }

    public static float lerp(float t, float a, float b) {
        return ((b - a) * t) + a;
    }

    private static void normalize2(float[] v) {
        float s = (float) Math.sqrt((double) ((v[0] * v[0]) + (v[1] * v[1])));
        v[0] = v[0] / s;
        v[1] = v[1] / s;
    }

    static void normalize3(float[] v) {
        float s = (float) Math.sqrt((double) ((v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2])));
        v[0] = v[0] / s;
        v[1] = v[1] / s;
        v[2] = v[2] / s;
    }

    private static int random() {
        return randomGenerator.nextInt() & Integer.MAX_VALUE;
    }

    private static void init() {
        for (int i = 0; i < B; i++) {
            p[i] = i;
            g1[i] = ((float) ((random() % 512) - B)) / 256.0f;
            for (int j = 0; j < 2; j++) {
                g2[i][j] = ((float) ((random() % 512) - B)) / 256.0f;
            }
            normalize2(g2[i]);
            for (int j2 = 0; j2 < 3; j2++) {
                g3[i][j2] = ((float) ((random() % 512) - B)) / 256.0f;
            }
            normalize3(g3[i]);
        }
        for (int i2 = BM; i2 >= 0; i2--) {
            int k = p[i2];
            int[] iArr = p;
            int[] iArr2 = p;
            int j3 = random() % B;
            iArr[i2] = iArr2[j3];
            p[j3] = k;
        }
        for (int i3 = 0; i3 < 258; i3++) {
            p[i3 + B] = p[i3];
            g1[i3 + B] = g1[i3];
            for (int j4 = 0; j4 < 2; j4++) {
                g2[i3 + B][j4] = g2[i3][j4];
            }
            for (int j5 = 0; j5 < 3; j5++) {
                g3[i3 + B][j5] = g3[i3][j5];
            }
        }
    }
}
