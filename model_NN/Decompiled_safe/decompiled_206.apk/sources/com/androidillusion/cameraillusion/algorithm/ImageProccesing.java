package com.androidillusion.cameraillusion.algorithm;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.Log;
import com.androidillusion.cameraillusion.CameraActivity;
import com.androidillusion.cameraillusion.camera.CameraSurfaceView;
import java.lang.reflect.Array;

public class ImageProccesing {
    private static final char BLACK = '@';
    private static final char CHARCOAL = '#';
    private static final char DARKGRAY = '8';
    private static final char GRAY = ':';
    private static final char LIGHTGRAY = '.';
    public static final int MAGIC_FAT = 2;
    public static final int MAGIC_SHORT = 4;
    public static final int MAGIC_TALL = 3;
    public static final int MAGIC_THIN = 1;
    private static final char MEDIUM = 'o';
    private static final char MEDIUMGRAY = '&';
    public static final int MIRROR_HORIZONTAL = 1;
    public static final int MIRROR_VERTICAL = 2;
    public static final int ORIENTATION_LANDSCAPE = 2;
    public static final int ORIENTATION_PORTRAIT = 1;
    private static final char SLATEGRAY = '*';
    private static final char WHITE = ' ';
    public static char[] asciiValue;
    public static int[] custom;
    public static Typeface mFace;
    public static int[] temp;
    public static float xScale = 4.0f;
    public static float yScale = 4.0f;

    public static native void NDKEffectCustom(int[] iArr, int[] iArr2, int[] iArr3, int i, int i2);

    public static native void NDKEffectFirstBubble(int[] iArr, int i, int i2);

    public static native void NDKEffectFirstMosaic(int[] iArr, int i, int i2);

    public static native void NDKEffectFirstPinch(int[] iArr, int i, int i2);

    public static native void NDKEffectFirstTunnel(int[] iArr, int i, int i2);

    public static native void NDKEffectFirstTwirl(int[] iArr, int i, int i2);

    public static native void NDKEffectMagic(int[] iArr, int[] iArr2, int i, int i2, int i3, int i4);

    public static native void NDKEffectMirror(int[] iArr, int i, int i2, int i3, int i4);

    public static native void NDKEffectPixelization(int[] iArr, int i, int i2);

    public static native void NDKFilterAscii(int[] iArr, byte[] bArr, int i, int i2);

    public static native void NDKFilterBW(int[] iArr, byte[] bArr, int i, int i2);

    public static native void NDKFilterColorUV(int[] iArr, byte[] bArr, int i, int i2, int i3, int i4);

    public static native void NDKFilterComic(int[] iArr, byte[] bArr, int i, int i2);

    public static native void NDKFilterCustomized(int[] iArr, byte[] bArr, int i, int i2, float f, float f2, float f3);

    public static native void NDKFilterEmboss(int[] iArr, byte[] bArr, int i, int i2);

    public static native void NDKFilterLomo(int[] iArr, byte[] bArr, int i, int i2);

    public static native void NDKFilterMono(int[] iArr, byte[] bArr, int i, int i2);

    public static native void NDKFilterNegative(int[] iArr, byte[] bArr, int i, int i2);

    public static native void NDKFilterNone(int[] iArr, byte[] bArr, int i, int i2);

    public static native void NDKFilterOil(int[] iArr, byte[] bArr, int i, int i2);

    public static native void NDKFilterOldPhoto(int[] iArr, byte[] bArr, int i, int i2);

    public static native void NDKFilterPencil(int[] iArr, byte[] bArr, int i, int i2, boolean z);

    public static native void NDKFilterRGB(int[] iArr, byte[] bArr, int i, int i2, int i3);

    public static native void NDKFilterThermal(int[] iArr, byte[] bArr, int i, int i2);

    public static native void NDKFilterXray(int[] iArr, byte[] bArr, int i, int i2);

    static {
        System.loadLibrary("camera-illusion");
    }

    /* JADX INFO: Multiple debug info for r2v9 int: [D('y1192' int), D('b' int)] */
    /* JADX INFO: Multiple debug info for r2v15 int: [D('b' int), D('i' int)] */
    /* JADX INFO: Multiple debug info for r2v18 int: [D('u' int), D('uvp' int)] */
    public static void decode(int[] rgb, byte[] yuv420sp, int width, int height) {
        int y;
        int v;
        int u;
        int uvp;
        int frameSize = width * height;
        int u2 = 0;
        int yp = 0;
        while (true) {
            int j = u2;
            if (j < height) {
                int v2 = 0;
                int yp2 = yp;
                int yp3 = frameSize + ((j >> 1) * width);
                int i = 0;
                int u3 = 0;
                while (i < width) {
                    int y2 = (yuv420sp[yp2] & 255) - 16;
                    if (y2 < 0) {
                        y = 0;
                    } else {
                        y = y2;
                    }
                    if ((i & 1) == 0) {
                        int uvp2 = yp3 + 1;
                        v = (yuv420sp[yp3] & 255) - 128;
                        u = (yuv420sp[uvp2] & 255) - 128;
                        uvp = uvp2 + 1;
                    } else {
                        v = v2;
                        u = u3;
                        uvp = yp3;
                    }
                    int y1192 = y * 1192;
                    int r = y1192 + (v * 1634);
                    int g = (y1192 - (v * 833)) - (u * 400);
                    int b = y1192 + (u * 2066);
                    if (r < 0) {
                        r = 0;
                    } else if (r > 262143) {
                        r = 262143;
                    }
                    if (g < 0) {
                        g = 0;
                    } else if (g > 262143) {
                        g = 262143;
                    }
                    if (b < 0) {
                        b = 0;
                    } else if (b > 262143) {
                        b = 262143;
                    }
                    rgb[yp2] = ((b >> 10) & 255) | ((g >> 2) & 65280) | ((r << 6) & 16711680) | -16777216;
                    i++;
                    v2 = v;
                    yp2++;
                    yp3 = uvp;
                    u3 = u;
                }
                u2 = j + 1;
                yp = yp2;
            } else {
                return;
            }
        }
    }

    static int getPixel(int x, int y, int width) {
        return (y * width) + x;
    }

    /* JADX INFO: Multiple debug info for r12v1 char[][]: [D('cameraMinSize' int), D('cadena' char[][])] */
    /* JADX INFO: Multiple debug info for r4v3 android.graphics.Paint: [D('j' int), D('paint' android.graphics.Paint)] */
    /* JADX INFO: Multiple debug info for r1v14 int: [D('j' int), D('i' int)] */
    public static char[][] filterAsciiArt(int[] rgbImage, int cameraMaxSize, int cameraMinSize) {
        int value;
        int i = 5 >> 1;
        int A2 = 8 >> 1;
        int xDesp = cameraMaxSize / 5;
        int yDesp = cameraMinSize / 8;
        char[][] cadena = new char[yDesp][];
        for (int i2 = 0; i2 < cadena.length; i2++) {
            cadena[i2] = new char[xDesp];
        }
        if (asciiValue == null) {
            asciiValue = new char[256];
            for (int i3 = 0; i3 < 256; i3++) {
                if (1 != 0) {
                    value = 255 - i3;
                } else {
                    value = i3;
                }
                if (value >= 230) {
                    asciiValue[i3] = WHITE;
                } else if (value >= 200) {
                    asciiValue[i3] = LIGHTGRAY;
                } else if (value >= 180) {
                    asciiValue[i3] = SLATEGRAY;
                } else if (value >= 160) {
                    asciiValue[i3] = GRAY;
                } else if (value >= 130) {
                    asciiValue[i3] = MEDIUM;
                } else if (value >= 100) {
                    asciiValue[i3] = MEDIUMGRAY;
                } else if (value >= 70) {
                    asciiValue[i3] = DARKGRAY;
                } else if (value >= 50) {
                    asciiValue[i3] = CHARCOAL;
                } else {
                    asciiValue[i3] = BLACK;
                }
            }
        }
        for (int j = 0; j < yDesp; j++) {
            for (int i4 = 0; i4 < xDesp; i4++) {
                cadena[j][i4] = asciiValue[rgbImage[get((i4 * 5) + 2, (j * 8) + 4, cameraMaxSize)]];
            }
        }
        if (mFace == null) {
            mFace = Typeface.createFromAsset(CameraActivity.instance.cameraView.getContext().getAssets(), "fonts/font.ttf");
        }
        Paint paint = new Paint();
        paint.setTypeface(mFace);
        if (1 != 0) {
            paint.setColor(-1);
        } else {
            paint.setColor(-16777216);
        }
        paint.setTextSize((float) 8);
        paint.setTextScaleX(1.0f);
        paint.setTextAlign(Paint.Align.LEFT);
        Bitmap imageBitmap = Bitmap.createBitmap(CameraSurfaceView.cameraMaxSize, CameraSurfaceView.cameraMinSize, Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas();
        canvas.setBitmap(imageBitmap);
        for (int i5 = 0; i5 < yDesp; i5++) {
            canvas.drawText(new String(cadena[i5]), 0.0f, (float) ((i5 * 8) + 8), paint);
        }
        imageBitmap.getPixels(rgbImage, 0, imageBitmap.getWidth(), 0, 0, imageBitmap.getWidth(), imageBitmap.getHeight());
        return cadena;
    }

    public static void effectBubble(int[] image, int width, int height) {
        if (custom == null) {
            custom = new int[(width * height)];
            NDKEffectFirstBubble(custom, width, height);
        }
        if (temp == null) {
            temp = new int[(width * height)];
        }
        System.arraycopy(image, 0, temp, 0, image.length);
        NDKEffectCustom(image, custom, temp, width, height);
    }

    public static void effectTwirl(int[] image, int width, int height) {
        if (custom == null) {
            custom = new int[(width * height)];
            NDKEffectFirstTwirl(custom, width, height);
        }
        if (temp == null) {
            temp = new int[(width * height)];
        }
        System.arraycopy(image, 0, temp, 0, image.length);
        NDKEffectCustom(image, custom, temp, width, height);
    }

    public static void effectMosaic(int[] image, int width, int height) {
        if (custom == null) {
            custom = new int[(width * height)];
            NDKEffectFirstMosaic(custom, width, height);
        }
        if (temp == null) {
            temp = new int[(width * height)];
        }
        System.arraycopy(image, 0, temp, 0, image.length);
        NDKEffectCustom(image, custom, temp, width, height);
    }

    public static void effectTunnel(int[] image, int width, int height) {
        if (custom == null) {
            custom = new int[(width * height)];
            NDKEffectFirstTunnel(custom, width, height);
        }
        if (temp == null) {
            temp = new int[(width * height)];
        }
        System.arraycopy(image, 0, temp, 0, image.length);
        NDKEffectCustom(image, custom, temp, width, height);
    }

    public static int displacementMap(int x, int y) {
        return clamp((int) (127.0f * (1.0f + Noise.noise2(((float) x) / xScale, ((float) y) / xScale))));
    }

    public static int clamp(int c) {
        if (c < 0) {
            return 0;
        }
        if (c <= 255) {
            return c;
        }
        return 255;
    }

    public static void effectGlass(int[] image, int width, int height) {
        long currentTimeMillis = System.currentTimeMillis();
        if (custom == null) {
            custom = new int[(width * height)];
            effectFirstGlass(custom, width, height);
            System.currentTimeMillis();
        }
        if (temp == null) {
            temp = new int[(width * height)];
        }
        System.arraycopy(image, 0, temp, 0, image.length);
        NDKEffectCustom(image, custom, temp, width, height);
    }

    /* JADX INFO: Multiple debug info for r4v3 int: [D('j' int), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v4 int: [D('x2' int), D('value' int)] */
    public static void effectFirstGlass(int[] bubble, int width, int height) {
        int displacement;
        int[] sinTable = new int[256];
        int[] cosTable = new int[256];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= 256) {
                break;
            }
            float angle = (6.2831855f * ((float) i2)) / 256.0f;
            sinTable[i2] = (int) (((double) (-yScale)) * Math.sin((double) angle));
            cosTable[i2] = (int) (((double) yScale) * Math.cos((double) angle));
            i = i2 + 1;
        }
        int widthTile = width / 4;
        int heightTile = height / 4;
        int[][] disp = (int[][]) Array.newInstance(Integer.TYPE, widthTile, heightTile);
        for (int j = 0; j < height; j++) {
            for (int i3 = 0; i3 < width; i3++) {
                if (j >= heightTile || i3 >= widthTile) {
                    displacement = disp[i3 % widthTile][j % heightTile];
                } else {
                    int[] iArr = disp[i3];
                    displacement = displacementMap(i3, j);
                    iArr[j] = displacement;
                }
                int x2 = sinTable[displacement] + i3;
                int y2 = cosTable[displacement] + j;
                if (x2 < 0 || x2 >= width || y2 < 0 || y2 >= height) {
                    bubble[get(i3, j, width)] = 0;
                } else {
                    bubble[get(i3, j, width)] = get(x2, y2, width) - ((j * width) + i3);
                }
            }
        }
    }

    /* JADX INFO: Multiple debug info for r0v12 int: [D('i' int), D('value' int)] */
    /* JADX INFO: Multiple debug info for r0v18 int: [D('y2' int), D('value' int)] */
    public static void effectFirstGlass2(int[] bubble, int width, int height) {
        int i = width * height;
        float[] sinTable = new float[256];
        float[] cosTable = new float[256];
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= 256) {
                break;
            }
            float angle = ((6.2831855f * ((float) i3)) / 256.0f) * 1.0f;
            sinTable[i3] = (float) (((double) (-yScale)) * Math.sin((double) angle));
            cosTable[i3] = (float) (((double) yScale) * Math.cos((double) angle));
            i2 = i3 + 1;
        }
        int length = bubble.length;
        for (int j = 0; j < height; j++) {
            int i4 = 0;
            while (i4 < width) {
                try {
                    int displacement = displacementMap(i4, j);
                    int x2 = ((int) sinTable[displacement]) + i4;
                    int y2 = ((int) cosTable[displacement]) + j;
                    if (x2 < 0 || x2 >= width || y2 < 0 || y2 >= height) {
                        bubble[get(i4, j, width)] = 0;
                        i4++;
                    } else {
                        bubble[get(i4, j, width)] = get(x2, y2, width) - ((j * width) + i4);
                        i4++;
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    /* JADX INFO: Multiple debug info for r6v3 double: [D('dy' double), D('y2' int)] */
    /* JADX INFO: Multiple debug info for r4v35 double: [D('c' double), D('radiusc' double)] */
    public static void effectFirstKal(int[] bubble, int width, int height) {
        double radiusc;
        int x2;
        float icentreX = (float) (width / 2);
        float icentreY = (float) (height / 2);
        int j = 0;
        while (true) {
            int j2 = j;
            if (j2 < height) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= width) {
                        break;
                    }
                    double dx = (double) (((float) i2) - icentreX);
                    double dy = (double) (((float) j2) - icentreY);
                    double r = Math.sqrt((dx * dx) + (dy * dy));
                    double theta = (double) triangle((float) ((((Math.atan2(dy, dx) - ((double) 0.0f)) - ((double) 0.0f)) / 3.141592653589793d) * ((double) 3) * 0.5d));
                    if (0.0f != 0.0f) {
                        double radiusc2 = ((double) 0.0f) / Math.cos(theta);
                        radiusc = radiusc2 * ((double) triangle((float) (r / radiusc2)));
                    } else {
                        radiusc = r;
                    }
                    double theta2 = theta + ((double) 0.0f);
                    int x22 = (int) (((double) icentreX) + (Math.cos(theta2) * radiusc));
                    int y2 = (int) ((radiusc * Math.sin(theta2)) + ((double) icentreY));
                    if (x22 >= width) {
                        x2 = ((width * 2) - x22) - 1;
                    } else {
                        x2 = x22;
                    }
                    if (y2 >= height) {
                        y2 = ((height * 2) - y2) - 1;
                    }
                    try {
                        bubble[(j2 * width) + i2] = ((y2 * width) + x2) - ((j2 * width) + i2);
                    } catch (Exception e) {
                    }
                    i = i2 + 1;
                }
                j = j2 + 1;
            } else {
                return;
            }
        }
    }

    public static float mod(float a, float b) {
        float a2 = a - (((float) ((int) (a / b))) * b);
        if (a2 < 0.0f) {
            return a2 + b;
        }
        return a2;
    }

    public static float triangle(float x) {
        float r = mod(x, 1.0f);
        return 2.0f * (((double) r) < 0.5d ? r : 1.0f - r);
    }

    public static void effectKal(int[] image, int width, int height) {
        long currentTimeMillis = System.currentTimeMillis();
        if (custom == null) {
            custom = new int[(width * height)];
            effectFirstKal(custom, width, height);
        }
        if (temp == null) {
            temp = new int[(width * height)];
        }
        System.arraycopy(image, 0, temp, 0, image.length);
        NDKEffectCustom(image, custom, temp, width, height);
    }

    /* JADX INFO: Multiple debug info for r4v11 int: [D('i' int), D('value' int)] */
    /* JADX INFO: Multiple debug info for r4v17 int: [D('y2' int), D('value' int)] */
    public static void effectFirstSwim(int[] bubble, int width, int height) {
        int j = 0;
        while (true) {
            int j2 = j;
            if (j2 < height) {
                int i = 0;
                while (i < width) {
                    float nx = ((((float) i) * 1.0f) + (((float) j2) * 0.0f)) / 32.0f;
                    float ny = ((((float) i) * 0.0f) + (((float) j2) * 1.0f)) / (32.0f * 1.0f);
                    try {
                        int x2 = ((int) (Noise.noise3(0.5f + nx, ny, 0.0f) * 12.0f)) + i;
                        int y2 = ((int) (Noise.noise3(nx, ny + 0.5f, 0.0f) * 12.0f)) + j2;
                        if (x2 < 0 || x2 >= width || y2 < 0 || y2 >= height) {
                            bubble[get(i, j2, width)] = 0;
                            i++;
                        } else {
                            bubble[get(i, j2, width)] = get(x2, y2, width) - ((j2 * width) + i);
                            i++;
                        }
                    } catch (Exception e) {
                        Log.i("Info", "Excepcion");
                    }
                }
                j = j2 + 1;
            } else {
                return;
            }
        }
    }

    public static void effectSwim(int[] image, int width, int height) {
        long currentTimeMillis = System.currentTimeMillis();
        if (custom == null) {
            custom = new int[(width * height)];
            effectFirstSwim(custom, width, height);
        }
        long currentTimeMillis2 = System.currentTimeMillis();
        if (temp == null) {
            temp = new int[(width * height)];
        }
        System.arraycopy(image, 0, temp, 0, image.length);
        NDKEffectCustom(image, custom, temp, width, height);
        long currentTimeMillis3 = System.currentTimeMillis();
    }

    public static void effectPinch(int[] image, int width, int height) {
        long currentTimeMillis = System.currentTimeMillis();
        if (custom == null) {
            custom = new int[(width * height)];
            NDKEffectFirstPinch(custom, width, height);
            System.currentTimeMillis();
        }
        if (temp == null) {
            temp = new int[(width * height)];
        }
        System.arraycopy(image, 0, temp, 0, image.length);
        NDKEffectCustom(image, custom, temp, width, height);
    }

    public static int getColor(int r, int g, int b) {
        return -16777216 | ((r << 16) & 16711680) | ((g << 8) & 65280) | (b & 255);
    }

    public static int get(int x, int y, int cameraMaxSize) {
        return (y * cameraMaxSize) + x;
    }

    public static void effectMagic(int[] image, int width, int height, int orientation, int type) {
        if (temp == null) {
            temp = new int[(width * height)];
        }
        System.arraycopy(image, 0, temp, 0, image.length);
        NDKEffectMagic(image, temp, width, height, orientation, type);
    }
}
