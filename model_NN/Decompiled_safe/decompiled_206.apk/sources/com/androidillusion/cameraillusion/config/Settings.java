package com.androidillusion.cameraillusion.config;

public class Settings {
    public static final String APP = "com.androidillusion.cameraillusion";
    public static String APP_VERSION = "";
    public static boolean AUTOFUCUS_BEFORE_SHOT = false;
    public static boolean CAMERA_SOUND = false;
    public static final String CNAME_PHOTO_ILLUSION = "com.androidillusion.photoillusion.PhotoActivity";
    public static final String CNAME_PHOTO_ILLUSION_PRO = "com.androidillusion.photoillusionpro.PhotoActivity";
    public static boolean DEBUG_MODE = false;
    public static boolean HIDE_STATUS_BAR = false;
    public static final int IMAGE_QUALITY = 95;
    public static final String MARKET_CAMERA_ILLUSION_PRO = "http://market.android.com/search?q=pname:com.androidillusion.cameraillusionpro";
    public static final String MARKET_PHOTO_ILLUSION = "http://market.android.com/search?q=pname:com.androidillusion.photoillusion";
    public static String MASK_DIRECTORY = (String.valueOf(PHOTOS_DIRECTORY) + "masks/");
    public static String PHOTOS_DIRECTORY = "/sdcard/CameraIllusion/";
    public static final String PNAME_PHOTO_ILLUSION = "com.androidillusion.photoillusion";
    public static final String PNAME_PHOTO_ILLUSION_PRO = "com.androidillusion.photoillusionpro";
    public static boolean PRO_BUTTON_INVISIBLE = false;
    public static boolean PRO_VERSION = false;
    public static boolean REMEMBER_SELECTED_OPTIONS;
    public static boolean SAVE_HTML_ASCII_ART;
    public static boolean STRETCH_PICTURE = false;
    public static boolean TOUCH_SCREEN_AUTOFOCUS = true;
    public static boolean VOLUME_KEY_SHUTTER = true;
}
