package com.androidillusion.cameraillusion.config;

public class ChangeOrientationSave {
    public String cadena;
    public int selectedEffectID;
    public int selectedFilterID;
    public int selectedMaskID;
    public long time;

    public ChangeOrientationSave(long time2, int selectedFilterID2, int selectedEffectID2, int selectedMaskID2, String cadena2) {
        this.time = time2;
        this.selectedFilterID = selectedFilterID2;
        this.selectedEffectID = selectedEffectID2;
        this.selectedMaskID = selectedMaskID2;
        this.cadena = cadena2;
    }
}
