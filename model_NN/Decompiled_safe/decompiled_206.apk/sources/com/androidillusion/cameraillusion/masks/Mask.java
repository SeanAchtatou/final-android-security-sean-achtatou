package com.androidillusion.cameraillusion.masks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;
import com.androidillusion.cameraillusion.CameraActivity;
import com.androidillusion.cameraillusion.R;
import com.androidillusion.cameraillusion.camera.CameraRefreshThread;
import com.androidillusion.cameraillusion.camera.CameraSurfaceView;
import com.androidillusion.cameraillusion.config.Settings;
import com.androidillusion.cameraillusion.media.MediaManager;
import java.util.Vector;

public class Mask {
    public static boolean isCustomMask = false;
    private int imageID;
    private Bitmap originalBitmap;
    private int originalHeight;
    float[] originalMargin;
    float[] originalMask;
    private Matrix originalMatrix;
    private int originalWidth;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public Mask(int imageID2, float[] mask, boolean custom) {
        this.imageID = imageID2;
        this.originalMask = mask;
        String fileString = "";
        isCustomMask = false;
        if (!custom) {
            this.originalBitmap = BitmapFactory.decodeResource(CameraActivity.instance.getResources(), imageID2);
        } else {
            Vector<String> customMasks = MediaManager.getInstance().checkMaskFiles(Settings.MASK_DIRECTORY);
            for (int i = 0; i < customMasks.size(); i++) {
                if (customMasks.elementAt(i).hashCode() == imageID2) {
                    fileString = String.valueOf(Settings.MASK_DIRECTORY) + customMasks.elementAt(i) + ".png";
                    isCustomMask = true;
                }
            }
            try {
                this.originalBitmap = BitmapFactory.decodeFile(fileString);
            } catch (Exception e) {
                Log.i("Info", "Exception loading file " + fileString);
                CameraRefreshThread.selectedMask = null;
                return;
            }
        }
        this.originalWidth = this.originalBitmap.getWidth();
        this.originalHeight = this.originalBitmap.getHeight();
        this.originalMargin = new float[]{0.0f, 0.0f, (float) CameraSurfaceView.cameraMaxSize, 0.0f, (float) CameraSurfaceView.cameraMaxSize, (float) CameraSurfaceView.cameraMinSize, 0.0f, (float) CameraSurfaceView.cameraMinSize};
        if (!(this.originalMask[7] == 0.0f || this.originalHeight == CameraSurfaceView.cameraMaxSize || this.originalHeight == 800)) {
            for (int i2 = 0; i2 < this.originalMask.length; i2++) {
                this.originalMask[i2] = (this.originalMask[i2] * ((float) CameraSurfaceView.cameraMinSize)) / 320.0f;
            }
        }
        this.originalMatrix = new Matrix();
        if (CameraActivity.orientation == 2) {
            Matrix maskMatrix = new Matrix();
            maskMatrix.preRotate(-90.0f);
            maskMatrix.postTranslate(0.0f, (float) this.originalWidth);
            this.originalBitmap = Bitmap.createBitmap(this.originalBitmap, 0, 0, this.originalWidth, this.originalHeight, maskMatrix, true);
            this.originalMask = new float[]{this.originalMask[1], ((float) CameraSurfaceView.cameraMinSize) - this.originalMask[0], this.originalMask[3], ((float) CameraSurfaceView.cameraMinSize) - this.originalMask[2], this.originalMask[5], ((float) CameraSurfaceView.cameraMinSize) - this.originalMask[4], this.originalMask[7], ((float) CameraSurfaceView.cameraMinSize) - this.originalMask[6]};
        }
        this.originalMatrix.setPolyToPoly(this.originalMargin, 0, this.originalMask, 0, 4);
    }

    public Matrix getOriginalMatrix() {
        return this.originalMatrix;
    }

    public Bitmap getOriginalBitmap() {
        return this.originalBitmap;
    }

    public static void loadMaskBitmap() {
        int maskID = CameraActivity.instance.getSelectedMaskID();
        if (maskID != R.id.mask1) {
            CameraRefreshThread.selectedMask = null;
            System.gc();
        }
        switch (maskID) {
            case R.id.mask1:
                CameraRefreshThread.selectedMask = null;
                return;
            case R.id.mask2:
                CameraRefreshThread.selectedMask = new Mask(R.drawable.htc, new float[]{218.0f, 117.0f, 175.0f, 345.0f, 67.0f, 327.0f, 108.0f, 117.0f}, false);
                return;
            case R.id.mask3:
                CameraRefreshThread.selectedMask = new Mask(R.drawable.frame, new float[]{236.0f, 116.0f, 236.0f, 356.0f, 75.0f, 356.0f, 75.0f, 116.0f}, false);
                return;
            case R.id.mask4:
                CameraRefreshThread.selectedMask = new Mask(R.drawable.card, new float[]{330.0f, 62.0f, 330.0f, 429.0f, 91.0f, 429.0f, 91.0f, 62.0f}, false);
                return;
            case R.id.mask5:
                CameraRefreshThread.selectedMask = new Mask(R.drawable.polaroid, new float[]{222.0f, 41.0f, 296.0f, 351.0f, 88.0f, 400.0f, 15.0f, 91.0f}, false);
                return;
            case R.id.mask6:
                CameraRefreshThread.selectedMask = new Mask(R.drawable.blackboard, new float[]{211.0f, 98.0f, 281.0f, 318.0f, 141.0f, 383.0f, 58.0f, 136.0f}, false);
                return;
            case R.id.mask7:
                CameraRefreshThread.selectedMask = new Mask(R.drawable.bricks, new float[]{(float) CameraSurfaceView.cameraMinSize, 0.0f, (float) CameraSurfaceView.cameraMinSize, (float) CameraSurfaceView.cameraMaxSize, 0.0f, (float) CameraSurfaceView.cameraMaxSize, 0.0f, 0.0f}, false);
                return;
            case R.id.mask8:
                CameraRefreshThread.selectedMask = new Mask(R.drawable.tv, new float[]{262.0f, 41.0f, 262.0f, 431.0f, 32.0f, 431.0f, 22.0f, 41.0f}, false);
                return;
            case R.id.mask12:
                CameraRefreshThread.selectedMask = new Mask(R.drawable.poster, new float[]{227.0f, 158.0f, 227.0f, 356.0f, 94.0f, 356.0f, 94.0f, 158.0f}, false);
                return;
            case R.id.mask9:
                CameraRefreshThread.selectedMask = new Mask(R.drawable.nexus_one, new float[]{349.0f, 193.0f, 349.0f, 535.0f, 132.0f, 535.0f, 132.0f, 193.0f}, false);
                return;
            case R.id.mask11:
                CameraRefreshThread.selectedMask = new Mask(R.drawable.moto_droid, new float[]{363.0f, 191.0f, 363.0f, 616.0f, 109.0f, 616.0f, 109.0f, 191.0f}, false);
                return;
            case R.id.mask10:
                CameraRefreshThread.selectedMask = new Mask(R.drawable.tattoo, new float[]{142.0f, 78.0f, 161.0f, 205.0f, 75.0f, 215.0f, 56.0f, 79.0f}, false);
                return;
            default:
                try {
                    CameraRefreshThread.selectedMask = new Mask(maskID, new float[]{(float) CameraSurfaceView.cameraMinSize, 0.0f, (float) CameraSurfaceView.cameraMinSize, (float) CameraSurfaceView.cameraMaxSize, 0.0f, (float) CameraSurfaceView.cameraMaxSize, 0.0f, 0.0f}, true);
                    return;
                } catch (Exception e) {
                    return;
                }
        }
    }
}
