package com.androidillusion.cameraillusion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.androidillusion.cameraillusion.camera.CameraRefreshThread;
import com.androidillusion.cameraillusion.config.Settings;
import java.io.File;

public class ImageViewerActivity extends Activity {
    public ImageViewerActivity instance;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.instance = this;
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        Bundle extras = getIntent().getExtras();
        final Uri uri = extras != null ? extras.getParcelable("uri") : "nothing passed in";
        final String cadena = extras != null ? extras.getString("path") : "nothing passed in";
        Bitmap bitmap = BitmapFactory.decodeFile(String.valueOf(Settings.PHOTOS_DIRECTORY) + cadena);
        setContentView((int) R.layout.image);
        ImageView image = (ImageView) findViewById(R.id.imagefull);
        if (bitmap != null) {
            image.setImageBitmap(bitmap);
        }
        ((Button) findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImageViewerActivity.this.finish();
            }
        });
        ((TextView) findViewById(R.id.TextView01)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImageViewerActivity.this.finish();
            }
        });
        ((Button) findViewById(R.id.share)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImageViewerActivity.this.shareAction(uri);
            }
        });
        ((TextView) findViewById(R.id.TextView02)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImageViewerActivity.this.shareAction(uri);
            }
        });
        ((Button) findViewById(R.id.delete)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImageViewerActivity.this.deleteAction(uri, cadena);
            }
        });
        ((TextView) findViewById(R.id.TextView03)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ImageViewerActivity.this.deleteAction(uri, cadena);
            }
        });
    }

    /* access modifiers changed from: private */
    public void deleteAction(final Uri uri, final String cadena) {
        new AlertDialog.Builder(this.instance).setMessage(getString(R.string.image_dialog_summary)).setTitle(getString(R.string.image_dialog_title)).setIcon(17301543).setCancelable(true).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ImageViewerActivity.this.deleteFile(uri, cadena);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public void shareAction(Uri uri) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SEND");
        intent.setType("image/jpeg");
        intent.putExtra("android.intent.extra.STREAM", uri);
        try {
            startActivity(Intent.createChooser(intent, getString(R.string.image_share_title)));
        } catch (ActivityNotFoundException e) {
        }
    }

    /* access modifiers changed from: private */
    public void deleteFile(Uri uri, String cadena) {
        File f = new File(String.valueOf(Settings.PHOTOS_DIRECTORY) + cadena);
        try {
            if (f.exists()) {
                f.delete();
                Log.i("Info", "Delete " + cadena);
            }
        } catch (Exception e) {
            Log.i("Info", "Delete error");
        }
        try {
            int delete = CameraActivity.instance.getContentResolver().delete(uri, null, null);
        } catch (Exception e2) {
            Log.i("Info", "Excepcion imageViewer " + e2.getMessage());
        }
        CameraActivity.instance.thumbButton.setVisibility(4);
        CameraRefreshThread.lastPhotoPath = "";
        finish();
    }
}
