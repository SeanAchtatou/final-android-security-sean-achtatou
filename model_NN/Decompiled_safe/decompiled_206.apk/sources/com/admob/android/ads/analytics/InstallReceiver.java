package com.admob.android.ads.analytics;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.e;
import com.admob.android.ads.h;
import com.admob.android.ads.r;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class InstallReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        try {
            String stringExtra = intent.getStringExtra("referrer");
            String userId = AdManager.getUserId(context);
            String a = a(stringExtra, userId, AdManager.getApplicationPackageName(context));
            if (a != null) {
                if (Log.isLoggable(AdManager.LOG, 2)) {
                    Log.v(AdManager.LOG, "Processing install from an AdMob ad (" + a + ").");
                }
                r a2 = e.a(a, "Conversion", userId);
                a2.a(new h(this) {
                    public final void a(r rVar) {
                        if (Log.isLoggable(AdManager.LOG, 3)) {
                            Log.d(AdManager.LOG, "Recorded install from an AdMob ad.");
                        }
                    }

                    public final void a(r rVar, Exception exc) {
                        if (Log.isLoggable(AdManager.LOG, 3)) {
                            Log.d(AdManager.LOG, "Failed to record install from an AdMob ad.", exc);
                        }
                    }
                });
                a2.a();
            }
        } catch (Exception e) {
            Log.e(AdManager.LOG, "Unhandled exception processing Market install.", e);
        }
    }

    private static String a(String str, String str2, String str3) {
        if (str != null) {
            try {
                String[] split = str.split("&");
                StringBuilder sb = null;
                for (String str4 : split) {
                    if (str4.startsWith("admob_")) {
                        String[] split2 = str4.substring("admob_".length()).split("=");
                        String encode = URLEncoder.encode(split2[0], "UTF-8");
                        String encode2 = URLEncoder.encode(split2[1], "UTF-8");
                        if (sb == null) {
                            sb = new StringBuilder(128);
                        } else {
                            sb.append("&");
                        }
                        sb.append(encode).append("=").append(encode2);
                    }
                }
                if (sb != null) {
                    sb.append("&").append("isu").append("=").append(URLEncoder.encode(str2, "UTF-8"));
                    sb.append("&").append("app_id").append("=").append(URLEncoder.encode(str3, "UTF-8"));
                    return "http://a.admob.com/f0?" + sb.toString();
                }
            } catch (UnsupportedEncodingException e) {
                Log.e(AdManager.LOG, "Could not create install URL.  Install not tracked.", e);
            }
        }
        return null;
    }
}
