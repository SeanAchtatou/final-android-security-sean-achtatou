package com.kasa0.android.slitherpuzzle;

import android.app.AlertDialog;
import android.graphics.Point;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;

class w implements GestureDetector.OnGestureListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ PuzzleView a;

    w(PuzzleView puzzleView) {
        this.a = puzzleView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kasa0.android.slitherpuzzle.PuzzleView.a(com.kasa0.android.slitherpuzzle.PuzzleView, boolean):void
     arg types: [com.kasa0.android.slitherpuzzle.PuzzleView, int]
     candidates:
      com.kasa0.android.slitherpuzzle.PuzzleView.a(int, int):int
      com.kasa0.android.slitherpuzzle.PuzzleView.a(com.kasa0.android.slitherpuzzle.PuzzleView, int):int
      com.kasa0.android.slitherpuzzle.PuzzleView.a(com.kasa0.android.slitherpuzzle.PuzzleView, byte):void
      com.kasa0.android.slitherpuzzle.PuzzleView.a(com.kasa0.android.slitherpuzzle.PuzzleView, float):void
      com.kasa0.android.slitherpuzzle.PuzzleView.a(com.kasa0.android.slitherpuzzle.PuzzleView, android.graphics.Point):void
      com.kasa0.android.slitherpuzzle.PuzzleView.a(com.kasa0.android.slitherpuzzle.PuzzleView, boolean):void */
    private void a() {
        this.a.p = true;
        this.a.d.a(true);
        this.a.d.a((Boolean) false);
        this.a.d.c(false);
        this.a.l();
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.d);
        builder.setTitle((int) C0003R.string.completed_title);
        builder.setMessage(this.a.d.getString(C0003R.string.congratulations, new Object[]{this.a.d.i()}));
        builder.setPositiveButton((int) C0003R.string.ok, new x(this));
        builder.create();
        builder.show();
    }

    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (!this.a.o.a()) {
            if (this.a.u) {
                if ((this.a.j - this.a.a) - 70 < this.a.h * this.a.b) {
                    PuzzleView puzzleView = this.a;
                    puzzleView.e = (int) (((float) puzzleView.e) - f);
                    this.a.e = this.a.a(this.a.e);
                }
            } else if (this.a.j - (this.a.a + this.a.a) < this.a.h * this.a.b) {
                PuzzleView puzzleView2 = this.a;
                puzzleView2.e = (int) (((float) puzzleView2.e) - f);
                this.a.e = this.a.a(this.a.e);
            }
            if (this.a.k - (this.a.a + this.a.a) < this.a.h * this.a.c) {
                PuzzleView puzzleView3 = this.a;
                puzzleView3.f = (int) (((float) puzzleView3.f) - f2);
                this.a.f = this.a.b(this.a.f);
            }
            this.a.invalidate();
        }
        return false;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kasa0.android.slitherpuzzle.PuzzleView.a(com.kasa0.android.slitherpuzzle.PuzzleView, int):int
     arg types: [com.kasa0.android.slitherpuzzle.PuzzleView, byte]
     candidates:
      com.kasa0.android.slitherpuzzle.PuzzleView.a(int, int):int
      com.kasa0.android.slitherpuzzle.PuzzleView.a(com.kasa0.android.slitherpuzzle.PuzzleView, byte):void
      com.kasa0.android.slitherpuzzle.PuzzleView.a(com.kasa0.android.slitherpuzzle.PuzzleView, float):void
      com.kasa0.android.slitherpuzzle.PuzzleView.a(com.kasa0.android.slitherpuzzle.PuzzleView, android.graphics.Point):void
      com.kasa0.android.slitherpuzzle.PuzzleView.a(com.kasa0.android.slitherpuzzle.PuzzleView, boolean):void
      com.kasa0.android.slitherpuzzle.PuzzleView.a(com.kasa0.android.slitherpuzzle.PuzzleView, int):int */
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        int e;
        boolean z;
        boolean z2;
        int i;
        int i2;
        byte b;
        int i3 = 0;
        if (this.a.p) {
            return false;
        }
        this.a.i();
        float x = motionEvent.getX() - ((float) this.a.e);
        float y = motionEvent.getY() - ((float) this.a.f);
        int e2 = (int) ((x / ((float) this.a.h)) + 0.5f);
        if (e2 >= 0 && e2 < this.a.b + 1 && (e = (int) ((y / ((float) this.a.h)) + 0.5f)) >= 0 && e < this.a.c + 1) {
            float e3 = x - ((float) (((int) (x / ((float) this.a.h))) * this.a.h));
            if (((double) e3) >= ((double) this.a.h) / 2.0d) {
                e3 = ((float) this.a.h) - e3;
                z = true;
            } else {
                z = false;
            }
            float e4 = y - ((float) (((int) (y / ((float) this.a.h))) * this.a.h));
            if (((double) e4) >= ((double) this.a.h) / 2.0d) {
                e4 = ((float) this.a.h) - e4;
                z2 = true;
            } else {
                z2 = false;
            }
            if (e3 < e4) {
                i = (e <= 0 || !z2) ? e : e - 1;
                if (e2 < 0 || i < 0 || i >= this.a.c) {
                    Log.v("SlitherPuzzle:PuzzleView", "Out of Index: (" + e2 + ", " + i + ")");
                    return false;
                }
                this.a.d.h();
                b = this.a.e(e2, i);
                i2 = e2;
            } else {
                int i4 = (e2 <= 0 || !z) ? e2 : e2 - 1;
                if (i4 < 0 || e < 0 || i4 >= this.a.b) {
                    Log.v("SlitherPuzzle:PuzzleView", "Out of Index: (" + i4 + ", " + e + ")");
                    return false;
                }
                this.a.d.h();
                i3 = 1;
                byte b2 = this.a.f(i4, e);
                i = e;
                i2 = i4;
                b = b2;
            }
            this.a.H = (Point) null;
            this.a.d.a(i2, i, i3, this.a.e((int) b));
            this.a.A.x = i2;
            this.a.A.y = i;
            this.a.B = (byte) b;
            this.a.d.c(true);
            if (this.a.s >= 1) {
                this.a.setPanelColor(this.a.s);
            }
            if (this.a.f()) {
                a();
            } else {
                this.a.d.b();
                this.a.invalidate();
            }
        }
        return true;
    }
}
