package com.kasa0.android.slitherpuzzle;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Commercial extends Activity implements View.OnClickListener {
    private static boolean a = false;
    private static boolean b = false;
    private int c;

    public void onClick(View view) {
        switch (view.getId()) {
            case C0003R.id.later_button /*2131427330*/:
                finish();
                return;
            case C0003R.id.no_need_button /*2131427331*/:
                SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
                if (this.c == 0) {
                    edit.putBoolean("INSATLL_SP2", true);
                } else {
                    edit.putBoolean("INSATLL_ROSARY", true);
                }
                edit.commit();
                finish();
                return;
            case C0003R.id.install_button /*2131427332*/:
                if (this.c == 0) {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.kasa0.android.slitherpuzzle2")));
                } else {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.kasa0.android.rosary")));
                }
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0003R.layout.commercial);
        ((Button) findViewById(C0003R.id.later_button)).setOnClickListener(this);
        ((Button) findViewById(C0003R.id.install_button)).setOnClickListener(this);
        ((Button) findViewById(C0003R.id.no_need_button)).setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.c = getIntent().getIntExtra("mode", 0);
        if (this.c == 1) {
            ((TextView) findViewById(C0003R.id.message)).setText((int) C0003R.string.install_rosary);
        }
    }
}
