package com.kasa0.android.slitherpuzzle;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import jp.adlantis.android.AdlantisAd;
import net.nend.android.NendAdIconLayout;

public class PuzzleControl extends Activity implements SharedPreferences.OnSharedPreferenceChangeListener, View.OnClickListener, View.OnLongClickListener {
    private static boolean s = false;
    private ImageButton a;
    private ImageButton b;
    private ImageButton c;
    private ImageButton d;
    private ImageButton e;
    private PuzzleView f;
    private m g;
    private long h = 0;
    private int i;
    private int j = 10;
    private int k = 10;
    private String l;
    /* access modifiers changed from: private */
    public int m;
    private boolean n = true;
    private int[] o = new int[3];
    private SoundPool p;
    /* access modifiers changed from: private */
    public Chronometer q;
    /* access modifiers changed from: private */
    public long r;
    private boolean t = false;
    private int u = 0;
    private String v;
    private boolean w = false;
    private boolean x = false;

    private void a(int i2) {
        LinearLayout linearLayout = (LinearLayout) findViewById(C0003R.id.LinearLayout01);
        TextView textView = (TextView) findViewById(C0003R.id.time);
        Chronometer chronometer = (Chronometer) findViewById(C0003R.id.elapsed_time);
        if (i2 == 0) {
            linearLayout.setBackgroundColor(-1);
            textView.setTextColor(-16777216);
            chronometer.setTextColor(-16777216);
            return;
        }
        linearLayout.setBackgroundColor(-16777216);
        textView.setTextColor(-1);
        chronometer.setTextColor(-1);
    }

    private void b(int i2) {
        switch (i2) {
            case NendAdIconLayout.HORIZONTAL /*0*/:
                setRequestedOrientation(-1);
                return;
            case 1:
                setRequestedOrientation(1);
                return;
            case AdlantisAd.ADTYPE_TEXT /*2*/:
                setRequestedOrientation(0);
                return;
            default:
                return;
        }
    }

    private void j() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) C0003R.string.retry_label);
        builder.setMessage((int) C0003R.string.retry_lmessage2);
        builder.setPositiveButton((int) C0003R.string.yes, new i(this));
        builder.setNegativeButton((int) C0003R.string.no_, (DialogInterface.OnClickListener) null);
        builder.create();
        builder.show();
    }

    /* access modifiers changed from: private */
    public void k() {
        this.f.e();
        this.g.a();
        this.g.b();
        a((Boolean) true);
        b((Boolean) false);
        c(false);
        a(false);
        this.m = 0;
        this.r = SystemClock.elapsedRealtime();
        this.q.setBase(this.r);
        this.q.start();
        this.r = 0;
    }

    /* access modifiers changed from: private */
    public void l() {
        this.g.c(this.f.getPuzzleData());
        if (this.g.b(this.f.getPuzzleData()) > 0) {
            this.f.a(this.g.e(), this.g.f(), this.g.g());
        }
        a(false);
    }

    private int m() {
        long j2;
        long j3;
        float f2 = ((float) (this.j * this.k)) / 100.0f;
        switch (this.i) {
            case NendAdIconLayout.HORIZONTAL /*0*/:
                j2 = (long) (80000.0f * f2);
                j3 = (long) (f2 * 120000.0f);
                break;
            case 1:
                j2 = (long) (180000.0f * f2);
                j3 = (long) (f2 * 240000.0f);
                break;
            case AdlantisAd.ADTYPE_TEXT /*2*/:
                j2 = (long) (240000.0f * f2);
                j3 = (long) (f2 * 360000.0f);
                break;
            default:
                j2 = (long) (300000.0f * f2);
                j3 = (long) (f2 * 420000.0f);
                break;
        }
        if (this.r <= j2) {
            return 3;
        }
        return this.r <= j3 ? 2 : 1;
    }

    public void a() {
        this.q.stop();
        SharedPreferences.Editor edit = getPreferences(0).edit();
        this.g.b(edit);
        edit.putLong(String.valueOf(this.v) + "puzzleTime", 0);
        edit.putInt("comb", this.u);
        edit.putLong("no", this.h).commit();
        if (!this.w) {
            Intent intent = new Intent();
            if (this.m == 0 && this.r == 0) {
                this.r = SystemClock.elapsedRealtime() - this.q.getBase();
            }
            intent.putExtra("puzzleTime", this.r);
            intent.putExtra("puzzleNo", this.h);
            intent.putExtra("puzzleData", this.f.getStrPuzzleData());
            intent.putExtra("puzzleSolved", this.m);
            setResult(-1, intent);
        }
    }

    public void a(int i2, int i3, int i4, int i5) {
        this.g.a(i2, i3, i4, i5);
    }

    public void a(Boolean bool) {
        this.b.setEnabled(bool.booleanValue());
        if (!bool.booleanValue()) {
            this.g.c();
        }
    }

    public void a(boolean z) {
        if (z) {
            this.r = SystemClock.elapsedRealtime() - this.q.getBase();
            this.q.stop();
            this.m = m();
        }
        if (this.u == 0) {
            n nVar = new n(getApplicationContext(), null);
            if (z) {
                nVar.a(this.h, this.r);
                nVar.b(this.h, this.r);
            } else {
                this.m = 0;
            }
            nVar.a(this.h, this.m);
        }
    }

    public void b() {
        this.g.h();
    }

    public void b(Boolean bool) {
        this.a.setEnabled(bool.booleanValue());
    }

    public void b(boolean z) {
        this.c.setEnabled(z);
    }

    public int c() {
        return this.j;
    }

    public void c(boolean z) {
        if (z) {
            this.x = false;
            if (this.d.getVisibility() == 8 && this.e.getVisibility() == 8) {
                return;
            }
        }
        if (Settings.d(this)) {
            this.d.setVisibility(0);
            this.d.setEnabled(z);
        }
        this.e.setVisibility(8);
    }

    public int d() {
        return this.k;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() != 4 || keyEvent.getAction() != 0 || this.u <= 0) {
            return super.dispatchKeyEvent(keyEvent);
        }
        a();
        getWindow().clearFlags(128);
        if (this.p != null) {
            this.p.release();
            this.p = null;
        }
        finish();
        return false;
    }

    public String e() {
        return this.l;
    }

    public boolean f() {
        return this.m > 0;
    }

    public void g() {
        this.d.setVisibility(8);
        this.e.setVisibility(0);
    }

    public void h() {
        if (this.n && this.p != null) {
            try {
                this.p.play(this.o[0], 1.0f, 1.0f, 0, 0, 1.0f);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public String i() {
        long j2;
        long j3 = this.r / 1000;
        long j4 = j3 / 60;
        long j5 = j3 - (j4 * 60);
        if (j4 >= 60) {
            j2 = j4 / 60;
            j4 -= 60 * j2;
        } else {
            j2 = 0;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(j5);
        if (sb.length() < 2) {
            sb.insert(0, "0");
        }
        sb.insert(0, String.valueOf(j4) + ":");
        if (j2 > 0) {
            if (j4 < 10) {
                sb.insert(0, "0");
            }
            sb.insert(0, String.valueOf(j2) + ":");
        }
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        s = false;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onClick(View view) {
        try {
            switch (view.getId()) {
                case C0003R.id.zoomOut_button /*2131427344*/:
                    if (!this.f.d()) {
                        b(false);
                        return;
                    } else if (this.n && this.p != null) {
                        this.p.play(this.o[0], 1.0f, 1.0f, 0, 0, 1.0f);
                        return;
                    } else {
                        return;
                    }
                case C0003R.id.zoomIn_button /*2131427345*/:
                    if (this.n && this.p != null) {
                        this.p.play(this.o[0], 1.0f, 1.0f, 0, 0, 1.0f);
                    }
                    this.f.c();
                    b(true);
                    return;
                case C0003R.id.save_button /*2131427346*/:
                    if (this.g.a(this.f.getPuzzleData())) {
                        if (this.n && this.p != null) {
                            this.p.play(this.o[1], 1.0f, 1.0f, 0, 0, 1.0f);
                        }
                        b((Boolean) true);
                        return;
                    }
                    return;
                case C0003R.id.stack_image0_disable /*2131427347*/:
                case C0003R.id.stack_image0 /*2131427348*/:
                case C0003R.id.stack_image1 /*2131427349*/:
                case C0003R.id.stack_image2 /*2131427350*/:
                case C0003R.id.stack_image3 /*2131427351*/:
                default:
                    return;
                case C0003R.id.load_button /*2131427352*/:
                    if (this.n && this.p != null) {
                        this.p.play(this.o[2], 1.0f, 1.0f, 0, 0, 1.0f);
                    }
                    if (this.m > 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setTitle((int) C0003R.string.overwrite_title);
                        builder.setMessage((int) C0003R.string.confirm_overwrite);
                        builder.setPositiveButton((int) C0003R.string.no_, (DialogInterface.OnClickListener) null);
                        builder.setNegativeButton((int) C0003R.string.yes, new j(this));
                        builder.create();
                        builder.show();
                        return;
                    }
                    l();
                    c(true);
                    this.x = true;
                    return;
                case C0003R.id.undo_button /*2131427353*/:
                    if (this.n && this.p != null) {
                        this.p.play(this.o[0], 1.0f, 1.0f, 0, 0, 1.0f);
                    }
                    if (this.x) {
                        this.g.d(this.f.getPuzzleData());
                        this.f.a((Point) null, 0, 0);
                        c(false);
                        this.x = false;
                        return;
                    }
                    this.f.g();
                    g();
                    return;
                case C0003R.id.redo_button /*2131427354*/:
                    if (this.n && this.p != null) {
                        this.p.play(this.o[0], 1.0f, 1.0f, 0, 0, 1.0f);
                    }
                    this.f.h();
                    c(true);
                    return;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        e2.printStackTrace();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        b(Settings.h(this));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int i2;
        super.onCreate(bundle);
        if (Settings.g(this)) {
            requestWindowFeature(1);
            getWindow().addFlags(1024);
        }
        setVolumeControlStream(3);
        Intent intent = getIntent();
        Uri data = intent.getData();
        if (data != null) {
            this.h = 0;
            this.i = 1;
            if (data.getQueryParameter("problem") != null) {
                String[] split = data.getQueryParameter("problem").split("/");
                if (split.length < 3) {
                    finish();
                    return;
                }
                this.j = Integer.parseInt(split[0]);
                this.k = Integer.parseInt(split[1]);
                if (split.length != this.k + 2) {
                    finish();
                    return;
                }
                this.l = g.a(this.j, this.k, split);
                if (this.l == null) {
                    finish();
                    return;
                }
                this.w = true;
            } else {
                try {
                    String[] split2 = data.getQuery().split("/");
                    if (!split2[0].equals("slither") || split2.length < 4) {
                        finish();
                        return;
                    }
                    this.j = Integer.parseInt(split2[1]);
                    this.k = Integer.parseInt(split2[2]);
                    this.l = g.a(this.j, this.k, split2[3]);
                    if (this.l == null) {
                        finish();
                        return;
                    }
                    this.w = true;
                } catch (NullPointerException e2) {
                    finish();
                    return;
                }
            }
        } else {
            this.h = intent.getLongExtra("puzzleNo", 0);
            this.i = intent.getIntExtra("puzzleLevel", 0);
            this.j = intent.getIntExtra("puzzleSizeX", 10);
            this.k = intent.getIntExtra("puzzleSizeY", 10);
            this.l = intent.getStringExtra("puzzleData");
        }
        if (bundle == null) {
            this.m = intent.getIntExtra("puzzleSolved", 0);
            this.r = intent.getLongExtra("puzzleTime", 0);
        } else {
            this.m = bundle.getInt("puzzleSolved", 0);
            this.r = bundle.getLong("puzzleTime", 0);
        }
        this.g = new m();
        if (this.g == null) {
            Log.e("SlithierPuzzle:PuzzleControl", "Create error(PuzzleDataStack)");
            finish();
            return;
        }
        PackageManager packageManager = getApplicationContext().getPackageManager();
        if (packageManager.checkPermission("android.permission.INTERNET", getPackageName()) != 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) C0003R.string.permission_error_title);
            builder.setMessage((int) C0003R.string.permission_error_message);
            builder.setPositiveButton((int) C0003R.string.ok, new h(this));
            builder.create().show();
        }
        CharSequence charSequence = null;
        ComponentName callingActivity = getCallingActivity();
        if (callingActivity != null) {
            this.v = callingActivity.getPackageName();
            if (this.v.equals("com.kasa0.android.slithermaker")) {
                this.u = 1;
            } else if (this.v.equals("com.kasa0.android.slitherpuzzle2")) {
                this.u = 2;
            } else {
                this.u = 3;
            }
            try {
                charSequence = packageManager.getApplicationLabel(packageManager.getActivityInfo(callingActivity, 1).applicationInfo);
            } catch (PackageManager.NameNotFoundException e3) {
                e3.printStackTrace();
            }
        }
        if (this.h > 0) {
            if (charSequence == null) {
                charSequence = getString(C0003R.string.app_name);
            }
            setTitle(((Object) charSequence) + " - No." + this.h);
        }
        SharedPreferences preferences = getPreferences(0);
        long j2 = preferences.getLong("no", 0);
        int i3 = preferences.getInt("comb", 0);
        if (j2 == this.h && this.h != 0 && this.u == i3) {
            i2 = this.g.a(preferences, this.j, this.k);
        } else {
            this.g.a();
            SharedPreferences.Editor edit = preferences.edit();
            this.g.a(edit);
            edit.commit();
            i2 = 0;
        }
        b(Settings.h(this));
        setContentView((int) C0003R.layout.puzzle_view);
        this.g.a(this);
        ((AdView) findViewById(C0003R.id.adView)).loadAd(new AdRequest());
        this.f = (PuzzleView) findViewById(C0003R.id.puzzle_board);
        this.a = (ImageButton) findViewById(C0003R.id.load_button);
        this.a.setOnClickListener(this);
        this.a.setOnLongClickListener(this);
        if (i2 > 0) {
            this.g.a(i2);
            b((Boolean) true);
        } else {
            b((Boolean) false);
        }
        this.b = (ImageButton) findViewById(C0003R.id.save_button);
        this.b.setOnClickListener(this);
        this.b.setOnLongClickListener(this);
        if (this.m != 0) {
            a((Boolean) false);
        }
        ((ImageButton) findViewById(C0003R.id.zoomIn_button)).setOnClickListener(this);
        this.c = (ImageButton) findViewById(C0003R.id.zoomOut_button);
        this.c.setOnClickListener(this);
        b(false);
        this.d = (ImageButton) findViewById(C0003R.id.undo_button);
        this.d.setOnClickListener(this);
        this.e = (ImageButton) findViewById(C0003R.id.redo_button);
        this.e.setOnClickListener(this);
        c(false);
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
        a(Settings.f(this));
        if (this.m > 0) {
            j();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(C0003R.menu.menu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
        if (this.f != null) {
            this.f.a();
            this.f = null;
        }
        this.g = null;
    }

    public boolean onLongClick(View view) {
        switch (view.getId()) {
            case C0003R.id.save_button /*2131427346*/:
                if (this.n && this.p != null) {
                    this.p.play(this.o[1], 1.0f, 1.0f, 0, 0, 1.0f);
                }
                this.g.d();
                return true;
            case C0003R.id.load_button /*2131427352*/:
                if (this.x) {
                    if (this.n) {
                        this.p.play(this.o[1], 1.0f, 1.0f, 0, 0, 1.0f);
                    }
                    this.g.d(this.f.getPuzzleData());
                    this.f.a((Point) null, 0, 0);
                    this.x = false;
                }
                return true;
            default:
                return false;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case C0003R.id.settings /*2131427361*/:
                this.q.stop();
                startActivity(new Intent(this, Settings.class));
                return true;
            case C0003R.id.help /*2131427362*/:
                s = true;
                this.q.stop();
                startActivityForResult(new Intent(this, Help.class), 0);
                return true;
            case C0003R.id.clear /*2131427363*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((int) C0003R.string.retry_label);
                builder.setMessage((int) C0003R.string.retry_message);
                builder.setPositiveButton((int) C0003R.string.no_, (DialogInterface.OnClickListener) null);
                builder.setNegativeButton((int) C0003R.string.yes, new k(this));
                builder.create();
                builder.show();
                return true;
            case C0003R.id.check /*2131427364*/:
                this.f.b();
                break;
            case C0003R.id.exit /*2131427365*/:
                Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
                intent.addCategory("android.intent.category.HOME");
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        SharedPreferences.Editor edit = getPreferences(0).edit();
        this.g.b(edit);
        edit.putLong("no", this.h);
        edit.putInt("comb", this.u);
        this.q.stop();
        if (this.u <= 0) {
            n nVar = new n(getApplicationContext(), null);
            nVar.a(this.h, this.f.getStrPuzzleData());
            if (this.m == 0 && this.r == 0) {
                this.r = SystemClock.elapsedRealtime() - this.q.getBase();
                nVar.a(this.h, this.r);
            }
        } else if (this.m == 0 && this.r == 0) {
            this.r = SystemClock.elapsedRealtime() - this.q.getBase();
            edit.putLong(String.valueOf(this.v) + "puzzleTime", this.r);
        }
        edit.commit();
        getWindow().clearFlags(128);
        if (this.p != null) {
            this.p.release();
            this.p = null;
        }
        this.l = null;
        this.v = null;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(C0003R.id.check).setEnabled(this.m == 0);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.t) {
            this.t = false;
            Intent intent = new Intent(this, PuzzleControl.class);
            intent.setAction("android.intent.action.VIEW");
            intent.setFlags(67108864);
            intent.putExtra("puzzleNo", this.h);
            intent.putExtra("puzzleLevel", this.i);
            intent.putExtra("puzzleSolved", this.m);
            intent.putExtra("puzzleSizeX", this.j);
            intent.putExtra("puzzleSizeY", this.k);
            intent.putExtra("puzzleTime", this.r);
            this.l = this.f.getStrPuzzleData();
            intent.putExtra("puzzleData", this.l);
            startActivity(intent);
        }
        if (this.u > 0) {
            long j2 = getPreferences(0).getLong(String.valueOf(this.v) + "puzzleTime", 0);
            if (j2 > 0) {
                this.r = j2;
            }
        }
        getWindow().addFlags(128);
        this.p = new SoundPool(3, 3, 0);
        this.o[0] = this.p.load(this, C0003R.raw.click, 1);
        this.o[1] = this.p.load(this, C0003R.raw.save, 1);
        this.o[2] = this.p.load(this, C0003R.raw.load, 1);
        this.n = Settings.e(this);
        this.q = (Chronometer) findViewById(C0003R.id.elapsed_time);
        this.q.setBase(SystemClock.elapsedRealtime() - this.r);
        if (!s && this.m == 0) {
            this.r = 0;
            this.q.start();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("puzzleSolved", this.m);
        if (this.m <= 0) {
            bundle.putLong("puzzleTime", SystemClock.elapsedRealtime() - this.q.getBase());
        } else {
            bundle.putLong("puzzleTime", this.r);
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if (str.equals("cross")) {
            this.f.setUseCrossMark(sharedPreferences.getBoolean("cross", true));
        } else if (str.equals("numberHelp")) {
            this.f.setNumberAssist(sharedPreferences.getBoolean("numberHelp", true));
        } else if (str.equals("inOutHelp")) {
            this.f.setColorPanelAssist(Integer.parseInt(sharedPreferences.getString("inOutHelp", "0")));
        } else if (str.equals("undoRedo")) {
            if (sharedPreferences.getBoolean("undoRedo", false)) {
                this.d.setVisibility(0);
                return;
            }
            this.d.setVisibility(8);
            this.e.setVisibility(8);
        } else if (str.equals("soundEffect")) {
            this.n = sharedPreferences.getBoolean("soundEffect", true);
        } else if (str.equals("backgroundColor")) {
            int parseInt = Integer.parseInt(sharedPreferences.getString("backgroundColor", "0"));
            this.f.setPaintColor(parseInt);
            a(parseInt);
        } else if (str.equals("fullScreen")) {
            this.t = true;
        } else if (str.equals("orientation")) {
            b(Integer.parseInt(sharedPreferences.getString("orientation", "0")));
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.q = null;
    }
}
