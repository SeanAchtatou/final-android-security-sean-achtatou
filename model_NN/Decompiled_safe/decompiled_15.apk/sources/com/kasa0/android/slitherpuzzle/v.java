package com.kasa0.android.slitherpuzzle;

public class v {
    public static String a(byte[][] bArr) {
        if (bArr == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer(250);
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            for (byte b : bArr[i]) {
                stringBuffer.append(Integer.toHexString((b & 255) + 256).substring(1, 3));
            }
        }
        return stringBuffer.toString();
    }

    public static byte[][] a(int i, int i2, String str) {
        if (str == null) {
            return null;
        }
        byte[][] bArr = new byte[(i2 + 1)][];
        int i3 = 0;
        int i4 = 0;
        while (i3 <= i2) {
            bArr[i3] = new byte[(i + 1)];
            int i5 = i4;
            for (int i6 = 0; i6 <= i; i6++) {
                bArr[i3][i6] = (byte) Integer.parseInt(str.substring(i5, i5 + 2), 16);
                i5 += 2;
            }
            i3++;
            i4 = i5;
        }
        return bArr;
    }

    public static String b(int i, int i2, String str) {
        String str2;
        String str3 = "";
        int i3 = 0;
        while (i3 < i2) {
            for (int i4 = 0; i4 < i; i4++) {
                str2 = str.charAt((i3 * i) + i4) == ' ' ? String.valueOf(str2) + "08" : String.valueOf(str2) + "0" + str.charAt((i3 * i) + i4);
            }
            i3++;
            str3 = String.valueOf(str2) + "08";
        }
        String str4 = str2;
        for (int i5 = 0; i5 <= i; i5++) {
            str4 = String.valueOf(str4) + "08";
        }
        return str4;
    }
}
