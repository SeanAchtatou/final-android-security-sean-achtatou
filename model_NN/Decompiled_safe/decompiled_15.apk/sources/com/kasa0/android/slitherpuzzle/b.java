package com.kasa0.android.slitherpuzzle;

import android.content.Context;
import android.os.Build;
import android.view.MotionEvent;

public abstract class b {
    e a;

    public static b a(Context context, e eVar) {
        b cVar = Build.VERSION.SDK_INT < 5 ? new c(null) : new d(null);
        cVar.a = eVar;
        return cVar;
    }

    public abstract void a(float f);

    public abstract boolean a();

    public abstract boolean a(MotionEvent motionEvent);
}
