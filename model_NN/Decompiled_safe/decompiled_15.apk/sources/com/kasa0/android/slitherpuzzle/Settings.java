package com.kasa0.android.slitherpuzzle;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class Settings extends PreferenceActivity {
    /* access modifiers changed from: private */
    public void a(ListPreference listPreference, String str) {
        String string = getResources().getString(C0003R.string.summary_inout_preference);
        String[] stringArray = getResources().getStringArray(C0003R.array.entries_inOutList_preference);
        String[] stringArray2 = getResources().getStringArray(C0003R.array.entryvalues_inOutList_preference);
        int length = stringArray2.length;
        for (int i = 0; i < length; i++) {
            if (stringArray2[i].equals(str)) {
                listPreference.setSummary(String.valueOf(string) + '(' + stringArray[i] + ')');
                return;
            }
        }
    }

    public static boolean a(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("cross", true);
    }

    /* access modifiers changed from: private */
    public void b(ListPreference listPreference, String str) {
        String string = getResources().getString(C0003R.string.summary_backgroundColor_preference);
        String[] stringArray = getResources().getStringArray(C0003R.array.entries_colorList_preference);
        String[] stringArray2 = getResources().getStringArray(C0003R.array.entryvalues_colorList_preference);
        int length = stringArray2.length;
        for (int i = 0; i < length; i++) {
            if (stringArray2[i].equals(str)) {
                listPreference.setSummary(String.valueOf(string) + '(' + stringArray[i] + ')');
                return;
            }
        }
    }

    public static boolean b(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("numberHelp", true);
    }

    public static int c(Context context) {
        try {
            return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("inOutHelp", "0"));
        } catch (ClassCastException e) {
            return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("inOutHelp", false) ? 2 : 0;
        }
    }

    /* access modifiers changed from: private */
    public void c(ListPreference listPreference, String str) {
        String string = getResources().getString(C0003R.string.summary_orientation_preference);
        String[] stringArray = getResources().getStringArray(C0003R.array.entries_orientationList_preference);
        String[] stringArray2 = getResources().getStringArray(C0003R.array.entryvalues_orientationList_preference);
        int length = stringArray2.length;
        for (int i = 0; i < length; i++) {
            if (stringArray2[i].equals(str)) {
                listPreference.setSummary(String.valueOf(string) + '(' + stringArray[i] + ')');
                return;
            }
        }
    }

    public static boolean d(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("undoRedo", false);
    }

    public static boolean e(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("soundEffect", true);
    }

    public static int f(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("backgroundColor", "0"));
    }

    public static boolean g(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("fullScreen", false);
    }

    public static int h(Context context) {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(context).getString("orientation", "0"));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Context baseContext = getBaseContext();
        try {
            PreferenceManager.getDefaultSharedPreferences(baseContext).getString("inOutHelp", "0");
        } catch (ClassCastException e) {
            boolean z = PreferenceManager.getDefaultSharedPreferences(baseContext).getBoolean("inOutHelp", false);
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(baseContext).edit();
            if (z) {
                edit.putString("inOutHelp", "2").commit();
            } else {
                edit.putString("inOutHelp", "0").commit();
            }
        }
        addPreferencesFromResource(C0003R.xml.settings);
        ListPreference listPreference = (ListPreference) findPreference("inOutHelp");
        a(listPreference, listPreference.getValue());
        listPreference.setOnPreferenceChangeListener(new z(this));
        ListPreference listPreference2 = (ListPreference) findPreference("backgroundColor");
        b(listPreference2, listPreference2.getValue());
        listPreference2.setOnPreferenceChangeListener(new aa(this));
        ListPreference listPreference3 = (ListPreference) findPreference("orientation");
        c(listPreference3, listPreference3.getValue());
        listPreference3.setOnPreferenceChangeListener(new ab(this));
    }
}
