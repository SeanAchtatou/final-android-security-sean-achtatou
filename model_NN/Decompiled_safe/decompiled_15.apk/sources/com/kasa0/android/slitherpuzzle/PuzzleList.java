package com.kasa0.android.slitherpuzzle;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;
import jp.adlantis.android.AdlantisAd;

public class PuzzleList extends ListActivity {
    private static String[] a = {"no", "solved", "sizeX", "sizeY", "firstest"};
    private static int[] b = {C0003R.id.No, C0003R.id.Solved, C0003R.id.SizeX, C0003R.id.SizeY, C0003R.id.Firstest};
    private static ProgressDialog f;
    /* access modifiers changed from: private */
    public static s j;
    private static ac k;
    private t c;
    /* access modifiers changed from: private */
    public n d = null;
    private Cursor e = null;
    /* access modifiers changed from: private */
    public int g = 0;
    private boolean h = true;
    /* access modifiers changed from: private */
    public int i;

    /* access modifiers changed from: private */
    public void a(int i2, boolean z) {
        Context applicationContext = getApplicationContext();
        try {
            this.e = this.d.a(i2, ListSettings.a(this));
            if (this.e != null) {
                this.c = new t(applicationContext, C0003R.layout.puzzle_row, this.e, a, b, getPreferences(0).getLong("no", 0));
                setListAdapter(this.c);
                this.c.notifyDataSetChanged();
                if (z) {
                    d();
                }
            }
        } catch (NullPointerException e2) {
            Log.e("SlitherPuzzle:PuzzleList", e2.getMessage());
        }
    }

    /* access modifiers changed from: private */
    public void a(Integer num) {
        if (f != null) {
            f.setProgress(num.intValue());
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        ListView listView = (ListView) findViewById(16908298);
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.putInt("DIFFICULTY", this.g);
        edit.putInt("POSITION", listView.getFirstVisiblePosition());
        View childAt = listView.getChildAt(0);
        if (childAt != null) {
            edit.putInt("POSITION_Y", childAt.getTop());
        }
        edit.commit();
    }

    private void d() {
        SharedPreferences preferences = getPreferences(0);
        ((ListView) findViewById(16908298)).setSelectionFromTop(preferences.getInt("POSITION", 0), preferences.getInt("POSITION_Y", 0));
    }

    /* access modifiers changed from: private */
    public void e() {
        f = new ProgressDialog(this);
        f.setMessage(getString(C0003R.string.makedb));
        f.setIndeterminate(false);
        f.setProgressStyle(1);
        f.setMax(300);
        f.setCancelable(false);
        f.show();
    }

    /* access modifiers changed from: private */
    public void f() {
        if (f != null) {
            try {
                f.dismiss();
                f = null;
            } catch (IllegalArgumentException e2) {
                Log.e("SlitherPuzzle:PuzzleList", e2.getMessage());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kasa0.android.slitherpuzzle.PuzzleList.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.kasa0.android.slitherpuzzle.PuzzleList.a(com.kasa0.android.slitherpuzzle.PuzzleList, int):void
      com.kasa0.android.slitherpuzzle.PuzzleList.a(com.kasa0.android.slitherpuzzle.PuzzleList, java.lang.Integer):void
      com.kasa0.android.slitherpuzzle.PuzzleList.a(int, boolean):void */
    public void a() {
        if (k != null) {
            k = null;
            a(this.g, false);
            Toast.makeText(this, (int) C0003R.string.added_puzzle, 0).show();
        }
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
        j = new s(this);
        j.execute(sQLiteDatabase);
    }

    public void a(SQLiteDatabase sQLiteDatabase, int i2, int i3) {
        k = new ac(this, this.d);
        k.execute(sQLiteDatabase, Integer.valueOf(i2), Integer.valueOf(i3));
    }

    public void a(boolean z) {
        setProgressBarIndeterminateVisibility(z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kasa0.android.slitherpuzzle.PuzzleList.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.kasa0.android.slitherpuzzle.PuzzleList.a(com.kasa0.android.slitherpuzzle.PuzzleList, int):void
      com.kasa0.android.slitherpuzzle.PuzzleList.a(com.kasa0.android.slitherpuzzle.PuzzleList, java.lang.Integer):void
      com.kasa0.android.slitherpuzzle.PuzzleList.a(int, boolean):void */
    public boolean onContextItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 1:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((int) C0003R.string.clear_solved);
                builder.setMessage((int) C0003R.string.retry_message);
                builder.setPositiveButton((int) C0003R.string.no_, (DialogInterface.OnClickListener) null);
                builder.setNegativeButton((int) C0003R.string.yes, new r(this));
                builder.create();
                builder.show();
                break;
            case AdlantisAd.ADTYPE_TEXT /*2*/:
                new n(getApplicationContext(), null).c((long) this.i);
                c();
                a(this.g, true);
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        requestWindowFeature(5);
        super.onCreate(bundle);
        setContentView((int) C0003R.layout.puzzle_list);
        TabHost tabHost = (TabHost) findViewById(C0003R.id.tabhost);
        tabHost.setup();
        tabHost.addTab(tabHost.newTabSpec("easy").setIndicator("Easy", getResources().getDrawable(C0003R.drawable.easy)).setContent((int) C0003R.id.LinearLayout00));
        tabHost.addTab(tabHost.newTabSpec("normal").setIndicator("Normal", getResources().getDrawable(C0003R.drawable.normal)).setContent((int) C0003R.id.LinearLayout00));
        tabHost.addTab(tabHost.newTabSpec("hard").setIndicator("Hard", getResources().getDrawable(C0003R.drawable.hard)).setContent((int) C0003R.id.LinearLayout00));
        tabHost.addTab(tabHost.newTabSpec("veryhard").setIndicator("Very Hard", getResources().getDrawable(C0003R.drawable.veryhard)).setContent((int) C0003R.id.LinearLayout00));
        tabHost.setCurrentTab(0);
        tabHost.setOnTabChangedListener(new p(this));
        try {
            setTitle(String.valueOf(getString(C0003R.string.app_name)) + " " + getPackageManager().getPackageInfo("com.kasa0.android.slitherpuzzle", 0).versionName);
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("SlitherPuzzle:PuzzleList", e2.getMessage());
        }
        setProgressBarIndeterminateVisibility(false);
        registerForContextMenu((ListView) findViewById(16908298));
        if (j != null && j.a) {
            e();
        }
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        Cursor cursor = (Cursor) getListAdapter().getItem(((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position);
        if (cursor != null) {
            this.i = cursor.getInt(cursor.getColumnIndex("no"));
            contextMenu.setHeaderTitle("No." + this.i);
            contextMenu.add(0, 1, 0, (int) C0003R.string.clear_solved);
            contextMenu.add(0, 3, 0, (int) C0003R.string.cancel);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(C0003R.menu.list_menu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        super.onListItemClick(listView, view, i2, j2);
        this.e.moveToPosition(i2);
        long j3 = this.e.getLong(1);
        getPreferences(0).edit().putLong("no", j3).commit();
        l a2 = new n(getApplicationContext(), this).a(j3);
        if (a2 != null) {
            Intent intent = new Intent(this, PuzzleControl.class);
            intent.setAction("android.intent.action.VIEW");
            intent.putExtra("puzzleNo", j3);
            intent.putExtra("puzzleLevel", this.g);
            intent.putExtra("puzzleSolved", a2.a());
            intent.putExtra("puzzleSizeX", a2.b());
            intent.putExtra("puzzleSizeY", a2.c());
            intent.putExtra("puzzleTime", a2.d());
            intent.putExtra("puzzleData", a2.e());
            startActivity(intent);
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case C0003R.id.list_settings /*2131427358*/:
                startActivity(new Intent(this, ListSettings.class));
                return true;
            case C0003R.id.reset /*2131427359*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((int) C0003R.string.reset_label);
                builder.setMessage((int) C0003R.string.reset_message);
                builder.setPositiveButton((int) C0003R.string.no_, (DialogInterface.OnClickListener) null);
                builder.setNegativeButton((int) C0003R.string.yes, new q(this));
                builder.create();
                builder.show();
                return true;
            case C0003R.id.about /*2131427360*/:
                startActivity(new Intent(this, About.class));
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        ListView listView = (ListView) findViewById(16908298);
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.putInt("DIFFICULTY", this.g);
        edit.putInt("POSITION", listView.getFirstVisiblePosition());
        View childAt = listView.getChildAt(0);
        if (childAt != null) {
            edit.putInt("POSITION_Y", childAt.getTop());
        }
        edit.commit();
        if (j != null && j.a) {
            f();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kasa0.android.slitherpuzzle.PuzzleList.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.kasa0.android.slitherpuzzle.PuzzleList.a(com.kasa0.android.slitherpuzzle.PuzzleList, int):void
      com.kasa0.android.slitherpuzzle.PuzzleList.a(com.kasa0.android.slitherpuzzle.PuzzleList, java.lang.Integer):void
      com.kasa0.android.slitherpuzzle.PuzzleList.a(int, boolean):void */
    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Context applicationContext = getApplicationContext();
        if (this.d == null) {
            this.d = new n(applicationContext, this);
        }
        if (this.d != null) {
            this.g = getPreferences(0).getInt("DIFFICULTY", 0);
            TabHost tabHost = (TabHost) findViewById(C0003R.id.tabhost);
            int currentTab = tabHost.getCurrentTab();
            tabHost.setCurrentTab(this.g);
            if (currentTab == this.g) {
                a(this.g, true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.e != null) {
            this.e.close();
            this.e = null;
        }
        if (j == null && k == null && this.d != null) {
            this.d.a();
            this.d = null;
        }
        super.onStop();
    }
}
