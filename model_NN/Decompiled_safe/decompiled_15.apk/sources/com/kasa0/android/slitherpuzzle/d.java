package com.kasa0.android.slitherpuzzle;

import android.util.FloatMath;
import android.view.MotionEvent;
import jp.adlantis.android.AdlantisAd;

class d extends b {
    private int b;
    private float c;
    private float d;
    private float e;
    private float f;

    private d() {
        this.b = 0;
        this.c = 0.0f;
    }

    /* synthetic */ d(d dVar) {
        this();
    }

    private float b(MotionEvent motionEvent) {
        float x = motionEvent.getX(0) - motionEvent.getX(1);
        float y = motionEvent.getY(0) - motionEvent.getY(1);
        return FloatMath.sqrt((x * x) + (y * y));
    }

    public void a(float f2) {
        this.d = f2;
    }

    public boolean a() {
        return this.b == 2;
    }

    public boolean a(MotionEvent motionEvent) {
        switch (motionEvent.getAction() & 255) {
            case AdlantisAd.ADTYPE_TEXT /*2*/:
                if (motionEvent.getPointerCount() != 2) {
                    this.b = 0;
                    break;
                } else {
                    this.b = 2;
                    float round = ((float) Math.round((float) (Math.round((b(motionEvent) / this.c) * 10.0f) / 2))) * 0.2f;
                    if (round != 1.0f) {
                        this.d = (round + this.e) - 1.0f;
                        if (this.d < 1.0f) {
                            this.d = 1.0f;
                        }
                        if (this.f != this.d) {
                            if (this.f < this.d) {
                                this.a.a(this.d);
                            } else if (this.f > this.d) {
                                this.a.b(this.d);
                            }
                            this.f = this.d;
                            break;
                        }
                    }
                }
                break;
            case 5:
                this.e = this.d;
                this.f = this.d;
                this.c = b(motionEvent);
                break;
        }
        return true;
    }
}
