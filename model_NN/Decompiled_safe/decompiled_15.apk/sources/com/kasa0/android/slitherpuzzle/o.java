package com.kasa0.android.slitherpuzzle;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

class o extends SQLiteOpenHelper {
    final /* synthetic */ n a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o(n nVar, Context context) {
        super(context, "slitherpuzzle.db", (SQLiteDatabase.CursorFactory) null, 28);
        this.a = nVar;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.execSQL("CREATE TABLE puzzles (_id INTEGER PRIMARY KEY AUTOINCREMENT, no INTEGER UNIQUE not null, solved INTEGER DEFAULT 0, sizeX INTEGER not null, sizeY INTEGER not null, diff INTEGER not null, firstest INTEGER, time INTEGER, data TEXT UNIQUE not null);");
            this.a.j.a(sQLiteDatabase);
        } catch (SQLiteException e) {
            Log.e("SlitherPuzzle:PuzzleDatabase", e.getMessage());
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (this.a.j == null || sQLiteDatabase == null) {
            Log.e("SlitherPuzzle:PuzzleDatabase", "Activity or DB is null");
        } else {
            this.a.j.a(sQLiteDatabase, i, i2);
        }
    }
}
