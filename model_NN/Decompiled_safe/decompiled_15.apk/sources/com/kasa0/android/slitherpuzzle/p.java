package com.kasa0.android.slitherpuzzle;

import android.widget.TabHost;

class p implements TabHost.OnTabChangeListener {
    final /* synthetic */ PuzzleList a;

    p(PuzzleList puzzleList) {
        this.a = puzzleList;
    }

    public void onTabChanged(String str) {
        boolean z = true;
        if (str.equals("easy")) {
            this.a.g = 0;
        } else if (str.equals("normal")) {
            this.a.g = 1;
        } else if (str.equals("hard")) {
            this.a.g = 2;
        } else if (str.equals("veryhard")) {
            this.a.g = 3;
        }
        if (this.a.g != this.a.getPreferences(0).getInt("DIFFICULTY", 0)) {
            z = false;
        }
        this.a.getPreferences(0).edit().putInt("DIFFICULTY", this.a.g).commit();
        this.a.a(this.a.g, z);
    }
}
