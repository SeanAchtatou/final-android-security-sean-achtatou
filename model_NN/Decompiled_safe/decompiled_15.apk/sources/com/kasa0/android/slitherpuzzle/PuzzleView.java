package com.kasa0.android.slitherpuzzle;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;
import jp.adlantis.android.AdlantisAd;
import net.nend.android.NendAdIconLayout;

public class PuzzleView extends View {
    /* access modifiers changed from: private */
    public Point A = new Point(-1, -1);
    /* access modifiers changed from: private */
    public byte B;
    private DashPathEffect C = new DashPathEffect(new float[]{3.0f, 4.0f}, 0.0f);
    private final GestureDetector.OnGestureListener D = new w(this);
    private int E;
    private int F;
    private int G;
    /* access modifiers changed from: private */
    public Point H;
    private int I;
    private int J;
    /* access modifiers changed from: private */
    public int a = 15;
    /* access modifiers changed from: private */
    public int b = 10;
    /* access modifiers changed from: private */
    public int c = 10;
    /* access modifiers changed from: private */
    public PuzzleControl d;
    /* access modifiers changed from: private */
    public int e = this.a;
    /* access modifiers changed from: private */
    public int f = this.a;
    private int g = 30;
    /* access modifiers changed from: private */
    public int h = 30;
    /* access modifiers changed from: private */
    public float i = 1.0f;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public int k;
    private Paint.FontMetrics l;
    private byte[][] m;
    private GestureDetector n;
    /* access modifiers changed from: private */
    public b o;
    /* access modifiers changed from: private */
    public boolean p;
    private boolean q;
    private boolean r;
    /* access modifiers changed from: private */
    public int s;
    private int t;
    /* access modifiers changed from: private */
    public boolean u = false;
    private Paint v = new Paint(1);
    private Paint w = new Paint();
    private Paint x = new Paint();
    private Paint y = new Paint();
    private Paint z = new Paint();

    public PuzzleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = (PuzzleControl) context;
        this.b = this.d.c();
        this.c = this.d.d();
        this.m = v.a(this.b, this.c, this.d.e());
        this.p = this.d.f();
        Context context2 = getContext();
        this.q = Settings.a(context2);
        this.r = Settings.b(context2);
        setPaintColor(Settings.f(context2));
        this.s = Settings.c(context2);
        a.a(this.b, this.c);
        if (this.s >= 1) {
            setPanelColor(this.s);
        }
        this.w.setStyle(Paint.Style.FILL);
        this.w.setStrokeWidth(2.0f);
        this.x.setStyle(Paint.Style.FILL);
        this.x.setStrokeWidth(2.0f);
        this.y.setStyle(Paint.Style.FILL);
        this.z.setStyle(Paint.Style.FILL);
        this.v.setStyle(Paint.Style.FILL);
        this.v.setTextSize(((float) this.h) * 0.75f);
        this.v.setTextScaleX(1.0f);
        this.v.setTextAlign(Paint.Align.CENTER);
        this.l = this.v.getFontMetrics();
        if (getResources().getConfiguration().orientation == 2) {
            this.u = true;
        }
        this.n = new GestureDetector(this.d, this.D);
        this.o = b.a(context, new y(this, null));
    }

    public static int a(byte b2) {
        return b2 & 15;
    }

    /* access modifiers changed from: private */
    public int a(int i2) {
        if (i2 >= this.a) {
            return this.a;
        }
        int i3 = (this.j - this.a) - (this.h * this.b);
        if (this.u) {
            i3 -= 60;
        }
        return i2 < i3 ? i3 : i2;
    }

    private int a(int i2, int i3) {
        if (!b(this.m[i3][i2]) || !c(this.m[i3][i2])) {
            if (!b(this.m[i3 - 1][i2]) || !c(this.m[i3][i2 - 1])) {
                if (!b(this.m[i3 - 1][i2]) || !c(this.m[i3][i2])) {
                    if (!b(this.m[i3][i2]) || !c(this.m[i3][i2 - 1]) || a.c(i2, i3) != a.c(i2 + 1, i3 + 1) || a.c(i2, i3) == 0) {
                        return 0;
                    }
                    return 0 + a.c(i2 + 1, i3, a.c(i2, i3));
                } else if (a.c(i2, i3) != a.c(i2 + 1, i3 + 1) || a.c(i2, i3) == 0) {
                    return 0;
                } else {
                    return 0 + a.c(i2, i3 + 1, a.c(i2, i3));
                }
            } else if (a.c(i2 + 1, i3) != a.c(i2, i3 + 1) || a.c(i2 + 1, i3) == 0) {
                return 0;
            } else {
                return 0 + a.c(i2 + 1, i3 + 1, a.c(i2 + 1, i3));
            }
        } else if (a.c(i2 + 1, i3) != a.c(i2, i3 + 1) || a.c(i2 + 1, i3) == 0) {
            return 0;
        } else {
            return 0 + a.c(i2, i3, a.c(i2 + 1, i3));
        }
    }

    private int a(int i2, int i3, int i4) {
        int i5 = 0;
        if (b(this.m[i3][i2])) {
            i5 = 1;
        }
        if (c(this.m[i3][i2])) {
            i5++;
        }
        if (b(this.m[i3][i2 + 1])) {
            i5++;
        }
        if (c(this.m[i3 + 1][i2])) {
            i5++;
        }
        return i5 - i4;
    }

    private void a(Canvas canvas) {
        boolean z2;
        a(canvas, this.x, this.b, this.c);
        b(canvas, this.x, this.b, this.c);
        if (this.E == 3) {
            this.w.setColor(-65536);
            this.w.setStrokeWidth(4.0f);
        }
        if (this.m != null) {
            for (int i2 = 0; i2 < this.c + 1; i2++) {
                int i3 = this.f + (this.h * i2);
                if (this.h + i3 >= 0 && i3 <= this.k) {
                    for (int i4 = 0; i4 < this.b + 1; i4++) {
                        int i5 = this.e + (this.h * i4);
                        if (this.h + i5 >= 0 && i5 <= this.j) {
                            if (this.s >= 1 && i4 < this.b && i2 < this.c) {
                                switch (a.c(i4 + 1, i2 + 1)) {
                                    case 1:
                                        canvas.drawRect((float) (i5 + 3), (float) (i3 + 3), (float) ((this.h + i5) - 3), (float) ((this.h + i3) - 3), this.y);
                                        z2 = true;
                                        break;
                                    case AdlantisAd.ADTYPE_TEXT /*2*/:
                                        canvas.drawRect((float) (i5 + 3), (float) (i3 + 3), (float) ((this.h + i5) - 3), (float) ((this.h + i3) - 3), this.z);
                                        z2 = true;
                                        break;
                                }
                                a(canvas, i4, i2, i5, i3, z2);
                                a(canvas, i5, i3, this.m[i2][i4]);
                            }
                            z2 = false;
                            a(canvas, i4, i2, i5, i3, z2);
                            a(canvas, i5, i3, this.m[i2][i4]);
                        }
                    }
                }
            }
            if (this.E == 2) {
                b(canvas);
            }
        }
    }

    private void a(Canvas canvas, int i2, int i3, byte b2) {
        if (b(b2)) {
            canvas.drawLine((float) i2, (float) i3, (float) i2, (float) (this.h + i3), this.w);
        } else if (d(b2)) {
            a(canvas, i2, (this.h / 2) + i3, this.x);
        }
        if (c(b2)) {
            canvas.drawLine((float) i2, (float) i3, (float) (this.h + i2), (float) i3, this.w);
        } else if (e(b2)) {
            a(canvas, (this.h / 2) + i2, i3, this.x);
        }
    }

    private void a(Canvas canvas, int i2, int i3, int i4, int i5, boolean z2) {
        int a2 = a(this.m[i3][i2]);
        if (a2 <= 4) {
            if ((this.E == 1 || this.r) && a(i2, i3, a2) != 0) {
                if (this.E == 1 || a(i2, i3, a2) >= 0) {
                    this.v.setColor(-65536);
                } else {
                    this.v.setColor(-7829368);
                }
            } else if (this.t == 0 || z2) {
                this.v.setColor(-16777216);
            } else {
                this.v.setColor(-1);
            }
            canvas.drawText(String.valueOf(a(this.m[i3][i2])), (float) ((this.h / 2) + i4), Build.VERSION.SDK_INT >= 14 ? ((float) ((this.h / 2) + i5)) - ((this.l.top + this.l.descent) / 2.0f) : ((float) ((this.h / 2) + i5)) - ((this.l.ascent + this.l.descent) / 2.0f), this.v);
        }
    }

    private void a(Canvas canvas, int i2, int i3, Paint paint) {
        int i4 = this.h / 12;
        int i5 = i4 < 3 ? 3 : i4;
        canvas.drawLine((float) (i2 - i5), (float) (i3 - i5), (float) (i5 + 1 + i2), (float) (i5 + 1 + i3), paint);
        canvas.drawLine((float) (i2 - i5), (float) (i5 + 1 + i3), (float) (i5 + 1 + i2), (float) (i3 - i5), paint);
    }

    private void a(Canvas canvas, Paint paint, int i2, int i3) {
        for (int i4 = 0; i4 < i3 + 1; i4++) {
            int i5 = (this.h * i4) + this.f;
            if (i5 >= -1 && i5 <= this.k) {
                for (int i6 = 0; i6 < i2 + 1; i6++) {
                    int i7 = this.e + (this.h * i6);
                    if (i7 >= -1 && i7 <= this.j) {
                        canvas.drawPoint((float) i7, (float) i5, paint);
                        canvas.drawPoint((float) (i7 - 1), (float) i5, paint);
                        canvas.drawPoint((float) (i7 + 1), (float) i5, paint);
                        canvas.drawPoint((float) i7, (float) (i5 - 1), paint);
                        canvas.drawPoint((float) i7, (float) (i5 + 1), paint);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public int b(int i2) {
        if (i2 >= this.a) {
            return this.a;
        }
        int i3 = (this.k - this.a) - (this.h * this.c);
        if (this.u) {
            i3 -= 105;
        }
        return i2 < i3 ? i3 : i2;
    }

    private int b(int i2, int i3) {
        int i4 = 0;
        byte c2 = a.c(i2 + 1, i3 + 1);
        if (c2 == 0) {
            return 0;
        }
        if (c(this.m[i3][i2])) {
            if (i2 > 0) {
                i4 = 0 + a.c(i2, i3 + 1, c2);
            }
            if (i2 < this.b - 1) {
                i4 += a.c(i2 + 2, i3 + 1, c2);
            }
            return i3 < this.c + -1 ? i4 + a.c(i2 + 1, i3 + 2, c2) : i4;
        } else if (c(this.m[i3 + 1][i2])) {
            if (i2 > 0) {
                i4 = 0 + a.c(i2, i3 + 1, c2);
            }
            if (i2 < this.b - 1) {
                i4 += a.c(i2 + 2, i3 + 1, c2);
            }
            return i3 > 0 ? i4 + a.c(i2 + 1, i3, c2) : i4;
        } else if (b(this.m[i3][i2])) {
            if (i3 > 0) {
                i4 = 0 + a.c(i2 + 1, i3, c2);
            }
            if (i2 < this.b - 1) {
                i4 += a.c(i2 + 2, i3 + 1, c2);
            }
            return i3 < this.c + -1 ? i4 + a.c(i2 + 1, i3 + 2, c2) : i4;
        } else if (!b(this.m[i3][i2 + 1])) {
            return 0;
        } else {
            if (i3 > 0) {
                i4 = 0 + a.c(i2 + 1, i3, c2);
            }
            if (i2 > 0) {
                i4 += a.c(i2, i3 + 1, c2);
            }
            return i3 < this.c + -1 ? i4 + a.c(i2 + 1, i3 + 2, c2) : i4;
        }
    }

    private void b(Canvas canvas) {
        int i2;
        int i3 = 0;
        int i4 = 0;
        loop0:
        while (true) {
            if (i3 >= this.c) {
                i2 = i4;
                break;
            }
            int i5 = 0;
            while (i5 < this.b + 1) {
                if (b(this.m[i3][i5])) {
                    i2 = i5;
                    break loop0;
                }
                i5++;
            }
            i3++;
            i4 = i5;
        }
        if (i3 < this.c) {
            this.w.setColor(-65536);
            this.w.setStrokeWidth(4.0f);
            this.F = i2;
            this.G = i3;
            int i6 = 0;
            int i7 = 1;
            while (true) {
                int i8 = this.f + (this.G * this.h);
                int i9 = this.e + (this.F * this.h);
                if (b(this.m[this.G][this.F])) {
                    canvas.drawLine((float) i9, (float) i8, (float) i9, (float) (this.h + i8), this.w);
                }
                if (c(this.m[this.G][this.F])) {
                    canvas.drawLine((float) i9, (float) i8, (float) (this.h + i9), (float) i8, this.w);
                }
                int i10 = i6 + 1;
                int d2 = d(i7);
                if ((i10 <= 3 || d2 != 1 || this.F != i2 || this.G != i3) && d2 != 0 && d2 != 5) {
                    i6 = i10;
                    i7 = d2;
                } else {
                    return;
                }
            }
        }
    }

    private void b(Canvas canvas, Paint paint, int i2, int i3) {
        int i4 = this.h / 3;
        int i5 = this.e + (this.h * i2);
        int i6 = this.f + (this.h * i3);
        canvas.drawLine((float) (this.e - i4), (float) (this.f - i4), (float) (i5 + i4), (float) (this.f - i4), this.w);
        canvas.drawLine((float) (i5 + i4), (float) (this.f - i4), (float) (i5 + i4), (float) (i6 + i4), this.w);
        canvas.drawLine((float) (i5 + i4), (float) (i6 + i4), (float) (this.e - i4), (float) (i6 + i4), this.w);
        canvas.drawLine((float) (this.e - i4), (float) (i6 + i4), (float) (this.e - i4), (float) (this.f - i4), this.w);
    }

    private static boolean b(byte b2) {
        return (b2 & 48) == 16;
    }

    private int c(int i2) {
        int i3 = 0;
        int i4 = 0;
        loop0:
        while (i3 < this.c) {
            i4 = 0;
            while (i4 < this.b + 1) {
                if (b(this.m[i3][i4])) {
                    break loop0;
                }
                i4++;
            }
            i3++;
        }
        if (i3 >= this.c) {
            return 2;
        }
        this.F = i4;
        this.G = i3;
        int i5 = 0;
        int i6 = 1;
        do {
            i5++;
            if (i5 > i2) {
                break;
            }
            i6 = d(i6);
            if (i6 != 1 || this.F != i4 || this.G != i3) {
                if (i6 == 0) {
                    break;
                }
            } else {
                return i5 != i2 ? 1 : 0;
            }
        } while (i6 != 5);
        return 2;
    }

    private int c(int i2, int i3) {
        byte c2 = a.c(i2 + 1, i3 + 1);
        int i4 = 0;
        if (c(this.m[i3][i2]) && b(this.m[i3][i2])) {
            if (i2 < this.b - 1) {
                i4 = 0 + a.c(i2 + 2, i3 + 1, c2);
            }
            return i3 < this.c + -1 ? i4 + a.c(i2 + 1, i3 + 2, c2) : i4;
        } else if (c(this.m[i3 + 1][i2]) && b(this.m[i3][i2])) {
            if (i2 < this.b - 1) {
                i4 = 0 + a.c(i2 + 2, i3 + 1, c2);
            }
            return i3 > 0 ? i4 + a.c(i2 + 1, i3, c2) : i4;
        } else if (c(this.m[i3][i2]) && b(this.m[i3][i2 + 1])) {
            if (i2 > 0) {
                i4 = 0 + a.c(i2, i3 + 1, c2);
            }
            return i3 < this.c + -1 ? i4 + a.c(i2 + 1, i3 + 2, c2) : i4;
        } else if (!c(this.m[i3 + 1][i2]) || !b(this.m[i3][i2 + 1])) {
            return 0;
        } else {
            if (i2 > 0) {
                i4 = 0 + a.c(i2, i3 + 1, c2);
            }
            return i3 > 0 ? i4 + a.c(i2 + 1, i3, c2) : i4;
        }
    }

    private void c(Canvas canvas) {
        if (this.H != null) {
            int color = this.w.getColor();
            this.w.setColor(-65536);
            this.w.setStrokeWidth(4.0f);
            this.w.setPathEffect(this.C);
            int i2 = this.e + (this.H.x * this.h);
            int i3 = this.f + (this.H.y * this.h);
            if (this.J == 2) {
                int i4 = this.h;
                this.h *= 2;
                if (this.I == 0) {
                    a(canvas, i2, (i4 / 2) + i3, this.w);
                } else {
                    a(canvas, i2 + (i4 / 2), i3, this.w);
                }
                this.h = i4;
            } else if (this.I == 0) {
                canvas.drawLine((float) i2, (float) i3, (float) i2, (float) (this.h + i3), this.w);
            } else {
                canvas.drawLine((float) i2, (float) i3, (float) (i2 + this.h), (float) i3, this.w);
            }
            this.w.setColor(color);
            this.w.setStrokeWidth(2.0f);
            this.w.setPathEffect(null);
        }
    }

    private static boolean c(byte b2) {
        return (b2 & -64) == 64;
    }

    private int d(int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        int i18 = 0;
        int i19 = -1;
        switch (i2) {
            case 1:
                if (c(this.m[this.G][this.F])) {
                    i3 = 4;
                    i17 = 1;
                } else {
                    i17 = 0;
                    i3 = 0;
                }
                if (this.G <= 0 || !b(this.m[this.G - 1][this.F])) {
                    i4 = i17;
                    i5 = 0;
                } else {
                    i3 = 1;
                    i4 = i17 + 1;
                    i5 = -1;
                }
                if (this.F > 0 && c(this.m[this.G][this.F - 1])) {
                    i7 = i4 + 1;
                    i18 = i5;
                    i6 = 3;
                    break;
                }
                i19 = 0;
                i18 = i5;
                i6 = i3;
                i7 = i4;
                break;
            case AdlantisAd.ADTYPE_TEXT /*2*/:
                if (this.G >= this.c || !c(this.m[this.G + 1][this.F])) {
                    i15 = 0;
                    i7 = 0;
                    i16 = 0;
                } else {
                    i7 = 1;
                    i16 = 4;
                    i15 = 1;
                }
                if (this.G < this.c - 1 && b(this.m[this.G + 1][this.F])) {
                    i7++;
                    i15 = 1;
                    i16 = 2;
                }
                if (this.F > 0 && this.G < this.c && c(this.m[this.G + 1][this.F - 1])) {
                    i7++;
                    i18 = 1;
                    i6 = 3;
                    break;
                } else {
                    i19 = 0;
                    i18 = i15;
                    i6 = i16;
                    break;
                }
                break;
            case 3:
                if (b(this.m[this.G][this.F])) {
                    i11 = 1;
                    i12 = 2;
                } else {
                    i11 = 0;
                    i12 = 0;
                }
                if (this.G <= 0 || !b(this.m[this.G - 1][this.F])) {
                    i13 = i11;
                    i14 = 0;
                } else {
                    i12 = 1;
                    i13 = i11 + 1;
                    i14 = -1;
                }
                if (this.F > 0 && c(this.m[this.G][this.F - 1])) {
                    i7 = i4 + 1;
                    i18 = i5;
                    i6 = 3;
                    break;
                }
                i19 = 0;
                i18 = i5;
                i6 = i3;
                i7 = i4;
                break;
            case 4:
                if (this.F >= this.b || !c(this.m[this.G][this.F + 1])) {
                    i8 = 0;
                    i9 = 0;
                    i10 = 0;
                } else {
                    i9 = 1;
                    i10 = 4;
                    i8 = 1;
                }
                if (this.F < this.b + 1 && b(this.m[this.G][this.F + 1])) {
                    i9++;
                    i8 = 1;
                    i10 = 2;
                }
                if (this.F < this.b + 1 && this.G > 0 && b(this.m[this.G - 1][this.F + 1])) {
                    i7++;
                    i18 = -1;
                    i6 = 1;
                    i19 = 1;
                    break;
                } else {
                    i19 = i8;
                    i6 = i10;
                    break;
                }
                break;
            default:
                i19 = 0;
                i7 = 0;
                i6 = 0;
                break;
        }
        if (i7 >= 2) {
            i6 = 5;
        }
        this.F += i19;
        this.G += i18;
        return i6;
    }

    private int d(int i2, int i3) {
        byte c2 = a.c(i2 + 1, i3 + 1);
        if (!c(this.m[i3][i2]) || !b(this.m[i3][i2]) || !b(this.m[i3][i2 + 1])) {
            if (!c(this.m[i3 + 1][i2]) || !b(this.m[i3][i2]) || !b(this.m[i3][i2 + 1])) {
                if (!b(this.m[i3][i2]) || !c(this.m[i3][i2]) || !c(this.m[i3 + 1][i2])) {
                    if (!b(this.m[i3][i2 + 1]) || !c(this.m[i3][i2]) || !c(this.m[i3 + 1][i2])) {
                        return 0;
                    }
                    if (i2 > 0) {
                        return c2 != 0 ? 0 + a.c(i2, i3 + 1, c2) : 0 + a.c(i2 + 1, i3 + 1, a.c(i2, i3 + 1));
                    }
                    if (c2 == 0) {
                        return 0 + a.c(i2 + 1, i3 + 1, (byte) 1);
                    }
                    return 0;
                } else if (i2 < this.b - 1) {
                    return c2 != 0 ? 0 + a.c(i2 + 2, i3 + 1, c2) : 0 + a.c(i2 + 1, i3 + 1, a.c(i2 + 2, i3 + 1));
                } else {
                    if (c2 == 0) {
                        return 0 + a.c(i2 + 1, i3 + 1, (byte) 1);
                    }
                    return 0;
                }
            } else if (i3 > 0) {
                return c2 != 0 ? 0 + a.c(i2 + 1, i3, c2) : 0 + a.c(i2 + 1, i3 + 1, a.c(i2 + 1, i3));
            } else {
                if (c2 == 0) {
                    return 0 + a.c(i2 + 1, i3 + 1, (byte) 1);
                }
                return 0;
            }
        } else if (i3 < this.c - 1) {
            return c2 != 0 ? 0 + a.c(i2 + 1, i3 + 2, c2) : 0 + a.c(i2 + 1, i3 + 1, a.c(i2 + 1, i3 + 2));
        } else {
            if (c2 == 0) {
                return 0 + a.c(i2 + 1, i3 + 1, (byte) 1);
            }
            return 0;
        }
    }

    private void d(Canvas canvas) {
        if (this.A.x != -1) {
            int i2 = this.e + (this.A.x * this.h);
            int i3 = this.f + (this.A.y * this.h);
            if (b(this.B)) {
                this.w.setStrokeWidth(4.0f);
                canvas.drawLine((float) i2, (float) i3, (float) i2, (float) (this.h + i3), this.w);
                this.w.setStrokeWidth(2.0f);
            } else if (this.q && d(this.B)) {
                this.x.setStrokeWidth(4.0f);
                a(canvas, i2, (this.h / 2) + i3, this.x);
                this.x.setStrokeWidth(2.0f);
            }
            if (c(this.B)) {
                this.w.setStrokeWidth(4.0f);
                canvas.drawLine((float) i2, (float) i3, (float) (this.h + i2), (float) i3, this.w);
                this.w.setStrokeWidth(2.0f);
            } else if (this.q && e(this.B)) {
                this.x.setStrokeWidth(4.0f);
                a(canvas, (this.h / 2) + i2, i3, this.x);
                this.x.setStrokeWidth(2.0f);
            }
        }
    }

    private static boolean d(byte b2) {
        return (b2 & 48) == 32;
    }

    /* access modifiers changed from: private */
    public byte e(int i2, int i3) {
        byte b2 = (byte) (((byte) (this.m[i3][i2] & 48)) + 16);
        if ((!this.q && b2 > 16) || b2 > 32) {
            b2 = 0;
        }
        this.m[i3][i2] = (byte) ((this.m[i3][i2] & -49) | b2);
        return (byte) (b2 | 1);
    }

    /* access modifiers changed from: private */
    public int e(int i2) {
        return ((i2 & -128) == -128 || (i2 & 32) == 32) ? 2 : 1;
    }

    private static boolean e(byte b2) {
        return (b2 & -64) == Byte.MIN_VALUE;
    }

    private static byte f(byte b2) {
        return (byte) (b2 & 207);
    }

    /* access modifiers changed from: private */
    public byte f(int i2, int i3) {
        short s2 = (short) (((short) (((short) (this.m[i3][i2] & -64)) & 255)) + 64);
        if ((!this.q && s2 > 64) || s2 > 128) {
            s2 = 0;
        }
        this.m[i3][i2] = (byte) ((this.m[i3][i2] & 63) | s2);
        return (byte) (s2 | 4);
    }

    private static byte g(byte b2) {
        return (byte) (b2 & 63);
    }

    private byte g(int i2, int i3) {
        byte b2 = (byte) (this.m[i3][i2] & 48);
        byte b3 = b2 == 0 ? this.q ? (byte) 32 : 16 : (byte) (b2 - 16);
        this.m[i3][i2] = (byte) ((this.m[i3][i2] & -49) | b3);
        return (byte) (b3 | 1);
    }

    private byte h(int i2, int i3) {
        short s2 = (short) (((short) (this.m[i3][i2] & -64)) & 255);
        short s3 = s2 == 0 ? this.q ? (short) -128 : 64 : (short) (s2 - 64);
        this.m[i3][i2] = (byte) ((this.m[i3][i2] & 63) | s3);
        return (byte) (s3 | 4);
    }

    /* access modifiers changed from: private */
    public void i() {
        this.E = 0;
        setPaintColor(this.t);
    }

    /* access modifiers changed from: private */
    public void j() {
        this.h = (int) (((float) this.g) * this.i);
        this.a = this.h / 2;
        if (this.j - this.a > this.h * this.b) {
            this.e = this.a;
        } else {
            this.e -= (this.g * this.b) / 10;
            this.e = a(this.e);
        }
        if (this.k - this.a > this.h * this.c) {
            this.f = this.a;
        } else {
            this.f -= (this.g * this.c) / 10;
            this.f = b(this.f);
        }
        this.v.setTextSize(((float) this.h) * 0.75f);
        this.l = this.v.getFontMetrics();
        invalidate();
    }

    /* access modifiers changed from: private */
    public void k() {
        this.h = (int) (((float) this.g) * this.i);
        this.a = this.h / 2;
        if (this.i <= 1.0f) {
            this.e = this.a;
            this.f = this.a;
            this.d.b(false);
        } else {
            if (this.j - this.a > this.h * this.b) {
                this.e = this.a;
            } else {
                this.e += (this.g * this.b) / 10;
                this.e = a(this.e);
            }
            if (this.k - this.a > this.h * this.c) {
                this.f = this.a;
            } else {
                this.f += (this.g * this.c) / 10;
                this.f = b(this.f);
            }
        }
        this.v.setTextSize(((float) this.h) * 0.75f);
        this.l = this.v.getFontMetrics();
        invalidate();
    }

    /* access modifiers changed from: private */
    public void l() {
        if (this.i > 1.0f) {
            this.i = 1.0f;
            k();
            return;
        }
        invalidate();
    }

    private void m() {
        for (int i2 = 0; i2 < this.c + 1; i2++) {
            for (int i3 = 0; i3 < this.b + 1; i3++) {
                if (d(this.m[i2][i3])) {
                    this.m[i2][i3] = f(this.m[i2][i3]);
                }
                if (e(this.m[i2][i3])) {
                    this.m[i2][i3] = g(this.m[i2][i3]);
                }
            }
        }
    }

    private int n() {
        int i2 = 0;
        for (int i3 = 0; i3 < this.c + 1; i3++) {
            int i4 = 0;
            while (i4 < this.b + 1) {
                if (i3 < this.c && b(this.m[i3][i4])) {
                    i2++;
                }
                if (i4 < this.b && c(this.m[i3][i4])) {
                    i2++;
                }
                i4++;
                i2 = i2;
            }
        }
        return i2;
    }

    private boolean o() {
        for (int i2 = 0; i2 < this.c; i2++) {
            for (int i3 = 0; i3 < this.b; i3++) {
                int a2 = a(this.m[i2][i3]);
                if (a2 <= 4 && a(i3, i2, a2) != 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public void a() {
        this.d = null;
        this.n = null;
        this.o = null;
        this.m = null;
    }

    public void a(Point point, int i2, int i3) {
        if (!Settings.a(getContext())) {
            m();
        }
        if (this.s >= 1) {
            setPanelColor(this.s);
        }
        this.H = point;
        this.I = i2;
        this.J = i3;
        this.A.x = -1;
        i();
        invalidate();
        this.p = false;
    }

    public void b() {
        this.E = 0;
        this.H = null;
        this.A.x = -1;
        if (!o()) {
            this.E = 1;
            Toast.makeText(this.d, (int) C0003R.string.unmatch_number, 1).show();
        } else if (c(n()) == 1) {
            this.E = 2;
            Toast.makeText(this.d, (int) C0003R.string.loop_multiple, 1).show();
        } else {
            this.E = 3;
            Toast.makeText(this.d, (int) C0003R.string.loop_none, 1).show();
        }
        l();
        invalidate();
    }

    public void c() {
        this.i += 0.2f;
        j();
    }

    public boolean d() {
        if (this.i > 1.0f) {
            this.i -= 0.2f;
            k();
            if (this.i > 1.0f) {
                return true;
            }
        }
        return false;
    }

    public void e() {
        this.H = null;
        this.A.x = -1;
        for (int i2 = 0; i2 < this.c + 1; i2++) {
            for (int i3 = 0; i3 < this.b + 1; i3++) {
                this.m[i2][i3] = (byte) a(this.m[i2][i3]);
            }
        }
        setPanelColor(this.s);
        invalidate();
        this.p = false;
    }

    public boolean f() {
        int n2 = n();
        return n2 >= 4 && o() && c(n2) == 0;
    }

    public void g() {
        if (this.A.x != -1) {
            if ((this.B & 1) > 0) {
                this.B = g(this.A.x, this.A.y);
            } else {
                this.B = h(this.A.x, this.A.y);
            }
            if (this.s >= 1) {
                setPanelColor(this.s);
            }
            invalidate();
        }
    }

    public byte[][] getPuzzleData() {
        return this.m;
    }

    public boolean getSolved() {
        return this.p;
    }

    public String getStrPuzzleData() {
        return v.a(this.m);
    }

    public void h() {
        if ((this.B & 1) > 0) {
            this.B = e(this.A.x, this.A.y);
        } else {
            this.B = f(this.A.x, this.A.y);
        }
        if (this.s >= 1) {
            setPanelColor(this.s);
        }
        invalidate();
    }

    public void onDraw(Canvas canvas) {
        if (this.t == 0) {
            canvas.drawColor(-1);
        } else {
            canvas.drawColor(-16777216);
        }
        a(canvas);
        c(canvas);
        d(canvas);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        Bundle bundle = (Bundle) parcelable;
        this.m = v.a(this.b, this.c, bundle.getString("puzzleData"));
        this.i = bundle.getFloat("zoomState", 1.0f);
        if (this.i > 1.0f) {
            this.o.a(this.i);
            this.d.b(true);
        }
        this.e = bundle.getInt("offsetX", this.a);
        this.f = bundle.getInt("offsetY", this.a);
        super.onRestoreInstanceState(bundle.getParcelable("viewState"));
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putString("puzzleData", v.a(this.m));
        bundle.putFloat("zoomState", this.i);
        bundle.putInt("offsetX", this.e);
        bundle.putInt("offsetY", this.f);
        bundle.putParcelable("viewState", onSaveInstanceState);
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        this.j = i2;
        this.k = i3;
        if (i3 > i2) {
            this.g = i2 / (this.b + 1);
        } else {
            this.g = i3 / (this.c + 1);
        }
        this.h = (int) (((float) this.g) * this.i);
        this.a = this.h / 2;
        if (this.e == 15) {
            this.e = this.a;
        } else if (this.j - this.a > this.h * this.b) {
            this.e = this.a;
        } else {
            this.e = a(this.e);
        }
        if (this.f == 15) {
            this.f = this.a;
        } else if (this.k - this.a > this.h * this.c) {
            this.f = this.a;
        } else {
            this.f = b(this.f);
        }
        if (this.s >= 1) {
            setPanelColor(this.s);
        }
        this.v.setTextSize(((float) this.h) * 0.75f);
        this.l = this.v.getFontMetrics();
        super.onSizeChanged(i2, i3, i4, i5);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.n == null) {
            return false;
        }
        if (this.n.onTouchEvent(motionEvent)) {
            return true;
        }
        this.o.a(motionEvent);
        return true;
    }

    public void setColorPanelAssist(int i2) {
        this.s = i2;
        if (this.s >= 1) {
            setPanelColor(this.s);
        }
        invalidate();
    }

    public void setNumberAssist(boolean z2) {
        this.r = z2;
        invalidate();
    }

    public void setPaintColor(int i2) {
        this.t = i2;
        if (this.t == 0) {
            this.w.setColor(-16777216);
            this.x.setColor(-16777216);
        } else {
            this.w.setColor(-1);
            this.x.setColor(-1);
        }
        this.w.setStrokeWidth(2.0f);
        Resources resources = getResources();
        this.y.setColor(resources.getColor(C0003R.color.outerPanelColor));
        this.z.setColor(resources.getColor(C0003R.color.innerPanelColor));
        invalidate();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kasa0.android.slitherpuzzle.a.a(int, int, int):int
     arg types: [int, int, byte]
     candidates:
      com.kasa0.android.slitherpuzzle.a.a(int, int, byte):int
      com.kasa0.android.slitherpuzzle.a.a(int, int, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kasa0.android.slitherpuzzle.a.b(int, int, int):int
     arg types: [int, int, byte]
     candidates:
      com.kasa0.android.slitherpuzzle.a.b(int, int, byte):int
      com.kasa0.android.slitherpuzzle.a.b(int, int, int):int */
    public void setPanelColor(int i2) {
        int i3;
        if (this.m != null) {
            a.a();
            do {
                i3 = 0;
                for (int i4 = 0; i4 < this.c; i4++) {
                    int i5 = i3;
                    for (int i6 = 0; i6 < this.b; i6++) {
                        i5 += a.a(i6 + 1, i4 + 1, (byte) (this.m[i4][i6] & 48));
                    }
                    i3 = i5;
                    int i7 = this.b;
                    while (i7 >= 0) {
                        int b2 = a.b(i7 + 1, i4 + 1, (byte) (this.m[i4][i7] & 48)) + i3;
                        i7--;
                        i3 = b2;
                    }
                }
                for (int i8 = 0; i8 < this.b; i8++) {
                    int i9 = i3;
                    for (int i10 = 0; i10 < this.c; i10++) {
                        i9 += a.a(i8 + 1, i10 + 1, (int) (this.m[i10][i8] & -64));
                    }
                    int i11 = i9;
                    int i12 = this.c;
                    while (i12 >= 0) {
                        int b3 = a.b(i8 + 1, i12 + 1, (int) (this.m[i12][i8] & -64)) + i3;
                        i12--;
                        i11 = b3;
                    }
                }
                for (int i13 = 1; i13 < this.c; i13++) {
                    int i14 = 1;
                    while (i14 < this.b) {
                        int a2 = a(i14, i13) + i3;
                        i14++;
                        i3 = a2;
                    }
                }
                for (int i15 = 0; i15 < this.c; i15++) {
                    int i16 = 0;
                    while (i16 < this.b) {
                        switch (a(this.m[i15][i16])) {
                            case NendAdIconLayout.HORIZONTAL /*0*/:
                                i3 += a.b(i16 + 1, i15 + 1);
                                break;
                            case 1:
                                if (i2 >= 2) {
                                    i3 += b(i16, i15);
                                }
                            case AdlantisAd.ADTYPE_TEXT /*2*/:
                                if (i2 < 3) {
                                    break;
                                } else {
                                    i3 += c(i16, i15);
                                    break;
                                }
                            case 3:
                                if (i2 < 4) {
                                    break;
                                } else {
                                    i3 += d(i16, i15);
                                    break;
                                }
                        }
                        i16++;
                        i3 = i3;
                    }
                }
            } while (i3 > 0);
        }
    }

    public void setUseCrossMark(boolean z2) {
        this.q = z2;
        if (!this.q) {
            m();
            invalidate();
        }
    }

    public void setZoom(float f2) {
        if (this.o != null && f2 >= 1.0f) {
            this.o.a(f2);
        }
    }
}
