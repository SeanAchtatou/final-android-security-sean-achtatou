package com.kasa0.android.slitherpuzzle;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

public class ac extends AsyncTask {
    private PuzzleList a;
    private n b = null;

    public ac(PuzzleList puzzleList, n nVar) {
        this.a = puzzleList;
        this.b = nVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Object... objArr) {
        SQLiteDatabase sQLiteDatabase = (SQLiteDatabase) objArr[0];
        int intValue = ((Integer) objArr[1]).intValue();
        int intValue2 = ((Integer) objArr[2]).intValue();
        if (intValue <= 1 && intValue2 >= 2) {
            try {
                this.b.a(sQLiteDatabase);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (intValue <= 2 && intValue2 >= 4) {
            this.b.b(sQLiteDatabase);
        }
        if ((intValue <= 4 && intValue2 >= 5) || intValue2 == 7) {
            this.b.d(sQLiteDatabase);
        }
        if (intValue <= 5 && intValue2 >= 6) {
            this.b.c(sQLiteDatabase);
        }
        if (intValue <= 6 && intValue2 >= 7) {
            this.b.e(sQLiteDatabase);
        }
        if (intValue <= 7 && intValue2 >= 8) {
            this.b.f(sQLiteDatabase);
        }
        if (intValue <= 8 && intValue2 >= 9) {
            this.b.g(sQLiteDatabase);
        }
        if (intValue <= 9 && intValue2 >= 10) {
            this.b.h(sQLiteDatabase);
        }
        if (intValue <= 10 && intValue2 >= 11) {
            this.b.i(sQLiteDatabase);
        }
        if (intValue <= 11 && intValue2 >= 12) {
            this.b.j(sQLiteDatabase);
        }
        if (intValue <= 12 && intValue2 >= 13) {
            this.b.k(sQLiteDatabase);
        }
        if (intValue <= 13 && intValue2 >= 14) {
            this.b.l(sQLiteDatabase);
        }
        if (intValue <= 14 && intValue2 >= 15) {
            this.b.m(sQLiteDatabase);
        }
        if (intValue <= 15 && intValue2 >= 16) {
            this.b.n(sQLiteDatabase);
        }
        if (intValue <= 16 && intValue2 >= 17) {
            this.b.o(sQLiteDatabase);
        }
        if (intValue <= 18 && intValue2 >= 19) {
            this.b.p(sQLiteDatabase);
            this.b.d(sQLiteDatabase);
        }
        if (intValue <= 19 && intValue2 >= 20) {
            this.b.q(sQLiteDatabase);
        }
        if (intValue <= 20 && intValue2 >= 21) {
            this.b.r(sQLiteDatabase);
        }
        if (intValue <= 21 && intValue2 >= 22) {
            this.b.s(sQLiteDatabase);
        }
        if (intValue <= 22 && intValue2 >= 23) {
            this.b.t(sQLiteDatabase);
        }
        if (intValue <= 23 && intValue2 >= 24) {
            this.b.u(sQLiteDatabase);
        }
        if (intValue <= 24 && intValue2 >= 25) {
            this.b.v(sQLiteDatabase);
        }
        if (intValue <= 25 && intValue2 >= 26) {
            this.b.w(sQLiteDatabase);
        }
        if (intValue <= 26 && intValue2 >= 27) {
            this.b.x(sQLiteDatabase);
        }
        if (intValue <= 27 && intValue2 >= 28) {
            this.b.y(sQLiteDatabase);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        if (this.a != null) {
            this.a.a(false);
            this.a.a();
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.a != null) {
            this.a.a(true);
        }
    }
}
