package com.kasa0.android.slitherpuzzle;

import android.database.Cursor;
import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import jp.adlantis.android.AdlantisAd;
import net.nend.android.NendAdIconLayout;

class u implements SimpleCursorAdapter.ViewBinder {
    final /* synthetic */ t a;
    private long b = 0;

    public u(t tVar, long j) {
        this.a = tVar;
        this.b = j;
    }

    public boolean setViewValue(View view, Cursor cursor, int i) {
        if (cursor.getColumnIndex("no") == i) {
            long j = (long) cursor.getInt(i);
            TextView textView = (TextView) view;
            if (j == this.b) {
                textView.setTypeface(Typeface.create(Typeface.SERIF, 3));
            } else {
                textView.setTypeface(Typeface.DEFAULT);
            }
            textView.setText(new StringBuilder().append(j).toString());
            return true;
        }
        if (cursor.getColumnIndex("firstest") == i) {
            long j2 = cursor.getLong(i) / 1000;
            if (j2 > 0) {
                String str = "";
                if (j2 >= 3600) {
                    long j3 = j2 / 3600;
                    j2 %= 3600;
                    str = String.valueOf(String.valueOf(j3)) + ":";
                }
                ((TextView) view).setText(String.valueOf(str) + String.format("%1$02d:%2$02d", Long.valueOf(j2 / 60), Long.valueOf(j2 % 60)));
                return true;
            }
        } else if (cursor.getColumnIndex("solved") == i) {
            ImageView imageView = (ImageView) view;
            switch (cursor.getInt(i)) {
                case NendAdIconLayout.HORIZONTAL /*0*/:
                    imageView.setImageResource(C0003R.drawable.not_solved);
                    break;
                case 1:
                    imageView.setImageResource(C0003R.drawable.solved);
                    break;
                case AdlantisAd.ADTYPE_TEXT /*2*/:
                    imageView.setImageResource(C0003R.drawable.tiara);
                    break;
                case 3:
                    imageView.setImageResource(C0003R.drawable.crown);
                    break;
            }
            return true;
        }
        return false;
    }
}
