package com.kasa0.android.slitherpuzzle;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class Help extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0003R.layout.help);
        ((TextView) findViewById(C0003R.id.help_text)).setText(Html.fromHtml(getString(C0003R.string.help_text), new f(this), null));
        getWindow().addFlags(128);
        setResult(-1);
    }
}
