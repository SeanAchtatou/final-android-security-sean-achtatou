package com.kasa0.android.slitherpuzzle;

import android.content.DialogInterface;
import android.os.SystemClock;

class j implements DialogInterface.OnClickListener {
    final /* synthetic */ PuzzleControl a;

    j(PuzzleControl puzzleControl) {
        this.a = puzzleControl;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.l();
        this.a.a((Boolean) true);
        this.a.q.setBase(SystemClock.elapsedRealtime() - this.a.r);
        this.a.q.start();
        this.a.m = 0;
    }
}
