package com.kasa0.android.slitherpuzzle;

import android.preference.ListPreference;
import android.preference.Preference;

class aa implements Preference.OnPreferenceChangeListener {
    final /* synthetic */ Settings a;

    aa(Settings settings) {
        this.a = settings;
    }

    public boolean onPreferenceChange(Preference preference, Object obj) {
        this.a.b((ListPreference) preference, obj.toString());
        return true;
    }
}
