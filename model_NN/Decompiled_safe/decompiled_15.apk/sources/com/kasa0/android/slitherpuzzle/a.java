package com.kasa0.android.slitherpuzzle;

import java.lang.reflect.Array;

public class a {
    private static int a;
    private static int b;
    private static byte[][] c;

    public static int a(int i, int i2, byte b2) {
        if (c[i2][i] != 0) {
            return 0;
        }
        if (b2 == 16) {
            if (c[i2][i - 1] == 1) {
                c[i2][i] = 2;
                return 1;
            } else if (c[i2][i - 1] != 2) {
                return 0;
            } else {
                c[i2][i] = 1;
                return 1;
            }
        } else if (b2 != 32) {
            return 0;
        } else {
            if (c[i2][i - 1] == 1) {
                c[i2][i] = 1;
                return 1;
            } else if (c[i2][i - 1] != 2) {
                return 0;
            } else {
                c[i2][i] = 2;
                return 1;
            }
        }
    }

    public static int a(int i, int i2, int i3) {
        if (c[i2][i] != 0) {
            return 0;
        }
        if (i3 == 64) {
            if (c[i2 - 1][i] == 1) {
                c[i2][i] = 2;
                return 1;
            } else if (c[i2 - 1][i] != 2) {
                return 0;
            } else {
                c[i2][i] = 1;
                return 1;
            }
        } else if (i3 != -128) {
            return 0;
        } else {
            if (c[i2 - 1][i] == 1) {
                c[i2][i] = 1;
                return 1;
            } else if (c[i2 - 1][i] != 2) {
                return 0;
            } else {
                c[i2][i] = 2;
                return 1;
            }
        }
    }

    public static void a() {
        for (int i = 0; i < a + 2; i++) {
            c[0][i] = 1;
            c[b + 1][i] = 1;
        }
        for (int i2 = 1; i2 < b + 1; i2++) {
            c[i2][0] = 1;
            c[i2][a + 1] = 1;
            for (int i3 = 1; i3 < a + 1; i3++) {
                c[i2][i3] = 0;
            }
        }
    }

    public static void a(int i, int i2) {
        a = i;
        b = i2;
        c = (byte[][]) Array.newInstance(Byte.TYPE, i2 + 2, i + 2);
    }

    public static int b(int i, int i2) {
        byte b2 = (byte) (c[i2][i] | c[i2 - 1][i] | c[i2][i - 1] | c[i2 + 1][i] | c[i2][i + 1]);
        if (b2 > 0) {
            int i3 = c[i2][i] > 0 ? 1 : 0;
            if (c[i2 - 1][i] > 0) {
                i3++;
            }
            if (c[i2][i - 1] > 0) {
                i3++;
            }
            if (c[i2 + 1][i] > 0) {
                i3++;
            }
            if (c[i2][i + 1] > 0) {
                i3++;
            }
            if (i3 < 5) {
                c[i2][i] = b2;
                c[i2 - 1][i] = b2;
                c[i2][i - 1] = b2;
                c[i2 + 1][i] = b2;
                c[i2][i + 1] = b2;
                return 1;
            }
        }
        return 0;
    }

    public static int b(int i, int i2, byte b2) {
        if (c[i2][i - 1] != 0) {
            return 0;
        }
        if (b2 == 16) {
            if (c[i2][i] == 1) {
                c[i2][i - 1] = 2;
                return 1;
            } else if (c[i2][i] != 2) {
                return 0;
            } else {
                c[i2][i - 1] = 1;
                return 1;
            }
        } else if (b2 != 32) {
            return 0;
        } else {
            if (c[i2][i] == 1) {
                c[i2][i - 1] = 1;
                return 1;
            } else if (c[i2][i] != 2) {
                return 0;
            } else {
                c[i2][i - 1] = 2;
                return 1;
            }
        }
    }

    public static int b(int i, int i2, int i3) {
        if (c[i2 - 1][i] != 0) {
            return 0;
        }
        if (i3 == 64) {
            if (c[i2][i] == 1) {
                c[i2 - 1][i] = 2;
                return 1;
            } else if (c[i2][i] != 2) {
                return 0;
            } else {
                c[i2 - 1][i] = 1;
                return 1;
            }
        } else if (i3 != -128) {
            return 0;
        } else {
            if (c[i2][i] == 1) {
                c[i2 - 1][i] = 1;
                return 1;
            } else if (c[i2][i] != 2) {
                return 0;
            } else {
                c[i2 - 1][i] = 2;
                return 1;
            }
        }
    }

    public static byte c(int i, int i2) {
        return c[i2][i];
    }

    public static int c(int i, int i2, byte b2) {
        if (c[i2][i] != 0 || b2 == 0) {
            return 0;
        }
        c[i2][i] = b2;
        return 1;
    }
}
