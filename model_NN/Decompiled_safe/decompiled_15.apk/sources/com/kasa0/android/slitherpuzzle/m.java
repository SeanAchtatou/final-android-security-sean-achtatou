package com.kasa0.android.slitherpuzzle;

import android.content.SharedPreferences;
import android.graphics.Point;
import android.util.Log;
import android.widget.ImageView;
import java.util.Arrays;
import jp.adlantis.android.AdlantisAd;
import net.nend.android.NendAdIconLayout;

public class m {
    private static byte[][][] a = new byte[3][][];
    private static byte[][] b;
    private static int c = 0;
    private static int d = 0;
    private static int e = 0;
    private static boolean f = false;
    private ImageView g;
    private ImageView h;
    private ImageView i;
    private ImageView j;
    private ImageView k;
    private boolean l = false;
    private int m = 0;
    private Point[] n = new Point[3];
    private int[] o = new int[3];
    private int[] p = new int[3];

    private void a(int i2, byte[][] bArr) {
        int length = bArr.length;
        a[i2] = new byte[length][];
        for (int i3 = 0; i3 < length; i3++) {
            a[i2][i3] = new byte[bArr[i3].length];
            System.arraycopy(bArr[i3], 0, a[i2][i3], 0, bArr[i3].length);
        }
    }

    private boolean e(byte[][] bArr) {
        if (c == 0) {
            return false;
        }
        int i2 = e - 1;
        if (i2 < 0) {
            i2 = 2;
        }
        return Arrays.deepEquals(a[i2], bArr);
    }

    private void i() {
        this.n[j()] = null;
    }

    private int j() {
        int i2 = e - 1;
        if (i2 < 0) {
            return 2;
        }
        return i2;
    }

    public int a(SharedPreferences sharedPreferences, int i2, int i3) {
        c = sharedPreferences.getInt("puzzleTempCnt", 0);
        for (int i4 = 0; i4 < c; i4++) {
            a(i4, v.a(i2, i3, sharedPreferences.getString("puzzleTemp" + (c - (i4 + 1)), null)));
            int i5 = sharedPreferences.getInt("firstPntX" + (c - (i4 + 1)), -1);
            if (i5 != -1) {
                this.n[i4] = new Point();
                this.n[i4].x = i5;
                this.n[i4].y = sharedPreferences.getInt("firstPntY" + (c - (i4 + 1)), -1);
                this.o[i4] = sharedPreferences.getInt("firstOrient" + (c - (i4 + 1)), 0);
                this.p[i4] = sharedPreferences.getInt("firstMark" + (c - (i4 + 1)), 1);
            }
        }
        e = c;
        d = c;
        if (e == 3) {
            e = 0;
        }
        return c;
    }

    public void a() {
        e = 0;
        c = 0;
        d = 0;
        f = false;
        for (int i2 = 0; i2 < 3; i2++) {
            if (a[i2] != null) {
                int length = a[i2].length;
                for (int i3 = 0; i3 < length; i3++) {
                    a[i2][i3] = null;
                }
                a[i2] = null;
            }
        }
        if (b != null) {
            int length2 = b.length;
            for (int i4 = 0; i4 < length2; i4++) {
                b[i4] = null;
            }
            b = null;
        }
    }

    public void a(int i2) {
        this.g.setVisibility(8);
        this.h.setVisibility(8);
        this.i.setVisibility(8);
        this.j.setVisibility(8);
        this.k.setVisibility(8);
        switch (i2) {
            case NendAdIconLayout.HORIZONTAL /*0*/:
            case 1:
                this.i.setVisibility(0);
                return;
            case AdlantisAd.ADTYPE_TEXT /*2*/:
                this.j.setVisibility(0);
                return;
            case 3:
                this.k.setVisibility(0);
                return;
            default:
                return;
        }
    }

    public void a(int i2, int i3, int i4, int i5) {
        int j2 = j();
        if (this.m == 1) {
            if (this.n[j2] == null) {
                this.n[j2] = new Point(i2, i3);
            } else {
                this.n[j2].x = i2;
                this.n[j2].y = i3;
            }
            this.o[j2] = i4;
            this.p[j2] = i5;
            this.m = 2;
        } else if (this.m != 2) {
        } else {
            if (this.n[j2].x == i2 && this.n[j2].y == i3 && this.o[j2] == i4) {
                this.p[j2] = i5;
            } else {
                this.m = 0;
            }
        }
    }

    public void a(SharedPreferences.Editor editor) {
        if (editor != null) {
            editor.putInt("puzzleTempCnt", 0);
        }
    }

    public void a(PuzzleControl puzzleControl) {
        if (puzzleControl == null) {
            Log.e("SlithierPuzzle:PuzzleDataStack", "control is Null");
            return;
        }
        this.g = (ImageView) puzzleControl.findViewById(C0003R.id.stack_image0_disable);
        this.h = (ImageView) puzzleControl.findViewById(C0003R.id.stack_image0);
        this.i = (ImageView) puzzleControl.findViewById(C0003R.id.stack_image1);
        this.j = (ImageView) puzzleControl.findViewById(C0003R.id.stack_image2);
        this.k = (ImageView) puzzleControl.findViewById(C0003R.id.stack_image3);
    }

    public boolean a(byte[][] bArr) {
        if (e(bArr)) {
            return false;
        }
        if (d > 0 && c == 0) {
            c++;
            e++;
            if (e == 3) {
                e = 0;
            }
        }
        int length = bArr.length;
        if (a[e] == null) {
            a[e] = new byte[length][];
        }
        for (int i2 = 0; i2 < length; i2++) {
            if (a[e][i2] == null) {
                a[e][i2] = new byte[bArr[i2].length];
            }
            System.arraycopy(bArr[i2], 0, a[e][i2], 0, bArr[i2].length);
        }
        e++;
        if (e == 3) {
            e = 0;
        }
        if (c < 3) {
            c++;
            if (c > d) {
                d = c;
            }
        }
        a(c);
        this.m = 1;
        i();
        return true;
    }

    public int b(byte[][] bArr) {
        int i2;
        int i3 = 2;
        this.m = 1;
        int i4 = c;
        int i5 = e;
        if (f) {
            if (c <= 0 || i5 - 1 >= 0) {
                i3 = i5;
            }
            f = false;
            i2 = i4;
        } else {
            if (c > 1) {
                c--;
                e--;
                if (e < 0) {
                    e = 2;
                }
            }
            int i6 = c;
            int i7 = e - 1;
            if (i7 < 0) {
                i2 = i6;
            } else {
                i3 = i7;
                i2 = i6;
            }
        }
        if (a[i3] == null) {
            return 0;
        }
        int length = a[i3].length;
        for (int i8 = 0; i8 < length; i8++) {
            System.arraycopy(a[i3][i8], 0, bArr[i8], 0, a[i3][i8].length);
        }
        a(i2);
        return i2 + 1;
    }

    public void b() {
        this.g.setVisibility(8);
        this.h.setVisibility(0);
        this.i.setVisibility(8);
        this.j.setVisibility(8);
        this.k.setVisibility(8);
    }

    public void b(SharedPreferences.Editor editor) {
        if (editor != null) {
            if (c != 0 || a[e] == null) {
                editor.putInt("puzzleTempCnt", c);
                int i2 = e;
                for (int i3 = 0; i3 < c; i3++) {
                    i2--;
                    if (i2 < 0) {
                        i2 = 2;
                    }
                    editor.putString("puzzleTemp" + i3, v.a(a[i2]));
                    if (this.n[i2] != null) {
                        editor.putInt("firstPntX" + i3, this.n[i2].x);
                        editor.putInt("firstPntY" + i3, this.n[i2].y);
                        editor.putInt("firstOrient" + i3, this.o[i2]);
                        editor.putInt("firstMark" + i3, this.p[i2]);
                    }
                }
                return;
            }
            editor.putInt("puzzleTempCnt", 1);
            editor.putString("puzzleTemp0", v.a(a[e]));
            if (this.n[e] != null) {
                editor.putInt("firstPntX0", this.n[e].x);
                editor.putInt("firstPntY0", this.n[e].y);
                editor.putInt("firstOrient0", this.o[e]);
                editor.putInt("firstMark0", this.p[e]);
            }
        }
    }

    public void c() {
        if (this.h.getVisibility() == 0) {
            this.g.setVisibility(0);
            this.h.setVisibility(8);
        }
    }

    public void c(byte[][] bArr) {
        int length = bArr.length;
        if (b == null) {
            b = new byte[length][];
        }
        for (int i2 = 0; i2 < length; i2++) {
            if (b[i2] == null) {
                b[i2] = new byte[bArr[i2].length];
            }
            System.arraycopy(bArr[i2], 0, b[i2], 0, bArr[i2].length);
        }
        this.l = f;
    }

    public void d() {
        c = 1;
        a(c);
    }

    public void d(byte[][] bArr) {
        int length = b.length;
        for (int i2 = 0; i2 < length; i2++) {
            System.arraycopy(b[i2], 0, bArr[i2], 0, b[i2].length);
        }
        if (!this.l) {
            e++;
            if (e == 3) {
                e = 0;
            }
            if (c < 3) {
                c++;
                if (c > d) {
                    d = c;
                }
            }
        }
        f = this.l;
        a(c);
    }

    public Point e() {
        return this.n[j()];
    }

    public int f() {
        return this.o[j()];
    }

    public int g() {
        return this.p[j()];
    }

    public void h() {
        f = true;
    }
}
