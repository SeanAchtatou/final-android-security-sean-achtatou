package com.kasa0.android.slitherpuzzle;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

public class s extends AsyncTask {
    protected boolean a = false;
    final /* synthetic */ PuzzleList b;

    public s(PuzzleList puzzleList) {
        this.b = puzzleList;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(SQLiteDatabase... sQLiteDatabaseArr) {
        this.b.d.a(sQLiteDatabaseArr[0], PuzzleList.j);
        this.b.d.a(sQLiteDatabaseArr[0]);
        publishProgress(80);
        this.b.d.b(sQLiteDatabaseArr[0]);
        publishProgress(85);
        this.b.d.c(sQLiteDatabaseArr[0]);
        publishProgress(90);
        this.b.d.e(sQLiteDatabaseArr[0]);
        publishProgress(100);
        this.b.d.f(sQLiteDatabaseArr[0]);
        publishProgress(110);
        this.b.d.g(sQLiteDatabaseArr[0]);
        publishProgress(120);
        this.b.d.h(sQLiteDatabaseArr[0]);
        publishProgress(130);
        this.b.d.i(sQLiteDatabaseArr[0]);
        publishProgress(140);
        this.b.d.j(sQLiteDatabaseArr[0]);
        publishProgress(150);
        this.b.d.k(sQLiteDatabaseArr[0]);
        publishProgress(160);
        this.b.d.l(sQLiteDatabaseArr[0]);
        publishProgress(170);
        this.b.d.m(sQLiteDatabaseArr[0]);
        publishProgress(180);
        this.b.d.n(sQLiteDatabaseArr[0]);
        publishProgress(190);
        this.b.d.o(sQLiteDatabaseArr[0]);
        publishProgress(200);
        this.b.d.p(sQLiteDatabaseArr[0]);
        publishProgress(210);
        this.b.d.q(sQLiteDatabaseArr[0]);
        publishProgress(220);
        this.b.d.r(sQLiteDatabaseArr[0]);
        publishProgress(230);
        this.b.d.s(sQLiteDatabaseArr[0]);
        publishProgress(240);
        this.b.d.t(sQLiteDatabaseArr[0]);
        publishProgress(250);
        this.b.d.u(sQLiteDatabaseArr[0]);
        publishProgress(260);
        this.b.d.v(sQLiteDatabaseArr[0]);
        publishProgress(270);
        this.b.d.w(sQLiteDatabaseArr[0]);
        publishProgress(280);
        this.b.d.x(sQLiteDatabaseArr[0]);
        publishProgress(290);
        this.b.d.y(sQLiteDatabaseArr[0]);
        publishProgress(300);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kasa0.android.slitherpuzzle.PuzzleList.a(com.kasa0.android.slitherpuzzle.PuzzleList, int, boolean):void
     arg types: [com.kasa0.android.slitherpuzzle.PuzzleList, int, int]
     candidates:
      com.kasa0.android.slitherpuzzle.PuzzleList.a(android.database.sqlite.SQLiteDatabase, int, int):void
      com.kasa0.android.slitherpuzzle.PuzzleList.a(com.kasa0.android.slitherpuzzle.PuzzleList, int, boolean):void */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        this.b.f();
        this.b.a(this.b.g, false);
        this.a = false;
        PuzzleList.j = (s) null;
    }

    public void a(Integer num) {
        publishProgress(num);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        this.b.a(numArr[0]);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.a = true;
        this.b.e();
    }
}
