package com.kasa0.android.slitherpuzzle;

import android.preference.ListPreference;
import android.preference.Preference;

class z implements Preference.OnPreferenceChangeListener {
    final /* synthetic */ Settings a;

    z(Settings settings) {
        this.a = settings;
    }

    public boolean onPreferenceChange(Preference preference, Object obj) {
        this.a.a((ListPreference) preference, obj.toString());
        return true;
    }
}
