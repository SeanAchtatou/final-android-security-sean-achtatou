package com.kasa0.android.slitherpuzzle;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.LinearLayout;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.customevent.CustomEventBanner;
import com.google.ads.mediation.customevent.CustomEventBannerListener;
import jp.adlantis.android.AdlantisView;
import net.nend.android.NendAdIconLayout;

public class MyAd implements CustomEventBanner {
    public void destroy() {
    }

    public void requestBannerAd(CustomEventBannerListener customEventBannerListener, Activity activity, String str, String str2, AdSize adSize, MediationAdRequest mediationAdRequest, Object obj) {
        Log.v("label", str);
        Log.v("serverParameter", str2);
        Display defaultDisplay = ((WindowManager) activity.getSystemService("window")).getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        if (str.equals("Adlantis")) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(defaultDisplay.getWidth(), (int) (displayMetrics.density * 50.0f));
            layoutParams.gravity = 1;
            AdlantisView adlantisView = new AdlantisView(activity);
            adlantisView.setLayoutParams(layoutParams);
            adlantisView.setPublisherID("MjMxMzI%3D%0A");
            customEventBannerListener.onReceivedAd(adlantisView);
        } else if (str.equals("Nend")) {
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(defaultDisplay.getWidth(), (int) (displayMetrics.density * 60.0f));
            layoutParams2.gravity = 1;
            NendAdIconLayout nendAdIconLayout = new NendAdIconLayout(activity, 108401, "8b3615572b75c7b54fb5bce1e2e12c6f0e16e6cd", 4);
            nendAdIconLayout.setLayoutParams(layoutParams2);
            customEventBannerListener.onReceivedAd(nendAdIconLayout);
            nendAdIconLayout.loadAd();
        }
    }
}
