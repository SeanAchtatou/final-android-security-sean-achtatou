package com.kasa0.android.slitherpuzzle;

import android.graphics.drawable.Drawable;
import android.text.Html;

class f implements Html.ImageGetter {
    final /* synthetic */ Help a;

    f(Help help) {
        this.a = help;
    }

    public Drawable getDrawable(String str) {
        Drawable drawable = this.a.getResources().getDrawable(this.a.getResources().getIdentifier(str, "drawable", this.a.getPackageName()));
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        return drawable;
    }
}
