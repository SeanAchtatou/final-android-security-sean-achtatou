package com.kasa0.android.slitherpuzzle;

public class g {
    public static String a(int i, int i2, String str) {
        StringBuffer stringBuffer = new StringBuffer(1000);
        int length = str.length();
        for (int i3 = 0; i3 < length; i3++) {
            char charAt = str.charAt(i3);
            if (charAt >= '0' && charAt <= '3') {
                stringBuffer.append(charAt);
            } else if (charAt >= '5' && charAt <= '8') {
                stringBuffer.append(String.valueOf(charAt - '5') + " ");
            } else if (charAt >= 'a' && charAt <= 'd') {
                stringBuffer.append(String.valueOf(charAt - 'a') + "  ");
            } else if (charAt >= 'g') {
                int i4 = charAt - 'f';
                for (int i5 = 0; i5 < i4; i5++) {
                    stringBuffer.append(" ");
                }
            }
        }
        if (i * i2 > stringBuffer.length()) {
            return null;
        }
        return v.b(i, i2, stringBuffer.toString());
    }

    public static String a(int i, int i2, String[] strArr) {
        StringBuffer stringBuffer = new StringBuffer(1000);
        int length = strArr.length;
        if (i2 > length - 2) {
            return null;
        }
        for (int i3 = 2; i3 < length; i3++) {
            int length2 = strArr[i3].length();
            if (i * 2 > length2) {
                return null;
            }
            for (int i4 = 0; i4 < length2; i4 += 2) {
                char charAt = strArr[i3].charAt(i4);
                if (charAt == '.') {
                    stringBuffer.append(' ');
                } else {
                    stringBuffer.append(charAt);
                }
            }
        }
        return v.b(i, i2, stringBuffer.toString());
    }
}
