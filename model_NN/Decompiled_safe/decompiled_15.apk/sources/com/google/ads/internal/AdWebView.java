package com.google.ads.internal;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.ads.AdActivity;
import com.google.ads.AdSize;
import com.google.ads.ak;
import com.google.ads.n;
import com.google.ads.util.AdUtil;
import com.google.ads.util.IcsUtil;
import com.google.ads.util.b;
import com.google.ads.util.g;
import com.google.ads.util.h;
import java.lang.ref.WeakReference;

public class AdWebView extends WebView {
    protected final n a;
    private WeakReference b = null;
    private AdSize c;
    private boolean d = false;
    private boolean e = false;
    private boolean f = false;

    public AdWebView(n nVar, AdSize adSize) {
        super((Context) nVar.f.a());
        this.a = nVar;
        this.c = adSize;
        setBackgroundColor(0);
        AdUtil.a(this);
        WebSettings settings = getSettings();
        settings.setSupportMultipleWindows(false);
        settings.setJavaScriptEnabled(true);
        settings.setSavePassword(false);
        setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
                try {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.parse(str), str4);
                    AdActivity i = AdWebView.this.i();
                    if (i != null && AdUtil.a(intent, i)) {
                        i.startActivity(intent);
                    }
                } catch (ActivityNotFoundException e) {
                    b.a("Couldn't find an Activity to view url/mimetype: " + str + " / " + str4);
                } catch (Throwable th) {
                    b.b("Unknown error trying to start activity to view URL: " + str, th);
                }
            }
        });
        if (AdUtil.a >= 17) {
            h.a(settings, nVar);
        } else if (AdUtil.a >= 11) {
            g.a(settings, nVar);
        }
        setScrollBarStyle(33554432);
        if (AdUtil.a >= 14) {
            setWebChromeClient(new IcsUtil.a(nVar));
        } else if (AdUtil.a >= 11) {
            setWebChromeClient(new g.a(nVar));
        }
    }

    public void a(boolean z) {
        if (z) {
            setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return motionEvent.getAction() == 2;
                }
            });
        } else {
            setOnTouchListener(null);
        }
    }

    public void destroy() {
        try {
            super.destroy();
        } catch (Throwable th) {
            b.d("An error occurred while destroying an AdWebView:", th);
        }
        try {
            setWebViewClient(new WebViewClient());
        } catch (Throwable th2) {
        }
    }

    public void f() {
        AdActivity i = i();
        if (i != null) {
            i.finish();
        }
    }

    public void g() {
        if (AdUtil.a >= 11) {
            g.a(this);
        }
        this.e = true;
    }

    public void h() {
        if (this.e && AdUtil.a >= 11) {
            g.b(this);
        }
        this.e = false;
    }

    public AdActivity i() {
        if (this.b != null) {
            return (AdActivity) this.b.get();
        }
        return null;
    }

    public boolean j() {
        return this.f;
    }

    public boolean k() {
        return this.e;
    }

    public void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        try {
            super.loadDataWithBaseURL(str, str2, str3, str4, str5);
        } catch (Throwable th) {
            b.d("An error occurred while loading data in AdWebView:", th);
        }
    }

    public void loadUrl(String str) {
        try {
            super.loadUrl(str);
        } catch (Throwable th) {
            b.d("An error occurred while loading a URL in AdWebView:", th);
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void onMeasure(int i, int i2) {
        int i3 = Integer.MAX_VALUE;
        synchronized (this) {
            if (isInEditMode()) {
                super.onMeasure(i, i2);
            } else if (this.c == null || this.d) {
                super.onMeasure(i, i2);
            } else {
                int mode = View.MeasureSpec.getMode(i);
                int size = View.MeasureSpec.getSize(i);
                int mode2 = View.MeasureSpec.getMode(i2);
                int size2 = View.MeasureSpec.getSize(i2);
                float f2 = getContext().getResources().getDisplayMetrics().density;
                int width = (int) (((float) this.c.getWidth()) * f2);
                int height = (int) (((float) this.c.getHeight()) * f2);
                int i4 = (mode == Integer.MIN_VALUE || mode == 1073741824) ? size : Integer.MAX_VALUE;
                if (mode2 == Integer.MIN_VALUE || mode2 == 1073741824) {
                    i3 = size2;
                }
                if (((float) width) - (f2 * 6.0f) > ((float) i4) || height > i3) {
                    b.b("Not enough space to show ad! Wants: <" + width + ", " + height + ">, Has: <" + size + ", " + size2 + ">");
                    setVisibility(8);
                    setMeasuredDimension(size, size2);
                } else {
                    setMeasuredDimension(width, height);
                }
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        ak akVar = (ak) this.a.r.a();
        if (akVar != null) {
            akVar.a(motionEvent);
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setAdActivity(AdActivity adActivity) {
        this.b = new WeakReference(adActivity);
    }

    public synchronized void setAdSize(AdSize adSize) {
        this.c = adSize;
        requestLayout();
    }

    public void setCustomClose(boolean z) {
        AdActivity adActivity;
        this.f = z;
        if (this.b != null && (adActivity = (AdActivity) this.b.get()) != null) {
            adActivity.setCustomClose(z);
        }
    }

    public void setIsExpandedMraid(boolean z) {
        this.d = z;
    }

    public void stopLoading() {
        try {
            super.stopLoading();
        } catch (Throwable th) {
            b.d("An error occurred while stopping loading in AdWebView:", th);
        }
    }
}
