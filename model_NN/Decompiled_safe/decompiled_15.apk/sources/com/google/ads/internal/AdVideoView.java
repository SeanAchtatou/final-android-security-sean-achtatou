package com.google.ads.internal;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.VideoView;
import com.google.ads.m;
import com.google.ads.util.b;
import java.lang.ref.WeakReference;

public class AdVideoView extends FrameLayout implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private static final a b = ((a) a.a.b());
    public MediaController a = null;
    private final WeakReference c;
    private final AdWebView d;
    private long e = 0;
    private final VideoView f;
    private String g = null;

    class a implements Runnable {
        private final WeakReference a;

        public a(AdVideoView adVideoView) {
            this.a = new WeakReference(adVideoView);
        }

        public void a() {
            ((Handler) m.a().c.a()).postDelayed(this, 250);
        }

        public void run() {
            AdVideoView adVideoView = (AdVideoView) this.a.get();
            if (adVideoView == null) {
                b.d("The video must be gone, so cancelling the timeupdate task.");
                return;
            }
            adVideoView.f();
            ((Handler) m.a().c.a()).postDelayed(this, 250);
        }
    }

    public AdVideoView(Activity activity, AdWebView adWebView) {
        super(activity);
        this.c = new WeakReference(activity);
        this.d = adWebView;
        this.f = new VideoView(activity);
        addView(this.f, new FrameLayout.LayoutParams(-1, -1, 17));
        a();
        this.f.setOnCompletionListener(this);
        this.f.setOnPreparedListener(this);
        this.f.setOnErrorListener(this);
    }

    /* access modifiers changed from: protected */
    public void a() {
        new a(this).a();
    }

    public void a(int i) {
        this.f.seekTo(i);
    }

    public void a(MotionEvent motionEvent) {
        this.f.onTouchEvent(motionEvent);
    }

    public void b() {
        if (!TextUtils.isEmpty(this.g)) {
            this.f.setVideoPath(this.g);
        } else {
            b.a(this.d, "onVideoEvent", "{'event': 'error', 'what': 'no_src'}");
        }
    }

    public void c() {
        this.f.pause();
    }

    public void d() {
        this.f.start();
    }

    public void e() {
        this.f.stopPlayback();
    }

    /* access modifiers changed from: package-private */
    public void f() {
        long currentPosition = (long) this.f.getCurrentPosition();
        if (this.e != currentPosition) {
            b.a(this.d, "onVideoEvent", "{'event': 'timeupdate', 'time': " + (((float) currentPosition) / 1000.0f) + "}");
            this.e = currentPosition;
        }
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        b.a(this.d, "onVideoEvent", "{'event': 'ended'}");
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        b.e("Video threw error! <what:" + i + ", extra:" + i2 + ">");
        b.a(this.d, "onVideoEvent", "{'event': 'error', 'what': '" + i + "', 'extra': '" + i2 + "'}");
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        b.a(this.d, "onVideoEvent", "{'event': 'canplaythrough', 'duration': '" + (((float) this.f.getDuration()) / 1000.0f) + "'}");
    }

    public void setMediaControllerEnabled(boolean z) {
        Activity activity = (Activity) this.c.get();
        if (activity == null) {
            b.e("adActivity was null while trying to enable controls on a video.");
        } else if (z) {
            if (this.a == null) {
                this.a = new MediaController(activity);
            }
            this.f.setMediaController(this.a);
        } else {
            if (this.a != null) {
                this.a.hide();
            }
            this.f.setMediaController(null);
        }
    }

    public void setSrc(String str) {
        this.g = str;
    }
}
