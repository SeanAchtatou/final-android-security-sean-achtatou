package com.google.ads.internal;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.ads.AdRequest;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;
import com.google.ads.util.g;
import java.util.Map;

public class i extends WebViewClient {
    private static final a c = ((a) a.a.b());
    protected d a;
    protected boolean b = false;
    private final Map d;
    private final boolean e;
    private boolean f;
    private boolean g;
    private boolean h = false;
    private boolean i = false;

    public i(d dVar, Map map, boolean z, boolean z2) {
        this.a = dVar;
        this.d = map;
        this.e = z;
        this.g = z2;
    }

    public static i a(d dVar, Map map, boolean z, boolean z2) {
        return AdUtil.a >= 11 ? new g.b(dVar, map, z, z2) : new i(dVar, map, z, z2);
    }

    public void a(boolean z) {
        this.b = z;
    }

    public boolean a() {
        return this.f;
    }

    public void b(boolean z) {
        this.g = z;
    }

    public void c(boolean z) {
        this.h = z;
    }

    public void d(boolean z) {
        this.i = z;
    }

    public void onPageFinished(WebView webView, String str) {
        this.f = false;
        if (this.h) {
            c k = this.a.k();
            if (k != null) {
                k.c();
            } else {
                b.a("adLoader was null while trying to setFinishedLoadingHtml().");
            }
            this.h = false;
        }
        if (this.i) {
            c.a(webView);
            this.i = false;
        }
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        this.f = true;
    }

    public void onReceivedError(WebView webView, int i2, String str, String str2) {
        this.f = false;
        c k = this.a.k();
        if (k != null) {
            k.a(AdRequest.ErrorCode.NETWORK_ERROR);
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean shouldOverrideUrlLoading(android.webkit.WebView r6, java.lang.String r7) {
        /*
            r5 = this;
            r3 = 1
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00cf }
            r0.<init>()     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r1 = "shouldOverrideUrlLoading(\""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00cf }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r1 = "\")"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00cf }
            com.google.ads.util.b.a(r0)     // Catch:{ Throwable -> 0x00cf }
            android.net.Uri r2 = android.net.Uri.parse(r7)     // Catch:{ Throwable -> 0x00cf }
            com.google.ads.internal.a r0 = com.google.ads.internal.i.c     // Catch:{ Throwable -> 0x00cf }
            boolean r0 = r0.a(r2)     // Catch:{ Throwable -> 0x00cf }
            if (r0 == 0) goto L_0x0034
            com.google.ads.internal.a r0 = com.google.ads.internal.i.c     // Catch:{ Throwable -> 0x00cf }
            com.google.ads.internal.d r1 = r5.a     // Catch:{ Throwable -> 0x00cf }
            java.util.Map r4 = r5.d     // Catch:{ Throwable -> 0x00cf }
            r0.a(r1, r4, r2, r6)     // Catch:{ Throwable -> 0x00cf }
            r0 = r3
        L_0x0033:
            return r0
        L_0x0034:
            boolean r0 = r5.g     // Catch:{ Throwable -> 0x00cf }
            if (r0 == 0) goto L_0x005b
            boolean r0 = com.google.ads.util.AdUtil.a(r2)     // Catch:{ Throwable -> 0x00cf }
            if (r0 == 0) goto L_0x0043
            boolean r0 = super.shouldOverrideUrlLoading(r6, r7)     // Catch:{ Throwable -> 0x00cf }
            goto L_0x0033
        L_0x0043:
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ Throwable -> 0x00cf }
            r0.<init>()     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r1 = "u"
            r0.put(r1, r7)     // Catch:{ Throwable -> 0x00cf }
            com.google.ads.internal.d r1 = r5.a     // Catch:{ Throwable -> 0x00cf }
            com.google.ads.internal.e r2 = new com.google.ads.internal.e     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r4 = "intent"
            r2.<init>(r4, r0)     // Catch:{ Throwable -> 0x00cf }
            com.google.ads.AdActivity.launchAdActivity(r1, r2)     // Catch:{ Throwable -> 0x00cf }
            r0 = r3
            goto L_0x0033
        L_0x005b:
            boolean r0 = r5.e     // Catch:{ Throwable -> 0x00cf }
            if (r0 == 0) goto L_0x00b6
            com.google.ads.internal.d r0 = r5.a     // Catch:{ am -> 0x009d }
            com.google.ads.n r1 = r0.i()     // Catch:{ am -> 0x009d }
            com.google.ads.util.i$b r0 = r1.f     // Catch:{ am -> 0x009d }
            java.lang.Object r0 = r0.a()     // Catch:{ am -> 0x009d }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ am -> 0x009d }
            com.google.ads.util.i$c r1 = r1.s     // Catch:{ am -> 0x009d }
            java.lang.Object r1 = r1.a()     // Catch:{ am -> 0x009d }
            com.google.ads.al r1 = (com.google.ads.al) r1     // Catch:{ am -> 0x009d }
            if (r1 == 0) goto L_0x00b4
            boolean r4 = r1.a(r2)     // Catch:{ am -> 0x009d }
            if (r4 == 0) goto L_0x00b4
            android.net.Uri r0 = r1.a(r2, r0)     // Catch:{ am -> 0x009d }
        L_0x0081:
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ Throwable -> 0x00cf }
            r1.<init>()     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r2 = "u"
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00cf }
            r1.put(r2, r0)     // Catch:{ Throwable -> 0x00cf }
            com.google.ads.internal.d r0 = r5.a     // Catch:{ Throwable -> 0x00cf }
            com.google.ads.internal.e r2 = new com.google.ads.internal.e     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r4 = "intent"
            r2.<init>(r4, r1)     // Catch:{ Throwable -> 0x00cf }
            com.google.ads.AdActivity.launchAdActivity(r0, r2)     // Catch:{ Throwable -> 0x00cf }
            r0 = r3
            goto L_0x0033
        L_0x009d:
            r0 = move-exception
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00cf }
            r0.<init>()     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r1 = "Unable to append parameter to URL: "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00cf }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00cf }
            com.google.ads.util.b.e(r0)     // Catch:{ Throwable -> 0x00cf }
        L_0x00b4:
            r0 = r2
            goto L_0x0081
        L_0x00b6:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00cf }
            r0.<init>()     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r1 = "URL is not a GMSG and can't handle URL: "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00cf }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Throwable -> 0x00cf }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00cf }
            com.google.ads.util.b.e(r0)     // Catch:{ Throwable -> 0x00cf }
        L_0x00cc:
            r0 = r3
            goto L_0x0033
        L_0x00cf:
            r0 = move-exception
            java.lang.String r1 = "An unknown error occurred in shouldOverrideUrlLoading."
            com.google.ads.util.b.d(r1, r0)
            goto L_0x00cc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.internal.i.shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String):boolean");
    }
}
