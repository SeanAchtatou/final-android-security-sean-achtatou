package com.google.ads.internal;

import android.os.Handler;
import com.google.ads.AdView;
import com.google.ads.m;
import com.google.ads.n;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;

public class ActivationOverlay extends AdWebView {
    private volatile boolean b = true;
    private boolean c = true;
    private int d = 0;
    private int e = 0;
    private final i f;

    public ActivationOverlay(n nVar) {
        super(nVar, null);
        if (AdUtil.a < ((Integer) ((m.a) ((m) nVar.d.a()).b.a()).c.a()).intValue()) {
            b.a("Disabling hardware acceleration for an activation overlay.");
            g();
        }
        this.f = i.a((d) nVar.b.a(), a.c, true, true);
        setWebViewClient(this.f);
    }

    public boolean a() {
        return this.b;
    }

    public boolean b() {
        return this.c;
    }

    public int c() {
        return this.e;
    }

    public boolean canScrollHorizontally(int i) {
        return false;
    }

    public boolean canScrollVertically(int i) {
        return false;
    }

    public int d() {
        return this.d;
    }

    public i e() {
        return this.f;
    }

    public void setOverlayActivated(boolean z) {
        this.c = z;
    }

    public void setOverlayEnabled(boolean z) {
        if (!z) {
            ((Handler) m.a().c.a()).post(new Runnable() {
                public void run() {
                    ((AdView) ActivationOverlay.this.a.j.a()).removeView(this);
                }
            });
        }
        this.b = z;
    }

    public void setXPosition(int i) {
        this.d = i;
    }

    public void setYPosition(int i) {
        this.e = i;
    }
}
