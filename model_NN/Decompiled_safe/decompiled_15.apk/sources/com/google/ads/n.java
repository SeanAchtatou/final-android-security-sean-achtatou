package com.google.ads;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import com.google.ads.internal.ActivationOverlay;
import com.google.ads.internal.d;
import com.google.ads.internal.h;
import com.google.ads.util.i;

public class n extends i {
    public final i.b a;
    public final i.b b;
    public final i.d c;
    public final i.b d;
    public final i.b e;
    public final i.b f;
    public final i.b g;
    public final i.b h;
    public final i.b i;
    public final i.b j;
    public final i.b k;
    public final i.c l = new i.c("currentAd", null);
    public final i.c m = new i.c("nextAd", null);
    public final i.c n;
    public final i.c o = new i.c("adListener");
    public final i.c p = new i.c("appEventListener");
    public final i.c q = new i.c("swipeableEventListener");
    public final i.c r = new i.c("spamSignals", null);
    public final i.c s = new i.c("spamSignalsUtil", null);
    public final i.c t = new i.c("usesManualImpressions", false);

    public n(m mVar, Ad ad, AdView adView, InterstitialAd interstitialAd, String str, Activity activity, Context context, ViewGroup viewGroup, h hVar, d dVar) {
        ActivationOverlay activationOverlay = null;
        this.d = new i.b("appState", mVar);
        this.a = new i.b("ad", ad);
        this.j = new i.b("adView", adView);
        this.g = new i.b("adType", hVar);
        this.h = new i.b("adUnitId", str);
        this.c = new i.d("activity", activity);
        this.k = new i.b("interstitialAd", interstitialAd);
        this.i = new i.b("bannerContainer", viewGroup);
        this.f = new i.b("applicationContext", context);
        this.n = new i.c("adSizes", null);
        this.b = new i.b("adManager", dVar);
        if (hVar != null && hVar.b()) {
            activationOverlay = new ActivationOverlay(this);
        }
        this.e = new i.b("activationOverlay", activationOverlay);
    }

    public boolean a() {
        return !b();
    }

    public boolean b() {
        return ((h) this.g.a()).a();
    }
}
