package com.google.ads.doubleclick;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.AppEventListener;
import com.google.ads.internal.h;

public class DfpAdView extends AdView {
    public DfpAdView(Activity activity, AdSize adSize, String str) {
        super(activity, adSize, str);
    }

    public DfpAdView(Activity activity, AdSize[] adSizeArr, String str) {
        super(activity, adSizeArr, str);
    }

    public DfpAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public DfpAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void enableManualImpressions(boolean z) {
        this.a.i().t.a(Boolean.valueOf(z));
    }

    public void recordImpression() {
        this.a.z();
    }

    public void resize(AdSize adSize) {
        this.a.l().setAdSize(adSize);
        ((h) this.a.i().g.a()).b(adSize);
    }

    public void setAppEventListener(AppEventListener appEventListener) {
        super.setAppEventListener(appEventListener);
    }

    public void setSupportedAdSizes(AdSize... adSizeArr) {
        super.setSupportedAdSizes(adSizeArr);
    }
}
