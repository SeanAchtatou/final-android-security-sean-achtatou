package com.google.ads.doubleclick;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import com.google.ads.AdSize;
import com.google.ads.SwipeableAdListener;
import com.google.ads.internal.ActivationOverlay;

public class SwipeableDfpAdView extends DfpAdView {
    public SwipeableDfpAdView(Activity activity, AdSize adSize, String str) {
        super(activity, adSize, str);
    }

    public SwipeableDfpAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SwipeableDfpAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void resize(AdSize adSize) {
        super.resize(adSize);
        if (((ActivationOverlay) this.a.i().e.a()).b()) {
            this.a.a(-1, -1, adSize.getWidth(), adSize.getHeight());
        }
    }

    public void setSwipeableEventListener(SwipeableAdListener swipeableAdListener) {
        super.setSwipeableEventListener(swipeableAdListener);
    }
}
