package com.google.ads.doubleclick;

import com.google.ads.mediation.admob.AdMobAdapterExtras;
import java.util.Map;

public class DfpExtras extends AdMobAdapterExtras {
    private String a;

    public DfpExtras() {
    }

    public DfpExtras(DfpExtras dfpExtras) {
        super(dfpExtras);
        if (dfpExtras != null) {
            this.a = dfpExtras.a;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ads.mediation.admob.AdMobAdapterExtras.addExtra(java.lang.String, java.lang.Object):com.google.ads.mediation.admob.AdMobAdapterExtras
     arg types: [java.lang.String, java.lang.Object]
     candidates:
      com.google.ads.doubleclick.DfpExtras.addExtra(java.lang.String, java.lang.Object):com.google.ads.doubleclick.DfpExtras
      com.google.ads.mediation.admob.AdMobAdapterExtras.addExtra(java.lang.String, java.lang.Object):com.google.ads.mediation.admob.AdMobAdapterExtras */
    public DfpExtras addExtra(String str, Object obj) {
        super.addExtra(str, obj);
        return this;
    }

    public DfpExtras clearExtras() {
        super.clearExtras();
        return this;
    }

    public String getPublisherProvidedId() {
        return this.a;
    }

    public DfpExtras setExtras(Map map) {
        super.setExtras(map);
        return this;
    }

    public DfpExtras setPlusOneOptOut(boolean z) {
        super.setPlusOneOptOut(z);
        return this;
    }

    public DfpExtras setPublisherProvidedId(String str) {
        this.a = str;
        return this;
    }

    public DfpExtras setUseExactAdSize(boolean z) {
        super.setUseExactAdSize(z);
        return this;
    }
}
