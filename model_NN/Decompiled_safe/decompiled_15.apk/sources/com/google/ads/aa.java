package com.google.ads;

import android.text.TextUtils;
import android.webkit.WebView;
import com.google.ads.internal.ActivationOverlay;
import com.google.ads.internal.d;
import com.google.ads.util.b;
import java.util.HashMap;

public class aa implements o {
    public void a(d dVar, HashMap hashMap, WebView webView) {
        int i = -1;
        if (webView instanceof ActivationOverlay) {
            try {
                int parseInt = !TextUtils.isEmpty((CharSequence) hashMap.get("w")) ? Integer.parseInt((String) hashMap.get("w")) : -1;
                int parseInt2 = !TextUtils.isEmpty((CharSequence) hashMap.get("h")) ? Integer.parseInt((String) hashMap.get("h")) : -1;
                int parseInt3 = !TextUtils.isEmpty((CharSequence) hashMap.get("x")) ? Integer.parseInt((String) hashMap.get("x")) : -1;
                if (!TextUtils.isEmpty((CharSequence) hashMap.get("y"))) {
                    i = Integer.parseInt((String) hashMap.get("y"));
                }
                if (hashMap.get("a") != null && ((String) hashMap.get("a")).equals("1")) {
                    dVar.a(null, true, parseInt3, i, parseInt, parseInt2);
                } else if (hashMap.get("a") == null || !((String) hashMap.get("a")).equals("0")) {
                    dVar.a(parseInt3, i, parseInt, parseInt2);
                } else {
                    dVar.a(null, false, parseInt3, i, parseInt, parseInt2);
                }
            } catch (NumberFormatException e) {
                b.d("Invalid number format in activation overlay response.", e);
            }
        } else {
            b.b("Trying to activate an overlay when this is not an overlay.");
        }
    }
}
