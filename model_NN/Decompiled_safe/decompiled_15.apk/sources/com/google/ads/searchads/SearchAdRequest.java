package com.google.ads.searchads;

import android.content.Context;
import android.graphics.Color;
import com.google.ads.AdRequest;
import com.google.ads.mediation.admob.AdMobAdapterExtras;
import java.util.Locale;
import java.util.Map;

public class SearchAdRequest extends AdRequest {
    private String a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private String h;
    private int i;
    private int j;
    private BorderType k;
    private int l;
    private String m;

    public enum BorderType {
        NONE("none"),
        DASHED("dashed"),
        DOTTED("dotted"),
        SOLID("solid");
        
        private String a;

        private BorderType(String str) {
            this.a = str;
        }

        public String toString() {
            return this.a;
        }
    }

    private String a(int i2) {
        return String.format(Locale.US, "#%06x", Integer.valueOf(16777215 & i2));
    }

    public Map getRequestMap(Context context) {
        AdMobAdapterExtras adMobAdapterExtras = (AdMobAdapterExtras) getNetworkExtras(AdMobAdapterExtras.class);
        if (adMobAdapterExtras == null) {
            adMobAdapterExtras = new AdMobAdapterExtras();
            setNetworkExtras(adMobAdapterExtras);
        }
        if (this.a != null) {
            adMobAdapterExtras.getExtras().put("q", this.a);
        }
        if (Color.alpha(this.b) != 0) {
            adMobAdapterExtras.getExtras().put("bgcolor", a(this.b));
        }
        if (Color.alpha(this.c) == 255 && Color.alpha(this.d) == 255) {
            adMobAdapterExtras.getExtras().put("gradientfrom", a(this.c));
            adMobAdapterExtras.getExtras().put("gradientto", a(this.d));
        }
        if (Color.alpha(this.e) != 0) {
            adMobAdapterExtras.getExtras().put("hcolor", a(this.e));
        }
        if (Color.alpha(this.f) != 0) {
            adMobAdapterExtras.getExtras().put("dcolor", a(this.f));
        }
        if (Color.alpha(this.g) != 0) {
            adMobAdapterExtras.getExtras().put("acolor", a(this.g));
        }
        if (this.h != null) {
            adMobAdapterExtras.getExtras().put("font", this.h);
        }
        adMobAdapterExtras.getExtras().put("headersize", Integer.toString(this.i));
        if (Color.alpha(this.j) != 0) {
            adMobAdapterExtras.getExtras().put("bcolor", a(this.j));
        }
        if (this.k != null) {
            adMobAdapterExtras.getExtras().put("btype", this.k.toString());
        }
        adMobAdapterExtras.getExtras().put("bthick", Integer.toString(this.l));
        if (this.m != null) {
            adMobAdapterExtras.getExtras().put("channel", this.m);
        }
        return super.getRequestMap(context);
    }

    public void setAnchorTextColor(int i2) {
        this.g = i2;
    }

    public void setBackgroundColor(int i2) {
        if (Color.alpha(i2) == 255) {
            this.b = i2;
            this.c = 0;
            this.d = 0;
        }
    }

    public void setBackgroundGradient(int i2, int i3) {
        if (Color.alpha(i2) == 255 && Color.alpha(i3) == 255) {
            this.b = Color.argb(0, 0, 0, 0);
            this.c = i2;
            this.d = i3;
        }
    }

    public void setBorderColor(int i2) {
        this.j = i2;
    }

    public void setBorderThickness(int i2) {
        this.l = i2;
    }

    public void setBorderType(BorderType borderType) {
        this.k = borderType;
    }

    public void setCustomChannels(String str) {
        this.m = str;
    }

    public void setDescriptionTextColor(int i2) {
        this.f = i2;
    }

    public void setFontFace(String str) {
        this.h = str;
    }

    public void setHeaderTextColor(int i2) {
        this.e = i2;
    }

    public void setHeaderTextSize(int i2) {
        this.i = i2;
    }

    public void setQuery(String str) {
        this.a = str;
    }
}
