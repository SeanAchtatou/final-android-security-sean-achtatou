package com.google.ads;

import com.google.ads.AdRequest;
import com.google.ads.g;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.ads.util.a;
import com.google.ads.util.b;

class k implements MediationInterstitialListener {
    private final h a;

    k(h hVar) {
        this.a = hVar;
    }

    public void onDismissScreen(MediationInterstitialAdapter mediationInterstitialAdapter) {
        synchronized (this.a) {
            this.a.j().b(this.a);
        }
    }

    public void onFailedToReceiveAd(MediationInterstitialAdapter mediationInterstitialAdapter, AdRequest.ErrorCode errorCode) {
        synchronized (this.a) {
            a.a(mediationInterstitialAdapter, this.a.i());
            b.a("Mediation adapter " + mediationInterstitialAdapter.getClass().getName() + " failed to receive ad with error code: " + errorCode);
            if (this.a.c()) {
                b.b("Got an onFailedToReceiveAd() callback after loadAdTask is done from an interstitial adapter.  Ignoring callback.");
            } else {
                this.a.a(false, errorCode == AdRequest.ErrorCode.NO_FILL ? g.a.NO_FILL : g.a.ERROR);
            }
        }
    }

    public void onLeaveApplication(MediationInterstitialAdapter mediationInterstitialAdapter) {
        synchronized (this.a) {
            this.a.j().c(this.a);
        }
    }

    public void onPresentScreen(MediationInterstitialAdapter mediationInterstitialAdapter) {
        synchronized (this.a) {
            this.a.j().a(this.a);
        }
    }

    public void onReceivedAd(MediationInterstitialAdapter mediationInterstitialAdapter) {
        synchronized (this.a) {
            a.a(mediationInterstitialAdapter, this.a.i());
            if (this.a.c()) {
                b.e("Got an onReceivedAd() callback after loadAdTask is done from an interstitial adapter. Ignoring callback.");
            } else {
                this.a.a(true, g.a.AD);
            }
        }
    }
}
