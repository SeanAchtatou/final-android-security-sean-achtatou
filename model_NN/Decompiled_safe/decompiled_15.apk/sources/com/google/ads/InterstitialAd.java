package com.google.ads;

import android.app.Activity;
import com.google.ads.internal.d;

public class InterstitialAd implements Ad {
    private d a;

    public InterstitialAd(Activity activity, String str) {
        this(activity, str, false);
    }

    public InterstitialAd(Activity activity, String str, boolean z) {
        this.a = new d(this, activity, null, str, null, z);
    }

    public boolean isReady() {
        return this.a.s();
    }

    public void loadAd(AdRequest adRequest) {
        this.a.a(adRequest);
    }

    public void setAdListener(AdListener adListener) {
        this.a.i().o.a(adListener);
    }

    /* access modifiers changed from: protected */
    public void setAppEventListener(AppEventListener appEventListener) {
        this.a.i().p.a(appEventListener);
    }

    public void show() {
        this.a.B();
    }

    public void stopLoading() {
        this.a.C();
    }
}
