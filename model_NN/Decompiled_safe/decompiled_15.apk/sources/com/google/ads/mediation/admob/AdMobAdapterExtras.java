package com.google.ads.mediation.admob;

import com.google.ads.mediation.NetworkExtras;
import java.util.HashMap;
import java.util.Map;

public class AdMobAdapterExtras implements NetworkExtras {
    private boolean a;
    private Map b;

    public AdMobAdapterExtras() {
        this.a = false;
        clearExtras();
    }

    public AdMobAdapterExtras(AdMobAdapterExtras adMobAdapterExtras) {
        this();
        if (adMobAdapterExtras != null) {
            this.a = adMobAdapterExtras.a;
            this.b.putAll(adMobAdapterExtras.b);
        }
    }

    public AdMobAdapterExtras addExtra(String str, Object obj) {
        this.b.put(str, obj);
        return this;
    }

    public AdMobAdapterExtras clearExtras() {
        this.b = new HashMap();
        return this;
    }

    public Map getExtras() {
        return this.b;
    }

    @Deprecated
    public boolean getPlusOneOptOut() {
        return false;
    }

    public boolean getUseExactAdSize() {
        return this.a;
    }

    public AdMobAdapterExtras setExtras(Map map) {
        if (map == null) {
            throw new IllegalArgumentException("Argument 'extras' may not be null");
        }
        this.b = map;
        return this;
    }

    @Deprecated
    public AdMobAdapterExtras setPlusOneOptOut(boolean z) {
        return this;
    }

    public AdMobAdapterExtras setUseExactAdSize(boolean z) {
        this.a = z;
        return this;
    }
}
