package com.google.ads.mediation.customevent;

import android.app.Activity;
import android.view.View;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.g;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;

public class CustomEventAdapter implements MediationBannerAdapter, MediationInterstitialAdapter {
    /* access modifiers changed from: private */
    public String a;
    private CustomEventBanner b = null;
    private a c = null;
    private CustomEventInterstitial d = null;

    class a implements CustomEventBannerListener {
        private View b;
        private final MediationBannerListener c;

        public a(MediationBannerListener mediationBannerListener) {
            this.c = mediationBannerListener;
        }

        private String b() {
            return "Banner custom event labeled '" + CustomEventAdapter.this.a + "'";
        }

        public synchronized View a() {
            return this.b;
        }

        public void onClick() {
            com.google.ads.util.b.a(b() + " called onClick().");
            this.c.onClick(CustomEventAdapter.this);
        }

        public void onDismissScreen() {
            com.google.ads.util.b.a(b() + " called onDismissScreen().");
            this.c.onDismissScreen(CustomEventAdapter.this);
        }

        public void onFailedToReceiveAd() {
            com.google.ads.util.b.a(b() + " called onFailedToReceiveAd().");
            this.c.onFailedToReceiveAd(CustomEventAdapter.this, AdRequest.ErrorCode.NO_FILL);
        }

        public synchronized void onLeaveApplication() {
            com.google.ads.util.b.a(b() + " called onLeaveApplication().");
            this.c.onLeaveApplication(CustomEventAdapter.this);
        }

        public void onPresentScreen() {
            com.google.ads.util.b.a(b() + " called onPresentScreen().");
            this.c.onPresentScreen(CustomEventAdapter.this);
        }

        public synchronized void onReceivedAd(View view) {
            com.google.ads.util.b.a(b() + " called onReceivedAd.");
            this.b = view;
            this.c.onReceivedAd(CustomEventAdapter.this);
        }
    }

    class b implements CustomEventInterstitialListener {
        private final MediationInterstitialListener b;

        public b(MediationInterstitialListener mediationInterstitialListener) {
            this.b = mediationInterstitialListener;
        }

        private String a() {
            return "Interstitial custom event labeled '" + CustomEventAdapter.this.a + "'";
        }

        public void onDismissScreen() {
            com.google.ads.util.b.a(a() + " called onDismissScreen().");
            this.b.onDismissScreen(CustomEventAdapter.this);
        }

        public void onFailedToReceiveAd() {
            com.google.ads.util.b.a(a() + " called onFailedToReceiveAd().");
            this.b.onFailedToReceiveAd(CustomEventAdapter.this, AdRequest.ErrorCode.NO_FILL);
        }

        public synchronized void onLeaveApplication() {
            com.google.ads.util.b.a(a() + " called onLeaveApplication().");
            this.b.onLeaveApplication(CustomEventAdapter.this);
        }

        public void onPresentScreen() {
            com.google.ads.util.b.a(a() + " called onPresentScreen().");
            this.b.onPresentScreen(CustomEventAdapter.this);
        }

        public void onReceivedAd() {
            com.google.ads.util.b.a(a() + " called onReceivedAd.");
            this.b.onReceivedAd(CustomEventAdapter.this);
        }
    }

    private Object a(String str, Class cls, String str2) {
        try {
            return g.a(str, cls);
        } catch (ClassNotFoundException e) {
            a("Make sure you created a visible class named: " + str + ". ", e);
        } catch (ClassCastException e2) {
            a("Make sure your custom event implements the " + cls.getName() + " interface.", e2);
        } catch (IllegalAccessException e3) {
            a("Make sure the default constructor for class " + str + " is visible. ", e3);
        } catch (InstantiationException e4) {
            a("Make sure the name " + str + " does not denote an abstract class or an interface.", e4);
        } catch (Throwable th) {
            a("", th);
        }
        return null;
    }

    private void a(String str, Throwable th) {
        com.google.ads.util.b.b("Error during processing of custom event with label: '" + this.a + "'. Skipping custom event. " + str, th);
    }

    public void destroy() {
        if (this.b != null) {
            this.b.destroy();
        }
        if (this.d != null) {
            this.d.destroy();
        }
    }

    public Class getAdditionalParametersType() {
        return CustomEventExtras.class;
    }

    public View getBannerView() {
        com.google.ads.util.a.b(this.c);
        return this.c.a();
    }

    public Class getServerParametersType() {
        return CustomEventServerParameters.class;
    }

    public void requestBannerAd(MediationBannerListener mediationBannerListener, Activity activity, CustomEventServerParameters customEventServerParameters, AdSize adSize, MediationAdRequest mediationAdRequest, CustomEventExtras customEventExtras) {
        com.google.ads.util.a.a((Object) this.a);
        this.a = customEventServerParameters.label;
        String str = customEventServerParameters.className;
        String str2 = customEventServerParameters.parameter;
        this.b = (CustomEventBanner) a(str, CustomEventBanner.class, this.a);
        if (this.b == null) {
            mediationBannerListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
            return;
        }
        com.google.ads.util.a.a(this.c);
        this.c = new a(mediationBannerListener);
        try {
            this.b.requestBannerAd(this.c, activity, this.a, str2, adSize, mediationAdRequest, customEventExtras == null ? null : customEventExtras.getExtra(this.a));
        } catch (Throwable th) {
            a("", th);
            mediationBannerListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
        }
    }

    public void requestInterstitialAd(MediationInterstitialListener mediationInterstitialListener, Activity activity, CustomEventServerParameters customEventServerParameters, MediationAdRequest mediationAdRequest, CustomEventExtras customEventExtras) {
        com.google.ads.util.a.a((Object) this.a);
        this.a = customEventServerParameters.label;
        String str = customEventServerParameters.className;
        String str2 = customEventServerParameters.parameter;
        this.d = (CustomEventInterstitial) a(str, CustomEventInterstitial.class, this.a);
        if (this.d == null) {
            mediationInterstitialListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
            return;
        }
        try {
            this.d.requestInterstitialAd(new b(mediationInterstitialListener), activity, this.a, str2, mediationAdRequest, customEventExtras == null ? null : customEventExtras.getExtra(this.a));
        } catch (Throwable th) {
            a("", th);
            mediationInterstitialListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
        }
    }

    public void showInterstitial() {
        com.google.ads.util.a.b(this.d);
        try {
            this.d.showInterstitial();
        } catch (Throwable th) {
            com.google.ads.util.b.b("Exception when showing custom event labeled '" + this.a + "'.", th);
        }
    }
}
