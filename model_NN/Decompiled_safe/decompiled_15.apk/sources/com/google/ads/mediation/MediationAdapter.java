package com.google.ads.mediation;

public interface MediationAdapter {
    void destroy();

    Class getAdditionalParametersType();

    Class getServerParametersType();
}
