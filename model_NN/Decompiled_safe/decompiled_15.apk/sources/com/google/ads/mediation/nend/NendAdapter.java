package com.google.ads.mediation.nend;

import android.app.Activity;
import android.view.View;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import net.nend.android.NendAdListener;
import net.nend.android.NendAdView;

public final class NendAdapter implements MediationBannerAdapter, MediationInterstitialAdapter, NendAdListener {
    public static final String VERSION = "1.1.0";
    private MediationBannerListener mListener;
    private NendAdView mNendAdView;

    public void destroy() {
        this.mNendAdView = null;
        this.mListener = null;
    }

    public Class getAdditionalParametersType() {
        return NendAdapterExtras.class;
    }

    public View getBannerView() {
        return this.mNendAdView;
    }

    public Class getServerParametersType() {
        return NendAdapterServerParameters.class;
    }

    public void onClick(NendAdView nendAdView) {
        if (this.mListener != null) {
            this.mListener.onClick(this);
            this.mListener.onPresentScreen(this);
            this.mListener.onLeaveApplication(this);
        }
    }

    public void onDismissScreen(NendAdView nendAdView) {
        if (this.mListener != null) {
            this.mListener.onDismissScreen(this);
        }
    }

    public void onFailedToReceiveAd(NendAdView nendAdView) {
        if (this.mListener != null) {
            this.mListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
        }
    }

    public void onReceiveAd(NendAdView nendAdView) {
        if (this.mListener != null) {
            this.mListener.onReceivedAd(this);
        }
    }

    public void requestBannerAd(MediationBannerListener mediationBannerListener, Activity activity, NendAdapterServerParameters nendAdapterServerParameters, AdSize adSize, MediationAdRequest mediationAdRequest, NendAdapterExtras nendAdapterExtras) {
        if (adSize.isSizeAppropriate(320, 50) || adSize.isSizeAppropriate(300, 250) || adSize.isSizeAppropriate(728, 90)) {
            this.mListener = mediationBannerListener;
            this.mNendAdView = new NendAdView(activity, Integer.parseInt(nendAdapterServerParameters.spotIdStr), nendAdapterServerParameters.apiKey);
            this.mNendAdView.pause();
            this.mNendAdView.setListener(this);
            this.mNendAdView.loadAd();
            return;
        }
        mediationBannerListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.NO_FILL);
    }

    public void requestInterstitialAd(MediationInterstitialListener mediationInterstitialListener, Activity activity, NendAdapterServerParameters nendAdapterServerParameters, MediationAdRequest mediationAdRequest, NendAdapterExtras nendAdapterExtras) {
        mediationInterstitialListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INVALID_REQUEST);
    }

    public void showInterstitial() {
        throw new UnsupportedOperationException();
    }
}
