package com.google.ads.mediation.customevent;

import com.google.ads.mediation.NetworkExtras;
import java.util.HashMap;

public class CustomEventExtras implements NetworkExtras {
    private final HashMap a = new HashMap();

    public CustomEventExtras addExtra(String str, Object obj) {
        this.a.put(str, obj);
        return this;
    }

    public CustomEventExtras clearExtras() {
        this.a.clear();
        return this;
    }

    public Object getExtra(String str) {
        return this.a.get(str);
    }

    public Object removeExtra(String str) {
        return this.a.remove(str);
    }
}
