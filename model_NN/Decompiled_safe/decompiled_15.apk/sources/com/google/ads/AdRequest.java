package com.google.ads;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import com.google.ads.doubleclick.DfpExtras;
import com.google.ads.mediation.NetworkExtras;
import com.google.ads.mediation.admob.AdMobAdapterExtras;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdRequest {
    public static final String LOGTAG = "Ads";
    public static final String TEST_EMULATOR = AdUtil.b("emulator");
    public static final String VERSION = "6.4.1";
    private static final SimpleDateFormat a = new SimpleDateFormat("yyyyMMdd");
    private static Method b;
    private static Method c;
    private Gender d = null;
    private Date e = null;
    private Set f = null;
    private Map g = null;
    private final Map h = new HashMap();
    private Location i = null;
    private boolean j = false;
    private boolean k = false;
    private Set l = null;

    public enum ErrorCode {
        INVALID_REQUEST("Invalid Ad request."),
        NO_FILL("Ad request successful, but no ad returned due to lack of ad inventory."),
        NETWORK_ERROR("A network error occurred."),
        INTERNAL_ERROR("There was an internal error.");
        
        private final String a;

        private ErrorCode(String str) {
            this.a = str;
        }

        public String toString() {
            return this.a;
        }
    }

    public enum Gender {
        UNKNOWN,
        MALE,
        FEMALE
    }

    static {
        b = null;
        c = null;
        try {
            for (Method method : Class.forName("com.google.analytics.tracking.android.AdMobInfo").getMethods()) {
                if (method.getName().equals("getInstance") && method.getParameterTypes().length == 0) {
                    b = method;
                } else if (method.getName().equals("getJoinIds") && method.getParameterTypes().length == 0) {
                    c = method;
                }
            }
            if (b == null || c == null) {
                b = null;
                c = null;
                b.e("No Google Analytics: Library Incompatible.");
            }
        } catch (ClassNotFoundException e2) {
            b.a("No Google Analytics: Library Not Found.");
        } catch (Throwable th) {
            b.a("No Google Analytics: Error Loading Library");
        }
    }

    private synchronized AdMobAdapterExtras a() {
        if (getNetworkExtras(AdMobAdapterExtras.class) == null) {
            setNetworkExtras(new AdMobAdapterExtras());
        }
        return (AdMobAdapterExtras) getNetworkExtras(AdMobAdapterExtras.class);
    }

    @Deprecated
    public AdRequest addExtra(String str, Object obj) {
        AdMobAdapterExtras a2 = a();
        if (a2.getExtras() == null) {
            a2.setExtras(new HashMap());
        }
        a2.getExtras().put(str, obj);
        return this;
    }

    public AdRequest addKeyword(String str) {
        if (this.f == null) {
            this.f = new HashSet();
        }
        this.f.add(str);
        return this;
    }

    public AdRequest addKeywords(Set set) {
        if (this.f == null) {
            this.f = new HashSet();
        }
        this.f.addAll(set);
        return this;
    }

    public AdRequest addMediationExtra(String str, Object obj) {
        if (this.g == null) {
            this.g = new HashMap();
        }
        this.g.put(str, obj);
        return this;
    }

    public AdRequest addTestDevice(String str) {
        if (this.l == null) {
            this.l = new HashSet();
        }
        this.l.add(str);
        return this;
    }

    public AdRequest clearBirthday() {
        this.e = null;
        return this;
    }

    public Date getBirthday() {
        return this.e;
    }

    public Gender getGender() {
        return this.d;
    }

    public Set getKeywords() {
        if (this.f == null) {
            return null;
        }
        return Collections.unmodifiableSet(this.f);
    }

    public Location getLocation() {
        return this.i;
    }

    public Object getNetworkExtras(Class cls) {
        return this.h.get(cls);
    }

    @Deprecated
    public boolean getPlusOneOptOut() {
        return a().getPlusOneOptOut();
    }

    public Map getRequestMap(Context context) {
        HashMap hashMap = new HashMap();
        if (this.f != null) {
            hashMap.put("kw", this.f);
        }
        if (this.d != null) {
            hashMap.put("cust_gender", Integer.valueOf(this.d.ordinal()));
        }
        if (this.e != null) {
            hashMap.put("cust_age", a.format(this.e));
        }
        if (this.i != null) {
            hashMap.put("uule", AdUtil.a(this.i));
        }
        if (this.j) {
            hashMap.put("testing", 1);
        }
        if (isTestDevice(context)) {
            hashMap.put("adtest", "on");
        } else if (!this.k) {
            b.c("To get test ads on this device, call adRequest.addTestDevice(" + (AdUtil.c() ? "AdRequest.TEST_EMULATOR" : "\"" + AdUtil.a(context) + "\"") + ");");
            this.k = true;
        }
        AdMobAdapterExtras adMobAdapterExtras = (AdMobAdapterExtras) getNetworkExtras(AdMobAdapterExtras.class);
        DfpExtras dfpExtras = (DfpExtras) getNetworkExtras(DfpExtras.class);
        if (dfpExtras != null && dfpExtras.getExtras() != null && !dfpExtras.getExtras().isEmpty()) {
            hashMap.put("extras", dfpExtras.getExtras());
        } else if (!(adMobAdapterExtras == null || adMobAdapterExtras.getExtras() == null || adMobAdapterExtras.getExtras().isEmpty())) {
            hashMap.put("extras", adMobAdapterExtras.getExtras());
        }
        if (dfpExtras != null) {
            String publisherProvidedId = dfpExtras.getPublisherProvidedId();
            if (!TextUtils.isEmpty(publisherProvidedId)) {
                hashMap.put("ppid", publisherProvidedId);
            }
        }
        if (this.g != null) {
            hashMap.put("mediation_extras", this.g);
        }
        try {
            if (b != null) {
                Map map = (Map) c.invoke(b.invoke(null, new Object[0]), new Object[0]);
                if (map != null && map.size() > 0) {
                    hashMap.put("analytics_join_id", map);
                }
            }
        } catch (Throwable th) {
            b.c("Internal Analytics Error:", th);
        }
        return hashMap;
    }

    public boolean isTestDevice(Context context) {
        String a2;
        return (this.l == null || (a2 = AdUtil.a(context)) == null || !this.l.contains(a2)) ? false : true;
    }

    public AdRequest removeNetworkExtras(Class cls) {
        this.h.remove(cls);
        return this;
    }

    @Deprecated
    public AdRequest setBirthday(String str) {
        if (str == "" || str == null) {
            this.e = null;
        } else {
            try {
                this.e = a.parse(str);
            } catch (ParseException e2) {
                b.e("Birthday format invalid.  Expected 'YYYYMMDD' where 'YYYY' is a 4 digit year, 'MM' is a two digit month, and 'DD' is a two digit day.  Birthday value ignored");
                this.e = null;
            }
        }
        return this;
    }

    public AdRequest setBirthday(Calendar calendar) {
        if (calendar == null) {
            this.e = null;
        } else {
            setBirthday(calendar.getTime());
        }
        return this;
    }

    public AdRequest setBirthday(Date date) {
        if (date == null) {
            this.e = null;
        } else {
            this.e = new Date(date.getTime());
        }
        return this;
    }

    @Deprecated
    public AdRequest setExtras(Map map) {
        a().setExtras(map);
        return this;
    }

    public AdRequest setGender(Gender gender) {
        this.d = gender;
        return this;
    }

    public AdRequest setKeywords(Set set) {
        this.f = set;
        return this;
    }

    public AdRequest setLocation(Location location) {
        this.i = location;
        return this;
    }

    public AdRequest setMediationExtras(Map map) {
        this.g = map;
        return this;
    }

    public AdRequest setNetworkExtras(NetworkExtras networkExtras) {
        if (networkExtras != null) {
            this.h.put(networkExtras.getClass(), networkExtras);
        }
        return this;
    }

    @Deprecated
    public AdRequest setPlusOneOptOut(boolean z) {
        a().setPlusOneOptOut(z);
        return this;
    }

    public AdRequest setTestDevices(Set set) {
        this.l = set;
        return this;
    }

    @Deprecated
    public AdRequest setTesting(boolean z) {
        this.j = z;
        return this;
    }
}
