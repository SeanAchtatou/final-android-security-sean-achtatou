package com.google.ads.util;

import android.annotation.TargetApi;
import android.view.View;
import android.webkit.WebChromeClient;
import com.google.ads.AdSize;
import com.google.ads.internal.ActivationOverlay;
import com.google.ads.internal.AdWebView;
import com.google.ads.n;
import com.google.ads.util.g;

@TargetApi(14)
public class IcsUtil {

    public class IcsAdWebView extends AdWebView {
        public IcsAdWebView(n nVar, AdSize adSize) {
            super(nVar, adSize);
        }

        public boolean canScrollHorizontally(int i) {
            return this.a.e.a() != null ? !((ActivationOverlay) this.a.e.a()).b() : super.canScrollHorizontally(i);
        }

        public boolean canScrollVertically(int i) {
            return this.a.e.a() != null ? !((ActivationOverlay) this.a.e.a()).b() : super.canScrollVertically(i);
        }
    }

    public class a extends g.a {
        public a(n nVar) {
            super(nVar);
        }

        public void onShowCustomView(View view, int i, WebChromeClient.CustomViewCallback customViewCallback) {
            customViewCallback.onCustomViewHidden();
        }
    }
}
