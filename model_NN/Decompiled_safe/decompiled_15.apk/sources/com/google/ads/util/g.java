package com.google.ads.util;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.webkit.ConsoleMessage;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.ads.AdActivity;
import com.google.ads.internal.AdWebView;
import com.google.ads.internal.c;
import com.google.ads.internal.d;
import com.google.ads.internal.i;
import com.google.ads.m;
import com.google.ads.n;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import jp.adlantis.android.AdlantisAd;

@TargetApi(11)
public class g {

    /* renamed from: com.google.ads.util.g$1  reason: invalid class name */
    /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[ConsoleMessage.MessageLevel.values().length];

        static {
            try {
                a[ConsoleMessage.MessageLevel.ERROR.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[ConsoleMessage.MessageLevel.WARNING.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[ConsoleMessage.MessageLevel.LOG.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[ConsoleMessage.MessageLevel.TIP.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                a[ConsoleMessage.MessageLevel.DEBUG.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    public class a extends WebChromeClient {
        private final n a;

        public a(n nVar) {
            this.a = nVar;
        }

        private static void a(AlertDialog.Builder builder, Context context, String str, String str2, final JsPromptResult jsPromptResult) {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(1);
            TextView textView = new TextView(context);
            textView.setText(str);
            final EditText editText = new EditText(context);
            editText.setText(str2);
            linearLayout.addView(textView);
            linearLayout.addView(editText);
            builder.setView(linearLayout).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    jsPromptResult.confirm(editText.getText().toString());
                }
            }).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    jsPromptResult.cancel();
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialogInterface) {
                    jsPromptResult.cancel();
                }
            }).create().show();
        }

        private static void a(AlertDialog.Builder builder, String str, final JsResult jsResult) {
            builder.setMessage(str).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    jsResult.confirm();
                }
            }).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    jsResult.cancel();
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialogInterface) {
                    jsResult.cancel();
                }
            }).create().show();
        }

        private static boolean a(WebView webView, String str, String str2, String str3, JsResult jsResult, JsPromptResult jsPromptResult, boolean z) {
            AdActivity i;
            if (!(webView instanceof AdWebView) || (i = ((AdWebView) webView).i()) == null) {
                return false;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(i);
            builder.setTitle(str);
            if (z) {
                a(builder, i, str2, str3, jsPromptResult);
            } else {
                a(builder, str2, jsResult);
            }
            return true;
        }

        public void onCloseWindow(WebView webView) {
            if (webView instanceof AdWebView) {
                ((AdWebView) webView).f();
            }
        }

        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            String str = "JS: " + consoleMessage.message() + " (" + consoleMessage.sourceId() + ":" + consoleMessage.lineNumber() + ")";
            switch (AnonymousClass1.a[consoleMessage.messageLevel().ordinal()]) {
                case 1:
                    b.b(str);
                    break;
                case AdlantisAd.ADTYPE_TEXT /*2*/:
                    b.e(str);
                    break;
                case 3:
                case 4:
                    b.c(str);
                    break;
                case 5:
                    b.a(str);
                    break;
            }
            return super.onConsoleMessage(consoleMessage);
        }

        public void onExceededDatabaseQuota(String str, String str2, long j, long j2, long j3, WebStorage.QuotaUpdater quotaUpdater) {
            m.a aVar = (m.a) ((m) this.a.d.a()).b.a();
            long longValue = ((Long) aVar.l.a()).longValue() - j3;
            if (longValue <= 0) {
                quotaUpdater.updateQuota(j);
                return;
            }
            if (j == 0) {
                if (j2 > longValue || j2 > ((Long) aVar.m.a()).longValue()) {
                    j2 = 0;
                }
            } else if (j2 == 0) {
                j2 = Math.min(Math.min(((Long) aVar.n.a()).longValue(), longValue) + j, ((Long) aVar.m.a()).longValue());
            } else {
                if (j2 <= Math.min(((Long) aVar.m.a()).longValue() - j, longValue)) {
                    j += j2;
                }
                j2 = j;
            }
            quotaUpdater.updateQuota(j2);
        }

        public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            return a(webView, str, str2, null, jsResult, null, false);
        }

        public boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
            return a(webView, str, str2, null, jsResult, null, false);
        }

        public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
            return a(webView, str, str2, null, jsResult, null, false);
        }

        public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
            return a(webView, str, str2, str3, null, jsPromptResult, true);
        }

        public void onReachedMaxAppCacheSize(long j, long j2, WebStorage.QuotaUpdater quotaUpdater) {
            m.a aVar = (m.a) ((m) this.a.d.a()).b.a();
            long longValue = ((Long) aVar.j.a()).longValue() + j;
            if (((Long) aVar.k.a()).longValue() - j2 < longValue) {
                quotaUpdater.updateQuota(0);
            } else {
                quotaUpdater.updateQuota(longValue);
            }
        }

        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
            customViewCallback.onCustomViewHidden();
        }
    }

    public class b extends i {
        public b(d dVar, Map map, boolean z, boolean z2) {
            super(dVar, map, z, z2);
        }

        private static WebResourceResponse a(String str, Context context) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            try {
                AdUtil.a(httpURLConnection, context.getApplicationContext());
                httpURLConnection.connect();
                return new WebResourceResponse("application/javascript", "UTF-8", new ByteArrayInputStream(AdUtil.a(new InputStreamReader(httpURLConnection.getInputStream())).getBytes("UTF-8")));
            } finally {
                httpURLConnection.disconnect();
            }
        }

        public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            try {
                if ("mraid.js".equalsIgnoreCase(new File(str).getName())) {
                    c k = this.a.k();
                    if (k != null) {
                        k.c(true);
                    } else {
                        this.a.a(true);
                    }
                    m.a aVar = (m.a) ((m) this.a.i().d.a()).b.a();
                    if (this.a.i().b()) {
                        String str2 = (String) aVar.g.a();
                        b.a("shouldInterceptRequest(" + str2 + ")");
                        return a(str2, webView.getContext());
                    } else if (this.b) {
                        String str3 = (String) aVar.f.a();
                        b.a("shouldInterceptRequest(" + str3 + ")");
                        return a(str3, webView.getContext());
                    } else {
                        String str4 = (String) aVar.e.a();
                        b.a("shouldInterceptRequest(" + str4 + ")");
                        return a(str4, webView.getContext());
                    }
                }
            } catch (IOException e) {
                b.d("IOException fetching MRAID JS.", e);
            } catch (Throwable th) {
                b.d("An unknown error occurred fetching MRAID JS.", th);
            }
            return super.shouldInterceptRequest(webView, str);
        }
    }

    public static void a(View view) {
        view.setLayerType(1, null);
    }

    public static void a(Window window) {
        window.setFlags(16777216, 16777216);
    }

    public static void a(WebSettings webSettings, n nVar) {
        Context context = (Context) nVar.f.a();
        webSettings.setAppCacheEnabled(true);
        webSettings.setAppCacheMaxSize(((Long) ((m.a) ((m) nVar.d.a()).b.a()).i.a()).longValue());
        webSettings.setAppCachePath(new File(context.getCacheDir(), "admob").getAbsolutePath());
        webSettings.setDatabaseEnabled(true);
        webSettings.setDatabasePath(context.getDatabasePath("admob").getAbsolutePath());
        webSettings.setDomStorageEnabled(true);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
    }

    public static void b(View view) {
        view.setLayerType(0, null);
    }
}
