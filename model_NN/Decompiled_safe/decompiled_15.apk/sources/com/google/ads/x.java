package com.google.ads;

import android.text.TextUtils;
import android.webkit.WebView;
import com.google.ads.internal.ActivationOverlay;
import com.google.ads.internal.AdWebView;
import com.google.ads.internal.d;
import com.google.ads.internal.h;
import com.google.ads.m;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;
import com.google.ads.util.g;
import com.google.ads.util.i;
import java.util.HashMap;

public class x implements o {
    private void a(HashMap hashMap, String str, i.c cVar) {
        try {
            String str2 = (String) hashMap.get(str);
            if (!TextUtils.isEmpty(str2)) {
                cVar.a(Integer.valueOf(str2));
            }
        } catch (NumberFormatException e) {
            b.a("Could not parse \"" + str + "\" constant.");
        }
    }

    private void b(HashMap hashMap, String str, i.c cVar) {
        try {
            String str2 = (String) hashMap.get(str);
            if (!TextUtils.isEmpty(str2)) {
                cVar.a(Long.valueOf(str2));
            }
        } catch (NumberFormatException e) {
            b.a("Could not parse \"" + str + "\" constant.");
        }
    }

    private void c(HashMap hashMap, String str, i.c cVar) {
        String str2 = (String) hashMap.get(str);
        if (!TextUtils.isEmpty(str2)) {
            cVar.a(str2);
        }
    }

    public void a(d dVar, HashMap hashMap, WebView webView) {
        n i = dVar.i();
        m.a aVar = (m.a) ((m) i.d.a()).b.a();
        c(hashMap, "as_domains", aVar.a);
        c(hashMap, "bad_ad_report_path", aVar.h);
        a(hashMap, "min_hwa_banner", aVar.b);
        a(hashMap, "min_hwa_activation_overlay", aVar.c);
        a(hashMap, "min_hwa_overlay", aVar.d);
        c(hashMap, "mraid_banner_path", aVar.e);
        c(hashMap, "mraid_expanded_banner_path", aVar.f);
        c(hashMap, "mraid_interstitial_path", aVar.g);
        b(hashMap, "ac_max_size", aVar.i);
        b(hashMap, "ac_padding", aVar.j);
        b(hashMap, "ac_total_quota", aVar.k);
        b(hashMap, "db_total_quota", aVar.l);
        b(hashMap, "db_quota_per_origin", aVar.m);
        b(hashMap, "db_quota_step_size", aVar.n);
        AdWebView l = dVar.l();
        if (AdUtil.a >= 11) {
            g.a(l.getSettings(), i);
            g.a(webView.getSettings(), i);
        }
        if (!((h) i.g.a()).a()) {
            boolean k = l.k();
            boolean z = AdUtil.a < ((Integer) aVar.b.a()).intValue();
            if (!z && k) {
                b.a("Re-enabling hardware acceleration for a banner after reading constants.");
                l.h();
            } else if (z && !k) {
                b.a("Disabling hardware acceleration for a banner after reading constants.");
                l.g();
            }
        }
        ActivationOverlay activationOverlay = (ActivationOverlay) i.e.a();
        if (!((h) i.g.a()).b() && activationOverlay != null) {
            boolean k2 = activationOverlay.k();
            boolean z2 = AdUtil.a < ((Integer) aVar.c.a()).intValue();
            if (!z2 && k2) {
                b.a("Re-enabling hardware acceleration for an activation overlay after reading constants.");
                activationOverlay.h();
            } else if (z2 && !k2) {
                b.a("Disabling hardware acceleration for an activation overlay after reading constants.");
                activationOverlay.g();
            }
        }
        String str = (String) aVar.a.a();
        al alVar = (al) i.s.a();
        if (alVar != null && !TextUtils.isEmpty(str)) {
            alVar.a(str);
        }
        aVar.o.a(true);
    }
}
