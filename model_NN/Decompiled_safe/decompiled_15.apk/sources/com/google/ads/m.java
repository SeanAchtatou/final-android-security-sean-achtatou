package com.google.ads;

import android.os.Handler;
import android.os.Looper;
import com.google.ads.util.i;

public class m extends i {
    private static final m d = new m();
    public final i.c a = new i.c("marketPackages", null);
    public final i.b b = new i.b("constants", new a());
    public final i.b c = new i.b("uiHandler", new Handler(Looper.getMainLooper()));

    public final class a extends i {
        public final i.c a = new i.c("ASDomains", null);
        public final i.c b = new i.c("minHwAccelerationVersionBanner", 18);
        public final i.c c = new i.c("minHwAccelerationVersionOverlay", 18);
        public final i.c d = new i.c("minHwAccelerationVersionOverlay", 14);
        public final i.c e = new i.c("mraidBannerPath", "http://media.admob.com/mraid/v1/mraid_app_banner.js");
        public final i.c f = new i.c("mraidExpandedBannerPath", "http://media.admob.com/mraid/v1/mraid_app_expanded_banner.js");
        public final i.c g = new i.c("mraidInterstitialPath", "http://media.admob.com/mraid/v1/mraid_app_interstitial.js");
        public final i.c h = new i.c("badAdReportPath", "https://badad.googleplex.com/s/reportAd");
        public final i.c i = new i.c("appCacheMaxSize", 0L);
        public final i.c j = new i.c("appCacheMaxSizePaddingInBytes", 131072L);
        public final i.c k = new i.c("maxTotalAppCacheQuotaInBytes", 5242880L);
        public final i.c l = new i.c("maxTotalDatabaseQuotaInBytes", 5242880L);
        public final i.c m = new i.c("maxDatabaseQuotaPerOriginInBytes", 1048576L);
        public final i.c n = new i.c("databaseQuotaIncreaseStepInBytes", 131072L);
        public final i.c o = new i.c("isInitialized", false);
    }

    private m() {
    }

    public static m a() {
        return d;
    }
}
