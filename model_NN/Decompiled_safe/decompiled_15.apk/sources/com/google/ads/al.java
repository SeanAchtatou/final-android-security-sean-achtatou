package com.google.ads;

import android.content.Context;
import android.net.Uri;

public class al {
    private String a = "googleads.g.doubleclick.net";
    private String b = "/pagead/ads";
    private String[] c = {".doubleclick.net", ".googleadservices.com", ".googlesyndication.com"};
    private ai d;
    private ah e = new ah();

    public al(ai aiVar) {
        this.d = aiVar;
    }

    private Uri a(Uri uri, Context context, String str, boolean z) {
        try {
            if (uri.getQueryParameter("ms") != null) {
                throw new am("Query parameter already exists: ms");
            }
            return a(uri, "ms", z ? this.d.a(context, str) : this.d.a(context));
        } catch (UnsupportedOperationException e2) {
            throw new am("Provided Uri is not in a valid state");
        }
    }

    private Uri a(Uri uri, String str, String str2) {
        String uri2 = uri.toString();
        int indexOf = uri2.indexOf("&adurl");
        if (indexOf == -1) {
            indexOf = uri2.indexOf("?adurl");
        }
        return indexOf != -1 ? Uri.parse(uri2.substring(0, indexOf + 1) + str + "=" + str2 + "&" + uri2.substring(indexOf + 1)) : uri.buildUpon().appendQueryParameter(str, str2).build();
    }

    public Uri a(Uri uri, Context context) {
        try {
            return a(uri, context, uri.getQueryParameter("ai"), true);
        } catch (UnsupportedOperationException e2) {
            throw new am("Provided Uri is not in a valid state");
        }
    }

    public void a(String str) {
        this.c = str.split(",");
    }

    public boolean a(Uri uri) {
        if (uri == null) {
            throw new NullPointerException();
        }
        try {
            String host = uri.getHost();
            for (String endsWith : this.c) {
                if (host.endsWith(endsWith)) {
                    return true;
                }
            }
            return false;
        } catch (NullPointerException e2) {
            return false;
        }
    }
}
