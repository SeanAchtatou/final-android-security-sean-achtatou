package jp.adlantis.android;

public class GreeAdManager extends AdManager {
    public String byline() {
        return "Ads by GREE";
    }

    /* access modifiers changed from: package-private */
    public AdNetworkConnection createConnection() {
        return new GreeAdConnection();
    }

    public String sdkDescription() {
        return "GREE Ad SDK";
    }
}
