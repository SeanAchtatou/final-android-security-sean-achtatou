package jp.adlantis.android.model;

import java.util.EventListener;

public interface OnChangeListener extends EventListener {
    void onChange(Object obj);
}
