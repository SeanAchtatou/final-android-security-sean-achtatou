package jp.adlantis.android.model;

public interface EasyObservable {
    void addListener(OnChangeListener onChangeListener);

    void removeListener(OnChangeListener onChangeListener);
}
