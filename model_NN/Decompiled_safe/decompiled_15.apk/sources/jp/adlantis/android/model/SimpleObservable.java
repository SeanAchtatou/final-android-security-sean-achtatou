package jp.adlantis.android.model;

import java.util.ArrayList;
import java.util.Iterator;

public class SimpleObservable implements EasyObservable {
    private final ArrayList listeners = new ArrayList();

    public void addListener(OnChangeListener onChangeListener) {
        synchronized (this.listeners) {
            this.listeners.add(onChangeListener);
        }
    }

    /* access modifiers changed from: protected */
    public void notifyListeners(Object obj) {
        synchronized (this.listeners) {
            Iterator it = this.listeners.iterator();
            while (it.hasNext()) {
                ((OnChangeListener) it.next()).onChange(obj);
            }
        }
    }

    public void removeListener(OnChangeListener onChangeListener) {
        synchronized (this.listeners) {
            this.listeners.remove(onChangeListener);
        }
    }
}
