package jp.adlantis.android;

import java.util.ArrayList;
import java.util.Iterator;

public class AdRequestListeners implements AdRequestNotifier {
    protected ArrayList listeners = new ArrayList();

    public void addRequestListener(AdRequestListener adRequestListener) {
        synchronized (this.listeners) {
            this.listeners.add(adRequestListener);
        }
    }

    public void addRequestListeners(ArrayList arrayList) {
        synchronized (this.listeners) {
            this.listeners.addAll(arrayList);
        }
    }

    public void addRequestListeners(AdRequestListeners adRequestListeners) {
        addRequestListeners(adRequestListeners.listeners);
    }

    public void notifyListenersAdReceived(AdRequestNotifier adRequestNotifier) {
        synchronized (this.listeners) {
            Iterator it = this.listeners.iterator();
            while (it.hasNext()) {
                ((AdRequestListener) it.next()).onReceiveAd(adRequestNotifier);
            }
        }
    }

    public void notifyListenersAdTouched(AdRequestNotifier adRequestNotifier) {
        synchronized (this.listeners) {
            Iterator it = this.listeners.iterator();
            while (it.hasNext()) {
                ((AdRequestListener) it.next()).onTouchAd(adRequestNotifier);
            }
        }
    }

    public void notifyListenersFailedToReceiveAd(AdRequestNotifier adRequestNotifier) {
        synchronized (this.listeners) {
            Iterator it = this.listeners.iterator();
            while (it.hasNext()) {
                ((AdRequestListener) it.next()).onFailedToReceiveAd(adRequestNotifier);
            }
        }
    }

    public void removeRequestListener(AdRequestListener adRequestListener) {
        synchronized (this.listeners) {
            this.listeners.remove(adRequestListener);
        }
    }

    public void removeRequestListeners(ArrayList arrayList) {
        synchronized (this.listeners) {
            this.listeners.removeAll(arrayList);
        }
    }

    public void removeRequestListeners(AdRequestListeners adRequestListeners) {
        removeRequestListeners(adRequestListeners.listeners);
    }
}
