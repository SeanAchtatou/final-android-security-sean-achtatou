package jp.adlantis.android;

import java.util.Vector;
import jp.adlantis.android.mediation.AdMediationNetworkParameters;
import jp.adlantis.android.model.OnChangeListener;
import jp.adlantis.android.model.SimpleObservable;

public class AdlantisAdsModel extends SimpleObservable {
    private AdlantisAd[] _ads = null;
    private String errorMessage;
    private AdMediationNetworkParameters[] networkParameters = null;

    public interface AdlantisAdsModelListener extends OnChangeListener {
    }

    public AdlantisAdsModel() {
    }

    public AdlantisAdsModel(AdlantisAd[] adlantisAdArr) {
        this._ads = adlantisAdArr;
    }

    private AdlantisAd[] filteredAdsForOrientation(int i) {
        Vector filteredAdsForOrientationVector = filteredAdsForOrientationVector(i);
        AdlantisAd[] adlantisAdArr = new AdlantisAd[filteredAdsForOrientationVector.size()];
        filteredAdsForOrientationVector.copyInto(adlantisAdArr);
        return adlantisAdArr;
    }

    public int adCount() {
        if (this._ads == null) {
            return 0;
        }
        return this._ads.length;
    }

    public int adCountForOrientation(int i) {
        return filteredAdsForOrientation(i).length;
    }

    public synchronized AdlantisAd[] adsForOrientation(int i) {
        return i == 2 ? filteredAdsForOrientation(2) : filteredAdsForOrientation(1);
    }

    public void clearAds() {
        setAds(null);
    }

    public synchronized Vector filteredAdsForOrientationVector(int i) {
        Vector vector;
        vector = new Vector();
        if (this._ads != null) {
            for (int i2 = 0; i2 < this._ads.length; i2++) {
                if (this._ads[i2].hasAdForOrientation(i)) {
                    vector.addElement(this._ads[i2]);
                }
            }
        }
        return vector;
    }

    public AdlantisAd[] getAds() {
        return this._ads;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public AdMediationNetworkParameters[] getNetworkParameters() {
        return this.networkParameters;
    }

    /* access modifiers changed from: protected */
    public void notifyListeners() {
        notifyListeners(this);
    }

    public void setAds(AdlantisAd[] adlantisAdArr) {
        synchronized (this) {
            this._ads = adlantisAdArr;
        }
        notifyListeners();
    }

    public void setErrorMessage(String str) {
        this.errorMessage = str;
    }

    public void setNetworkParameters(AdMediationNetworkParameters[] adMediationNetworkParametersArr) {
        this.networkParameters = adMediationNetworkParametersArr;
    }
}
