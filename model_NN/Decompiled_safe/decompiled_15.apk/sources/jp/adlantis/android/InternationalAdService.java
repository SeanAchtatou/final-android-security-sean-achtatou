package jp.adlantis.android;

import android.content.Context;
import android.view.View;

public class InternationalAdService extends AdService {
    private String adSpace;

    public InternationalAdService(String str) {
        this.adSpace = str;
    }

    public AdViewAdapter adViewAdapter(Context context) {
        return new NullAdViewAdapter(createAdView(context));
    }

    public View createAdView(Context context) {
        return null;
    }

    public String getAdSpace() {
        return this.adSpace;
    }

    public void pause() {
    }

    public void resume() {
    }
}
