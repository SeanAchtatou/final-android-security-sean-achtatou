package jp.adlantis.android.utils;

import android.net.Uri;

public class ADLStringUtils {
    public static boolean isHttpUrl(String str) {
        if (str == null) {
            return false;
        }
        String scheme = Uri.parse(str).getScheme();
        return "http".equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme);
    }
}
