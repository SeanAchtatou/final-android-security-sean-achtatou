package jp.adlantis.android.utils;

import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import jp.adlantis.android.AdlantisAd;
import org.json.JSONException;
import org.json.JSONObject;

public class AdlantisUtils {
    public static int adHeightForOrientation(int i) {
        return adSizeForOrientation(i).height();
    }

    public static int adHeightPixels(Context context) {
        return displayPointsToPixels(context, adHeightForOrientation(context.getResources().getConfiguration().orientation));
    }

    public static Rect adSizeForOrientation(int i) {
        switch (i) {
            case AdlantisAd.ADTYPE_TEXT:
                return new Rect(0, 0, 480, 32);
            default:
                return new Rect(0, 0, 320, 50);
        }
    }

    public static String convertInputToString(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                break;
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        inputStream.close();
        return sb.toString();
    }

    public static float displayDensity(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    static float displayDensityDpi(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        try {
            Field field = DisplayMetrics.class.getField("densityDpi");
            return field != null ? (float) field.getInt(displayMetrics) : -1.0f;
        } catch (IllegalAccessException | NoSuchFieldException e) {
            return -1.0f;
        }
    }

    public static int displayPointsToPixels(Context context, int i) {
        return (int) (displayDensity(context) * ((float) i));
    }

    public static boolean findClass(String[] strArr) {
        try {
            for (String cls : strArr) {
                if (Class.forName(cls) == null) {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean hasHighResolutionDisplay(Context context) {
        return displayDensityDpi(context) >= 240.0f;
    }

    public static HashMap jsonObjectToHashMap(JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            try {
                Object obj = jSONObject.get(next);
                if (obj instanceof JSONObject) {
                    obj = jsonObjectToHashMap((JSONObject) obj);
                }
                hashMap.put(next, obj);
            } catch (JSONException e) {
            }
        }
        return hashMap;
    }

    public static final String md5(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : digest) {
                String hexString = Integer.toHexString(b & 255);
                while (hexString.length() < 2) {
                    hexString = "0" + hexString;
                }
                stringBuffer.append(hexString);
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static int orientation(View view) {
        return view.getResources().getConfiguration().orientation;
    }

    public static String orientationToString(int i) {
        switch (i) {
            case AdlantisAd.ADTYPE_TEXT:
                return "landscape";
            default:
                return "portrait";
        }
    }

    public static void setUriParamsFromMap(Uri.Builder builder, Map map) {
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                builder.appendQueryParameter((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    public static void setViewVisibilityOnMainThread(final View view, int i) {
        new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message message) {
                view.setVisibility(message.what);
            }
        }.sendEmptyMessage(i);
    }
}
