package jp.adlantis.android.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.HashMap;

public class AsyncImageLoader {
    private HashMap imageCache = new HashMap();

    public interface ImageLoadedCallback {
        void imageLoaded(Drawable drawable, String str);
    }

    protected static InputStream inputStreamForUrl(String str) {
        return new URL(str).openStream();
    }

    public static Drawable loadImageFromUrl(String str) {
        InputStream inputStream;
        Log.d("AsyncImageLoader", "loadImageFromUrl=" + str);
        if (str == null) {
            return null;
        }
        try {
            inputStream = inputStreamForUrl(str);
        } catch (IOException e) {
            System.out.println(e);
            inputStream = null;
        }
        if (inputStream == null) {
            return null;
        }
        try {
            return Drawable.createFromStream(inputStream, "src");
        } catch (OutOfMemoryError e2) {
            Log.e("AsyncImageLoader", "exception calling Drawable.createFromStream() " + e2);
            return null;
        }
    }

    public void clear() {
        this.imageCache.clear();
    }

    public Drawable loadDrawable(Context context, final String str, final ImageLoadedCallback imageLoadedCallback) {
        Drawable drawable;
        IOException e;
        Drawable drawable2;
        if (this.imageCache.containsKey(str) && (drawable2 = (Drawable) ((SoftReference) this.imageCache.get(str)).get()) != null) {
            return drawable2;
        }
        if (context == null || !ADLAssetUtils.isAssetUrl(str)) {
            final AnonymousClass1 r0 = new Handler(Looper.getMainLooper()) {
                public void handleMessage(Message message) {
                    if (imageLoadedCallback != null) {
                        imageLoadedCallback.imageLoaded((Drawable) message.obj, str);
                    }
                }
            };
            new Thread() {
                public void run() {
                    Drawable loadImageFromUrl = AsyncImageLoader.loadImageFromUrl(str);
                    if (loadImageFromUrl != null) {
                        AsyncImageLoader.this.putDrawableInCache(str, loadImageFromUrl);
                        r0.sendMessage(r0.obtainMessage(0, loadImageFromUrl));
                    }
                }
            }.start();
            return null;
        }
        try {
            drawable = Drawable.createFromStream(ADLAssetUtils.inputStreamFromAssetUri(context, Uri.parse(str)), str);
            try {
                putDrawableInCache(str, drawable);
                return drawable;
            } catch (IOException e2) {
                e = e2;
            }
        } catch (IOException e3) {
            IOException iOException = e3;
            drawable = null;
            e = iOException;
        }
        Log.e(getClass().getSimpleName(), "exception calling Drawable.createFromStream() " + e);
        return drawable;
    }

    public void putDrawableInCache(String str, Drawable drawable) {
        this.imageCache.put(str, new SoftReference(drawable));
        Log.d(getClass().getSimpleName(), "imageCache.size()=" + this.imageCache.size());
    }
}
