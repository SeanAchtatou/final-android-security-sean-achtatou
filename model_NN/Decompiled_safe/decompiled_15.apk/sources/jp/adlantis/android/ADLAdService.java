package jp.adlantis.android;

import android.content.Context;
import android.view.View;

public class ADLAdService extends AdService {
    private String publisherId;

    public ADLAdService(String str) {
        this.publisherId = str;
    }

    public AdViewAdapter adViewAdapter(Context context) {
        return new AdlantisViewAdapter(createAdView(context));
    }

    public View createAdView(Context context) {
        return new AdlantisAdViewContainer(context);
    }

    public String getPublisherId() {
        return this.publisherId;
    }

    public void pause() {
    }

    public void resume() {
    }

    public void setPublisherId(String str) {
        this.publisherId = str;
    }
}
