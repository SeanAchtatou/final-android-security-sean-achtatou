package jp.adlantis.android;

import android.content.Context;
import android.telephony.TelephonyManager;
import java.util.Locale;
import jp.adlantis.android.AdService;

public class AdServiceManager {
    ADLAdService adlAdService;
    InternationalAdService internationalAdService;

    private String guessCountry(Context context) {
        if (getTargetingParams().getCountry() != null) {
            return getTargetingParams().getCountry();
        }
        String userCountry = GreeApiDelegator.getUserCountry();
        if (userCountry != null) {
            return userCountry;
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            String simCountryIso = telephonyManager.getSimCountryIso();
            if (simCountryIso != null && !"".equals(simCountryIso)) {
                return simCountryIso;
            }
            String networkCountryIso = telephonyManager.getNetworkCountryIso();
            if (networkCountryIso != null && !"".equals(networkCountryIso)) {
                return networkCountryIso;
            }
        }
        Locale locale = Locale.getDefault();
        return locale != null ? locale.getCountry() : "JP";
    }

    public void addService(AdService adService) {
        if (adService instanceof ADLAdService) {
            setAdlAdService((ADLAdService) adService);
        } else {
            setInternationalAdService((InternationalAdService) adService);
        }
    }

    public AdService getActiveAdService(Context context) {
        if (!isGreeSdk()) {
            return getAdlAdService();
        }
        if (getInternationalAdService() == null) {
            return getAdlAdService();
        }
        String guessCountry = guessCountry(context);
        return ("JP".equalsIgnoreCase(guessCountry) || "JPN".equalsIgnoreCase(guessCountry)) ? getAdlAdService() : getInternationalAdService();
    }

    /* access modifiers changed from: package-private */
    public ADLAdService getAdlAdService() {
        return this.adlAdService;
    }

    /* access modifiers changed from: package-private */
    public InternationalAdService getInternationalAdService() {
        return this.internationalAdService;
    }

    /* access modifiers changed from: package-private */
    public AdService.TargetingParams getTargetingParams() {
        return AdManager.getInstance().getTargetingParam();
    }

    /* access modifiers changed from: package-private */
    public boolean isGreeSdk() {
        return AdManager.isGreeSdk();
    }

    /* access modifiers changed from: package-private */
    public void setAdlAdService(ADLAdService aDLAdService) {
        this.adlAdService = aDLAdService;
    }

    /* access modifiers changed from: package-private */
    public void setInternationalAdService(InternationalAdService internationalAdService2) {
        this.internationalAdService = internationalAdService2;
    }
}
