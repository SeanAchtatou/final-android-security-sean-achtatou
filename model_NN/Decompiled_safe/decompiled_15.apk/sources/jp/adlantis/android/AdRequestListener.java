package jp.adlantis.android;

public interface AdRequestListener {
    void onFailedToReceiveAd(AdRequestNotifier adRequestNotifier);

    void onReceiveAd(AdRequestNotifier adRequestNotifier);

    void onTouchAd(AdRequestNotifier adRequestNotifier);
}
