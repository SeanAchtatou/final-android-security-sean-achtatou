package jp.adlantis.android;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Map;
import jp.adlantis.android.utils.ADLAssetUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.protocol.HttpContext;

public class AdRequest extends NetworkRequest implements AdRequestNotifier {
    protected static String DEBUG_TASK = "AdRequest";
    protected AdlantisAdsModel adModel;
    protected AdRequestListeners listeners = new AdRequestListeners();

    public interface AdRequestManagerCallback {
        void adsLoaded();
    }

    public AdRequest(AdlantisAdsModel adlantisAdsModel) {
        this.adModel = adlantisAdsModel;
    }

    /* access modifiers changed from: protected */
    public Uri adRequestUri(Context context, Map map) {
        return getAdNetworkConnection().adRequestUri(adManager(), context, map);
    }

    public void addRequestListener(AdRequestListener adRequestListener) {
        this.listeners.addRequestListener(adRequestListener);
    }

    public void addRequestListeners(AdRequestListeners adRequestListeners) {
        this.listeners.addRequestListeners(adRequestListeners.listeners);
    }

    public AdlantisAd[] adsForRequestUri(Context context, Uri uri) {
        return AdlantisAd.adsFromJSONInputStream(inputStreamForUri(context, uri));
    }

    public void connect(final Context context, final Map map, final AdRequestManagerCallback adRequestManagerCallback) {
        if (Looper.getMainLooper() == null) {
            log_e("Looper.getMainLooper() == null connect() failed.");
        }
        final AnonymousClass2 r0 = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message message) {
                if (adRequestManagerCallback != null) {
                    adRequestManagerCallback.adsLoaded();
                }
            }
        };
        new Thread() {
            public void run() {
                AdRequest.this.doAdRequest(context, map);
                r0.sendMessage(r0.obtainMessage(0, this));
            }
        }.start();
    }

    public boolean doAdRequest(Context context, Map map) {
        boolean z;
        boolean z2 = false;
        try {
            AdlantisAd[] adsForRequestUri = adsForRequestUri(context, adRequestUri(context, map));
            if (adsForRequestUri != null) {
                if (adsForRequestUri.length > 0) {
                    z2 = true;
                }
                getAdsModel().setAds(adsForRequestUri);
                log_d(adsForRequestUri.length + " ads loaded");
            }
            z = z2;
        } catch (MalformedURLException e) {
            MalformedURLException malformedURLException = e;
            z = false;
            log_e(malformedURLException.toString());
        } catch (IOException e2) {
            IOException iOException = e2;
            z = false;
            log_e(iOException.toString());
        }
        if (z) {
            notifyListenersAdReceived(null);
        } else {
            notifyListenersFailedToReceiveAd(null);
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public AdlantisAdsModel getAdsModel() {
        return this.adModel;
    }

    /* access modifiers changed from: protected */
    public InputStream inputStreamForHttpUri(Uri uri, String str, String str2) {
        AnonymousClass1 r0 = new HttpRequestInterceptor() {
            public void process(HttpRequest httpRequest, HttpContext httpContext) {
                Credentials credentials;
                AuthState authState = (AuthState) httpContext.getAttribute("http.auth.target-scope");
                CredentialsProvider credentialsProvider = (CredentialsProvider) httpContext.getAttribute("http.auth.credentials-provider");
                HttpHost httpHost = (HttpHost) httpContext.getAttribute("http.target_host");
                if (authState.getAuthScheme() == null && (credentials = credentialsProvider.getCredentials(new AuthScope(httpHost.getHostName(), httpHost.getPort()))) != null) {
                    authState.setAuthScheme(new BasicScheme());
                    authState.setCredentials(credentials);
                }
            }
        };
        HttpHost httpHost = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
        AbstractHttpClient httpClientFactory = httpClientFactory();
        httpClientFactory.addRequestInterceptor(r0, 0);
        httpClientFactory.getCredentialsProvider().setCredentials(new AuthScope(httpHost.getHostName(), httpHost.getPort()), new UsernamePasswordCredentials(str, str2));
        String uri2 = uri.toString();
        log_d(uri2);
        return httpClientFactory.execute(new HttpGet(uri2)).getEntity().getContent();
    }

    /* access modifiers changed from: protected */
    public InputStream inputStreamForUri(Context context, Uri uri) {
        if (isHttp(uri)) {
            return inputStreamForHttpUri(uri, "", "");
        }
        if (!isFile(uri) || !ADLAssetUtils.isAssetUri(uri)) {
            return null;
        }
        return ADLAssetUtils.inputStreamFromAssetUri(context, uri);
    }

    public boolean isFile(Uri uri) {
        return uri.getScheme().equals("file");
    }

    public boolean isHttp(Uri uri) {
        return uri.getScheme().equals("http") || uri.getScheme().equals("https");
    }

    public void notifyListenersAdReceived(AdRequestNotifier adRequestNotifier) {
        this.listeners.notifyListenersAdReceived(adRequestNotifier);
    }

    public void notifyListenersFailedToReceiveAd(AdRequestNotifier adRequestNotifier) {
        this.listeners.notifyListenersFailedToReceiveAd(adRequestNotifier);
    }

    public void removeRequestListener(AdRequestListener adRequestListener) {
        this.listeners.removeRequestListener(adRequestListener);
    }
}
