package jp.adlantis.android;

import android.content.Context;
import android.view.View;

public class NullAdService extends AdService {
    public AdViewAdapter adViewAdapter(Context context) {
        return new NullAdViewAdapter(createAdView(context));
    }

    public View createAdView(Context context) {
        return new NullAdView(context);
    }

    public void pause() {
    }

    public void resume() {
    }
}
