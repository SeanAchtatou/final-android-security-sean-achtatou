package jp.adlantis.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;
import java.util.HashMap;
import java.util.Map;
import jp.adlantis.android.AdManager;
import jp.adlantis.android.AdRequest;
import jp.adlantis.android.AdlantisAdsModel;
import jp.adlantis.android.mediation.AdMediationManager;
import jp.adlantis.android.mediation.AdMediationRequest;
import jp.adlantis.android.utils.ADLDebugUtils;
import jp.adlantis.android.utils.AdlantisUtils;
import net.nend.android.NendAdIconLayout;

public class AdlantisAdViewContainer extends RelativeLayout implements AdRequestNotifier {
    static int _animationDuration = 500;
    static final int touchHighlightAnimationDuration = 150;
    private AdlantisAdView[] _adViews;
    AdlantisAdsModel _adsModel = new AdlantisAdsModel();
    private boolean _buttonPressed;
    private int _currentAdIndex = 0;
    private boolean _detachingFromWindow;
    /* access modifiers changed from: private */
    public Handler _handler = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public boolean _handlingUserEvent;
    private long _idNotSpecifiedWarningInterval = 5000;
    private boolean _inOnWindowVisibilityChanged;
    private boolean _layoutComplete;
    private int _onWindowVisibilityChangedVisibility;
    private int _previousAdCount = 0;
    /* access modifiers changed from: private */
    public ProgressBar _processIndicator;
    private ViewFlipper _rootViewFlipper;
    /* access modifiers changed from: private */
    public View _touchHighlight;
    protected String lastUsedPublisherID;
    protected AdRequestListeners listeners = new AdRequestListeners();
    private long mAdFetchInterval = adManager().adFetchInterval();
    protected AdRequest mCurrentAdRequest;
    private Runnable mRotateAdTask = new Runnable() {
        public void run() {
            AdlantisAdViewContainer.this.showNextAd();
            AdlantisAdViewContainer.this._handler.postAtTime(this, SystemClock.uptimeMillis() + AdlantisAdViewContainer.this.adDisplayInterval());
        }
    };
    private Runnable mUpdateAdsTask = new Runnable() {
        public void run() {
            AdlantisAdViewContainer.this.connect();
        }
    };

    /* renamed from: jp.adlantis.android.AdlantisAdViewContainer$11  reason: invalid class name */
    /* synthetic */ class AnonymousClass11 {
        static final /* synthetic */ int[] $SwitchMap$jp$adlantis$android$AdlantisAdViewContainer$AnimationType = new int[AnimationType.values().length];

        static {
            try {
                $SwitchMap$jp$adlantis$android$AdlantisAdViewContainer$AnimationType[AnimationType.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$jp$adlantis$android$AdlantisAdViewContainer$AnimationType[AnimationType.FADE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$jp$adlantis$android$AdlantisAdViewContainer$AnimationType[AnimationType.SLIDE_FROM_RIGHT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$jp$adlantis$android$AdlantisAdViewContainer$AnimationType[AnimationType.SLIDE_FROM_LEFT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public enum AnimationType {
        NONE,
        FADE,
        SLIDE_FROM_RIGHT,
        SLIDE_FROM_LEFT
    }

    public AdlantisAdViewContainer(Context context) {
        super(context);
        commonInit();
    }

    public AdlantisAdViewContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        String attributeValue = attributeSet.getAttributeValue("http://schemas.android.com/apk/res/jp.adlantis.android", "publisherID");
        if (attributeValue != null) {
            setPublisherID(attributeValue);
        }
        commonInit();
    }

    private void adCountChanged() {
        int adCountForCurrentOrientation = adCountForCurrentOrientation();
        if (this._rootViewFlipper == null) {
            Log.w(getClass().getSimpleName(), "adCountChanged called when _rootViewFlipper is not available");
        } else if (adCountForCurrentOrientation > 0 && this._previousAdCount == 0) {
            this._rootViewFlipper.setVisibility(0);
            this._rootViewFlipper.startAnimation(fadeInAnimation());
        } else if (adCountForCurrentOrientation == 0 && this._previousAdCount > 0) {
            this._rootViewFlipper.startAnimation(fadeOutAnimation());
            this._rootViewFlipper.setVisibility(4);
        }
        this._previousAdCount = adCountForCurrentOrientation;
    }

    /* access modifiers changed from: private */
    public long adDisplayInterval() {
        return adManager().adDisplayInterval();
    }

    private long adFetchInterval() {
        return this.mAdFetchInterval;
    }

    public static int animationDuration() {
        return _animationDuration;
    }

    private void commonInit() {
        setupLayout();
        setupModelListener();
        if (getPublisherID() == null) {
            String packagePublisherID = packagePublisherID();
            if (packagePublisherID != null) {
                if (AdManager.isGreeSdk()) {
                    setGapPublisherID(packagePublisherID);
                } else {
                    setPublisherID(packagePublisherID);
                }
            }
        } else if (hasAdsForCurrentOrientation()) {
            startTimers();
        } else {
            connect();
        }
        showToastIfPublisherIdNotSpecified();
    }

    /* access modifiers changed from: private */
    public void connectIfPublisherIDChanged() {
        if (publisherIdChanged()) {
            connect();
        }
    }

    private AdlantisAd currentAd() {
        AdlantisAd[] adsForCurrentOrientation = adsForCurrentOrientation();
        if (adsForCurrentOrientation == null || adsForCurrentOrientation.length <= 0 || this._currentAdIndex >= adsForCurrentOrientation.length) {
            return null;
        }
        return adsForCurrentOrientation[this._currentAdIndex];
    }

    static int defaultBackgroundColor() {
        return ViewSettings.defaultBackgroundColor();
    }

    static Animation fadeInAnimation() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration((long) animationDuration());
        return alphaAnimation;
    }

    static Animation fadeOutAnimation() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration((long) animationDuration());
        return alphaAnimation;
    }

    private void handleUserTouchUp() {
        AdlantisAd currentAd = currentAd();
        if (currentAd != null && !this._handlingUserEvent) {
            this._handlingUserEvent = true;
            this._processIndicator.setVisibility(0);
            currentAd.sendImpressionCount();
            if (currentAd.tapUrlString() != null) {
                handleClickRequest(currentAd);
            } else {
                this._handlingUserEvent = false;
            }
        }
    }

    static Animation inFromLeftAnimation() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, -1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration((long) animationDuration());
        return translateAnimation;
    }

    static Animation inFromRightAnimation() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration((long) animationDuration());
        return translateAnimation;
    }

    private boolean inView(MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        boolean z = x >= 0.0f && x <= ((float) getWidth()) && y >= 0.0f && y <= ((float) getHeight());
        return z ? motionEvent.getEdgeFlags() == 0 : z;
    }

    /* access modifiers changed from: private */
    public boolean openUri(Uri uri) {
        Intent intent = new Intent("android.intent.action.VIEW", uri);
        intent.addFlags(268435456);
        try {
            getContext().startActivity(intent);
            return true;
        } catch (ActivityNotFoundException e) {
            Log.e(getClass().getSimpleName(), "activity not found for url=" + uri + " exception=" + e);
            return false;
        }
    }

    static Animation outToLeftAnimation() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 0.0f, 2, -1.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration((long) animationDuration());
        return translateAnimation;
    }

    static Animation outToRightAnimation() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 0.0f, 2, 1.0f, 2, 0.0f, 2, 0.0f);
        translateAnimation.setDuration((long) animationDuration());
        return translateAnimation;
    }

    private String packagePublisherID() {
        try {
            ApplicationInfo applicationInfo = getContext().getPackageManager().getApplicationInfo(getContext().getPackageName(), 128);
            if (!(applicationInfo == null || applicationInfo.metaData == null)) {
                if (adManager().hasTestAdRequestUrls()) {
                    String str = (String) applicationInfo.metaData.get("Adlantis_adRequestUrl");
                    String[] strArr = {str};
                    if (str != null) {
                        adManager().setTestAdRequestUrls(strArr);
                    }
                }
                String str2 = (String) applicationInfo.metaData.get("Adlantis_keywords");
                if (str2 != null) {
                    adManager().setKeywords(str2);
                }
                String str3 = (String) applicationInfo.metaData.get("Adlantis_host");
                if (str3 != null) {
                    adManager().setHost(str3);
                }
                return (String) applicationInfo.metaData.get(AdManager.getInstance().publisherIDMetadataKey());
            }
        } catch (Exception e) {
            logD("packagePublisherID" + e);
        }
        return null;
    }

    private void setAdByIndex(int i) {
        logD("setAdByIndex=" + i + " this=" + this);
        if (this._rootViewFlipper == null) {
            logD("setAdByIndex _rootViewFlipper == null");
            return;
        }
        int adCountForCurrentOrientation = adCountForCurrentOrientation();
        if (adCountForCurrentOrientation != 0) {
            if (i >= adCountForCurrentOrientation) {
                i = 0;
            }
            (this._rootViewFlipper.getCurrentView() == this._adViews[0] ? this._adViews[1] : this._adViews[0]).setAdByIndex(i);
            logD("Animation: " + getClass().getSimpleName() + ".adCountChanged _rootViewFlipper.showNext view = " + this);
            this._rootViewFlipper.showNext();
        }
    }

    public static void setAnimationDuration(int i) {
        _animationDuration = i;
    }

    private void setButtonState(boolean z) {
        if (z != this._buttonPressed) {
            if (z) {
                this._touchHighlight.setVisibility(0);
                this._touchHighlight.startAnimation(touchFadeIn());
                setPressed(true);
            } else {
                this._touchHighlight.startAnimation(touchFadeOut());
                setPressed(false);
            }
            this._buttonPressed = z;
        }
    }

    /* access modifiers changed from: private */
    public void setupLayout() {
        setClickable(true);
        this._rootViewFlipper = createRootViewFlipper();
        addView(this._rootViewFlipper, rootViewFlipperLayoutParams());
        View createTouchHighlight = createTouchHighlight();
        this._touchHighlight = createTouchHighlight;
        addView(createTouchHighlight);
        this._processIndicator = createProgressBar();
        addView(this._processIndicator);
        this._layoutComplete = true;
        logD("setupLayout setting _layoutComplete = true");
        setAnimationType(AnimationType.FADE);
        if (hasAdsForCurrentOrientation()) {
            logD("setupLayout calling setAdByIndex");
            this._currentAdIndex = 0;
            setAdByIndex(0);
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: private */
    public void startTimers() {
        if (hasAdsForCurrentOrientation()) {
            this._handler.removeCallbacks(this.mRotateAdTask);
            this._handler.postDelayed(this.mRotateAdTask, adDisplayInterval());
        }
        this._handler.removeCallbacks(this.mUpdateAdsTask);
        if (this.mAdFetchInterval > 0) {
            this._handler.postDelayed(this.mUpdateAdsTask, adFetchInterval());
        }
    }

    private void stopTimers() {
        if (this._handler != null) {
            this._handler.removeCallbacks(this.mRotateAdTask);
            this._handler.removeCallbacks(this.mUpdateAdsTask);
        }
    }

    private Animation touchFadeIn() {
        Animation fadeInAnimation = fadeInAnimation();
        fadeInAnimation.setDuration(150);
        fadeInAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        return fadeInAnimation;
    }

    private Animation touchFadeOut() {
        Animation fadeOutAnimation = fadeOutAnimation();
        fadeOutAnimation.setDuration(150);
        fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                AdlantisAdViewContainer.this._touchHighlight.setVisibility(4);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        return fadeOutAnimation;
    }

    private void viewVisibilityChanged() {
        if (shouldRunAdTimers()) {
            startTimers();
        } else {
            stopTimers();
        }
    }

    /* access modifiers changed from: protected */
    public int adCountForCurrentOrientation() {
        return getModel().adCountForOrientation(orientation());
    }

    /* access modifiers changed from: protected */
    public AdManager adManager() {
        return AdManager.getInstance();
    }

    public void addRequestListener(AdRequestListener adRequestListener) {
        this.listeners.addRequestListener(adRequestListener);
    }

    public Map additionalParametersForAdRequest() {
        HashMap hashMap = new HashMap();
        hashMap.put("orientation", orientationString());
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public AdlantisAd[] adsForCurrentOrientation() {
        return getModel().adsForOrientation(orientation());
    }

    /* access modifiers changed from: protected */
    public void adsWereLoaded() {
        adCountChanged();
        if (hasAdsForCurrentOrientation()) {
            this._currentAdIndex = 0;
            if (this._layoutComplete) {
                setAdByIndex(this._currentAdIndex);
            } else {
                logD("adsWereLoaded() layout not complete!!!");
            }
        }
        startTimers();
    }

    public void clearAds() {
        this._handler.removeCallbacks(this.mRotateAdTask);
        getModel().clearAds();
        adCountChanged();
    }

    public void connect() {
        if (getPublisherID() != null) {
            logD("connect view =" + this);
            this.lastUsedPublisherID = getPublisherID();
            createAdRequest().connect(getContext(), additionalParametersForAdRequest(), new AdRequest.AdRequestManagerCallback() {
                public void adsLoaded() {
                    if (!AdManager.isGreeSdk()) {
                        AdlantisAdViewContainer adlantisAdViewContainer = AdlantisAdViewContainer.this;
                        if (AdlantisAdViewContainer.this._adsModel.getNetworkParameters() == null || AdlantisAdViewContainer.this._adsModel.getNetworkParameters().length <= 0) {
                            AdMediationManager.getInstance().destroy();
                            if (adlantisAdViewContainer.getChildCount() == 0) {
                                AdlantisAdViewContainer.this.setupLayout();
                                return;
                            }
                            return;
                        }
                        AdlantisAdViewContainer.this.logD("start ad mediation...");
                        AdMediationManager.getInstance().requestAd((Activity) adlantisAdViewContainer.getContext(), adlantisAdViewContainer, AdlantisAdViewContainer.this._adsModel.getNetworkParameters());
                    }
                }
            });
            return;
        }
        Log.e(getClass().getSimpleName(), getClass().getSimpleName() + ": can't connect because publisherID hasn't been set.");
    }

    public AdRequest createAdRequest() {
        this.mCurrentAdRequest = new AdMediationRequest(getModel());
        logD("createAdRequest adRequest = " + this.mCurrentAdRequest);
        this.mCurrentAdRequest.addRequestListener(new AdRequestListener() {
            public void onFailedToReceiveAd(AdRequestNotifier adRequestNotifier) {
                AdlantisAdViewContainer.this.logD("onFailedToReceiveAd adRequest = " + AdlantisAdViewContainer.this.mCurrentAdRequest);
                AdlantisAdViewContainer.this.listeners.notifyListenersFailedToReceiveAd(adRequestNotifier);
                AdlantisAdViewContainer.this.mCurrentAdRequest = null;
            }

            public void onReceiveAd(AdRequestNotifier adRequestNotifier) {
                AdlantisAdViewContainer.this.logD("onReceiveAd adRequest = " + AdlantisAdViewContainer.this.mCurrentAdRequest);
                AdlantisAdViewContainer.this.listeners.notifyListenersAdReceived(adRequestNotifier);
                AdlantisAdViewContainer.this.mCurrentAdRequest = null;
            }

            public void onTouchAd(AdRequestNotifier adRequestNotifier) {
            }
        });
        return this.mCurrentAdRequest;
    }

    /* access modifiers changed from: protected */
    public AdlantisAdView createAdlantisAdView() {
        AdlantisAdView adlantisAdView = new AdlantisAdView(getContext());
        adlantisAdView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        adlantisAdView.setAdsModel(getModel());
        return adlantisAdView;
    }

    /* access modifiers changed from: protected */
    public ProgressBar createProgressBar() {
        ProgressBar progressBar = new ProgressBar(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        progressBar.setLayoutParams(layoutParams);
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(4);
        return progressBar;
    }

    /* access modifiers changed from: protected */
    public ViewFlipper createRootViewFlipper() {
        AdlantisViewFlipper adlantisViewFlipper = new AdlantisViewFlipper(getContext());
        adlantisViewFlipper.setBackgroundColor(defaultBackgroundColor());
        if (!hasAdsForCurrentOrientation()) {
            adlantisViewFlipper.setVisibility(4);
        }
        this._adViews = new AdlantisAdView[2];
        for (int i = 0; i < this._adViews.length; i++) {
            this._adViews[i] = createAdlantisAdView();
            adlantisViewFlipper.addView(this._adViews[i]);
        }
        return adlantisViewFlipper;
    }

    /* access modifiers changed from: protected */
    public View createTouchHighlight() {
        View view = new View(getContext());
        view.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        view.setBackgroundColor(1728053247);
        view.setVisibility(4);
        return view;
    }

    /* access modifiers changed from: protected */
    public void dumpLayoutGeometry() {
        ADLDebugUtils.dumpSubviewLayout(this, 0);
    }

    /* access modifiers changed from: protected */
    public AdlantisAdsModel getModel() {
        return this._adsModel;
    }

    public String getPublisherID() {
        return adManager().getPublisherID();
    }

    /* access modifiers changed from: protected */
    public int getWindowCurrentVisibility() {
        return this._inOnWindowVisibilityChanged ? this._onWindowVisibilityChangedVisibility : getWindowVisibility();
    }

    /* access modifiers changed from: protected */
    public void handleClickRequest(AdlantisAd adlantisAd) {
        adManager().handleClickRequest(adlantisAd, new AdManager.AdManagerRedirectUrlProcessedCallback() {
            public void redirectProcessed(Uri uri) {
                boolean access$400 = AdlantisAdViewContainer.this.openUri(uri);
                AdlantisAdViewContainer.this._processIndicator.setVisibility(4);
                boolean unused = AdlantisAdViewContainer.this._handlingUserEvent = false;
                if (!access$400) {
                    AdlantisAdViewContainer.this.startTimers();
                }
            }
        });
        logD("onTouchAd AdRequestNotifier = " + this);
        this.listeners.notifyListenersAdTouched(this);
    }

    /* access modifiers changed from: protected */
    public boolean hasAdsForCurrentOrientation() {
        return adCountForCurrentOrientation() > 0;
    }

    /* access modifiers changed from: protected */
    public boolean isAttachedToWindow() {
        return !this._detachingFromWindow && getWindowToken() != null && getWindowToken().pingBinder();
    }

    /* access modifiers changed from: package-private */
    public boolean isDoingAdRequest() {
        return this.mCurrentAdRequest != null;
    }

    /* access modifiers changed from: protected */
    public void logD(String str) {
        Log.d(getClass().getSimpleName(), str);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        viewVisibilityChanged();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this._detachingFromWindow = true;
        super.onDetachedFromWindow();
        viewVisibilityChanged();
        this._detachingFromWindow = false;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this._rootViewFlipper.setLayoutParams(rootViewFlipperLayoutParams());
        postRequestLayout();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (currentAd() == null) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case NendAdIconLayout.HORIZONTAL /*0*/:
                stopTimers();
                setButtonState(true);
                break;
            case 1:
                setButtonState(false);
                if (!inView(motionEvent)) {
                    startTimers();
                    break;
                } else {
                    handleUserTouchUp();
                    break;
                }
            case AdlantisAd.ADTYPE_TEXT:
                setButtonState(inView(motionEvent));
                break;
            case 3:
            case 4:
                setButtonState(false);
                startTimers();
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        this._inOnWindowVisibilityChanged = true;
        this._onWindowVisibilityChangedVisibility = i;
        super.onWindowVisibilityChanged(i);
        viewVisibilityChanged();
        this._inOnWindowVisibilityChanged = false;
    }

    /* access modifiers changed from: protected */
    public int orientation() {
        return AdlantisUtils.orientation(this);
    }

    /* access modifiers changed from: protected */
    public String orientationString() {
        return AdlantisUtils.orientationToString(orientation());
    }

    /* access modifiers changed from: protected */
    public void postRequestLayout() {
        post(new Runnable() {
            public void run() {
                AdlantisAdViewContainer.this.requestLayout();
            }
        });
    }

    public boolean publisherIdChanged() {
        String publisherID = getPublisherID();
        return publisherID != this.lastUsedPublisherID || (publisherID != null && !publisherID.equals(this.lastUsedPublisherID));
    }

    public void removeRequestListener(AdRequestListener adRequestListener) {
        this.listeners.removeRequestListener(adRequestListener);
    }

    /* access modifiers changed from: protected */
    public RelativeLayout.LayoutParams rootViewFlipperLayoutParams() {
        Rect adSizeForOrientation = AdlantisUtils.adSizeForOrientation(orientation());
        float displayDensity = AdlantisUtils.displayDensity(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (((float) adSizeForOrientation.width()) * displayDensity), (int) (((float) adSizeForOrientation.height()) * displayDensity));
        layoutParams.addRule(13);
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public void setAdFetchInterval(long j) {
        this.mAdFetchInterval = j;
        startTimers();
    }

    public void setAnimationType(AnimationType animationType) {
        if (this._rootViewFlipper != null) {
            switch (AnonymousClass11.$SwitchMap$jp$adlantis$android$AdlantisAdViewContainer$AnimationType[animationType.ordinal()]) {
                case 1:
                    this._rootViewFlipper.setInAnimation(null);
                    this._rootViewFlipper.setOutAnimation(null);
                    return;
                case AdlantisAd.ADTYPE_TEXT:
                    this._rootViewFlipper.setInAnimation(fadeInAnimation());
                    this._rootViewFlipper.setOutAnimation(fadeOutAnimation());
                    return;
                case 3:
                    this._rootViewFlipper.setInAnimation(inFromRightAnimation());
                    this._rootViewFlipper.setOutAnimation(outToLeftAnimation());
                    return;
                case 4:
                    this._rootViewFlipper.setInAnimation(inFromLeftAnimation());
                    this._rootViewFlipper.setOutAnimation(outToRightAnimation());
                    return;
                default:
                    return;
            }
        }
    }

    public void setGapPublisherID(String str) {
        adManager().setGapPublisherID(str);
        connectIfPublisherIDChanged();
    }

    public void setKeywords(String str) {
        adManager().setKeywords(str);
    }

    public void setPublisherID(String str) {
        adManager().setPublisherID(str);
        connectIfPublisherIDChanged();
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        viewVisibilityChanged();
    }

    public void setupModelListener() {
        getModel().addListener(new AdlantisAdsModel.AdlantisAdsModelListener() {
            public void onChange(AdlantisAdsModel adlantisAdsModel) {
                AdlantisAdViewContainer.this._handler.post(new Runnable() {
                    public void run() {
                        AdlantisAdViewContainer.this.adsWereLoaded();
                    }
                });
            }
        });
    }

    /* access modifiers changed from: protected */
    public boolean shouldRunAdTimers() {
        return getParent() != null && getVisibility() == 0 && isAttachedToWindow() && getWindowCurrentVisibility() == 0;
    }

    public void showNextAd() {
        int adCountForCurrentOrientation = adCountForCurrentOrientation();
        if (adCountForCurrentOrientation > 1) {
            this._currentAdIndex = (this._currentAdIndex + 1) % adCountForCurrentOrientation;
            setAdByIndex(this._currentAdIndex);
        }
    }

    public void showToastIfPublisherIdNotSpecified() {
        if (getPublisherID() == null) {
            this._handler.postDelayed(new Runnable() {
                public void run() {
                    if (AdlantisAdViewContainer.this.getPublisherID() == null) {
                        Toast.makeText(AdlantisAdViewContainer.this.getContext(), "AdlantisView publisher id not set", 1).show();
                        Log.e(getClass().getSimpleName(), getClass().getSimpleName() + ": can't display ads because publisherID hasn't been set.");
                    } else if (!AdlantisAdViewContainer.this.isDoingAdRequest()) {
                        AdlantisAdViewContainer.this.connectIfPublisherIDChanged();
                    }
                }
            }, this._idNotSpecifiedWarningInterval);
        }
    }
}
