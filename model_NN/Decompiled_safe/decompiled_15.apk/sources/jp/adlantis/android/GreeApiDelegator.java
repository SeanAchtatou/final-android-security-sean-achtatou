package jp.adlantis.android;

import android.util.Log;
import java.lang.reflect.Field;

public class GreeApiDelegator {
    static final String digestClassSpecifier = "net.gree.asdk.core.codec.Digest";
    static final String greeApiClassSpecifier = "net.gree.asdk.api.GreePlatform";
    static final String[] greeClasses = {greeApiClassSpecifier, digestClassSpecifier};

    protected static Class getGreePlatformClass() {
        return Class.forName(greeApiClassSpecifier);
    }

    public static String getSha1DigestInString(String str) {
        try {
            Class<?> cls = Class.forName(digestClassSpecifier);
            Object newInstance = cls.getConstructor(String.class).newInstance("SHA-1");
            return (String) cls.getMethod("getDigestInString", String.class).invoke(newInstance, str);
        } catch (Exception e) {
            Log.e("GreeApiDelegator", "getSha1DigestInString exception=" + e);
            logProGuardError();
            return null;
        }
    }

    public static String getUserCountry() {
        return getUserFieldValue("region");
    }

    private static String getUserFieldValue(String str) {
        try {
            Class greePlatformClass = getGreePlatformClass();
            Object invoke = greePlatformClass.getMethod("getLocalUser", new Class[0]).invoke(greePlatformClass, new Object[0]);
            if (invoke == null) {
                return null;
            }
            Field declaredField = invoke.getClass().getDeclaredField(str);
            declaredField.setAccessible(true);
            return (String) declaredField.get(invoke);
        } catch (Exception e) {
            Log.e("GreeApiDelegator", "getUserField('" + str + "') exception=" + e);
            logProGuardError();
            return null;
        }
    }

    public static String getUserId() {
        return getUserFieldValue("id");
    }

    public static boolean greePlatformAvailable() {
        try {
            return getGreePlatformClass() != null;
        } catch (Exception e) {
            return false;
        }
    }

    private static void logProGuardError() {
        Log.e("GreeApiDelegator", "If using ProGuard, include the following lines in your proguard.cfg file:");
        String[] strArr = greeClasses;
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            Log.e("GreeApiDelegator", " -keep public class " + strArr[i] + " { public static *; }");
        }
    }
}
