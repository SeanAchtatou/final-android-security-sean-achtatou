package jp.adlantis.android;

import android.app.Activity;
import android.widget.RelativeLayout;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.customevent.CustomEventBanner;
import com.google.ads.mediation.customevent.CustomEventBannerListener;

public class AdlantisCustomEventBanner implements CustomEventBanner, AdRequestListener {
    private AdlantisView adView;
    private CustomEventBannerListener bannerListener;

    public void destroy() {
    }

    public void onFailedToReceiveAd(AdRequestNotifier adRequestNotifier) {
        this.bannerListener.onFailedToReceiveAd();
    }

    public void onReceiveAd(AdRequestNotifier adRequestNotifier) {
        this.bannerListener.onReceivedAd(this.adView);
    }

    public void onTouchAd(AdRequestNotifier adRequestNotifier) {
        this.bannerListener.onClick();
    }

    public void requestBannerAd(CustomEventBannerListener customEventBannerListener, Activity activity, String str, String str2, AdSize adSize, MediationAdRequest mediationAdRequest, Object obj) {
        this.bannerListener = customEventBannerListener;
        this.adView = new AdlantisView(activity);
        this.adView.setAdFetchInterval(0);
        this.adView.addRequestListener(this);
        this.adView.setLayoutParams(new RelativeLayout.LayoutParams(-2, adSize.getHeightInPixels(activity)));
        this.adView.setPublisherID(str2);
    }
}
