package jp.adlantis.android;

import android.view.View;
import android.view.ViewParent;

public class AdlantisViewAdapter extends AdViewAdapter {
    public AdlantisViewAdapter(View view) {
        super(view);
        ((AdlantisAdViewContainer) this.adView).addRequestListener(new AdRequestListener() {
            public void onFailedToReceiveAd(AdRequestNotifier adRequestNotifier) {
                AdlantisViewAdapter.this.listeners.notifyListenersFailedToReceiveAd(AdlantisViewAdapter.this.adlantisView());
            }

            public void onReceiveAd(AdRequestNotifier adRequestNotifier) {
                AdlantisViewAdapter.this.listeners.notifyListenersAdReceived(AdlantisViewAdapter.this.adlantisView());
            }

            public void onTouchAd(AdRequestNotifier adRequestNotifier) {
                AdlantisViewAdapter.this.listeners.notifyListenersAdTouched(AdlantisViewAdapter.this.adlantisView());
            }
        });
    }

    /* access modifiers changed from: protected */
    public AdlantisView adlantisView() {
        ViewParent parent = this.adView.getParent();
        if (parent instanceof AdlantisView) {
            return (AdlantisView) parent;
        }
        return null;
    }

    public void clearAds() {
        ((AdlantisAdViewContainer) this.adView).clearAds();
    }

    public void connect() {
        ((AdlantisAdViewContainer) this.adView).connect();
    }
}
