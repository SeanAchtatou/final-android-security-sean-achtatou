package jp.adlantis.android;

import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import java.util.HashMap;
import java.util.Map;
import jp.adlantis.android.AdService;
import jp.adlantis.android.utils.AdlantisUtils;

public abstract class AdNetworkConnection {
    protected String _conversionTagHost;
    protected String _conversionTagTestHost;
    private HashMap _defaultParamMap;
    protected String _host;
    private int _testAdRequestUrlIndex;
    private String[] _testAdRequestUrls;

    private Uri adRequestURI_internal(AdManager adManager, Context context, Map map) {
        Uri.Builder defaultRequestBuilder = defaultRequestBuilder(context, null);
        defaultRequestBuilder.scheme("http");
        defaultRequestBuilder.authority(getHost());
        defaultRequestBuilder.path("/sp/load_app_ads");
        defaultRequestBuilder.appendQueryParameter("callbackid", "0");
        defaultRequestBuilder.appendQueryParameter("zid", adManager.getPublisherID());
        defaultRequestBuilder.appendQueryParameter("adl_app_flg", "1");
        if (adManager.keywords() != null) {
            defaultRequestBuilder.appendQueryParameter("keywords", adManager.keywords());
        }
        if (map != null) {
            AdlantisUtils.setUriParamsFromMap(defaultRequestBuilder, map);
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        defaultRequestBuilder.appendQueryParameter("displaySize", displayMetrics.widthPixels + "x" + displayMetrics.heightPixels);
        defaultRequestBuilder.appendQueryParameter("displayDensity", Float.toString(displayMetrics.density));
        return defaultRequestBuilder.build();
    }

    private Uri.Builder appendTargetingParameters(Uri.Builder builder) {
        AdService.TargetingParams targetingParam = AdManager.getInstance().getTargetingParam();
        String country = targetingParam.getCountry();
        if (country != null) {
            builder.appendQueryParameter("country", country);
        }
        String locale = targetingParam.getLocale();
        if (locale != null) {
            builder.appendQueryParameter("locale", locale);
        }
        Location location = targetingParam.getLocation();
        if (location != null) {
            builder.appendQueryParameter("lat", Double.toString(location.getLatitude()));
            builder.appendQueryParameter("lng", Double.toString(location.getLongitude()));
        }
        return builder;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.HashMap defaultParameters(android.content.Context r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.util.HashMap r0 = r3._defaultParamMap     // Catch:{ all -> 0x00b6 }
            if (r0 == 0) goto L_0x0009
            java.util.HashMap r0 = r3._defaultParamMap     // Catch:{ all -> 0x00b6 }
            monitor-exit(r3)     // Catch:{ all -> 0x00b6 }
        L_0x0008:
            return r0
        L_0x0009:
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x00b6 }
            r0.<init>()     // Catch:{ all -> 0x00b6 }
            r3._defaultParamMap = r0     // Catch:{ all -> 0x00b6 }
            if (r4 == 0) goto L_0x001d
            java.util.HashMap r0 = r3._defaultParamMap     // Catch:{ all -> 0x00b6 }
            java.lang.String r1 = "appIdentifier"
            java.lang.String r2 = r4.getPackageName()     // Catch:{ all -> 0x00b6 }
            r0.put(r1, r2)     // Catch:{ all -> 0x00b6 }
        L_0x001d:
            java.util.HashMap r0 = r3._defaultParamMap     // Catch:{ all -> 0x00b6 }
            java.lang.String r1 = "deviceClass"
            java.lang.String r2 = "android"
            r0.put(r1, r2)     // Catch:{ all -> 0x00b6 }
            java.lang.String r0 = android.os.Build.VERSION.RELEASE     // Catch:{ all -> 0x00b6 }
            if (r0 == 0) goto L_0x0044
            java.util.HashMap r1 = r3._defaultParamMap     // Catch:{ all -> 0x00b6 }
            java.lang.String r2 = "deviceOsVersionFull"
            r1.put(r2, r0)     // Catch:{ all -> 0x00b6 }
            java.text.NumberFormat r1 = java.text.NumberFormat.getNumberInstance()     // Catch:{ ParseException -> 0x00b1 }
            java.lang.Number r0 = r1.parse(r0)     // Catch:{ ParseException -> 0x00b1 }
            java.lang.String r0 = r0.toString()     // Catch:{ ParseException -> 0x00b1 }
            java.util.HashMap r1 = r3._defaultParamMap     // Catch:{ ParseException -> 0x00b1 }
            java.lang.String r2 = "deviceOsVersion"
            r1.put(r2, r0)     // Catch:{ ParseException -> 0x00b1 }
        L_0x0044:
            java.lang.String r0 = android.os.Build.MODEL     // Catch:{ all -> 0x00b6 }
            if (r0 == 0) goto L_0x0059
            java.lang.String r1 = "sdk"
            int r1 = r0.compareTo(r1)     // Catch:{ all -> 0x00b6 }
            if (r1 != 0) goto L_0x0052
            java.lang.String r0 = "simulator"
        L_0x0052:
            java.util.HashMap r1 = r3._defaultParamMap     // Catch:{ all -> 0x00b6 }
            java.lang.String r2 = "deviceFamily"
            r1.put(r2, r0)     // Catch:{ all -> 0x00b6 }
        L_0x0059:
            java.lang.String r0 = android.os.Build.BRAND     // Catch:{ all -> 0x00b6 }
            if (r0 == 0) goto L_0x0066
            java.util.HashMap r0 = r3._defaultParamMap     // Catch:{ all -> 0x00b6 }
            java.lang.String r1 = "deviceBrand"
            java.lang.String r2 = android.os.Build.BRAND     // Catch:{ all -> 0x00b6 }
            r0.put(r1, r2)     // Catch:{ all -> 0x00b6 }
        L_0x0066:
            java.lang.String r0 = android.os.Build.DEVICE     // Catch:{ all -> 0x00b6 }
            if (r0 == 0) goto L_0x0073
            java.util.HashMap r0 = r3._defaultParamMap     // Catch:{ all -> 0x00b6 }
            java.lang.String r1 = "deviceName"
            java.lang.String r2 = android.os.Build.DEVICE     // Catch:{ all -> 0x00b6 }
            r0.put(r1, r2)     // Catch:{ all -> 0x00b6 }
        L_0x0073:
            java.lang.String r0 = r3.md5_uniqueID(r4)     // Catch:{ all -> 0x00b6 }
            if (r0 == 0) goto L_0x0080
            java.util.HashMap r1 = r3._defaultParamMap     // Catch:{ all -> 0x00b6 }
            java.lang.String r2 = "udid"
            r1.put(r2, r0)     // Catch:{ all -> 0x00b6 }
        L_0x0080:
            java.lang.String r0 = jp.adlantis.android.GreeDsp.getUUID()     // Catch:{ all -> 0x00b6 }
            if (r0 == 0) goto L_0x008d
            java.util.HashMap r1 = r3._defaultParamMap     // Catch:{ all -> 0x00b6 }
            java.lang.String r2 = "uuid"
            r1.put(r2, r0)     // Catch:{ all -> 0x00b6 }
        L_0x008d:
            java.util.HashMap r0 = r3._defaultParamMap     // Catch:{ all -> 0x00b6 }
            java.lang.String r1 = "sdkVersion"
            java.lang.String r2 = r3.sdkVersion()     // Catch:{ all -> 0x00b6 }
            r0.put(r1, r2)     // Catch:{ all -> 0x00b6 }
            java.util.HashMap r0 = r3._defaultParamMap     // Catch:{ all -> 0x00b6 }
            java.lang.String r1 = "sdkBuild"
            java.lang.String r2 = r3.sdkBuild()     // Catch:{ all -> 0x00b6 }
            r0.put(r1, r2)     // Catch:{ all -> 0x00b6 }
            java.util.HashMap r0 = r3._defaultParamMap     // Catch:{ all -> 0x00b6 }
            java.lang.String r1 = "adlProtocolVersion"
            java.lang.String r2 = "3"
            r0.put(r1, r2)     // Catch:{ all -> 0x00b6 }
            monitor-exit(r3)     // Catch:{ all -> 0x00b6 }
            java.util.HashMap r0 = r3._defaultParamMap
            goto L_0x0008
        L_0x00b1:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00b6 }
            goto L_0x0044
        L_0x00b6:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00b6 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.adlantis.android.AdNetworkConnection.defaultParameters(android.content.Context):java.util.HashMap");
    }

    public Uri adRequestUri(AdManager adManager, Context context, Map map) {
        if (this._testAdRequestUrls == null || this._testAdRequestUrls.length <= 0) {
            return adRequestURI_internal(adManager, context, map);
        }
        adRequestURI_internal(adManager, context, map);
        Uri parse = Uri.parse(this._testAdRequestUrls[this._testAdRequestUrlIndex]);
        this._testAdRequestUrlIndex = (this._testAdRequestUrlIndex + 1) % this._testAdRequestUrls.length;
        return parse;
    }

    public String androidId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    public Uri.Builder appendParameters(Uri.Builder builder) {
        return builder;
    }

    public String buildCompleteHttpUri(Context context, String str) {
        return defaultRequestBuilder(context, Uri.parse(str)).build().toString();
    }

    public Uri conversionTagRequestUri(Context context, String str, boolean z) {
        Uri.Builder defaultRequestBuilder = defaultRequestBuilder(context, null);
        defaultRequestBuilder.scheme("http");
        if (z) {
            defaultRequestBuilder.authority(getConversionTagTestHost());
            defaultRequestBuilder.path("/ctt");
        } else {
            defaultRequestBuilder.authority(getConversionTagHost());
            defaultRequestBuilder.path("/sp/conv");
        }
        defaultRequestBuilder.appendQueryParameter("tid", str);
        defaultRequestBuilder.appendQueryParameter("output", "js");
        return defaultRequestBuilder.build();
    }

    public Uri.Builder defaultRequestBuilder(Context context, Uri uri) {
        Uri.Builder buildUpon = uri != null ? uri.buildUpon() : new Uri.Builder();
        AdlantisUtils.setUriParamsFromMap(buildUpon, defaultParameters(context));
        return appendParameters(appendTargetingParameters(buildUpon));
    }

    /* access modifiers changed from: protected */
    public String deviceId(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            return null;
        }
    }

    public String getConversionTagHost() {
        return this._conversionTagHost;
    }

    public String getConversionTagTestHost() {
        return this._conversionTagTestHost;
    }

    public String getHost() {
        return this._host;
    }

    public int getPort() {
        return 80;
    }

    /* access modifiers changed from: package-private */
    public boolean hasTestAdRequestUrls() {
        return this._testAdRequestUrls != null;
    }

    public String md5_uniqueID(Context context) {
        String uniqueID;
        if (context == null || (uniqueID = uniqueID(context)) == null) {
            return null;
        }
        return AdlantisUtils.md5(uniqueID);
    }

    public abstract String publisherIDMetadataKey();

    public String sdkBuild() {
        return AdManager.getInstance().sdkBuild();
    }

    public String sdkVersion() {
        return AdManager.getInstance().sdkVersion();
    }

    public void setHost(String str) {
        this._host = str;
    }

    public void setTestAdRequestUrls(String[] strArr) {
        this._testAdRequestUrls = strArr;
    }

    public String uniqueID(Context context) {
        String androidId = androidId(context);
        return androidId == null ? deviceId(context) : androidId;
    }
}
