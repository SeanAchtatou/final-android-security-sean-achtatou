package jp.adlantis.android;

public interface AdRequestNotifier {
    void addRequestListener(AdRequestListener adRequestListener);

    void removeRequestListener(AdRequestListener adRequestListener);
}
