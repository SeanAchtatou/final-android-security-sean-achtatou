package jp.adlantis.android;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class AdlantisView extends RelativeLayout implements AdRequestNotifier {
    protected AdViewAdapter adViewAdapter = null;

    public AdlantisView(Context context) {
        super(context);
        setupView();
    }

    public AdlantisView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setupView();
    }

    /* access modifiers changed from: protected */
    public AdManager adManager() {
        return AdManager.getInstance();
    }

    /* access modifiers changed from: protected */
    public AdService adService() {
        return isInEditMode() ? new NullAdService() : adManager().getActiveAdService(getContext());
    }

    public void addRequestListener(AdRequestListener adRequestListener) {
        this.adViewAdapter.addRequestListener(adRequestListener);
    }

    /* access modifiers changed from: protected */
    public AdlantisAdViewContainer adlantisAdViewContainer() {
        View adView = this.adViewAdapter.adView();
        if (adView == null || !(adView instanceof AdlantisAdViewContainer)) {
            return null;
        }
        return (AdlantisAdViewContainer) adView;
    }

    public void clearAds() {
        this.adViewAdapter.clearAds();
    }

    public void connect() {
        this.adViewAdapter.connect();
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        AdService adService = adService();
        if (adService == null) {
            return;
        }
        if (i == 0) {
            adService.resume();
        } else {
            adService.pause();
        }
    }

    public void removeRequestListener(AdRequestListener adRequestListener) {
        this.adViewAdapter.removeRequestListener(adRequestListener);
    }

    public void setAdFetchInterval(long j) {
        AdlantisAdViewContainer adlantisAdViewContainer = adlantisAdViewContainer();
        if (adlantisAdViewContainer != null) {
            adlantisAdViewContainer.setAdFetchInterval(j);
        }
    }

    public void setGapPublisherID(String str) {
        AdlantisAdViewContainer adlantisAdViewContainer = adlantisAdViewContainer();
        if (adlantisAdViewContainer != null) {
            adlantisAdViewContainer.setGapPublisherID(str);
        }
    }

    public void setKeywords(String str) {
        AdlantisAdViewContainer adlantisAdViewContainer = adlantisAdViewContainer();
        if (adlantisAdViewContainer != null) {
            adlantisAdViewContainer.setKeywords(str);
        }
    }

    public void setPublisherID(String str) {
        AdlantisAdViewContainer adlantisAdViewContainer = adlantisAdViewContainer();
        if (adlantisAdViewContainer != null) {
            adlantisAdViewContainer.setPublisherID(str);
        }
    }

    /* access modifiers changed from: protected */
    public void setupView() {
        AdService adService = adService();
        if (adService != null) {
            adService.setTargetingParam(adManager().getTargetingParam());
            this.adViewAdapter = adService.adViewAdapter(getContext());
            View adView = this.adViewAdapter.adView();
            if (adView != null) {
                addView(adView);
            }
        }
    }
}
