package jp.adlantis.android;

import android.view.View;

public class AdViewAdapter implements AdRequestNotifier {
    protected View adView;
    protected AdRequestListeners listeners = new AdRequestListeners();

    AdViewAdapter(View view) {
        this.adView = view;
    }

    public View adView() {
        return this.adView;
    }

    public void addRequestListener(AdRequestListener adRequestListener) {
        this.listeners.addRequestListener(adRequestListener);
    }

    public void clearAds() {
    }

    public void connect() {
    }

    public void removeRequestListener(AdRequestListener adRequestListener) {
        this.listeners.removeRequestListener(adRequestListener);
    }
}
