package jp.adlantis.android;

import android.util.Log;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

public class NetworkRequest {
    protected static String DEBUG_TASK = "NetworkRequest";

    protected static AbstractHttpClient httpClientFactory() {
        return new DefaultHttpClient();
    }

    /* access modifiers changed from: package-private */
    public AdManager adManager() {
        return AdManager.getInstance();
    }

    /* access modifiers changed from: package-private */
    public AdNetworkConnection getAdNetworkConnection() {
        return adManager().getAdNetworkConnection();
    }

    /* access modifiers changed from: protected */
    public void log_d(String str) {
        Log.d(DEBUG_TASK, str);
    }

    /* access modifiers changed from: protected */
    public void log_e(String str) {
        Log.e(DEBUG_TASK, str);
    }
}
