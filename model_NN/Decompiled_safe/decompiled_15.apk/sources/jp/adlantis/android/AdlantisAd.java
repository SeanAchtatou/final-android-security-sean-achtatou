package jp.adlantis.android;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import jp.adlantis.android.utils.ADLStringUtils;
import jp.adlantis.android.utils.AdlantisUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdlantisAd extends HashMap implements Serializable, Map {
    public static final int ADTYPE_BANNER = 1;
    public static final int ADTYPE_TEXT = 2;
    private static final long IMPRESSION_COUNT_INTERVAL_NANOSECONDS = 2000000000;
    private static final String LOG_TAG = "AdlantisAd";
    private static final long NANOSECONDS_IN_SECOND = 1000000000;
    private static final long serialVersionUID = -94783938293849L;
    protected Handler _impressionHandler;
    protected boolean _isBeingViewed;
    protected boolean _sendImpressionCountFailed;
    protected boolean _sendingCountExpand;
    protected boolean _sendingImpressionCount;
    protected boolean _sentCountExpand;
    protected boolean _sentImpressionCount;
    protected long _viewStartTime;
    protected long _viewedTime;

    public AdlantisAd(HashMap hashMap) {
        super(hashMap);
        this._sendImpressionCountFailed = false;
    }

    public AdlantisAd(JSONObject jSONObject) {
        this(AdlantisUtils.jsonObjectToHashMap(jSONObject));
    }

    public static AdlantisAd[] adsFromJSONInputStream(InputStream inputStream) {
        return adsFromJSONString(AdlantisUtils.convertInputToString(inputStream));
    }

    public static AdlantisAd[] adsFromJSONString(String str) {
        AdlantisAd[] adlantisAdArr = null;
        try {
            JSONArray extractJSONAdArray = extractJSONAdArray(str);
            if (extractJSONAdArray != null) {
                adlantisAdArr = new AdlantisAd[extractJSONAdArray.length()];
                for (int i = 0; i < extractJSONAdArray.length(); i++) {
                    adlantisAdArr[i] = new AdlantisAd(extractJSONAdArray.getJSONObject(i));
                }
            } else {
                Log.i(LOG_TAG, "Adlantis: no ads received (this is not an error)");
            }
        } catch (Exception e) {
            logError("exception parsing JSON data " + e);
        }
        return adlantisAdArr;
    }

    private Uri buildURIFrom(Context context, String str) {
        if (str == null) {
            return null;
        }
        return AdManager.getInstance().getAdNetworkConnection().defaultRequestBuilder(context, Uri.parse(str)).build();
    }

    private Uri buildURIFromProperty(Context context, String str) {
        return buildURIFrom(context, (String) get(str));
    }

    private int currentOrientation(View view) {
        return view.getResources().getConfiguration().orientation;
    }

    public static String errorMessageFromJSONString(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str).getJSONObject("adlantis_ads");
            if (jSONObject != null) {
                return jSONObject.getJSONObject("error").getString("description");
            }
            return null;
        } catch (JSONException e) {
            return null;
        }
    }

    private static JSONArray extractJSONAdArray(String str) {
        JSONArray jSONArray;
        JSONObject jSONObject;
        try {
            jSONArray = new JSONArray(str);
        } catch (JSONException e) {
            jSONArray = null;
        }
        if (jSONArray != null) {
            return jSONArray;
        }
        try {
            JSONObject jSONObject2 = new JSONObject(str);
            try {
                jSONObject = jSONObject2.getJSONObject("adlantis_ads");
                if (jSONObject == null) {
                    jSONObject = jSONObject2;
                }
            } catch (JSONException e2) {
                jSONObject = jSONObject2;
            }
            return jSONObject.getJSONArray("ads");
        } catch (JSONException e3) {
            return jSONArray;
        }
    }

    private boolean hasHighResolutionDisplay(Context context) {
        return AdlantisUtils.hasHighResolutionDisplay(context);
    }

    private static String iphone_orientationKey(int i) {
        return orientationIsLandscape(i) ? "iphone_landscape" : "iphone_portrait";
    }

    protected static void logDebug(String str) {
        Log.d(LOG_TAG, str);
    }

    protected static void logError(String str) {
        Log.e(LOG_TAG, str);
    }

    protected static void logWarn(String str) {
        Log.w(LOG_TAG, str);
    }

    private static boolean orientationIsLandscape(int i) {
        return i == 2;
    }

    private static String orientationKey(int i) {
        return orientationIsLandscape(i) ? "landscape" : "portrait";
    }

    /* access modifiers changed from: private */
    public void setSendImpressionCountFailed(boolean z) {
        this._sendImpressionCountFailed = z;
        this._sendingImpressionCount = false;
    }

    /* access modifiers changed from: private */
    public void setSendingImpressionCount(boolean z) {
        this._sendingImpressionCount = z;
    }

    /* access modifiers changed from: private */
    public void setSentImpressionCount(boolean z) {
        this._sentImpressionCount = z;
        this._sendingImpressionCount = false;
    }

    public int adType() {
        return "sp_banner".compareTo((String) get("type")) == 0 ? 1 : 2;
    }

    public String altTextString(int i) {
        Map bannerInfoForOrientation = bannerInfoForOrientation(i);
        if (bannerInfoForOrientation == null) {
            return null;
        }
        String str = (String) bannerInfoForOrientation.get("alt");
        return str != null ? Uri.decode(str) : str;
    }

    public String altTextString(View view) {
        Map bannerInfoForCurrentOrientation = bannerInfoForCurrentOrientation(view);
        if (bannerInfoForCurrentOrientation == null) {
            return null;
        }
        String str = (String) bannerInfoForCurrentOrientation.get("alt");
        return str != null ? Uri.decode(str) : str;
    }

    public Map bannerInfoForCurrentOrientation(View view) {
        if (adType() == 1) {
            return bannerInfoForOrientation(currentOrientation(view));
        }
        return null;
    }

    public Map bannerInfoForOrientation(int i) {
        if (adType() == 1) {
            Object obj = (Map) get(orientationKey(i));
            if (obj == null) {
                obj = get(iphone_orientationKey(i));
            }
            if (obj instanceof Map) {
                return (Map) obj;
            }
        }
        return null;
    }

    public String bannerURLForCurrentOrientation(View view) {
        return bannerURLForOrientation(currentOrientation(view), view.getContext());
    }

    public String bannerURLForOrientation(int i, Context context) {
        return bannerURLForOrientation(i, hasHighResolutionDisplay(context));
    }

    public String bannerURLForOrientation(int i, boolean z) {
        Map bannerInfoForOrientation;
        String str = null;
        if (adType() != 1 || (bannerInfoForOrientation = bannerInfoForOrientation(i)) == null) {
            return null;
        }
        if (z) {
            str = (String) bannerInfoForOrientation.get("src_2x");
        }
        return str == null ? (String) bannerInfoForOrientation.get("src") : str;
    }

    /* access modifiers changed from: protected */
    public void clearImpressionHandler() {
        this._impressionHandler = null;
    }

    public Uri countExpandUri(Context context) {
        return buildURIFromProperty(context, "count_expand");
    }

    public Uri countImpressionUri(Context context) {
        return buildURIFromProperty(context, "count_impression");
    }

    /* access modifiers changed from: protected */
    public void doSendImpressionCountThread() {
        new Thread() {
            public void run() {
                boolean sendRequestForProperty = AdlantisAd.this.sendRequestForProperty("count_impression", "sendImpressionCount");
                AdlantisAd.this.setSendingImpressionCount(false);
                if (sendRequestForProperty) {
                    AdlantisAd.this.setSentImpressionCount(true);
                } else {
                    AdlantisAd.this.setSendImpressionCountFailed(true);
                }
            }
        }.start();
    }

    public boolean hasAdForOrientation(int i) {
        if (adType() == 2) {
            return true;
        }
        if (adType() == 1) {
            return bannerInfoForOrientation(i) != null;
        }
        return false;
    }

    public HashMap hashMapRepresentation() {
        return new HashMap(this);
    }

    /* access modifiers changed from: protected */
    public AbstractHttpClient httpClientFactory() {
        return new DefaultHttpClient();
    }

    public String iconURL(View view) {
        return iconURL(hasHighResolutionDisplay(view.getContext()));
    }

    public String iconURL(boolean z) {
        Map map;
        String str = null;
        if (adType() != 2 || (map = (Map) get("iphone_icon")) == null) {
            return null;
        }
        if (z) {
            str = (String) map.get("src_2x");
        }
        return str == null ? (String) map.get("src") : str;
    }

    public String imageURL(int i, boolean z) {
        int adType = adType();
        if (adType == 2) {
            return iconURL(z);
        }
        if (adType == 1) {
            return bannerURLForOrientation(i, z);
        }
        return null;
    }

    public String imageURL(View view) {
        return bannerURLForOrientation(currentOrientation(view), hasHighResolutionDisplay(view.getContext()));
    }

    /* access modifiers changed from: protected */
    public long impressionCountIntervalMilliseconds() {
        return TimeUnit.NANOSECONDS.toMillis(impressionCountIntervalNanoseconds());
    }

    /* access modifiers changed from: protected */
    public long impressionCountIntervalNanoseconds() {
        return IMPRESSION_COUNT_INTERVAL_NANOSECONDS;
    }

    /* access modifiers changed from: protected */
    public boolean impressionCountIntervalPassed() {
        return viewedTime() >= impressionCountIntervalNanoseconds();
    }

    /* access modifiers changed from: package-private */
    public boolean isRedirectingUrl(String str) {
        return ADLStringUtils.isHttpUrl(tapUrlString()) && str.indexOf("url=") != -1;
    }

    public boolean isWebLink() {
        return "web".equals(linkType());
    }

    public String linkType() {
        return (String) get("link_type");
    }

    /* access modifiers changed from: protected */
    public void sendImpressionCount() {
        if (shouldSendImpressionCount()) {
            setSendingImpressionCount(true);
            doSendImpressionCountThread();
        }
    }

    /* access modifiers changed from: protected */
    public boolean sendRequestForProperty(String str, String str2) {
        try {
            String uri = buildURIFromProperty(null, str).toString();
            if (uri == null) {
                return false;
            }
            int statusCode = httpClientFactory().execute(new HttpGet(uri)).getStatusLine().getStatusCode();
            if (statusCode >= 200 && statusCode < 400) {
                return true;
            }
            logError(str2 + " status=" + statusCode);
            return false;
        } catch (MalformedURLException e) {
            logError(str2 + " exception=" + e.toString());
            return false;
        } catch (IOException e2) {
            logError(str2 + " exception=" + e2.toString());
            return false;
        } catch (OutOfMemoryError e3) {
            logError(str2 + " OutOfMemoryError=" + e3.toString());
            return false;
        }
    }

    public boolean shouldHandleRedirect() {
        return isRedirectingUrl(tapUrlString()) && ("appstore".equals(linkType()) || "itunes".equals(linkType()));
    }

    /* access modifiers changed from: protected */
    public boolean shouldSendImpressionCount() {
        return !this._sentImpressionCount && !this._sendingImpressionCount && !this._sendImpressionCountFailed;
    }

    public String tapUriRedirect() {
        Uri.Builder buildUpon = Uri.parse(tapUrlString()).buildUpon();
        buildUpon.appendQueryParameter("adlDoRedirect", "1");
        return buildUpon.toString();
    }

    public String tapUrlString() {
        return (String) get("href");
    }

    public String textAdString() {
        return Uri.decode((String) get("string"));
    }

    public String urlString() {
        return tapUrlString();
    }

    /* access modifiers changed from: protected */
    public long viewedTime() {
        if (this._isBeingViewed) {
            long nanoTime = System.nanoTime();
            this._viewedTime += nanoTime - this._viewStartTime;
            this._viewStartTime = nanoTime;
        }
        return this._viewedTime;
    }

    public void viewingEnded() {
        if (!this._isBeingViewed) {
            logWarn("viewingEnded() called without matching viewingStarted()");
        }
        this._isBeingViewed = false;
        clearImpressionHandler();
        if (impressionCountIntervalPassed()) {
            sendImpressionCount();
        }
    }

    public void viewingStarted() {
        this._viewStartTime = System.nanoTime();
        this._isBeingViewed = true;
        if (shouldSendImpressionCount()) {
            this._impressionHandler = new Handler(Looper.getMainLooper());
            this._impressionHandler.postDelayed(new Runnable() {
                public void run() {
                    if (AdlantisAd.this.impressionCountIntervalPassed()) {
                        AdlantisAd.this.sendImpressionCount();
                    }
                }
            }, impressionCountIntervalMilliseconds());
        }
    }
}
