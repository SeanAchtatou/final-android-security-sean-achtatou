package jp.adlantis.android;

import android.util.Log;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class GreeDsp {
    private static final String LOG_TAG = "GreeDsp";
    public static final String UUID_LOCAL_STORAGE_KEY = "uuid";

    public static String getUUID() {
        String property = SharedStorage.getInstance().get(UUID_LOCAL_STORAGE_KEY) != null ? SharedStorage.getInstance().getProperty(UUID_LOCAL_STORAGE_KEY) : null;
        if (property != null && property.length() != 0) {
            return property;
        }
        String md5 = md5(UUID.randomUUID().toString());
        SharedStorage.getInstance().setProperty(UUID_LOCAL_STORAGE_KEY, md5);
        try {
            SharedStorage.getInstance().store();
            return md5;
        } catch (IOException e) {
            Log.w(LOG_TAG, "failed to store uuid:" + e.getMessage());
            return null;
        }
    }

    private static String md5(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : digest) {
                String hexString = Integer.toHexString(b & 255);
                while (hexString.length() < 2) {
                    hexString = "0" + hexString;
                }
                stringBuffer.append(hexString);
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return str;
        }
    }
}
