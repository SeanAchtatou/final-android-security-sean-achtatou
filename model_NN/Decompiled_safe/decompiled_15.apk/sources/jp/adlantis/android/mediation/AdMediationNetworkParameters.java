package jp.adlantis.android.mediation;

import android.util.Log;
import java.io.InputStream;
import java.util.Map;
import jp.adlantis.android.utils.AdlantisUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdMediationNetworkParameters {
    private static final String LOG_TAG = "AdMediationNetworkParameters";
    private Map parameters;

    public AdMediationNetworkParameters(Map map) {
        this.parameters = map;
    }

    public AdMediationNetworkParameters(JSONObject jSONObject) {
        this(AdlantisUtils.jsonObjectToHashMap(jSONObject));
    }

    public static AdMediationNetworkParameters[] networksFromJSONInputStream(InputStream inputStream) {
        return networksFromJSONString(AdlantisUtils.convertInputToString(inputStream));
    }

    public static AdMediationNetworkParameters[] networksFromJSONString(String str) {
        AdMediationNetworkParameters[] adMediationNetworkParametersArr = null;
        try {
            JSONArray jSONArray = new JSONObject(str).getJSONArray("networks");
            if (jSONArray != null) {
                adMediationNetworkParametersArr = new AdMediationNetworkParameters[jSONArray.length()];
                for (int i = 0; i < jSONArray.length(); i++) {
                    adMediationNetworkParametersArr[i] = new AdMediationNetworkParameters(jSONArray.getJSONObject(i));
                }
            } else {
                Log.i(LOG_TAG, "Adlantis: no networks received (this is not an error)");
            }
        } catch (Exception e) {
        }
        return adMediationNetworkParametersArr;
    }

    public String getCountRequestUrl() {
        return (String) this.parameters.get("count_req");
    }

    public String getCountTapUrl() {
        return (String) this.parameters.get("count_tap");
    }

    public String getNetworkName() {
        return (String) this.parameters.get("name");
    }

    public String getParameter(String str) {
        Map map = (Map) this.parameters.get("info");
        if (map == null) {
            return null;
        }
        return (String) map.get(str);
    }
}
