package jp.adlantis.android.mediation.adapters;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import jp.adlantis.android.mediation.AdMediationAdapter;
import jp.adlantis.android.mediation.AdMediationAdapterListener;
import jp.adlantis.android.mediation.AdMediationNetworkParameters;
import jp.co.imobile.android.AdRequestResult;
import jp.co.imobile.android.AdView;
import jp.co.imobile.android.AdViewRequestListener;

public class IMobileAdapter implements AdMediationAdapter, AdViewRequestListener {
    private AdView adView = null;
    private ViewGroup adViewHolder = null;
    /* access modifiers changed from: private */
    public AdMediationAdapterListener listener = null;

    public void destroy() {
        if (this.adView != null) {
            this.adView.setOnRequestListener((AdViewRequestListener) null);
            this.adView.stop();
        }
        this.adView = null;
        this.adViewHolder = null;
    }

    public void onCompleted(AdRequestResult adRequestResult, AdView adView2) {
        if (this.listener != null) {
            this.listener.onReceivedAd(this, this.adViewHolder);
            this.adView.stop();
        }
    }

    public void onFailed(AdRequestResult adRequestResult, AdView adView2) {
        if (this.listener != null) {
            this.listener.onFailedToReceiveAd(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.ViewGroup.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [jp.co.imobile.android.AdView, android.widget.RelativeLayout$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      ClspMth{android.view.ViewGroup.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public View requestAd(AdMediationAdapterListener adMediationAdapterListener, Activity activity, AdMediationNetworkParameters adMediationNetworkParameters) {
        this.listener = adMediationAdapterListener;
        this.adView = AdView.create(activity, Integer.parseInt(adMediationNetworkParameters.getParameter("media_id")), Integer.parseInt(adMediationNetworkParameters.getParameter("spot_id")));
        this.adView.setOnRequestListener(this);
        this.adView.start();
        this.adViewHolder = new RelativeLayout(activity) {
            public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                IMobileAdapter.this.listener.onTouchAd(IMobileAdapter.this);
                return false;
            }
        };
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.adViewHolder.addView((View) this.adView, (ViewGroup.LayoutParams) layoutParams);
        return this.adViewHolder;
    }
}
