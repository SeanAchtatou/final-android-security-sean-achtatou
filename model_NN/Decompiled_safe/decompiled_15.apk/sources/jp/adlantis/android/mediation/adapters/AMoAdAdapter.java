package jp.adlantis.android.mediation.adapters;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import jp.adlantis.android.mediation.AdMediationAdapter;
import jp.adlantis.android.mediation.AdMediationAdapterListener;
import jp.adlantis.android.mediation.AdMediationNetworkParameters;
import jp.co.cyberagent.AMoAdView;
import jp.co.cyberagent.AdCallback;

public class AMoAdAdapter implements AdMediationAdapter, AdCallback {
    private AMoAdView adView = null;
    private ViewGroup adViewHolder = null;
    /* access modifiers changed from: private */
    public AdMediationAdapterListener listener = null;

    public void destroy() {
        if (this.adView != null) {
            this.adView.setCallback((AdCallback) null);
        }
        this.adView = null;
        this.adViewHolder = null;
    }

    public void didFailToReceiveAdWithError() {
        if (this.listener != null) {
            this.listener.onFailedToReceiveAd(this);
        }
    }

    public void didReceiveAd() {
        if (this.listener != null) {
            this.listener.onReceivedAd(this, this.adViewHolder);
        }
    }

    public void didReceiveEmptyAd() {
        if (this.listener != null) {
            this.listener.onFailedToReceiveAd(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.ViewGroup.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [jp.co.cyberagent.AMoAdView, android.widget.RelativeLayout$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      ClspMth{android.view.ViewGroup.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public View requestAd(AdMediationAdapterListener adMediationAdapterListener, Activity activity, AdMediationNetworkParameters adMediationNetworkParameters) {
        this.listener = adMediationAdapterListener;
        String parameter = adMediationNetworkParameters.getParameter("ad_sid");
        this.adView = new AMoAdView(activity);
        this.adView.setCallback(this);
        this.adView.setContentSizeIdentifier(2);
        this.adView.setSid(parameter);
        this.adView.stopRotation();
        this.adView.requestFreshAd();
        this.adViewHolder = new RelativeLayout(activity) {
            public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
                if (motionEvent.getAction() != 0) {
                    return false;
                }
                AMoAdAdapter.this.listener.onTouchAd(AMoAdAdapter.this);
                return false;
            }
        };
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.adViewHolder.addView((View) this.adView, (ViewGroup.LayoutParams) layoutParams);
        return null;
    }
}
