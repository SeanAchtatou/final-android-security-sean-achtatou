package jp.adlantis.android.mediation;

import android.app.Activity;
import android.view.View;

public interface AdMediationAdapter {
    void destroy();

    View requestAd(AdMediationAdapterListener adMediationAdapterListener, Activity activity, AdMediationNetworkParameters adMediationNetworkParameters);
}
