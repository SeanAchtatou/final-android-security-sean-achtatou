package jp.adlantis.android.mediation;

import android.content.Context;
import android.util.Log;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Map;
import jp.adlantis.android.AdRequest;
import jp.adlantis.android.AdlantisAd;
import jp.adlantis.android.AdlantisAdsModel;
import jp.adlantis.android.NetworkRequest;
import jp.adlantis.android.utils.AdlantisUtils;
import org.apache.http.client.methods.HttpGet;

public class AdMediationRequest extends AdRequest {
    public AdMediationRequest(AdlantisAdsModel adlantisAdsModel) {
        super(adlantisAdsModel);
    }

    public static boolean sendGetRequest(String str) {
        try {
            int statusCode = NetworkRequest.httpClientFactory().execute(new HttpGet(str)).getStatusLine().getStatusCode();
            return statusCode >= 200 && statusCode < 400;
        } catch (Exception e) {
            Log.e(DEBUG_TASK, "sendGetRequest exception=" + e.toString());
            return false;
        }
    }

    public boolean doAdRequest(Context context, Map map) {
        boolean z = false;
        try {
            String convertInputToString = AdlantisUtils.convertInputToString(inputStreamForUri(context, adRequestUri(context, map)));
            AdlantisAd[] adsFromJSONString = AdlantisAd.adsFromJSONString(convertInputToString);
            if (adsFromJSONString == null) {
                adsFromJSONString = new AdlantisAd[0];
            }
            getAdsModel().setAds(adsFromJSONString);
            log_d(adsFromJSONString.length + " ads loaded");
            if (adsFromJSONString.length > 0) {
                z = true;
            }
            if (z) {
                getAdsModel().setNetworkParameters(null);
            } else {
                AdMediationNetworkParameters[] networksFromJSONString = AdMediationNetworkParameters.networksFromJSONString(convertInputToString);
                getAdsModel().setNetworkParameters(networksFromJSONString);
                if (networksFromJSONString != null) {
                    log_d(networksFromJSONString.length + " networks loaded");
                } else {
                    log_d("no networks loaded");
                }
            }
            getAdsModel().setErrorMessage(AdlantisAd.errorMessageFromJSONString(convertInputToString));
        } catch (MalformedURLException e) {
            log_e(e.toString());
        } catch (IOException e2) {
            log_e(e2.toString());
        }
        if (z) {
            notifyListenersAdReceived(null);
        } else {
            notifyListenersFailedToReceiveAd(null);
        }
        return z;
    }
}
