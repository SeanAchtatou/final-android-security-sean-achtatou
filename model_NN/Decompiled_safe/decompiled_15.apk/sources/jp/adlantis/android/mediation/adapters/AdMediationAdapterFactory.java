package jp.adlantis.android.mediation.adapters;

import android.util.Log;
import jp.adlantis.android.mediation.AdMediationAdapter;
import jp.adlantis.android.mediation.AdMediationNetworkParameters;
import jp.adlantis.android.utils.AdlantisUtils;

public class AdMediationAdapterFactory {
    private static final String LOG_TAG = "AdMediationAdapterFactory";

    public static AdMediationAdapter create(AdMediationNetworkParameters adMediationNetworkParameters) {
        if ("admob".equalsIgnoreCase(adMediationNetworkParameters.getNetworkName())) {
            return createAdapter("jp.adlantis.android.mediation.adapters.AdMobAdapter", new String[]{"com.google.ads.Ad", "com.google.ads.AdListener", "com.google.ads.AdRequest", "com.google.ads.AdSize", "com.google.ads.AdView"}, adMediationNetworkParameters, new String[]{"ad_unit_id"});
        } else if ("amoad".equalsIgnoreCase(adMediationNetworkParameters.getNetworkName())) {
            return createAdapter("jp.adlantis.android.mediation.adapters.AMoAdAdapter", new String[]{"jp.co.cyberagent.AMoAdView", "jp.co.cyberagent.AdCallback"}, adMediationNetworkParameters, new String[]{"ad_sid"});
        } else if ("imobile".equalsIgnoreCase(adMediationNetworkParameters.getNetworkName())) {
            return createAdapter("jp.adlantis.android.mediation.adapters.IMobileAdapter", new String[]{"jp.co.imobile.android.AdView", "jp.co.imobile.android.AdViewRequestListener", "jp.co.imobile.android.AdRequestResult", "jp.co.imobile.android.AdRequestResultType"}, adMediationNetworkParameters, new String[]{"media_id", "spot_id"});
        } else if ("mediba".equalsIgnoreCase(adMediationNetworkParameters.getNetworkName())) {
            return createAdapter("jp.adlantis.android.mediation.adapters.MedibaAdAdapter", new String[]{"mediba.ad.sdk.android.openx.MasAdListener", "mediba.ad.sdk.android.openx.MasAdView", "com.mediba.jp.KSL"}, adMediationNetworkParameters, new String[]{"sid"});
        } else if (!"nend".equalsIgnoreCase(adMediationNetworkParameters.getNetworkName())) {
            return null;
        } else {
            return createAdapter("jp.adlantis.android.mediation.adapters.NendAdapter", new String[]{"net.nend.android.NendAdView"}, adMediationNetworkParameters, new String[]{"api_key", "spot_id"});
        }
    }

    private static AdMediationAdapter createAdapter(String str, String[] strArr, AdMediationNetworkParameters adMediationNetworkParameters, String[] strArr2) {
        AdMediationAdapter adMediationAdapter;
        Exception e;
        for (String parameter : strArr2) {
            String parameter2 = adMediationNetworkParameters.getParameter(parameter);
            if (parameter2 == null || "".equals(parameter2)) {
                Log.d(LOG_TAG, "can not find required parameters: " + adMediationNetworkParameters.getNetworkName() + "[" + parameter2 + "]");
                return null;
            }
        }
        if (!AdlantisUtils.findClass(strArr)) {
            Log.d(LOG_TAG, "can not load required classes: " + adMediationNetworkParameters.getNetworkName());
            return null;
        }
        try {
            adMediationAdapter = (AdMediationAdapter) Class.forName(str).newInstance();
            try {
                Log.d(LOG_TAG, "created AdMediationAdapter: " + adMediationNetworkParameters.getNetworkName());
                return adMediationAdapter;
            } catch (Exception e2) {
                e = e2;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            adMediationAdapter = null;
            e = exc;
            Log.d(LOG_TAG, "exception on creating AdMediationAdapter: " + adMediationNetworkParameters.getNetworkName());
            Log.d(LOG_TAG, "exception = " + e.toString());
            return adMediationAdapter;
        }
    }
}
