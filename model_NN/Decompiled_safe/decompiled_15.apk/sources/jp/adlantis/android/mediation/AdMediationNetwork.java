package jp.adlantis.android.mediation;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import jp.adlantis.android.AdManager;
import jp.adlantis.android.mediation.adapters.AdMediationAdapterFactory;

public class AdMediationNetwork implements AdMediationAdapterListener {
    private static final String LOG_TAG = "AdMediationNetwork";
    private static final int TIMEOUT_SECONDS = 10;
    private AdMediationAdapter adapter = null;
    private Handler handler = new Handler();
    /* access modifiers changed from: private */
    public AdMediationNetworkParameters networkParameters;
    /* access modifiers changed from: private */
    public boolean receivedAd = false;
    private ScheduledExecutorService timer = Executors.newScheduledThreadPool(1);
    private ScheduledFuture timerFuture = null;

    public AdMediationNetwork(AdMediationNetworkParameters adMediationNetworkParameters) {
        this.networkParameters = adMediationNetworkParameters;
    }

    /* access modifiers changed from: private */
    public boolean addToParentView(View view) {
        boolean z = false;
        ViewGroup parentView = AdMediationManager.getInstance().getParentView();
        if (parentView == null) {
            return false;
        }
        int i = 0;
        while (true) {
            if (i >= parentView.getChildCount()) {
                break;
            } else if (view == parentView.getChildAt(i)) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            parentView.removeAllViews();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13);
            parentView.addView(view, layoutParams);
        }
        return true;
    }

    private void killTimer() {
        if (this.timerFuture != null) {
            this.timerFuture.cancel(false);
        }
        Log.d(LOG_TAG, "killed timer: " + this.networkParameters.getNetworkName());
    }

    /* access modifiers changed from: private */
    public void nextNetwork() {
        this.handler.post(new Runnable() {
            public void run() {
                AdMediationNetwork.this.destroy();
                AdMediationManager.getInstance().nextNetwork();
            }
        });
    }

    private void startTimer() {
        this.timerFuture = this.timer.schedule(new Runnable() {
            public void run() {
                Log.d(AdMediationNetwork.LOG_TAG, "timer timeout: " + AdMediationNetwork.this.networkParameters.getNetworkName());
                AdMediationNetwork.this.nextNetwork();
            }
        }, 10, TimeUnit.SECONDS);
        Log.d(LOG_TAG, "started timer: " + this.networkParameters.getNetworkName());
    }

    public void destroy() {
        if (this.adapter != null) {
            this.adapter.destroy();
            this.adapter = null;
        }
        if (this.timerFuture != null) {
            this.timerFuture.cancel(true);
        }
    }

    public AdMediationAdapter getAdapter() {
        return this.adapter;
    }

    public boolean isReceivedAd() {
        return this.receivedAd;
    }

    public void onDismissScreen(AdMediationAdapter adMediationAdapter) {
        Log.d(LOG_TAG, "onDismissScreen: " + this.networkParameters.getNetworkName());
    }

    public void onFailedToReceiveAd(AdMediationAdapter adMediationAdapter) {
        Log.d(LOG_TAG, "onFailedToReceiveAd: " + this.networkParameters.getNetworkName());
        killTimer();
        startCountRequest();
        nextNetwork();
    }

    public void onLeaveApplication(AdMediationAdapter adMediationAdapter) {
        Log.d(LOG_TAG, "onLeaveApplication: " + this.networkParameters.getNetworkName());
    }

    public void onPresentScreen(AdMediationAdapter adMediationAdapter) {
        Log.d(LOG_TAG, "onPresentScreen: " + this.networkParameters.getNetworkName());
    }

    public void onReceivedAd(AdMediationAdapter adMediationAdapter, final View view) {
        Log.d(LOG_TAG, "onReceivedAd: " + this.networkParameters.getNetworkName());
        killTimer();
        Activity activity = AdMediationManager.getInstance().getActivity();
        if (activity == null) {
            Log.d(LOG_TAG, "can not get Activity: " + this.networkParameters.getNetworkName());
            destroy();
            return;
        }
        activity.runOnUiThread(new Runnable() {
            public void run() {
                if (AdMediationNetwork.this.addToParentView(view)) {
                    Log.d(AdMediationNetwork.LOG_TAG, "show ads in AdlantisView: " + AdMediationNetwork.this.networkParameters.getNetworkName());
                }
                AdMediationNetwork.this.startCountImpression();
                boolean unused = AdMediationNetwork.this.receivedAd = true;
            }
        });
    }

    public void onTouchAd(AdMediationAdapter adMediationAdapter) {
        Log.d(LOG_TAG, "onTouchAd: " + this.networkParameters.getNetworkName());
        if (this.receivedAd) {
            startCountTap();
        }
    }

    public boolean requestAd() {
        Log.d(LOG_TAG, "request network ad: " + this.networkParameters.getNetworkName());
        if (this.adapter == null) {
            this.adapter = AdMediationAdapterFactory.create(this.networkParameters);
        }
        if (this.adapter == null) {
            return false;
        }
        Activity activity = AdMediationManager.getInstance().getActivity();
        this.receivedAd = false;
        try {
            if (activity.getResources().getConfiguration().orientation != 2 || !"mediba".equalsIgnoreCase(this.networkParameters.getNetworkName())) {
                final View requestAd = this.adapter.requestAd(this, activity, this.networkParameters);
                if (requestAd != null) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            boolean unused = AdMediationNetwork.this.addToParentView(requestAd);
                        }
                    });
                }
                startTimer();
                return true;
            }
            Log.d(LOG_TAG, "skip loadscape mode: " + this.networkParameters.getNetworkName());
            return false;
        } catch (Exception e) {
            Log.d(LOG_TAG, "exception in requestAd of adapter: " + this.networkParameters.getNetworkName());
            Log.d(LOG_TAG, "exception = " + e.toString());
            return false;
        }
    }

    public void setAdapter(AdMediationAdapter adMediationAdapter) {
        this.adapter = adMediationAdapter;
    }

    /* access modifiers changed from: protected */
    public void startCountImpression() {
        new Thread(new Runnable() {
            public void run() {
                String countRequestUrl = AdMediationNetwork.this.networkParameters.getCountRequestUrl();
                if (countRequestUrl == null) {
                    Log.d(AdMediationNetwork.LOG_TAG, "cannot get count_imp_url: " + AdMediationNetwork.this.networkParameters.getNetworkName());
                    return;
                }
                String buildCompleteHttpUri = AdManager.getInstance().getAdNetworkConnection().buildCompleteHttpUri(AdMediationManager.getInstance().getActivity(), countRequestUrl + "&impFlag=1");
                Log.d(AdMediationNetwork.LOG_TAG, "count_imp_url of " + AdMediationNetwork.this.networkParameters.getNetworkName() + ": " + buildCompleteHttpUri);
                AdMediationRequest.sendGetRequest(buildCompleteHttpUri);
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public void startCountRequest() {
        new Thread(new Runnable() {
            public void run() {
                String countRequestUrl = AdMediationNetwork.this.networkParameters.getCountRequestUrl();
                if (countRequestUrl == null) {
                    Log.d(AdMediationNetwork.LOG_TAG, "cannot get count_req_url: " + AdMediationNetwork.this.networkParameters.getNetworkName());
                    return;
                }
                String buildCompleteHttpUri = AdManager.getInstance().getAdNetworkConnection().buildCompleteHttpUri(AdMediationManager.getInstance().getActivity(), countRequestUrl + "&impFlag=0");
                Log.d(AdMediationNetwork.LOG_TAG, "count_req_url of " + AdMediationNetwork.this.networkParameters.getNetworkName() + ": " + buildCompleteHttpUri);
                AdMediationRequest.sendGetRequest(buildCompleteHttpUri);
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public void startCountTap() {
        new Thread(new Runnable() {
            public void run() {
                String countTapUrl = AdMediationNetwork.this.networkParameters.getCountTapUrl();
                if (countTapUrl == null) {
                    Log.d(AdMediationNetwork.LOG_TAG, "cannot get count_tap_url: " + AdMediationNetwork.this.networkParameters.getNetworkName());
                    return;
                }
                String buildCompleteHttpUri = AdManager.getInstance().getAdNetworkConnection().buildCompleteHttpUri(AdMediationManager.getInstance().getActivity(), countTapUrl);
                Log.d(AdMediationNetwork.LOG_TAG, "count_tap_url of " + AdMediationNetwork.this.networkParameters.getNetworkName() + ": " + buildCompleteHttpUri);
                AdMediationRequest.sendGetRequest(buildCompleteHttpUri);
            }
        }).start();
    }
}
