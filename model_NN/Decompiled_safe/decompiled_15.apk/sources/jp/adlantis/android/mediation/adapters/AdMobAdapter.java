package jp.adlantis.android.mediation.adapters;

import android.app.Activity;
import android.view.View;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import jp.adlantis.android.mediation.AdMediationAdapter;
import jp.adlantis.android.mediation.AdMediationAdapterListener;
import jp.adlantis.android.mediation.AdMediationNetworkParameters;

public class AdMobAdapter implements AdListener, AdMediationAdapter {
    private AdView adView = null;
    private AdMediationAdapterListener listener = null;

    public void destroy() {
        if (this.adView != null) {
            this.adView.setAdListener(null);
            this.adView.setOnTouchListener(null);
            this.adView.stopLoading();
            this.adView.destroy();
        }
        this.adView = null;
    }

    public void onDismissScreen(Ad ad) {
        if (this.listener != null) {
            this.listener.onDismissScreen(this);
        }
    }

    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode errorCode) {
        if (this.listener != null) {
            this.listener.onFailedToReceiveAd(this);
        }
    }

    public void onLeaveApplication(Ad ad) {
        if (this.listener != null) {
            this.listener.onLeaveApplication(this);
        }
    }

    public void onPresentScreen(Ad ad) {
        if (this.listener != null) {
            this.listener.onTouchAd(this);
        }
    }

    public void onReceiveAd(Ad ad) {
        if (this.listener != null) {
            this.adView.stopLoading();
            this.listener.onReceivedAd(this, this.adView);
        }
    }

    public View requestAd(AdMediationAdapterListener adMediationAdapterListener, Activity activity, AdMediationNetworkParameters adMediationNetworkParameters) {
        this.listener = adMediationAdapterListener;
        this.adView = new AdView(activity, AdSize.SMART_BANNER, adMediationNetworkParameters.getParameter("ad_unit_id"));
        this.adView.setAdListener(this);
        this.adView.loadAd(new AdRequest());
        return null;
    }
}
