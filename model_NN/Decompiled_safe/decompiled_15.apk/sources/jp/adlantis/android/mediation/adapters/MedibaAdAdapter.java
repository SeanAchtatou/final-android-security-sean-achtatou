package jp.adlantis.android.mediation.adapters;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import jp.adlantis.android.mediation.AdMediationAdapter;
import jp.adlantis.android.mediation.AdMediationAdapterListener;
import jp.adlantis.android.mediation.AdMediationNetworkParameters;
import mediba.ad.sdk.android.openx.MasAdListener;
import mediba.ad.sdk.android.openx.MasAdView;

public class MedibaAdAdapter implements AdMediationAdapter {
    /* access modifiers changed from: private */
    public MasAdView adView = null;
    /* access modifiers changed from: private */
    public ViewGroup adViewHolder = null;
    /* access modifiers changed from: private */
    public AdMediationAdapterListener listener = null;

    class MedibaAdListener extends MasAdListener {
        private MedibaAdListener() {
        }

        public void onFailedToReceiveAd() {
            if (MedibaAdAdapter.this.listener != null) {
                MedibaAdAdapter.this.listener.onFailedToReceiveAd(MedibaAdAdapter.this);
            }
        }

        public void onFailedToReceiveRefreshedAd() {
            if (MedibaAdAdapter.this.listener != null) {
                MedibaAdAdapter.this.listener.onFailedToReceiveAd(MedibaAdAdapter.this);
            }
        }

        public void onInternalBrowserClose() {
            if (MedibaAdAdapter.this.listener != null) {
                MedibaAdAdapter.this.listener.onDismissScreen(MedibaAdAdapter.this);
            }
        }

        public void onInternalBrowserOpen() {
            if (MedibaAdAdapter.this.listener != null) {
                MedibaAdAdapter.this.listener.onPresentScreen(MedibaAdAdapter.this);
            }
        }

        public void onReceiveAd() {
            if (MedibaAdAdapter.this.listener != null) {
                MedibaAdAdapter.this.listener.onReceivedAd(MedibaAdAdapter.this, MedibaAdAdapter.this.adViewHolder);
                MedibaAdAdapter.this.adView.stop();
            }
        }

        public void onReceiveRefreshedAd() {
        }

        public void onVideoPlayerEnd() {
            if (MedibaAdAdapter.this.listener != null) {
                MedibaAdAdapter.this.listener.onDismissScreen(MedibaAdAdapter.this);
            }
        }

        public void onVideoPlayerStart() {
            if (MedibaAdAdapter.this.listener != null) {
                MedibaAdAdapter.this.listener.onPresentScreen(MedibaAdAdapter.this);
            }
        }
    }

    public void destroy() {
        if (this.adView != null) {
            this.adView.setAdListener((MasAdListener) null);
            this.adView.stop();
            this.adView.destroy();
        }
        this.adView = null;
        this.adViewHolder = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.ViewGroup.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [mediba.ad.sdk.android.openx.MasAdView, android.widget.RelativeLayout$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      ClspMth{android.view.ViewGroup.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public View requestAd(AdMediationAdapterListener adMediationAdapterListener, Activity activity, AdMediationNetworkParameters adMediationNetworkParameters) {
        this.listener = adMediationAdapterListener;
        String parameter = adMediationNetworkParameters.getParameter("sid");
        this.adView = new MasAdView(activity);
        this.adView.setSid(parameter);
        this.adView.setAdListener(new MedibaAdListener());
        this.adView.start();
        this.adViewHolder = new RelativeLayout(activity) {
            public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                MedibaAdAdapter.this.listener.onTouchAd(MedibaAdAdapter.this);
                return false;
            }
        };
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.adViewHolder.addView((View) this.adView, (ViewGroup.LayoutParams) layoutParams);
        return null;
    }
}
