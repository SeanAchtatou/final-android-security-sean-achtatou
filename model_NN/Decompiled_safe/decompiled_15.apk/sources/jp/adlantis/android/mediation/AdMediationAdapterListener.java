package jp.adlantis.android.mediation;

import android.view.View;

public interface AdMediationAdapterListener {
    void onDismissScreen(AdMediationAdapter adMediationAdapter);

    void onFailedToReceiveAd(AdMediationAdapter adMediationAdapter);

    void onLeaveApplication(AdMediationAdapter adMediationAdapter);

    void onPresentScreen(AdMediationAdapter adMediationAdapter);

    void onReceivedAd(AdMediationAdapter adMediationAdapter, View view);

    void onTouchAd(AdMediationAdapter adMediationAdapter);
}
