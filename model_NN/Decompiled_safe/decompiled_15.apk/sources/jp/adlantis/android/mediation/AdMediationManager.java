package jp.adlantis.android.mediation;

import android.app.Activity;
import android.util.Log;
import android.view.ViewGroup;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.LinkedList;

public class AdMediationManager {
    private static final String LOG_TAG = "AdMediationManager";
    private WeakReference activityRef;
    private LinkedList networkParametersList;
    private WeakReference parentViewRef;

    public class AdMediationManagerHolder {
        protected static AdMediationManager INSTANCE = new AdMediationManager();

        protected AdMediationManagerHolder() {
        }
    }

    public static AdMediationManager getInstance() {
        return AdMediationManagerHolder.INSTANCE;
    }

    public void destroy() {
        ViewGroup parentView = getParentView();
        if (parentView != null) {
            parentView.removeAllViews();
        }
        if (this.networkParametersList != null) {
            synchronized (this.networkParametersList) {
                this.networkParametersList.clear();
            }
        }
        this.activityRef = null;
        this.parentViewRef = null;
        Log.d(LOG_TAG, "destroy mediation.");
    }

    public Activity getActivity() {
        if (this.activityRef == null) {
            return null;
        }
        return (Activity) this.activityRef.get();
    }

    public ViewGroup getParentView() {
        if (this.parentViewRef == null) {
            return null;
        }
        return (ViewGroup) this.parentViewRef.get();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        r0 = new jp.adlantis.android.mediation.AdMediationNetwork(r0).requestAd();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void nextNetwork() {
        /*
            r2 = this;
            java.lang.String r0 = "AdMediationManager"
            java.lang.String r1 = "go to next network."
            android.util.Log.d(r0, r1)
            r0 = 0
        L_0x0008:
            if (r0 != 0) goto L_0x001c
            java.util.LinkedList r0 = r2.networkParametersList
            if (r0 == 0) goto L_0x001c
            java.util.LinkedList r1 = r2.networkParametersList
            monitor-enter(r1)
            java.util.LinkedList r0 = r2.networkParametersList     // Catch:{ all -> 0x0028 }
            java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x0028 }
            jp.adlantis.android.mediation.AdMediationNetworkParameters r0 = (jp.adlantis.android.mediation.AdMediationNetworkParameters) r0     // Catch:{ all -> 0x0028 }
            if (r0 != 0) goto L_0x001d
            monitor-exit(r1)     // Catch:{ all -> 0x0028 }
        L_0x001c:
            return
        L_0x001d:
            monitor-exit(r1)     // Catch:{ all -> 0x0028 }
            jp.adlantis.android.mediation.AdMediationNetwork r1 = new jp.adlantis.android.mediation.AdMediationNetwork
            r1.<init>(r0)
            boolean r0 = r1.requestAd()
            goto L_0x0008
        L_0x0028:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0028 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.adlantis.android.mediation.AdMediationManager.nextNetwork():void");
    }

    public void requestAd(Activity activity, ViewGroup viewGroup, AdMediationNetworkParameters[] adMediationNetworkParametersArr) {
        if (activity == null || viewGroup == null || adMediationNetworkParametersArr == null) {
            Log.d(LOG_TAG, "mediation requestAd parameter error!");
            return;
        }
        setActivity(activity);
        setParentView(viewGroup);
        this.networkParametersList = new LinkedList(Arrays.asList(adMediationNetworkParametersArr));
        nextNetwork();
    }

    public void setActivity(Activity activity) {
        this.activityRef = new WeakReference(activity);
    }

    public void setParentView(ViewGroup viewGroup) {
        this.parentViewRef = new WeakReference(viewGroup);
    }
}
