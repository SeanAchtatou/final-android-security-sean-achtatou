package jp.adlantis.android.mediation.adapters;

import android.app.Activity;
import android.view.View;
import jp.adlantis.android.mediation.AdMediationAdapter;
import jp.adlantis.android.mediation.AdMediationAdapterListener;
import jp.adlantis.android.mediation.AdMediationNetworkParameters;
import net.nend.android.NendAdListener;
import net.nend.android.NendAdView;

public class NendAdapter implements AdMediationAdapter, NendAdListener {
    private NendAdView adView = null;
    private AdMediationAdapterListener listener = null;

    public void destroy() {
        if (this.adView != null) {
            this.adView.setListener(null);
        }
        this.adView = null;
    }

    public void onClick(NendAdView nendAdView) {
        if (this.listener != null) {
            this.listener.onTouchAd(this);
        }
    }

    public void onFailedToReceiveAd(NendAdView nendAdView) {
        if (this.listener != null) {
            this.listener.onFailedToReceiveAd(this);
        }
    }

    public void onReceiveAd(NendAdView nendAdView) {
        if (this.listener != null) {
            this.listener.onReceivedAd(this, this.adView);
            this.adView.pause();
        }
    }

    public View requestAd(AdMediationAdapterListener adMediationAdapterListener, Activity activity, AdMediationNetworkParameters adMediationNetworkParameters) {
        this.listener = adMediationAdapterListener;
        String parameter = adMediationNetworkParameters.getParameter("spot_id");
        this.adView = new NendAdView(activity, Integer.parseInt(parameter), adMediationNetworkParameters.getParameter("api_key"));
        this.adView.setListener(this);
        this.adView.loadAd();
        return null;
    }
}
