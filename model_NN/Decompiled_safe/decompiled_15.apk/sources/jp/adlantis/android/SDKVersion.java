package jp.adlantis.android;

public class SDKVersion {
    public static final String BUILDDATE = "2013-07-01 15:18:26";
    public static final String BUILDNUMBER = "1744";
    public static final String GIT_SHA = "49d1d37";
    public static final String VCS_VERSION = "v1.4.0-3-g49d1d37";
    public static final String VERSION = "1.4.0";
}
