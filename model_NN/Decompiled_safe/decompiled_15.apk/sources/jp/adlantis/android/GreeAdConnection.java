package jp.adlantis.android;

import android.net.Uri;

public class GreeAdConnection extends AdNetworkConnection {
    public GreeAdConnection() {
        this._host = "sp.ad.gap.adlantis.jp";
        this._conversionTagHost = "sp.conv.gap.adlantis.jp";
        this._conversionTagTestHost = "sp.gap.developer.gree.co.jp";
    }

    public Uri.Builder appendParameters(Uri.Builder builder) {
        String userId = GreeApiDelegator.getUserId();
        String str = null;
        if (userId != null) {
            str = GreeApiDelegator.getSha1DigestInString(userId);
        }
        if (str != null) {
            builder.appendQueryParameter("luid", "gree:" + str);
        }
        builder.appendQueryParameter("partner", "gap");
        return builder;
    }

    public String publisherIDMetadataKey() {
        return "GAP_Publisher_ID";
    }
}
