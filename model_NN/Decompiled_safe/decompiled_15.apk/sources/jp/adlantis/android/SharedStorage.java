package jp.adlantis.android;

import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.util.Properties;

public class SharedStorage extends Properties {
    private static final String LOCAL_STORAGE_PATH = "net.gree.android.ads/.data";
    private static final String LOG_TAG = "SharedStorage";
    private static String dataPath = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + LOCAL_STORAGE_PATH);
    private static SharedStorage instance;

    private SharedStorage() {
    }

    public static synchronized SharedStorage getInstance() {
        SharedStorage sharedStorage;
        synchronized (SharedStorage.class) {
            if (instance == null) {
                instance = new SharedStorage();
                try {
                    instance.load();
                } catch (Exception e) {
                    Log.w(LOG_TAG, "failed to load: " + e.getMessage());
                }
            }
            sharedStorage = instance;
        }
        return sharedStorage;
    }

    public static void setDataPath(String str) {
        dataPath = str;
    }

    public synchronized void clearAll() {
        File file = new File(dataPath);
        if (file.exists()) {
            file.delete();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0022 A[SYNTHETIC, Splitter:B:17:0x0022] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void load() {
        /*
            r3 = this;
            monitor-enter(r3)
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x0026 }
            java.lang.String r1 = jp.adlantis.android.SharedStorage.dataPath     // Catch:{ all -> 0x0026 }
            r0.<init>(r1)     // Catch:{ all -> 0x0026 }
            boolean r1 = r0.exists()     // Catch:{ all -> 0x0026 }
            if (r1 == 0) goto L_0x001c
            r2 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ all -> 0x001e }
            r1.<init>(r0)     // Catch:{ all -> 0x001e }
            super.load(r1)     // Catch:{ all -> 0x0029 }
            if (r1 == 0) goto L_0x001c
            r1.close()     // Catch:{ all -> 0x0026 }
        L_0x001c:
            monitor-exit(r3)
            return
        L_0x001e:
            r0 = move-exception
            r1 = r2
        L_0x0020:
            if (r1 == 0) goto L_0x0025
            r1.close()     // Catch:{ all -> 0x0026 }
        L_0x0025:
            throw r0     // Catch:{ all -> 0x0026 }
        L_0x0026:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x0029:
            r0 = move-exception
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.adlantis.android.SharedStorage.load():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x002e A[SYNTHETIC, Splitter:B:19:0x002e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void store() {
        /*
            r3 = this;
            monitor-enter(r3)
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x0032 }
            java.lang.String r1 = jp.adlantis.android.SharedStorage.dataPath     // Catch:{ all -> 0x0032 }
            r0.<init>(r1)     // Catch:{ all -> 0x0032 }
            boolean r1 = r0.exists()     // Catch:{ all -> 0x0032 }
            if (r1 != 0) goto L_0x0018
            java.io.File r1 = r0.getParentFile()     // Catch:{ all -> 0x0032 }
            r1.mkdirs()     // Catch:{ all -> 0x0032 }
            r0.createNewFile()     // Catch:{ all -> 0x0032 }
        L_0x0018:
            r2 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ all -> 0x002a }
            r1.<init>(r0)     // Catch:{ all -> 0x002a }
            java.lang.String r0 = ""
            super.store(r1, r0)     // Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0028
            r1.close()     // Catch:{ all -> 0x0032 }
        L_0x0028:
            monitor-exit(r3)
            return
        L_0x002a:
            r0 = move-exception
            r1 = r2
        L_0x002c:
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ all -> 0x0032 }
        L_0x0031:
            throw r0     // Catch:{ all -> 0x0032 }
        L_0x0032:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x0035:
            r0 = move-exception
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.adlantis.android.SharedStorage.store():void");
    }
}
