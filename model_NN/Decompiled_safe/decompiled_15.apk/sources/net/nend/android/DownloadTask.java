package net.nend.android;

import android.os.AsyncTask;
import java.lang.ref.WeakReference;
import org.apache.http.HttpEntity;

final class DownloadTask extends AsyncTask {
    /* access modifiers changed from: private */
    public final WeakReference mReference;

    interface Downloadable {
        String getRequestUrl();

        Object makeResponse(HttpEntity httpEntity);

        void onDownload(Object obj);
    }

    DownloadTask(Downloadable downloadable) {
        this.mReference = new WeakReference(downloadable);
    }

    /* access modifiers changed from: protected */
    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.Thread.setPriority(int):void in method: net.nend.android.DownloadTask.doInBackground(java.lang.Void[]):java.lang.Object, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.Thread.setPriority(int):void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public java.lang.Object doInBackground(java.lang.Void... r1) {
        /*
            r5 = this;
            r1 = 0
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            r2 = 10
            r0.setPriority(r2)
            boolean r0 = r5.isCancelled()
            if (r0 == 0) goto L_0x0012
            r0 = r1
        L_0x0011:
            return r0
        L_0x0012:
            java.lang.ref.WeakReference r0 = r5.mReference
            java.lang.Object r0 = r0.get()
            net.nend.android.DownloadTask$Downloadable r0 = (net.nend.android.DownloadTask.Downloadable) r0
            if (r0 == 0) goto L_0x00b2
            java.lang.String r2 = r0.getRequestUrl()
            if (r2 == 0) goto L_0x00b2
            java.lang.String r2 = r0.getRequestUrl()
            int r2 = r2.length()
            if (r2 <= 0) goto L_0x00b2
            java.lang.String r0 = r0.getRequestUrl()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Download from "
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            net.nend.android.NendLog.v(r2)
            org.apache.http.impl.client.DefaultHttpClient r2 = new org.apache.http.impl.client.DefaultHttpClient
            r2.<init>()
            org.apache.http.params.HttpParams r3 = r2.getParams()     // Catch:{ ClientProtocolException -> 0x0070, IOException -> 0x007f, IllegalStateException -> 0x008d, IllegalArgumentException -> 0x009b }
            r4 = 10000(0x2710, float:1.4013E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r3, r4)     // Catch:{ ClientProtocolException -> 0x0070, IOException -> 0x007f, IllegalStateException -> 0x008d, IllegalArgumentException -> 0x009b }
            r4 = 10000(0x2710, float:1.4013E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r3, r4)     // Catch:{ ClientProtocolException -> 0x0070, IOException -> 0x007f, IllegalStateException -> 0x008d, IllegalArgumentException -> 0x009b }
            java.lang.String r3 = "start request!"
            net.nend.android.NendLog.d(r3)     // Catch:{ ClientProtocolException -> 0x0070, IOException -> 0x007f, IllegalStateException -> 0x008d, IllegalArgumentException -> 0x009b }
            org.apache.http.client.methods.HttpGet r3 = new org.apache.http.client.methods.HttpGet     // Catch:{ ClientProtocolException -> 0x0070, IOException -> 0x007f, IllegalStateException -> 0x008d, IllegalArgumentException -> 0x009b }
            r3.<init>(r0)     // Catch:{ ClientProtocolException -> 0x0070, IOException -> 0x007f, IllegalStateException -> 0x008d, IllegalArgumentException -> 0x009b }
            net.nend.android.DownloadTask$1 r4 = new net.nend.android.DownloadTask$1     // Catch:{ ClientProtocolException -> 0x0070, IOException -> 0x007f, IllegalStateException -> 0x008d, IllegalArgumentException -> 0x009b }
            r4.<init>(r0)     // Catch:{ ClientProtocolException -> 0x0070, IOException -> 0x007f, IllegalStateException -> 0x008d, IllegalArgumentException -> 0x009b }
            java.lang.Object r0 = r2.execute(r3, r4)     // Catch:{ ClientProtocolException -> 0x0070, IOException -> 0x007f, IllegalStateException -> 0x008d, IllegalArgumentException -> 0x009b }
            org.apache.http.conn.ClientConnectionManager r1 = r2.getConnectionManager()
            r1.shutdown()
            goto L_0x0011
        L_0x0070:
            r0 = move-exception
            net.nend.android.NendStatus r3 = net.nend.android.NendStatus.ERR_HTTP_REQUEST     // Catch:{ all -> 0x00a9 }
            net.nend.android.NendLog.w(r3, r0)     // Catch:{ all -> 0x00a9 }
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
        L_0x007d:
            r0 = r1
            goto L_0x0011
        L_0x007f:
            r0 = move-exception
            net.nend.android.NendStatus r3 = net.nend.android.NendStatus.ERR_HTTP_REQUEST     // Catch:{ all -> 0x00a9 }
            net.nend.android.NendLog.w(r3, r0)     // Catch:{ all -> 0x00a9 }
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            goto L_0x007d
        L_0x008d:
            r0 = move-exception
            net.nend.android.NendStatus r3 = net.nend.android.NendStatus.ERR_HTTP_REQUEST     // Catch:{ all -> 0x00a9 }
            net.nend.android.NendLog.w(r3, r0)     // Catch:{ all -> 0x00a9 }
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            goto L_0x007d
        L_0x009b:
            r0 = move-exception
            net.nend.android.NendStatus r3 = net.nend.android.NendStatus.ERR_HTTP_REQUEST     // Catch:{ all -> 0x00a9 }
            net.nend.android.NendLog.w(r3, r0)     // Catch:{ all -> 0x00a9 }
            org.apache.http.conn.ClientConnectionManager r0 = r2.getConnectionManager()
            r0.shutdown()
            goto L_0x007d
        L_0x00a9:
            r0 = move-exception
            org.apache.http.conn.ClientConnectionManager r1 = r2.getConnectionManager()
            r1.shutdown()
            throw r0
        L_0x00b2:
            net.nend.android.NendStatus r0 = net.nend.android.NendStatus.ERR_INVALID_URL
            net.nend.android.NendLog.w(r0)
            goto L_0x007d
        */
        throw new UnsupportedOperationException("Method not decompiled: net.nend.android.DownloadTask.doInBackground(java.lang.Void[]):java.lang.Object");
    }

    /* access modifiers changed from: package-private */
    public boolean isFinished() {
        return getStatus() == AsyncTask.Status.FINISHED;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object obj) {
        Downloadable downloadable = (Downloadable) this.mReference.get();
        if (!isCancelled() && downloadable != null) {
            downloadable.onDownload(obj);
        }
    }
}
