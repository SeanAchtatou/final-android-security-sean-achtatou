package net.nend.android;

import android.text.TextUtils;
import java.util.ArrayList;
import jp.adlantis.android.AdlantisAd;
import net.nend.android.AdParameter;

final class NendAdIconResponse {
    private static /* synthetic */ int[] $SWITCH_TABLE$net$nend$android$AdParameter$ViewType;
    private ArrayList mAdParameterList;
    private String mImpressionCountUrl;
    private String mMessage;
    private final int mReloadIntervalInSeconds;
    private int mStatusCode;
    private final AdParameter.ViewType mViewType;

    final class Builder {
        static final /* synthetic */ boolean $assertionsDisabled = (!NendAdIconResponse.class.desiredAssertionStatus());
        /* access modifiers changed from: private */
        public ArrayList mAdParameterList;
        /* access modifiers changed from: private */
        public String mImpressionCountUrl;
        /* access modifiers changed from: private */
        public String mMessage;
        /* access modifiers changed from: private */
        public int mReloadIntervalInSeconds;
        /* access modifiers changed from: private */
        public int mStatusCode;
        /* access modifiers changed from: private */
        public AdParameter.ViewType mViewType = AdParameter.ViewType.NONE;

        Builder() {
        }

        /* access modifiers changed from: package-private */
        public NendAdIconResponse build() {
            return new NendAdIconResponse(this, null);
        }

        /* access modifiers changed from: package-private */
        public Builder setAdParameterList(ArrayList arrayList) {
            this.mAdParameterList = arrayList;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setImpressionCountUrl(String str) {
            if (str != null) {
                this.mImpressionCountUrl = str.replaceAll(" ", "%20");
            } else {
                this.mImpressionCountUrl = null;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setMessage(String str) {
            this.mMessage = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setReloadIntervalInSeconds(int i) {
            this.mReloadIntervalInSeconds = i;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setStatusCode(int i) {
            this.mStatusCode = i;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setViewType(AdParameter.ViewType viewType) {
            if ($assertionsDisabled || viewType != null) {
                this.mViewType = viewType;
                return this;
            }
            throw new AssertionError();
        }
    }

    static /* synthetic */ int[] $SWITCH_TABLE$net$nend$android$AdParameter$ViewType() {
        int[] iArr = $SWITCH_TABLE$net$nend$android$AdParameter$ViewType;
        if (iArr == null) {
            iArr = new int[AdParameter.ViewType.values().length];
            try {
                iArr[AdParameter.ViewType.ADVIEW.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AdParameter.ViewType.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdParameter.ViewType.WEBVIEW.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$net$nend$android$AdParameter$ViewType = iArr;
        }
        return iArr;
    }

    private NendAdIconResponse(Builder builder) {
        switch ($SWITCH_TABLE$net$nend$android$AdParameter$ViewType()[builder.mViewType.ordinal()]) {
            case AdlantisAd.ADTYPE_TEXT:
                break;
            default:
                throw new IllegalArgumentException("Unknown view type.");
        }
        if (TextUtils.isEmpty(builder.mImpressionCountUrl)) {
            throw new IllegalArgumentException("ImpressionCount Url is invalid.");
        }
        this.mViewType = AdParameter.ViewType.ADVIEW;
        this.mReloadIntervalInSeconds = builder.mReloadIntervalInSeconds;
        this.mStatusCode = builder.mStatusCode;
        this.mMessage = builder.mMessage;
        this.mImpressionCountUrl = builder.mImpressionCountUrl;
        this.mAdParameterList = builder.mAdParameterList;
    }

    /* synthetic */ NendAdIconResponse(Builder builder, NendAdIconResponse nendAdIconResponse) {
        this(builder);
    }

    public ArrayList getAdParameterList() {
        return this.mAdParameterList;
    }

    public String getImpressionCountUrl() {
        return this.mImpressionCountUrl;
    }

    public String getMessage() {
        return this.mMessage;
    }

    public int getReloadIntervalInSeconds() {
        return this.mReloadIntervalInSeconds;
    }

    public int getStatusCode() {
        return this.mStatusCode;
    }

    public AdParameter.ViewType getViewType() {
        return this.mViewType;
    }
}
