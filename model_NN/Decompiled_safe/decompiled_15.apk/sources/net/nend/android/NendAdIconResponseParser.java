package net.nend.android;

import android.content.Context;
import java.util.ArrayList;
import net.nend.android.AbsNendAdResponseParser;
import net.nend.android.AdParameter;
import net.nend.android.NendAdIconResponse;
import net.nend.android.NendAdResponse;
import org.json.JSONArray;
import org.json.JSONObject;

final class NendAdIconResponseParser extends AbsNendAdResponseParser {
    private static /* synthetic */ int[] $SWITCH_TABLE$net$nend$android$AbsNendAdResponseParser$ResponseType;
    private final int mIconViewCount;

    static /* synthetic */ int[] $SWITCH_TABLE$net$nend$android$AbsNendAdResponseParser$ResponseType() {
        int[] iArr = $SWITCH_TABLE$net$nend$android$AbsNendAdResponseParser$ResponseType;
        if (iArr == null) {
            iArr = new int[AbsNendAdResponseParser.ResponseType.values().length];
            try {
                iArr[AbsNendAdResponseParser.ResponseType.BANNER_APP_TARGETING.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AbsNendAdResponseParser.ResponseType.BANNER_NORMAL.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AbsNendAdResponseParser.ResponseType.BANNER_WEB_VIEW.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[AbsNendAdResponseParser.ResponseType.ICON_APP_TARGETING.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[AbsNendAdResponseParser.ResponseType.ICON_NORMAL.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[AbsNendAdResponseParser.ResponseType.UNSUPPORTED.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$net$nend$android$AbsNendAdResponseParser$ResponseType = iArr;
        }
        return iArr;
    }

    NendAdIconResponseParser(Context context, int i) {
        super(context);
        this.mIconViewCount = i;
    }

    private NendAdIconResponse getAppTargetingAd(JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList();
        NendAdIconResponse.Builder builder = new NendAdIconResponse.Builder();
        JSONArray jSONArray = jSONObject.getJSONArray("targeting_ads");
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            JSONArray jSONArray2 = jSONObject2.getJSONArray("conditions");
            int length2 = jSONArray2.length();
            int i2 = 0;
            while (true) {
                if (i2 >= length2) {
                    break;
                } else if (isTarget(jSONArray2.getJSONArray(i2))) {
                    NendAdResponse.Builder clickUrl = new NendAdResponse.Builder().setViewType(AdParameter.ViewType.ADVIEW).setIconId(jSONObject2.getString("icon_id")).setImageUrl(jSONObject2.getString("image_url")).setClickUrl(jSONObject2.getString("click_url"));
                    if (!jSONObject2.isNull("icon_text")) {
                        clickUrl.setTitleText(jSONObject2.getString("icon_text"));
                    }
                    arrayList.add(clickUrl.build());
                } else {
                    i2++;
                }
            }
            if (arrayList.size() >= this.mIconViewCount) {
                break;
            }
        }
        if (arrayList.size() < this.mIconViewCount && !jSONObject.isNull("default_ads")) {
            JSONArray jSONArray3 = jSONObject.getJSONArray("default_ads");
            for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                JSONObject jSONObject3 = jSONArray3.getJSONObject(i3);
                NendAdResponse.Builder clickUrl2 = new NendAdResponse.Builder().setViewType(AdParameter.ViewType.ADVIEW).setIconId(jSONObject3.getString("icon_id")).setImageUrl(jSONObject3.getString("image_url")).setClickUrl(jSONObject3.getString("click_url"));
                if (!jSONObject3.isNull("icon_text")) {
                    clickUrl2.setTitleText(jSONObject3.getString("icon_text"));
                }
                arrayList.add(clickUrl2.build());
                if (arrayList.size() >= this.mIconViewCount) {
                    break;
                }
            }
        }
        if (arrayList.size() == 0) {
            throw new NendException(NendStatus.ERR_OUT_OF_STOCK);
        }
        builder.setAdParameterList(arrayList);
        if (!jSONObject.isNull("status_code")) {
            builder.setStatusCode(jSONObject.getInt("status_code"));
        }
        if (!jSONObject.isNull("message")) {
            builder.setMessage(jSONObject.getString("message"));
        }
        if (!jSONObject.isNull("reload")) {
            builder.setReloadIntervalInSeconds(jSONObject.getInt("reload"));
        }
        if (!jSONObject.isNull("impression_count_url")) {
            builder.setImpressionCountUrl(jSONObject.getString("impression_count_url"));
        }
        builder.setViewType(AdParameter.ViewType.ADVIEW);
        return builder.build();
    }

    private NendAdIconResponse getNormalAd(JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList();
        NendAdIconResponse.Builder builder = new NendAdIconResponse.Builder();
        JSONArray jSONArray = jSONObject.getJSONArray("default_ads");
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            NendAdResponse.Builder clickUrl = new NendAdResponse.Builder().setViewType(AdParameter.ViewType.ADVIEW).setIconId(jSONObject2.getString("icon_id")).setImageUrl(jSONObject2.getString("image_url")).setClickUrl(jSONObject2.getString("click_url"));
            if (!jSONObject2.isNull("icon_text")) {
                clickUrl.setTitleText(jSONObject2.getString("icon_text"));
            }
            arrayList.add(clickUrl.build());
            if (arrayList.size() >= this.mIconViewCount) {
                break;
            }
        }
        if (arrayList.size() == 0) {
            throw new NendException(NendStatus.ERR_OUT_OF_STOCK);
        }
        builder.setAdParameterList(arrayList);
        if (!jSONObject.isNull("status_code")) {
            builder.setStatusCode(jSONObject.getInt("status_code"));
        }
        if (!jSONObject.isNull("message")) {
            builder.setMessage(jSONObject.getString("message"));
        }
        if (!jSONObject.isNull("reload")) {
            builder.setReloadIntervalInSeconds(jSONObject.getInt("reload"));
        }
        if (!jSONObject.isNull("impression_count_url")) {
            builder.setImpressionCountUrl(jSONObject.getString("impression_count_url"));
        }
        builder.setViewType(AdParameter.ViewType.ADVIEW);
        return builder.build();
    }

    /* access modifiers changed from: package-private */
    public NendAdIconResponse getResponseObject(AbsNendAdResponseParser.ResponseType responseType, JSONObject jSONObject) {
        switch ($SWITCH_TABLE$net$nend$android$AbsNendAdResponseParser$ResponseType()[responseType.ordinal()]) {
            case 5:
                return getNormalAd(jSONObject);
            case 6:
                return getAppTargetingAd(jSONObject);
            default:
                throw new NendException(NendStatus.ERR_INVALID_RESPONSE_TYPE);
        }
    }
}
