package net.nend.android;

import android.content.Context;
import android.util.DisplayMetrics;
import java.lang.ref.WeakReference;
import jp.adlantis.android.AdlantisAd;
import net.nend.android.AdParameter;
import net.nend.android.DownloadTask;
import net.nend.android.NendAdView;

final class NendAd implements Ad, DownloadTask.Downloadable {
    private static /* synthetic */ int[] $SWITCH_TABLE$net$nend$android$AdParameter$ViewType;
    static final /* synthetic */ boolean $assertionsDisabled = (!NendAd.class.desiredAssertionStatus());
    private String mClickUrl = null;
    private final Context mContext;
    private int mHeight = 50;
    private String mIconId;
    private String mImageUrl = null;
    private WeakReference mListenerReference = null;
    private DisplayMetrics mMetrics;
    private int mReloadIntervalInSeconds = 60;
    private final NendAdRequest mRequest;
    private DownloadTask mTask = null;
    private String mTitleText = null;
    private final String mUid;
    private AdParameter.ViewType mViewType = AdParameter.ViewType.NONE;
    private String mWebViewUrl = null;
    private int mWidth = 320;

    static /* synthetic */ int[] $SWITCH_TABLE$net$nend$android$AdParameter$ViewType() {
        int[] iArr = $SWITCH_TABLE$net$nend$android$AdParameter$ViewType;
        if (iArr == null) {
            iArr = new int[AdParameter.ViewType.values().length];
            try {
                iArr[AdParameter.ViewType.ADVIEW.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AdParameter.ViewType.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdParameter.ViewType.WEBVIEW.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$net$nend$android$AdParameter$ViewType = iArr;
        }
        return iArr;
    }

    NendAd(Context context, int i, String str, DisplayMetrics displayMetrics) {
        if (context == null) {
            throw new NullPointerException("Context is null.");
        } else if (i <= 0) {
            throw new IllegalArgumentException("Spot id is invalid. spot id : " + i);
        } else if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("Api key is invalid. api key : " + str);
        } else {
            this.mContext = context;
            this.mMetrics = displayMetrics;
            this.mRequest = new NendAdRequest(context, i, str);
            this.mUid = NendHelper.makeUid(context);
        }
    }

    private void setAdViewParam(AdParameter adParameter) {
        if ($assertionsDisabled || adParameter != null) {
            this.mViewType = AdParameter.ViewType.ADVIEW;
            this.mReloadIntervalInSeconds = NendHelper.setReloadIntervalInSeconds(adParameter.getReloadIntervalInSeconds());
            this.mImageUrl = adParameter.getImageUrl();
            this.mClickUrl = adParameter.getClickUrl();
            this.mTitleText = adParameter.getTitleText();
            this.mHeight = adParameter.getHeight();
            this.mWidth = adParameter.getWidth();
            this.mIconId = adParameter.getIconId();
            this.mWebViewUrl = null;
            return;
        }
        throw new AssertionError();
    }

    private void setWebViewParam(AdParameter adParameter) {
        if ($assertionsDisabled || adParameter != null) {
            this.mViewType = AdParameter.ViewType.WEBVIEW;
            this.mWebViewUrl = adParameter.getWebViewUrl();
            this.mImageUrl = null;
            this.mClickUrl = null;
            this.mTitleText = null;
            this.mIconId = null;
            this.mHeight = adParameter.getHeight();
            this.mWidth = adParameter.getWidth();
            return;
        }
        throw new AssertionError();
    }

    public void cancelRequest() {
        if (this.mTask != null) {
            this.mTask.cancel(true);
        }
    }

    public String getClickUrl() {
        return this.mClickUrl;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public String getIconId() {
        return this.mIconId;
    }

    public String getImageUrl() {
        return this.mImageUrl;
    }

    public AdListener getListener() {
        if (this.mListenerReference != null) {
            return (AdListener) this.mListenerReference.get();
        }
        return null;
    }

    public int getReloadIntervalInSeconds() {
        return this.mReloadIntervalInSeconds;
    }

    public String getRequestUrl() {
        return this.mRequest.getRequestUrl(this.mUid);
    }

    public String getTitleText() {
        return this.mTitleText;
    }

    public String getUid() {
        return this.mUid;
    }

    public AdParameter.ViewType getViewType() {
        return this.mViewType;
    }

    public String getWebViewUrl() {
        return this.mWebViewUrl;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public boolean isRequestable() {
        return this.mTask == null || this.mTask.isFinished();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.nend.android.NendLog.d(net.nend.android.NendStatus, java.lang.Throwable):int
     arg types: [net.nend.android.NendStatus, org.apache.http.ParseException]
     candidates:
      net.nend.android.NendLog.d(java.lang.String, java.lang.String):int
      net.nend.android.NendLog.d(java.lang.String, java.lang.Throwable):int
      net.nend.android.NendLog.d(net.nend.android.NendStatus, java.lang.String):int
      net.nend.android.NendLog.d(net.nend.android.NendStatus, java.lang.Throwable):int */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public net.nend.android.AdParameter makeResponse(org.apache.http.HttpEntity r4) {
        /*
            r3 = this;
            r1 = 0
            if (r4 != 0) goto L_0x0005
            r0 = r1
        L_0x0004:
            return r0
        L_0x0005:
            net.nend.android.NendAdResponseParser r0 = new net.nend.android.NendAdResponseParser     // Catch:{ ParseException -> 0x0017, IOException -> 0x0029 }
            android.content.Context r2 = r3.mContext     // Catch:{ ParseException -> 0x0017, IOException -> 0x0029 }
            r0.<init>(r2)     // Catch:{ ParseException -> 0x0017, IOException -> 0x0029 }
            java.lang.String r2 = org.apache.http.util.EntityUtils.toString(r4)     // Catch:{ ParseException -> 0x0017, IOException -> 0x0029 }
            java.lang.Object r0 = r0.parseResponse(r2)     // Catch:{ ParseException -> 0x0017, IOException -> 0x0029 }
            net.nend.android.AdParameter r0 = (net.nend.android.AdParameter) r0     // Catch:{ ParseException -> 0x0017, IOException -> 0x0029 }
            goto L_0x0004
        L_0x0017:
            r0 = move-exception
            boolean r2 = net.nend.android.NendAd.$assertionsDisabled
            if (r2 != 0) goto L_0x0022
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x0022:
            net.nend.android.NendStatus r2 = net.nend.android.NendStatus.ERR_HTTP_REQUEST
            net.nend.android.NendLog.d(r2, r0)
        L_0x0027:
            r0 = r1
            goto L_0x0004
        L_0x0029:
            r0 = move-exception
            boolean r2 = net.nend.android.NendAd.$assertionsDisabled
            if (r2 != 0) goto L_0x0034
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x0034:
            net.nend.android.NendStatus r2 = net.nend.android.NendStatus.ERR_HTTP_REQUEST
            net.nend.android.NendLog.d(r2, r0)
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: net.nend.android.NendAd.makeResponse(org.apache.http.HttpEntity):net.nend.android.AdParameter");
    }

    public void onDownload(AdParameter adParameter) {
        boolean z = true;
        AdListener listener = getListener();
        if (adParameter != null) {
            float f = this.mMetrics.density;
            float width = ((float) adParameter.getWidth()) * f;
            float height = f * ((float) adParameter.getHeight());
            if (width / 2.0f > ((float) this.mMetrics.widthPixels) || height / 2.0f > ((float) this.mMetrics.heightPixels) || width > ((float) this.mMetrics.widthPixels) || height > ((float) this.mMetrics.heightPixels)) {
                z = false;
                listener.onFailedToReceiveAd(NendAdView.NendError.AD_SIZE_TOO_LARGE);
            }
            switch ($SWITCH_TABLE$net$nend$android$AdParameter$ViewType()[adParameter.getViewType().ordinal()]) {
                case AdlantisAd.ADTYPE_TEXT:
                    setAdViewParam(adParameter);
                    break;
                case 3:
                    setWebViewParam(adParameter);
                    break;
                default:
                    if (!$assertionsDisabled) {
                        throw new AssertionError();
                    } else if (listener != null) {
                        listener.onFailedToReceiveAd(NendAdView.NendError.INVALID_RESPONSE_TYPE);
                        return;
                    } else {
                        return;
                    }
            }
            if (listener != null && z) {
                listener.onReceiveAd();
            }
        } else if (listener != null) {
            listener.onFailedToReceiveAd(NendAdView.NendError.FAILED_AD_REQUEST);
        }
    }

    public void removeListener() {
        this.mListenerReference = null;
    }

    public boolean requestAd() {
        if (!isRequestable()) {
            return false;
        }
        this.mTask = new DownloadTask(this);
        this.mTask.execute(new Void[0]);
        return true;
    }

    public void setListener(AdListener adListener) {
        this.mListenerReference = new WeakReference(adListener);
    }
}
