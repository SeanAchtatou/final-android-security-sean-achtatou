package net.nend.android;

import android.util.Log;
import jp.adlantis.android.AdlantisAd;

final class NendLog {
    static final String TAG = "nend_SDK";

    private NendLog() {
    }

    static int d(String str) {
        return outputLog(3, TAG, str, null);
    }

    static int d(String str, String str2) {
        return outputLog(3, str, str2, null);
    }

    static int d(String str, String str2, Throwable th) {
        return outputLog(3, str, str2, th);
    }

    static int d(String str, Throwable th) {
        return outputLog(3, TAG, str, th);
    }

    static int d(NendStatus nendStatus) {
        return outputLog(3, TAG, nendStatus.getMsg(), null);
    }

    static int d(NendStatus nendStatus, String str) {
        return outputLog(3, TAG, nendStatus.getMsg(str), null);
    }

    static int d(NendStatus nendStatus, Throwable th) {
        return outputLog(3, TAG, nendStatus.getMsg(), th);
    }

    static int e(String str) {
        return outputLog(6, TAG, str, null);
    }

    static int e(String str, String str2) {
        return outputLog(6, str, str2, null);
    }

    static int e(String str, String str2, Throwable th) {
        return outputLog(6, str, str2, th);
    }

    static int e(String str, Throwable th) {
        return outputLog(6, TAG, str, th);
    }

    static int e(NendStatus nendStatus) {
        return outputLog(6, TAG, nendStatus.getMsg(), null);
    }

    static int e(NendStatus nendStatus, String str) {
        return outputLog(6, TAG, nendStatus.getMsg(str), null);
    }

    static int e(NendStatus nendStatus, Throwable th) {
        return outputLog(6, TAG, nendStatus.getMsg(), th);
    }

    static int i(String str) {
        return outputLog(4, TAG, str, null);
    }

    static int i(String str, String str2) {
        return outputLog(4, str, str2, null);
    }

    static int i(String str, String str2, Throwable th) {
        return outputLog(4, str, str2, th);
    }

    static int i(String str, Throwable th) {
        return outputLog(4, TAG, str, th);
    }

    static int i(NendStatus nendStatus) {
        return outputLog(4, TAG, nendStatus.getMsg(), null);
    }

    static int i(NendStatus nendStatus, String str) {
        return outputLog(4, TAG, nendStatus.getMsg(str), null);
    }

    static int i(NendStatus nendStatus, Throwable th) {
        return outputLog(4, TAG, nendStatus.getMsg(), th);
    }

    private static boolean isLoggable(String str, int i) {
        switch (i) {
            case AdlantisAd.ADTYPE_TEXT:
            case 3:
                return Log.isLoggable(str, i) && NendHelper.isDebuggable() && NendHelper.isDev();
            default:
                return Log.isLoggable(str, i) && NendHelper.isDebuggable();
        }
    }

    private static int outputLog(int i, String str, String str2, Throwable th) {
        if (!isLoggable(str, i)) {
            return 0;
        }
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[4];
        String str3 = String.valueOf(stackTraceElement.getClassName()) + "." + stackTraceElement.getMethodName() + ":\n" + str2;
        if (th != null) {
            str3 = String.valueOf(str3) + 10 + Log.getStackTraceString(th);
        }
        return Log.println(i, str, str3);
    }

    static int v(String str) {
        return outputLog(2, TAG, str, null);
    }

    static int v(String str, String str2) {
        return outputLog(2, str, str2, null);
    }

    static int v(String str, String str2, Throwable th) {
        return outputLog(2, str, str2, th);
    }

    static int v(String str, Throwable th) {
        return outputLog(2, TAG, str, th);
    }

    static int v(NendStatus nendStatus) {
        return outputLog(2, TAG, nendStatus.getMsg(), null);
    }

    static int v(NendStatus nendStatus, String str) {
        return outputLog(2, TAG, nendStatus.getMsg(str), null);
    }

    static int v(NendStatus nendStatus, Throwable th) {
        return outputLog(2, TAG, nendStatus.getMsg(), th);
    }

    static int w(String str) {
        return outputLog(5, TAG, str, null);
    }

    static int w(String str, String str2) {
        return outputLog(5, str, str2, null);
    }

    static int w(String str, String str2, Throwable th) {
        return outputLog(5, str, str2, th);
    }

    static int w(String str, Throwable th) {
        return outputLog(5, TAG, str, th);
    }

    static int w(NendStatus nendStatus) {
        return outputLog(5, TAG, nendStatus.getMsg(), null);
    }

    static int w(NendStatus nendStatus, String str) {
        return outputLog(5, TAG, nendStatus.getMsg(str), null);
    }

    static int w(NendStatus nendStatus, Throwable th) {
        return outputLog(5, TAG, nendStatus.getMsg(), th);
    }
}
