package net.nend.android;

import android.os.AsyncTask;
import android.text.TextUtils;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

final class ImpressionCountTask extends AsyncTask {
    ImpressionCountTask() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.nend.android.NendLog.w(net.nend.android.NendStatus, java.lang.Throwable):int
     arg types: [net.nend.android.NendStatus, org.apache.http.client.ClientProtocolException]
     candidates:
      net.nend.android.NendLog.w(java.lang.String, java.lang.String):int
      net.nend.android.NendLog.w(java.lang.String, java.lang.Throwable):int
      net.nend.android.NendLog.w(net.nend.android.NendStatus, java.lang.String):int
      net.nend.android.NendLog.w(net.nend.android.NendStatus, java.lang.Throwable):int */
    /* access modifiers changed from: protected */
    public Void doInBackground(String... strArr) {
        Thread.currentThread().setPriority(10);
        if (isCancelled()) {
            return null;
        }
        String str = strArr[0];
        if (!TextUtils.isEmpty(str)) {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            try {
                HttpParams params = defaultHttpClient.getParams();
                HttpConnectionParams.setConnectionTimeout(params, 10000);
                HttpConnectionParams.setSoTimeout(params, 10000);
                NendLog.d("start request!");
                return (Void) defaultHttpClient.execute(new HttpGet(str), new ResponseHandler() {
                    public Void handleResponse(HttpResponse httpResponse) {
                        return null;
                    }
                });
            } catch (ClientProtocolException e) {
                NendLog.w(NendStatus.ERR_HTTP_REQUEST, (Throwable) e);
            } catch (IOException e2) {
                NendLog.w(NendStatus.ERR_HTTP_REQUEST, e2);
            } catch (IllegalStateException e3) {
                NendLog.w(NendStatus.ERR_HTTP_REQUEST, e3);
            } catch (IllegalArgumentException e4) {
                NendLog.w(NendStatus.ERR_HTTP_REQUEST, e4);
            } finally {
                defaultHttpClient.getConnectionManager().shutdown();
            }
        } else {
            NendLog.w(NendStatus.ERR_INVALID_URL);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Void voidR) {
    }
}
