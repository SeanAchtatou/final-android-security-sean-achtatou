package net.nend.android;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import net.nend.android.NendAdIconLoader;
import net.nend.android.NendConstants;

public class NendAdIconLayout extends LinearLayout implements NendAdIconLoader.OnClickListner, NendAdIconLoader.OnFailedListner, NendAdIconLoader.OnReceiveListner {
    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;
    private final int WC = -2;
    private String mApiKey;
    private NendAdIconLoader.OnClickListner mClickListner;
    private NendAdIconLoader.OnFailedListner mFailedListner;
    private int mIconCount = 0;
    private NendAdIconLoader mIconLoader;
    private NendAdIconLoader.OnReceiveListner mReceiveListner;
    private int mSpotId;
    private int mTitleColor = -16777216;
    private boolean mTitleVisible = true;

    public NendAdIconLayout(Context context, int i, String str, int i2) {
        super(context);
        this.mSpotId = i;
        this.mApiKey = str;
        this.mIconCount = i2;
    }

    public NendAdIconLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (attributeSet == null) {
            throw new NullPointerException(NendStatus.ERR_INVALID_ATTRIBUTE_SET.getMsg());
        }
        String attributeValue = attributeSet.getAttributeValue(null, NendConstants.IconAttribute.ICON_COUNT.getName());
        if (attributeValue == null) {
            throw new NullPointerException(NendStatus.ERR_INVALID_ICON_COUNT.getMsg());
        }
        String attributeValue2 = attributeSet.getAttributeValue(null, NendConstants.IconAttribute.ICON_ORIENTATION.getName());
        if (attributeValue2 != null && "vertical".equals(attributeValue2)) {
            setOrientation(1);
        }
        String attributeValue3 = attributeSet.getAttributeValue(null, NendConstants.IconAttribute.TITLE_COLOR.getName());
        if (attributeValue3 != null) {
            try {
                this.mTitleColor = Color.parseColor(attributeValue3);
            } catch (Exception e) {
                this.mTitleColor = -16777216;
            }
        }
        this.mTitleVisible = attributeSet.getAttributeBooleanValue(null, NendConstants.IconAttribute.TITLE_VISIBLE.getName(), true);
        this.mIconCount = Integer.parseInt(attributeValue);
        this.mSpotId = Integer.parseInt(attributeSet.getAttributeValue(null, NendConstants.Attribute.SPOT_ID.getName()));
        this.mApiKey = attributeSet.getAttributeValue(null, NendConstants.Attribute.API_KEY.getName());
        loadAd();
    }

    public void loadAd() {
        this.mIconLoader = new NendAdIconLoader(getContext(), this.mSpotId, this.mApiKey);
        int i = 0;
        while (i < this.mIconCount && i <= 3) {
            NendAdIconView nendAdIconView = new NendAdIconView(getContext());
            nendAdIconView.setTitleColor(this.mTitleColor);
            nendAdIconView.setTitleVisible(this.mTitleVisible);
            this.mIconLoader.addIconView(nendAdIconView);
            addView(nendAdIconView, new LinearLayout.LayoutParams(-2, -2));
            i++;
        }
        this.mIconLoader.loadAd();
        this.mIconLoader.setOnClickListner(this);
        this.mIconLoader.setOnFailedListner(this);
        this.mIconLoader.setOnReceiveLisner(this);
    }

    public void onClick(NendAdIconView nendAdIconView) {
        if (this.mClickListner != null) {
            this.mClickListner.onClick(nendAdIconView);
        }
    }

    public void onFailedToReceiveAd(NendIconError nendIconError) {
        if (this.mFailedListner != null) {
            this.mFailedListner.onFailedToReceiveAd(nendIconError);
        }
    }

    public void onReceiveAd(NendAdIconView nendAdIconView) {
        if (this.mReceiveListner != null) {
            this.mReceiveListner.onReceiveAd(nendAdIconView);
        }
    }

    public void pause() {
        this.mIconLoader.pause();
    }

    public void resume() {
        this.mIconLoader.resume();
    }

    public void setIconOrientation(int i) {
        setOrientation(i);
    }

    public void setOnClickListner(NendAdIconLoader.OnClickListner onClickListner) {
        this.mClickListner = onClickListner;
    }

    public void setOnFailedListner(NendAdIconLoader.OnFailedListner onFailedListner) {
        this.mFailedListner = onFailedListner;
    }

    public void setOnReceiveLisner(NendAdIconLoader.OnReceiveListner onReceiveListner) {
        this.mReceiveListner = onReceiveListner;
    }

    public void setTitleColor(int i) {
        this.mTitleColor = i;
    }

    public void setTitleVisible(boolean z) {
        this.mTitleVisible = z;
    }
}
