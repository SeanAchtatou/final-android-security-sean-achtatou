package net.nend.android;

import android.content.Context;
import jp.adlantis.android.AdlantisAd;
import net.nend.android.AbsNendAdResponseParser;
import net.nend.android.AdParameter;
import net.nend.android.NendAdResponse;
import org.json.JSONArray;
import org.json.JSONObject;

final class NendAdResponseParser extends AbsNendAdResponseParser {
    private static /* synthetic */ int[] $SWITCH_TABLE$net$nend$android$AbsNendAdResponseParser$ResponseType;

    static /* synthetic */ int[] $SWITCH_TABLE$net$nend$android$AbsNendAdResponseParser$ResponseType() {
        int[] iArr = $SWITCH_TABLE$net$nend$android$AbsNendAdResponseParser$ResponseType;
        if (iArr == null) {
            iArr = new int[AbsNendAdResponseParser.ResponseType.values().length];
            try {
                iArr[AbsNendAdResponseParser.ResponseType.BANNER_APP_TARGETING.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AbsNendAdResponseParser.ResponseType.BANNER_NORMAL.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AbsNendAdResponseParser.ResponseType.BANNER_WEB_VIEW.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[AbsNendAdResponseParser.ResponseType.ICON_APP_TARGETING.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[AbsNendAdResponseParser.ResponseType.ICON_NORMAL.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[AbsNendAdResponseParser.ResponseType.UNSUPPORTED.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            $SWITCH_TABLE$net$nend$android$AbsNendAdResponseParser$ResponseType = iArr;
        }
        return iArr;
    }

    public NendAdResponseParser(Context context) {
        super(context);
    }

    private AdParameter getAppTargetingAd(JSONObject jSONObject) {
        JSONArray jSONArray = jSONObject.getJSONArray("targeting_ads");
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            JSONArray jSONArray2 = jSONObject2.getJSONArray("conditions");
            int length2 = jSONArray2.length();
            for (int i2 = 0; i2 < length2; i2++) {
                if (isTarget(jSONArray2.getJSONArray(i2))) {
                    NendAdResponse.Builder width = new NendAdResponse.Builder().setViewType(AdParameter.ViewType.ADVIEW).setImageUrl(jSONObject2.getString("image_url")).setClickUrl(jSONObject2.getString("click_url")).setHeight(jSONObject.getInt("height")).setWidth(jSONObject.getInt("width"));
                    if (!jSONObject.isNull("reload")) {
                        width.setReloadIntervalInSeconds(jSONObject.getInt("reload"));
                    }
                    return width.build();
                }
            }
        }
        if (!jSONObject.isNull("default_ad")) {
            return getNormalAd(jSONObject);
        }
        throw new NendException(NendStatus.ERR_OUT_OF_STOCK);
    }

    private AdParameter getNormalAd(JSONObject jSONObject) {
        JSONObject jSONObject2 = jSONObject.getJSONObject("default_ad");
        NendAdResponse.Builder width = new NendAdResponse.Builder().setViewType(AdParameter.ViewType.ADVIEW).setImageUrl(jSONObject2.getString("image_url")).setClickUrl(jSONObject2.getString("click_url")).setHeight(jSONObject.getInt("height")).setWidth(jSONObject.getInt("width"));
        if (!jSONObject.isNull("reload")) {
            width.setReloadIntervalInSeconds(jSONObject.getInt("reload"));
        }
        return width.build();
    }

    private AdParameter getWebViewAd(JSONObject jSONObject) {
        return new NendAdResponse.Builder().setViewType(AdParameter.ViewType.WEBVIEW).setWebViewUrl(jSONObject.getString("web_view_url")).setHeight(jSONObject.getInt("height")).setWidth(jSONObject.getInt("width")).build();
    }

    /* access modifiers changed from: package-private */
    public AdParameter getResponseObject(AbsNendAdResponseParser.ResponseType responseType, JSONObject jSONObject) {
        switch ($SWITCH_TABLE$net$nend$android$AbsNendAdResponseParser$ResponseType()[responseType.ordinal()]) {
            case AdlantisAd.ADTYPE_TEXT:
                return getNormalAd(jSONObject);
            case 3:
                return getWebViewAd(jSONObject);
            case 4:
                return getAppTargetingAd(jSONObject);
            default:
                throw new NendException(NendStatus.ERR_INVALID_RESPONSE_TYPE);
        }
    }
}
