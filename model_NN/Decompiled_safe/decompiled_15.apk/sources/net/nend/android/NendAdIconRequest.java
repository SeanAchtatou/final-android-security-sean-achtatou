package net.nend.android;

import android.content.Context;
import android.net.Uri;

public class NendAdIconRequest extends AbsNendAdRequest {
    private int mRequestCount;

    NendAdIconRequest(Context context, int i, String str) {
        super(context, i, str);
    }

    /* access modifiers changed from: package-private */
    public String buildRequestUrl(String str) {
        return new Uri.Builder().scheme(this.mProtocol).authority(this.mDomain).path(this.mPath).appendQueryParameter("apikey", this.mApiKey).appendQueryParameter("spot", String.valueOf(this.mSpotId)).appendQueryParameter("uid", str).appendQueryParameter("os", getOS()).appendQueryParameter("version", getVersion()).appendQueryParameter("model", getModel()).appendQueryParameter("device", getDevice()).appendQueryParameter("localize", getLocale()).appendQueryParameter("sdkver", getSDKVersion()).appendQueryParameter("ad_num", String.valueOf(getRequestCount())).toString();
    }

    /* access modifiers changed from: package-private */
    public String getDomain() {
        return "ad3.nend.net";
    }

    /* access modifiers changed from: package-private */
    public String getPath() {
        return "nia.php";
    }

    /* access modifiers changed from: package-private */
    public int getRequestCount() {
        return this.mRequestCount;
    }

    /* access modifiers changed from: package-private */
    public void setRequestCount(int i) {
        this.mRequestCount = i;
    }
}
