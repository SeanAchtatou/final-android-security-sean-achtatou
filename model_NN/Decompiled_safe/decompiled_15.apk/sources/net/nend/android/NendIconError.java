package net.nend.android;

import net.nend.android.NendAdView;

public class NendIconError {
    public static final int ERROR_ICONVIEW = 1;
    public static final int ERROR_LOADER = 0;
    private int errorType;
    private NendAdIconView iconView = null;
    private NendAdIconLoader loader = null;
    private NendAdView.NendError nendError;

    public int getErrorType() {
        return this.errorType;
    }

    public NendAdIconView getIconView() {
        return this.iconView;
    }

    public NendAdIconLoader getLoader() {
        return this.loader;
    }

    public NendAdView.NendError getNendError() {
        return this.nendError;
    }

    public void setErrorType(int i) {
        this.errorType = i;
    }

    /* access modifiers changed from: package-private */
    public void setIconView(NendAdIconView nendAdIconView) {
        this.iconView = nendAdIconView;
    }

    /* access modifiers changed from: package-private */
    public void setLoader(NendAdIconLoader nendAdIconLoader) {
        this.loader = nendAdIconLoader;
    }

    public void setNendError(NendAdView.NendError nendError2) {
        this.nendError = nendError2;
    }
}
