package net.nend.android;

final class NendConstants {
    static final boolean IS_DEBUG_CODE = false;
    static final String NEND_UID_FILE_NAME = "NENDUUID";
    static final String VERSION = "2.3.0";

    class AdDefaultParams {
        static final int HEIGHT = 50;
        static final int MAX_AD_RELOAD_INTERVAL_IN_SECONDS = 99999;
        static final int MIN_AD_RELOAD_INTERVAL_IN_SECONDS = 30;
        static final int RELOAD_INTERVAL_IN_SECONDS = 60;
        static final int WIDTH = 320;

        private AdDefaultParams() {
        }
    }

    enum Attribute {
        SPOT_ID("NendSpotId"),
        API_KEY("NendApiKey"),
        RELOADABLE("NendReloadable");
        
        private String name;

        private Attribute(String str) {
            this.name = str;
        }

        /* access modifiers changed from: package-private */
        public String getName() {
            return this.name;
        }
    }

    enum IconAttribute {
        TITLE_COLOR("NendTitleColor"),
        TITLE_VISIBLE("NendTitleVisible"),
        ICON_COUNT("NendIconCount"),
        ICON_ORIENTATION("NendOrientation");
        
        private String name;

        private IconAttribute(String str) {
            this.name = str;
        }

        /* access modifiers changed from: package-private */
        public String getName() {
            return this.name;
        }
    }

    enum MetaData {
        ADSCHEME("NendAdScheme"),
        ADAUTHORITY("NendAdAuthority"),
        ADPATH("NendAdPath"),
        OPT_OUT_URL("NendOptOutUrl"),
        OPT_OUT_IMAGE_URL("NendOptOutImageUrl");
        
        private String name;

        private MetaData(String str) {
            this.name = str;
        }

        /* access modifiers changed from: package-private */
        public String getName() {
            return this.name;
        }
    }

    class NendHttpParams {
        static final int CONNECTION_TIMEOUT_IN_SECOND = 10;
        static final int SOCKET_TIMEOUT_IN_SECOND = 10;

        private NendHttpParams() {
        }
    }

    final class OptOutParams {
        static final String PAGE_URL = "http://nend.net/privacy/optsdkgate";

        private OptOutParams() {
        }
    }

    final class RequestParams {
        static final String BANNER_DOMAIN = "ad1.nend.net";
        static final String BANNER_PATH = "na.php";
        static final String ICON_DOMAIN = "ad3.nend.net";
        static final String ICON_PATH = "nia.php";
        static final String PROTOCOL = "http";

        private RequestParams() {
        }
    }

    private NendConstants() {
    }
}
