package net.nend.android;

import android.content.Context;
import android.net.Uri;

public class NendAdRequest extends AbsNendAdRequest {
    public NendAdRequest(Context context, int i, String str) {
        super(context, i, str);
    }

    /* access modifiers changed from: package-private */
    public String buildRequestUrl(String str) {
        return new Uri.Builder().scheme(this.mProtocol).authority(this.mDomain).path(this.mPath).appendQueryParameter("apikey", this.mApiKey).appendQueryParameter("spot", String.valueOf(this.mSpotId)).appendQueryParameter("uid", str).appendQueryParameter("os", getOS()).appendQueryParameter("version", getVersion()).appendQueryParameter("model", getModel()).appendQueryParameter("device", getDevice()).appendQueryParameter("localize", getLocale()).appendQueryParameter("sdkver", getSDKVersion()).toString();
    }

    /* access modifiers changed from: package-private */
    public String getDomain() {
        return "ad1.nend.net";
    }

    /* access modifiers changed from: package-private */
    public String getPath() {
        return "na.php";
    }
}
