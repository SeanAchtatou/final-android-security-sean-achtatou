package net.nend.android;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import java.io.IOException;
import jp.adlantis.android.AdlantisAd;
import net.nend.android.AdParameter;
import net.nend.android.DownloadTask;
import net.nend.android.NendAdImageView;
import net.nend.android.NendConstants;
import org.apache.http.HttpEntity;

public final class NendAdView extends RelativeLayout implements AdListener, DownloadTask.Downloadable, NendAdImageView.OnAdImageClickListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$net$nend$android$AdParameter$ViewType;
    static final /* synthetic */ boolean $assertionsDisabled = (!NendAdView.class.desiredAssertionStatus());
    private Ad mAd;
    private String mApiKey;
    private NendAdController mController;
    private float mDensity;
    private boolean mHasWindowFocus;
    private NendAdImageView mImageView;
    private boolean mIsClicked;
    private RelativeLayout mLayout;
    private NendAdListener mListener;
    private DisplayMetrics mMetrics;
    private NendError mNendError;
    private OptOutImageView mOptOutImageView;
    private int mSpotId;
    private DownloadTask mTask;
    private WebView mWebView;

    public enum NendError {
        AD_SIZE_TOO_LARGE(840, "Ad size will not fit on screen."),
        INVALID_RESPONSE_TYPE(841, "Response type is invalid."),
        FAILED_AD_REQUEST(842, "Failed to Ad request."),
        FAILED_AD_DOWNLOAD(843, "Failed to Ad download."),
        AD_SIZE_DIFFERENCES(844, "Ad size differences.");
        
        private final int code;
        private final String message;

        private NendError(int i, String str) {
            this.code = i;
            this.message = str;
        }

        public int getCode() {
            return this.code;
        }

        public String getMessage() {
            return this.message;
        }
    }

    static /* synthetic */ int[] $SWITCH_TABLE$net$nend$android$AdParameter$ViewType() {
        int[] iArr = $SWITCH_TABLE$net$nend$android$AdParameter$ViewType;
        if (iArr == null) {
            iArr = new int[AdParameter.ViewType.values().length];
            try {
                iArr[AdParameter.ViewType.ADVIEW.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AdParameter.ViewType.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdParameter.ViewType.WEBVIEW.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$net$nend$android$AdParameter$ViewType = iArr;
        }
        return iArr;
    }

    public NendAdView(Context context, int i, String str) {
        super(context, null, 0);
        this.mDensity = 1.0f;
        this.mAd = null;
        this.mController = null;
        this.mListener = null;
        this.mTask = null;
        this.mHasWindowFocus = false;
        this.mLayout = null;
        this.mOptOutImageView = null;
        this.mImageView = null;
        this.mWebView = null;
        this.mIsClicked = false;
        init(context, i, str);
    }

    public NendAdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NendAdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mDensity = 1.0f;
        this.mAd = null;
        this.mController = null;
        this.mListener = null;
        this.mTask = null;
        this.mHasWindowFocus = false;
        this.mLayout = null;
        this.mOptOutImageView = null;
        this.mImageView = null;
        this.mWebView = null;
        this.mIsClicked = false;
        if (attributeSet == null) {
            throw new NullPointerException(NendStatus.ERR_INVALID_ATTRIBUTE_SET.getMsg());
        }
        init(context, Integer.parseInt(attributeSet.getAttributeValue(null, NendConstants.Attribute.SPOT_ID.getName())), attributeSet.getAttributeValue(null, NendConstants.Attribute.API_KEY.getName()));
        if (!attributeSet.getAttributeBooleanValue(null, NendConstants.Attribute.RELOADABLE.getName(), true)) {
            pause();
        }
        loadAd();
    }

    private void deallocateAd() {
        if (this.mAd != null) {
            this.mAd.removeListener();
            this.mAd = null;
        }
    }

    private void deallocateAdView() {
        if (this.mLayout != null) {
            this.mLayout = null;
        }
        if (this.mOptOutImageView != null) {
            this.mOptOutImageView.setImageDrawable(null);
            this.mOptOutImageView.deallocateImage();
            this.mOptOutImageView = null;
        }
        if (this.mImageView != null) {
            this.mImageView = null;
        }
    }

    private void deallocateChildView() {
        removeAllViews();
        deallocateAdView();
        deallocateWebView();
    }

    private void deallocateController() {
        if (this.mController != null) {
            this.mController.cancelRequest();
            this.mController = null;
        }
    }

    private void deallocateField() {
        deallocateController();
        deallocateTask();
        deallocateAd();
        removeListener();
        deallocateChildView();
    }

    private void deallocateTask() {
        if (this.mTask != null) {
            this.mTask.cancel(true);
            this.mTask = null;
        }
    }

    private void deallocateWebView() {
        if (this.mWebView != null) {
            this.mWebView.stopLoading();
            this.mWebView.pauseTimers();
            this.mWebView.getSettings().setJavaScriptEnabled(false);
            this.mWebView.setWebChromeClient(null);
            this.mWebView.setWebViewClient(null);
            removeView(this.mWebView);
            this.mWebView.removeAllViews();
            this.mWebView.destroy();
            this.mWebView = null;
        }
    }

    private void init(Context context, int i, String str) {
        if (i <= 0) {
            throw new IllegalArgumentException(NendStatus.ERR_INVALID_SPOT_ID.getMsg("spot id : " + i));
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException(NendStatus.ERR_INVALID_API_KEY.getMsg("api key : " + str));
        } else {
            NendHelper.setDebuggable(context);
            this.mMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(this.mMetrics);
            this.mDensity = this.mMetrics.density;
            this.mSpotId = i;
            this.mApiKey = str;
            this.mAd = new NendAd(context, i, str, this.mMetrics);
            this.mAd.setListener(this);
            this.mController = new NendAdController(this.mAd);
        }
    }

    private boolean isDeallocate() {
        return this.mAd == null;
    }

    private boolean isSizeAppropriate(Bitmap bitmap) {
        int height = this.mAd.getHeight();
        int width = this.mAd.getWidth();
        int height2 = bitmap.getHeight();
        int width2 = bitmap.getWidth();
        if (width2 == 320 && height2 == 48) {
            height2 = 50;
        }
        return (height == height2 && width == width2) || (height * 2 == height2 && width * 2 == width2);
    }

    private void restartController() {
        if (this.mController == null) {
            if (this.mAd == null) {
                this.mAd = new NendAd(getContext(), this.mSpotId, this.mApiKey, this.mMetrics);
                this.mAd.setListener(this);
            }
            this.mController = new NendAdController(this.mAd);
        }
    }

    private void setAdView(Bitmap bitmap) {
        if (!$assertionsDisabled && bitmap == null) {
            throw new AssertionError();
        } else if (!$assertionsDisabled && this.mAd == null) {
            throw new AssertionError();
        } else if (this.mAd != null) {
            removeAllViews();
            deallocateWebView();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (((float) this.mAd.getWidth()) * this.mDensity), (int) (((float) this.mAd.getHeight()) * this.mDensity));
            if (this.mLayout == null || this.mImageView == null || this.mOptOutImageView == null || !this.mOptOutImageView.hasDrawable()) {
                this.mLayout = new RelativeLayout(getContext());
                this.mImageView = new NendAdImageView(getContext());
                this.mImageView.setAdInfo(bitmap, this.mAd.getClickUrl());
                this.mImageView.setOnAdImageClickListener(this);
                this.mLayout.addView(this.mImageView, layoutParams);
                this.mOptOutImageView = new OptOutImageView(getContext(), this.mAd.getUid(), this.mSpotId);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.addRule(11);
                this.mLayout.addView(this.mOptOutImageView, layoutParams2);
            } else {
                this.mImageView.setAdInfo(bitmap, this.mAd.getClickUrl());
            }
            this.mOptOutImageView.bringToFront();
            addView(this.mLayout, layoutParams);
        }
    }

    private void setWebView() {
        if ($assertionsDisabled || this.mAd != null) {
            removeAllViews();
            deallocateAdView();
            this.mWebView = new NendAdWebView(getContext());
            addView(this.mWebView, new RelativeLayout.LayoutParams((int) (((float) this.mAd.getWidth()) * this.mDensity), (int) (((float) this.mAd.getHeight()) * this.mDensity)));
            this.mWebView.resumeTimers();
            this.mWebView.loadUrl(this.mAd.getWebViewUrl());
            return;
        }
        throw new AssertionError();
    }

    public NendError getNendError() {
        return this.mNendError;
    }

    public String getRequestUrl() {
        return this.mAd.getImageUrl();
    }

    /* access modifiers changed from: package-private */
    public boolean hasView(View view) {
        return indexOfChild(view) >= 0;
    }

    public void loadAd() {
        restartController();
        this.mController.requestAd();
    }

    public Bitmap makeResponse(HttpEntity httpEntity) {
        if (httpEntity == null) {
            return null;
        }
        try {
            return BitmapFactory.decodeStream(httpEntity.getContent());
        } catch (IllegalStateException e) {
            if (!$assertionsDisabled) {
                throw new AssertionError();
            }
            NendLog.d(NendStatus.ERR_HTTP_REQUEST, e);
            return null;
        } catch (IOException e2) {
            if (!$assertionsDisabled) {
                throw new AssertionError();
            }
            NendLog.d(NendStatus.ERR_HTTP_REQUEST, e2);
            return null;
        }
    }

    public void onAdImageClick(View view) {
        this.mIsClicked = true;
        if (this.mListener != null) {
            this.mListener.onClick(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.mAd == null) {
            this.mAd = new NendAd(getContext(), this.mSpotId, this.mApiKey, this.mMetrics);
            this.mAd.setListener(this);
            this.mController = new NendAdController(this.mAd);
            loadAd();
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        deallocateField();
        init(getContext(), this.mSpotId, this.mApiKey);
        loadAd();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        NendLog.d("onDetachedFromWindow!");
        deallocateField();
        super.onDetachedFromWindow();
    }

    public void onDownload(Bitmap bitmap) {
        NendLog.d("onImageDownload!");
        if (bitmap == null || this.mController == null) {
            onFailedToReceiveAd(NendError.FAILED_AD_DOWNLOAD);
        } else if (!isSizeAppropriate(bitmap)) {
            onFailedToReceiveAd(NendError.AD_SIZE_DIFFERENCES);
        } else {
            setAdView(bitmap);
            this.mController.reloadAd();
            if (this.mListener != null) {
                this.mListener.onReceiveAd(this);
            }
        }
    }

    public void onFailedToReceiveAd(NendError nendError) {
        NendLog.d("onFailedToReceive!");
        if (!$assertionsDisabled && this.mController == null) {
            throw new AssertionError();
        } else if (!isDeallocate() && this.mController != null) {
            if (!this.mController.reloadAd()) {
                NendLog.d("Failed to reload.");
            }
            if (this.mListener != null) {
                this.mNendError = nendError;
                this.mListener.onFailedToReceiveAd(this);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (z) {
            this.mController.onWindowFocusChanged(true);
        }
    }

    public void onReceiveAd() {
        NendLog.d("onReceive!");
        if (!$assertionsDisabled && this.mAd == null) {
            throw new AssertionError();
        } else if (!isDeallocate()) {
            this.mNendError = null;
            if (getWidth() != 0 && getHeight() != 0 && !isShown()) {
                this.mController.onWindowFocusChanged(false);
            } else if (NendHelper.isConnected(getContext())) {
                switch ($SWITCH_TABLE$net$nend$android$AdParameter$ViewType()[this.mAd.getViewType().ordinal()]) {
                    case AdlantisAd.ADTYPE_TEXT:
                        this.mTask = new DownloadTask(this);
                        this.mTask.execute(new Void[0]);
                        return;
                    case 3:
                        if (this.mHasWindowFocus) {
                            setWebView();
                        }
                        if (this.mListener != null) {
                            this.mListener.onReceiveAd(this);
                            return;
                        }
                        return;
                    default:
                        if (!$assertionsDisabled) {
                            throw new AssertionError();
                        }
                        onFailedToReceiveAd(NendError.INVALID_RESPONSE_TYPE);
                        return;
                }
            } else {
                onFailedToReceiveAd(NendError.FAILED_AD_REQUEST);
            }
        }
    }

    public void onWindowFocusChanged(boolean z) {
        NendLog.d("onWindowFocusChanged!" + String.valueOf(z));
        super.onWindowFocusChanged(z);
        this.mHasWindowFocus = z;
        if (this.mController != null) {
            this.mController.onWindowFocusChanged(z);
            if (z && this.mIsClicked) {
                this.mIsClicked = false;
                if (this.mListener != null) {
                    this.mListener.onDismissScreen(this);
                }
            }
        }
    }

    public void pause() {
        NendLog.d("pause!");
        restartController();
        this.mController.setReloadable(false);
        if (this.mAd.getViewType() == AdParameter.ViewType.WEBVIEW) {
            deallocateWebView();
        }
    }

    public void removeListener() {
        this.mListener = null;
    }

    public void resume() {
        NendLog.d("resume!");
        restartController();
        this.mController.setReloadable(true);
        if (this.mAd.getViewType() == AdParameter.ViewType.WEBVIEW) {
            setWebView();
        }
    }

    public void setListener(NendAdListener nendAdListener) {
        this.mListener = nendAdListener;
    }
}
