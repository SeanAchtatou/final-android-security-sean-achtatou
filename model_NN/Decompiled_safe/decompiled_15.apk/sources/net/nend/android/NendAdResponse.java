package net.nend.android;

import android.text.TextUtils;
import jp.adlantis.android.AdlantisAd;
import net.nend.android.AdParameter;

final class NendAdResponse implements AdParameter {
    private static /* synthetic */ int[] $SWITCH_TABLE$net$nend$android$AdParameter$ViewType;
    private final String mClickUrl;
    private final int mHeight;
    private final String mIconId;
    private final String mImageUrl;
    private final int mReloadIntervalInSeconds;
    private final String mTitleText;
    private final AdParameter.ViewType mViewType;
    private final String mWebViewUrl;
    private final int mWidth;

    final class Builder {
        static final /* synthetic */ boolean $assertionsDisabled = (!NendAdResponse.class.desiredAssertionStatus());
        /* access modifiers changed from: private */
        public String mClickUrl;
        /* access modifiers changed from: private */
        public int mHeight;
        /* access modifiers changed from: private */
        public String mIconId;
        /* access modifiers changed from: private */
        public String mImageUrl;
        /* access modifiers changed from: private */
        public int mReloadIntervalInSeconds;
        /* access modifiers changed from: private */
        public String mTitleText;
        /* access modifiers changed from: private */
        public AdParameter.ViewType mViewType = AdParameter.ViewType.NONE;
        /* access modifiers changed from: private */
        public String mWebViewUrl;
        /* access modifiers changed from: private */
        public int mWidth;

        Builder() {
        }

        /* access modifiers changed from: package-private */
        public NendAdResponse build() {
            return new NendAdResponse(this, null);
        }

        /* access modifiers changed from: package-private */
        public Builder setClickUrl(String str) {
            if (str != null) {
                this.mClickUrl = str.replaceAll(" ", "%20");
            } else {
                this.mClickUrl = null;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setHeight(int i) {
            this.mHeight = i;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setIconId(String str) {
            this.mIconId = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setImageUrl(String str) {
            if (str != null) {
                this.mImageUrl = str.replaceAll(" ", "%20");
            } else {
                this.mImageUrl = null;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setReloadIntervalInSeconds(int i) {
            this.mReloadIntervalInSeconds = i;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setTitleText(String str) {
            this.mTitleText = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setViewType(AdParameter.ViewType viewType) {
            if ($assertionsDisabled || viewType != null) {
                this.mViewType = viewType;
                return this;
            }
            throw new AssertionError();
        }

        /* access modifiers changed from: package-private */
        public Builder setWebViewUrl(String str) {
            if (str != null) {
                this.mWebViewUrl = str.replaceAll(" ", "%20");
            } else {
                this.mWebViewUrl = null;
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public Builder setWidth(int i) {
            this.mWidth = i;
            return this;
        }
    }

    static /* synthetic */ int[] $SWITCH_TABLE$net$nend$android$AdParameter$ViewType() {
        int[] iArr = $SWITCH_TABLE$net$nend$android$AdParameter$ViewType;
        if (iArr == null) {
            iArr = new int[AdParameter.ViewType.values().length];
            try {
                iArr[AdParameter.ViewType.ADVIEW.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AdParameter.ViewType.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdParameter.ViewType.WEBVIEW.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$net$nend$android$AdParameter$ViewType = iArr;
        }
        return iArr;
    }

    private NendAdResponse(Builder builder) {
        switch ($SWITCH_TABLE$net$nend$android$AdParameter$ViewType()[builder.mViewType.ordinal()]) {
            case AdlantisAd.ADTYPE_TEXT:
                if (TextUtils.isEmpty(builder.mImageUrl)) {
                    throw new IllegalArgumentException("Image url is invalid.");
                } else if (TextUtils.isEmpty(builder.mClickUrl)) {
                    throw new IllegalArgumentException("Click url is invalid");
                } else {
                    this.mViewType = AdParameter.ViewType.ADVIEW;
                    this.mImageUrl = builder.mImageUrl;
                    this.mClickUrl = builder.mClickUrl;
                    this.mWebViewUrl = null;
                    this.mTitleText = builder.mTitleText;
                    this.mReloadIntervalInSeconds = builder.mReloadIntervalInSeconds;
                    this.mHeight = builder.mHeight;
                    this.mWidth = builder.mWidth;
                    this.mIconId = builder.mIconId;
                    return;
                }
            case 3:
                if (TextUtils.isEmpty(builder.mWebViewUrl)) {
                    throw new IllegalArgumentException("Web view url is invalid");
                }
                this.mViewType = AdParameter.ViewType.WEBVIEW;
                this.mImageUrl = null;
                this.mClickUrl = null;
                this.mWebViewUrl = builder.mWebViewUrl;
                this.mTitleText = null;
                this.mReloadIntervalInSeconds = 0;
                this.mHeight = builder.mHeight;
                this.mWidth = builder.mWidth;
                this.mIconId = null;
                return;
            default:
                throw new IllegalArgumentException("Uknown view type.");
        }
    }

    /* synthetic */ NendAdResponse(Builder builder, NendAdResponse nendAdResponse) {
        this(builder);
    }

    public String getClickUrl() {
        return this.mClickUrl;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public String getIconId() {
        return this.mIconId;
    }

    public String getImageUrl() {
        return this.mImageUrl;
    }

    public int getReloadIntervalInSeconds() {
        return this.mReloadIntervalInSeconds;
    }

    public String getTitleText() {
        return this.mTitleText;
    }

    public AdParameter.ViewType getViewType() {
        return this.mViewType;
    }

    public String getWebViewUrl() {
        return this.mWebViewUrl;
    }

    public int getWidth() {
        return this.mWidth;
    }
}
