package net.nend.android;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.view.View;
import android.widget.ImageView;
import java.io.IOException;
import net.nend.android.NendConstants;
import net.nend.android.NendHelper;

@SuppressLint({"ViewConstructor"})
final class OptOutImageView extends ImageView implements View.OnClickListener {
    static final /* synthetic */ boolean $assertionsDisabled = (!OptOutImageView.class.desiredAssertionStatus());
    private final float mDensity = getContext().getResources().getDisplayMetrics().density;
    private Bitmap mOptOutImage;
    private final String mOptOutUrl;

    final class TapMargin {
        private static final int BOTTOM = 18;
        private static final int LEFT = 18;

        private TapMargin() {
        }
    }

    OptOutImageView(Context context, String str, int i) {
        super(context);
        this.mOptOutUrl = String.valueOf(NendHelper.MetaDataHelper.getStringValue(context, NendConstants.MetaData.OPT_OUT_URL.getName(), "http://nend.net/privacy/optsdkgate")) + "?uid=" + str + "&spot=" + i;
        setPadding(18, 0, 0, 18);
        setOnClickListener(this);
        try {
            this.mOptOutImage = resizeBitmap(BitmapFactory.decodeStream(getResources().getAssets().open("icon.png")));
            setImageBitmap(this.mOptOutImage);
        } catch (IOException e) {
            this.mOptOutImage = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private Bitmap resizeBitmap(Bitmap bitmap) {
        if ($assertionsDisabled || bitmap != null) {
            Matrix matrix = new Matrix();
            matrix.setScale(this.mDensity / 2.0f, this.mDensity / 2.0f);
            Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            if (bitmap != createBitmap) {
                bitmap.recycle();
            }
            return createBitmap;
        }
        throw new AssertionError();
    }

    public void deallocateImage() {
        if (this.mOptOutImage != null) {
            if (!this.mOptOutImage.isRecycled()) {
                this.mOptOutImage.recycle();
            }
            this.mOptOutImage = null;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean hasDrawable() {
        return getDrawable() != null;
    }

    public void onClick(View view) {
        NendHelper.startBrowser(getContext(), this.mOptOutUrl);
    }
}
