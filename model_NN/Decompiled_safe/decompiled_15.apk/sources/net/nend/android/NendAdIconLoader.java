package net.nend.android;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.nend.android.DownloadTask;
import net.nend.android.NendAdIconView;
import net.nend.android.NendAdView;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;

public class NendAdIconLoader implements DownloadTask.Downloadable, NendAdIconView.AdListener {
    static final /* synthetic */ boolean $assertionsDisabled = (!NendAdIconLoader.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    private static final int MESSAGE_CODE = 719;
    private boolean isStarted = $assertionsDisabled;
    /* access modifiers changed from: private */
    public NendAdIconRequest mAdRequest;
    private OnClickListner mClickListner;
    private Context mContext;
    /* access modifiers changed from: private */
    public DownloadTask mDownloadTask;
    private OnFailedListner mFailedListner;
    private Handler mHandler;
    private boolean mHasWindowForcus;
    /* access modifiers changed from: private */
    public List mIconViewList;
    private ImpressionCountTask mImpressionTask;
    private OnReceiveListner mReceiveListner;
    private int mReloadIntervalInSeconds = 60;
    private boolean mReloadable = true;
    private int mSpotId;

    public interface OnClickListner {
        void onClick(NendAdIconView nendAdIconView);
    }

    public interface OnFailedListner {
        void onFailedToReceiveAd(NendIconError nendIconError);
    }

    public interface OnReceiveListner {
        void onReceiveAd(NendAdIconView nendAdIconView);
    }

    public NendAdIconLoader(Context context, int i, String str) {
        this.mContext = context;
        this.mSpotId = i;
        if (i <= 0) {
            throw new IllegalArgumentException(NendStatus.ERR_INVALID_SPOT_ID.getMsg("spot id : " + i));
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException(NendStatus.ERR_INVALID_API_KEY.getMsg("api key : " + str));
        } else {
            NendHelper.setDebuggable(context);
            this.mIconViewList = new ArrayList();
            this.mHandler = new Handler() {
                public void handleMessage(Message message) {
                    super.handleMessage(message);
                    if (NendAdIconLoader.this.mIconViewList.size() > 0) {
                        NendAdIconLoader.this.mAdRequest.setRequestCount(NendAdIconLoader.this.mIconViewList.size());
                        NendAdIconLoader.this.mDownloadTask = new DownloadTask(NendAdIconLoader.this);
                        NendAdIconLoader.this.mDownloadTask.execute(new Void[0]);
                        return;
                    }
                    NendLog.d("");
                }
            };
            this.mAdRequest = new NendAdIconRequest(context, i, str);
        }
    }

    private void startLoading() {
        if (this.mReloadable) {
            if (this.mHandler == null) {
                this.mHandler = new Handler();
            }
            this.mHandler.sendEmptyMessageDelayed(MESSAGE_CODE, (long) (this.mReloadIntervalInSeconds * 1000));
        }
    }

    private void stopLoading() {
        if (this.mDownloadTask != null && !this.mDownloadTask.isCancelled()) {
            this.mDownloadTask.cancel(true);
        }
        if (this.mHandler != null) {
            this.mHandler.removeMessages(MESSAGE_CODE);
        }
    }

    public void addIconView(NendAdIconView nendAdIconView) {
        if (nendAdIconView != null && this.mIconViewList.size() < 4 && !this.mIconViewList.contains(nendAdIconView)) {
            if (this.isStarted) {
                loadAd();
            }
            this.mReloadable = true;
            this.mIconViewList.add(nendAdIconView);
            nendAdIconView.setListner(this);
        }
    }

    public int getIconCount() {
        return this.mIconViewList.size();
    }

    public String getRequestUrl() {
        return this.mAdRequest.getRequestUrl(NendHelper.makeUid(this.mContext));
    }

    public void loadAd() {
        if (this.mHandler == null) {
            this.mHandler = new Handler();
        }
        this.mHandler.removeMessages(MESSAGE_CODE);
        this.mHandler.sendEmptyMessage(MESSAGE_CODE);
        this.isStarted = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: net.nend.android.NendLog.d(net.nend.android.NendStatus, java.lang.Throwable):int
     arg types: [net.nend.android.NendStatus, org.apache.http.ParseException]
     candidates:
      net.nend.android.NendLog.d(java.lang.String, java.lang.String):int
      net.nend.android.NendLog.d(java.lang.String, java.lang.Throwable):int
      net.nend.android.NendLog.d(net.nend.android.NendStatus, java.lang.String):int
      net.nend.android.NendLog.d(net.nend.android.NendStatus, java.lang.Throwable):int */
    public NendAdIconResponse makeResponse(HttpEntity httpEntity) {
        if (httpEntity != null) {
            try {
                return (NendAdIconResponse) new NendAdIconResponseParser(this.mContext, this.mIconViewList.size()).parseResponse(EntityUtils.toString(httpEntity));
            } catch (ParseException e) {
                if (!$assertionsDisabled) {
                    throw new AssertionError();
                }
                NendLog.d(NendStatus.ERR_HTTP_REQUEST, (Throwable) e);
            } catch (IOException e2) {
                if (!$assertionsDisabled) {
                    throw new AssertionError();
                }
                NendLog.d(NendStatus.ERR_HTTP_REQUEST, e2);
            }
        }
        return null;
    }

    public void onClick(View view) {
        if (this.mClickListner != null) {
            this.mClickListner.onClick((NendAdIconView) view);
        }
    }

    public void onDownload(NendAdIconResponse nendAdIconResponse) {
        String str;
        if (nendAdIconResponse != null) {
            this.mReloadIntervalInSeconds = NendHelper.setReloadIntervalInSeconds(nendAdIconResponse.getReloadIntervalInSeconds());
            String impressionCountUrl = nendAdIconResponse.getImpressionCountUrl();
            ArrayList adParameterList = nendAdIconResponse.getAdParameterList();
            String str2 = impressionCountUrl;
            for (int i = 0; i < this.mIconViewList.size(); i++) {
                if (adParameterList.size() > i) {
                    AdParameter adParameter = (AdParameter) adParameterList.get(i);
                    if (!TextUtils.isEmpty(str2)) {
                        str = String.valueOf(str2) + String.format("&ic[]=%s", adParameter.getIconId());
                    } else {
                        str = str2;
                    }
                    ((NendAdIconView) this.mIconViewList.get(i)).loadImage(adParameter, this.mSpotId);
                    str2 = str;
                }
            }
            this.mImpressionTask = new ImpressionCountTask();
            this.mImpressionTask.execute(str2);
        } else {
            NendLog.d("onFailedToImageDownload!");
            if (this.mFailedListner != null) {
                NendIconError nendIconError = new NendIconError();
                nendIconError.setLoader(this);
                nendIconError.setErrorType(0);
                nendIconError.setNendError(NendAdView.NendError.FAILED_AD_REQUEST);
                this.mFailedListner.onFailedToReceiveAd(nendIconError);
            }
        }
        if (this.mReloadable) {
            this.mHandler.sendEmptyMessageDelayed(MESSAGE_CODE, (long) (this.mReloadIntervalInSeconds * 1000));
        }
    }

    public void onFailedToReceive(NendIconError nendIconError) {
        if (this.mFailedListner != null) {
            this.mFailedListner.onFailedToReceiveAd(nendIconError);
        }
    }

    public void onReceive(View view) {
        if (this.mReceiveListner != null) {
            this.mReceiveListner.onReceiveAd((NendAdIconView) view);
        }
    }

    public void onWindowFocusChanged(boolean z) {
        if (z) {
            if (!this.mHasWindowForcus) {
                this.mHasWindowForcus = true;
                startLoading();
            }
        } else if (this.mHasWindowForcus) {
            this.mHasWindowForcus = $assertionsDisabled;
            stopLoading();
        }
    }

    public void pause() {
        this.mReloadable = $assertionsDisabled;
        stopLoading();
    }

    public void removeIconView(NendAdIconView nendAdIconView) {
        if (nendAdIconView != null) {
            nendAdIconView.deallocate();
            this.mIconViewList.remove(nendAdIconView);
            if (this.mIconViewList.size() == 0) {
                pause();
            }
        }
    }

    public void resume() {
        this.mReloadable = true;
        startLoading();
    }

    public void setOnClickListner(OnClickListner onClickListner) {
        this.mClickListner = onClickListner;
    }

    public void setOnFailedListner(OnFailedListner onFailedListner) {
        this.mFailedListner = onFailedListner;
    }

    public void setOnReceiveLisner(OnReceiveListner onReceiveListner) {
        this.mReceiveListner = onReceiveListner;
    }
}
