package net.nend.android;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import java.util.Locale;
import net.nend.android.NendConstants;
import net.nend.android.NendHelper;

abstract class AbsNendAdRequest {
    protected final String mApiKey;
    protected final String mDomain;
    protected final String mPath;
    protected final String mProtocol;
    protected final int mSpotId;

    AbsNendAdRequest(Context context, int i, String str) {
        if (context == null) {
            throw new NullPointerException("Context is null.");
        } else if (i <= 0) {
            throw new IllegalArgumentException("Spot id is invalid. spot id : " + i);
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Api key is invalid. api key : " + str);
        } else {
            this.mSpotId = i;
            this.mApiKey = str;
            this.mProtocol = NendHelper.MetaDataHelper.getStringValue(context, NendConstants.MetaData.ADSCHEME.getName(), "http");
            this.mDomain = NendHelper.MetaDataHelper.getStringValue(context, NendConstants.MetaData.ADAUTHORITY.getName(), getDomain());
            this.mPath = NendHelper.MetaDataHelper.getStringValue(context, NendConstants.MetaData.ADPATH.getName(), getPath());
        }
    }

    /* access modifiers changed from: package-private */
    public abstract String buildRequestUrl(String str);

    /* access modifiers changed from: protected */
    public String getDevice() {
        return Build.DEVICE;
    }

    /* access modifiers changed from: package-private */
    public abstract String getDomain();

    /* access modifiers changed from: protected */
    public String getLocale() {
        return Locale.getDefault().toString();
    }

    /* access modifiers changed from: protected */
    public String getModel() {
        return Build.MODEL;
    }

    /* access modifiers changed from: protected */
    public String getOS() {
        return "android";
    }

    /* access modifiers changed from: package-private */
    public abstract String getPath();

    /* access modifiers changed from: package-private */
    public String getRequestUrl(String str) {
        if (!TextUtils.isEmpty(str)) {
            return buildRequestUrl(str);
        }
        throw new IllegalArgumentException("UID is invalid. uid : " + str);
    }

    /* access modifiers changed from: protected */
    public String getSDKVersion() {
        return "2.3.0";
    }

    /* access modifiers changed from: protected */
    public String getVersion() {
        return Build.VERSION.RELEASE;
    }
}
