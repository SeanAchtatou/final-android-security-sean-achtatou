package net.nend.android;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

@SuppressLint({"ViewConstructor"})
final class NendAdImageView extends ImageView implements View.OnClickListener {
    private OnAdImageClickListener listener;
    private Bitmap mBitmap;
    private String mClickUrl = "";

    interface OnAdImageClickListener {
        void onAdImageClick(View view);
    }

    NendAdImageView(Context context) {
        super(context);
        setScaleType(ImageView.ScaleType.FIT_XY);
        setOnClickListener(this);
    }

    public void onClick(View view) {
        NendLog.v("click!! url: " + this.mClickUrl);
        if (this.listener != null) {
            this.listener.onAdImageClick(view);
        }
        NendHelper.startBrowser(getContext(), this.mClickUrl);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mBitmap != null) {
            this.mBitmap.recycle();
            this.mBitmap = null;
        }
        setImageDrawable(null);
    }

    /* access modifiers changed from: package-private */
    public void setAdInfo(Bitmap bitmap, String str) {
        this.mBitmap = bitmap;
        setImageBitmap(this.mBitmap);
        if (str != null) {
            this.mClickUrl = str;
        }
    }

    /* access modifiers changed from: package-private */
    public void setOnAdImageClickListener(OnAdImageClickListener onAdImageClickListener) {
        this.listener = onAdImageClickListener;
    }
}
