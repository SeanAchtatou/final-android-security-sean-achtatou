package net.nend.android;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

final class NendHelper {
    static final /* synthetic */ boolean $assertionsDisabled = (!NendHelper.class.desiredAssertionStatus());
    private static boolean mDebuggable = false;
    private static boolean mDev = false;

    public class MetaDataHelper {
        static final /* synthetic */ boolean $assertionsDisabled = (!NendHelper.class.desiredAssertionStatus());

        public static boolean getBooleanValue(Context context, String str, boolean z) {
            Bundle metaData = getMetaData(context);
            return metaData != null ? metaData.getBoolean(str, z) : z;
        }

        private static Bundle getMetaData(Context context) {
            try {
                return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            } catch (PackageManager.NameNotFoundException e) {
                if (!$assertionsDisabled) {
                    throw new AssertionError();
                }
                NendLog.d(NendStatus.ERR_UNEXPECTED, e);
                return null;
            }
        }

        public static String getStringValue(Context context, String str, String str2) {
            Bundle metaData = getMetaData(context);
            return (metaData == null || metaData.getString(str) == null) ? str2 : metaData.getString(str);
        }
    }

    private NendHelper() {
    }

    static void disableDebug() {
        mDebuggable = false;
        mDev = false;
    }

    private static boolean hasImplicitIntent(Context context, Intent intent) {
        return context.getPackageManager().queryIntentActivities(intent, 65536).size() > 0;
    }

    static boolean isConnected(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    static boolean isDebuggable() {
        return mDebuggable;
    }

    static boolean isDev() {
        return mDev;
    }

    static String makeUid(Context context) {
        if ($assertionsDisabled || context != null) {
            String md5String = md5String(UUID.randomUUID().toString());
            if (!new File(context.getFilesDir(), "NENDUUID").exists()) {
                try {
                    FileOutputStream openFileOutput = context.openFileOutput("NENDUUID", 0);
                    openFileOutput.write(md5String.getBytes());
                    openFileOutput.close();
                    return md5String;
                } catch (Exception e) {
                    return md5String;
                }
            } else {
                try {
                    return new BufferedReader(new InputStreamReader(context.openFileInput("NENDUUID"))).readLine();
                } catch (Exception e2) {
                    return md5String;
                }
            }
        } else {
            throw new AssertionError();
        }
    }

    static String md5String(String str) {
        byte[] bArr = new byte[16];
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            bArr = instance.digest();
        } catch (NoSuchAlgorithmException e) {
            NendLog.e("nend_SDK", e.getMessage(), e);
        }
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            sb.append(Integer.toString((b & 255) + 256, 16).substring(1));
        }
        return sb.toString();
    }

    static void setDebuggable(Context context) {
        mDebuggable = MetaDataHelper.getBooleanValue(context, "NendDebuggable", false);
        mDev = MetaDataHelper.getBooleanValue(context, "NendDev", false);
    }

    public static int setReloadIntervalInSeconds(int i) {
        if (i > 99999) {
            return 99999;
        }
        if (isDev() || i > 30) {
            return i;
        }
        return 30;
    }

    static void startBrowser(Context context, String str) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        if (hasImplicitIntent(context, intent)) {
            intent.setFlags(268435456);
            context.startActivity(intent);
        }
    }
}
