package net.nend.android;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.SparseArray;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

abstract class AbsNendAdResponseParser {
    static final /* synthetic */ boolean $assertionsDisabled = (!AbsNendAdResponseParser.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    protected static final String RESPONSE_ENCODING = "UTF-8";
    private final PackageManager mPackageManager;

    public final class JsonParam {
        protected static final String CLICK_URL = "click_url";
        protected static final String CONDITIONS = "conditions";
        protected static final String DEFAULT_AD = "default_ad";
        protected static final String DEFAULT_ADS = "default_ads";
        protected static final String HEIGHT = "height";
        protected static final String ICON_ID = "icon_id";
        protected static final String IMAGE_URL = "image_url";
        protected static final String IMPRESSHON_COUNT_URL = "impression_count_url";
        protected static final String LOGICAL_OPERATOR = "logical_operator";
        protected static final String MESSAGE = "message";
        protected static final String PACKAGE_NAME = "url_scheme";
        protected static final String RELOAD = "reload";
        protected static final String RESPONSE_TYPE = "response_type";
        protected static final String STATUS_CODE = "status_code";
        protected static final String TARGETING_ADS = "targeting_ads";
        protected static final String TITLE_TEXT = "icon_text";
        protected static final String WEB_VIEW_URL = "web_view_url";
        protected static final String WIDTH = "width";

        private JsonParam() {
        }
    }

    public enum ResponseType {
        UNSUPPORTED(0),
        BANNER_NORMAL(1),
        BANNER_WEB_VIEW(2),
        BANNER_APP_TARGETING(3),
        ICON_NORMAL(11),
        ICON_APP_TARGETING(13);
        
        private static final SparseArray intToEnum = new SparseArray();
        private int type;

        static {
            for (ResponseType responseType : values()) {
                intToEnum.put(responseType.type, responseType);
            }
        }

        private ResponseType(int i) {
            this.type = i;
        }

        protected static ResponseType valueOf(int i) {
            return (ResponseType) intToEnum.get(i, UNSUPPORTED);
        }
    }

    public AbsNendAdResponseParser(Context context) {
        if (context == null) {
            throw new NullPointerException(NendStatus.ERR_INVALID_CONTEXT.getMsg());
        }
        this.mPackageManager = context.getPackageManager();
    }

    /* access modifiers changed from: package-private */
    public abstract Object getResponseObject(ResponseType responseType, JSONObject jSONObject);

    /* access modifiers changed from: protected */
    public boolean isTarget(JSONArray jSONArray) {
        if ($assertionsDisabled || jSONArray != null) {
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                int i2 = jSONObject.getInt("logical_operator");
                if (i2 == 1) {
                    try {
                        this.mPackageManager.getPackageInfo(jSONObject.getString("url_scheme"), 1);
                    } catch (PackageManager.NameNotFoundException e) {
                        return $assertionsDisabled;
                    }
                } else if (i2 != 2) {
                    return $assertionsDisabled;
                } else {
                    try {
                        this.mPackageManager.getPackageInfo(jSONObject.getString("url_scheme"), 1);
                        return $assertionsDisabled;
                    } catch (PackageManager.NameNotFoundException e2) {
                    }
                }
            }
            return true;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public Object parseResponse(String str) {
        if (str != null) {
            try {
                if (str.length() != 0) {
                    JSONObject jSONObject = new JSONObject(URLDecoder.decode(str, RESPONSE_ENCODING));
                    if (jSONObject.getInt("status_code") == NendStatus.SUCCESS.getCode()) {
                        return getResponseObject(ResponseType.valueOf(jSONObject.getInt("response_type")), jSONObject);
                    }
                    throw new NendException(NendStatus.ERR_INVALID_AD_STATUS, "Ad status : " + jSONObject.getInt("status_code") + ", Message : " + jSONObject.getString("message"));
                }
            } catch (UnsupportedEncodingException e) {
                if (!$assertionsDisabled) {
                    throw new AssertionError();
                }
            } catch (JSONException e2) {
                NendLog.w(NendStatus.ERR_FAILED_TO_PARSE, e2);
            } catch (NendException e3) {
                NendLog.w(NendStatus.ERR_FAILED_TO_PARSE, e3);
            } catch (IllegalArgumentException e4) {
                NendLog.w(NendStatus.ERR_FAILED_TO_PARSE, e4);
            }
        }
        throw new IllegalArgumentException(NendStatus.ERR_INVALID_RESPONSE.getMsg());
        return null;
    }
}
