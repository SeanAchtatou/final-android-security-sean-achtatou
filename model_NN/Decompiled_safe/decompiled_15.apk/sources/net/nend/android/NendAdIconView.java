package net.nend.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import java.io.IOException;
import net.nend.android.DownloadTask;
import net.nend.android.NendAdView;
import net.nend.android.NendConstants;
import org.apache.http.HttpEntity;

public class NendAdIconView extends View implements DownloadTask.Downloadable {
    static final /* synthetic */ boolean $assertionsDisabled = (!NendAdIconView.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    private static final int DEFAULT_FONT_SIZE = 10;
    private static final int ICON_CONTAINER_SIZE = 75;
    private static final int ICON_SIZE = 57;
    private static final int MINIMUM_FONT_SIZE = 6;
    private static final String NAME_SPACE = "http://schemas.android.com/apk/res/android";
    private static final int OPTOUT_MARGIN = 3;
    private static final int OPTOUT_SIZE = 12;
    private static final int WP = -2;
    private boolean isDefaultSize = $assertionsDisabled;
    private AdParameter mAdParameter;
    private float mDensity;
    private String mDisplayTitle = "";
    private DownloadTask mDownloadTask;
    private BitmapDrawable mIconBitmap = null;
    private float mIconMargin;
    private float mIconSize;
    private AdListener mListener;
    private BitmapDrawable mOptoutBitmap = null;
    private float mOptoutMargin;
    private Rect mRect;
    private int mSpotId;
    private Paint mTextPaint;
    private int mTextPosX;
    private int mTextPosY;
    private int mTitleColor = -16777216;
    private boolean mTitleVisible = true;

    interface AdListener {
        void onClick(View view);

        void onFailedToReceive(NendIconError nendIconError);

        void onReceive(View view);

        void onWindowFocusChanged(boolean z);
    }

    public NendAdIconView(Context context) {
        super(context);
        init(context);
    }

    public NendAdIconView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (Integer.toString(-2).equals(attributeSet.getAttributeValue(NAME_SPACE, "layout_width")) && Integer.toString(-2).equals(attributeSet.getAttributeValue(NAME_SPACE, "layout_height"))) {
            this.isDefaultSize = true;
        }
        String attributeValue = attributeSet.getAttributeValue(null, NendConstants.IconAttribute.TITLE_COLOR.getName());
        if (attributeValue != null) {
            try {
                this.mTitleColor = Color.parseColor(attributeValue);
            } catch (Exception e) {
                this.mTitleColor = -16777216;
            }
        }
        this.mTitleVisible = attributeSet.getAttributeBooleanValue(null, NendConstants.IconAttribute.TITLE_VISIBLE.getName(), true);
        init(context);
    }

    private void adjustTitleText() {
        this.mDisplayTitle = this.mAdParameter.getTitleText();
        float displayableTextSize = getDisplayableTextSize(this.mDisplayTitle, this.mTextPaint);
        if (0.0f != displayableTextSize) {
            this.mTextPaint.setTextSize(displayableTextSize);
        } else {
            this.mTextPaint.setTextSize(6.0f * this.mDensity);
            this.mDisplayTitle = TextUtils.ellipsize(this.mDisplayTitle, new TextPaint(this.mTextPaint), (float) getWidth(), TextUtils.TruncateAt.END).toString();
        }
        Paint.FontMetrics fontMetrics = this.mTextPaint.getFontMetrics();
        this.mTextPosY = (int) ((this.mIconSize + this.mIconMargin) - ((fontMetrics.ascent + fontMetrics.descent) / 2.0f));
    }

    private float getDisplayableTextSize(String str, Paint paint) {
        Paint paint2 = new Paint(paint);
        paint2.setTextSize(10.0f * this.mDensity);
        while (true) {
            Paint.FontMetrics fontMetrics = paint2.getFontMetrics();
            if (((float) getWidth()) > paint2.measureText(str)) {
                if (this.mIconMargin > Math.abs(fontMetrics.descent + fontMetrics.ascent)) {
                    return paint2.getTextSize();
                }
            }
            float textSize = paint2.getTextSize() - 1.0f;
            if (6.0f * this.mDensity > textSize) {
                return 0.0f;
            }
            paint2.setTextSize(textSize);
        }
    }

    private void init(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        this.mDensity = displayMetrics.density;
        this.mTextPaint = new Paint();
        this.mTextPaint.setColor(this.mTitleColor);
        this.mTextPaint.setAntiAlias(true);
        this.mTextPaint.setTextAlign(Paint.Align.CENTER);
        this.mOptoutMargin = 3.0f * this.mDensity;
        this.mRect = new Rect();
        try {
            this.mOptoutBitmap = new BitmapDrawable(getResources(), BitmapFactory.decodeStream(getResources().getAssets().open("icon.png")));
        } catch (IOException e) {
        }
    }

    public void deallocate() {
        if (this.mDownloadTask != null && !this.mDownloadTask.isCancelled()) {
            this.mDownloadTask.cancel(true);
        }
        setOnClickListener(null);
        this.mListener = null;
        this.mIconBitmap = null;
    }

    public String getRequestUrl() {
        return this.mAdParameter.getImageUrl();
    }

    /* access modifiers changed from: package-private */
    public void loadImage(AdParameter adParameter, int i) {
        if (adParameter == null) {
            NendIconError nendIconError = new NendIconError();
            nendIconError.setErrorType(1);
            nendIconError.setIconView(this);
            nendIconError.setNendError(NendAdView.NendError.FAILED_AD_DOWNLOAD);
            this.mListener.onFailedToReceive(nendIconError);
            return;
        }
        this.mAdParameter = adParameter;
        this.mSpotId = i;
        this.mDisplayTitle = "";
        this.mDownloadTask = new DownloadTask(this);
        this.mDownloadTask.execute(new Void[0]);
    }

    public Bitmap makeResponse(HttpEntity httpEntity) {
        if (httpEntity != null && !this.mDownloadTask.isCancelled()) {
            try {
                return BitmapFactory.decodeStream(httpEntity.getContent());
            } catch (IllegalStateException e) {
                if (!$assertionsDisabled) {
                    throw new AssertionError();
                }
                NendLog.d(NendStatus.ERR_HTTP_REQUEST, e);
            } catch (IOException e2) {
                if (!$assertionsDisabled) {
                    throw new AssertionError();
                }
                NendLog.d(NendStatus.ERR_HTTP_REQUEST, e2);
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        deallocate();
    }

    public void onDownload(Bitmap bitmap) {
        if (!this.mDownloadTask.isCancelled()) {
            if (bitmap != null) {
                NendLog.d("onImageDownload!");
                this.mIconBitmap = new BitmapDrawable(getResources(), bitmap);
                postInvalidate();
                if (this.mListener != null) {
                    this.mListener.onReceive(this);
                    return;
                }
                return;
            }
            NendLog.d("onFailedToImageDownload!");
            if (this.mListener != null) {
                NendIconError nendIconError = new NendIconError();
                nendIconError.setIconView(this);
                nendIconError.setErrorType(1);
                nendIconError.setNendError(NendAdView.NendError.FAILED_AD_DOWNLOAD);
                this.mListener.onFailedToReceive(nendIconError);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mIconBitmap != null) {
            this.mIconBitmap.setBounds((int) this.mIconMargin, 0, (int) (this.mIconMargin + this.mIconSize), (int) this.mIconSize);
            this.mIconBitmap.draw(canvas);
            if (this.mTitleVisible && this.mAdParameter.getTitleText() != null) {
                adjustTitleText();
                canvas.drawText(this.mDisplayTitle, (float) this.mTextPosX, (float) this.mTextPosY, this.mTextPaint);
            }
            if (this.mOptoutBitmap != null) {
                this.mOptoutBitmap.setBounds(this.mRect);
                this.mOptoutBitmap.draw(canvas);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (z) {
            int i5 = i3 - i;
            this.mIconSize = (((float) i5) / (75.0f * this.mDensity)) * 57.0f * this.mDensity;
            this.mIconMargin = (((float) i5) - this.mIconSize) / 2.0f;
            this.mTextPosX = i5 / 2;
            int i6 = (int) (12.0f * this.mDensity);
            int i7 = (int) (this.mIconSize + this.mIconMargin);
            this.mRect.top = 0;
            this.mRect.left = i7 - i6;
            this.mRect.right = i7;
            this.mRect.bottom = i6;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams != null && layoutParams.width == -2 && layoutParams.height == -2) {
            this.isDefaultSize = true;
        }
        if (this.isDefaultSize) {
            setMeasuredDimension((int) (this.mDensity * 75.0f), (int) (this.mDensity * 75.0f));
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        switch (motionEvent.getAction()) {
            case 1:
                if (this.mIconBitmap != null) {
                    if (this.mOptoutBitmap != null && x > ((float) this.mRect.left) - this.mOptoutMargin && x < ((float) this.mRect.right) && y > ((float) this.mRect.top) && y < ((float) this.mRect.bottom) + this.mOptoutMargin) {
                        NendHelper.startBrowser(getContext(), "http://nend.net/privacy/optsdkgate?uid=" + NendHelper.makeUid(getContext()) + "&spot=" + this.mSpotId);
                        return $assertionsDisabled;
                    } else if (x > this.mIconMargin && x < this.mIconSize && y > 0.0f && y < this.mIconSize) {
                        NendLog.v("click!! url: " + this.mAdParameter.getClickUrl());
                        if (this.mListener != null) {
                            this.mListener.onClick(this);
                        }
                        NendHelper.startBrowser(getContext(), this.mAdParameter.getClickUrl());
                        return $assertionsDisabled;
                    }
                }
                break;
        }
        return true;
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (this.mListener != null) {
            this.mListener.onWindowFocusChanged(z);
        }
    }

    /* access modifiers changed from: package-private */
    public void setListner(AdListener adListener) {
        this.mListener = adListener;
    }

    public void setTitleColor(int i) {
        this.mTitleColor = i;
        this.mTextPaint.setColor(this.mTitleColor);
        invalidate();
    }

    public void setTitleVisible(boolean z) {
        this.mTitleVisible = z;
        invalidate();
    }
}
