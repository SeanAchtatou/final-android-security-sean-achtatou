package org.anddev.andengine.opengl.texture.compressed.pvr;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.opengl.texture.ITexture;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.util.ArrayUtils;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.MathUtils;
import org.anddev.andengine.util.StreamUtils;

public abstract class PVRTexture extends Texture {
    public static final int FLAG_ALPHA = 32768;
    public static final int FLAG_BUMPMAP = 1024;
    public static final int FLAG_CUBEMAP = 4096;
    public static final int FLAG_FALSEMIPCOL = 8192;
    public static final int FLAG_MIPMAP = 256;
    public static final int FLAG_TILING = 2048;
    public static final int FLAG_TWIDDLE = 512;
    public static final int FLAG_VERTICALFLIP = 65536;
    public static final int FLAG_VOLUME = 16384;
    private final PVRTextureHeader mPVRTextureHeader;

    /* access modifiers changed from: protected */
    public abstract InputStream onGetInputStream() throws IOException;

    public PVRTexture(PVRTextureFormat pPVRTextureFormat) throws IllegalArgumentException, IOException {
        this(pPVRTextureFormat, TextureOptions.DEFAULT, null);
    }

    public PVRTexture(PVRTextureFormat pPVRTextureFormat, ITexture.ITextureStateListener pTextureStateListener) throws IllegalArgumentException, IOException {
        this(pPVRTextureFormat, TextureOptions.DEFAULT, pTextureStateListener);
    }

    public PVRTexture(PVRTextureFormat pPVRTextureFormat, TextureOptions pTextureOptions) throws IllegalArgumentException, IOException {
        this(pPVRTextureFormat, pTextureOptions, null);
    }

    /* JADX INFO: finally extract failed */
    public PVRTexture(PVRTextureFormat pPVRTextureFormat, TextureOptions pTextureOptions, ITexture.ITextureStateListener pTextureStateListener) throws IllegalArgumentException, IOException {
        super(pPVRTextureFormat.getPixelFormat(), pTextureOptions, pTextureStateListener);
        InputStream inputStream = null;
        try {
            inputStream = onGetInputStream();
            this.mPVRTextureHeader = new PVRTextureHeader(StreamUtils.streamToBytes(inputStream, 52));
            StreamUtils.close(inputStream);
            if (!MathUtils.isPowerOfTwo(getWidth()) || !MathUtils.isPowerOfTwo(getHeight())) {
                throw new IllegalArgumentException("mWidth and mHeight must be a power of 2!");
            } else if (this.mPVRTextureHeader.getPVRTextureFormat().getPixelFormat() != pPVRTextureFormat.getPixelFormat()) {
                throw new IllegalArgumentException("Other PVRTextureFormat: '" + this.mPVRTextureHeader.getPVRTextureFormat().getPixelFormat() + "' found than expected: '" + pPVRTextureFormat.getPixelFormat() + "'.");
            } else if (this.mPVRTextureHeader.getPVRTextureFormat().isCompressed()) {
                throw new IllegalArgumentException("Invalid PVRTextureFormat: '" + this.mPVRTextureHeader.getPVRTextureFormat() + "'.");
            } else {
                this.mUpdateOnHardwareNeeded = true;
            }
        } catch (Throwable th) {
            StreamUtils.close(inputStream);
            throw th;
        }
    }

    public int getWidth() {
        return this.mPVRTextureHeader.getWidth();
    }

    public int getHeight() {
        return this.mPVRTextureHeader.getHeight();
    }

    public PVRTextureHeader getPVRTextureHeader() {
        return this.mPVRTextureHeader;
    }

    /* access modifiers changed from: protected */
    public void generateHardwareTextureID(GL10 pGL) {
        pGL.glPixelStorei(3317, 1);
        super.generateHardwareTextureID(pGL);
    }

    /* access modifiers changed from: protected */
    public void writeTextureToHardware(GL10 pGL) throws IOException {
        InputStream inputStream = onGetInputStream();
        try {
            byte[] data = StreamUtils.streamToBytes(inputStream);
            ByteBuffer dataByteBuffer = ByteBuffer.wrap(data);
            dataByteBuffer.rewind();
            dataByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
            int width = getWidth();
            int height = getHeight();
            int dataLength = this.mPVRTextureHeader.getDataLength();
            int glFormat = this.mPixelFormat.getGLFormat();
            int glType = this.mPixelFormat.getGLType();
            int bytesPerPixel = this.mPVRTextureHeader.getBitsPerPixel() / 8;
            int mipmapLevel = 0;
            int currentPixelDataOffset = 0;
            while (currentPixelDataOffset < dataLength) {
                int currentPixelDataSize = width * height * bytesPerPixel;
                ByteBuffer pixelData = ByteBuffer.allocate(currentPixelDataSize).order(ByteOrder.nativeOrder());
                pixelData.put(data, currentPixelDataOffset + 52, currentPixelDataSize);
                if (mipmapLevel > 0 && !(width == height && MathUtils.nextPowerOfTwo(width) == width)) {
                    Debug.w(String.format("Mipmap level '%u' is not squared. Width: '%u', height: '%u'. Texture won't render correctly.", Integer.valueOf(mipmapLevel), Integer.valueOf(width), Integer.valueOf(height)));
                }
                pGL.glTexImage2D(3553, mipmapLevel, glFormat, width, height, 0, glFormat, glType, pixelData);
                GLHelper.checkGLError(pGL);
                currentPixelDataOffset += currentPixelDataSize;
                width = Math.max(width >> 1, 1);
                height = Math.max(height >> 1, 1);
                mipmapLevel++;
            }
        } finally {
            StreamUtils.close(inputStream);
        }
    }

    public static class PVRTextureHeader {
        private static final int FORMAT_FLAG_MASK = 255;
        public static final byte[] MAGIC_IDENTIFIER = {80, 86, 82, 33};
        public static final int SIZE = 52;
        private final ByteBuffer mDataByteBuffer;
        private final PVRTextureFormat mPVRTextureFormat;

        public PVRTextureHeader(byte[] pData) {
            this.mDataByteBuffer = ByteBuffer.wrap(pData);
            this.mDataByteBuffer.rewind();
            this.mDataByteBuffer.order(ByteOrder.LITTLE_ENDIAN);
            if (!ArrayUtils.equals(pData, 44, MAGIC_IDENTIFIER, 0, MAGIC_IDENTIFIER.length)) {
                throw new IllegalArgumentException("Invalid " + getClass().getSimpleName() + "!");
            }
            this.mPVRTextureFormat = PVRTextureFormat.fromID(getFlags() & FORMAT_FLAG_MASK);
        }

        public PVRTextureFormat getPVRTextureFormat() {
            return this.mPVRTextureFormat;
        }

        public int headerLength() {
            return this.mDataByteBuffer.getInt(0);
        }

        public int getHeight() {
            return this.mDataByteBuffer.getInt(4);
        }

        public int getWidth() {
            return this.mDataByteBuffer.getInt(8);
        }

        public int getNumMipmaps() {
            return this.mDataByteBuffer.getInt(12);
        }

        public int getFlags() {
            return this.mDataByteBuffer.getInt(16);
        }

        public int getDataLength() {
            return this.mDataByteBuffer.getInt(20);
        }

        public int getBitsPerPixel() {
            return this.mDataByteBuffer.getInt(24);
        }

        public int getBitmaskRed() {
            return this.mDataByteBuffer.getInt(28);
        }

        public int getBitmaskGreen() {
            return this.mDataByteBuffer.getInt(32);
        }

        public int getBitmaskBlue() {
            return this.mDataByteBuffer.getInt(36);
        }

        public int getBitmaskAlpha() {
            return this.mDataByteBuffer.getInt(40);
        }

        public boolean hasAlpha() {
            return getBitmaskAlpha() != 0;
        }

        public int getPVRTag() {
            return this.mDataByteBuffer.getInt(44);
        }

        public int numSurfs() {
            return this.mDataByteBuffer.getInt(48);
        }
    }

    public enum PVRTextureFormat {
        RGBA_4444(16, false, Texture.PixelFormat.RGBA_4444),
        RGBA_5551(17, false, Texture.PixelFormat.RGBA_5551),
        RGBA_8888(18, false, Texture.PixelFormat.RGBA_8888),
        RGB_565(19, false, Texture.PixelFormat.RGB_565),
        I_8(22, false, Texture.PixelFormat.I_8),
        AI_88(23, false, Texture.PixelFormat.AI_8),
        A_8(27, false, Texture.PixelFormat.A_8);
        
        private final boolean mCompressed;
        private final int mID;
        private final Texture.PixelFormat mPixelFormat;

        private PVRTextureFormat(int pID, boolean pCompressed, Texture.PixelFormat pPixelFormat) {
            this.mID = pID;
            this.mCompressed = pCompressed;
            this.mPixelFormat = pPixelFormat;
        }

        public static PVRTextureFormat fromID(int pID) {
            for (PVRTextureFormat pvrTextureFormat : values()) {
                if (pvrTextureFormat.mID == pID) {
                    return pvrTextureFormat;
                }
            }
            throw new IllegalArgumentException("Unexpected " + PVRTextureFormat.class.getSimpleName() + "-ID: '" + pID + "'.");
        }

        public int getID() {
            return this.mID;
        }

        public boolean isCompressed() {
            return this.mCompressed;
        }

        public Texture.PixelFormat getPixelFormat() {
            return this.mPixelFormat;
        }
    }
}
