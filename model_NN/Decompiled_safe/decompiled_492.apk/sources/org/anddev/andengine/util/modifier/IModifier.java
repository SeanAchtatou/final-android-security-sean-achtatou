package org.anddev.andengine.util.modifier;

import java.util.Comparator;

public interface IModifier<T> extends Cloneable {
    public static final Comparator<IModifier<?>> MODIFIER_COMPARATOR_DURATION_DESCENDING = new Comparator<IModifier<?>>() {
        public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            return compare((IModifier<?>) ((IModifier) obj), (IModifier<?>) ((IModifier) obj2));
        }

        public int compare(IModifier<?> pModifierA, IModifier<?> pModifierB) {
            return (int) Math.signum(pModifierA.getDuration() - pModifierB.getDuration());
        }
    };

    public static class CloneNotSupportedException extends RuntimeException {
        private static final long serialVersionUID = -5838035434002587320L;
    }

    public interface IModifierListener<T> {
        void onModifierFinished(IModifier iModifier, Object obj);

        void onModifierStarted(IModifier iModifier, Object obj);
    }

    void addModifierListener(IModifierListener<T> iModifierListener);

    IModifier<T> clone() throws CloneNotSupportedException;

    float getDuration();

    float getSecondsElapsed();

    boolean isFinished();

    boolean isRemoveWhenFinished();

    float onUpdate(float f, Object obj);

    boolean removeModifierListener(IModifierListener<T> iModifierListener);

    void reset();

    void setRemoveWhenFinished(boolean z);
}
