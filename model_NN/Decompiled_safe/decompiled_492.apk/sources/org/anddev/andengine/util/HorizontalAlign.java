package org.anddev.andengine.util;

public enum HorizontalAlign {
    LEFT,
    CENTER,
    RIGHT
}
