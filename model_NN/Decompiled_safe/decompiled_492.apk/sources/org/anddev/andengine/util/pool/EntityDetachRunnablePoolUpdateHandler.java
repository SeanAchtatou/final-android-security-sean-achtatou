package org.anddev.andengine.util.pool;

public class EntityDetachRunnablePoolUpdateHandler extends RunnablePoolUpdateHandler<EntityDetachRunnablePoolItem> {
    /* access modifiers changed from: protected */
    public EntityDetachRunnablePoolItem onAllocatePoolItem() {
        return new EntityDetachRunnablePoolItem();
    }
}
