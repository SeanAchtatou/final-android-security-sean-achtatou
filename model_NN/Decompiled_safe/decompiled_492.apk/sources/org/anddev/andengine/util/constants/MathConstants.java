package org.anddev.andengine.util.constants;

public interface MathConstants {
    public static final float DEG_TO_RAD = 0.017453292f;
    public static final float PI = 3.1415927f;
    public static final float PI_HALF = 1.5707964f;
    public static final float PI_TWICE = 6.2831855f;
    public static final float RAD_TO_DEG = 57.295776f;
}
