package org.anddev.andengine.util.pool;

import java.util.ArrayList;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.util.pool.PoolItem;

public abstract class PoolUpdateHandler<T extends PoolItem> implements IUpdateHandler {
    private final Pool<T> mPool;
    private final ArrayList<T> mScheduledPoolItems;

    /* access modifiers changed from: protected */
    public abstract T onAllocatePoolItem();

    /* access modifiers changed from: protected */
    public abstract void onHandlePoolItem(PoolItem poolItem);

    public PoolUpdateHandler() {
        this.mScheduledPoolItems = new ArrayList<>();
        this.mPool = new Pool<T>() {
            /* access modifiers changed from: protected */
            public T onAllocatePoolItem() {
                return PoolUpdateHandler.this.onAllocatePoolItem();
            }
        };
    }

    public PoolUpdateHandler(int pInitialPoolSize) {
        this.mScheduledPoolItems = new ArrayList<>();
        this.mPool = new Pool<T>(pInitialPoolSize) {
            /* access modifiers changed from: protected */
            public T onAllocatePoolItem() {
                return PoolUpdateHandler.this.onAllocatePoolItem();
            }
        };
    }

    public PoolUpdateHandler(int pInitialPoolSize, int pGrowth) {
        this.mScheduledPoolItems = new ArrayList<>();
        this.mPool = new Pool<T>(pInitialPoolSize, pGrowth) {
            /* access modifiers changed from: protected */
            public T onAllocatePoolItem() {
                return PoolUpdateHandler.this.onAllocatePoolItem();
            }
        };
    }

    public void onUpdate(float pSecondsElapsed) {
        ArrayList<T> scheduledPoolItems = this.mScheduledPoolItems;
        synchronized (scheduledPoolItems) {
            int count = scheduledPoolItems.size();
            if (count > 0) {
                Pool<T> pool = this.mPool;
                for (int i = 0; i < count; i++) {
                    T item = (PoolItem) scheduledPoolItems.get(i);
                    onHandlePoolItem(item);
                    pool.recyclePoolItem((PoolItem) item);
                }
                scheduledPoolItems.clear();
            }
        }
    }

    public void reset() {
        ArrayList<T> scheduledPoolItems = this.mScheduledPoolItems;
        synchronized (scheduledPoolItems) {
            int count = scheduledPoolItems.size();
            Pool<T> pool = this.mPool;
            for (int i = count - 1; i >= 0; i--) {
                pool.recyclePoolItem((PoolItem) scheduledPoolItems.get(i));
            }
            scheduledPoolItems.clear();
        }
    }

    public T obtainPoolItem() {
        return (PoolItem) this.mPool.obtainPoolItem();
    }

    public void postPoolItem(T pPoolItem) {
        synchronized (this.mScheduledPoolItems) {
            if (pPoolItem == null) {
                throw new IllegalArgumentException("PoolItem already recycled!");
            } else if (!this.mPool.ownsPoolItem(pPoolItem)) {
                throw new IllegalArgumentException("PoolItem from another pool!");
            } else {
                this.mScheduledPoolItems.add(pPoolItem);
            }
        }
    }
}
