package org.anddev.andengine.entity.scene.background.modifier;

import org.anddev.andengine.entity.scene.background.IBackground;
import org.anddev.andengine.util.modifier.IModifier;

public interface IBackgroundModifier extends IModifier<IBackground> {

    public interface IBackgroundModifierListener extends IModifier.IModifierListener<IBackground> {
    }

    IBackgroundModifier clone() throws IModifier.CloneNotSupportedException;
}
