package com.vpon.adon.android;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.vpon.adon.android.utils.StringUtils;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;

public class WebInApp extends Activity {
    /* access modifiers changed from: private */
    public WebInAppWebChromeClient adOnWebChromeClient;
    private Button button;
    private View customView;
    private RelativeLayout mainView;
    private WebView webView;

    enum WebState {
        CUSTOMVIEW,
        WEBVIEW
    }

    public void onCreate(Bundle savedInstanceState) {
        RelativeLayout.LayoutParams buttonLayoutParams;
        String buttonImagePath;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(PVRTexture.FLAG_BUMPMAP);
        this.mainView = new RelativeLayout(this);
        setContentView(this.mainView);
        Bundle bundle = getIntent().getExtras();
        String url = bundle.getString("url");
        if (StringUtils.isBlank(url)) {
            finish();
        }
        ProgressBar progressBar = new ProgressBar(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        progressBar.setLayoutParams(layoutParams);
        this.mainView.addView(progressBar);
        this.webView = new WebView(this);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setSupportZoom(true);
        this.webView.getSettings().setUseWideViewPort(true);
        this.webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        this.webView.loadUrl(url);
        this.mainView.addView(this.webView, new RelativeLayout.LayoutParams(-1, -1));
        if (bundle.getInt("adWidth") == 480) {
            buttonLayoutParams = new RelativeLayout.LayoutParams(75, 75);
            buttonImagePath = "/close75.png";
        } else {
            buttonLayoutParams = new RelativeLayout.LayoutParams(50, 50);
            buttonImagePath = "/close50.png";
        }
        Drawable buttonImageDrawable = new BitmapDrawable(getClass().getResourceAsStream(buttonImagePath));
        this.button = new Button(this);
        this.button.setBackgroundDrawable(buttonImageDrawable);
        this.button.setLayoutParams(buttonLayoutParams);
        this.button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (WebInApp.this.adOnWebChromeClient != null) {
                    WebInApp.this.adOnWebChromeClient.onCustomViewHidden();
                }
                WebInApp.this.finish();
            }
        });
        this.mainView.addView(this.button);
        this.adOnWebChromeClient = new WebInAppWebChromeClient(this.customView, this.mainView, this.webView, this.button);
        this.adOnWebChromeClient.setWebState(WebState.WEBVIEW);
        this.webView.setWebChromeClient(this.adOnWebChromeClient);
        this.webView.setWebViewClient(new WebInAppWebClient(this));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case 4:
                if (this.adOnWebChromeClient != null) {
                    this.adOnWebChromeClient.onCustomViewHidden();
                }
                if (!this.adOnWebChromeClient.isCustomViewState()) {
                    finish();
                    break;
                } else {
                    this.adOnWebChromeClient.setWebState(WebState.WEBVIEW);
                    this.mainView.removeView(this.customView);
                    this.webView.setVisibility(0);
                    this.button.setVisibility(0);
                    break;
                }
        }
        return false;
    }
}
