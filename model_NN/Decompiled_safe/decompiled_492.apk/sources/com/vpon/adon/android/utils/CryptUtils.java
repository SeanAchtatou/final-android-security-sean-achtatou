package com.vpon.adon.android.utils;

import com.vpon.adon.android.entity.RespClickList;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.LinkedList;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;

public class CryptUtils {
    public static String algorithm = "RSA/NONE/NoPadding";
    public static String provider = "BC";

    public static SealedObject clickListEnc(String key, RespClickList respClickList) {
        try {
            byte[] cipherKey = new byte[24];
            System.arraycopy(key.getBytes("UTF-8"), 0, cipherKey, 0, 24);
            SecretKey secretKey = new SecretKeySpec(cipherKey, "DESede");
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(1, secretKey);
            return new SealedObject(respClickList, cipher);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
            return null;
        } catch (NoSuchPaddingException e3) {
            e3.printStackTrace();
            return null;
        } catch (InvalidKeyException e4) {
            e4.printStackTrace();
            return null;
        } catch (IllegalBlockSizeException e5) {
            e5.printStackTrace();
            return null;
        } catch (IOException e6) {
            e6.printStackTrace();
            return null;
        } catch (Exception e7) {
            e7.printStackTrace();
            return null;
        }
    }

    public static RespClickList clickListDnc(String key, SealedObject sealedObject) {
        try {
            byte[] cipherKey = new byte[24];
            System.arraycopy(key.getBytes("UTF-8"), 0, cipherKey, 0, 24);
            SecretKey secretKey = new SecretKeySpec(cipherKey, "DESede");
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(2, secretKey);
            return (RespClickList) sealedObject.getObject(cipher);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return new RespClickList(new LinkedList());
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
            return new RespClickList(new LinkedList());
        } catch (NoSuchPaddingException e3) {
            e3.printStackTrace();
            return new RespClickList(new LinkedList());
        } catch (InvalidKeyException e4) {
            e4.printStackTrace();
            return new RespClickList(new LinkedList());
        } catch (IllegalBlockSizeException e5) {
            e5.printStackTrace();
            return new RespClickList(new LinkedList());
        } catch (BadPaddingException e6) {
            e6.printStackTrace();
            return new RespClickList(new LinkedList());
        } catch (IOException e7) {
            e7.printStackTrace();
            return new RespClickList(new LinkedList());
        } catch (ClassNotFoundException e8) {
            e8.printStackTrace();
            return new RespClickList(new LinkedList());
        } catch (Exception e9) {
            e9.printStackTrace();
            return new RespClickList(new LinkedList());
        }
    }

    public static String encTripleDES(String key, String plainText) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        byte[] cipherKey = new byte[24];
        System.arraycopy(key.getBytes("UTF-8"), 0, cipherKey, 0, 24);
        SecretKey deskey = new SecretKeySpec(cipherKey, "DESede");
        Cipher c1 = Cipher.getInstance("DESede");
        c1.init(1, deskey);
        return Base64.encodeToString(c1.doFinal(plainText.getBytes()), false);
    }

    public static String decTripleDES(String key, String strBaseEncData) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        byte[] cipherKey = new byte[24];
        System.arraycopy(key.getBytes("UTF-8"), 0, cipherKey, 0, 24);
        SecretKey deskey = new SecretKeySpec(cipherKey, "DESede");
        Cipher c2 = Cipher.getInstance("DESede");
        c2.init(2, deskey);
        return new String(c2.doFinal(Base64.decode(strBaseEncData)), "UTF-8");
    }

    public static PublicKey getRsaPubKey(byte[] encodedPublicKey) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(encodedPublicKey));
    }

    public static PrivateKey getRsaPriKey(byte[] encodedPrivateKey) throws NoSuchAlgorithmException, IOException, InvalidKeySpecException {
        return KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(encodedPrivateKey));
    }

    public static Cipher getRsaEncryptCipher(PublicKey pubKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, NoSuchProviderException {
        Cipher cipher = Cipher.getInstance(algorithm, provider);
        cipher.init(1, pubKey);
        return cipher;
    }

    public static Cipher getRsaDecryptCipher(PrivateKey priKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, NoSuchProviderException {
        Cipher cipher = Cipher.getInstance(algorithm, provider);
        cipher.init(2, priKey);
        return cipher;
    }

    public static byte[] encryptData(byte[] data, Cipher cipher) throws IllegalBlockSizeException, BadPaddingException, IOException {
        int thisBlockSize;
        ByteArrayOutputStream out = new ByteArrayOutputStream(53);
        for (int i = 0; i < data.length; i += 245) {
            if (data.length - i <= 245) {
                thisBlockSize = data.length - i;
            } else {
                thisBlockSize = 245;
            }
            out.write(cipher.doFinal(data, i, thisBlockSize));
        }
        return out.toByteArray();
    }

    public static byte[] decryptData(byte[] encrypedData, Cipher cipher) throws IllegalBlockSizeException, BadPaddingException, IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream(PVRTexture.FLAG_MIPMAP);
        for (int j = 0; encrypedData.length - (j * PVRTexture.FLAG_MIPMAP) > 0; j++) {
            out.write(cipher.doFinal(encrypedData, j * PVRTexture.FLAG_MIPMAP, PVRTexture.FLAG_MIPMAP));
        }
        return out.toByteArray();
    }
}
