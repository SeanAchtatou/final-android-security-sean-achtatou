package com.vpon.adon.android;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.vpon.adon.android.entity.Ad;
import com.vpon.adon.android.entity.AdRedirectPack;
import com.vpon.adon.android.entity.RespClick;
import com.vpon.adon.android.entity.RespClickList;
import com.vpon.adon.android.exception.ServiceUnavailableException;
import com.vpon.adon.android.utils.AdOnJsonUtil;
import com.vpon.adon.android.utils.AdOnServerUtil;
import com.vpon.adon.android.utils.AdOnUrlUtil;
import com.vpon.adon.android.utils.CellTowerUtil;
import com.vpon.adon.android.utils.IOUtils;
import com.vpon.adon.android.utils.LocationUtil;
import com.vpon.adon.android.utils.PhoneStateUtils;
import com.vpon.adon.android.utils.VPLog;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

public final class AdManager implements LocationListener {
    /* access modifiers changed from: private */
    public static AdManager instance = null;
    public static final String vponServer = "service@vpon.com";
    private List<AdView> adViews = new LinkedList();
    private Context context;
    private double lat;
    private double lon;
    private List<RespClick> respClicks;
    private Location userLocation;

    private AdManager(Context context2) {
        this.context = context2;
        LocationUtil.instance(context2).addLocationListener(this);
    }

    /* access modifiers changed from: package-private */
    public void addAdView(AdView adView) {
        this.adViews.add(adView);
    }

    /* access modifiers changed from: package-private */
    public void performAdRequester(final AdView adView, final Handler handler) {
        new Thread(new Runnable() {
            public void run() {
                synchronized (AdManager.instance) {
                    AdManager.this.adRequester(adView, handler);
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void adRequester(AdView adView, Handler handler) {
        try {
            JSONObject jsonObject = AdOnJsonUtil.getWebviewAdReqJson(this.context, this.lat, this.lon, adView.getLicenseKey());
            VPLog.i("AdManager", "json: " + jsonObject.toString());
            String url = AdOnUrlUtil.getAdReqUrl(adView.getPlatform());
            HttpResponse httpResp = IOUtils.instance().connectJSONServer(jsonObject.toString(), url, AppConfig.SDKVERSION);
            int reStatus = Integer.valueOf(httpResp.getLastHeader("X-ADON-STATUS").getValue()).intValue();
            if (reStatus != 0) {
                AdOnServerUtil.printErrorLog(reStatus);
                displayNextAd(adView, handler, null);
                return;
            }
            String html = EntityUtils.toString(httpResp.getEntity());
            Ad ad = new Ad();
            ad.setAdHtml(html);
            ad.setAdId(httpResp.getLastHeader("X-ADON-AD_ID").getValue());
            ad.setAdHeight(Integer.valueOf(httpResp.getLastHeader("X-ADON-AD_HEIGHT").getValue()).intValue());
            ad.setAdWidth(Integer.valueOf(httpResp.getLastHeader("X-ADON-AD_WIDTH").getValue()).intValue());
            ad.setRefreshTime(Integer.valueOf(httpResp.getLastHeader("X-ADON-REFRESH_TIME").getValue()).intValue());
            if (httpResp.getLastHeader("X-ADON-LAT") != null) {
                ad.setMapLat(Double.valueOf(httpResp.getLastHeader("X-ADON-LAT").getValue()).doubleValue());
            }
            if (httpResp.getLastHeader("X-ADON-LON") != null) {
                ad.setMapLon(Double.valueOf(httpResp.getLastHeader("X-ADON-LON").getValue()).doubleValue());
            }
            String imei = PhoneStateUtils.getIMEI(this.context);
            String cellId = CellTowerUtil.instance(this.context).getCellId();
            String lac = CellTowerUtil.instance(this.context).getLac();
            String mnc = CellTowerUtil.instance(this.context).getMnc();
            String mcc = CellTowerUtil.instance(this.context).getMcc();
            AdRedirectPack adRedirectPack = new AdRedirectPack();
            adRedirectPack.setAdId(ad.getAdId());
            adRedirectPack.setCellId(cellId);
            adRedirectPack.setImei(imei);
            adRedirectPack.setLac(lac);
            adRedirectPack.setLat(Double.valueOf(this.lat));
            adRedirectPack.setLicensekey(adView.getLicenseKey());
            adRedirectPack.setLon(Double.valueOf(this.lon));
            adRedirectPack.setMcc(mcc);
            adRedirectPack.setMnc(mnc);
            adRedirectPack.setLocation(this.userLocation);
            ad.setAdRedirectPack(adRedirectPack);
            displayNextAd(adView, handler, ad);
        } catch (JSONException e) {
            displayNextAd(adView, handler, null);
        } catch (InvalidKeyException e2) {
            displayNextAd(adView, handler, null);
        } catch (ParseException e3) {
            displayNextAd(adView, handler, null);
        } catch (NoSuchAlgorithmException e4) {
            displayNextAd(adView, handler, null);
        } catch (NoSuchPaddingException e5) {
            displayNextAd(adView, handler, null);
        } catch (InvalidKeySpecException e6) {
            displayNextAd(adView, handler, null);
        } catch (IllegalBlockSizeException e7) {
            displayNextAd(adView, handler, null);
        } catch (BadPaddingException e8) {
            displayNextAd(adView, handler, null);
        } catch (NoSuchProviderException e9) {
            displayNextAd(adView, handler, null);
        } catch (IOException e10) {
            displayNextAd(adView, handler, null);
        } catch (ServiceUnavailableException e11) {
            displayNextAd(adView, handler, null);
        } catch (URISyntaxException e12) {
            displayNextAd(adView, handler, null);
        } catch (Exception e13) {
            displayNextAd(adView, handler, null);
        }
    }

    private void displayNextAd(final AdView adView, Handler handler, final Ad ad) {
        handler.post(new Runnable() {
            public void run() {
                adView.displayNextAd(ad);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void performErrorRequester(final AdView adView, final String errorMessage) {
        new Thread(new Runnable() {
            public void run() {
                synchronized (AdManager.instance) {
                    AdManager.this.errorRequester(adView, errorMessage);
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void errorRequester(AdView adView, String errorMessage) {
        try {
            JSONObject json = AdOnJsonUtil.getErrorJson(this.context, this.lat, this.lon, adView.getLicenseKey(), String.valueOf(adView.getLicenseKey()) + " generate error: " + errorMessage);
            IOUtils.instance().connectJSONServer(json.toString(), AdOnUrlUtil.getAdErrorUrl(adView.getPlatform()), AppConfig.SDKVERSION);
        } catch (ServiceUnavailableException | IOException | Exception | URISyntaxException | InvalidKeyException | NoSuchAlgorithmException | NoSuchProviderException | InvalidKeySpecException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException | ParseException | JSONException e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void performClickRequester(final AdView adView, final Ad currentAd) {
        new Thread(new Runnable() {
            public void run() {
                synchronized (AdManager.instance) {
                    AdManager.this.clickRequester(adView, currentAd);
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void clickRequester(AdView adView, Ad currentAd) {
        String currentTime = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z").format(Calendar.getInstance().getTime());
        String cellId = CellTowerUtil.instance(this.context).getCellId();
        String lac = CellTowerUtil.instance(this.context).getLac();
        String mnc = CellTowerUtil.instance(this.context).getMnc();
        String mcc = CellTowerUtil.instance(this.context).getMcc();
        RespClick respClick = new RespClick();
        respClick.setLicenseKey(adView.getLicenseKey());
        respClick.setAdId(currentAd.getAdId());
        respClick.setLat(this.lat);
        respClick.setLon(this.lon);
        respClick.setCellID(cellId);
        respClick.setLac(lac);
        respClick.setMcc(mcc);
        respClick.setMnc(mnc);
        respClick.setTime(currentTime);
        if (!new File(getClickFilePath()).exists()) {
            writeClickListToFile(new RespClickList(new LinkedList()));
        }
        this.respClicks = readClickListFromFile().getRespClicks();
        this.respClicks.add(respClick);
        VPLog.i("respClicks size", String.valueOf(this.respClicks.size()));
        sendRespClicks(adView);
    }

    private void sendRespClicks(AdView adView) {
        VPLog.i("AdMAnager", "sendRespClicks");
        ObjectOutputStream oos = null;
        try {
            JSONObject json = AdOnJsonUtil.getClickJson(this.context, this.lat, this.lon, adView.getLicenseKey(), this.respClicks);
            String url = AdOnUrlUtil.getAdClickUrl(adView.getPlatform());
            IOUtils instance2 = IOUtils.instance();
            if (instance2 != null) {
                int reStatus = Integer.valueOf(instance2.connectJSONServer(json.toString(), url, AppConfig.SDKVERSION).getLastHeader("X-ADON-STATUS").getValue()).intValue();
                VPLog.i("AdMAnager", "reStatus: " + String.valueOf(reStatus));
                if (reStatus >= 0) {
                    this.respClicks = new LinkedList();
                }
                writeClickListToFile(new RespClickList(this.respClicks));
                VPLog.i("respClicks size", String.valueOf(this.respClicks.size()));
                if (oos != null) {
                    try {
                        oos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        } catch (JSONException e3) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            }
        } catch (InvalidKeyException e5) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e6) {
                    e6.printStackTrace();
                }
            }
        } catch (ParseException e7) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e8) {
                    e8.printStackTrace();
                }
            }
        } catch (NoSuchAlgorithmException e9) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e10) {
                    e10.printStackTrace();
                }
            }
        } catch (NoSuchPaddingException e11) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e12) {
                    e12.printStackTrace();
                }
            }
        } catch (InvalidKeySpecException e13) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e14) {
                    e14.printStackTrace();
                }
            }
        } catch (IllegalBlockSizeException e15) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e16) {
                    e16.printStackTrace();
                }
            }
        } catch (BadPaddingException e17) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e18) {
                    e18.printStackTrace();
                }
            }
        } catch (NoSuchProviderException e19) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e20) {
                    e20.printStackTrace();
                }
            }
        } catch (IOException e21) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e22) {
                    e22.printStackTrace();
                }
            }
        } catch (ServiceUnavailableException e23) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e24) {
                    e24.printStackTrace();
                }
            }
        } catch (URISyntaxException e25) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e26) {
                    e26.printStackTrace();
                }
            }
        } catch (Exception e27) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e28) {
                    e28.printStackTrace();
                }
            }
        } catch (Throwable th) {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e29) {
                    e29.printStackTrace();
                }
            }
            throw th;
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:15:0x0031=Splitter:B:15:0x0031, B:38:0x0059=Splitter:B:38:0x0059} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void writeClickListToFile(com.vpon.adon.android.entity.RespClickList r9) {
        /*
            r8 = this;
            java.io.File r1 = new java.io.File
            java.lang.String r7 = r8.getClickFilePath()
            r1.<init>(r7)
            r4 = 0
            r2 = 0
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x002f, IOException -> 0x0057 }
            r3.<init>(r1)     // Catch:{ FileNotFoundException -> 0x002f, IOException -> 0x0057 }
            java.io.ObjectOutputStream r5 = new java.io.ObjectOutputStream     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00ce, all -> 0x00c7 }
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00ce, all -> 0x00c7 }
            r7.<init>(r1)     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00ce, all -> 0x00c7 }
            r5.<init>(r7)     // Catch:{ FileNotFoundException -> 0x00d7, IOException -> 0x00ce, all -> 0x00c7 }
            java.lang.String r7 = "http://www.vpon.com/v/index.jsp88623698333service@vpon.com"
            javax.crypto.SealedObject r6 = com.vpon.adon.android.utils.CryptUtils.clickListEnc(r7, r9)     // Catch:{ FileNotFoundException -> 0x00dc, IOException -> 0x00d2, all -> 0x00ca }
            r5.writeObject(r6)     // Catch:{ FileNotFoundException -> 0x00dc, IOException -> 0x00d2, all -> 0x00ca }
            r5.flush()     // Catch:{ FileNotFoundException -> 0x00dc, IOException -> 0x00d2, all -> 0x00ca }
            r5.close()     // Catch:{ IOException -> 0x00a3 }
            r3.close()     // Catch:{ IOException -> 0x00bf }
            r2 = r3
            r4 = r5
        L_0x002e:
            return
        L_0x002f:
            r7 = move-exception
            r0 = r7
        L_0x0031:
            r0.printStackTrace()     // Catch:{ all -> 0x007f }
            r4.close()     // Catch:{ IOException -> 0x0040 }
            r2.close()     // Catch:{ IOException -> 0x003b }
            goto L_0x002e
        L_0x003b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002e
        L_0x0040:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x004d }
            r2.close()     // Catch:{ IOException -> 0x0048 }
            goto L_0x002e
        L_0x0048:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002e
        L_0x004d:
            r7 = move-exception
            r2.close()     // Catch:{ IOException -> 0x0052 }
        L_0x0051:
            throw r7
        L_0x0052:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0051
        L_0x0057:
            r7 = move-exception
            r0 = r7
        L_0x0059:
            r0.printStackTrace()     // Catch:{ all -> 0x007f }
            r4.close()     // Catch:{ IOException -> 0x0068 }
            r2.close()     // Catch:{ IOException -> 0x0063 }
            goto L_0x002e
        L_0x0063:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002e
        L_0x0068:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0075 }
            r2.close()     // Catch:{ IOException -> 0x0070 }
            goto L_0x002e
        L_0x0070:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002e
        L_0x0075:
            r7 = move-exception
            r2.close()     // Catch:{ IOException -> 0x007a }
        L_0x0079:
            throw r7
        L_0x007a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0079
        L_0x007f:
            r7 = move-exception
        L_0x0080:
            r4.close()     // Catch:{ IOException -> 0x0087 }
            r2.close()     // Catch:{ IOException -> 0x009e }
        L_0x0086:
            throw r7
        L_0x0087:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0094 }
            r2.close()     // Catch:{ IOException -> 0x008f }
            goto L_0x0086
        L_0x008f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0086
        L_0x0094:
            r7 = move-exception
            r2.close()     // Catch:{ IOException -> 0x0099 }
        L_0x0098:
            throw r7
        L_0x0099:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0098
        L_0x009e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0086
        L_0x00a3:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00b5 }
            r3.close()     // Catch:{ IOException -> 0x00ad }
            r2 = r3
            r4 = r5
            goto L_0x002e
        L_0x00ad:
            r0 = move-exception
            r0.printStackTrace()
            r2 = r3
            r4 = r5
            goto L_0x002e
        L_0x00b5:
            r7 = move-exception
            r3.close()     // Catch:{ IOException -> 0x00ba }
        L_0x00b9:
            throw r7
        L_0x00ba:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00b9
        L_0x00bf:
            r0 = move-exception
            r0.printStackTrace()
            r2 = r3
            r4 = r5
            goto L_0x002e
        L_0x00c7:
            r7 = move-exception
            r2 = r3
            goto L_0x0080
        L_0x00ca:
            r7 = move-exception
            r2 = r3
            r4 = r5
            goto L_0x0080
        L_0x00ce:
            r7 = move-exception
            r0 = r7
            r2 = r3
            goto L_0x0059
        L_0x00d2:
            r7 = move-exception
            r0 = r7
            r2 = r3
            r4 = r5
            goto L_0x0059
        L_0x00d7:
            r7 = move-exception
            r0 = r7
            r2 = r3
            goto L_0x0031
        L_0x00dc:
            r7 = move-exception
            r0 = r7
            r2 = r3
            r4 = r5
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vpon.adon.android.AdManager.writeClickListToFile(com.vpon.adon.android.entity.RespClickList):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0041 A[SYNTHETIC, Splitter:B:18:0x0041] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005b A[SYNTHETIC, Splitter:B:27:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0075 A[SYNTHETIC, Splitter:B:36:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x008f A[SYNTHETIC, Splitter:B:45:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x009b A[SYNTHETIC, Splitter:B:51:0x009b] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:42:0x0080=Splitter:B:42:0x0080, B:15:0x0032=Splitter:B:15:0x0032, B:33:0x0066=Splitter:B:33:0x0066, B:24:0x004c=Splitter:B:24:0x004c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.vpon.adon.android.entity.RespClickList readClickListFromFile() {
        /*
            r8 = this;
            java.io.File r1 = new java.io.File
            java.lang.String r6 = r8.getClickFilePath()
            r1.<init>(r6)
            r2 = 0
            java.io.ObjectInputStream r3 = new java.io.ObjectInputStream     // Catch:{ StreamCorruptedException -> 0x0030, FileNotFoundException -> 0x004a, IOException -> 0x0064, ClassNotFoundException -> 0x007e }
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ StreamCorruptedException -> 0x0030, FileNotFoundException -> 0x004a, IOException -> 0x0064, ClassNotFoundException -> 0x007e }
            r6.<init>(r1)     // Catch:{ StreamCorruptedException -> 0x0030, FileNotFoundException -> 0x004a, IOException -> 0x0064, ClassNotFoundException -> 0x007e }
            r3.<init>(r6)     // Catch:{ StreamCorruptedException -> 0x0030, FileNotFoundException -> 0x004a, IOException -> 0x0064, ClassNotFoundException -> 0x007e }
            java.lang.Object r5 = r3.readObject()     // Catch:{ StreamCorruptedException -> 0x00b9, FileNotFoundException -> 0x00b5, IOException -> 0x00b1, ClassNotFoundException -> 0x00ad, all -> 0x00aa }
            if (r5 != 0) goto L_0x0021
            java.lang.String r6 = "adManger"
            java.lang.String r7 = "sealedObject null"
            com.vpon.adon.android.utils.VPLog.i(r6, r7)     // Catch:{ StreamCorruptedException -> 0x00b9, FileNotFoundException -> 0x00b5, IOException -> 0x00b1, ClassNotFoundException -> 0x00ad, all -> 0x00aa }
        L_0x0021:
            java.lang.String r6 = "http://www.vpon.com/v/index.jsp88623698333service@vpon.com"
            javax.crypto.SealedObject r5 = (javax.crypto.SealedObject) r5     // Catch:{ StreamCorruptedException -> 0x00b9, FileNotFoundException -> 0x00b5, IOException -> 0x00b1, ClassNotFoundException -> 0x00ad, all -> 0x00aa }
            com.vpon.adon.android.entity.RespClickList r4 = com.vpon.adon.android.utils.CryptUtils.clickListDnc(r6, r5)     // Catch:{ StreamCorruptedException -> 0x00b9, FileNotFoundException -> 0x00b5, IOException -> 0x00b1, ClassNotFoundException -> 0x00ad, all -> 0x00aa }
            if (r3 == 0) goto L_0x00a8
            r3.close()     // Catch:{ IOException -> 0x00a4 }
            r2 = r3
        L_0x002f:
            return r4
        L_0x0030:
            r6 = move-exception
            r0 = r6
        L_0x0032:
            r0.printStackTrace()     // Catch:{ all -> 0x0098 }
            com.vpon.adon.android.entity.RespClickList r4 = new com.vpon.adon.android.entity.RespClickList     // Catch:{ all -> 0x0098 }
            java.util.LinkedList r6 = new java.util.LinkedList     // Catch:{ all -> 0x0098 }
            r6.<init>()     // Catch:{ all -> 0x0098 }
            r4.<init>(r6)     // Catch:{ all -> 0x0098 }
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x0045 }
            goto L_0x002f
        L_0x0045:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002f
        L_0x004a:
            r6 = move-exception
            r0 = r6
        L_0x004c:
            r0.printStackTrace()     // Catch:{ all -> 0x0098 }
            com.vpon.adon.android.entity.RespClickList r4 = new com.vpon.adon.android.entity.RespClickList     // Catch:{ all -> 0x0098 }
            java.util.LinkedList r6 = new java.util.LinkedList     // Catch:{ all -> 0x0098 }
            r6.<init>()     // Catch:{ all -> 0x0098 }
            r4.<init>(r6)     // Catch:{ all -> 0x0098 }
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x005f }
            goto L_0x002f
        L_0x005f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002f
        L_0x0064:
            r6 = move-exception
            r0 = r6
        L_0x0066:
            r0.printStackTrace()     // Catch:{ all -> 0x0098 }
            com.vpon.adon.android.entity.RespClickList r4 = new com.vpon.adon.android.entity.RespClickList     // Catch:{ all -> 0x0098 }
            java.util.LinkedList r6 = new java.util.LinkedList     // Catch:{ all -> 0x0098 }
            r6.<init>()     // Catch:{ all -> 0x0098 }
            r4.<init>(r6)     // Catch:{ all -> 0x0098 }
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x0079 }
            goto L_0x002f
        L_0x0079:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002f
        L_0x007e:
            r6 = move-exception
            r0 = r6
        L_0x0080:
            r0.printStackTrace()     // Catch:{ all -> 0x0098 }
            com.vpon.adon.android.entity.RespClickList r4 = new com.vpon.adon.android.entity.RespClickList     // Catch:{ all -> 0x0098 }
            java.util.LinkedList r6 = new java.util.LinkedList     // Catch:{ all -> 0x0098 }
            r6.<init>()     // Catch:{ all -> 0x0098 }
            r4.<init>(r6)     // Catch:{ all -> 0x0098 }
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x0093 }
            goto L_0x002f
        L_0x0093:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002f
        L_0x0098:
            r6 = move-exception
        L_0x0099:
            if (r2 == 0) goto L_0x009e
            r2.close()     // Catch:{ IOException -> 0x009f }
        L_0x009e:
            throw r6
        L_0x009f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x009e
        L_0x00a4:
            r0 = move-exception
            r0.printStackTrace()
        L_0x00a8:
            r2 = r3
            goto L_0x002f
        L_0x00aa:
            r6 = move-exception
            r2 = r3
            goto L_0x0099
        L_0x00ad:
            r6 = move-exception
            r0 = r6
            r2 = r3
            goto L_0x0080
        L_0x00b1:
            r6 = move-exception
            r0 = r6
            r2 = r3
            goto L_0x0066
        L_0x00b5:
            r6 = move-exception
            r0 = r6
            r2 = r3
            goto L_0x004c
        L_0x00b9:
            r6 = move-exception
            r0 = r6
            r2 = r3
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vpon.adon.android.AdManager.readClickListFromFile():com.vpon.adon.android.entity.RespClickList");
    }

    private String getClickFilePath() {
        return String.valueOf(this.context.getFilesDir().getParent()) + File.separator + "appDataFile";
    }

    static AdManager instance(Context context2) {
        if (instance == null) {
            instance = new AdManager(context2);
        }
        return instance;
    }

    public void remove() {
        instance = null;
    }

    public void onLocationChanged(Location location) {
        this.userLocation = location;
        this.lat = location.getLatitude();
        this.lon = location.getLongitude();
        Log.v("SDK lat", Double.toString(this.lat));
        Log.v("SDK lon", Double.toString(this.lon));
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
}
