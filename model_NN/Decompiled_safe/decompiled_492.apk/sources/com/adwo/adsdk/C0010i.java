package com.adwo.adsdk;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.adwo.adsdk.i  reason: case insensitive filesystem */
public final class C0010i {
    private static byte[] A = null;
    private static String B = null;
    private static int C = 0;
    protected static String a = null;
    protected static int b = 1;
    protected static int c = 2;
    protected static int d = 3;
    protected static int e = 4;
    protected static volatile int f = 1;
    protected static String g = "";
    protected static Set h = new HashSet();
    private static TelephonyManager i;
    private static boolean j = false;
    private static byte[] k = null;
    private static byte[] l = null;
    private static String m;
    private static String n = null;
    private static Context o = null;
    private static volatile boolean p = false;
    private static byte q = 0;
    private static byte r = 0;
    private static byte[] s = null;
    private static byte[] t = null;
    private static byte u = 0;
    private static short v = 0;
    private static byte[] w = null;
    private static byte x = 2;
    private static byte y = 2;
    private static byte z = 2;

    protected static String a(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                return applicationInfo.metaData.getString("Adwo_PID");
            }
            return null;
        } catch (Exception e2) {
            Log.e("Adwo SDK", "Could not read Adwo_PID meta-data from AndroidManifest.xml.");
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x007b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void b(android.content.Context r10) {
        /*
            r9 = 6
            r8 = 9
            r7 = 3
            r6 = 0
            r5 = 1
            com.adwo.adsdk.C0010i.o = r10
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r10.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            com.adwo.adsdk.C0010i.i = r0
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            int r0 = r10.checkCallingOrSelfPermission(r0)
            r1 = -1
            if (r0 != r1) goto L_0x0020
            java.lang.String r0 = "Cannot request an ad without READ_PHONE_STATE permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />"
            a(r0)
        L_0x0020:
            java.lang.String r0 = com.adwo.adsdk.C0010i.a
            if (r0 != 0) goto L_0x0030
            android.telephony.TelephonyManager r0 = com.adwo.adsdk.C0010i.i
            if (r0 == 0) goto L_0x0030
            android.telephony.TelephonyManager r0 = com.adwo.adsdk.C0010i.i
            java.lang.String r0 = r0.getDeviceId()
            com.adwo.adsdk.C0010i.a = r0
        L_0x0030:
            java.lang.String r0 = com.adwo.adsdk.C0010i.a
            if (r0 != 0) goto L_0x0040
            android.content.ContentResolver r0 = r10.getContentResolver()
            java.lang.String r1 = "android_id"
            java.lang.String r0 = android.provider.Settings.System.getString(r0, r1)
            com.adwo.adsdk.C0010i.a = r0
        L_0x0040:
            java.lang.String r0 = com.adwo.adsdk.C0010i.a
            if (r0 != 0) goto L_0x0048
            java.lang.String r0 = "00000000"
            com.adwo.adsdk.C0010i.a = r0
        L_0x0048:
            java.lang.String r0 = com.adwo.adsdk.C0010i.a     // Catch:{ UnsupportedEncodingException -> 0x0a27 }
            if (r0 == 0) goto L_0x0056
            java.lang.String r0 = com.adwo.adsdk.C0010i.a     // Catch:{ UnsupportedEncodingException -> 0x0a27 }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x0a27 }
            com.adwo.adsdk.C0010i.l = r0     // Catch:{ UnsupportedEncodingException -> 0x0a27 }
        L_0x0056:
            android.content.res.Resources r0 = r10.getResources()
            android.content.res.Configuration r0 = r0.getConfiguration()
            java.util.Locale r0 = r0.locale
            java.lang.String r0 = r0.getLanguage()
            java.lang.String r1 = "en"
            boolean r1 = r0.contains(r1)
            if (r1 != 0) goto L_0x01f9
            java.lang.String r1 = "zh"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01a0
            r0 = r6
        L_0x0075:
            com.adwo.adsdk.C0010i.x = r0
            java.lang.String r0 = com.adwo.adsdk.C0010i.n
            if (r0 != 0) goto L_0x0089
            android.telephony.TelephonyManager r0 = com.adwo.adsdk.C0010i.i
            if (r0 == 0) goto L_0x01fc
            android.telephony.TelephonyManager r0 = com.adwo.adsdk.C0010i.i
            java.lang.String r0 = r0.getSimSerialNumber()
            com.adwo.adsdk.C0010i.m = r0
        L_0x0087:
            com.adwo.adsdk.C0010i.n = r0
        L_0x0089:
            byte[] r0 = com.adwo.adsdk.C0010i.A
            if (r0 != 0) goto L_0x00b2
            android.telephony.TelephonyManager r0 = com.adwo.adsdk.C0010i.i
            if (r0 == 0) goto L_0x00b2
            android.telephony.TelephonyManager r0 = com.adwo.adsdk.C0010i.i
            java.lang.String r0 = r0.getLine1Number()
            if (r0 == 0) goto L_0x00b2
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x0a24 }
            java.lang.String r2 = "phoneNumber:"
            r1.<init>(r2)     // Catch:{ UnsupportedEncodingException -> 0x0a24 }
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x0a24 }
            r1.toString()     // Catch:{ UnsupportedEncodingException -> 0x0a24 }
            com.adwo.adsdk.O.a()     // Catch:{ UnsupportedEncodingException -> 0x0a24 }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x0a24 }
            com.adwo.adsdk.C0010i.A = r0     // Catch:{ UnsupportedEncodingException -> 0x0a24 }
        L_0x00b2:
            java.lang.String r0 = r10.getPackageName()
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x01ff }
            com.adwo.adsdk.C0010i.w = r0     // Catch:{ UnsupportedEncodingException -> 0x01ff }
        L_0x00be:
            c(r10)
            java.util.Properties r0 = java.lang.System.getProperties()
            java.lang.String r1 = "os.version"
            java.lang.Object r10 = r0.get(r1)
            java.lang.String r10 = (java.lang.String) r10
            if (r10 == 0) goto L_0x00e5
            int r0 = r10.length()
            if (r0 < r7) goto L_0x00e5
            java.lang.String r0 = r10.substring(r6, r7)
            r1 = 4621819117588971520(0x4024000000000000, double:10.0)
            double r3 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x0a21 }
            double r0 = r1 * r3
            int r0 = (int) r0     // Catch:{ Exception -> 0x0a21 }
            byte r0 = (byte) r0     // Catch:{ Exception -> 0x0a21 }
            com.adwo.adsdk.C0010i.u = r0     // Catch:{ Exception -> 0x0a21 }
        L_0x00e5:
            java.lang.Class<android.os.Build> r0 = android.os.Build.class
            java.lang.String r1 = android.os.Build.MODEL     // Catch:{ SecurityException -> 0x0a1e, NoSuchFieldException -> 0x0a1b, IllegalArgumentException -> 0x0a18, IllegalAccessException -> 0x0a15, UnsupportedEncodingException -> 0x0a12 }
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ SecurityException -> 0x0a1e, NoSuchFieldException -> 0x0a1b, IllegalArgumentException -> 0x0a18, IllegalAccessException -> 0x0a15, UnsupportedEncodingException -> 0x0a12 }
            java.lang.String r2 = "UTF-8"
            byte[] r1 = r1.getBytes(r2)     // Catch:{ SecurityException -> 0x0a1e, NoSuchFieldException -> 0x0a1b, IllegalArgumentException -> 0x0a18, IllegalAccessException -> 0x0a15, UnsupportedEncodingException -> 0x0a12 }
            com.adwo.adsdk.C0010i.t = r1     // Catch:{ SecurityException -> 0x0a1e, NoSuchFieldException -> 0x0a1b, IllegalArgumentException -> 0x0a18, IllegalAccessException -> 0x0a15, UnsupportedEncodingException -> 0x0a12 }
            java.lang.String r1 = "MANUFACTURER"
            java.lang.reflect.Field r0 = r0.getField(r1)     // Catch:{ SecurityException -> 0x0a1e, NoSuchFieldException -> 0x0a1b, IllegalArgumentException -> 0x0a18, IllegalAccessException -> 0x0a15, UnsupportedEncodingException -> 0x0a12 }
            android.os.Build r1 = new android.os.Build     // Catch:{ SecurityException -> 0x0a1e, NoSuchFieldException -> 0x0a1b, IllegalArgumentException -> 0x0a18, IllegalAccessException -> 0x0a15, UnsupportedEncodingException -> 0x0a12 }
            r1.<init>()     // Catch:{ SecurityException -> 0x0a1e, NoSuchFieldException -> 0x0a1b, IllegalArgumentException -> 0x0a18, IllegalAccessException -> 0x0a15, UnsupportedEncodingException -> 0x0a12 }
            java.lang.Object r10 = r0.get(r1)     // Catch:{ SecurityException -> 0x0a1e, NoSuchFieldException -> 0x0a1b, IllegalArgumentException -> 0x0a18, IllegalAccessException -> 0x0a15, UnsupportedEncodingException -> 0x0a12 }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ SecurityException -> 0x0a1e, NoSuchFieldException -> 0x0a1b, IllegalArgumentException -> 0x0a18, IllegalAccessException -> 0x0a15, UnsupportedEncodingException -> 0x0a12 }
            if (r10 == 0) goto L_0x011a
            int r0 = r10.length()     // Catch:{ SecurityException -> 0x0a1e, NoSuchFieldException -> 0x0a1b, IllegalArgumentException -> 0x0a18, IllegalAccessException -> 0x0a15, UnsupportedEncodingException -> 0x0a12 }
            if (r0 <= 0) goto L_0x011a
            java.lang.String r0 = r10.toLowerCase()     // Catch:{ SecurityException -> 0x0a1e, NoSuchFieldException -> 0x0a1b, IllegalArgumentException -> 0x0a18, IllegalAccessException -> 0x0a15, UnsupportedEncodingException -> 0x0a12 }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ SecurityException -> 0x0a1e, NoSuchFieldException -> 0x0a1b, IllegalArgumentException -> 0x0a18, IllegalAccessException -> 0x0a15, UnsupportedEncodingException -> 0x0a12 }
            com.adwo.adsdk.C0010i.s = r0     // Catch:{ SecurityException -> 0x0a1e, NoSuchFieldException -> 0x0a1b, IllegalArgumentException -> 0x0a18, IllegalAccessException -> 0x0a15, UnsupportedEncodingException -> 0x0a12 }
        L_0x011a:
            android.telephony.TelephonyManager r0 = com.adwo.adsdk.C0010i.i     // Catch:{ Exception -> 0x0a0f }
            java.lang.String r0 = r0.getNetworkOperator()     // Catch:{ Exception -> 0x0a0f }
            if (r0 == 0) goto L_0x013d
            int r1 = r0.length()     // Catch:{ Exception -> 0x0a0f }
            r2 = 4
            if (r1 < r2) goto L_0x013d
            r1 = 0
            r2 = 3
            java.lang.String r1 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x0a0f }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x0a0f }
            com.adwo.adsdk.C0010i.C = r1     // Catch:{ Exception -> 0x0a0f }
            r1 = 3
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x0a0f }
            java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0a0f }
        L_0x013d:
            int r0 = com.adwo.adsdk.C0010i.C
            r1 = 460(0x1cc, float:6.45E-43)
            if (r0 != r1) goto L_0x019f
            java.lang.String r0 = com.adwo.adsdk.C0010i.n
            if (r0 == 0) goto L_0x019f
            java.lang.String r0 = com.adwo.adsdk.C0010i.n
            r1 = 2
            byte[] r1 = new byte[r1]
            int r2 = r0.length()
            if (r2 < r9) goto L_0x0197
            r3 = 4
            java.lang.String r3 = r0.substring(r3, r9)
            java.lang.String r4 = "0"
            boolean r4 = r3.endsWith(r4)
            if (r4 != 0) goto L_0x016f
            java.lang.String r4 = "2"
            boolean r4 = r3.endsWith(r4)
            if (r4 != 0) goto L_0x016f
            java.lang.String r4 = "7"
            boolean r4 = r3.endsWith(r4)
            if (r4 == 0) goto L_0x0459
        L_0x016f:
            r3 = 7
            if (r2 < r3) goto L_0x0197
            r3 = 7
            java.lang.String r3 = r0.substring(r9, r3)
            java.lang.String r4 = "0"
            boolean r4 = r3.equals(r4)
            if (r4 == 0) goto L_0x0207
            r1[r6] = r6
        L_0x0181:
            r3 = 10
            if (r2 < r3) goto L_0x0197
            r2 = 8
            r3 = 10
            java.lang.String r0 = r0.substring(r2, r3)
            java.lang.String r2 = "01"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02bf
            r1[r5] = r5
        L_0x0197:
            byte r0 = r1[r6]
            com.adwo.adsdk.C0010i.q = r0
            byte r0 = r1[r5]
            com.adwo.adsdk.C0010i.r = r0
        L_0x019f:
            return
        L_0x01a0:
            java.lang.String r1 = "ko"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01ab
            r0 = 5
            goto L_0x0075
        L_0x01ab:
            java.lang.String r1 = "fr"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01b6
            r0 = r7
            goto L_0x0075
        L_0x01b6:
            java.lang.String r1 = "es"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01c2
            r0 = 8
            goto L_0x0075
        L_0x01c2:
            java.lang.String r1 = "de"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01cd
            r0 = r9
            goto L_0x0075
        L_0x01cd:
            java.lang.String r1 = "it"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01d8
            r0 = 7
            goto L_0x0075
        L_0x01d8:
            java.lang.String r1 = "ja"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01e3
            r0 = 4
            goto L_0x0075
        L_0x01e3:
            java.lang.String r1 = "ru"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01ee
            r0 = r8
            goto L_0x0075
        L_0x01ee:
            java.lang.String r1 = "pt"
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x01f9
            r0 = r5
            goto L_0x0075
        L_0x01f9:
            r0 = 2
            goto L_0x0075
        L_0x01fc:
            r0 = 0
            goto L_0x0087
        L_0x01ff:
            r0 = move-exception
            java.lang.String r0 = "Package Name ERROR:  Incorrect application pakage name.  "
            a(r0)
            goto L_0x00be
        L_0x0207:
            java.lang.String r4 = "1"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0213
            r1[r6] = r5
            goto L_0x0181
        L_0x0213:
            java.lang.String r4 = "2"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0220
            r3 = 2
            r1[r6] = r3
            goto L_0x0181
        L_0x0220:
            java.lang.String r4 = "3"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x022c
            r1[r6] = r7
            goto L_0x0181
        L_0x022c:
            java.lang.String r4 = "4"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0239
            r3 = 4
            r1[r6] = r3
            goto L_0x0181
        L_0x0239:
            java.lang.String r4 = "5"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0246
            r3 = 5
            r1[r6] = r3
            goto L_0x0181
        L_0x0246:
            java.lang.String r4 = "6"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0252
            r1[r6] = r9
            goto L_0x0181
        L_0x0252:
            java.lang.String r4 = "7"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x025f
            r3 = 7
            r1[r6] = r3
            goto L_0x0181
        L_0x025f:
            java.lang.String r4 = "8"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x026d
            r3 = 8
            r1[r6] = r3
            goto L_0x0181
        L_0x026d:
            java.lang.String r4 = "9"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0279
            r1[r6] = r8
            goto L_0x0181
        L_0x0279:
            java.lang.String r4 = "A"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0287
            r3 = 10
            r1[r6] = r3
            goto L_0x0181
        L_0x0287:
            java.lang.String r4 = "B"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0295
            r3 = 11
            r1[r6] = r3
            goto L_0x0181
        L_0x0295:
            java.lang.String r4 = "C"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x02a3
            r3 = 12
            r1[r6] = r3
            goto L_0x0181
        L_0x02a3:
            java.lang.String r4 = "D"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x02b1
            r3 = 13
            r1[r6] = r3
            goto L_0x0181
        L_0x02b1:
            java.lang.String r4 = "E"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x0181
            r3 = 14
            r1[r6] = r3
            goto L_0x0181
        L_0x02bf:
            java.lang.String r2 = "02"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02cb
            r1[r5] = r7
            goto L_0x0197
        L_0x02cb:
            java.lang.String r2 = "03"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02d7
            r1[r5] = r8
            goto L_0x0197
        L_0x02d7:
            java.lang.String r2 = "04"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02e5
            r0 = 12
            r1[r5] = r0
            goto L_0x0197
        L_0x02e5:
            java.lang.String r2 = "05"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02f3
            r0 = 8
            r1[r5] = r0
            goto L_0x0197
        L_0x02f3:
            java.lang.String r2 = "06"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0300
            r0 = 7
            r1[r5] = r0
            goto L_0x0197
        L_0x0300:
            java.lang.String r2 = "07"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x030c
            r1[r5] = r9
            goto L_0x0197
        L_0x030c:
            java.lang.String r2 = "08"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0319
            r0 = 5
            r1[r5] = r0
            goto L_0x0197
        L_0x0319:
            java.lang.String r2 = "09"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0326
            r0 = 2
            r1[r5] = r0
            goto L_0x0197
        L_0x0326:
            java.lang.String r2 = "10"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0334
            r0 = 14
            r1[r5] = r0
            goto L_0x0197
        L_0x0334:
            java.lang.String r2 = "11"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0342
            r0 = 18
            r1[r5] = r0
            goto L_0x0197
        L_0x0342:
            java.lang.String r2 = "12"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0350
            r0 = 13
            r1[r5] = r0
            goto L_0x0197
        L_0x0350:
            java.lang.String r2 = "13"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x035e
            r0 = 19
            r1[r5] = r0
            goto L_0x0197
        L_0x035e:
            java.lang.String r2 = "14"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x036c
            r0 = 15
            r1[r5] = r0
            goto L_0x0197
        L_0x036c:
            java.lang.String r2 = "15"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x037a
            r0 = 11
            r1[r5] = r0
            goto L_0x0197
        L_0x037a:
            java.lang.String r2 = "16"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0388
            r0 = 10
            r1[r5] = r0
            goto L_0x0197
        L_0x0388:
            java.lang.String r2 = "17"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0396
            r0 = 17
            r1[r5] = r0
            goto L_0x0197
        L_0x0396:
            java.lang.String r2 = "18"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03a4
            r0 = 16
            r1[r5] = r0
            goto L_0x0197
        L_0x03a4:
            java.lang.String r2 = "19"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03b2
            r0 = 20
            r1[r5] = r0
            goto L_0x0197
        L_0x03b2:
            java.lang.String r2 = "20"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03c0
            r0 = 29
            r1[r5] = r0
            goto L_0x0197
        L_0x03c0:
            java.lang.String r2 = "21"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03ce
            r0 = 27
            r1[r5] = r0
            goto L_0x0197
        L_0x03ce:
            java.lang.String r2 = "22"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03dc
            r0 = 24
            r1[r5] = r0
            goto L_0x0197
        L_0x03dc:
            java.lang.String r2 = "23"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03ea
            r0 = 25
            r1[r5] = r0
            goto L_0x0197
        L_0x03ea:
            java.lang.String r2 = "24"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03f8
            r0 = 26
            r1[r5] = r0
            goto L_0x0197
        L_0x03f8:
            java.lang.String r2 = "25"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0406
            r0 = 30
            r1[r5] = r0
            goto L_0x0197
        L_0x0406:
            java.lang.String r2 = "26"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0414
            r0 = 21
            r1[r5] = r0
            goto L_0x0197
        L_0x0414:
            java.lang.String r2 = "27"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0422
            r0 = 22
            r1[r5] = r0
            goto L_0x0197
        L_0x0422:
            java.lang.String r2 = "28"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0430
            r0 = 23
            r1[r5] = r0
            goto L_0x0197
        L_0x0430:
            java.lang.String r2 = "29"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x043e
            r0 = 28
            r1[r5] = r0
            goto L_0x0197
        L_0x043e:
            java.lang.String r2 = "30"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x044c
            r0 = 31
            r1[r5] = r0
            goto L_0x0197
        L_0x044c:
            java.lang.String r2 = "31"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0197
            r0 = 4
            r1[r5] = r0
            goto L_0x0197
        L_0x0459:
            java.lang.String r4 = "1"
            boolean r4 = r3.endsWith(r4)
            if (r4 == 0) goto L_0x072d
            if (r2 < r8) goto L_0x0197
            r3 = 8
            java.lang.String r3 = r0.substring(r3, r8)
            java.lang.String r4 = "0"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x048d
            r3 = 24
            r1[r6] = r3
        L_0x0475:
            r3 = 13
            if (r2 < r3) goto L_0x0197
            r2 = 10
            r3 = 13
            java.lang.String r0 = r0.substring(r2, r3)
            java.lang.String r2 = "010"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x04c1
            r1[r5] = r5
            goto L_0x0197
        L_0x048d:
            java.lang.String r4 = "1"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x049a
            r3 = 15
            r1[r6] = r3
            goto L_0x0475
        L_0x049a:
            java.lang.String r4 = "2"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x04a7
            r3 = 16
            r1[r6] = r3
            goto L_0x0475
        L_0x04a7:
            java.lang.String r4 = "5"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x04b4
            r3 = 19
            r1[r6] = r3
            goto L_0x0475
        L_0x04b4:
            java.lang.String r4 = "6"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x0475
            r3 = 20
            r1[r6] = r3
            goto L_0x0475
        L_0x04c1:
            java.lang.String r2 = "022"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x04cd
            r1[r5] = r7
            goto L_0x0197
        L_0x04cd:
            java.lang.String r2 = "31"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x04dd
            java.lang.String r2 = "33"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x04e1
        L_0x04dd:
            r1[r5] = r8
            goto L_0x0197
        L_0x04e1:
            java.lang.String r2 = "35"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x04f1
            java.lang.String r2 = "34"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x04f7
        L_0x04f1:
            r0 = 12
            r1[r5] = r0
            goto L_0x0197
        L_0x04f7:
            java.lang.String r2 = "47"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0507
            java.lang.String r2 = "48"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x050d
        L_0x0507:
            r0 = 8
            r1[r5] = r0
            goto L_0x0197
        L_0x050d:
            java.lang.String r2 = "024"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0525
            java.lang.String r2 = "41"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0525
            java.lang.String r2 = "42"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x052a
        L_0x0525:
            r0 = 7
            r1[r5] = r0
            goto L_0x0197
        L_0x052a:
            java.lang.String r2 = "43"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0536
            r1[r5] = r9
            goto L_0x0197
        L_0x0536:
            java.lang.String r2 = "45"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0546
            java.lang.String r2 = "46"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x054b
        L_0x0546:
            r0 = 5
            r1[r5] = r0
            goto L_0x0197
        L_0x054b:
            java.lang.String r2 = "021"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x0558
            r0 = 2
            r1[r5] = r0
            goto L_0x0197
        L_0x0558:
            java.lang.String r2 = "025"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0570
            java.lang.String r2 = "51"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0570
            java.lang.String r2 = "52"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0576
        L_0x0570:
            r0 = 14
            r1[r5] = r0
            goto L_0x0197
        L_0x0576:
            java.lang.String r2 = "57"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0584
            r0 = 18
            r1[r5] = r0
            goto L_0x0197
        L_0x0584:
            java.lang.String r2 = "55"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0594
            java.lang.String r2 = "56"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x059a
        L_0x0594:
            r0 = 13
            r1[r5] = r0
            goto L_0x0197
        L_0x059a:
            java.lang.String r2 = "59"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x05a8
            r0 = 19
            r1[r5] = r0
            goto L_0x0197
        L_0x05a8:
            java.lang.String r2 = "79"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x05b8
            java.lang.String r2 = "70"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x05be
        L_0x05b8:
            r0 = 15
            r1[r5] = r0
            goto L_0x0197
        L_0x05be:
            java.lang.String r2 = "53"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x05d6
            java.lang.String r2 = "54"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x05d6
            java.lang.String r2 = "63"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x05dc
        L_0x05d6:
            r0 = 11
            r1[r5] = r0
            goto L_0x0197
        L_0x05dc:
            java.lang.String r2 = "37"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x05ec
            java.lang.String r2 = "39"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x05f2
        L_0x05ec:
            r0 = 10
            r1[r5] = r0
            goto L_0x0197
        L_0x05f2:
            java.lang.String r2 = "027"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x060a
            java.lang.String r2 = "71"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x060a
            java.lang.String r2 = "72"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0610
        L_0x060a:
            r0 = 17
            r1[r5] = r0
            goto L_0x0197
        L_0x0610:
            java.lang.String r2 = "73"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0620
            java.lang.String r2 = "74"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0626
        L_0x0620:
            r0 = 16
            r1[r5] = r0
            goto L_0x0197
        L_0x0626:
            java.lang.String r2 = "020"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0646
            java.lang.String r2 = "75"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0646
            java.lang.String r2 = "76"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0646
            java.lang.String r2 = "66"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x064c
        L_0x0646:
            r0 = 20
            r1[r5] = r0
            goto L_0x0197
        L_0x064c:
            java.lang.String r2 = "77"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x065a
            r0 = 29
            r1[r5] = r0
            goto L_0x0197
        L_0x065a:
            java.lang.String r2 = "898"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x0668
            r0 = 27
            r1[r5] = r0
            goto L_0x0197
        L_0x0668:
            java.lang.String r2 = "028"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0688
            java.lang.String r2 = "81"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0688
            java.lang.String r2 = "82"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0688
            java.lang.String r2 = "83"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x068e
        L_0x0688:
            r0 = 24
            r1[r5] = r0
            goto L_0x0197
        L_0x068e:
            java.lang.String r2 = "85"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0197
            java.lang.String r2 = "87"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x06ae
            java.lang.String r2 = "88"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x06ae
            java.lang.String r2 = "69"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x06b4
        L_0x06ae:
            r0 = 26
            r1[r5] = r0
            goto L_0x0197
        L_0x06b4:
            java.lang.String r2 = "89"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x06c2
            r0 = 30
            r1[r5] = r0
            goto L_0x0197
        L_0x06c2:
            java.lang.String r2 = "029"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x06d2
            java.lang.String r2 = "91"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x06d8
        L_0x06d2:
            r0 = 21
            r1[r5] = r0
            goto L_0x0197
        L_0x06d8:
            java.lang.String r2 = "93"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x06e8
            java.lang.String r2 = "94"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x06ee
        L_0x06e8:
            r0 = 22
            r1[r5] = r0
            goto L_0x0197
        L_0x06ee:
            java.lang.String r2 = "97"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x06fc
            r0 = 23
            r1[r5] = r0
            goto L_0x0197
        L_0x06fc:
            java.lang.String r2 = "95"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x070a
            r0 = 28
            r1[r5] = r0
            goto L_0x0197
        L_0x070a:
            java.lang.String r2 = "90"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x071a
            java.lang.String r2 = "99"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0720
        L_0x071a:
            r0 = 31
            r1[r5] = r0
            goto L_0x0197
        L_0x0720:
            java.lang.String r2 = "023"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0197
            r0 = 4
            r1[r5] = r0
            goto L_0x0197
        L_0x072d:
            java.lang.String r4 = "3"
            boolean r3 = r3.endsWith(r4)
            if (r3 == 0) goto L_0x0197
            if (r2 < r8) goto L_0x0197
            r3 = 8
            java.lang.String r3 = r0.substring(r3, r8)
            java.lang.String r4 = "3"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0761
            r3 = 17
            r1[r6] = r3
        L_0x0749:
            r3 = 13
            if (r2 < r3) goto L_0x0197
            r2 = 10
            r3 = 13
            java.lang.String r0 = r0.substring(r2, r3)
            java.lang.String r2 = "010"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0795
            r1[r5] = r5
            goto L_0x0197
        L_0x0761:
            java.lang.String r4 = "4"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x076e
            r3 = 18
            r1[r6] = r3
            goto L_0x0749
        L_0x076e:
            java.lang.String r4 = "7"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x077b
            r3 = 21
            r1[r6] = r3
            goto L_0x0749
        L_0x077b:
            java.lang.String r4 = "8"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0788
            r3 = 22
            r1[r6] = r3
            goto L_0x0749
        L_0x0788:
            java.lang.String r4 = "9"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x0749
            r3 = 23
            r1[r6] = r3
            goto L_0x0749
        L_0x0795:
            java.lang.String r2 = "022"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x07a1
            r1[r5] = r7
            goto L_0x0197
        L_0x07a1:
            java.lang.String r2 = "31"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x07b1
            java.lang.String r2 = "33"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x07b5
        L_0x07b1:
            r1[r5] = r8
            goto L_0x0197
        L_0x07b5:
            java.lang.String r2 = "35"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x07c5
            java.lang.String r2 = "34"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x07cb
        L_0x07c5:
            r0 = 12
            r1[r5] = r0
            goto L_0x0197
        L_0x07cb:
            java.lang.String r2 = "47"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x07db
            java.lang.String r2 = "48"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x07e1
        L_0x07db:
            r0 = 8
            r1[r5] = r0
            goto L_0x0197
        L_0x07e1:
            java.lang.String r2 = "024"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x07f9
            java.lang.String r2 = "41"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x07f9
            java.lang.String r2 = "42"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x07fe
        L_0x07f9:
            r0 = 7
            r1[r5] = r0
            goto L_0x0197
        L_0x07fe:
            java.lang.String r2 = "43"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x080a
            r1[r5] = r9
            goto L_0x0197
        L_0x080a:
            java.lang.String r2 = "45"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x081a
            java.lang.String r2 = "46"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x081f
        L_0x081a:
            r0 = 5
            r1[r5] = r0
            goto L_0x0197
        L_0x081f:
            java.lang.String r2 = "021"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x082c
            r0 = 2
            r1[r5] = r0
            goto L_0x0197
        L_0x082c:
            java.lang.String r2 = "025"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0844
            java.lang.String r2 = "51"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0844
            java.lang.String r2 = "52"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x084a
        L_0x0844:
            r0 = 14
            r1[r5] = r0
            goto L_0x0197
        L_0x084a:
            java.lang.String r2 = "57"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x085a
            java.lang.String r2 = "58"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0860
        L_0x085a:
            r0 = 18
            r1[r5] = r0
            goto L_0x0197
        L_0x0860:
            java.lang.String r2 = "55"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0870
            java.lang.String r2 = "56"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0876
        L_0x0870:
            r0 = 13
            r1[r5] = r0
            goto L_0x0197
        L_0x0876:
            java.lang.String r2 = "59"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0884
            r0 = 19
            r1[r5] = r0
            goto L_0x0197
        L_0x0884:
            java.lang.String r2 = "79"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0894
            java.lang.String r2 = "70"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x089a
        L_0x0894:
            r0 = 15
            r1[r5] = r0
            goto L_0x0197
        L_0x089a:
            java.lang.String r2 = "53"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x08b2
            java.lang.String r2 = "54"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x08b2
            java.lang.String r2 = "63"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x08b8
        L_0x08b2:
            r0 = 11
            r1[r5] = r0
            goto L_0x0197
        L_0x08b8:
            java.lang.String r2 = "37"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x08c8
            java.lang.String r2 = "39"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x08ce
        L_0x08c8:
            r0 = 10
            r1[r5] = r0
            goto L_0x0197
        L_0x08ce:
            java.lang.String r2 = "027"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x08e6
            java.lang.String r2 = "71"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x08e6
            java.lang.String r2 = "72"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x08ec
        L_0x08e6:
            r0 = 17
            r1[r5] = r0
            goto L_0x0197
        L_0x08ec:
            java.lang.String r2 = "73"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x08fc
            java.lang.String r2 = "74"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0902
        L_0x08fc:
            r0 = 16
            r1[r5] = r0
            goto L_0x0197
        L_0x0902:
            java.lang.String r2 = "020"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x0922
            java.lang.String r2 = "75"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0922
            java.lang.String r2 = "76"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0922
            java.lang.String r2 = "66"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0928
        L_0x0922:
            r0 = 20
            r1[r5] = r0
            goto L_0x0197
        L_0x0928:
            java.lang.String r2 = "77"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0936
            r0 = 29
            r1[r5] = r0
            goto L_0x0197
        L_0x0936:
            java.lang.String r2 = "898"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0944
            r0 = 27
            r1[r5] = r0
            goto L_0x0197
        L_0x0944:
            java.lang.String r2 = "028"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x0964
            java.lang.String r2 = "81"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0964
            java.lang.String r2 = "82"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0964
            java.lang.String r2 = "83"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x096a
        L_0x0964:
            r0 = 24
            r1[r5] = r0
            goto L_0x0197
        L_0x096a:
            java.lang.String r2 = "85"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0978
            r0 = 25
            r1[r5] = r0
            goto L_0x0197
        L_0x0978:
            java.lang.String r2 = "87"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0990
            java.lang.String r2 = "88"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0990
            java.lang.String r2 = "69"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0996
        L_0x0990:
            r0 = 26
            r1[r5] = r0
            goto L_0x0197
        L_0x0996:
            java.lang.String r2 = "89"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x09a4
            r0 = 30
            r1[r5] = r0
            goto L_0x0197
        L_0x09a4:
            java.lang.String r2 = "029"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x09b4
            java.lang.String r2 = "91"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x09ba
        L_0x09b4:
            r0 = 21
            r1[r5] = r0
            goto L_0x0197
        L_0x09ba:
            java.lang.String r2 = "93"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x09ca
            java.lang.String r2 = "94"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x09d0
        L_0x09ca:
            r0 = 22
            r1[r5] = r0
            goto L_0x0197
        L_0x09d0:
            java.lang.String r2 = "97"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x09de
            r0 = 23
            r1[r5] = r0
            goto L_0x0197
        L_0x09de:
            java.lang.String r2 = "95"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x09ec
            r0 = 28
            r1[r5] = r0
            goto L_0x0197
        L_0x09ec:
            java.lang.String r2 = "90"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x09fc
            java.lang.String r2 = "99"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0a02
        L_0x09fc:
            r0 = 31
            r1[r5] = r0
            goto L_0x0197
        L_0x0a02:
            java.lang.String r2 = "023"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0197
            r0 = 4
            r1[r5] = r0
            goto L_0x0197
        L_0x0a0f:
            r0 = move-exception
            goto L_0x013d
        L_0x0a12:
            r0 = move-exception
            goto L_0x011a
        L_0x0a15:
            r0 = move-exception
            goto L_0x011a
        L_0x0a18:
            r0 = move-exception
            goto L_0x011a
        L_0x0a1b:
            r0 = move-exception
            goto L_0x011a
        L_0x0a1e:
            r0 = move-exception
            goto L_0x011a
        L_0x0a21:
            r0 = move-exception
            goto L_0x00e5
        L_0x0a24:
            r0 = move-exception
            goto L_0x00b2
        L_0x0a27:
            r0 = move-exception
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0010i.b(android.content.Context):void");
    }

    protected static void a(String str) {
        Log.e("Adwo SDK", str);
        throw new IllegalArgumentException(str);
    }

    protected static void b(String str) {
        if (str == null || str.length() != 32) {
            a("CONFIGURATION ERROR:  Incorrect Adwo publisher ID.  Should 32 [a-z,0-9] characters:  " + k);
        }
        Log.d("Adwo SDK", "Your Adwo PID is " + str);
        try {
            k = str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e2) {
            a("CONFIGURATION ERROR:  Incorrect Adwo publisher ID.  Should 32 [a-z,0-9] characters:  " + k);
        }
    }

    protected static void a(boolean z2) {
        j = z2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x023a, code lost:
        r34 = com.adwo.adsdk.C0007f.a(r34, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0241, code lost:
        if (r34 == null) goto L_0x024a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:?, code lost:
        com.adwo.adsdk.C0010i.v = (short) (com.adwo.adsdk.C0010i.v + 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x024a, code lost:
        if (r3 == null) goto L_0x024f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x024f, code lost:
        if (r36 == null) goto L_0x0254;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0251, code lost:
        r36.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0254, code lost:
        if (r35 == null) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0036, code lost:
        r36 = com.adwo.adsdk.N.a(com.adwo.adsdk.C0010i.y, com.adwo.adsdk.C0010i.z, com.adwo.adsdk.C0010i.u, r35, com.adwo.adsdk.C0010i.x, r8, com.adwo.adsdk.C0010i.k, r36, 0, com.adwo.adsdk.C0010i.l, com.adwo.adsdk.C0010i.s, com.adwo.adsdk.C0010i.t, 0, 0, (short) com.adwo.adsdk.C0010i.C, com.adwo.adsdk.C0010i.q, com.adwo.adsdk.C0010i.r, com.adwo.adsdk.C0010i.A, com.adwo.adsdk.C0010i.w, false, 0.0d, 0.0d, (byte) 0, r28, r29, com.adwo.adsdk.C0010i.v);
        r3 = null;
        com.adwo.adsdk.C0010i.f = com.adwo.adsdk.C0010i.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0256, code lost:
        r35.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:?, code lost:
        r35.setRequestProperty("Content-Length", java.lang.Integer.toString(r36.length));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:?, code lost:
        r4.write(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0275, code lost:
        r34 = r35;
        r35 = r36;
        r36 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x027d, code lost:
        android.util.Log.w("Adwo SDK", "Could not get ad from Adwo servers.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0287, code lost:
        r34 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x028b, code lost:
        r34 = r36;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0295, code lost:
        r34 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x0298, code lost:
        r36 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x0299, code lost:
        r33 = r36;
        r36 = r35;
        r35 = r34;
        r34 = r33;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x02a4, code lost:
        r33 = r35;
        r35 = r36;
        r36 = r34;
        r34 = r33;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x02ae, code lost:
        r34 = r36;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x02b2, code lost:
        r35 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0078, code lost:
        if (com.adwo.adsdk.C0010i.f != com.adwo.adsdk.C0010i.b) goto L_0x00f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x007c, code lost:
        if (com.adwo.adsdk.C0010i.j == false) goto L_0x00c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x007e, code lost:
        r0 = new java.net.URL(new java.lang.String(com.adwo.adsdk.O.d, "UTF-8"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x008f, code lost:
        r35 = (java.net.HttpURLConnection) r35.openConnection();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r35.setConnectTimeout(com.adwo.adsdk.O.a);
        r35.setReadTimeout(com.adwo.adsdk.O.a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00a5, code lost:
        if (r35 != null) goto L_0x01e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a7, code lost:
        if (r35 == null) goto L_0x00ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r35.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r0 = new java.net.URL(new java.lang.String(com.adwo.adsdk.O.c, "UTF-8"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00d8, code lost:
        r34 = null;
        r35 = null;
        r36 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        android.util.Log.w("Adwo SDK", "Could not get ad from Adwo servers,Network Error!");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00e5, code lost:
        if (r3 != null) goto L_0x00e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ea, code lost:
        if (r35 != null) goto L_0x00ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00ec, code lost:
        r35.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ef, code lost:
        if (r34 == null) goto L_0x02ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00f1, code lost:
        r34.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00f4, code lost:
        r34 = r36;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00fe, code lost:
        if (com.adwo.adsdk.C0010i.f == com.adwo.adsdk.C0010i.c) goto L_0x0109;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0107, code lost:
        if (com.adwo.adsdk.C0010i.f != com.adwo.adsdk.C0010i.d) goto L_0x017e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x010b, code lost:
        if (com.adwo.adsdk.C0010i.j == false) goto L_0x0154;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x010d, code lost:
        r0 = new java.net.URL(new java.lang.String(com.adwo.adsdk.O.f, "utf-8"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x011e, code lost:
        r35 = (java.net.HttpURLConnection) r35.openConnection();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        r35.setRequestProperty("X-Online-Host", new java.lang.String(com.adwo.adsdk.O.e, "utf-8"));
        r35.setConnectTimeout(com.adwo.adsdk.O.a * 2);
        r35.setReadTimeout(com.adwo.adsdk.O.a * 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x014d, code lost:
        r34 = r35;
        r36 = null;
        r35 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        r0 = new java.net.URL(new java.lang.String(com.adwo.adsdk.O.g, "utf-8"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0166, code lost:
        r34 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0167, code lost:
        r35 = null;
        r36 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x016b, code lost:
        if (r3 != null) goto L_0x016d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0170, code lost:
        if (r36 != null) goto L_0x0172;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0172, code lost:
        r36.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0175, code lost:
        if (r35 != null) goto L_0x0177;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0177, code lost:
        r35.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
        throw r34;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0185, code lost:
        if (com.adwo.adsdk.C0010i.f != com.adwo.adsdk.C0010i.e) goto L_0x02b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0189, code lost:
        if (com.adwo.adsdk.C0010i.j == false) goto L_0x01d6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x018b, code lost:
        r0 = new java.net.URL(new java.lang.String(com.adwo.adsdk.O.d, "UTF-8"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x019c, code lost:
        r35 = (java.net.HttpURLConnection) r35.openConnection(new java.net.Proxy(java.net.Proxy.Type.HTTP, new java.net.InetSocketAddress(new java.lang.String(com.adwo.adsdk.O.h, "utf-8"), 80)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        r35.setConnectTimeout(com.adwo.adsdk.O.a * 2);
        r35.setReadTimeout(com.adwo.adsdk.O.a * 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x01d2, code lost:
        r34 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01d3, code lost:
        r36 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        r0 = new java.net.URL(new java.lang.String(com.adwo.adsdk.O.c, "UTF-8"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        r35.setRequestMethod("POST");
        r35.setDoOutput(true);
        r35.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0204, code lost:
        if (com.adwo.adsdk.C0010i.j == false) goto L_0x025e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0206, code lost:
        r35.setRequestProperty("Content-Length", java.lang.Integer.toString(r36.length));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0217, code lost:
        r3 = r35.getOutputStream();
        r5 = com.adwo.adsdk.C0010i.j;
        r3.write(r36);
        r36 = r35.getInputStream();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
        r4 = new java.io.ByteArrayOutputStream();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x022c, code lost:
        r5 = r36.read();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0231, code lost:
        if (r5 != -1) goto L_0x0270;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0233, code lost:
        r4 = r4.toByteArray();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0238, code lost:
        if (r4.length <= 0) goto L_0x027d;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0295 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:94:0x0227] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x02ae  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e7 A[SYNTHETIC, Splitter:B:38:0x00e7] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00ec A[Catch:{ Exception -> 0x028a }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f1 A[Catch:{ Exception -> 0x028a }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x016d A[SYNTHETIC, Splitter:B:64:0x016d] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0172 A[Catch:{ Exception -> 0x0292 }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0177 A[Catch:{ Exception -> 0x0292 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static synchronized com.adwo.adsdk.C0007f a(android.content.Context r34, byte r35, byte r36) {
        /*
            java.lang.Class<com.adwo.adsdk.i> r31 = com.adwo.adsdk.C0010i.class
            monitor-enter(r31)
            java.lang.String r3 = "android.permission.INTERNET"
            r0 = r34
            r1 = r3
            int r3 = r0.checkCallingOrSelfPermission(r1)     // Catch:{ all -> 0x017b }
            r4 = -1
            if (r3 != r4) goto L_0x0014
            java.lang.String r3 = "Cannot request an ad without INTERNET permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            a(r3)     // Catch:{ all -> 0x017b }
        L_0x0014:
            r32 = 0
            boolean r8 = com.adwo.adsdk.C0010i.j     // Catch:{ all -> 0x017b }
            java.util.Set r3 = com.adwo.adsdk.C0010i.h     // Catch:{ all -> 0x017b }
            int r3 = r3.size()     // Catch:{ all -> 0x017b }
            r0 = r3
            short r0 = (short) r0     // Catch:{ all -> 0x017b }
            r28 = r0
            r0 = r28
            java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ all -> 0x017b }
            r29 = r0
            r3 = 0
            java.util.Set r4 = com.adwo.adsdk.C0010i.h     // Catch:{ all -> 0x017b }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x017b }
            r5 = r3
        L_0x0030:
            boolean r3 = r4.hasNext()     // Catch:{ all -> 0x017b }
            if (r3 != 0) goto L_0x00b0
            byte r3 = com.adwo.adsdk.C0010i.y     // Catch:{ all -> 0x017b }
            byte r4 = com.adwo.adsdk.C0010i.z     // Catch:{ all -> 0x017b }
            byte r5 = com.adwo.adsdk.C0010i.u     // Catch:{ all -> 0x017b }
            byte r7 = com.adwo.adsdk.C0010i.x     // Catch:{ all -> 0x017b }
            byte[] r9 = com.adwo.adsdk.C0010i.k     // Catch:{ all -> 0x017b }
            r11 = 0
            byte[] r12 = com.adwo.adsdk.C0010i.l     // Catch:{ all -> 0x017b }
            byte[] r13 = com.adwo.adsdk.C0010i.s     // Catch:{ all -> 0x017b }
            byte[] r14 = com.adwo.adsdk.C0010i.t     // Catch:{ all -> 0x017b }
            r15 = 0
            r16 = 0
            int r6 = com.adwo.adsdk.C0010i.C     // Catch:{ all -> 0x017b }
            r0 = r6
            short r0 = (short) r0     // Catch:{ all -> 0x017b }
            r17 = r0
            byte r18 = com.adwo.adsdk.C0010i.q     // Catch:{ all -> 0x017b }
            byte r19 = com.adwo.adsdk.C0010i.r     // Catch:{ all -> 0x017b }
            byte[] r20 = com.adwo.adsdk.C0010i.A     // Catch:{ all -> 0x017b }
            byte[] r21 = com.adwo.adsdk.C0010i.w     // Catch:{ all -> 0x017b }
            r22 = 0
            r23 = 0
            r25 = 0
            r27 = 0
            short r30 = com.adwo.adsdk.C0010i.v     // Catch:{ all -> 0x017b }
            r6 = r35
            r10 = r36
            byte[] r36 = com.adwo.adsdk.N.a(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r25, r27, r28, r29, r30)     // Catch:{ all -> 0x017b }
            r3 = 0
            r4 = 0
            r5 = 0
            int r35 = com.adwo.adsdk.C0010i.b     // Catch:{ all -> 0x017b }
            com.adwo.adsdk.C0010i.f = r35     // Catch:{ all -> 0x017b }
            int r35 = com.adwo.adsdk.C0010i.f     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            int r6 = com.adwo.adsdk.C0010i.b     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            r0 = r35
            r1 = r6
            if (r0 != r1) goto L_0x00f7
            boolean r35 = com.adwo.adsdk.C0010i.j     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            if (r35 == 0) goto L_0x00c5
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            byte[] r7 = com.adwo.adsdk.O.d     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r8 = "UTF-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            r0 = r35
            r1 = r6
            r0.<init>(r1)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
        L_0x008f:
            java.net.URLConnection r35 = r35.openConnection()     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.net.HttpURLConnection r35 = (java.net.HttpURLConnection) r35     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            int r5 = com.adwo.adsdk.O.a     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            r0 = r35
            r1 = r5
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            int r5 = com.adwo.adsdk.O.a     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            r0 = r35
            r1 = r5
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
        L_0x00a5:
            if (r35 != 0) goto L_0x01e8
            if (r35 == 0) goto L_0x00ac
            r35.disconnect()     // Catch:{ Exception -> 0x028f }
        L_0x00ac:
            r34 = 0
        L_0x00ae:
            monitor-exit(r31)
            return r34
        L_0x00b0:
            java.lang.Object r3 = r4.next()     // Catch:{ all -> 0x017b }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ all -> 0x017b }
            int r3 = r3.intValue()     // Catch:{ all -> 0x017b }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x017b }
            r29[r5] = r3     // Catch:{ all -> 0x017b }
            int r3 = r5 + 1
            r5 = r3
            goto L_0x0030
        L_0x00c5:
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            byte[] r7 = com.adwo.adsdk.O.c     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r8 = "UTF-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            r0 = r35
            r1 = r6
            r0.<init>(r1)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            goto L_0x008f
        L_0x00d7:
            r34 = move-exception
            r34 = r5
            r35 = r4
            r36 = r32
        L_0x00de:
            java.lang.String r4 = "Adwo SDK"
            java.lang.String r5 = "Could not get ad from Adwo servers,Network Error!"
            android.util.Log.w(r4, r5)     // Catch:{ all -> 0x0298 }
            if (r3 == 0) goto L_0x00ea
            r3.close()     // Catch:{ Exception -> 0x028a }
        L_0x00ea:
            if (r35 == 0) goto L_0x00ef
            r35.close()     // Catch:{ Exception -> 0x028a }
        L_0x00ef:
            if (r34 == 0) goto L_0x02ae
            r34.disconnect()     // Catch:{ Exception -> 0x028a }
            r34 = r36
            goto L_0x00ae
        L_0x00f7:
            int r35 = com.adwo.adsdk.C0010i.f     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            int r6 = com.adwo.adsdk.C0010i.c     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            r0 = r35
            r1 = r6
            if (r0 == r1) goto L_0x0109
            int r35 = com.adwo.adsdk.C0010i.f     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            int r6 = com.adwo.adsdk.C0010i.d     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            r0 = r35
            r1 = r6
            if (r0 != r1) goto L_0x017e
        L_0x0109:
            boolean r35 = com.adwo.adsdk.C0010i.j     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            if (r35 == 0) goto L_0x0154
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            byte[] r7 = com.adwo.adsdk.O.f     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r8 = "utf-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            r0 = r35
            r1 = r6
            r0.<init>(r1)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
        L_0x011e:
            java.net.URLConnection r35 = r35.openConnection()     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.net.HttpURLConnection r35 = (java.net.HttpURLConnection) r35     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r5 = "X-Online-Host"
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            byte[] r7 = com.adwo.adsdk.O.e     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            java.lang.String r8 = "utf-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            r0 = r35
            r1 = r5
            r2 = r6
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            int r5 = com.adwo.adsdk.O.a     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            int r5 = r5 * 2
            r0 = r35
            r1 = r5
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            int r5 = com.adwo.adsdk.O.a     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            int r5 = r5 * 2
            r0 = r35
            r1 = r5
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            goto L_0x00a5
        L_0x014c:
            r34 = move-exception
            r34 = r35
            r36 = r32
            r35 = r4
            goto L_0x00de
        L_0x0154:
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            byte[] r7 = com.adwo.adsdk.O.g     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r8 = "utf-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            r0 = r35
            r1 = r6
            r0.<init>(r1)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            goto L_0x011e
        L_0x0166:
            r34 = move-exception
            r35 = r5
            r36 = r4
        L_0x016b:
            if (r3 == 0) goto L_0x0170
            r3.close()     // Catch:{ Exception -> 0x0292 }
        L_0x0170:
            if (r36 == 0) goto L_0x0175
            r36.close()     // Catch:{ Exception -> 0x0292 }
        L_0x0175:
            if (r35 == 0) goto L_0x017a
            r35.disconnect()     // Catch:{ Exception -> 0x0292 }
        L_0x017a:
            throw r34     // Catch:{ all -> 0x017b }
        L_0x017b:
            r34 = move-exception
            monitor-exit(r31)
            throw r34
        L_0x017e:
            int r35 = com.adwo.adsdk.C0010i.f     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            int r6 = com.adwo.adsdk.C0010i.e     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            r0 = r35
            r1 = r6
            if (r0 != r1) goto L_0x02b2
            boolean r35 = com.adwo.adsdk.C0010i.j     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            if (r35 == 0) goto L_0x01d6
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            byte[] r7 = com.adwo.adsdk.O.d     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r8 = "UTF-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            r0 = r35
            r1 = r6
            r0.<init>(r1)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
        L_0x019c:
            java.net.Proxy r6 = new java.net.Proxy     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.net.Proxy$Type r7 = java.net.Proxy.Type.HTTP     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.net.InetSocketAddress r8 = new java.net.InetSocketAddress     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r9 = new java.lang.String     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            byte[] r10 = com.adwo.adsdk.O.h     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r11 = "utf-8"
            r9.<init>(r10, r11)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            r10 = 80
            r8.<init>(r9, r10)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            r0 = r35
            r1 = r6
            java.net.URLConnection r35 = r0.openConnection(r1)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.net.HttpURLConnection r35 = (java.net.HttpURLConnection) r35     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            int r5 = com.adwo.adsdk.O.a     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            int r5 = r5 * 2
            r0 = r35
            r1 = r5
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            int r5 = com.adwo.adsdk.O.a     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            int r5 = r5 * 2
            r0 = r35
            r1 = r5
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            goto L_0x00a5
        L_0x01d2:
            r34 = move-exception
            r36 = r4
            goto L_0x016b
        L_0x01d6:
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            byte[] r7 = com.adwo.adsdk.O.c     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            java.lang.String r8 = "UTF-8"
            r6.<init>(r7, r8)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            r0 = r35
            r1 = r6
            r0.<init>(r1)     // Catch:{ Exception -> 0x00d7, all -> 0x0166 }
            goto L_0x019c
        L_0x01e8:
            java.lang.String r5 = "POST"
            r0 = r35
            r1 = r5
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            r5 = 1
            r0 = r35
            r1 = r5
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            java.lang.String r5 = "Content-Type"
            java.lang.String r6 = "application/x-www-form-urlencoded"
            r0 = r35
            r1 = r5
            r2 = r6
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            boolean r5 = com.adwo.adsdk.C0010i.j     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            if (r5 == 0) goto L_0x025e
            java.lang.String r5 = "Content-Length"
            r0 = r36
            int r0 = r0.length     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            r6 = r0
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            r0 = r35
            r1 = r5
            r2 = r6
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
        L_0x0217:
            java.io.OutputStream r3 = r35.getOutputStream()     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            boolean r5 = com.adwo.adsdk.C0010i.j     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            r0 = r3
            r1 = r36
            r0.write(r1)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            java.io.InputStream r36 = r35.getInputStream()     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0274, all -> 0x0295 }
            r4.<init>()     // Catch:{ Exception -> 0x0274, all -> 0x0295 }
        L_0x022c:
            int r5 = r36.read()     // Catch:{ Exception -> 0x0274, all -> 0x0295 }
            r6 = -1
            if (r5 != r6) goto L_0x0270
            byte[] r4 = r4.toByteArray()     // Catch:{ Exception -> 0x0274, all -> 0x0295 }
            int r5 = r4.length     // Catch:{ Exception -> 0x0274, all -> 0x0295 }
            if (r5 <= 0) goto L_0x027d
            r0 = r34
            r1 = r4
            com.adwo.adsdk.f r34 = com.adwo.adsdk.C0007f.a(r0, r1)     // Catch:{ Exception -> 0x0274, all -> 0x0295 }
            if (r34 == 0) goto L_0x024a
            short r4 = com.adwo.adsdk.C0010i.v     // Catch:{ Exception -> 0x02a3, all -> 0x0295 }
            int r4 = r4 + 1
            short r4 = (short) r4     // Catch:{ Exception -> 0x02a3, all -> 0x0295 }
            com.adwo.adsdk.C0010i.v = r4     // Catch:{ Exception -> 0x02a3, all -> 0x0295 }
        L_0x024a:
            if (r3 == 0) goto L_0x024f
            r3.close()     // Catch:{ Exception -> 0x025b }
        L_0x024f:
            if (r36 == 0) goto L_0x0254
            r36.close()     // Catch:{ Exception -> 0x025b }
        L_0x0254:
            if (r35 == 0) goto L_0x00ae
            r35.disconnect()     // Catch:{ Exception -> 0x025b }
            goto L_0x00ae
        L_0x025b:
            r35 = move-exception
            goto L_0x00ae
        L_0x025e:
            java.lang.String r5 = "Content-Length"
            r0 = r36
            int r0 = r0.length     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            r6 = r0
            java.lang.String r6 = java.lang.Integer.toString(r6)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            r0 = r35
            r1 = r5
            r2 = r6
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x014c, all -> 0x01d2 }
            goto L_0x0217
        L_0x0270:
            r4.write(r5)     // Catch:{ Exception -> 0x0274, all -> 0x0295 }
            goto L_0x022c
        L_0x0274:
            r34 = move-exception
            r34 = r35
            r35 = r36
            r36 = r32
            goto L_0x00de
        L_0x027d:
            java.lang.String r34 = "Adwo SDK"
            java.lang.String r4 = "Could not get ad from Adwo servers."
            r0 = r34
            r1 = r4
            android.util.Log.w(r0, r1)     // Catch:{ Exception -> 0x0274, all -> 0x0295 }
            r34 = r32
            goto L_0x024a
        L_0x028a:
            r34 = move-exception
            r34 = r36
            goto L_0x00ae
        L_0x028f:
            r34 = move-exception
            goto L_0x00ac
        L_0x0292:
            r35 = move-exception
            goto L_0x017a
        L_0x0295:
            r34 = move-exception
            goto L_0x016b
        L_0x0298:
            r36 = move-exception
            r33 = r36
            r36 = r35
            r35 = r34
            r34 = r33
            goto L_0x016b
        L_0x02a3:
            r4 = move-exception
            r33 = r35
            r35 = r36
            r36 = r34
            r34 = r33
            goto L_0x00de
        L_0x02ae:
            r34 = r36
            goto L_0x00ae
        L_0x02b2:
            r35 = r5
            goto L_0x00a5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0010i.a(android.content.Context, byte, byte):com.adwo.adsdk.f");
    }

    protected static byte a(int i2, int i3, boolean z2) {
        if (i2 < 240 || i2 >= 320) {
            if (i2 < 320 || i2 >= 480) {
                if (i2 < 480 || i2 >= 640) {
                    if (i2 < 640 || i2 >= 720) {
                        if (i2 < 720 || i2 > 1024) {
                            if (i2 > 1024 && i3 > 768) {
                                if (z2) {
                                    return 4;
                                }
                                return 9;
                            }
                        } else if (i3 >= 320 || i3 < 240) {
                            if (i3 < 320 || i3 >= 640) {
                                if (i3 >= 640) {
                                    if (z2) {
                                        return 3;
                                    }
                                    return 9;
                                }
                            } else if (z2) {
                                return 3;
                            } else {
                                return 9;
                            }
                        } else if (z2) {
                            return 3;
                        } else {
                            return 9;
                        }
                    } else if (i3 >= 320 || i3 < 240) {
                        if (i3 < 320 || i3 >= 640) {
                            if (i3 >= 640) {
                                if (z2) {
                                    return 3;
                                }
                                return 8;
                            }
                        } else if (z2) {
                            return 3;
                        } else {
                            return 8;
                        }
                    } else if (z2) {
                        return 1;
                    } else {
                        return 8;
                    }
                } else if (i3 >= 320 || i3 < 240) {
                    if (i3 < 320 || i3 >= 640) {
                        if (i3 >= 640) {
                            if (z2) {
                                return 2;
                            }
                            return 7;
                        }
                    } else if (z2) {
                        return 2;
                    } else {
                        return 7;
                    }
                } else if (z2) {
                    return 1;
                } else {
                    return 6;
                }
            } else if (i3 >= 320 || i3 < 240) {
                if (i3 < 320 || i3 >= 640) {
                    if (i3 >= 640) {
                        if (z2) {
                            return 8;
                        }
                        return 8;
                    }
                } else if (z2) {
                    return 1;
                } else {
                    return 6;
                }
            } else if (z2) {
                return 1;
            } else {
                return 6;
            }
        } else if (i3 >= 320 || i3 < 240) {
            if (i3 >= 320 && i3 < 480) {
                if (z2) {
                    return 1;
                }
                return 6;
            }
        } else if (z2) {
            return 5;
        } else {
            return 5;
        }
        return !z2 ? (byte) 6 : 1;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String c(android.content.Context r4) {
        /*
            java.lang.Class<android.webkit.WebSettings> r0 = android.webkit.WebSettings.class
            r1 = 2
            java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x0038 }
            r2 = 0
            java.lang.Class<android.content.Context> r3 = android.content.Context.class
            r1[r2] = r3     // Catch:{ Exception -> 0x0038 }
            r2 = 1
            java.lang.Class<android.webkit.WebView> r3 = android.webkit.WebView.class
            r1[r2] = r3     // Catch:{ Exception -> 0x0038 }
            java.lang.reflect.Constructor r1 = r0.getDeclaredConstructor(r1)     // Catch:{ Exception -> 0x0038 }
            r0 = 1
            r1.setAccessible(r0)     // Catch:{ Exception -> 0x0038 }
            r0 = 2
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0032 }
            r2 = 0
            r0[r2] = r4     // Catch:{ all -> 0x0032 }
            r2 = 1
            r3 = 0
            r0[r2] = r3     // Catch:{ all -> 0x0032 }
            java.lang.Object r0 = r1.newInstance(r0)     // Catch:{ all -> 0x0032 }
            android.webkit.WebSettings r0 = (android.webkit.WebSettings) r0     // Catch:{ all -> 0x0032 }
            java.lang.String r0 = r0.getUserAgentString()     // Catch:{ all -> 0x0032 }
            com.adwo.adsdk.C0010i.g = r0     // Catch:{ all -> 0x0032 }
            r2 = 0
            r1.setAccessible(r2)     // Catch:{ Exception -> 0x0038 }
        L_0x0031:
            return r0
        L_0x0032:
            r0 = move-exception
            r2 = 0
            r1.setAccessible(r2)     // Catch:{ Exception -> 0x0038 }
            throw r0     // Catch:{ Exception -> 0x0038 }
        L_0x0038:
            r0 = move-exception
            android.webkit.WebView r0 = new android.webkit.WebView
            r0.<init>(r4)
            android.webkit.WebSettings r0 = r0.getSettings()
            java.lang.String r0 = r0.getUserAgentString()
            com.adwo.adsdk.C0010i.g = r0
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0010i.c(android.content.Context):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0046 A[SYNTHETIC, Splitter:B:16:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0052 A[SYNTHETIC, Splitter:B:22:0x0052] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void c(java.lang.String r6) {
        /*
            r3 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0041, all -> 0x004d }
            r0.<init>(r6)     // Catch:{ Exception -> 0x0041, all -> 0x004d }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0041, all -> 0x004d }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0041, all -> 0x004d }
            java.lang.String r1 = "GET"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            int r1 = com.adwo.adsdk.O.a     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            java.lang.String r1 = "User-Agent"
            java.lang.String r2 = com.adwo.adsdk.C0010i.g     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            r0.connect()     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x0069, all -> 0x005f }
            java.lang.String r2 = "adwo"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            java.lang.String r4 = "connect to beacon: "
            r3.<init>(r4)     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            android.util.Log.v(r2, r3)     // Catch:{ Exception -> 0x006d, all -> 0x0064 }
            if (r1 == 0) goto L_0x003d
            r1.close()     // Catch:{ IOException -> 0x005d }
        L_0x003d:
            r0.disconnect()
        L_0x0040:
            return
        L_0x0041:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x0044:
            if (r0 == 0) goto L_0x0049
            r0.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0049:
            r1.disconnect()
            goto L_0x0040
        L_0x004d:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()     // Catch:{ IOException -> 0x005b }
        L_0x0055:
            r2.disconnect()
            throw r0
        L_0x0059:
            r0 = move-exception
            goto L_0x0049
        L_0x005b:
            r1 = move-exception
            goto L_0x0055
        L_0x005d:
            r1 = move-exception
            goto L_0x003d
        L_0x005f:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0050
        L_0x0064:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x0050
        L_0x0069:
            r1 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x0044
        L_0x006d:
            r2 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.C0010i.c(java.lang.String):void");
    }
}
