package com.adwo.adsdk;

import android.app.Activity;
import android.content.Context;

final class L implements Runnable {
    private Context a;
    private /* synthetic */ G b;

    public L(G g, Context context) {
        this.b = g;
        this.a = context;
    }

    public final void run() {
        this.b.h = C0010i.a(this.a, G.c, (byte) 3);
        if (this.b.h != null) {
            this.b.a();
            return;
        }
        if (this.b.o != null) {
            this.b.o.b();
        }
        ((Activity) this.a).finish();
    }
}
