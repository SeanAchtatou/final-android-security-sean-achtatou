package com.waps;

import android.content.Intent;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class r extends WebViewClient {
    final /* synthetic */ OffersWebView a;

    private r(OffersWebView offersWebView) {
        this.a = offersWebView;
    }

    /* synthetic */ r(OffersWebView offersWebView, q qVar) {
        this(offersWebView);
    }

    public void onPageFinished(WebView webView, String str) {
        this.a.e.setVisibility(8);
        super.onPageFinished(webView, str);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        this.a.e.setVisibility(0);
        super.onPageStarted(webView, str, bitmap);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        webView.setScrollBarStyle(0);
        webView.loadDataWithBaseURL("", "<html><body bgcolor=\"000000\" align=\"center\"><br/><font color=\"ffffff\">网络链接失败，请检查网络。</font><br/></body></html>", "text/html", "utf-8", "");
        boolean unused = this.a.m = false;
        super.onReceivedError(webView, i, str, str2);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String substring;
        Intent intent;
        String unused = this.a.l = str;
        if (str.startsWith("load://")) {
            if (str.contains(";http://")) {
                substring = str.substring("load://".length(), str.indexOf(";"));
                this.a.a = str.substring(str.indexOf(";") + 1);
            } else {
                substring = str.substring("load://".length());
            }
            if (substring != "") {
                try {
                    Intent launchIntentForPackage = this.a.getPackageManager().getLaunchIntentForPackage(substring);
                    try {
                        AppConnect.getInstanceNoConnect(this.a).package_receiver(substring, 1);
                        intent = launchIntentForPackage;
                    } catch (Exception e) {
                        intent = launchIntentForPackage;
                    }
                } catch (Exception e2) {
                    intent = null;
                }
                if (intent != null) {
                    this.a.startActivity(intent);
                    return true;
                }
                webView.loadUrl(this.a.a);
                return true;
            }
        }
        return false;
    }
}
