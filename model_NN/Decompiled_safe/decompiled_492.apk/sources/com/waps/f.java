package com.waps;

import android.os.AsyncTask;

class f extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private f(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ f(AppConnect appConnect, d dVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = AppConnect.x.a("http://app.wapx.cn/action/account/getinfo?", this.a.N);
        if (a2 != null) {
            z = this.a.handleGetPointsResponse(a2);
        }
        if (!z) {
            AppConnect.Z.getUpdatePointsFailed("无法更新积分");
        }
        return Boolean.valueOf(z);
    }
}
