package com.waps;

import android.webkit.DownloadListener;
import android.widget.Toast;

class q implements DownloadListener {
    final /* synthetic */ OffersWebView a;

    q(OffersWebView offersWebView) {
        this.a = offersWebView;
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        Toast.makeText(this.a, "正在下载,请稍候...", 0).show();
        this.a.b = new k(this.a, this.a.c, this.a.l);
        this.a.b.execute(this.a.l);
        if (this.a.k != null && "true".equals(this.a.k)) {
            this.a.finish();
        }
    }
}
