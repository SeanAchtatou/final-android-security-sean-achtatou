package com.waps.ads;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.waps.n;
import java.io.IOException;
import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

class e implements Runnable {
    private String a;

    public e(String str) {
        this.a = str;
    }

    public void run() {
        String str;
        HttpHost httpHost;
        Log.d("AdGroup_SDK", "Pinging URL: " + this.a);
        n nVar = new n();
        Activity access$500 = AdGroupLayout.x;
        Activity unused = AdGroupLayout.x;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) access$500.getSystemService("connectivity")).getActiveNetworkInfo();
        if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().equals("cmwap"))) {
            nVar.a(true);
        }
        if (!nVar.a()) {
            try {
                new DefaultHttpClient().execute(new HttpGet(this.a));
            } catch (ClientProtocolException e) {
                Log.e("AdGroup_SDK", "Caught ClientProtocolException in PingUrlRunnable", e);
            } catch (IOException e2) {
                Log.e("AdGroup_SDK", "Caught IOException in PingUrlRunnable", e2);
            }
        } else {
            String str2 = this.a;
            HttpHost httpHost2 = new HttpHost("10.0.0.172", 80, "http");
            if (str2.startsWith("http://app")) {
                HttpHost httpHost3 = new HttpHost("ads.wapx.cn", 80, "http");
                str = str2.replaceAll(" ", "%20").replaceFirst("http://app.wapx.cn", "");
                httpHost = httpHost3;
            } else if (str2.startsWith("http://ads")) {
                HttpHost httpHost4 = new HttpHost("ads.wapx.cn", 80, "http");
                str = str2.replaceAll(" ", "%20").replaceFirst("http://ads.wapx.cn", "");
                httpHost = httpHost4;
            } else {
                str = str2;
                httpHost = null;
            }
            this.a = str;
            HttpGet httpGet = new HttpGet(this.a);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost2);
            try {
                defaultHttpClient.execute(httpHost, httpGet);
            } catch (ClientProtocolException e3) {
                Log.e("AdGroup_SDK", "Caught ClientProtocolException in PingUrlRunnable", e3);
            } catch (IOException e4) {
                Log.e("AdGroup_SDK", "Caught IOException in PingUrlRunnable", e4);
            }
        }
    }
}
