package com.waps.ads.adapters;

import android.app.Activity;
import android.view.Display;
import android.view.ViewGroup;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.AdView;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.AdGroupTargeting;
import com.waps.ads.a.a;
import com.waps.ads.b.c;
import com.waps.ads.g;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;

public class AdchinaAdapter extends a {
    public AdchinaAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void
     arg types: [com.waps.ads.AdGroupLayout, com.adchina.android.ads.AdView]
     candidates:
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.View):void
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void */
    public void handle() {
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
        if (adGroupLayout != null) {
            Display defaultDisplay = ((Activity) adGroupLayout.a.get()).getWindowManager().getDefaultDisplay();
            AdManager.setResolution(String.valueOf(defaultDisplay.getWidth()) + TMXConstants.TAG_OBJECT_ATTRIBUTE_X + defaultDisplay.getHeight());
            AdManager.setAdspaceId(this.d.e);
            AdManager.setDebugMode(AdGroupTargeting.getTestMode());
            AdView adView = new AdView(adGroupLayout.getContext());
            adView.start();
            adGroupLayout.j.resetRollover();
            adGroupLayout.b.post(new g(adGroupLayout, (ViewGroup) adView));
            adGroupLayout.rotateThreadedDelayed();
        }
    }
}
