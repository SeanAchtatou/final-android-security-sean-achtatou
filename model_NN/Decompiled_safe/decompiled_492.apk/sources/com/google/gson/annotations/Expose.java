package com.google.gson.annotations;

public @interface Expose {
    boolean deserialize();

    boolean serialize();
}
