package com.cmdu.app;

public class AI {
    private final int[] a = {1732584193, -271733879, -1732584194, 271733878, -1009589776};
    private int[] b = new int[5];
    private int[] c = new int[80];

    private int b(byte[] paramArrayOfByte) {
        System.arraycopy(this.a, 0, this.b, 0, this.a.length);
        byte[] arrayOfByte = c(paramArrayOfByte);
        int i = arrayOfByte.length / 64;
        for (int j = 0; j < i; j++) {
            for (int k = 0; k < 16; k++) {
                this.c[k] = a(arrayOfByte, (j * 64) + (k * 4));
            }
            a();
        }
        return 20;
    }

    private byte[] c(byte[] paramArrayOfByte) {
        int i;
        int j;
        int k = paramArrayOfByte.length;
        int m = k % 64;
        if (m < 56) {
            i = 55 - m;
            j = (k - m) + 64;
        } else if (m == 56) {
            i = 63;
            j = k + 8 + 64;
        } else {
            i = (63 - m) + 56;
            j = ((k + 64) - m) + 64;
        }
        byte[] arrayOfByte = new byte[j];
        System.arraycopy(paramArrayOfByte, 0, arrayOfByte, 0, k);
        int n = k;
        int n2 = n + 1;
        arrayOfByte[n] = Byte.MIN_VALUE;
        int i1 = 0;
        while (i1 < i) {
            arrayOfByte[n2] = 0;
            i1++;
            n2++;
        }
        long l = ((long) k) * 8;
        int n3 = n2 + 1;
        arrayOfByte[n2] = (byte) ((int) (l >> 56));
        int n4 = n3 + 1;
        arrayOfByte[n3] = (byte) ((int) ((l >> 48) & 255));
        int n5 = n4 + 1;
        arrayOfByte[n4] = (byte) ((int) ((l >> 40) & 255));
        int n6 = n5 + 1;
        arrayOfByte[n5] = (byte) ((int) ((l >> 32) & 255));
        int n7 = n6 + 1;
        arrayOfByte[n6] = (byte) ((int) ((l >> 24) & 255));
        int n8 = n7 + 1;
        arrayOfByte[n7] = (byte) ((int) ((l >> 16) & 255));
        int n9 = n8 + 1;
        arrayOfByte[n8] = (byte) ((int) ((l >> 8) & 255));
        int i2 = n9 + 1;
        arrayOfByte[n9] = (byte) ((int) (255 & l));
        return arrayOfByte;
    }

    private int a(int paramInt1, int paramInt2, int paramInt3) {
        return (paramInt1 & paramInt2) | ((paramInt1 ^ -1) & paramInt3);
    }

    private int b(int paramInt1, int paramInt2, int paramInt3) {
        return (paramInt1 ^ paramInt2) ^ paramInt3;
    }

    private int c(int paramInt1, int paramInt2, int paramInt3) {
        return (paramInt1 & paramInt2) | (paramInt1 & paramInt3) | (paramInt2 & paramInt3);
    }

    private int a(int paramInt1, int paramInt2) {
        return (paramInt1 << paramInt2) | (paramInt1 >>> (32 - paramInt2));
    }

    private void a() {
        for (int i = 16; i <= 79; i++) {
            this.c[i] = a(((this.c[i - 3] ^ this.c[i - 8]) ^ this.c[i - 14]) ^ this.c[i - 16], 1);
        }
        int[] arrayOfInt = new int[5];
        for (int j = 0; j < arrayOfInt.length; j++) {
            arrayOfInt[j] = this.b[j];
        }
        for (int j2 = 0; j2 <= 19; j2++) {
            int k = a(arrayOfInt[0], 5) + a(arrayOfInt[1], arrayOfInt[2], arrayOfInt[3]) + arrayOfInt[4] + this.c[j2] + 1518500249;
            arrayOfInt[4] = arrayOfInt[3];
            arrayOfInt[3] = arrayOfInt[2];
            arrayOfInt[2] = a(arrayOfInt[1], 30);
            arrayOfInt[1] = arrayOfInt[0];
            arrayOfInt[0] = k;
        }
        for (int j3 = 20; j3 <= 39; j3++) {
            int k2 = a(arrayOfInt[0], 5) + b(arrayOfInt[1], arrayOfInt[2], arrayOfInt[3]) + arrayOfInt[4] + this.c[j3] + 1859775393;
            arrayOfInt[4] = arrayOfInt[3];
            arrayOfInt[3] = arrayOfInt[2];
            arrayOfInt[2] = a(arrayOfInt[1], 30);
            arrayOfInt[1] = arrayOfInt[0];
            arrayOfInt[0] = k2;
        }
        for (int j4 = 40; j4 <= 59; j4++) {
            int k3 = (((a(arrayOfInt[0], 5) + c(arrayOfInt[1], arrayOfInt[2], arrayOfInt[3])) + arrayOfInt[4]) + this.c[j4]) - 1894007588;
            arrayOfInt[4] = arrayOfInt[3];
            arrayOfInt[3] = arrayOfInt[2];
            arrayOfInt[2] = a(arrayOfInt[1], 30);
            arrayOfInt[1] = arrayOfInt[0];
            arrayOfInt[0] = k3;
        }
        for (int j5 = 60; j5 <= 79; j5++) {
            int k4 = (((a(arrayOfInt[0], 5) + b(arrayOfInt[1], arrayOfInt[2], arrayOfInt[3])) + arrayOfInt[4]) + this.c[j5]) - 899497514;
            arrayOfInt[4] = arrayOfInt[3];
            arrayOfInt[3] = arrayOfInt[2];
            arrayOfInt[2] = a(arrayOfInt[1], 30);
            arrayOfInt[1] = arrayOfInt[0];
            arrayOfInt[0] = k4;
        }
        for (int j6 = 0; j6 < arrayOfInt.length; j6++) {
            int[] iArr = this.b;
            iArr[j6] = iArr[j6] + arrayOfInt[j6];
        }
        for (int j7 = 0; j7 < this.c.length; j7++) {
            this.c[j7] = 0;
        }
    }

    private int a(byte[] paramArrayOfByte, int paramInt) {
        return ((paramArrayOfByte[paramInt] & 255) << 24) | ((paramArrayOfByte[paramInt + 1] & 255) << 16) | ((paramArrayOfByte[paramInt + 2] & 255) << 8) | (paramArrayOfByte[paramInt + 3] & 255);
    }

    private void a(int paramInt1, byte[] paramArrayOfByte, int paramInt2) {
        paramArrayOfByte[paramInt2] = (byte) (paramInt1 >>> 24);
        paramArrayOfByte[paramInt2 + 1] = (byte) (paramInt1 >>> 16);
        paramArrayOfByte[paramInt2 + 2] = (byte) (paramInt1 >>> 8);
        paramArrayOfByte[paramInt2 + 3] = (byte) paramInt1;
    }

    /* access modifiers changed from: package-private */
    public byte[] a(byte[] paramArrayOfByte) {
        b(paramArrayOfByte);
        byte[] arrayOfByte = new byte[20];
        for (int i = 0; i < this.b.length; i++) {
            a(this.b[i], arrayOfByte, i * 4);
        }
        return arrayOfByte;
    }
}
