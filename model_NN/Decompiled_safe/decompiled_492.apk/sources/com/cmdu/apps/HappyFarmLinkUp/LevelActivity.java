package com.cmdu.apps.HappyFarmLinkUp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import com.waps.AppConnect;
import java.util.ArrayList;
import java.util.Iterator;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.SpriteBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.level.util.constants.LevelConstants;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.HorizontalAlign;

public class LevelActivity extends BaseActivity {
    private int CAMERA_HEIGHT = 480;
    private int CAMERA_WIDTH = 720;
    private ArrayList<Sprite> levelSpriteList = new ArrayList<>();
    private int levels = 18;
    private TextureRegion mBackgroundTextureRegion;
    private BitmapTextureAtlas mBitmapTexture;
    private Camera mCamera;
    private Font mFont;
    private BitmapTextureAtlas mFontTexture;
    private TextureRegion mLevelLocked;
    private TextureRegion mLevelOn;
    private TextureRegion mLevelStar;
    private Scene mScene;
    /* access modifiers changed from: private */
    public Store store;

    public Engine onLoadEngine() {
        this.mCamera = new Camera(0.0f, 0.0f, (float) this.CAMERA_WIDTH, (float) this.CAMERA_HEIGHT);
        return new Engine(new EngineOptions(true, EngineOptions.ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy((float) this.CAMERA_WIDTH, (float) this.CAMERA_HEIGHT), this.mCamera).setNeedsSound(true).setNeedsMusic(true));
    }

    public void onLoadResources() {
        this.mBitmapTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_BUMPMAP, (int) PVRTexture.FLAG_BUMPMAP, TextureOptions.DEFAULT);
        this.mBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "bg.jpg", 0, 0);
        this.mLevelOn = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "level_on.png", 0, 480);
        this.mLevelLocked = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "level_locked.png", 60, 480);
        this.mLevelStar = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "level_star.png", 120, 480);
        this.mEngine.getTextureManager().loadTexture(this.mBitmapTexture);
        this.mFontTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFont = FontFactory.createFromAsset(this.mFontTexture, this, "font/droid.ttf", 28.0f, true, -256);
        this.mEngine.getTextureManager().loadTexture(this.mFontTexture);
        this.mEngine.getFontManager().loadFont(this.mFont);
    }

    public Scene onLoadScene() {
        this.mScene = new Scene();
        this.mScene.setBackground(new SpriteBackground(new Sprite(0.0f, 0.0f, (float) this.CAMERA_WIDTH, (float) this.CAMERA_HEIGHT, this.mBackgroundTextureRegion)));
        this.store = new Store(this);
        return this.mScene;
    }

    public void onResumeGame() {
        TextureRegion textureRegion;
        if (this.levelSpriteList != null && this.levelSpriteList.size() > 0) {
            Iterator<Sprite> it = this.levelSpriteList.iterator();
            while (it.hasNext()) {
                this.mScene.detachChild(it.next());
            }
        }
        this.mScene.clearTouchAreas();
        for (int i = 0; i < this.levels; i++) {
            final int level = i + 1;
            float f = (float) (((i % 6) * 60) + ((i % 6) * 20) + 125);
            float ceil = (float) ((Math.ceil((double) (i / 6)) * 90.0d) + 100.0d);
            if (level == 1 || this.store.getInt("level_" + (level - 1)) == 1) {
                textureRegion = this.mLevelOn.clone();
            } else {
                textureRegion = this.mLevelLocked.clone();
            }
            Sprite levelSprite = new Sprite(f, ceil, textureRegion) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    int i;
                    int i2;
                    if (!Global.isWaps(LevelActivity.this) || LevelActivity.this.store.getInt("buy_" + level) != 0) {
                        if (level == 1 || LevelActivity.this.store.getInt("level_" + (level - 1)) == 1) {
                            Intent intent = new Intent(LevelActivity.this, GameActivity.class);
                            intent.putExtra(LevelConstants.TAG_LEVEL, level);
                            intent.setFlags(67108864);
                            LevelActivity.this.startActivity(intent);
                        }
                        return false;
                    } else if (LevelActivity.this.point < LevelActivity.this.needPoint) {
                        AlertDialog.Builder title = new AlertDialog.Builder(LevelActivity.this).setTitle("积分不足");
                        StringBuilder sb = new StringBuilder("请先激活当前关卡，永久开启此关卡需要 ");
                        if (level > 1) {
                            i2 = LevelActivity.this.levelPoint;
                        } else {
                            i2 = LevelActivity.this.needPoint;
                        }
                        title.setMessage(sb.append(i2).append(" 积分，你当前积分余额为：").append(LevelActivity.this.point).append(" ，您可以通过下载安装精品应用 ‘免费赚积分’。为确保积分到账，安装后请启动一次！").toString()).setPositiveButton("免费赚取积分", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                                AppConnect.getInstance(LevelActivity.this).showOffers(LevelActivity.this);
                            }
                        }).show();
                        return false;
                    } else {
                        LevelActivity.this.point -= level > 1 ? LevelActivity.this.levelPoint : LevelActivity.this.needPoint;
                        AppConnect instance = AppConnect.getInstance(LevelActivity.this);
                        if (level > 1) {
                            i = LevelActivity.this.levelPoint;
                        } else {
                            i = LevelActivity.this.needPoint;
                        }
                        instance.spendPoints(i, LevelActivity.this);
                        LevelActivity.this.store.set("buy_" + level, 1);
                        new AlertDialog.Builder(LevelActivity.this).setTitle("激活关卡成功").setMessage("恭喜您，你已经成功激活此关卡，重新点击进入游戏吧！").setPositiveButton("返回", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                            }
                        }).show();
                        return false;
                    }
                }
            };
            if (level == 1 || this.store.getInt("level_" + (level - 1)) == 1) {
                levelSprite.attachChild(new Text(13.0f, 15.0f, this.mFont, String.valueOf(level < 10 ? "0" : "") + level, HorizontalAlign.CENTER));
            }
            int score = this.store.getInt("score_" + level);
            int star = 0;
            if (score >= 800) {
                star = 3;
            } else if (score >= 550) {
                star = 2;
            } else if (score > 0) {
                star = 1;
            }
            for (int j = 0; j < star; j++) {
                levelSprite.attachChild(new Sprite((float) ((j * 15) + 7), 60.0f, this.mLevelStar.clone()));
            }
            this.mScene.attachChild(levelSprite);
            this.mScene.registerTouchArea(levelSprite);
            this.levelSpriteList.add(levelSprite);
        }
    }
}
