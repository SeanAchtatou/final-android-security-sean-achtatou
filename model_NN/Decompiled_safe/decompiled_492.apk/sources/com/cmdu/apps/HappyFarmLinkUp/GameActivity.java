package com.cmdu.apps.HappyFarmLinkUp;

import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.entity.modifier.LoopEntityModifier;
import org.anddev.andengine.entity.modifier.PathModifier;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.SpriteBackground;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.level.util.constants.LevelConstants;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.modifier.ease.EaseSineInOut;

public class GameActivity extends BaseActivity {
    private int CAMERA_HEIGHT = 480;
    private int CAMERA_WIDTH = 720;
    private final int ONE_HEIGHT = 55;
    private final int ONE_WIDTH = 55;
    private AnimatedSprite SoundSprite;
    /* access modifiers changed from: private */
    public int clockNum = 1;
    private AnimatedSprite clockSprite;
    /* access modifiers changed from: private */
    public int combo = 0;
    /* access modifiers changed from: private */
    public int gameTime = 90;
    /* access modifiers changed from: private */
    public boolean isSound = true;
    /* access modifiers changed from: private */
    public int level = 0;
    private List<Sprite> listSprite;
    private TextureRegion mBackgroundTextureRegion;
    private TextureRegion mBarBg;
    private TextureRegion mBarRight;
    private BitmapTextureAtlas mBitmapTexture;
    private Camera mCamera;
    private TiledTextureRegion mClock;
    /* access modifiers changed from: private */
    public ChangeableText mClockText;
    /* access modifiers changed from: private */
    public TextureRegion mCursor;
    /* access modifiers changed from: private */
    public GameDialog mDialog;
    /* access modifiers changed from: private */
    public TextureRegion mDialogBg;
    private Font mFont;
    private Font mFontBlue;
    private BitmapTextureAtlas mFontTexture;
    private BitmapTextureAtlas mFontTextureBlue;
    private BitmapTextureAtlas mFontTextureYellow;
    private Font mFontYellow;
    private TextureRegion mLevelBg;
    private ChangeableText mLevelText;
    private TiledTextureRegion mLightH;
    private TiledTextureRegion mLightV;
    /* access modifiers changed from: private */
    public Music mMusicAlarm;
    /* access modifiers changed from: private */
    public Music mMusicBg;
    private TextureRegion mProgressDown;
    private TextureRegion mProgressUp;
    private TiledTextureRegion mReset;
    /* access modifiers changed from: private */
    public ChangeableText mResetText;
    /* access modifiers changed from: private */
    public Scene mScene;
    /* access modifiers changed from: private */
    public ChangeableText mScoreText;
    public Sound mSoundClick;
    public Sound mSoundLost;
    public Sound mSoundMatch;
    private TiledTextureRegion mSoundTx;
    public Sound mSoundWin;
    private TiledTextureRegion mTips;
    /* access modifiers changed from: private */
    public ChangeableText mTipsText;
    private final int offsetX = 45;
    private final int offsetY = 80;
    /* access modifiers changed from: private */
    public Sprite pre;
    private Sprite progressDown;
    /* access modifiers changed from: private */
    public Sprite progressUp;
    /* access modifiers changed from: private */
    public int progressWidth = 345;
    /* access modifiers changed from: private */
    public Point pt0;
    /* access modifiers changed from: private */
    public Point pt1;
    /* access modifiers changed from: private */
    public int resetNum = 1;
    private AnimatedSprite resetSprite;
    /* access modifiers changed from: private */
    public int score = 0;
    /* access modifiers changed from: private */
    public Store store;
    private TextureRegion[] textureRegions = new TextureRegion[10];
    /* access modifiers changed from: private */
    public int time = 90;
    /* access modifiers changed from: private */
    public TimerHandler timerHandler;
    /* access modifiers changed from: private */
    public Sprite tipsCursor1;
    /* access modifiers changed from: private */
    public Sprite tipsCursor2;
    /* access modifiers changed from: private */
    public int tipsNum = 1;
    private AnimatedSprite tipsSprite;

    public Engine onLoadEngine() {
        this.mCamera = new Camera(0.0f, 0.0f, (float) this.CAMERA_WIDTH, (float) this.CAMERA_HEIGHT);
        return new Engine(new EngineOptions(true, EngineOptions.ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy((float) this.CAMERA_WIDTH, (float) this.CAMERA_HEIGHT), this.mCamera).setNeedsSound(true).setNeedsMusic(true));
    }

    public void onLoadResources() {
        this.mBitmapTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_BUMPMAP, (int) PVRTexture.FLAG_BUMPMAP, TextureOptions.DEFAULT);
        this.mBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "bg.jpg", 0, 0);
        for (int i = 0; i < this.textureRegions.length; i++) {
            this.textureRegions[i] = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, String.valueOf(i) + ".png", 720, i * 60);
        }
        this.mProgressUp = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "progress_up.png", 0, 480);
        this.mProgressDown = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "progress_down.png", 0, 500);
        this.mCursor = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "cursor.png", 0, 520);
        this.mLightH = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTexture, this, "lightH.png", 0, 640, 3, 1);
        this.mLightV = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTexture, this, "lightV.png", 780, 0, 1, 3);
        this.mReset = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTexture, this, "reset.png", 0, 700, 3, 1);
        this.mTips = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTexture, this, "tips.png", 144, 700, 3, 1);
        this.mClock = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTexture, this, "clock.png", 288, 700, 4, 1);
        this.mBarBg = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "bar_bg.png", 0, 800);
        this.mBarRight = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "bar_right.png", 850, 0);
        this.mLevelBg = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "level_bg.png", 0, 875);
        this.mSoundTx = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTexture, this, "sound.png", 480, 736, 2, 1);
        this.mDialogBg = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "dialog_bg.png", 720, 600);
        this.mEngine.getTextureManager().loadTexture(this.mBitmapTexture);
        try {
            this.mMusicBg = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), this, "sound/bg.ogg");
            this.mMusicBg.setLooping(true);
            this.mMusicAlarm = MusicFactory.createMusicFromAsset(this.mEngine.getMusicManager(), this, "sound/alarm.ogg");
            this.mMusicAlarm.setLooping(true);
        } catch (IOException e) {
            Debug.e(e);
        }
        try {
            this.mSoundClick = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "sound/click.ogg");
            this.mSoundMatch = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "sound/match.wav");
            this.mSoundWin = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "sound/win.ogg");
            this.mSoundLost = SoundFactory.createSoundFromAsset(this.mEngine.getSoundManager(), this, "sound/lost.ogg");
        } catch (IOException e2) {
            Debug.e(e2);
        }
        this.mFontTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFont = FontFactory.createFromAsset(this.mFontTexture, this, "font/droid.ttf", 28.0f, true, -65536);
        this.mFontTextureYellow = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFontYellow = FontFactory.createFromAsset(this.mFontTextureYellow, this, "font/droid.ttf", 28.0f, true, -256);
        this.mFontTextureBlue = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFontBlue = FontFactory.createFromAsset(this.mFontTextureBlue, this, "font/droid.ttf", 23.0f, true, -16776961);
        this.mEngine.getTextureManager().loadTextures(this.mFontTexture, this.mFontTextureYellow, this.mFontTextureBlue);
        this.mEngine.getFontManager().loadFonts(this.mFont, this.mFontYellow, this.mFontBlue);
    }

    public Scene onLoadScene() {
        this.level = getIntent().getExtras().getInt(LevelConstants.TAG_LEVEL);
        this.store = new Store(this);
        this.mScene = new Scene();
        this.mScene.setBackground(new SpriteBackground(new Sprite(0.0f, 0.0f, (float) this.CAMERA_WIDTH, (float) this.CAMERA_HEIGHT, this.mBackgroundTextureRegion)));
        this.mScene.attachChild(new Sprite(0.0f, 0.0f, this.mBarBg));
        this.mScene.attachChild(new Sprite(645.0f, 0.0f, this.mBarRight));
        this.mScene.attachChild(new Sprite(105.0f, 3.0f, this.mLevelBg));
        this.progressUp = new Sprite(175.0f, 30.0f, (float) this.progressWidth, 14.0f, this.mProgressUp);
        this.progressDown = new Sprite(175.0f, 27.0f, (float) this.progressWidth, 20.0f, this.mProgressDown);
        this.mScene.attachChild(this.progressDown);
        this.mScene.attachChild(this.progressUp);
        this.mLevelText = new ChangeableText(125.0f, 19.0f, this.mFontYellow, "00", 2);
        this.mScene.attachChild(this.mLevelText);
        this.mScoreText = new ChangeableText(610.0f, 24.0f, this.mFont, new StringBuilder(String.valueOf(this.score)).toString(), 5);
        this.mScene.attachChild(this.mScoreText);
        this.mResetText = new ChangeableText(670.0f, 125.0f, this.mFontBlue, TMXConstants.TAG_OBJECT_ATTRIBUTE_X + this.resetNum, 2);
        this.mScene.attachChild(this.mResetText);
        this.mTipsText = new ChangeableText(670.0f, 205.0f, this.mFontBlue, TMXConstants.TAG_OBJECT_ATTRIBUTE_X + this.tipsNum, 2);
        this.mScene.attachChild(this.mTipsText);
        this.mClockText = new ChangeableText(670.0f, 285.0f, this.mFontBlue, TMXConstants.TAG_OBJECT_ATTRIBUTE_X + this.clockNum, 2);
        this.mScene.attachChild(this.mClockText);
        this.tipsCursor1 = new Sprite(0.0f, 0.0f, 55.0f, 55.0f, this.mCursor.clone());
        this.tipsCursor1.setVisible(false);
        this.tipsCursor2 = new Sprite(0.0f, 0.0f, 55.0f, 55.0f, this.mCursor.clone());
        this.tipsCursor2.setVisible(false);
        this.mScene.attachChild(this.tipsCursor1);
        this.mScene.attachChild(this.tipsCursor2);
        this.timerHandler = new TimerHandler(1.0f, true, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                GameActivity gameActivity = GameActivity.this;
                gameActivity.time = gameActivity.time - 1;
                GameActivity.this.progressUp.setWidth((float) ((GameActivity.this.progressWidth * GameActivity.this.time) / GameActivity.this.gameTime));
                if (GameActivity.this.time < GameActivity.this.gameTime / 5 && GameActivity.this.isSound) {
                    GameActivity.this.mMusicAlarm.play();
                }
                if (GameActivity.this.time <= 0) {
                    if (GameActivity.this.isSound) {
                        GameActivity.this.mMusicAlarm.pause();
                        GameActivity.this.mSoundLost.play();
                    }
                    GameActivity.this.runOnUpdateThread(new Runnable() {
                        public void run() {
                            GameActivity.this.mScene.unregisterUpdateHandler(GameActivity.this.timerHandler);
                            GameActivity.this.mScene.clearTouchAreas();
                            GameActivity.this.mDialog = new GameDialog(GameActivity.this, GameActivity.this.mEngine, GameActivity.this.mScene, GameActivity.this.mDialogBg);
                            GameActivity.this.mScene.attachChild(GameActivity.this.mDialog);
                            GameActivity.this.mDialog.show(false, GameActivity.this.score, GameActivity.this.level);
                        }
                    });
                }
            }
        });
        gameLogic();
        return this.mScene;
    }

    public void gameLogic() {
        gameLogic(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int, int, boolean):org.anddev.andengine.entity.sprite.AnimatedSprite
     arg types: [long[], int, int, int]
     candidates:
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int, int, int):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int[], int, org.anddev.andengine.entity.sprite.AnimatedSprite$IAnimationListener):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int, int, boolean):org.anddev.andengine.entity.sprite.AnimatedSprite */
    public void gameLogic(boolean isReset) {
        this.mScene.clearTouchAreas();
        this.pt0 = null;
        this.pt1 = null;
        this.pre = null;
        if (isReset) {
            BlockArray.reset();
        } else {
            initGame();
            this.mScene.registerUpdateHandler(this.timerHandler);
            if (this.mMusicAlarm.isPlaying()) {
                this.mMusicAlarm.pause();
            }
        }
        if (this.listSprite != null && this.listSprite.size() > 0) {
            for (Sprite sp : this.listSprite) {
                this.mScene.detachChild(sp);
            }
        }
        if (this.resetSprite != null) {
            this.mScene.detachChild(this.resetSprite);
        }
        if (this.tipsSprite != null) {
            this.mScene.detachChild(this.tipsSprite);
        }
        if (this.clockSprite != null) {
            this.mScene.detachChild(this.clockSprite);
        }
        this.resetSprite = new AnimatedSprite(660.0f, 80.0f, this.mReset) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (!pSceneTouchEvent.isActionDown() || GameActivity.this.resetNum <= 0) {
                    return false;
                }
                GameActivity gameActivity = GameActivity.this;
                gameActivity.resetNum = gameActivity.resetNum - 1;
                if (GameActivity.this.resetNum == 0) {
                    stopAnimation(2);
                }
                GameActivity.this.runOnUpdateThread(new Runnable() {
                    public void run() {
                        GameActivity.this.gameLogic(true);
                        GameActivity.this.mResetText.setText(TMXConstants.TAG_OBJECT_ATTRIBUTE_X + GameActivity.this.resetNum);
                    }
                });
                return false;
            }
        };
        this.mScene.attachChild(this.resetSprite);
        if (this.resetNum > 0) {
            this.mScene.registerTouchArea(this.resetSprite);
            this.resetSprite.animate(new long[]{300, 300}, 0, 1, true);
        } else {
            this.resetSprite.stopAnimation(2);
        }
        this.tipsSprite = new AnimatedSprite(660.0f, 160.0f, this.mTips) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (!pSceneTouchEvent.isActionDown() || GameActivity.this.tipsNum <= 0) {
                    return false;
                }
                GameActivity gameActivity = GameActivity.this;
                gameActivity.tipsNum = gameActivity.tipsNum - 1;
                if (GameActivity.this.tipsNum == 0) {
                    stopAnimation(2);
                }
                final Point[] point = BlockArray.scane();
                GameActivity.this.runOnUpdateThread(new Runnable() {
                    public void run() {
                        if (point == null) {
                            GameActivity.this.gameLogic(true);
                        } else {
                            GameActivity.this.tipsCursor1.setPosition((float) (((point[0].getX() - 1) * 55) + 45), (float) (((point[0].getY() - 1) * 55) + 80));
                            GameActivity.this.tipsCursor1.setVisible(true);
                            GameActivity.this.tipsCursor2.setPosition((float) (((point[1].getX() - 1) * 55) + 45), (float) (((point[1].getY() - 1) * 55) + 80));
                            GameActivity.this.tipsCursor2.setVisible(true);
                        }
                        GameActivity.this.mTipsText.setText(TMXConstants.TAG_OBJECT_ATTRIBUTE_X + GameActivity.this.tipsNum);
                    }
                });
                return false;
            }
        };
        this.mScene.attachChild(this.tipsSprite);
        if (this.tipsNum > 0) {
            this.tipsSprite.animate(new long[]{300, 300}, 0, 1, true);
            this.mScene.registerTouchArea(this.tipsSprite);
        } else {
            this.tipsSprite.stopAnimation(2);
        }
        this.clockSprite = new AnimatedSprite(660.0f, 240.0f, this.mClock) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (!pSceneTouchEvent.isActionDown() || GameActivity.this.clockNum <= 0) {
                    return false;
                }
                GameActivity gameActivity = GameActivity.this;
                gameActivity.clockNum = gameActivity.clockNum - 1;
                if (GameActivity.this.clockNum == 0) {
                    stopAnimation(3);
                }
                GameActivity.this.runOnUpdateThread(new Runnable() {
                    public void run() {
                        GameActivity access$0 = GameActivity.this;
                        access$0.time = access$0.time + 10;
                        if (GameActivity.this.time > GameActivity.this.gameTime) {
                            GameActivity.this.time = GameActivity.this.gameTime;
                        }
                        GameActivity.this.mClockText.setText(TMXConstants.TAG_OBJECT_ATTRIBUTE_X + GameActivity.this.clockNum);
                    }
                });
                return false;
            }
        };
        this.mScene.attachChild(this.clockSprite);
        if (this.clockNum > 0) {
            this.clockSprite.animate(new long[]{200, 200, 200}, 0, 2, true);
            this.mScene.registerTouchArea(this.clockSprite);
        } else {
            this.clockSprite.stopAnimation(3);
        }
        this.SoundSprite = new AnimatedSprite(660.0f, 410.0f, this.mSoundTx) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionDown()) {
                    if (GameActivity.this.isSound) {
                        GameActivity.this.isSound = false;
                        stopAnimation(1);
                        GameActivity.this.mMusicBg.pause();
                        if (GameActivity.this.mMusicAlarm.isPlaying()) {
                            GameActivity.this.mMusicAlarm.pause();
                        }
                    } else {
                        GameActivity.this.isSound = true;
                        stopAnimation(0);
                        GameActivity.this.mMusicBg.play();
                    }
                }
                return false;
            }
        };
        this.mScene.attachChild(this.SoundSprite);
        this.mScene.registerTouchArea(this.SoundSprite);
        if (this.isSound) {
            this.mMusicBg.play();
            this.SoundSprite.stopAnimation(0);
        } else {
            this.mMusicBg.pause();
            this.clockSprite.stopAnimation(1);
        }
        for (int i = 0; i < this.textureRegions.length; i++) {
            this.textureRegions[i] = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, String.valueOf(i) + ".png", 720, i * 60);
        }
        this.mEngine.getTextureManager().loadTexture(this.mBitmapTexture);
        this.listSprite = new ArrayList();
        for (int i2 = 1; i2 < BlockArray.array.length - 1; i2++) {
            for (int j = 1; j < BlockArray.array[i2].length - 1; j++) {
                int temp = BlockArray.array[i2][j];
                if (temp != -1) {
                    Sprite sp2 = new Sprite((float) (((i2 - 1) * 55) + 45), (float) (((j - 1) * 55) + 80), 55.0f, 55.0f, this.textureRegions[temp]) {
                        public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                            int x = ((int) ((getX() - 45.0f) / 55.0f)) + 1;
                            int y = ((int) ((getY() - 80.0f) / 55.0f)) + 1;
                            if (pSceneTouchEvent.isActionDown() && isVisible()) {
                                if (GameActivity.this.isSound) {
                                    GameActivity.this.mSoundClick.play();
                                }
                                if (GameActivity.this.pt0 == null && GameActivity.this.pt1 == null) {
                                    if (GameActivity.this.pre != null) {
                                        GameActivity.this.pre.detachChild(GameActivity.this.pre.getFirstChild());
                                    }
                                    GameActivity.this.pt0 = new Point(x, y);
                                    GameActivity.this.pre = this;
                                    attachChild(new Sprite(0.0f, 0.0f, 55.0f, 55.0f, GameActivity.this.mCursor.clone()));
                                    return true;
                                } else if (!(GameActivity.this.pt0 == null || (GameActivity.this.pt0.x == x && GameActivity.this.pt0.y == y))) {
                                    GameActivity.this.pt1 = new Point(x, y);
                                    attachChild(new Sprite(0.0f, 0.0f, 55.0f, 55.0f, GameActivity.this.mCursor.clone()));
                                    if (BlockArray.check(GameActivity.this.pt0, GameActivity.this.pt1)) {
                                        GameActivity.this.tipsCursor1.setVisible(false);
                                        GameActivity.this.tipsCursor2.setVisible(false);
                                        if (GameActivity.this.isSound) {
                                            GameActivity.this.mSoundMatch.play();
                                        }
                                        BlockArray.array[GameActivity.this.pt0.x][GameActivity.this.pt0.y] = -1;
                                        BlockArray.array[GameActivity.this.pt1.x][GameActivity.this.pt1.y] = -1;
                                        switch (BlockArray.matchType) {
                                            case 0:
                                                GameActivity.this.matchAmin(GameActivity.this.pre.getX(), GameActivity.this.pre.getY(), getX(), getY());
                                                break;
                                            case 1:
                                                float cornerX = (float) (((BlockArray.points[0].getX() - 1) * 55) + 45);
                                                float cornerY = (float) (((BlockArray.points[0].getY() - 1) * 55) + 80);
                                                GameActivity.this.matchAmin(GameActivity.this.pre.getX(), GameActivity.this.pre.getY(), cornerX, cornerY);
                                                GameActivity.this.matchAmin(cornerX, cornerY, getX(), getY());
                                                break;
                                            case 2:
                                                float cornerX1 = (float) (((BlockArray.points[1].getX() - 1) * 55) + 45);
                                                float cornerY1 = (float) (((BlockArray.points[1].getY() - 1) * 55) + 80);
                                                float cornerX2 = (float) (((BlockArray.points[0].getX() - 1) * 55) + 45);
                                                float cornerY2 = (float) (((BlockArray.points[0].getY() - 1) * 55) + 80);
                                                GameActivity.this.matchAmin(GameActivity.this.pre.getX(), GameActivity.this.pre.getY(), cornerX1, cornerY1);
                                                GameActivity.this.matchAmin(cornerX1, cornerY1, cornerX2, cornerY2);
                                                GameActivity.this.matchAmin(cornerX2, cornerY2, getX(), getY());
                                                break;
                                        }
                                        if (GameActivity.this.pre.isVisible()) {
                                            GameActivity.this.pre.setVisible(false);
                                        }
                                        if (isVisible()) {
                                            setVisible(false);
                                        }
                                        int scorePlus = ((BlockArray.matchType + 1) * 5) + (GameActivity.this.combo * 2);
                                        GameActivity gameActivity = GameActivity.this;
                                        gameActivity.score = gameActivity.score + scorePlus;
                                        float scorePlusX = getX();
                                        final float f = scorePlusX;
                                        final float y2 = getY();
                                        final int i = scorePlus;
                                        GameActivity.this.runOnUpdateThread(new Runnable() {
                                            public void run() {
                                                GameActivity.this.scoreAnim(f, y2, i);
                                            }
                                        });
                                        GameActivity.this.store.set("score_" + GameActivity.this.level, GameActivity.this.score);
                                        GameActivity gameActivity2 = GameActivity.this;
                                        gameActivity2.combo = gameActivity2.combo + 1;
                                        if (BlockArray.isWin()) {
                                            GameActivity.this.runOnUpdateThread(new Runnable() {
                                                public void run() {
                                                    if (GameActivity.this.isSound) {
                                                        GameActivity.this.mSoundWin.play();
                                                    }
                                                    GameActivity.this.store.set("level_" + GameActivity.this.level, 1);
                                                    GameActivity.this.mScene.unregisterUpdateHandler(GameActivity.this.timerHandler);
                                                    GameActivity.this.mDialog = new GameDialog(GameActivity.this, GameActivity.this.mEngine, GameActivity.this.mScene, GameActivity.this.mDialogBg);
                                                    GameActivity.this.mScene.attachChild(GameActivity.this.mDialog);
                                                    GameActivity.this.mDialog.show(true, GameActivity.this.score, GameActivity.this.level);
                                                    GameActivity access$0 = GameActivity.this;
                                                    access$0.level = access$0.level + 1;
                                                }
                                            });
                                        }
                                    } else {
                                        if (GameActivity.this.pre != null) {
                                            GameActivity.this.pre.detachChild(GameActivity.this.pre.getFirstChild());
                                            GameActivity.this.combo = 0;
                                        }
                                        if (GameActivity.this.isSound) {
                                            GameActivity.this.mSoundClick.play();
                                        }
                                        GameActivity.this.pre = this;
                                    }
                                    GameActivity.this.pt0 = null;
                                    GameActivity.this.pt1 = null;
                                }
                            }
                            return false;
                        }
                    };
                    this.mScene.attachChild(sp2);
                    this.mScene.registerTouchArea(sp2);
                    this.listSprite.add(sp2);
                }
            }
        }
        this.mScene.setTouchAreaBindingEnabled(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, boolean, org.anddev.andengine.entity.sprite.AnimatedSprite$IAnimationListener):org.anddev.andengine.entity.sprite.AnimatedSprite
     arg types: [int, int, com.cmdu.apps.HappyFarmLinkUp.GameActivity$7]
     candidates:
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, int, org.anddev.andengine.entity.sprite.AnimatedSprite$IAnimationListener):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int, org.anddev.andengine.entity.sprite.AnimatedSprite$IAnimationListener):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], boolean, org.anddev.andengine.entity.sprite.AnimatedSprite$IAnimationListener):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int[], int):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, boolean, org.anddev.andengine.entity.sprite.AnimatedSprite$IAnimationListener):org.anddev.andengine.entity.sprite.AnimatedSprite */
    public void matchAmin(float fX, float fY, float tX, float tY) {
        final AnimatedSprite lightSprite;
        if (fX == tX) {
            lightSprite = new AnimatedSprite(fX, fY > tY ? tY + 27.0f : fY + 27.0f, 55.0f, Math.abs(tY - fY), this.mLightV.clone());
        } else {
            lightSprite = new AnimatedSprite(fX > tX ? tX + 27.0f : fX + 27.0f, fY, Math.abs(tX - fX), 55.0f, this.mLightH.clone());
        }
        lightSprite.animate(150L, false, (AnimatedSprite.IAnimationListener) new AnimatedSprite.IAnimationListener() {
            public void onAnimationEnd(AnimatedSprite pAnimatedSprite) {
                GameActivity gameActivity = GameActivity.this;
                final AnimatedSprite animatedSprite = lightSprite;
                gameActivity.runOnUpdateThread(new Runnable() {
                    public void run() {
                        GameActivity.this.mScene.detachChild(animatedSprite);
                    }
                });
            }
        });
        this.mScene.attachChild(lightSprite);
    }

    public void initGame() {
        this.gameTime = 90;
        this.time = 90;
        this.resetNum = 1;
        this.tipsNum = 1;
        this.clockNum = 1;
        this.score = 0;
        this.combo = 0;
        Log.d("++++level+++", new StringBuilder(String.valueOf(this.level)).toString());
        setLevelText();
        this.mScoreText.setText(new StringBuilder(String.valueOf(this.score)).toString());
        this.mResetText.setText(TMXConstants.TAG_OBJECT_ATTRIBUTE_X + this.resetNum);
        this.mTipsText.setText(TMXConstants.TAG_OBJECT_ATTRIBUTE_X + this.tipsNum);
        this.mClockText.setText(TMXConstants.TAG_OBJECT_ATTRIBUTE_X + this.clockNum);
        BlockArray.initLevel(this.level);
        BlockArray.print();
    }

    public void setLevelText() {
        this.mLevelText.setText(String.valueOf(this.level < 10 ? "0" : "") + this.level);
    }

    public void scoreAnim(float fX, float fY, int scorePlus) {
        final ChangeableText scoreAnimText = new ChangeableText(fX, fY, this.mFont, "+" + scorePlus, 3);
        this.mScene.attachChild(scoreAnimText);
        scoreAnimText.registerEntityModifier(new LoopEntityModifier(new PathModifier(1.0f, new PathModifier.Path(2).to(fX, fY).to(610.0f, 24.0f), null, new PathModifier.IPathModifierListener() {
            public void onPathStarted(PathModifier pPathModifier, IEntity pEntity) {
            }

            public void onPathWaypointStarted(PathModifier pPathModifier, IEntity pEntity, int pWaypointIndex) {
            }

            public void onPathWaypointFinished(PathModifier pPathModifier, IEntity pEntity, int pWaypointIndex) {
            }

            public void onPathFinished(PathModifier pPathModifier, IEntity pEntity) {
                GameActivity gameActivity = GameActivity.this;
                final ChangeableText changeableText = scoreAnimText;
                gameActivity.runOnUpdateThread(new Runnable() {
                    public void run() {
                        GameActivity.this.mScene.detachChild(changeableText);
                        GameActivity.this.mScoreText.setText(new StringBuilder(String.valueOf(GameActivity.this.score)).toString());
                    }
                });
            }
        }, EaseSineInOut.getInstance()), 1));
    }
}
