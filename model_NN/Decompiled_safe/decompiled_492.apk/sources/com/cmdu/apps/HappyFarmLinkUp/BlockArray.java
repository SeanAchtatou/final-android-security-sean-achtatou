package com.cmdu.apps.HappyFarmLinkUp;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class BlockArray {
    public static int[][] array;
    private static int height;
    private static List<Integer> list = new ArrayList();
    public static int matchType = 0;
    public static Point[] points;
    private static int width;

    public static void init(int w, int h) {
        width = w + 2;
        height = h + 2;
        array = (int[][]) Array.newInstance(Integer.TYPE, width, height);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (i == 0 || i == width - 1 || j == 0 || j == height - 1) {
                    array[i][j] = -1;
                }
            }
        }
        for (int i2 = 0; i2 < width - 2; i2++) {
            for (int j2 = 0; j2 < height - 2; j2++) {
                list.add(Integer.valueOf(i2));
            }
        }
        fill();
    }

    public static void initLevel(int level) {
        array = new int[0][];
        array = Level.getLevelData(level);
        print();
        width = array.length;
        height = array[0].length;
        int size = 0;
        for (int i = 0; i < array[0].length; i++) {
            for (int[] iArr : array) {
                if (iArr[i] != -1) {
                    size++;
                }
            }
        }
        List<Integer> vList = new ArrayList<>();
        for (int i2 = 0; i2 < size; i2 += 2) {
            int v = getRandom(9);
            vList.add(Integer.valueOf(v));
            vList.add(Integer.valueOf(v));
        }
        Collections.shuffle(vList);
        int k = 0;
        for (int i3 = 0; i3 < array[0].length; i3++) {
            for (int j = 0; j < array.length; j++) {
                if (array[j][i3] != -1) {
                    array[j][i3] = ((Integer) vList.get(k)).intValue();
                    k++;
                }
            }
        }
    }

    public static void print() {
        for (int i = 0; i < array[0].length; i++) {
            for (int[] iArr : array) {
                System.out.print(iArr[i]);
            }
            System.out.println();
        }
    }

    public static void fill() {
        List<Integer> list2 = new ArrayList<>();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            int temp = (int) (Math.random() * ((double) list.size()));
            list2.add(list.get(temp));
            list.remove(temp);
        }
        for (int i2 = 1; i2 < width - 1; i2++) {
            for (int j = 1; j < height - 1; j++) {
                array[i2][j] = ((Integer) list2.get(((i2 - 1) * (height - 2)) + (j - 1))).intValue();
            }
        }
    }

    public static void reset() {
        List<Integer> listVal = new ArrayList<>();
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[j][i] != -1) {
                    listVal.add(Integer.valueOf(array[j][i]));
                }
            }
        }
        Collections.shuffle(listVal);
        int k = 0;
        for (int i2 = 0; i2 < array[0].length; i2++) {
            for (int j2 = 0; j2 < array.length; j2++) {
                if (array[j2][i2] != -1) {
                    array[j2][i2] = ((Integer) listVal.get(k)).intValue();
                    k++;
                }
            }
        }
    }

    public static boolean isWin() {
        for (int i = 1; i < width; i++) {
            for (int j = 1; j < height; j++) {
                if (array[i][j] != -1) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean checkLine(Point pt0, Point pt1) {
        if (pt0.x == pt1.x) {
            int min = Math.min(pt0.y, pt1.y);
            int max = Math.max(pt0.y, pt1.y);
            if (max - min == 1) {
                return true;
            }
            for (int i = min + 1; i < max; i++) {
                if (array[pt0.x][i] != -1) {
                    return false;
                }
            }
            return true;
        } else if (pt0.y != pt1.y) {
            return false;
        } else {
            int min2 = Math.min(pt0.x, pt1.x);
            int max2 = Math.max(pt0.x, pt1.x);
            if (max2 - min2 == 1) {
                return true;
            }
            for (int i2 = min2 + 1; i2 < max2; i2++) {
                if (array[i2][pt0.y] != -1) {
                    return false;
                }
            }
            return true;
        }
    }

    public static boolean checkTurn(Point pt0, Point pt1) {
        if (!(pt0.x == pt1.x || pt0.y == pt1.y)) {
            if (array[pt0.x][pt1.y] == -1 && checkLine(new Point(pt0.x, pt1.y), pt1) && checkLine(new Point(pt0.x, pt1.y), pt0)) {
                if (points[0] == null) {
                    points[0] = new Point(pt0.x, pt1.y);
                }
                return true;
            } else if (array[pt1.x][pt0.y] == -1 && checkLine(new Point(pt1.x, pt0.y), pt1) && checkLine(new Point(pt1.x, pt0.y), pt0)) {
                if (points[0] == null) {
                    points[0] = new Point(pt1.x, pt0.y);
                }
                return true;
            }
        }
        return false;
    }

    public static boolean checkDoubleTurn(Point pt0, Point pt1) {
        int i = pt0.x - 1;
        while (i >= 0 && array[i][pt0.y] < 0) {
            if (checkTurn(new Point(i, pt0.y), pt1)) {
                points[1] = new Point(i, pt0.y);
                return true;
            }
            i--;
        }
        int i2 = pt0.x + 1;
        while (i2 < width && array[i2][pt0.y] < 0) {
            if (checkTurn(new Point(i2, pt0.y), pt1)) {
                points[1] = new Point(i2, pt0.y);
                return true;
            }
            i2++;
        }
        int i3 = pt0.y - 1;
        while (i3 >= 0 && array[pt0.x][i3] < 0) {
            if (checkTurn(new Point(pt0.x, i3), pt1)) {
                points[1] = new Point(pt0.x, i3);
                return true;
            }
            i3--;
        }
        int i4 = pt0.y + 1;
        while (i4 < height && array[pt0.x][i4] < 0) {
            if (checkTurn(new Point(pt0.x, i4), pt1)) {
                points[1] = new Point(pt0.x, i4);
                return true;
            }
            i4++;
        }
        return false;
    }

    public static boolean check(Point pt0, Point pt1) {
        boolean value;
        points = new Point[2];
        if (array[pt0.x][pt0.y] == array[pt1.x][pt1.y]) {
            value = true;
        } else {
            value = false;
        }
        boolean line = checkLine(pt0, pt1);
        boolean turn = checkTurn(pt0, pt1);
        boolean doubleTurn = checkDoubleTurn(pt0, pt1);
        if (doubleTurn) {
            matchType = 2;
        }
        if (turn) {
            matchType = 1;
        }
        if (line) {
            matchType = 0;
        }
        if (!value || (!line && !turn && !doubleTurn)) {
            return false;
        }
        return true;
    }

    public static Point[] scane() {
        Point[] point = new Point[2];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[j][i] != -1) {
                    point[0] = new Point(j, i);
                    for (int ii = 0; ii < array[0].length; ii++) {
                        for (int jj = 0; jj < array.length; jj++) {
                            if (!(array[jj][ii] == -1 || (jj == j && ii == i))) {
                                point[1] = new Point(jj, ii);
                                if (check(point[0], point[1])) {
                                    return point;
                                }
                            }
                        }
                    }
                    continue;
                }
            }
        }
        return null;
    }

    public static int getRandom(int max) {
        return getRandom(0, max);
    }

    public static int getRandom(int a, int b) {
        int b2 = b + 1;
        if (a <= b2) {
            return new Random().nextInt(b2 - a) + a;
        }
        try {
            return new Random().nextInt(a - b2) + b2;
        } catch (Exception e) {
            e.printStackTrace();
            return 0 + a;
        }
    }

    public static void main(String[] args) {
        init(10, 6);
        int[] iArr = new int[12];
        iArr[0] = -1;
        iArr[1] = 8;
        iArr[2] = 3;
        iArr[5] = 3;
        iArr[6] = 5;
        iArr[7] = 2;
        iArr[8] = 7;
        iArr[9] = 4;
        iArr[10] = 7;
        iArr[11] = -1;
        int[] iArr2 = new int[12];
        iArr2[0] = -1;
        iArr2[2] = 3;
        iArr2[4] = 3;
        iArr2[5] = 9;
        iArr2[6] = 6;
        iArr2[7] = 5;
        iArr2[8] = 4;
        iArr2[9] = 4;
        iArr2[10] = 3;
        iArr2[11] = -1;
        int[] iArr3 = new int[12];
        iArr3[0] = -1;
        iArr3[3] = 3;
        iArr3[4] = 6;
        iArr3[5] = 1;
        iArr3[6] = 9;
        iArr3[7] = 7;
        iArr3[9] = 4;
        iArr3[10] = 3;
        iArr3[11] = -1;
        int[] iArr4 = new int[12];
        iArr4[0] = -1;
        iArr4[1] = 6;
        iArr4[2] = 3;
        iArr4[4] = 1;
        iArr4[5] = 8;
        iArr4[7] = 5;
        iArr4[8] = 1;
        iArr4[9] = 1;
        iArr4[10] = 5;
        iArr4[11] = -1;
        int[] iArr5 = new int[12];
        iArr5[0] = -1;
        iArr5[1] = 9;
        iArr5[2] = 3;
        iArr5[4] = 5;
        iArr5[5] = 3;
        iArr5[6] = 7;
        iArr5[7] = 4;
        iArr5[8] = 7;
        iArr5[9] = 4;
        iArr5[10] = 2;
        iArr5[11] = -1;
        array = new int[][]{new int[]{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, iArr, iArr2, iArr3, iArr4, iArr5, new int[]{-1, 9, 2, 3, 8, 2, 2, 3, 8, 8, 1, -1}, new int[]{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}};
    }
}
