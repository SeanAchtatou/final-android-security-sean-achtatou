package com.cmdu.apps.HappyFarmLinkUp;

public class Point {
    public int x;
    public int y;

    public Point(int x2, int y2) {
        this.x = x2;
        this.y = y2;
    }

    public Point() {
        this.x = 0;
        this.y = 0;
    }

    public int getX() {
        return this.x;
    }

    public void setX(int x2) {
        this.x = x2;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int y2) {
        this.y = y2;
    }
}
