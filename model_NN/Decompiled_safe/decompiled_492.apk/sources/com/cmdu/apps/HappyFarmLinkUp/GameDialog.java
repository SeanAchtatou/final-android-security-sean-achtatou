package com.cmdu.apps.HappyFarmLinkUp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.font.FontFactory;
import org.anddev.andengine.opengl.font.StrokeFont;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class GameDialog extends Sprite {
    /* access modifiers changed from: private */
    public Context context;
    private BitmapTextureAtlas mBitmapTexture;
    private Engine mEngine;
    private Font mFont;
    private BitmapTextureAtlas mFontTexture;
    private TextureRegion mLevel;
    private TextureRegion mNext;
    private TextureRegion mReplay;
    private Scene mScene;
    private TiledTextureRegion mStar;
    private StrokeFont mStrokeFont;
    private BitmapTextureAtlas mStrokeFontTexture;
    /* access modifiers changed from: private */
    public Store store;

    public GameDialog(float pX, float pY, TextureRegion pTextureRegion) {
        super(pX, pY, pTextureRegion);
    }

    public GameDialog(Context context2, Engine mEngine2, Scene mScene2, TextureRegion pTextureRegion) {
        super(180.0f, 130.0f, pTextureRegion);
        this.context = context2;
        this.mEngine = mEngine2;
        this.mScene = mScene2;
        loadResources();
        this.store = new Store(this.context);
    }

    private void loadResources() {
        this.mBitmapTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mLevel = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this.context, "level.png", 0, 0);
        this.mReplay = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this.context, "replay.png", 0, 48);
        this.mNext = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this.context, "next.png", 0, 96);
        this.mStar = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTexture, this.context, "star_big.png", 0, 144, 2, 1);
        this.mEngine.getTextureManager().loadTexture(this.mBitmapTexture);
        this.mFontTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mFont = FontFactory.createFromAsset(this.mFontTexture, this.context, "font/droid.ttf", 28.0f, true, -16711936);
        this.mStrokeFontTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_MIPMAP, (int) PVRTexture.FLAG_MIPMAP, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mStrokeFont = FontFactory.createStrokeFromAsset(this.mStrokeFontTexture, this.context, "font/droid.ttf", 30.0f, true, -16777216, 2.0f, -1, true);
        this.mEngine.getTextureManager().loadTextures(this.mFontTexture, this.mStrokeFontTexture);
        this.mEngine.getFontManager().loadFonts(this.mFont, this.mStrokeFont);
    }

    public void show(boolean isWin, int score, int level) {
        this.mScene.clearTouchAreas();
        attachChild(new Text(110.0f, 8.0f, this.mFont, new StringBuilder(String.valueOf(score)).toString()));
        attachChild(new Text(15.0f, 110.0f, this.mStrokeFont, isWin ? level == 18 ? "Game Completed!" : "Level Completed!" : "Level Failed!"));
        attachChild(new AnimatedSprite(90.0f, 38.0f, this.mStar.clone()));
        AnimatedSprite animatedSprite = new AnimatedSprite(126.0f, 38.0f, this.mStar.clone());
        if (score < 550) {
            animatedSprite.stopAnimation(1);
        }
        attachChild(animatedSprite);
        AnimatedSprite animatedSprite2 = new AnimatedSprite(162.0f, 38.0f, this.mStar.clone());
        if (score < 800) {
            animatedSprite2.stopAnimation(1);
        }
        attachChild(animatedSprite2);
        AnonymousClass1 r0 = new Sprite(25.0f, 160.0f, this.mLevel) {
            /* Debug info: failed to restart local var, previous not found, register: 4 */
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (!pSceneTouchEvent.isActionDown()) {
                    return false;
                }
                setScale(1.25f);
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setScale(1.0f);
                Intent intent = new Intent(GameDialog.this.context, LevelActivity.class);
                intent.setFlags(67108864);
                GameDialog.this.context.startActivity(intent);
                GameDialog.this.detach();
                ((Activity) GameDialog.this.context).finish();
                return false;
            }
        };
        attachChild(r0);
        this.mScene.registerTouchArea(r0);
        AnonymousClass2 r02 = new Sprite(125.0f, 160.0f, this.mReplay) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (!pSceneTouchEvent.isActionDown()) {
                    return false;
                }
                setScale(1.25f);
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setScale(1.0f);
                ((GameActivity) GameDialog.this.context).gameLogic();
                GameDialog.this.detach();
                return false;
            }
        };
        attachChild(r02);
        this.mScene.registerTouchArea(r02);
        final boolean z = isWin;
        final int i = level;
        AnonymousClass3 r5 = new Sprite(227.0f, 160.0f, this.mNext) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (!pSceneTouchEvent.isActionDown()) {
                    return false;
                }
                setScale(1.25f);
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setScale(1.0f);
                if ((!z || i != 18) && (!Global.isWaps(GameDialog.this.context) || GameDialog.this.store.getInt("buy_" + (i + 1)) != 0)) {
                    ((GameActivity) GameDialog.this.context).gameLogic();
                } else {
                    Intent intent = new Intent(GameDialog.this.context, LevelActivity.class);
                    intent.setFlags(67108864);
                    GameDialog.this.context.startActivity(intent);
                    ((Activity) GameDialog.this.context).finish();
                }
                GameDialog.this.detach();
                return false;
            }
        };
        attachChild(r5);
        this.mScene.registerTouchArea(r5);
    }

    public void detach() {
        this.mScene.detachChild(this);
    }
}
