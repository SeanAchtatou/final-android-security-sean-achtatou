package com.cmdu.apps.HappyFarmLinkUp;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.Map;

public class Store {
    private SharedPreferences.Editor editor;
    private int mode = 0;
    private String name = "BrainTrainer";
    private SharedPreferences sp;

    public Store(Context context) {
        this.sp = context.getSharedPreferences(this.name, this.mode);
        this.editor = this.sp.edit();
    }

    public void set(Map<String, String> map) {
        for (String key : map.keySet()) {
            this.editor.putString(key, map.get(key));
        }
        this.editor.commit();
    }

    public void set(String key, String val) {
        this.editor.putString(key, val);
        this.editor.commit();
    }

    public void set(String key, int val) {
        this.editor.putInt(key, val);
        this.editor.commit();
    }

    public void deleteAll() throws Exception {
        this.editor.clear();
        this.editor.commit();
    }

    public void delete(String key) throws Exception {
        this.editor.remove(key);
        this.editor.commit();
    }

    public String get(String key) {
        if (this.sp != null) {
            return this.sp.getString(key, "");
        }
        return "";
    }

    public int getInt(String key) {
        if (this.sp != null) {
            return this.sp.getInt(key, 0);
        }
        return 0;
    }
}
