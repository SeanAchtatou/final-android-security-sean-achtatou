package com.cmdu.apps.HappyFarmLinkUp;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int LinearLayout01 = 2131034112;
        public static final int OfferProgressBar = 2131034115;
        public static final int RelativeLayout01 = 2131034113;
        public static final int app = 2131034116;
        public static final int appIcon = 2131034117;
        public static final int description = 2131034121;
        public static final int notification = 2131034119;
        public static final int offersWebView = 2131034114;
        public static final int progress_bar = 2131034122;
        public static final int progress_text = 2131034118;
        public static final int title = 2131034120;
    }

    public static final class layout {
        public static final int offers_web_view = 2130903040;
        public static final int umeng_download_notification = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int lang = 2130968576;
    }
}
