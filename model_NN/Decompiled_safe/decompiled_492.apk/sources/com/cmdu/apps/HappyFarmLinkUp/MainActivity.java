package com.cmdu.apps.HappyFarmLinkUp;

import android.content.Intent;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.SpriteBackground;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.compressed.pvr.PVRTexture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class MainActivity extends BaseActivity {
    private int CAMERA_HEIGHT = 480;
    private int CAMERA_WIDTH = 720;
    private TextureRegion mBackgroundTextureRegion;
    private BitmapTextureAtlas mBitmapTexture;
    private Camera mCamera;
    private TextureRegion mLogo;
    private TextureRegion mLogoImg;
    private Scene mScene;
    private TextureRegion mStart;

    public Engine onLoadEngine() {
        this.mCamera = new Camera(0.0f, 0.0f, (float) this.CAMERA_WIDTH, (float) this.CAMERA_HEIGHT);
        return new Engine(new EngineOptions(true, EngineOptions.ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy((float) this.CAMERA_WIDTH, (float) this.CAMERA_HEIGHT), this.mCamera).setNeedsSound(true).setNeedsMusic(true));
    }

    public void onLoadResources() {
        this.mBitmapTexture = new BitmapTextureAtlas((int) PVRTexture.FLAG_BUMPMAP, (int) PVRTexture.FLAG_BUMPMAP, TextureOptions.DEFAULT);
        this.mBackgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "bg.jpg", 0, 0);
        this.mLogo = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, Global.isChinese(this) ? "logo.png" : "logo_en.png", 0, 480);
        this.mLogoImg = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, "logo_img.png", 0, 585);
        this.mStart = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTexture, this, Global.isChinese(this) ? "start_game.png" : "start_game_en.png", 0, 755);
        this.mEngine.getTextureManager().loadTexture(this.mBitmapTexture);
    }

    public Scene onLoadScene() {
        this.mScene = new Scene();
        this.mScene.setBackground(new SpriteBackground(new Sprite(0.0f, 0.0f, (float) this.CAMERA_WIDTH, (float) this.CAMERA_HEIGHT, this.mBackgroundTextureRegion)));
        this.mScene.attachChild(new Sprite((float) ((this.CAMERA_WIDTH - this.mLogo.getWidth()) / 2), 10.0f, this.mLogo));
        this.mScene.attachChild(new Sprite((float) ((this.CAMERA_WIDTH - this.mLogoImg.getWidth()) / 2), 130.0f, this.mLogoImg));
        Sprite startSprite = new Sprite((float) ((this.CAMERA_WIDTH - this.mStart.getWidth()) / 2), 350.0f, this.mStart) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (!pSceneTouchEvent.isActionDown()) {
                    return false;
                }
                setScale(1.25f);
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                setScale(1.0f);
                Intent intent = new Intent(MainActivity.this, LevelActivity.class);
                intent.setFlags(67108864);
                MainActivity.this.startActivity(intent);
                return false;
            }
        };
        this.mScene.attachChild(startSprite);
        this.mScene.registerTouchArea(startSprite);
        return this.mScene;
    }
}
