package com.cmdu.apps.HappyFarmLinkUp;

import com.mobclick.android.MobclickAgent;
import com.waps.AppConnect;
import com.waps.UpdatePointsNotifier;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.ui.activity.BaseGameActivity;

public class BaseActivity extends BaseGameActivity implements UpdatePointsNotifier {
    public Ads ads;
    public int levelPoint = 30;
    public int needPoint = 50;
    public int point = 0;

    public Engine onLoadEngine() {
        return null;
    }

    public void onLoadResources() {
    }

    public Scene onLoadScene() {
        return null;
    }

    public void onLoadComplete() {
        this.ads = new Ads(this, super.createSurfaceViewLayoutParams());
        this.ads.getAd().getWb();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Global.disableWaps("2011-08-18 18:00");
        MobclickAgent.onResume(this);
        AppConnect.getInstance(this).getPoints(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AppConnect.getInstance(this).finalize();
        if (this.ads != null) {
            this.ads.finalize();
        }
    }

    public void getUpdatePoints(String arg0, int point2) {
        this.point = point2;
    }

    public void getUpdatePointsFailed(String arg0) {
        this.point = 0;
    }
}
