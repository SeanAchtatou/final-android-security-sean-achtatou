package com.games.gapssolitaire;

public class GameData {
    public int cardsInPlace;
    public long duration;
    public int score;

    public GameData(int cardsInPlace2, int score2, long duration2) {
        this.cardsInPlace = cardsInPlace2;
        this.score = score2;
        this.duration = duration2;
    }
}
