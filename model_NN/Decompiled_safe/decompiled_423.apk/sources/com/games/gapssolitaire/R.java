package com.games.gapssolitaire;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int background = 2131034112;
        public static final int black = 2131034113;
        public static final int dark = 2131034115;
        public static final int gray = 2131034117;
        public static final int inactive = 2131034116;
        public static final int menu = 2131034118;
        public static final int white = 2131034114;
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int background_menu = 2130837505;
        public static final int c10 = 2130837506;
        public static final int c6 = 2130837507;
        public static final int c7 = 2130837508;
        public static final int c8 = 2130837509;
        public static final int c9 = 2130837510;
        public static final int ca = 2130837511;
        public static final int cj = 2130837512;
        public static final int ck = 2130837513;
        public static final int cq = 2130837514;
        public static final int d10 = 2130837515;
        public static final int d6 = 2130837516;
        public static final int d7 = 2130837517;
        public static final int d8 = 2130837518;
        public static final int d9 = 2130837519;
        public static final int da = 2130837520;
        public static final int dj = 2130837521;
        public static final int dk = 2130837522;
        public static final int dq = 2130837523;
        public static final int h10 = 2130837524;
        public static final int h6 = 2130837525;
        public static final int h7 = 2130837526;
        public static final int h8 = 2130837527;
        public static final int h9 = 2130837528;
        public static final int ha = 2130837529;
        public static final int hj = 2130837530;
        public static final int hk = 2130837531;
        public static final int hq = 2130837532;
        public static final int icon = 2130837533;
        public static final int main_icon = 2130837534;
        public static final int s10 = 2130837535;
        public static final int s6 = 2130837536;
        public static final int s7 = 2130837537;
        public static final int s8 = 2130837538;
        public static final int s9 = 2130837539;
        public static final int sa = 2130837540;
        public static final int sj = 2130837541;
        public static final int sk = 2130837542;
        public static final int sleep = 2130837543;
        public static final int sq = 2130837544;
    }

    public static final class id {
        public static final int ad_layout = 2131165193;
        public static final int chronometer = 2131165196;
        public static final int chronometer_title = 2131165195;
        public static final int field_layout = 2131165188;
        public static final int footer = 2131165189;
        public static final int high_score_main = 2131165197;
        public static final int howtoplay_main = 2131165207;
        public static final int howtoplay_text_layout = 2131165208;
        public static final int main_layout = 2131165187;
        public static final int main_menu_header = 2131165210;
        public static final int main_menu_layout = 2131165211;
        public static final int score_layout = 2131165190;
        public static final int score_layout_header = 2131165198;
        public static final int score_layout_header_date = 2131165203;
        public static final int score_layout_header_place = 2131165199;
        public static final int score_layout_header_score = 2131165201;
        public static final int score_layout_header_separator_left = 2131165200;
        public static final int score_layout_header_separator_right = 2131165202;
        public static final int score_layout_main = 2131165204;
        public static final int score_layout_scroll = 2131165205;
        public static final int textView_high_score = 2131165214;
        public static final int textView_howtoplay = 2131165213;
        public static final int textView_main_menu = 2131165186;
        public static final int textView_resume_game = 2131165184;
        public static final int textView_return = 2131165206;
        public static final int textView_start_game = 2131165212;
        public static final int textView_start_new = 2131165185;
        public static final int text_howtoplay = 2131165209;
        public static final int text_view_score = 2131165192;
        public static final int text_view_score_title = 2131165191;
        public static final int time_layout = 2131165194;
    }

    public static final class layout {
        public static final int alert_view = 2130903040;
        public static final int end_of_game = 2130903041;
        public static final int field = 2130903042;
        public static final int high_score = 2130903043;
        public static final int howtoplay = 2130903044;
        public static final int main = 2130903045;
    }

    public static final class raw {
        public static final int step = 2130968576;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int cardTitle = 2131099650;
        public static final int hello = 2131099648;
        public static final int howtoplay = 2131099655;
        public static final int no = 2131099654;
        public static final int quit = 2131099651;
        public static final int really_quit = 2131099652;
        public static final int yes = 2131099653;
    }
}
