package com.games.gapssolitaire;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;

public class GapsSolitaireActivity extends Activity {
    /* access modifiers changed from: private */
    public Dialog dialogHowToPlay;
    /* access modifiers changed from: private */
    public Context mContext;
    private View.OnClickListener textView_high_score_click = new View.OnClickListener() {
        public void onClick(View v) {
            GapsSolitaireActivity.this.dialogHowToPlay.setTitle("High score");
            View layout = ((LayoutInflater) GapsSolitaireActivity.this.mContext.getSystemService("layout_inflater")).inflate(R.layout.high_score, (ViewGroup) null);
            GapsSolitaireActivity.this.dialogHowToPlay.setContentView(layout, new ViewGroup.LayoutParams((GapsSolitaireActivity.this.yScreenSize * 7) / 8, (GapsSolitaireActivity.this.xScreenSize * 2) / 3));
            GapsSolitaireActivity.this.dialogHowToPlay.setCancelable(true);
            ((LinearLayout) GapsSolitaireActivity.this.dialogHowToPlay.findViewById(R.id.score_layout_main)).setLayoutParams(new LinearLayout.LayoutParams((GapsSolitaireActivity.this.yScreenSize * 6) / 8, (GapsSolitaireActivity.this.xScreenSize * 1) / 2));
            LinearLayout score_layout = (LinearLayout) GapsSolitaireActivity.this.dialogHowToPlay.findViewById(R.id.score_layout);
            ArrayList<FileEntry> listEntries = FileHelper.getScoreFileData(GapsSolitaireActivity.this.mContext);
            if (listEntries != null) {
                Typeface fontHeader = Typeface.createFromAsset(GapsSolitaireActivity.this.getAssets(), "Ironman2.ttf");
                TextView score_layout_header_place = (TextView) GapsSolitaireActivity.this.dialogHowToPlay.findViewById(R.id.score_layout_header_place);
                score_layout_header_place.setWidth(((int) (((double) GapsSolitaireActivity.this.yScreenSize) * 1.15d)) / 5);
                score_layout_header_place.setTypeface(fontHeader);
                score_layout_header_place.setText("Place");
                ((TextView) GapsSolitaireActivity.this.dialogHowToPlay.findViewById(R.id.score_layout_header_separator_left)).setWidth((GapsSolitaireActivity.this.yScreenSize * 1) / 22);
                TextView score_layout_header_score = (TextView) GapsSolitaireActivity.this.dialogHowToPlay.findViewById(R.id.score_layout_header_score);
                score_layout_header_score.setTypeface(fontHeader);
                score_layout_header_score.setText("Score");
                ((TextView) GapsSolitaireActivity.this.dialogHowToPlay.findViewById(R.id.score_layout_header_separator_right)).setWidth((GapsSolitaireActivity.this.yScreenSize * 1) / 6);
                TextView score_layout_header_date = (TextView) GapsSolitaireActivity.this.dialogHowToPlay.findViewById(R.id.score_layout_header_date);
                score_layout_header_date.setWidth((GapsSolitaireActivity.this.yScreenSize * 2) / 8);
                score_layout_header_date.setTypeface(fontHeader);
                score_layout_header_date.setText("Date");
                Typeface fontKittyKat = Typeface.createFromAsset(GapsSolitaireActivity.this.getAssets(), "Kitty Kat.ttf");
                for (int i = 0; i < listEntries.size(); i++) {
                    LinearLayout linearLayout = new LinearLayout(GapsSolitaireActivity.this.mContext);
                    linearLayout.setOrientation(0);
                    TextView textView = new TextView(GapsSolitaireActivity.this.mContext);
                    textView.setText(String.valueOf(i + 1));
                    textView.setTextColor(R.color.dark);
                    textView.setWidth((GapsSolitaireActivity.this.yScreenSize * 1) / 16);
                    textView.setGravity(5);
                    textView.setTypeface(fontKittyKat);
                    textView.setTextSize(11.0f);
                    linearLayout.addView(textView);
                    TextView textView2 = new TextView(GapsSolitaireActivity.this.mContext);
                    textView2.setText(String.valueOf(listEntries.get(i).score));
                    textView2.setTextColor(R.color.dark);
                    textView2.setWidth((GapsSolitaireActivity.this.yScreenSize * 1) / 4);
                    textView2.setGravity(1);
                    textView2.setTypeface(fontKittyKat);
                    textView2.setTextSize(11.0f);
                    linearLayout.addView(textView2);
                    TextView textView3 = new TextView(GapsSolitaireActivity.this.mContext);
                    textView3.setText(listEntries.get(i).date);
                    textView3.setTextColor(R.color.dark);
                    textView3.setGravity(3);
                    textView3.setWidth(GapsSolitaireActivity.this.yScreenSize / 3);
                    textView3.setTypeface(fontKittyKat);
                    textView3.setTextSize(11.0f);
                    linearLayout.addView(textView3);
                    score_layout.addView(linearLayout, new LinearLayout.LayoutParams((GapsSolitaireActivity.this.yScreenSize * 5) / 8, GapsSolitaireActivity.this.xScreenSize / 15));
                }
            } else {
                TextView score_layout_header_place2 = (TextView) GapsSolitaireActivity.this.dialogHowToPlay.findViewById(R.id.score_layout_header_place);
                score_layout_header_place2.setPadding(5, 5, 5, 5);
                score_layout_header_place2.setText("No results yet.");
            }
            TextView textView_return = (TextView) GapsSolitaireActivity.this.dialogHowToPlay.findViewById(R.id.textView_return);
            textView_return.setTypeface(Typeface.createFromAsset(GapsSolitaireActivity.this.getAssets(), "Ironman2.ttf"));
            textView_return.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    GapsSolitaireActivity.this.dialogHowToPlay.cancel();
                }
            });
            GapsSolitaireActivity.this.dialogHowToPlay.show();
        }
    };
    private View.OnClickListener textView_howtoplay_click = new View.OnClickListener() {
        public void onClick(View v) {
            GapsSolitaireActivity.this.dialogHowToPlay.setTitle("How to play?");
            GapsSolitaireActivity.this.dialogHowToPlay.setContentView(((LayoutInflater) GapsSolitaireActivity.this.mContext.getSystemService("layout_inflater")).inflate((int) R.layout.howtoplay, (ViewGroup) null), new ViewGroup.LayoutParams((GapsSolitaireActivity.this.yScreenSize * 7) / 8, (GapsSolitaireActivity.this.xScreenSize * 2) / 3));
            GapsSolitaireActivity.this.dialogHowToPlay.setCancelable(true);
            ((LinearLayout) GapsSolitaireActivity.this.dialogHowToPlay.findViewById(R.id.howtoplay_text_layout)).setLayoutParams(new LinearLayout.LayoutParams((GapsSolitaireActivity.this.yScreenSize * 7) / 8, GapsSolitaireActivity.this.xScreenSize / 2));
            ((TextView) GapsSolitaireActivity.this.dialogHowToPlay.findViewById(R.id.text_howtoplay)).setTypeface(Typeface.createFromAsset(GapsSolitaireActivity.this.getAssets(), "Kitty Kat.ttf"));
            TextView textView_return = (TextView) GapsSolitaireActivity.this.dialogHowToPlay.findViewById(R.id.textView_return);
            textView_return.setTypeface(Typeface.createFromAsset(GapsSolitaireActivity.this.getAssets(), "Ironman2.ttf"));
            textView_return.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    GapsSolitaireActivity.this.dialogHowToPlay.cancel();
                }
            });
            GapsSolitaireActivity.this.dialogHowToPlay.show();
        }
    };
    private View.OnClickListener textView_resume_game_click = new View.OnClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onClick(View v) {
            if (FileHelper.isSavedGameExist(GapsSolitaireActivity.this.mContext)) {
                Intent intent = new Intent(GapsSolitaireActivity.this, FieldActivity.class);
                intent.putExtra("new_game", false);
                GapsSolitaireActivity.this.startActivity(intent);
            }
        }
    };
    private View.OnClickListener textView_start_game_click = new View.OnClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onClick(View v) {
            Intent intent = new Intent(GapsSolitaireActivity.this, FieldActivity.class);
            intent.putExtra("new_game", true);
            GapsSolitaireActivity.this.startActivity(intent);
        }
    };
    /* access modifiers changed from: private */
    public int xScreenSize;
    /* access modifiers changed from: private */
    public int yScreenSize;

    public void onResume() {
        super.onResume();
        TextView textView_resume = (TextView) findViewById(R.id.textView_resume_game);
        if (FileHelper.isSavedGameExist(this)) {
            textView_resume.setTextColor(Color.argb(255, 170, 221, 255));
        } else {
            textView_resume.setTextColor(Color.argb(255, 187, 187, 187));
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        PreferenceManager.getDefaultSharedPreferences(this).getClass();
        this.mContext = this;
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        this.xScreenSize = metrics.heightPixels;
        this.yScreenSize = metrics.widthPixels;
        this.dialogHowToPlay = new Dialog(this);
        ((LinearLayout) findViewById(R.id.main_menu_header)).setLayoutParams(new LinearLayout.LayoutParams(this.yScreenSize, (this.xScreenSize * 8) / 20));
        Typeface fontHotplate = Typeface.createFromAsset(getAssets(), "Hotplate.ttf");
        TextView textView_highscore = (TextView) findViewById(R.id.textView_high_score);
        textView_highscore.setOnClickListener(this.textView_high_score_click);
        textView_highscore.setTypeface(fontHotplate);
        TextView textView_start = (TextView) findViewById(R.id.textView_start_game);
        textView_start.setOnClickListener(this.textView_start_game_click);
        textView_start.setTypeface(fontHotplate);
        TextView textView_howtoplay = (TextView) findViewById(R.id.textView_howtoplay);
        textView_howtoplay.setOnClickListener(this.textView_howtoplay_click);
        textView_howtoplay.setTypeface(fontHotplate);
        TextView textView_resume = (TextView) findViewById(R.id.textView_resume_game);
        textView_resume.setTypeface(fontHotplate);
        textView_resume.setOnClickListener(this.textView_resume_game_click);
        if (!FileHelper.isSavedGameExist(this)) {
            textView_resume.setTextColor((int) R.color.inactive);
        }
    }
}
